* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva1bdr                                                        *
*              DRIVER IMPORT DOCUMENTI                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-26                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pFILE,pOBJXML,pSERIAL,pFUNRIC,pVERBOSE,pLOG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva1bdr",oParentObject,m.pCODSTR,m.pFILE,m.pOBJXML,m.pSERIAL,m.pFUNRIC,m.pVERBOSE,m.pLOG)
return(i_retval)

define class tgsva1bdr as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pFILE = space(254)
  pOBJXML = .NULL.
  pSERIAL = space(10)
  pFUNRIC = space(30)
  pVERBOSE = .f.
  pLOG = .NULL.
  w_OBJ_XML = .NULL.
  w_CODSTR = space(10)
  w_CODENT = space(15)
  w_TABPRI = space(30)
  w_OBJ_ROOT = .NULL.
  w_NODO_ROOT = space(30)
  w_CHILD_ROOT = 0
  w_TIPO = space(1)
  w_OBJ = .NULL.
  w_OK = .f.
  w_NODO = space(30)
  w_DFSERIAL = space(10)
  w_FLATAB = space(15)
  w_FL_AZI = space(1)
  w_RESULT = 0
  w_CODTAB = space(30)
  w_TIPFIL = space(1)
  w_FLATORI = space(15)
  w_FILE = space(100)
  w_PADIRXML = space(200)
  w_CODAZI = space(5)
  * --- WorkFile variables
  VASTRUTT_idx=0
  TMP_TAGELE_idx=0
  VAELEMEN_idx=0
  VAANAIMP_idx=0
  DOC_MAST_idx=0
  FLATMAST_idx=0
  ENT_DETT_idx=0
  FLATDETT_idx=0
  PAR_VEFA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice struttura
    * --- File di import
    * --- Oggetto XML
    * --- Seriale anagrafica che rappresenta il flusso
    * --- Funzione ricorsiva
    * --- Verbose per condizionare msg di log
    * --- Oggetto della maschera chiamante dove � definita la MSG di log
    this.w_RESULT = 0
    this.w_OBJ_XML = this.pOBJXML.oXML
    this.w_CODSTR = this.pCODSTR
    this.w_OK = .T.
    AddMsgNL("Elaborazione iniziata alle %1",this.pLog, Time() , , , ,, )
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STTIPFIL,STFLATAB,STCODENT"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STTIPFIL,STFLATAB,STCODENT;
        from (i_cTable) where;
            STCODICE = this.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      this.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ENT_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ENT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_DETT_idx,2],.t.,this.ENT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "EN_TABLE"+;
        " from "+i_cTable+" ENT_DETT where ";
            +"ENCODICE = "+cp_ToStrODBC(this.w_CODENT);
            +" and ENPRINCI = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        EN_TABLE;
        from (i_cTable) where;
            ENCODICE = this.w_CODENT;
            and ENPRINCI = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TABPRI = NVL(cp_ToDate(_read_.EN_TABLE),cp_NullValue(_read_.EN_TABLE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Verifica presenza delle chiavi nelle tabelle EDI
    if this.w_TIPFIL="X"
      if VarType( this.pLog ) ="O" 
        AddMsgNL("Creazione  tabella temporanea con elenco record importati",this.pLOG )
      endif
    endif
    * --- Create temporary table TMP_TAGELE
    i_nIdx=cp_AddTableDef('TMP_TAGELE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\VEFA\EXE\QUERY\GSVA1QTA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_TAGELE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Valorizzo oggetto XML
     
 DIMENSION ArProg(1,2) 
 ArProg[1,1]=this.w_TABPRI 
 ArProg[1,2]=0
    * --- Verifico che la radice abbia figli
    if Type("this.w_OBJ_XML.documentElement")<>"O"
      this.pOBJXML = createobject("Ah_XMLObject")
      if cp_fileexist(this.pfile) 
        this.pOBJXML.oxml.load(this.pFile)     
        this.w_OBJ_XML = this.pOBJXML.oxml
      else
        AddMsgNL("Attenzione il file selezionato non esiste!",this.pLOG)
        this.w_RESULT = -1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Read from FLATMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.FLATMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FLATMAST_idx,2],.t.,this.FLATMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FTFL_AZI"+;
        " from "+i_cTable+" FLATMAST where ";
            +"FTCODICE = "+cp_ToStrODBC(this.w_FLATAB);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FTFL_AZI;
        from (i_cTable) where;
            FTCODICE = this.w_FLATAB;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FL_AZI = NVL(cp_ToDate(_read_.FTFL_AZI),cp_NullValue(_read_.FTFL_AZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CHILD_ROOT = 0
    * --- Eseguo controllo presenza campi chiave nelle tabelle EDI inserite nella tabella piatta
    *     appena riscontro anomalia mi fermo e segnalo errore
    this.w_FLATORI = alltrim(this.w_FLATAB)
    this.w_FLATAB = IIF(this.w_FL_AZI="S","xxx"+Alltrim(this.w_FLATAB),alltrim(this.w_FLATAB))
    * --- Select from FLATDETT
    i_nConn=i_TableProp[this.FLATDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select FTTABNAM,FTCODICE  from "+i_cTable+" FLATDETT ";
          +" where FTCODICE="+cp_ToStrODBC(this.w_FLATORI)+"";
          +" group by FTCODICE,FTTABNAM";
          +" order by FTTABNAM";
           ,"_Curs_FLATDETT")
    else
      select FTTABNAM,FTCODICE from (i_cTable);
       where FTCODICE=this.w_FLATORI;
       group by FTCODICE,FTTABNAM;
       order by FTTABNAM;
        into cursor _Curs_FLATDETT
    endif
    if used('_Curs_FLATDETT')
      select _Curs_FLATDETT
      locate for 1=1
      do while not(eof())
      if cp_ExistTableDef( Nvl(_Curs_FLATDETT.FTTABNAM,Space(20)))
        this.w_RESULT = gsva_bcc(This,alltrim(this.w_FLATAB),_Curs_FLATDETT.FTTABNAM,this.pSERIAL,this.pVERBOSE,this.pLOG)
        if this.w_RESULT=-1
          AddMsgNL("Attenzione campi chiave non inseriti per tabella %1 ", this.pLog,Alltrim(Nvl(_Curs_FLATDETT.FTTABNAM," ")))
          exit
        endif
      endif
        select _Curs_FLATDETT
        continue
      enddo
      use
    endif
    if this.w_RESULT=-1
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Azzero tabelle Edi in funzione del seriale di importazione
    *     se rilevo errori eseguo la return del batch e non proseguo
    this.w_RESULT = DELRECIMP(alltrim(this.w_FLATAB),this.pSERIAL,this.pVERBOSE,this.pLOG)
    if this.w_RESULT=-1
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_OBJ_ROOT = this.w_OBJ_XML.documentElement
    if Type("This.w_OBJ_ROOT")="O"
      this.w_NODO_ROOT = IIF(this.w_TIPFIL<>"X",SUBSTR(this.w_obj_root.basename,2),this.w_obj_root.basename)
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EL__TIPO,EL_SEGNO"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
              +" and ELVALTXT = "+cp_ToStrODBC(this.w_NODO_ROOT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EL__TIPO,EL_SEGNO;
          from (i_cTable) where;
              ELCODSTR = this.w_CODSTR;
              and ELVALTXT = this.w_NODO_ROOT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPO = NVL(cp_ToDate(_read_.EL__TIPO),cp_NullValue(_read_.EL__TIPO))
        w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do while this.w_CHILD_ROOT<this.w_OBJ_ROOT.ChildNodes.Length and this.w_RESULT<>-1
        this.w_OBJ = this.w_OBJ_ROOT.ChildNodes.Item(this.w_CHILD_ROOT)
        if this.w_TIPFIL="X"
          this.w_NODO = this.w_obj.basename
          * --- Read from VAELEMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAELEMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EL__TIPO,EL_SEGNO,ELTABIMP"+;
              " from "+i_cTable+" VAELEMEN where ";
                  +"ELCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                  +" and ELVALTXT = "+cp_ToStrODBC(this.w_NODO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EL__TIPO,EL_SEGNO,ELTABIMP;
              from (i_cTable) where;
                  ELCODSTR = this.w_CODSTR;
                  and ELVALTXT = this.w_NODO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPO = NVL(cp_ToDate(_read_.EL__TIPO),cp_NullValue(_read_.EL__TIPO))
            w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
            this.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_NODO = SUBSTR(this.w_obj.basename,2)
          * --- Read from VAELEMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAELEMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EL__TIPO,EL_SEGNO,ELTABIMP"+;
              " from "+i_cTable+" VAELEMEN where ";
                  +"ELCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                  +" and ELCODICE = "+cp_ToStrODBC(this.w_NODO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EL__TIPO,EL_SEGNO,ELTABIMP;
              from (i_cTable) where;
                  ELCODSTR = this.w_CODSTR;
                  and ELCODICE = this.w_NODO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPO = NVL(cp_ToDate(_read_.EL__TIPO),cp_NullValue(_read_.EL__TIPO))
            w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
            this.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if this.w_TIPO="O"
          * --- Ho pi� di un documento nel file
          this.w_DFSERIAL = SPACE(10)
          * --- Try
          local bErr_035B1228
          bErr_035B1228=bTrsErr
          this.Try_035B1228()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            AddMsgNL("Errore nella creazione del progressivo",this.pLOG )
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_035B1228
          * --- End
           
 DIMENSION ArProg(1,2) 
 ArProg[1,1]=this.w_TABPRI 
 ArProg[1,2]=0
        endif
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_FUN="This.w_OK = "+ALLTRIM(this.pfunric)+" (This.w_OBJ, ALLTRIM(this.w_CODSTR),ALLTRIM(this.w_DFSERIAL),ALLTRIM(this.w_CODTAB),"+; 
 "ALLTRIM(this.pSERIAL),this.pVERBOSE,this.pLOG,iif(This.w_TIPFIL='X',.f.,.t.))" 
 &L_FUN 
 on error &L_OldError 
        if L_err
          addMsg("%0Errore nell'esecuzione della funzione ricorsiva: %1" ,this.pLOG,Message())
        endif
        this.w_CHILD_ROOT = this.w_CHILD_ROOT + 1
        if Not this.w_OK
          this.w_RESULT = -1
        endif
      enddo
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      AddMsgNL("Oggetto root xml non presente nel file",this.pLOG )
      this.w_RESULT = -1
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    AddMsgNL("Elaborazione finita alle %1",this.pLog, Time() , , , ,, )
  endproc
  proc Try_035B1228()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "DELFO", "i_codazi,w_DFSERIAL")
    * --- Array utilizzato per memorizzare indici tabelle
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if VarType( "pSERIAL" ) = "C"
      this.w_CODAZI = i_CODAZI
      this.w_PADIRXML = ""
      * --- Read from PAR_VEFA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VEFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VEFA_idx,2],.t.,this.PAR_VEFA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PADIRXML"+;
          " from "+i_cTable+" PAR_VEFA where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PADIRXML;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PADIRXML = NVL(cp_ToDate(_read_.PADIRXML),cp_NullValue(_read_.PADIRXML))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not EMPTY(NVL(this.w_PADIRXML, " "))
        this.w_FILE = ADDBS(ALLTRIM(this.w_PADIRXML))
      else
        this.w_FILE = ALLTRIM(ADDBS(SYS(2023)))
      endif
      this.w_FILE = this.w_FILE + Alltrim(this.pSERIAL) + ".XML"
      this.pOBJXML.SaveXml(this.w_FILE)     
      * --- Try
      local bErr_037B8110
      bErr_037B8110=bTrsErr
      this.Try_037B8110()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        AddMsgNL("Attenzione errore nella scrittura path file XML ", this.pLog )
      endif
      bTrsErr=bTrsErr or bErr_037B8110
      * --- End
    endif
    if this.w_RESULT=-1
      * --- Azzero oggetto XML
      this.pOBJXML.DestroyXml()     
    endif
    AddMsgNL("Elaborazione finita alle %1",this.pLog, Time() , , , ,, )
    * --- Drop temporary table TMP_TAGELE
    i_nIdx=cp_GetTableDefIdx('TMP_TAGELE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_TAGELE')
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc
  proc Try_037B8110()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMPATXML ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'VAANAIMP','IMPATXML');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
             )
    else
      update (i_cTable) set;
          IMPATXML = this.w_FILE;
          &i_ccchkf. ;
       where;
          IMSERIAL = this.pSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pCODSTR,pFILE,pOBJXML,pSERIAL,pFUNRIC,pVERBOSE,pLOG)
    this.pCODSTR=pCODSTR
    this.pFILE=pFILE
    this.pOBJXML=pOBJXML
    this.pSERIAL=pSERIAL
    this.pFUNRIC=pFUNRIC
    this.pVERBOSE=pVERBOSE
    this.pLOG=pLOG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='*TMP_TAGELE'
    this.cWorkTables[3]='VAELEMEN'
    this.cWorkTables[4]='VAANAIMP'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='FLATMAST'
    this.cWorkTables[7]='ENT_DETT'
    this.cWorkTables[8]='FLATDETT'
    this.cWorkTables[9]='PAR_VEFA'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_FLATDETT')
      use in _Curs_FLATDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pFILE,pOBJXML,pSERIAL,pFUNRIC,pVERBOSE,pLOG"
endproc
