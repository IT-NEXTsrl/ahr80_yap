* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bpa                                                        *
*              SELEZIONA PERCORSI PARAMETRI EDI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-01                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pDESTIN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bpa",oParentObject,m.pOPER,m.pDESTIN)
return(i_retval)

define class tgsva_bpa as StdBatch
  * --- Local variables
  pOPER = space(5)
  pDESTIN = space(10)
  w_PADRE = .NULL.
  w_OCTRL = .NULL.
  w_SELFLD = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="SELFI"
        this.w_OCTRL = this.w_PADRE.GetCtrl("w_"+ALLTRIM(this.pDESTIN))
        this.w_SELFLD = ""
        this.w_SELFLD = cp_GETDIR(ALLTRIM(this.w_OCTRL.Value) , ALLTRIM(this.w_OCTRL.Tooltiptext) , "")
        if !EMPTY(NVL(this.w_SELFLD," "))
          L_MAC="this.oParentObject.w_"+ALLTRIM(this.pDESTIN)+" = '" +this.w_SELFLD + "'"
          &L_MAC
          this.w_OCTRL.Value = ADDBS(Alltrim(this.w_SELFLD))
        endif
        this.w_OCTRL = .NULL.
    endcase
    this.w_PADRE = .NULL.
  endproc


  proc Init(oParentObject,pOPER,pDESTIN)
    this.pOPER=pOPER
    this.pDESTIN=pDESTIN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pDESTIN"
endproc
