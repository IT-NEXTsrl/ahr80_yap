* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_afo                                                        *
*              Formati                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_23]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2015-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_afo"))

* --- Class definition
define class tgsva_afo as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 694
  Height = 351+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-08"
  HelpContextID=209030551
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  VAFORMAT_IDX = 0
  VASTRUTT_IDX = 0
  cFile = "VAFORMAT"
  cKeySelect = "FOCODSTR,FOCODICE"
  cKeyWhere  = "FOCODSTR=this.w_FOCODSTR and FOCODICE=this.w_FOCODICE"
  cKeyWhereODBC = '"FOCODSTR="+cp_ToStrODBC(this.w_FOCODSTR)';
      +'+" and FOCODICE="+cp_ToStrODBC(this.w_FOCODICE)';

  cKeyWhereODBCqualified = '"VAFORMAT.FOCODSTR="+cp_ToStrODBC(this.w_FOCODSTR)';
      +'+" and VAFORMAT.FOCODICE="+cp_ToStrODBC(this.w_FOCODICE)';

  cPrg = "gsva_afo"
  cComment = "Formati"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FOCODSTR = space(10)
  w_FOCODICE = space(20)
  w_FODESCRI = space(30)
  w_DESSTR = space(30)
  w_FLPROV = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Children pointers
  GSVA_MDF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VAFORMAT','gsva_afo')
    stdPageFrame::Init()
    *set procedure to GSVA_MDF additive
    with this
      .Pages(1).addobject("oPag","tgsva_afoPag1","gsva_afo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Formato")
      .Pages(1).HelpContextID = 151379626
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCODSTR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVA_MDF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAFORMAT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VAFORMAT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VAFORMAT_IDX,3]
  return

  function CreateChildren()
    this.GSVA_MDF = CREATEOBJECT('stdDynamicChild',this,'GSVA_MDF',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSVA_MDF.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVA_MDF)
      this.GSVA_MDF.DestroyChildrenChain()
      this.GSVA_MDF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVA_MDF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVA_MDF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVA_MDF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVA_MDF.SetKey(;
            .w_FOCODSTR,"DFCODSTR";
            ,.w_FOCODICE,"DFCODFOR";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVA_MDF.ChangeRow(this.cRowID+'      1',1;
             ,.w_FOCODSTR,"DFCODSTR";
             ,.w_FOCODICE,"DFCODFOR";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSVA_MDF)
        i_f=.GSVA_MDF.BuildFilter()
        if !(i_f==.GSVA_MDF.cQueryFilter)
          i_fnidx=.GSVA_MDF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVA_MDF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVA_MDF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVA_MDF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVA_MDF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FOCODSTR = NVL(FOCODSTR,space(10))
      .w_FOCODICE = NVL(FOCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VAFORMAT where FOCODSTR=KeySet.FOCODSTR
    *                            and FOCODICE=KeySet.FOCODICE
    *
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VAFORMAT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VAFORMAT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VAFORMAT '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FOCODSTR',this.w_FOCODSTR  ,'FOCODICE',this.w_FOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESSTR = space(30)
        .w_FLPROV = space(1)
        .w_FOCODSTR = NVL(FOCODSTR,space(10))
          if link_1_1_joined
            this.w_FOCODSTR = NVL(STCODICE101,NVL(this.w_FOCODSTR,space(10)))
            this.w_DESSTR = NVL(STDESCRI101,space(30))
            this.w_FLPROV = NVL(STFLPROV101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_FOCODICE = NVL(FOCODICE,space(20))
        .w_FODESCRI = NVL(FODESCRI,space(30))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'VAFORMAT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsva_afo
    * --- Propone l'ultima struttura Inserita
    p_OLDSTR=space(10)
    if p_OLDSTR<>this.w_FOCODSTR
       p_OLDSTR=this.w_FOCODSTR
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FOCODSTR = space(10)
      .w_FOCODICE = space(20)
      .w_FODESCRI = space(30)
      .w_DESSTR = space(30)
      .w_FLPROV = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_FOCODSTR = IIF(p_OLDSTR<>space(10), p_OLDSTR, CERSTRUPRE('1 ASC'))
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_FOCODSTR))
          .link_1_1('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'VAFORMAT')
    this.DoRTCalc(2,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFOCODSTR_1_1.enabled = i_bVal
      .Page1.oPag.oFOCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oFODESCRI_1_5.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFOCODSTR_1_1.enabled = .f.
        .Page1.oPag.oFOCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFOCODSTR_1_1.enabled = .t.
        .Page1.oPag.oFOCODICE_1_3.enabled = .t.
      endif
    endwith
    this.GSVA_MDF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'VAFORMAT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVA_MDF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FOCODSTR,"FOCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FOCODICE,"FOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FODESCRI,"FODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    i_lTable = "VAFORMAT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VAFORMAT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVA_SFO with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VAFORMAT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VAFORMAT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VAFORMAT')
        i_extval=cp_InsertValODBCExtFlds(this,'VAFORMAT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FOCODSTR,FOCODICE,FODESCRI,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_FOCODSTR)+;
                  ","+cp_ToStrODBC(this.w_FOCODICE)+;
                  ","+cp_ToStrODBC(this.w_FODESCRI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VAFORMAT')
        i_extval=cp_InsertValVFPExtFlds(this,'VAFORMAT')
        cp_CheckDeletedKey(i_cTable,0,'FOCODSTR',this.w_FOCODSTR,'FOCODICE',this.w_FOCODICE)
        INSERT INTO (i_cTable);
              (FOCODSTR,FOCODICE,FODESCRI,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FOCODSTR;
                  ,this.w_FOCODICE;
                  ,this.w_FODESCRI;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VAFORMAT_IDX,i_nConn)
      *
      * update VAFORMAT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VAFORMAT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FODESCRI="+cp_ToStrODBC(this.w_FODESCRI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VAFORMAT')
        i_cWhere = cp_PKFox(i_cTable  ,'FOCODSTR',this.w_FOCODSTR  ,'FOCODICE',this.w_FOCODICE  )
        UPDATE (i_cTable) SET;
              FODESCRI=this.w_FODESCRI;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVA_MDF : Saving
      this.GSVA_MDF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_FOCODSTR,"DFCODSTR";
             ,this.w_FOCODICE,"DFCODFOR";
             )
      this.GSVA_MDF.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVA_MDF : Deleting
    this.GSVA_MDF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_FOCODSTR,"DFCODSTR";
           ,this.w_FOCODICE,"DFCODFOR";
           )
    this.GSVA_MDF.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VAFORMAT_IDX,i_nConn)
      *
      * delete VAFORMAT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FOCODSTR',this.w_FOCODSTR  ,'FOCODICE',this.w_FOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FOCODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FOCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_FOCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPROV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_FOCODSTR))
          select STCODICE,STDESCRI,STFLPROV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FOCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FOCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oFOCODSTR_1_1'),i_cWhere,'GSVA_AST',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPROV";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FOCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_FOCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_FOCODSTR)
            select STCODICE,STDESCRI,STFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FOCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_FLPROV = NVL(_Link_.STFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_FOCODSTR = space(10)
      endif
      this.w_DESSTR = space(30)
      this.w_FLPROV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FOCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.STCODICE as STCODICE101"+ ",link_1_1.STDESCRI as STDESCRI101"+ ",link_1_1.STFLPROV as STFLPROV101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on VAFORMAT.FOCODSTR=link_1_1.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and VAFORMAT.FOCODSTR=link_1_1.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCODSTR_1_1.value==this.w_FOCODSTR)
      this.oPgFrm.Page1.oPag.oFOCODSTR_1_1.value=this.w_FOCODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODICE_1_3.value==this.w_FOCODICE)
      this.oPgFrm.Page1.oPag.oFOCODICE_1_3.value=this.w_FOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oFODESCRI_1_5.value==this.w_FODESCRI)
      this.oPgFrm.Page1.oPag.oFODESCRI_1_5.value=this.w_FODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_7.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_7.value=this.w_DESSTR
    endif
    cp_SetControlsValueExtFlds(this,'VAFORMAT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_FLPROV='S')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, struttura confermata impossibile caricare")
          case   (empty(.w_FOCODSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODSTR_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FOCODSTR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_FOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVA_MDF.CheckForm()
      if i_bres
        i_bres=  .GSVA_MDF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSVA_MDF : Depends On
    this.GSVA_MDF.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile modificare"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile eliminare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsva_afoPag1 as StdContainer
  Width  = 690
  height = 354
  stdWidth  = 690
  stdheight = 354
  resizeXpos=354
  resizeYpos=222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCODSTR_1_1 as StdField with uid="WMMVCDTHQY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FOCODSTR", cQueryName = "FOCODSTR",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 200732248,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=171, Top=25, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_FOCODSTR"

  func oFOCODSTR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oFOCODSTR_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFOCODSTR_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oFOCODSTR_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Strutture",'',this.parent.oContained
  endproc
  proc oFOCODSTR_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_FOCODSTR
     i_obj.ecpSave()
  endproc

  add object oFOCODICE_1_3 as StdField with uid="DXXQQAMEHZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FOCODICE", cQueryName = "FOCODSTR,FOCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 100068965,;
   bGlobalFont=.t.,;
    Height=21, Width=164, Left=171, Top=51, InputMask=replicate('X',20)

  add object oFODESCRI_1_5 as StdField with uid="KSNBKYKRWG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FODESCRI", cQueryName = "FODESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 185654881,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=337, Top=51, InputMask=replicate('X',30)


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="JUIKWTFZND",left=1, top=82, width=686, height=272, bOnScreen=.t.;


  add object oDESSTR_1_7 as StdField with uid="QXTALHZSJD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 200407242,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=275, Top=25, InputMask=replicate('X',30)

  add object oStr_1_2 as StdString with uid="TRKPYDEIYC",Visible=.t., Left=3, Top=29,;
    Alignment=1, Width=167, Height=18,;
    Caption="Codice struttura:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="VSZHYTXDTE",Visible=.t., Left=3, Top=55,;
    Alignment=1, Width=167, Height=18,;
    Caption="Codice formato:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_afo','VAFORMAT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FOCODSTR=VAFORMAT.FOCODSTR";
  +" and "+i_cAliasName2+".FOCODICE=VAFORMAT.FOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
