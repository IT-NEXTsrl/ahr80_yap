* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bee                                                        *
*              IMPORT/EXPORT DEFINIZIONE TABELLE PIATTE                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-02                                                      *
* Last revis.: 2012-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bee",oParentObject,m.pOper)
return(i_retval)

define class tgsva_bee as StdBatch
  * --- Local variables
  pOper = space(5)
  w_READ_RES = space(254)
  w_NOERROR = .f.
  * --- WorkFile variables
  FLATMAST_idx=0
  FLATDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="SELEZ" Or this.pOPER=="DESEL" Or this.pOPER=="INVSE"
        UPDATE (this.oParentObject.w_EDITABLE.cCursor) SET XCHK=ICASE(this.pOPER=="SELEZ", 1, this.pOPER=="DESEL", 0, IIF(XCHK=0,1,0))
      case this.pOPER=="ELABO"
        this.oParentObject.w_MSG = ""
        this.oParentObject.oPGFRM.ActivePage=2
        this.w_NOERROR = .T.
        if this.oParentObject.w_RADSELIE1 = "E"
          addMsgNL("Inizio esportazione tabelle %1" ,This,TTOC(DATETIME()))
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          addMsgNL("Inizio importazione tabelle %1" ,This,TTOC(DATETIME()))
          * --- begin transaction
          cp_BeginTrs()
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_NOERROR and !bTrsErr
            * --- commit
            cp_EndTrs(.t.)
          else
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
        endif
        wait clear
        if this.oParentObject.w_RADSELIE1="E"
          addMsg("Fine esportazione tabelle %1" ,This,TTOC(DATETIME()))
          if this.w_NOERROR
            ah_ErrorMsg("Procedura di esportazione terminata con successo",64,"")
          else
            ah_ErrorMsg("Errore durante l'esportazione delle tabelle",48,"")
          endif
        else
          addMsg("Fine importazione tabelle %1" ,This,TTOC(DATETIME()))
          if this.w_NOERROR
            ah_ErrorMsg("Procedura di importazione terminata con successo",64,"")
          else
            ah_ErrorMsg("Errore durante l'importazione delle tabelle",48,"")
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Salvo le sole tabelle selezionate per poi effettuare la inner join
    SELECT DISTINCT FTCODICE AS TABSEL FROM (this.oParentObject.w_EDITABLE.cCursor) WHERE XCHK=1 INTO CURSOR "_SelTab_"
    this.w_READ_RES = ""
    * --- Leggo dati tabella master
    addMsgNL("Lettura dati testata" ,This)
    this.w_READ_RES = READTABLE("FLATMAST","*","",This, this.oParentObject.w_FLVERBOS ,"1=1")
    if EMPTY(this.w_READ_RES)
      addMsgNL("Errore durante la lettura dei dati della tabella FLATMAST" ,This)
      this.w_NOERROR = .F.
    else
      * --- Estraggo i soli dati delle tabelle selezionate
      addMsgNL("Eliminazione tabelle non selezionate (testata)" ,This)
      L_MAC = (this.w_READ_RES)+".* "
      SELECT &L_MAC FROM (this.w_READ_RES) Inner Join "_SelTab_" on FTCODICE=_Seltab_.TABSEL INTO CURSOR "FLATMAST"
      * --- Salvo il cursore
      if used("FLATMAST")
        addMsgNL("Salvataggio dati testata" ,This)
        select "FLATMAST"
        go top
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 copy to (alltrim(this.oParentObject.w_DBF1)) TYPE FOX2X 
 on error &L_OldError 
        if l_err
          ah_ErrorMsg("Impossibile creare il file %1 nella cartella indicata", "" , "", Alltrim((this.oParentObject.w_DBF1)))
          this.w_NOERROR = .F.
        endif
        USE IN "FLATMAST"
      else
        this.w_NOERROR = .F.
      endif
      USE IN (this.w_READ_RES)
      this.w_READ_RES = ""
      if this.w_NOERROR
        * --- Leggo dati tabella detail
        this.w_READ_RES = READTABLE("FLATDETT","*","", This, this.oParentObject.w_FLVERBOS,"1=1")
        if EMPTY(this.w_READ_RES)
          addMsgNL("Errore durante la lettura dei dati della tabella FLATDETT",This)
          this.w_NOERROR = .F.
        else
          * --- Estraggo i soli dati delle tabelle selezionate
          addMsgNL("Eliminazione tabelle non selezionate (dettaglio)" ,This)
          L_MAC = (this.w_READ_RES)+".* "
          SELECT &L_MAC FROM (this.w_READ_RES) Inner Join "_SelTab_" on FTCODICE=_Seltab_.TABSEL INTO CURSOR "FLATDETT"
          * --- Salvo il cursore
          if used("FLATDETT")
            addMsgNL("Salvataggio dati dettaglio" ,This)
            select "FLATDETT"
            go top
             
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 copy to (alltrim(this.oParentObject.w_DBF11)) TYPE FOX2X 
 on error &L_OldError 
            if l_err
              ah_ErrorMsg("Impossibile creare il file %1 nella cartella indicata", "" , "", Alltrim((this.oParentObject.w_DBF11)))
              this.w_NOERROR = .F.
            endif
            USE IN "FLATDETT"
          else
            this.w_NOERROR = .F.
          endif
          USE IN (this.w_READ_RES)
          if used("_SelTab_")
            USE IN "_SelTab_"
          endif
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    use (ALLTRIM(this.oParentObject.w_DBF1)) alias "FLATMAST" IN 0
    use (ALLTRIM(this.oParentObject.w_DBF11)) alias "FLATDETT" IN 0
    if !used("FLATMAST") OR !used("FLATDETT") OR RECCOUNT("FLATMAST")<1 OR RECCOUNT("FLATDETT")<1
      addmsgnl("Errore durante la lettura dei dati dai file d'ingresso o file inesistenti", this)
    else
      SELECT "FLATMAST"
      GO TOP
      do while !EOF() and this.w_NOERROR
        addmsgnl("Analisi dati di testata (%1 di %2)", this, ALLTRIM(STR(RECNO())), ALLTRIM(STR(RECCOUNT())))
        * --- Try
        local bErr_035EDC10
        bErr_035EDC10=bTrsErr
        this.Try_035EDC10()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if this.w_NOERROR
            * --- accept error
            bTrsErr=.f.
            if this.oParentObject.w_FLDELDEF
              PUBLIC ARRAY ARRFILTER(1,2)
              ARRFILTER(1,1)= "FTCODICE"
              ARRFILTER(1,2)= ALLTRIM(FLATMAST.FTCODICE)
              addmsgnl(CHR(9)+"Aggiornamento campi di testata", this)
              L_RESWRITE=-1000
              if !EMPTY(writetable("FLATMAST", @ARRINS, @ARRFILTER,This, this.oParentObject.w_FLVERBOS,"", @L_RESWRITE)) OR L_RESWRITE<1
                this.w_NOERROR = .F.
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=ah_MsgFormat("Errore aggiornamento tabella FLATMAST")
              endif
            endif
          else
            this.w_NOERROR = .F.
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat("Errore creazione array campi di testata")
          endif
        endif
        bTrsErr=bTrsErr or bErr_035EDC10
        * --- End
        RELEASE ARRINS
        RELEASE ARRFILTER
        SELECT "FLATMAST"
        SKIP
      enddo
      USE IN "FLATMAST"
      if this.w_NOERROR
        SELECT "FLATDETT"
        GO TOP
        do while !EOF()
          addmsgnl("Analisi dati dettaglio (%1 di %2)", this, ALLTRIM(STR(RECNO())), ALLTRIM(STR(RECCOUNT())))
          * --- Try
          local bErr_037C3590
          bErr_037C3590=bTrsErr
          this.Try_037C3590()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            if this.w_NOERROR
              * --- accept error
              bTrsErr=.f.
              if this.oParentObject.w_FLDELDEF
                PUBLIC ARRAY ARRFILTER(2,2)
                ARRFILTER(1,1)= "FTTABNAM"
                ARRFILTER(1,2)= ALLTRIM(FLATDETT.FTTABNAM)
                ARRFILTER(2,1)= "FTFLDNAM"
                ARRFILTER(2,2)= ALLTRIM(FLATDETT.FTFLDNAM)
                addmsgnl(CHR(9)+"Aggiornamento campi dettaglio", this)
                if !EMPTY(writetable("FLATDETT", @ARRINS, @ARRFILTER, This, this.oParentObject.w_FLVERBOS))
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=ah_MsgFormat("Errore aggiornamento tabella FLATDETT")
                endif
              endif
            else
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=ah_MsgFormat("Errore creazione array campi dettaglio")
            endif
          endif
          bTrsErr=bTrsErr or bErr_037C3590
          * --- End
          RELEASE ARRINS
          RELEASE ARRFILTER
          SELECT "FLATDETT"
          SKIP
        enddo
      endif
      USE IN "FLATDETT"
    endif
    * --- Aggiorno dizionario
    gsva_bcx(this," ",.t.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_035EDC10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    PUBLIC ARRAY ARRINS(1,1)
    if curtoarr("FLATMAST",0,@ARRINS)<0
      addmsgnl("Errore creazione array campi di testata", this)
      this.w_NOERROR = .F.
      * --- Raise
      i_Error="CreaArrErr"
      return
    endif
    addmsgnl(CHR(9)+"Inserimento campi di testata", this)
    if !EMPTY(inserttable("FLATMAST", @ARRINS, This, this.oParentObject.w_FLVERBOS))
      * --- Raise
      i_Error="InsMastErr"
      return
    endif
    return
  proc Try_037C3590()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    PUBLIC ARRAY ARRINS(1,1)
    if curtoarr("FLATDETT",RECNO(),@ARRINS)<0
      addmsgnl("Errore creazione array campi dettaglio", this)
      this.w_NOERROR = .F.
      * --- Raise
      i_Error="CreaArrErr"
      return
    endif
    addmsgnl(CHR(9)+"Inserimento campi dettaglio", this)
    if !EMPTY(inserttable("FLATDETT", @ARRINS, This, this.oParentObject.w_FLVERBOS))
      * --- Raise
      i_Error="InsDettErr"
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='FLATMAST'
    this.cWorkTables[2]='FLATDETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
