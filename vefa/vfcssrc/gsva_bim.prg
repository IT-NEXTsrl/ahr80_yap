* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bim                                                        *
*              IMPORT FILE EDI                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_557]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-03                                                      *
* Last revis.: 2018-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFILE,pCODSTR,pTIPOPE,pGranu
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bim",oParentObject,m.pFILE,m.pCODSTR,m.pTIPOPE,m.pGranu)
return(i_retval)

define class tgsva_bim as StdBatch
  * --- Local variables
  pFILE = space(100)
  w_FIELD = space(10)
  pCODSTR = space(10)
  pTIPOPE = space(5)
  pGranu = space(1)
  w_OBJROOT = .NULL.
  w_OBJROOT = .NULL.
  w_HANDLEROOT = 0
  w_ROOTLEN = 0
  w_I = 0
  w_MVSERIAL = space(10)
  w_ErrCode = 0
  w_Risultato = space(254)
  w_TIPFIL = space(1)
  w_ELEMENTO = space(20)
  w_LIMITE = 0
  w_CODSTR = space(10)
  w_CODNOR = space(10)
  w_BATPRE = space(50)
  w_BATPOS = space(50)
  w_BATCHK = space(50)
  w_FLATAB = space(15)
  w_DESCOD = space(41)
  w_DESSUP = space(41)
  w_CODLIN = space(3)
  w_oERRORLOG = .NULL.
  w_EMESS = space(0)
  w_PADRE = .NULL.
  w_oERRORLOG = .NULL.
  w_SERIAL = space(10)
  pDOCINFO = .NULL.
  PAR = .NULL.
  w_RET = .NULL.
  w_contrate = 0
  w_OKDOC = .f.
  w_LOG = space(0)
  w_TEST = .f.
  w_NUMREC = 0
  w_SERGDOC = space(10)
  w_GDKEYRIF = space(10)
  w_OKBGE = .f.
  w_OLDSERLOG = space(10)
  w_LGTIPRIG = space(1)
  w_LGMESSAG = space(254)
  w_LGSERORI = space(10)
  w_LGROWORI = 0
  pDOCINFO = .NULL.
  PAR = .NULL.
  w_contrate = 0
  w_MATRICOLA = space(40)
  w_RIGA = 0
  w_TIPREC = space(1)
  w_OKTRAD = .f.
  w_COMAG = space(5)
  w_COMAT = space(5)
  w_MVRIFDIC = space(15)
  w_MVCODIVE = space(5)
  w_MVCODDES = space(5)
  w_MVCODAGE = space(5)
  w_MVSCOPAG = 0
  w_MVSPEINC = 0
  w_MVIVAIMB = space(5)
  w_MVFLNOAN = space(1)
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_PRODOC = space(2)
  w_FLPDOC = space(1)
  w_MVPRD = space(2)
  MVCODBAN = space(10)
  MVCODBA2 = space(15)
  w_MVTCOLIS = space(5)
  w_DOCLIS = space(5)
  w_CLFLIS = space(5)
  w_XCONORN = space(15)
  w_MVCODORN = space(15)
  w_MVTIPORN = space(15)
  w_MVTIPDOC = space(5)
  w_MVCAOVAL = 0
  w_MVCODPAG = space(5)
  w_MVCLADOC = space(2)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVCODPAG = space(5)
  w_MVCAUCON = space(5)
  w_TDCODMAG = space(5)
  w_MVTCAMAG = space(5)
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVFLVEAC = space(1)
  w_MVTFRAGG = space(1)
  w_MVCODVAL = space(5)
  w_MVVALNAZ = space(5)
  w_MV_SEGNO = space(1)
  w_MVFLPROV = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVANNPRO = 0
  w_MVANNDOC = 0
  w_MVCODESE = space(5)
  w_MVALFEST = space(10)
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_FLPPRO = space(1)
  w_TIPREG = space(1)
  w_MVPRP = space(2)
  w_MVDATEST = ctod("  /  /  ")
  w_CFLPP = space(1)
  w_DFLPP = space(1)
  w_MAGTER = space(5)
  w_FLMGPR = space(1)
  w_FLMTPR = space(1)
  w_MVFLSCAF = space(1)
  w_TDFLSCOR = space(1)
  w_MVFLSCOR = space(1)
  w_CATCLI = space(5)
  w_CATCOM = space(3)
  w_PRZVAC = space(10)
  w_FLSCOR = space(1)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(15)
  w_DECUNI = 0
  w_DECTOT = 0
  w_MVDATCIV = ctod("  /  /  ")
  w_MVIVABOL = space(5)
  w_MVIVACAU = space(5)
  w_MVMINTRA = space(2)
  w_MVORATRA = space(2)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVFLSCOM = space(1)
  w_DMVFLNOAN = space(1)
  w_DMVSCOCL1 = 0
  w_DMVSCOCL2 = 0
  w_DMVCODBAN = space(10)
  w_DMVCODBA2 = space(15)
  w_DMVCODPAG = space(5)
  w_DMVCLADOC = space(2)
  w_DMVTIPCON = space(1)
  w_DMVCODPAG = space(5)
  w_DMVCAUCON = space(5)
  w_TDCODMAG = space(5)
  w_DMVTCAMAG = space(5)
  w_DMVFLACCO = space(1)
  w_DMVFLINTE = space(1)
  w_DMVFLVEAC = space(1)
  w_DMVTFRAGG = space(1)
  w_DMVCODVAL = space(5)
  w_DMVALNAZ = space(5)
  w_DMV_SEGNO = space(1)
  w_DMVANNPRO = 0
  w_DMVANNDOC = 0
  w_DMVCODESE = space(5)
  w_DMVALFEST = space(10)
  w_DMVCODORN = space(15)
  w_FLELAN = space(1)
  w_MVFLELAN = space(1)
  w_ALFDOC = space(10)
  w_MVALFDOC = space(10)
  w_MVFLRIMB = space(1)
  w_MVFLRTRA = space(1)
  w_MVFLRINC = space(1)
  w_MV__ANNO = 0
  w_MV__MESE = 0
  w_MVTIPDIS = space(2)
  w_MVSTFILCB = space(1)
  w_MVFLGINC = space(1)
  w_TDRIPCON = space(1)
  w_MVEMERIC = space(1)
  w_PREDEF = space(1)
  w_MCALSI4 = space(1)
  w_CODNAZ1 = space(1)
  w_MCALST4 = space(1)
  w_CODAGE = space(5)
  w_CODPOR = space(5)
  w_CODVET = space(5)
  w_CODSPE = space(5)
  w_AGENTE = space(5)
  w_MVCODVET = space(5)
  w_MVCODSPE = space(5)
  w_MVCODPOR = space(5)
  w_DATCAL = ctod("  /  /  ")
  w_MVFLSEND = space(1)
  w_MVNUMDOC = 0
  w_IVACLI = space(5)
  w_ANCODAG = space(5)
  w_DMVCONCON = space(1)
  w_MVCONCON = space(1)
  w_DMVNUMCOR = space(25)
  w_DMVSPEINC = 0
  w_AGEPRO = space(1)
  w_AGSCOPAG = space(1)
  w_AGCZOAGE = space(5)
  w_TIPOPE = space(1)
  w_NDIC = 0
  w_ALFADIC = space(2)
  w_MESS = space(10)
  w_IMPDIC = 0
  w_ADIC = space(4)
  w_NUCON = 0
  w_IMPUTI = 0
  w_DDIC = ctod("  /  /  ")
  w_APPO = space(1)
  w_CODIVE = space(5)
  w_TDIC = space(1)
  w_OK = .f.
  w_RIFDIC = space(10)
  w_DICODCON = space(15)
  w_DITIPCON = space(1)
  w_DIDATDOC = ctod("  /  /  ")
  w_CODIVE = space(5)
  w_OK_LET = .f.
  w_OK_LET = .f.
  w_DIDATINI = ctod("  /  /  ")
  w_DIDATFIN = ctod("  /  /  ")
  w_INDIVE = 0
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_MVSCOPAG = 0
  w_CAONAZ = 0
  w_MVIVATRA = space(5)
  w_MVIVAINC = space(5)
  w_PERIMB = 0
  w_PERTRA = 0
  w_PERINC = 0
  w_MVSERIAL = 0
  w_MVIMPSCO = 0
  w_CPROWNUM = 0
  w_MVCODMAT = space(5)
  w_MVCODMAG = space(5)
  w_MVFLOMAG = space(1)
  w_MVNUMRIF = 0
  w_MVCODIVA = space(5)
  w_MVTIPRIG = space(1)
  w_MVVALRIG = 0
  w_MVCODICE = space(41)
  w_MVPERPRO = 0
  w_MVCODART = space(20)
  w_MVIMPPRO = 0
  w_MVDESART = space(40)
  w_MVSERRIF = space(10)
  w_MVDESSUP = space(10)
  w_MVROWRIF = 0
  w_MVUNIMIS = space(3)
  w_MVPESNET = 0
  w_MVCATCON = space(5)
  w_MVFLTRAS = space(1)
  w_MVCONTRO = space(15)
  w_MVNOMENC = space(8)
  w_MVCODCLA = space(3)
  w_MVUMSUPP = space(3)
  w_MVCONTRA = space(15)
  w_MVMOLSUP = 0
  w_MVCODLIS = space(5)
  w_MVNUMCOL = 0
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVCONIND = space(15)
  w_MVPREZZO = 0
  w_MVVALMAG = 0
  w_MVSCONT1 = 0
  w_MVIMPNAZ = 0
  w_MVSCONT2 = 0
  w_MVIMPACC = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVCODCOM = space(15)
  w_MVIMPCOM = 0
  w_MVIMPAC2 = 0
  w_UMCAL = space(3)
  w_MVCAUMAG = space(5)
  w_MVVOCCEN = space(15)
  w_MVCAUCOL = space(5)
  w_MVFLRISE = space(1)
  w_MVFLCASC = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2RISE = space(1)
  w_MV_FLAGG = space(1)
  w_MVCODART = space(20)
  w_MVKEYSAL = space(20)
  w_OPERA3 = space(1)
  w_OPERA2 = space(1)
  w_OPERA1 = space(1)
  w_UNMIS3 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS1 = space(3)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_MVVOCRIC = space(15)
  w_FLLOTT = space(1)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_MVCODLOT = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_OLDUNIMIS = space(3)
  w_DMVCODIVA = space(5)
  w_DMVCATCON = space(5)
  w_DMVCODCLA = space(2)
  w_DMVVOCCEN = space(15)
  w_DMVTIPRIG = space(1)
  w_DMVVOCRIC = space(15)
  w_DMVFLRISE = space(1)
  w_DMVFLCASC = space(1)
  w_DMVFLIMPE = space(1)
  w_DMVFLORDI = space(1)
  w_DMVF2ORDI = space(1)
  w_DMVF2CASC = space(1)
  w_DMVF2IMPE = space(1)
  w_DMVF2RISE = space(1)
  w_DMVCODART = space(20)
  w_DMVCODMAG = space(5)
  w_DMVCODMAT = space(5)
  w_MAGPRE = space(5)
  w_OMAG = space(5)
  w_OMAT = space(5)
  w_MVDATEVA = ctod("  /  /  ")
  w_FLCOMM = space(1)
  w_OPERAT = space(1)
  w_MVFLCOCO = space(1)
  w_MVVALULT = 0
  w_MVQTASAL = 0
  w_MVFLORCO = space(1)
  w_MVCODATT = space(15)
  w_PREZUM = space(1)
  w_FLSERG = space(1)
  w_ARRIPCON = space(1)
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_NOFRAZ = space(1)
  w_DESUM = space(35)
  w_FLAVA1 = space(1)
  w_ULTCAR = ctod("  /  /  ")
  w_ULTSCA = ctod("  /  /  ")
  w_MVFLULCA = space(1)
  w_MVFLULPV = space(1)
  w_MVTIPPRO = space(2)
  w_MVTIPPR2 = space(2)
  w_MVDATOAI = ctod("  /  /  ")
  w_MVPROORD = space(2)
  w_INDIVA = 0
  w_LIPREZZO = 0
  w_IVALIS = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_VALUCA = 0
  w_CODVAA = space(3)
  w_PROG = space(3)
  w_LIPREZZO = 0
  w_SCOLIS = space(1)
  w_CALPRZ = 0
  w_LISCON = 0
  w_PERIVA = 0
  w_APPO = space(12)
  w_IVACON = space(1)
  w_LDESART = space(40)
  w_LDESSUP = space(0)
  w_QTAEV1 = 0
  w_QTAEVA = 0
  w_IMPEVA = 0
  w_DOCGENE = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_EXPRCOL = space(100)
  w_COD_TMP = space(10)
  w_LROWNUM = 0
  w_SERIALE = space(0)
  w_ETIPOER = space(0)
  w_EDI_TAB = space(10)
  w_CODTAB = space(30)
  w_TMP_GEN = space(30)
  w_TMP_EDI = space(30)
  w_LOOP = 0
  w_LENTAB = 0
  w_LENFLAT = 0
  w_TIPCAMPO = space(1)
  w_INDTAB = 0
  w_STATUS = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ENT_DETT_idx=0
  ENT_MAST_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  LOG_GEND_idx=0
  MAGAZZIN_idx=0
  MOVIMATR_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  TIP_DOCU_idx=0
  TMPDETTI_idx=0
  TMPLIST_idx=0
  TMPMASTI_idx=0
  TMPMATRI_idx=0
  TMPVFABBLOTUL_idx=0
  TMPVFDOCDETT_idx=0
  TMPVFDOCMAST_idx=0
  TMPVFDOCRATE_idx=0
  TMPVFMOVILOTT_idx=0
  TMPVFMOVIMATR_idx=0
  TRS_DETT_idx=0
  TRS_MAST_idx=0
  VADETTFO_idx=0
  VAELEMEN_idx=0
  VALUTE_idx=0
  VAPREDEF_idx=0
  VASTRUTT_idx=0
  VATRASCO_idx=0
  VOCIIVA_idx=0
  XDC_FIELDS_idx=0
  VAPREDEF_idx=0
  FLATDETT_idx=0
  LDOCGENE_idx=0
  GENERDOC_idx=0
  TMPDOCRATE_idx=0
  CAM_AGAZ_idx=0
  UNIMIS_idx=0
  DIC_INTE_idx=0
  AGENTI_idx=0
  DES_DIVE_idx=0
  PAG_AMEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- File di import
    * --- codice struttura
    * --- I: esegue valorizzazione tabelle clone da file  import
    *     C: esegue commit dati nelle tabelle ufficiali
    *     A: esegue analisi\verifica dati simulando una commit
    *     E: esegue controlli esistenza tabelle
    *     V: valorizza con valore di default dello zoom  campo aggiornamento
    * --- Granularit� import massivo
    if Vartype(this.pGranu)<>"C"
      this.pGranu = " "
    endif
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    DIMENSION ARREIT(1)
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STTIPFIL,STCODNOR,STCODENT,STFLATAB"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.pCODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STTIPFIL,STCODNOR,STCODENT,STFLATAB;
        from (i_cTable) where;
            STCODICE = this.pCODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_CODNOR = NVL(cp_ToDate(_read_.STCODNOR),cp_NullValue(_read_.STCODNOR))
      w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ENT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ENT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_MAST_idx,2],.t.,this.ENT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ENBATPRE,ENBATPOS,ENBATCHK"+;
        " from "+i_cTable+" ENT_MAST where ";
            +"ENCODICE = "+cp_ToStrODBC(w_CODENT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ENBATPRE,ENBATPOS,ENBATCHK;
        from (i_cTable) where;
            ENCODICE = w_CODENT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BATPRE = NVL(cp_ToDate(_read_.ENBATPRE),cp_NullValue(_read_.ENBATPRE))
      this.w_BATPOS = NVL(cp_ToDate(_read_.ENBATPOS),cp_NullValue(_read_.ENBATPOS))
      this.w_BATCHK = NVL(cp_ToDate(_read_.ENBATCHK),cp_NullValue(_read_.ENBATCHK))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PADRE = This.oparentobject
    LTrsOk = .F.
    do case
      case this.pTIPOPE = "IMPOR"
        if EMPTY(this.oParentObject.w_PARAM)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
        endif
        * --- Rilancio zoom
        this.oParentObject.Notifyevent("Esegui")
        this.w_EXPRCOL = "ICASE(NVL(TIPREC,' ') ='O', RGB(35,150,35),Not empty(NVL(TIPREC,' ')),RGB(255,0,0), RGB(0,0,0))"
        gsva_bzc(this,this.oParentObject.w_ZOOMDOC,this.w_EXPRCOL,"C")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_oErrorLog.IsFullLog()
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
        else
          AddMsgNL("Inserimento in tabelle clone completato con successo",this )
        endif
        this.oParentObject.w_FASE2 = .T.
        this.oParentObject.w_FASE = .T.
      case this.pTIPOPE = "CONFE" or this.pTIPOPE = "VERIF"
        this.w_TEST = .F.
        this.oParentObject.w_Msg = ""
        if Upper(this.w_PADRE.Class)="TGSVA_BCD"
          this.w_NUMREC = 1
        else
           
 AddMsgNL("Inzio verifica documenti",this ) 
 Select (this.oParentObject.w_ZOOMDOC.cCURSOR) 
 COUNT FOR XCHK=1 TO this.w_NUMREC
        endif
        if this.w_NUMREC>0
          this.w_GDKEYRIF = this.oParentObject.w_SER_ELAB
          this.w_TEST = .t.
          if g_APPLICATION="ad hoc ENTERPRISE"
            GSVE_BIG(this,"C", this.class, "N", "N", "N", "N", "S", "N", "A", this.oParentObject.w_FLRPZ, "S", "", iif(this.pTIPOPE = "CONFE" ,"N","S")) 
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Adesso devo valorizzare tabelle temporanee utilizzate dal generatore 
            *     costruendo una frase di insert con i campi realmente esistenti nella 
            *     tabella EDI di adhoc
          else
            * --- Create temporary table TMPMASTI
            i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.DOC_MAST_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                  +" where 1=0";
                  )
            this.TMPMASTI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Create temporary table TMPDETTI
            i_nIdx=cp_AddTableDef('TMPDETTI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                  +" where 1=0";
                  )
            this.TMPDETTI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Create temporary table TMPMATRI
            i_nIdx=cp_AddTableDef('TMPMATRI') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.MOVIMATR_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                  +" where 1=0";
                  )
            this.TMPMATRI_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Create temporary table TMPDOCRATE
            i_nIdx=cp_AddTableDef('TMPDOCRATE') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.DOC_RATE_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                  +" where 1=0";
                  )
            this.TMPDOCRATE_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
           
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="MVSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
          this.w_CODTAB = "DOC_MAST"
          this.w_TMP_GEN = "TMPMASTI"
          this.w_TMP_EDI = "TMPVFDOCMAST"
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="MVSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
          this.w_CODTAB = "DOC_DETT"
          this.w_TMP_GEN = "TMPDETTI"
          this.w_TMP_EDI = "TMPVFDOCDETT"
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Aggiorno MVUNIMIS se mancante in quanto campo obbligatorio per il generatore
          This.TMPDETTI_idx=cp_GetTableDefIdx("TMPDETTI")
          if This.TMPDETTI_idx<>0
            * --- Write into TMPDETTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPDETTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM"
              do vq_exec with 'GSVA_BGE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDETTI_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPDETTI.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPDETTI.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVUNIMIS = _t2.MVUNIMIS";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPDETTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPDETTI.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPDETTI.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDETTI, "+i_cQueryTable+" _t2 set ";
                  +"TMPDETTI.MVUNIMIS = _t2.MVUNIMIS";
                  +Iif(Empty(i_ccchkf),"",",TMPDETTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="TMPDETTI.MVSERIAL = t2.MVSERIAL";
                      +" and "+"TMPDETTI.CPROWNUM = t2.CPROWNUM";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDETTI set (";
                  +"MVUNIMIS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.MVUNIMIS";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="TMPDETTI.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"TMPDETTI.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPDETTI set ";
                  +"MVUNIMIS = _t2.MVUNIMIS";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVUNIMIS = (select MVUNIMIS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
           
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="MTSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
          this.w_CODTAB = "MOVIMATR"
          this.w_TMP_GEN = "TMPMATRI"
          this.w_TMP_EDI = "TMPVFMOVIMATR"
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if g_APPLICATION="ADHOC REVOLUTION"
             
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="RSSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
            this.w_CODTAB = "DOC_RATE"
            this.w_TMP_GEN = "TMPDOCRATE"
            this.w_TMP_EDI = "TMPVFDOCRATE"
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_NUMREC = 1
          if g_APPLICATION="ad hoc ENTERPRISE"
            if !EMPTY(NVL(this.oParentObject.w_TIPDOC, " "))
               
 DIMENSION Arrval(1,2),ArrWrite(1,2) 
 Arrwrite[1,1]="MVTIPDOC" 
 Arrwrite[1,2]=this.oParentObject.w_TIPDOC 
 ArrVAL[1,1]="" 
 Arrval[1,2]="" 
 WRITETABLE( "TMPMASTI" , @ArrWrite , .F. , "",.F.,"1=1" ) 
            endif
            this.w_OKBGE = .T.
            if this.pTIPOPE = "CONFE" and this.pGranu<>"C"
              this.w_OLDSERLOG = this.oParentObject.w_SERGDOC 
              * --- Verifico la presenza di errori 
              * --- Read from LOG_GEND
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LOG_GEND_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LGMESSAG"+;
                  " from "+i_cTable+" LOG_GEND where ";
                      +"LGSERIAL = "+cp_ToStrODBC(this.w_OLDSERLOG);
                      +" and LGTIPRIG = "+cp_ToStrODBC("E");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LGMESSAG;
                  from (i_cTable) where;
                      LGSERIAL = this.w_OLDSERLOG;
                      and LGTIPRIG = "E";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.LGMESSAG),cp_NullValue(_read_.LGMESSAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_ROWS<>0
                * --- Ho almeno un errore bloccante non eseguo generazione
                this.w_OKBGE = .F.
              endif
            endif
            this.oParentObject.w_SERGDOC = this.w_SERGDOC
            if this.w_OKBGE
              GSVE_BGE(this,this.w_SERGDOC)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            GSVE_BIG(this,"D")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.pTIPOPE = "CONFE" AND this.w_OKBGE
              * --- Aggiorno stautus Tabelle
              * --- Select from FLATDETT
              i_nConn=i_TableProp[this.FLATDETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select FTCODICE,FTTABNAM  from "+i_cTable+" FLATDETT ";
                    +" where FTCODICE="+cp_ToStrODBC(this.w_FLATAB)+"";
                    +" group by FTCODICE,FTTABNAM";
                     ,"_Curs_FLATDETT")
              else
                select FTCODICE,FTTABNAM from (i_cTable);
                 where FTCODICE=this.w_FLATAB;
                 group by FTCODICE,FTTABNAM;
                  into cursor _Curs_FLATDETT
              endif
              if used('_Curs_FLATDETT')
                select _Curs_FLATDETT
                locate for 1=1
                do while not(eof())
                this.w_CODTAB = _Curs_FLATDETT.FTTABNAM
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                  select _Curs_FLATDETT
                  continue
                enddo
                use
              endif
              * --- Ricostruisco tabelle temporanee che interrogano tabelle clone
              *     per aggiornare dati rispetto all'importazione eseguita
              if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Rilancio zoom
              this.oParentObject.Notifyevent("Esegui")
            else
              * --- Aggiorno lo stato degli errori a seguito di verifica
              if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
                UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET ERRORMSG = "" WHERE XCHK=1
              endif
              this.w_LGMESSAG = " "
              this.w_TIPREC = "O"
              * --- Select from LOG_GEND
              i_nConn=i_TableProp[this.LOG_GEND_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(LGTIPRIG) AS LGTIPRIG,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM  from "+i_cTable+" LOG_GEND ";
                    +" where LGSERIAL="+cp_ToStrODBC(this.w_SERGDOC)+" AND LGTIPRIG<>'L'";
                    +" group by LGSERORI, LGROWORI, LGMESSAG,CPROWNUM";
                    +" order by CPROWNUM";
                     ,"_Curs_LOG_GEND")
              else
                select MAX(LGTIPRIG) AS LGTIPRIG,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM from (i_cTable);
                 where LGSERIAL=this.w_SERGDOC AND LGTIPRIG<>"L";
                 group by LGSERORI, LGROWORI, LGMESSAG,CPROWNUM;
                 order by CPROWNUM;
                  into cursor _Curs_LOG_GEND
              endif
              if used('_Curs_LOG_GEND')
                select _Curs_LOG_GEND
                locate for 1=1
                do while not(eof())
                this.w_LGSERORI = _Curs_LOG_GEND.LGSERORI
                this.w_LGROWORI = _Curs_LOG_GEND.LGROWORI
                this.w_LGTIPRIG = _Curs_LOG_GEND.LGTIPRIG
                this.w_LGMESSAG = this.w_LGMESSAG+ chr(13) + IIF(!EMPTY(NVL(this.w_LGROWORI, " ")), ah_msgformat("Riga %1: ", ALLTRIM(STR(this.w_LGROWORI))) , "") + ALLTRIM(_Curs_LOG_GEND.LGMESSAG)
                if Nvl(_Curs_LOG_GEND.LGTIPRIG," ")="E"
                  this.w_TIPREC = "E"
                  SELECT (this.oParentObject.w_ZOOMDOC.cCURSOR)
                  UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET TIPREC=this.w_TIPREC,ERRORMSG = IIF(EMPTY(NVL(ERRORMSG, " ")), "", ERRORMSG+CHR(10))+this.w_LGMESSAG where MVSERIAL=this.w_LGSERORI
                endif
                  select _Curs_LOG_GEND
                  continue
                enddo
                use
              endif
              if Upper(this.w_PADRE.Class)<>"TGSVA_BCD" 
                if this.w_TIPREC<>"E"
                  this.w_LGMESSAG = "Verifica OK"
                endif
                SELECT (this.oParentObject.w_ZOOMDOC.cCURSOR)
                UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET TIPREC=this.w_TIPREC,ERRORMSG = IIF(EMPTY(NVL(ERRORMSG, " ")), "", ERRORMSG+CHR(10))+this.w_LGMESSAG 
              endif
            endif
          else
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if !EMPTY(NVL(this.oParentObject.w_TIPDOC, " "))
               
 DIMENSION Arrval(1,2),ArrWrite(1,2) 
 Arrwrite[1,1]="MVTIPDOC" 
 Arrwrite[1,2]=this.oParentObject.w_TIPDOC 
 ArrVAL[1,1]="" 
 Arrval[1,2]="" 
 WRITETABLE( "TMPVFDOCMAST" , @ArrWrite , .F. , "",.F.,"1=1" ) 
            endif
            this.w_DOCGENE = 0
            * --- Create temporary table TMPLIST
            i_nIdx=cp_AddTableDef('TMPLIST') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\VEFA\EXE\QUERY\GSVALBIM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPLIST_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            if Upper(this.w_PADRE.Class)="TGSVA_BCD"
              * --- Query che legge tutto VF_doc_mast in cursore di appoggio
              vq_exec("..\VEFA\EXE\QUERY\GSVATKIM.VQR", this,"Tmp_Doc")
            else
               
 Select * from (this.oParentObject.w_ZOOMDOC.cCURSOR) where XCHK=this.w_NUMREC into Cursor Tmp_Doc
            endif
             
 Select Tmp_Doc 
 Go Top 
 Scan 
            this.w_TEST = .T.
            * --- Ricalcolo castelletto IVA e scadenze...
            this.w_SERIAL = MVSERIAL
            this.w_OKTRAD = .T.
            * --- Inserimento Testata documento
            this.w_RIGA = 0
            * --- Try
            local bErr_0511D0A8
            bErr_0511D0A8=bTrsErr
            this.Try_0511D0A8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              if this.w_OKTRAD
                this.w_log = cp_translatedbmessage(MESSAGE(),.t.)
              endif
              if not empty(this.w_LOG) 
                * --- Insert into TMPLIST
                i_nConn=i_TableProp[this.TMPLIST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPLIST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MVSERIAL"+",TIPREC"+",CPROWNUM"+",LOGERROR"+",MATRICOLA"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPLIST','MVSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREC),'TMPLIST','TIPREC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'TMPLIST','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_LOG),'TMPLIST','LOGERROR');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MATRICOLA),'TMPLIST','MATRICOLA');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'TIPREC',this.w_TIPREC,'CPROWNUM',this.w_RIGA,'LOGERROR',this.w_LOG,'MATRICOLA',this.w_MATRICOLA)
                  insert into (i_cTable) (MVSERIAL,TIPREC,CPROWNUM,LOGERROR,MATRICOLA &i_ccchkf. );
                     values (;
                       this.w_SERIAL;
                       ,this.w_TIPREC;
                       ,this.w_RIGA;
                       ,this.w_LOG;
                       ,this.w_MATRICOLA;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error='ERRORE INSERIMENTO LOG'
                  return
                endif
                * --- Scrivo errore nel campo di log
                if this.w_TIPREC<>"O"
                  this.w_ETIPOER = "E"
                  this.w_LROWNUM = this.w_LROWNUM + 1
                  this.w_EMESS = ah_msgformat("Inserimento dati di generazione")
                  * --- Insert into LOG_GEND
                  i_nConn=i_TableProp[this.LOG_GEND_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_GEND_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"LGSERIAL"+",CPROWNUM"+",LGTIPRIG"+",LGMESSAG"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'LOG_GEND','LGSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_LROWNUM),'LOG_GEND','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ETIPOER),'LOG_GEND','LGTIPRIG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_LOG),'LOG_GEND','LGMESSAG');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'LGSERIAL',this.w_SERIALE,'CPROWNUM',this.w_LROWNUM,'LGTIPRIG',this.w_ETIPOER,'LGMESSAG',this.w_LOG)
                    insert into (i_cTable) (LGSERIAL,CPROWNUM,LGTIPRIG,LGMESSAG &i_ccchkf. );
                       values (;
                         this.w_SERIALE;
                         ,this.w_LROWNUM;
                         ,this.w_ETIPOER;
                         ,this.w_LOG;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              endif
            endif
            bTrsErr=bTrsErr or bErr_0511D0A8
            * --- End
             
 Select Tmp_Doc 
 Endscan
             
 Use in Tmp_Doc
          endif
          * --- Definisco propiet� dello zoom delle testate
          if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
            * --- Rilancio zoom
            if LTrsOk AND this.pTIPOPE = "CONFE" 
              * --- Drop temporary table TMPVFDOCDETT
              i_nIdx=cp_GetTableDefIdx('TMPVFDOCDETT')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('TMPVFDOCDETT')
              endif
              * --- Drop temporary table TMPVFDOCMAST
              i_nIdx=cp_GetTableDefIdx('TMPVFDOCMAST')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('TMPVFDOCMAST')
              endif
              * --- Drop temporary table TMPVFDOCRATE
              i_nIdx=cp_GetTableDefIdx('TMPVFDOCRATE')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('TMPVFDOCRATE')
              endif
              * --- Drop temporary table TMPVFMOVIMATR
              i_nIdx=cp_GetTableDefIdx('TMPVFMOVIMATR')
              if i_nIdx<>0
                cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                cp_RemoveTableDef('TMPVFMOVIMATR')
              endif
              * --- Ricostruisco tabelle temporanee che interrogano tabelle clone
              *     per aggiornare dati rispetto all'importazione eseguita
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if ! Isahe()
              this.oParentObject.Notifyevent("Esegui")
            endif
            SELECT (this.oParentObject.w_ZOOMDOC.cCURSOR)
            GO TOP
            this.oParentObject.w_ERRORLOG = ALLTRIM(ERRORMSG)
            this.w_EXPRCOL = "ICASE(NVL(TIPREC,' ') ='O', RGB(35,150,35),Not empty(NVL(TIPREC,' ')),RGB(255,0,0), RGB(0,0,0))"
            gsva_bzc(this,this.oParentObject.w_ZOOMDOC,this.w_EXPRCOL,"COLOR")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_TEST
            if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
              if this.pTIPOPE = "CONFE" 
                ah_ErrorMsg("Import terminato con successo","i","")
              else
                ah_ErrorMsg("Verifica terminata","i","")
              endif
            endif
            this.oParentObject.w_FASE2 = .T.
          else
            if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
              ah_ErrorMsg("Attenzione nessun documento selezionato! ",,"")
            endif
          endif
        else
          if Upper(this.w_PADRE.Class)<>"TGSVA_BCD"
            ah_ErrorMsg("Attenzione! Nessun documento selezionato",,"")
          endif
        endif
      case this.pTIPOPE = "DONE"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPOPE = "SELEZ" Or this.pTIPOPE = "DESEL" Or this.pTIPOPE = "INVSE"
        ND=this.oParentObject.w_ZOOMDOC.cCURSOR
        UPDATE (ND) SET XCHK=ICASE(this.pTIPOPE="SELEZ",1,this.pTIPOPE="DESEL", 0, IIF(XCHK=1,0,1))
      case this.pTIPOPE = "DEFAC"
        * --- Valorizza campo valore da aggiornare con default dello zoom
        ND=this.oParentObject.w_ZOOMDOC.cCURSOR
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 VALTRAS=(ND)+"."+Alltrim(this.oParentObject.w_CAMPO) 
 VALTRAS=&VALTRAS 
 on error &L_OldError 
        if Not L_err
          do case
            case this.oParentObject.w_TIPCAM="N"
              this.oParentObject.w_NUMERIC = VALTRAS
            case this.oParentObject.w_TIPCAM="D"
              this.oParentObject.w_DATA = VALTRAS
            otherwise
              this.oParentObject.w_CARACTER = VALTRAS
          endcase
        endif
    endcase
  endproc
  proc Try_0511D0A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if Not Empty(this.w_BATPRE)
      LTrsOk = .T.
       
 Dimension ARRCAMPI(1,2) 
 ARRCAMPI[1,1]="MVSERIAL" 
 ARRCAMPI[1,2]=this.w_SERIAL
      l_Prg = Alltrim(this.w_BATPRE)+"( @ArrCampi, 'Pre', 'I', @LTrsOk)"
      this.w_LOG = &l_Prg
      if Not LTrsOk
        * --- Se ci sono errori/worning 
        this.w_OKTRAD = .F.
        this.w_TIPREC = "T"
        * --- Raise
        i_Error="Errore"
        return
      endif
    endif
    this.w_TIPREC = "T"
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\VEFA\EXE\QUERY\GSVA_TES.VQR",this.DOC_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='ERRORE INSERIMENTO DOCMAST'
      return
    endif
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
      this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_MVCODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
      this.w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
      this.w_MVCODESE = NVL(cp_ToDate(_read_.MVCODESE),cp_NullValue(_read_.MVCODESE))
      this.w_MVANNDOC = NVL(cp_ToDate(_read_.MVANNDOC),cp_NullValue(_read_.MVANNDOC))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_MVANNPRO = NVL(cp_ToDate(_read_.MVANNPRO),cp_NullValue(_read_.MVANNPRO))
      this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
      this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
      this.w_MVPRP = NVL(cp_ToDate(_read_.MVPRP),cp_NullValue(_read_.MVPRP))
      this.w_MVCODORN = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
      this.w_MVTIPORN = NVL(cp_ToDate(_read_.MVTIPORN),cp_NullValue(_read_.MVTIPORN))
      this.w_MVCODBAN = NVL(cp_ToDate(_read_.MVCODBAN),cp_NullValue(_read_.MVCODBAN))
      this.w_MVCODBA2 = NVL(cp_ToDate(_read_.MVCODBA2),cp_NullValue(_read_.MVCODBA2))
      this.w_MVSCOCL1 = NVL(cp_ToDate(_read_.MVSCOCL1),cp_NullValue(_read_.MVSCOCL1))
      this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_MVSCOCL2 = NVL(cp_ToDate(_read_.MVSCOCL2),cp_NullValue(_read_.MVSCOCL2))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_MVFLSCOR = NVL(cp_ToDate(_read_.MVFLSCOR),cp_NullValue(_read_.MVFLSCOR))
      this.w_MVPRD = NVL(cp_ToDate(_read_.MVPRD),cp_NullValue(_read_.MVPRD))
      this.w_MVNUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
      this.w_MVDATCIV = NVL(cp_ToDate(_read_.MVDATCIV),cp_NullValue(_read_.MVDATCIV))
      this.w_MVIVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
      this.w_MVDATTRA = NVL(cp_ToDate(_read_.MVDATTRA),cp_NullValue(_read_.MVDATTRA))
      this.w_MVIVACAU = NVL(cp_ToDate(_read_.MVIVACAU),cp_NullValue(_read_.MVIVACAU))
      this.w_MVDATEST = NVL(cp_ToDate(_read_.MVDATEST),cp_NullValue(_read_.MVDATEST))
      this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_MVFLPROV = NVL(cp_ToDate(_read_.MVFLPROV),cp_NullValue(_read_.MVFLPROV))
      this.w_MVIVAIMB = NVL(cp_ToDate(_read_.MVIVAIMB),cp_NullValue(_read_.MVIVAIMB))
      this.w_MVRIFDIC = NVL(cp_ToDate(_read_.MVRIFDIC),cp_NullValue(_read_.MVRIFDIC))
      this.w_MVCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
      this.w_MVCODAGE = NVL(cp_ToDate(_read_.MVCODAGE),cp_NullValue(_read_.MVCODAGE))
      this.w_MVCODDES = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
      this.w_MVSCOPAG = NVL(cp_ToDate(_read_.MVSCOPAG),cp_NullValue(_read_.MVSCOPAG))
      this.w_MVSPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
      this.w_MVIVATRA = NVL(cp_ToDate(_read_.MVIVATRA),cp_NullValue(_read_.MVIVATRA))
      this.w_MVIVAINC = NVL(cp_ToDate(_read_.MVIVAINC),cp_NullValue(_read_.MVIVAINC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Legge causale documenti
    * --- Applico causale documento prioritaria
    this.w_MVTIPDOC = iif(Not empty(this.oParentObject.w_TIPDOC),this.oParentObject.w_TIPDOC,this.w_MVTIPDOC)
    if Not Empty(this.w_MVTIPDOC)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDCATDOC,TDCAUCON,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLVEAC,TFFLRAGG,TD_SEGNO,TDFLPPRO,TDSERPRO,TDCODMAT,TDFLMGPR,TDFLMTPR,TDCODLIS,TDFLSCOR,TDPRZVAC,TDPRODOC,TDFLELAN,TDALFDOC,TDRIPINC,TDRIPTRA,TDRIPIMB,TDPRZDES,TDRIPCON,TDEMERIC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDCATDOC,TDCAUCON,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLVEAC,TFFLRAGG,TD_SEGNO,TDFLPPRO,TDSERPRO,TDCODMAT,TDFLMGPR,TDFLMTPR,TDCODLIS,TDFLSCOR,TDPRZVAC,TDPRODOC,TDFLELAN,TDALFDOC,TDRIPINC,TDRIPTRA,TDRIPIMB,TDPRZDES,TDRIPCON,TDEMERIC;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
        this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
        this.w_TDCODMAG = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
        this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
        this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
        this.w_DMV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
        this.w_DFLPP = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
        this.w_DMVALFEST = NVL(cp_ToDate(_read_.TDSERPRO),cp_NullValue(_read_.TDSERPRO))
        this.w_COMAG = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
        this.w_COMAT = NVL(cp_ToDate(_read_.TDCODMAT),cp_NullValue(_read_.TDCODMAT))
        this.w_FLMGPR = NVL(cp_ToDate(_read_.TDFLMGPR),cp_NullValue(_read_.TDFLMGPR))
        this.w_FLMTPR = NVL(cp_ToDate(_read_.TDFLMTPR),cp_NullValue(_read_.TDFLMTPR))
        this.w_DOCLIS = NVL(cp_ToDate(_read_.TDCODLIS),cp_NullValue(_read_.TDCODLIS))
        this.w_TDFLSCOR = NVL(cp_ToDate(_read_.TDFLSCOR),cp_NullValue(_read_.TDFLSCOR))
        this.w_PRZVAC = NVL(cp_ToDate(_read_.TDPRZVAC),cp_NullValue(_read_.TDPRZVAC))
        this.w_PRODOC = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
        this.w_FLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
        this.w_ALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
        this.w_MVFLRINC = NVL(cp_ToDate(_read_.TDRIPINC),cp_NullValue(_read_.TDRIPINC))
        this.w_MVFLRTRA = NVL(cp_ToDate(_read_.TDRIPTRA),cp_NullValue(_read_.TDRIPTRA))
        this.w_MVFLRIMB = NVL(cp_ToDate(_read_.TDRIPIMB),cp_NullValue(_read_.TDRIPIMB))
        this.oParentObject.w_FLRPZ = NVL(cp_ToDate(_read_.TDPRZDES),cp_NullValue(_read_.TDPRZDES))
        this.w_TDRIPCON = NVL(cp_ToDate(_read_.TDRIPCON),cp_NullValue(_read_.TDRIPCON))
        this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCTIPREG,CCFLPPRO"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_MVCAUCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCTIPREG,CCFLPPRO;
          from (i_cTable) where;
              CCCODICE = this.w_MVCAUCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
        this.w_CFLPP = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Scrivo errore nel campo di log
      this.w_RIGA = 0
      this.w_TIPREC = "T"
      this.w_OKTRAD = .F.
      this.w_LOG = ah_msgformat("Attenzione, causale documento non presente")
      * --- Raise
      i_Error="Errore"
      return
    endif
    if Empty(nvl(this.w_MVCODDES,space(5)))
      * --- Read from DES_DIVE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DDCODDES,DDCODVET,DDCODPOR,DDCODSPE,DDPREDEF,DDCODNAZ,DDMCCODI,DDMCCODT,DDCODAGE"+;
          " from "+i_cTable+" DES_DIVE where ";
              +"DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
              +" and DDTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and DDTIPRIF = "+cp_ToStrODBC("CO");
              +" and DDPREDEF = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DDCODDES,DDCODVET,DDCODPOR,DDCODSPE,DDPREDEF,DDCODNAZ,DDMCCODI,DDMCCODT,DDCODAGE;
          from (i_cTable) where;
              DDCODICE = this.w_MVCODCON;
              and DDTIPCON = this.w_MVTIPCON;
              and DDTIPRIF = "CO";
              and DDPREDEF = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODDES = NVL(cp_ToDate(_read_.DDCODDES),cp_NullValue(_read_.DDCODDES))
        this.w_CODVET = NVL(cp_ToDate(_read_.DDCODVET),cp_NullValue(_read_.DDCODVET))
        this.w_CODPOR = NVL(cp_ToDate(_read_.DDCODPOR),cp_NullValue(_read_.DDCODPOR))
        this.w_CODSPE = NVL(cp_ToDate(_read_.DDCODSPE),cp_NullValue(_read_.DDCODSPE))
        this.w_PREDEF = NVL(cp_ToDate(_read_.DDPREDEF),cp_NullValue(_read_.DDPREDEF))
        this.w_CODNAZ1 = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
        this.w_MCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
        this.w_MCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
        this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from DES_DIVE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DDCODVET,DDCODPOR,DDCODSPE,DDPREDEF,DDCODNAZ,DDMCCODI,DDMCCODT,DDCODAGE"+;
          " from "+i_cTable+" DES_DIVE where ";
              +"DDCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
              +" and DDTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and DDCODDES = "+cp_ToStrODBC(this.w_MVCODDES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DDCODVET,DDCODPOR,DDCODSPE,DDPREDEF,DDCODNAZ,DDMCCODI,DDMCCODT,DDCODAGE;
          from (i_cTable) where;
              DDCODICE = this.w_MVCODCON;
              and DDTIPCON = this.w_MVTIPCON;
              and DDCODDES = this.w_MVCODDES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODVET = NVL(cp_ToDate(_read_.DDCODVET),cp_NullValue(_read_.DDCODVET))
        this.w_CODPOR = NVL(cp_ToDate(_read_.DDCODPOR),cp_NullValue(_read_.DDCODPOR))
        this.w_CODSPE = NVL(cp_ToDate(_read_.DDCODSPE),cp_NullValue(_read_.DDCODSPE))
        this.w_PREDEF = NVL(cp_ToDate(_read_.DDPREDEF),cp_NullValue(_read_.DDPREDEF))
        this.w_CODNAZ1 = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
        this.w_MCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
        this.w_MCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
        this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVCAUCON = iif(empty(this.w_MVCAUCON),this.w_DMVCAUCON,this.w_MVCAUCON)
    this.w_MVCODVET = iif(empty(this.w_MVCODVET),this.w_CODVET,this.w_MVCODVET)
    this.w_MVCODPOR = iif(empty(this.w_MVCODPOR),this.w_CODPOR,this.w_MVCODPOR)
    this.w_MVCODSPE = iif(empty(this.w_MVCODSPE),this.w_CODSPE,this.w_MVCODSPE)
    this.w_MVDATREG = iif(empty(this.w_MVDATREG),i_DATSYS,this.w_MVDATREG)
    this.w_MVCODESE = iif(empty(this.w_MVCODESE),g_CODESE,this.w_MVCODESE)
    this.w_MVCLADOC = iif(empty(this.w_MVCLADOC),this.w_DMVCLADOC,this.w_MVCLADOC)
    this.w_MVALFEST = iif(empty(this.w_MVALFEST),this.w_DMVALFEST,this.w_MVALFEST)
    this.w_FLPPRO = IIF(this.w_DFLPP="P" OR this.w_MVCLADOC $ "FA-NC", this.w_CFLPP, this.w_DFLPP)
    this.w_MVANNPRO = iif(empty(this.w_MVANNPRO),CALPRO(this.w_MVDATREG,this.w_MVCODESE,this.w_FLPPRO),this.w_MVANNPRO)
    this.w_MVPRD = iif(empty(Nvl(this.w_MVPRD," ")),iif(this.w_FLPDOC="S",iif(this.w_MVCLADOC="DT","DV",iif(this.w_MVCLADOC="DI","IV",this.w_PRODOC)),this.w_PRODOC),this.w_MVPRD)
    this.w_MVPRP = iif(empty(Nvl(this.w_MVPRP," ")),IIF(this.w_TIPREG="A" AND this.w_MVCLADOC $ "FA-NC", "AC", "NN"),this.w_MVPRP)
    this.w_MVFLSEND = "N"
    if this.w_MVANNPRO<>" "
      if this.w_MVNUMEST=0
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST")
      endif
    endif
    this.w_MVALFDOC = iif(empty(this.w_MVALFDOC),this.w_ALFDOC,this.w_MVALFDOC)
    this.w_MVDATDOC = iif(empty(this.w_MVDATDOC),i_DATSYS,this.w_MVDATDOC)
    this.w_MVANNDOC = iif(empty(this.w_MVANNDOC),STR(YEAR(this.w_MVDATDOC), 4, 0),this.w_MVANNDOC)
    this.w_MV__ANNO = YEAR(this.w_MVDATDOC)
    this.w_MV__MESE = MONTH(this.w_MVDATDOC)
    this.w_MVTIPDIS = "N"
    this.w_MVSTFILCB = "1"
    this.w_MVFLGINC = "N"
    if this.w_MVEMERIC="V"
      this.w_MVDATREG = this.w_MVDATDOC
    endif
    this.w_DATCAL = IIF(EMPTY(this.w_MVDATDOC), this.w_MVDATREG, this.w_MVDATDOC)
    if (this.w_MVFLVEAC="V" OR (this.w_MVFLVEAC="A" AND (this.w_MVPRD="DV" OR this.w_MVPRD="IV"))) AND empty(this.w_MVNUMDOC)
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    endif
    this.w_MVDATEST = iif(empty(this.w_MVDATEST) and this.w_MVFLVEAC="A" AND this.w_MVCLADOC <> "OR" ,i_DATSYS,this.w_MVDATEST)
    this.w_MVFLINTE = iif(empty(this.w_MVFLINTE),this.w_DMVFLINTE,this.w_MVFLINTE)
    this.w_MVTIPCON = iif(empty(this.w_MVTIPCON),IIF(this.w_MVFLINTE $ "C-F" ,this.w_MVFLINTE," "),this.w_MVTIPCON)
    if Not Empty(this.w_MVCODCON)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODVAL,ANMAGTER,ANCODORN,ANSCORPO,ANCATSCM,ANCONCON,ANCODIVA,ANCODAG1,ANNUMLIS,ANCODPAG,AN1SCONT,AN2SCONT,ANCODBAN,ANCODBA2,ANCATCOM,ANNUMCOR,ANSPEINC"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODVAL,ANMAGTER,ANCODORN,ANSCORPO,ANCATSCM,ANCONCON,ANCODIVA,ANCODAG1,ANNUMLIS,ANCODPAG,AN1SCONT,AN2SCONT,ANCODBAN,ANCODBA2,ANCATCOM,ANNUMCOR,ANSPEINC;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DMVCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
        this.w_MAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
        this.w_DMVCODORN = NVL(cp_ToDate(_read_.ANCODORN),cp_NullValue(_read_.ANCODORN))
        this.w_FLSCOR = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
        this.w_CATCLI = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
        this.w_DMVCONCON = NVL(cp_ToDate(_read_.ANCONCON),cp_NullValue(_read_.ANCONCON))
        this.w_IVACLI = NVL(cp_ToDate(_read_.ANCODIVA),cp_NullValue(_read_.ANCODIVA))
        this.w_ANCODAG = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        this.w_CLFLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
        this.w_DMVCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        this.w_DMVSCOCL1 = NVL(cp_ToDate(_read_.AN1SCONT),cp_NullValue(_read_.AN1SCONT))
        this.w_DMVSCOCL2 = NVL(cp_ToDate(_read_.AN2SCONT),cp_NullValue(_read_.AN2SCONT))
        this.w_DMVCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_DMVCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_CATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
        this.w_DMVNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
        this.w_DMVSPEINC = NVL(cp_ToDate(_read_.ANSPEINC),cp_NullValue(_read_.ANSPEINC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVCODORN = iif(empty(this.w_MVCODORN),this.w_DMVCODORN,this.w_MVCODORN)
    this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
    this.w_MVTIPORN = iif(empty(this.w_MVTIPORN),this.w_MVTIPCON,this.w_MVTIPORN)
    this.w_AGENTE = IIF(EMPTY(NVL(this.w_CODAGE,"")),this.w_ANCODAG,this.w_CODAGE)
    this.w_MVCODAGE = iif(empty(Nvl(this.w_MVCODAGE,Space(5))),this.w_AGENTE,this.w_MVCODAGE)
    if Not Empty(NVL(this.w_MVCODORN," "))
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANNUMLIS,ANCODPAG,AN1SCONT,AN2SCONT,ANCODBAN,ANCODBA2,ANCATCOM,ANCONCON,ANNUMCOR,ANSPEINC"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPORN);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_XCONORN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANNUMLIS,ANCODPAG,AN1SCONT,AN2SCONT,ANCODBAN,ANCODBA2,ANCATCOM,ANCONCON,ANNUMCOR,ANSPEINC;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPORN;
              and ANCODICE = this.w_XCONORN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLFLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
        this.w_DMVCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        this.w_DMVSCOCL1 = NVL(cp_ToDate(_read_.AN1SCONT),cp_NullValue(_read_.AN1SCONT))
        this.w_DMVSCOCL2 = NVL(cp_ToDate(_read_.AN2SCONT),cp_NullValue(_read_.AN2SCONT))
        this.w_DMVCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_DMVCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_CATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
        this.w_DMVCONCON = NVL(cp_ToDate(_read_.ANCONCON),cp_NullValue(_read_.ANCONCON))
        this.w_DMVNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
        this.w_DMVSPEINC = NVL(cp_ToDate(_read_.ANSPEINC),cp_NullValue(_read_.ANSPEINC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !Empty(NVL(this.w_MVCODAGE, " "))
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGCATPRO,AGSCOPAG,AGCZOAGE"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGCATPRO,AGSCOPAG,AGCZOAGE;
          from (i_cTable) where;
              AGCODAGE = this.w_MVCODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGEPRO = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
        this.w_AGSCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
        this.w_AGCZOAGE = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_MVCODAGE = NULL
    endif
    if EMPTY(NVL(this.w_MVRIFDIC, " "))
      this.w_CODIVE = NVL(this.w_MVCODIVE, " ")
      this.w_OK_LET = .T.
    endif
    if (this.w_MVTIPCON $ "CF" AND (EMPTY(NVL(this.w_MVCODIVE, " ")) )OR( !EMPTY(NVL(this.w_MVRIFDIC, " ")) ) AND NOT EMPTY(this.w_MVCODCON) )
      this.w_OK = .F.
      * --- Se Cliente/Fornitore e no Codice Iva Non Imponibile
      this.w_DIDATDOC = this.w_MVDATDOC
      if EMPTY(NVL(this.w_MVRIFDIC, " "))
        * --- Cerca la Dichiarazione di Intento Valida
        this.w_TIPOPE = "X"
        this.w_IMPDIC = 0
        this.w_IMPUTI = 0
        this.w_CODIVE = SPACE(5)
        this.w_RIFDIC = SPACE(10)
        this.w_NDIC = 0
        this.w_ALFADIC = Space(2)
        this.w_ADIC = SPACE(4)
        this.w_DDIC = cp_CharToDate("  -  -  ")
        this.w_TDIC = "X"
        this.w_DICODCON = this.w_MVCODCON
        this.w_DITIPCON = this.w_MVTIPCON
        * --- Lettura lettera di intento valida
        DECLARE ARRDIC (14,1)
        * --- Azzero l'Array che verr� riempito dalla Funzione
        ARRDIC(1)=0
        if Not Empty(this.w_MVCODORN) And g_XCONDI = "S"
          this.w_OK_LET = CAL_LETT(this.w_DIDATDOC,this.w_MVTIPORN,this.w_XCONORN, @ArrDic)
        else
          this.w_OK_LET = CAL_LETT(this.w_DIDATDOC,this.w_MVTIPCON,this.w_MVCODCON, @ArrDic, SPACE(10),this.w_MVCODDES)
        endif
        if this.w_OK_LET
          * --- Parametri
          *     pDatRif : Data di Riferimento per filtro su Lettere di intento
          *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
          *     pCodCon : Codice Cliente/Fornitore
          *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
          *     
          *     pArrDic[ 1 ]   = Numero Dichiarazione
          *     pArrDic[ 2 ]   = Tipo Operazione
          *     pArrDic[ 3 ]   = Anno Dichiarazione
          *     pArrDic[ 4 ]   = Importo Dichiarazione
          *     pArrDic[ 5 ]   = Data Dichiarazione
          *     pArrDic[ 6 ]   = Importo Utilizzato
          *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
          *     pArrDic[ 8 ]   = Codice Iva Agevolata
          *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
          *     pArrDic[ 10 ] = Data Inizio Validit�
          *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
          *     pArrDic[ 12 ] = Data Obsolescenza
          this.w_NDIC = ArrDic(1)
          this.w_TIPOPE = ArrDic(2)
          this.w_ADIC = ArrDic(3)
          this.w_IMPDIC = ArrDic(4)
          this.w_DDIC = ArrDic(5)
          this.w_IMPUTI = ArrDic(6)
          this.w_TDIC = ArrDic(7)
          this.w_CODIVE = ArrDic(8)
          this.w_DIDATINI = ArrDic(10)
          this.w_RIFDIC = ArrDic(11)
          this.w_ALFADIC = ArrDic(13)
          this.w_DIDATFIN = ArrDic(12)
        endif
      else
        this.w_RIFDIC = this.w_MVRIFDIC
        this.w_MVRIFDIC = ""
        * --- Read from DIC_INTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DINUMDIC,DITIPOPE,DI__ANNO,DIIMPDIC,DIDATDIC,DIIMPUTI,DITIPCON,DICODIVA,DIALFDIC,DIDATINI,DIDATFIN"+;
            " from "+i_cTable+" DIC_INTE where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_RIFDIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DINUMDIC,DITIPOPE,DI__ANNO,DIIMPDIC,DIDATDIC,DIIMPUTI,DITIPCON,DICODIVA,DIALFDIC,DIDATINI,DIDATFIN;
            from (i_cTable) where;
                DISERIAL = this.w_RIFDIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NDIC = NVL(cp_ToDate(_read_.DINUMDIC),cp_NullValue(_read_.DINUMDIC))
          this.w_TIPOPE = NVL(cp_ToDate(_read_.DITIPOPE),cp_NullValue(_read_.DITIPOPE))
          this.w_ADIC = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
          this.w_IMPDIC = NVL(cp_ToDate(_read_.DIIMPDIC),cp_NullValue(_read_.DIIMPDIC))
          this.w_DDIC = NVL(cp_ToDate(_read_.DIDATDIC),cp_NullValue(_read_.DIDATDIC))
          this.w_IMPUTI = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
          this.w_TDIC = NVL(cp_ToDate(_read_.DITIPCON),cp_NullValue(_read_.DITIPCON))
          this.w_CODIVE = NVL(cp_ToDate(_read_.DICODIVA),cp_NullValue(_read_.DICODIVA))
          this.w_ALFADIC = NVL(cp_ToDate(_read_.DIALFDIC),cp_NullValue(_read_.DIALFDIC))
          this.w_DIDATINI = NVL(cp_ToDate(_read_.DIDATINI),cp_NullValue(_read_.DIDATINI))
          this.w_DIDATFIN = NVL(cp_ToDate(_read_.DIDATFIN),cp_NullValue(_read_.DIDATFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      do case
        case this.w_TIPOPE = "I"
          * --- Importo Definito OK Se Importo Dichiarato>Importo Utilizzato
          if this.w_IMPDIC>this.w_IMPUTI
            this.w_OK = .T.
          endif
        case this.w_TIPOPE = "O"
          * --- Operazione Specifica
        case this.w_TIPOPE = "D"
          * --- Periodo Definito
          this.w_OK = .T.
      endcase
      * --- Se Ok riporta i dati dell'Esenzione
      if this.w_OK
        this.w_MVRIFDIC = this.w_RIFDIC
        this.w_OK_LET = .T.
      endif
    endif
    this.w_MVCODIVE = iif(empty(this.w_CODIVE) OR !this.w_OK_LET,this.w_IVACLI,this.w_CODIVE)
    if !Empty(NVL(this.w_MVCODIVE, " "))
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVBOLIVA,IVPERIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVBOLIVA,IVPERIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_MVCODIVE = NULL
    endif
    this.w_MVFLSCOR = IIF(Not empty(this.w_MVFLSCOR),this.w_MVFLSCOR,IIF( this.w_TDFLSCOR="I", this.w_FLSCOR, this.w_TDFLSCOR ))
    this.w_MVTCOLIS = IIF(EMPTY(this.w_CLFLIS), this.w_DOCLIS, this.w_CLFLIS)
    this.w_MVCODBAN = iif(empty(this.w_MVCODBAN),this.w_DMVCODBAN,this.w_MVCODBAN)
    this.w_MVCODBA2 = IIF(NOT EMPTY(this.w_DMVCODBA2) , this.w_DMVCODBA2, NsBancaPref() )
    this.w_MVSCOCL1 = iif(empty(this.w_MVSCOCL1),this.w_DMVSCOCL1,this.w_MVSCOCL1)
    this.w_MVSCOCL2 = iif(empty(this.w_MVSCOCL2),this.w_DMVSCOCL2,this.w_MVSCOCL2)
    this.w_MVCODORN = iif(empty(this.w_MVCODORN),this.w_DMVCODORN,this.w_MVCODORN)
    this.w_MVFLVEAC = iif(empty(this.w_MVFLVEAC),this.w_DMVFLVEAC,this.w_MVFLVEAC)
    this.w_MVTFRAGG = iif(empty(this.w_MVTFRAGG),this.w_DMVTFRAGG,this.w_MVTFRAGG)
    this.w_MVCODPAG = iif(empty(this.w_MVCODPAG),this.w_DMVCODPAG,this.w_MVCODPAG)
    this.w_MVSPEINC = iif(this.w_MVSPEINC=0,this.w_DMVSPEINC,this.w_MVSPEINC)
    if !EMPTY(NVL(this.w_MVCODPAG,""))
      * --- Read from PAG_AMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PASCONTO"+;
          " from "+i_cTable+" PAG_AMEN where ";
              +"PACODICE = "+cp_ToStrODBC(this.w_MVCODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PASCONTO;
          from (i_cTable) where;
              PACODICE = this.w_MVCODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVSCOPAG = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVTCAMAG = iif(empty(this.w_MVTCAMAG),this.w_DMVTCAMAG,this.w_MVTCAMAG)
    this.w_MVFLACCO = iif(empty(this.w_MVFLACCO),this.w_DMvFLACCO,this.w_MVFLACCO)
    this.w_MVCODVAL = icase(Not empty(this.w_MVCODVAL),this.w_MVCODVAL,Not empty(this.w_DMVCODVAL),this.w_DMVCODVAL,g_perval)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_MVCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAONAZ = GETCAM( this.w_MVVALNAZ , this.w_MVDATREG , 0)
    this.w_MVCAOVAL = iif(empty(this.w_MVCAOVAL),GETCAM(this.w_MVCODVAL, IIF(this.w_MVFLVEAC="A",this.w_MVDATREG,this.w_MVDATDOC), 0),this.w_MVCAOVAL)
    this.w_MVVALNAZ = iif(empty(this.w_MVVALNAZ),g_perval,this.w_MVVALNAZ)
    this.w_MVFLPROV = iif(empty(this.w_MVFLPROV),"N",this.w_MVFLPROV)
    this.w_MVFLSCAF = IIF(this.oParentObject.w_FLRAT="S"," ","S")
    this.w_MVNUMREG = iif(empty(this.w_MVNUMREG),this.w_MVNUMDOC,this.w_MVNUMREG)
    this.w_MVDATCIV = iif(empty(this.w_MVDATCIV),this.w_MVDATREG,this.w_MVDATCIV)
    this.w_MVIVABOL = iif(empty(this.w_MVIVABOL),IIF(this.w_MVFLVEAC="V", g_COIBOL, g_COABOL),this.w_MVIVABOL)
    this.w_MVCONCON = iif(empty(this.w_MVCONCON),this.w_DMVCONCON,this.w_MVCONCON)
    this.w_MVMINTRA = substr(time(),4,2)
    this.w_MVORATRA = left(time(),2)
    this.w_MVDATTRA = iif(empty(this.w_MVDATTRA),this.w_MVDATREG,this.w_MVDATTRA)
    this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
    this.w_MVIVACAU = iif(empty(this.w_MVIVACAU),g_COICAU,this.w_MVIVACAU)
    this.w_MVIVAIMB = iif(empty(NVL(this.w_MVIVAIMB, " ")),g_coiimb,this.w_MVIVAIMB)
    if Not empty(this.w_MVIVAIMB) AND Not empty(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVIVAIMB = IIF(NOT EMPTY(Nvl(this.w_MVCODIVE," ")), IIF(this.w_PERIVE<this.w_PERIMB,this.w_MVCODIVE,this.w_MVIVAIMB), this.w_MVIVAIMB)
    endif
    this.w_MVIVATRA = iif(empty(NVL(this.w_MVIVATRA, " ")),g_COITRA,this.w_MVIVATRA)
    if Not empty(this.w_MVIVATRA) AND Not empty(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVATRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVATRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERTRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVIVATRA = IIF(NOT EMPTY(Nvl(this.w_MVCODIVE," ")), IIF(this.w_PERIVE<this.w_PERTRA,this.w_MVCODIVE,this.w_MVIVATRA), this.w_MVIVATRA)
    endif
    this.w_MVIVAINC = iif(empty(this.w_MVIVAINC),g_COIINC,this.w_MVIVAINC)
    if Not empty(this.w_MVIVAINC) AND Not empty(this.w_MVCODIVE)
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVIVAINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVIVAINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVIVAINC = IIF(NOT EMPTY(Nvl(this.w_MVCODIVE," ")), IIF(this.w_PERIVE<this.w_PERINC,this.w_MVCODIVE,this.w_MVIVAINC), this.w_MVIVAINC)
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCLADOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +",MVCAUCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +",MVTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +",MVFLACCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +",MVFLINTE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +",MVFLVEAC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +",MVTFRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +",MVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_MAST','MVDATREG');
      +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +",MVANNDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODESE),'DOC_MAST','MVCODESE');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +",MVDATEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEST),'DOC_MAST','MVDATEST');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFEST),'DOC_MAST','MVALFEST');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVFLSCAF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCAF),'DOC_MAST','MVFLSCAF');
      +",MVTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +",MVCODORN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODORN),'DOC_MAST','MVCODORN');
      +",MVTCOLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +",MVTIPORN ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPORN),'DOC_MAST','MVTIPORN');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBA2),'DOC_MAST','MVCODBA2');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVFLSCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +",MVPRD ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
      +",MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +",MVNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +",MVALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +",MVDATCIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONCON),'DOC_MAST','MVCONCON');
      +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVMINTRA),'DOC_MAST','MVMINTRA');
      +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVORATRA),'DOC_MAST','MVORATRA');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'DOC_MAST','UTDV');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCV');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'DOC_MAST','UTDC');
      +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
      +",MVIVACAU ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVACAU),'DOC_MAST','MVIVACAU');
      +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATTRA),'DOC_MAST','MVDATTRA');
      +",MVRIFDIC ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DOC_MAST','MVRIFDIC');
      +",MVCODDES ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +",MVCODVET ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +",MVCODPOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +",MVCODSPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +",MVCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
      +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.w_DMVNUMCOR),'DOC_MAST','MVNUMCOR');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.w_AGCZOAGE),'DOC_MAST','MVCODAG2');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVEMERIC ="+cp_NullLink(cp_ToStrODBC(this.w_MVEMERIC),'DOC_MAST','MVEMERIC');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVCLADOC = this.w_MVCLADOC;
          ,MVCAUCON = this.w_MVCAUCON;
          ,MVTCAMAG = this.w_MVTCAMAG;
          ,MVFLACCO = this.w_MVFLACCO;
          ,MVFLINTE = this.w_MVFLINTE;
          ,MVFLVEAC = this.w_MVFLVEAC;
          ,MVTFRAGG = this.w_MVTFRAGG;
          ,MVTIPCON = this.w_MVTIPCON;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVFLPROV = this.w_MVFLPROV;
          ,MVCODPAG = this.w_MVCODPAG;
          ,MVDATREG = this.w_MVDATREG;
          ,MVDATDOC = this.w_MVDATDOC;
          ,MVANNDOC = this.w_MVANNDOC;
          ,MVANNPRO = this.w_MVANNPRO;
          ,MVCODESE = this.w_MVCODESE;
          ,MVPRP = this.w_MVPRP;
          ,MVNUMEST = this.w_MVNUMEST;
          ,MVDATEST = this.w_MVDATEST;
          ,MVALFEST = this.w_MVALFEST;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVFLSCAF = this.w_MVFLSCAF;
          ,MVTIPDOC = this.w_MVTIPDOC;
          ,MVCODORN = this.w_MVCODORN;
          ,MVTCOLIS = this.w_MVTCOLIS;
          ,MVTIPORN = this.w_MVTIPORN;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVCODBA2 = this.w_MVCODBA2;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVFLSCOR = this.w_MVFLSCOR;
          ,MVPRD = this.w_MVPRD;
          ,MVNUMDOC = this.w_MVNUMDOC;
          ,MVNUMREG = this.w_MVNUMREG;
          ,MVALFDOC = this.w_MVALFDOC;
          ,MVDATCIV = this.w_MVDATCIV;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVCONCON = this.w_MVCONCON;
          ,MVMINTRA = this.w_MVMINTRA;
          ,MVORATRA = this.w_MVORATRA;
          ,UTCC = i_CODUTE;
          ,UTDV = SetInfoDate(g_CALUTD);
          ,UTCV = i_CODUTE;
          ,UTDC = SetInfoDate(g_CALUTD);
          ,MVFLSCOM = this.w_MVFLSCOM;
          ,MVIVACAU = this.w_MVIVACAU;
          ,MVDATTRA = this.w_MVDATTRA;
          ,MVRIFDIC = this.w_MVRIFDIC;
          ,MVCODDES = this.w_MVCODDES;
          ,MVCODVET = this.w_MVCODVET;
          ,MVCODPOR = this.w_MVCODPOR;
          ,MVCODSPE = this.w_MVCODSPE;
          ,MVCODAGE = this.w_MVCODAGE;
          ,MVNUMCOR = this.w_DMVNUMCOR;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVCODAG2 = this.w_AGCZOAGE;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVEMERIC = this.w_MVEMERIC;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVIVAIMB = this.w_MVIVAIMB;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno righe documento
    *     GSAR_BRD
    *     popolo righe documento con GENEAPP
    * --- Inserimento dettaglio documento se tutto ok nella testata
    this.w_TIPREC = "D"
    * --- Select from GSVADKIM
    do vq_exec with 'GSVADKIM',this,'_Curs_GSVADKIM','',.f.,.t.
    if used('_Curs_GSVADKIM')
      select _Curs_GSVADKIM
      locate for 1=1
      do while not(eof())
      this.w_RIGA = _Curs_GSVADKIM.CPROWNUM
      * --- Insert into DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\VEFA\EXE\QUERY\GSVA_DET.VQR",this.DOC_DETT_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSVADKIM
        continue
      enddo
      use
    endif
    * --- Ricalcolo i totali
    GSAR_BRD(this,this.w_SERIAL,.F.,.T.,.T.,.T.,.F.,.T.,.F.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Used("GENEAPP")
      this.w_OLDUNIMIS = "@@@"
      SELECT GeneApp
      GO TOP
      SCAN 
      * --- Scrive il Dettaglio
      this.w_MVSERIAL = t_MVSERIAL
      this.w_MVIMPSCO = t_MVIMPSCO
      this.w_CPROWNUM = t_CPROWNUM
      this.w_RIGA = t_CPROWNUM
      this.w_MVFLOMAG = t_MVFLOMAG
      this.w_MVNUMRIF = t_MVNUMRIF
      this.w_MVCODIVA = t_MVCODIVA
      this.w_MVTIPRIG = t_MVTIPRIG
      this.w_MVVALRIG = t_MVVALRIG
      this.w_MVCODICE = t_MVCODICE
      this.w_MVPERPRO = t_MVPERPRO
      this.w_MVCODART = t_MVCODART
      this.w_MVIMPPRO = t_MVIMPPRO
      this.w_MVDESART = t_MVDESART
      this.w_MVSERRIF = t_MVSERRIF
      this.w_MVDESSUP = t_MVDESSUP
      this.w_MVROWRIF = t_MVROWRIF
      this.w_MVUNIMIS = t_MVUNIMIS
      this.w_MVPESNET = t_MVPESNET
      this.w_MVCATCON = t_MVCATCON
      this.w_MVFLTRAS = t_MVFLTRAS
      this.w_MVCONTRO = t_MVCONTRO
      this.w_MVNOMENC = t_MVNOMENC
      this.w_MVCODCLA = t_MVCODCLA
      this.w_MVUMSUPP = t_MVUMSUPP
      this.w_MVCONTRA = t_MVCONTRA
      this.w_MVMOLSUP = t_MVMOLSUP
      this.w_MVCODLIS = IIF(Not Empty(nvl(t_MVCODLIS," ")),t_MVCODLIS,this.w_MVTCOLIS)
      this.w_MVNUMCOL = t_MVNUMCOL
      this.w_MVQTAMOV = t_MVQTAMOV
      this.w_MVQTAUM1 = t_MVQTAUM1
      this.w_MVCONIND = t_MVCONIND
      this.w_MVPREZZO = t_MVPREZZO
      this.w_MVVALMAG = t_MVVALMAG
      this.w_MVSCONT1 = t_MVSCONT1
      this.w_MVIMPNAZ = t_MVIMPNAZ
      this.w_MVSCONT2 = t_MVSCONT2
      this.w_MVIMPACC = t_MVIMPACC
      this.w_MVSCONT3 = t_MVSCONT3
      this.w_MVSCONT4 = t_MVSCONT4
      this.w_MVCODCOM = t_MVCODCOM
      this.w_MVIMPCOM = t_MVIMPCOM
      this.w_MVIMPAC2 = t_MVIMPAC2
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MV_FLAGG,MVCAUMAG,MVCODART,MVCODMAG,MVKEYSAL,MVQTAUM1,MVUNIMIS,MVVOCCEN,MV_SEGNO,MVCODMAT,MVDATEVA,MVFLCOCO,MVFLORCO,MVFLELAN,MVCODATT,MVQTAEV1,MVQTAEVA,MVIMPEVA,MVFLNOAN,MVCODUBI,MVCODUB2"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MV_FLAGG,MVCAUMAG,MVCODART,MVCODMAG,MVKEYSAL,MVQTAUM1,MVUNIMIS,MVVOCCEN,MV_SEGNO,MVCODMAT,MVDATEVA,MVFLCOCO,MVFLORCO,MVFLELAN,MVCODATT,MVQTAEV1,MVQTAEVA,MVIMPEVA,MVFLNOAN,MVCODUBI,MVCODUB2;
          from (i_cTable) where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.w_CPROWNUM;
              and MVNUMRIF = this.w_MVNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MV_FLAGG = NVL(cp_ToDate(_read_.MV_FLAGG),cp_NullValue(_read_.MV_FLAGG))
        this.w_MVCAUMAG = NVL(cp_ToDate(_read_.MVCAUMAG),cp_NullValue(_read_.MVCAUMAG))
        this.w_MVCODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
        this.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        this.w_MVKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
        this.w_MVQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
        this.w_MVUNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
        this.w_MVVOCCEN = NVL(cp_ToDate(_read_.MVVOCCEN),cp_NullValue(_read_.MVVOCCEN))
        this.w_MV_SEGNO = NVL(cp_ToDate(_read_.MV_SEGNO),cp_NullValue(_read_.MV_SEGNO))
        this.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
        this.w_MVDATEVA = NVL(cp_ToDate(_read_.MVDATEVA),cp_NullValue(_read_.MVDATEVA))
        this.w_MVFLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
        this.w_MVFLORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
        this.w_MVFLELAN = NVL(cp_ToDate(_read_.MVFLELAN),cp_NullValue(_read_.MVFLELAN))
        this.w_MVCODATT = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
        this.w_QTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
        this.w_QTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
        this.w_IMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
        this.w_DMVFLNOAN = NVL(cp_ToDate(_read_.MVFLNOAN),cp_NullValue(_read_.MVFLNOAN))
        this.w_MVCODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
        this.w_MVCODUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART,CA__TIPO,CAOPERAT,CAUNIMIS,CAMOLTIP"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART,CA__TIPO,CAOPERAT,CAUNIMIS,CAMOLTIP;
          from (i_cTable) where;
              CACODICE = this.w_MVCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_DMVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
        this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVUNIMIS = iif(Empty(Nvl(t_MVUNIMIS," ")),this.w_UNMIS3,Nvl(t_MVUNIMIS," "))
      if EMPTY(this.w_MVCAUMAG) AND Not EMPTY(this.w_MVTCAMAG)
        this.w_MVCAUMAG = this.w_MVTCAMAG
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMCAUCOL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLAVAL,CMFLCOMM,CMVARVAL"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.w_MVCAUMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMCAUCOL,CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE,CMFLAVAL,CMFLCOMM,CMVARVAL;
            from (i_cTable) where;
                CMCODICE = this.w_MVCAUMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
          this.w_DMVFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_DMVFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          this.w_DMVFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
          this.w_DMVFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
          this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          this.w_FLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
          this.w_MV_FLAGG = NVL(cp_ToDate(_read_.CMVARVAL),cp_NullValue(_read_.CMVARVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not EMPTY(this.w_MVCAUCOL)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MVCAUCOL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLRISE,CMFLORDI,CMFLIMPE;
              from (i_cTable) where;
                  CMCODICE = this.w_MVCAUCOL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DMVF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_DMVF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_DMVF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_DMVF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      this.w_MVVALULT = IIF(this.w_MVQTAUM1=0, 0, cp_ROUND(this.w_MVIMPNAZ/this.w_MVQTAUM1, this.w_DECTOT))
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODIVA,ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARCATCON,ARCODCLA,ARVOCCEN,ARVOCRIC,ARFLLOTT,ARMAGPRE,ARPESNET,ARNOMENC,ARUMSUPP,ARMOLSUP,ARFLUSEP,ARPREZUM,ARFLSERG,ARRIPCON"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODIVA,ARUNMIS1,ARUNMIS2,ARMOLTIP,AROPERAT,ARCATCON,ARCODCLA,ARVOCCEN,ARVOCRIC,ARFLLOTT,ARMAGPRE,ARPESNET,ARNOMENC,ARUMSUPP,ARMOLSUP,ARFLUSEP,ARPREZUM,ARFLSERG,ARRIPCON;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DMVCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
        this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_DMVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
        this.w_DMVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
        this.w_DMVVOCCEN = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
        this.w_DMVVOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
        this.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        this.w_MVPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
        this.w_MVNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
        this.w_MVUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
        this.w_MVMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
        this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
        this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
        this.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
        this.w_ARRIPCON = NVL(cp_ToDate(_read_.ARRIPCON),cp_NullValue(_read_.ARRIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLFRAZ,UMMODUM2"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.w_UNMIS1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLFRAZ,UMMODUM2;
          from (i_cTable) where;
              UMCODICE = this.w_UNMIS1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
        this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_MVUNIMIS) AND this.w_OLDUNIMIS<>this.w_MVUNIMIS
        if Not Empty(this.w_UNMIS1)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMDESCRI"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.w_MVUNIMIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMDESCRI;
              from (i_cTable) where;
                  UMCODICE = this.w_MVUNIMIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESUM = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
            use
            if i_Rows=0
              this.w_LOG = ah_Msgformat("Unit� di misura: %1 non presente", this.w_MVUNIMIS)
              this.w_OKTRAD = .F.
              * --- Raise
              i_Error="Errore"
              return
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if ! CHKUNIMI(IIF(this.w_FLSERG="S", "***", this.w_MVUNIMIS), this.w_UNMIS1, this.w_UNMIS2, this.w_UNMIS3 ,"S")
            this.w_LOG = ah_Msgformat("Unit� di misura: %1 non congruente", this.w_MVUNIMIS)
            this.w_OKTRAD = .F.
            * --- Raise
            i_Error="Errore"
            return
          endif
        endif
      endif
      this.w_MVFLCOCO = IIF(Not empty(this.w_MVFLCOCO),this.w_MVFLCOCO,IIF(g_COMM="S" , iif(this.w_FLCOMM="C","+",IIF(this.w_FLCOMM="S","-"," "))," "))
      this.w_MVFLORCO = IIF(Not empty(this.w_MVFLORCO),this.w_MVFLORCO,IIF(g_COMM="S" ,iif(this.w_FLCOMM="I","+",IIF(this.w_FLCOMM="D","-"," "))," "))
      this.w_MVDATEVA = IIF(Not Empty(this.w_MVDATEVA),this.w_MVDATEVA,this.w_MVDATDOC)
      this.w_MVFLRISE = IIF(Not Empty(Nvl(this.w_MVFLRISE," ")),this.w_MVFLRISE,this.w_DMVFLRISE)
      this.w_MVFLCASC = IIF(Not Empty(Nvl(this.w_MVFLCASC," ")),this.w_MVFLCASC,this.w_DMVFLCASC)
      this.w_MVFLIMPE = IIF(Not Empty(Nvl(this.w_MVFLIMPE," ")),this.w_MVFLIMPE,this.w_DMVFLIMPE)
      this.w_MVFLORDI = IIF(Not Empty(Nvl(this.w_MVFLORDI," ")),this.w_MVFLORDI,this.w_DMVFLORDI)
      this.w_MVF2ORDI = IIF(Not Empty(Nvl(this.w_MVF2ORDI," ")),this.w_MVF2ORDI,this.w_DMVF2ORDI)
      this.w_MVF2CASC = IIF(Not Empty(Nvl(this.w_MVF2CASC," ")),this.w_MVF2CASC,this.w_DMVF2CASC)
      this.w_MVF2IMPE = IIF(Not Empty(Nvl(this.w_MVF2IMPE," ")),this.w_MVF2IMPE,this.w_DMVF2IMPE)
      this.w_MVF2RISE = IIF(Not Empty(Nvl(this.w_MVF2RISE," ")),this.w_MVF2RISE,this.w_DMVF2RISE)
      this.w_OMAG = IIF(EMPTY(this.w_MVCODMAG), IIF(EMPTY(this.w_OMAG),IIF(EMPTY(this.w_COMAG), g_MAGAZI, this.w_COMAG),this.w_OMAG), this.w_MVCODMAG)
      this.w_OMAT = IIF(EMPTY(this.w_MVCODMAT), IIF(EMPTY(this.w_OMAT),this.w_COMAT,this.w_OMAT), this.w_MVCODMAT)
      this.w_DMVCODMAG = IIF(this.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.w_MVFLCASC+this.w_MVFLRISE+this.w_MVFLORDI+this.w_MVFLIMPE))Or (this.w_MV_FLAGG $"+-" And Empty(this.w_MVSERRIF))), IIF(NOT EMPTY(IIF(this.w_FLLOTT<>"C",SPACE(5),this.w_MVCODMAG)),IIF(this.w_FLLOTT<>"C",SPACE(5),this.w_MVCODMAG), CALCMAG(1, this.w_FLMGPR, "     ", this.w_COMAG, this.w_OMAG, this.w_MAGPRE, this.w_MAGTER)), SPACE(5))
      this.w_DMVCODMAT = IIF(this.w_MVTIPRIG="R" AND NOT EMPTY(this.w_MVF2CASC+this.w_MVF2RISE+this.w_MVF2ORDI+this.w_MVF2IMPE), CALCMAG(1, this.w_FLMTPR, "     ", this.w_COMAT, this.w_OMAT, this.w_MAGPRE, this.w_MAGTER), SPACE(5))
      this.w_MVCODMAG = IIF(Not Empty(Nvl(this.w_MVCODMAG," ")),this.w_MVCODMAG,this.w_DMVCODMAG)
      this.w_MVCODMAT = IIF(Not Empty(Nvl(this.w_MVCODMAT," ")),this.w_MVCODMAT,this.w_DMVCODMAT)
      this.w_MVCODART = IIF(Not Empty(Nvl(this.w_MVCODART," ")),this.w_MVCODART,this.w_DMVCODART)
      this.w_MVTIPRIG = IIF(Not Empty(Nvl(this.w_MVTIPRIG," ")),this.w_MVTIPRIG,this.w_DMVTIPRIG)
      this.w_MVCODIVA = IIF(Not Empty(Nvl(this.w_MVCODIVA," ")),this.w_MVCODIVA,this.w_DMVCODIVA)
      this.w_MVCATCON = IIF(Not Empty(Nvl(this.w_MVCATCON," ")),this.w_MVCATCON,this.w_DMVCATCON)
      this.w_MVCODCLA = IIF(Not Empty(Nvl(this.w_MVCODCLA," ")),this.w_MVCODCLA,this.w_DMVCODCLA)
      this.w_MVVOCCEN = iif( empty(Nvl(this.w_MVVOCCEN," ")), IIF(this.w_MVFLVEAC="A", this.w_DMVVOCCEN, this.w_DMVVOCRIC), this.w_MVVOCCEN )
      this.w_MVUNIMIS = IIF(Not Empty(Nvl(this.w_MVUNIMIS," ")),this.w_MVUNIMIS,IIF(NOT EMPTY(this.w_UNMIS3) AND this.w_MOLTI3<>0, this.w_UNMIS3, this.w_UNMIS1))
      this.w_MVFLOMAG = IIF(Not Empty(Nvl(this.w_MVFLOMAG," ")),this.w_MVFLOMAG,"X")
      this.w_MV_SEGNO = iif(empty(Nvl(this.w_MV_SEGNO," ")),this.w_DMV_SEGNO,this.w_MV_SEGNO)
      this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.w_MVFLCASC+this.w_MVFLRISE+this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVF2CASC+this.w_MVF2RISE+this.w_MVF2ORDI+this.w_MVF2IMPE))Or (this.w_MV_FLAGG $"+-" And Empty(this.w_MVSERRIF)) ), this.w_MVCODART, SPACE(20))
      this.w_MVFLELAN = iif(empty(this.w_MVFLELAN),this.w_FLELAN,this.w_MVFLELAN)
      this.w_MVTIPPRO = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
      this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
      this.w_MVDATOAI = i_DATSYS
      if Not empty(this.w_MVKEYSAL) AND Not empty(this.w_MVCODMAG)
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLDATUCA,SLDATUPV"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLDATUCA,SLDATUPV;
            from (i_cTable) where;
                SLCODICE = this.w_MVKEYSAL;
                and SLCODMAG = this.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
          this.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_MVFLULCA = IIF(this.w_FLAVA1="A" AND this.w_MVFLCASC="+" AND this.w_MVDATDOC>=this.w_ULTCAR AND this.w_MVFLOMAG<>"S", "=", " ")
      this.w_MVFLULPV = IIF(this.w_FLAVA1="V" AND this.w_MVFLCASC="-" AND this.w_MVDATDOC>=this.w_ULTSCA AND this.w_MVFLOMAG<>"S" AND (this.w_MVCLADOC<>"DT" OR this.w_MVPREZZO<>0), "=", " ")
      if this.w_MVQTAUM1=0 AND this.w_MVTIPRIG<>"D" AND this.w_OKTRAD
        msg=""
        this.w_UMCAL = LEFT(this.w_UNMIS1+"   ",3)+LEFT(this.w_UNMIS2+"   ",3)+LEFT(this.w_UNMIS3+"   ",3)+LEFT(this.w_MVUNIMIS+"   ",3)+LEFT(this.w_OPERAT+" ",1)+LEFT(this.w_OPERA3+" ",1)
        this.w_MVQTAUM1 = iif( empty(this.w_MVQTAUM1).and.(!empty(this.w_MVQTAMOV)) ,CALQTA(this.w_MVQTAMOV,this.w_MVUNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, this.w_FLUSEP, this.w_NOFRAZ, this.w_MODUM2, @msg,this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3),this.w_MVQTAMOV)
        if not empty(msg)
          * --- Scrivo errore nel campo di log
          this.w_OKTRAD = .F.
          this.w_LOG = ah_msgformat(msg)
          * --- Raise
          i_Error="Errore"
          return
        endif
      endif
      this.w_MVQTASAL = this.w_MVQTAUM1
      if Not Empty(this.w_MVCODMAG)
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGFLUBIC,MGPROMAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGFLUBIC,MGPROMAG;
            from (i_cTable) where;
                MGCODMAG = this.w_MVCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
          this.w_MVPROORD = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_MVPROORD = iif(empty(this.w_MVPROORD), g_PROAZI,this.w_MVPROORD)
      if g_MADV="S"
        if Not Empty(this.w_MVCODMAT)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGFLUBIC"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGFLUBIC;
              from (i_cTable) where;
                  MGCODMAG = this.w_MVCODMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_MVFLLOTT = IIF(NOT EMPTY(this.w_MVFLLOTT),this.w_MVFLLOTT,IIF(this.w_FLLOTT $ "S-C" OR this.w_FLUBIC="S",LEFT(ALLTRIM(this.w_MVFLCASC)+IIF(this.w_MVFLRISE="+", "-", IIF(this.w_MVFLRISE="-", "+", " ")), 1), " "))
        this.w_MVF2LOTT = IIF(NOT EMPTY(this.w_MVF2LOTT),this.w_MVF2LOTT,IIF(this.w_FLLOTT $ "S-C" OR this.w_F2UBIC="S",LEFT(ALLTRIM(this.w_MVF2CASC) + IIF(this.w_MVF2RISE="+", "-", IIF(this.w_MVF2RISE="-", "+", " ")), 1), " "))
        this.w_MVCODLOT = IIF(this.w_FLLOTT $ "SC" AND (this.w_MVF2LOTT $ "+-" OR this.w_MVFLLOTT $ "+-") ,this.w_MVCODLOT,Space(20))
        this.w_MVCODUBI = IIF(Not Empty(this.w_MVCODMAG) AND this.w_MVFLLOTT $ "+-" ,this.w_MVCODUBI,Space(20))
        this.w_MVCODUB2 = IIF(Not Empty(this.w_MVCODMAT) AND this.w_MVF2LOTT $ "+-" ,this.w_MVCODUB2,Space(20))
      endif
      if Not Empty(Nvl(this.w_MVCODIVA," "))
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_MVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.oParentObject.w_FLRPZ="S" 
        * --- Ricalcolo il prezzo devo azzerare sconti presenti
        this.w_MVIMPSCO = 0
        * --- Memorizzo la qt� nella 1^ UM prima di lanciare CALPRZLI poich� all'interno
        *     della funzione potrebbe essere modificata da il Ricalcolo da Lotto di Riordino
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLVALUCA,SLCODVAA"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLVALUCA,SLCODVAA;
            from (i_cTable) where;
                SLCODICE = this.w_MVKEYSAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
          this.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSFLSCON,LSIVALIS"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_MVCODLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSFLSCON,LSIVALIS;
            from (i_cTable) where;
                LSCODLIS = this.w_MVCODLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
          this.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_QTAUM3 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        this.w_QTAUM2 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        DIMENSION pArrUm[9]
        pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
        this.w_PROG = "V" 
        DECLARE ARRCALC (16,1)
        * --- Azzero l'Array che verr� riempito dalla Funzione
         
 ARRCALC(1)=0
        * --- Se parametro='Z' allora non devo calcolare il contratto...
        *     Al posto del Gruppo merceologico passo 'XXXXXX' per eliminazione variabile GRUMER dal Body dei documenti.
        *     In questo caso rileggo il gruppo merceologico all'interno di CALPRZLI
        *     Stessa cosa per Categoria Sconti maggiorazioni dell'articolo
        this.w_CALPRZ = CalPrzli( Nvl(this.w_MVCONTRA," ") , this.w_MVTIPCON , this.w_MVCODLIS , this.w_MVCODART , "XXXXXX" , this.w_MVQTAUM1 , this.w_MVCODVAL , this.w_MVCAOVAL , this.w_DATCAL , this.w_CATCLI , "XXXXXX", this.w_CODVAA, this.w_XCONORN, this.w_CATCOM, this.w_MVFLSCOR, this.w_SCOLIS, this.w_VALUCA,this.w_PROG, @ARRCALC, this.w_PRZVAC, this.w_MVFLVEAC, "N", @pArrUm )
        this.w_MVCONTRA = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
        * --- Se il prezzo � calcolato da Listino/Contratto/U.C.A., U.P.V. LIPREZZO = al prezzo calcolato
        *     Nel caso in cui � presente un contratto valido, in ogni caso LIPREZZO = prezzo da contratto anche se 0
        *     Se non trovo un prezzo e non esiste contratto reimposto il prezzo precedentemente calcolato o inserito a mano
        *     Se lancio questo batch da Import documenti nel caso di Ricalcolo Qt� da Lotto di Riordino non devo ricalcolare il prezzo
        this.w_LIPREZZO = ARRCALC(5) 
        this.w_LISCON = ARRCALC(7)
        * --- Aggiorno gli sconti solo se arrivano da Listino/Contratto/Tabella ScontiMagg.
        *     solo se non sono nel ricalcolo Qt� da Lotto di Riordino in fase di Import
        this.w_MVSCONT1 = ARRCALC(1) 
        this.w_MVSCONT2 = ARRCALC(2) 
        this.w_MVSCONT3 = ARRCALC(3) 
        this.w_MVSCONT4 = ARRCALC(4) 
        this.w_IVACON = ARRCALC(12) 
        this.w_APPO = (IIF(Empty(this.w_UNMIS1),Space(3),this.w_UNMIS1))+(IIF(Empty(this.w_UNMIS2),Space(3),this.w_UNMIS2))+(IIF(Empty(this.w_UNMIS3),Space(3),this.w_UNMIS3))
        this.w_MVPREZZO = cp_Round(CALMMLIS(this.w_LIPREZZO, this.w_APPO+this.w_MVUNIMIS+this.w_OPERAT+this.w_OPERA3+IIF(this.w_LISCON=2, this.w_IVALIS, "N")+"P"+ALLTRIM(STR(this.w_DECUNI)), this.w_MOLTIP, this.w_MOLTI3, IIF(this.w_MVFLSCOR="S",0,this.w_PERIVA)),this.w_DECUNI)
        this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
      endif
      if Not Empty(Nvl(this.w_MVCODICE," "))
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CADESSUP"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CADESSUP;
            from (i_cTable) where;
                CACODICE = this.w_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.w_LDESSUP = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          use
          if i_Rows=0
            if Not Empty(Nvl(this.w_MVCODART," "))
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARDESART,ARDESSUP"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARDESART,ARDESSUP;
                  from (i_cTable) where;
                      ARCODART = this.w_MVCODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                this.w_LDESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVDESART = iif(Not Empty(Nvl(this.w_MVDESART," ")),this.w_MVDESART,this.w_LDESART)
        this.w_MVDESSUP = iif(Not Empty(Nvl(this.w_MVDESSUP," ")),this.w_MVDESSUP,this.w_LDESSUP)
      endif
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODLIN"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODLIN;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPCON;
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Legge la Traduzione
      DECLARE ARRRIS (2) 
 a=Tradlin(this.w_MVCODICE,this.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
      if Not Empty(this.w_DESCOD)
        this.w_MVDESART = this.w_DESCOD
        this.w_MVDESSUP = this.w_DESSUP
      endif
      * --- Aggiorno valore fiscale
      this.w_MVVALMAG = CAVALMAG(this.w_MVFLSCOR,this.w_MVVALRIG,Nvl(this.w_MVIMPSCO,0),Nvl(this.w_MVIMPACC,0),this.w_PERIVA,this.w_DECTOT,this.w_MVCODIVE,this.w_PERIVE,,this.w_TDRIPCON,this.w_ARRIPCON,0)
      this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, this.w_MVCODIVE, this.w_PERIVE, this.w_INDIVE, this.w_PERIVA, this.w_INDIVA )
      this.w_QTAEV1 = Nvl(this.w_QTAEV1,0)
      this.w_QTAEVA = Nvl(this.w_QTAEVA,0)
      this.w_IMPEVA = Nvl(this.w_IMPEVA,0)
      this.w_MVFLNOAN = iif(Nvl(this.w_DMVFLNOAN,"N")="S","S","N")
      this.w_OLDUNIMIS = this.w_MVUNIMIS
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
        +",MVCODART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
        +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
        +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
        +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
        +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
        +",MVCODLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
        +",MVCONIND ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONIND),'DOC_DETT','MVCONIND');
        +",MVCONTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
        +",MVCONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRO),'DOC_DETT','MVCONTRO');
        +",MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
        +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
        +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
        +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
        +",MVIMPAC2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPAC2),'DOC_DETT','MVIMPAC2');
        +",MVIMPACC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
        +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
        +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
        +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
        +",MVIMPSCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
        +",MVMOLSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
        +",MVNOMENC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
        +",MVNUMCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMCOL),'DOC_DETT','MVNUMCOL');
        +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
        +",MVPESNET ="+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
        +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
        +",MVPROORD ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROORD),'DOC_DETT','MVPROORD');
        +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
        +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
        +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
        +",MVROWRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
        +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
        +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
        +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
        +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
        +",MVSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
        +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
        +",MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
        +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
        +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_MVUMSUPP),'DOC_DETT','MVUMSUPP');
        +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
        +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
        +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
        +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCASC),'DOC_DETT','MVFLCASC');
        +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRISE),'DOC_DETT','MVFLRISE');
        +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORDI),'DOC_DETT','MVFLORDI');
        +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLIMPE),'DOC_DETT','MVFLIMPE');
        +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2CASC),'DOC_DETT','MVF2CASC');
        +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2RISE),'DOC_DETT','MVF2RISE');
        +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2ORDI),'DOC_DETT','MVF2ORDI');
        +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2IMPE),'DOC_DETT','MVF2IMPE');
        +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
        +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCOL),'DOC_DETT','MVCAUCOL');
        +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
        +",MV_FLAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MV_FLAGG),'DOC_DETT','MV_FLAGG');
        +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
        +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
        +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
        +",MVCODLOT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODLOT),'DOC_DETT','MVCODLOT');
        +",MVCODUBI ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUBI),'DOC_DETT','MVCODUBI');
        +",MVCODUB2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUB2),'DOC_DETT','MVCODUB2');
        +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
        +",MVCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
        +",MVCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
        +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
        +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC("A"),'DOC_DETT','MVTIPATT');
        +",MVFLRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
        +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
        +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLULCA),'DOC_DETT','MVFLULCA');
        +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLULPV),'DOC_DETT','MVFLULPV');
        +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_QTAEVA),'DOC_DETT','MVQTAEVA');
        +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAEV1),'DOC_DETT','MVQTAEV1');
        +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_IMPEVA),'DOC_DETT','MVIMPEVA');
        +",MVFLNOAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLNOAN),'DOC_DETT','MVFLNOAN');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVCATCON = this.w_MVCATCON;
            ,MVCODART = this.w_MVCODART;
            ,MVCODCLA = this.w_MVCODCLA;
            ,MVCODCOM = this.w_MVCODCOM;
            ,MVCODICE = this.w_MVCODICE;
            ,MVCODIVA = this.w_MVCODIVA;
            ,MVCODLIS = this.w_MVCODLIS;
            ,MVCONIND = this.w_MVCONIND;
            ,MVCONTRA = this.w_MVCONTRA;
            ,MVCONTRO = this.w_MVCONTRO;
            ,MVDESART = this.w_MVDESART;
            ,MVDESSUP = this.w_MVDESSUP;
            ,MVFLOMAG = this.w_MVFLOMAG;
            ,MVFLTRAS = this.w_MVFLTRAS;
            ,MVIMPAC2 = this.w_MVIMPAC2;
            ,MVIMPACC = this.w_MVIMPACC;
            ,MVIMPCOM = this.w_MVIMPCOM;
            ,MVIMPNAZ = this.w_MVIMPNAZ;
            ,MVIMPPRO = this.w_MVIMPPRO;
            ,MVIMPSCO = this.w_MVIMPSCO;
            ,MVMOLSUP = this.w_MVMOLSUP;
            ,MVNOMENC = this.w_MVNOMENC;
            ,MVNUMCOL = this.w_MVNUMCOL;
            ,MVPERPRO = this.w_MVPERPRO;
            ,MVPESNET = this.w_MVPESNET;
            ,MVPREZZO = this.w_MVPREZZO;
            ,MVPROORD = this.w_MVPROORD;
            ,MVQTAMOV = this.w_MVQTAMOV;
            ,MVQTASAL = this.w_MVQTASAL;
            ,MVQTAUM1 = this.w_MVQTAUM1;
            ,MVROWRIF = this.w_MVROWRIF;
            ,MVSCONT1 = this.w_MVSCONT1;
            ,MVSCONT2 = this.w_MVSCONT2;
            ,MVSCONT3 = this.w_MVSCONT3;
            ,MVSCONT4 = this.w_MVSCONT4;
            ,MVSERRIF = this.w_MVSERRIF;
            ,MVTIPPR2 = this.w_MVTIPPR2;
            ,MVTIPPRO = this.w_MVTIPPRO;
            ,MVTIPRIG = this.w_MVTIPRIG;
            ,MVUMSUPP = this.w_MVUMSUPP;
            ,MVUNIMIS = this.w_MVUNIMIS;
            ,MVVALMAG = this.w_MVVALMAG;
            ,MVVALRIG = this.w_MVVALRIG;
            ,MVFLCASC = this.w_MVFLCASC;
            ,MVFLRISE = this.w_MVFLRISE;
            ,MVFLORDI = this.w_MVFLORDI;
            ,MVFLIMPE = this.w_MVFLIMPE;
            ,MVF2CASC = this.w_MVF2CASC;
            ,MVF2RISE = this.w_MVF2RISE;
            ,MVF2ORDI = this.w_MVF2ORDI;
            ,MVF2IMPE = this.w_MVF2IMPE;
            ,MVCAUMAG = this.w_MVCAUMAG;
            ,MVCAUCOL = this.w_MVCAUCOL;
            ,MVVOCCEN = this.w_MVVOCCEN;
            ,MV_FLAGG = this.w_MV_FLAGG;
            ,MVKEYSAL = this.w_MVKEYSAL;
            ,MVFLLOTT = this.w_MVFLLOTT;
            ,MVF2LOTT = this.w_MVF2LOTT;
            ,MVCODLOT = this.w_MVCODLOT;
            ,MVCODUBI = this.w_MVCODUBI;
            ,MVCODUB2 = this.w_MVCODUB2;
            ,MV_SEGNO = this.w_MV_SEGNO;
            ,MVCODMAG = this.w_MVCODMAG;
            ,MVCODMAT = this.w_MVCODMAT;
            ,MVDATEVA = this.w_MVDATEVA;
            ,MVTIPATT = "A";
            ,MVFLRAGG = this.w_MVTFRAGG;
            ,MVFLELAN = this.w_MVFLELAN;
            ,MVFLULCA = this.w_MVFLULCA;
            ,MVFLULPV = this.w_MVFLULPV;
            ,MVQTAEVA = this.w_QTAEVA;
            ,MVQTAEV1 = this.w_QTAEV1;
            ,MVIMPEVA = this.w_IMPEVA;
            ,MVFLNOAN = this.w_MVFLNOAN;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT GeneApp
      ENDSCAN
      if used("GeneApp")
        select GeneApp
        use
      endif
      * --- Eseguo gsar_brd per aggiornare campi di riga in funzione del cambiamento 
      *     di MVPREZZO
      GSAR_BRD(this,this.w_SERIAL,.F.,.T.,.T.,.T.,.F.,.T.,.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_TIPREC = "R"
    * --- Select from GSVASKIM_1
    do vq_exec with 'GSVASKIM_1',this,'_Curs_GSVASKIM_1','',.f.,.t.
    if used('_Curs_GSVASKIM_1')
      select _Curs_GSVASKIM_1
      locate for 1=1
      do while not(eof())
      this.w_RIGA = _Curs_GSVASKIM_1.CPROWNUM
      * --- Insert into DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\VEFA\EXE\QUERY\GSVA_RAT.VQR",this.DOC_RATE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSVASKIM_1
        continue
      enddo
      use
    endif
    this.w_TIPREC = "M"
    * --- Select from GSVAMKIM_1
    do vq_exec with 'GSVAMKIM_1',this,'_Curs_GSVAMKIM_1','',.f.,.t.
    if used('_Curs_GSVAMKIM_1')
      select _Curs_GSVAMKIM_1
      locate for 1=1
      do while not(eof())
      this.w_RIGA = _Curs_GSVAMKIM_1.MTROWNUM
      this.w_MATRICOLA = _Curs_GSVAMKIM_1.MTCODMAT
      * --- Insert into MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\VEFA\EXE\QUERY\GSVA_MAT.VQR",this.MOVIMATR_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_GSVAMKIM_1
        continue
      enddo
      use
    endif
    * --- --
    this.PAR=createobject("PARAMETRO")
    this.pDOCINFO=createobject("DOCOBJ",this.par)
    this.pDOCINFO.cMVSERIAL = this.w_SERIAL
    * --- Rileggo il documento dal database...
    this.pDOCINFO.cRead = "A"
    this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
    if Empty(this.w_RET.nErrorLog)
      if this.oParentObject.w_FLRAT="S"
        * --- Azzero rate importate ...
        * --- Delete from DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                RSSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        this.w_contrate = 0
        this.w_RIGA = 0
        this.w_TIPREC = "T"
        this.w_RET.mcRate.gotop()     
        do while not this.w_RET.mcRate.eof()
          if Nvl(this.w_RET.mcRate.IMPNET,0)>0
            this.w_contrate = this.w_contrate + 1
            * --- Insert into DOC_RATE
            i_nConn=i_TableProp[this.DOC_RATE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLPROV"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_RATE','RSSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_contrate),'DOC_RATE','RSNUMRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.DATRAT),'DOC_RATE','RSDATRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET),'DOC_RATE','RSIMPRAT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.MODPAG),'DOC_RATE','RSMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.FLPROV),'DOC_RATE','RSFLPROV');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_SERIAL,'RSNUMRAT',this.w_contrate,'RSDATRAT',this.w_RET.mcRate.DATRAT,'RSIMPRAT',this.w_RET.mcRate.IMPNET,'RSMODPAG',this.w_RET.mcRate.MODPAG,'RSFLPROV',this.w_RET.mcRate.FLPROV)
              insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLPROV &i_ccchkf. );
                 values (;
                   this.w_SERIAL;
                   ,this.w_contrate;
                   ,this.w_RET.mcRate.DATRAT;
                   ,this.w_RET.mcRate.IMPNET;
                   ,this.w_RET.mcRate.MODPAG;
                   ,this.w_RET.mcRate.FLPROV;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          this.w_RET.mcRate.Next()     
        enddo
      endif
      * --- Aggiorno i totali con il risultato di Castiva presente
      *     nei memory cursor dell'oggetto
      if this.oParentObject.w_FLCAST="S"
        this.w_contrate = 1
        this.w_RET.mcCastiva.gotop()     
         
 Dimension ArrWrite[2,2],ArrVal[1,3] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=this.w_SERIAL
        do while not this.w_RET.mcCastiva.eof()
          do case
            case this.w_contrate=1
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN1');
                +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS1');
                +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA1');
                +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM1');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN1 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS1 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA1 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM1 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=2
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN2');
                +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS2');
                +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA2');
                +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM2');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN2 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS2 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA2 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM2 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=3
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN3');
                +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS3');
                +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA3');
                +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM3');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN3 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS3 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA3 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM3 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=4
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN4');
                +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS4');
                +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA4');
                +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM4');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN4 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS4 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA4 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM4 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=5
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN5');
                +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS5');
                +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA5');
                +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM5');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN5 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS5 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA5 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM5 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_contrate=6
              * --- Write into DOC_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN6');
                +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS6');
                +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA6');
                +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM6');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                update (i_cTable) set;
                    MVAIMPN6 = this.w_RET.mcCastiva.IMPON;
                    ,MVAIMPS6 = this.w_RET.mcCastiva.IMPOS;
                    ,MVACIVA6 = this.w_RET.mcCastiva.CODIVA;
                    ,MVAFLOM6 = this.w_RET.mcCastiva.FLOMAG;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_SERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
          this.w_contrate = this.w_contrate+1
          this.w_RET.mcCastiva.Next()     
        enddo
      endif
    else
      * --- Scrivo errore nel campo di log
      this.w_RIGA = 0
      this.w_TIPREC = "T"
      this.w_OKTRAD = .F.
      this.w_LOG = this.w_RET.nErrorLog
      * --- Raise
      i_Error="Errore"
      return
    endif
    * --- Eseguo controlli finali sul documento
    if Not Empty(this.w_BATPOS)
      LTrsOk = .T.
       
 Dimension ARRCAMPI(1,2) 
 ARRCAMPI[1,1]="MVSERIAL" 
 ARRCAMPI[1,2]=this.w_SERIAL
      l_Prg = Alltrim(this.w_BATPOS)+"( @ArrCampi, 'Post', 'I', @LTrsOk)"
      this.w_LOG = &l_Prg
      this.w_TIPREC = "T"
      this.w_MATRICOLA = SPACE(40)
      this.w_RIGA = 0
      if Not LTrsOk
        * --- Se ci sono errori/worning 
        * --- Non devo tradurre msg di errore 
        this.w_OKTRAD = .F.
        this.w_TIPREC = "T"
        * --- Raise
        i_Error="Errore"
        return
      else
        this.w_LOG = ""
      endif
    endif
    if Not Empty(this.w_BATCHK)
      LTrsOk = .T.
      this.w_TIPREC = " "
       
 Dimension ARRCAMPI(1,2) 
 ARRCAMPI[1,1]="MVSERIAL" 
 ARRCAMPI[1,2]=this.w_SERIAL
      l_Prg = Alltrim(this.w_BATCHK)+"( This, 'I', @ArrCampi, .Null., @LTrsOk)"
      this.w_LOG = &l_Prg
      this.w_LOG = IIF(Not Empty(g_MSG),g_MSG,this.w_LOG)
      this.w_MATRICOLA = SPACE(40)
      this.w_RIGA = 0
      if Not LTrsOk
        * --- Se ci sono errori/worning 
        * --- Non devo tradurre msg di errore 
        this.w_OKTRAD = .F.
        this.w_TIPREC = iif(Empty(this.w_TIPREC),"T",this.w_TIPREC)
        * --- Raise
        i_Error="Errore"
        return
      else
        this.w_LOG = ""
      endif
    else
      this.w_LOG = " "
      l_Prg = .t.
    endif
    if empty(this.w_LOG) AND this.pTIPOPE = "CONFE" AND Not empty(this.w_SERIAL)
      * --- Read from TMPLIST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TMPLIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2],.t.,this.TMPLIST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LOGERROR"+;
          " from "+i_cTable+" TMPLIST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LOGERROR;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LOG = NVL(cp_ToDate(_read_.LOGERROR),cp_NullValue(_read_.LOGERROR))
        use
        if i_Rows=0
          * --- Aggiorno stautus Tabelle
          * --- Select from FLATDETT
          i_nConn=i_TableProp[this.FLATDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select FTCODICE,FTTABNAM  from "+i_cTable+" FLATDETT ";
                +" where FTCODICE="+cp_ToStrODBC(this.w_FLATAB)+"";
                +" group by FTCODICE,FTTABNAM";
                 ,"_Curs_FLATDETT")
          else
            select FTCODICE,FTTABNAM from (i_cTable);
             where FTCODICE=this.w_FLATAB;
             group by FTCODICE,FTTABNAM;
              into cursor _Curs_FLATDETT
          endif
          if used('_Curs_FLATDETT')
            select _Curs_FLATDETT
            locate for 1=1
            do while not(eof())
            this.w_CODTAB = _Curs_FLATDETT.FTTABNAM
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
              select _Curs_FLATDETT
              continue
            enddo
            use
          endif
          * --- Drop temporary table TMPVFDOCDETT
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCDETT')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCDETT')
          endif
          * --- Drop temporary table TMPVFDOCMAST
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCMAST')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCMAST')
          endif
          * --- Drop temporary table TMPVFDOCRATE
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCRATE')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCRATE')
          endif
          * --- Drop temporary table TMPVFMOVIMATR
          i_nIdx=cp_GetTableDefIdx('TMPVFMOVIMATR')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFMOVIMATR')
          endif
          * --- Ricostruisco tabelle temporanee che interrogano tabelle clone
          *     per aggiornare dati rispetto all'importazione eseguita
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Rilancio zoom
          this.oParentObject.Notifyevent("Esegui")
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.pTIPOPE = "CONFE" 
      * --- Aggiunge Alla Lista dei Documenti Generati
      this.w_DOCGENE = this.w_DOCGENE + 1
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVCODCON,MVFLVEAC,MVTIPDOC,MVCLADOC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPCON,MVCODCON,MVFLVEAC,MVTIPDOC,MVCLADOC;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Insert into LDOCGENE
      i_nConn=i_TableProp[this.LDOCGENE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LDOCGENE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LDOCGENE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LDSERIAL"+",CPROWNUM"+",LDSERDOC"+",LDNUMDOC"+",LDALFDOC"+",LDDATDOC"+",LDTIPCLF"+",LDCODCLF"+",LDPARAME"+",LDTIPDOC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'LDOCGENE','LDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DOCGENE),'LDOCGENE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'LDOCGENE','LDSERDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'LDOCGENE','LDNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'LDOCGENE','LDALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'LDOCGENE','LDDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'LDOCGENE','LDTIPCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'LDOCGENE','LDCODCLF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC+this.w_MVCLADOC),'LDOCGENE','LDPARAME');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'LDOCGENE','LDTIPDOC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LDSERIAL',this.w_SERIALE,'CPROWNUM',this.w_DOCGENE,'LDSERDOC',this.w_SERIAL,'LDNUMDOC',this.w_MVNUMDOC,'LDALFDOC',this.w_MVALFDOC,'LDDATDOC',this.w_MVDATDOC,'LDTIPCLF',this.w_MVTIPCON,'LDCODCLF',this.w_MVCODCON,'LDPARAME',this.w_MVFLVEAC+this.w_MVCLADOC,'LDTIPDOC',this.w_MVTIPDOC)
        insert into (i_cTable) (LDSERIAL,CPROWNUM,LDSERDOC,LDNUMDOC,LDALFDOC,LDDATDOC,LDTIPCLF,LDCODCLF,LDPARAME,LDTIPDOC &i_ccchkf. );
           values (;
             this.w_SERIALE;
             ,this.w_DOCGENE;
             ,this.w_SERIAL;
             ,this.w_MVNUMDOC;
             ,this.w_MVALFDOC;
             ,this.w_MVDATDOC;
             ,this.w_MVTIPCON;
             ,this.w_MVCODCON;
             ,this.w_MVFLVEAC+this.w_MVCLADOC;
             ,this.w_MVTIPDOC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
    else
      this.w_OKTRAD = .F.
      this.w_TIPREC = "O"
      this.w_LOG = "Verifica OK"
      * --- Raise
      i_Error="Verifica ok"
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo tabelle temporanee
    if ChketStatus(alltrim(this.w_FLATAB), "DOC_MAST")=0
      this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DOC_MAST")
      this.w_COD_TMP = "TMPVFDOCMAST"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ChketStatus(alltrim(this.w_FLATAB), "DOC_DETT")=0
      this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DOC_DETT")
      this.w_COD_TMP = "TMPVFDOCDETT"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ChketStatus(alltrim(this.w_FLATAB), "DOC_RATE")=0
      this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DOC_RATE")
      this.w_COD_TMP = "TMPVFDOCRATE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if ChketStatus(alltrim(this.w_FLATAB), "MOVIMATR")=0
      this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "MOVIMATR")
      this.w_COD_TMP = "TMPVFMOVIMATR"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_APPLICATION="ad hoc ENTERPRISE"
      if ChketStatus(alltrim(this.w_FLATAB), "MOVILOTT")=0
        this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "MOVILOTT")
        this.w_COD_TMP = "TMPVFMOVILOTT"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if ChketStatus(alltrim(this.w_FLATAB), "ABBLOTUL")=0
        this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "ABBLOTUL")
        this.w_COD_TMP = "TMPVFABBLOTUL"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Create temporary table TMPLIST
    i_nIdx=cp_AddTableDef('TMPLIST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\VEFA\EXE\QUERY\GSVALBIM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPLIST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Drop temporary table TMPVFDOCDETT
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCDETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCDETT')
    endif
    * --- Drop temporary table TMPVFDOCMAST
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCMAST')
    endif
    * --- Drop temporary table TMPVFDOCRATE
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCRATE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCRATE')
    endif
    * --- Drop temporary table TMPVFMOVIMATR
    i_nIdx=cp_GetTableDefIdx('TMPVFMOVIMATR')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFMOVIMATR')
    endif
    * --- Drop temporary table TMPVFMOVILOTT
    i_nIdx=cp_GetTableDefIdx('TMPVFMOVILOTT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFMOVILOTT')
    endif
    * --- Drop temporary table TMPVFABBLOTUL
    i_nIdx=cp_GetTableDefIdx('TMPVFABBLOTUL')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFABBLOTUL')
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    * --- Drop temporary table TMPLIST
    i_nIdx=cp_GetTableDefIdx('TMPLIST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPLIST')
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il seriale da assegnare all'elaborazione
    * --- Try
    local bErr_053F4748
    bErr_053F4748=bTrsErr
    this.Try_053F4748()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_053F4748
    * --- End
  endproc
  proc Try_053F4748()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_LROWNUM = 0
    this.w_SERIALE = SPACE(10)
    i_Conn=i_TableProp[this.GENERDOC_IDX, 3]
    cp_NextTableProg(this,i_Conn,"SEGED","i_codazi,w_SERIALE")
    * --- Insert into GENERDOC
    i_nConn=i_TableProp[this.GENERDOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GENERDOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GENERDOC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GDSERIAL"+",GDDATGEN"+",GDPARAME"+",GDTIPOEL"+",GDFLRSPE"+",GDFLRRIG"+",GDFLRTIP"+",GDFLRSDO"+",GDFLCHKD"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",GDKEYRIF"+",GDNOTE"+",GDFLSIMU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'GENERDOC','GDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'GENERDOC','GDDATGEN');
      +","+cp_NullLink(cp_ToStrODBC(this.class),'GENERDOC','GDPARAME');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDTIPOEL');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDFLRSPE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDFLRRIG');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDFLRTIP');
      +","+cp_NullLink(cp_ToStrODBC("S"),'GENERDOC','GDFLRSDO');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDFLCHKD');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'GENERDOC','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'GENERDOC','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'GENERDOC','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'GENERDOC','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GDKEYRIF),'GENERDOC','GDKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(""),'GENERDOC','GDNOTE');
      +","+cp_NullLink(cp_ToStrODBC(IIF(this.pTIPOPE = "VERIF", "S", "N")),'GENERDOC','GDFLSIMU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GDSERIAL',this.w_SERIALE,'GDDATGEN',DATE(),'GDPARAME',this.class,'GDTIPOEL',"N",'GDFLRSPE',"N",'GDFLRRIG',"N",'GDFLRTIP',"N",'GDFLRSDO',"S",'GDFLCHKD',"N",'UTCC',i_CODUTE,'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0)
      insert into (i_cTable) (GDSERIAL,GDDATGEN,GDPARAME,GDTIPOEL,GDFLRSPE,GDFLRRIG,GDFLRTIP,GDFLRSDO,GDFLCHKD,UTCC,UTDC,UTCV,UTDV,GDKEYRIF,GDNOTE,GDFLSIMU &i_ccchkf. );
         values (;
           this.w_SERIALE;
           ,DATE();
           ,this.class;
           ,"N";
           ,"N";
           ,"N";
           ,"N";
           ,"S";
           ,"N";
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_GDKEYRIF;
           ,"";
           ,IIF(this.pTIPOPE = "VERIF", "S", "N");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ETIPOER = "L"
    this.w_LROWNUM = this.w_LROWNUM + 1
    this.w_EMESS = ah_msgformat("Inserimento dati di generazione")
    * --- Insert into LOG_GEND
    i_nConn=i_TableProp[this.LOG_GEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_GEND_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LGSERIAL"+",CPROWNUM"+",LGTIPRIG"+",LGMESSAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'LOG_GEND','LGSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LROWNUM),'LOG_GEND','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ETIPOER),'LOG_GEND','LGTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EMESS),'LOG_GEND','LGMESSAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LGSERIAL',this.w_SERIALE,'CPROWNUM',this.w_LROWNUM,'LGTIPRIG',this.w_ETIPOER,'LGMESSAG',this.w_EMESS)
      insert into (i_cTable) (LGSERIAL,CPROWNUM,LGTIPRIG,LGMESSAG &i_ccchkf. );
         values (;
           this.w_SERIALE;
           ,this.w_LROWNUM;
           ,this.w_ETIPOER;
           ,this.w_EMESS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.oParentObject.w_SERGDOC = this.w_SERIALE
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 CURS=READTABLE(this.w_CODSTR,"*","","",.F.,"FT___UID='"+this.oParentObject.w_SER_ELAB+"'")
    if Used((curs))
      this.w_Risultato = CurToTab(Curs, this.w_COD_TMP)
       
 use in (curs)
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if ChketStatus(alltrim(this.w_FLATAB), this.w_CODTAB)=0
      this.w_EDI_TAB = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTAB)
      * --- Aggiorno status elaborazione
       
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="FT___UID" 
 ArrVal[1,2]=this.oParentObject.w_SER_ELAB
       
 DIMENSION Arrval(1,2),ArrWrite(1,2) 
 Arrwrite[1,1]="FTSTATUS" 
 Arrwrite[1,2]="S" 
 WRITETABLE( this.w_EDI_TAB, @ArrWrite , @ArrVal , "",.F.,"" ) 
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico presenza della tabella
    if ChketStatus(alltrim(this.w_FLATAB), this.w_CODTAB)=0
       
 DIMENSION Arraval(1,2),Fil(1) 
 ARRAVAL[1,1]="FT___UID" 
 ARRAVAL[1,2]="@@@@@@@@@" 
 CURS=READTABLE(this.w_TMP_EDI,"*",@ARRAVAL,,,) 
 A=AFIELDS(FIL,CURS) 
 SELECT (CURS) 
 use in (CURS)
      this.w_EDI_TAB = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTAB)
      this.w_LOOP = 0
       
 Sel=""
       
 SelC=""
      do while this.w_LOOP<ALEN(fil,1)
        this.w_LOOP = this.w_LOOP+1
        this.w_FIELD = Fil(this.w_LOOP,1)
        this.w_INDTAB = I_DCX.GETfieldidx(Alltrim(this.w_CODTAB),Alltrim(this.w_FIELD))
        if SUBSTR(this.w_FIELD,1,2) <> "FT" and this.w_INDTAB>0
          * --- Escludo campi di sistema
          this.w_LENTAB = I_DCX.GEtfieldlen(Alltrim(this.w_CODTAB),this.w_INDTAB)
          this.w_TIPCAMPO = I_DCX.GEtfieldtype(Alltrim(this.w_CODTAB),this.w_INDTAB)
           
 NC=READTABLE(this.w_TMP_GEN,this.w_FIELD,@ARRREAD,,,)
          if used(NC)
            if this.w_TIPCAMPO="C"
              * --- Read from FLATDETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.FLATDETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "FTFLDDIM"+;
                  " from "+i_cTable+" FLATDETT where ";
                      +"FTCODICE = "+cp_ToStrODBC(this.w_FLATAB);
                      +" and FTTABNAM = "+cp_ToStrODBC(this.w_CODTAB);
                      +" and FTFLDNAM = "+cp_ToStrODBC(this.w_FIELD);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  FTFLDDIM;
                  from (i_cTable) where;
                      FTCODICE = this.w_FLATAB;
                      and FTTABNAM = this.w_CODTAB;
                      and FTFLDNAM = this.w_FIELD;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LENFLAT = NVL(cp_ToDate(_read_.FTFLDDIM),cp_NullValue(_read_.FTFLDDIM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_LENFLAT>this.w_LENTAB
                 
 sel=sel+"Rtrim("+alltrim(this.w_FIELD)+") as " +alltrim(this.w_FIELD) + "," 
              else
                 
 sel=sel+alltrim(this.w_FIELD)+"," 
              endif
            else
               
 sel=sel+alltrim(this.w_FIELD)+"," 
            endif
             
 selC=selC+alltrim(this.w_FIELD)+"," 
             
 SELECT (NC) 
 use in (NC)
          endif
        endif
      enddo
      * --- Elimino ultima virgola
       
 Sel=left(alltrim(Sel),len(alltrim(Sel))-1) 
 SelC=left(alltrim(SelC),len(alltrim(SelC))-1)
       
 L_FONTE_IDX=Cp_OpenTable( this.w_TMP_GEN) 
 L_nConn = i_TableProp[L_FONTE_IDX,3]
      this.w_STATUS = "S"
       
 Tab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 cmsql="Insert Into "+ alltrim(Tab) + " ( "+ alltrim(selC) +" ) Select " + sel + " from " + alltrim(this.w_EDI_TAB) + " Where FT___UID = " +cp_tostr(Alltrim(this.oParentObject.w_SER_ELAB)) + " and FTSTATUS is NULL  " 
 cp_SQL(L_nConn, cmsql, this.w_TMP_GEN )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pFILE,pCODSTR,pTIPOPE,pGranu)
    this.pFILE=pFILE
    this.pCODSTR=pCODSTR
    this.pTIPOPE=pTIPOPE
    this.pGranu=pGranu
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,47)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='DOC_RATE'
    this.cWorkTables[7]='ENT_DETT'
    this.cWorkTables[8]='ENT_MAST'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='LISTINI'
    this.cWorkTables[11]='LOG_GEND'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='MOVIMATR'
    this.cWorkTables[14]='SALDIART'
    this.cWorkTables[15]='TIP_DOCU'
    this.cWorkTables[16]='TIP_DOCU'
    this.cWorkTables[17]='*TMPDETTI'
    this.cWorkTables[18]='*TMPLIST'
    this.cWorkTables[19]='*TMPMASTI'
    this.cWorkTables[20]='*TMPMATRI'
    this.cWorkTables[21]='*TMPVFABBLOTUL'
    this.cWorkTables[22]='*TMPVFDOCDETT'
    this.cWorkTables[23]='*TMPVFDOCMAST'
    this.cWorkTables[24]='*TMPVFDOCRATE'
    this.cWorkTables[25]='*TMPVFMOVILOTT'
    this.cWorkTables[26]='*TMPVFMOVIMATR'
    this.cWorkTables[27]='TRS_DETT'
    this.cWorkTables[28]='TRS_MAST'
    this.cWorkTables[29]='VADETTFO'
    this.cWorkTables[30]='VAELEMEN'
    this.cWorkTables[31]='VALUTE'
    this.cWorkTables[32]='VAPREDEF'
    this.cWorkTables[33]='VASTRUTT'
    this.cWorkTables[34]='VATRASCO'
    this.cWorkTables[35]='VOCIIVA'
    this.cWorkTables[36]='XDC_FIELDS'
    this.cWorkTables[37]='VAPREDEF'
    this.cWorkTables[38]='FLATDETT'
    this.cWorkTables[39]='LDOCGENE'
    this.cWorkTables[40]='GENERDOC'
    this.cWorkTables[41]='*TMPDOCRATE'
    this.cWorkTables[42]='CAM_AGAZ'
    this.cWorkTables[43]='UNIMIS'
    this.cWorkTables[44]='DIC_INTE'
    this.cWorkTables[45]='AGENTI'
    this.cWorkTables[46]='DES_DIVE'
    this.cWorkTables[47]='PAG_AMEN'
    return(this.OpenAllTables(47))

  proc CloseCursors()
    if used('_Curs_FLATDETT')
      use in _Curs_FLATDETT
    endif
    if used('_Curs_LOG_GEND')
      use in _Curs_LOG_GEND
    endif
    if used('_Curs_GSVADKIM')
      use in _Curs_GSVADKIM
    endif
    if used('_Curs_GSVASKIM_1')
      use in _Curs_GSVASKIM_1
    endif
    if used('_Curs_GSVAMKIM_1')
      use in _Curs_GSVAMKIM_1
    endif
    if used('_Curs_FLATDETT')
      use in _Curs_FLATDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFILE,pCODSTR,pTIPOPE,pGranu"
endproc
