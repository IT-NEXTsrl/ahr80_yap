* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_mvp                                                        *
*              Valori predefiniti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-13                                                      *
* Last revis.: 2015-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsva_mvp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsva_mvp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsva_mvp")
  return

* --- Class definition
define class tgsva_mvp as StdPCForm
  Width  = 668
  Height = 293
  Top    = 4
  Left   = 251
  cComment = "Valori predefiniti"
  cPrg = "gsva_mvp"
  HelpContextID=221613463
  add object cnt as tcgsva_mvp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsva_mvp as PCContext
  w_PRCODSTR = space(10)
  w_PRCODENT = space(15)
  w_PRCODTAB = space(30)
  w_FLATAB = space(10)
  w_PR_CAMPO = space(30)
  w_PR_LUNGH = 0
  w_PRTIPCAM = space(1)
  w_PRVALCAR = space(254)
  w_PR__TIPO = space(1)
  w_PR_TABLE = space(30)
  w_PRVALDAT = space(8)
  w_PRVALNUM = 0
  w_CPROWORD = 0
  w_PR_FIELD = space(30)
  w_TIPCAM = space(1)
  w_LUNGH = 0
  w_DECIMA = 0
  w_TDECIMA = 0
  w_ELCODENT   = space(10)
  proc Save(i_oFrom)
    this.w_PRCODSTR = i_oFrom.w_PRCODSTR
    this.w_PRCODENT = i_oFrom.w_PRCODENT
    this.w_PRCODTAB = i_oFrom.w_PRCODTAB
    this.w_FLATAB = i_oFrom.w_FLATAB
    this.w_PR_CAMPO = i_oFrom.w_PR_CAMPO
    this.w_PR_LUNGH = i_oFrom.w_PR_LUNGH
    this.w_PRTIPCAM = i_oFrom.w_PRTIPCAM
    this.w_PRVALCAR = i_oFrom.w_PRVALCAR
    this.w_PR__TIPO = i_oFrom.w_PR__TIPO
    this.w_PR_TABLE = i_oFrom.w_PR_TABLE
    this.w_PRVALDAT = i_oFrom.w_PRVALDAT
    this.w_PRVALNUM = i_oFrom.w_PRVALNUM
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PR_FIELD = i_oFrom.w_PR_FIELD
    this.w_TIPCAM = i_oFrom.w_TIPCAM
    this.w_LUNGH = i_oFrom.w_LUNGH
    this.w_DECIMA = i_oFrom.w_DECIMA
    this.w_TDECIMA = i_oFrom.w_TDECIMA
    this.w_ELCODENT   = i_oFrom.w_ELCODENT  
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PRCODSTR = this.w_PRCODSTR
    i_oTo.w_PRCODENT = this.w_PRCODENT
    i_oTo.w_PRCODTAB = this.w_PRCODTAB
    i_oTo.w_FLATAB = this.w_FLATAB
    i_oTo.w_PR_CAMPO = this.w_PR_CAMPO
    i_oTo.w_PR_LUNGH = this.w_PR_LUNGH
    i_oTo.w_PRTIPCAM = this.w_PRTIPCAM
    i_oTo.w_PRVALCAR = this.w_PRVALCAR
    i_oTo.w_PR__TIPO = this.w_PR__TIPO
    i_oTo.w_PR_TABLE = this.w_PR_TABLE
    i_oTo.w_PRVALDAT = this.w_PRVALDAT
    i_oTo.w_PRVALNUM = this.w_PRVALNUM
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PR_FIELD = this.w_PR_FIELD
    i_oTo.w_TIPCAM = this.w_TIPCAM
    i_oTo.w_LUNGH = this.w_LUNGH
    i_oTo.w_DECIMA = this.w_DECIMA
    i_oTo.w_TDECIMA = this.w_TDECIMA
    i_oTo.w_ELCODENT   = this.w_ELCODENT  
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsva_mvp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 668
  Height = 293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-07"
  HelpContextID=221613463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VAPREDEF_IDX = 0
  ENT_DETT_IDX = 0
  XDC_FIELDS_IDX = 0
  FLATDETT_IDX = 0
  cFile = "VAPREDEF"
  cKeySelect = "PRCODSTR"
  cKeyWhere  = "PRCODSTR=this.w_PRCODSTR"
  cKeyDetail  = "PRCODSTR=this.w_PRCODSTR and PRCODTAB=this.w_PRCODTAB and PR_CAMPO=this.w_PR_CAMPO"
  cKeyWhereODBC = '"PRCODSTR="+cp_ToStrODBC(this.w_PRCODSTR)';

  cKeyDetailWhereODBC = '"PRCODSTR="+cp_ToStrODBC(this.w_PRCODSTR)';
      +'+" and PRCODTAB="+cp_ToStrODBC(this.w_PRCODTAB)';
      +'+" and PR_CAMPO="+cp_ToStrODBC(this.w_PR_CAMPO)';

  cKeyWhereODBCqualified = '"VAPREDEF.PRCODSTR="+cp_ToStrODBC(this.w_PRCODSTR)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VAPREDEF.CPROWORD '
  cPrg = "gsva_mvp"
  cComment = "Valori predefiniti"
  i_nRowNum = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODSTR = space(10)
  w_PRCODENT = space(15)
  w_PRCODTAB = space(30)
  w_FLATAB = space(10)
  w_PR_CAMPO = space(30)
  w_PR_LUNGH = 0
  w_PRTIPCAM = space(1)
  w_PRVALCAR = space(254)
  w_PR__TIPO = space(1)
  o_PR__TIPO = space(1)
  w_PR_TABLE = space(30)
  w_PRVALDAT = ctod('  /  /  ')
  w_PRVALNUM = 0
  w_CPROWORD = 0
  w_PR_FIELD = space(30)
  w_TIPCAM = space(1)
  w_LUNGH = 0
  w_DECIMA = 0
  w_TDECIMA = 0
  w_ELCODENT   = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mvpPag1","gsva_mvp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ENT_DETT'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='FLATDETT'
    this.cWorkTables[4]='VAPREDEF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VAPREDEF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VAPREDEF_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsva_mvp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VAPREDEF where PRCODSTR=KeySet.PRCODSTR
    *                            and PRCODTAB=KeySet.PRCODTAB
    *                            and PR_CAMPO=KeySet.PR_CAMPO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2],this.bLoadRecFilter,this.VAPREDEF_IDX,"gsva_mvp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VAPREDEF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VAPREDEF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VAPREDEF '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODSTR',this.w_PRCODSTR  )
      select * from (i_cTable) VAPREDEF where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TDECIMA = 0
        .w_PRCODSTR = NVL(PRCODSTR,space(10))
        .w_PRCODENT = NVL(PRCODENT,space(15))
        .w_FLATAB = This.oparentobject .w_STFLATAB
        .w_ELCODENT   = .w_PRCODENT  
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'VAPREDEF')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_TIPCAM = space(1)
          .w_LUNGH = 0
          .w_DECIMA = 0
          .w_PRCODTAB = NVL(PRCODTAB,space(30))
          * evitabile
          *.link_2_1('Load')
          .w_PR_CAMPO = NVL(PR_CAMPO,space(30))
          .link_2_2('Load')
          .w_PR_LUNGH = NVL(PR_LUNGH,0)
          .w_PRTIPCAM = NVL(PRTIPCAM,space(1))
          .w_PRVALCAR = NVL(PRVALCAR,space(254))
          .w_PR__TIPO = NVL(PR__TIPO,space(1))
          .w_PR_TABLE = NVL(PR_TABLE,space(30))
          * evitabile
          *.link_2_7('Load')
          .w_PRVALDAT = NVL(cp_ToDate(PRVALDAT),ctod("  /  /  "))
          .w_PRVALNUM = NVL(PRVALNUM,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PR_FIELD = NVL(PR_FIELD,space(30))
          .link_2_11('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PRCODTAB with .w_PRCODTAB
          replace PR_CAMPO with .w_PR_CAMPO
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLATAB = This.oparentobject .w_STFLATAB
        .w_ELCODENT   = .w_PRCODENT  
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PRCODSTR=space(10)
      .w_PRCODENT=space(15)
      .w_PRCODTAB=space(30)
      .w_FLATAB=space(10)
      .w_PR_CAMPO=space(30)
      .w_PR_LUNGH=0
      .w_PRTIPCAM=space(1)
      .w_PRVALCAR=space(254)
      .w_PR__TIPO=space(1)
      .w_PR_TABLE=space(30)
      .w_PRVALDAT=ctod("  /  /  ")
      .w_PRVALNUM=0
      .w_CPROWORD=10
      .w_PR_FIELD=space(30)
      .w_TIPCAM=space(1)
      .w_LUNGH=0
      .w_DECIMA=0
      .w_TDECIMA=0
      .w_ELCODENT  =space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_PRCODENT = This.oparentobject .w_STCODENT
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PRCODTAB))
         .link_2_1('Full')
        endif
        .w_FLATAB = This.oparentobject .w_STFLATAB
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PR_CAMPO))
         .link_2_2('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_PRTIPCAM = 'C'
        .DoRTCalc(8,8,.f.)
        .w_PR__TIPO = 'F'
        .w_PR_TABLE = Space(30)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PR_TABLE))
         .link_2_7('Full')
        endif
        .DoRTCalc(11,13,.f.)
        .w_PR_FIELD = Space(30)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_PR_FIELD))
         .link_2_11('Full')
        endif
        .DoRTCalc(15,18,.f.)
        .w_ELCODENT   = .w_PRCODENT  
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VAPREDEF')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPRVALCAR_2_5.enabled = i_bVal
      .Page1.oPag.oPR__TIPO_2_6.enabled = i_bVal
      .Page1.oPag.oPR_TABLE_2_7.enabled = i_bVal
      .Page1.oPag.oPRVALDAT_2_8.enabled = i_bVal
      .Page1.oPag.oPRVALNUM_2_9.enabled = i_bVal
      .Page1.oPag.oPR_FIELD_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'VAPREDEF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODSTR,"PRCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODENT,"PRCODENT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PRCODTAB C(30);
      ,t_PR_CAMPO C(30);
      ,t_PR_LUNGH N(3);
      ,t_PRTIPCAM N(3);
      ,t_PRVALCAR C(254);
      ,t_PR__TIPO N(3);
      ,t_PR_TABLE C(30);
      ,t_PRVALDAT D(8);
      ,t_PRVALNUM N(18,5);
      ,t_CPROWORD N(5);
      ,t_PR_FIELD C(30);
      ,PRCODTAB C(30);
      ,PR_CAMPO C(30);
      ,t_TIPCAM C(1);
      ,t_LUNGH N(5);
      ,t_DECIMA N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mvpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODTAB_2_1.controlsource=this.cTrsName+'.t_PRCODTAB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPR_CAMPO_2_2.controlsource=this.cTrsName+'.t_PR_CAMPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPR_LUNGH_2_3.controlsource=this.cTrsName+'.t_PR_LUNGH'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.controlsource=this.cTrsName+'.t_PRTIPCAM'
    this.oPgFRm.Page1.oPag.oPRVALCAR_2_5.controlsource=this.cTrsName+'.t_PRVALCAR'
    this.oPgFRm.Page1.oPag.oPR__TIPO_2_6.controlsource=this.cTrsName+'.t_PR__TIPO'
    this.oPgFRm.Page1.oPag.oPR_TABLE_2_7.controlsource=this.cTrsName+'.t_PR_TABLE'
    this.oPgFRm.Page1.oPag.oPRVALDAT_2_8.controlsource=this.cTrsName+'.t_PRVALDAT'
    this.oPgFRm.Page1.oPag.oPRVALNUM_2_9.controlsource=this.cTrsName+'.t_PRVALNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oPR_FIELD_2_11.controlsource=this.cTrsName+'.t_PR_FIELD'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(285)
    this.AddVLine(513)
    this.AddVLine(551)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODTAB_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2])
      *
      * insert into VAPREDEF
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VAPREDEF')
        i_extval=cp_InsertValODBCExtFlds(this,'VAPREDEF')
        i_cFldBody=" "+;
                  "(PRCODSTR,PRCODENT,PRCODTAB,PR_CAMPO,PR_LUNGH"+;
                  ",PRTIPCAM,PRVALCAR,PR__TIPO,PR_TABLE,PRVALDAT"+;
                  ",PRVALNUM,CPROWORD,PR_FIELD,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PRCODSTR)+","+cp_ToStrODBC(this.w_PRCODENT)+","+cp_ToStrODBCNull(this.w_PRCODTAB)+","+cp_ToStrODBCNull(this.w_PR_CAMPO)+","+cp_ToStrODBC(this.w_PR_LUNGH)+;
             ","+cp_ToStrODBC(this.w_PRTIPCAM)+","+cp_ToStrODBC(this.w_PRVALCAR)+","+cp_ToStrODBC(this.w_PR__TIPO)+","+cp_ToStrODBCNull(this.w_PR_TABLE)+","+cp_ToStrODBC(this.w_PRVALDAT)+;
             ","+cp_ToStrODBC(this.w_PRVALNUM)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_PR_FIELD)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VAPREDEF')
        i_extval=cp_InsertValVFPExtFlds(this,'VAPREDEF')
        cp_CheckDeletedKey(i_cTable,0,'PRCODSTR',this.w_PRCODSTR,'PRCODTAB',this.w_PRCODTAB,'PR_CAMPO',this.w_PR_CAMPO)
        INSERT INTO (i_cTable) (;
                   PRCODSTR;
                  ,PRCODENT;
                  ,PRCODTAB;
                  ,PR_CAMPO;
                  ,PR_LUNGH;
                  ,PRTIPCAM;
                  ,PRVALCAR;
                  ,PR__TIPO;
                  ,PR_TABLE;
                  ,PRVALDAT;
                  ,PRVALNUM;
                  ,CPROWORD;
                  ,PR_FIELD;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PRCODSTR;
                  ,this.w_PRCODENT;
                  ,this.w_PRCODTAB;
                  ,this.w_PR_CAMPO;
                  ,this.w_PR_LUNGH;
                  ,this.w_PRTIPCAM;
                  ,this.w_PRVALCAR;
                  ,this.w_PR__TIPO;
                  ,this.w_PR_TABLE;
                  ,this.w_PRVALDAT;
                  ,this.w_PRVALNUM;
                  ,this.w_CPROWORD;
                  ,this.w_PR_FIELD;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PRCODTAB)) AND not(Empty(t_PR_CAMPO))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VAPREDEF')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " PRCODENT="+cp_ToStrODBC(this.w_PRCODENT)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PRCODTAB="+cp_ToStrODBC(&i_TN.->PRCODTAB)+;
                 " and PR_CAMPO="+cp_ToStrODBC(&i_TN.->PR_CAMPO)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VAPREDEF')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  PRCODENT=this.w_PRCODENT;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PRCODTAB=&i_TN.->PRCODTAB;
                      and PR_CAMPO=&i_TN.->PR_CAMPO;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PRCODTAB)) AND not(Empty(t_PR_CAMPO))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PRCODTAB="+cp_ToStrODBC(&i_TN.->PRCODTAB)+;
                            " and PR_CAMPO="+cp_ToStrODBC(&i_TN.->PR_CAMPO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PRCODTAB=&i_TN.->PRCODTAB;
                            and PR_CAMPO=&i_TN.->PR_CAMPO;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PRCODTAB with this.w_PRCODTAB
              replace PR_CAMPO with this.w_PR_CAMPO
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VAPREDEF
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VAPREDEF')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PRCODENT="+cp_ToStrODBC(this.w_PRCODENT)+;
                     ",PR_LUNGH="+cp_ToStrODBC(this.w_PR_LUNGH)+;
                     ",PRTIPCAM="+cp_ToStrODBC(this.w_PRTIPCAM)+;
                     ",PRVALCAR="+cp_ToStrODBC(this.w_PRVALCAR)+;
                     ",PR__TIPO="+cp_ToStrODBC(this.w_PR__TIPO)+;
                     ",PR_TABLE="+cp_ToStrODBCNull(this.w_PR_TABLE)+;
                     ",PRVALDAT="+cp_ToStrODBC(this.w_PRVALDAT)+;
                     ",PRVALNUM="+cp_ToStrODBC(this.w_PRVALNUM)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PR_FIELD="+cp_ToStrODBCNull(this.w_PR_FIELD)+;
                     ",PRCODTAB="+cp_ToStrODBC(this.w_PRCODTAB)+;
                     ",PR_CAMPO="+cp_ToStrODBC(this.w_PR_CAMPO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PRCODTAB="+cp_ToStrODBC(PRCODTAB)+;
                             " and PR_CAMPO="+cp_ToStrODBC(PR_CAMPO)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VAPREDEF')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PRCODENT=this.w_PRCODENT;
                     ,PR_LUNGH=this.w_PR_LUNGH;
                     ,PRTIPCAM=this.w_PRTIPCAM;
                     ,PRVALCAR=this.w_PRVALCAR;
                     ,PR__TIPO=this.w_PR__TIPO;
                     ,PR_TABLE=this.w_PR_TABLE;
                     ,PRVALDAT=this.w_PRVALDAT;
                     ,PRVALNUM=this.w_PRVALNUM;
                     ,CPROWORD=this.w_CPROWORD;
                     ,PR_FIELD=this.w_PR_FIELD;
                     ,PRCODTAB=this.w_PRCODTAB;
                     ,PR_CAMPO=this.w_PR_CAMPO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PRCODTAB=&i_TN.->PRCODTAB;
                                      and PR_CAMPO=&i_TN.->PR_CAMPO;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PRCODTAB)) AND not(Empty(t_PR_CAMPO))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VAPREDEF
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PRCODTAB="+cp_ToStrODBC(&i_TN.->PRCODTAB)+;
                            " and PR_CAMPO="+cp_ToStrODBC(&i_TN.->PR_CAMPO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PRCODTAB=&i_TN.->PRCODTAB;
                              and PR_CAMPO=&i_TN.->PR_CAMPO;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PRCODTAB)) AND not(Empty(t_PR_CAMPO))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAPREDEF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAPREDEF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_PRCODENT = This.oparentobject .w_STCODENT
        .DoRTCalc(3,3,.t.)
          .w_FLATAB = This.oparentobject .w_STFLATAB
        .DoRTCalc(5,9,.t.)
        if .o_PR__TIPO<>.w_PR__TIPO
          .w_PR_TABLE = Space(30)
          .link_2_7('Full')
        endif
        .DoRTCalc(11,13,.t.)
        if .o_PR__TIPO<>.w_PR__TIPO
          .w_PR_FIELD = Space(30)
          .link_2_11('Full')
        endif
        .DoRTCalc(15,18,.t.)
          .w_ELCODENT   = .w_PRCODENT  
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPCAM with this.w_TIPCAM
      replace t_LUNGH with this.w_LUNGH
      replace t_DECIMA with this.w_DECIMA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPR_CAMPO_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPR_CAMPO_2_2.mCond()
    this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oPRVALCAR_2_5.visible=!this.oPgFrm.Page1.oPag.oPRVALCAR_2_5.mHide()
    this.oPgFrm.Page1.oPag.oPR_TABLE_2_7.visible=!this.oPgFrm.Page1.oPag.oPR_TABLE_2_7.mHide()
    this.oPgFrm.Page1.oPag.oPRVALDAT_2_8.visible=!this.oPgFrm.Page1.oPag.oPRVALDAT_2_8.mHide()
    this.oPgFrm.Page1.oPag.oPRVALNUM_2_9.visible=!this.oPgFrm.Page1.oPag.oPRVALNUM_2_9.mHide()
    this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.visible=!this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODTAB
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ENT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EN_TABLE like "+cp_ToStrODBC(trim(this.w_PRCODTAB)+"%");
                   +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);

          i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE,EN_TABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',this.w_PRCODENT;
                     ,'EN_TABLE',trim(this.w_PRCODTAB))
          select ENCODICE,EN_TABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE,EN_TABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODTAB)==trim(_Link_.EN_TABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODTAB) and !this.bDontReportError
            deferred_cp_zoom('ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(oSource.parent,'oPRCODTAB_2_1'),i_cWhere,'',"Elenco tabelle entita'",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRCODENT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1);
                       ,'EN_TABLE',oSource.xKey(2))
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(this.w_PRCODTAB);
                   +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_PRCODENT;
                       ,'EN_TABLE',this.w_PRCODTAB)
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODTAB = NVL(_Link_.EN_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODTAB = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.EN_TABLE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PR_CAMPO
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PR_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_PR_CAMPO)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_PRCODTAB);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_PRCODTAB;
                     ,'FLNAME',trim(this.w_PR_CAMPO))
          select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PR_CAMPO)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PR_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oPR_CAMPO_2_2'),i_cWhere,'',"Campi tabella",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRCODTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_PRCODTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PR_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_PR_CAMPO);
                   +" and TBNAME="+cp_ToStrODBC(this.w_PRCODTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_PRCODTAB;
                       ,'FLNAME',this.w_PR_CAMPO)
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PR_CAMPO = NVL(_Link_.FLNAME,space(30))
      this.w_PRTIPCAM = NVL(_Link_.FLTYPE,space(1))
      this.w_PR_LUNGH = NVL(_Link_.FLLENGHT,0)
      this.w_TDECIMA = NVL(_Link_.FLDECIMA,0)
    else
      if i_cCtrl<>'Load'
        this.w_PR_CAMPO = space(30)
      endif
      this.w_PRTIPCAM = space(1)
      this.w_PR_LUNGH = 0
      this.w_TDECIMA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PR_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PR_TABLE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PR_TABLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ENT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EN_TABLE like "+cp_ToStrODBC(trim(this.w_PR_TABLE)+"%");
                   +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);

          i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE,EN_TABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',this.w_PRCODENT;
                     ,'EN_TABLE',trim(this.w_PR_TABLE))
          select ENCODICE,EN_TABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE,EN_TABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PR_TABLE)==trim(_Link_.EN_TABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PR_TABLE) and !this.bDontReportError
            deferred_cp_zoom('ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(oSource.parent,'oPR_TABLE_2_7'),i_cWhere,'',"Dettaglio entit�",'GSVA_AED.ENT_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRCODENT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1);
                       ,'EN_TABLE',oSource.xKey(2))
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PR_TABLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(this.w_PR_TABLE);
                   +" and ENCODICE="+cp_ToStrODBC(this.w_PRCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_PRCODENT;
                       ,'EN_TABLE',this.w_PR_TABLE)
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PR_TABLE = NVL(_Link_.EN_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PR_TABLE = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.EN_TABLE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PR_TABLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PR_FIELD
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLATDETT_IDX,3]
    i_lTable = "FLATDETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2], .t., this.FLATDETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PR_FIELD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FLATDETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FTFLDNAM like "+cp_ToStrODBC(trim(this.w_PR_FIELD)+"%");
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_PR_TABLE);

          i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FTCODICE,FTTABNAM,FTFLDNAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FTCODICE',this.w_FLATAB;
                     ,'FTTABNAM',this.w_PR_TABLE;
                     ,'FTFLDNAM',trim(this.w_PR_FIELD))
          select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FTCODICE,FTTABNAM,FTFLDNAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PR_FIELD)==trim(_Link_.FTFLDNAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PR_FIELD) and !this.bDontReportError
            deferred_cp_zoom('FLATDETT','*','FTCODICE,FTTABNAM,FTFLDNAM',cp_AbsName(oSource.parent,'oPR_FIELD_2_11'),i_cWhere,'',"Dettaglio tabelle piatte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLATAB<>oSource.xKey(1);
           .or. this.w_PR_TABLE<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, campo non inserito oppure incongruente per tipo.")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(oSource.xKey(3));
                     +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                     +" and FTTABNAM="+cp_ToStrODBC(this.w_PR_TABLE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',oSource.xKey(1);
                       ,'FTTABNAM',oSource.xKey(2);
                       ,'FTFLDNAM',oSource.xKey(3))
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PR_FIELD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                   +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(this.w_PR_FIELD);
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_PR_TABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',this.w_FLATAB;
                       ,'FTTABNAM',this.w_PR_TABLE;
                       ,'FTFLDNAM',this.w_PR_FIELD)
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PR_FIELD = NVL(_Link_.FTFLDNAM,space(30))
      this.w_TIPCAM = NVL(_Link_.FTFLDTYP,space(1))
      this.w_LUNGH = NVL(_Link_.FTFLDDIM,0)
      this.w_DECIMA = NVL(_Link_.FTFLDDEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_PR_FIELD = space(30)
      endif
      this.w_TIPCAM = space(1)
      this.w_LUNGH = 0
      this.w_DECIMA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPCAM=.w_PRTIPCAM AND Not Empty(.w_PR_FIELD) ) or (Empty(.w_PR_FIELD) and  Empty(.w_PR_TABLE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, campo non inserito oppure incongruente per tipo.")
        endif
        this.w_PR_FIELD = space(30)
        this.w_TIPCAM = space(1)
        this.w_LUNGH = 0
        this.w_DECIMA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])+'\'+cp_ToStr(_Link_.FTCODICE,1)+'\'+cp_ToStr(_Link_.FTTABNAM,1)+'\'+cp_ToStr(_Link_.FTFLDNAM,1)
      cp_ShowWarn(i_cKey,this.FLATDETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PR_FIELD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPRVALCAR_2_5.value==this.w_PRVALCAR)
      this.oPgFrm.Page1.oPag.oPRVALCAR_2_5.value=this.w_PRVALCAR
      replace t_PRVALCAR with this.oPgFrm.Page1.oPag.oPRVALCAR_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPR__TIPO_2_6.RadioValue()==this.w_PR__TIPO)
      this.oPgFrm.Page1.oPag.oPR__TIPO_2_6.SetRadio()
      replace t_PR__TIPO with this.oPgFrm.Page1.oPag.oPR__TIPO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPR_TABLE_2_7.value==this.w_PR_TABLE)
      this.oPgFrm.Page1.oPag.oPR_TABLE_2_7.value=this.w_PR_TABLE
      replace t_PR_TABLE with this.oPgFrm.Page1.oPag.oPR_TABLE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRVALDAT_2_8.value==this.w_PRVALDAT)
      this.oPgFrm.Page1.oPag.oPRVALDAT_2_8.value=this.w_PRVALDAT
      replace t_PRVALDAT with this.oPgFrm.Page1.oPag.oPRVALDAT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRVALNUM_2_9.value==this.w_PRVALNUM)
      this.oPgFrm.Page1.oPag.oPRVALNUM_2_9.value=this.w_PRVALNUM
      replace t_PRVALNUM with this.oPgFrm.Page1.oPag.oPRVALNUM_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.value==this.w_PR_FIELD)
      this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.value=this.w_PR_FIELD
      replace t_PR_FIELD with this.oPgFrm.Page1.oPag.oPR_FIELD_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODTAB_2_1.value==this.w_PRCODTAB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODTAB_2_1.value=this.w_PRCODTAB
      replace t_PRCODTAB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODTAB_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_CAMPO_2_2.value==this.w_PR_CAMPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_CAMPO_2_2.value=this.w_PR_CAMPO
      replace t_PR_CAMPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_CAMPO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_LUNGH_2_3.value==this.w_PR_LUNGH)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_LUNGH_2_3.value=this.w_PR_LUNGH
      replace t_PR_LUNGH with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR_LUNGH_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.RadioValue()==this.w_PRTIPCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.SetRadio()
      replace t_PRTIPCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'VAPREDEF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not((Not Empty(.w_PR_FIELD) and  Not Empty(.w_PR_TABLE) and .w_PR__TIPO='V') or   .w_PR__TIPO<>'V')
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Attenzione, riga di tipo variabile indicare campo per valorizzazione  ")
        case   not((.w_TIPCAM=.w_PRTIPCAM AND Not Empty(.w_PR_FIELD) ) or (Empty(.w_PR_FIELD) and  Empty(.w_PR_TABLE))) and (Not Empty(.w_PR_TABLE)) and not(empty(.w_PR_FIELD)) and (not(Empty(.w_PRCODTAB)) AND not(Empty(.w_PR_CAMPO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oPR_FIELD_2_11
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, campo non inserito oppure incongruente per tipo.")
      endcase
      if not(Empty(.w_PRCODTAB)) AND not(Empty(.w_PR_CAMPO))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PR__TIPO = this.w_PR__TIPO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PRCODTAB)) AND not(Empty(t_PR_CAMPO)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PRCODTAB=space(30)
      .w_PR_CAMPO=space(30)
      .w_PR_LUNGH=0
      .w_PRTIPCAM=space(1)
      .w_PRVALCAR=space(254)
      .w_PR__TIPO=space(1)
      .w_PR_TABLE=space(30)
      .w_PRVALDAT=ctod("  /  /  ")
      .w_PRVALNUM=0
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_PR_FIELD=space(30)
      .w_TIPCAM=space(1)
      .w_LUNGH=0
      .w_DECIMA=0
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_PRCODTAB))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,5,.f.)
      if not(empty(.w_PR_CAMPO))
        .link_2_2('Full')
      endif
      .DoRTCalc(6,6,.f.)
        .w_PRTIPCAM = 'C'
      .DoRTCalc(8,8,.f.)
        .w_PR__TIPO = 'F'
        .w_PR_TABLE = Space(30)
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_PR_TABLE))
        .link_2_7('Full')
      endif
      .DoRTCalc(11,13,.f.)
        .w_PR_FIELD = Space(30)
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_PR_FIELD))
        .link_2_11('Full')
      endif
    endwith
    this.DoRTCalc(15,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PRCODTAB = t_PRCODTAB
    this.w_PR_CAMPO = t_PR_CAMPO
    this.w_PR_LUNGH = t_PR_LUNGH
    this.w_PRTIPCAM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.RadioValue(.t.)
    this.w_PRVALCAR = t_PRVALCAR
    this.w_PR__TIPO = this.oPgFrm.Page1.oPag.oPR__TIPO_2_6.RadioValue(.t.)
    this.w_PR_TABLE = t_PR_TABLE
    this.w_PRVALDAT = t_PRVALDAT
    this.w_PRVALNUM = t_PRVALNUM
    this.w_CPROWORD = t_CPROWORD
    this.w_PR_FIELD = t_PR_FIELD
    this.w_TIPCAM = t_TIPCAM
    this.w_LUNGH = t_LUNGH
    this.w_DECIMA = t_DECIMA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PRCODTAB with this.w_PRCODTAB
    replace t_PR_CAMPO with this.w_PR_CAMPO
    replace t_PR_LUNGH with this.w_PR_LUNGH
    replace t_PRTIPCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCAM_2_4.ToRadio()
    replace t_PRVALCAR with this.w_PRVALCAR
    replace t_PR__TIPO with this.oPgFrm.Page1.oPag.oPR__TIPO_2_6.ToRadio()
    replace t_PR_TABLE with this.w_PR_TABLE
    replace t_PRVALDAT with this.w_PRVALDAT
    replace t_PRVALNUM with this.w_PRVALNUM
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PR_FIELD with this.w_PR_FIELD
    replace t_TIPCAM with this.w_TIPCAM
    replace t_LUNGH with this.w_LUNGH
    replace t_DECIMA with this.w_DECIMA
    if i_srv='A'
      replace PRCODTAB with this.w_PRCODTAB
      replace PR_CAMPO with this.w_PR_CAMPO
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsva_mvpPag1 as StdContainer
  Width  = 664
  height = 293
  stdWidth  = 664
  stdheight = 293
  resizeYpos=203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=0, width=650,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="PRCODTAB",Label2="Codice Tabella",Field3="PR_CAMPO",Label3="Codice campo",Field4="PR_LUNGH",Label4="Lung.",Field5="PRTIPCAM",Label5="Tipo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 833402

  add object oStr_1_4 as StdString with uid="LABQYEATJM",Visible=.t., Left=303, Top=269,;
    Alignment=1, Width=144, Height=18,;
    Caption="Valore predefinito:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_PR__TIPO<>'F')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="WYCXKWMYQS",Visible=.t., Left=4, Top=269,;
    Alignment=1, Width=37, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="JCKCFFMKEN",Visible=.t., Left=144, Top=269,;
    Alignment=1, Width=51, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_PR__TIPO<>'V')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="IVLRZGLWKO",Visible=.t., Left=396, Top=269,;
    Alignment=1, Width=51, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_PR__TIPO<>'V')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=19,;
    width=647+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.5000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=20,width=646+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.5000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='ENT_DETT|XDC_FIELDS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPRVALCAR_2_5.Refresh()
      this.Parent.oPR__TIPO_2_6.Refresh()
      this.Parent.oPR_TABLE_2_7.Refresh()
      this.Parent.oPRVALDAT_2_8.Refresh()
      this.Parent.oPRVALNUM_2_9.Refresh()
      this.Parent.oPR_FIELD_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='ENT_DETT'
        oDropInto=this.oBodyCol.oRow.oPRCODTAB_2_1
      case cFile='XDC_FIELDS'
        oDropInto=this.oBodyCol.oRow.oPR_CAMPO_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPRVALCAR_2_5 as StdTrsField with uid="FCAYHDEVYL",rtseq=8,rtrep=.t.,;
    cFormVar="w_PRVALCAR",value=space(254),;
    HelpContextID = 180599480,;
    cTotal="", bFixedPos=.t., cQueryName = "PRVALCAR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=458, Top=267, InputMask=replicate('X',254)

  func oPRVALCAR_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_PRTIPCAM $ 'M-C'  or .w_PR__TIPO<>'F')
    endwith
    endif
  endfunc

  add object oPR__TIPO_2_6 as StdTrsCombo with uid="LWNVDBVNUK",rtrep=.t.,;
    cFormVar="w_PR__TIPO", RowSource=""+"Fisso,"+"Variabile,"+"Var. globale" , ;
    HelpContextID = 69544635,;
    Height=25, Width=97, Left=43, Top=267,;
    cTotal="", cQueryName = "PR__TIPO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPR__TIPO_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PR__TIPO,&i_cF..t_PR__TIPO),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'V',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oPR__TIPO_2_6.GetRadio()
    this.Parent.oContained.w_PR__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oPR__TIPO_2_6.ToRadio()
    this.Parent.oContained.w_PR__TIPO=trim(this.Parent.oContained.w_PR__TIPO)
    return(;
      iif(this.Parent.oContained.w_PR__TIPO=='F',1,;
      iif(this.Parent.oContained.w_PR__TIPO=='V',2,;
      iif(this.Parent.oContained.w_PR__TIPO=='G',3,;
      0))))
  endfunc

  func oPR__TIPO_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPR_TABLE_2_7 as StdTrsField with uid="NYAEVZNPTH",rtseq=10,rtrep=.t.,;
    cFormVar="w_PR_TABLE",value=space(30),;
    HelpContextID = 207628997,;
    cTotal="", bFixedPos=.t., cQueryName = "PR_TABLE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=200, Top=267, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="ENT_DETT", oKey_1_1="ENCODICE", oKey_1_2="this.w_PRCODENT", oKey_2_1="EN_TABLE", oKey_2_2="this.w_PR_TABLE"

  func oPR_TABLE_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PR__TIPO<>'V')
    endwith
    endif
  endfunc

  func oPR_TABLE_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
      if .not. empty(.w_PR_FIELD)
        bRes2=.link_2_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPR_TABLE_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPR_TABLE_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ENT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStrODBC(this.Parent.oContained.w_PRCODENT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStr(this.Parent.oContained.w_PRCODENT)
    endif
    do cp_zoom with 'ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(this.parent,'oPR_TABLE_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dettaglio entit�",'GSVA_AED.ENT_DETT_VZM',this.parent.oContained
  endproc

  add object oPRVALDAT_2_8 as StdTrsField with uid="SXEDMEXXUH",rtseq=11,rtrep=.t.,;
    cFormVar="w_PRVALDAT",value=ctod("  /  /  "),;
    HelpContextID = 163822262,;
    cTotal="", bFixedPos=.t., cQueryName = "PRVALDAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=458, Top=267

  func oPRVALDAT_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPCAM <> 'D' or .w_PR__TIPO<>'F')
    endwith
    endif
  endfunc

  add object oPRVALNUM_2_9 as StdTrsField with uid="CPCNOEHAWD",rtseq=12,rtrep=.t.,;
    cFormVar="w_PRVALNUM",value=0,;
    HelpContextID = 3949891,;
    cTotal="", bFixedPos=.t., cQueryName = "PRVALNUM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=458, Top=267, cSayPict=["999999999999.99999"], cGetPict=["999999999999.99999"]

  func oPRVALNUM_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRTIPCAM <> 'N' or .w_PR__TIPO<>'F')
    endwith
    endif
  endfunc

  add object oPR_FIELD_2_11 as StdTrsField with uid="UPCLWYPGLL",rtseq=14,rtrep=.t.,;
    cFormVar="w_PR_FIELD",value=space(30),;
    HelpContextID = 149826246,;
    cTotal="", bFixedPos=.t., cQueryName = "PR_FIELD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, campo non inserito oppure incongruente per tipo.",;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=458, Top=267, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="FLATDETT", oKey_1_1="FTCODICE", oKey_1_2="this.w_FLATAB", oKey_2_1="FTTABNAM", oKey_2_2="this.w_PR_TABLE", oKey_3_1="FTFLDNAM", oKey_3_2="this.w_PR_FIELD"

  func oPR_FIELD_2_11.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_PR_TABLE))
    endwith
  endfunc

  func oPR_FIELD_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PR__TIPO<>'V')
    endwith
    endif
  endfunc

  func oPR_FIELD_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPR_FIELD_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPR_FIELD_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.FLATDETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FTCODICE="+cp_ToStrODBC(this.Parent.oContained.w_FLATAB)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FTTABNAM="+cp_ToStrODBC(this.Parent.oContained.w_PR_TABLE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FTCODICE="+cp_ToStr(this.Parent.oContained.w_FLATAB)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FTTABNAM="+cp_ToStr(this.Parent.oContained.w_PR_TABLE)
    endif
    do cp_zoom with 'FLATDETT','*','FTCODICE,FTTABNAM,FTFLDNAM',cp_AbsName(this.parent,'oPR_FIELD_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Dettaglio tabelle piatte",'',this.parent.oContained
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsva_mvpBodyRow as CPBodyRowCnt
  Width=637
  Height=int(fontmetric(1,"Arial",9,"")*1*1.5000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPRCODTAB_2_1 as StdTrsField with uid="LHNFWMLSLP",rtseq=3,rtrep=.t.,;
    cFormVar="w_PRCODTAB",value=space(30),isprimarykey=.t.,;
    HelpContextID = 171371208,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=48, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="ENT_DETT", oKey_1_1="ENCODICE", oKey_1_2="this.w_PRCODENT", oKey_2_1="EN_TABLE", oKey_2_2="this.w_PRCODTAB"

  func oPRCODTAB_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_PR_CAMPO)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPRCODTAB_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRCODTAB_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPRCODTAB_2_1.readonly and this.parent.oPRCODTAB_2_1.isprimarykey)
    if i_TableProp[this.parent.oContained.ENT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStrODBC(this.Parent.oContained.w_PRCODENT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStr(this.Parent.oContained.w_PRCODENT)
    endif
    do cp_zoom with 'ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(this.parent,'oPRCODTAB_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco tabelle entita'",'',this.parent.oContained
   endif
  endproc

  add object oPR_CAMPO_2_2 as StdTrsField with uid="BNPVLITLAU",rtseq=5,rtrep=.t.,;
    cFormVar="w_PR_CAMPO",value=space(30),isprimarykey=.t.,;
    HelpContextID = 24193723,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=276, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_PRCODTAB", oKey_2_1="FLNAME", oKey_2_2="this.w_PR_CAMPO"

  func oPR_CAMPO_2_2.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_PRCODTAB))
    endwith
  endfunc

  func oPR_CAMPO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPR_CAMPO_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPR_CAMPO_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPR_CAMPO_2_2.readonly and this.parent.oPR_CAMPO_2_2.isprimarykey)
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_PRCODTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_PRCODTAB)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oPR_CAMPO_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campi tabella",'',this.parent.oContained
   endif
  endproc

  add object oPR_LUNGH_2_3 as StdTrsField with uid="DOQNVNQJHX",rtseq=6,rtrep=.t.,;
    cFormVar="w_PR_LUNGH",value=0,enabled=.f.,;
    HelpContextID = 14144830,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=503, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oPRTIPCAM_2_4 as StdTrsCombo with uid="ENQRPIWOKQ",rtrep=.t.,;
    cFormVar="w_PRTIPCAM", RowSource=""+"Data,"+"Numerico,"+"Memo,"+"Carattere" , enabled=.f.,;
    HelpContextID = 175889085,;
    Height=21, Width=90, Left=542, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPRTIPCAM_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PRTIPCAM,&i_cF..t_PRTIPCAM),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'N',;
    iif(xVal =3,'M',;
    iif(xVal =4,'C',;
    space(1))))))
  endfunc
  func oPRTIPCAM_2_4.GetRadio()
    this.Parent.oContained.w_PRTIPCAM = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPCAM_2_4.ToRadio()
    this.Parent.oContained.w_PRTIPCAM=trim(this.Parent.oContained.w_PRTIPCAM)
    return(;
      iif(this.Parent.oContained.w_PRTIPCAM=='D',1,;
      iif(this.Parent.oContained.w_PRTIPCAM=='N',2,;
      iif(this.Parent.oContained.w_PRTIPCAM=='M',3,;
      iif(this.Parent.oContained.w_PRTIPCAM=='C',4,;
      0)))))
  endfunc

  func oPRTIPCAM_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCPROWORD_2_10 as StdTrsField with uid="YXBMBOCPUU",rtseq=13,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 235273622,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPRCODTAB_2_1.When()
    return(.t.)
  proc oPRCODTAB_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPRCODTAB_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_mvp','VAPREDEF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODSTR=VAPREDEF.PRCODSTR";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
