* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva1bim                                                        *
*              IMPORT FILE EDI                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_557]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-03                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFILE,pCODSTR,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva1bim",oParentObject,m.pFILE,m.pCODSTR,m.pTIPOPE)
return(i_retval)

define class tgsva1bim as StdBatch
  * --- Local variables
  pFILE = space(100)
  w_FIELD = space(10)
  pCODSTR = space(10)
  pTIPOPE = space(5)
  w_OBJROOT = .NULL.
  w_OBJROOT = .NULL.
  w_HANDLEROOT = 0
  w_ROOTLEN = 0
  w_I = 0
  w_MVSERIAL = space(10)
  w_ErrCode = 0
  w_Risultato = space(254)
  w_TIPFIL = space(1)
  w_ELEMENTO = space(20)
  w_LIMITE = 0
  w_CODSTR = space(10)
  w_CODNOR = space(10)
  w_oERRORLOG = .NULL.
  w_BATPRE = space(50)
  w_BATPOS = space(50)
  w_BATCHK = space(50)
  w_FLATAB = space(15)
  w_PADRE = .NULL.
  w_SERIAL = space(10)
  pDOCINFO = .NULL.
  PAR = .NULL.
  w_RET = .NULL.
  w_contrate = 0
  w_OKDOC = .f.
  w_LOG = space(0)
  w_TEST = .f.
  w_NUMREC = 0
  w_SERGDOC = space(10)
  w_GDKEYRIF = space(10)
  w_OLDDES = space(5)
  w_OLDSER = space(10)
  w_SERIAL = space(10)
  w_ROWNUM = ctod("  /  /  ")
  w_QTAMOV = 0
  w_MVQTAMOV = 0
  w_GIORNI = 0
  w_DFDTPFQU = space(1)
  w_DFSTADAT = ctod("  /  /  ")
  w_DFENDDAT = ctod("  /  /  ")
  w_DFTIMUNI = space(12)
  w_INIANNO = ctod("  /  /  ")
  w_SETTIM = 0
  w_DATEVA = ctod("  /  /  ")
  w_LGIOLIM = 0
  w_ANNO = space(4)
  w_MVCODICE = space(20)
  w_MVDATEVA = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_FINEMESE = ctod("  /  /  ")
  w_LTIPCAM = space(1)
  w_DECIMAL = 0
  w_LENGHT = 0
  w_CODTAB = space(30)
  w_LCAMPO = space(30)
  w_LCODTAB = space(10)
  w_PHNAME = space(30)
  w_OKBGE = .f.
  w_OLDSERLOG = space(10)
  w_LGTIPRIG = space(1)
  w_LGMESSAG = space(254)
  w_LGSERORI = space(10)
  w_LGROWORI = 0
  w_EXPRCOL = space(100)
  w_COD_TMP = space(10)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_TRFLALLT = space(1)
  w_CODTRA = space(10)
  w_CODRAG = space(10)
  w_CODENT = space(10)
  w_BATCH = space(30)
  w_TIPTRA = space(1)
  w_TMP_VAL = space(30)
  w_FILTER = space(30)
  w_EDI_TAB = space(10)
  w_CODTAB = space(30)
  w_TMP_GEN = space(30)
  w_TMP_EDI = space(30)
  w_OQUERY = space(100)
  w_LOOP = 0
  w_STATUS = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ENT_DETT_idx=0
  ENT_MAST_idx=0
  FLATDETT_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  LOG_GEND_idx=0
  MAGAZZIN_idx=0
  MOVIMATR_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  TIP_DOCU_idx=0
  TMP_ELEMEN_idx=0
  TMPDETTI_idx=0
  TMPDOCU_idx=0
  TMPLIST_idx=0
  TMPMASTI_idx=0
  TMPMATRI_idx=0
  TMPVFABBLOTUL_idx=0
  TMPVFDOCDETT_idx=0
  TMPVFDOCMAST_idx=0
  TMPVFDOCRATE_idx=0
  TMPVFMOVILOTT_idx=0
  TMPVFMOVIMATR_idx=0
  TRS_DETT_idx=0
  TRS_MAST_idx=0
  VADETTFO_idx=0
  VAELEMEN_idx=0
  VALUTE_idx=0
  VAPREDEF_idx=0
  VAPREDEF_idx=0
  VASTRUTT_idx=0
  VATRASCO_idx=0
  VOCIIVA_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- File di import
    * --- codice struttura
    * --- I: esegue valorizzazione tabelle clone da file  import
    *     C: esegue commit dati nelle tabelle ufficiali
    *     A: esegue analisi\verifica dati simulando una commit
    *     E: esegue controlli esistenza tabelle
    *     V: valorizza con valore di default dello zoom  campo aggiornamento
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    DIMENSION ARREIT(1)
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STTIPFIL,STCODNOR,STCODENT,STLUNTAG,STLUNSEG,STSEPARA,STFLATAB"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.pCODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STTIPFIL,STCODNOR,STCODENT,STLUNTAG,STLUNSEG,STSEPARA,STFLATAB;
        from (i_cTable) where;
            STCODICE = this.pCODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_CODNOR = NVL(cp_ToDate(_read_.STCODNOR),cp_NullValue(_read_.STCODNOR))
      this.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      w_LUNTAG = NVL(cp_ToDate(_read_.STLUNTAG),cp_NullValue(_read_.STLUNTAG))
      w_LUNSEG = NVL(cp_ToDate(_read_.STLUNSEG),cp_NullValue(_read_.STLUNSEG))
      w_SEPARA = NVL(cp_ToDate(_read_.STSEPARA),cp_NullValue(_read_.STSEPARA))
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ENT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ENT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_MAST_idx,2],.t.,this.ENT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ENBATPRE,ENBATPOS,ENBATCHK"+;
        " from "+i_cTable+" ENT_MAST where ";
            +"ENCODICE = "+cp_ToStrODBC(this.w_CODENT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ENBATPRE,ENBATPOS,ENBATCHK;
        from (i_cTable) where;
            ENCODICE = this.w_CODENT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BATPRE = NVL(cp_ToDate(_read_.ENBATPRE),cp_NullValue(_read_.ENBATPRE))
      this.w_BATPOS = NVL(cp_ToDate(_read_.ENBATPOS),cp_NullValue(_read_.ENBATPOS))
      this.w_BATCHK = NVL(cp_ToDate(_read_.ENBATCHK),cp_NullValue(_read_.ENBATCHK))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PADRE = This.oparentobject
    do case
      case this.pTIPOPE = "IMPOR"
        if EMPTY(this.oParentObject.w_PARAM)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.oParentObject.Notifyevent("Esegui")
        endif
        this.w_EXPRCOL = "ICASE(NVL(TIPREC,' ') ='O', RGB(35,150,35),Not empty(NVL(TIPREC,' ')),RGB(255,0,0), RGB(0,0,0))"
        gsva_bzc(this,this.oParentObject.w_ZOOMDOC,this.w_EXPRCOL,"C")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_oErrorLog.IsFullLog()
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
        else
          AddMsgNL("Inserimento in tabelle clone completato con successo",this )
        endif
        this.oParentObject.w_FASE2 = .T.
        this.oParentObject.w_FASE = .T.
      case this.pTIPOPE = "CONFE" or this.pTIPOPE = "VERIF"
        this.w_TEST = .F.
        this.oParentObject.w_Msg = ""
        * --- Create temporary table TMPDETTI
        i_nIdx=cp_AddTableDef('TMPDETTI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\VEFA\EXE\QUERY\GSVALBIM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPDETTI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if Upper(this.w_PADRE.Class)="TGSVA1BCD"
          this.w_NUMREC = 1
        else
          AddMsgNL("Inzio verifica documenti",this )
          Select (this.oParentObject.w_ZOOMDOC.cCURSOR)
          COUNT FOR XCHK=1 TO this.w_NUMREC
        endif
        if this.w_NUMREC>0
          this.w_TEST = .t.
          this.w_GDKEYRIF = this.oParentObject.w_SER_ELAB
          * --- Creo tabelle generatore con rottura per destinazione diversa
          GSVE_BIG(this,"C", this.class, "N", "S", "N", "N", "S", "N", "A", this.oParentObject.w_FLRPZ, "S", "", IIF(this.pTIPOPE = "VERIF", "S", "N")) 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Adesso devo valorizzare tabelle temporanee utilizzate dal generatore 
          *     costruendo una frase di insert con i campi realmente esistenti nella 
          *     tabella EDI di adhoc
           
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="MVSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
          this.w_CODTAB = "DELFOR_1"
          this.w_TMP_GEN = "TMPMASTI"
          this.w_TMP_EDI = "TMPVFDOCMAST"
          this.w_OQUERY = "..\VEFA\EXE\QUERY\GSVA_DEL"
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Dimension ARRREAD(1,2) 
 ARRREAD[1,1]="MVSERIAL" 
 ARRREAD[1,2]="@@@@@@@@@"
          this.w_OQUERY = "..\VEFA\EXE\QUERY\GSVA1DEL"
          this.w_TMP_GEN = "TMPDETTI"
          this.w_TMP_EDI = "TMPVFDOCDETT"
          this.w_CODTAB = "DELFOR_3"
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_TIPEVA="S"
            this.w_OLDDES = REPL("@",6)
            this.w_OLDSER = REPL("@",11)
            this.w_CPROWNUM = 0
            * --- Select from GSVA2DEL
            do vq_exec with 'GSVA2DEL',this,'_Curs_GSVA2DEL','',.f.,.t.
            if used('_Curs_GSVA2DEL')
              select _Curs_GSVA2DEL
              locate for 1=1
              do while not(eof())
              if this.w_OLDSER<> this.w_SERIAL OR this.w_OLDDES<> Nvl(_Curs_GSVA2DEL.MVCODDES," ")
                this.w_LGIOLIM = this.oParentObject.w_GIOLIM
              endif
              this.w_MVCODICE = _Curs_GSVA2DEL.MVCODICE
              this.w_MVDATEVA = _Curs_GSVA2DEL.MVDATEVA
              this.w_SERIAL = _Curs_GSVA2DEL.MVSERIAL
              this.w_ROWNUM = _Curs_GSVA2DEL.CPROWNUM
              this.w_DFDTPFQU = Nvl(_Curs_GSVA2DEL.DFDTPFQU, " ")
              this.w_DFTIMUNI = Nvl(_Curs_GSVA2DEL.DFTIMUNI, " ")
              this.w_QTAMOV = Nvl(_Curs_GSVA2DEL.MVQTAMOV,0)
              if this.w_OLDSER<> this.w_SERIAL
                * --- Determino max cprownum
                * --- Select from TMPDETTI
                i_nConn=i_TableProp[this.TMPDETTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2],.t.,this.TMPDETTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" TMPDETTI ";
                      +" where MVSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
                       ,"_Curs_TMPDETTI")
                else
                  select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
                   where MVSERIAL=this.w_SERIAL;
                    into cursor _Curs_TMPDETTI
                endif
                if used('_Curs_TMPDETTI')
                  select _Curs_TMPDETTI
                  locate for 1=1
                  do while not(eof())
                  this.w_CPROWNUM = Nvl(_Curs_TMPDETTI.CPROWNUM,0)
                  exit
                    select _Curs_TMPDETTI
                    continue
                  enddo
                  use
                endif
              endif
              do case
                case this.w_DFDTPFQU="2"
                  * --- maschera AAAAWW
                  this.w_ANNO = SUBSTR(this.w_DFTIMUNI,1,4)
                  this.w_SETTIM = VAL(SUBSTR(this.w_DFTIMUNI,5,2))
                  this.w_INIANNO = Cp_CharTodate("01-01-"+Alltrim(this.w_ANNO))
                  this.w_DFSTADAT = IIF(DOW(this.w_INIANNO)<>1,this.w_INIANNO-DOW(this.w_INIANNO)+2,this.w_INIANNO-6)+((this.w_SETTIM-1)*7)
                  this.w_DFENDDAT = IIF(DOW(this.w_INIANNO)<>1,this.w_INIANNO-DOW(this.w_INIANNO)+8,this.w_INIANNO)+ ((this.w_SETTIM-1)*7)
                case this.w_DFDTPFQU="716"
                  * --- maschera AAAAWWAAAAWW
                  this.w_ANNO = SUBSTR(this.w_DFTIMUNI,1,4)
                  this.w_SETTIM = VAL(SUBSTR(this.w_DFTIMUNI,5,2))
                  this.w_INIANNO = Cp_CharTodate("01-01-"+Alltrim(this.w_ANNO))
                  this.w_DFSTADAT = IIF(DOW(this.w_INIANNO)<>1,this.w_INIANNO-DOW(this.w_INIANNO)+2,this.w_INIANNO-6)+((this.w_SETTIM-1)*7)
                  this.w_ANNO = SUBSTR(this.w_DFTIMUNI,7,4)
                  this.w_SETTIM = VAL(SUBSTR(this.w_DFTIMUNI,11,2))
                  this.w_INIANNO = Cp_CharTodate("01-01-"+Alltrim(this.w_ANNO))
                  this.w_DFENDDAT = IIF(DOW(this.w_INIANNO)<>1,this.w_INIANNO-DOW(this.w_INIANNO)+8,this.w_INIANNO)+ ((this.w_SETTIM-1)*7)
                otherwise
                  this.w_DFSTADAT = CP_TODATE(_Curs_GSVA2DEL.DFSTADAT)
                  this.w_DFENDDAT = CP_TODATE(_Curs_GSVA2DEL.DFENDDAT)
              endcase
              if Not Empty(this.w_DFSTADAT) and Not Empty(this.w_DFENDDAT)
                * --- Scrivo data inizio e fine periodo
                this.w_GIORNI = (this.w_DFENDDAT-this.w_DFSTADAT)
                if this.w_GIORNI>0
                  this.w_MVQTAMOV = IIF(INT(this.w_QTAMOV/this.w_GIORNI)<this.oParentObject.w_QTAMIN,this.oParentObject.w_QTAMIN,INT(this.w_QTAMOV/this.w_GIORNI))
                  this.w_DATEVA = this.w_DFSTADAT
                  if this.oParentObject.w_TIPSCA="F"
                    * --- Tipo scadenza fine mese devo continuare a splittare fino al primo fine mese
                    this.w_FINEMESE = Cp_Chartodate("01-"+Right("00"+str(Month(this.w_DFSTADAT)+1),2)+"-"+str(year(this.w_DFSTADAT)))-1
                  endif
                  * --- Esco dal ciclo se:
                  *     -finisco la qta
                  *     -finisco i giorni del periodo
                  *     -finisco i giorni limite e sono per data inizio pianificazione
                  *     -finisco i giorni limite e sono per fine mese e sono arrivato alla fine del mese 
                  do while this.w_QTAMOV>0 AND this.w_GIORNI>0 and ((this.oParentObject.w_TIPSCA="I" and this.w_LGIOLIM>0) OR (this.oParentObject.w_TIPSCA="F" AND (this.w_DATEVA<>this.w_FINEMESE or this.w_LGIOLIM>0)))
                    do while DOW(this.w_DATEVA)=1 OR DOW(this.w_DATEVA)=7
                      * --- Escludo i festivi
                      this.w_DATEVA = this.w_DATEVA + 1
                    enddo
                    this.w_CPROWNUM = this.w_CPROWNUM + 1
                    if this.w_QTAMOV<this.w_MVQTAMOV
                      this.w_MVQTAMOV = this.w_QTAMOV
                    endif
                    * --- Insert into TMPDETTI
                    i_nConn=i_TableProp[this.TMPDETTI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPDETTI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"MVSERIAL"+",MVNUMRIF"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVRIFEDI"+",MVDESSUP"+",MVDATEVA"+",MVQTAMOV"+",CPROWNUM"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVSERIAL),'TMPDETTI','MVSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVNUMRIF),'TMPDETTI','MVNUMRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVCODICE),'TMPDETTI','MVCODICE');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVDESART),'TMPDETTI','MVDESART');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVUNIMIS),'TMPDETTI','MVUNIMIS');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVRIFEDI),'TMPDETTI','MVRIFEDI');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVDESSUP),'TMPDETTI','MVDESSUP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'TMPDETTI','MVDATEVA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'TMPDETTI','MVQTAMOV');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMPDETTI','CPROWNUM');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',_Curs_GSVA2DEL.MVSERIAL,'MVNUMRIF',_Curs_GSVA2DEL.MVNUMRIF,'MVCODICE',_Curs_GSVA2DEL.MVCODICE,'MVDESART',_Curs_GSVA2DEL.MVDESART,'MVUNIMIS',_Curs_GSVA2DEL.MVUNIMIS,'MVRIFEDI',_Curs_GSVA2DEL.MVRIFEDI,'MVDESSUP',_Curs_GSVA2DEL.MVDESSUP,'MVDATEVA',this.w_DATEVA,'MVQTAMOV',this.w_MVQTAMOV,'CPROWNUM',this.w_CPROWNUM)
                      insert into (i_cTable) (MVSERIAL,MVNUMRIF,MVCODICE,MVDESART,MVUNIMIS,MVRIFEDI,MVDESSUP,MVDATEVA,MVQTAMOV,CPROWNUM &i_ccchkf. );
                         values (;
                           _Curs_GSVA2DEL.MVSERIAL;
                           ,_Curs_GSVA2DEL.MVNUMRIF;
                           ,_Curs_GSVA2DEL.MVCODICE;
                           ,_Curs_GSVA2DEL.MVDESART;
                           ,_Curs_GSVA2DEL.MVUNIMIS;
                           ,_Curs_GSVA2DEL.MVRIFEDI;
                           ,_Curs_GSVA2DEL.MVDESSUP;
                           ,this.w_DATEVA;
                           ,this.w_MVQTAMOV;
                           ,this.w_CPROWNUM;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    this.w_GIORNI = this.w_GIORNI-1
                    this.w_QTAMOV = this.w_QTAMOV-this.w_MVQTAMOV
                    if this.w_GIORNI=0 and this.w_QTAMOV>0
                      * --- Finisco il periodo ed ho del resto lo inserisco sull'ultima riga
                      * --- Write into TMPDETTI
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.TMPDETTI_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPDETTI_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVQTAMOV =MVQTAMOV+ "+cp_ToStrODBC(this.w_QTAMOV);
                            +i_ccchkf ;
                        +" where ";
                            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                               )
                      else
                        update (i_cTable) set;
                            MVQTAMOV = MVQTAMOV + this.w_QTAMOV;
                            &i_ccchkf. ;
                         where;
                            MVSERIAL = this.w_SERIAL;
                            and CPROWNUM = this.w_CPROWNUM;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      this.w_QTAMOV = 0
                    endif
                    this.w_LGIOLIM = this.w_LGIOLIM-1
                    this.w_FINEMESE = Cp_Chartodate("01-"+Right("00"+str(Month(this.w_DATEVA)+1),2)+"-"+str(year(this.w_DATEVA)))-1
                    this.w_DATEVA = this.w_DATEVA + 1
                  enddo
                  if this.w_QTAMOV>0
                    * --- Ho un residuo per effetto dell'uscita in anticipo per i giorini limite
                    *     devo mettere il residuo in un'ulteriore riga successiva
                    this.w_DATEVA = this.w_DATEVA+ 1
                    this.w_CPROWNUM = this.w_CPROWNUM + 1
                    * --- Insert into TMPDETTI
                    i_nConn=i_TableProp[this.TMPDETTI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TMPDETTI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPDETTI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"MVSERIAL"+",MVNUMRIF"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVRIFEDI"+",MVDESSUP"+",MVDATEVA"+",MVQTAMOV"+",CPROWNUM"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVSERIAL),'TMPDETTI','MVSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVNUMRIF),'TMPDETTI','MVNUMRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVCODICE),'TMPDETTI','MVCODICE');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVDESART),'TMPDETTI','MVDESART');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVUNIMIS),'TMPDETTI','MVUNIMIS');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVRIFEDI),'TMPDETTI','MVRIFEDI');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_GSVA2DEL.MVDESSUP),'TMPDETTI','MVDESSUP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'TMPDETTI','MVDATEVA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAMOV),'TMPDETTI','MVQTAMOV');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMPDETTI','CPROWNUM');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',_Curs_GSVA2DEL.MVSERIAL,'MVNUMRIF',_Curs_GSVA2DEL.MVNUMRIF,'MVCODICE',_Curs_GSVA2DEL.MVCODICE,'MVDESART',_Curs_GSVA2DEL.MVDESART,'MVUNIMIS',_Curs_GSVA2DEL.MVUNIMIS,'MVRIFEDI',_Curs_GSVA2DEL.MVRIFEDI,'MVDESSUP',_Curs_GSVA2DEL.MVDESSUP,'MVDATEVA',this.w_DATEVA,'MVQTAMOV',this.w_QTAMOV,'CPROWNUM',this.w_CPROWNUM)
                      insert into (i_cTable) (MVSERIAL,MVNUMRIF,MVCODICE,MVDESART,MVUNIMIS,MVRIFEDI,MVDESSUP,MVDATEVA,MVQTAMOV,CPROWNUM &i_ccchkf. );
                         values (;
                           _Curs_GSVA2DEL.MVSERIAL;
                           ,_Curs_GSVA2DEL.MVNUMRIF;
                           ,_Curs_GSVA2DEL.MVCODICE;
                           ,_Curs_GSVA2DEL.MVDESART;
                           ,_Curs_GSVA2DEL.MVUNIMIS;
                           ,_Curs_GSVA2DEL.MVRIFEDI;
                           ,_Curs_GSVA2DEL.MVDESSUP;
                           ,this.w_DATEVA;
                           ,this.w_QTAMOV;
                           ,this.w_CPROWNUM;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    this.w_QTAMOV = 0
                  endif
                endif
              endif
              this.w_OLDDES = _Curs_GSVA2DEL.MVCODDES
              this.w_OLDSER = _Curs_GSVA2DEL.MVSERIAL
                select _Curs_GSVA2DEL
                continue
              enddo
              use
            endif
          endif
          if this.pTIPOPE = "CONFE" 
            * --- Aggiorno evasione piani precedenti in base alle azioni previste
            *      -DFMESFUN 5 sostituisce uno precedente ( Vanno considerati tutti gli ordini aperti per quel intestatario)
            *      **DFPROIND (solo per delfor97A per il quale ha priorit�, identifica se valorizzato a 37 che la precedente ordinazione non � pi� valida prevedere eventuale auto evasione per estrometterla) 
            *     DFACTREQ 3 azione di riga su ordini con seriale dato da DSLASFOR
            * --- -DFMESFUN 5 sostituisce uno precedente ( Vanno considerati tutti gli ordini aperti per quel intestatario)
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
              do vq_exec with 'GSVA4DEL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
              +"DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVFLEVAS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura evasione azione di testata 5'
              return
            endif
            * --- -DFMESFUN 4  modifica  (rispetto al 5 chiudo solo le righe di di competenza del periodo estrappolato dalle caratteristiche del DTM campo DFMESDAT).
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
              do vq_exec with 'GSVA5DEL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
              +"DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVFLEVAS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                      +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                      +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore scrittura evasione azione di riga '
              return
            endif
          endif
          this.w_NUMREC = 1
          * --- Applico Valori predefiniti
          * --- Applico valori predefiniti eventualmente previsti nella struttura 
          *     sui campi che non presentano una valorizzazione specifica (a NULL)
          * --- Select from VAPREDEF
          i_nConn=i_TableProp[this.VAPREDEF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAPREDEF_idx,2],.t.,this.VAPREDEF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" VAPREDEF ";
                +" where PRCODSTR="+cp_ToStrODBC(this.pCODSTR)+"";
                +" order by CPROWORD";
                 ,"_Curs_VAPREDEF")
          else
            select * from (i_cTable);
             where PRCODSTR=this.pCODSTR;
             order by CPROWORD;
              into cursor _Curs_VAPREDEF
          endif
          if used('_Curs_VAPREDEF')
            select _Curs_VAPREDEF
            locate for 1=1
            do while not(eof())
            this.w_TIPVAL = Nvl(_Curs_VAPREDEF.PR__TIPO,"F")
            this.w_CODTAB = Nvl(_Curs_VAPREDEF.PRCODTAB,space(30))
            this.w_LCAMPO = Nvl(_Curs_VAPREDEF.PR_CAMPO,space(30))
            this.w_CODTABVAL = Nvl(_Curs_VAPREDEF.PR_TABLE,space(30))
            this.w_CAMPOVAL = Nvl(_Curs_VAPREDEF.PR_FIELD,space(30))
            this.w_LTIPCAM = _Curs_VAPREDEF.PRTIPCAM
            Dimension ArrWrite[1,2],ArrVal[1,3]
            * --- Read from XDC_FIELDS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FLDECIMA,FLLENGHT"+;
                " from "+i_cTable+" XDC_FIELDS where ";
                    +"TBNAME = "+cp_ToStrODBC(this.w_CODTAB);
                    +" and FLNAME = "+cp_ToStrODBC(this.w_LCAMPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FLDECIMA,FLLENGHT;
                from (i_cTable) where;
                    TBNAME = this.w_CODTAB;
                    and FLNAME = this.w_LCAMPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DECIMAL = NVL(cp_ToDate(_read_.FLDECIMA),cp_NullValue(_read_.FLDECIMA))
              this.w_LENGHT = NVL(cp_ToDate(_read_.FLLENGHT),cp_NullValue(_read_.FLLENGHT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_LTIPCAM="D"
                 
 ArrVal[1,1]=Alltrim(this.w_LCAMPO) 
 ArrVal[1,2]="" 
 ArrVal[1,3]= "IS NULL"
                if this.w_TIPVAL="F"
                  VALORE=cp_todate(_Curs_VAPREDEF.PRVALDAT)
                endif
              case this.w_LTIPCAM="N"
                if this.w_TIPVAL="F"
                  VALORE=Nvl(_Curs_VAPREDEF.PRVALNUM,0)
                endif
                 
 ArrVal[1,1]=this.w_LCAMPO 
 ArrVal[1,2]=0 
 ArrVal[1,3]= "="
              otherwise
                if this.w_LTIPCAM="M"
                   
 VALORE=_Curs_VAPREDEF.PRVALCAR
                else
                   
 VALORE=SUBSTR(_Curs_VAPREDEF.PRVALCAR,1,this.w_LENGHT)
                endif
                if this.w_TIPVAL="F"
                  VALORE=SUBSTR(_Curs_VAPREDEF.PRVALCAR,1,this.w_LENGHT)
                endif
                 
 ArrVal[1,1]=Alltrim(this.w_LCAMPO) 
 ArrVal[1,2]="" 
 ArrVal[1,3]= "IS NULL"
            endcase
            if this.w_TIPVAL="V"
              if Not Empty(this.w_CAMPOVAL)
                this.w_LCODTAB = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTABVAL)
                 
 NC=READTABLE(this.w_LCODTAB,Alltrim(this.w_CAMPOVAL),"","",.F.,"FT___UID='"+this.oParentObject.w_SER_ELAB+"'")
                if Reccount((Nc)) <>0
                  Valore=Evaluate(Alltrim(this.w_CAMPOVAL))
                endif
              endif
              * --- Chiudo cursore read
              if Used((NC))
                 
 Select (NC) 
 use
              endif
            endif
             
 ArrWrite[1,1]=this.w_LCAMPO 
 ArrWrite[1,2]=valore
            do case
              case this.w_codtab="DOC_MAST"
                this.w_PHNAME = "TMPMASTI"
              case this.w_codtab="DOC_DETT"
                this.w_PHNAME = "TMPDETTI"
              case this.w_codtab="MOVIMATR"
                this.w_PHNAME = "TMPMATRI"
              case this.w_codtab="DOC_RATE" AND g_APPLICATION="ADHOC REVOLUTION"
                this.w_PHNAME = "TMPDOCRATE"
              case this.w_codtab="MOVILOTT" AND g_APPLICATION="ad hoc ENTERPRISE"
                this.w_PHNAME = "TMPLOTTI"
            endcase
            WRITETABLE( this.w_PHNAME , @ArrWrite , @ArrVal , this.w_oERRORLOG ) 
              select _Curs_VAPREDEF
              continue
            enddo
            use
          endif
          if !EMPTY(NVL(this.oParentObject.w_TIPDOC, " "))
             
 DIMENSION Arrval(1,2),ArrWrite(1,2) 
 Arrwrite[1,1]="MVTIPDOC" 
 Arrwrite[1,2]=this.oParentObject.w_TIPDOC 
 ArrVAL[1,1]="" 
 Arrval[1,2]="" 
 WRITETABLE( "TMPMASTI" , @ArrWrite , .F. , "",.F.,"1=1" ) 
          endif
          this.w_OKBGE = .T.
          if this.pTIPOPE = "CONFE" 
            this.w_OLDSERLOG = this.oParentObject.w_SERGDOC 
            * --- Verifico la presenza di errori 
            * --- Read from LOG_GEND
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LOG_GEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LGMESSAG"+;
                " from "+i_cTable+" LOG_GEND where ";
                    +"LGSERIAL = "+cp_ToStrODBC(this.w_OLDSERLOG);
                    +" and LGTIPRIG = "+cp_ToStrODBC("E");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LGMESSAG;
                from (i_cTable) where;
                    LGSERIAL = this.w_OLDSERLOG;
                    and LGTIPRIG = "E";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.LGMESSAG),cp_NullValue(_read_.LGMESSAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS<>0
              * --- Ho almeno un errore bloccante non eseguo generazione
              this.w_OKBGE = .F.
            endif
          endif
          this.oParentObject.w_SERGDOC = this.w_SERGDOC
          if this.w_OKBGE
            GSVE_BGE(this,this.w_SERGDOC)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          GSVE_BIG(this,"D")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Eseguo evasioni \azioni previste nel file
          if this.pTIPOPE = "CONFE" 
            * --- Verifico la presenza di errori 
            * --- Read from LOG_GEND
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LOG_GEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LGMESSAG"+;
                " from "+i_cTable+" LOG_GEND where ";
                    +"LGSERIAL = "+cp_ToStrODBC(this.w_SERGDOC);
                    +" and LGTIPRIG = "+cp_ToStrODBC("E");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LGMESSAG;
                from (i_cTable) where;
                    LGSERIAL = this.w_SERGDOC;
                    and LGTIPRIG = "E";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.LGMESSAG),cp_NullValue(_read_.LGMESSAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS<>0
              * --- Ho almeno un errore bloccante non eseguo generazione
              this.w_OKBGE = .F.
            endif
          endif
          if this.pTIPOPE = "CONFE" AND this.w_OKBGE
            * --- Aggiorno stautus Tabelle
            * --- Select from FLATDETT
            i_nConn=i_TableProp[this.FLATDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select FTCODICE,FTTABNAM  from "+i_cTable+" FLATDETT ";
                  +" where FTCODICE="+cp_ToStrODBC(this.w_FLATAB)+"";
                  +" group by FTCODICE,FTTABNAM";
                   ,"_Curs_FLATDETT")
            else
              select FTCODICE,FTTABNAM from (i_cTable);
               where FTCODICE=this.w_FLATAB;
               group by FTCODICE,FTTABNAM;
                into cursor _Curs_FLATDETT
            endif
            if used('_Curs_FLATDETT')
              select _Curs_FLATDETT
              locate for 1=1
              do while not(eof())
              this.w_CODTAB = _Curs_FLATDETT.FTTABNAM
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
                select _Curs_FLATDETT
                continue
              enddo
              use
            endif
            this.Pag8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ricostruisco tabelle temporanee che interrogano tabelle clone
            *     per aggiornare dati rispetto all'importazione eseguita
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Aggiorno lo stato degli errori a seguito di verifica
            if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
              UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET ERRORMSG = "" WHERE XCHK=1
            endif
            * --- Select from LOG_GEND
            i_nConn=i_TableProp[this.LOG_GEND_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MAX(LGTIPRIG) AS LGTIPRIG,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM  from "+i_cTable+" LOG_GEND ";
                  +" where LGSERIAL="+cp_ToStrODBC(this.w_SERGDOC)+" AND LGTIPRIG<>'L'";
                  +" group by LGSERORI, LGROWORI, LGMESSAG,CPROWNUM";
                  +" order by CPROWNUM";
                   ,"_Curs_LOG_GEND")
            else
              select MAX(LGTIPRIG) AS LGTIPRIG,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM from (i_cTable);
               where LGSERIAL=this.w_SERGDOC AND LGTIPRIG<>"L";
               group by LGSERORI, LGROWORI, LGMESSAG,CPROWNUM;
               order by CPROWNUM;
                into cursor _Curs_LOG_GEND
            endif
            if used('_Curs_LOG_GEND')
              select _Curs_LOG_GEND
              locate for 1=1
              do while not(eof())
              this.w_LGSERORI = _Curs_LOG_GEND.LGSERORI
              this.w_LGROWORI = _Curs_LOG_GEND.LGROWORI
              this.w_LGTIPRIG = _Curs_LOG_GEND.LGTIPRIG
              this.w_LGMESSAG = IIF(!EMPTY(NVL(this.w_LGROWORI, " ")), ah_msgformat("Riga %1: ", ALLTRIM(STR(this.w_LGROWORI))) , "") + ALLTRIM(_Curs_LOG_GEND.LGMESSAG)
              if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
                UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET ERRORMSG = IIF(EMPTY(NVL(ERRORMSG, " ")), "", ERRORMSG+CHR(10))+this.w_LGMESSAG WHERE DFSERIAL=this.w_LGSERORI
              endif
                select _Curs_LOG_GEND
                continue
              enddo
              use
            endif
            if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
              UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET TIPREC=IIF(EMPTY(NVL(ERRORMSG, " ")), "O", "E") WHERE XCHK=1
              SELECT (this.oParentObject.w_ZOOMDOC.cCURSOR)
              GO TOP
              this.oParentObject.w_ERRORLOG = ALLTRIM(ERRORMSG)
            endif
          endif
          * --- Definisco propiet� dello zoom delle testate
          if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
            this.w_EXPRCOL = "ICASE(NVL(TIPREC,' ') ='O', RGB(35,150,35),Not empty(NVL(TIPREC,' ')),RGB(255,0,0), RGB(0,0,0))"
            gsva_bzc(this,this.oParentObject.w_ZOOMDOC,this.w_EXPRCOL,"COLOR")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_TEST
            if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
              if this.pTIPOPE = "CONFE" 
                ah_ErrorMsg("Import terminato con successo","i","")
              else
                ah_ErrorMsg("Verifica terminata","i","")
              endif
            endif
            this.oParentObject.w_FASE2 = .T.
          else
            if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
              ah_ErrorMsg("Attenzione nessun documento selezionato! ",,"")
            endif
          endif
        else
          if Upper(this.w_PADRE.Class)<>"TGSVA1BCD"
            ah_ErrorMsg("Attenzione! Nessun documento selezionato",,"")
          endif
        endif
        if Upper(this.w_PADRE.Class)="TGSVA1BCD"
          this.Pag8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pTIPOPE = "DONE"
        this.Pag8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPOPE = "SELEZ" Or this.pTIPOPE = "DESEL" Or this.pTIPOPE = "INVSE"
        ND=this.oParentObject.w_ZOOMDOC.cCURSOR
        UPDATE (ND) SET XCHK=ICASE(this.pTIPOPE="SELEZ",1,this.pTIPOPE="DESEL", 0, IIF(XCHK=1,0,1))
      case this.pTIPOPE = "DEFAC"
        * --- Valorizza campo valore da aggiornare con default dello zoom
        ND=this.oParentObject.w_ZOOMDOC.cCURSOR
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 VALTRAS=(ND)+"."+Alltrim(this.oParentObject.w_CAMPO) 
 VALTRAS=&VALTRAS 
 on error &L_OldError 
        if Not L_err
          do case
            case this.oParentObject.w_TIPCAM="N"
              this.oParentObject.w_NUMERIC = VALTRAS
            case this.oParentObject.w_TIPCAM="D"
              this.oParentObject.w_DATA = VALTRAS
            otherwise
              this.oParentObject.w_CARACTER = VALTRAS
          endcase
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo tabelle temporanee
    this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DELFOR_1")
    this.w_COD_TMP = "TMPVFDOCMAST"
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DELFOR_2")
    this.w_COD_TMP = "TMPVFDOCDETT"
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DELFOR_3")
    this.w_COD_TMP = "TMPVFDOCRATE"
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODSTR = GetETPhname(alltrim(this.w_FLATAB), "DELFOR_4")
    this.w_COD_TMP = "TMPVFMOVIMATR"
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\VEFA\EXE\QUERY\GSVA_QPR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPLIST
    i_nIdx=cp_AddTableDef('TMPLIST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\VEFA\EXE\QUERY\GSVALBIM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPLIST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Rilancio zoom
    this.oParentObject.Notifyevent("Esegui")
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Applico eventuali trascodifiche prerviste nell'elemento
    this.w_PHNAME = getclonename("DOC_MAST")
     
 Dimension ArrTest[1,2] 
 ArrTest[1,1]="MVSERIAL" 
 ArrTest[1,2]=this.w_MVSERIAL
    CURSOR=READTABLE(this.w_PHNAME,"MVCODCON,MVTIPCON",@ARRTEST,,,)
    this.w_TIPCON = &CURSOR..MVTIPCON
    this.w_CODCON = &CURSOR..MVCODCON
    do case
      case this.w_TIPTRA="I"
        * --- Da intestatario
        if Not empty(this.w_CODCON) 
          if Not empty(this.w_CODCON)
            * --- Cerco trascodifica specifica dell'intestatario
            * --- Read from VATRASCO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VATRASCO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2],.t.,this.VATRASCO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TRCODTRI"+;
                " from "+i_cTable+" VATRASCO where ";
                    +"TRCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                    +" and TRCODENT = "+cp_ToStrODBC(this.w_CODENT);
                    +" and TRCODELE = "+cp_ToStrODBC(w_CODELE);
                    +" and TRCODCON = "+cp_ToStrODBC(this.w_CODCON);
                    +" and TRTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TRCODTRI;
                from (i_cTable) where;
                    TRCODSTR = this.w_CODSTR;
                    and TRCODENT = this.w_CODENT;
                    and TRCODELE = w_CODELE;
                    and TRCODCON = this.w_CODCON;
                    and TRTIPCON = this.w_TIPCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODGRU"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODGRU;
                from (i_cTable) where;
                    ANTIPCON = this.w_TIPCON;
                    and ANCODICE = this.w_CODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODRAG = NVL(cp_ToDate(_read_.ANCODGRU),cp_NullValue(_read_.ANCODGRU))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if Empty(this.w_CODTRA) and Not Empty(this.w_CODRAG)
            * --- Se non trovo trascodifica specifica dell'intestatario
            *     cerco quella del gruppo
            * --- Read from VATRASCO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VATRASCO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2],.t.,this.VATRASCO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TRCODTRI"+;
                " from "+i_cTable+" VATRASCO where ";
                    +"TRCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                    +" and TRCODENT = "+cp_ToStrODBC(this.w_CODENT);
                    +" and TRCODELE = "+cp_ToStrODBC(w_CODELE);
                    +" and TRCODRAG = "+cp_ToStrODBC(this.w_CODRAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TRCODTRI;
                from (i_cTable) where;
                    TRCODSTR = this.w_CODSTR;
                    and TRCODENT = this.w_CODENT;
                    and TRCODELE = w_CODELE;
                    and TRCODRAG = this.w_CODRAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if Not Empty(this.w_CODTRA)
            * --- Read from TRS_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TRS_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRS_MAST_idx,2],.t.,this.TRS_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TRFLALLT"+;
                " from "+i_cTable+" TRS_MAST where ";
                    +"TRCODICE = "+cp_ToStrODBC(this.w_CODTRA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TRFLALLT;
                from (i_cTable) where;
                    TRCODICE = this.w_CODTRA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Applico alltrim  se previsto
            EXPR=NVL(EXPR,"")
            if this.w_TRFLALLT$ "I-T"
              EXPR=ALLTRIM(EXPR)
            endif
            * --- Read from TRS_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TRS_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2],.t.,this.TRS_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TRCODINT"+;
                " from "+i_cTable+" TRS_DETT where ";
                    +"TRCODICE = "+cp_ToStrODBC(this.w_CODTRA);
                    +" and TRCODEXT = "+cp_ToStrODBC(EXPR);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TRCODINT;
                from (i_cTable) where;
                    TRCODICE = this.w_CODTRA;
                    and TRCODEXT = EXPR;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS<>0
               
 EXPR=this.w_TMP_VAL
            endif
          endif
        endif
      case this.w_TIPTRA="S"
        * --- Da struttura
        * --- Read from VATRASCO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VATRASCO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2],.t.,this.VATRASCO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRCODTRI"+;
            " from "+i_cTable+" VATRASCO where ";
                +"TRCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                +" and TRCODENT = "+cp_ToStrODBC(this.w_CODENT);
                +" and TRCODELE = "+cp_ToStrODBC(w_CODELE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRCODTRI;
            from (i_cTable) where;
                TRCODSTR = this.w_CODSTR;
                and TRCODENT = this.w_CODENT;
                and TRCODELE = w_CODELE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty(this.w_CODTRA)
          * --- Read from TRS_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TRS_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TRS_MAST_idx,2],.t.,this.TRS_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRFLALLT"+;
              " from "+i_cTable+" TRS_MAST where ";
                  +"TRCODICE = "+cp_ToStrODBC(this.w_CODTRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRFLALLT;
              from (i_cTable) where;
                  TRCODICE = this.w_CODTRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Applico alltrim  se previsto
          EXPR=NVL(EXPR,"")
          if this.w_TRFLALLT$ "I-T"
            EXPR=ALLTRIM(EXPR)
          endif
          * --- Read from TRS_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TRS_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TRS_DETT_idx,2],.t.,this.TRS_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODINT"+;
              " from "+i_cTable+" TRS_DETT where ";
                  +"TRCODICE = "+cp_ToStrODBC(this.w_CODTRA);
                  +" and TRCODEXT = "+cp_ToStrODBC(EXPR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODINT;
              from (i_cTable) where;
                  TRCODICE = this.w_CODTRA;
                  and TRCODEXT = EXPR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS<>0
             
 EXPR=this.w_TMP_VAL
          endif
        endif
      case this.w_TIPTRA="B" and Not empty(this.w_BATCH)
        * --- Da routine
         
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 Batch=ALLTRIM(this.w_BATCH) 
 Batch=IIF(AT("(",Batch)>0, Batch, Batch+"(This)") 
 VALTRAS=&Batch 
 EXPR=IIF(Not empty(VALTRAS),VALTRAS,EXPR)
        on error &L_OldError 
    endcase
    * --- Chiudo cursore utilizzato per fare Read
    if Used((CURSOR))
       
 Select (CURSOR) 
 use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PHNAME = getclonename(this.w_LCODTAB)
     
 CURS=READTABLE(this.w_PHNAME,"*",,,,this.w_FILTER)
    if Reccount((CURS))>0
      DELETETABLE( this.w_PHNAME , @ArrVal , this.w_oERRORLOG ) 
    endif
    * --- Chiudo cursore creato per eseguire la Read
    if Used((CURS))
       
 Select (CURS) 
 use
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 CURS=READTABLE(this.w_CODSTR,"*","","",.F.,"FT___UID='"+this.oParentObject.w_SER_ELAB+"'")
    this.w_Risultato = CurToTab(Curs, this.w_COD_TMP)
     
 use in (curs)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if ChketStatus(alltrim(this.w_FLATAB), this.w_CODTAB)=0
      this.w_EDI_TAB = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTAB)
      * --- Aggiorno status elaborazione
       
 DIMENSION Arrval(1,2),ArrWrite(1,2) 
 Arrwrite[1,1]="FTSTATUS" 
 Arrwrite[1,2]="S" 
 ArrVAL[1,1]="" 
 Arrval[1,2]="" 
 WRITETABLE( this.w_EDI_TAB, @ArrWrite , .F. , "",.F.,"1=1" ) 
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico presenza della tabella
    if ChketStatus(alltrim(this.w_FLATAB), this.w_CODTAB)=0
       
 DIMENSION Arraval(1,2),Fil(1) 
 ARRAVAL[1,1]="FT___UID" 
 ARRAVAL[1,2]="@@@@@@@@@" 
 CURS=SYS(2015) 
 VQ_EXEC(this.w_OQUERY,this,Curs) 
 A=AFIELDS(FIL,CURS) 
 SELECT (CURS) 
 use in (CURS)
      this.w_EDI_TAB = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTAB)
    endif
    * --- Devo creare una tabella di appoggio perch� i campi di delfor non hanno
    *     lo stesso nome delle tabelle temporanee del generatore
    * --- Create temporary table TMPDOCU
    i_nIdx=cp_AddTableDef('TMPDOCU') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec(''+this.w_OQUERY+'',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPDOCU_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_LOOP = 0
     
 Sel=""
    * --- Verifico la presenza dei campi nella tabella del generatore
    do while this.w_LOOP<ALEN(fil,1)
      this.w_LOOP = this.w_LOOP+1
      this.w_FIELD = Fil(this.w_LOOP,1)
       
 NC=READTABLE(this.w_TMP_GEN,this.w_FIELD,@ARRREAD,,,)
      if used(NC)
         
 sel=sel+alltrim(this.w_FIELD)+"," 
 SELECT (NC) 
 use in (NC)
      endif
    enddo
    * --- Elimino ultima virgola
     
 Sel=left(alltrim(Sel),len(alltrim(Sel))-1)
     
 L_FONTE_IDX=Cp_OpenTable( this.w_TMP_GEN) 
 L_TMPDOC_IDX=Cp_OpenTable( "TMPDOCU") 
 L_nConn = i_TableProp[L_FONTE_IDX,3]
    this.w_STATUS = "S"
    * --- Valorizzo tabelle del generatore con elenco campi estrappolati 
    *     da tabelle edi
     
 Tab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 TabOri=cp_setazi( I_TABLEPROP[L_TMPDOC_IDX,2]) 
 cmsql="Insert Into "+ alltrim(Tab) + " ( "+ alltrim(sel) +" ) Select " + sel + " from " + alltrim(TabOri) 
 cp_SQL(L_nConn, cmsql, this.w_TMP_GEN )
    * --- Drop temporary table TMPDOCU
    i_nIdx=cp_GetTableDefIdx('TMPDOCU')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDOCU')
    endif
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Drop temporary table TMPVFDOCDETT
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCDETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCDETT')
    endif
    * --- Drop temporary table TMPVFDOCMAST
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCMAST')
    endif
    * --- Drop temporary table TMPVFDOCRATE
    i_nIdx=cp_GetTableDefIdx('TMPVFDOCRATE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFDOCRATE')
    endif
    * --- Drop temporary table TMPVFMOVIMATR
    i_nIdx=cp_GetTableDefIdx('TMPVFMOVIMATR')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFMOVIMATR')
    endif
    * --- Drop temporary table TMPVFMOVILOTT
    i_nIdx=cp_GetTableDefIdx('TMPVFMOVILOTT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFMOVILOTT')
    endif
    * --- Drop temporary table TMPVFABBLOTUL
    i_nIdx=cp_GetTableDefIdx('TMPVFABBLOTUL')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVFABBLOTUL')
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    * --- Drop temporary table TMPLIST
    i_nIdx=cp_GetTableDefIdx('TMPLIST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPLIST')
    endif
  endproc


  proc Init(oParentObject,pFILE,pCODSTR,pTIPOPE)
    this.pFILE=pFILE
    this.pCODSTR=pCODSTR
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,40)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='DOC_RATE'
    this.cWorkTables[7]='ENT_DETT'
    this.cWorkTables[8]='ENT_MAST'
    this.cWorkTables[9]='FLATDETT'
    this.cWorkTables[10]='KEY_ARTI'
    this.cWorkTables[11]='LISTINI'
    this.cWorkTables[12]='LOG_GEND'
    this.cWorkTables[13]='MAGAZZIN'
    this.cWorkTables[14]='MOVIMATR'
    this.cWorkTables[15]='SALDIART'
    this.cWorkTables[16]='TIP_DOCU'
    this.cWorkTables[17]='TIP_DOCU'
    this.cWorkTables[18]='*TMP_ELEMEN'
    this.cWorkTables[19]='*TMPDETTI'
    this.cWorkTables[20]='*TMPDOCU'
    this.cWorkTables[21]='*TMPLIST'
    this.cWorkTables[22]='*TMPMASTI'
    this.cWorkTables[23]='*TMPMATRI'
    this.cWorkTables[24]='*TMPVFABBLOTUL'
    this.cWorkTables[25]='*TMPVFDOCDETT'
    this.cWorkTables[26]='*TMPVFDOCMAST'
    this.cWorkTables[27]='*TMPVFDOCRATE'
    this.cWorkTables[28]='*TMPVFMOVILOTT'
    this.cWorkTables[29]='*TMPVFMOVIMATR'
    this.cWorkTables[30]='TRS_DETT'
    this.cWorkTables[31]='TRS_MAST'
    this.cWorkTables[32]='VADETTFO'
    this.cWorkTables[33]='VAELEMEN'
    this.cWorkTables[34]='VALUTE'
    this.cWorkTables[35]='VAPREDEF'
    this.cWorkTables[36]='VAPREDEF'
    this.cWorkTables[37]='VASTRUTT'
    this.cWorkTables[38]='VATRASCO'
    this.cWorkTables[39]='VOCIIVA'
    this.cWorkTables[40]='XDC_FIELDS'
    return(this.OpenAllTables(40))

  proc CloseCursors()
    if used('_Curs_GSVA2DEL')
      use in _Curs_GSVA2DEL
    endif
    if used('_Curs_TMPDETTI')
      use in _Curs_TMPDETTI
    endif
    if used('_Curs_VAPREDEF')
      use in _Curs_VAPREDEF
    endif
    if used('_Curs_FLATDETT')
      use in _Curs_FLATDETT
    endif
    if used('_Curs_LOG_GEND')
      use in _Curs_LOG_GEND
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFILE,pCODSTR,pTIPOPE"
endproc
