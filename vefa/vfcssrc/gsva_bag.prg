* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bag                                                        *
*              AGGIORNA VALORI TABELLE CLONE                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-30                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pZOOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bag",oParentObject,m.pZOOM)
return(i_retval)

define class tgsva_bag as StdBatch
  * --- Local variables
  pZOOM = .NULL.
  w_obj = .NULL.
  w_PHNAME = space(30)
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_TIPREC = space(1)
  w_MATRICOLA = space(40)
  w_OKAGG = .f.
  w_COD_TMP = space(10)
  w_ABNUMRIF = 0
  w_DFROWORD = 0
  w_DFROWORD = 0
  w_DFROWOR2 = 0
  w_Risultato = space(254)
  w_LOG = space(254)
  * --- WorkFile variables
  TMPVFMOVIMATR_idx=0
  TMPVFDOCMAST_idx=0
  TMPVFDOCDETT_idx=0
  TMPVFDOCRATE_idx=0
  TMPLIST_idx=0
  TMPVFABBLOTUL_idx=0
  TMPVFMOVILOTT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Oggetto zoom
    * --- Oggetto padre
    this.w_obj = oparentobject
    Dimension Arrwrite[1,3]
    this.w_OKAGG = .F.
    this.w_PHNAME = GetETPhname(alltrim(this.w_obj.w_FLATAB), this.w_obj.w_TBNAME)
     
 Select (this.pZOOM.cCURSOR) 
 Scan for XCHK=1
    do case
      case this.w_obj.w_TBNAME="DOC_MAST"
        Dimension ArrVal[1,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=MVSERIAL
        this.w_COD_TMP = "TMPVFDOCMAST"
        this.w_SERIAL = MVSERIAL
        this.w_CPROWNUM = 0
        this.w_TIPREC = "T"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DOC_DETT"
        Dimension ArrVal[2,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=MVSERIAL 
 ArrVal[2,1]="CPROWNUM" 
 ArrVal[2,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFDOCDETT"
        this.w_SERIAL = MVSERIAL
        this.w_CPROWNUM = CPROWNUM
        this.w_TIPREC = "D"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DOC_RATE"
         
 Dimension ArrVal[2,2] 
 ArrVal[1,1]="RSSERIAL" 
 ArrVal[1,2]=RSSERIAL 
 ArrVal[2,1]="RSNUMRAT" 
 ArrVal[2,2]=RSNUMRAT
        this.w_SERIAL = RSSERIAL
        this.w_COD_TMP = "TMPVFDOCRATE"
        this.w_CPROWNUM = RSNUMRAT
        this.w_TIPREC = "R"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="MOVIMATR"
         
 Dimension ArrVal[4,2] 
 ArrVal[1,1]="MLSERIAL" 
 ArrVal[1,2]=MLSERIAL 
 ArrVal[2,1]="MLROWORD" 
 ArrVal[2,2]=MLROWORD 
 ArrVal[3,1]="MTNUMRIF" 
 ArrVal[3,2]=MTNUMRIF 
 ArrVal[4,1]="CPROWNUM" 
 ArrVal[4,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFMOVIMATR"
        this.w_SERIAL = MLSERIAL
        this.w_CPROWNUM = MTROWNUM
        this.w_TIPREC = "L"
        this.w_MATRICOLA = MTCODMAT
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="MOVILOTT"
         
 Dimension ArrVal[4,2] 
 ArrVal[1,1]="MTSERIAL" 
 ArrVal[1,2]=MTSERIAL 
 ArrVal[2,1]="MTROWNUM" 
 ArrVal[2,2]=MTROWNUM 
 ArrVal[3,1]="MTKEYART" 
 ArrVal[3,2]=MTKEYART 
 ArrVal[4,1]="MTCODMAT" 
 ArrVal[4,2]=MTCODMAT
        this.w_COD_TMP = "TMPVFMOVILOTT"
        this.w_SERIAL = MTSERIAL
        this.w_CPROWNUM = MTROWNUM
        this.w_TIPREC = "M"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="ABBLOTUL"
         
 Dimension ArrVal[4,2] 
 ArrVal[1,1]="ABSERIAL" 
 ArrVal[1,2]=ABSERIAL 
 ArrVal[2,1]="ABROWNUM" 
 ArrVal[2,2]=ABROWNUM 
 ArrVal[3,1]="ABNUMRIF" 
 ArrVal[3,2]=ABNUMRIF 
 ArrVal[4,1]="CPROWNUM" 
 ArrVal[4,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFABBLOTUL"
        this.w_SERIAL = ABSERIAL
        this.w_CPROWNUM = ABROWNUM
        this.w_ABNUMRIF = ABNUMRIF
        this.w_TIPREC = "A"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DELFOR_1"
        Dimension ArrVal[1,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=DFSERIAL
        this.w_COD_TMP = "TMPVFDOCMAST"
        this.w_SERIAL = DFSERIAL
        this.w_CPROWNUM = 0
        this.w_TIPREC = "T"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DELFOR_2"
        Dimension ArrVal[2,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=DFSERIAL 
 ArrVal[2,1]="CPROWNUM" 
 ArrVal[2,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFDOCDETT"
        this.w_SERIAL = DFSERIAL
        this.w_CPROWNUM = CPROWNUM
        this.w_TIPREC = "D"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DELFOR_3"
        Dimension ArrVal[3,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=DFSERIAL 
 ArrVal[2,1]="DFROWORD" 
 ArrVal[2,2]=DFROWORD 
 ArrVal[3,1]="CPROWNUM" 
 ArrVal[3,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFDOCRATE"
        this.w_SERIAL = DFSERIAL
        this.w_DFROWORD = DFROWORD
        this.w_CPROWNUM = CPROWNUM
        this.w_TIPREC = "D"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_obj.w_TBNAME="DELFOR_4"
        Dimension ArrVal[4,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=DFSERIAL 
 ArrVal[2,1]="DFROWORD" 
 ArrVal[2,2]=DFROWORD 
 ArrVal[3,1]="DFROWOR2" 
 ArrVal[3,2]=DFROWOR2 
 ArrVal[4,1]="CPROWNUM" 
 ArrVal[4,2]=CPROWNUM
        this.w_COD_TMP = "TMPVFMOVIMATR"
        this.w_SERIAL = DFSERIAL
        this.w_DFROWORD = DFROWORD
        this.w_DFROWOR2 = DFROWOR2
        this.w_CPROWNUM = CPROWNUM
        this.w_TIPREC = "D"
        this.w_MATRICOLA = SPACE(40)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    this.w_OKAGG = .T.
     
 ENDSCAN
    if this.w_OKAGG
       
 CURS=READTABLE(this.w_PHNAME,"*","","",.F.,"FT___UID='"+this.w_obj.w_SER_ELAB+"'")
      do case
        case this.w_obj.w_TBNAME="DOC_MAST" OR this.w_obj.w_TBNAME="DELFOR_1"
          * --- Drop temporary table TMPVFDOCMAST
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCMAST')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCMAST')
          endif
        case this.w_obj.w_TBNAME="DOC_DETT" OR this.w_obj.w_TBNAME="DELFOR_2"
          * --- Drop temporary table TMPVFDOCDETT
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCDETT')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCDETT')
          endif
        case this.w_obj.w_TBNAME="DOC_RATE" OR this.w_obj.w_TBNAME="DELFOR_3"
          * --- Drop temporary table TMPVFDOCRATE
          i_nIdx=cp_GetTableDefIdx('TMPVFDOCRATE')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFDOCRATE')
          endif
        case this.w_obj.w_TBNAME="MOVIMATR" OR this.w_obj.w_TBNAME="DELFOR_4"
          * --- Drop temporary table TMPVFMOVIMATR
          i_nIdx=cp_GetTableDefIdx('TMPVFMOVIMATR')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVFMOVIMATR')
          endif
      endcase
      this.w_Risultato = CurToTab(Curs, this.w_COD_TMP)
       
 use in (curs)
      this.w_obj.Notifyevent("Esegui")     
      ah_ErrorMsg("Aggiornamento terminato con successo", 64,"")
    else
      ah_ErrorMsg("Attenzione nessuna riga selezionata ",,"")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_obj.w_TIPCAM="C"
        if this.oParentObject.w_FLNULL="S"
           
 ArrWrite[1,2]=.NULL. 
 
        else
          ArrWrite[1,2]=SUBSTR(this.w_obj.w_CARACTER,1,this.w_obj.w_LUNG)
        endif
      case this.w_obj.w_TIPCAM="N"
        ArrWrite[1,2]=cp_ROUND(this.w_obj.w_NUMERIC,this.w_obj.w_DECIMAL)
      case this.w_obj.w_TIPCAM="D"
        ArrWrite[1,2]=this.w_obj.w_DATA
    endcase
    ArrWrite[1,1]=this.w_obj.w_campo
    this.w_LOG = WRITETABLE( this.w_PHNAME , @ArrWrite , @ArrVal , " " ) 
    * --- Inserisco eventuale LOG
    if Not empty(this.w_LOG)
      * --- Try
      local bErr_036C6BE0
      bErr_036C6BE0=bTrsErr
      this.Try_036C6BE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_036C6BE0
      * --- End
      * --- Write into TMPLIST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPLIST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPLIST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LOGERROR ="+cp_NullLink(cp_ToStrODBC(this.w_LOG),'TMPLIST','LOGERROR');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and TIPREC = "+cp_ToStrODBC(this.w_TIPREC);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
            +" and MATRICOLA = "+cp_ToStrODBC(this.w_MATRICOLA);
               )
      else
        update (i_cTable) set;
            LOGERROR = this.w_LOG;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIAL;
            and TIPREC = this.w_TIPREC;
            and CPROWNUM = this.w_CPROWNUM;
            and MATRICOLA = this.w_MATRICOLA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_036C6BE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPLIST
    i_nConn=i_TableProp[this.TMPLIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPLIST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",TIPREC"+",CPROWNUM"+",MATRICOLA"+",LOGERROR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPLIST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREC),'TMPLIST','TIPREC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMPLIST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATRICOLA),'TMPLIST','MATRICOLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOG),'TMPLIST','LOGERROR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'TIPREC',this.w_TIPREC,'CPROWNUM',this.w_CPROWNUM,'MATRICOLA',this.w_MATRICOLA,'LOGERROR',this.w_LOG)
      insert into (i_cTable) (MVSERIAL,TIPREC,CPROWNUM,MATRICOLA,LOGERROR &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_TIPREC;
           ,this.w_CPROWNUM;
           ,this.w_MATRICOLA;
           ,this.w_LOG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='ERRORE INSERIMENTO LOG'
      return
    endif
    return


  proc Init(oParentObject,pZOOM)
    this.pZOOM=pZOOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='*TMPVFMOVIMATR'
    this.cWorkTables[2]='*TMPVFDOCMAST'
    this.cWorkTables[3]='*TMPVFDOCDETT'
    this.cWorkTables[4]='*TMPVFDOCRATE'
    this.cWorkTables[5]='TMPLIST'
    this.cWorkTables[6]='*TMPVFABBLOTUL'
    this.cWorkTables[7]='*TMPVFMOVILOTT'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pZOOM"
endproc
