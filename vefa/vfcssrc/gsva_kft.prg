* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kft                                                        *
*              Manutenzione tabelle piatte e tabelle per import EDI            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-27                                                      *
* Last revis.: 2008-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kft",oParentObject))

* --- Class definition
define class tgsva_kft as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 747
  Height = 560+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-04-03"
  HelpContextID=48919145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsva_kft"
  cComment = "Manutenzione tabelle piatte e tabelle per import EDI"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COLTABOK = 0
  w_COLTABAG = 0
  w_COLTABCR = 0
  w_TABELLE = .NULL.
  w_TABELLE_EDI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kftPag1","gsva_kft",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione tabelle piatte")
      .Pages(2).addobject("oPag","tgsva_kftPag2","gsva_kft",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Manutenzione tabelle per import EDI")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TABELLE = this.oPgFrm.Pages(1).oPag.TABELLE
    this.w_TABELLE_EDI = this.oPgFrm.Pages(2).oPag.TABELLE_EDI
    DoDefault()
    proc Destroy()
      this.w_TABELLE = .NULL.
      this.w_TABELLE_EDI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COLTABOK=0
      .w_COLTABAG=0
      .w_COLTABCR=0
        .w_COLTABOK = RGB(0,200,0)
        .w_COLTABAG = RGB(250,250,0)
        .w_COLTABCR = RGB(200,0,0)
      .oPgFrm.Page1.oPag.TABELLE.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate('Tabelle da verificare','0',RGB(255,255,255))
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate('Tabelle da creare','0',.w_COLTABCR)
      .oPgFrm.Page2.oPag.TABELLE_EDI.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_5.Calculate('Tabelle da verificare','0',RGB(255,255,255))
      .oPgFrm.Page2.oPag.oObj_2_6.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
      .oPgFrm.Page2.oPag.oObj_2_7.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
      .oPgFrm.Page2.oPag.oObj_2_8.Calculate('Tabelle da creare','0',.w_COLTABCR)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TABELLE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('Tabelle da verificare','0',RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate('Tabelle da creare','0',.w_COLTABCR)
        .oPgFrm.Page2.oPag.TABELLE_EDI.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate('Tabelle da verificare','0',RGB(255,255,255))
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate('Tabelle da creare','0',.w_COLTABCR)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TABELLE.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('Tabelle da verificare','0',RGB(255,255,255))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate('Tabelle da creare','0',.w_COLTABCR)
        .oPgFrm.Page2.oPag.TABELLE_EDI.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_5.Calculate('Tabelle da verificare','0',RGB(255,255,255))
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate('Tabelle aggiornate','0',.w_COLTABOK)
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate('Tabelle da aggiornare','0',.w_COLTABAG)
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate('Tabelle da creare','0',.w_COLTABCR)
    endwith
  return

  proc Calculate_WYTZTMCYCZ()
    with this
          * --- Verifica tabelle - gsva_bft(verif)
          gsva_bft(this;
              ,'VERIF';
              ,'FT';
             )
          gsva_bft(this;
              ,'VERIF';
              ,'ET';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TABELLE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Verifica")
          .Calculate_WYTZTMCYCZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.TABELLE_EDI.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_5.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_7.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_kftPag1 as StdContainer
  Width  = 743
  height = 560
  stdWidth  = 743
  stdheight = 560
  resizeXpos=740
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="CZRHGAVNYF",left=638, top=502, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 48591018;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"SAVE","FT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="NYQDDFCUPJ",left=688, top=502, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 254103113;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object TABELLE as cp_szoombox with uid="KJZVTWKUEV",left=13, top=6, width=717,height=466,;
    caption='TABELLE',;
   bGlobalFont=.t.,;
    cTable="FLATMAST",cZoomFile="GSVA_KFT",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 236909622


  add object oObj_1_8 as cp_calclbl with uid="WGNXHODJSV",left=169, top=502, width=203,height=19,;
    caption='VERIFICARE',;
   bGlobalFont=.t.,;
    caption="Tabelle da verificare",;
    nPag=1;
    , HelpContextID = 180634295


  add object oObj_1_9 as cp_calclbl with uid="HRWGCYCBDP",left=377, top=502, width=203,height=19,;
    caption='AGGIORNATE',;
   bGlobalFont=.t.,;
    caption="Tabelle aggiornate",;
    nPag=1;
    , HelpContextID = 195849337


  add object oObj_1_10 as cp_calclbl with uid="BVOKLZVWIC",left=377, top=528, width=203,height=19,;
    caption='DA_AGGIORN',;
   bGlobalFont=.t.,;
    caption="Tabelle da aggiornare",;
    nPag=1;
    , HelpContextID = 120777051


  add object oObj_1_11 as cp_calclbl with uid="LSBHTWZNPR",left=169, top=528, width=203,height=19,;
    caption='CREARE',;
   bGlobalFont=.t.,;
    caption="Tabelle da creare",;
    nPag=1;
    , HelpContextID = 142920666


  add object oBtn_1_13 as StdButton with uid="QRLDBUEXQQ",left=8, top=502, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le tabelle";
    , HelpContextID = 48591018;
    , caption='\<Selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"SELEZ","FT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="KHDCGALOWP",left=58, top=502, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le tabelle";
    , HelpContextID = 254103113;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"DESEL","FT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="IPQWAHIUJN",left=108, top=502, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione delle tabelle";
    , HelpContextID = 254103113;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"INVSEL","FT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="RCPIXTUDNA",left=588, top=502, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire nuovamente la verifica";
    , HelpContextID = 48591018;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"VERIF","FT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="SORBJMDEPJ",Visible=.t., Left=160, Top=478,;
    Alignment=0, Width=305, Height=18,;
    Caption="Legenda"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="GAPKCPIAKM",left=162, top=496, width=423,height=57
enddefine
define class tgsva_kftPag2 as StdContainer
  Width  = 743
  height = 560
  stdWidth  = 743
  stdheight = 560
  resizeYpos=343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_1 as StdButton with uid="DMGMZYWFFO",left=638, top=502, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 48591018;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_1.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"SAVE","ET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_2 as StdButton with uid="UCYNTVYTRD",left=688, top=502, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 254103113;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object TABELLE_EDI as cp_szoombox with uid="TQRGLXWSNN",left=13, top=6, width=717,height=466,;
    caption='TABELLE_EDI',;
   bGlobalFont=.t.,;
    cTable="FLATMAST",cZoomFile="GSVAEKFT",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    nPag=2;
    , HelpContextID = 237227237


  add object oObj_2_5 as cp_calclbl with uid="KEJZMQEKNF",left=169, top=502, width=203,height=19,;
    caption='VERIFICARE',;
   bGlobalFont=.t.,;
    caption="Tabelle da verificare",;
    nPag=2;
    , HelpContextID = 180634295


  add object oObj_2_6 as cp_calclbl with uid="KVEOYVJZBK",left=377, top=502, width=203,height=19,;
    caption='AGGIORNATE',;
   bGlobalFont=.t.,;
    caption="Tabelle aggiornate",;
    nPag=2;
    , HelpContextID = 195849337


  add object oObj_2_7 as cp_calclbl with uid="OEBRNOCUFP",left=377, top=528, width=203,height=19,;
    caption='DA_AGGIORN',;
   bGlobalFont=.t.,;
    caption="Tabelle da aggiornare",;
    nPag=2;
    , HelpContextID = 120777051


  add object oObj_2_8 as cp_calclbl with uid="BRSLUTWEOM",left=169, top=528, width=203,height=19,;
    caption='CREARE',;
   bGlobalFont=.t.,;
    caption="Tabelle da creare",;
    nPag=2;
    , HelpContextID = 142920666


  add object oBtn_2_9 as StdButton with uid="HSUQYVIELE",left=8, top=502, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare tutte le tabelle";
    , HelpContextID = 48591018;
    , caption='\<Selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"SELEZ","ET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_10 as StdButton with uid="NEXPIRWZPC",left=58, top=502, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare tutte le tabelle";
    , HelpContextID = 254103113;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"DESEL","ET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="CKSHTWNGUC",left=108, top=502, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione delle tabelle";
    , HelpContextID = 254103113;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"INVSEL","ET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_13 as StdButton with uid="HMQGFLZHWO",left=588, top=502, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire nuovamente la verifica";
    , HelpContextID = 48591018;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSVA_BFT(this.Parent.oContained,"VERIF", "ET")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_4 as StdString with uid="GKXJVYECLO",Visible=.t., Left=160, Top=478,;
    Alignment=0, Width=305, Height=18,;
    Caption="Legenda"  ;
  , bGlobalFont=.t.

  add object oBox_2_12 as StdBox with uid="PPATDMJSUH",left=162, top=496, width=423,height=57
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kft','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
