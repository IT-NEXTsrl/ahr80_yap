* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kwi                                                        *
*              Assistente importazione file EDI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-23                                                      *
* Last revis.: 2015-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kwi",oParentObject))

* --- Class definition
define class tgsva_kwi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 792
  Height = 521+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-23"
  HelpContextID=236293527
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  VAANAIMP_IDX = 0
  VASTRUTT_IDX = 0
  PAR_VEFA_IDX = 0
  cPrg = "gsva_kwi"
  cComment = "Assistente importazione file EDI"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AICODAZI = space(5)
  w_CUR_STEP = 0
  w_TOT_STEP = 0
  w_SER_ELAB = space(10)
  w_FILEINGR = space(254)
  w_STCODICE = space(10)
  w_FLT_ELAB = space(1)
  w_MSG = space(0)
  w_IMSERIAL = space(10)
  w_IMSSTEPA = space(1)
  w_IMSSTEPB = space(1)
  w_IMSSTEPC = space(1)
  w_STDESCRI = space(30)
  w_FILE_XML = space(254)
  w_MSG = space(0)
  w_FLVERBOS = .F.
  w_FLVERBOS = .F.
  w_OBJ__XML = space(1)
  w_IMFLELAB = space(1)
  w_FLVERBOS = .F.
  w_MSG = space(0)
  w_STMODMSK = space(30)
  w_STTIPGES = space(1)
  w_STDRIVER = space(30)
  w_STBACIMP = space(30)
  w_STBACXML = space(30)
  w_STFUNRIC = space(30)
  w_ONLY_VERIF = .F.
  w_FLFREEZE = space(1)
  w_PATHFING = space(254)
  w_GENERR = space(1)
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  w_STEPS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kwiPag1","gsva_kwi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(2).addobject("oPag","tgsva_kwiPag2","gsva_kwi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Pag.2")
      .Pages(3).addobject("oPag","tgsva_kwiPag3","gsva_kwi",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Pag.3")
      .Pages(4).addobject("oPag","tgsva_kwiPag4","gsva_kwi",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pag.4")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSER_ELAB_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_STEPS = this.oPgFrm.Pages(1).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(2).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(3).oPag.STEPS
    this.w_STEPS = this.oPgFrm.Pages(4).oPag.STEPS
    DoDefault()
    proc Destroy()
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      this.w_STEPS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VAANAIMP'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='PAR_VEFA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AICODAZI=space(5)
      .w_CUR_STEP=0
      .w_TOT_STEP=0
      .w_SER_ELAB=space(10)
      .w_FILEINGR=space(254)
      .w_STCODICE=space(10)
      .w_FLT_ELAB=space(1)
      .w_MSG=space(0)
      .w_IMSERIAL=space(10)
      .w_IMSSTEPA=space(1)
      .w_IMSSTEPB=space(1)
      .w_IMSSTEPC=space(1)
      .w_STDESCRI=space(30)
      .w_FILE_XML=space(254)
      .w_MSG=space(0)
      .w_FLVERBOS=.f.
      .w_FLVERBOS=.f.
      .w_OBJ__XML=space(1)
      .w_IMFLELAB=space(1)
      .w_FLVERBOS=.f.
      .w_MSG=space(0)
      .w_STMODMSK=space(30)
      .w_STTIPGES=space(1)
      .w_STDRIVER=space(30)
      .w_STBACIMP=space(30)
      .w_STBACXML=space(30)
      .w_STFUNRIC=space(30)
      .w_ONLY_VERIF=.f.
      .w_FLFREEZE=space(1)
      .w_PATHFING=space(254)
      .w_GENERR=space(1)
        .w_AICODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AICODAZI))
          .link_1_1('Full')
        endif
        .w_CUR_STEP = 1
        .w_TOT_STEP = this.oPGFRM.PageCount
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_SER_ELAB))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_STCODICE))
          .link_1_8('Full')
        endif
      .oPgFrm.Page1.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
      .oPgFrm.Page2.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .w_FLT_ELAB = 'N'
          .DoRTCalc(8,15,.f.)
        .w_FLVERBOS = .F.
        .w_FLVERBOS = .F.
      .oPgFrm.Page3.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
          .DoRTCalc(18,19,.f.)
        .w_FLVERBOS = .F.
      .oPgFrm.Page4.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
          .DoRTCalc(21,27,.f.)
        .w_ONLY_VERIF = .F.
        .w_FLFREEZE = 'N'
          .DoRTCalc(30,30,.f.)
        .w_GENERR = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsva_kwi
    * Nascondo le tab
    if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
    else
       this.oPGFRM.Tabs=.F.
       this.refresh()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_AICODAZI = i_CODAZI
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page2.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page4.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .DoRTCalc(2,28,.t.)
            .w_FLFREEZE = 'N'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(30,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page2.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page3.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
        .oPgFrm.Page4.oPag.STEPS.Calculate('Passo '+ALLTRIM(STR(.w_CUR_STEP))+' di '+ALLTRIM(STR(.w_TOT_STEP)),0)
    endwith
  return

  proc Calculate_UBILKJJPFX()
    with this
          * --- Caricamento elaborazione precedente
          gsva_bwi(this;
              ,'CAREL';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSER_ELAB_1_4.enabled = this.oPgFrm.Page1.oPag.oSER_ELAB_1_4.mCond()
    this.oPgFrm.Page1.oPag.oFILEINGR_1_6.enabled = this.oPgFrm.Page1.oPag.oFILEINGR_1_6.mCond()
    this.oPgFrm.Page1.oPag.oSTCODICE_1_8.enabled = this.oPgFrm.Page1.oPag.oSTCODICE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_3.enabled = this.oPgFrm.Page3.oPag.oBtn_3_3.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_4.enabled = this.oPgFrm.Page3.oPag.oBtn_3_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_3.enabled = this.oPgFrm.Page4.oPag.oBtn_4_3.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_5.enabled = this.oPgFrm.Page4.oPag.oBtn_4_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_8.enabled = this.oPgFrm.Page3.oPag.oBtn_3_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page2.oPag.STEPS.Event(cEvent)
        if lower(cEvent)==lower("w_SER_ELAB Changed")
          .Calculate_UBILKJJPFX()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.STEPS.Event(cEvent)
      .oPgFrm.Page4.oPag.STEPS.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsva_kwi
    If cevent='ApriA'
       =opengest('A','GSVA_AIM','IMSERIAL',this.w_SER_ELAB)
       i_curform.mEnableControls()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AICODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_lTable = "PAR_VEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2], .t., this.PAR_VEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AICODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AICODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PADIRFIL";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_AICODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_AICODAZI)
            select PACODAZI,PADIRFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AICODAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_PATHFING = NVL(_Link_.PADIRFIL,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_AICODAZI = space(5)
      endif
      this.w_PATHFING = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_VEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AICODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SER_ELAB
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAANAIMP_IDX,3]
    i_lTable = "VAANAIMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2], .t., this.VAANAIMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SER_ELAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AIM',True,'VAANAIMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMSERIAL like "+cp_ToStrODBC(trim(this.w_SER_ELAB)+"%");

          i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMSERIAL',trim(this.w_SER_ELAB))
          select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SER_ELAB)==trim(_Link_.IMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SER_ELAB) and !this.bDontReportError
            deferred_cp_zoom('VAANAIMP','*','IMSERIAL',cp_AbsName(oSource.parent,'oSER_ELAB_1_4'),i_cWhere,'GSVA_AIM',"Importazioni",'gsva_kwi.VAANAIMP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML";
                     +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',oSource.xKey(1))
            select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SER_ELAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML";
                   +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(this.w_SER_ELAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',this.w_SER_ELAB)
            select IMSERIAL,IMFLELAB,IMSSTEPA,IMSSTEPB,IMSSTEPC,IMPATORI,IMLSTEPA,IMCODSTR,IMPATXML;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SER_ELAB = NVL(_Link_.IMSERIAL,space(10))
      this.w_IMFLELAB = NVL(_Link_.IMFLELAB,space(1))
      this.w_IMSERIAL = NVL(_Link_.IMSERIAL,space(10))
      this.w_IMSSTEPA = NVL(_Link_.IMSSTEPA,space(1))
      this.w_IMSSTEPB = NVL(_Link_.IMSSTEPB,space(1))
      this.w_IMSSTEPC = NVL(_Link_.IMSSTEPC,space(1))
      this.w_FILEINGR = NVL(_Link_.IMPATORI,space(254))
      this.w_MSG = NVL(_Link_.IMLSTEPA,space(0))
      this.w_STCODICE = NVL(_Link_.IMCODSTR,space(10))
      this.w_FILE_XML = NVL(_Link_.IMPATXML,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SER_ELAB = space(10)
      endif
      this.w_IMFLELAB = space(1)
      this.w_IMSERIAL = space(10)
      this.w_IMSSTEPA = space(1)
      this.w_IMSSTEPB = space(1)
      this.w_IMSSTEPC = space(1)
      this.w_FILEINGR = space(254)
      this.w_MSG = space(0)
      this.w_STCODICE = space(10)
      this.w_FILE_XML = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_IMFLELAB<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Importazione inesistente o con dati gi� elaborati")
        endif
        this.w_SER_ELAB = space(10)
        this.w_IMFLELAB = space(1)
        this.w_IMSERIAL = space(10)
        this.w_IMSSTEPA = space(1)
        this.w_IMSSTEPB = space(1)
        this.w_IMSSTEPC = space(1)
        this.w_FILEINGR = space(254)
        this.w_MSG = space(0)
        this.w_STCODICE = space(10)
        this.w_FILE_XML = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])+'\'+cp_ToStr(_Link_.IMSERIAL,1)
      cp_ShowWarn(i_cKey,this.VAANAIMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SER_ELAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODICE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_STCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_STCODICE))
          select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODICE)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODICE) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oSTCODICE_1_8'),i_cWhere,'GSVA_AST',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_STCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_STCODICE)
            select STCODICE,STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODICE = NVL(_Link_.STCODICE,space(10))
      this.w_STDESCRI = NVL(_Link_.STDESCRI,space(30))
      this.w_STMODMSK = NVL(_Link_.STMODMSK,space(30))
      this.w_STTIPGES = NVL(_Link_.STTIPGES,space(1))
      this.w_STBACIMP = NVL(_Link_.STBACIMP,space(30))
      this.w_STDRIVER = NVL(_Link_.STDRIVER,space(30))
      this.w_STBACXML = NVL(_Link_.STBACXML,space(30))
      this.w_STFUNRIC = NVL(_Link_.STFUNRIC,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_STCODICE = space(10)
      endif
      this.w_STDESCRI = space(30)
      this.w_STMODMSK = space(30)
      this.w_STTIPGES = space(1)
      this.w_STBACIMP = space(30)
      this.w_STDRIVER = space(30)
      this.w_STBACXML = space(30)
      this.w_STFUNRIC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=SearchPrin(.w_STCODICE,'DOC_MAST')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Struttura EDI incongruente o inesistente!")
        endif
        this.w_STCODICE = space(10)
        this.w_STDESCRI = space(30)
        this.w_STMODMSK = space(30)
        this.w_STTIPGES = space(1)
        this.w_STBACIMP = space(30)
        this.w_STDRIVER = space(30)
        this.w_STBACXML = space(30)
        this.w_STFUNRIC = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSER_ELAB_1_4.value==this.w_SER_ELAB)
      this.oPgFrm.Page1.oPag.oSER_ELAB_1_4.value=this.w_SER_ELAB
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEINGR_1_6.value==this.w_FILEINGR)
      this.oPgFrm.Page1.oPag.oFILEINGR_1_6.value=this.w_FILEINGR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODICE_1_8.value==this.w_STCODICE)
      this.oPgFrm.Page1.oPag.oSTCODICE_1_8.value=this.w_STCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMSG_1_17.value==this.w_MSG)
      this.oPgFrm.Page1.oPag.oMSG_1_17.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_23.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_23.value=this.w_STDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFILE_XML_2_6.value==this.w_FILE_XML)
      this.oPgFrm.Page2.oPag.oFILE_XML_2_6.value=this.w_FILE_XML
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_9.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_9.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVERBOS_1_24.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page1.oPag.oFLVERBOS_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVERBOS_2_10.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page2.oPag.oFLVERBOS_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFLVERBOS_3_6.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page3.oPag.oFLVERBOS_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMSG_3_7.value==this.w_MSG)
      this.oPgFrm.Page3.oPag.oMSG_3_7.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_IMFLELAB<>'S')  and (!EMPTY(.w_SER_ELAB) OR (EMPTY(.w_SER_ELAB) AND EMPTY(.w_FILEINGR)))  and not(empty(.w_SER_ELAB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSER_ELAB_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Importazione inesistente o con dati gi� elaborati")
          case   not(SearchPrin(.w_STCODICE,'DOC_MAST'))  and (EMPTY(.w_SER_ELAB))  and not(empty(.w_STCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTCODICE_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Struttura EDI incongruente o inesistente!")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_kwiPag1 as StdContainer
  Width  = 788
  height = 521
  stdWidth  = 788
  stdheight = 521
  resizeXpos=332
  resizeYpos=250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSER_ELAB_1_4 as StdField with uid="MXSAOHRZCC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SER_ELAB", cQueryName = "SER_ELAB",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Importazione inesistente o con dati gi� elaborati",;
    ToolTipText = "Permette di selezionare una precedente elaborazione eseguita con errori",;
    HelpContextID = 248117352,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=154, Top=65, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAANAIMP", cZoomOnZoom="GSVA_AIM", oKey_1_1="IMSERIAL", oKey_1_2="this.w_SER_ELAB"

  func oSER_ELAB_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_SER_ELAB) OR (EMPTY(.w_SER_ELAB) AND EMPTY(.w_FILEINGR)))
    endwith
   endif
  endfunc

  func oSER_ELAB_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSER_ELAB_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSER_ELAB_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VAANAIMP','*','IMSERIAL',cp_AbsName(this.parent,'oSER_ELAB_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AIM',"Importazioni",'gsva_kwi.VAANAIMP_VZM',this.parent.oContained
  endproc
  proc oSER_ELAB_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMSERIAL=this.parent.oContained.w_SER_ELAB
     i_obj.ecpSave()
  endproc


  add object oBtn_1_5 as StdButton with uid="XFZTSGQDIZ",left=654, top=69, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare il file da processare";
    , HelpContextID = 99728902;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"SELFI", "FILEINGR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (EMPTY(.w_SER_ELAB))
      endwith
    endif
  endfunc

  add object oFILEINGR_1_6 as StdField with uid="VSBRFYMOZB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FILEINGR", cQueryName = "FILEINGR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Specificare il file completo di percorso da importare",;
    HelpContextID = 252732504,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=154, Top=92, InputMask=replicate('X',254)

  func oFILEINGR_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SER_ELAB))
    endwith
   endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="FGZXLVJSGL",left=707, top=69, width=48,height=45,;
    CpPicture="BMP\CODICI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file attualmente selezionato con l'applicazione predefinita";
    , HelpContextID = 99728902;
    , caption='A\<pri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        ViewFile(.w_FILEINGR)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_FILEINGR))
      endwith
    endif
  endfunc

  add object oSTCODICE_1_8 as StdField with uid="HIDMTICKXE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STCODICE", cQueryName = "STCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Struttura EDI incongruente o inesistente!",;
    ToolTipText = "Codice della struttura con cui elaborare il file",;
    HelpContextID = 72804501,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=154, Top=122, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_STCODICE"

  func oSTCODICE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_SER_ELAB))
    endwith
   endif
  endfunc

  func oSTCODICE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODICE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODICE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oSTCODICE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Strutture",'',this.parent.oContained
  endproc
  proc oSTCODICE_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_STCODICE
     i_obj.ecpSave()
  endproc


  add object STEPS as cp_calclbl with uid="UBYOMHFWGE",left=16, top=15, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=1;
    , HelpContextID = 60438310


  add object oBtn_1_12 as StdButton with uid="PHCFUELQHM",left=601, top=472, width=48,height=45,;
    CpPicture="BMP\LEFT.BMP", caption="", nPag=1;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 120241931;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"BACK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="FRSHLUKCSC",left=654, top=472, width=48,height=45,;
    CpPicture="BMP\RIGHT.BMP", caption="", nPag=1;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 248125446;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP AND !EMPTY(.w_FILEINGR) AND !EMPTY(.w_STCODICE))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="YEDHIQOSNF",left=707, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 246292474;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMSG_1_17 as StdMemo with uid="NJTSHEBGTR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 236606918,;
   bGlobalFont=.t.,;
    Height=315, Width=749, Left=6, Top=150, readonly=.T.

  add object oSTDESCRI_1_23 as StdField with uid="ZYDHGMVOKP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 110045039,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=256, Top=122, InputMask=replicate('X',30)

  add object oFLVERBOS_1_24 as StdCheck with uid="GQRLSKZSCN",rtseq=16,rtrep=.f.,left=6, top=472, caption="Produci log dettagliato",;
    HelpContextID = 92290729,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLVERBOS_1_24.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLVERBOS_1_24.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_1_24.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc


  add object oBtn_1_29 as StdButton with uid="KYBVAQZECK",left=548, top=472, width=48,height=45,;
    CpPicture="BMP\treeview.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il file XML intermedio con l'applicazione predefinita";
    , HelpContextID = 99728902;
    , caption='Apri \<XML';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        ViewFile(.w_FILE_XML)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_IMSSTEPA<>'E' OR EMPTY(.w_FILE_XML))
     endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="VFYGHISYSX",Visible=.t., Left=16, Top=38,;
    Alignment=0, Width=716, Height=19,;
    Caption="Selezionare file da importare oppure una precedente elaborazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="ETFSPQOCFA",Visible=.t., Left=12, Top=95,;
    Alignment=1, Width=139, Height=18,;
    Caption="File d'ingresso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FHSZURWUVF",Visible=.t., Left=12, Top=66,;
    Alignment=1, Width=139, Height=18,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BNINAJUPXZ",Visible=.t., Left=12, Top=124,;
    Alignment=1, Width=139, Height=18,;
    Caption="Codice struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="AMWRBWXQGV",Visible=.t., Left=373, Top=555,;
    Alignment=0, Width=596, Height=19,;
    Caption="La maschera � pi� corta perch� i tab vengono nascosti runtime"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine
define class tgsva_kwiPag2 as StdContainer
  Width  = 788
  height = 521
  stdWidth  = 788
  stdheight = 521
  resizeXpos=260
  resizeYpos=359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object STEPS as cp_calclbl with uid="LCVMQAMVXB",left=16, top=15, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=2;
    , HelpContextID = 60438310


  add object oBtn_2_3 as StdButton with uid="SSAJGWAOGG",left=654, top=472, width=48,height=45,;
    CpPicture="BMP\RIGHT.BMP", caption="", nPag=2;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 248125446;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP AND !EMPTY(.w_FILE_XML) AND .w_IMSSTEPA='O')
      endwith
    endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="KHOWLAZJQB",left=707, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 246292474;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_5 as StdButton with uid="XGQLTWWQKO",left=601, top=472, width=48,height=45,;
    CpPicture="BMP\LEFT.BMP", caption="", nPag=2;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 120241931;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"BACK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc

  add object oFILE_XML_2_6 as StdField with uid="ZGYHQNBYQP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FILE_XML", cQueryName = "FILE_XML",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indica il percorso e il nome del file XML intermedio",;
    HelpContextID = 206543778,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=127, Top=92, InputMask=replicate('X',254), readonly=.t.


  add object oBtn_2_8 as StdButton with uid="IJHHVTXGBL",left=707, top=69, width=48,height=45,;
    CpPicture="BMP\CODICI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aprire il file XML intermedio con l'applicazione predefinita";
    , HelpContextID = 99728902;
    , caption='A\<pri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      with this.Parent.oContained
        ViewFile(.w_FILE_XML)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_FILE_XML))
      endwith
    endif
  endfunc

  add object oMSG_2_9 as StdMemo with uid="KWMIAMXDOU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 236606918,;
   bGlobalFont=.t.,;
    Height=315, Width=749, Left=6, Top=150, readonly=.T.

  add object oFLVERBOS_2_10 as StdCheck with uid="UWTAUVGAAG",rtseq=17,rtrep=.f.,left=6, top=472, caption="Produci log dettagliato",;
    HelpContextID = 92290729,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLVERBOS_2_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLVERBOS_2_10.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_2_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc

  add object oStr_2_2 as StdString with uid="DISRYEIMZW",Visible=.t., Left=16, Top=38,;
    Alignment=0, Width=716, Height=19,;
    Caption="Visualizzazione file XML intermedio e inserimento dati in tabelle EDI"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="ZBRETLLANX",Visible=.t., Left=21, Top=95,;
    Alignment=1, Width=103, Height=18,;
    Caption="File XML:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsva_kwiPag3 as StdContainer
  Width  = 788
  height = 521
  stdWidth  = 788
  stdheight = 521
  resizeXpos=419
  resizeYpos=320
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object STEPS as cp_calclbl with uid="FWMJPRLSBC",left=16, top=15, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=3;
    , HelpContextID = 60438310


  add object oBtn_3_3 as StdButton with uid="JYNNRNAZBB",left=601, top=472, width=48,height=45,;
    CpPicture="BMP\LEFT.BMP", caption="", nPag=3;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 120241931;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_3.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"BACK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_3_4 as StdButton with uid="QOOFQUYCIH",left=654, top=472, width=48,height=45,;
    CpPicture="BMP\RIGHT.BMP", caption="", nPag=3;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 248125446;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_3_5 as StdButton with uid="CUIBFINHPP",left=707, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 246292474;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLVERBOS_3_6 as StdCheck with uid="DZTDXSGBKR",rtseq=20,rtrep=.f.,left=6, top=472, caption="Produci log dettagliato",;
    HelpContextID = 92290729,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLVERBOS_3_6.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLVERBOS_3_6.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_3_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc

  add object oMSG_3_7 as StdMemo with uid="YJBWUBBXQH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 236606918,;
   bGlobalFont=.t.,;
    Height=315, Width=749, Left=6, Top=150, readonly=.T.


  add object oBtn_3_8 as StdButton with uid="QDSUAJJHMQ",left=16, top=85, width=48,height=45,;
    CpPicture="BMP\Modifica.bmp", caption="", nPag=3;
    , ToolTipText = "Permette di accedere alla gestione per la manutenzione dei dati dell'attuale elaborazione presenti nelle tabelle EDI";
    , HelpContextID = 35306969;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_8.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"MANUT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_STMODMSK))
      endwith
    endif
  endfunc

  add object oStr_3_2 as StdString with uid="HHEEWUBTRO",Visible=.t., Left=16, Top=38,;
    Alignment=0, Width=737, Height=19,;
    Caption="Manutenzione dati in tabelle EDI, verifiche pre inserimento dati e inserimento in tabelle finali"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_9 as StdString with uid="XSCTRMMZHS",Visible=.t., Left=2, Top=61,;
    Alignment=0, Width=783, Height=18,;
    Caption="Tramite il bottone sottostante � possibile accedere alla gestione per la manutenzione e verifica dei dati presenti nelle tabelle EDI."  ;
  , bGlobalFont=.t.
enddefine
define class tgsva_kwiPag4 as StdContainer
  Width  = 788
  height = 521
  stdWidth  = 788
  stdheight = 521
  resizeXpos=528
  resizeYpos=261
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object STEPS as cp_calclbl with uid="FDVPHVUMYH",left=16, top=15, width=372,height=19,;
    caption='STEPS',;
   bGlobalFont=.t.,;
    caption="Passo 1 di 1",;
    nPag=4;
    , HelpContextID = 60438310


  add object oBtn_4_3 as StdButton with uid="TDSHFBOINX",left=654, top=472, width=48,height=45,;
    CpPicture="BMP\docdest.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per aprire anagrafica importazioni";
    , HelpContextID = 54493265;
    , Caption='\<Ana. imp.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_3.Click()
      this.parent.oContained.NotifyEvent("ApriA")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_4_4 as StdButton with uid="THHZEGLHRI",left=707, top=472, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 51818486;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_5 as StdButton with uid="EXGPNWIDER",left=601, top=472, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per importare un nuovo file o per selezionare una precedente importazione errata";
    , HelpContextID = 92469206;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      with this.Parent.oContained
        GSVA_BWI(this.Parent.oContained,"NUOVO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP=.w_TOT_STEP)
      endwith
    endif
  endfunc

  add object oStr_4_2 as StdString with uid="XIQNQXQSUF",Visible=.t., Left=16, Top=38,;
    Alignment=0, Width=737, Height=19,;
    Caption="La procedura assistita d'importazione dati EDI � terminata correttamente!"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kwi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
