* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bgf                                                        *
*              GENERAZIONE FILE EDI DA PRINT SYSTEM                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-26                                                      *
* Last revis.: 2008-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOBJ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bgf",oParentObject,m.pOBJ)
return(i_retval)

define class tgsva_bgf as StdBatch
  * --- Local variables
  pOBJ = .NULL.
  w_PADRE = .NULL.
  w_LCODSTR = space(10)
  w_NAZION = space(3)
  w_CODSTR = space(10)
  w_OK = .f.
  * --- WorkFile variables
  STR_INTE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_VEFA="S" 
      * --- Creazione FIle EDI
      this.w_PADRE = this.pOBJ.Oparentobject
      this.w_OK = .T.
       
 DECLARE ARKEY(1,3) 
 ARKEY[1,3]="DOC_MAST" 
 ARKEY[1,1]=this.w_PADRE.w_MVSERIAL 
 ARKEY[1,2]="MVSERIAL"
      if this.w_PADRE.w_MVFLSEND="S"
        this.w_OK = Ah_yesno("Un file EDI risulta gi� generato. Procedere ugualmente?")
      endif
      if this.w_OK
        if Not empty(this.w_PADRE.w_MVCODCON)
          * --- Se impostata applico struttura implementata sull'intestatario
          * --- Read from STR_INTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STR_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STR_INTE_idx,2],.t.,this.STR_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SICODSTR"+;
              " from "+i_cTable+" STR_INTE where ";
                  +"SITIPCON = "+cp_ToStrODBC(this.w_PADRE.w_MVTIPCON);
                  +" and SICODCON = "+cp_ToStrODBC(this.w_PADRE.w_MVCODCON);
                  +" and SITIPDOC = "+cp_ToStrODBC(this.w_PADRE.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SICODSTR;
              from (i_cTable) where;
                  SITIPCON = this.w_PADRE.w_MVTIPCON;
                  and SICODCON = this.w_PADRE.w_MVCODCON;
                  and SITIPDOC = this.w_PADRE.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LCODSTR = NVL(cp_ToDate(_read_.SICODSTR),cp_NullValue(_read_.SICODSTR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CODSTR = IIF(NOT EMPTY(this.w_LCODSTR),this.w_LCODSTR,this.w_padre.w_CODSTR)
        endif
        if Not empty(this.w_CODSTR)
          gsar_bee(this,this.w_CODSTR,this.w_PADRE.w_MVCODCON,this.w_PADRE.w_MVTIPCON,"", @ARKEY,0,"S","")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !bTrsErr
            * --- Aggiorno flag generazione file edi
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVFLSEND ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSEND');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_PADRE.w_MVSERIAL);
                     )
            else
              update (i_cTable) set;
                  MVFLSEND = "S";
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_PADRE.w_MVSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          ah_errormsg("Attenzione! Struttura EDI non presente")
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pOBJ)
    this.pOBJ=pOBJ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STR_INTE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOBJ"
endproc
