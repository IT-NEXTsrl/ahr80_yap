* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kgd                                                        *
*              Export documenti EDI                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-06                                                      *
* Last revis.: 2015-08-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kgd",oParentObject))

* --- Class definition
define class tgsva_kgd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 697
  Height = 569
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-08-24"
  HelpContextID=32141929
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  GRU_INTE_IDX = 0
  cPrg = "gsva_kgd"
  cComment = "Export documenti EDI"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ACQU = space(1)
  w_TIPDOC = space(5)
  o_TIPDOC = space(5)
  w_CATDO2 = space(2)
  o_CATDO2 = space(2)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_OBTEST = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_NUMINI_R = 0
  o_NUMINI_R = 0
  w_NUMFIN_R = 0
  o_NUMFIN_R = 0
  w_TIPOCF = space(1)
  w_ALFDOC = space(10)
  w_CODCON = space(15)
  w_STRUTT = space(10)
  w_FLSEND1 = space(10)
  o_FLSEND1 = space(10)
  w_GRUPPO = space(10)
  w_FLFILE = space(10)
  o_FLFILE = space(10)
  w_TIPSTR = space(1)
  w_CODSTR = space(10)
  w_DESSTR = space(30)
  w_DESDOC = space(35)
  w_CODDES = space(35)
  w_DATOBSO = ctod('  /  /  ')
  w_CATDOC = space(2)
  w_FLVEAC1 = space(1)
  w_FLSEND = space(1)
  w_TIPDIS = space(1)
  w_DESGRU = space(35)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_NUMINI_E = 0
  o_NUMINI_E = 0
  w_NUMFIN_E = 0
  o_NUMFIN_E = 0
  w_Zoomdoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kgdPag1","gsva_kgd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPDOC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomdoc = this.oPgFrm.Pages(1).oPag.Zoomdoc
    DoDefault()
    proc Destroy()
      this.w_Zoomdoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='GRU_INTE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsva_bgd(this,"ESPOR")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ACQU=space(1)
      .w_TIPDOC=space(5)
      .w_CATDO2=space(2)
      .w_CATDO1=space(2)
      .w_OBTEST=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_FLVEAC=space(1)
      .w_NUMINI_R=0
      .w_NUMFIN_R=0
      .w_TIPOCF=space(1)
      .w_ALFDOC=space(10)
      .w_CODCON=space(15)
      .w_STRUTT=space(10)
      .w_FLSEND1=space(10)
      .w_GRUPPO=space(10)
      .w_FLFILE=space(10)
      .w_TIPSTR=space(1)
      .w_CODSTR=space(10)
      .w_DESSTR=space(30)
      .w_DESDOC=space(35)
      .w_CODDES=space(35)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CATDOC=space(2)
      .w_FLVEAC1=space(1)
      .w_FLSEND=space(1)
      .w_TIPDIS=space(1)
      .w_DESGRU=space(35)
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_NUMINI_E=0
      .w_NUMFIN_E=0
        .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_2('Full')
        endif
        .w_CATDO2 = 'XX'
        .w_CATDO1 = 'XX'
        .w_OBTEST = i_DATSYS
        .w_DOCINI = g_INIESE
        .w_DOCFIN = g_FINESE
        .w_FLVEAC = 'T'
          .DoRTCalc(9,10,.f.)
        .w_TIPOCF = 'C'
          .DoRTCalc(12,12,.f.)
        .w_CODCON = IIF(NVL(.w_TIPOCF,'N') $ 'CF',nvl(.w_CODCON,''),'')
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODCON))
          .link_1_13('Full')
        endif
        .w_STRUTT = iif(Not Empty(.w_TIPDOC),Space(10),.w_STRUTT)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_STRUTT))
          .link_1_14('Full')
        endif
        .w_FLSEND1 = 'S'
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_GRUPPO))
          .link_1_16('Full')
        endif
      .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
          .DoRTCalc(17,17,.f.)
        .w_TIPSTR = 'S'
        .w_CODSTR = Space(10)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODSTR))
          .link_1_21('Full')
        endif
          .DoRTCalc(20,23,.f.)
        .w_CATDOC = IIF(.w_ACQU='S', IIF(.w_CATDO1='XX','  ',.w_CATDO1), ICASE(.w_CATDO2='IF', 'DI', .w_CATDO2='DF', 'DT',  .w_CATDO2='XX','  ',.w_CATDO2))
        .w_FLVEAC1 = IIF(.w_FLVEAC='T',' ',.w_FLVEAC)
        .w_FLSEND = IIF(.w_FLSEND1='S','N',' ')
        .w_TIPDIS = 'N'
          .DoRTCalc(28,28,.f.)
        .w_NUMINI = iif(Isahe(),.w_NUMINI_E,.w_NUMINI_R)
        .w_NUMFIN = iif(Isahe(),.w_NUMFIN_E,.w_NUMFIN_R)
    endwith
    this.DoRTCalc(31,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .DoRTCalc(2,4,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(6,12,.t.)
        if .o_TIPDOC<>.w_TIPDOC
            .w_CODCON = IIF(NVL(.w_TIPOCF,'N') $ 'CF',nvl(.w_CODCON,''),'')
          .link_1_13('Full')
        endif
        if .o_FLFILE<>.w_FLFILE.or. .o_TIPDOC<>.w_TIPDOC
            .w_STRUTT = iif(Not Empty(.w_TIPDOC),Space(10),.w_STRUTT)
          .link_1_14('Full')
        endif
        .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
        .DoRTCalc(15,18,.t.)
        if .o_FLFILE<>.w_FLFILE
            .w_CODSTR = Space(10)
          .link_1_21('Full')
        endif
        .DoRTCalc(20,23,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2
            .w_CATDOC = IIF(.w_ACQU='S', IIF(.w_CATDO1='XX','  ',.w_CATDO1), ICASE(.w_CATDO2='IF', 'DI', .w_CATDO2='DF', 'DT',  .w_CATDO2='XX','  ',.w_CATDO2))
        endif
        if .o_FLVEAC<>.w_FLVEAC
            .w_FLVEAC1 = IIF(.w_FLVEAC='T',' ',.w_FLVEAC)
        endif
        if .o_FLSEND1<>.w_FLSEND1
            .w_FLSEND = IIF(.w_FLSEND1='S','N',' ')
        endif
            .w_TIPDIS = 'N'
        .DoRTCalc(28,28,.t.)
        if .o_NUMINI_E<>.w_NUMINI_E.or. .o_NUMINI_R<>.w_NUMINI_R
            .w_NUMINI = iif(Isahe(),.w_NUMINI_E,.w_NUMINI_R)
        endif
        if .o_NUMFIN_E<>.w_NUMFIN_E.or. .o_NUMFIN_R<>.w_NUMFIN_R
            .w_NUMFIN = iif(Isahe(),.w_NUMFIN_E,.w_NUMFIN_R)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoomdoc.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATDO1_1_4.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_13.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_13.mCond()
    this.oPgFrm.Page1.oPag.oSTRUTT_1_14.enabled = this.oPgFrm.Page1.oPag.oSTRUTT_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATDO1_1_4.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oNUMINI_R_1_9.visible=!this.oPgFrm.Page1.oPag.oNUMINI_R_1_9.mHide()
    this.oPgFrm.Page1.oPag.oNUMFIN_R_1_10.visible=!this.oPgFrm.Page1.oPag.oNUMFIN_R_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTIPSTR_1_20.visible=!this.oPgFrm.Page1.oPag.oTIPSTR_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCODSTR_1_21.visible=!this.oPgFrm.Page1.oPag.oCODSTR_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oNUMINI_E_1_51.visible=!this.oPgFrm.Page1.oPag.oNUMINI_E_1_51.mHide()
    this.oPgFrm.Page1.oPag.oNUMFIN_E_1_52.visible=!this.oPgFrm.Page1.oPag.oNUMFIN_E_1_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoomdoc.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_2'),i_cWhere,'GSVE_ATD',"Tipi documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCF;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_13'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCF;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_CODDES = NVL(_Link_.ANDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_CODDES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_CODDES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTT
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_STRUTT)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_STRUTT))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTT)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStrODBC(trim(this.w_STRUTT)+"%");

            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStr(trim(this.w_STRUTT)+"%");

            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STRUTT) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oSTRUTT_1_14'),i_cWhere,'GSVA_AST',"Elenco strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_STRUTT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_STRUTT)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTT = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTT = space(10)
      endif
      this.w_DESSTR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=SearchPrin(.w_STRUTT,'DOC_MAST')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Struttura EDI incongruente o inesistente!")
        endif
        this.w_STRUTT = space(10)
        this.w_DESSTR = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPPO
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_lTable = "GRU_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2], .t., this.GRU_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGR',True,'GRU_INTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_GRUPPO))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GRDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GRDESCRI like "+cp_ToStr(trim(this.w_GRUPPO)+"%");

            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('GRU_INTE','*','GRCODICE',cp_AbsName(oSource.parent,'oGRUPPO_1_16'),i_cWhere,'GSAR_AGR',"Gruppi intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_GRUPPO)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.GRCODICE,space(10))
      this.w_DESGRU = NVL(_Link_.GRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(10)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSTR
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CODSTR))
          select STCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCODSTR_1_21'),i_cWhere,'GSVA_AST',"Elenco strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CODSTR)
            select STCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSTR = NVL(_Link_.STCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODSTR = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=SearchPrin(.w_CODSTR,'DOC_MAST')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Struttura EDI incongruente o inesistente!")
        endif
        this.w_CODSTR = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_2.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_2.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_3.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_4.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_6.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_6.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_7.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_7.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_8.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_R_1_9.value==this.w_NUMINI_R)
      this.oPgFrm.Page1.oPag.oNUMINI_R_1_9.value=this.w_NUMINI_R
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_R_1_10.value==this.w_NUMFIN_R)
      this.oPgFrm.Page1.oPag.oNUMFIN_R_1_10.value=this.w_NUMFIN_R
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCF_1_11.RadioValue()==this.w_TIPOCF)
      this.oPgFrm.Page1.oPag.oTIPOCF_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOC_1_12.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oALFDOC_1_12.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_13.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_13.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTT_1_14.value==this.w_STRUTT)
      this.oPgFrm.Page1.oPag.oSTRUTT_1_14.value=this.w_STRUTT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSEND1_1_15.RadioValue()==this.w_FLSEND1)
      this.oPgFrm.Page1.oPag.oFLSEND1_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_16.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_16.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLFILE_1_19.RadioValue()==this.w_FLFILE)
      this.oPgFrm.Page1.oPag.oFLFILE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTR_1_20.RadioValue()==this.w_TIPSTR)
      this.oPgFrm.Page1.oPag.oTIPSTR_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSTR_1_21.value==this.w_CODSTR)
      this.oPgFrm.Page1.oPag.oCODSTR_1_21.value=this.w_CODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_24.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_24.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_26.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_26.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODDES_1_31.value==this.w_CODDES)
      this.oPgFrm.Page1.oPag.oCODDES_1_31.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_46.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_46.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_E_1_51.value==this.w_NUMINI_E)
      this.oPgFrm.Page1.oPag.oNUMINI_E_1_51.value=this.w_NUMINI_E
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_E_1_52.value==this.w_NUMFIN_E)
      this.oPgFrm.Page1.oPag.oNUMFIN_E_1_52.value=this.w_NUMFIN_E
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (NVL(.w_TIPOCF,'N') $ 'CF')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(SearchPrin(.w_STRUTT,'DOC_MAST'))  and (Empty(.w_TIPDOC))  and not(empty(.w_STRUTT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTRUTT_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Struttura EDI incongruente o inesistente!")
          case   not(SearchPrin(.w_CODSTR,'DOC_MAST'))  and not(.w_TIPSTR<>'S' or .w_FLFILE<>'S')  and not(empty(.w_CODSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSTR_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Struttura EDI incongruente o inesistente!")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPDOC = this.w_TIPDOC
    this.o_CATDO2 = this.w_CATDO2
    this.o_CATDO1 = this.w_CATDO1
    this.o_FLVEAC = this.w_FLVEAC
    this.o_NUMINI_R = this.w_NUMINI_R
    this.o_NUMFIN_R = this.w_NUMFIN_R
    this.o_FLSEND1 = this.w_FLSEND1
    this.o_FLFILE = this.w_FLFILE
    this.o_NUMINI_E = this.w_NUMINI_E
    this.o_NUMFIN_E = this.w_NUMFIN_E
    return

enddefine

* --- Define pages as container
define class tgsva_kgdPag1 as StdContainer
  Width  = 693
  height = 569
  stdWidth  = 693
  stdheight = 569
  resizeXpos=281
  resizeYpos=221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPDOC_1_2 as StdField with uid="SOMWIURCTL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Codice tipo documento di selezione (spazio = no selezione)",;
    HelpContextID = 162603978,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=121, Top=10, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'',this.parent.oContained
  endproc
  proc oTIPDOC_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc


  add object oCATDO2_1_3 as StdCombo with uid="UPJNZIKQML",rtseq=3,rtrep=.f.,left=545,top=10,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 179367130;
    , cFormVar="w_CATDO2",RowSource=""+"Tutti i documenti,"+"Ordini,"+"Doc. di trasporto,"+"Documenti interni,"+"Fatture,"+"Note di credito,"+"DDT a fornitore,"+"Carichi a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_3.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'DI',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'DF',;
    iif(this.value =8,'IF',;
    space(2))))))))))
  endfunc
  func oCATDO2_1_3.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_3.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='OR',2,;
      iif(this.Parent.oContained.w_CATDO2=='DT',3,;
      iif(this.Parent.oContained.w_CATDO2=='DI',4,;
      iif(this.Parent.oContained.w_CATDO2=='FA',5,;
      iif(this.Parent.oContained.w_CATDO2=='NC',6,;
      iif(this.Parent.oContained.w_CATDO2=='DF',7,;
      iif(this.Parent.oContained.w_CATDO2=='IF',8,;
      0))))))))
  endfunc

  func oCATDO2_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQU<>'S')
    endwith
   endif
  endfunc

  func oCATDO2_1_3.mHide()
    with this.Parent.oContained
      return (.w_ACQU='S')
    endwith
  endfunc


  add object oCATDO1_1_4 as StdCombo with uid="QDJUHAEQBR",rtseq=4,rtrep=.f.,left=545,top=10,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 196144346;
    , cFormVar="w_CATDO1",RowSource=""+"Tutti i documenti,"+"Ordini,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DI',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    space(2))))))))
  endfunc
  func oCATDO1_1_4.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_4.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='OR',2,;
      iif(this.Parent.oContained.w_CATDO1=='DI',3,;
      iif(this.Parent.oContained.w_CATDO1=='DT',4,;
      iif(this.Parent.oContained.w_CATDO1=='FA',5,;
      iif(this.Parent.oContained.w_CATDO1=='NC',6,;
      0))))))
  endfunc

  func oCATDO1_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_4.mHide()
    with this.Parent.oContained
      return (.w_ACQU<>'S')
    endwith
  endfunc

  add object oDOCINI_1_6 as StdField with uid="ZGFEWOOULR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 62713546,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=121, Top=36

  func oDOCINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_7 as StdField with uid="MSJYJXJTGQ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 15733046,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=351, Top=38

  func oDOCFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc


  add object oFLVEAC_1_8 as StdCombo with uid="CYOVMZWVVQ",rtseq=8,rtrep=.f.,left=545,top=38,width=130,height=21;
    , ToolTipText = "Tipo documento";
    , HelpContextID = 177193386;
    , cFormVar="w_FLVEAC",RowSource=""+"Acquisti,"+"Vendite,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLVEAC_1_8.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_8.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='A',1,;
      iif(this.Parent.oContained.w_FLVEAC=='V',2,;
      iif(this.Parent.oContained.w_FLVEAC=='T',3,;
      0)))
  endfunc

  add object oNUMINI_R_1_9 as StdField with uid="MUNXKFVXTI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NUMINI_R", cQueryName = "NUMINI_R",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento iniziale",;
    HelpContextID = 62670808,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=121, Top=63, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMINI_R_1_9.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oNUMFIN_R_1_10 as StdField with uid="LTEGZCEXHB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMFIN_R", cQueryName = "NUMFIN_R",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento finale",;
    HelpContextID = 252659672,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=351, Top=63, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMFIN_R_1_10.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc


  add object oTIPOCF_1_11 as StdCombo with uid="AVRBJRPXEK",value=3,rtseq=11,rtrep=.f.,left=121,top=89,width=130,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 124134346;
    , cFormVar="w_TIPOCF",RowSource=""+"Clienti,"+"Fornitori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCF_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPOCF_1_11.GetRadio()
    this.Parent.oContained.w_TIPOCF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCF_1_11.SetRadio()
    this.Parent.oContained.w_TIPOCF=trim(this.Parent.oContained.w_TIPOCF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCF=='C',1,;
      iif(this.Parent.oContained.w_TIPOCF=='F',2,;
      iif(this.Parent.oContained.w_TIPOCF=='',3,;
      0)))
  endfunc

  func oTIPOCF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oALFDOC_1_12 as StdField with uid="PGASOKMZAJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 162644474,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=545, Top=63, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oCODCON_1_13 as StdField with uid="DKKVSZWZQO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Cliente/fornitore intestatario del documento",;
    HelpContextID = 21831974,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=121, Top=116, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NVL(.w_TIPOCF,'N') $ 'CF')
    endwith
   endif
  endfunc

  func oCODCON_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCON_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOCF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oSTRUTT_1_14 as StdField with uid="SROOSWIGII",rtseq=14,rtrep=.f.,;
    cFormVar = "w_STRUTT", cQueryName = "STRUTT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Struttura EDI incongruente o inesistente!",;
    ToolTipText = "Codice struttura associata alla causale documento",;
    HelpContextID = 128976678,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=121, Top=145, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_STRUTT"

  func oSTRUTT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_TIPDOC))
    endwith
   endif
  endfunc

  func oSTRUTT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTT_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTT_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oSTRUTT_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture",'',this.parent.oContained
  endproc
  proc oSTRUTT_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_STRUTT
     i_obj.ecpSave()
  endproc

  add object oFLSEND1_1_15 as StdCheck with uid="DDSTHLMBAT",rtseq=15,rtrep=.f.,left=497, top=117, caption="Escludi documenti generati",;
    ToolTipText = "Se attivo, esclude documenti per i quali � stato generato il ifle",;
    HelpContextID = 121638486,;
    cFormVar="w_FLSEND1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSEND1_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSEND1_1_15.GetRadio()
    this.Parent.oContained.w_FLSEND1 = this.RadioValue()
    return .t.
  endfunc

  func oFLSEND1_1_15.SetRadio()
    this.Parent.oContained.w_FLSEND1=trim(this.Parent.oContained.w_FLSEND1)
    this.value = ;
      iif(this.Parent.oContained.w_FLSEND1=='S',1,;
      0)
  endfunc

  add object oGRUPPO_1_16 as StdField with uid="PGYAHHYHSS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo intestatario",;
    HelpContextID = 40580198,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=121, Top=174, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRU_INTE", cZoomOnZoom="GSAR_AGR", oKey_1_1="GRCODICE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_INTE','*','GRCODICE',cp_AbsName(this.parent,'oGRUPPO_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGR',"Gruppi intestatari",'',this.parent.oContained
  endproc
  proc oGRUPPO_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_GRUPPO
     i_obj.ecpSave()
  endproc


  add object oBtn_1_17 as StdButton with uid="KMGHCQHBEF",left=628, top=141, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , HelpContextID = 144780310;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .Notifyevent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Zoomdoc as cp_szoombox with uid="MRJMLVEPWI",left=38, top=206, width=638,height=280,;
    caption='ZOOMDOC',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVA_KGD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init,Esegui",;
    nPag=1;
    , HelpContextID = 27775638

  add object oFLFILE_1_19 as StdCheck with uid="FVFHFHCQHG",rtseq=17,rtrep=.f.,left=34, top=493, caption="File unico",;
    ToolTipText = "Se attivo, determina la generazione di un unico file cumulativo",;
    HelpContextID = 131908010,;
    cFormVar="w_FLFILE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLFILE_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLFILE_1_19.GetRadio()
    this.Parent.oContained.w_FLFILE = this.RadioValue()
    return .t.
  endfunc

  func oFLFILE_1_19.SetRadio()
    this.Parent.oContained.w_FLFILE=trim(this.Parent.oContained.w_FLFILE)
    this.value = ;
      iif(this.Parent.oContained.w_FLFILE=='S',1,;
      0)
  endfunc


  add object oTIPSTR_1_20 as StdCombo with uid="XIJAXNFTDW",rtseq=18,rtrep=.f.,left=376,top=496,width=134,height=21;
    , ToolTipText = "Criterio di applicazione della struttura";
    , HelpContextID = 95280182;
    , cFormVar="w_TIPSTR",RowSource=""+"Da intestatario,"+"Da causale,"+"Forzata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTR_1_20.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oTIPSTR_1_20.GetRadio()
    this.Parent.oContained.w_TIPSTR = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTR_1_20.SetRadio()
    this.Parent.oContained.w_TIPSTR=trim(this.Parent.oContained.w_TIPSTR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTR=='I',1,;
      iif(this.Parent.oContained.w_TIPSTR=='D',2,;
      iif(this.Parent.oContained.w_TIPSTR=='S',3,;
      0)))
  endfunc

  func oTIPSTR_1_20.mHide()
    with this.Parent.oContained
      return (.w_FLFILE<>'S')
    endwith
  endfunc

  add object oCODSTR_1_21 as StdField with uid="ZXETOWAPEN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODSTR", cQueryName = "CODSTR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Struttura EDI incongruente o inesistente!",;
    ToolTipText = "Codice struttura utilizzata nel caso di file unico",;
    HelpContextID = 95232294,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=523, Top=495, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_CODSTR"

  func oCODSTR_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPSTR<>'S' or .w_FLFILE<>'S')
    endwith
  endfunc

  func oCODSTR_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSTR_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTR_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCODSTR_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture",'',this.parent.oContained
  endproc
  proc oCODSTR_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_CODSTR
     i_obj.ecpSave()
  endproc


  add object oBtn_1_22 as StdButton with uid="RLBYLLHJDM",left=575, top=520, width=48,height=45,;
    CpPicture="BMP\ELABORA.BMP", caption="", nPag=1;
    , HelpContextID = 162268486;
    , caption='\<Esporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        gsva_bgd(this.Parent.oContained,"ESPOR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="EITOYBANNL",left=626, top=520, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 22142982;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSTR_1_24 as StdField with uid="XRLCLECHVM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 95291190,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=205, Top=145, InputMask=replicate('X',30)

  add object oDESDOC_1_26 as StdField with uid="OADXZRBATB",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 162592970,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=183, Top=10, InputMask=replicate('X',35)

  add object oCODDES_1_31 as StdField with uid="SDVTRWQICB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 95297830,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=250, Top=116, InputMask=replicate('X',35)


  add object oBtn_1_42 as StdButton with uid="LUHNMBSWMQ",left=34, top=520, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i documenti da esportare";
    , HelpContextID = 199116762;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSVA_BGD(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="ABJFIATDMP",left=85, top=520, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i documenti da esportare";
    , HelpContextID = 199116762;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSVA_BGD(this.Parent.oContained,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_44 as StdButton with uid="PDWIPENRCZ",left=136, top=520, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezionare dei documenti da esportare";
    , HelpContextID = 199116762;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSVA_BGD(this.Parent.oContained,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESGRU_1_46 as StdField with uid="CCSWEHLVJL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142739254,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=205, Top=174, InputMask=replicate('X',35)

  add object oNUMINI_E_1_51 as StdField with uid="MXAECKOYAL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NUMINI_E", cQueryName = "NUMINI_E",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento iniziale",;
    HelpContextID = 62670821,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=121, Top=63, cSayPict='"999999"', cGetPict='"999999"', nMaxValue = 999999

  func oNUMINI_E_1_51.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oNUMFIN_E_1_52 as StdField with uid="RZKGSYURCI",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NUMFIN_E", cQueryName = "NUMFIN_E",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento finale",;
    HelpContextID = 252659685,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=351, Top=63, cSayPict='"999999"', cGetPict='"999999"', nMaxValue = 999999

  func oNUMFIN_E_1_52.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="SAMITUQYPT",Visible=.t., Left=12, Top=145,;
    Alignment=1, Width=108, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WPBOLNVROY",Visible=.t., Left=12, Top=10,;
    Alignment=1, Width=108, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="OOQMYWSQRY",Visible=.t., Left=12, Top=40,;
    Alignment=1, Width=108, Height=18,;
    Caption="Doc. dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KTZRCRKILE",Visible=.t., Left=259, Top=40,;
    Alignment=1, Width=89, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GABZWQEWEK",Visible=.t., Left=12, Top=116,;
    Alignment=1, Width=108, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HLEBWDKZSD",Visible=.t., Left=478, Top=65,;
    Alignment=1, Width=65, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="LYWXYRNSYZ",Visible=.t., Left=12, Top=65,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="SSXMIEIOAX",Visible=.t., Left=247, Top=65,;
    Alignment=1, Width=101, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="TLUYDJGPYM",Visible=.t., Left=183, Top=496,;
    Alignment=1, Width=190, Height=18,;
    Caption="Struttura da utilizzare:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_FLFILE<>'S')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="HMBMYHFGRE",Visible=.t., Left=430, Top=10,;
    Alignment=1, Width=113, Height=16,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DSORJRRLOZ",Visible=.t., Left=442, Top=39,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="GRJUVXLJYM",Visible=.t., Left=59, Top=174,;
    Alignment=1, Width=61, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="OYUDFFIUVX",Visible=.t., Left=93, Top=91,;
    Alignment=0, Width=27, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kgd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
