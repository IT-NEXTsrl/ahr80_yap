* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_sel                                                        *
*              Stampa elementi                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_10]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-24                                                      *
* Last revis.: 2008-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_sel",oParentObject))

* --- Class definition
define class tgsva_sel as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 652
  Height = 183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-02-20"
  HelpContextID=211127703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  VAELEMEN_IDX = 0
  cPrg = "gsva_sel"
  cComment = "Stampa elementi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ELCODSTR = space(20)
  w_STDESCRI = space(30)
  w_ELCODICE = space(20)
  w_ELCODIC1 = space(20)
  w_ELDESCRI = space(45)
  w_ELDESCR1 = space(45)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_selPag1","gsva_sel",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELCODSTR_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAELEMEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ELCODSTR=space(20)
      .w_STDESCRI=space(30)
      .w_ELCODICE=space(20)
      .w_ELCODIC1=space(20)
      .w_ELDESCRI=space(45)
      .w_ELDESCR1=space(45)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ELCODSTR))
          .link_1_3('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_ELCODICE))
          .link_1_5('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ELCODIC1))
          .link_1_6('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODSTR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_ELCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_ELCODSTR))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oELCODSTR_1_3'),i_cWhere,'',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_ELCODSTR)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODSTR = NVL(_Link_.STCODICE,space(20))
      this.w_STDESCRI = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODSTR = space(20)
      endif
      this.w_STDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODICE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_SEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ELCODICE)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_ELCODSTR;
                     ,'ELCODICE',trim(this.w_ELCODICE))
          select ELCODSTR,ELCODICE,ELDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODICE)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODICE)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStr(trim(this.w_ELCODICE)+"%");
                   +" and ELCODSTR="+cp_ToStr(this.w_ELCODSTR);

            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODICE) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oELCODICE_1_5'),i_cWhere,'GSVA_SEL',"Elenco elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ELCODICE);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_ELCODSTR;
                       ,'ELCODICE',this.w_ELCODICE)
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODICE = NVL(_Link_.ELCODICE,space(20))
      this.w_ELDESCRI = NVL(_Link_.ELDESCRI,space(45))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODICE = space(20)
      endif
      this.w_ELDESCRI = space(45)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODIC1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODIC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_SEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ELCODIC1)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_ELCODSTR;
                     ,'ELCODICE',trim(this.w_ELCODIC1))
          select ELCODSTR,ELCODICE,ELDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODIC1)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODIC1)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStr(trim(this.w_ELCODIC1)+"%");
                   +" and ELCODSTR="+cp_ToStr(this.w_ELCODSTR);

            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODIC1) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oELCODIC1_1_6'),i_cWhere,'GSVA_SEL',"Elenco elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODIC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ELCODIC1);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_ELCODSTR;
                       ,'ELCODICE',this.w_ELCODIC1)
            select ELCODSTR,ELCODICE,ELDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODIC1 = NVL(_Link_.ELCODICE,space(20))
      this.w_ELDESCR1 = NVL(_Link_.ELDESCRI,space(45))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODIC1 = space(20)
      endif
      this.w_ELDESCR1 = space(45)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODIC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODSTR_1_3.value==this.w_ELCODSTR)
      this.oPgFrm.Page1.oPag.oELCODSTR_1_3.value=this.w_ELCODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_4.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_4.value=this.w_STDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODICE_1_5.value==this.w_ELCODICE)
      this.oPgFrm.Page1.oPag.oELCODICE_1_5.value=this.w_ELCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODIC1_1_6.value==this.w_ELCODIC1)
      this.oPgFrm.Page1.oPag.oELCODIC1_1_6.value=this.w_ELCODIC1
    endif
    if not(this.oPgFrm.Page1.oPag.oELDESCRI_1_7.value==this.w_ELDESCRI)
      this.oPgFrm.Page1.oPag.oELDESCRI_1_7.value=this.w_ELDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELDESCR1_1_13.value==this.w_ELDESCR1)
      this.oPgFrm.Page1.oPag.oELDESCR1_1_13.value=this.w_ELDESCR1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_selPag1 as StdContainer
  Width  = 648
  height = 183
  stdWidth  = 648
  stdheight = 183
  resizeXpos=501
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODSTR_1_3 as StdField with uid="XLZVBKYZOJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ELCODSTR", cQueryName = "ELCODSTR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 198635880,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VASTRUTT", oKey_1_1="STCODICE", oKey_1_2="this.w_ELCODSTR"

  func oELCODSTR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_ELCODICE)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_ELCODIC1)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCODSTR_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODSTR_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oELCODSTR_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture",'',this.parent.oContained
  endproc

  add object oSTDESCRI_1_4 as StdField with uid="MEPPMYUBNR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 183556241,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=317, Top=12, InputMask=replicate('X',30)

  add object oELCODICE_1_5 as StdField with uid="DJZUCVEJMX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ELCODICE", cQueryName = "ELCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice elemento iniziale",;
    HelpContextID = 97972597,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=41, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_SEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_ELCODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_ELCODICE"

  func oELCODICE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODICE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODICE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_ELCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_ELCODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oELCODICE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_SEL',"Elenco elementi",'',this.parent.oContained
  endproc
  proc oELCODICE_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSVA_SEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_ELCODSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_ELCODICE
     i_obj.ecpSave()
  endproc

  add object oELCODIC1_1_6 as StdField with uid="DTQNJUSSMI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ELCODIC1", cQueryName = "ELCODIC1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice elemento finale",;
    HelpContextID = 97972617,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=70, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_SEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_ELCODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_ELCODIC1"

  func oELCODIC1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODIC1_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODIC1_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_ELCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_ELCODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oELCODIC1_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_SEL',"Elenco elementi",'',this.parent.oContained
  endproc
  proc oELCODIC1_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSVA_SEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_ELCODSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_ELCODIC1
     i_obj.ecpSave()
  endproc

  add object oELDESCRI_1_7 as StdField with uid="QGZULXASBO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELDESCRI", cQueryName = "ELDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 183558513,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=317, Top=41, InputMask=replicate('X',45)


  add object oObj_1_8 as cp_outputCombo with uid="ZLERXIVPWT",left=158, top=102, width=480,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 147745562


  add object oBtn_1_9 as StdButton with uid="CCGSUHUDGX",left=538, top=130, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 211148262;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="YXYHLPQOEL",left=590, top=130, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 3022842;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oELDESCR1_1_13 as StdField with uid="PVJIJLCSDK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ELDESCR1", cQueryName = "ELDESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 183558537,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=317, Top=70, InputMask=replicate('X',45)

  add object oStr_1_1 as StdString with uid="MHSJGPKKJF",Visible=.t., Left=9, Top=14,;
    Alignment=1, Width=145, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="DURCNPNXWQ",Visible=.t., Left=9, Top=42,;
    Alignment=1, Width=145, Height=18,;
    Caption="Da elemento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=9, Top=102,;
    Alignment=1, Width=145, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GMGSBLYWML",Visible=.t., Left=9, Top=72,;
    Alignment=1, Width=145, Height=18,;
    Caption="A elemento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_sel','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
