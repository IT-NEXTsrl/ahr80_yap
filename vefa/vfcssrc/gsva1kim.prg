* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva1kim                                                        *
*              Import delfor                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_109]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-07                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva1kim",oParentObject))

* --- Class definition
define class tgsva1kim as StdForm
  Top    = 11
  Left   = 11

  * --- Standard Properties
  Width  = 780
  Height = 609+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-19"
  HelpContextID=266833303
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=36

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  XDC_FIELDS_IDX = 0
  TIP_DOCU_IDX = 0
  CONTROPA_IDX = 0
  VAANAIMP_IDX = 0
  PAR_VEFA_IDX = 0
  FLATDETT_IDX = 0
  cPrg = "gsva1kim"
  cComment = "Import delfor"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(50)
  o_PARAM = space(50)
  w_READAZI = space(5)
  w_READPAR = space(5)
  w_CODSTR = space(10)
  w_FLT_ELAB = space(15)
  w_SER_ELAB = space(10)
  w_PATHEDI = space(200)
  w_FILE = space(254)
  w_TIPDOC = space(5)
  w_FLCAST = space(1)
  w_FLRAT = space(1)
  w_FLRPZ = space(1)
  w_DESSTR = space(30)
  w_ERRORLOG = space(0)
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_TBNAME = space(10)
  w_FLDEFA = space(10)
  o_FLDEFA = space(10)
  w_TIPCAM = space(1)
  w_DATA = ctot('')
  w_CARACTER = space(254)
  w_FLNULL = space(10)
  o_FLNULL = space(10)
  w_NUMERIC = 0
  w_LUNG = 0
  w_DECIMAL = 0
  w_FASE = .F.
  w_DESDOC = space(35)
  w_FASE2 = .F.
  w_SERGDOC = space(10)
  w_Msg = space(0)
  w_STEPB = space(1)
  w_FLATAB = space(15)
  w_TIPEVA = space(1)
  w_QTAMIN = 0
  w_GIOLIM = 0
  w_TIPSCA = space(1)
  w_CAMPO = space(10)
  o_CAMPO = space(10)
  w_ZOOMDOC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva1kimPag1","gsva1kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Import")
      .Pages(2).addobject("oPag","tgsva1kimPag2","gsva1kim",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODSTR_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMDOC = this.oPgFrm.Pages(1).oPag.ZOOMDOC
    DoDefault()
    proc Destroy()
      this.w_ZOOMDOC = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='VAANAIMP'
    this.cWorkTables[6]='PAR_VEFA'
    this.cWorkTables[7]='FLATDETT'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsva1kim
    * --- Propone l'ultima commessa Inserita
    p_OLDSTR=space(10)
    if p_OLDSTR<>this.w_CODSTR
       p_OLDSTR=this.w_CODSTR
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(50)
      .w_READAZI=space(5)
      .w_READPAR=space(5)
      .w_CODSTR=space(10)
      .w_FLT_ELAB=space(15)
      .w_SER_ELAB=space(10)
      .w_PATHEDI=space(200)
      .w_FILE=space(254)
      .w_TIPDOC=space(5)
      .w_FLCAST=space(1)
      .w_FLRAT=space(1)
      .w_FLRPZ=space(1)
      .w_DESSTR=space(30)
      .w_ERRORLOG=space(0)
      .w_SERIAL=space(10)
      .w_TBNAME=space(10)
      .w_FLDEFA=space(10)
      .w_TIPCAM=space(1)
      .w_DATA=ctot("")
      .w_CARACTER=space(254)
      .w_FLNULL=space(10)
      .w_NUMERIC=0
      .w_LUNG=0
      .w_DECIMAL=0
      .w_FASE=.f.
      .w_DESDOC=space(35)
      .w_FASE2=.f.
      .w_SERGDOC=space(10)
      .w_Msg=space(0)
      .w_STEPB=space(1)
      .w_FLATAB=space(15)
      .w_TIPEVA=space(1)
      .w_QTAMIN=0
      .w_GIOLIM=0
      .w_TIPSCA=space(1)
      .w_CAMPO=space(10)
        .w_PARAM = this.oParentObject
        .w_READAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READAZI))
          .link_1_2('Full')
        endif
        .w_READPAR = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_READPAR))
          .link_1_3('Full')
        endif
        .w_CODSTR = IIF(p_OLDSTR<>space(10) OR !EMPTY(.w_PARAM), IIF(EMPTY(.w_PARAM),p_OLDSTR, RIGHT(ALLTRIM(.w_PARAM), LEN(ALLTRIM(.w_PARAM))-AT('#',.w_PARAM))), CERSTRUPRE('1 ASC'))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODSTR))
          .link_1_4('Full')
        endif
        .w_FLT_ELAB = 'N'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SER_ELAB))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_9('Full')
        endif
        .w_FLCAST = 'S'
        .w_FLRAT = 'S'
        .w_FLRPZ = 'N'
      .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
          .DoRTCalc(13,13,.f.)
        .w_ERRORLOG = .w_ZOOMDOC.GETVAR('ERRORMSG')
        .w_SERIAL = .w_ZOOMDOC.GETVAR('DFSERIAL')
        .w_TBNAME = 'DELFOR_1'
        .w_FLDEFA = 'N'
          .DoRTCalc(18,19,.f.)
        .w_CARACTER = ' '
        .w_FLNULL = 'N'
          .DoRTCalc(22,24,.f.)
        .w_FASE = .F.
          .DoRTCalc(26,26,.f.)
        .w_FASE2 = .F.
        .DoRTCalc(28,36,.f.)
        if not(empty(.w_CAMPO))
          .link_1_50('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsva1kim
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_CAMPO")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_READAZI = i_CODAZI
          .link_1_2('Full')
            .w_READPAR = i_CODAZI
          .link_1_3('Full')
        if .o_PARAM<>.w_PARAM
            .w_CODSTR = IIF(p_OLDSTR<>space(10) OR !EMPTY(.w_PARAM), IIF(EMPTY(.w_PARAM),p_OLDSTR, RIGHT(ALLTRIM(.w_PARAM), LEN(ALLTRIM(.w_PARAM))-AT('#',.w_PARAM))), CERSTRUPRE('1 ASC'))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_PARAM<>.w_PARAM
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
        .DoRTCalc(7,13,.t.)
        if .o_SERIAL<>.w_SERIAL
            .w_ERRORLOG = .w_ZOOMDOC.GETVAR('ERRORMSG')
        endif
            .w_SERIAL = .w_ZOOMDOC.GETVAR('DFSERIAL')
            .w_TBNAME = 'DELFOR_1'
        if .o_CAMPO<>.w_CAMPO
            .w_FLDEFA = 'N'
        endif
        .DoRTCalc(18,19,.t.)
        if .o_FLNULL<>.w_FLNULL.or. .o_CAMPO<>.w_CAMPO
            .w_CARACTER = ' '
        endif
        if .o_FLDEFA<>.w_FLDEFA
          .Calculate_ZSJAANYDLS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,36,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
    endwith
  return

  proc Calculate_TGHVPJMLKN()
    with this
          * --- Gsva1bim(done) done, elimina
          GSVA1BIM(this;
              ,.w_FILE;
              ,.w_CODSTR;
              ,'DONE';
             )
    endwith
  endproc
  proc Calculate_ZSJAANYDLS()
    with this
          * --- Gsva1bim(defac) w_fldefa changed
          GSVA1BIM(this;
              ,.w_FILE;
              ,.w_CODSTR;
              ,'DEFAC';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODSTR_1_4.enabled = this.oPgFrm.Page1.oPag.oCODSTR_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSER_ELAB_1_6.enabled = this.oPgFrm.Page1.oPag.oSER_ELAB_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCARACTER_1_25.enabled = this.oPgFrm.Page1.oPag.oCARACTER_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLCAST_1_10.visible=!this.oPgFrm.Page1.oPag.oFLCAST_1_10.mHide()
    this.oPgFrm.Page1.oPag.oFLRAT_1_11.visible=!this.oPgFrm.Page1.oPag.oFLRAT_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFLRPZ_1_12.visible=!this.oPgFrm.Page1.oPag.oFLRPZ_1_12.mHide()
    this.oPgFrm.Page1.oPag.oFLDEFA_1_22.visible=!this.oPgFrm.Page1.oPag.oFLDEFA_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDATA_1_24.visible=!this.oPgFrm.Page1.oPag.oDATA_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCARACTER_1_25.visible=!this.oPgFrm.Page1.oPag.oCARACTER_1_25.mHide()
    this.oPgFrm.Page1.oPag.oFLNULL_1_27.visible=!this.oPgFrm.Page1.oPag.oFLNULL_1_27.mHide()
    this.oPgFrm.Page1.oPag.oNUMERIC_1_28.visible=!this.oPgFrm.Page1.oPag.oNUMERIC_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMDOC.Event(cEvent)
        if lower(cEvent)==lower("Done") or lower(cEvent)==lower("Elimina")
          .Calculate_TGHVPJMLKN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHED";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_READAZI)
            select COCODAZI,COPATHED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATHEDI = NVL(_Link_.COPATHED,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_PATHEDI = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_lTable = "PAR_VEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2], .t., this.PAR_VEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACRIPEV,PAQTAMIN,PAGIOLIM,PASCADEN";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READPAR)
            select PACODAZI,PACRIPEV,PAQTAMIN,PAGIOLIM,PASCADEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PACODAZI,space(5))
      this.w_TIPEVA = NVL(_Link_.PACRIPEV,space(1))
      this.w_QTAMIN = NVL(_Link_.PAQTAMIN,0)
      this.w_GIOLIM = NVL(_Link_.PAGIOLIM,0)
      this.w_TIPSCA = NVL(_Link_.PASCADEN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(5)
      endif
      this.w_TIPEVA = space(1)
      this.w_QTAMIN = 0
      this.w_GIOLIM = 0
      this.w_TIPSCA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_VEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSTR
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLATAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CODSTR))
          select STCODICE,STDESCRI,STFLATAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCODSTR_1_4'),i_cWhere,'GSVA_AST',"Elenco strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLATAB";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STFLATAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLATAB";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CODSTR)
            select STCODICE,STDESCRI,STFLATAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_FLATAB = NVL(_Link_.STFLATAB,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODSTR = space(10)
      endif
      this.w_DESSTR = space(30)
      this.w_FLATAB = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SER_ELAB
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAANAIMP_IDX,3]
    i_lTable = "VAANAIMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2], .t., this.VAANAIMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SER_ELAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AIM',True,'VAANAIMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMSERIAL like "+cp_ToStrODBC(trim(this.w_SER_ELAB)+"%");
                   +" and IMFLELAB="+cp_ToStrODBC(this.w_FLT_ELAB);

          i_ret=cp_SQL(i_nConn,"select IMFLELAB,IMSERIAL,IMSSTEPB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMFLELAB,IMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMFLELAB',this.w_FLT_ELAB;
                     ,'IMSERIAL',trim(this.w_SER_ELAB))
          select IMFLELAB,IMSERIAL,IMSSTEPB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMFLELAB,IMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SER_ELAB)==trim(_Link_.IMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SER_ELAB) and !this.bDontReportError
            deferred_cp_zoom('VAANAIMP','*','IMFLELAB,IMSERIAL',cp_AbsName(oSource.parent,'oSER_ELAB_1_6'),i_cWhere,'GSVA_AIM',"Importazioni",'gsva_kwi.VAANAIMP_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLT_ELAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMFLELAB,IMSERIAL,IMSSTEPB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select IMFLELAB,IMSERIAL,IMSSTEPB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Importazione non al terzo step!")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMFLELAB,IMSERIAL,IMSSTEPB";
                     +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and IMFLELAB="+cp_ToStrODBC(this.w_FLT_ELAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMFLELAB',oSource.xKey(1);
                       ,'IMSERIAL',oSource.xKey(2))
            select IMFLELAB,IMSERIAL,IMSSTEPB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SER_ELAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMFLELAB,IMSERIAL,IMSSTEPB";
                   +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(this.w_SER_ELAB);
                   +" and IMFLELAB="+cp_ToStrODBC(this.w_FLT_ELAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMFLELAB',this.w_FLT_ELAB;
                       ,'IMSERIAL',this.w_SER_ELAB)
            select IMFLELAB,IMSERIAL,IMSSTEPB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SER_ELAB = NVL(_Link_.IMSERIAL,space(10))
      this.w_STEPB = NVL(_Link_.IMSSTEPB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SER_ELAB = space(10)
      endif
      this.w_STEPB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_STEPB='O'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Importazione non al terzo step!")
        endif
        this.w_SER_ELAB = space(10)
        this.w_STEPB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])+'\'+cp_ToStr(_Link_.IMFLELAB,1)+'\'+cp_ToStr(_Link_.IMSERIAL,1)
      cp_ShowWarn(i_cKey,this.VAANAIMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SER_ELAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_9'),i_cWhere,'GSVE_ATD',"Tipi documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMPO
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLATDETT_IDX,3]
    i_lTable = "FLATDETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2], .t., this.FLATDETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('no Struttura Tabelle',True,'FLATDETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FTFLDNAM like "+cp_ToStrODBC(trim(this.w_CAMPO)+"%");
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);

          i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FTCODICE,FTTABNAM,FTFLDNAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FTCODICE',this.w_FLATAB;
                     ,'FTTABNAM',this.w_TBNAME;
                     ,'FTFLDNAM',trim(this.w_CAMPO))
          select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FTCODICE,FTTABNAM,FTFLDNAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMPO)==trim(_Link_.FTFLDNAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('FLATDETT','*','FTCODICE,FTTABNAM,FTFLDNAM',cp_AbsName(oSource.parent,'oCAMPO_1_50'),i_cWhere,'no Struttura Tabelle',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLATAB<>oSource.xKey(1);
           .or. this.w_TBNAME<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(oSource.xKey(3));
                     +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                     +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',oSource.xKey(1);
                       ,'FTTABNAM',oSource.xKey(2);
                       ,'FTFLDNAM',oSource.xKey(3))
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                   +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(this.w_CAMPO);
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',this.w_FLATAB;
                       ,'FTTABNAM',this.w_TBNAME;
                       ,'FTFLDNAM',this.w_CAMPO)
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMPO = NVL(_Link_.FTFLDNAM,space(10))
      this.w_TIPCAM = NVL(_Link_.FTFLDTYP,space(1))
      this.w_LUNG = NVL(_Link_.FTFLDDIM,0)
      this.w_DECIMAL = NVL(_Link_.FTFLDDEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CAMPO = space(10)
      endif
      this.w_TIPCAM = space(1)
      this.w_LUNG = 0
      this.w_DECIMAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])+'\'+cp_ToStr(_Link_.FTCODICE,1)+'\'+cp_ToStr(_Link_.FTTABNAM,1)+'\'+cp_ToStr(_Link_.FTFLDNAM,1)
      cp_ShowWarn(i_cKey,this.FLATDETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODSTR_1_4.value==this.w_CODSTR)
      this.oPgFrm.Page1.oPag.oCODSTR_1_4.value=this.w_CODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oSER_ELAB_1_6.value==this.w_SER_ELAB)
      this.oPgFrm.Page1.oPag.oSER_ELAB_1_6.value=this.w_SER_ELAB
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_9.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_9.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCAST_1_10.RadioValue()==this.w_FLCAST)
      this.oPgFrm.Page1.oPag.oFLCAST_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRAT_1_11.RadioValue()==this.w_FLRAT)
      this.oPgFrm.Page1.oPag.oFLRAT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRPZ_1_12.RadioValue()==this.w_FLRPZ)
      this.oPgFrm.Page1.oPag.oFLRPZ_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_14.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_14.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oERRORLOG_1_17.value==this.w_ERRORLOG)
      this.oPgFrm.Page1.oPag.oERRORLOG_1_17.value=this.w_ERRORLOG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFA_1_22.RadioValue()==this.w_FLDEFA)
      this.oPgFrm.Page1.oPag.oFLDEFA_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_1_24.value==this.w_DATA)
      this.oPgFrm.Page1.oPag.oDATA_1_24.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oCARACTER_1_25.value==this.w_CARACTER)
      this.oPgFrm.Page1.oPag.oCARACTER_1_25.value=this.w_CARACTER
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNULL_1_27.RadioValue()==this.w_FLNULL)
      this.oPgFrm.Page1.oPag.oFLNULL_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERIC_1_28.value==this.w_NUMERIC)
      this.oPgFrm.Page1.oPag.oNUMERIC_1_28.value=this.w_NUMERIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_33.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_33.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oMsg_2_1.value==this.w_Msg)
      this.oPgFrm.Page2.oPag.oMsg_2_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO_1_50.RadioValue()==this.w_CAMPO)
      this.oPgFrm.Page1.oPag.oCAMPO_1_50.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODSTR))  and (EMPTY(.w_PARAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSTR_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODSTR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_STEPB='O')  and (EMPTY(.w_PARAM))  and not(empty(.w_SER_ELAB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSER_ELAB_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Importazione non al terzo step!")
          case   not(LEN(ALLTRIM(.w_CARACTER))<=.w_LUNG)  and not(.w_TIPCAM<>'C')  and (.w_FLNULL='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCARACTER_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il valore impostato eccede la dimensione del campo")
          case   not(.w_NUMERIC<=VAL(REPL('9',.w_LUNG-.w_DECIMAL)))  and not(.w_TIPCAM<>'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERIC_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, valore eccedente la lunghezza del campo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PARAM = this.w_PARAM
    this.o_SERIAL = this.w_SERIAL
    this.o_FLDEFA = this.w_FLDEFA
    this.o_FLNULL = this.w_FLNULL
    this.o_CAMPO = this.w_CAMPO
    return

enddefine

* --- Define pages as container
define class tgsva1kimPag1 as StdContainer
  Width  = 776
  height = 609
  stdWidth  = 776
  stdheight = 609
  resizeXpos=448
  resizeYpos=351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODSTR_1_4 as StdField with uid="SROOSWIGII",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODSTR", cQueryName = "CODSTR",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 142663386,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=112, Top=8, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_CODSTR"

  func oCODSTR_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_PARAM))
    endwith
   endif
  endfunc

  func oCODSTR_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSTR_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTR_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCODSTR_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture",'',this.parent.oContained
  endproc
  proc oCODSTR_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_CODSTR
     i_obj.ecpSave()
  endproc

  add object oSER_ELAB_1_6 as StdField with uid="QDMKHOLGYV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SER_ELAB", cQueryName = "SER_ELAB",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Importazione non al terzo step!",;
    HelpContextID = 258213784,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=112, Top=37, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAANAIMP", cZoomOnZoom="GSVA_AIM", oKey_1_1="IMFLELAB", oKey_1_2="this.w_FLT_ELAB", oKey_2_1="IMSERIAL", oKey_2_2="this.w_SER_ELAB"

  func oSER_ELAB_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_PARAM))
    endwith
   endif
  endfunc

  func oSER_ELAB_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSER_ELAB_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSER_ELAB_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAANAIMP_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IMFLELAB="+cp_ToStrODBC(this.Parent.oContained.w_FLT_ELAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"IMFLELAB="+cp_ToStr(this.Parent.oContained.w_FLT_ELAB)
    endif
    do cp_zoom with 'VAANAIMP','*','IMFLELAB,IMSERIAL',cp_AbsName(this.parent,'oSER_ELAB_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AIM',"Importazioni",'gsva_kwi.VAANAIMP_VZM',this.parent.oContained
  endproc
  proc oSER_ELAB_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.IMFLELAB=w_FLT_ELAB
     i_obj.w_IMSERIAL=this.parent.oContained.w_SER_ELAB
     i_obj.ecpSave()
  endproc

  add object oTIPDOC_1_9 as StdField with uid="SOMWIURCTL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Causale documento da utilizzare in modo prioritario rispetto a quanto previsto nel file o nei valori predefiniti",;
    HelpContextID = 132064202,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=112, Top=68, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'',this.parent.oContained
  endproc
  proc oTIPDOC_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oFLCAST_1_10 as StdCheck with uid="ASYRLIUOUP",rtseq=10,rtrep=.f.,left=112, top=99, caption="Aggiorna castelletto IVA",;
    ToolTipText = "Se attivo, aggiorna campi castelletto IVA",;
    HelpContextID = 111341994,;
    cFormVar="w_FLCAST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCAST_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCAST_1_10.GetRadio()
    this.Parent.oContained.w_FLCAST = this.RadioValue()
    return .t.
  endfunc

  func oFLCAST_1_10.SetRadio()
    this.Parent.oContained.w_FLCAST=trim(this.Parent.oContained.w_FLCAST)
    this.value = ;
      iif(this.Parent.oContained.w_FLCAST=='S',1,;
      0)
  endfunc

  func oFLCAST_1_10.mHide()
    with this.Parent.oContained
      return (g_Application='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oFLRAT_1_11 as StdCheck with uid="VOXOXRTIYN",rtseq=11,rtrep=.f.,left=305, top=99, caption="Ricalcola rate\scadenze",;
    ToolTipText = "Se attivo, ricalcola rate prima di inserire documento azzerando le rate da file",;
    HelpContextID = 91094614,;
    cFormVar="w_FLRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRAT_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLRAT_1_11.GetRadio()
    this.Parent.oContained.w_FLRAT = this.RadioValue()
    return .t.
  endfunc

  func oFLRAT_1_11.SetRadio()
    this.Parent.oContained.w_FLRAT=trim(this.Parent.oContained.w_FLRAT)
    this.value = ;
      iif(this.Parent.oContained.w_FLRAT=='S',1,;
      0)
  endfunc

  func oFLRAT_1_11.mHide()
    with this.Parent.oContained
      return (g_Application='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oFLRPZ_1_12 as StdCheck with uid="CPEYXBCKVB",rtseq=12,rtrep=.f.,left=510, top=99, caption="Ricalcola prezzi",;
    ToolTipText = "Se attivo, ricalcola pezzi da listni\contratti",;
    HelpContextID = 98369110,;
    cFormVar="w_FLRPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRPZ_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLRPZ_1_12.GetRadio()
    this.Parent.oContained.w_FLRPZ = this.RadioValue()
    return .t.
  endfunc

  func oFLRPZ_1_12.SetRadio()
    this.Parent.oContained.w_FLRPZ=trim(this.Parent.oContained.w_FLRPZ)
    this.value = ;
      iif(this.Parent.oContained.w_FLRPZ=='S',1,;
      0)
  endfunc

  func oFLRPZ_1_12.mHide()
    with this.Parent.oContained
      return (g_Application='ad hoc ENTERPRISE')
    endwith
  endfunc


  add object oBtn_1_13 as StdButton with uid="RMTOYGGCOK",left=714, top=558, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 215752698;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSTR_1_14 as StdField with uid="XRLCLECHVM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 142604490,;
   bGlobalFont=.t.,;
    Height=21, Width=536, Left=207, Top=8, InputMask=replicate('X',30)


  add object ZOOMDOC as cp_szoombox with uid="IDMLORUCJH",left=9, top=126, width=762,height=308,;
    caption='',;
   bGlobalFont=.t.,;
    cMenuFile="",cZoomOnZoom="",cTable="DELFOR_1",cZoomFile="GSVA1KIM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,ndesflags=3,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 266833398


  add object oBtn_1_16 as StdButton with uid="ACELKHAMWG",left=249, top=437, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per verificare i dati";
    , HelpContextID = 4712265;
    , caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_FILE,.w_CODSTR,"VERIF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FASE)
      endwith
    endif
  endfunc

  add object oERRORLOG_1_17 as StdMemo with uid="PYOWYDGYBL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ERRORLOG", cQueryName = "ERRORLOG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 245627763,;
   bGlobalFont=.t.,;
    Height=65, Width=761, Left=10, Top=487, readonly=.T.


  add object oBtn_1_18 as StdButton with uid="KZTUXLJOWZ",left=302, top=437, width=48,height=45,;
    CpPicture="BMP\CONFTESTA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i dati";
    , HelpContextID = 123412089;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_FILE,.w_CODSTR,"CONFE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FASE)
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="RYBRFUUGDQ",left=714, top=438, width=48,height=45,;
    CpPicture="BMp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dettagli";
    , HelpContextID = 225654687;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      do GSVA2KIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FASE2  And ChketStatus(alltrim(.w_FLATAB), 'DELFOR_2')=0)
      endwith
    endif
  endfunc

  add object oFLDEFA_1_22 as StdCheck with uid="CLGLVNRZYS",rtseq=17,rtrep=.f.,left=237, top=565, caption="Default",;
    ToolTipText = "Se attivo imposta il valore attuale del campo",;
    HelpContextID = 175038890,;
    cFormVar="w_FLDEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDEFA_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDEFA_1_22.GetRadio()
    this.Parent.oContained.w_FLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFA_1_22.SetRadio()
    this.Parent.oContained.w_FLDEFA=trim(this.Parent.oContained.w_FLDEFA)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFA=='S',1,;
      0)
  endfunc

  func oFLDEFA_1_22.mHide()
    with this.Parent.oContained
      return (Empty(.w_CAMPO))
    endwith
  endfunc

  add object oDATA_1_24 as StdField with uid="SFGSYAZGLB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Valore data da aggiornare",;
    HelpContextID = 3019574,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=462, Top=565, bHasZoom = .t. 

  func oDATA_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'D')
    endwith
  endfunc

  proc oDATA_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATA_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCARACTER_1_25 as StdField with uid="NIVVMVXGKU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CARACTER", cQueryName = "CARACTER",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il valore impostato eccede la dimensione del campo",;
    ToolTipText = "Valore carattere da aggiornare",;
    HelpContextID = 128060552,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=453, Top=566, InputMask=replicate('X',254)

  func oCARACTER_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNULL='N')
    endwith
   endif
  endfunc

  func oCARACTER_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'C')
    endwith
  endfunc

  func oCARACTER_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (LEN(ALLTRIM(.w_CARACTER))<=.w_LUNG)
    endwith
    return bRes
  endfunc


  add object oBtn_1_26 as StdButton with uid="GTULXLRYOW",left=333, top=558, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati";
    , HelpContextID = 112760473;
    , caption='A\<ggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSVA_BAG(this.Parent.oContained,.w_ZOOMDOC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_SERIAL) and Not empty(.w_CAMPO))
      endwith
    endif
  endfunc

  add object oFLNULL_1_27 as StdCheck with uid="LHPRISBZJM",rtseq=21,rtrep=.f.,left=387, top=565, caption="Null",;
    ToolTipText = "Se attivo imposta valore null nel campo aggiornato",;
    HelpContextID = 251543978,;
    cFormVar="w_FLNULL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNULL_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLNULL_1_27.GetRadio()
    this.Parent.oContained.w_FLNULL = this.RadioValue()
    return .t.
  endfunc

  func oFLNULL_1_27.SetRadio()
    this.Parent.oContained.w_FLNULL=trim(this.Parent.oContained.w_FLNULL)
    this.value = ;
      iif(this.Parent.oContained.w_FLNULL=='S',1,;
      0)
  endfunc

  func oFLNULL_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'C')
    endwith
  endfunc

  add object oNUMERIC_1_28 as StdField with uid="ECXJNNFRYF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NUMERIC", cQueryName = "NUMERIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, valore eccedente la lunghezza del campo",;
    ToolTipText = "Valore numerico da aggiornare",;
    HelpContextID = 28198954,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=453, Top=567, cSayPict="'99999999999999.99999'", cGetPict="'99999999999999.99999'"

  func oNUMERIC_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'N')
    endwith
  endfunc

  func oNUMERIC_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERIC<=VAL(REPL('9',.w_LUNG-.w_DECIMAL)))
    endwith
    return bRes
  endfunc

  add object oDESDOC_1_33 as StdField with uid="YAHQAFTOMS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 132053194,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=207, Top=68, InputMask=replicate('X',35)


  add object oBtn_1_36 as StdButton with uid="LUHNMBSWMQ",left=10, top=437, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i documenti da importare";
    , HelpContextID = 99858470;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_FILE,.w_CODSTR,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="ABJFIATDMP",left=61, top=437, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i documenti da importare";
    , HelpContextID = 99858470;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_FILE,.w_CODSTR,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="PDWIPENRCZ",left=112, top=437, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezionare dei documenti da importare";
    , HelpContextID = 99858470;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_FILE,.w_CODSTR,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="DIQWQCPZIU",left=714, top=78, width=48,height=45,;
    CpPicture="BMP\VISUALI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati importati";
    , HelpContextID = 75628666;
    , caption='Vis\<ualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSVA1BIM(this.Parent.oContained,.w_SER_ELAB,.w_CODSTR,"IMPOR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not empty(.w_FILE) OR Not empty(.w_CODSTR))
      endwith
    endif
  endfunc


  add object oCAMPO_1_50 as StdZTamTableCombo with uid="SJFZRVIEMH",rtseq=36,rtrep=.f.,left=10,top=567,width=223,height=21;
    , ToolTipText = "Campo da aggiornare";
    , HelpContextID = 86811430;
    , cFormVar="w_CAMPO",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="FLATDETT";
    , cTable='GSVA_QCO.VQR',cKey='FLNAME',cValue='FLCOMMEN',cOrderBy='FLCOMMEN',xDefault=space(10);
  , bGlobalFont=.t.


  func oCAMPO_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMPO_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oStr_1_32 as StdString with uid="UQWHJOWCXJ",Visible=.t., Left=7, Top=10,;
    Alignment=1, Width=102, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="DNGUBEPCUP",Visible=.t., Left=7, Top=72,;
    Alignment=1, Width=102, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="OAQEOEDGJE",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=103, Height=18,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsva1kimPag2 as StdContainer
  Width  = 776
  height = 609
  stdWidth  = 776
  stdheight = 609
  resizeXpos=450
  resizeYpos=436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_2_1 as StdMemo with uid="BQJOIBGZCP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 267285958,;
   bGlobalFont=.t.,;
    Height=585, Width=761, Left=7, Top=15, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva1kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsva1kim
* --- Classe per gestire la combo PICONDCON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif
  endproc


enddefine
* --- Fine Area Manuale
