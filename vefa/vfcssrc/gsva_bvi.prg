* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bvi                                                        *
*              Visualizzazione saldi imballi                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-11                                                      *
* Last revis.: 2006-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bvi",oParentObject)
return(i_retval)

define class tgsva_bvi as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo dello zoom e dei totali nella Maschera GSVA_KVI
    this.w_PADRE = This.oParentObject
    this.w_PADRE.NotifyEvent("Ricerca")     
    SELECT (this.oParentObject.w_ZoomSal.cCursor)
    GO TOP
    SUM SIQTAESI, SIQTASTO TO this.oParentObject.w_TOTESI, this.oParentObject.w_TOTSTO
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
