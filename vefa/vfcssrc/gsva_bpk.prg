* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bpk                                                        *
*              Controllo campi chiave                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-29                                                      *
* Last revis.: 2012-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bpk",oParentObject)
return(i_retval)

define class tgsva_bpk as StdBatch
  * --- Local variables
  w_oMESS = .NULL.
  w_GSVA_MFT = .NULL.
  w_FTCODICE = space(15)
  w_CAMPO = space(15)
  w_NOMCAM = space(15)
  w_DESCAM = space(30)
  w_CHIAVE = space(15)
  w_ARCHIVIO = space(15)
  w_POSKEY = 0
  w_OK = .f.
  * --- WorkFile variables
  FLATDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla Gestione tabelle piatte (gsva_mft), controlla se esistono uno o pi� campi chiave non dichiarati per una o pi� tabelle definite. 
    this.w_oMESS=createobject("AH_ErrorLog")
    this.w_OK = .T.
    this.w_GSVA_MFT = THIS.OPARENTOBJECT
    this.w_GSVA_MFT.Exec_Select("_TMP_MFT","t_FTTABNAM AS FTTABNAM","","","FTTABNAM","")     
    this.w_FTCODICE = this.w_GSVA_MFT.w_FTCODICE
    if Reccount("_TMP_MFT")>0
      Select _TMP_MFT 
 Go Top 
 Scan
      this.w_ARCHIVIO = _TMP_MFT.FTTABNAM
      this.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( this.w_ARCHIVIO ,1 ) )
      sel=""
      this.w_POSKEY = 1
      do while  this.w_POSKEY<>0
        this.w_POSKEY = At( "," , this.w_CHIAVE )
        if this.w_POSKEY>0
          this.w_CAMPO = Left( this.w_CHIAVE , this.w_POSKEY -1 )
        else
          this.w_CAMPO = Alltrim( this.w_CHIAVE )
        endif
        if this.w_GSVA_MFT.Search("NVL(t_FTTABNAM, ' ')='"+ Alltrim(this.w_ARCHIVIO) + "' AND NVL(t_FTFLDNAM, ' ')='" + Alltrim(this.w_CAMPO)+ "'  AND NOT DELETED()") = -1
          this.w_OK = .F.
          this.w_DESCAM = I_DCX.GEtfieldDESCR(Alltrim(this.w_ARCHIVIO),I_DCX.GETfieldidx(Alltrim(this.w_ARCHIVIO),Alltrim(this.w_CAMPO)))
          this.w_oMESS.AddMsgLogPartNoTrans(SPACE(4),"Tabella: %1  -  Campo: %2  %3",ALLTRIM(this.w_ARCHIVIO),ALLTRIM(this.w_CAMPO),ALLTRIM(this.w_DESCAM))     
        endif
        this.w_CHIAVE = SUBSTR(this.w_CHIAVE,this.w_POSKEY+1,LEN(this.w_CHIAVE)-this.w_POSKEY)
        sel=sel + Alltrim(this.w_ARCHIVIO)+"."+Alltrim(this.w_CAMPO)+","
      enddo
      Select _TMP_MFT 
 EndScan
      if this.w_OK
        this.w_OK = .T.
      else
        this.w_oMESS.PrintLog(this,"Elenco campi chiave mancanti",.T.,"Per una o pi� tabelle non sono stati indicati i campi chiave!%0Stampare l'elenco dei campi chiave mancanti?")     
        this.w_OK = .F.
      endif
    endif
    Use in _TMP_MFT
    this.w_oMESS = null
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FLATDETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
