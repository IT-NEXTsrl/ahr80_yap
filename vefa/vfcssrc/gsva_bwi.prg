* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bwi                                                        *
*              ASSISTENTE IMPORTAZIONE FILE EDI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-23                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pOBJDEST,pOBJMSG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bwi",oParentObject,m.pOPER,m.pOBJDEST,m.pOBJMSG)
return(i_retval)

define class tgsva_bwi as StdBatch
  * --- Local variables
  pOPER = space(5)
  pOBJDEST = space(50)
  pOBJMSG = .NULL.
  w_oCtrl = .NULL.
  w_NOMEFILE = space(254)
  w_PROC_OK = .f.
  w_PADRE = .NULL.
  w_DIRXML = space(200)
  w_OLD_g_MSG = space(0)
  w_GENERR = space(1)
  w_STEPA = space(1)
  w_oERRORLOG = .NULL.
  w_LTIPCAM = space(1)
  w_DECIMAL = 0
  w_LENGHT = 0
  w_CODTAB = space(30)
  w_LCAMPO = space(30)
  w_TIPVAL = space(1)
  w_CODTABVAL = space(30)
  w_CAMPOVAL = space(30)
  w_OKNULL = .f.
  w_LCODTAB = space(10)
  w_STEPB = space(1)
  w_PATH = space(100)
  w_ERRODIR = .f.
  w_CODAZI = space(5)
  w_DIRFIL = space(200)
  w_DIRSPO = space(200)
  w_FLATAB = space(15)
  w_PHNAME = space(30)
  * --- WorkFile variables
  VAANAIMP_idx=0
  VASTRUTT_idx=0
  PAR_VEFA_idx=0
  VAELEMEN_idx=0
  VAPREDEF_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Oggeto di riferimento per valorizzazione Log
    this.pOBJMSG = IIF(Vartype(this.pOBJMSG)="O",this.pOBJMSG,This.oparentobject)
    this.w_PADRE = this.pOBJMSG
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_VEFA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_VEFA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_VEFA_idx,2],.t.,this.PAR_VEFA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PADIRFIL,PADIRSPO,PADIRXML"+;
        " from "+i_cTable+" PAR_VEFA where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PADIRFIL,PADIRSPO,PADIRXML;
        from (i_cTable) where;
            PACODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DIRFIL = NVL(cp_ToDate(_read_.PADIRFIL),cp_NullValue(_read_.PADIRFIL))
      this.w_DIRSPO = NVL(cp_ToDate(_read_.PADIRSPO),cp_NullValue(_read_.PADIRSPO))
      this.w_DIRXML = NVL(cp_ToDate(_read_.PADIRXML),cp_NullValue(_read_.PADIRXML))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pOPER=="BACK"
        this.oParentObject.w_MSG = ""
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="NEXT"
        this.oParentObject.w_MSG = ""
        if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
          this.w_OLD_g_MSG = g_MSG
           
 g_MSG=""
        endif
        this.w_GENERR = this.pOBJMSG.w_GENERR
        do case
          case this.oParentObject.w_CUR_STEP=1
            * --- Primo passo, interpretazione file EDI in XML intermedio
            this.w_PROC_OK = .T.
            if EMPTY(NVL(this.oParentObject.w_IMSERIAL, " "))
              * --- begin transaction
              cp_BeginTrs()
              i_Conn=i_TableProp[this.VAANAIMP_IDX, 3]
              cp_NextTableProg( This.oparentobject , i_Conn, "VASERIMP", "i_codazi,w_IMSERIAL")
              * --- Try
              local bErr_030857B0
              bErr_030857B0=bTrsErr
              this.Try_030857B0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                this.w_PROC_OK = .F.
                this.oParentObject.w_IMSERIAL = ""
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                addMsg("%0Errore inserimento anagrafica elaborazioni. Impossibile proseguire",this.w_PADRE)
              endif
              bTrsErr=bTrsErr or bErr_030857B0
              * --- End
            endif
            if this.w_PROC_OK AND (Nvl(this.oParentObject.w_IMSSTEPA," ") <> "O" or ah_yesno("Il passaggio attuale � gi� stato eseguito. Si desidera ripetere l'esecuzione?"))
              if Not Empty(this.oParentObject.w_STBACXML) 
                if this.oParentObject.w_IMSSTEPA="N"
                  * --- Try
                  local bErr_0306A810
                  bErr_0306A810=bTrsErr
                  this.Try_0306A810()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    addMsg("%0Errore aggiornamento anagrafica elaborazioni (Step A). Impossibile proseguire",this.w_PADRE)
                    this.w_PROC_OK = .F.
                  endif
                  bTrsErr=bTrsErr or bErr_0306A810
                  * --- End
                endif
                if this.w_PROC_OK
                  * --- Eseguo la routine per la creazione del file XML
                  this.oParentObject.w_OBJ__XML = .NULL.
                  this.oParentObject.w_OBJ__XML = createobject("Ah_XMLObject")
                   
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_MAC="This.w_PROC_OK = "+ALLTRIM(this.oParentObject.w_STBACXML)+" ( This , ALLTRIM(this.oParentObject.w_STCODICE),ALLTRIM(this.oParentObject.w_FILEINGR),this.oParentObject.w_OBJ__XML,"+; 
 "ALLTRIM(this.oParentObject.w_IMSERIAL),this.oParentObject.w_FLVERBOS,this.w_PADRE)=0" 
 &L_MAC 
 on error &L_OldError 
                  if L_err
                    addMsg("%0Errore nell'esecuzione del driver: %1" ,this.w_PADRE,Message())
                    this.w_PROC_OK = .F.
                  endif
                  this.oParentObject.w_IMSSTEPA = IIF(this.w_PROC_OK, "O", "E")
                  if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
                    this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(g_MSG,4000),g_MSG)
                     
 g_MSG=this.w_OLD_g_MSG+g_MSG
                  else
                    this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(this.w_PADRE.w_MSG,4000),this.w_PADRE.w_MSG)
                  endif
                  * --- Try
                  local bErr_03071F50
                  bErr_03071F50=bTrsErr
                  this.Try_03071F50()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    addMsg("%0Errore aggiornamento anagrafica elaborazioni (Step A). Impossibile proseguire",this.w_PADRE)
                    this.w_PROC_OK = .F.
                  endif
                  bTrsErr=bTrsErr or bErr_03071F50
                  * --- End
                endif
              else
                addMsg("%0Attenzione routine di creazione file XML non specificata nella struttura EDI",this.w_PADRE)
                this.w_PROC_OK = .F.
              endif
            endif
          case this.oParentObject.w_CUR_STEP=2
            this.w_PROC_OK = .T.
            if VARTYPE(this.oParentObject.w_OBJ__XML)<>"O"
              this.oParentObject.w_OBJ__XML = createobject("Ah_XMLObject")
              this.oParentObject.w_OBJ__XML.oxml.load(this.oParentObject.w_File_Xml)     
            endif
            if this.w_PROC_OK AND (Nvl(this.oParentObject.w_IMSSTEPB," ") <> "O" or ah_yesno("Il passaggio attuale � gi� stato eseguito. Si desidera ripetere l'esecuzione?"))
              if Not Empty(this.oParentObject.w_STDRIVER)
                if Not Empty(this.oParentObject.w_STFUNRIC)
                   
 Public g_ARRSEZ 
 DIMENSION g_ARRSEZ[1,2]
                   
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_MAC="This.w_PROC_OK = "+ALLTRIM(this.oParentObject.w_STDRIVER)+" ( This , ALLTRIM(this.oParentObject.w_STCODICE),ALLTRIM(this.oParentObject.w_FILE_XML),this.oParentObject.w_OBJ__XML,"+; 
 "ALLTRIM(this.oParentObject.w_IMSERIAL),ALLTRIM(this.oParentObject.w_STFUNRIC),this.oParentObject.w_FLVERBOS,this.w_PADRE)=0" 
 &L_MAC 
 on error &L_OldError 
                  this.w_oERRORLOG=createobject("AH_ErrorLog")
                  if L_err
                    addMsg("%0Errore nell'esecuzione del driver: %1" ,this.w_PADRE,Message())
                    this.w_PROC_OK = .F.
                  else
                    * --- Applico Valori predefiniti
                    * --- Applico valori predefiniti eventualmente previsti nella struttura 
                    *     sui campi che non presentano una valorizzazione specifica (a NULL)
                    * --- Read from VASTRUTT
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "STFLATAB"+;
                        " from "+i_cTable+" VASTRUTT where ";
                            +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_STCODICE);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        STFLATAB;
                        from (i_cTable) where;
                            STCODICE = this.oParentObject.w_STCODICE;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Try
                    local bErr_0444B060
                    bErr_0444B060=bTrsErr
                    this.Try_0444B060()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- rollback
                      bTrsErr=.t.
                      cp_EndTrs(.t.)
                      addMsg("%0Errore nell'applicazione dei valori predefiniti: %1" ,this.w_PADRE,Message())
                      this.w_PROC_OK = .F.
                    endif
                    bTrsErr=bTrsErr or bErr_0444B060
                    * --- End
                    if this.w_oErrorLog.IsFullLog()
                      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
                    endif
                    * --- Rilascio variabili globali
                    if this.w_PROC_OK and Not Empty(this.oParentObject.w_STCODICE)
                      L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t.
                      * --- Select from VAELEMEN
                      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select ELCODSTR,ELMEMDAT,ELCAMIMP  from "+i_cTable+" VAELEMEN ";
                            +" where "+cp_ToStrODBC(this.oParentObject.w_STCODICE)+"=ELCODSTR and ELMEMDAT='S'";
                             ,"_Curs_VAELEMEN")
                      else
                        select ELCODSTR,ELMEMDAT,ELCAMIMP from (i_cTable);
                         where this.oParentObject.w_STCODICE=ELCODSTR and ELMEMDAT="S";
                          into cursor _Curs_VAELEMEN
                      endif
                      if used('_Curs_VAELEMEN')
                        select _Curs_VAELEMEN
                        locate for 1=1
                        do while not(eof())
                        if Not Empty(Nvl(_Curs_VAELEMEN.ELCAMIMP," "))
                           
 L_test="vartype(g_"+Alltrim(Nvl(_Curs_VAELEMEN.ELCAMIMP," "))+")" 
 L_resu=&L_test 
 
                          if L_resu<>"U"
                             
 L_VAR="Release g_"+Alltrim(Nvl(_Curs_VAELEMEN.ELCAMIMP," ")) 
 &L_VAR 
 
                          endif
                        endif
                          select _Curs_VAELEMEN
                          continue
                        enddo
                        use
                      endif
                      on error &L_OldError 
                      if L_err
                        addMsg("%0Errore nel rilascio variabili: %1%2" ,this.w_PADRE,Message(),l_var)
                        this.w_PROC_OK = .F.
                      endif
                    endif
                  endif
                  Release g_ARRSEZ
                else
                  addMsg("%0Attenzione funzione ricorsiva per valorizzazione tabelle EDI, non specificata nella struttura EDI",this.w_PADRE)
                  this.w_PROC_OK = .F.
                endif
              else
                addMsg("%0Attenzione routine driver non specificata nella struttura EDI",this.w_PADRE)
                this.w_PROC_OK = .F.
              endif
              this.oParentObject.w_IMSSTEPB = IIF(this.w_PROC_OK, "O", "E")
              if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
                this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(g_MSG,4000),g_MSG)
                 
 g_MSG=this.w_OLD_g_MSG+g_MSG
              else
                this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(this.w_PADRE.w_MSG,4000),this.w_PADRE.w_MSG)
              endif
              * --- Try
              local bErr_04425538
              bErr_04425538=bTrsErr
              this.Try_04425538()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                addMsg("%0Errore aggiornamento anagrafica elaborazioni (Step B). Impossibile proseguire",this.w_PADRE)
                this.w_PROC_OK = .F.
              endif
              bTrsErr=bTrsErr or bErr_04425538
              * --- End
            endif
          case this.oParentObject.w_CUR_STEP=3
            this.w_PROC_OK = .T.
            this.w_ERRODIR = .T.
            if ! Directory(Alltrim(this.w_DIRSPO))
              this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_DIRSPO))
            endif
            if !this.w_ERRODIR
              AddMsgNL("%0Errore nella creazione della directory di spostamento!",this.w_PADRE)
            endif
            if !Cp_Fileexist(Substr(ADDBS(Alltrim(this.w_DIRSPO)),1,Len(ADDBS(Alltrim(this.w_DIRSPO)))-1))
              this.w_PROC_OK = .f.
              if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
                this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(g_MSG,4000),g_MSG)
                 
 g_MSG=this.w_OLD_g_MSG+g_MSG
              else
                this.oParentObject.w_MSG = iif(upper(CP_DBTYPE)="ORACLE",Left(this.w_PADRE.w_MSG,4000),this.w_PADRE.w_MSG)
              endif
              * --- Write into VAANAIMP
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.VAANAIMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IMSSTEPC ="+cp_NullLink(cp_ToStrODBC("E"),'VAANAIMP','IMSSTEPC');
                +",IMLSTEPC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'VAANAIMP','IMLSTEPC');
                    +i_ccchkf ;
                +" where ";
                    +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
                       )
              else
                update (i_cTable) set;
                    IMSSTEPC = "E";
                    ,IMLSTEPC = this.oParentObject.w_MSG;
                    &i_ccchkf. ;
                 where;
                    IMSERIAL = this.oParentObject.w_IMSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            if this.w_PROC_OK AND (Nvl(this.oParentObject.w_IMSSTEPC," ") <> "O" or ah_yesno("Il passaggio attuale � gi� stato eseguito. Si desidera ripetere l'esecuzione?"))
              if Not Empty(this.oParentObject.w_STBACIMP)
                 
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_MAC="This.w_PROC_OK =  "+ALLTRIM(this.oParentObject.w_STBACIMP)+"( This , ALLTRIM(this.oParentObject.w_STCODICE),ALLTRIM(this.oParentObject.w_IMSERIAL),this.w_PADRE,"+; 
 "this.oParentObject.w_ONLY_VERIF,this.oParentObject.w_FLVERBOS,this.pOBJMSG.w_GENERR)=0" 
 &L_MAC 
 on error &L_OldError 
                if L_err
                  addMsg("%0Errore nell'esecuzione della routine import: " ,this.w_PADRE)
                  this.w_PROC_OK = .F.
                endif
                this.oParentObject.w_IMSSTEPC = IIF(this.w_PROC_OK, "O", "E")
              else
                addMsg("%0Attenzione routine import non specificata nella struttura EDI",this.w_PADRE)
                this.w_PROC_OK = .F.
              endif
            endif
        endcase
        if this.w_PROC_OK
          * --- Svuoto il messaggio perch� posso procedere al passo seguente
          this.oParentObject.w_MSG = ""
          this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
        else
          addMsgnl("%0Si � verificato un errore durante l'elaborazione. Verificare il log dell'elaborazione",this.w_PADRE)
        endif
      case this.pOPER=="SELFI"
        if Empty(Nvl(this.w_DIRSPO," ")) or Empty(Nvl(this.w_DIRXML," ")) OR Empty(Nvl(this.w_DIRFIL," ")) or ! Directory(Alltrim(this.w_DIRFIL)) or ! Directory(Alltrim(this.w_DIRXML)) or ! Directory(Alltrim(this.w_DIRSPO))
          AddMsgNL("%0Attenzione! uno o pi� percorsi nei parametri non sono corretti oppure sono inesistenti!",this.w_PADRE)
          i_retcode = 'stop'
          return
        endif
        this.w_NOMEFILE = ""
        L_OldPath = Sys(5)+Sys(2003)
        L_SearchDefa= this.oParentObject.w_PATHFING
        Set Default To &L_SearchDefa 
        this.w_NOMEFILE = GetFile("")
        * --- Ripristino puntamento directory di partenza
        Set Default to &L_OldPath
        if !EMPTY(this.w_NOMEFILE)
          L_MAC="this.oParentObject.w_"+ALLTRIM(this.pOBJDEST)+" = '"+this.w_NOMEFILE+"'"
          &L_MAC
        endif
      case this.pOPER=="CAREL"
        * --- Read from VASTRUTT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VASTRUTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC,STFLATAB"+;
            " from "+i_cTable+" VASTRUTT where ";
                +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_STCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC,STFLATAB;
            from (i_cTable) where;
                STCODICE = this.oParentObject.w_STCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_STDESCRI = NVL(cp_ToDate(_read_.STDESCRI),cp_NullValue(_read_.STDESCRI))
          this.oParentObject.w_STMODMSK = NVL(cp_ToDate(_read_.STMODMSK),cp_NullValue(_read_.STMODMSK))
          this.oParentObject.w_STTIPGES = NVL(cp_ToDate(_read_.STTIPGES),cp_NullValue(_read_.STTIPGES))
          this.oParentObject.w_STBACIMP = NVL(cp_ToDate(_read_.STBACIMP),cp_NullValue(_read_.STBACIMP))
          this.oParentObject.w_STDRIVER = NVL(cp_ToDate(_read_.STDRIVER),cp_NullValue(_read_.STDRIVER))
          this.oParentObject.w_STBACXML = NVL(cp_ToDate(_read_.STBACXML),cp_NullValue(_read_.STBACXML))
          this.oParentObject.w_STFUNRIC = NVL(cp_ToDate(_read_.STFUNRIC),cp_NullValue(_read_.STFUNRIC))
          this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !EMPTY(this.oParentObject.w_SER_ELAB)
          do case
            case Nvl(this.oParentObject.w_IMSSTEPC,"N")<>"N" OR this.oParentObject.w_IMSSTEPB="O"
              this.oParentObject.w_CUR_STEP = 3
            case Nvl(this.oParentObject.w_IMSSTEPB,"N")<>"N" OR this.oParentObject.w_IMSSTEPA="O"
              this.oParentObject.w_CUR_STEP = 2
            otherwise
              this.oParentObject.w_CUR_STEP = 1
          endcase
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOPER=="NUOVO"
        this.w_PADRE.BlankRec()     
        this.w_PADRE.mCalc()     
        this.w_PADRE.SetControlsValue()     
        this.w_PADRE.mEnableControls()     
        this.w_PADRE.mHideControls()     
      case this.pOPER=="MANUT"
        if this.oParentObject.w_STTIPGES="M"
          L_MAC="do "+ALLTRIM(this.oParentObject.w_STMODMSK)+" with '"+ALLTRIM(this.oParentObject.w_SER_ELAB)+"#"+ALLTRIM(this.oParentObject.w_STCODICE)+"'"
        else
          L_MAC="do "+ALLTRIM(this.oParentObject.w_STMODMSK)+" with This , ALLTRIM(this.oParentObject.w_SER_ELAB),ALLTRIM(this.oParentObject.w_STCODICE)"
        endif
        &L_MAC
    endcase
    if Upper(this.w_PADRE.Class)="TGSVA_KWI"
      this.w_PADRE.oPGFRM.ActivePage=this.oParentObject.w_CUR_STEP
    endif
  endproc
  proc Try_030857B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VAANAIMP
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IMSERIAL"+",IMCODSTR"+",IMDATIMP"+",IMORAIMP"+",IMPATORI"+",IMFLREEZ"+",IMGRANUL"+",IMFLELAB"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMSERIAL),'VAANAIMP','IMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STCODICE),'VAANAIMP','IMCODSTR');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'VAANAIMP','IMDATIMP');
      +","+cp_NullLink(cp_ToStrODBC(TTOC(DATETIME(),2)),'VAANAIMP','IMORAIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FILEINGR),'VAANAIMP','IMPATORI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLFREEZE),'VAANAIMP','IMFLREEZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GENERR),'VAANAIMP','IMGRANUL');
      +","+cp_NullLink(cp_ToStrODBC(" "),'VAANAIMP','IMFLELAB');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IMSERIAL',this.oParentObject.w_IMSERIAL,'IMCODSTR',this.oParentObject.w_STCODICE,'IMDATIMP',i_DATSYS,'IMORAIMP',TTOC(DATETIME(),2),'IMPATORI',this.oParentObject.w_FILEINGR,'IMFLREEZ',this.oParentObject.w_FLFREEZE,'IMGRANUL',this.w_GENERR,'IMFLELAB'," ")
      insert into (i_cTable) (IMSERIAL,IMCODSTR,IMDATIMP,IMORAIMP,IMPATORI,IMFLREEZ,IMGRANUL,IMFLELAB &i_ccchkf. );
         values (;
           this.oParentObject.w_IMSERIAL;
           ,this.oParentObject.w_STCODICE;
           ,i_DATSYS;
           ,TTOC(DATETIME(),2);
           ,this.oParentObject.w_FILEINGR;
           ,this.oParentObject.w_FLFREEZE;
           ,this.w_GENERR;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.oParentObject.w_IMSSTEPA = "N"
    this.oParentObject.w_IMSSTEPB = "N"
    this.oParentObject.w_IMSSTEPC = "N"
    this.oParentObject.w_SER_ELAB = this.oParentObject.w_IMSERIAL
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0306A810()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STCODICE),'VAANAIMP','IMCODSTR');
      +",IMPATORI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FILEINGR),'VAANAIMP','IMPATORI');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
             )
    else
      update (i_cTable) set;
          IMCODSTR = this.oParentObject.w_STCODICE;
          ,IMPATORI = this.oParentObject.w_FILEINGR;
          &i_ccchkf. ;
       where;
          IMSERIAL = this.oParentObject.w_IMSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03071F50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Leggo il nome e la posizione del file XML
    * --- Read from VAANAIMP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IMPATXML,IMSSTEPA"+;
        " from "+i_cTable+" VAANAIMP where ";
            +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IMPATXML,IMSSTEPA;
        from (i_cTable) where;
            IMSERIAL = this.oParentObject.w_IMSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_FILE_XML = NVL(cp_ToDate(_read_.IMPATXML),cp_NullValue(_read_.IMPATXML))
      this.w_STEPA = NVL(cp_ToDate(_read_.IMSSTEPA),cp_NullValue(_read_.IMSSTEPA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !(this.pOBJMSG.w_GENERR="C" and this.w_STEPA="E")
      this.w_STEPA = this.oParentObject.w_IMSSTEPA
    endif
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMSSTEPA ="+cp_NullLink(cp_ToStrODBC(this.w_STEPA),'VAANAIMP','IMSSTEPA');
      +",IMLSTEPA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'VAANAIMP','IMLSTEPA');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
             )
    else
      update (i_cTable) set;
          IMSSTEPA = this.w_STEPA;
          ,IMLSTEPA = this.oParentObject.w_MSG;
          &i_ccchkf. ;
       where;
          IMSERIAL = this.oParentObject.w_IMSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0444B060()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from VAPREDEF
    i_nConn=i_TableProp[this.VAPREDEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAPREDEF_idx,2],.t.,this.VAPREDEF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" VAPREDEF ";
          +" where PRCODSTR="+cp_ToStrODBC(this.oParentObject.w_STCODICE)+"";
          +" order by CPROWORD";
           ,"_Curs_VAPREDEF")
    else
      select * from (i_cTable);
       where PRCODSTR=this.oParentObject.w_STCODICE;
       order by CPROWORD;
        into cursor _Curs_VAPREDEF
    endif
    if used('_Curs_VAPREDEF')
      select _Curs_VAPREDEF
      locate for 1=1
      do while not(eof())
      this.w_OKNULL = .F.
      this.w_TIPVAL = Nvl(_Curs_VAPREDEF.PR__TIPO,"F")
      this.w_CODTAB = alltrim(Nvl(_Curs_VAPREDEF.PRCODTAB,space(30)))
      this.w_LCAMPO = Nvl(_Curs_VAPREDEF.PR_CAMPO,space(30))
      this.w_CODTABVAL = alltrim(Nvl(_Curs_VAPREDEF.PR_TABLE,space(30)))
      this.w_CAMPOVAL = Nvl(_Curs_VAPREDEF.PR_FIELD,space(30))
      this.w_LTIPCAM = _Curs_VAPREDEF.PRTIPCAM
      this.w_PHNAME = GetETPhName(ALLTRIM(this.w_FLATAB),this.w_CODTAB)
      Dimension ArrWrite[1,2],ArrVal[2,3]
      * --- Read from XDC_FIELDS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FLDECIMA,FLLENGHT"+;
          " from "+i_cTable+" XDC_FIELDS where ";
              +"TBNAME = "+cp_ToStrODBC(this.w_CODTAB);
              +" and FLNAME = "+cp_ToStrODBC(this.w_LCAMPO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FLDECIMA,FLLENGHT;
          from (i_cTable) where;
              TBNAME = this.w_CODTAB;
              and FLNAME = this.w_LCAMPO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECIMAL = NVL(cp_ToDate(_read_.FLDECIMA),cp_NullValue(_read_.FLDECIMA))
        this.w_LENGHT = NVL(cp_ToDate(_read_.FLLENGHT),cp_NullValue(_read_.FLLENGHT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
       
 ArrVal[1,1]="FT___UID" 
 ArrVal[1,2]=this.oParentObject.w_IMSERIAL 
 ArrVal[1,3]= "="
      do case
        case this.w_LTIPCAM="D"
           
 ArrVal[2,1]=Alltrim(this.w_LCAMPO) 
 ArrVal[2,2]="" 
 ArrVal[2,3]= "IS NULL"
          if this.w_TIPVAL="F"
            VALORE=cp_todate(_Curs_VAPREDEF.PRVALDAT)
          endif
        case this.w_LTIPCAM="N"
          if this.w_TIPVAL="F"
            VALORE=Nvl(_Curs_VAPREDEF.PRVALNUM,0)
          endif
           
 ArrVal[2,1]=Alltrim(this.w_LCAMPO) 
 ArrVal[2,2]=0 
 ArrVal[2,3]= "="
          this.w_OKNULL = .T.
        otherwise
          if this.w_LTIPCAM="M"
             
 VALORE=_Curs_VAPREDEF.PRVALCAR
          else
             
 VALORE=SUBSTR(_Curs_VAPREDEF.PRVALCAR,1,this.w_LENGHT)
          endif
          if this.w_TIPVAL="F"
            VALORE=SUBSTR(_Curs_VAPREDEF.PRVALCAR,1,this.w_LENGHT)
          endif
           
 ArrVal[2,1]=Alltrim(this.w_LCAMPO) 
          this.w_OKNULL = .T.
          ArrVal[2,2]=" " 
 ArrVal[2,3]= "="
      endcase
      do case
        case this.w_TIPVAL="V"
          if Not Empty(this.w_CAMPOVAL)
            this.w_LCODTAB = GetETPhname(this.w_FLATAB, Alltrim(this.w_CODTABVAL))
            if Upper(this.w_CODTABVAL)="DOC_MAST"
              Dimension ArrVal[3,3]
               
 NC=READTABLE(this.w_LCODTAB,Alltrim(this.w_CAMPOVAL) + ",MVSERIAL","","",.F.,"FT___UID='"+this.oParentObject.w_SER_ELAB+"'")
              if Reccount((Nc)) <>0
                Select (NC) 
 Go Top 
 Scan
                ArrVal[3,1]="MVSERIAL" 
 ArrVal[3,2]=MVSERIAL 
 ArrVal[3,3]= "="
                Valore=Evaluate(Alltrim(this.w_CAMPOVAL))
                this.Pag3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                Endscan
              endif
            else
               
 NC=READTABLE(this.w_LCODTAB,Alltrim(this.w_CAMPOVAL),"","",.F.,"FT___UID='"+this.oParentObject.w_SER_ELAB+"'")
              if Reccount((Nc)) <>0
                Valore=Evaluate(Alltrim(this.w_CAMPOVAL))
              endif
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Chiudo cursore read
          if Used((NC))
             
 Select (NC) 
 use
          endif
        case this.w_TIPVAL="G"
           
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_test="vartype(g_"+Alltrim(this.w_LCAMPO)+")" 
 L_resu=&L_test 
 
          if L_resu<>"U"
             
 L_VAR= " VALORE = " + "g_"+Alltrim(this.w_LCAMPO) 
 &L_VAR 
 
          endif
          on error &L_OldError 
          if L_err
            addMsg("%0Errore applicazione valori predefiniti: %1%2" ,this.w_PADRE,Message(),l_var)
            this.w_PROC_OK = .F.
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        otherwise
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
        select _Curs_VAPREDEF
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04425538()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.pOBJMSG.w_GENERR="C"
      * --- Read from VAANAIMP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAANAIMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMSSTEPB"+;
          " from "+i_cTable+" VAANAIMP where ";
              +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMSSTEPB;
          from (i_cTable) where;
              IMSERIAL = this.oParentObject.w_IMSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STEPB = NVL(cp_ToDate(_read_.IMSSTEPB),cp_NullValue(_read_.IMSSTEPB))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !(this.pOBJMSG.w_GENERR="C" and this.w_STEPB="E")
      this.w_STEPB = this.oParentObject.w_IMSSTEPB
    endif
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMSSTEPB ="+cp_NullLink(cp_ToStrODBC(this.w_STEPB),'VAANAIMP','IMSSTEPB');
      +",IMLSTEPB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'VAANAIMP','IMLSTEPB');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
             )
    else
      update (i_cTable) set;
          IMSSTEPB = this.w_STEPB;
          ,IMLSTEPB = this.oParentObject.w_MSG;
          &i_ccchkf. ;
       where;
          IMSERIAL = this.oParentObject.w_IMSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_CUR_STEP=1
        if !EMPTY(this.oParentObject.w_IMSERIAL)
          * --- Read from VAANAIMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAANAIMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IMLSTEPA"+;
              " from "+i_cTable+" VAANAIMP where ";
                  +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IMLSTEPA;
              from (i_cTable) where;
                  IMSERIAL = this.oParentObject.w_IMSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.IMLSTEPA),cp_NullValue(_read_.IMLSTEPA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.oParentObject.w_CUR_STEP=2
        if !EMPTY(this.oParentObject.w_IMSERIAL)
          * --- Read from VAANAIMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAANAIMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IMLSTEPB"+;
              " from "+i_cTable+" VAANAIMP where ";
                  +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IMLSTEPB;
              from (i_cTable) where;
                  IMSERIAL = this.oParentObject.w_IMSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.IMLSTEPB),cp_NullValue(_read_.IMLSTEPB))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.oParentObject.w_CUR_STEP=3
        if !EMPTY(this.oParentObject.w_IMSERIAL)
          * --- Read from VAANAIMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAANAIMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IMLSTEPC"+;
              " from "+i_cTable+" VAANAIMP where ";
                  +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IMLSTEPC;
              from (i_cTable) where;
                  IMSERIAL = this.oParentObject.w_IMSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MSG = NVL(cp_ToDate(_read_.IMLSTEPC),cp_NullValue(_read_.IMLSTEPC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Type("valore")="C"
       
 ArrWrite[1,1]=this.w_LCAMPO 
 ArrWrite[1,2]=Alltrim(valore)
    else
       
 ArrWrite[1,1]=this.w_LCAMPO 
 ArrWrite[1,2]=valore
    endif
    I_ERROR=WRITETABLE( this.w_PHNAME , @ArrWrite , @ArrVal , this.w_oERRORLOG ) 
    if this.w_OKNULL
      ArrVal[2,2]="" 
 ArrVal[2,3]= "IS NULL"
      I_ERROR=WRITETABLE( this.w_PHNAME , @ArrWrite , @ArrVal , this.w_oERRORLOG ) 
    endif
  endproc


  proc Init(oParentObject,pOPER,pOBJDEST,pOBJMSG)
    this.pOPER=pOPER
    this.pOBJDEST=pOBJDEST
    this.pOBJMSG=pOBJMSG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='VAANAIMP'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='PAR_VEFA'
    this.cWorkTables[4]='VAELEMEN'
    this.cWorkTables[5]='VAPREDEF'
    this.cWorkTables[6]='XDC_FIELDS'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_VAPREDEF')
      use in _Curs_VAPREDEF
    endif
    if used('_Curs_VAELEMEN')
      use in _Curs_VAELEMEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsva_bwi
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pOBJDEST,pOBJMSG"
endproc
