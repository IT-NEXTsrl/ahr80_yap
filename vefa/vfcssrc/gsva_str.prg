* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_str                                                        *
*              Stampa associazione trascodifiche                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_10]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-24                                                      *
* Last revis.: 2008-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_str",oParentObject))

* --- Class definition
define class tgsva_str as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 653
  Height = 151
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-04-03"
  HelpContextID=74084969
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  VAELEMEN_IDX = 0
  cPrg = "gsva_str"
  cComment = "Stampa associazione trascodifiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TRCODSTR = space(20)
  w_STDESCRI = space(30)
  w_TRCODELE = space(20)
  w_DESELE = space(45)
  w_TIPO = space(1)
  w_TIPTRA = space(1)
  w_TIPTR2 = space(1)
  w_CAMPO = space(30)
  w_CAMP1 = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_strPag1","gsva_str",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODSTR_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAELEMEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODSTR=space(20)
      .w_STDESCRI=space(30)
      .w_TRCODELE=space(20)
      .w_DESELE=space(45)
      .w_TIPO=space(1)
      .w_TIPTRA=space(1)
      .w_TIPTR2=space(1)
      .w_CAMPO=space(30)
      .w_CAMP1=space(30)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_TRCODSTR))
          .link_1_3('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_TRCODELE))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
    this.DoRTCalc(4,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODSTR
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_TRCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_TRCODSTR))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oTRCODSTR_1_3'),i_cWhere,'',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_TRCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_TRCODSTR)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODSTR = NVL(_Link_.STCODICE,space(20))
      this.w_STDESCRI = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODSTR = space(20)
      endif
      this.w_STDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODELE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODELE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_TRCODELE)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_TRCODSTR;
                     ,'ELCODICE',trim(this.w_TRCODELE))
          select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODELE)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODELE) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oTRCODELE_1_5'),i_cWhere,'',"Elementi",'gsva_atr.VAELEMEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TRCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascidifica incongruente o con campo non valorizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODELE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_TRCODELE);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_TRCODSTR;
                       ,'ELCODICE',this.w_TRCODELE)
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODELE = NVL(_Link_.ELCODICE,space(20))
      this.w_DESELE = NVL(_Link_.ELDESCRI,space(45))
      this.w_TIPO = NVL(_Link_.EL__TIPO,space(1))
      this.w_CAMPO = NVL(_Link_.EL_CAMPO,space(30))
      this.w_TIPTRA = NVL(_Link_.ELTIPTRA,space(1))
      this.w_TIPTR2 = NVL(_Link_.ELTIPTR2,space(1))
      this.w_CAMP1 = NVL(_Link_.ELCAMIMP,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODELE = space(20)
      endif
      this.w_DESELE = space(45)
      this.w_TIPO = space(1)
      this.w_CAMPO = space(30)
      this.w_TIPTRA = space(1)
      this.w_TIPTR2 = space(1)
      this.w_CAMP1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO $ 'V-E'  AND ((.w_TIPTRA $ 'S-I'  and NOT EMPTY(.w_CAMPO)) OR ((.w_TIPTR2 $ 'S-I') and  NOT EMPTY(.w_CAMP1)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascidifica incongruente o con campo non valorizzato")
        endif
        this.w_TRCODELE = space(20)
        this.w_DESELE = space(45)
        this.w_TIPO = space(1)
        this.w_CAMPO = space(30)
        this.w_TIPTRA = space(1)
        this.w_TIPTR2 = space(1)
        this.w_CAMP1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODELE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODSTR_1_3.value==this.w_TRCODSTR)
      this.oPgFrm.Page1.oPag.oTRCODSTR_1_3.value=this.w_TRCODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_4.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_4.value=this.w_STDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODELE_1_5.value==this.w_TRCODELE)
      this.oPgFrm.Page1.oPag.oTRCODELE_1_5.value=this.w_TRCODELE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_6.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_6.value=this.w_DESELE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPO $ 'V-E'  AND ((.w_TIPTRA $ 'S-I'  and NOT EMPTY(.w_CAMPO)) OR ((.w_TIPTR2 $ 'S-I') and  NOT EMPTY(.w_CAMP1))))  and not(empty(.w_TRCODELE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODELE_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascidifica incongruente o con campo non valorizzato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_strPag1 as StdContainer
  Width  = 649
  height = 151
  stdWidth  = 649
  stdheight = 151
  resizeXpos=380
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODSTR_1_3 as StdField with uid="XLZVBKYZOJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODSTR", cQueryName = "TRCODSTR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 53024136,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=156, Top=13, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VASTRUTT", oKey_1_1="STCODICE", oKey_1_2="this.w_TRCODSTR"

  func oTRCODSTR_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_TRCODELE)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTRCODSTR_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODSTR_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oTRCODSTR_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture",'',this.parent.oContained
  endproc

  add object oSTDESCRI_1_4 as StdField with uid="MEPPMYUBNR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 68101999,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=315, Top=13, InputMask=replicate('X',30)

  add object oTRCODELE_1_5 as StdField with uid="DJZUCVEJMX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRCODELE", cQueryName = "TRCODELE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione elemento non di tipo valore/espressione, tipo trascidifica incongruente o con campo non valorizzato",;
    HelpContextID = 181856901,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=156, Top=42, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", oKey_1_1="ELCODSTR", oKey_1_2="this.w_TRCODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_TRCODELE"

  func oTRCODELE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODELE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODELE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_TRCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_TRCODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oTRCODELE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elementi",'gsva_atr.VAELEMEN_VZM',this.parent.oContained
  endproc

  add object oDESELE_1_6 as StdField with uid="QGZULXASBO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 174061770,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=315, Top=42, InputMask=replicate('X',45)


  add object oObj_1_7 as cp_outputCombo with uid="ZLERXIVPWT",left=156, top=72, width=480,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103912678


  add object oBtn_1_8 as StdButton with uid="CCGSUHUDGX",left=537, top=100, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 74064410;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="YXYHLPQOEL",left=588, top=100, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 248635398;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="MHSJGPKKJF",Visible=.t., Left=10, Top=15,;
    Alignment=1, Width=141, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="DURCNPNXWQ",Visible=.t., Left=10, Top=43,;
    Alignment=1, Width=141, Height=15,;
    Caption="Elemento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=10, Top=72,;
    Alignment=1, Width=141, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_str','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
