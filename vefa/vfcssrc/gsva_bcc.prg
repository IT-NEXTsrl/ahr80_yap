* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bcc                                                        *
*              CHECK DEFINIZIONE CHIAVI PRIMARIE TABELLE EDI                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-27                                                      *
* Last revis.: 2007-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEDITABLE,pTABLE,pSERIAL,pVERBOSE,pOBJLOG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bcc",oParentObject,m.pEDITABLE,m.pTABLE,m.pSERIAL,m.pVERBOSE,m.pOBJLOG)
return(i_retval)

define class tgsva_bcc as StdBatch
  * --- Local variables
  pEDITABLE = space(10)
  pTABLE = space(10)
  pSERIAL = space(10)
  pVERBOSE = .f.
  pOBJLOG = .NULL.
  w_KEY_FLDS = space(254)
  w_PHNAME_ET = space(254)
  w_STATUS_ET = 0
  w_VALRET = 0
  w_RESREAD = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_VALRET = -1
    * --- Carico dizionario
    if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
      AddMsgNL("Caricamento dizionario dati", this.pOBJLOG )
    endif
    cp_readxdc()
    if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
      AddMsgNL("Identificazione chiave tabella", this.pOBJLOG)
    endif
    this.w_KEY_FLDS = cp_KeyToSQL ( I_DCX.GetIdxDef( this.pTable ,1 ) )
    this.w_KEY_FLDS = ALLTRIM(this.w_KEY_FLDS)
    if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
      AddMsgNL("Identificazione nome fisico tabella EDI", this.pOBJLOG)
    endif
    this.w_PHNAME_ET = ALLTRIM(GetETPhname( this.pEDITABLE , this.pTABLE ))
    if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
      AddMsgNL("Verifica stato tabella %1", this.pOBJLOG, this.w_PHNAME_ET)
    endif
    this.w_STATUS_ET = ChkETStatus( this.pEDITABLE , this.pTABLE )
    if this.w_STATUS_ET=0
      if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
        AddMsgNL("Verifica chiavi tabella", this.pOBJLOG)
      endif
      this.w_RESREAD = ReadTable(this.w_PHNAME_ET, this.w_KEY_FLDS, "", this.pOBJLOG, this.pVERBOSE, "1=0")
      if EMPTY(this.w_RESREAD)
        if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
          AddMsgNL("Nella tabella %1 non sono presenti uno o pi� campi chiave della lista %2", this.pOBJLOG, this.w_PHNAME_ET,this.w_KEY_FLDS)
        endif
        this.w_VALRET = -1
      else
        if VARTYPE(this.pOBJLOG)="O" AND this.pVERBOSE
          AddMsgNL("Lettura campi chiave avvenuta correttamente", this.pOBJLOG)
        endif
        if USED(this.w_RESREAD)
          USE IN (this.w_RESREAD)
        endif
        this.w_VALRET = 0
      endif
    else
      if VARTYPE(this.pOBJLOG)="O"
        AddMsgNL("La tabella %1 non � aggiornata correttamente", this.pOBJLOG, this.w_PHNAME_ET)
      endif
      this.w_VALRET = -1
    endif
    i_retcode = 'stop'
    i_retval = this.w_VALRET
    return
  endproc


  proc Init(oParentObject,pEDITABLE,pTABLE,pSERIAL,pVERBOSE,pOBJLOG)
    this.pEDITABLE=pEDITABLE
    this.pTABLE=pTABLE
    this.pSERIAL=pSERIAL
    this.pVERBOSE=pVERBOSE
    this.pOBJLOG=pOBJLOG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEDITABLE,pTABLE,pSERIAL,pVERBOSE,pOBJLOG"
endproc
