* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bcx                                                        *
*              CARICA TABELLE EDI IN XDCTABLE                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-19                                                      *
* Last revis.: 2008-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTABLE,pNoTrans
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bcx",oParentObject,m.pTABLE,m.pNoTrans)
return(i_retval)

define class tgsva_bcx as StdBatch
  * --- Local variables
  pTABLE = space(30)
  pNoTrans = .f.
  w_TABNAME = space(30)
  w_OLDTBNAME = space(30)
  w_FTFL_AZI = space(1)
  w_FTDESCRI = space(30)
  w_FTCODICE = space(30)
  w_FTFLDNAM = space(30)
  w_FTFLDTYP = space(1)
  w_FTFLDDIM = 0
  w_FTFLDDEC = 0
  w_FTFLDDES = space(50)
  w_FLDNUM = 0
  w_FLATABAZI = .f.
  w_TBPHNAME = space(30)
  * --- WorkFile variables
  XDC_TABLE_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tabella da aggiornare (Opzionale, se non passato aggiorna tutte le tabelle EDI)
    this.pTABLE = IIF(VARTYPE(this.pTABLE)="C", this.pTABLE, "")
    this.w_OLDTBNAME = REPL("#",30)
    vq_exec("..\vefa\exe\query\gsva_bcx",this,"EDITable")
    SELECT "EDITable"
    GO TOP
    SCAN
    this.w_FTCODICE = EDITable.FTCODICE
    this.w_FTDESCRI = NVL(EDITable.FTDESCRI," ")
    this.w_FTFL_AZI = EDITable.FTFL_AZI
    this.w_TABNAME = EDITable.FTTABNAM
    this.w_FTFLDNAM = EDITable.FTFLDNAM
    this.w_FTFLDTYP = EDITable.FTFLDTYP
    this.w_FTFLDDIM = EDITable.FTFLDDIM
    this.w_FTFLDDEC = EDITable.FTFLDDEC
    this.w_FTFLDDES = NVL(EDITable.FTFLDDES," ")
    this.w_FLATABAZI = this.w_FTFL_AZI="S"
    this.w_TBPHNAME = GetETPhName( this.w_FTCODICE, this.w_TABNAME)
    * --- Try
    local bErr_035EE420
    bErr_035EE420=bTrsErr
    this.Try_035EE420()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if Not this.pNoTrans
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    bTrsErr=bTrsErr or bErr_035EE420
    * --- End
    SELECT "EDITable"
    ENDSCAN
    if USED("EDITable")
      USE IN "EDITable"
    endif
  endproc
  proc Try_035EE420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not this.pNoTrans
      * --- begin transaction
      cp_BeginTrs()
    endif
    ah_msg( "Caricamento tabella EDI %1...",.t.,.f.,.f.,alltrim(this.w_TABNAME) +chr(32)+chr(46)+chr(32)+ alltrim(this.w_FTFLDDES))
    if this.w_OLDTBNAME<>this.w_TABNAME
      this.w_OLDTBNAME = this.w_TABNAME
      this.w_FLDNUM = 0
      * --- Try
      local bErr_035EDAF0
      bErr_035EDAF0=bTrsErr
      this.Try_035EDAF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_035EDAF0
      * --- End
    endif
    this.w_FLDNUM = this.w_FLDNUM + 1
    * --- Try
    local bErr_035EE180
    bErr_035EE180=bTrsErr
    this.Try_035EE180()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_035EE180
    * --- End
    if Not this.pNoTrans
      * --- commit
      cp_EndTrs(.t.)
    endif
    return
  proc Try_035EDAF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into XDC_TABLE
    i_nConn=i_TableProp[this.XDC_TABLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_TABLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.XDC_TABLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TBNAME"+",TBPHNAME"+",TBCOMMENT"+",TBCOMPANY"+",TIMESTAMP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TABNAME),'XDC_TABLE','TBNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TBPHNAME),'XDC_TABLE','TBPHNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTDESCRI ),'XDC_TABLE','TBCOMMENT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLATABAZI),'XDC_TABLE','TBCOMPANY');
      +","+cp_NullLink(cp_ToStrODBC(TTOC(DATETIME(),1)),'XDC_TABLE','TIMESTAMP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TBNAME',this.w_TABNAME,'TBPHNAME',this.w_TBPHNAME,'TBCOMMENT',this.w_FTDESCRI ,'TBCOMPANY',this.w_FLATABAZI,'TIMESTAMP',TTOC(DATETIME(),1))
      insert into (i_cTable) (TBNAME,TBPHNAME,TBCOMMENT,TBCOMPANY,TIMESTAMP &i_ccchkf. );
         values (;
           this.w_TABNAME;
           ,this.w_TBPHNAME;
           ,this.w_FTDESCRI ;
           ,this.w_FLATABAZI;
           ,TTOC(DATETIME(),1);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_035EE180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into XDC_FIELDS
    i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.XDC_FIELDS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TBNAME"+",CPROWNUM"+",FLNAME"+",FLTYPE"+",FLLENGHT"+",FLDECIMA"+",FLCOMMEN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TABNAME),'XDC_FIELDS','TBNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLDNUM),'XDC_FIELDS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDNAM),'XDC_FIELDS','FLNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDTYP),'XDC_FIELDS','FLTYPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDIM),'XDC_FIELDS','FLLENGHT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDEC),'XDC_FIELDS','FLDECIMA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTFLDDES),'XDC_FIELDS','FLCOMMEN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TBNAME',this.w_TABNAME,'CPROWNUM',this.w_FLDNUM,'FLNAME',this.w_FTFLDNAM,'FLTYPE',this.w_FTFLDTYP,'FLLENGHT',this.w_FTFLDDIM,'FLDECIMA',this.w_FTFLDDEC,'FLCOMMEN',this.w_FTFLDDES)
      insert into (i_cTable) (TBNAME,CPROWNUM,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN &i_ccchkf. );
         values (;
           this.w_TABNAME;
           ,this.w_FLDNUM;
           ,this.w_FTFLDNAM;
           ,this.w_FTFLDTYP;
           ,this.w_FTFLDDIM;
           ,this.w_FTFLDDEC;
           ,this.w_FTFLDDES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTABLE,pNoTrans)
    this.pTABLE=pTABLE
    this.pNoTrans=pNoTrans
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTABLE,pNoTrans"
endproc
