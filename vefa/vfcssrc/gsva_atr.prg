* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_atr                                                        *
*              Associazione trascodifiche                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-11                                                      *
* Last revis.: 2015-08-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_atr"))

* --- Class definition
define class tgsva_atr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 781
  Height = 212+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-08-24"
  HelpContextID=92959337
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  VATRASCO_IDX = 0
  TRS_MAST_IDX = 0
  VASTRUTT_IDX = 0
  VAELEMEN_IDX = 0
  CONTI_IDX = 0
  cFile = "VATRASCO"
  cKeySelect = "TRCODSTR,TRCODENT,TRCODELE,TRTIPCON,TRCODRAG,TRCODCON"
  cKeyWhere  = "TRCODSTR=this.w_TRCODSTR and TRCODENT=this.w_TRCODENT and TRCODELE=this.w_TRCODELE and TRTIPCON=this.w_TRTIPCON and TRCODRAG=this.w_TRCODRAG and TRCODCON=this.w_TRCODCON"
  cKeyWhereODBC = '"TRCODSTR="+cp_ToStrODBC(this.w_TRCODSTR)';
      +'+" and TRCODENT="+cp_ToStrODBC(this.w_TRCODENT)';
      +'+" and TRCODELE="+cp_ToStrODBC(this.w_TRCODELE)';
      +'+" and TRTIPCON="+cp_ToStrODBC(this.w_TRTIPCON)';
      +'+" and TRCODRAG="+cp_ToStrODBC(this.w_TRCODRAG)';
      +'+" and TRCODCON="+cp_ToStrODBC(this.w_TRCODCON)';

  cKeyWhereODBCqualified = '"VATRASCO.TRCODSTR="+cp_ToStrODBC(this.w_TRCODSTR)';
      +'+" and VATRASCO.TRCODENT="+cp_ToStrODBC(this.w_TRCODENT)';
      +'+" and VATRASCO.TRCODELE="+cp_ToStrODBC(this.w_TRCODELE)';
      +'+" and VATRASCO.TRTIPCON="+cp_ToStrODBC(this.w_TRTIPCON)';
      +'+" and VATRASCO.TRCODRAG="+cp_ToStrODBC(this.w_TRCODRAG)';
      +'+" and VATRASCO.TRCODCON="+cp_ToStrODBC(this.w_TRCODCON)';

  cPrg = "gsva_atr"
  cComment = "Associazione trascodifiche "
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCODSTR = space(10)
  w_TRCODENT = space(15)
  w_TRCODELE = space(20)
  o_TRCODELE = space(20)
  w_TIPTRA = space(1)
  w_TRTIPCON = space(1)
  o_TRTIPCON = space(1)
  w_TRCODRAG = space(10)
  o_TRCODRAG = space(10)
  w_TRCODCON = space(15)
  o_TRCODCON = space(15)
  w_CODCON = space(15)
  w_DESSTR = space(30)
  w_CODTAB = space(30)
  w_DESELE = space(50)
  w_CAMPO = space(30)
  w_TABIMP = space(30)
  w_CAMP1 = space(30)
  w_TRCODTRA = space(20)
  w_TRCODTRI = space(20)
  w_TIPO = space(1)
  w_DESTRA = space(50)
  w_DESINT = space(40)
  w_DESTRI = space(50)
  w_TIPTR2 = space(1)
  w_FLPROV = space(1)
  w_CAMTRA = space(30)
  w_TABEL1 = space(30)
  w_CAMTRI = space(30)
  w_TABEL = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VATRASCO','gsva_atr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_atrPag1","gsva_atr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Associazione trascodifica")
      .Pages(1).HelpContextID = 41685996
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODSTR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TRS_MAST'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='VAELEMEN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='VATRASCO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VATRASCO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VATRASCO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TRCODSTR = NVL(TRCODSTR,space(10))
      .w_TRCODENT = NVL(TRCODENT,space(15))
      .w_TRCODELE = NVL(TRCODELE,space(20))
      .w_TRTIPCON = NVL(TRTIPCON,space(1))
      .w_TRCODRAG = NVL(TRCODRAG,space(10))
      .w_TRCODCON = NVL(TRCODCON,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VATRASCO where TRCODSTR=KeySet.TRCODSTR
    *                            and TRCODENT=KeySet.TRCODENT
    *                            and TRCODELE=KeySet.TRCODELE
    *                            and TRTIPCON=KeySet.TRTIPCON
    *                            and TRCODRAG=KeySet.TRCODRAG
    *                            and TRCODCON=KeySet.TRCODCON
    *
    i_nConn = i_TableProp[this.VATRASCO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VATRASCO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VATRASCO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VATRASCO '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODSTR',this.w_TRCODSTR  ,'TRCODENT',this.w_TRCODENT  ,'TRCODELE',this.w_TRCODELE  ,'TRTIPCON',this.w_TRTIPCON  ,'TRCODRAG',this.w_TRCODRAG  ,'TRCODCON',this.w_TRCODCON  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPTRA = space(1)
        .w_DESSTR = space(30)
        .w_CODTAB = space(30)
        .w_DESELE = space(50)
        .w_CAMPO = space(30)
        .w_TABIMP = space(30)
        .w_CAMP1 = space(30)
        .w_TIPO = space(1)
        .w_DESTRA = space(50)
        .w_DESTRI = space(50)
        .w_TIPTR2 = space(1)
        .w_FLPROV = space(1)
        .w_CAMTRA = space(30)
        .w_TABEL1 = space(30)
        .w_CAMTRI = space(30)
        .w_TABEL = space(30)
        .w_TRCODSTR = NVL(TRCODSTR,space(10))
          if link_1_1_joined
            this.w_TRCODSTR = NVL(STCODICE101,NVL(this.w_TRCODSTR,space(10)))
            this.w_DESSTR = NVL(STDESCRI101,space(30))
            this.w_TRCODENT = NVL(STCODENT101,space(15))
            this.w_FLPROV = NVL(STFLPROV101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_TRCODENT = NVL(TRCODENT,space(15))
        .w_TRCODELE = NVL(TRCODELE,space(20))
          if link_1_4_joined
            this.w_TRCODELE = NVL(ELCODICE104,NVL(this.w_TRCODELE,space(20)))
            this.w_DESELE = NVL(ELDESCRI104,space(50))
            this.w_TIPO = NVL(EL__TIPO104,space(1))
            this.w_CODTAB = NVL(ELCODTAB104,space(30))
            this.w_CAMPO = NVL(EL_CAMPO104,space(30))
            this.w_TIPTRA = NVL(ELTIPTRA104,space(1))
            this.w_TIPTR2 = NVL(ELTIPTR2104,space(1))
            this.w_CAMP1 = NVL(ELCAMIMP104,space(30))
            this.w_TABIMP = NVL(ELTABIMP104,space(30))
          else
          .link_1_4('Load')
          endif
        .w_TRTIPCON = NVL(TRTIPCON,space(1))
        .w_TRCODRAG = NVL(TRCODRAG,space(10))
        .w_TRCODCON = NVL(TRCODCON,space(15))
        .w_CODCON = .w_TRCODCON
          .link_1_12('Load')
        .w_TRCODTRA = NVL(TRCODTRA,space(20))
          if link_1_20_joined
            this.w_TRCODTRA = NVL(TRCODICE120,NVL(this.w_TRCODTRA,space(20)))
            this.w_DESTRA = NVL(TRDESCRI120,space(50))
            this.w_TABEL = NVL(TR_TABEL120,space(30))
            this.w_CAMTRA = NVL(TR_CAMPO120,space(30))
          else
          .link_1_20('Load')
          endif
        .w_TRCODTRI = NVL(TRCODTRI,space(20))
          if link_1_21_joined
            this.w_TRCODTRI = NVL(TRCODICE121,NVL(this.w_TRCODTRI,space(20)))
            this.w_DESTRI = NVL(TRDESCRI121,space(50))
            this.w_TABEL1 = NVL(TR_TABEL121,space(30))
            this.w_CAMTRI = NVL(TR_CAMPO121,space(30))
          else
          .link_1_21('Load')
          endif
        .w_DESINT = IIF(empty(.w_DESINT),SPACE(40),.w_DESINT)
        cp_LoadRecExtFlds(this,'VATRASCO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODSTR = space(10)
      .w_TRCODENT = space(15)
      .w_TRCODELE = space(20)
      .w_TIPTRA = space(1)
      .w_TRTIPCON = space(1)
      .w_TRCODRAG = space(10)
      .w_TRCODCON = space(15)
      .w_CODCON = space(15)
      .w_DESSTR = space(30)
      .w_CODTAB = space(30)
      .w_DESELE = space(50)
      .w_CAMPO = space(30)
      .w_TABIMP = space(30)
      .w_CAMP1 = space(30)
      .w_TRCODTRA = space(20)
      .w_TRCODTRI = space(20)
      .w_TIPO = space(1)
      .w_DESTRA = space(50)
      .w_DESINT = space(40)
      .w_DESTRI = space(50)
      .w_TIPTR2 = space(1)
      .w_FLPROV = space(1)
      .w_CAMTRA = space(30)
      .w_TABEL1 = space(30)
      .w_CAMTRI = space(30)
      .w_TABEL = space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_TRCODSTR))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,3,.f.)
          if not(empty(.w_TRCODELE))
          .link_1_4('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_TRTIPCON = 'N'
        .w_TRCODRAG = SPACE(10)
        .w_TRCODCON = SPACE(15)
        .w_CODCON = .w_TRCODCON
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CODCON))
          .link_1_12('Full')
          endif
          .DoRTCalc(9,14,.f.)
        .w_TRCODTRA = Space(20)
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_TRCODTRA))
          .link_1_20('Full')
          endif
        .w_TRCODTRI = Space(20)
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_TRCODTRI))
          .link_1_21('Full')
          endif
          .DoRTCalc(17,18,.f.)
        .w_DESINT = IIF(empty(.w_DESINT),SPACE(40),.w_DESINT)
      endif
    endwith
    cp_BlankRecExtFlds(this,'VATRASCO')
    this.DoRTCalc(20,26,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODSTR_1_1.enabled = i_bVal
      .Page1.oPag.oTRCODELE_1_4.enabled = i_bVal
      .Page1.oPag.oTRTIPCON_1_6.enabled = i_bVal
      .Page1.oPag.oTRCODRAG_1_10.enabled = i_bVal
      .Page1.oPag.oTRCODCON_1_11.enabled = i_bVal
      .Page1.oPag.oTRCODTRA_1_20.enabled = i_bVal
      .Page1.oPag.oTRCODTRI_1_21.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODSTR_1_1.enabled = .f.
        .Page1.oPag.oTRCODELE_1_4.enabled = .f.
        .Page1.oPag.oTRTIPCON_1_6.enabled = .f.
        .Page1.oPag.oTRCODRAG_1_10.enabled = .f.
        .Page1.oPag.oTRCODCON_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODSTR_1_1.enabled = .t.
        .Page1.oPag.oTRCODELE_1_4.enabled = .t.
        .Page1.oPag.oTRTIPCON_1_6.enabled = .t.
        .Page1.oPag.oTRCODRAG_1_10.enabled = .t.
        .Page1.oPag.oTRCODCON_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VATRASCO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VATRASCO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODSTR,"TRCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODENT,"TRCODENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODELE,"TRCODELE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPCON,"TRTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODRAG,"TRCODRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODCON,"TRCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODTRA,"TRCODTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODTRI,"TRCODTRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VATRASCO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
    i_lTable = "VATRASCO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VATRASCO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsva_str with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VATRASCO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VATRASCO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VATRASCO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VATRASCO')
        i_extval=cp_InsertValODBCExtFlds(this,'VATRASCO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TRCODSTR,TRCODENT,TRCODELE,TRTIPCON,TRCODRAG"+;
                  ",TRCODCON,TRCODTRA,TRCODTRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_TRCODSTR)+;
                  ","+cp_ToStrODBC(this.w_TRCODENT)+;
                  ","+cp_ToStrODBCNull(this.w_TRCODELE)+;
                  ","+cp_ToStrODBC(this.w_TRTIPCON)+;
                  ","+cp_ToStrODBC(this.w_TRCODRAG)+;
                  ","+cp_ToStrODBC(this.w_TRCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_TRCODTRA)+;
                  ","+cp_ToStrODBCNull(this.w_TRCODTRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VATRASCO')
        i_extval=cp_InsertValVFPExtFlds(this,'VATRASCO')
        cp_CheckDeletedKey(i_cTable,0,'TRCODSTR',this.w_TRCODSTR,'TRCODENT',this.w_TRCODENT,'TRCODELE',this.w_TRCODELE,'TRTIPCON',this.w_TRTIPCON,'TRCODRAG',this.w_TRCODRAG,'TRCODCON',this.w_TRCODCON)
        INSERT INTO (i_cTable);
              (TRCODSTR,TRCODENT,TRCODELE,TRTIPCON,TRCODRAG,TRCODCON,TRCODTRA,TRCODTRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TRCODSTR;
                  ,this.w_TRCODENT;
                  ,this.w_TRCODELE;
                  ,this.w_TRTIPCON;
                  ,this.w_TRCODRAG;
                  ,this.w_TRCODCON;
                  ,this.w_TRCODTRA;
                  ,this.w_TRCODTRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VATRASCO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VATRASCO_IDX,i_nConn)
      *
      * update VATRASCO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VATRASCO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TRCODTRA="+cp_ToStrODBCNull(this.w_TRCODTRA)+;
             ",TRCODTRI="+cp_ToStrODBCNull(this.w_TRCODTRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VATRASCO')
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODSTR',this.w_TRCODSTR  ,'TRCODENT',this.w_TRCODENT  ,'TRCODELE',this.w_TRCODELE  ,'TRTIPCON',this.w_TRTIPCON  ,'TRCODRAG',this.w_TRCODRAG  ,'TRCODCON',this.w_TRCODCON  )
        UPDATE (i_cTable) SET;
              TRCODTRA=this.w_TRCODTRA;
             ,TRCODTRI=this.w_TRCODTRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VATRASCO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VATRASCO_IDX,i_nConn)
      *
      * delete VATRASCO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODSTR',this.w_TRCODSTR  ,'TRCODENT',this.w_TRCODENT  ,'TRCODELE',this.w_TRCODELE  ,'TRTIPCON',this.w_TRTIPCON  ,'TRCODRAG',this.w_TRCODRAG  ,'TRCODCON',this.w_TRCODCON  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VATRASCO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VATRASCO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_TRTIPCON<>.w_TRTIPCON
            .w_TRCODRAG = SPACE(10)
        endif
        if .o_TRTIPCON<>.w_TRTIPCON
            .w_TRCODCON = SPACE(15)
        endif
        if .o_TRCODCON<>.w_TRCODCON
            .w_CODCON = .w_TRCODCON
          .link_1_12('Full')
        endif
        .DoRTCalc(9,14,.t.)
        if .o_TRCODELE<>.w_TRCODELE
            .w_TRCODTRA = Space(20)
          .link_1_20('Full')
        endif
        if .o_TRCODELE<>.w_TRCODELE
            .w_TRCODTRI = Space(20)
          .link_1_21('Full')
        endif
        .DoRTCalc(17,18,.t.)
        if .o_TRTIPCON<>.w_TRTIPCON
            .w_DESINT = IIF(empty(.w_DESINT),SPACE(40),.w_DESINT)
        endif
        if .o_TRCODCON<>.w_TRCODCON.or. .o_TRCODRAG<>.w_TRCODRAG
          .Calculate_UMULTZPKTR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_UMULTZPKTR()
    with this
          * --- Gsva_bcl(c)
          GSVA_BCL(this;
              ,'C';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.enabled = this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.mCond()
    this.oPgFrm.Page1.oPag.oTRCODCON_1_11.enabled = this.oPgFrm.Page1.oPag.oTRCODCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oTRCODTRA_1_20.enabled = this.oPgFrm.Page1.oPag.oTRCODTRA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oTRCODTRI_1_21.enabled = this.oPgFrm.Page1.oPag.oTRCODTRI_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.visible=!this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTRCODCON_1_11.visible=!this.oPgFrm.Page1.oPag.oTRCODCON_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDESINT_1_26.visible=!this.oPgFrm.Page1.oPag.oDESINT_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_TRCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STCODENT,STFLPROV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_TRCODSTR))
          select STCODICE,STDESCRI,STCODENT,STFLPROV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStrODBC(trim(this.w_TRCODSTR)+"%");

            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STCODENT,STFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" STDESCRI like "+cp_ToStr(trim(this.w_TRCODSTR)+"%");

            select STCODICE,STDESCRI,STCODENT,STFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TRCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oTRCODSTR_1_1'),i_cWhere,'GSVA_AST',"Elenco strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STCODENT,STFLPROV";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STCODENT,STFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STCODENT,STFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_TRCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_TRCODSTR)
            select STCODICE,STDESCRI,STCODENT,STFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_TRCODENT = NVL(_Link_.STCODENT,space(15))
      this.w_FLPROV = NVL(_Link_.STFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODSTR = space(10)
      endif
      this.w_DESSTR = space(30)
      this.w_TRCODENT = space(15)
      this.w_FLPROV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.STCODICE as STCODICE101"+ ",link_1_1.STDESCRI as STDESCRI101"+ ",link_1_1.STCODENT as STCODENT101"+ ",link_1_1.STFLPROV as STFLPROV101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on VATRASCO.TRCODSTR=link_1_1.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and VATRASCO.TRCODSTR=link_1_1.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRCODELE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODELE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_TRCODELE)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_TRCODSTR;
                     ,'ELCODICE',trim(this.w_TRCODELE))
          select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODELE)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODELE) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oTRCODELE_1_4'),i_cWhere,'GSVA_AEL',"Elementi",'gsva_atr.VAELEMEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TRCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascodifica incongruente o tabella/campo dell'elemento non valorizzati")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODELE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_TRCODELE);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_TRCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_TRCODSTR;
                       ,'ELCODICE',this.w_TRCODELE)
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO,ELCODTAB,EL_CAMPO,ELTIPTRA,ELTIPTR2,ELCAMIMP,ELTABIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODELE = NVL(_Link_.ELCODICE,space(20))
      this.w_DESELE = NVL(_Link_.ELDESCRI,space(50))
      this.w_TIPO = NVL(_Link_.EL__TIPO,space(1))
      this.w_CODTAB = NVL(_Link_.ELCODTAB,space(30))
      this.w_CAMPO = NVL(_Link_.EL_CAMPO,space(30))
      this.w_TIPTRA = NVL(_Link_.ELTIPTRA,space(1))
      this.w_TIPTR2 = NVL(_Link_.ELTIPTR2,space(1))
      this.w_CAMP1 = NVL(_Link_.ELCAMIMP,space(30))
      this.w_TABIMP = NVL(_Link_.ELTABIMP,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODELE = space(20)
      endif
      this.w_DESELE = space(50)
      this.w_TIPO = space(1)
      this.w_CODTAB = space(30)
      this.w_CAMPO = space(30)
      this.w_TIPTRA = space(1)
      this.w_TIPTR2 = space(1)
      this.w_CAMP1 = space(30)
      this.w_TABIMP = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO $ 'V-E'  AND ((.w_TIPTRA $ 'S-I'  and NOT EMPTY(.w_CAMPO)) OR ((.w_TIPTR2 $ 'S-I') and  NOT EMPTY(.w_CAMP1)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascodifica incongruente o tabella/campo dell'elemento non valorizzati")
        endif
        this.w_TRCODELE = space(20)
        this.w_DESELE = space(50)
        this.w_TIPO = space(1)
        this.w_CODTAB = space(30)
        this.w_CAMPO = space(30)
        this.w_TIPTRA = space(1)
        this.w_TIPTR2 = space(1)
        this.w_CAMP1 = space(30)
        this.w_TABIMP = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODELE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VAELEMEN_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ELCODICE as ELCODICE104"+ ",link_1_4.ELDESCRI as ELDESCRI104"+ ",link_1_4.EL__TIPO as EL__TIPO104"+ ",link_1_4.ELCODTAB as ELCODTAB104"+ ",link_1_4.EL_CAMPO as EL_CAMPO104"+ ",link_1_4.ELTIPTRA as ELTIPTRA104"+ ",link_1_4.ELTIPTR2 as ELTIPTR2104"+ ",link_1_4.ELCAMIMP as ELCAMIMP104"+ ",link_1_4.ELTABIMP as ELTABIMP104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on VATRASCO.TRCODELE=link_1_4.ELCODICE"+" and VATRASCO.TRCODSTR=link_1_4.ELCODSTR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and VATRASCO.TRCODELE=link_1_4.ELCODICE(+)"'+'+" and VATRASCO.TRCODSTR=link_1_4.ELCODSTR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TRTIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TRCODTRA
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_lTable = "TRS_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2], .t., this.TRS_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTR',True,'TRS_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_TRCODTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_TRCODTRA))
          select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODTRA)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODTRA) and !this.bDontReportError
            deferred_cp_zoom('TRS_MAST','*','TRCODICE',cp_AbsName(oSource.parent,'oTRCODTRA_1_20'),i_cWhere,'GSAR_MTR',"Trascodifiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TRCODTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TRCODTRA)
            select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODTRA = NVL(_Link_.TRCODICE,space(20))
      this.w_DESTRA = NVL(_Link_.TRDESCRI,space(50))
      this.w_TABEL = NVL(_Link_.TR_TABEL,space(30))
      this.w_CAMTRA = NVL(_Link_.TR_CAMPO,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODTRA = space(20)
      endif
      this.w_DESTRA = space(50)
      this.w_TABEL = space(30)
      this.w_CAMTRA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CAMTRA=.w_CAMPO AND .w_TABEL=.w_CODTAB
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, trascodifica associata a campo/tabella incongruenti")
        endif
        this.w_TRCODTRA = space(20)
        this.w_DESTRA = space(50)
        this.w_TABEL = space(30)
        this.w_CAMTRA = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TRS_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRS_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.TRCODICE as TRCODICE120"+ ",link_1_20.TRDESCRI as TRDESCRI120"+ ",link_1_20.TR_TABEL as TR_TABEL120"+ ",link_1_20.TR_CAMPO as TR_CAMPO120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on VATRASCO.TRCODTRA=link_1_20.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and VATRASCO.TRCODTRA=link_1_20.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRCODTRI
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRS_MAST_IDX,3]
    i_lTable = "TRS_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2], .t., this.TRS_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTR',True,'TRS_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_TRCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_TRCODTRI))
          select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODTRI)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRS_MAST','*','TRCODICE',cp_AbsName(oSource.parent,'oTRCODTRI_1_21'),i_cWhere,'GSAR_MTR',"Trascodifiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TRCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TRCODTRI)
            select TRCODICE,TRDESCRI,TR_TABEL,TR_CAMPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODTRI = NVL(_Link_.TRCODICE,space(20))
      this.w_DESTRI = NVL(_Link_.TRDESCRI,space(50))
      this.w_TABEL1 = NVL(_Link_.TR_TABEL,space(30))
      this.w_CAMTRI = NVL(_Link_.TR_CAMPO,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODTRI = space(20)
      endif
      this.w_DESTRI = space(50)
      this.w_TABEL1 = space(30)
      this.w_CAMTRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CAMTRI=.w_CAMP1 AND .w_TABEL1=.w_TABIMP
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, trascodifica associata a campo/tabella incongruenti")
        endif
        this.w_TRCODTRI = space(20)
        this.w_DESTRI = space(50)
        this.w_TABEL1 = space(30)
        this.w_CAMTRI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TRS_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRS_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRS_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.TRCODICE as TRCODICE121"+ ",link_1_21.TRDESCRI as TRDESCRI121"+ ",link_1_21.TR_TABEL as TR_TABEL121"+ ",link_1_21.TR_CAMPO as TR_CAMPO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on VATRASCO.TRCODTRI=link_1_21.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and VATRASCO.TRCODTRI=link_1_21.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODSTR_1_1.value==this.w_TRCODSTR)
      this.oPgFrm.Page1.oPag.oTRCODSTR_1_1.value=this.w_TRCODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODELE_1_4.value==this.w_TRCODELE)
      this.oPgFrm.Page1.oPag.oTRCODELE_1_4.value=this.w_TRCODELE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTIPCON_1_6.RadioValue()==this.w_TRTIPCON)
      this.oPgFrm.Page1.oPag.oTRTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.value==this.w_TRCODRAG)
      this.oPgFrm.Page1.oPag.oTRCODRAG_1_10.value=this.w_TRCODRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODCON_1_11.value==this.w_TRCODCON)
      this.oPgFrm.Page1.oPag.oTRCODCON_1_11.value=this.w_TRCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_14.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_14.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODTAB_1_15.value==this.w_CODTAB)
      this.oPgFrm.Page1.oPag.oCODTAB_1_15.value=this.w_CODTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_16.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_16.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO_1_17.value==this.w_CAMPO)
      this.oPgFrm.Page1.oPag.oCAMPO_1_17.value=this.w_CAMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oTABIMP_1_18.value==this.w_TABIMP)
      this.oPgFrm.Page1.oPag.oTABIMP_1_18.value=this.w_TABIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMP1_1_19.value==this.w_CAMP1)
      this.oPgFrm.Page1.oPag.oCAMP1_1_19.value=this.w_CAMP1
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODTRA_1_20.value==this.w_TRCODTRA)
      this.oPgFrm.Page1.oPag.oTRCODTRA_1_20.value=this.w_TRCODTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODTRI_1_21.value==this.w_TRCODTRI)
      this.oPgFrm.Page1.oPag.oTRCODTRI_1_21.value=this.w_TRCODTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRA_1_25.value==this.w_DESTRA)
      this.oPgFrm.Page1.oPag.oDESTRA_1_25.value=this.w_DESTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINT_1_26.value==this.w_DESINT)
      this.oPgFrm.Page1.oPag.oDESINT_1_26.value=this.w_DESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRI_1_28.value==this.w_DESTRI)
      this.oPgFrm.Page1.oPag.oDESTRI_1_28.value=this.w_DESTRI
    endif
    cp_SetControlsValueExtFlds(this,'VATRASCO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((Not Empty(.w_TRCODTRI) or Not Empty(.w_TRCODTRA) ) and .w_FLPROV='S'  )
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, struttura confermata o non � stata indicata nessuna trascodifica import/export")
          case   (empty(.w_TRCODSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODSTR_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TRCODSTR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TRCODELE)) or not(.w_TIPO $ 'V-E'  AND ((.w_TIPTRA $ 'S-I'  and NOT EMPTY(.w_CAMPO)) OR ((.w_TIPTR2 $ 'S-I') and  NOT EMPTY(.w_CAMP1)))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODELE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TRCODELE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione elemento non di tipo valore/espressione, tipo trascodifica incongruente o tabella/campo dell'elemento non valorizzati")
          case   (empty(.w_TRTIPCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRTIPCON_1_6.SetFocus()
            i_bnoObbl = !empty(.w_TRTIPCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TRCODCON))  and not(Not .w_TRTIPCON $ 'C-F')  and (.w_TRTIPCON $ 'C-F' AND NOT .cFunction='Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODCON_1_11.SetFocus()
            i_bnoObbl = !empty(.w_TRCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CAMTRA=.w_CAMPO AND .w_TABEL=.w_CODTAB)  and (Not Empty(.w_CAMPO))  and not(empty(.w_TRCODTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODTRA_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, trascodifica associata a campo/tabella incongruenti")
          case   not(.w_CAMTRI=.w_CAMP1 AND .w_TABEL1=.w_TABIMP)  and (Not Empty(.w_CAMP1))  and not(empty(.w_TRCODTRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODTRI_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, trascodifica associata a campo/tabella incongruenti")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TRCODELE = this.w_TRCODELE
    this.o_TRTIPCON = this.w_TRTIPCON
    this.o_TRCODRAG = this.w_TRCODRAG
    this.o_TRCODCON = this.w_TRCODCON
    return

  func CanEdit()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile modificare"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile eliminare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsva_atrPag1 as StdContainer
  Width  = 777
  height = 212
  stdWidth  = 777
  stdheight = 212
  resizeXpos=689
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODSTR_1_1 as StdField with uid="GDNFMVGXQL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODSTR", cQueryName = "TRCODSTR",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 34149768,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=158, Top=18, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_TRCODSTR"

  func oTRCODSTR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_TRCODELE)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTRCODSTR_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODSTR_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oTRCODSTR_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Elenco strutture",'',this.parent.oContained
  endproc
  proc oTRCODSTR_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_TRCODSTR
     i_obj.ecpSave()
  endproc

  add object oTRCODELE_1_4 as StdField with uid="SKOXAIGKEQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRCODELE", cQueryName = "TRCODSTR,TRCODENT,TRCODELE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione elemento non di tipo valore/espressione, tipo trascodifica incongruente o tabella/campo dell'elemento non valorizzati",;
    HelpContextID = 200731269,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=44, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_TRCODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_TRCODELE"

  func oTRCODELE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODELE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODELE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_TRCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_TRCODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oTRCODELE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AEL',"Elementi",'gsva_atr.VAELEMEN_VZM',this.parent.oContained
  endproc
  proc oTRCODELE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_TRCODSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_TRCODELE
     i_obj.ecpSave()
  endproc


  add object oTRTIPCON_1_6 as StdCombo with uid="VKZHYCGNYI",rtseq=5,rtrep=.f.,left=158,top=72,width=91,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 222026364;
    , cFormVar="w_TRTIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Gruppo,"+"Nessuno", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTRTIPCON_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oTRTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_TRTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_TRTIPCON=trim(this.Parent.oContained.w_TRTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TRTIPCON=='C',1,;
      iif(this.Parent.oContained.w_TRTIPCON=='F',2,;
      iif(this.Parent.oContained.w_TRTIPCON=='G',3,;
      iif(this.Parent.oContained.w_TRTIPCON=='N',4,;
      0))))
  endfunc

  func oTRTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_12('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oTRCODRAG_1_10 as StdField with uid="XARVUQLVRV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TRCODRAG", cQueryName = "TRCODSTR,TRCODENT,TRCODELE,TRTIPCON,TRCODRAG",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 17372541,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=355, Top=70, InputMask=replicate('X',10), bHasZoom = .t. 

  func oTRCODRAG_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPCON ='G')
    endwith
   endif
  endfunc

  func oTRCODRAG_1_10.mHide()
    with this.Parent.oContained
      return (.w_TRTIPCON <>'G')
    endwith
  endfunc

  proc oTRCODRAG_1_10.mZoom
      with this.Parent.oContained
        GSVA_BCL(this.Parent.oContained,"G")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTRCODCON_1_11 as StdField with uid="JBCGRYIWCV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TRCODCON", cQueryName = "TRCODSTR,TRCODENT,TRCODELE,TRTIPCON,TRCODRAG,TRCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 234285692,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=342, Top=70, InputMask=replicate('X',15), bHasZoom = .t. 

  func oTRCODCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPCON $ 'C-F' AND NOT .cFunction='Edit')
    endwith
   endif
  endfunc

  func oTRCODCON_1_11.mHide()
    with this.Parent.oContained
      return (Not .w_TRTIPCON $ 'C-F')
    endwith
  endfunc

  proc oTRCODCON_1_11.mZoom
      with this.Parent.oContained
        GSVA_BCL(this.Parent.oContained,"V")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDESSTR_1_14 as StdField with uid="XSMJLVVBSH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 233961674,;
   bGlobalFont=.t.,;
    Height=21, Width=516, Left=256, Top=18, InputMask=replicate('X',30)

  add object oCODTAB_1_15 as StdField with uid="BGQJIFLWXI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODTAB", cQueryName = "CODTAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 253877978,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=158, Top=97, InputMask=replicate('X',30)

  add object oDESELE_1_16 as StdField with uid="IDOWFTJBMZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 192936138,;
   bGlobalFont=.t.,;
    Height=21, Width=458, Left=314, Top=44, InputMask=replicate('X',50)

  add object oCAMPO_1_17 as StdField with uid="CTWTCZPAUY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAMPO", cQueryName = "CAMPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 4545754,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=542, Top=97, InputMask=replicate('X',30)

  add object oTABIMP_1_18 as StdField with uid="LEZGIUZKZR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TABIMP", cQueryName = "TABIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 7146442,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=158, Top=151, InputMask=replicate('X',30)

  add object oCAMP1_1_19 as StdField with uid="RPFEQHBMWZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CAMP1", cQueryName = "CAMP1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 36003034,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=542, Top=151, InputMask=replicate('X',30)

  add object oTRCODTRA_1_20 as StdField with uid="ARWLDPLHEN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TRCODTRA", cQueryName = "TRCODTRA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, trascodifica associata a campo/tabella incongruenti",;
    HelpContextID = 50926967,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=124, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="TRS_MAST", cZoomOnZoom="GSAR_MTR", oKey_1_1="TRCODICE", oKey_1_2="this.w_TRCODTRA"

  func oTRCODTRA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CAMPO))
    endwith
   endif
  endfunc

  func oTRCODTRA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODTRA_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODTRA_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRS_MAST','*','TRCODICE',cp_AbsName(this.parent,'oTRCODTRA_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTR',"Trascodifiche",'',this.parent.oContained
  endproc
  proc oTRCODTRA_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_TRCODTRA
     i_obj.ecpSave()
  endproc

  add object oTRCODTRI_1_21 as StdField with uid="GESDHILDWG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TRCODTRI", cQueryName = "TRCODTRI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, trascodifica associata a campo/tabella incongruenti",;
    HelpContextID = 50926975,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=158, Top=178, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="TRS_MAST", cZoomOnZoom="GSAR_MTR", oKey_1_1="TRCODICE", oKey_1_2="this.w_TRCODTRI"

  func oTRCODTRI_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CAMP1))
    endwith
   endif
  endfunc

  func oTRCODTRI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODTRI_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRCODTRI_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRS_MAST','*','TRCODICE',cp_AbsName(this.parent,'oTRCODTRI_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTR',"Trascodifiche",'',this.parent.oContained
  endproc
  proc oTRCODTRI_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_TRCODTRI
     i_obj.ecpSave()
  endproc

  add object oDESTRA_1_25 as StdField with uid="GPKOSMATWG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESTRA", cQueryName = "DESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 252770506,;
   bGlobalFont=.t.,;
    Height=21, Width=458, Left=314, Top=124, InputMask=replicate('X',50)

  add object oDESINT_1_26 as StdField with uid="LMGGSPTBCY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESINT", cQueryName = "DESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207354058,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=473, Top=71, InputMask=replicate('X',40)

  func oDESINT_1_26.mHide()
    with this.Parent.oContained
      return (.w_TRTIPCON='N')
    endwith
  endfunc

  add object oDESTRI_1_28 as StdField with uid="GQIAVGBDPS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESTRI", cQueryName = "DESTRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 118552778,;
   bGlobalFont=.t.,;
    Height=21, Width=458, Left=314, Top=178, InputMask=replicate('X',50)

  add object oStr_1_2 as StdString with uid="XRVKOWYFWS",Visible=.t., Left=8, Top=20,;
    Alignment=1, Width=149, Height=18,;
    Caption="Codice struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KMVQMMVZSH",Visible=.t., Left=8, Top=45,;
    Alignment=1, Width=149, Height=18,;
    Caption="Codice elemento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JRTFTKBPXC",Visible=.t., Left=8, Top=74,;
    Alignment=1, Width=149, Height=18,;
    Caption="Tipo intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IPVOJDNNTW",Visible=.t., Left=256, Top=72,;
    Alignment=1, Width=84, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TRTIPCON='N')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="SXCKTKXKEJ",Visible=.t., Left=8, Top=125,;
    Alignment=1, Width=149, Height=18,;
    Caption="Trascodifica export:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JBJHVCGZWA",Visible=.t., Left=8, Top=99,;
    Alignment=1, Width=149, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QIOQQGKOTI",Visible=.t., Left=429, Top=98,;
    Alignment=1, Width=110, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="EDVYNQSAEX",Visible=.t., Left=8, Top=180,;
    Alignment=1, Width=149, Height=18,;
    Caption="Trascodifica import:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="MGGIEUSOPB",Visible=.t., Left=8, Top=154,;
    Alignment=1, Width=149, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MUUNCUCVHG",Visible=.t., Left=414, Top=154,;
    Alignment=1, Width=125, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_atr','VATRASCO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODSTR=VATRASCO.TRCODSTR";
  +" and "+i_cAliasName2+".TRCODENT=VATRASCO.TRCODENT";
  +" and "+i_cAliasName2+".TRCODELE=VATRASCO.TRCODELE";
  +" and "+i_cAliasName2+".TRTIPCON=VATRASCO.TRTIPCON";
  +" and "+i_cAliasName2+".TRCODRAG=VATRASCO.TRCODRAG";
  +" and "+i_cAliasName2+".TRCODCON=VATRASCO.TRCODCON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
