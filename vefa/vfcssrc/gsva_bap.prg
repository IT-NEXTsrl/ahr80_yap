* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bap                                                        *
*              AGGIORNA PERCORSI                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-18                                                      *
* Last revis.: 2010-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC,pPATH
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bap",oParentObject,m.pEXEC,m.pPATH)
return(i_retval)

define class tgsva_bap as StdBatch
  * --- Local variables
  pEXEC = space(1)
  pPATH = space(200)
  w_CODAZI = space(5)
  * --- WorkFile variables
  CONTROPA_idx=0
  PAR_VEFA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- E: eseguito da parametri EDI GSVA_APA
    *     P: eseguito da gestione percorsi GSAR_AO8
    * --- Esegue aggiornamento campo Path EDI
    this.w_CODAZI = i_CODAZI
    if this.pEXEC="E"
      * --- Write into CONTROPA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COPATHED ="+cp_NullLink(cp_ToStrODBC(this.pPATH),'CONTROPA','COPATHED');
            +i_ccchkf ;
        +" where ";
            +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            COPATHED = this.pPATH;
            &i_ccchkf. ;
         where;
            COCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into PAR_VEFA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_VEFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VEFA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_VEFA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PAPATHED ="+cp_NullLink(cp_ToStrODBC(this.pPATH),'PAR_VEFA','PAPATHED');
            +i_ccchkf ;
        +" where ";
            +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            PAPATHED = this.pPATH;
            &i_ccchkf. ;
         where;
            PACODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pEXEC,pPATH)
    this.pEXEC=pEXEC
    this.pPATH=pPATH
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='PAR_VEFA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC,pPATH"
endproc
