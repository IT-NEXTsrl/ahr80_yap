* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kif                                                        *
*              Import massivo file EDI                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-19                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kif",oParentObject))

* --- Class definition
define class tgsva_kif as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 756
  Height = 580
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-15"
  HelpContextID=267022953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  PAR_VEFA_IDX = 0
  cPrg = "gsva_kif"
  cComment = "Import massivo file EDI"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(10)
  w_LGENERR = space(1)
  w_LDIRFIL = space(200)
  w_LDIRSPO = space(200)
  w_Msg = space(0)
  w_DIRSPO = space(200)
  w_DIRXML = space(200)
  w_DIRFIL = space(50)
  w_GENERR = space(1)
  w_FLAGLOG = .F.
  w_FLFREEZE = space(1)
  w_FLCHKFIL = space(1)
  w_FILEELAB = space(0)
  w_LDIRXML = space(200)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kifPag1","gsva_kif",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMsg_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PAR_VEFA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(10)
      .w_LGENERR=space(1)
      .w_LDIRFIL=space(200)
      .w_LDIRSPO=space(200)
      .w_Msg=space(0)
      .w_DIRSPO=space(200)
      .w_DIRXML=space(200)
      .w_DIRFIL=space(50)
      .w_GENERR=space(1)
      .w_FLAGLOG=.f.
      .w_FLFREEZE=space(1)
      .w_FLCHKFIL=space(1)
      .w_FILEELAB=space(0)
      .w_LDIRXML=space(200)
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,5,.f.)
        .w_DIRSPO = .w_LDIRSPO
        .w_DIRXML = .w_LDIRXML
        .w_DIRFIL = .w_LDIRFIL
        .w_GENERR = .w_LGENERR
        .w_FLAGLOG = .F.
        .w_FLFREEZE = 'S'
        .w_FLCHKFIL = 'S'
    endwith
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_READAZI = i_CODAZI
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_lTable = "PAR_VEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2], .t., this.PAR_VEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PADIRFIL,PADIRSPO,PAGENERR,PADIRXML";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PADIRFIL,PADIRSPO,PAGENERR,PADIRXML;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_LDIRFIL = NVL(_Link_.PADIRFIL,space(200))
      this.w_LDIRSPO = NVL(_Link_.PADIRSPO,space(200))
      this.w_LGENERR = NVL(_Link_.PAGENERR,space(1))
      this.w_LDIRXML = NVL(_Link_.PADIRXML,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_LDIRFIL = space(200)
      this.w_LDIRSPO = space(200)
      this.w_LGENERR = space(1)
      this.w_LDIRXML = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_VEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMsg_1_5.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_5.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRSPO_1_8.value==this.w_DIRSPO)
      this.oPgFrm.Page1.oPag.oDIRSPO_1_8.value=this.w_DIRSPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRXML_1_9.value==this.w_DIRXML)
      this.oPgFrm.Page1.oPag.oDIRXML_1_9.value=this.w_DIRXML
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRFIL_1_10.value==this.w_DIRFIL)
      this.oPgFrm.Page1.oPag.oDIRFIL_1_10.value=this.w_DIRFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oGENERR_1_11.RadioValue()==this.w_GENERR)
      this.oPgFrm.Page1.oPag.oGENERR_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGLOG_1_17.RadioValue()==this.w_FLAGLOG)
      this.oPgFrm.Page1.oPag.oFLAGLOG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKFIL_1_19.RadioValue()==this.w_FLCHKFIL)
      this.oPgFrm.Page1.oPag.oFLCHKFIL_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEELAB_1_20.value==this.w_FILEELAB)
      this.oPgFrm.Page1.oPag.oFILEELAB_1_20.value=this.w_FILEELAB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Upper(.w_DIRSPO) <> Upper(.w_DIRFIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIRSPO_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, directory utilizzata per selezionare i file di  import")
          case   ((empty(.w_DIRFIL)) or not(Upper(.w_DIRSPO) <> Upper(.w_DIRFIL)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIRFIL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DIRFIL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, directory utilizzata per spostare i file di  import")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_kifPag1 as StdContainer
  Width  = 752
  height = 580
  stdWidth  = 752
  stdheight = 580
  resizeXpos=417
  resizeYpos=306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_1_5 as StdMemo with uid="BQJOIBGZCP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266570298,;
   bGlobalFont=.t.,;
    Height=254, Width=732, Left=9, Top=164, tabstop = .f., readonly = .t.


  add object oBtn_1_6 as StdButton with uid="FRSHLUKCSC",left=638, top=526, width=48,height=45,;
    CpPicture="bmp\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per importare";
    , HelpContextID = 195821446;
    , Caption='I\<mporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do gsva_bif with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="YEDHIQOSNF",left=691, top=526, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 55697414;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDIRSPO_1_8 as StdField with uid="TLQIBTDIUW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DIRSPO", cQueryName = "DIRSPO",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, directory utilizzata per selezionare i file di  import",;
    ToolTipText = "Directory di spostamento file",;
    HelpContextID = 74316598,;
   bGlobalFont=.t.,;
    Height=21, Width=545, Left=168, Top=55, InputMask=replicate('X',200)

  func oDIRSPO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Upper(.w_DIRSPO) <> Upper(.w_DIRFIL))
    endwith
    return bRes
  endfunc

  add object oDIRXML_1_9 as StdField with uid="KPUXZWFZTN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DIRXML", cQueryName = "DIRXML",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory file XML intermedi",;
    HelpContextID = 21166902,;
   bGlobalFont=.t.,;
    Height=21, Width=545, Left=167, Top=83, InputMask=replicate('X',200)

  add object oDIRFIL_1_10 as StdField with uid="TTXFZVQVWW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DIRFIL", cQueryName = "DIRFIL",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, directory utilizzata per spostare i file di  import",;
    ToolTipText = "Directory di selezione file import",;
    HelpContextID = 15792950,;
   bGlobalFont=.t.,;
    Height=21, Width=545, Left=168, Top=27, InputMask=replicate('X',50)

  func oDIRFIL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Upper(.w_DIRSPO) <> Upper(.w_DIRFIL))
    endwith
    return bRes
  endfunc


  add object oGENERR_1_11 as StdCombo with uid="EQPEIZLSIG",rtseq=9,rtrep=.f.,left=168,top=110,width=219,height=21;
    , ToolTipText = "Modalit� di gestione errori adottata nella funzione Import massivo file EDI";
    , HelpContextID = 125810534;
    , cFormVar="w_GENERR",RowSource=""+"Annulla elaborazione corrente,"+"Annulla elaborazione struttura corrente,"+"Annulla elaborazione file corrente,"+"Annulla solo documenti errati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGENERR_1_11.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oGENERR_1_11.GetRadio()
    this.Parent.oContained.w_GENERR = this.RadioValue()
    return .t.
  endfunc

  func oGENERR_1_11.SetRadio()
    this.Parent.oContained.w_GENERR=trim(this.Parent.oContained.w_GENERR)
    this.value = ;
      iif(this.Parent.oContained.w_GENERR=='E',1,;
      iif(this.Parent.oContained.w_GENERR=='S',2,;
      iif(this.Parent.oContained.w_GENERR=='N',3,;
      iif(this.Parent.oContained.w_GENERR=='C',4,;
      0))))
  endfunc


  add object oBtn_1_12 as StdButton with uid="PXTLIVJJVS",left=720, top=57, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 266821930;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .w_DIRSPO=left(cp_getdir(IIF(EMPTY(.w_DIRSPO),sys(5)+sys(2003),.w_DIRSPO),"Directory di spostamento")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="EURFKODUPB",left=720, top=28, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 266821930;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        .w_DIRFIL=left(cp_getdir(IIF(EMPTY(.w_DIRFIL),sys(5)+sys(2003),.w_DIRFIL),"Directory file import")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLAGLOG_1_17 as StdCheck with uid="GQRLSKZSCN",rtseq=10,rtrep=.f.,left=14, top=548, caption="Produci log dettagliato",;
    ToolTipText = "Se attivo produce log dettagliato",;
    HelpContextID = 199168426,;
    cFormVar="w_FLAGLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAGLOG_1_17.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLAGLOG_1_17.GetRadio()
    this.Parent.oContained.w_FLAGLOG = this.RadioValue()
    return .t.
  endfunc

  func oFLAGLOG_1_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLAGLOG==.T.,1,;
      0)
  endfunc

  add object oFLCHKFIL_1_19 as StdCheck with uid="MMWWJWGSBB",rtseq=12,rtrep=.f.,left=169, top=135, caption="Salto elaborazione file gi� processati",;
    HelpContextID = 82702686,;
    cFormVar="w_FLCHKFIL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKFIL_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCHKFIL_1_19.GetRadio()
    this.Parent.oContained.w_FLCHKFIL = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKFIL_1_19.SetRadio()
    this.Parent.oContained.w_FLCHKFIL=trim(this.Parent.oContained.w_FLCHKFIL)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKFIL=='S',1,;
      0)
  endfunc

  add object oFILEELAB_1_20 as StdMemo with uid="IQHUUQREVG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FILEELAB", cQueryName = "FILEELAB",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 11508632,;
   bGlobalFont=.t.,;
    Height=99, Width=732, Left=9, Top=422, readonly=.T.


  add object oBtn_1_21 as StdButton with uid="ACRWOUFMJA",left=720, top=85, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 266821930;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .w_DIRXML=left(cp_getdir(IIF(EMPTY(.w_DIRXML),sys(5)+sys(2003),.w_DIRXML),"Directory file XML intermedi")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_14 as StdString with uid="SAVVDBSQIT",Visible=.t., Left=3, Top=111,;
    Alignment=1, Width=160, Height=18,;
    Caption="Modalit� gestione errori:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UWBYHZJQHP",Visible=.t., Left=4, Top=30,;
    Alignment=1, Width=159, Height=18,;
    Caption="Directory file import:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TUHPDZIZAQ",Visible=.t., Left=6, Top=58,;
    Alignment=1, Width=157, Height=18,;
    Caption="Directory spostamento file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JZHUOWYDIT",Visible=.t., Left=-55, Top=86,;
    Alignment=1, Width=218, Height=18,;
    Caption="Directory file XML intermedi:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kif','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
