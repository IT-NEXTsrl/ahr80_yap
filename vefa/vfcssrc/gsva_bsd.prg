* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bsd                                                        *
*              SEL/DES ARCHIVI IMPORT/EXPORT DBF                               *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_20]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2007-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bsd",oParentObject,m.pOper)
return(i_retval)

define class tgsva_bsd as StdBatch
  * --- Local variables
  pOper = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pOper=="ARCHI"
      * --- Selezione e Deselezione Archivi Import/Export da files DBF
      this.oParentObject.w_SELENT = iif( this.oParentObject.w_RADSELEZ1="D" , "", "EN" )
      this.oParentObject.w_SELTRR = iif( this.oParentObject.w_RADSELEZ1="D" , "", "TE" )
      this.oParentObject.w_SELSTR = iif( this.oParentObject.w_RADSELEZ1="D" , "", "ST" )
      this.oParentObject.w_SELVAP = iif( this.oParentObject.w_RADSELEZ1="D" , "", "VP" )
      this.oParentObject.w_SELTRS = iif( this.oParentObject.w_RADSELEZ1="D" , "", "TS" )
      this.oParentObject.w_SELCAR = iif( this.oParentObject.w_RADSELEZ1="D" , "", "CS" )
      this.oParentObject.w_SELELE = iif( this.oParentObject.w_RADSELEZ1="D" , "", "EL" )
      this.oParentObject.w_SELFOR = iif( this.oParentObject.w_RADSELEZ1="D" , "", "FO" )
    else
      UPDATE (this.oParentObject.w_STRUTTURE.cCursor) SET XCHK=IIF(this.oParentObject.w_RADSELEZ2="S", 1 , 0)
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
