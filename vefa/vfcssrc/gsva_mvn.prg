* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_mvn                                                        *
*              Variabili nome file                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-24                                                      *
* Last revis.: 2012-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsva_mvn")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsva_mvn")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsva_mvn")
  return

* --- Class definition
define class tgsva_mvn as StdPCForm
  Width  = 515
  Height = 242
  Top    = 10
  Left   = 15
  cComment = "Variabili nome file"
  cPrg = "gsva_mvn"
  HelpContextID=221613463
  add object cnt as tcgsva_mvn
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsva_mvn as PCContext
  w_VNCODICE = space(10)
  w_CPROWORD = 0
  w_VNCODVAR = space(10)
  w_VN__MASK = space(50)
  w_VNZERFIL = space(1)
  proc Save(i_oFrom)
    this.w_VNCODICE = i_oFrom.w_VNCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_VNCODVAR = i_oFrom.w_VNCODVAR
    this.w_VN__MASK = i_oFrom.w_VN__MASK
    this.w_VNZERFIL = i_oFrom.w_VNZERFIL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_VNCODICE = this.w_VNCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_VNCODVAR = this.w_VNCODVAR
    i_oTo.w_VN__MASK = this.w_VN__MASK
    i_oTo.w_VNZERFIL = this.w_VNZERFIL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsva_mvn as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 515
  Height = 242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-31"
  HelpContextID=221613463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VAVARNOM_IDX = 0
  cFile = "VAVARNOM"
  cKeySelect = "VNCODICE"
  cKeyWhere  = "VNCODICE=this.w_VNCODICE"
  cKeyDetail  = "VNCODICE=this.w_VNCODICE and VNCODVAR=this.w_VNCODVAR"
  cKeyWhereODBC = '"VNCODICE="+cp_ToStrODBC(this.w_VNCODICE)';

  cKeyDetailWhereODBC = '"VNCODICE="+cp_ToStrODBC(this.w_VNCODICE)';
      +'+" and VNCODVAR="+cp_ToStrODBC(this.w_VNCODVAR)';

  cKeyWhereODBCqualified = '"VAVARNOM.VNCODICE="+cp_ToStrODBC(this.w_VNCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VAVARNOM.CPROWORD '
  cPrg = "gsva_mvn"
  cComment = "Variabili nome file"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VNCODICE = space(10)
  w_CPROWORD = 0
  w_VNCODVAR = space(10)
  o_VNCODVAR = space(10)
  w_VN__MASK = space(50)
  o_VN__MASK = space(50)
  w_VNZERFIL = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mvnPag1","gsva_mvn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VAVARNOM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VAVARNOM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VAVARNOM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsva_mvn'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VAVARNOM where VNCODICE=KeySet.VNCODICE
    *                            and VNCODVAR=KeySet.VNCODVAR
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2],this.bLoadRecFilter,this.VAVARNOM_IDX,"gsva_mvn")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VAVARNOM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VAVARNOM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VAVARNOM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VNCODICE',this.w_VNCODICE  )
      select * from (i_cTable) VAVARNOM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VNCODICE = NVL(VNCODICE,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'VAVARNOM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_VNCODVAR = NVL(VNCODVAR,space(10))
          .w_VN__MASK = NVL(VN__MASK,space(50))
          .w_VNZERFIL = NVL(VNZERFIL,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace VNCODVAR with .w_VNCODVAR
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_VNCODICE=space(10)
      .w_CPROWORD=10
      .w_VNCODVAR=space(10)
      .w_VN__MASK=space(50)
      .w_VNZERFIL=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_VN__MASK = iif(Left(.w_VNCODVAR,3)='DAT','AAAAMMGG',' ')
        .w_VNZERFIL = iif((Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE') and  Not Empty(.w_VN__MASK) ,.w_VNZERFIL,'N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VAVARNOM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'VAVARNOM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VNCODICE,"VNCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_VNCODVAR N(3);
      ,t_VN__MASK C(50);
      ,t_VNZERFIL N(3);
      ,VNCODVAR C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mvnbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.controlsource=this.cTrsName+'.t_VNCODVAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVN__MASK_2_3.controlsource=this.cTrsName+'.t_VN__MASK'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.controlsource=this.cTrsName+'.t_VNZERFIL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(279)
    this.AddVLine(464)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2])
      *
      * insert into VAVARNOM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VAVARNOM')
        i_extval=cp_InsertValODBCExtFlds(this,'VAVARNOM')
        i_cFldBody=" "+;
                  "(VNCODICE,CPROWORD,VNCODVAR,VN__MASK,VNZERFIL,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_VNCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_VNCODVAR)+","+cp_ToStrODBC(this.w_VN__MASK)+","+cp_ToStrODBC(this.w_VNZERFIL)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VAVARNOM')
        i_extval=cp_InsertValVFPExtFlds(this,'VAVARNOM')
        cp_CheckDeletedKey(i_cTable,0,'VNCODICE',this.w_VNCODICE,'VNCODVAR',this.w_VNCODVAR)
        INSERT INTO (i_cTable) (;
                   VNCODICE;
                  ,CPROWORD;
                  ,VNCODVAR;
                  ,VN__MASK;
                  ,VNZERFIL;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_VNCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_VNCODVAR;
                  ,this.w_VN__MASK;
                  ,this.w_VNZERFIL;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_VNCODVAR)) AND not(Empty(t_VN__MASK))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VAVARNOM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and VNCODVAR="+cp_ToStrODBC(&i_TN.->VNCODVAR)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VAVARNOM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and VNCODVAR=&i_TN.->VNCODVAR;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_VNCODVAR)) AND not(Empty(t_VN__MASK))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and VNCODVAR="+cp_ToStrODBC(&i_TN.->VNCODVAR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and VNCODVAR=&i_TN.->VNCODVAR;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace VNCODVAR with this.w_VNCODVAR
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VAVARNOM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VAVARNOM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",VN__MASK="+cp_ToStrODBC(this.w_VN__MASK)+;
                     ",VNZERFIL="+cp_ToStrODBC(this.w_VNZERFIL)+;
                     ",VNCODVAR="+cp_ToStrODBC(this.w_VNCODVAR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and VNCODVAR="+cp_ToStrODBC(VNCODVAR)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VAVARNOM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,VN__MASK=this.w_VN__MASK;
                     ,VNZERFIL=this.w_VNZERFIL;
                     ,VNCODVAR=this.w_VNCODVAR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and VNCODVAR=&i_TN.->VNCODVAR;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_VNCODVAR)) AND not(Empty(t_VN__MASK))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VAVARNOM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and VNCODVAR="+cp_ToStrODBC(&i_TN.->VNCODVAR)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and VNCODVAR=&i_TN.->VNCODVAR;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_VNCODVAR)) AND not(Empty(t_VN__MASK))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAVARNOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAVARNOM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_VNCODVAR<>.w_VNCODVAR
          .w_VN__MASK = iif(Left(.w_VNCODVAR,3)='DAT','AAAAMMGG',' ')
        endif
        if .o_VNCODVAR<>.w_VNCODVAR.or. .o_VN__MASK<>.w_VN__MASK
          .w_VNZERFIL = iif((Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE') and  Not Empty(.w_VN__MASK) ,.w_VNZERFIL,'N')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVNZERFIL_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oVNZERFIL_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.RadioValue()==this.w_VNCODVAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.SetRadio()
      replace t_VNCODVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVN__MASK_2_3.value==this.w_VN__MASK)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVN__MASK_2_3.value=this.w_VN__MASK
      replace t_VN__MASK with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVN__MASK_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.RadioValue()==this.w_VNZERFIL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.SetRadio()
      replace t_VNZERFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'VAVARNOM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(icase(Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE',OCCURS('9',Alltrim(.w_VN__MASK))=LEN(Alltrim(.w_VN__MASK)),Left(.w_VNCODVAR,3)='DAT',OCCURS('\',Alltrim(.w_VN__MASK))+OCCURS(':',Alltrim(.w_VN__MASK))+OCCURS('/',Alltrim(.w_VN__MASK))+OCCURS('*',Alltrim(.w_VN__MASK))+OCCURS('?',Alltrim(.w_VN__MASK))+OCCURS('"',Alltrim(.w_VN__MASK))+OCCURS('<',Alltrim(.w_VN__MASK))+OCCURS('>',Alltrim(.w_VN__MASK))+OCCURS('|',Alltrim(.w_VN__MASK))+OCCURS("'",Alltrim(.w_VN__MASK))=0,OCCURS('X',Alltrim(.w_VN__MASK))=LEN(Alltrim(.w_VN__MASK)))) and (not(Empty(.w_VNCODVAR)) AND not(Empty(.w_VN__MASK)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVN__MASK_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, maschera incongruente al tipo di variabile")
      endcase
      if not(Empty(.w_VNCODVAR)) AND not(Empty(.w_VN__MASK))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VNCODVAR = this.w_VNCODVAR
    this.o_VN__MASK = this.w_VN__MASK
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_VNCODVAR)) AND not(Empty(t_VN__MASK)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_VNCODVAR=space(10)
      .w_VN__MASK=space(50)
      .w_VNZERFIL=space(1)
      .DoRTCalc(1,3,.f.)
        .w_VN__MASK = iif(Left(.w_VNCODVAR,3)='DAT','AAAAMMGG',' ')
        .w_VNZERFIL = iif((Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE') and  Not Empty(.w_VN__MASK) ,.w_VNZERFIL,'N')
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_VNCODVAR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.RadioValue(.t.)
    this.w_VN__MASK = t_VN__MASK
    this.w_VNZERFIL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_VNCODVAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNCODVAR_2_2.ToRadio()
    replace t_VN__MASK with this.w_VN__MASK
    replace t_VNZERFIL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oVNZERFIL_2_4.ToRadio()
    if i_srv='A'
      replace VNCODVAR with this.w_VNCODVAR
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsva_mvnPag1 as StdContainer
  Width  = 511
  height = 242
  stdWidth  = 511
  stdheight = 242
  resizeXpos=360
  resizeYpos=148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=19, width=497,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="VNCODVAR",Label2="Codice variabile",Field3="VN__MASK",Label3="Maschera",Field4="VNZERFIL",Label4="Zerofill",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 267602054

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=2,top=41,;
    width=488+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=3,top=42,width=487+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsva_mvnBodyRow as CPBodyRowCnt
  Width=478
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="COWTXAQNXU",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 235273622,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0

  add object oVNCODVAR_2_2 as StdTrsCombo with uid="KGWPFSYYPF",rtrep=.t.,;
    cFormVar="w_VNCODVAR", RowSource=""+"Numero documento,"+"Serie documento,"+"Data documento,"+"Data registrazione,"+"Intestatario,"+"Numero protocollo,"+"Serie protocollo,"+"Date ,"+"Time,"+"Progressivo" , ;
    HelpContextID = 137817688,;
    Height=21, Width=213, Left=50, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oVNCODVAR_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VNCODVAR,&i_cF..t_VNCODVAR),this.value)
    return(iif(xVal =1,'NUMDOC',;
    iif(xVal =2,'ALFDOC',;
    iif(xVal =3,'DATDOC',;
    iif(xVal =4,'DATREG',;
    iif(xVal =5,'CODCON',;
    iif(xVal =6,'NUMPRO',;
    iif(xVal =7,'ALFPRO',;
    iif(xVal =8,'DATE',;
    iif(xVal =9,'TIME',;
    iif(xVal =10,'PROGRE',;
    space(10))))))))))))
  endfunc
  func oVNCODVAR_2_2.GetRadio()
    this.Parent.oContained.w_VNCODVAR = this.RadioValue()
    return .t.
  endfunc

  func oVNCODVAR_2_2.ToRadio()
    this.Parent.oContained.w_VNCODVAR=trim(this.Parent.oContained.w_VNCODVAR)
    return(;
      iif(this.Parent.oContained.w_VNCODVAR=='NUMDOC',1,;
      iif(this.Parent.oContained.w_VNCODVAR=='ALFDOC',2,;
      iif(this.Parent.oContained.w_VNCODVAR=='DATDOC',3,;
      iif(this.Parent.oContained.w_VNCODVAR=='DATREG',4,;
      iif(this.Parent.oContained.w_VNCODVAR=='CODCON',5,;
      iif(this.Parent.oContained.w_VNCODVAR=='NUMPRO',6,;
      iif(this.Parent.oContained.w_VNCODVAR=='ALFPRO',7,;
      iif(this.Parent.oContained.w_VNCODVAR=='DATE',8,;
      iif(this.Parent.oContained.w_VNCODVAR=='TIME',9,;
      iif(this.Parent.oContained.w_VNCODVAR=='PROGRE',10,;
      0)))))))))))
  endfunc

  func oVNCODVAR_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oVN__MASK_2_3 as StdTrsField with uid="OBGVBTYSUI",rtseq=4,rtrep=.t.,;
    cFormVar="w_VN__MASK",value=space(50),;
    HelpContextID = 211103327,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, maschera incongruente al tipo di variabile",;
   bGlobalFont=.t.,;
    Height=17, Width=179, Left=268, Top=0, InputMask=replicate('X',50)

  func oVN__MASK_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (icase(Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE',OCCURS('9',Alltrim(.w_VN__MASK))=LEN(Alltrim(.w_VN__MASK)),Left(.w_VNCODVAR,3)='DAT',OCCURS('\',Alltrim(.w_VN__MASK))+OCCURS(':',Alltrim(.w_VN__MASK))+OCCURS('/',Alltrim(.w_VN__MASK))+OCCURS('*',Alltrim(.w_VN__MASK))+OCCURS('?',Alltrim(.w_VN__MASK))+OCCURS('"',Alltrim(.w_VN__MASK))+OCCURS('<',Alltrim(.w_VN__MASK))+OCCURS('>',Alltrim(.w_VN__MASK))+OCCURS('|',Alltrim(.w_VN__MASK))+OCCURS("'",Alltrim(.w_VN__MASK))=0,OCCURS('X',Alltrim(.w_VN__MASK))=LEN(Alltrim(.w_VN__MASK))))
    endwith
    return bRes
  endfunc

  add object oVNZERFIL_2_4 as StdTrsCheck with uid="ZUGCMPTYPB",rtrep=.t.,;
    cFormVar="w_VNZERFIL",  caption="",;
    HelpContextID = 144736674,;
    Left=454, Top=-1, Width=19,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oVNZERFIL_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VNZERFIL,&i_cF..t_VNZERFIL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oVNZERFIL_2_4.GetRadio()
    this.Parent.oContained.w_VNZERFIL = this.RadioValue()
    return .t.
  endfunc

  func oVNZERFIL_2_4.ToRadio()
    this.Parent.oContained.w_VNZERFIL=trim(this.Parent.oContained.w_VNZERFIL)
    return(;
      iif(this.Parent.oContained.w_VNZERFIL=='S',1,;
      0))
  endfunc

  func oVNZERFIL_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVNZERFIL_2_4.mCond()
    with this.Parent.oContained
      return ((Left(.w_VNCODVAR,3)='NUM' or .w_VNCODVAR='PROGRE') and Not Empty(.w_VN__MASK))
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_mvn','VAVARNOM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VNCODICE=VAVARNOM.VNCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
