* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kas                                                        *
*              Aggiornamento massivo listini                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_170]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-20                                                      *
* Last revis.: 2008-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kas",oParentObject))

* --- Class definition
define class tgsva_kas as StdForm
  Top    = 3
  Left   = 19

  * --- Standard Properties
  Width  = 843
  Height = 501+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-07"
  HelpContextID=132805225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Constant Properties
  _IDX = 0
  CLLSMAST_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  GRUMERC_IDX = 0
  FAM_ARTI_IDX = 0
  CLA_RICA_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  VOCIIVA_IDX = 0
  cPrg = "gsva_kas"
  cComment = "Aggiornamento massivo listini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATRIF = ctod('  /  /  ')
  o_DATRIF = ctod('  /  /  ')
  w_CLSERIAL = space(10)
  w_FLSELE = 0
  w_SCHEDINI = space(10)
  w_SCHEDFIN = space(10)
  w_CRITER = space(2)
  o_CRITER = space(2)
  w_TIPUMI = space(1)
  w_DESCHEDINI = space(50)
  w_DESCHEDFIN = space(50)
  w_TIPO = space(10)
  w_DESLINI = space(40)
  w_DESLFIN = space(40)
  w_DTOBSLIS = ctod('  /  /  ')
  w_DESGRINI = space(35)
  w_DESGRFIN = space(35)
  w_ARTINI = space(20)
  w_DESINI = space(40)
  w_ARTFIN = space(20)
  w_DESFIN = space(40)
  w_LISTINI = space(5)
  w_LISTFIN = space(5)
  w_GRUMERINI = space(5)
  w_GRUMERFIN = space(5)
  w_FAMINI = space(5)
  w_DESFAMINI = space(35)
  w_FAMFIN = space(5)
  w_DESFAMFIN = space(35)
  w_CLARICINI = space(5)
  w_DESCLINI = space(35)
  w_DESCLFIN = space(35)
  w_CLARICFIN = space(5)
  w_PRIORINI = 0
  w_PRIORFIN = 0
  w_SELEZI = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_VALFIN = ctod('  /  /  ')
  w_VALIN2 = ctod('  /  /  ')
  w_VALFIN2 = ctod('  /  /  ')
  w_VALINI = ctod('  /  /  ')
  w_FLAGGIO = space(1)
  w_ARGRUMINI = space(5)
  w_ARGRUMFIN = space(5)
  w_MARCINI = space(5)
  w_MARCFIN = space(5)
  w_CODIVA = space(5)
  w_DESMARINI = space(35)
  w_FORABIT = space(15)
  w_FODESCRI = space(40)
  w_FORULTC = space(15)
  w_FUDESCRI = space(40)
  w_DESMARFIN = space(35)
  w_DESCRIVA = space(35)
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kasPag1","gsva_kas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsva_kasPag2","gsva_kas",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATRIF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CLLSMAST'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='CLA_RICA'
    this.cWorkTables[7]='MARCHI'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='VOCIIVA'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATRIF=ctod("  /  /  ")
      .w_CLSERIAL=space(10)
      .w_FLSELE=0
      .w_SCHEDINI=space(10)
      .w_SCHEDFIN=space(10)
      .w_CRITER=space(2)
      .w_TIPUMI=space(1)
      .w_DESCHEDINI=space(50)
      .w_DESCHEDFIN=space(50)
      .w_TIPO=space(10)
      .w_DESLINI=space(40)
      .w_DESLFIN=space(40)
      .w_DTOBSLIS=ctod("  /  /  ")
      .w_DESGRINI=space(35)
      .w_DESGRFIN=space(35)
      .w_ARTINI=space(20)
      .w_DESINI=space(40)
      .w_ARTFIN=space(20)
      .w_DESFIN=space(40)
      .w_LISTINI=space(5)
      .w_LISTFIN=space(5)
      .w_GRUMERINI=space(5)
      .w_GRUMERFIN=space(5)
      .w_FAMINI=space(5)
      .w_DESFAMINI=space(35)
      .w_FAMFIN=space(5)
      .w_DESFAMFIN=space(35)
      .w_CLARICINI=space(5)
      .w_DESCLINI=space(35)
      .w_DESCLFIN=space(35)
      .w_CLARICFIN=space(5)
      .w_PRIORINI=0
      .w_PRIORFIN=0
      .w_SELEZI=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_VALFIN=ctod("  /  /  ")
      .w_VALIN2=ctod("  /  /  ")
      .w_VALFIN2=ctod("  /  /  ")
      .w_VALINI=ctod("  /  /  ")
      .w_FLAGGIO=space(1)
      .w_ARGRUMINI=space(5)
      .w_ARGRUMFIN=space(5)
      .w_MARCINI=space(5)
      .w_MARCFIN=space(5)
      .w_CODIVA=space(5)
      .w_DESMARINI=space(35)
      .w_FORABIT=space(15)
      .w_FODESCRI=space(40)
      .w_FORULTC=space(15)
      .w_FUDESCRI=space(40)
      .w_DESMARFIN=space(35)
      .w_DESCRIVA=space(35)
        .w_DATRIF = i_DATSYS
        .w_CLSERIAL = .w_Zoom.getVar('CLSERIAL')
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_SCHEDINI))
          .link_1_8('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_SCHEDFIN))
          .link_1_9('Full')
        endif
        .w_CRITER = 'TU'
        .w_TIPUMI = IIF(LEFT(.w_CRITER,1)='L' AND NOT EMPTY(.w_TIPUMI), .w_TIPUMI, 'P')
          .DoRTCalc(8,9,.f.)
        .w_TIPO = 'F'
        .DoRTCalc(11,16,.f.)
        if not(empty(.w_ARTINI))
          .link_2_11('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_ARTFIN))
          .link_2_13('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_LISTINI))
          .link_2_15('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_LISTFIN))
          .link_2_17('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_GRUMERINI))
          .link_2_18('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_GRUMERFIN))
          .link_2_19('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_FAMINI))
          .link_2_21('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_FAMFIN))
          .link_2_23('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_CLARICINI))
          .link_2_27('Full')
        endif
        .DoRTCalc(29,31,.f.)
        if not(empty(.w_CLARICFIN))
          .link_2_32('Full')
        endif
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(32,33,.f.)
        .w_SELEZI = 'D'
          .DoRTCalc(35,35,.f.)
        .w_OBTEST = .w_DATRIF
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate('',RGB(255,255,0),RGB(255,255,0))
          .DoRTCalc(37,40,.f.)
        .w_FLAGGIO = IIF(left(.w_CRITER,1) $'LC', .w_FLAGGIO,' ')
        .DoRTCalc(42,44,.f.)
        if not(empty(.w_MARCINI))
          .link_2_37('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_MARCFIN))
          .link_2_38('Full')
        endif
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_CODIVA))
          .link_2_39('Full')
        endif
        .DoRTCalc(47,48,.f.)
        if not(empty(.w_FORABIT))
          .link_2_42('Full')
        endif
        .DoRTCalc(49,50,.f.)
        if not(empty(.w_FORULTC))
          .link_2_45('Full')
        endif
    endwith
    this.DoRTCalc(51,53,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CLSERIAL = .w_Zoom.getVar('CLSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(3,6,.t.)
        if .o_CRITER<>.w_CRITER
            .w_TIPUMI = IIF(LEFT(.w_CRITER,1)='L' AND NOT EMPTY(.w_TIPUMI), .w_TIPUMI, 'P')
        endif
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(8,35,.t.)
        if .o_DATRIF<>.w_DATRIF
            .w_OBTEST = .w_DATRIF
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate('',RGB(255,255,0),RGB(255,255,0))
        .DoRTCalc(37,40,.t.)
        if .o_CRITER<>.w_CRITER
            .w_FLAGGIO = IIF(left(.w_CRITER,1) $'LC', .w_FLAGGIO,' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate('',RGB(255,255,0),RGB(255,255,0))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLAGGIO_1_31.enabled = this.oPgFrm.Page1.oPag.oFLAGGIO_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPUMI_1_11.visible=!this.oPgFrm.Page1.oPag.oTIPUMI_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsva_kas
    * --- Blocco colonne Zoom per problemi c0005
    If lower(cEvent)='w_zoom after query'
     BloccaZoom( this.w_ZOOM )
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCHEDINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_lTable = "CLLSMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2], .t., this.CLLSMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCHEDINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA1BBC',True,'CLLSMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLSERIAL like "+cp_ToStrODBC(trim(this.w_SCHEDINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLSERIAL',trim(this.w_SCHEDINI))
          select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCHEDINI)==trim(_Link_.CLSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_SCHEDINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_SCHEDINI)+"%");

            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCHEDINI) and !this.bDontReportError
            deferred_cp_zoom('CLLSMAST','*','CLSERIAL',cp_AbsName(oSource.parent,'oSCHEDINI_1_8'),i_cWhere,'GSVA1BBC',"Schede di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLSERIAL',oSource.xKey(1))
            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCHEDINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(this.w_SCHEDINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLSERIAL',this.w_SCHEDINI)
            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCHEDINI = NVL(_Link_.CLSERIAL,space(10))
      this.w_DESCHEDINI = NVL(_Link_.CLDESCRI,space(50))
      this.w_VALINI = NVL(cp_ToDate(_Link_.CLVALINI),ctod("  /  /  "))
      this.w_VALFIN = NVL(cp_ToDate(_Link_.CLVALFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCHEDINI = space(10)
      endif
      this.w_DESCHEDINI = space(50)
      this.w_VALINI = ctod("  /  /  ")
      this.w_VALFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_VALINI<=.w_DATRIF AND .w_VALFIN>=.w_DATRIF) AND ((empty(.w_SCHEDFIN)) OR  (.w_SCHEDINI<=.w_SCHEDFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Scheda non valida o intervallo di schede errato")
        endif
        this.w_SCHEDINI = space(10)
        this.w_DESCHEDINI = space(50)
        this.w_VALINI = ctod("  /  /  ")
        this.w_VALFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])+'\'+cp_ToStr(_Link_.CLSERIAL,1)
      cp_ShowWarn(i_cKey,this.CLLSMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCHEDINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCHEDFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_lTable = "CLLSMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2], .t., this.CLLSMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCHEDFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA1BBC',True,'CLLSMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLSERIAL like "+cp_ToStrODBC(trim(this.w_SCHEDFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLSERIAL',trim(this.w_SCHEDFIN))
          select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCHEDFIN)==trim(_Link_.CLSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStrODBC(trim(this.w_SCHEDFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLDESCRI like "+cp_ToStr(trim(this.w_SCHEDFIN)+"%");

            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SCHEDFIN) and !this.bDontReportError
            deferred_cp_zoom('CLLSMAST','*','CLSERIAL',cp_AbsName(oSource.parent,'oSCHEDFIN_1_9'),i_cWhere,'GSVA1BBC',"Schede di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLSERIAL',oSource.xKey(1))
            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCHEDFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(this.w_SCHEDFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLSERIAL',this.w_SCHEDFIN)
            select CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCHEDFIN = NVL(_Link_.CLSERIAL,space(10))
      this.w_DESCHEDFIN = NVL(_Link_.CLDESCRI,space(50))
      this.w_VALIN2 = NVL(cp_ToDate(_Link_.CLVALINI),ctod("  /  /  "))
      this.w_VALFIN2 = NVL(cp_ToDate(_Link_.CLVALFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCHEDFIN = space(10)
      endif
      this.w_DESCHEDFIN = space(50)
      this.w_VALIN2 = ctod("  /  /  ")
      this.w_VALFIN2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_VALIN2<=.w_DATRIF AND .w_VALFIN2>=.w_DATRIF) AND (.w_SCHEDFIN>=.w_SCHEDINI or empty(.w_SCHEDFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Scheda non valida o intervallo di schede errato")
        endif
        this.w_SCHEDFIN = space(10)
        this.w_DESCHEDFIN = space(50)
        this.w_VALIN2 = ctod("  /  /  ")
        this.w_VALFIN2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])+'\'+cp_ToStr(_Link_.CLSERIAL,1)
      cp_ShowWarn(i_cKey,this.CLLSMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCHEDFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_2_11'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_ARGRUMINI = NVL(_Link_.ARGRUMER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ARGRUMINI = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_artfin)) OR  (UPPER(.w_artini)<= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ARGRUMINI = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_2_13'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARGRUMER";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO,ARGRUMER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_ARGRUMFIN = NVL(_Link_.ARGRUMER,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ARGRUMFIN = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ARGRUMFIN = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINI
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINI))
          select LSCODLIS,LSDESLIS,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINI)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_LISTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_LISTINI)+"%");

            select LSCODLIS,LSDESLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LISTINI) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINI_2_15'),i_cWhere,'GSAR_ALI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINI)
            select LSCODLIS,LSDESLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINI = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLINI = NVL(_Link_.LSDESLIS,space(40))
      this.w_DTOBSLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINI = space(5)
      endif
      this.w_DESLINI = space(40)
      this.w_DTOBSLIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSLIS) OR .w_DTOBSLIS>.w_DATRIF) AND ((empty(.w_LISTFIN)) OR  (.w_LISTINI<=.w_LISTFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto")
        endif
        this.w_LISTINI = space(5)
        this.w_DESLINI = space(40)
        this.w_DTOBSLIS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTFIN
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTFIN))
          select LSCODLIS,LSDESLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTFIN)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_LISTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_LISTFIN)+"%");

            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LISTFIN) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTFIN_2_17'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTFIN)
            select LSCODLIS,LSDESLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTFIN = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLFIN = NVL(_Link_.LSDESLIS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LISTFIN = space(5)
      endif
      this.w_DESLFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSLIS) OR .w_DTOBSLIS>.w_DATRIF ) AND (.w_LISTFIN>=.w_LISTINI or empty(.w_LISTFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto")
        endif
        this.w_LISTFIN = space(5)
        this.w_DESLFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMERINI
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMERINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMERINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMERINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMERINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMERINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMERINI_2_18'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMERINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMERINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMERINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMERINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRINI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMERINI = space(5)
      endif
      this.w_DESGRINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_GRUMERFIN)) OR  (.w_GRUMERINI<=.w_GRUMERFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_GRUMERINI = space(5)
        this.w_DESGRINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMERINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMERFIN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMERFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMERFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMERFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMERFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMERFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMERFIN_2_19'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMERFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMERFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMERFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMERFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRFIN = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMERFIN = space(5)
      endif
      this.w_DESGRFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUMERFIN>=.w_GRUMERINI or empty(.w_GRUMERFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_GRUMERFIN = space(5)
        this.w_DESGRFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMERFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMINI
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_FAMINI)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_FAMINI))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAMINI)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAMINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMINI_2_21'),i_cWhere,'GSAR_AFA',"Famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMINI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMINI = space(5)
      endif
      this.w_DESFAMINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_FAMFIN) OR  .w_FAMINI<=.w_FAMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_FAMINI = space(5)
        this.w_DESFAMINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMFIN
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_FAMFIN)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_FAMFIN))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAMFIN)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAMFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMFIN_2_23'),i_cWhere,'GSAR_AFA',"Famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMFIN = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMFIN = space(5)
      endif
      this.w_DESFAMFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_FAMFIN) OR  .w_FAMINI<=.w_FAMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_FAMFIN = space(5)
        this.w_DESFAMFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLARICINI
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLARICINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_CLARICINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_CLARICINI))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLARICINI)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStrODBC(trim(this.w_CLARICINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStr(trim(this.w_CLARICINI)+"%");

            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLARICINI) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oCLARICINI_2_27'),i_cWhere,'GSAR_ACR',"Classi di ricarico dei listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLARICINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_CLARICINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_CLARICINI)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLARICINI = NVL(_Link_.CRCODICE,space(5))
      this.w_DESCLINI = NVL(_Link_.CRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLARICINI = space(5)
      endif
      this.w_DESCLINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CLARICFIN) OR  .w_CLARICINI<=.w_CLARICFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_CLARICINI = space(5)
        this.w_DESCLINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLARICINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLARICFIN
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLARICFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_CLARICFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_CLARICFIN))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLARICFIN)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStrODBC(trim(this.w_CLARICFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStr(trim(this.w_CLARICFIN)+"%");

            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLARICFIN) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oCLARICFIN_2_32'),i_cWhere,'GSAR_ACR',"Classi di ricarico dei listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLARICFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_CLARICFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_CLARICFIN)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLARICFIN = NVL(_Link_.CRCODICE,space(5))
      this.w_DESCLFIN = NVL(_Link_.CRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLARICFIN = space(5)
      endif
      this.w_DESCLFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_CLARICFIN) OR  .w_CLARICINI<=.w_CLARICFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_CLARICFIN = space(5)
        this.w_DESCLFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLARICFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARCINI
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARCINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARCINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARCINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MARCINI)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MARCINI)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MARCINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARCINI_2_37'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARCINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARCINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARCINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARINI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARCINI = space(5)
      endif
      this.w_DESMARINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_MARCFIN) OR  .w_MARCINI<=.w_MARCFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_MARCINI = space(5)
        this.w_DESMARINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARCFIN
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARCFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARCFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MARCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MARCFIN)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MARCFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARCFIN_2_38'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARCFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARCFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARFIN = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARCFIN = space(5)
      endif
      this.w_DESMARFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_MARCFIN) OR  .w_MARCINI<=.w_MARCFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_MARCFIN = space(5)
        this.w_DESMARFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_2_39'),i_cWhere,'GSAR_AIV',"Voci iva",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESCRIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_DESCRIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODIVA = space(5)
        this.w_DESCRIVA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORABIT
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORABIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_FORABIT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORABIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORABIT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORABIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORABIT_2_42'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORABIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORABIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_FORABIT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORABIT = NVL(_Link_.ANCODICE,space(15))
      this.w_FODESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORABIT = space(15)
      endif
      this.w_FODESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_FORABIT = space(15)
        this.w_FODESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORABIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORULTC
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORULTC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_FORULTC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORULTC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORULTC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORULTC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORULTC_2_45'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORULTC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORULTC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_FORULTC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORULTC = NVL(_Link_.ANCODICE,space(15))
      this.w_FUDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORULTC = space(15)
      endif
      this.w_FUDESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_FORULTC = space(15)
        this.w_FUDESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORULTC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATRIF_1_1.value==this.w_DATRIF)
      this.oPgFrm.Page1.oPag.oDATRIF_1_1.value=this.w_DATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oSCHEDINI_1_8.value==this.w_SCHEDINI)
      this.oPgFrm.Page1.oPag.oSCHEDINI_1_8.value=this.w_SCHEDINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCHEDFIN_1_9.value==this.w_SCHEDFIN)
      this.oPgFrm.Page1.oPag.oSCHEDFIN_1_9.value=this.w_SCHEDFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCRITER_1_10.RadioValue()==this.w_CRITER)
      this.oPgFrm.Page1.oPag.oCRITER_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPUMI_1_11.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page1.oPag.oTIPUMI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCHEDINI_1_14.value==this.w_DESCHEDINI)
      this.oPgFrm.Page1.oPag.oDESCHEDINI_1_14.value=this.w_DESCHEDINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCHEDFIN_1_15.value==this.w_DESCHEDFIN)
      this.oPgFrm.Page1.oPag.oDESCHEDFIN_1_15.value=this.w_DESCHEDFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLINI_2_4.value==this.w_DESLINI)
      this.oPgFrm.Page2.oPag.oDESLINI_2_4.value=this.w_DESLINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLFIN_2_5.value==this.w_DESLFIN)
      this.oPgFrm.Page2.oPag.oDESLFIN_2_5.value=this.w_DESLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRINI_2_7.value==this.w_DESGRINI)
      this.oPgFrm.Page2.oPag.oDESGRINI_2_7.value=this.w_DESGRINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRFIN_2_8.value==this.w_DESGRFIN)
      this.oPgFrm.Page2.oPag.oDESGRFIN_2_8.value=this.w_DESGRFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oARTINI_2_11.value==this.w_ARTINI)
      this.oPgFrm.Page2.oPag.oARTINI_2_11.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_12.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_12.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oARTFIN_2_13.value==this.w_ARTFIN)
      this.oPgFrm.Page2.oPag.oARTFIN_2_13.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_14.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_14.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oLISTINI_2_15.value==this.w_LISTINI)
      this.oPgFrm.Page2.oPag.oLISTINI_2_15.value=this.w_LISTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oLISTFIN_2_17.value==this.w_LISTFIN)
      this.oPgFrm.Page2.oPag.oLISTFIN_2_17.value=this.w_LISTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMERINI_2_18.value==this.w_GRUMERINI)
      this.oPgFrm.Page2.oPag.oGRUMERINI_2_18.value=this.w_GRUMERINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMERFIN_2_19.value==this.w_GRUMERFIN)
      this.oPgFrm.Page2.oPag.oGRUMERFIN_2_19.value=this.w_GRUMERFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMINI_2_21.value==this.w_FAMINI)
      this.oPgFrm.Page2.oPag.oFAMINI_2_21.value=this.w_FAMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMINI_2_22.value==this.w_DESFAMINI)
      this.oPgFrm.Page2.oPag.oDESFAMINI_2_22.value=this.w_DESFAMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMFIN_2_23.value==this.w_FAMFIN)
      this.oPgFrm.Page2.oPag.oFAMFIN_2_23.value=this.w_FAMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMFIN_2_24.value==this.w_DESFAMFIN)
      this.oPgFrm.Page2.oPag.oDESFAMFIN_2_24.value=this.w_DESFAMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARICINI_2_27.value==this.w_CLARICINI)
      this.oPgFrm.Page2.oPag.oCLARICINI_2_27.value=this.w_CLARICINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLINI_2_28.value==this.w_DESCLINI)
      this.oPgFrm.Page2.oPag.oDESCLINI_2_28.value=this.w_DESCLINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLFIN_2_29.value==this.w_DESCLFIN)
      this.oPgFrm.Page2.oPag.oDESCLFIN_2_29.value=this.w_DESCLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARICFIN_2_32.value==this.w_CLARICFIN)
      this.oPgFrm.Page2.oPag.oCLARICFIN_2_32.value=this.w_CLARICFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIORINI_1_16.value==this.w_PRIORINI)
      this.oPgFrm.Page1.oPag.oPRIORINI_1_16.value=this.w_PRIORINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIORFIN_1_17.value==this.w_PRIORFIN)
      this.oPgFrm.Page1.oPag.oPRIORFIN_1_17.value=this.w_PRIORFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_22.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGGIO_1_31.RadioValue()==this.w_FLAGGIO)
      this.oPgFrm.Page1.oPag.oFLAGGIO_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMARCINI_2_37.value==this.w_MARCINI)
      this.oPgFrm.Page2.oPag.oMARCINI_2_37.value=this.w_MARCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMARCFIN_2_38.value==this.w_MARCFIN)
      this.oPgFrm.Page2.oPag.oMARCFIN_2_38.value=this.w_MARCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODIVA_2_39.value==this.w_CODIVA)
      this.oPgFrm.Page2.oPag.oCODIVA_2_39.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARINI_2_40.value==this.w_DESMARINI)
      this.oPgFrm.Page2.oPag.oDESMARINI_2_40.value=this.w_DESMARINI
    endif
    if not(this.oPgFrm.Page2.oPag.oFORABIT_2_42.value==this.w_FORABIT)
      this.oPgFrm.Page2.oPag.oFORABIT_2_42.value=this.w_FORABIT
    endif
    if not(this.oPgFrm.Page2.oPag.oFODESCRI_2_44.value==this.w_FODESCRI)
      this.oPgFrm.Page2.oPag.oFODESCRI_2_44.value=this.w_FODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFORULTC_2_45.value==this.w_FORULTC)
      this.oPgFrm.Page2.oPag.oFORULTC_2_45.value=this.w_FORULTC
    endif
    if not(this.oPgFrm.Page2.oPag.oFUDESCRI_2_47.value==this.w_FUDESCRI)
      this.oPgFrm.Page2.oPag.oFUDESCRI_2_47.value=this.w_FUDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARFIN_2_48.value==this.w_DESMARFIN)
      this.oPgFrm.Page2.oPag.oDESMARFIN_2_48.value=this.w_DESMARFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRIVA_2_51.value==this.w_DESCRIVA)
      this.oPgFrm.Page2.oPag.oDESCRIVA_2_51.value=this.w_DESCRIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATRIF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATRIF_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATRIF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VALINI<=.w_DATRIF AND .w_VALFIN>=.w_DATRIF) AND ((empty(.w_SCHEDFIN)) OR  (.w_SCHEDINI<=.w_SCHEDFIN)))  and not(empty(.w_SCHEDINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCHEDINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Scheda non valida o intervallo di schede errato")
          case   not((.w_VALIN2<=.w_DATRIF AND .w_VALFIN2>=.w_DATRIF) AND (.w_SCHEDFIN>=.w_SCHEDINI or empty(.w_SCHEDFIN)))  and not(empty(.w_SCHEDFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCHEDFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Scheda non valida o intervallo di schede errato")
          case   not(((empty(.w_artfin)) OR  (UPPER(.w_artini)<= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTINI_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTFIN_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not((EMPTY(.w_DTOBSLIS) OR .w_DTOBSLIS>.w_DATRIF) AND ((empty(.w_LISTFIN)) OR  (.w_LISTINI<=.w_LISTFIN)))  and not(empty(.w_LISTINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLISTINI_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto")
          case   not((EMPTY(.w_DTOBSLIS) OR .w_DTOBSLIS>.w_DATRIF ) AND (.w_LISTFIN>=.w_LISTINI or empty(.w_LISTFIN)))  and not(empty(.w_LISTFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLISTFIN_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto")
          case   not((empty(.w_GRUMERFIN)) OR  (.w_GRUMERINI<=.w_GRUMERFIN))  and not(empty(.w_GRUMERINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUMERINI_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(.w_GRUMERFIN>=.w_GRUMERINI or empty(.w_GRUMERFIN))  and not(empty(.w_GRUMERFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUMERFIN_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(empty(.w_FAMFIN) OR  .w_FAMINI<=.w_FAMFIN)  and not(empty(.w_FAMINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMINI_2_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(empty(.w_FAMFIN) OR  .w_FAMINI<=.w_FAMFIN)  and not(empty(.w_FAMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMFIN_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(empty(.w_CLARICFIN) OR  .w_CLARICINI<=.w_CLARICFIN)  and not(empty(.w_CLARICINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLARICINI_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(empty(.w_CLARICFIN) OR  .w_CLARICINI<=.w_CLARICFIN)  and not(empty(.w_CLARICFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLARICFIN_2_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not((empty(.w_PRIORFIN)) OR  (.w_PRIORINI<=.w_PRIORFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRIORINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'ordine di elaborazione di inizio selezione � maggiore dell'ordine di elaborazione di fine selezione")
          case   not((.w_PRIORFIN>=.w_PRIORINI or empty(.w_PRIORFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRIORFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'ordine di elaborazione di inizio selezione � maggiore dell'ordine di elaborazione di fine selezione")
          case   not(empty(.w_MARCFIN) OR  .w_MARCINI<=.w_MARCFIN)  and not(empty(.w_MARCINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMARCINI_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(empty(.w_MARCFIN) OR  .w_MARCINI<=.w_MARCFIN)  and not(empty(.w_MARCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMARCFIN_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODIVA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODIVA_2_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF))  and not(empty(.w_FORABIT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORABIT_2_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATRIF))  and not(empty(.w_FORULTC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORULTC_2_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATRIF = this.w_DATRIF
    this.o_CRITER = this.w_CRITER
    return

enddefine

* --- Define pages as container
define class tgsva_kasPag1 as StdContainer
  Width  = 839
  height = 501
  stdWidth  = 839
  stdheight = 501
  resizeXpos=390
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATRIF_1_1 as StdField with uid="WFZKVFRNAF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATRIF", cQueryName = "DATRIF",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per la validit� delle schede di calcolo",;
    HelpContextID = 218295498,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=141, Top=17


  add object oObj_1_6 as cp_runprogram with uid="DNCXRYGZBW",left=108, top=513, width=239,height=25,;
    caption='GSVA_BAS(S)',;
   bGlobalFont=.t.,;
    prg='GSVA_BAS("S")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 263247047


  add object oObj_1_7 as cp_runprogram with uid="QKHRGIARBC",left=108, top=540, width=239,height=25,;
    caption='GSVA_BAS(D)',;
   bGlobalFont=.t.,;
    prg='GSVA_BAS("D")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 263250887

  add object oSCHEDINI_1_8 as StdField with uid="EOWVIKPGGF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCHEDINI", cQueryName = "SCHEDINI",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Scheda non valida o intervallo di schede errato",;
    ToolTipText = "Scheda di inizio selezione",;
    HelpContextID = 174107025,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=141, Top=42, cSayPict="'9999999999'", cGetPict="'9999999999'", InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLLSMAST", cZoomOnZoom="GSVA1BBC", oKey_1_1="CLSERIAL", oKey_1_2="this.w_SCHEDINI"

  func oSCHEDINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCHEDINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCHEDINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLLSMAST','*','CLSERIAL',cp_AbsName(this.parent,'oSCHEDINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA1BBC',"Schede di calcolo",'',this.parent.oContained
  endproc
  proc oSCHEDINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSVA1BBC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLSERIAL=this.parent.oContained.w_SCHEDINI
     i_obj.ecpSave()
  endproc

  add object oSCHEDFIN_1_9 as StdField with uid="ZSKPYKNVMF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCHEDFIN", cQueryName = "SCHEDFIN",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Scheda non valida o intervallo di schede errato",;
    ToolTipText = "Scheda di fine selezione",;
    HelpContextID = 43996788,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=141, Top=67, cSayPict="'9999999999'", cGetPict="'9999999999'", InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CLLSMAST", cZoomOnZoom="GSVA1BBC", oKey_1_1="CLSERIAL", oKey_1_2="this.w_SCHEDFIN"

  func oSCHEDFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCHEDFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCHEDFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLLSMAST','*','CLSERIAL',cp_AbsName(this.parent,'oSCHEDFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA1BBC',"Schede di calcolo",'',this.parent.oContained
  endproc
  proc oSCHEDFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSVA1BBC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CLSERIAL=this.parent.oContained.w_SCHEDFIN
     i_obj.ecpSave()
  endproc


  add object oCRITER_1_10 as StdCombo with uid="ZEDDEXLZRK",rtseq=6,rtrep=.f.,left=141,top=93,width=273,height=21;
    , ToolTipText = "Criterio di aggiornamento dei listini";
    , HelpContextID = 21072858;
    , cFormVar="w_CRITER",RowSource=""+"Prezzo di listino,"+"Prezzo di listino al netto degli sconti,"+"Ultimo costo,"+"Costo medio ponderato annuo,"+"Costo medio ponderato periodo,"+"LIFO continuo,"+"LIFO scatti,"+"FIFO,"+"Costo standard,"+"Ultimo costo dei saldi,"+"Ultimo prezzo dei saldi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRITER_1_10.RadioValue()
    return(iif(this.value =1,'LI',;
    iif(this.value =2,'LN',;
    iif(this.value =3,'IU',;
    iif(this.value =4,'IA',;
    iif(this.value =5,'IP',;
    iif(this.value =6,'IL',;
    iif(this.value =7,'IS',;
    iif(this.value =8,'IF',;
    iif(this.value =9,'CS',;
    iif(this.value =10,'UC',;
    iif(this.value =11,'UP',;
    iif(this.value =12,'TU',;
    space(2))))))))))))))
  endfunc
  func oCRITER_1_10.GetRadio()
    this.Parent.oContained.w_CRITER = this.RadioValue()
    return .t.
  endfunc

  func oCRITER_1_10.SetRadio()
    this.Parent.oContained.w_CRITER=trim(this.Parent.oContained.w_CRITER)
    this.value = ;
      iif(this.Parent.oContained.w_CRITER=='LI',1,;
      iif(this.Parent.oContained.w_CRITER=='LN',2,;
      iif(this.Parent.oContained.w_CRITER=='IU',3,;
      iif(this.Parent.oContained.w_CRITER=='IA',4,;
      iif(this.Parent.oContained.w_CRITER=='IP',5,;
      iif(this.Parent.oContained.w_CRITER=='IL',6,;
      iif(this.Parent.oContained.w_CRITER=='IS',7,;
      iif(this.Parent.oContained.w_CRITER=='IF',8,;
      iif(this.Parent.oContained.w_CRITER=='CS',9,;
      iif(this.Parent.oContained.w_CRITER=='UC',10,;
      iif(this.Parent.oContained.w_CRITER=='UP',11,;
      iif(this.Parent.oContained.w_CRITER=='TU',12,;
      0))))))))))))
  endfunc


  add object oTIPUMI_1_11 as StdCombo with uid="ZLVISPSMQV",rtseq=7,rtrep=.f.,left=444,top=93,width=120,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima o seconda)";
    , HelpContextID = 163587018;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPUMI_1_11.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPUMI_1_11.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_1_11.SetRadio()
    this.Parent.oContained.w_TIPUMI=trim(this.Parent.oContained.w_TIPUMI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI=='P',1,;
      iif(this.Parent.oContained.w_TIPUMI=='S',2,;
      0))
  endfunc

  func oTIPUMI_1_11.mHide()
    with this.Parent.oContained
      return (!LEFT(.w_CRITER,1)='L')
    endwith
  endfunc

  add object oDESCHEDINI_1_14 as StdField with uid="ASNQTJLGUV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCHEDINI", cQueryName = "DESCHEDINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 31348063,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=237, Top=42, InputMask=replicate('X',50)

  add object oDESCHEDFIN_1_15 as StdField with uid="CEOAKWHHVM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCHEDFIN", cQueryName = "DESCHEDFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 31349260,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=237, Top=67, InputMask=replicate('X',50)

  add object oPRIORINI_1_16 as StdField with uid="YAMJSAEBYE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PRIORINI", cQueryName = "PRIORINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'ordine di elaborazione di inizio selezione � maggiore dell'ordine di elaborazione di fine selezione",;
    ToolTipText = "Ordine di elaborazione di inizio selezione",;
    HelpContextID = 158763713,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=141, Top=116, cSayPict='"999"', cGetPict='"999"'

  func oPRIORINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_PRIORFIN)) OR  (.w_PRIORINI<=.w_PRIORFIN))
    endwith
    return bRes
  endfunc

  add object oPRIORFIN_1_17 as StdField with uid="OBLYDVCIOC",rtseq=33,rtrep=.f.,;
    cFormVar = "w_PRIORFIN", cQueryName = "PRIORFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'ordine di elaborazione di inizio selezione � maggiore dell'ordine di elaborazione di fine selezione",;
    ToolTipText = "Ordine di elaborazione di fine selezione",;
    HelpContextID = 59340100,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=141, Top=141, cSayPict='"999"', cGetPict='"999"'

  func oPRIORFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PRIORFIN>=.w_PRIORINI or empty(.w_PRIORFIN)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="OWJAVZXBGE",left=610, top=42, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 44117014;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do Lanciazoom with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DATRIF))
      endwith
    endif
  endfunc


  add object Zoom as cp_szoombox with uid="HVNHBNCDGU",left=-6, top=166, width=860,height=284,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSVA_KAS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="CLLSMAST",cMenuFile="",bQueryOnLoad=.f.,cZoomOnZoom="GSVA1BBC",;
    cEvent = "Lanciazoom",;
    nPag=1;
    , HelpContextID = 45192422

  add object oSELEZI_1_22 as StdRadio with uid="ZXLDALFOBQ",rtseq=34,rtrep=.f.,left=28, top=453, width=151,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_22.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 151021530
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 151021530
      this.Buttons(2).Top=15
      this.SetAll("Width",149)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_22.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_22.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_23 as StdButton with uid="IIIHCGWDQG",left=735, top=456, width=48,height=45,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare listini e basi di calcolo in seguito alle modifiche effettuate";
    , HelpContextID = 243963545;
    , tabStop=.f., Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSVA_BAS(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CLSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="VAYMUUHAYT",left=785, top=456, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125487802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_25 as cp_calclbl with uid="PNKQPMDGQW",left=445, top=148, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 132805130

  add object oFLAGGIO_1_31 as StdCheck with uid="NVSQBRFIPR",rtseq=41,rtrep=.f.,left=445, top=123, caption="Aggiorna basi di calcolo",;
    ToolTipText = "Se attivo, alla pressione del bottone aggiorna vengono aggiornate le basi di calcolo",;
    HelpContextID = 170856874,;
    cFormVar="w_FLAGGIO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oFLAGGIO_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLAGGIO_1_31.GetRadio()
    this.Parent.oContained.w_FLAGGIO = this.RadioValue()
    return .t.
  endfunc

  func oFLAGGIO_1_31.SetRadio()
    this.Parent.oContained.w_FLAGGIO=trim(this.Parent.oContained.w_FLAGGIO)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGGIO=='S',1,;
      0)
  endfunc

  func oFLAGGIO_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (left(.w_CRITER,1) $'LC')
    endwith
   endif
  endfunc

  add object oStr_1_2 as StdString with uid="CXJQUBYAPJ",Visible=.t., Left=35, Top=17,;
    Alignment=1, Width=102, Height=18,;
    Caption="Data riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="HAQSDIXQTT",Visible=.t., Left=2, Top=92,;
    Alignment=1, Width=135, Height=18,;
    Caption="Base di calcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QQRGWJZHVN",Visible=.t., Left=35, Top=67,;
    Alignment=1, Width=102, Height=18,;
    Caption="A scheda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="XMMVZSQQCC",Visible=.t., Left=35, Top=42,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da scheda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="GQUBPVUXKP",Visible=.t., Left=35, Top=116,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZXJLYFIGTH",Visible=.t., Left=35, Top=141,;
    Alignment=1, Width=102, Height=18,;
    Caption="A ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="WASNBKROBS",Visible=.t., Left=482, Top=148,;
    Alignment=0, Width=130, Height=17,;
    Caption="Listino da creare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsva_kasPag2 as StdContainer
  Width  = 839
  height = 501
  stdWidth  = 839
  stdheight = 501
  resizeXpos=386
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESLINI_2_4 as StdField with uid="FKDHWASRGD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESLINI", cQueryName = "DESLINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 183961398,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=96, InputMask=replicate('X',40)

  add object oDESLFIN_2_5 as StdField with uid="KCEULWISVW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESLFIN", cQueryName = "DESLFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 171505866,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=120, InputMask=replicate('X',40)

  add object oDESGRINI_2_7 as StdField with uid="CLTAFFWTRW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESGRINI", cQueryName = "DESGRINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159250561,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=144, InputMask=replicate('X',35)

  add object oDESGRFIN_2_8 as StdField with uid="KQENDFRIHA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESGRFIN", cQueryName = "DESGRFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 58853252,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=168, InputMask=replicate('X',35)

  add object oARTINI_2_11 as StdField with uid="TFOXAFTDJV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 163306490,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=148, Top=44, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oARTINI_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_2_12 as StdField with uid="CHMQSNFQMU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 163313866,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=317, Top=44, InputMask=replicate('X',40)

  add object oARTFIN_2_13 as StdField with uid="PFSBOHPKDW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 84859898,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=148, Top=70, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oARTFIN_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_2_14 as StdField with uid="TPNLPGVTQY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84867274,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=317, Top=70, InputMask=replicate('X',40)

  add object oLISTINI_2_15 as StdField with uid="HUNHNSYRIN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_LISTINI", cQueryName = "LISTINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto",;
    ToolTipText = "Codice listino da aggiornare di inizio selezione",;
    HelpContextID = 184486838,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINI"

  func oLISTINI_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINI_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINI_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINI_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"",'',this.parent.oContained
  endproc
  proc oLISTINI_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINI
     i_obj.ecpSave()
  endproc

  add object oLISTFIN_2_17 as StdField with uid="MKYHKECCTZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_LISTFIN", cQueryName = "LISTFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale o � inesistente o obsoleto",;
    ToolTipText = "Codice listino da aggiornare di fine selezione",;
    HelpContextID = 170980426,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTFIN"

  func oLISTFIN_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTFIN_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTFIN_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTFIN_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGRUMERINI_2_18 as StdField with uid="PDCZRZBJCN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_GRUMERINI", cQueryName = "GRUMERINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "gruppo merceologico di inizio selezione",;
    HelpContextID = 246954308,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMERINI"

  func oGRUMERINI_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMERINI_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMERINI_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMERINI_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMERINI_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMERINI
     i_obj.ecpSave()
  endproc

  add object oGRUMERFIN_2_19 as StdField with uid="XGJPWJDLPQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_GRUMERFIN", cQueryName = "GRUMERFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "gruppo merceologico di fine selezione",;
    HelpContextID = 246954383,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMERFIN"

  func oGRUMERFIN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMERFIN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMERFIN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMERFIN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMERFIN_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMERFIN
     i_obj.ecpSave()
  endproc

  add object oFAMINI_2_21 as StdField with uid="TIMHSCSIDL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_FAMINI", cQueryName = "FAMINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Famiglia articolo di inizio selezione",;
    HelpContextID = 163339434,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=192, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMINI"

  func oFAMINI_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMINI_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMINI_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMINI_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie",'',this.parent.oContained
  endproc
  proc oFAMINI_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMINI
     i_obj.ecpSave()
  endproc

  add object oDESFAMINI_2_22 as StdField with uid="DFPKWJWJKQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESFAMINI", cQueryName = "DESFAMINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 158403604,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=192, InputMask=replicate('X',35)

  add object oFAMFIN_2_23 as StdField with uid="TFVYEPCWYH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_FAMFIN", cQueryName = "FAMFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Famiglia articolo di fine selezione",;
    HelpContextID = 84892842,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=216, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMFIN"

  func oFAMFIN_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMFIN_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMFIN_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMFIN_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie",'',this.parent.oContained
  endproc
  proc oFAMFIN_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMFIN
     i_obj.ecpSave()
  endproc

  add object oDESFAMFIN_2_24 as StdField with uid="LFJLKZREDP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESFAMFIN", cQueryName = "DESFAMFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 158403679,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=216, InputMask=replicate('X',35)

  add object oCLARICINI_2_27 as StdField with uid="QINCTUNIXQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CLARICINI", cQueryName = "CLARICINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Classe di ricarico di inizio selezione",;
    HelpContextID = 268169988,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=240, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_CLARICINI"

  func oCLARICINI_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLARICINI_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLARICINI_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oCLARICINI_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico dei listini",'',this.parent.oContained
  endproc
  proc oCLARICINI_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_CLARICINI
     i_obj.ecpSave()
  endproc

  add object oDESCLINI_2_28 as StdField with uid="NTVPXNRHWC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCLINI", cQueryName = "DESCLINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 165804161,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=240, InputMask=replicate('X',35)

  add object oDESCLFIN_2_29 as StdField with uid="GSADXGGHOA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCLFIN", cQueryName = "DESCLFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52299652,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=217, Top=264, InputMask=replicate('X',35)

  add object oCLARICFIN_2_32 as StdField with uid="KKOXURYONH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CLARICFIN", cQueryName = "CLARICFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Classe di ricarico di fine selezione",;
    HelpContextID = 268170063,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=264, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_CLARICFIN"

  func oCLARICFIN_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLARICFIN_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLARICFIN_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oCLARICFIN_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico dei listini",'',this.parent.oContained
  endproc
  proc oCLARICFIN_2_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_CLARICFIN
     i_obj.ecpSave()
  endproc

  add object oMARCINI_2_37 as StdField with uid="AYENGNQTZY",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MARCINI", cQueryName = "MARCINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Marca articolo di inizio selezione",;
    HelpContextID = 183366598,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=289, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARCINI"

  func oMARCINI_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARCINI_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARCINI_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARCINI_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARCINI_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARCINI
     i_obj.ecpSave()
  endproc

  add object oMARCFIN_2_38 as StdField with uid="QZMFXLOKRE",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MARCFIN", cQueryName = "MARCFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    ToolTipText = "Marca articolo di fine selezione",;
    HelpContextID = 172100666,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=315, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARCFIN"

  func oMARCFIN_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARCFIN_2_38.ecpDrop(oSource)
    this.Parent.oContained.link_2_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARCFIN_2_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARCFIN_2_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMARCFIN_2_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARCFIN
     i_obj.ecpSave()
  endproc

  add object oCODIVA_2_39 as StdField with uid="QMIJMBDYFI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA articolo di selezione",;
    HelpContextID = 20766426,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=341, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci iva",'',this.parent.oContained
  endproc
  proc oCODIVA_2_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oDESMARINI_2_40 as StdField with uid="PLAOCFNXPP",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESMARINI", cQueryName = "DESMARINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242748436,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=217, Top=288, InputMask=replicate('X',35)

  add object oFORABIT_2_42 as StdField with uid="DMKRORRVUP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_FORABIT", cQueryName = "FORABIT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Fornitore abituale di selezione",;
    HelpContextID = 92012886,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=147, Top=367, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORABIT"

  func oFORABIT_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORABIT_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORABIT_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORABIT_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oFORABIT_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_FORABIT
     i_obj.ecpSave()
  endproc

  add object oFODESCRI_2_44 as StdField with uid="DTCOKRUCSH",rtseq=49,rtrep=.f.,;
    cFormVar = "w_FODESCRI", cQueryName = "FODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9380255,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=367, InputMask=replicate('X',40)

  add object oFORULTC_2_45 as StdField with uid="XXJBTNJEPR",rtseq=50,rtrep=.f.,;
    cFormVar = "w_FORULTC", cQueryName = "FORULTC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Fornitore dal quale l'articolo � stato acquistato",;
    HelpContextID = 19923286,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=146, Top=393, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORULTC"

  func oFORULTC_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORULTC_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORULTC_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORULTC_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oFORULTC_2_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_FORULTC
     i_obj.ecpSave()
  endproc

  add object oFUDESCRI_2_47 as StdField with uid="HFLIWQSWEU",rtseq=51,rtrep=.f.,;
    cFormVar = "w_FUDESCRI", cQueryName = "FUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9381791,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=393, InputMask=replicate('X',40)

  add object oDESMARFIN_2_48 as StdField with uid="DWNIHXINHO",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESMARFIN", cQueryName = "DESMARFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 242748511,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=217, Top=314, InputMask=replicate('X',35)

  add object oDESCRIVA_2_51 as StdField with uid="BOHSKZBYUS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCRIVA", cQueryName = "DESCRIVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 108922743,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=217, Top=341, InputMask=replicate('X',35)

  add object oStr_2_2 as StdString with uid="PCZAMPAKED",Visible=.t., Left=5, Top=120,;
    Alignment=1, Width=140, Height=18,;
    Caption="A listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="KABPBQPOQQ",Visible=.t., Left=5, Top=96,;
    Alignment=1, Width=140, Height=18,;
    Caption="Da listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="MXMLRHGTRI",Visible=.t., Left=5, Top=144,;
    Alignment=1, Width=140, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="GWRIPHUDOV",Visible=.t., Left=5, Top=168,;
    Alignment=1, Width=140, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="FNODJPRENP",Visible=.t., Left=5, Top=44,;
    Alignment=1, Width=140, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="OXEGJSQCOM",Visible=.t., Left=5, Top=70,;
    Alignment=1, Width=140, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="XXPPPXCZYR",Visible=.t., Left=5, Top=192,;
    Alignment=1, Width=140, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="GLABUQUJZT",Visible=.t., Left=5, Top=216,;
    Alignment=1, Width=140, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="AAFRIQTGBA",Visible=.t., Left=5, Top=240,;
    Alignment=1, Width=140, Height=18,;
    Caption="Da classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="CISTLCPYLP",Visible=.t., Left=5, Top=264,;
    Alignment=1, Width=140, Height=18,;
    Caption="A classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="AQJLNGYDYC",Visible=.t., Left=5, Top=289,;
    Alignment=1, Width=140, Height=18,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="ZOZVIDOJBG",Visible=.t., Left=5, Top=367,;
    Alignment=1, Width=140, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="PSKSMHLRJK",Visible=.t., Left=5, Top=393,;
    Alignment=1, Width=140, Height=18,;
    Caption="Fornitore ult.costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="CBRGRLGXXV",Visible=.t., Left=5, Top=315,;
    Alignment=1, Width=140, Height=18,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="IFMIEQVNIM",Visible=.t., Left=5, Top=341,;
    Alignment=1, Width=140, Height=15,;
    Caption="Cod. IVA:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kas','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsva_kas
* --- Lancia lo zoom ed avvisa se il risultato � vuoto
proc Lanciazoom(pParent)
   pParent.NotifyEvent("Lanciazoom")
   If reccount(pParent.w_Zoom.cCursor)<=0
    ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
   Endif
endproc


* --- Blocca colonne Zoom
* --- per rpboema di c0005
proc Bloccazoom(oZoom)
   * --- Disabilito la possibilit� di muovere le colonne
   * --- x problema c0005...
   local ni
   For ni=1 to oZoom.Grd.ColumnCount
    oZoom.Grd.Columns[ni].movable=.f.
   endfor
endproc
* --- Fine Area Manuale
