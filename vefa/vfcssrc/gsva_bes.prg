* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bes                                                        *
*              ELIMINAZIONE STRUTTURA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-14                                                      *
* Last revis.: 2015-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bes",oParentObject)
return(i_retval)

define class tgsva_bes as StdBatch
  * --- Local variables
  w_OK = .f.
  w_RESULT = 0
  w_CURSORNA = space(10)
  w_ELCODICE = space(10)
  * --- WorkFile variables
  VAFORMAT_idx=0
  VASTRUTT_idx=0
  VAELEMEN_idx=0
  VADETTFO_idx=0
  VATRASCO_idx=0
  TIP_DOCU_idx=0
  VAPREDEF_idx=0
  STR_INTE_idx=0
  PAT_FILE_idx=0
  VAVARNOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSVA_KDS
    this.w_OK = .T.
    * --- Elimino le strutture
    if this.oParentObject.w_CHKROOT=this.oParentObject.w_DELROOT
      this.w_OK = Ah_yesno("Verranno cancellati tutti gli elementi, formati e trascodifiche della struttura selezionata. Si desidera procedere?")
    else
      this.w_OK = Ah_yesno("Verr� cancellato l'elemento %1 della struttura %2 e tutti gli elementi ad esso collegati. Si desidera procedere?","",ALLTRIM(this.oParentObject.w_DELROOT), ALLTRIM(this.oParentObject.w_CODSTR))
    endif
    if this.w_OK
      if this.oParentObject.w_CHKROOT=this.oParentObject.w_DELROOT
        * --- Select from TIP_DOCU
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select TDCODSTR,TDTIPDOC  from "+i_cTable+" TIP_DOCU ";
              +" where TDCODSTR="+cp_ToStrODBC(this.oParentObject.w_CODSTR)+"";
               ,"_Curs_TIP_DOCU")
        else
          select TDCODSTR,TDTIPDOC from (i_cTable);
           where TDCODSTR=this.oParentObject.w_CODSTR;
            into cursor _Curs_TIP_DOCU
        endif
        if used('_Curs_TIP_DOCU')
          select _Curs_TIP_DOCU
          locate for 1=1
          do while not(eof())
          ah_ErrorMsg("Attenzione struttura utilizzata nella causale %1",,"",ALLTRIM(_Curs_TIP_DOCU.TDTIPDOC))
          this.w_OK = .F.
            select _Curs_TIP_DOCU
            continue
          enddo
          use
        endif
      endif
      if this.w_OK
        if this.oParentObject.w_CHKROOT=this.oParentObject.w_DELROOT
          * --- Select from STR_INTE
          i_nConn=i_TableProp[this.STR_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STR_INTE_idx,2],.t.,this.STR_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SICODSTR,SICODCON,SITIPDOC  from "+i_cTable+" STR_INTE ";
                +" where SICODSTR="+cp_ToStrODBC(this.oParentObject.w_CODSTR)+"";
                 ,"_Curs_STR_INTE")
          else
            select SICODSTR,SICODCON,SITIPDOC from (i_cTable);
             where SICODSTR=this.oParentObject.w_CODSTR;
              into cursor _Curs_STR_INTE
          endif
          if used('_Curs_STR_INTE')
            select _Curs_STR_INTE
            locate for 1=1
            do while not(eof())
            ah_ErrorMsg("Attenzione struttura utilizzata nell'ntestatario %1 per la causale %2",,"", ALLTRIM(_Curs_STR_INTE.SICODCON), ALLTRIM(_Curs_STR_INTE.SITIPDOC))
            this.w_OK = .F.
              select _Curs_STR_INTE
              continue
            enddo
            use
          endif
        endif
        if this.w_OK
          * --- Try
          local bErr_02C0AE58
          bErr_02C0AE58=bTrsErr
          this.Try_02C0AE58()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore durante l'eliminazione")
          endif
          bTrsErr=bTrsErr or bErr_02C0AE58
          * --- End
          this.oParentObject.w_CODSTR = Space(10)
          this.oParentObject.w_DESCRI = SPACE(35)
          this.oParentObject.w_DELROOT = SPACE(20)
          this.oParentObject.w_CHKROOT = SPACE(20)
        endif
      endif
    endif
  endproc
  proc Try_02C0AE58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_CHKROOT=this.oParentObject.w_DELROOT
      ah_Msg("Eliminazione trascodifiche")
      * --- Delete from VATRASCO
      i_nConn=i_TableProp[this.VATRASCO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"TRCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              TRCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Elimino gli elementi
      ah_Msg("Eliminazione elementi")
      * --- Delete from VAELEMEN
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ELCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              ELCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Elimino i Formati
      ah_Msg("eliminazione formati")
      * --- Delete from VADETTFO
      i_nConn=i_TableProp[this.VADETTFO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VADETTFO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"DFCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              DFCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from VAFORMAT
      i_nConn=i_TableProp[this.VAFORMAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAFORMAT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"FOCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              FOCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ah_Msg("eliminazione valori predefiniti")
      * --- Delete from VAPREDEF
      i_nConn=i_TableProp[this.VAPREDEF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAPREDEF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              PRCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ah_Msg("eliminazione variabili nome file")
      * --- Delete from VAVARNOM
      i_nConn=i_TableProp[this.VAVARNOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAVARNOM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"VNCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              VNCODICE = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ah_Msg("Eliminazione file associati")
      * --- Delete from PAT_FILE
      i_nConn=i_TableProp[this.PAT_FILE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAT_FILE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PFCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              PFCODSTR = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      ah_Msg("eliminazione strutture")
      * --- Delete from VASTRUTT
      i_nConn=i_TableProp[this.VASTRUTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
               )
      else
        delete from (i_cTable) where;
              STCODICE = this.oParentObject.w_CODSTR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      this.w_CURSORNA = SYS(2015)
      this.w_RESULT = GSAR_BPT(this, this.w_CURSORNA, this.oParentObject.w_CODSTR, this.oParentObject.w_DELROOT)
      if this.w_RESULT<0
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
      SELECT (this.w_CURSORNA)
      GO TOP
      SCAN
      this.w_ELCODICE = ELCODICE
      ah_Msg("Eliminazione trascodifiche elemento %1", .T., .F., .F., ALLTRIM(this.w_ELCODICE))
      * --- Delete from VATRASCO
      i_nConn=i_TableProp[this.VATRASCO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"TRCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
              +" and TRCODELE = "+cp_ToStrODBC(this.w_ELCODICE);
               )
      else
        delete from (i_cTable) where;
              TRCODSTR = this.oParentObject.w_CODSTR;
              and TRCODELE = this.w_ELCODICE;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Elimino gli elementi
      ah_Msg("Eliminazione elemento %1", .T., .F., .F., ALLTRIM(this.w_ELCODICE))
      * --- Delete from VAELEMEN
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ELCODSTR = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
              +" and ELCODICE = "+cp_ToStrODBC(this.w_ELCODICE);
               )
      else
        delete from (i_cTable) where;
              ELCODSTR = this.oParentObject.w_CODSTR;
              and ELCODICE = this.w_ELCODICE;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      SELECT (this.w_CURSORNA)
      ENDSCAN
    endif
    if USED(this.w_CURSORNA)
      SELECT (this.w_CURSORNA)
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Eliminazione completata con successo", 64)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='VAFORMAT'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='VAELEMEN'
    this.cWorkTables[4]='VADETTFO'
    this.cWorkTables[5]='VATRASCO'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='VAPREDEF'
    this.cWorkTables[8]='STR_INTE'
    this.cWorkTables[9]='PAT_FILE'
    this.cWorkTables[10]='VAVARNOM'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_TIP_DOCU')
      use in _Curs_TIP_DOCU
    endif
    if used('_Curs_STR_INTE')
      use in _Curs_STR_INTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
