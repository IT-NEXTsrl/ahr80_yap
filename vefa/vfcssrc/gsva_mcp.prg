* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_mcp                                                        *
*              Configurazioni ordinamento/rottura piani di spedizione          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-10                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_mcp"))

* --- Class definition
define class tgsva_mcp as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 547
  Height = 528+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=171281815
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VACONPIA_IDX = 0
  TIP_DOCU_IDX = 0
  XDC_FIELDS_IDX = 0
  PIA_SPED_IDX = 0
  cFile = "VACONPIA"
  cKeySelect = "CPSERIAL"
  cKeyWhere  = "CPSERIAL=this.w_CPSERIAL"
  cKeyDetail  = "CPSERIAL=this.w_CPSERIAL"
  cKeyWhereODBC = '"CPSERIAL="+cp_ToStrODBC(this.w_CPSERIAL)';

  cKeyDetailWhereODBC = '"CPSERIAL="+cp_ToStrODBC(this.w_CPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"VACONPIA.CPSERIAL="+cp_ToStrODBC(this.w_CPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VACONPIA.CPROWORD'
  cPrg = "gsva_mcp"
  cComment = "Configurazioni ordinamento/rottura piani di spedizione"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 20
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPSERIAL = space(10)
  w_CPDESCRI = space(30)
  w_CPCODCAU = space(5)
  w_CPFLPRED = space(1)
  w_CPROWORD = 0
  w_CPNOMCAM = space(46)
  w_CPFLROTT = space(1)
  w_CPFLORDI = space(1)
  w_TDDESDOC = space(35)
  w_CPTABNAM = space(15)
  w_CPFLDNAM = space(30)
  w_FLCOMMEN = space(80)
  w_TDFLVEAC = space(1)
  w_TDASSCES = space(1)
  w_TDEMERIC = space(1)
  w_TDCATDOC = space(2)
  w_FLCONFUS = space(10)
  w_PSSERIAL = space(10)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CPSERIAL = this.W_CPSERIAL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VACONPIA','gsva_mcp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mcpPag1","gsva_mcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Configurazione ordinamenti/rotture piani di spedizione")
      .Pages(1).HelpContextID = 14499369
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCPSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='PIA_SPED'
    this.cWorkTables[4]='VACONPIA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VACONPIA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VACONPIA_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CPSERIAL = NVL(CPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VACONPIA where CPSERIAL=KeySet.CPSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2],this.bLoadRecFilter,this.VACONPIA_IDX,"gsva_mcp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VACONPIA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VACONPIA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VACONPIA '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CPSERIAL',this.w_CPSERIAL  )
      select * from (i_cTable) VACONPIA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TDDESDOC = space(35)
        .w_TDFLVEAC = space(1)
        .w_TDASSCES = space(1)
        .w_TDEMERIC = space(1)
        .w_TDCATDOC = space(2)
        .w_PSSERIAL = space(10)
        .w_CPSERIAL = NVL(CPSERIAL,space(10))
        .op_CPSERIAL = .w_CPSERIAL
        .w_CPDESCRI = NVL(CPDESCRI,space(30))
        .w_CPCODCAU = NVL(CPCODCAU,space(5))
          if link_1_3_joined
            this.w_CPCODCAU = NVL(TDTIPDOC103,NVL(this.w_CPCODCAU,space(5)))
            this.w_TDDESDOC = NVL(TDDESDOC103,space(35))
            this.w_TDFLVEAC = NVL(TDFLVEAC103,space(1))
            this.w_TDASSCES = NVL(TDASSCES103,space(1))
            this.w_TDEMERIC = NVL(TDEMERIC103,space(1))
            this.w_TDCATDOC = NVL(TDCATDOC103,space(2))
          else
          .link_1_3('Load')
          endif
        .w_CPFLPRED = NVL(CPFLPRED,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLCONFUS = .w_CPSERIAL
          .link_1_15('Load')
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'VACONPIA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_FLCOMMEN = space(80)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CPNOMCAM = NVL(CPNOMCAM,space(46))
          .w_CPFLROTT = NVL(CPFLROTT,space(1))
          .w_CPFLORDI = NVL(CPFLORDI,space(1))
        .w_CPTABNAM = IIF( AT(".", .w_CPNOMCAM) > 0, LEFT(ALLTRIM( .w_CPNOMCAM), AT(".", .w_CPNOMCAM) - 1 ), "")
        .w_CPFLDNAM = IIF( AT(".", ALLTRIM(.w_CPNOMCAM)) > 0,RIGHT(ALLTRIM( .w_CPNOMCAM), LEN(ALLTRIM(.w_CPNOMCAM)) - AT(".", ALLTRIM(.w_CPNOMCAM)) ), "")
          .link_2_6('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLCONFUS = .w_CPSERIAL
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CPSERIAL=space(10)
      .w_CPDESCRI=space(30)
      .w_CPCODCAU=space(5)
      .w_CPFLPRED=space(1)
      .w_CPROWORD=10
      .w_CPNOMCAM=space(46)
      .w_CPFLROTT=space(1)
      .w_CPFLORDI=space(1)
      .w_TDDESDOC=space(35)
      .w_CPTABNAM=space(15)
      .w_CPFLDNAM=space(30)
      .w_FLCOMMEN=space(80)
      .w_TDFLVEAC=space(1)
      .w_TDASSCES=space(1)
      .w_TDEMERIC=space(1)
      .w_TDCATDOC=space(2)
      .w_FLCONFUS=space(10)
      .w_PSSERIAL=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_CPCODCAU))
         .link_1_3('Full')
        endif
        .w_CPFLPRED = 'N'
        .DoRTCalc(5,6,.f.)
        .w_CPFLROTT = 'S'
        .w_CPFLORDI = 'A'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(9,9,.f.)
        .w_CPTABNAM = IIF( AT(".", .w_CPNOMCAM) > 0, LEFT(ALLTRIM( .w_CPNOMCAM), AT(".", .w_CPNOMCAM) - 1 ), "")
        .w_CPFLDNAM = IIF( AT(".", ALLTRIM(.w_CPNOMCAM)) > 0,RIGHT(ALLTRIM( .w_CPNOMCAM), LEN(ALLTRIM(.w_CPNOMCAM)) - AT(".", ALLTRIM(.w_CPNOMCAM)) ), "")
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CPFLDNAM))
         .link_2_6('Full')
        endif
        .DoRTCalc(12,16,.f.)
        .w_FLCONFUS = .w_CPSERIAL
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_FLCONFUS))
         .link_1_15('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'VACONPIA')
    this.DoRTCalc(18,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCPSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oCPDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oCPCODCAU_1_3.enabled = i_bVal
      .Page1.oPag.oCPFLPRED_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCPSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCPSERIAL_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VACONPIA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRPSP","i_codazi,w_CPSERIAL")
      .op_codazi = .w_codazi
      .op_CPSERIAL = .w_CPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPSERIAL,"CPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPDESCRI,"CPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCODCAU,"CPCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPFLPRED,"CPFLPRED",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
    i_lTable = "VACONPIA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VACONPIA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CPNOMCAM C(46);
      ,t_CPFLROTT N(3);
      ,t_CPFLORDI N(3);
      ,t_FLCOMMEN C(80);
      ,CPROWNUM N(10);
      ,t_CPTABNAM C(15);
      ,t_CPFLDNAM C(30);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mcpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPNOMCAM_2_2.controlsource=this.cTrsName+'.t_CPNOMCAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.controlsource=this.cTrsName+'.t_CPFLROTT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.controlsource=this.cTrsName+'.t_CPFLORDI'
    this.oPgFRm.Page1.oPag.oFLCOMMEN_2_7.controlsource=this.cTrsName+'.t_FLCOMMEN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.AddVLine(362)
    this.AddVLine(420)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"PRPSP","i_codazi,w_CPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VACONPIA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
      *
      * insert into VACONPIA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VACONPIA')
        i_extval=cp_InsertValODBCExtFlds(this,'VACONPIA')
        i_cFldBody=" "+;
                  "(CPSERIAL,CPDESCRI,CPCODCAU,CPFLPRED,CPROWORD"+;
                  ",CPNOMCAM,CPFLROTT,CPFLORDI,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPSERIAL)+","+cp_ToStrODBC(this.w_CPDESCRI)+","+cp_ToStrODBCNull(this.w_CPCODCAU)+","+cp_ToStrODBC(this.w_CPFLPRED)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_CPNOMCAM)+","+cp_ToStrODBC(this.w_CPFLROTT)+","+cp_ToStrODBC(this.w_CPFLORDI)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VACONPIA')
        i_extval=cp_InsertValVFPExtFlds(this,'VACONPIA')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CPSERIAL',this.w_CPSERIAL)
        INSERT INTO (i_cTable) (;
                   CPSERIAL;
                  ,CPDESCRI;
                  ,CPCODCAU;
                  ,CPFLPRED;
                  ,CPROWORD;
                  ,CPNOMCAM;
                  ,CPFLROTT;
                  ,CPFLORDI;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPSERIAL;
                  ,this.w_CPDESCRI;
                  ,this.w_CPCODCAU;
                  ,this.w_CPFLPRED;
                  ,this.w_CPROWORD;
                  ,this.w_CPNOMCAM;
                  ,this.w_CPFLROTT;
                  ,this.w_CPFLORDI;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.VACONPIA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VACONPIA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " CPDESCRI="+cp_ToStrODBC(this.w_CPDESCRI)+;
                 ",CPCODCAU="+cp_ToStrODBCNull(this.w_CPCODCAU)+;
                 ",CPFLPRED="+cp_ToStrODBC(this.w_CPFLPRED)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VACONPIA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  CPDESCRI=this.w_CPDESCRI;
                 ,CPCODCAU=this.w_CPCODCAU;
                 ,CPFLPRED=this.w_CPFLPRED;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VACONPIA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VACONPIA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPDESCRI="+cp_ToStrODBC(this.w_CPDESCRI)+;
                     ",CPCODCAU="+cp_ToStrODBCNull(this.w_CPCODCAU)+;
                     ",CPFLPRED="+cp_ToStrODBC(this.w_CPFLPRED)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CPNOMCAM="+cp_ToStrODBC(this.w_CPNOMCAM)+;
                     ",CPFLROTT="+cp_ToStrODBC(this.w_CPFLROTT)+;
                     ",CPFLORDI="+cp_ToStrODBC(this.w_CPFLORDI)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VACONPIA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPDESCRI=this.w_CPDESCRI;
                     ,CPCODCAU=this.w_CPCODCAU;
                     ,CPFLPRED=this.w_CPFLPRED;
                     ,CPROWORD=this.w_CPROWORD;
                     ,CPNOMCAM=this.w_CPNOMCAM;
                     ,CPFLROTT=this.w_CPFLROTT;
                     ,CPFLORDI=this.w_CPFLORDI;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VACONPIA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VACONPIA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,9,.t.)
          .w_CPTABNAM = IIF( AT(".", .w_CPNOMCAM) > 0, LEFT(ALLTRIM( .w_CPNOMCAM), AT(".", .w_CPNOMCAM) - 1 ), "")
          .w_CPFLDNAM = IIF( AT(".", ALLTRIM(.w_CPNOMCAM)) > 0,RIGHT(ALLTRIM( .w_CPNOMCAM), LEN(ALLTRIM(.w_CPNOMCAM)) - AT(".", ALLTRIM(.w_CPNOMCAM)) ), "")
          .link_2_6('Full')
        .DoRTCalc(12,16,.t.)
          .w_FLCONFUS = .w_CPSERIAL
          .link_1_15('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"PRPSP","i_codazi,w_CPSERIAL")
          .op_CPSERIAL = .w_CPSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(18,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPTABNAM with this.w_CPTABNAM
      replace t_CPFLDNAM with this.w_CPFLDNAM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CPCODCAU
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CPCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CPCODCAU))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCODCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CPCODCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCPCODCAU_1_3'),i_cWhere,'',"Causali documenti",'GSVA1APS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CPCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CPCODCAU)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDASSCES,TDEMERIC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCODCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDASSCES = NVL(_Link_.TDASSCES,space(1))
      this.w_TDEMERIC = NVL(_Link_.TDEMERIC,space(1))
      this.w_TDCATDOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CPCODCAU = space(5)
      endif
      this.w_TDDESDOC = space(35)
      this.w_TDFLVEAC = space(1)
      this.w_TDASSCES = space(1)
      this.w_TDEMERIC = space(1)
      this.w_TDCATDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLVEAC='V' AND .w_TDASSCES<>'S' AND .w_TDEMERIC = 'V' AND .w_TDCATDOC='DT'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CPCODCAU = space(5)
        this.w_TDDESDOC = space(35)
        this.w_TDFLVEAC = space(1)
        this.w_TDASSCES = space(1)
        this.w_TDEMERIC = space(1)
        this.w_TDCATDOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.TDTIPDOC as TDTIPDOC103"+ ",link_1_3.TDDESDOC as TDDESDOC103"+ ",link_1_3.TDFLVEAC as TDFLVEAC103"+ ",link_1_3.TDASSCES as TDASSCES103"+ ",link_1_3.TDEMERIC as TDEMERIC103"+ ",link_1_3.TDCATDOC as TDCATDOC103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on VACONPIA.CPCODCAU=link_1_3.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and VACONPIA.CPCODCAU=link_1_3.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CPFLDNAM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPFLDNAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPFLDNAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_CPFLDNAM);
                   +" and TBNAME="+cp_ToStrODBC(this.w_CPTABNAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_CPTABNAM;
                       ,'FLNAME',this.w_CPFLDNAM)
            select TBNAME,FLNAME,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPFLDNAM = NVL(_Link_.FLNAME,space(30))
      this.w_FLCOMMEN = NVL(_Link_.FLCOMMEN,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CPFLDNAM = space(30)
      endif
      this.w_FLCOMMEN = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPFLDNAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FLCONFUS
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_lTable = "PIA_SPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2], .t., this.PIA_SPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FLCONFUS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FLCONFUS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERCOR,PSSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERCOR="+cp_ToStrODBC(this.w_FLCONFUS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERCOR',this.w_FLCONFUS)
            select PSSERCOR,PSSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FLCONFUS = NVL(_Link_.PSSERCOR,space(10))
      this.w_PSSERIAL = NVL(_Link_.PSSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_FLCONFUS = space(10)
      endif
      this.w_PSSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])+'\'+cp_ToStr(_Link_.PSSERCOR,1)
      cp_ShowWarn(i_cKey,this.PIA_SPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FLCONFUS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCPSERIAL_1_1.value==this.w_CPSERIAL)
      this.oPgFrm.Page1.oPag.oCPSERIAL_1_1.value=this.w_CPSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESCRI_1_2.value==this.w_CPDESCRI)
      this.oPgFrm.Page1.oPag.oCPDESCRI_1_2.value=this.w_CPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCODCAU_1_3.value==this.w_CPCODCAU)
      this.oPgFrm.Page1.oPag.oCPCODCAU_1_3.value=this.w_CPCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCPFLPRED_1_4.RadioValue()==this.w_CPFLPRED)
      this.oPgFrm.Page1.oPag.oCPFLPRED_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_7.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_7.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOMMEN_2_7.value==this.w_FLCOMMEN)
      this.oPgFrm.Page1.oPag.oFLCOMMEN_2_7.value=this.w_FLCOMMEN
      replace t_FLCOMMEN with this.oPgFrm.Page1.oPag.oFLCOMMEN_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPNOMCAM_2_2.value==this.w_CPNOMCAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPNOMCAM_2_2.value=this.w_CPNOMCAM
      replace t_CPNOMCAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPNOMCAM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.RadioValue()==this.w_CPFLROTT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.SetRadio()
      replace t_CPFLROTT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.RadioValue()==this.w_CPFLORDI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.SetRadio()
      replace t_CPFLORDI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'VACONPIA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_CPFLPRED<>'S' OR GSVA_BCP( this, 'CHKPRED' ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Esiste gi� una configurazione predefinita globale o per la causale documento indicata"))
          case   not(.w_TDFLVEAC='V' AND .w_TDASSCES<>'S' AND .w_TDEMERIC = 'V' AND .w_TDCATDOC='DT')  and not(empty(.w_CPCODCAU))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCPCODCAU_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsva_mcp
      i_bRes = GSVA_BCP( this, 'CHKDUPFLD')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not(Empty(.w_CPNOMCAM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not(Empty(t_CPNOMCAM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CPNOMCAM=space(46)
      .w_CPFLROTT=space(1)
      .w_CPFLORDI=space(1)
      .w_CPTABNAM=space(15)
      .w_CPFLDNAM=space(30)
      .w_FLCOMMEN=space(80)
      .DoRTCalc(1,6,.f.)
        .w_CPFLROTT = 'S'
        .w_CPFLORDI = 'A'
      .DoRTCalc(9,9,.f.)
        .w_CPTABNAM = IIF( AT(".", .w_CPNOMCAM) > 0, LEFT(ALLTRIM( .w_CPNOMCAM), AT(".", .w_CPNOMCAM) - 1 ), "")
        .w_CPFLDNAM = IIF( AT(".", ALLTRIM(.w_CPNOMCAM)) > 0,RIGHT(ALLTRIM( .w_CPNOMCAM), LEN(ALLTRIM(.w_CPNOMCAM)) - AT(".", ALLTRIM(.w_CPNOMCAM)) ), "")
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_CPFLDNAM))
        .link_2_6('Full')
      endif
    endwith
    this.DoRTCalc(12,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CPNOMCAM = t_CPNOMCAM
    this.w_CPFLROTT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.RadioValue(.t.)
    this.w_CPFLORDI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.RadioValue(.t.)
    this.w_CPTABNAM = t_CPTABNAM
    this.w_CPFLDNAM = t_CPFLDNAM
    this.w_FLCOMMEN = t_FLCOMMEN
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CPNOMCAM with this.w_CPNOMCAM
    replace t_CPFLROTT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLROTT_2_3.ToRadio()
    replace t_CPFLORDI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPFLORDI_2_4.ToRadio()
    replace t_CPTABNAM with this.w_CPTABNAM
    replace t_CPFLDNAM with this.w_CPFLDNAM
    replace t_FLCOMMEN with this.w_FLCOMMEN
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanDelete()
    local i_res
    i_res=EMPTY(NVL(this.w_PSSERIAL, ' '))
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Configurazione utilizzata da almeno un piano di spedizione. Impossibile eliminarla"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsva_mcpPag1 as StdContainer
  Width  = 543
  height = 528
  stdWidth  = 543
  stdheight = 528
  resizeXpos=292
  resizeYpos=339
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCPSERIAL_1_1 as StdField with uid="OLEWTZRJGL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CPSERIAL", cQueryName = "CPSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 123727246,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=178, Top=9, InputMask=replicate('X',10)

  add object oCPDESCRI_1_2 as StdField with uid="XLAKFTRWTH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CPDESCRI", cQueryName = "CPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 223403409,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=178, Top=33, InputMask=replicate('X',30)

  add object oCPCODCAU_1_3 as StdField with uid="OMNLLCIBIQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPCODCAU", cQueryName = "CPCODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 238480773,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=178, Top=57, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CPCODCAU"

  func oCPCODCAU_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCODCAU_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCODCAU_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCPCODCAU_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSVA1APS.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oCPFLPRED_1_4 as StdCheck with uid="PQJGUPHVDI",rtseq=4,rtrep=.f.,left=265, top=9, caption="Predefinita",;
    HelpContextID = 25576042,;
    cFormVar="w_CPFLPRED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCPFLPRED_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CPFLPRED,&i_cF..t_CPFLPRED),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCPFLPRED_1_4.GetRadio()
    this.Parent.oContained.w_CPFLPRED = this.RadioValue()
    return .t.
  endfunc

  func oCPFLPRED_1_4.ToRadio()
    this.Parent.oContained.w_CPFLPRED=trim(this.Parent.oContained.w_CPFLPRED)
    return(;
      iif(this.Parent.oContained.w_CPFLPRED=='S',1,;
      0))
  endfunc

  func oCPFLPRED_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTDDESDOC_1_7 as StdField with uid="DAMLEZNYZV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 206628999,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=248, Top=57, InputMask=replicate('X',35)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=20, top=89, width=520,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="CPNOMCAM",Label2="Nome campo rottura/ordinamento",Field3="CPFLROTT",Label3="Rottura",Field4="CPFLORDI",Label4="Ordinamento",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49498246


  add object oBtn_1_10 as StdButton with uid="GRRFZXKFAO",left=489, top=4, width=48,height=45,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il caricamento rapido dei campi";
    , HelpContextID = 26055130;
    , TabStop=.f., Caption='\<Car. rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      do GSVA_KCP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="VWMOCOQCHB",Visible=.t., Left=11, Top=11,;
    Alignment=1, Width=165, Height=18,;
    Caption="Codice configurazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="WNJSLEIQTY",Visible=.t., Left=11, Top=59,;
    Alignment=1, Width=165, Height=18,;
    Caption="Codice causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ZEIEPHCSWW",Visible=.t., Left=11, Top=35,;
    Alignment=1, Width=165, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=108,;
    width=516+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=109,width=515+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*20*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oFLCOMMEN_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oFLCOMMEN_2_7 as StdTrsField with uid="PIWEOSBHQE",rtseq=12,rtrep=.t.,;
    cFormVar="w_FLCOMMEN",value=space(80),enabled=.f.,;
    HelpContextID = 207163044,;
    cTotal="", bFixedPos=.t., cQueryName = "FLCOMMEN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=381, Left=156, Top=502, InputMask=replicate('X',80)

  add object oStr_2_8 as StdString with uid="HNQFRHYYIM",Visible=.t., Left=10, Top=504,;
    Alignment=1, Width=144, Height=18,;
    Caption="Descrizione campo:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsva_mcpBodyRow as CPBodyRowCnt
  Width=506
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="WNZNMZCXLM",rtseq=5,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17169814,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCPNOMCAM_2_2 as StdTrsField with uid="ZQVZJYZLYW",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPNOMCAM",value=space(46),;
    HelpContextID = 228998541,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=287, Left=51, Top=0, InputMask=replicate('X',46), bHasZoom = .t. 

  proc oCPNOMCAM_2_2.mZoom
      with this.Parent.oContained
        GSVA_BCP(this.Parent.oContained,"ZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCPFLROTT_2_3 as StdTrsCombo with uid="ZCTIHSRJMN",rtrep=.t.,;
    cFormVar="w_CPFLROTT", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 22658438,;
    Height=22, Width=54, Left=342, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCPFLROTT_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CPFLROTT,&i_cF..t_CPFLROTT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    'N')))
  endfunc
  func oCPFLROTT_2_3.GetRadio()
    this.Parent.oContained.w_CPFLROTT = this.RadioValue()
    return .t.
  endfunc

  func oCPFLROTT_2_3.ToRadio()
    this.Parent.oContained.w_CPFLROTT=trim(this.Parent.oContained.w_CPFLROTT)
    return(;
      iif(this.Parent.oContained.w_CPFLROTT=='S',1,;
      iif(this.Parent.oContained.w_CPFLROTT=='N',2,;
      0)))
  endfunc

  func oCPFLROTT_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCPFLORDI_2_4 as StdTrsCombo with uid="OKJSAZVFXX",rtrep=.t.,;
    cFormVar="w_CPFLORDI", RowSource=""+"Ascendente,"+"Discendente,"+"No" , ;
    HelpContextID = 24527471,;
    Height=22, Width=101, Left=400, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCPFLORDI_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CPFLORDI,&i_cF..t_CPFLORDI),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'D',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oCPFLORDI_2_4.GetRadio()
    this.Parent.oContained.w_CPFLORDI = this.RadioValue()
    return .t.
  endfunc

  func oCPFLORDI_2_4.ToRadio()
    this.Parent.oContained.w_CPFLORDI=trim(this.Parent.oContained.w_CPFLORDI)
    return(;
      iif(this.Parent.oContained.w_CPFLORDI=='A',1,;
      iif(this.Parent.oContained.w_CPFLORDI=='D',2,;
      iif(this.Parent.oContained.w_CPFLORDI=='N',3,;
      0))))
  endfunc

  func oCPFLORDI_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=19
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_mcp','VACONPIA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CPSERIAL=VACONPIA.CPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
