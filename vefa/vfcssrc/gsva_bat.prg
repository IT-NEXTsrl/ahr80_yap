* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bat                                                        *
*              AGGIORNA\CREA TABELLE CLONE                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-07                                                      *
* Last revis.: 2006-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bat",oParentObject,m.pEXEC)
return(i_retval)

define class tgsva_bat as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODTAB = space(30)
  w_IDXTABLE = 0
  w_CONN = 0
  w_DCX = .NULL.
  w_PHNAME = space(30)
  w_IDXTABLE = 0
  w_NUMCAM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- A: aggiorna tabelle selezionate
    *     I: marca tabelle da aggiornare
    cp_ReadXDC()
    this.w_DCX = i_DCX
    do case
      case this.pEXEC="A"
         
 Public MSGWND 
 MSGWND=createobject("mkdbmsg") 
 MSGWND.show() 
 MSGWND.btnok.enabled=.T.
         
 Select (this.oParentObject.w_ZOOMENT.cCursor) 
 Scan for XCHK=1
        this.w_CODTAB = EN_TABLE
        this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CODTAB) ,.T.)
        if this.w_IDXTABLE>0
          this.w_CONN = i_TableProp[ cp_OpenTable( this.w_CODTAB ,.T.) , 3 ] 
        else
          this.w_CONN = i_serverconn[1,2]
        endif
        cDatabaseType=SQLXGetDatabaseType(this.w_CONN)
        if !MakeTableClone(this.w_CODTAB,this.w_Conn,this.w_DCX,cDatabaseType,trim(i_codazi))
           
 replace stato with "Errore nell'aggiornamento"
        else
          if !MakeindexClone(this.w_CODTAB,this.w_CONN,this.w_DCX,cDatabaseType,trim(i_codazi))
             
 replace stato with "Errore nell'aggiornamento"
          else
             
 Select (this.oParentObject.w_ZOOMENT.cCursor) 
 replace stato with "OK" 
 replace xchk with 0
          endif
        endif
        Select (this.oParentObject.w_ZOOMENT.cCursor) 
 Endscan
      case this.pEXEC="I"
         
 Select (this.oParentObject.w_ZOOMENT.cCursor) 
 Scan
        this.w_CODTAB = EN_TABLE
        this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_CODTAB) ,.T.)
        if this.w_IDXTABLE>0
          this.w_CONN = i_TableProp[ cp_OpenTable( this.w_CODTAB ,.T.) , 3 ] 
        else
          this.w_CONN = i_serverconn[1,2]
        endif
        this.w_PHNAME = cp_SetAzi(i_TableProp[ this.w_IDXTABLE ,2]) 
        CURS=READTABLE(this.w_PHNAME,"*",,,,"1=0") 
 Select ((CURS))
        this.w_NUMCAM = FCOUNT()
        if Used((CURS))
           
 Select (CURS) 
 use
        endif
        this.w_PHNAME = getclonename(this.w_CODTAB)
        if Not cp_ExistTable( this.w_PHNAME , this.w_CONN ,"","","")
           
 Select (this.oParentObject.w_ZOOMENT.cCursor) 
 replace stato with "Da aggiornare" 
 replace xchk with 1
        else
          CURS=READTABLE(this.w_PHNAME,"*",,,,"1=0") 
 Select ((CURS))
          if Fcount()<>this.w_NUMCAM
             
 Select (this.oParentObject.w_ZOOMENT.cCursor) 
 replace stato with "Da aggiornare" 
 replace xchk with 1
          endif
        endif
        if Used((CURS))
           
 Select (CURS) 
 use
        endif
        Select (this.oParentObject.w_ZOOMENT.cCursor) 
 Endscan
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
