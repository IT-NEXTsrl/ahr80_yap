* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_btr                                                        *
*              GESTIONE TRASCODIFICHE EDI                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-18                                                      *
* Last revis.: 2006-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pCODENT,pCODELE,pCODRAG,pTIPCON,pCODCON,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_btr",oParentObject,m.pCODSTR,m.pCODENT,m.pCODELE,m.pCODRAG,m.pTIPCON,m.pCODCON,m.pAZIONE)
return(i_retval)

define class tgsva_btr as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pCODENT = space(15)
  pCODELE = space(20)
  pCODRAG = space(10)
  pTIPCON = space(1)
  pCODCON = space(15)
  pAZIONE = space(1)
  w_PROG = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre gestione trascodifiche EDI
    * --- Codice struttura
    * --- Codice Entit�
    * --- Codice elemento
    * --- Codice raggruppamento
    * --- Tipo intestatario
    * --- Codice intestatario
    * --- Azione da svolgere
    *     O: origine
    *     C: carica
    this.w_PROG = GSVA_ATR()
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    do case
      case this.pAZIONE="O"
        * --- Apre trascodifica relativa
        this.w_PROG.EcpFIlter()     
        this.w_PROG.w_TRCODSTR = this.pCODSTR
        this.w_OBJ = this.w_PROG.GetCtrl("w_TRCODSTR")
        this.w_OBJ.Check()     
        this.w_PROG.w_TRCODENT = this.pCODENT
        this.w_PROG.w_TRCODELE = this.pCODELE
        this.w_PROG.w_TRCODRAG = this.pCODRAG
        this.w_PROG.w_TRTIPCON = this.pTIPCON
        this.w_PROG.w_TRCODCON = this.pCODCON
        this.w_PROG.EcpSave()     
      case this.pAZIONE="C"
        this.w_PROG.EcpLoad()     
        this.w_PROG.w_TRCODSTR = this.pCODSTR
        this.w_OBJ = this.w_PROG.GetCtrl("w_TRCODSTR")
        this.w_OBJ.Check()     
        this.w_PROG.w_TRCODENT = this.pCODENT
        this.w_PROG.w_TRCODELE = this.pCODELE
        this.w_OBJ = this.w_PROG.GetcTRL("w_TRCODELE")
        this.w_OBJ.Check()     
        this.w_PROG.mCalc(.T.)     
        this.w_PROG.SetControlsValue()     
    endcase
  endproc


  proc Init(oParentObject,pCODSTR,pCODENT,pCODELE,pCODRAG,pTIPCON,pCODCON,pAZIONE)
    this.pCODSTR=pCODSTR
    this.pCODENT=pCODENT
    this.pCODELE=pCODELE
    this.pCODRAG=pCODRAG
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pCODENT,pCODELE,pCODRAG,pTIPCON,pCODCON,pAZIONE"
endproc
