* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bsi                                                        *
*              Elimina articoli non imballi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-19                                                      *
* Last revis.: 2006-07-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bsi",oParentObject)
return(i_retval)

define class tgsva_bsi as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lanciata alla Load Record End del file GSAR_MIR per eliminare le righe
    *     che non riguardano articoli imballi (Articoli kit e prodotti finiti)
    this.w_OK = .T.
    this.w_PADRE = this.oParentObject
    this.w_PADRE.FirstRow()     
    * --- Ciclo sul cursore per eliminare le righe di articoli che non sono imballi
    do while Not this.w_PADRE.Eof_Trs()
      this.w_PADRE.SetRow()     
      if this.w_PADRE.FullRow() And this.oParentObject.w_KITIMB<>"R"
        * --- Se non sono righe di imballi a rendere le elimino perch� non devo visualizzarle
        *     Potrebbero essere distinte o articoli kit
        this.w_PADRE.DeleteRow()     
      else
        if this.oParentObject.w_KITIMB="R" And this.w_OK
          * --- Alla prima riga imballo che trovo memorizzo la posizione per poi riposizionarmi alla fine
          this.w_PADRE.MarkPos()     
          this.w_OK = .F.
        endif
      endif
      if this.w_PADRE.Eof_Trs()
        * --- Se arrivo in fondo al cursore esco altrimenti la NextRow da errore
        exit
      endif
      this.w_PADRE.NextRow()     
    enddo
    if this.w_OK
      * --- Se ho svuotato tutto il cursore aggiungo una riga e mi ci posiziono per non dare errore
      this.w_PADRE.AddRow()     
      this.w_PADRE.MarkPos()     
    endif
    this.w_PADRE.RePos()     
    if this.w_PADRE.NumRow()=0
      this.w_PADRE.BlankRec()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
