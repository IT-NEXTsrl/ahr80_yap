* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsvalkim                                                        *
*              Dettaglio lotti/ubicazioni                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_63]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-07                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsvalkim",oParentObject))

* --- Class definition
define class tgsvalkim as StdForm
  Top    = 3
  Left   = 4

  * --- Standard Properties
  Width  = 740
  Height = 569
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-19"
  HelpContextID=2264471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  FLATDETT_IDX = 0
  cPrg = "gsvalkim"
  cComment = "Dettaglio lotti/ubicazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TBNAME = space(15)
  w_ERRORLOG = space(0)
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_DECIMAL = 0
  w_TIPCAM = space(1)
  w_LUNG = 0
  w_FLDEFA = space(10)
  o_FLDEFA = space(10)
  w_FLNULL = space(10)
  o_FLNULL = space(10)
  w_DATA = ctot('')
  w_NUMERIC = 0
  w_CARACTER = space(254)
  w_ABNUMRIF = 0
  w_FLATAB = space(30)
  w_SER_ELAB = space(10)
  w_CAMPO = space(10)
  o_CAMPO = space(10)
  w_ZOOMDOC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsvalkimPag1","gsvalkim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLDEFA_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMDOC = this.oPgFrm.Pages(1).oPag.ZOOMDOC
    DoDefault()
    proc Destroy()
      this.w_ZOOMDOC = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='FLATDETT'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TBNAME=space(15)
      .w_ERRORLOG=space(0)
      .w_SERIAL=space(10)
      .w_CPROWNUM=0
      .w_DECIMAL=0
      .w_TIPCAM=space(1)
      .w_LUNG=0
      .w_FLDEFA=space(10)
      .w_FLNULL=space(10)
      .w_DATA=ctot("")
      .w_NUMERIC=0
      .w_CARACTER=space(254)
      .w_ABNUMRIF=0
      .w_FLATAB=space(30)
      .w_SER_ELAB=space(10)
      .w_CAMPO=space(10)
      .w_SERIAL=oParentObject.w_SERIAL
      .w_CPROWNUM=oParentObject.w_CPROWNUM
      .w_FLATAB=oParentObject.w_FLATAB
      .w_SER_ELAB=oParentObject.w_SER_ELAB
      .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
        .w_TBNAME = 'MOVILOTT'
        .w_ERRORLOG = .w_ZOOMDOC.GETVAR('ERRORMSG')
          .DoRTCalc(3,7,.f.)
        .w_FLDEFA = 'N'
        .w_FLNULL = 'N'
          .DoRTCalc(10,11,.f.)
        .w_CARACTER = ' '
        .w_ABNUMRIF = -30
        .DoRTCalc(14,16,.f.)
        if not(empty(.w_CAMPO))
          .link_1_25('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsvalkim
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_CAMPO")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.w_CPROWNUM=.w_CPROWNUM
      .oParentObject.w_FLATAB=.w_FLATAB
      .oParentObject.w_SER_ELAB=.w_SER_ELAB
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
            .w_TBNAME = 'MOVILOTT'
            .w_ERRORLOG = .w_ZOOMDOC.GETVAR('ERRORMSG')
        .DoRTCalc(3,7,.t.)
        if .o_CAMPO<>.w_CAMPO
            .w_FLDEFA = 'N'
        endif
        .DoRTCalc(9,11,.t.)
        if .o_FLNULL<>.w_FLNULL.or. .o_CAMPO<>.w_CAMPO
            .w_CARACTER = ' '
        endif
        if .o_FLDEFA<>.w_FLDEFA
          .Calculate_XAPTFYRWGW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMDOC.Calculate()
    endwith
  return

  proc Calculate_JLXVINJECR()
    with this
          * --- Colora zoom
          GSVA_BZC(this;
              ,.w_ZOOMDOC;
              ,'IIF(Not Empty(NVL(TIPREC,"")), RGB(255,0,0), RGB(0,0,0))';
              ,'COLOR';
             )
    endwith
  endproc
  proc Calculate_XAPTFYRWGW()
    with this
          * --- Gsva_bim(v) - w_fldefa changed
          gsva_bim(this;
              ,'';
              ,'';
              ,'V';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCARACTER_1_15.enabled = this.oPgFrm.Page1.oPag.oCARACTER_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLDEFA_1_10.visible=!this.oPgFrm.Page1.oPag.oFLDEFA_1_10.mHide()
    this.oPgFrm.Page1.oPag.oFLNULL_1_11.visible=!this.oPgFrm.Page1.oPag.oFLNULL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDATA_1_12.visible=!this.oPgFrm.Page1.oPag.oDATA_1_12.mHide()
    this.oPgFrm.Page1.oPag.oNUMERIC_1_14.visible=!this.oPgFrm.Page1.oPag.oNUMERIC_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCARACTER_1_15.visible=!this.oPgFrm.Page1.oPag.oCARACTER_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsvalkim
      IF cevent='Done'
       This.oparentobject.Notifyevent('Esegui')
      endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMDOC.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_JLXVINJECR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAMPO
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLATDETT_IDX,3]
    i_lTable = "FLATDETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2], .t., this.FLATDETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('no Struttura Tabelle',True,'FLATDETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FTFLDNAM like "+cp_ToStrODBC(trim(this.w_CAMPO)+"%");
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);

          i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FTCODICE,FTTABNAM,FTFLDNAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FTCODICE',this.w_FLATAB;
                     ,'FTTABNAM',this.w_TBNAME;
                     ,'FTFLDNAM',trim(this.w_CAMPO))
          select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FTCODICE,FTTABNAM,FTFLDNAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMPO)==trim(_Link_.FTFLDNAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('FLATDETT','*','FTCODICE,FTTABNAM,FTFLDNAM',cp_AbsName(oSource.parent,'oCAMPO_1_25'),i_cWhere,'no Struttura Tabelle',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLATAB<>oSource.xKey(1);
           .or. this.w_TBNAME<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                     +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(oSource.xKey(3));
                     +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                     +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',oSource.xKey(1);
                       ,'FTTABNAM',oSource.xKey(2);
                       ,'FTFLDNAM',oSource.xKey(3))
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC";
                   +" from "+i_cTable+" "+i_lTable+" where FTFLDNAM="+cp_ToStrODBC(this.w_CAMPO);
                   +" and FTCODICE="+cp_ToStrODBC(this.w_FLATAB);
                   +" and FTTABNAM="+cp_ToStrODBC(this.w_TBNAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',this.w_FLATAB;
                       ,'FTTABNAM',this.w_TBNAME;
                       ,'FTFLDNAM',this.w_CAMPO)
            select FTCODICE,FTTABNAM,FTFLDNAM,FTFLDTYP,FTFLDDIM,FTFLDDEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMPO = NVL(_Link_.FTFLDNAM,space(10))
      this.w_TIPCAM = NVL(_Link_.FTFLDTYP,space(1))
      this.w_LUNG = NVL(_Link_.FTFLDDIM,0)
      this.w_DECIMAL = NVL(_Link_.FTFLDDEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CAMPO = space(10)
      endif
      this.w_TIPCAM = space(1)
      this.w_LUNG = 0
      this.w_DECIMAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLATDETT_IDX,2])+'\'+cp_ToStr(_Link_.FTCODICE,1)+'\'+cp_ToStr(_Link_.FTTABNAM,1)+'\'+cp_ToStr(_Link_.FTFLDNAM,1)
      cp_ShowWarn(i_cKey,this.FLATDETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oERRORLOG_1_3.value==this.w_ERRORLOG)
      this.oPgFrm.Page1.oPag.oERRORLOG_1_3.value=this.w_ERRORLOG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFA_1_10.RadioValue()==this.w_FLDEFA)
      this.oPgFrm.Page1.oPag.oFLDEFA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNULL_1_11.RadioValue()==this.w_FLNULL)
      this.oPgFrm.Page1.oPag.oFLNULL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_1_12.value==this.w_DATA)
      this.oPgFrm.Page1.oPag.oDATA_1_12.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERIC_1_14.value==this.w_NUMERIC)
      this.oPgFrm.Page1.oPag.oNUMERIC_1_14.value=this.w_NUMERIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCARACTER_1_15.value==this.w_CARACTER)
      this.oPgFrm.Page1.oPag.oCARACTER_1_15.value=this.w_CARACTER
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMPO_1_25.RadioValue()==this.w_CAMPO)
      this.oPgFrm.Page1.oPag.oCAMPO_1_25.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_NUMERIC<=VAL(REPL('9',.w_LUNG-.w_DECIMAL)))  and not(.w_TIPCAM<>'N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERIC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, valore eccedente la lunghezza del campo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLDEFA = this.w_FLDEFA
    this.o_FLNULL = this.w_FLNULL
    this.o_CAMPO = this.w_CAMPO
    return

enddefine

* --- Define pages as container
define class tgsvalkimPag1 as StdContainer
  Width  = 736
  height = 569
  stdWidth  = 736
  stdheight = 569
  resizeXpos=217
  resizeYpos=310
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMDOC as cp_szoombox with uid="IDMLORUCJH",left=9, top=10, width=717,height=387,;
    caption='',;
   bGlobalFont=.t.,;
    cMenuFile="",cZoomOnZoom="",cTable="MOVILOTT",cZoomFile="GSVALKIM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,bRetriveAllRows=.f.,ndesflags=3,;
    cEvent = "Init,Esegui",;
    nPag=1;
    , HelpContextID = 2264566

  add object oERRORLOG_1_3 as StdMemo with uid="PYOWYDGYBL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ERRORLOG", cQueryName = "ERRORLOG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 241761139,;
   bGlobalFont=.t.,;
    Height=65, Width=717, Left=10, Top=448

  add object oFLDEFA_1_10 as StdCheck with uid="CLGLVNRZYS",rtseq=8,rtrep=.f.,left=239, top=529, caption="Default",;
    ToolTipText = "Se attivo imposta il valore attuale del campo",;
    HelpContextID = 171172266,;
    cFormVar="w_FLDEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDEFA_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDEFA_1_10.GetRadio()
    this.Parent.oContained.w_FLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFA_1_10.SetRadio()
    this.Parent.oContained.w_FLDEFA=trim(this.Parent.oContained.w_FLDEFA)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFA=='S',1,;
      0)
  endfunc

  func oFLDEFA_1_10.mHide()
    with this.Parent.oContained
      return (Empty(.w_CAMPO))
    endwith
  endfunc

  add object oFLNULL_1_11 as StdCheck with uid="LHPRISBZJM",rtseq=9,rtrep=.f.,left=386, top=529, caption="Null",;
    HelpContextID = 247677354,;
    cFormVar="w_FLNULL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNULL_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLNULL_1_11.GetRadio()
    this.Parent.oContained.w_FLNULL = this.RadioValue()
    return .t.
  endfunc

  func oFLNULL_1_11.SetRadio()
    this.Parent.oContained.w_FLNULL=trim(this.Parent.oContained.w_FLNULL)
    this.value = ;
      iif(this.Parent.oContained.w_FLNULL=='S',1,;
      0)
  endfunc

  func oFLNULL_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'C')
    endwith
  endfunc

  add object oDATA_1_12 as StdField with uid="SFGSYAZGLB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 6886198,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=460, Top=528, bHasZoom = .t. 

  func oDATA_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'D')
    endwith
  endfunc

  proc oDATA_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATA_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_13 as StdButton with uid="GTULXLRYOW",left=333, top=518, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 108893849;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSVA_BAG(this.Parent.oContained,.w_ZOOMMAT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERIC_1_14 as StdField with uid="BBNTSSUTNJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMERIC", cQueryName = "NUMERIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, valore eccedente la lunghezza del campo",;
    HelpContextID = 24332330,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=460, Top=528, cSayPict="'99999999999999.99999'", cGetPict="'99999999999999.99999'"

  func oNUMERIC_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'N')
    endwith
  endfunc

  func oNUMERIC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMERIC<=VAL(REPL('9',.w_LUNG-.w_DECIMAL)))
    endwith
    return bRes
  endfunc

  add object oCARACTER_1_15 as StdField with uid="JQEHMECGLW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CARACTER", cQueryName = "CARACTER",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 124193928,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=460, Top=528, InputMask=replicate('X',254)

  func oCARACTER_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNULL='N')
    endwith
   endif
  endfunc

  func oCARACTER_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPCAM<>'C')
    endwith
  endfunc


  add object oBtn_1_16 as StdButton with uid="LUHNMBSWMQ",left=9, top=401, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le matricole";
    , HelpContextID = 103725094;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSVA_BZC(this.Parent.oContained,.w_ZOOMDOC, " " , "SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="ABJFIATDMP",left=60, top=401, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le matricole";
    , HelpContextID = 103725094;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSVA_BZC(this.Parent.oContained,.w_ZOOMDOC, " " , "DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="PDWIPENRCZ",left=111, top=401, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione di tutte le matricole";
    , HelpContextID = 103725094;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSVA_BZC(this.Parent.oContained,.w_ZOOMDOC, " " , "INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="RMTOYGGCOK",left=679, top=518, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 211886074;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="RJHHJLQWBT",left=679, top=401, width=48,height=45,;
    CpPicture="BMP\Abbina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla gestione delle unit� logistiche";
    , HelpContextID = 24800442;
    , caption='\<Unit� log.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      do GSVAAKIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (ChketStatus(alltrim(.w_FLATAB), 'ABBLOTUL')=0)
      endwith
    endif
  endfunc


  add object oCAMPO_1_25 as StdZTamTableCombo with uid="SJFZRVIEMH",rtseq=16,rtrep=.f.,left=10,top=529,width=223,height=21;
    , ToolTipText = "Campo da aggiornare";
    , HelpContextID = 90678054;
    , cFormVar="w_CAMPO",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="FLATDETT";
    , cTable='GSVA_QCO.VQR',cKey='FLNAME',cValue='FLCOMMEN',cOrderBy='FLCOMMEN',xDefault=space(10);
  , bGlobalFont=.t.


  func oCAMPO_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMPO_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsvalkim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsvalkim
* --- Classe per gestire la combo PICONDCON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif
  endproc


enddefine
* --- Fine Area Manuale
