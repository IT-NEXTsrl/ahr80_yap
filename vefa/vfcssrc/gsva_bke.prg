* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bke                                                        *
*              Trascodifica codici di ricerca                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-19                                                      *
* Last revis.: 2012-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV,pTIPBAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bke",oParentObject,m.pPROV,m.pTIPBAR)
return(i_retval)

define class tgsva_bke as StdBatch
  * --- Local variables
  pPROV = space(1)
  pTIPBAR = space(1)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_CODICE = space(20)
  w_CODART = space(20)
  w_VALORE = space(20)
  w_CODTAB = space(20)
  w_CAMPO = space(20)
  w_ANFLCODI = space(1)
  w_ANCODSEC = space(5)
  w_CALENSCF = 0
  w_DTOBSO = ctod("  /  /  ")
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_CODCOL = space(5)
  w_MVTIPDOC = space(5)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  VAELEMEN_idx=0
  CONTI_idx=0
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine di Trascodifica Codici di ricerca di tipo Cli\For
    * --- Provenienza codice di ricerca
    * --- Tipo barcode cercato
    if VarType(this.pPROV)<>"C"
      this.pPROV = "C"
    endif
    if VarType(this.pTIPBAR)<>"C"
      this.pTIPBAR = "0"
    endif
    this.w_VALORE = " "
    if USED("DOC_DETT")
      this.w_SERIAL = DOC_DETT.MVSERIAL
      this.w_ROWNUM = DOC_DETT.CPROWNUM
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODCON,MVTIPCON"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODCON,MVTIPCON;
          from (i_cTable) where;
              MVSERIAL = this.w_SERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Isahe()
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVCODICE"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVCODICE;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODICE = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVCODICE,MVCODCOL"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVCODICE,MVCODCOL;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODICE = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
          this.w_CODCOL = NVL(cp_ToDate(_read_.MVCODCOL),cp_NullValue(_read_.MVCODCOL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      if type("oparentobject")="O"
        this.w_SERIAL = oparentobject.w_MVSERIAL
        this.w_CODCON = oparentobject.w_CODCON
        this.w_ROWNUM = oparentobject.w_RIGHE
        this.w_TIPCON = oparentobject.w_TIPCON
        this.w_MVTIPDOC = oparentobject.Oparentobject.w_TIPDOC
      else
        this.w_CODICE = m.w_VALORE
      endif
      if Type("g_MVCODCON")="C"
        this.w_CODCON = g_MVCODCON
      endif
      if empty(Nvl(this.w_TIPCON," "))
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLINTE"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLINTE;
            from (i_cTable) where;
                TDTIPDOC = this.w_MVTIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCON = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART;
        from (i_cTable) where;
            CACODICE = this.w_CODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pPROV $ "C-F"
        if this.w_TIPCON $ "C-F"
          * --- Se esiste intestatario
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANFLCODI"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANFLCODI;
              from (i_cTable) where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANFLCODI = NVL(cp_ToDate(_read_.ANFLCODI),cp_NullValue(_read_.ANFLCODI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_ANFLCODI="S"
            * --- Select from KEY_ARTI
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CADTOBSO,CACODCON,CALENSCF,CACODICE,CATIPCON,CACODART  from "+i_cTable+" KEY_ARTI ";
                  +" where CACODART="+cp_ToStrODBC(this.w_CODART)+" AND CACODCON="+cp_ToStrODBC(this.w_CODCON)+" AND CATIPCON="+cp_ToStrODBC(this.pPROV)+" ";
                   ,"_Curs_KEY_ARTI")
            else
              select CADTOBSO,CACODCON,CALENSCF,CACODICE,CATIPCON,CACODART from (i_cTable);
               where CACODART=this.w_CODART AND CACODCON=this.w_CODCON AND CATIPCON=this.pPROV ;
                into cursor _Curs_KEY_ARTI
            endif
            if used('_Curs_KEY_ARTI')
              select _Curs_KEY_ARTI
              locate for 1=1
              do while not(eof())
              this.w_DTOBSO = CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)
              this.w_CALENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
              if Empty(this.w_DTOBSO) or i_DATSYS<=this.w_DTOBSO
                this.w_VALORE = _Curs_KEY_ARTI.CACODICE
                if this.w_CALENSCF>0
                  this.w_VALORE = SUBSTR(this.w_VALORE,1,LEN(ALLTRIM(this.w_VALORE))-this.w_CALENSCF)
                endif
                Exit
              endif
                select _Curs_KEY_ARTI
                continue
              enddo
              use
            endif
          endif
        endif
      case this.pPROV = "R"
        * --- Select from KEY_ARTI
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CADTOBSO,CALENSCF,CACODICE,CATIPBAR,CACODART  from "+i_cTable+" KEY_ARTI ";
              +" where CACODART="+cp_ToStrODBC(this.w_CODART)+" AND CATIPBAR="+cp_ToStrODBC(this.pTIPBAR)+" AND CATIPCON="+cp_ToStrODBC(this.pPROV)+" ";
               ,"_Curs_KEY_ARTI")
        else
          select CADTOBSO,CALENSCF,CACODICE,CATIPBAR,CACODART from (i_cTable);
           where CACODART=this.w_CODART AND CATIPBAR=this.pTIPBAR AND CATIPCON=this.pPROV ;
            into cursor _Curs_KEY_ARTI
        endif
        if used('_Curs_KEY_ARTI')
          select _Curs_KEY_ARTI
          locate for 1=1
          do while not(eof())
          this.w_DTOBSO = CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)
          this.w_CALENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
          if Empty(this.w_DTOBSO) or i_DATSYS<=this.w_DTOBSO
            this.w_VALORE = _Curs_KEY_ARTI.CACODICE
            if this.w_CALENSCF>0
              this.w_VALORE = SUBSTR(this.w_VALORE,1,LEN(ALLTRIM(this.w_VALORE))-this.w_CALENSCF)
            endif
            Exit
          endif
            select _Curs_KEY_ARTI
            continue
          enddo
          use
        endif
      case this.pPROV = "A"
        * --- restituisco codice di ricerca di tipo interno uguale al codice articolo
        this.w_VALORE = " "
        * --- Select from KEY_ARTI
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CACODCON,CATIPCON,CACODICE,CACODART  from "+i_cTable+" KEY_ARTI ";
              +" where CACODCON="+cp_ToStrODBC(this.w_CODCON)+" and CATIPCON="+cp_ToStrODBC(this.pTIPBAR)+"";
               ,"_Curs_KEY_ARTI")
        else
          select CACODCON,CATIPCON,CACODICE,CACODART from (i_cTable);
           where CACODCON=this.w_CODCON and CATIPCON=this.pTIPBAR;
            into cursor _Curs_KEY_ARTI
        endif
        if used('_Curs_KEY_ARTI')
          select _Curs_KEY_ARTI
          locate for 1=1
          do while not(eof())
          if this.w_CODICE $ Nvl(_Curs_KEY_ARTI.CACODICE," ")
            this.w_VALORE = Nvl(_Curs_KEY_ARTI.CACODART," ")
            Exit
          endif
            select _Curs_KEY_ARTI
            continue
          enddo
          use
        endif
        if Not Empty(this.w_CODART) and Empty(this.w_VALORE)
          this.w_VALORE = this.w_CODART
        endif
      case this.pPROV = "I"
        * --- Cerco codice di ricerca di tipo imballo
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODART = "+cp_ToStrODBC(this.w_CODART);
                +" and CAFLIMBA = "+cp_ToStrODBC("S");
                +" and CATIPCO3 = "+cp_ToStrODBC(this.w_CODCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CACODART = this.w_CODART;
                and CAFLIMBA = "S";
                and CATIPCO3 = this.w_CODCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALORE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
          if i_Rows=0
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CACODICE"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODART = "+cp_ToStrODBC(this.w_CODART);
                    +" and CAFLIMBA = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CACODICE;
                from (i_cTable) where;
                    CACODART = this.w_CODART;
                    and CAFLIMBA = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_VALORE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      otherwise
        this.w_VALORE = " "
    endcase
    i_retcode = 'stop'
    i_retval = this.w_VALORE
    return
  endproc


  proc Init(oParentObject,pPROV,pTIPBAR)
    this.pPROV=pPROV
    this.pTIPBAR=pTIPBAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='VAELEMEN'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='DOC_DETT'
    this.cWorkTables[6]='TIP_DOCU'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV,pTIPBAR"
endproc
