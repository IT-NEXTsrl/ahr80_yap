* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_brp                                                        *
*              Controllo righe piene schede di calcolo                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-30                                                      *
* Last revis.: 2004-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_brp",oParentObject,m.pPAR)
return(i_retval)

define class tgsva_brp as StdBatch
  * --- Local variables
  pPAR = space(1)
  w_MSGERR = space(100)
  w_COUNTER = 0
  w_RECPOS = 0
  w_AbsRow = 0
  w_nRelRow = 0
  w_PADRE = .NULL.
  w_RECO = 0
  w_LOBSART = ctod("  /  /  ")
  w_LCODART = space(20)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo righe piene in gsva_msl
    * --- codice articolo su dettaglio dela scheda
    this.w_PADRE = this.OparentObject
    * --- ===========================
    this.oParentObject.w_NORIGHE = .F.
    do case
      case this.pPAR $ "ND"
        if not empty(this.oParentObject.w_CLCODART)
          this.w_COUNTER = 0
          do case
            case this.pPAR="N"
              this.w_PADRE.MarkPos()     
              position=recno()
              * --- CONTROLLA SE UN ARTICOLO E' STATO GIA' INSERITO.
              * --- conto le righe del cursore del dettaglio (diverse da quella attualmente modificata)
              * --- aventi articolo uguale a quello appena impostato.
              this.w_COUNTER = 0
              Count for (t_CLCODART==this.oParentObject.w_CLCODART and recno()<>position) And Not Deleted() To this.w_COUNTER
              * --- Mi rimetto nella riga di partenza
              this.w_PADRE.RePos()     
              if this.w_COUNTER>0
                * --- deve esistere almeno un'altra riga con quell'articolo
                this.w_MSGERR = "Articolo gi� presente nella scheda"
                ah_ErrorMsg(this.w_MSGERR,"!")
                this.oParentObject.w_CLCODART = space(20)
                this.oParentObject.w_DESART = space(40)
                this.w_PADRE.SaveRow()     
                this.w_PADRE.Set("w_CLCODART", SPACE(20))     
                this.w_PADRE.Set("w_DESART", SPACE(40))     
              endif
          endcase
          do case
            case this.pPAR="D"
              * --- CONTROLLA SE UN ARTICOLO E' OBSOLETO
              this.w_PADRE.MarkPos()     
              this.w_PADRE.FirstRow()     
              do while Not this.w_PADRE.Eof_Trs()
                if not empty(this.oParentObject.w_CLCODART)
                  this.w_LCODART = this.w_PADRE.GET("t_CLCODART")
                  this.w_LOBSART = this.w_PADRE.GET("t_OBSART")
                  = CHKDTOBS(this.w_LOBSART,this.oParentObject.w_CLVALFIN,"<%1> articolo obsoleto",.T. , ALLTRIM(this.w_LCODART) )
                endif
                this.w_PADRE.NextRow()     
              enddo
              this.w_PADRE.RePos()     
          endcase
        endif
      case this.pPAR="R"
         
 Select(this.w_Padre.cTrsName) 
 Count for Not Deleted() And (Not Empty(t_CLGRUMER) Or Not Empty(t_CLCODART)) To this.w_RECO
        if this.w_RECO=0
          this.oParentObject.w_NORIGHE = .T.
        endif
    endcase
  endproc


  proc Init(oParentObject,pPAR)
    this.pPAR=pPAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPAR"
endproc
