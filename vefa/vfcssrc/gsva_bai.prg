* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bai                                                        *
*              GESTIONE ANAGRAFICA IMPORTAZIONI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-25                                                      *
* Last revis.: 2007-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bai",oParentObject,m.pOPER)
return(i_retval)

define class tgsva_bai as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_PADRE = .NULL.
  * --- WorkFile variables
  VAANAIMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="ENPAG"
        this.w_PADRE.oPGFRM.Page2.Enabled = this.oParentObject.w_IMSSTEPA<>"N" AND !EMPTY(NVL(this.oParentObject.w_IMSSTEPA, " "))
        this.w_PADRE.oPGFRM.Page3.Enabled = this.oParentObject.w_IMSSTEPB<>"N" AND !EMPTY(NVL(this.oParentObject.w_IMSSTEPB, " "))
        this.w_PADRE.oPGFRM.Page4.Enabled = this.oParentObject.w_IMSSTEPC<>"N" AND !EMPTY(NVL(this.oParentObject.w_IMSSTEPC, " "))
      case this.pOPER=="NLOCK"
        if this.oParentObject.w_IMFLREEZ="S" AND !EMPTY(NVL(this.oParentObject.w_IMSERIAL," "))
          * --- Write into VAANAIMP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VAANAIMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMFLREEZ ="+cp_NullLink(cp_ToStrODBC("N"),'VAANAIMP','IMFLREEZ');
                +i_ccchkf ;
            +" where ";
                +"IMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IMSERIAL);
                   )
          else
            update (i_cTable) set;
                IMFLREEZ = "N";
                &i_ccchkf. ;
             where;
                IMSERIAL = this.oParentObject.w_IMSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.oParentObject.w_IMFLREEZ = "N"
          this.w_PADRE.LoadRec()     
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VAANAIMP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
