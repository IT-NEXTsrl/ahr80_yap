* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bgd                                                        *
*              EXPORT DOCUMENTI                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_65]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-05                                                      *
* Last revis.: 2012-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bgd",oParentObject,m.pEXEC)
return(i_retval)

define class tgsva_bgd as StdBatch
  * --- Local variables
  pEXEC = space(5)
  w_MVTIPDOC = space(5)
  w_MVCODSED = space(5)
  w_MVCODSPE = space(3)
  w_MVCODBA2 = space(15)
  w_MVTIPCON = space(15)
  w_MVCODCON = space(15)
  w_LCODSTR = space(10)
  w_MVCODDES = space(10)
  w_MVNUMCOR = space(25)
  w_MVCODBAN = space(10)
  w_XCONORN = space(10)
  w_CODAZI = space(5)
  w_PATHED = space(254)
  w_MVSERIAL = space(10)
  w_NAZION = space(3)
  w_CODGRU = space(10)
  w_oERRORLOG = .NULL.
  w_INDENT = 0
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_RIFDOC = space(30)
  w_PROGRE = space(14)
  w_CODESE = space(5)
  w_POS = 0
  w_MASK = space(50)
  w_FILL = space(1)
  w_ERRODIR = .f.
  w_HANDLEFILE = 0
  w_NOMFIL = space(200)
  w_CODELE = space(10)
  w_MESS = space(100)
  w_NUMREC = 0
  w_INDICE = 0
  w_CODTAB = space(30)
  w_INDPROG = 0
  * --- WorkFile variables
  VASTRUTT_idx=0
  CONTROPA_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  STR_INTE_idx=0
  VAVARNOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pEXEC="ESPOR"
      this.w_oERRORLOG=createobject("AH_ErrorLog")
       
 Select XCHK FROM (this.oParentObject.w_ZOOMDOC.ccursor) WHERE XCHK=1 into cursor Tmp_Exp
      if Reccount("Tmp_Exp")=0
        ah_errormsg("Attenzione, nessun documento selezionato!")
        i_retcode = 'stop'
        return
      endif
      if Used("Tmp_Exp")
        Select Tmp_Exp 
 Use
      endif
       
 CountSegm=0 
 CountSect=0 
 CountIndent=0 
 TestRiga=.t. 
 Predi=" "
      if this.oParentObject.w_FLFILE="S"
        this.w_CODAZI = i_CODAZI
        do case
          case this.oParentObject.w_TIPSTR="I"
            if Empty(this.oParentObject.w_TIPDOC) or Empty(this.oParentObject.w_CODCON) 
              if Empty(this.oParentObject.w_CODCON) 
                ah_errormsg("Attenzione, per il criterio scelto � necessario valorizzare l'intestatario!")
              else
                ah_errormsg("Attenzione, per il criterio scelto � necessario valorizzare la causale documento!")
              endif
              i_retcode = 'stop'
              return
            else
              * --- Read from STR_INTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.STR_INTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.STR_INTE_idx,2],.t.,this.STR_INTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SICODSTR"+;
                  " from "+i_cTable+" STR_INTE where ";
                      +"SITIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPOCF);
                      +" and SICODCON = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
                      +" and SITIPDOC = "+cp_ToStrODBC(this.oParentObject.w_TIPDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SICODSTR;
                  from (i_cTable) where;
                      SITIPCON = this.oParentObject.w_TIPOCF;
                      and SICODCON = this.oParentObject.w_CODCON;
                      and SITIPDOC = this.oParentObject.w_TIPDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LCODSTR = NVL(cp_ToDate(_read_.SICODSTR),cp_NullValue(_read_.SICODSTR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          case this.oParentObject.w_TIPSTR="D"
            if Empty(this.oParentObject.w_TIPDOC) 
              ah_errormsg("Attenzione, per il criterio scelto � necessario valorizzare la causale documento!")
              i_retcode = 'stop'
              return
            else
              * --- Read from TIP_DOCU
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TDCODSTR"+;
                  " from "+i_cTable+" TIP_DOCU where ";
                      +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_TIPDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TDCODSTR;
                  from (i_cTable) where;
                      TDTIPDOC = this.oParentObject.w_TIPDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LCODSTR = NVL(cp_ToDate(_read_.TDCODSTR),cp_NullValue(_read_.TDCODSTR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          otherwise
            this.w_LCODSTR = this.oParentObject.w_CODSTR
        endcase
        if Empty(this.w_LCODSTR)
          ah_errormsg("Attenzione, per il criterio scelto struttura non presente!")
          i_retcode = 'stop'
          return
        endif
        if Not Empty(this.oParentObject.w_CODCON) and Empty(this.oParentObject.w_TIPDOC) and this.oParentObject.w_TIPSTR <>"S"
          ah_errormsg("Attenzione, causale documento non specificata!")
          i_retcode = 'stop'
          return
        endif
        * --- Read from CONTROPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COPATHED"+;
            " from "+i_cTable+" CONTROPA where ";
                +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COPATHED;
            from (i_cTable) where;
                COCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PATHED = NVL(cp_ToDate(_read_.COPATHED),cp_NullValue(_read_.COPATHED))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VASTRUTT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VASTRUTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "STNOMFIL,STCODENT,STTIPFIL,STINDENT"+;
            " from "+i_cTable+" VASTRUTT where ";
                +"STCODICE = "+cp_ToStrODBC(this.w_LCODSTR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            STNOMFIL,STCODENT,STTIPFIL,STINDENT;
            from (i_cTable) where;
                STCODICE = this.w_LCODSTR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NOMFIL = NVL(cp_ToDate(_read_.STNOMFIL),cp_NullValue(_read_.STNOMFIL))
          w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
          w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
          this.w_INDENT = NVL(cp_ToDate(_read_.STINDENT),cp_NullValue(_read_.STINDENT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Empty(Nvl(this.w_PATHED," "))
          ah_errormsg("Attenzione! Occorre indicare il percorso di generazione del file nei parametri!")
          i_retcode = 'stop'
          return
        else
          this.w_PATHED = ADDBS(ALLTRIM(this.w_PATHED))
        endif
        this.w_HANDLEFILE = -1
        * --- Try
        local bErr_035F1A80
        bErr_035F1A80=bTrsErr
        this.Try_035F1A80()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_035F1A80
        * --- End
        if this.w_HANDLEFILE<0
          i_retcode = 'stop'
          return
        endif
        Predi=this.w_PROGRE
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
       
 Predi=this.w_PROGRE
       
 ND=this.oParentObject.w_ZOOMDOC.ccursor 
 Select (ND) 
 go top 
 scan for XCHK=1
      this.w_MVSERIAL = &ND..MVSERIAL
      this.w_MVCODSED = &ND..MVCODDES
      this.w_MVCODSPE = &ND..MVCODSPE
      this.w_MVCODBA2 = &ND..MVCODBA2
      this.w_MVTIPCON = &ND..MVTIPCON
      this.w_MVCODCON = &ND..MVCODCON
      this.w_MVCODDES = &ND..MVCODDES
      this.w_MVNUMCOR = &ND..MVNUMCOR
      this.w_MVCODBAN = &ND..MVCODBAN
      this.w_XCONORN = &ND..MVCODORN
      this.w_MVTIPDOC = &ND..MVTIPDOC
      this.w_MVNUMDOC = Nvl(&ND..MVNUMDOC,0)
      this.w_MVALFDOC = Nvl(&ND..MVALFDOC,"")
      this.w_MVDATDOC = cp_todate(&ND..MVDATDOC)
      this.w_RIFDOC = Alltrim(str(this.w_MVNUMDOC))+IIF(Not Empty(this.w_mvalfdoc),"/"+Alltrim(this.w_MVALFDOC),Alltrim(this.w_MVALFDOC))
      if this.oParentObject.w_FLFILE<>"S"
        if Not empty(this.w_MVCODCON)
          * --- Se impostata applico struttura implementata sull'intestatario
          * --- Read from STR_INTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STR_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STR_INTE_idx,2],.t.,this.STR_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SICODSTR"+;
              " from "+i_cTable+" STR_INTE where ";
                  +"SITIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and SICODCON = "+cp_ToStrODBC(this.w_MVCODCON);
                  +" and SITIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SICODSTR;
              from (i_cTable) where;
                  SITIPCON = this.w_MVTIPCON;
                  and SICODCON = this.w_MVCODCON;
                  and SITIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LCODSTR = NVL(cp_ToDate(_read_.SICODSTR),cp_NullValue(_read_.SICODSTR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if EMPTY(this.w_LCODSTR)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDCODSTR"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDCODSTR;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LCODSTR = NVL(cp_ToDate(_read_.TDCODSTR),cp_NullValue(_read_.TDCODSTR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      if Not empty(this.w_LCODSTR)
        * --- Creazione FIle EDI
         
 DECLARE ARRKEY(1,3) 
 ARRKEY[1,3]="DOC_MAST" 
 ARRKEY[1,1]=this.w_MVSERIAL 
 ARRKEY[1,2]="MVSERIAL"
        gsar_bee(this,this.w_LCODSTR,this.w_MVCODCON,this.w_MVTIPCON,this.w_CODGRU, @ARRKEY,this.w_HANDLEFILE,IIF(this.oParentObject.w_FLFILE="S","U","M"),this.w_oERRORLOG)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if !bTrsErr
          * --- Aggiorno flag generazione file edi
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLSEND ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLSEND');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   )
          else
            update (i_cTable) set;
                MVFLSEND = "S";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4),"Documento numero %1 del %2 con causale %3 non esportato, struttura mancante",Alltrim(this.w_RIFDOC),DTOC(this.w_MVDATDOC),Alltrim(this.w_MVTIPDOC))     
      endif
      if Not Empty(this.w_NOMFIL)
        Ah_msg("Generato file %1",.t.,.f.,1,Alltrim(this.w_nomfil))
      endif
       
 Select (ND) 
 Endscan
      if this.oParentObject.w_FLFILE="S"
        * --- Eseguo scruttura elementi di chiusura file
        * --- Select from gsva0bvs
        do vq_exec with 'gsva0bvs',this,'_Curs_gsva0bvs','',.f.,.t.
        if used('_Curs_gsva0bvs')
          select _Curs_gsva0bvs
          locate for 1=1
          do while not(eof())
          this.w_CODELE = _Curs_gsva0bvs.ELCODICE
           
 Dimension Arprog(1,3) 
 Arprog[1,1]="" 
 Arprog[1,2]=0 
 Arprog[1,3]=0
           
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_LCODSTR,w_CODENT,this.w_CODELE,"","","",this.w_HANDLEFILE,0,this.w_oerrorlog,"C","",this.w_INDENT,0,@Arprog)
            select _Curs_gsva0bvs
            continue
          enddo
          use
        endif
      endif
      * --- Chiusura ciclo documenti
      *     chiusura file 
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
      this.w_MESS = IIF(this.oParentObject.w_FLFILE="S","File generato con successo","File generati con successo")
      if this.oParentObject.w_FLFILE="S"
        if not (FCLOSE(this.w_HANDLEFILE))
          ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_NOMFIL)
        endif
      endif
      if this.w_oErrorLog.IsFullLog()
        ah_ErrorMsg("Generazione completata","i","")
      else
        ah_ErrorMsg("Generazione completata con successo","i","")
      endif
    else
      ND=this.oParentObject.w_ZOOMDOC.cCURSOR
      UPDATE (ND) SET XCHK=ICASE(this.pEXEC="SELEZ",1,this.pEXEC="DESEL", 0, IIF(XCHK=1,0,1))
    endif
  endproc
  proc Try_035F1A80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PROGRE = SPACE(14)
    this.w_CODESE = g_CODESE
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PREDI", "i_CODAZI,w_CODESE,w_PROGRE")
    if Not Empty(this.w_NOMFIL)
       
 DIMENSION ARPARAM[8,2] 
 ARPARAM[1,1]="DATE" 
 ARPARAM[1,2]=ALLTRIM(STR(YEAR(i_DATSYS))+Right("00"+alltrim(STR(MONTH(i_DATSYS))),2)+Right("00"+alltrim(STR(DAY(i_DATSYS))),2)) 
 ARPARAM[2,1]="TIME" 
 ARPARAM[2,2]=ALLTRIM(STRTRAN(TIME(),":","")) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="NUMDOC" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="PROGRE" 
 ARPARAM[5,2]=this.w_PROGRE 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=this.oParentObject.w_CODCON 
 ARPARAM[7,1]="TIPCON" 
 ARPARAM[7,2]=this.oParentObject.w_TIPOCF 
 ARPARAM[8,1]="DATREG" 
 ARPARAM[8,2]=""
       
 DIMENSION ARVAR[8,2] 
 ARVAR[1,1]="DATE" 
 ARVAR[1,2]=i_DATSYS 
 ARVAR[2,1]="TIME" 
 ARVAR[2,2]=TIME() 
 ARVAR[3,1]="DATDOC" 
 ARVAR[3,2]="" 
 ARVAR[4,1]="NUMDOC" 
 ARVAR[4,2]=0 
 ARVAR[5,1]="PROGRE" 
 ARVAR[5,2]=this.w_PROGRE 
 ARVAR[6,1]="CODCON" 
 ARVAR[6,2]=this.oParentObject.w_CODCON 
 ARVAR[7,1]="TIPCON" 
 ARVAR[7,2]=this.oParentObject.w_TIPOCF 
 ARPARAM[8,1]="DATREG" 
 ARPARAM[8,2]=""
      * --- Select from VAVARNOM
      i_nConn=i_TableProp[this.VAVARNOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAVARNOM_idx,2],.t.,this.VAVARNOM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" VAVARNOM ";
            +" where VNCODICE="+cp_ToStrODBC(this.oParentObject.w_CODSTR)+"";
             ,"_Curs_VAVARNOM")
      else
        select * from (i_cTable);
         where VNCODICE=this.oParentObject.w_CODSTR;
          into cursor _Curs_VAVARNOM
      endif
      if used('_Curs_VAVARNOM')
        select _Curs_VAVARNOM
        locate for 1=1
        do while not(eof())
        this.w_MASK = _Curs_VAVARNOM.VN__MASK
        this.w_FILL = _Curs_VAVARNOM.VNZERFIL
        this.w_POS = INT((ASCAN("ARVAR",Alltrim(Nvl(_Curs_VAVARNOM.VNCODVAR," ")))+1)/2)
        if this.w_POS>0
          * --- Applico maschera
          if Not Empty(this.w_MASK)
            do case
              case VarType(ARVAR[this.w_POS,2]) $ "D-T"
                 
 VALDAT=cp_todate(ARVAR[this.w_POS,2])
                if Not Empty(VALDAT)
                  * --- Anno
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("A",OCCURS("A",this.w_MASK)),right(ALLTRIM(STR(year(VALDAT))),oCCURS("A",this.w_MASK)))
                  * --- Mese
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("M",OCCURS("M",this.w_MASK)),right("00"+ALLTRIM(STR(month(VALDAT))),oCCURS("M",this.w_MASK)))
                  * --- giorno
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("G",OCCURS("G",this.w_MASK)),right("00"+ALLTRIM(STR(day(VALDAT))),oCCURS("G",this.w_MASK)))
                  * --- ora
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("O",OCCURS("O",this.w_MASK)),right(ALLTRIM(STR(HOUR(VALDAT))),oCCURS("O",this.w_MASK)))
                  * --- minuti
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("T",OCCURS("T",this.w_MASK)),right(ALLTRIM(STR(MINUTE(VALDAT))),oCCURS("T",this.w_MASK)))
                  * --- Secondi
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("S",OCCURS("S",this.w_MASK)),right(ALLTRIM(STR(SEC(VALDAT))),oCCURS("S",this.w_MASK)))
                   
 ARPARAM[this.w_POS,2]=strtran(Alltrim(this.w_MASK),SET ("MARK"),"")
                endif
              case VarType(ARVAR[this.w_POS,2]) = "C"
                if this.w_FILL<>"S"
                   
 ARPARAM[this.w_POS,2]=Left(Alltrim(ARPARAM[this.w_POS,2]),Len(alltrim(this.w_MASK)))
                endif
            endcase
          endif
          * --- Applico zerofil in funzione della maschera inputata
          if this.w_FILL="S" and Not Empty(this.w_MASK) and Not Empty(ARVAR[this.w_POS,2])
            ARPARAM[this.w_POS,2]=Right(REPLICATE("0",len(Alltrim(this.w_MASK)))+ARPARAM[this.w_POS,2],len(Alltrim(this.w_MASK)))
          endif
        endif
          select _Curs_VAVARNOM
          continue
        enddo
        use
      endif
      this.w_NOMFIL = CALDESPA(this.w_NOMFIL,@ARPARAM,.T.)
    else
      ah_errormsg("Attenzione, nome file non presente!")
      * --- Raise
      i_Error="Errore nella creazione file EDI"
      return
    endif
    if ! Directory(Alltrim(this.w_PATHED))
      this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_PATHED))
    endif
    if ! this.w_ERRODIR
      if Not File(Alltrim(this.w_PATHED)+Alltrim(this.w_NOMFIL)) Or ah_YesNo("Il file esiste gi�, si desidera sovrascriverlo?")
        this.w_HANDLEFILE = FCREATE(Alltrim(this.w_PATHED)+Alltrim(this.w_NOMFIL))
      endif
    endif
    if this.w_HANDLEFILE<0
      * --- Raise
      i_Error="Errore nella creazione file EDI"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione FIle EDI
     
 DECLARE ARRKEY(1,3) 
 ARRKEY[1,3]="DOC_MAST" 
 ARRKEY[1,1]="@@@@@@@@@@" 
 ARRKEY[1,2]="MVSERIAL"
    * --- Eseguo creazione cursori con nome tabelle dell'entit�
    this.w_INDPROG = 1
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = Alltrim(Nvl(_Curs_GSVA1BGE.ELCODTAB,Space(30)))
       
 Dimension Arprog(this.w_INDPROG,3) 
 Arprog[this.w_INDPROG,1]=this.w_CODTAB 
 Arprog[this.w_INDPROG,2]=0 
 Arprog[this.w_INDPROG,3]=0
      this.w_INDPROG = this.w_INDPROG + 1
       
 a=creacuredi(this.w_LCODSTR,Alltrim(_Curs_gsva1bge.ELCODTAB),@Arrkey)
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    * --- Select from gsva0bvs
    do vq_exec with 'gsva0bvs',this,'_Curs_gsva0bvs','',.f.,.t.
    if used('_Curs_gsva0bvs')
      select _Curs_gsva0bvs
      locate for 1=1
      do while not(eof())
      this.w_CODELE = _Curs_gsva0bvs.ELCODICE
       
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_LCODSTR,w_CODENT,this.w_CODELE,this.oParentObject.w_CODCON,this.oParentObject.w_TIPOCF,"",this.w_HANDLEFILE,0,this.w_oerrorlog,"A","",this.w_INDENT,0,@Arprog)
        select _Curs_gsva0bvs
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='STR_INTE'
    this.cWorkTables[6]='VAVARNOM'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_VAVARNOM')
      use in _Curs_VAVARNOM
    endif
    if used('_Curs_gsva0bvs')
      use in _Curs_gsva0bvs
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    if used('_Curs_gsva0bvs')
      use in _Curs_gsva0bvs
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsva_bgd
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
