* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bzc                                                        *
*              COLORA ZOOM TABELLE DI CONFINE                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_9]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-27                                                      *
* Last revis.: 2006-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOBJ,pEXPR,pSELEZI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bzc",oParentObject,m.pOBJ,m.pEXPR,m.pSELEZI)
return(i_retval)

define class tgsva_bzc as StdBatch
  * --- Local variables
  pOBJ = .NULL.
  pEXPR = space(100)
  pSELEZI = space(5)
  w_LOOP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SELEZ-DESEL-INSEL seleziona\deseleziona tutto\inverte selezione
    *     COLOR  colora zoom
    this.w_LOOP = 0
    if this.pSELEZI == "COLOR"
      this.w_LOOP = 1
      do while this.w_LOOP<= this.pOBJ.grd.ColumnCount
        if UPPER(this.pOBJ.grd.Columns[ this.w_LOOP ].ControlSource) <> "XCHK"
          this.pOBJ.grd.Columns[ this.w_LOOP ].DynamicForeColor = this.pEXPR
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    else
      UPDATE (this.pOBJ.cCURSOR) SET XCHK=ICASE(this.pSELEZI="SELEZ",1,this.pSELEZI="DESEL", 0, IIF(XCHK=1,0,1))
    endif
  endproc


  proc Init(oParentObject,pOBJ,pEXPR,pSELEZI)
    this.pOBJ=pOBJ
    this.pEXPR=pEXPR
    this.pSELEZI=pSELEZI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOBJ,pEXPR,pSELEZI"
endproc
