* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bxm                                                        *
*              CREA OGGETTO XML DA FILE                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-20                                                      *
* Last revis.: 2017-01-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pFILE,pOBJXML,pSERIAL,pVERBOSE,pLOG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bxm",oParentObject,m.pCODSTR,m.pFILE,m.pOBJXML,m.pSERIAL,m.pVERBOSE,m.pLOG)
return(i_retval)

define class tgsva_bxm as StdBatch
  * --- Local variables
  w_RESULT = 0
  pCODSTR = space(10)
  pFILE = space(254)
  pOBJXML = .NULL.
  pSERIAL = space(10)
  pVERBOSE = .f.
  pLOG = .NULL.
  w_CODSTR = space(10)
  w_ROOT = space(20)
  w_STRINGA = space(0)
  w_CODELE = space(20)
  w_OCCOR = 0
  w_VALORE = space(254)
  w_INISEZ = space(1)
  w_POS = 0
  w_TIPELE = space(1)
  w_INDEN = 0
  w_LUNTAG = 0
  w_LUNSEG = 0
  w_LUN_SEG = 0
  w_COUNT_LUN = 0
  w_LTIPCAM = space(1)
  w_DECIMAL = 0
  w_LENGHT = 0
  w_CODTAB = space(30)
  w_LCAMPO = space(30)
  w_OKTAG = .f.
  w_QUALIF = space(254)
  w_RIGHE = 0
  w_LEN = 0
  w_LUNELE = 0
  w_STRFILE = space(0)
  w_OKTAG = .f.
  w_LOOP = 0
  w_ELE_FIGLIO = space(20)
  w_INIZIO = 0
  w_TABELE = space(30)
  w_CARFIL = space(1)
  w_OBJ_ROOT = .NULL.
  w_OBJ_XML = .NULL.
  w_TIPFIL = space(1)
  w_STCARSEP = space(1)
  w_STTIPFIN = space(1)
  w_STCARFIN = space(1)
  w_STCARSEC = space(1)
  w_INIZIO = 0
  w_FINE = 0
  w_TOTOCC = 0
  w_CARATTERE = space(10)
  w_CRITERIO = 0
  w_TABELE = space(30)
  w_MASK = space(50)
  w_FLTYPE = space(1)
  w_ORDASS = 0
  w_ELQUALIF = space(1)
  w_STRCONF = space(254)
  w_LCODELE = space(20)
  w_LUNQUA = 0
  w_POSQUA = 0
  w_OKQUA = .f.
  w_CAMIMP = space(30)
  w_CAMPAGG = space(30)
  w_OBJ_NODO = .NULL.
  w_OBJ_PADRE = .NULL.
  w_CODPAD = space(20)
  w_LCODELE = space(20)
  w_FILE = space(100)
  w_ERR_CODE = space(100)
  w_REASON = space(254)
  w_PADIRXML = space(200)
  w_CODAZI = space(5)
  w_ERRODIR = .f.
  * --- WorkFile variables
  VASTRUTT_idx=0
  VAELEMEN_idx=0
  TMP_TAGELE_idx=0
  XDC_FIELDS_idx=0
  VAANAIMP_idx=0
  PAR_VEFA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riceve oggetto xml per riferimento e lo restituisco popolato
    *     Nodi = codice elemento
    *     Text= valore del nodo
    * --- Codice struttura
    * --- File di import
    * --- Oggetto XML
    * --- Seriale anagrafica che rappresenta il flusso
    * --- Verbose per condizionare msg di log
    * --- Oggetto della maschera chiamante dove � definita la MSG di log
    this.w_OBJ_XML = this.pOBJXML.oXML
    this.w_CODSTR = this.pCODSTR
    this.w_RESULT = 0
    * --- Determino elemento Radice
    AddMsgNL("%0Elaborazione file %2 iniziata alle %1",this.pLog, Time() ,Alltrim(this.pFile) , , ,, )
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STTIPFIL,STELROOT,STLUNTAG,STLUNSEG,STCARSEP,STTIPFIN,STCARFIN,STCARSEC"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STTIPFIL,STELROOT,STLUNTAG,STLUNSEG,STCARSEP,STTIPFIN,STCARFIN,STCARSEC;
        from (i_cTable) where;
            STCODICE = this.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_ROOT = NVL(cp_ToDate(_read_.STELROOT),cp_NullValue(_read_.STELROOT))
      this.w_LUNTAG = NVL(cp_ToDate(_read_.STLUNTAG),cp_NullValue(_read_.STLUNTAG))
      this.w_LUNSEG = NVL(cp_ToDate(_read_.STLUNSEG),cp_NullValue(_read_.STLUNSEG))
      this.w_STCARSEP = NVL(cp_ToDate(_read_.STCARSEP),cp_NullValue(_read_.STCARSEP))
      this.w_STTIPFIN = NVL(cp_ToDate(_read_.STTIPFIN),cp_NullValue(_read_.STTIPFIN))
      this.w_STCARFIN = NVL(cp_ToDate(_read_.STCARFIN),cp_NullValue(_read_.STCARFIN))
      this.w_STCARSEC = NVL(cp_ToDate(_read_.STCARSEC),cp_NullValue(_read_.STCARSEC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_STCARSEP = IIF(Not Empty(Nvl(this.w_STCARSEP,"+")),this.w_STCARSEP,"+")
    this.w_STCARSEC = IIF(this.w_STCARSEP="+",":",Nvl(this.w_STCARSEC," "))
    if Empty(this.w_ROOT)
      if VarType( this.pLog ) ="O" 
        AddMsgNL("Attenzione la struttura utilizzata � priva di elemento radice ", this.pLog )
        this.w_RESULT = -1
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if this.w_TIPFIL<>"X"
        * --- Creo nodo radice da inserire nell'oggetto XML
        AddMsgNL("Creazione nodo radice", this.pLog)
        this.w_OBJ_ROOT = this.w_OBJ_XML.createNode("element","_"+ Alltrim(this.w_ROOT), "")
        this.w_OBJ_XML.appendChild(this.w_OBJ_ROOT)     
        if VarType( this.pLog ) ="O" and this.pVerbose
          AddMsgNL("Aggiunto nodo radice: %1", this.pLog,Alltrim(this.w_ROOT))
        endif
        * --- Ricavo informazioni dal file e valorizzo tabelle EDI costruite in precedenza
        * --- Create temporary table TMP_TAGELE
        i_nIdx=cp_AddTableDef('TMP_TAGELE') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\VEFA\EXE\QUERY\GSVA_QTA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_TAGELE_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      do case
        case this.w_TIPFIL="X"
          * --- Nel caso di struttura di tipo XML eseguo sola valorizzazione dell'oggetto XML
          AddMsgNL("Valorizzazione oggetto xml", this.pLog)
          if cp_fileexist(this.pfile)
            * --- Try
            local bErr_0362C3A0
            bErr_0362C3A0=bTrsErr
            this.Try_0362C3A0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              AddMsgNL("Errore nella lettura del file%1 !",this.pLog,this.pFile )
            endif
            bTrsErr=bTrsErr or bErr_0362C3A0
            * --- End
          else
            AddMsgNL("Attenzione il file selezionato non esiste!",this.pLog )
            this.w_RESULT = -1
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        case this.w_TIPFIL="E"
          * --- Edifact
          if cp_fileexist(this.pfile)
            AddMsgNL("Creazione cursore stringhe", this.pLog)
             
 CREATE CURSOR CURSASCII ( STRINGA M(10))
            this.w_STRFILE = FILETOSTR(this.pFILE)
          else
            AddMsgNL("Attenzione il file selezionato non esiste!",this.pLog )
            this.w_RESULT = -1
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Ho pi� segmenti nella stessa riga 
          this.w_CRITERIO = 1
          * --- Tolgo eventuale fine riga
          if this.w_STTIPFIN $ "A-T"
            this.w_STRFILE = STRTRAN(this.w_STRFILE,chr(13)+chr(10),"")
            this.w_STRFILE = STRTRAN(this.w_STRFILE,chr(10),"")
          endif
          if this.w_STTIPFIN $ "A-T-R"
            this.w_STRFILE = STRTRAN(this.w_STRFILE,chr(13),"")
          endif
          if Not EMpty(this.w_STCARSEC)
            this.w_STRFILE = STRTRAN(this.w_STRFILE,"?"+Alltrim(this.w_STCARSEC),"&dots&")
          endif
          if this.w_STTIPFIN="A"
            this.w_STRFILE = STRTRAN(this.w_STRFILE,"?'","&apos&")
          endif
          this.w_STRFILE = STRTRAN(this.w_STRFILE,"?"+Alltrim(this.w_STCARSEP),"&plus&")
          this.w_CARATTERE = icase(this.w_STTIPFIN="A","'",this.w_STTIPFIN="R",CHR(10),this.w_STTIPFIN="F",CHR(13)+CHR(10),this.w_STTIPFIN="T" and Not empty(this.w_STCARFIN),this.w_STCARFIN,"'")
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Select CursAscii 
 go top 
 Scan 
          this.w_STRINGA = CURSASCII.STRINGA
          * --- Preparo stringa per successiva iterazione
          this.w_STRINGA = STRTRAN(this.w_STRINGA,Alltrim(this.w_STCARSEP),"&#@#")
          if this.w_STTIPFIN="A"
            this.w_STRINGA = STRTRAN(this.w_STRINGA,"'","&#@#")
          endif
          if Not EMpty(this.w_STCARSEC)
            this.w_STRINGA = STRTRAN(this.w_STRINGA,Alltrim(this.w_STCARSEC),"&#@#")
            this.w_STRINGA = STRTRAN(this.w_STRINGA,"&dots&",Alltrim(this.w_STCARSEC))
          endif
          * --- Riporto i valori originari
          if this.w_STTIPFIN="A"
            this.w_STRINGA = STRTRAN(this.w_STRINGA,"&apos&","'")
          endif
          this.w_STRINGA = STRTRAN(this.w_STRINGA,"&plus&",this.w_STCARSEP)
          this.w_STRINGA = STRTRAN(this.w_STRINGA,"??","?")
          * --- Estrapolo valore dell'elemento Tag
          this.w_VALORE = SUBSTR(this.w_STRINGA,1,AT("&#@#",this.w_STRINGA)-1)
          AddMsgNL("Eseguita analisi riga con tag %1",this.pLog, Alltrim(this.w_VALORE) , , , ,, )
          Inkey(0.001,"H")
          * --- Determino elemento tag
          this.w_RIGHE = 0
          this.w_OKTAG = .F.
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_INISEZ="S"
            * --- Write into TMP_TAGELE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("N"),'TMP_TAGELE','FLUTILI');
              +",CAMPO ="+cp_NullLink(cp_ToStrODBC(Space(30)),'TMP_TAGELE','CAMPO');
                  +i_ccchkf ;
              +" where ";
                  +"TABELLA = "+cp_ToStrODBC(this.w_CODTAB);
                     )
            else
              update (i_cTable) set;
                  FLUTILI = "N";
                  ,CAMPO = Space(30);
                  &i_ccchkf. ;
               where;
                  TABELLA = this.w_CODTAB;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Marco elemento tag utilizzato
          * --- Try
          local bErr_0346CF28
          bErr_0346CF28=bTrsErr
          this.Try_0346CF28()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            AddMsgNL("Errore nella scruttura flag utilizzato nell'elemento%1 ", this.pLog,Alltrim(this.w_CODELE))
          endif
          bTrsErr=bTrsErr or bErr_0346CF28
          * --- End
          if Not Empty(this.w_CODELE)
            if NOT EMPTY(this.w_CODTAB) 
              if CHKSTRNUM(this.w_CODELE,"_,-,.") 
                * --- Determino tag di riga  Tag prima occorrenza di :
                *     determino elemento  collegato
                *     determino elementi figli
                 
 DIMENSION ELEMEN[1,2] 
 elemen[1,1]=" " 
 elemen[1,1]=0
                a=creaelenele(this.pCODSTR,this.w_codele,@ELEMEN,0,"E")
                this.w_OCCOR = 1
                do while this.w_OCCOR<=OCCURS("&#@#",this.w_STRINGA) and this.w_OCCOR<=int(Alen(Elemen)/2)
                  this.w_CODELE = elemen[this.w_OCCOR,1]
                  * --- Read from XDC_FIELDS
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "FLDECIMA,FLLENGHT,FLTYPE"+;
                      " from "+i_cTable+" XDC_FIELDS where ";
                          +"TBNAME = "+cp_ToStrODBC(this.w_TABELE);
                          +" and FLNAME = "+cp_ToStrODBC(this.w_LCAMPO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      FLDECIMA,FLLENGHT,FLTYPE;
                      from (i_cTable) where;
                          TBNAME = this.w_TABELE;
                          and FLNAME = this.w_LCAMPO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_DECIMAL = NVL(cp_ToDate(_read_.FLDECIMA),cp_NullValue(_read_.FLDECIMA))
                    this.w_LENGHT = NVL(cp_ToDate(_read_.FLLENGHT),cp_NullValue(_read_.FLLENGHT))
                    this.w_FLTYPE = NVL(cp_ToDate(_read_.FLTYPE),cp_NullValue(_read_.FLTYPE))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_VALORE = SUBSTR(this.w_STRINGA,AT("&#@#",this.w_STRINGA,this.w_OCCOR)+4,AT("&#@#",this.w_STRINGA,this.w_OCCOR+1)-AT("&#@#",this.w_STRINGA,this.w_OCCOR)-4)
                  this.w_OCCOR = this.w_OCCOR + 1
                enddo
              else
                if VarType( this.pLog ) ="O" 
                  AddMsgNL("Attenzione! L'elemento %1 contiene caratteri non ammessi.%0Impossibile procedere con l'importazione!", this.pLog,Alltrim(this.w_CODELE))
                  this.w_RESULT = -1
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            else
              if VarType( this.pLog ) ="O" 
                AddMsgNL("Attenzione, il valore del tag %1 corrisponde ad un elemento della struttura privo di tabella", this.pLog,Alltrim(this.w_VALORE))
                this.w_RESULT = -1
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          else
            if VarType( this.pLog ) ="O" 
              AddMsgNL("Attenzione valore del tag: %1 non corrisponde a nessun elemento della struttura.%0 Verificarne l'esistenza e la corretta valorizzazione del campo tabella import.", this.pLog,Alltrim(this.w_VALORE))
              this.w_RESULT = -1
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if Not Empty(this.w_QUALIF) and this.w_INISEZ="S" and this.w_ELQUALIF $ "T-S"
            * --- Pulisco flag utilizzato nel  caso di tag di rottura sezione replicati in punti diversi con qualificatore diverso
            if this.w_ELQUALIF="T"
              * --- Read from VAELEMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VAELEMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ELTABIMP"+;
                  " from "+i_cTable+" VAELEMEN where ";
                      +"ELCODSTR = "+cp_ToStrODBC(this.pCODSTR);
                      +" and ELVALTX3 = "+cp_ToStrODBC(this.w_QUALIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ELTABIMP;
                  from (i_cTable) where;
                      ELCODSTR = this.pCODSTR;
                      and ELVALTX3 = this.w_QUALIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Read from VAELEMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VAELEMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ELTABIMP"+;
                  " from "+i_cTable+" VAELEMEN where ";
                      +"ELCODSTR = "+cp_ToStrODBC(this.pCODSTR);
                      +" and ELVALTX3 = "+cp_ToStrODBC(this.w_QUALIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ELTABIMP;
                  from (i_cTable) where;
                      ELCODSTR = this.pCODSTR;
                      and ELVALTX3 = this.w_QUALIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if Not empty(this.w_CODTAB)
              * --- Write into TMP_TAGELE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("N"),'TMP_TAGELE','FLUTILI');
                +",CAMPO ="+cp_NullLink(cp_ToStrODBC(Space(30)),'TMP_TAGELE','CAMPO');
                    +i_ccchkf ;
                +" where ";
                    +"TABELLA = "+cp_ToStrODBC(this.w_CODTAB);
                       )
              else
                update (i_cTable) set;
                    FLUTILI = "N";
                    ,CAMPO = Space(30);
                    &i_ccchkf. ;
                 where;
                    TABELLA = this.w_CODTAB;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
           
 Select CURSASCII 
 EndScan
        case this.w_TIPFIL="L"
          * --- Edifact
          *     Verifico che la radice abbia figli
          if cp_fileexist(this.pfile)
            AddMsgNL("Creazione cursore stringhe", this.pLog)
             
 CREATE CURSOR CURSASCII ( STRINGA M(10))
            this.w_STRFILE = FILETOSTR(this.pFILE)
          else
            AddMsgNL("Attenzione il file selezionato non esiste!", this.pLog)
          endif
          if this.w_LUNSEG>0
            this.w_CRITERIO = 2
          else
            this.w_CRITERIO = 1
            if this.w_STTIPFIN = "R"
              this.w_CARATTERE = Chr(10)
            else
              this.w_CARATTERE = Chr(13)+Chr(10)
            endif
          endif
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Select CursAscii 
 go top 
 Scan 
          this.w_STRINGA = CURSASCII.STRINGA
          * --- Estrapolo valore dell'elemento Tag
          this.w_VALORE = SUBSTR(this.w_STRINGA,1,this.w_LUNTAG)
          AddMsgNL("Eseguita analisi riga con tag %1",this.pLog, Alltrim(this.w_VALORE) , , , ,, )
          this.w_OKTAG = .F.
          this.w_RIGHE = 0
          * --- Determino elemento tag dalla lista 
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_INISEZ="S"
            * --- smarco elementi tag utilizzati della tabella
            * --- Try
            local bErr_04EDB3B0
            bErr_04EDB3B0=bTrsErr
            this.Try_04EDB3B0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              AddMsgNL("Errore nell'azzeramento flag utilizzato su tabella %1 ", this.pLog,Alltrim(this.w_CODTAB))
            endif
            bTrsErr=bTrsErr or bErr_04EDB3B0
            * --- End
          endif
          * --- Marco elemento tag utilizzato
          * --- Try
          local bErr_04ED0388
          bErr_04ED0388=bTrsErr
          this.Try_04ED0388()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            AddMsgNL("Errore nella scrittura flag utilizzato nell'elemento %1 ", this.pLog,Alltrim(this.w_CODELE))
          endif
          bTrsErr=bTrsErr or bErr_04ED0388
          * --- End
          if Not Empty(this.w_CODELE)
            if NOT EMPTY(this.w_CODTAB) 
              if CHKSTRNUM(this.w_CODELE,"_,-,.") 
                * --- Determino tag di riga  Tag prima occorrenza di :
                *     determino elemento  collegato
                *     determino elementi figli
                 
 DIMENSION ELEMEN(1,2) 
 elemen[1,1]=" " 
 elemen[1,2]=" " 
 a=creaelenele(this.pCODSTR,this.w_codele,@ELEMEN,0,"E")
                this.w_OCCOR = 1
                this.w_INIZIO = 1
                * --- Ciclo sulla riga e scrivo nodi collegati al relativo Tag
                do while this.w_OCCOR<=int(Alen(Elemen)/2)
                  this.w_CODELE = elemen[this.w_OCCOR,1]
                  * --- Read from VAELEMEN
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.VAELEMEN_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ELTIPCAM,ELTABIMP,ELCAMIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELLUNMAX,ELCARIMP,ELALLIMP,EL__MASK"+;
                      " from "+i_cTable+" VAELEMEN where ";
                          +"ELCODSTR = "+cp_ToStrODBC(this.w_CODSTR);
                          +" and ELCODICE = "+cp_ToStrODBC(this.w_CODELE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ELTIPCAM,ELTABIMP,ELCAMIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELLUNMAX,ELCARIMP,ELALLIMP,EL__MASK;
                      from (i_cTable) where;
                          ELCODSTR = this.w_CODSTR;
                          and ELCODICE = this.w_CODELE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_LTIPCAM = NVL(cp_ToDate(_read_.ELTIPCAM),cp_NullValue(_read_.ELTIPCAM))
                    this.w_TABELE = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
                    this.w_LCAMPO = NVL(cp_ToDate(_read_.ELCAMIMP),cp_NullValue(_read_.ELCAMIMP))
                    w_TIPTRA = NVL(cp_ToDate(_read_.ELTIPTR2),cp_NullValue(_read_.ELTIPTR2))
                    w_BATCH = NVL(cp_ToDate(_read_.EL_BATC2),cp_NullValue(_read_.EL_BATC2))
                    w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
                    this.w_LUNELE = NVL(cp_ToDate(_read_.ELLUNMAX),cp_NullValue(_read_.ELLUNMAX))
                    w_CARIMP = NVL(cp_ToDate(_read_.ELCARIMP),cp_NullValue(_read_.ELCARIMP))
                    w_ALLIMP = NVL(cp_ToDate(_read_.ELALLIMP),cp_NullValue(_read_.ELALLIMP))
                    this.w_MASK = NVL(cp_ToDate(_read_.EL__MASK),cp_NullValue(_read_.EL__MASK))
                    this.w_CARFIL = NVL(cp_ToDate(_read_.ELCARIMP),cp_NullValue(_read_.ELCARIMP))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_VALORE = SUBSTR(this.w_STRINGA,this.w_INIZIO,this.w_LUNELE)
                  this.w_INIZIO = this.w_INIZIO+this.w_LUNELE
                  this.Page_3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_OCCOR = this.w_OCCOR + 1
                enddo
              else
                if VarType( this.pLog ) ="O" 
                  AddMsgNL("Attenzione! L'elemento %1 contiene caratteri non ammessi.%0Impossibile procedere con l'importazione!", this.pLog,Alltrim(this.w_CODELE))
                  this.w_RESULT = -1
                  this.Page_4()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            else
              if VarType( this.pLog ) ="O" 
                AddMsgNL("Attenzione, il valore del tag %1 corrisponde ad un elemento della struttura privo di tabella", this.pLog,Alltrim(this.w_VALORE))
                this.w_RESULT = -1
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          else
            if VarType( this.pLog ) ="O" 
              AddMsgNL("Attenzione, il valore del tag %1 non corrisponde a nessun elemento della struttura", this.pLog,Alltrim(this.w_VALORE))
              this.w_RESULT = -1
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
           
 Select CURSASCII 
 EndScan
        otherwise
          * --- Tipologia file generico
      endcase
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_0362C3A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OBJ_XML.load(this.pFILE)     
    return
  proc Try_0346CF28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(this.w_CODELE);
          +" and TAGELE = "+cp_ToStrODBC(this.w_VALORE);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          &i_ccchkf. ;
       where;
          CODICE = this.w_CODELE;
          and TAGELE = this.w_VALORE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04EDB3B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("N"),'TMP_TAGELE','FLUTILI');
      +",CAMPO ="+cp_NullLink(cp_ToStrODBC(Space(30)),'TMP_TAGELE','CAMPO');
          +i_ccchkf ;
      +" where ";
          +"TABELLA = "+cp_ToStrODBC(this.w_CODTAB);
             )
    else
      update (i_cTable) set;
          FLUTILI = "N";
          ,CAMPO = Space(30);
          &i_ccchkf. ;
       where;
          TABELLA = this.w_CODTAB;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04ED0388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(this.w_CODELE);
          +" and TAGELE = "+cp_ToStrODBC(this.w_VALORE);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          &i_ccchkf. ;
       where;
          CODICE = this.w_CODELE;
          and TAGELE = this.w_VALORE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerco elemento tag
    *     Eseguo prima ricerca su elementi con qualificatore
    this.w_OKQUA = .F.
    * --- Select from TMP_TAGELE
    i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2],.t.,this.TMP_TAGELE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_TAGELE ";
          +" where FLUTILI='N' AND TAGELE="+cp_ToStrODBC(this.w_VALORE)+"";
          +" order by INDEN";
           ,"_Curs_TMP_TAGELE")
    else
      select * from (i_cTable);
       where FLUTILI="N" AND TAGELE=this.w_VALORE;
       order by INDEN;
        into cursor _Curs_TMP_TAGELE
    endif
    if used('_Curs_TMP_TAGELE')
      select _Curs_TMP_TAGELE
      locate for 1=1
      do while not(eof())
      this.w_CODELE = _Curs_TMP_TAGELE.CODICE
       
 DIMENSION ELEMEN(1,2) 
 elemen[1,1]=" " 
 elemen[1,2]=" "
      this.w_OKQUA = creaelenele(this.pCODSTR,this.w_codele,@ELEMEN,0,"Q")
      this.w_LOOP = 0
      if this.w_OKQUA
        * --- Eseguo marcatura solo se la funzione restituisce un array significativo
        do while this.w_LOOP<int(Alen(Elemen)/2)
          this.w_LOOP = this.w_LOOP+1
          this.w_LCODELE = ELEMEN[this.w_LOOP,1]
          this.w_QUALIF = ELEMEN[this.w_LOOP,2]
          if Not Empty(this.w_LCODELE)
            * --- Read from VAELEMEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VAELEMEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ELPOSQUA,ELLUNMAX,ELQUALIF"+;
                " from "+i_cTable+" VAELEMEN where ";
                    +"ELCODSTR = "+cp_ToStrODBC(this.pCODSTR);
                    +" and ELCODICE = "+cp_ToStrODBC(this.w_LCODELE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ELPOSQUA,ELLUNMAX,ELQUALIF;
                from (i_cTable) where;
                    ELCODSTR = this.pCODSTR;
                    and ELCODICE = this.w_LCODELE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_POSQUA = NVL(cp_ToDate(_read_.ELPOSQUA),cp_NullValue(_read_.ELPOSQUA))
              this.w_LUNQUA = NVL(cp_ToDate(_read_.ELLUNMAX),cp_NullValue(_read_.ELLUNMAX))
              this.w_ELQUALIF = NVL(cp_ToDate(_read_.ELQUALIF),cp_NullValue(_read_.ELQUALIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Select from gsva_qca
            do vq_exec with 'gsva_qca',this,'_Curs_gsva_qca','',.f.,.t.
            if used('_Curs_gsva_qca')
              select _Curs_gsva_qca
              locate for 1=1
              do while not(eof())
              this.w_CAMIMP = _Curs_gsva_qca.ELCAMIMP
              this.w_CAMPAGG = _Curs_gsva_qca.CAMPAGG
              EXIT
                select _Curs_gsva_qca
                continue
              enddo
              use
            endif
            * --- Cerco tra i suoi fratelli elemento con campo in import
            if Empty(Nvl(this.w_CAMIMP," ")) AND this.w_POSQUA>0
              do case
                case this.w_TIPFIL="E"
                  * --- Identifico nella stringa in base alla posizione e al separatore unico  l'informazione del qualificatore
                  this.w_STRCONF = substr(this.w_STRINGA,AT("&#@#",this.w_STRINGA,this.w_POSQUA) + 4,this.w_LUNQUA)
                case this.w_TIPFIL="L"
                  * --- Identifico nella stringa in base alla posizione e alla lunghezza dell'elemento l'informazione del qualificatore
                  this.w_STRCONF = substr(this.w_STRINGA,this.w_POSQUA,this.w_LUNQUA)
              endcase
              if OCCURS(ALLTRIM(this.w_QUALIF),this.w_STRCONF)>0 and Not Empty(this.w_QUALIF) AND this.w_POSQUA>0
                * --- Ho trovato elemento figlio qualificatore esco subito
                this.w_OKTAG = .T.
                this.w_LOOP = int(Alen(Elemen)/2)
              endif
            endif
          endif
        enddo
        this.w_TIPELE = _Curs_TMP_TAGELE.TIPELE
        this.w_INISEZ = _Curs_TMP_TAGELE.INISEZ
        this.w_CODTAB = _Curs_TMP_TAGELE.TABELLA
        this.w_CODELE = _Curs_TMP_TAGELE.CODICE
        if this.w_OKTAG
          * --- Ho trovato elemento figlio qualificatore esco subito
          *     Valorizzo campo di import per eseguire esclusione elemento con stesso tag
          *     e stesso campo in import
          * --- Write into TMP_TAGELE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CAMPO ="+cp_NullLink(cp_ToStrODBC(this.w_CAMPAGG),'TMP_TAGELE','CAMPO');
                +i_ccchkf ;
            +" where ";
                +"CODICE = "+cp_ToStrODBC(this.w_CODELE);
                   )
          else
            update (i_cTable) set;
                CAMPO = this.w_CAMPAGG;
                &i_ccchkf. ;
             where;
                CODICE = this.w_CODELE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_ORDASS = Nvl(_Curs_TMP_TAGELE.INDEN,0)
          Exit
        else
          * --- Marco cmq gli elementi processati come utilizzati
          * --- Write into TMP_TAGELE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
                +i_ccchkf ;
            +" where ";
                +"CODICE = "+cp_ToStrODBC(this.w_CODELE);
                   )
          else
            update (i_cTable) set;
                FLUTILI = "S";
                &i_ccchkf. ;
             where;
                CODICE = this.w_CODELE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_TMP_TAGELE
        continue
      enddo
      use
    endif
    if NOT this.w_OKTAG
      * --- Cerco elemento senza criterio del qualificatore
      * --- Select from TMP_TAGELE
      i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2],.t.,this.TMP_TAGELE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_TAGELE ";
            +" where FLUTILI='N' AND TAGELE="+cp_ToStrODBC(this.w_VALORE)+"";
            +" order by INDEN";
             ,"_Curs_TMP_TAGELE")
      else
        select * from (i_cTable);
         where FLUTILI="N" AND TAGELE=this.w_VALORE;
         order by INDEN;
          into cursor _Curs_TMP_TAGELE
      endif
      if used('_Curs_TMP_TAGELE')
        select _Curs_TMP_TAGELE
        locate for 1=1
        do while not(eof())
        this.w_CODELE = _Curs_TMP_TAGELE.CODICE
        this.w_TIPELE = _Curs_TMP_TAGELE.TIPELE
        this.w_INISEZ = _Curs_TMP_TAGELE.INISEZ
        this.w_CODTAB = _Curs_TMP_TAGELE.TABELLA
        this.w_ORDASS = Nvl(_Curs_TMP_TAGELE.INDEN,0)
        this.w_OKTAG = .T.
        Exit
          select _Curs_TMP_TAGELE
          continue
        enddo
        use
      endif
    endif
    if this.w_OKTAG
      * --- Write into TMP_TAGELE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_TAGELE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,TAGELE"
        do vq_exec with 'GSVA_QOA',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_TAGELE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 set ";
        +"TMP_TAGELE.FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +Iif(Empty(i_ccchkf),"",",TMP_TAGELE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_TAGELE.CODICE = t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = t2.TAGELE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set (";
            +"FLUTILI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set ";
        +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".TAGELE = "+i_cQueryTable+".TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Se esco dalla select senza aver trovato un elemento tag 
      *     siginifica che ho utilizzato tutti e sono ritornato all'elemento iniziale
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ELCODICE,EL__TIPO,EL_SEGNO,ELINISEZ,ELTABIMP"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(this.pCODSTR);
              +" and ELVALTXT = "+cp_ToStrODBC(this.w_VALORE);
              +" and ELINISEZ = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ELCODICE,EL__TIPO,EL_SEGNO,ELINISEZ,ELTABIMP;
          from (i_cTable) where;
              ELCODSTR = this.pCODSTR;
              and ELVALTXT = this.w_VALORE;
              and ELINISEZ = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODELE = NVL(cp_ToDate(_read_.ELCODICE),cp_NullValue(_read_.ELCODICE))
        this.w_TIPELE = NVL(cp_ToDate(_read_.EL__TIPO),cp_NullValue(_read_.EL__TIPO))
        w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
        this.w_INISEZ = NVL(cp_ToDate(_read_.ELINISEZ),cp_NullValue(_read_.ELINISEZ))
        this.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if CHKSTRNUM(this.w_CODELE,"_,-,.") 
      this.w_LOOP = 1000
      this.w_OBJ_NODO = " "
      * --- Creo nodo da aggiungere
      * --- Aggiungo _ davanti al codice per evitare errore di creazione del nodo 
      *     nel caso di codice che inzia per numero
      this.w_OBJ_NODO = this.w_OBJ_XML.createNode("element", "_"+Alltrim(this.w_CODELE), "")
      if VarType( this.pLog ) ="O" and this.pVerbose
        AddMsgNL("Creato nodo: %1 con valore: %2", this.pLog,Alltrim(this.w_CODELE),this.w_VALORE)
      endif
      if this.w_OCCOR=1
        this.w_OBJ_PADRE = this.w_OBJ_ROOT
        this.w_CODPAD = this.w_ROOT
      else
        this.w_OBJ_NODO.text = this.w_VALORE
        * --- Collego nodo al relativo padre nell'obj  XML
        this.w_LCODELE = this.w_CODELE
      endif
      * --- Aggiungo il nodo
      this.w_OBJ_PADRE.appendChild(this.w_OBJ_NODO)     
      if VarType( this.pLog ) ="O" and this.pVerbose
        AddMsgNL("Aggiunto nodo: %1 al nodo padre: %2", this.pLog,Alltrim(this.w_CODELE),this.w_CODPAD)
      endif
      if this.w_OCCOR=1
        * --- Memorizzo nodo padre equivalente a elemento TAG presente nella prima posizione
        *     dell'array
        this.w_OBJ_PADRE = this.w_OBJ_NODO
        this.w_CODPAD = this.w_CODELE
      endif
    else
      if VarType( this.pLog ) ="O" 
        AddMsgNL("Attenzione! L'elemento %1 contiene caratteri non ammessi.%0Impossibile procedere con l'importazione!", this.pLog,Alltrim(this.w_CODELE))
        this.w_RESULT = -1
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      exit
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if VarType( "pSERIAL" ) = "C"
      this.w_CODAZI = i_CODAZI
      this.w_PADIRXML = ""
      * --- Read from PAR_VEFA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VEFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VEFA_idx,2],.t.,this.PAR_VEFA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PADIRXML"+;
          " from "+i_cTable+" PAR_VEFA where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PADIRXML;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PADIRXML = NVL(cp_ToDate(_read_.PADIRXML),cp_NullValue(_read_.PADIRXML))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_FILE = ADDBS(ALLTRIM(this.w_PADIRXML))
      if !DIRECTORY(Alltrim(this.w_FILE))
        this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_FILE))
      endif
      if this.w_ERRODIR
        AddMsgNL("Impossibile creare la cartella: %1 ", this.pLog,Alltrim(this.w_FILE))
      else
        this.w_FILE = this.w_FILE + Alltrim(this.pSERIAL) + ".XML"
        this.pOBJXML.SaveXml(this.w_FILE)     
        * --- Eseguo validazione
        this.w_ERR_CODE = this.pOBJXML.Oxml.Validate.Errorcode
        this.w_REASON = this.pOBJXML.Oxml.Validate.Reason
        * --- Se il codice errore � -1072897500 equivale alla mancata indicazione del DTD o
        *     di uno schema che in questo file XML non � significaticvo
        if alltrim(str(this.w_err_code,100,0))<>"-1072897500"
          AddMsgNL("Attenzione errore di validazione: %1 ragione: %2 ", this.pLog,alltrim(str(this.w_ERR_CODE,100,0)),Alltrim(this.w_REASON) )
        endif
        * --- Eseguo scrittura in anagrafica importazioni
        * --- Try
        local bErr_04F71400
        bErr_04F71400=bTrsErr
        this.Try_04F71400()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          AddMsgNL("Attenzione errore nella scrittura path file XML ", this.pLog )
        endif
        bTrsErr=bTrsErr or bErr_04F71400
        * --- End
      endif
    endif
    if Used("CURSASCII")
      Select CURSASCII 
 Use
    endif
    if this.w_RESULT=-1
      * --- Azzero oggetto XML
      this.pOBJXML.DestroyXml()     
    endif
    AddMsgNL("%0Elaborazione file %2 finita alle %1",this.pLog, Time() ,Alltrim(this.pFile) , , ,, )
    * --- Drop temporary table TMP_TAGELE
    i_nIdx=cp_GetTableDefIdx('TMP_TAGELE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_TAGELE')
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc
  proc Try_04F71400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMPATXML ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'VAANAIMP','IMPATXML');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
             )
    else
      update (i_cTable) set;
          IMPATXML = this.w_FILE;
          &i_ccchkf. ;
       where;
          IMSERIAL = this.pSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Criteri di rottura stringa da file
    * --- 1) in funzione di un carattere  speciale
    do case
      case this.w_CRITERIO=1
        this.w_INIZIO = 1
        this.w_FINE = 1
        this.w_TOTOCC = OCCUR(Alltrim(this.w_CARATTERE),this.w_STRFILE)
        do while this.w_TOTOCC>0
          * --- Devo creare tante stringhe utilizzando come elemento di rottura le occorrenze della stringa 
          *     passata come carattere
          this.w_STRINGA = SUBSTR(this.w_STRFILE,this.w_INIZIO,AT(Alltrim(this.w_CARATTERE),this.w_STRFILE,this.w_FINE)-(this.w_INIZIO-Len(Alltrim(this.w_CARATTERE))))
          this.w_INIZIO = AT(Alltrim(this.w_CARATTERE),this.w_STRFILE,this.w_FINE)+Len(Alltrim(this.w_CARATTERE))
          this.w_FINE = this.w_FINE + 1
           
 Insert Into CURSASCII (STRINGA) VALUES (this.w_STRINGA)
          this.w_TOTOCC = this.w_TOTOCC-1
        enddo
      case this.w_CRITERIO=2
        this.w_LUN_SEG = this.w_LUNSEG
        this.w_COUNT_LUN = 1
        this.w_STRINGA = SUBSTR(this.w_STRFILE,this.w_COUNT_LUN,this.w_LUN_SEG)
        * --- Eseguo ciclo nel caso di pi� segmenti nella stessa riga file in tal caso la
        *     rottura dei segmenti sar� determinata dalla lunghezza indicata nella struttura
        do while Not Empty(this.w_STRINGA)
           
 Insert Into CURSASCII (STRINGA) VALUES (this.w_STRINGA)
          this.w_COUNT_LUN = this.w_COUNT_LUN+this.w_LUN_SEG
          this.w_STRINGA = SUBSTR(this.w_STRFILE,this.w_COUNT_LUN,this.w_LUN_SEG)
        enddo
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pCODSTR,pFILE,pOBJXML,pSERIAL,pVERBOSE,pLOG)
    this.pCODSTR=pCODSTR
    this.pFILE=pFILE
    this.pOBJXML=pOBJXML
    this.pSERIAL=pSERIAL
    this.pVERBOSE=pVERBOSE
    this.pLOG=pLOG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAELEMEN'
    this.cWorkTables[3]='*TMP_TAGELE'
    this.cWorkTables[4]='XDC_FIELDS'
    this.cWorkTables[5]='VAANAIMP'
    this.cWorkTables[6]='PAR_VEFA'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_TMP_TAGELE')
      use in _Curs_TMP_TAGELE
    endif
    if used('_Curs_gsva_qca')
      use in _Curs_gsva_qca
    endif
    if used('_Curs_TMP_TAGELE')
      use in _Curs_TMP_TAGELE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsva_bxm
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pFILE,pOBJXML,pSERIAL,pVERBOSE,pLOG"
endproc
