* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_sfo                                                        *
*              Stampa formati                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_21]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-21                                                      *
* Last revis.: 2008-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_sfo",oParentObject))

* --- Class definition
define class tgsva_sfo as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 602
  Height = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-02-20"
  HelpContextID=227904919
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  VAFORMAT_IDX = 0
  VASTRUTT_IDX = 0
  cPrg = "gsva_sfo"
  cComment = "Stampa formati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODSTR = space(10)
  w_CODFORIN = space(10)
  w_CODFORFI = space(10)
  w_DESCRI = space(20)
  w_DESCRI1 = space(20)
  w_STDESCRI = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_sfoPag1","gsva_sfo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODSTR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VAFORMAT'
    this.cWorkTables[2]='VASTRUTT'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODSTR=space(10)
      .w_CODFORIN=space(10)
      .w_CODFORFI=space(10)
      .w_DESCRI=space(20)
      .w_DESCRI1=space(20)
      .w_STDESCRI=space(30)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODSTR))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFORIN))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFORFI))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
    this.DoRTCalc(4,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CODSTR))
          select STCODICE,STDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCODSTR_1_1'),i_cWhere,'GSVA_AST',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CODSTR)
            select STCODICE,STDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_STDESCRI = NVL(_Link_.STDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODSTR = space(10)
      endif
      this.w_STDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFORIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_lTable = "VAFORMAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2], .t., this.VAFORMAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFORIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AFO',True,'VAFORMAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_CODFORIN)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);

          i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODSTR,FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODSTR',this.w_CODSTR;
                     ,'FOCODICE',trim(this.w_CODFORIN))
          select FOCODSTR,FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODSTR,FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFORIN)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStrODBC(trim(this.w_CODFORIN)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);

            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStr(trim(this.w_CODFORIN)+"%");
                   +" and FOCODSTR="+cp_ToStr(this.w_CODSTR);

            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFORIN) and !this.bDontReportError
            deferred_cp_zoom('VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(oSource.parent,'oCODFORIN_1_2'),i_cWhere,'GSVA_AFO',"Formati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',oSource.xKey(1);
                       ,'FOCODICE',oSource.xKey(2))
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFORIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_CODFORIN);
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',this.w_CODSTR;
                       ,'FOCODICE',this.w_CODFORIN)
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFORIN = NVL(_Link_.FOCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.FODESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFORIN = space(10)
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODSTR,1)+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.VAFORMAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFORIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFORFI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_lTable = "VAFORMAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2], .t., this.VAFORMAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFORFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AFO',True,'VAFORMAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_CODFORFI)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);

          i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODSTR,FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODSTR',this.w_CODSTR;
                     ,'FOCODICE',trim(this.w_CODFORFI))
          select FOCODSTR,FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODSTR,FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFORFI)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStrODBC(trim(this.w_CODFORFI)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);

            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStr(trim(this.w_CODFORFI)+"%");
                   +" and FOCODSTR="+cp_ToStr(this.w_CODSTR);

            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFORFI) and !this.bDontReportError
            deferred_cp_zoom('VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(oSource.parent,'oCODFORFI_1_3'),i_cWhere,'GSVA_AFO',"Formati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',oSource.xKey(1);
                       ,'FOCODICE',oSource.xKey(2))
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFORFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_CODFORFI);
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',this.w_CODSTR;
                       ,'FOCODICE',this.w_CODFORFI)
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFORFI = NVL(_Link_.FOCODICE,space(10))
      this.w_DESCRI1 = NVL(_Link_.FODESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODFORFI = space(10)
      endif
      this.w_DESCRI1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODSTR,1)+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.VAFORMAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFORFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODSTR_1_1.value==this.w_CODSTR)
      this.oPgFrm.Page1.oPag.oCODSTR_1_1.value=this.w_CODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFORIN_1_2.value==this.w_CODFORIN)
      this.oPgFrm.Page1.oPag.oCODFORIN_1_2.value=this.w_CODFORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFORFI_1_3.value==this.w_CODFORFI)
      this.oPgFrm.Page1.oPag.oCODFORFI_1_3.value=this.w_CODFORFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_12.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_12.value=this.w_STDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsva_sfoPag1 as StdContainer
  Width  = 598
  height = 193
  stdWidth  = 598
  stdheight = 193
  resizeXpos=373
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODSTR_1_1 as StdField with uid="MHOKLWMPLP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODSTR", cQueryName = "CODSTR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 181591770,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=149, Top=12, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_CODSTR"

  func oCODSTR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_CODFORIN)
        bRes2=.link_1_2('Full')
      endif
      if .not. empty(.w_CODFORFI)
        bRes2=.link_1_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODSTR_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTR_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCODSTR_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Strutture",'',this.parent.oContained
  endproc
  proc oCODSTR_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_CODSTR
     i_obj.ecpSave()
  endproc

  add object oCODFORIN_1_2 as StdField with uid="MQPFKCNFRO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFORIN", cQueryName = "CODFORIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice formato",;
    HelpContextID = 80748916,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=149, Top=44, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAFORMAT", cZoomOnZoom="GSVA_AFO", oKey_1_1="FOCODSTR", oKey_1_2="this.w_CODSTR", oKey_2_1="FOCODICE", oKey_2_2="this.w_CODFORIN"

  func oCODFORIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFORIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFORIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAFORMAT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_CODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStr(this.Parent.oContained.w_CODSTR)
    endif
    do cp_zoom with 'VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(this.parent,'oCODFORIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AFO',"Formati",'',this.parent.oContained
  endproc
  proc oCODFORIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AFO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FOCODSTR=w_CODSTR
     i_obj.w_FOCODICE=this.parent.oContained.w_CODFORIN
     i_obj.ecpSave()
  endproc

  add object oCODFORFI_1_3 as StdField with uid="YYTMMSUHKJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFORFI", cQueryName = "CODFORFI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice formato",;
    HelpContextID = 80748911,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=149, Top=70, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAFORMAT", cZoomOnZoom="GSVA_AFO", oKey_1_1="FOCODSTR", oKey_1_2="this.w_CODSTR", oKey_2_1="FOCODICE", oKey_2_2="this.w_CODFORFI"

  func oCODFORFI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFORFI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFORFI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAFORMAT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_CODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStr(this.Parent.oContained.w_CODSTR)
    endif
    do cp_zoom with 'VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(this.parent,'oCODFORFI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AFO',"Formati",'',this.parent.oContained
  endproc
  proc oCODFORFI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AFO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FOCODSTR=w_CODSTR
     i_obj.w_FOCODICE=this.parent.oContained.w_CODFORFI
     i_obj.ecpSave()
  endproc


  add object oObj_1_4 as cp_outputCombo with uid="ZLERXIVPWT",left=149, top=109, width=436,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 130968346


  add object oBtn_1_5 as StdButton with uid="CCGSUHUDGX",left=490, top=139, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 227925478;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="YXYHLPQOEL",left=542, top=139, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 170795002;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI_1_8 as StdField with uid="SHOKQHXTJZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione formato",;
    HelpContextID = 67238090,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=250, Top=44, InputMask=replicate('X',20)

  add object oDESCRI1_1_10 as StdField with uid="TADAZGBISZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione formato",;
    HelpContextID = 67238090,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=250, Top=70, InputMask=replicate('X',20)

  add object oSTDESCRI_1_12 as StdField with uid="IMYCHUGEZP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 166779025,;
   bGlobalFont=.t.,;
    Height=21, Width=340, Left=250, Top=12, InputMask=replicate('X',30)

  add object oStr_1_7 as StdString with uid="KURZTBJWHQ",Visible=.t., Left=10, Top=109,;
    Alignment=1, Width=136, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="UNSBIVVYHC",Visible=.t., Left=10, Top=47,;
    Alignment=1, Width=136, Height=18,;
    Caption="Da formato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="UCFVSZFVJQ",Visible=.t., Left=10, Top=73,;
    Alignment=1, Width=136, Height=18,;
    Caption="A formato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DWBMAYFWED",Visible=.t., Left=10, Top=15,;
    Alignment=1, Width=136, Height=18,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_sfo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
