* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bco                                                        *
*              Import/Export configurazioni piani di spedizione                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-05                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pCURNAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bco",oParentObject,m.pOPER,m.pCURNAME)
return(i_retval)

define class tgsva_bco as StdBatch
  * --- Local variables
  pOPER = space(10)
  pCURNAME = space(10)
  w_CPSERIAL = space(10)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CPDESCRI = space(30)
  w_CPCODCAU = space(5)
  w_CPFLPRED = space(1)
  w_CPNOMCAM = space(46)
  w_CPFLROTT = space(1)
  w_CPFLORDI = space(1)
  w_OLDSERIA = space(10)
  w_NEWSERIA = space(10)
  w_OKWRITE = .f.
  w_NOMESSAG = .f.
  w_APSERIAL = space(10)
  * --- WorkFile variables
  VACONPIA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER == "IMPORT"
        this.w_NOMESSAG = .F.
        this.w_NEWSERIA = SPACE(10)
        SELECT (this.pCURNAME)
        GO TOP
        SCAN
        this.w_CPSERIAL = CPSERIAL
        this.w_CPDESCRI = CPDESCRI
        this.w_CPCODCAU = CPCODCAU
        this.w_CPFLPRED = CPFLPRED
        this.w_CPNOMCAM = CPNOMCAM
        this.w_CPFLROTT = CPFLROTT
        this.w_CPFLORDI = CPFLORDI
        if EMPTY(this.w_OLDSERIA) OR this.w_OLDSERIA<>this.w_CPSERIAL
          this.w_OLDSERIA = SPACE(10)
          this.w_OKWRITE = .T.
          this.w_CPROWNUM = 0
          this.w_NEWSERIA = SPACE(10)
          i_nConn=i_TableProp[this.VACONPIA_IDX, 3]
          cp_NextTableProg(this,i_nConn,"PRPSP","i_codazi,w_NEWSERIA")
          if this.w_CPFLPRED = "S"
            this.w_APSERIAL = this.w_CPSERIAL
            this.w_CPSERIAL = this.w_NEWSERIA
            * --- Select from gsva_bcp
            do vq_exec with 'gsva_bcp',this,'_Curs_gsva_bcp','',.f.,.t.
            if used('_Curs_gsva_bcp')
              select _Curs_gsva_bcp
              locate for 1=1
              do while not(eof())
              this.w_OKWRITE = NVL(_Curs_gsva_bcp.CONTA, 0 ) = 0
                select _Curs_gsva_bcp
                continue
              enddo
              use
            endif
            this.w_CPSERIAL = this.w_APSERIAL
          endif
        endif
        if !this.w_OKWRITE
          this.w_CPFLPRED = "N"
          if !this.w_NOMESSAG
            ah_ErrorMsg("Almeno una configurazione da importare con flag predefinita attivato in conflitto con le configurazioni gi� esistenti.%0Per le configurazioni importante in conflitto verr� disattivato il flag predefinita")
            this.w_NOMESSAG = .T.
          endif
        endif
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWNUM * 10
        * --- Insert into VACONPIA
        i_nConn=i_TableProp[this.VACONPIA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VACONPIA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VACONPIA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CPSERIAL"+",CPDESCRI"+",CPCODCAU"+",CPFLPRED"+",CPROWNUM"+",CPROWORD"+",CPNOMCAM"+",CPFLROTT"+",CPFLORDI"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWSERIA),'VACONPIA','CPSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPDESCRI),'VACONPIA','CPDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPCODCAU),'VACONPIA','CPCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPFLPRED),'VACONPIA','CPFLPRED');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'VACONPIA','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'VACONPIA','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPNOMCAM),'VACONPIA','CPNOMCAM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPFLROTT),'VACONPIA','CPFLROTT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPFLORDI),'VACONPIA','CPFLORDI');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CPSERIAL',this.w_NEWSERIA,'CPDESCRI',this.w_CPDESCRI,'CPCODCAU',this.w_CPCODCAU,'CPFLPRED',this.w_CPFLPRED,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'CPNOMCAM',this.w_CPNOMCAM,'CPFLROTT',this.w_CPFLROTT,'CPFLORDI',this.w_CPFLORDI)
          insert into (i_cTable) (CPSERIAL,CPDESCRI,CPCODCAU,CPFLPRED,CPROWNUM,CPROWORD,CPNOMCAM,CPFLROTT,CPFLORDI &i_ccchkf. );
             values (;
               this.w_NEWSERIA;
               ,this.w_CPDESCRI;
               ,this.w_CPCODCAU;
               ,this.w_CPFLPRED;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_CPNOMCAM;
               ,this.w_CPFLROTT;
               ,this.w_CPFLORDI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_OLDSERIA = this.w_CPSERIAL
        SELECT (this.pCURNAME)
        ENDSCAN
      case this.pOPER == "EXPORT"
        vq_exec("..\vefa\exe\query\gsva_bco.vqr", this, this.pCURNAME )
    endcase
  endproc


  proc Init(oParentObject,pOPER,pCURNAME)
    this.pOPER=pOPER
    this.pCURNAME=pCURNAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VACONPIA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gsva_bcp')
      use in _Curs_gsva_bcp
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pCURNAME"
endproc
