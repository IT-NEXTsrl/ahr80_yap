* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bft                                                        *
*              GENERAZIONE TABELLA PIATTA                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-04-27                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pTABLE,pNOWARN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bft",oParentObject,m.pOPER,m.pTABLE,m.pNOWARN)
return(i_retval)

define class tgsva_bft as StdBatch
  * --- Local variables
  pOPER = space(5)
  pTABLE = space(2)
  pNOWARN = .f.
  w_TABATT = space(10)
  w_ARCHATT = space(30)
  w_CHKTABRES = 0
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non vengono segnalati errori non bloccanti
    do case
      case this.pOPER=="SAVE"
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        do case
          case this.pTABLE=="FT"
            SELECT (this.oParentObject.w_TABELLE.cCursor)
          case this.pTABLE=="ET"
            SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
          otherwise
            SELECT TABELLE
        endcase
        GO TOP
        do while !EOF()
          if  XCHK=1
            this.w_TABATT = IIF(FTFL_AZI="S","xxx","")+ALLTRIM(FTCODICE)
            if this.pTABLE=="ET" OR this.pTABLE=="EC"
              this.w_ARCHATT = ALLTRIM(FTTABNAM)
            endif
            do case
              case this.pTABLE=="FT" OR this.pTABLE=="FC"
                this.w_CHKTABRES = AdminFlatTable(this.w_TABATT)
              case this.pTABLE=="ET" OR this.pTABLE=="EC"
                this.w_CHKTABRES = AdminEDITable(this.w_TABATT, this.w_ARCHATT)
            endcase
            do case
              case this.w_CHKTABRES<0
                this.w_oERRORLOG.AddMsgLog("Errore durante la manutenzione della tabella %1",ALLTRIM(cp_SetAzi(this.w_TABATT)))     
              case this.w_CHKTABRES=0 and Not this.pNOWARN
                this.w_oERRORLOG.AddMsgLog("La tabella %1 non necessita di manutenzione",ALLTRIM(cp_SetAzi(this.w_TABATT)))     
            endcase
          endif
          do case
            case this.pTABLE=="FT"
              SELECT (this.oParentObject.w_TABELLE.cCursor)
            case this.pTABLE=="ET"
              SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
            otherwise
              SELECT TABELLE
          endcase
          SKIP
        enddo
        if this.w_oERRORLOG.IsFullLog()
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di manutenzione tabelle")     
        else
          if this.pTABLE=="FT" OR this.pTABLE=="ET"
            ah_ErrorMsg("Operazione terminata con successo",64)
          endif
        endif
        if this.pTABLE=="FT" OR this.pTABLE=="ET"
          this.oParentObject.NotifyEvent("Verifica")
        endif
      case this.pOPER=="VERIF"
        do case
          case this.pTABLE=="FT"
            SELECT (this.oParentObject.w_TABELLE.cCursor)
          case this.pTABLE=="ET"
            SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
        endcase
        GO TOP
        do while !EOF()
          do case
            case this.pTABLE=="FT"
              SELECT (this.oParentObject.w_TABELLE.cCursor)
            case this.pTABLE=="ET"
              SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
          endcase
          this.w_CHKTABRES = -1000
          this.w_TABATT = IIF(FTFL_AZI="S","xxx","")+ALLTRIM(FTCODICE)
          if this.pTABLE=="ET"
            this.w_ARCHATT = ALLTRIM(FTTABNAM)
          endif
          do case
            case this.pTABLE=="FT"
              this.w_CHKTABRES = ChkFTStatus(this.w_TABATT)
            case this.pTABLE=="ET"
              this.w_CHKTABRES = ChkETStatus(this.w_TABATT, this.w_ARCHATT)
          endcase
          do case
            case this.pTABLE=="FT"
              SELECT (this.oParentObject.w_TABELLE.cCursor)
            case this.pTABLE=="ET"
              SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
          endcase
          do case
            case this.w_CHKTABRES=0
              REPLACE FTBCKCOL WITH this.oParentObject.w_COLTABOK, FTSTATUS WITH "A", XCHK WITH 0
            case this.w_CHKTABRES=-1
              REPLACE FTBCKCOL WITH this.oParentObject.w_COLTABCR, FTSTATUS WITH "C", XCHK WITH 1
            case this.w_CHKTABRES=1
              REPLACE FTBCKCOL WITH this.oParentObject.w_COLTABAG, FTSTATUS WITH "D", XCHK WITH 1
            otherwise
              REPLACE FTBCKCOL WITH RGB(0,0,200), FTSTATUS WITH "E", XCHK WITH 0
          endcase
          do case
            case this.pTABLE=="FT"
              SELECT (this.oParentObject.w_TABELLE.cCursor)
            case this.pTABLE=="ET"
              SELECT (this.oParentObject.w_TABELLE_EDI.cCursor)
          endcase
          SKIP
        enddo
      case this.pOPER=="SELEZ" or this.pOPER=="DESEL" Or this.pOPER=="INVSEL"
        do case
          case this.pTABLE=="FT"
            UPDATE (this.oParentObject.w_TABELLE.cCursor) SET XCHK=ICASE(this.pOPER=="SELEZ", 1, this.pOPER=="DESEL", 0 , IIF(XCHK=0,1,0))
          case this.pTABLE=="ET"
            UPDATE (this.oParentObject.w_TABELLE_EDI.cCursor) SET XCHK=ICASE(this.pOPER=="SELEZ", 1, this.pOPER=="DESEL", 0 , IIF(XCHK=0,1,0))
        endcase
    endcase
  endproc


  proc Init(oParentObject,pOPER,pTABLE,pNOWARN)
    this.pOPER=pOPER
    this.pTABLE=pTABLE
    this.pNOWARN=pNOWARN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pTABLE,pNOWARN"
endproc
