* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bi3                                                        *
*              LANCIA MASCHERA DI IMPORTAZIONE EDI                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-30                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pCODSTR,pNOMSK
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bi3",oParentObject,m.pSERIAL,m.pCODSTR,m.pNOMSK)
return(i_retval)

define class tgsva_bi3 as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pCODSTR = space(10)
  pNOMSK = .f.
  w_SER_ELAB = space(10)
  w_STCODICE = space(10)
  w_COD_TMP = space(10)
  w_EDI_TAB = space(10)
  w_Risultato = space(254)
  w_FLATAB = space(30)
  w_PROG = .NULL.
  w_CODTAB = space(30)
  w_DESSTR = space(30)
  * --- WorkFile variables
  TMPLIST_idx=0
  VASTRUTT_idx=0
  FLATDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo tabelle temporanee
    if Vartype(this.pCODSTR)<>"C"
      this.pCODSTR = CERSTRUPRE("1 ASC")
    endif
    if Vartype(this.pSERIAL)<>"C"
      this.pSERIAL = ""
    endif
    if Empty(this.pCODSTR)
      ah_ErrorMsg("Attenzione nessuna struttura predefinita")
      i_retcode = 'stop'
      return
    endif
    this.w_STCODICE = this.pCODSTR
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STFLATAB,STDESCRI"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.w_STCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STFLATAB,STDESCRI;
        from (i_cTable) where;
            STCODICE = this.w_STCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      this.w_DESSTR = NVL(cp_ToDate(_read_.STDESCRI),cp_NullValue(_read_.STDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_SER_ELAB = this.pSERIAL
    this.w_CODTAB = "DOC_MAST"
    this.w_COD_TMP = "TMPVFDOCMAST"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODTAB = "DOC_DETT"
    this.w_COD_TMP = "TMPVFDOCDETT"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODTAB = "DOC_RATE"
    this.w_COD_TMP = "TMPVFDOCRATE"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODTAB = "MOVIMATR"
    this.w_COD_TMP = "TMPVFMOVIMATR"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODTAB = "MOVILOTT"
    this.w_COD_TMP = "TMPVFMOVILOTT"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODTAB = "ABBLOTUL"
    this.w_COD_TMP = "TMPVFABBLOTUL"
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Create temporary table TMPLIST
    i_nIdx=cp_AddTableDef('TMPLIST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\VEFA\EXE\QUERY\GSVALBIM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPLIST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if Not this.pNOMSK
      if Type("oparentobject")="O" and Upper(oparentobject.class)="TGSVA_BWI"
        this.w_PROG = GSVA_KIM("NOSER")
        this.w_PROG.w_FASE2 = .T.
      else
        this.w_PROG = GSVA_KIM()
      endif
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.w_SER_ELAB = this.w_SER_ELAB
      this.w_PROG.w_CODSTR = this.w_STCODICE
      this.w_PROG.w_FLATAB = alltrim(this.w_FLATAB)
      this.w_PROG.w_FASE = .T.
      this.w_PROG.w_STDES = this.w_DESSTR
      this.w_PROG.mCalc(.T.)     
      CTRL_CONCON= this.w_PROG.GetCtrl("w_CAMPO") 
 CTRL_CONCON.Popola()
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_EDI_TAB = GetETPhname(Alltrim(this.w_FLATAB), this.w_CODTAB)
    if ChketStatus(Alltrim(this.w_FLATAB), this.w_CODTAB)=0
       
 CURS=READTABLE(this.w_EDI_TAB,"*","","",.F.,"FT___UID='"+this.w_SER_ELAB+"'")
      this.w_Risultato = CurToTab(Curs, this.w_COD_TMP)
       
 use in (curs)
    else
      if this.w_CODTAB="DOC_MAST"
        Ah_ErrorMsg("Attenzione tabella %1,non definita nella struttura %2","","",this.w_CODTAB,this.pCODSTR)
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pCODSTR,pNOMSK)
    this.pSERIAL=pSERIAL
    this.pCODSTR=pCODSTR
    this.pNOMSK=pNOMSK
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='*TMPLIST'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='FLATDETT'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pCODSTR,pNOMSK"
endproc
