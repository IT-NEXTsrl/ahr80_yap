* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kee                                                        *
*              Import/export definizione tabelle piatte                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-02                                                      *
* Last revis.: 2009-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kee",oParentObject))

* --- Class definition
define class tgsva_kee as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 619
  Height = 522+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-30"
  HelpContextID=65696361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsva_kee"
  cComment = "Import/export definizione tabelle piatte"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_RADSELIE1 = space(10)
  o_RADSELIE1 = space(10)
  w_DBF1 = space(200)
  w_DBF11 = space(200)
  w_FLDELDEF = .F.
  w_MSG = space(0)
  w_FLVERBOS = .F.
  w_EDITABLE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_keePag1","gsva_kee",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tabelle")
      .Pages(2).addobject("oPag","tgsva_keePag2","gsva_kee",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRADSELIE1_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_EDITABLE = this.oPgFrm.Pages(1).oPag.EDITABLE
    DoDefault()
    proc Destroy()
      this.w_EDITABLE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RADSELIE1=space(10)
      .w_DBF1=space(200)
      .w_DBF11=space(200)
      .w_FLDELDEF=.f.
      .w_MSG=space(0)
      .w_FLVERBOS=.f.
        .w_RADSELIE1 = 'E'
      .oPgFrm.Page1.oPag.EDITABLE.Calculate()
        .w_DBF1 = "..\VEFA\EXE\FLATMAST.DBF"
        .w_DBF11 = "..\VEFA\EXE\FLATDETT.DBF"
        .w_FLDELDEF = IIF(.w_RADSELIE1='E', .F. , .w_FLDELDEF)
          .DoRTCalc(5,5,.f.)
        .w_FLVERBOS = .F.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.EDITABLE.Calculate()
        .DoRTCalc(1,3,.t.)
        if .o_RADSELIE1<>.w_RADSELIE1
            .w_FLDELDEF = IIF(.w_RADSELIE1='E', .F. , .w_FLDELDEF)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.EDITABLE.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLDELDEF_1_15.enabled = this.oPgFrm.Page1.oPag.oFLDELDEF_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.EDITABLE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADSELIE1_1_6.RadioValue()==this.w_RADSELIE1)
      this.oPgFrm.Page1.oPag.oRADSELIE1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF1_1_9.value==this.w_DBF1)
      this.oPgFrm.Page1.oPag.oDBF1_1_9.value=this.w_DBF1
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF11_1_13.value==this.w_DBF11)
      this.oPgFrm.Page1.oPag.oDBF11_1_13.value=this.w_DBF11
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDELDEF_1_15.RadioValue()==this.w_FLDELDEF)
      this.oPgFrm.Page1.oPag.oFLDELDEF_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVERBOS_2_2.RadioValue()==this.w_FLVERBOS)
      this.oPgFrm.Page2.oPag.oFLVERBOS_2_2.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBF1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF1_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DBF1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DBF11))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF11_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DBF11)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RADSELIE1 = this.w_RADSELIE1
    return

enddefine

* --- Define pages as container
define class tgsva_keePag1 as StdContainer
  Width  = 615
  height = 522
  stdWidth  = 615
  stdheight = 522
  resizeXpos=396
  resizeYpos=214
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="CZRHGAVNYF",left=501, top=470, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65368234;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        GSVA_BEE(this.Parent.oContained,"ELABO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_RADSELIE1))
      endwith
    endif
  endfunc


  add object oBtn_1_2 as StdButton with uid="NYQDDFCUPJ",left=553, top=470, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 2444873;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="LUHNMBSWMQ",left=10, top=471, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti gli archivi da importare/esportare";
    , HelpContextID = 232671194;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSVA_BEE(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="ABJFIATDMP",left=61, top=471, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti gli archivi da importare/esportare";
    , HelpContextID = 232671194;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSVA_BEE(this.Parent.oContained,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="PDWIPENRCZ",left=112, top=471, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione degli archivi da importare/esportare";
    , HelpContextID = 232671194;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSVA_BEE(this.Parent.oContained,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRADSELIE1_1_6 as StdRadio with uid="OKXXQIMNCR",rtseq=1,rtrep=.f.,left=139, top=16, width=415,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELIE1_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file DBF"
      this.Buttons(1).HelpContextID = 54716565
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file DBF"
      this.Buttons(2).HelpContextID = 54716565
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE1_1_6.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(10))))
  endfunc
  func oRADSELIE1_1_6.GetRadio()
    this.Parent.oContained.w_RADSELIE1 = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE1_1_6.SetRadio()
    this.Parent.oContained.w_RADSELIE1=trim(this.Parent.oContained.w_RADSELIE1)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE1=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE1=='E',2,;
      0))
  endfunc


  add object EDITABLE as cp_szoombox with uid="EEMQECTAEO",left=4, top=140, width=606,height=321,;
    caption='EDITABLE',;
   bGlobalFont=.t.,;
    cZoomFile="default",bOptions=.f.,cTable="FLATMAST",bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",bAdvOptions=.t.,cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 226597237

  add object oDBF1_1_9 as StdField with uid="ZPJIIFTPAA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DBF1", cQueryName = "DBF1",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 62180298,;
   bGlobalFont=.t.,;
    Height=21, Width=418, Left=159, Top=46, InputMask=replicate('X',200)


  add object oBtn_1_10 as StdButton with uid="MKUMAQUBOB",left=582, top=47, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65495338;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_DBF1=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDBF11_1_13 as StdField with uid="CZIBBZGIKR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DBF11", cQueryName = "DBF11",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 10800074,;
   bGlobalFont=.t.,;
    Height=21, Width=418, Left=159, Top=74, InputMask=replicate('X',200)


  add object oBtn_1_14 as StdButton with uid="GJSGDJJLXS",left=582, top=77, width=18,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 65495338;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .w_DBF11=left(getfile("dbf")+space(50),50)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLDELDEF_1_15 as StdCheck with uid="PCAJTEIWUE",rtseq=4,rtrep=.f.,left=160, top=96, caption="Sovrascrivi dati esistenti",;
    HelpContextID = 85925532,;
    cFormVar="w_FLDELDEF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDELDEF_1_15.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLDELDEF_1_15.GetRadio()
    this.Parent.oContained.w_FLDELDEF = this.RadioValue()
    return .t.
  endfunc

  func oFLDELDEF_1_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLDELDEF==.T.,1,;
      0)
  endfunc

  func oFLDELDEF_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RADSELIE1 = 'I')
    endwith
   endif
  endfunc

  add object oStr_1_7 as StdString with uid="HKXMTIVULE",Visible=.t., Left=10, Top=120,;
    Alignment=0, Width=541, Height=18,;
    Caption="Selezione archivi da esportare"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="VYLOOFYPCC",Visible=.t., Left=7, Top=48,;
    Alignment=1, Width=149, Height=18,;
    Caption="File esportazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='I')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="KCUJUSACJJ",Visible=.t., Left=7, Top=48,;
    Alignment=1, Width=149, Height=18,;
    Caption="File importazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE1='E')
    endwith
  endfunc
enddefine
define class tgsva_keePag2 as StdContainer
  Width  = 615
  height = 522
  stdWidth  = 615
  stdheight = 522
  resizeXpos=474
  resizeYpos=345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="QVQLZYFOGE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65382970,;
   bGlobalFont=.t.,;
    Height=481, Width=603, Left=6, Top=12, Readonly=.T.

  add object oFLVERBOS_2_2 as StdCheck with uid="GJREOHVBHZ",rtseq=6,rtrep=.f.,left=8, top=496, caption="Produci log dettagliato",;
    HelpContextID = 209699159,;
    cFormVar="w_FLVERBOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLVERBOS_2_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLVERBOS_2_2.GetRadio()
    this.Parent.oContained.w_FLVERBOS = this.RadioValue()
    return .t.
  endfunc

  func oFLVERBOS_2_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLVERBOS==.T.,1,;
      0)
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kee','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
