* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bdk                                                        *
*              CONTROLLI ELEMENTI                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-21                                                      *
* Last revis.: 2015-08-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bdk",oParentObject,m.pEXEC)
return(i_retval)

define class tgsva_bdk as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OK = .f.
  w_OBJ = .NULL.
  * --- WorkFile variables
  VAELEMEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OK = .T.
    this.oParentObject.w_MESS = ah_Msgformat("Operazione abbandonata")
    if this.pEXEC="Load"
      do case
        case this.oParentObject.w_FLPROV<>"S"
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, struttura confermata impossibile confermare")
          this.w_OK = .F.
        case this.oParentObject.w_STFLTYPE<>"I" AND this.oParentObject.w_EL__TIPO="D" AND Empty(this.oParentObject.w_ELCODTAB)
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, elemento reiterabile, inserire tabella di export")
          this.w_OK = .F.
        case this.oParentObject.w_STFLTYPE<>"O" AND (Not empty(this.oParentObject.w_ELVALTXT) and this.oParentObject.w_TIPFIL="E" AND EMPTY(this.oParentObject.w_ELTABIMP) AND NoT this.oParentObject.w_EL__TIPO $ "C-A")
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, elemento con id primario, inserire tabella di import")
          this.w_OBJ = This.oparentobject.getctrl("w_ELTABIMP")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        case this.oParentObject.w_STFLTYPE<>"O" AND (Not empty(this.oParentObject.w_ELVALTXT) and this.oParentObject.w_TIPFIL="E" AND this.oParentObject.w_ELORDASS=0 AND NoT this.oParentObject.w_EL__TIPO $ "C-A") and this.oParentObject.w_STFLTYPE $ "I-E"
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, elemento con id primario, inserire ordinamento assoluto")
          this.w_OBJ = This.oparentobject.getctrl("w_ELORDASS")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        case this.oParentObject.w_STFLTYPE<>"I" AND ( this.oParentObject.w_EL__TIPO="V" AND Empty(this.oParentObject.w_TESTXT) and Empty(this.oParentObject.w_ELCODTAB))
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, elemento di tipo valore, inserire tabella export o elemento txt")
          this.w_OK = .F.
        case ((this.oParentObject.w_TIPFIL="L" AND Empty(this.oParentObject.w_ELVALTXT)) or this.oParentObject.w_TIPFIL<> "L" ) and this.oParentObject.w_ELLUNMAX=0 and this.oParentObject.w_EL__TIPO $ "E-V" 
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, inserire lunghezza massima del campo\espressione")
          this.w_OBJ = This.oparentobject.getctrl("w_ELLUNMAX")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        case this.oParentObject.w_TIPO="S"
          * --- Select from VAELEMEN
          i_nConn=i_TableProp[this.VAELEMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" VAELEMEN ";
                +" where ELCODSTR="+cp_ToStrODBC(this.oParentObject.w_ELCODSTR)+" AND ELCODICE<>"+cp_ToStrODBC(this.oParentObject.w_ELCODICE)+" AND ELIDCHLD="+cp_ToStrODBC(this.oParentObject.w_ELIDCHLD)+" AND ELCODPAD="+cp_ToStrODBC(this.oParentObject.w_ELCODPAD)+"";
                 ,"_Curs_VAELEMEN")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where ELCODSTR=this.oParentObject.w_ELCODSTR AND ELCODICE<>this.oParentObject.w_ELCODICE AND ELIDCHLD=this.oParentObject.w_ELIDCHLD AND ELCODPAD=this.oParentObject.w_ELCODPAD;
              into cursor _Curs_VAELEMEN
          endif
          if used('_Curs_VAELEMEN')
            select _Curs_VAELEMEN
            locate for 1=1
            do while not(eof())
            if _Curs_VAELEMEN.CONTA >0
              this.oParentObject.w_MESS = ah_Msgformat("Attenzione, � gi� presente un elemento con lo stesso identificativo numerico")
              this.w_OK = .F.
            endif
            exit
              select _Curs_VAELEMEN
              continue
            enddo
            use
          endif
      endcase
    endif
    if this.w_OK AND this.oParentObject.w_TIPFIL<>"X" AND NOT EMPTY(this.oParentObject.w_ELVALTXT)
      if this.oParentObject.w_STFLTYPE $ "I-E"
        if this.w_OK AND EMPTY(this.oParentObject.w_ELTABIMP)
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, tabella di import non valorizzata")
          this.w_OBJ = This.oparentobject.getctrl("w_ELTABIMP")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        endif
        if this.w_OK AND EMPTY(this.oParentObject.w_ELORDASS) and this.oParentObject.w_STFLTYPE $ "I-E"
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, occorre indicare l'ordine assoluto")
          this.w_OBJ = This.oparentobject.getctrl("w_ELORDASS")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        endif
      endif
      if this.oParentObject.w_STFLTYPE $ "O-E"
        if this.w_OK AND EMPTY(this.oParentObject.w_ELCODTAB)
          this.oParentObject.w_MESS = ah_Msgformat("Attenzione, tabella di export non valorizzata")
          this.w_OBJ = This.oparentobject.getctrl("w_ELCODTAB")
          this.w_OBJ.setfocus()     
          this.w_OK = .F.
        endif
      endif
    endif
    if this.w_OK AND this.oParentObject.w_ELTIPCAM $ "D-N" AND Empty(this.oParentObject.w_EL__MASK)
      if Ah_Yesno("Attenzione, elemento di tipo data o numerico. Non � stata indicata la maschera!%0Procedo ugualmente?")
        this.w_OK = .T.
      else
        this.w_OK = .F.
      endif
    endif
    * --- Se .F. operazione non possibile...
    this.w_OBJ = .Null.
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VAELEMEN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_VAELEMEN')
      use in _Curs_VAELEMEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
