* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bdi                                                        *
*              CANCELLAZIONE ANAGRAFICA IMPORTAZIONI                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-12                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bdi",oParentObject,m.pOPER)
return(i_retval)

define class tgsva_bdi as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_PADRE = .NULL.
  w_IMSERIAL = space(10)
  w_IMPATORI = space(254)
  w_IMPATXML = space(254)
  w_ELABOK = .f.
  w_oERRORLOG = .NULL.
  w_IMSERGEN = space(10)
  w_IMCODSTR = space(10)
  w_SERIAL = space(10)
  w_PROG = .NULL.
  w_CLADOC = space(2)
  w_LOG = space(0)
  w_AGGLOTUBI = space(1)
  w_AGGCOMME = space(1)
  w_FLCP = space(1)
  w_FLCAU = space(1)
  w_GGINTERV = 0
  w_NUMDOC = 0
  w_INITIME = space(10)
  w_ENDTIME = space(10)
  w_GGINTERV = 0
  w_NUMDOC = 0
  OldNumGG = 0
  OldNumDoc = 0
  w_FLATAB = space(10)
  w_PHNAME = space(30)
  w_CODTAB = space(30)
  w_FILTER = space(30)
  * --- WorkFile variables
  VAANAIMP_idx=0
  DOC_MAST_idx=0
  LDOCGENE_idx=0
  VASTRUTT_idx=0
  FLATDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = This.oParentObject
    do case
      case this.pOPER=="SELEZ" Or this.pOPER=="DESEL" Or this.pOPER=="INVSE"
        UPDATE (this.oParentObject.w_IMPSEL.cCursor) SET XCHK=ICASE(this.pOPER=="SELEZ", 1, this.pOPER=="DESEL", 0, IIF(XCHK=0,1,0))
      case this.pOPER=="CANC"
        if ah_YesNo("Confermi l'eliminazione delle importazioni selezionate?")
          this.oParentObject.w_MSG = ""
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          this.oParentObject.oPGFRM.ActivePage=2
          addMsgNL("Inizio cancellazione importazioni %1" ,This,TTOC(DATETIME()))
          SELECT IMSERIAL,IMCODSTR FROM (this.oParentObject.w_IMPSEL.cCursor) WHERE XCHK=1 INTO CURSOR "_Elim_"
          if USED("_Elim_")
            if RECCOUNT("_Elim_")>0
              SELECT "_Elim_"
              GO TOP
              SCAN
              this.w_ELABOK = .T.
              this.w_IMSERIAL = _Elim_.IMSERIAL
              this.w_IMCODSTR = _Elim_.IMCODSTR
              * --- Read from VAANAIMP
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VAANAIMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IMPATORI,IMPATXML,IMSERGEN"+;
                  " from "+i_cTable+" VAANAIMP where ";
                      +"IMSERIAL = "+cp_ToStrODBC(this.w_IMSERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IMPATORI,IMPATXML,IMSERGEN;
                  from (i_cTable) where;
                      IMSERIAL = this.w_IMSERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_IMPATORI = NVL(cp_ToDate(_read_.IMPATORI),cp_NullValue(_read_.IMPATORI))
                this.w_IMPATXML = NVL(cp_ToDate(_read_.IMPATXML),cp_NullValue(_read_.IMPATXML))
                this.w_IMSERGEN = NVL(cp_ToDate(_read_.IMSERGEN),cp_NullValue(_read_.IMSERGEN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              addMsgNL("Inizio eliminazione importazione %1" ,This,ALLTRIM(this.w_IMSERIAL) )
              addMsgNL(CHR(9)+"Verifica presenza file origine %1" ,This,ALLTRIM(this.w_IMPATORI) )
              if this.oParentObject.w_FLDELFIL $ "OE" AND cp_FileExist(this.w_IMPATORI)
                addMsgNL(CHR(9)+"Eliminazione file origine %1" ,This,ALLTRIM(this.w_IMPATORI) )
                L_OLDERROR=On("Error") 
                L_Err=.F.
                On Error L_Err=.T.
                DELETE FILE (this.w_IMPATORI)
                this.w_ELABOK = !L_Err
                on error &L_OldError
              endif
              if this.w_ELABOK
                addMsgNL(CHR(9)+"Verifica presenza file XML intermedio %1" ,This,ALLTRIM(this.w_IMPATXML) )
                if this.oParentObject.w_FLDELFIL $ "XE" AND cp_FileExist(this.w_IMPATXML)
                  addMsgNL(CHR(9)+"Eliminazione file XML intermedio %1" ,This,ALLTRIM(this.w_IMPATXML) )
                  L_OLDERROR=On("Error") 
                  L_Err=.F.
                  On Error L_Err=.T.
                  DELETE FILE (this.w_IMPATXML)
                  this.w_ELABOK = !L_Err
                  on error &L_OldError
                endif
              else
                addMsgNL(CHR(9)+"Errore eliminazione file origine %1" ,This,ALLTRIM(this.w_IMPATORI) )
              endif
              if this.w_ELABOK and this.oParentObject.w_FLELDOC="S"
                * --- Try
                local bErr_0359F770
                bErr_0359F770=bTrsErr
                this.Try_0359F770()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  addMsgNL(CHR(9)+"Errore eliminazione documenti %1" ,This,ALLTRIM(this.w_IMSERIAL) )
                  this.w_ELABOK = .F.
                endif
                bTrsErr=bTrsErr or bErr_0359F770
                * --- End
              else
                addMsgNL(CHR(9)+"Errore eliminazione documenti %1" ,This,ALLTRIM(this.w_IMSERIAL) )
              endif
              this.w_SERIAL = Space(10)
              * --- Select from GSVA1BDI
              do vq_exec with 'GSVA1BDI',this,'_Curs_GSVA1BDI','',.f.,.t.
              if used('_Curs_GSVA1BDI')
                select _Curs_GSVA1BDI
                locate for 1=1
                do while not(eof())
                this.w_SERIAL = Nvl(_Curs_GSVA1BDI.MVSERIAL,Space(10))
                exit
                  select _Curs_GSVA1BDI
                  continue
                enddo
                use
              endif
              if Not EMpty(this.w_IMCODSTR)
                * --- Read from VASTRUTT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VASTRUTT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "STFLATAB"+;
                    " from "+i_cTable+" VASTRUTT where ";
                        +"STCODICE = "+cp_ToStrODBC(this.w_IMCODSTR);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    STFLATAB;
                    from (i_cTable) where;
                        STCODICE = this.w_IMCODSTR;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Select from FLATDETT
                i_nConn=i_TableProp[this.FLATDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.FLATDETT_idx,2],.t.,this.FLATDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select FTCODICE,FTTABNAM  from "+i_cTable+" FLATDETT ";
                      +" where FTCODICE="+cp_ToStrODBC(this.w_FLATAB)+"";
                      +" group by FTCODICE,FTTABNAM";
                       ,"_Curs_FLATDETT")
                else
                  select FTCODICE,FTTABNAM from (i_cTable);
                   where FTCODICE=this.w_FLATAB;
                   group by FTCODICE,FTTABNAM;
                    into cursor _Curs_FLATDETT
                endif
                if used('_Curs_FLATDETT')
                  select _Curs_FLATDETT
                  locate for 1=1
                  do while not(eof())
                  this.w_CODTAB = _Curs_FLATDETT.FTTABNAM
                  this.w_PHNAME = GetETPhname(alltrim(this.w_FLATAB), this.w_CODTAB)
                  this.w_FILTER = "1=1"
                   
 CURS=READTABLE(this.w_PHNAME,"*",,,,this.w_FILTER)
                  if Reccount((CURS))>0
                     
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="FT___UID" 
 ArrVal[1,2]=this.w_IMSERIAL
                    DELETETABLE( this.w_PHNAME , @ArrVal , this.w_oERRORLOG ) 
                    addMsgNL(CHR(9)+"Eliminazione record tabella  %1" ,This,ALLTRIM(this.w_PHNAME) )
                  endif
                  * --- Chiudo cursore creato per eseguire la Read
                  if Used((CURS))
                     
 Select (CURS) 
 use
                  endif
                    select _Curs_FLATDETT
                    continue
                  enddo
                  use
                endif
              endif
              if this.w_ELABOK AND ((this.oParentObject.w_FLELDOC="S" AND Empty(this.w_SERIAL)) OR this.oParentObject.w_FLELDOC<>"S" )
                * --- Elimino anagrafica importazioni
                * --- Try
                local bErr_0367B550
                bErr_0367B550=bTrsErr
                this.Try_0367B550()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  addMsgNL(CHR(9)+"Errore eliminazione importazione %1" ,This,ALLTRIM(this.w_IMSERIAL) )
                endif
                bTrsErr=bTrsErr or bErr_0367B550
                * --- End
              else
                addMsgNL(CHR(9)+"Errore eliminazione documenti %1" ,This,ALLTRIM(this.w_IMPATXML) )
              endif
              SELECT "_Elim_"
              ENDSCAN
              if g_APPLICATION="ADHOC REVOLUTION" and this.w_oErrorLog.IsFullLog()
                this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
              endif
            else
              addMsgNL("Nessuna importazione selezionata" ,This)
            endif
            USE IN "_Elim_"
          else
            addMsgNL("Impossibile creare il cursore '_Elim_'" ,This)
          endif
          addMsgNL("Fine cancellazione importazioni %1" ,This,TTOC(DATETIME()))
          this.w_PADRE.NotifyEvent("Aggiorna")     
        else
          ah_Errormsg("Operazione interrotta come richiesto",64)
        endif
    endcase
  endproc
  proc Try_0359F770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from GSVA_BDI
    do vq_exec with 'GSVA_BDI',this,'_Curs_GSVA_BDI','',.f.,.t.
    if used('_Curs_GSVA_BDI')
      select _Curs_GSVA_BDI
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_GSVA_BDI.MVSERIAL
      * --- Verifico correttezza documento
      this.w_LOG = ""
      l_Prg = "gsar_bdk( .Null.,this.w_SERIAL )"
      this.w_LOG = &l_Prg
      this.w_LOG = IIF(Not Empty(g_MSG),g_MSG,this.w_LOG)
      if Not empty(this.w_LOG)
        this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(10)+Alltrim(this.w_LOG))     
      endif
        select _Curs_GSVA_BDI
        continue
      enddo
      use
    endif
    if Isahe()
      * --- Ricostruisco i saldi
      do Gsma_brs with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_0367B550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from VAANAIMP
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IMSERIAL = "+cp_ToStrODBC(this.w_IMSERIAL);
             )
    else
      delete from (i_cTable) where;
            IMSERIAL = this.w_IMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='VAANAIMP'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='LDOCGENE'
    this.cWorkTables[4]='VASTRUTT'
    this.cWorkTables[5]='FLATDETT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSVA_BDI')
      use in _Curs_GSVA_BDI
    endif
    if used('_Curs_GSVA1BDI')
      use in _Curs_GSVA1BDI
    endif
    if used('_Curs_FLATDETT')
      use in _Curs_FLATDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
