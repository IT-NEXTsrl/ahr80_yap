* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bix                                                        *
*              VALUTA NODO                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-03                                                      *
* Last revis.: 2006-08-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFILE,pNODO,pRICO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bix",oParentObject,m.pFILE,m.pNODO,m.pRICO)
return(i_retval)

define class tgsva_bix as StdBatch
  * --- Local variables
  pFILE = space(254)
  pNODO = space(254)
  pRICO = 0
  w_ErrCode = 0
  w_Risultato = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    gsut_bix(this,this.pFILE,this.pNODO,this.pRICO)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = this.w_Risultato
    return
  endproc


  proc Init(oParentObject,pFILE,pNODO,pRICO)
    this.pFILE=pFILE
    this.pNODO=pNODO
    this.pRICO=pRICO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFILE,pNODO,pRICO"
endproc
