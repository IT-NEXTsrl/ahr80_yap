* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kdi                                                        *
*              Cancellazione anagrafica importazioni                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-11                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kdi",oParentObject))

* --- Class definition
define class tgsva_kdi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 645
  Height = 489+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=82473577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  VAANAIMP_IDX = 0
  cPrg = "gsva_kdi"
  cComment = "Cancellazione anagrafica importazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AISERINI = space(10)
  o_AISERINI = space(10)
  w_AISERFIN = space(10)
  o_AISERFIN = space(10)
  w_AIDATINI = ctod('  /  /  ')
  o_AIDATINI = ctod('  /  /  ')
  w_AIDATFIN = ctod('  /  /  ')
  o_AIDATFIN = ctod('  /  /  ')
  w_FLDELFIL = space(1)
  w_MSG = space(0)
  w_FLELDOC = space(1)
  w_AIDATIMI = ctod('  /  /  ')
  w_AIDATIMF = ctod('  /  /  ')
  w_AIFLELAB = space(1)
  w_AIFLREEZ = space(1)
  w_IMPSEL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kdiPag1","gsva_kdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri cancellazione")
      .Pages(2).addobject("oPag","tgsva_kdiPag2","gsva_kdi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAISERINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_IMPSEL = this.oPgFrm.Pages(1).oPag.IMPSEL
    DoDefault()
    proc Destroy()
      this.w_IMPSEL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VAANAIMP'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AISERINI=space(10)
      .w_AISERFIN=space(10)
      .w_AIDATINI=ctod("  /  /  ")
      .w_AIDATFIN=ctod("  /  /  ")
      .w_FLDELFIL=space(1)
      .w_MSG=space(0)
      .w_FLELDOC=space(1)
      .w_AIDATIMI=ctod("  /  /  ")
      .w_AIDATIMF=ctod("  /  /  ")
      .w_AIFLELAB=space(1)
      .w_AIFLREEZ=space(1)
        .w_AISERINI = IIF(!EMPTY(.w_AIDATINI) OR !EMPTY(.w_AIDATFIN), SPACE(10), .w_AISERINI)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AISERINI))
          .link_1_3('Full')
        endif
        .w_AISERFIN = IIF(!EMPTY(.w_AIDATINI) OR !EMPTY(.w_AIDATFIN), SPACE(10), .w_AISERFIN)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AISERFIN))
          .link_1_4('Full')
        endif
        .w_AIDATINI = IIF(!EMPTY(.w_AISERINI) OR !EMPTY(.w_AISERFIN), CTOD('  -  -    '), .w_AIDATINI)
        .w_AIDATFIN = IIF(!EMPTY(.w_AISERINI) OR !EMPTY(.w_AISERFIN), CTOD('  -  -    '), .w_AIDATFIN)
        .w_FLDELFIL = 'N'
          .DoRTCalc(6,6,.f.)
        .w_FLELDOC = 'S'
      .oPgFrm.Page1.oPag.IMPSEL.Calculate()
          .DoRTCalc(8,9,.f.)
        .w_AIFLELAB = 'N'
        .w_AIFLREEZ = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_AIDATINI<>.w_AIDATINI.or. .o_AIDATFIN<>.w_AIDATFIN
            .w_AISERINI = IIF(!EMPTY(.w_AIDATINI) OR !EMPTY(.w_AIDATFIN), SPACE(10), .w_AISERINI)
          .link_1_3('Full')
        endif
        if .o_AIDATINI<>.w_AIDATINI.or. .o_AIDATFIN<>.w_AIDATFIN
            .w_AISERFIN = IIF(!EMPTY(.w_AIDATINI) OR !EMPTY(.w_AIDATFIN), SPACE(10), .w_AISERFIN)
          .link_1_4('Full')
        endif
        if .o_AISERINI<>.w_AISERINI.or. .o_AISERFIN<>.w_AISERFIN
            .w_AIDATINI = IIF(!EMPTY(.w_AISERINI) OR !EMPTY(.w_AISERFIN), CTOD('  -  -    '), .w_AIDATINI)
        endif
        if .o_AISERINI<>.w_AISERINI.or. .o_AISERFIN<>.w_AISERFIN
            .w_AIDATFIN = IIF(!EMPTY(.w_AISERINI) OR !EMPTY(.w_AISERFIN), CTOD('  -  -    '), .w_AIDATFIN)
        endif
        .oPgFrm.Page1.oPag.IMPSEL.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.IMPSEL.Calculate()
    endwith
  return

  proc Calculate_CVSOZSMQXJ()
    with this
          * --- Aggiorna xchk zoom
          gsva_bdi(this;
              ,'SELEZ';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAISERINI_1_3.enabled = this.oPgFrm.Page1.oPag.oAISERINI_1_3.mCond()
    this.oPgFrm.Page1.oPag.oAISERFIN_1_4.enabled = this.oPgFrm.Page1.oPag.oAISERFIN_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAIDATINI_1_7.enabled = this.oPgFrm.Page1.oPag.oAIDATINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oAIDATFIN_1_8.enabled = this.oPgFrm.Page1.oPag.oAIDATFIN_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.IMPSEL.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_CVSOZSMQXJ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AISERINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAANAIMP_IDX,3]
    i_lTable = "VAANAIMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2], .t., this.VAANAIMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AISERINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VAANAIMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMSERIAL like "+cp_ToStrODBC(trim(this.w_AISERINI)+"%");

          i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMSERIAL',trim(this.w_AISERINI))
          select IMSERIAL,IMDATIMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AISERINI)==trim(_Link_.IMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AISERINI) and !this.bDontReportError
            deferred_cp_zoom('VAANAIMP','*','IMSERIAL',cp_AbsName(oSource.parent,'oAISERINI_1_3'),i_cWhere,'',"Anagrafica importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                     +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',oSource.xKey(1))
            select IMSERIAL,IMDATIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AISERINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                   +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(this.w_AISERINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',this.w_AISERINI)
            select IMSERIAL,IMDATIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AISERINI = NVL(_Link_.IMSERIAL,space(10))
      this.w_AIDATIMI = NVL(cp_ToDate(_Link_.IMDATIMP),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AISERINI = space(10)
      endif
      this.w_AIDATIMI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_AISERFIN) OR .w_AISERFIN>=.w_AISERINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Seriale inesistente o seriale iniziale maggiore del seriale finale")
        endif
        this.w_AISERINI = space(10)
        this.w_AIDATIMI = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])+'\'+cp_ToStr(_Link_.IMSERIAL,1)
      cp_ShowWarn(i_cKey,this.VAANAIMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AISERINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AISERFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAANAIMP_IDX,3]
    i_lTable = "VAANAIMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2], .t., this.VAANAIMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AISERFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VAANAIMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMSERIAL like "+cp_ToStrODBC(trim(this.w_AISERFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMSERIAL',trim(this.w_AISERFIN))
          select IMSERIAL,IMDATIMP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AISERFIN)==trim(_Link_.IMSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AISERFIN) and !this.bDontReportError
            deferred_cp_zoom('VAANAIMP','*','IMSERIAL',cp_AbsName(oSource.parent,'oAISERFIN_1_4'),i_cWhere,'',"Anagrafica importazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                     +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',oSource.xKey(1))
            select IMSERIAL,IMDATIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AISERFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMSERIAL,IMDATIMP";
                   +" from "+i_cTable+" "+i_lTable+" where IMSERIAL="+cp_ToStrODBC(this.w_AISERFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMSERIAL',this.w_AISERFIN)
            select IMSERIAL,IMDATIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AISERFIN = NVL(_Link_.IMSERIAL,space(10))
      this.w_AIDATIMF = NVL(cp_ToDate(_Link_.IMDATIMP),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AISERFIN = space(10)
      endif
      this.w_AIDATIMF = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_AISERINI) OR .w_AISERFIN>=.w_AISERINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Seriale inesistente o seriale finale minore del seriale iniziale")
        endif
        this.w_AISERFIN = space(10)
        this.w_AIDATIMF = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAANAIMP_IDX,2])+'\'+cp_ToStr(_Link_.IMSERIAL,1)
      cp_ShowWarn(i_cKey,this.VAANAIMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AISERFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAISERINI_1_3.value==this.w_AISERINI)
      this.oPgFrm.Page1.oPag.oAISERINI_1_3.value=this.w_AISERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oAISERFIN_1_4.value==this.w_AISERFIN)
      this.oPgFrm.Page1.oPag.oAISERFIN_1_4.value=this.w_AISERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATINI_1_7.value==this.w_AIDATINI)
      this.oPgFrm.Page1.oPag.oAIDATINI_1_7.value=this.w_AIDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATFIN_1_8.value==this.w_AIDATFIN)
      this.oPgFrm.Page1.oPag.oAIDATFIN_1_8.value=this.w_AIDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDELFIL_1_11.RadioValue()==this.w_FLDELFIL)
      this.oPgFrm.Page1.oPag.oFLDELFIL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLELDOC_1_12.RadioValue()==this.w_FLELDOC)
      this.oPgFrm.Page1.oPag.oFLELDOC_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATIMI_1_13.value==this.w_AIDATIMI)
      this.oPgFrm.Page1.oPag.oAIDATIMI_1_13.value=this.w_AIDATIMI
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATIMF_1_14.value==this.w_AIDATIMF)
      this.oPgFrm.Page1.oPag.oAIDATIMF_1_14.value=this.w_AIDATIMF
    endif
    if not(this.oPgFrm.Page1.oPag.oAIFLELAB_1_20.RadioValue()==this.w_AIFLELAB)
      this.oPgFrm.Page1.oPag.oAIFLELAB_1_20.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_AISERFIN) OR .w_AISERFIN>=.w_AISERINI)  and (EMPTY(.w_AIDATINI) AND EMPTY(.w_AIDATFIN))  and not(empty(.w_AISERINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAISERINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Seriale inesistente o seriale iniziale maggiore del seriale finale")
          case   not(EMPTY(.w_AISERINI) OR .w_AISERFIN>=.w_AISERINI)  and (EMPTY(.w_AIDATINI) AND EMPTY(.w_AIDATFIN))  and not(empty(.w_AISERFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAISERFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Seriale inesistente o seriale finale minore del seriale iniziale")
          case   not(EMPTY(.w_AIDATFIN) OR .w_AIDATINI<=.w_AIDATFIN)  and (EMPTY(.w_AISERINI) AND EMPTY(.w_AISERFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere minore o uguale alla data finale")
          case   not(EMPTY(.w_AIDATINI) OR .w_AIDATINI<=.w_AIDATFIN)  and (EMPTY(.w_AISERINI) AND EMPTY(.w_AISERFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale deve essere maggiore o uguale alla data iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AISERINI = this.w_AISERINI
    this.o_AISERFIN = this.w_AISERFIN
    this.o_AIDATINI = this.w_AIDATINI
    this.o_AIDATFIN = this.w_AIDATFIN
    return

enddefine

* --- Define pages as container
define class tgsva_kdiPag1 as StdContainer
  Width  = 641
  height = 489
  stdWidth  = 641
  stdheight = 489
  resizeXpos=439
  resizeYpos=292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="GTULXLRYOW",left=535, top=442, width=48,height=45,;
    CpPicture="BMP\ELIMINA2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cancellare le importazioni selezionate";
    , HelpContextID = 266760634;
    , caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        GSVA_BDI(this.Parent.oContained,"CANC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="MWICDNLWLT",left=587, top=442, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 240246790;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAISERINI_1_3 as StdField with uid="QBNLKWHBAW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AISERINI", cQueryName = "AISERINI",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Seriale inesistente o seriale iniziale maggiore del seriale finale",;
    HelpContextID = 159386447,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=168, Top=16, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAANAIMP", oKey_1_1="IMSERIAL", oKey_1_2="this.w_AISERINI"

  func oAISERINI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_AIDATINI) AND EMPTY(.w_AIDATFIN))
    endwith
   endif
  endfunc

  func oAISERINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAISERINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAISERINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VAANAIMP','*','IMSERIAL',cp_AbsName(this.parent,'oAISERINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica importazioni",'',this.parent.oContained
  endproc

  add object oAISERFIN_1_4 as StdField with uid="REQHZWKUCT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AISERFIN", cQueryName = "AISERFIN",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Seriale inesistente o seriale finale minore del seriale iniziale",;
    HelpContextID = 159380652,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=168, Top=43, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VAANAIMP", oKey_1_1="IMSERIAL", oKey_1_2="this.w_AISERFIN"

  func oAISERFIN_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_AIDATINI) AND EMPTY(.w_AIDATFIN))
    endwith
   endif
  endfunc

  func oAISERFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oAISERFIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAISERFIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VAANAIMP','*','IMSERIAL',cp_AbsName(this.parent,'oAISERFIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica importazioni",'',this.parent.oContained
  endproc

  add object oAIDATINI_1_7 as StdField with uid="LHVIBBMNMQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AIDATINI", cQueryName = "AIDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere minore o uguale alla data finale",;
    HelpContextID = 161160015,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=559, Top=16

  func oAIDATINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_AISERINI) AND EMPTY(.w_AISERFIN))
    endwith
   endif
  endfunc

  func oAIDATINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AIDATFIN) OR .w_AIDATINI<=.w_AIDATFIN)
    endwith
    return bRes
  endfunc

  add object oAIDATFIN_1_8 as StdField with uid="BGIQCSVIVN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AIDATFIN", cQueryName = "AIDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale deve essere maggiore o uguale alla data iniziale",;
    HelpContextID = 157607084,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=559, Top=43

  func oAIDATFIN_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_AISERINI) AND EMPTY(.w_AISERFIN))
    endwith
   endif
  endfunc

  func oAIDATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AIDATINI) OR .w_AIDATINI<=.w_AIDATFIN)
    endwith
    return bRes
  endfunc


  add object oFLDELFIL_1_11 as StdCombo with uid="ZDBYZNBGUI",rtseq=5,rtrep=.f.,left=168,top=94,width=190,height=21;
    , HelpContextID = 165732702;
    , cFormVar="w_FLDELFIL",RowSource=""+"No,"+"Solo file XML intermedi,"+"Solo file originari,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDELFIL_1_11.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'X',;
    iif(this.value =3,'O',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oFLDELFIL_1_11.GetRadio()
    this.Parent.oContained.w_FLDELFIL = this.RadioValue()
    return .t.
  endfunc

  func oFLDELFIL_1_11.SetRadio()
    this.Parent.oContained.w_FLDELFIL=trim(this.Parent.oContained.w_FLDELFIL)
    this.value = ;
      iif(this.Parent.oContained.w_FLDELFIL=='N',1,;
      iif(this.Parent.oContained.w_FLDELFIL=='X',2,;
      iif(this.Parent.oContained.w_FLDELFIL=='O',3,;
      iif(this.Parent.oContained.w_FLDELFIL=='E',4,;
      0))))
  endfunc

  add object oFLELDOC_1_12 as StdCheck with uid="WZIWDAAXVV",rtseq=7,rtrep=.f.,left=365, top=91, caption="Elimina documenti collegati",;
    ToolTipText = "Se attivo elimina anche i documenti associati",;
    HelpContextID = 245771862,;
    cFormVar="w_FLELDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLELDOC_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLELDOC_1_12.GetRadio()
    this.Parent.oContained.w_FLELDOC = this.RadioValue()
    return .t.
  endfunc

  func oFLELDOC_1_12.SetRadio()
    this.Parent.oContained.w_FLELDOC=trim(this.Parent.oContained.w_FLELDOC)
    this.value = ;
      iif(this.Parent.oContained.w_FLELDOC=='S',1,;
      0)
  endfunc

  add object oAIDATIMI_1_13 as StdField with uid="RBVCHPOGFN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AIDATIMI", cQueryName = "AIDATIMI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 161160015,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=283, Top=16

  add object oAIDATIMF_1_14 as StdField with uid="ODEQWKUTVX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AIDATIMF", cQueryName = "AIDATIMF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 161160012,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=283, Top=43


  add object IMPSEL as cp_szoombox with uid="KXMCKHDCDE",left=4, top=124, width=633,height=310,;
    caption='IMPSEL',;
   bGlobalFont=.t.,;
    cZoomFile="GSVA_KDI",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="VAANAIMP",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 196992902


  add object oBtn_1_16 as StdButton with uid="SMWIQHHVKY",left=587, top=70, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare lo zoom";
    , HelpContextID = 74803559;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .NotifyEvent("Aggiorna")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="LUHNMBSWMQ",left=11, top=442, width=48,height=45,;
    CpPicture="BMP\CHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le importazioni da cancellare";
    , HelpContextID = 18987046;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSVA_BDI(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="ABJFIATDMP",left=62, top=442, width=48,height=45,;
    CpPicture="BMP\UNCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le elaborazioni da cancellare";
    , HelpContextID = 18987046;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSVA_BDI(this.Parent.oContained,"DESEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="PDWIPENRCZ",left=113, top=442, width=48,height=45,;
    CpPicture="BMP\INVCHECK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione delle importazioni da cancellare";
    , HelpContextID = 18987046;
    , caption='\<Inv. sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSVA_BDI(this.Parent.oContained,"INVSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oAIFLELAB_1_20 as StdCombo with uid="NZQBVNOBZH",rtseq=10,rtrep=.f.,left=168,top=70,width=190,height=21;
    , ToolTipText = "Permette di selezionare lo stato delle elaborazioni da mostrare (Solo elaborate, solo da elaborare oppure tutte)";
    , HelpContextID = 196492104;
    , cFormVar="w_AIFLELAB",RowSource=""+"Solo da elaborare,"+"Solo elaborate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAIFLELAB_1_20.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oAIFLELAB_1_20.GetRadio()
    this.Parent.oContained.w_AIFLELAB = this.RadioValue()
    return .t.
  endfunc

  func oAIFLELAB_1_20.SetRadio()
    this.Parent.oContained.w_AIFLELAB=trim(this.Parent.oContained.w_AIFLELAB)
    this.value = ;
      iif(this.Parent.oContained.w_AIFLELAB=='N',1,;
      iif(this.Parent.oContained.w_AIFLELAB=='S',2,;
      iif(this.Parent.oContained.w_AIFLELAB=='T',3,;
      0)))
  endfunc

  add object oStr_1_5 as StdString with uid="GUUUYUZJVO",Visible=.t., Left=5, Top=18,;
    Alignment=1, Width=159, Height=18,;
    Caption="Da seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CVTBVYVQAY",Visible=.t., Left=5, Top=44,;
    Alignment=1, Width=159, Height=18,;
    Caption="A seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ZCRKWAARSX",Visible=.t., Left=395, Top=18,;
    Alignment=1, Width=159, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MEKOZIIEIE",Visible=.t., Left=395, Top=44,;
    Alignment=1, Width=159, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HHVVATPWFY",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=159, Height=18,;
    Caption="Stato elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VHFCOGLHWL",Visible=.t., Left=5, Top=94,;
    Alignment=1, Width=159, Height=18,;
    Caption="Eliminazione file:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsva_kdiPag2 as StdContainer
  Width  = 641
  height = 489
  stdWidth  = 641
  stdheight = 489
  resizeXpos=421
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="QVQLZYFOGE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 82160186,;
   bGlobalFont=.t.,;
    Height=476, Width=629, Left=5, Top=9, Readonly=.T.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
