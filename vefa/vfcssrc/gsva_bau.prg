* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bau                                                        *
*              Aggiorna campo ora in schede di calcolo                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-08                                                      *
* Last revis.: 2007-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CLSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bau",oParentObject,m.w_CLSERIAL)
return(i_retval)

define class tgsva_bau as StdBatch
  * --- Local variables
  w_CLSERIAL = space(10)
  w_ULTORA = 0
  w_ULTMIN = 0
  * --- WorkFile variables
  CLLSMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno ora e minuto dell'ultimo lancio della scheda di calcolo (da gsar_bag, gsva_bbc)
    this.w_ULTORA = HOUR(DATETIME( ))
    this.w_ULTMIN = MINUTE(DATETIME( ))
    * --- Write into CLLSMAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CLLSMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLLSMAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CLLSMAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLULTORA ="+cp_NullLink(cp_ToStrODBC(this.w_ULTORA),'CLLSMAST','CLULTORA');
      +",CLULTMIN ="+cp_NullLink(cp_ToStrODBC(this.w_ULTMIN),'CLLSMAST','CLULTMIN');
          +i_ccchkf ;
      +" where ";
          +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
             )
    else
      update (i_cTable) set;
          CLULTORA = this.w_ULTORA;
          ,CLULTMIN = this.w_ULTMIN;
          &i_ccchkf. ;
       where;
          CLSERIAL = this.w_CLSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,w_CLSERIAL)
    this.w_CLSERIAL=w_CLSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CLLSMAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CLSERIAL"
endproc
