* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bcp                                                        *
*              Gestione configurazioni ordinamenti/rotture piano di spedizione *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-10                                                      *
* Last revis.: 2012-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bcp",oParentObject,m.pOPER)
return(i_retval)

define class tgsva_bcp as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_CHKFLPRE = .f.
  w_OBJGEST = .NULL.
  w_PADRE = .NULL.
  w_NUMREC = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_TBNAME = space(15)
  w_TBCOMMENT = space(60)
  w_FLNAME = space(30)
  w_FLCOMMEN = space(80)
  w_FLTYPE = space(1)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_FLOUTVAL = space(46)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="CHKPRED"
        this.w_CHKFLPRE = .T.
        * --- Select from gsva_bcp
        do vq_exec with 'gsva_bcp',this,'_Curs_gsva_bcp','',.f.,.t.
        if used('_Curs_gsva_bcp')
          select _Curs_gsva_bcp
          locate for 1=1
          do while not(eof())
          this.w_CHKFLPRE = NVL(_Curs_gsva_bcp.CONTA, 0 ) = 0
            select _Curs_gsva_bcp
            continue
          enddo
          use
        endif
        i_RetVal = this.w_CHKFLPRE
      case this.pOPER=="ZOOM"
        this.w_FLOUTVAL = ""
        vx_exec("..\VEFA\EXE\QUERY\GSVA_MCP.VZM",this)
        if !EMPTY(NVL(this.w_FLOUTVAL, " "))
          this.oParentObject.w_CPNOMCAM = this.w_FLOUTVAL
        endif
      case this.pOPER=="CARRAPIDO"
        this.w_PADRE = this.oParentObject
        this.w_OBJGEST = this.w_PADRE.w_PADRE
        SELECT FLOUTVAL FROM (this.w_PADRE.w_SFLDZOOM.cCursor) WHERE XCHK=1 INTO CURSOR "CAMPISEL"
        if USED("CAMPISEL")
          this.w_OBJGEST.LastRow()     
          SELECT "CAMPISEL"
          GO TOP
          SCAN
          if this.w_OBJGEST.FullRow()
            this.w_OBJGEST.AddRow()     
          endif
          SetValueLinked("D", this.w_OBJGEST, "w_CPNOMCAM", CAMPISEL.FLOUTVAL )
          this.w_OBJGEST.SaveRow()     
          SELECT "CAMPISEL"
          ENDSCAN
          this.w_OBJGEST.Refresh()     
        endif
      case this.pOPER=="CHKDUPFLD"
        this.w_NUMREC = 0
        this.w_PADRE = this.oParentObject
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        this.w_PADRE.Exec_Select("ChkDupFld", "t_CPNOMCAM, Count(*) As Conta", "t_CPFLORDI=1", "", "t_CPNOMCAM")     
        if USED("ChkDupFld")
          this.w_oMESS = createobject("ah_message")
          SELECT "ChkDupFld"
          GO TOP
          SCAN FOR Conta > 1
          if this.w_NUMREC = 0
            this.w_oPART = this.w_oMESS.addmsgpartNL("Il seguenti campi sono stati indicati due o pi� volte con il flag ordinamento attivo:")
          endif
          this.w_oPART = this.w_oMESS.addmsgpartNL("%1")
          this.w_oPART.AddParam(ALLTRIM(ChkDupFld.t_CPNOMCAM))     
          this.w_NUMREC = this.w_NUMREC + 1
          ENDSCAN
        endif
        USE IN SELECT("ChkDupFld")
        i_RetVal = this.w_NUMREC = 0 OR ah_YesNo( this.w_oMESS.ComposeMessage() + "La duplicazione dei campi pu� provocare errori in fase di estrazione dati%0Proseguire comunque?")
        this.w_PADRE.RePos()     
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gsva_bcp')
      use in _Curs_gsva_bcp
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
