* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_mki                                                        *
*              Kit imballi                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-26                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_mki"))

* --- Class definition
define class tgsva_mki as StdTrsForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 587
  Height = 366+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=37064087
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=57

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  DISMBASE_IDX = 0
  DISTBASE_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  cFile = "DISMBASE"
  cFileDetail = "DISTBASE"
  cKeySelect = "DBCODICE"
  cQueryFilter="DBDISKIT='I'"
  cKeyWhere  = "DBCODICE=this.w_DBCODICE"
  cKeyDetail  = "DBCODICE=this.w_DBCODICE"
  cKeyWhereODBC = '"DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';

  cKeyDetailWhereODBC = '"DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"DISTBASE.DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DISTBASE.CPROWORD '
  cPrg = "gsva_mki"
  cComment = "Kit imballi"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DBCODICE = space(20)
  w_DBDESCRI = space(40)
  w_DBFLSTAT = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_CPROWORD = 0
  w_DBDTINVA = ctod('  /  /  ')
  w_DBDTOBSO = ctod('  /  /  ')
  w_UTDV = ctot('')
  w_DBCODCOM = space(20)
  o_DBCODCOM = space(20)
  w_OPERA3 = space(1)
  w_DBARTCOM = space(20)
  w_TIPRIG = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_DESART = space(40)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_DBDESCOM = space(40)
  w_DBUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_DBQTADIS = 0
  w_DBFLESPL = space(1)
  w_DBPERSCA = 0
  w_DBRECSCA = 0
  w_DBPERSFR = 0
  o_DBPERSFR = 0
  w_DBRECSFR = 0
  w_DBPERRIC = 0
  w_DBDATULT = ctod('  /  /  ')
  w_DB__NOTE = space(0)
  w_OBTEST = ctod('  /  /  ')
  w_DESCIC = space(40)
  w_DBFLVARC = space(1)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_EXPCOART = space(20)
  w_DBINIVAL = ctod('  /  /  ')
  w_DBFINVAL = ctod('  /  /  ')
  w_VERDAT = .F.
  w_DBCOEUM1 = 0
  w_DBRIFFAS = 0
  w_DESFAS = space(40)
  w_DBFLVARI = space(1)
  w_DBNOTAGG = space(0)
  w_DBDISKIT = space(1)
  w_DBPERSCR = 0
  w_DBVALSCR = 0
  w_DBPERSCO = 0
  w_DBVALSCO = 0
  w_CODESC = space(5)
  w_DBFLOMAG = space(1)
  w_IMBREN = 0
  w_MODUM2 = space(1)
  w_LegQta = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DISMBASE','gsva_mki')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mkiPag1","gsva_mki",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Kit")
      .Pages(1).HelpContextID = 37567398
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDBCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LegQta = this.oPgFrm.Pages(1).oPag.LegQta
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_LegQta = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DISTBASE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DISMBASE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DISMBASE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_DBCODICE = NVL(DBCODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from DISMBASE where DBCODICE=KeySet.DBCODICE
    *
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2],this.bLoadRecFilter,this.DISMBASE_IDX,"gsva_mki")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DISMBASE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DISMBASE.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"DISTBASE.","DISMBASE.")
      i_cTable = i_cTable+' DISMBASE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESCIC = space(40)
        .w_VERDAT = .F.
        .w_CODESC = space(5)
        .w_IMBREN = 3
        .w_DBCODICE = NVL(DBCODICE,space(20))
        .w_DBDESCRI = NVL(DBDESCRI,space(40))
        .w_DBFLSTAT = NVL(DBFLSTAT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_DBDTINVA = NVL(cp_ToDate(DBDTINVA),ctod("  /  /  "))
        .w_DBDTOBSO = NVL(cp_ToDate(DBDTOBSO),ctod("  /  /  "))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_OBTEST = .w_UTDC
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_DBNOTAGG = NVL(DBNOTAGG,space(0))
        .w_DBDISKIT = NVL(DBDISKIT,space(1))
        .w_DBPERSCO = NVL(DBPERSCO,0)
        .w_DBVALSCO = NVL(DBVALSCO,0)
        .oPgFrm.Page1.oPag.LegQta.Calculate(Ah_MsgFormat("(*) La quantit� indica le unit� dell'articolo di magazzino, cui � stato associato%0questo kit, che sono contenute nel singolo articolo imballo.%0Il tutto espresso nella prima unit� di misura."))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DISMBASE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from DISTBASE where DBCODICE=KeySet.DBCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.DISTBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('DISTBASE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "DISTBASE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" DISTBASE"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
        select * from (i_cTable) DISTBASE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_OPERA3 = space(1)
          .w_TIPRIG = space(1)
          .w_DTOBS1 = ctod("  /  /  ")
          .w_DESART = space(40)
          .w_UNMIS3 = space(3)
          .w_MOLTI3 = 0
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_OPERAT = space(1)
          .w_MOLTIP = 0
          .w_FLFRAZ = space(1)
          .w_DESFAS = space(40)
          .w_MODUM2 = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DBCODCOM = NVL(DBCODCOM,space(20))
          if link_2_2_joined
            this.w_DBCODCOM = NVL(CACODICE202,NVL(this.w_DBCODCOM,space(20)))
            this.w_DBARTCOM = NVL(CACODART202,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_DESART = NVL(CADESART202,space(40))
            this.w_TIPRIG = NVL(CA__TIPO202,space(1))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          .w_DBARTCOM = NVL(DBARTCOM,space(20))
          if link_2_4_joined
            this.w_DBARTCOM = NVL(ARCODART204,NVL(this.w_DBARTCOM,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_OPERAT = NVL(AROPERAT204,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP204,0)
          else
          .link_2_4('Load')
          endif
          .w_DBDESCOM = NVL(DBDESCOM,space(40))
          .w_DBUNIMIS = NVL(DBUNIMIS,space(3))
          if link_2_15_joined
            this.w_DBUNIMIS = NVL(UMCODICE215,NVL(this.w_DBUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ215,space(1))
            this.w_MODUM2 = NVL(UMMODUM2215,space(1))
          else
          .link_2_15('Load')
          endif
          .w_DBQTADIS = NVL(DBQTADIS,0)
          .w_DBFLESPL = NVL(DBFLESPL,space(1))
          .w_DBPERSCA = NVL(DBPERSCA,0)
          .w_DBRECSCA = NVL(DBRECSCA,0)
          .w_DBPERSFR = NVL(DBPERSFR,0)
          .w_DBRECSFR = NVL(DBRECSFR,0)
          .w_DBPERRIC = NVL(DBPERRIC,0)
          .w_DBDATULT = NVL(cp_ToDate(DBDATULT),ctod("  /  /  "))
          .w_DB__NOTE = NVL(DB__NOTE,space(0))
          .w_DBFLVARC = NVL(DBFLVARC,space(1))
          .w_DBINIVAL = NVL(cp_ToDate(DBINIVAL),ctod("  /  /  "))
          .w_DBFINVAL = NVL(cp_ToDate(DBFINVAL),ctod("  /  /  "))
          .w_DBCOEUM1 = NVL(DBCOEUM1,0)
          .w_DBRIFFAS = NVL(DBRIFFAS,0)
          .w_DBFLVARI = NVL(DBFLVARI,space(1))
          .w_DBPERSCR = NVL(DBPERSCR,0)
          .w_DBVALSCR = NVL(DBVALSCR,0)
          .w_DBFLOMAG = NVL(DBFLOMAG,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_UTDC
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.LegQta.Calculate(Ah_MsgFormat("(*) La quantit� indica le unit� dell'articolo di magazzino, cui � stato associato%0questo kit, che sono contenute nel singolo articolo imballo.%0Il tutto espresso nella prima unit� di misura."))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DBCODICE=space(20)
      .w_DBDESCRI=space(40)
      .w_DBFLSTAT=space(1)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_CPROWORD=10
      .w_DBDTINVA=ctod("  /  /  ")
      .w_DBDTOBSO=ctod("  /  /  ")
      .w_UTDV=ctot("")
      .w_DBCODCOM=space(20)
      .w_OPERA3=space(1)
      .w_DBARTCOM=space(20)
      .w_TIPRIG=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DBDESCOM=space(40)
      .w_DBUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_DBQTADIS=0
      .w_DBFLESPL=space(1)
      .w_DBPERSCA=0
      .w_DBRECSCA=0
      .w_DBPERSFR=0
      .w_DBRECSFR=0
      .w_DBPERRIC=0
      .w_DBDATULT=ctod("  /  /  ")
      .w_DB__NOTE=space(0)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCIC=space(40)
      .w_DBFLVARC=space(1)
      .w_MAXLEVEL=0
      .w_VERIFICA=space(1)
      .w_EXPCOART=space(20)
      .w_DBINIVAL=ctod("  /  /  ")
      .w_DBFINVAL=ctod("  /  /  ")
      .w_VERDAT=.f.
      .w_DBCOEUM1=0
      .w_DBRIFFAS=0
      .w_DESFAS=space(40)
      .w_DBFLVARI=space(1)
      .w_DBNOTAGG=space(0)
      .w_DBDISKIT=space(1)
      .w_DBPERSCR=0
      .w_DBVALSCR=0
      .w_DBPERSCO=0
      .w_DBVALSCO=0
      .w_CODESC=space(5)
      .w_DBFLOMAG=space(1)
      .w_IMBREN=0
      .w_MODUM2=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_DBFLSTAT = 'S'
        .DoRTCalc(4,11,.f.)
        if not(empty(.w_DBCODCOM))
         .link_2_2('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_DBARTCOM))
         .link_2_4('Full')
        endif
        .DoRTCalc(14,22,.f.)
        .w_DBDESCOM = .w_DESART
        .w_DBUNIMIS = .w_UNMIS1
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_DBUNIMIS))
         .link_2_15('Full')
        endif
        .DoRTCalc(25,25,.f.)
        .w_DBQTADIS = 1
        .w_DBFLESPL = ' '
        .DoRTCalc(28,34,.f.)
        .w_OBTEST = .w_UTDC
        .DoRTCalc(36,36,.f.)
        .w_DBFLVARC = .w_DBFLVARI
        .w_MAXLEVEL = 999
        .w_VERIFICA = 'S C'
        .w_EXPCOART = SPACE(20)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_DBINIVAL = i_DATSYS
        .w_DBFINVAL = i_FINDAT
        .w_VERDAT = .F.
        .w_DBCOEUM1 = CALQTA(.w_DBQTADIS,.w_DBUNIMIS, .w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', .w_FLFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD, 'D')
        .DoRTCalc(45,46,.f.)
        .w_DBFLVARI = ' '
        .DoRTCalc(48,48,.f.)
        .w_DBDISKIT = 'I'
        .DoRTCalc(50,54,.f.)
        .w_DBFLOMAG = 'X'
        .w_IMBREN = 3
        .oPgFrm.Page1.oPag.LegQta.Calculate(Ah_MsgFormat("(*) La quantit� indica le unit� dell'articolo di magazzino, cui � stato associato%0questo kit, che sono contenute nel singolo articolo imballo.%0Il tutto espresso nella prima unit� di misura."))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DISMBASE')
    this.DoRTCalc(57,57,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsva_mki
    Local LegQtaCtrl
    LegQtaCtrl = this.GetCtrl("LegQta")
    LegQtaCtrl.FontSize=8
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDBCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDBDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oDBNOTAGG_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDBCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDBCODICE_1_1.enabled = .t.
        .Page1.oPag.oDBDESCRI_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DISMBASE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBCODICE,"DBCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDESCRI,"DBDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBFLSTAT,"DBFLSTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDTINVA,"DBDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDTOBSO,"DBDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBNOTAGG,"DBNOTAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBDISKIT,"DBDISKIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBPERSCO,"DBPERSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DBVALSCO,"DBVALSCO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    i_lTable = "DISMBASE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DISMBASE_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do Stampa with this
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DBCODCOM C(20);
      ,t_DBDESCOM C(40);
      ,t_DBQTADIS N(15,6);
      ,CPROWNUM N(10);
      ,t_OPERA3 C(1);
      ,t_DBARTCOM C(20);
      ,t_TIPRIG C(1);
      ,t_DTOBS1 D(8);
      ,t_DESART C(40);
      ,t_UNMIS3 C(3);
      ,t_MOLTI3 N(10,4);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_OPERAT C(1);
      ,t_MOLTIP N(10,4);
      ,t_DBUNIMIS C(3);
      ,t_FLFRAZ C(1);
      ,t_DBFLESPL C(1);
      ,t_DBPERSCA N(6,2);
      ,t_DBRECSCA N(6,2);
      ,t_DBPERSFR N(6,2);
      ,t_DBRECSFR N(6,2);
      ,t_DBPERRIC N(6,2);
      ,t_DBDATULT D(8);
      ,t_DB__NOTE M(10);
      ,t_DBFLVARC C(1);
      ,t_DBINIVAL D(8);
      ,t_DBFINVAL D(8);
      ,t_DBCOEUM1 N(15,6);
      ,t_DBRIFFAS N(4);
      ,t_DESFAS C(40);
      ,t_DBFLVARI C(1);
      ,t_DBPERSCR N(6,2);
      ,t_DBVALSCR N(18,5);
      ,t_DBFLOMAG C(1);
      ,t_MODUM2 C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mkibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_2.controlsource=this.cTrsName+'.t_DBCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_14.controlsource=this.cTrsName+'.t_DBDESCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_17.controlsource=this.cTrsName+'.t_DBQTADIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(68)
    this.AddVLine(218)
    this.AddVLine(473)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISMBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DISMBASE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DISMBASE')
        i_extval=cp_InsertValODBCExtFlds(this,'DISMBASE')
        local i_cFld
        i_cFld=" "+;
                  "(DBCODICE,DBDESCRI,DBFLSTAT,UTCC,UTCV"+;
                  ",UTDC,DBDTINVA,DBDTOBSO,UTDV,DBNOTAGG"+;
                  ",DBDISKIT,DBPERSCO,DBVALSCO"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_DBCODICE)+;
                    ","+cp_ToStrODBC(this.w_DBDESCRI)+;
                    ","+cp_ToStrODBC(this.w_DBFLSTAT)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_DBDTINVA)+;
                    ","+cp_ToStrODBC(this.w_DBDTOBSO)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_DBNOTAGG)+;
                    ","+cp_ToStrODBC(this.w_DBDISKIT)+;
                    ","+cp_ToStrODBC(this.w_DBPERSCO)+;
                    ","+cp_ToStrODBC(this.w_DBVALSCO)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DISMBASE')
        i_extval=cp_InsertValVFPExtFlds(this,'DISMBASE')
        cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_DBCODICE)
        INSERT INTO (i_cTable);
              (DBCODICE,DBDESCRI,DBFLSTAT,UTCC,UTCV,UTDC,DBDTINVA,DBDTOBSO,UTDV,DBNOTAGG,DBDISKIT,DBPERSCO,DBVALSCO &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_DBCODICE;
                  ,this.w_DBDESCRI;
                  ,this.w_DBFLSTAT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_DBDTINVA;
                  ,this.w_DBDTOBSO;
                  ,this.w_UTDV;
                  ,this.w_DBNOTAGG;
                  ,this.w_DBDISKIT;
                  ,this.w_DBPERSCO;
                  ,this.w_DBVALSCO;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DISTBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
      *
      * insert into DISTBASE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(DBCODICE,CPROWORD,DBCODCOM,DBARTCOM,DBDESCOM"+;
                  ",DBUNIMIS,DBQTADIS,DBFLESPL,DBPERSCA,DBRECSCA"+;
                  ",DBPERSFR,DBRECSFR,DBPERRIC,DBDATULT,DB__NOTE"+;
                  ",DBFLVARC,DBINIVAL,DBFINVAL,DBCOEUM1,DBRIFFAS"+;
                  ",DBFLVARI,DBPERSCR,DBVALSCR,DBFLOMAG,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DBCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DBCODCOM)+","+cp_ToStrODBCNull(this.w_DBARTCOM)+","+cp_ToStrODBC(this.w_DBDESCOM)+;
             ","+cp_ToStrODBCNull(this.w_DBUNIMIS)+","+cp_ToStrODBC(this.w_DBQTADIS)+","+cp_ToStrODBC(this.w_DBFLESPL)+","+cp_ToStrODBC(this.w_DBPERSCA)+","+cp_ToStrODBC(this.w_DBRECSCA)+;
             ","+cp_ToStrODBC(this.w_DBPERSFR)+","+cp_ToStrODBC(this.w_DBRECSFR)+","+cp_ToStrODBC(this.w_DBPERRIC)+","+cp_ToStrODBC(this.w_DBDATULT)+","+cp_ToStrODBC(this.w_DB__NOTE)+;
             ","+cp_ToStrODBC(this.w_DBFLVARC)+","+cp_ToStrODBC(this.w_DBINIVAL)+","+cp_ToStrODBC(this.w_DBFINVAL)+","+cp_ToStrODBC(this.w_DBCOEUM1)+","+cp_ToStrODBC(this.w_DBRIFFAS)+;
             ","+cp_ToStrODBC(this.w_DBFLVARI)+","+cp_ToStrODBC(this.w_DBPERSCR)+","+cp_ToStrODBC(this.w_DBVALSCR)+","+cp_ToStrODBC(this.w_DBFLOMAG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DBCODICE',this.w_DBCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_DBCODICE,this.w_CPROWORD,this.w_DBCODCOM,this.w_DBARTCOM,this.w_DBDESCOM"+;
                ",this.w_DBUNIMIS,this.w_DBQTADIS,this.w_DBFLESPL,this.w_DBPERSCA,this.w_DBRECSCA"+;
                ",this.w_DBPERSFR,this.w_DBRECSFR,this.w_DBPERRIC,this.w_DBDATULT,this.w_DB__NOTE"+;
                ",this.w_DBFLVARC,this.w_DBINIVAL,this.w_DBFINVAL,this.w_DBCOEUM1,this.w_DBRIFFAS"+;
                ",this.w_DBFLVARI,this.w_DBPERSCR,this.w_DBVALSCR,this.w_DBFLOMAG,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DISMBASE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update DISMBASE
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'DISMBASE')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " DBDESCRI="+cp_ToStrODBC(this.w_DBDESCRI)+;
             ",DBFLSTAT="+cp_ToStrODBC(this.w_DBFLSTAT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",DBDTINVA="+cp_ToStrODBC(this.w_DBDTINVA)+;
             ",DBDTOBSO="+cp_ToStrODBC(this.w_DBDTOBSO)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",DBNOTAGG="+cp_ToStrODBC(this.w_DBNOTAGG)+;
             ",DBDISKIT="+cp_ToStrODBC(this.w_DBDISKIT)+;
             ",DBPERSCO="+cp_ToStrODBC(this.w_DBPERSCO)+;
             ",DBVALSCO="+cp_ToStrODBC(this.w_DBVALSCO)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'DISMBASE')
          i_cWhere = cp_PKFox(i_cTable  ,'DBCODICE',this.w_DBCODICE  )
          UPDATE (i_cTable) SET;
              DBDESCRI=this.w_DBDESCRI;
             ,DBFLSTAT=this.w_DBFLSTAT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,DBDTINVA=this.w_DBDTINVA;
             ,DBDTOBSO=this.w_DBDTOBSO;
             ,UTDV=this.w_UTDV;
             ,DBNOTAGG=this.w_DBNOTAGG;
             ,DBDISKIT=this.w_DBDISKIT;
             ,DBPERSCO=this.w_DBPERSCO;
             ,DBVALSCO=this.w_DBVALSCO;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.DISTBASE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from DISTBASE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DISTBASE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DBCODCOM="+cp_ToStrODBCNull(this.w_DBCODCOM)+;
                     ",DBARTCOM="+cp_ToStrODBCNull(this.w_DBARTCOM)+;
                     ",DBDESCOM="+cp_ToStrODBC(this.w_DBDESCOM)+;
                     ",DBUNIMIS="+cp_ToStrODBCNull(this.w_DBUNIMIS)+;
                     ",DBQTADIS="+cp_ToStrODBC(this.w_DBQTADIS)+;
                     ",DBFLESPL="+cp_ToStrODBC(this.w_DBFLESPL)+;
                     ",DBPERSCA="+cp_ToStrODBC(this.w_DBPERSCA)+;
                     ",DBRECSCA="+cp_ToStrODBC(this.w_DBRECSCA)+;
                     ",DBPERSFR="+cp_ToStrODBC(this.w_DBPERSFR)+;
                     ",DBRECSFR="+cp_ToStrODBC(this.w_DBRECSFR)+;
                     ",DBPERRIC="+cp_ToStrODBC(this.w_DBPERRIC)+;
                     ",DBDATULT="+cp_ToStrODBC(this.w_DBDATULT)+;
                     ",DB__NOTE="+cp_ToStrODBC(this.w_DB__NOTE)+;
                     ",DBFLVARC="+cp_ToStrODBC(this.w_DBFLVARC)+;
                     ",DBINIVAL="+cp_ToStrODBC(this.w_DBINIVAL)+;
                     ",DBFINVAL="+cp_ToStrODBC(this.w_DBFINVAL)+;
                     ",DBCOEUM1="+cp_ToStrODBC(this.w_DBCOEUM1)+;
                     ",DBRIFFAS="+cp_ToStrODBC(this.w_DBRIFFAS)+;
                     ",DBFLVARI="+cp_ToStrODBC(this.w_DBFLVARI)+;
                     ",DBPERSCR="+cp_ToStrODBC(this.w_DBPERSCR)+;
                     ",DBVALSCR="+cp_ToStrODBC(this.w_DBVALSCR)+;
                     ",DBFLOMAG="+cp_ToStrODBC(this.w_DBFLOMAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DBCODCOM=this.w_DBCODCOM;
                     ,DBARTCOM=this.w_DBARTCOM;
                     ,DBDESCOM=this.w_DBDESCOM;
                     ,DBUNIMIS=this.w_DBUNIMIS;
                     ,DBQTADIS=this.w_DBQTADIS;
                     ,DBFLESPL=this.w_DBFLESPL;
                     ,DBPERSCA=this.w_DBPERSCA;
                     ,DBRECSCA=this.w_DBRECSCA;
                     ,DBPERSFR=this.w_DBPERSFR;
                     ,DBRECSFR=this.w_DBRECSFR;
                     ,DBPERRIC=this.w_DBPERRIC;
                     ,DBDATULT=this.w_DBDATULT;
                     ,DB__NOTE=this.w_DB__NOTE;
                     ,DBFLVARC=this.w_DBFLVARC;
                     ,DBINIVAL=this.w_DBINIVAL;
                     ,DBFINVAL=this.w_DBFINVAL;
                     ,DBCOEUM1=this.w_DBCOEUM1;
                     ,DBRIFFAS=this.w_DBRIFFAS;
                     ,DBFLVARI=this.w_DBFLVARI;
                     ,DBPERSCR=this.w_DBPERSCR;
                     ,DBVALSCR=this.w_DBVALSCR;
                     ,DBFLOMAG=this.w_DBFLOMAG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.DISTBASE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISTBASE_IDX,2])
        *
        * delete DISTBASE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.DISMBASE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
        *
        * delete DISMBASE
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM)))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_DBCODCOM<>.w_DBCODCOM
          .link_2_4('Full')
        endif
        .DoRTCalc(14,22,.t.)
        if .o_DBCODCOM<>.w_DBCODCOM
          .w_DBDESCOM = .w_DESART
        endif
        if .o_DBCODCOM<>.w_DBCODCOM
          .w_DBUNIMIS = .w_UNMIS1
          .link_2_15('Full')
        endif
        .DoRTCalc(25,34,.t.)
          .w_OBTEST = .w_UTDC
        .DoRTCalc(36,36,.t.)
          .w_DBFLVARC = .w_DBFLVARI
          .w_MAXLEVEL = 999
          .w_VERIFICA = 'S C'
          .w_EXPCOART = SPACE(20)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(41,43,.t.)
          .w_DBCOEUM1 = CALQTA(.w_DBQTADIS,.w_DBUNIMIS, .w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', .w_FLFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD, 'D')
        .oPgFrm.Page1.oPag.LegQta.Calculate(Ah_MsgFormat("(*) La quantit� indica le unit� dell'articolo di magazzino, cui � stato associato%0questo kit, che sono contenute nel singolo articolo imballo.%0Il tutto espresso nella prima unit� di misura."))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(45,57,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OPERA3 with this.w_OPERA3
      replace t_DBARTCOM with this.w_DBARTCOM
      replace t_TIPRIG with this.w_TIPRIG
      replace t_DTOBS1 with this.w_DTOBS1
      replace t_DESART with this.w_DESART
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_OPERAT with this.w_OPERAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_DBUNIMIS with this.w_DBUNIMIS
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_DBFLESPL with this.w_DBFLESPL
      replace t_DBPERSCA with this.w_DBPERSCA
      replace t_DBRECSCA with this.w_DBRECSCA
      replace t_DBPERSFR with this.w_DBPERSFR
      replace t_DBRECSFR with this.w_DBRECSFR
      replace t_DBPERRIC with this.w_DBPERRIC
      replace t_DBDATULT with this.w_DBDATULT
      replace t_DB__NOTE with this.w_DB__NOTE
      replace t_DBFLVARC with this.w_DBFLVARC
      replace t_DBINIVAL with this.w_DBINIVAL
      replace t_DBFINVAL with this.w_DBFINVAL
      replace t_DBCOEUM1 with this.w_DBCOEUM1
      replace t_DBRIFFAS with this.w_DBRIFFAS
      replace t_DESFAS with this.w_DESFAS
      replace t_DBFLVARI with this.w_DBFLVARI
      replace t_DBPERSCR with this.w_DBPERSCR
      replace t_DBVALSCR with this.w_DBVALSCR
      replace t_DBFLOMAG with this.w_DBFLOMAG
      replace t_MODUM2 with this.w_MODUM2
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.LegQta.Calculate(Ah_MsgFormat("(*) La quantit� indica le unit� dell'articolo di magazzino, cui � stato associato%0questo kit, che sono contenute nel singolo articolo imballo.%0Il tutto espresso nella prima unit� di misura."))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_HZQCKHQIGM()
    with this
          * --- Aggiorna modifca da\il
          .w_UTCV = i_CODUTE
          .w_UTDV = i_DATSYS
          .bHeaderUpdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBQTADIS_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDBQTADIS_2_17.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        if lower(cEvent)==lower("Edit Started")
          .Calculate_HZQCKHQIGM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LegQta.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DBCODCOM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DBCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DBCODCOM))
          select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODCOM)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODCOM) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDBCODCOM_2_2'),i_cWhere,'GSMA_BZA',"Articoli imballo",'GSVA_MKI.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DBCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DBCODCOM)
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODCOM = NVL(_Link_.CACODICE,space(20))
      this.w_DBARTCOM = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODCOM = space(20)
      endif
      this.w_DBARTCOM = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DESART = space(40)
      this.w_TIPRIG = space(1)
      this.w_DTOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG='R' AND (.w_DTOBS1>.w_UTDC OR EMPTY(.w_DTOBS1)) And Nvl(LookTab('ART_ICOL','ARKITIMB','ARCODART',.w_DBARTCOM),'N')$'RP'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo non di tipo imballo oppure obsoleto")
        endif
        this.w_DBCODCOM = space(20)
        this.w_DBARTCOM = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DESART = space(40)
        this.w_TIPRIG = space(1)
        this.w_DTOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CA__TIPO as CA__TIPO202"+ ",link_2_2.CADTOBSO as CADTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DISTBASE.DBCODCOM=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DISTBASE.DBCODCOM=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DBARTCOM
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBARTCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBARTCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DBARTCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DBARTCOM)
            select ARCODART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBARTCOM = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DBARTCOM = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBARTCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.AROPERAT as AROPERAT204"+ ",link_2_4.ARMOLTIP as ARMOLTIP204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DISTBASE.DBARTCOM=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DISTBASE.DBARTCOM=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DBUNIMIS
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DBUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_DBUNIMIS)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DBUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_DBUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_DBUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
        this.w_MODUM2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.UMCODICE as UMCODICE215"+ ",link_2_15.UMFLFRAZ as UMFLFRAZ215"+ ",link_2_15.UMMODUM2 as UMMODUM2215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on DISTBASE.DBUNIMIS=link_2_15.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and DISTBASE.DBUNIMIS=link_2_15.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDBCODICE_1_1.value==this.w_DBCODICE)
      this.oPgFrm.Page1.oPag.oDBCODICE_1_1.value=this.w_DBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_2.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_2.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBNOTAGG_1_19.value==this.w_DBNOTAGG)
      this.oPgFrm.Page1.oPag.oDBNOTAGG_1_19.value=this.w_DBNOTAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_2.value==this.w_DBCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_2.value=this.w_DBCODCOM
      replace t_DBCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_14.value==this.w_DBDESCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_14.value=this.w_DBDESCOM
      replace t_DBDESCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBDESCOM_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_17.value==this.w_DBQTADIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_17.value=this.w_DBQTADIS
      replace t_DBQTADIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_17.value
    endif
    cp_SetControlsValueExtFlds(this,'DISMBASE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDBCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DBCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM))));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPRIG='R' AND (.w_DTOBS1>.w_UTDC OR EMPTY(.w_DTOBS1)) And Nvl(LookTab('ART_ICOL','ARKITIMB','ARCODART',.w_DBARTCOM),'N')$'RP') and not(empty(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBCODCOM_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Articolo non di tipo imballo oppure obsoleto")
        case   (empty(.w_DBUNIMIS) or not(CHKUNIMI(.w_DBUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDBUNIMIS_2_15
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   (empty(.w_DBQTADIS) or not(.w_DBQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_DBQTADIS=INT(.w_DBQTADIS)))) and (NOT EMPTY(.w_DBCODCOM)) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDBQTADIS_2_17
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   empty(.w_DBINIVAL) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDBINIVAL_2_27
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_DBFINVAL) and (not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDBFINVAL_2_28
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data di fine validit� minore o uguale alla data di inizio validit�")
      endcase
      if not(Empty(.w_CPROWORD)) AND (NOT EMPTY(.w_DBCODCOM) OR (.w_DBFLVARC='S' AND NOT EMPTY(.w_DBDESCOM)))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DBCODCOM = this.w_DBCODCOM
    this.o_DBPERSFR = this.w_DBPERSFR
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND (NOT EMPTY(t_DBCODCOM) OR (t_DBFLVARC='S' AND NOT EMPTY(t_DBDESCOM))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DBCODCOM=space(20)
      .w_OPERA3=space(1)
      .w_DBARTCOM=space(20)
      .w_TIPRIG=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DESART=space(40)
      .w_UNMIS3=space(3)
      .w_MOLTI3=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_DBDESCOM=space(40)
      .w_DBUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_DBQTADIS=0
      .w_DBFLESPL=space(1)
      .w_DBPERSCA=0
      .w_DBRECSCA=0
      .w_DBPERSFR=0
      .w_DBRECSFR=0
      .w_DBPERRIC=0
      .w_DBDATULT=ctod("  /  /  ")
      .w_DB__NOTE=space(0)
      .w_DBFLVARC=space(1)
      .w_DBINIVAL=ctod("  /  /  ")
      .w_DBFINVAL=ctod("  /  /  ")
      .w_DBCOEUM1=0
      .w_DBRIFFAS=0
      .w_DESFAS=space(40)
      .w_DBFLVARI=space(1)
      .w_DBPERSCR=0
      .w_DBVALSCR=0
      .w_DBFLOMAG=space(1)
      .w_MODUM2=space(1)
      .DoRTCalc(1,11,.f.)
      if not(empty(.w_DBCODCOM))
        .link_2_2('Full')
      endif
      .DoRTCalc(12,13,.f.)
      if not(empty(.w_DBARTCOM))
        .link_2_4('Full')
      endif
      .DoRTCalc(14,22,.f.)
        .w_DBDESCOM = .w_DESART
        .w_DBUNIMIS = .w_UNMIS1
      .DoRTCalc(24,24,.f.)
      if not(empty(.w_DBUNIMIS))
        .link_2_15('Full')
      endif
      .DoRTCalc(25,25,.f.)
        .w_DBQTADIS = 1
        .w_DBFLESPL = ' '
      .DoRTCalc(28,36,.f.)
        .w_DBFLVARC = .w_DBFLVARI
      .DoRTCalc(38,40,.f.)
        .w_DBINIVAL = i_DATSYS
        .w_DBFINVAL = i_FINDAT
      .DoRTCalc(43,43,.f.)
        .w_DBCOEUM1 = CALQTA(.w_DBQTADIS,.w_DBUNIMIS, .w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', .w_FLFRAZ, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3, g_PERPQD, 'D')
      .DoRTCalc(45,46,.f.)
        .w_DBFLVARI = ' '
      .DoRTCalc(48,54,.f.)
        .w_DBFLOMAG = 'X'
    endwith
    this.DoRTCalc(56,57,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DBCODCOM = t_DBCODCOM
    this.w_OPERA3 = t_OPERA3
    this.w_DBARTCOM = t_DBARTCOM
    this.w_TIPRIG = t_TIPRIG
    this.w_DTOBS1 = t_DTOBS1
    this.w_DESART = t_DESART
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTI3 = t_MOLTI3
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_OPERAT = t_OPERAT
    this.w_MOLTIP = t_MOLTIP
    this.w_DBDESCOM = t_DBDESCOM
    this.w_DBUNIMIS = t_DBUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_DBQTADIS = t_DBQTADIS
    this.w_DBFLESPL = t_DBFLESPL
    this.w_DBPERSCA = t_DBPERSCA
    this.w_DBRECSCA = t_DBRECSCA
    this.w_DBPERSFR = t_DBPERSFR
    this.w_DBRECSFR = t_DBRECSFR
    this.w_DBPERRIC = t_DBPERRIC
    this.w_DBDATULT = t_DBDATULT
    this.w_DB__NOTE = t_DB__NOTE
    this.w_DBFLVARC = t_DBFLVARC
    this.w_DBINIVAL = t_DBINIVAL
    this.w_DBFINVAL = t_DBFINVAL
    this.w_DBCOEUM1 = t_DBCOEUM1
    this.w_DBRIFFAS = t_DBRIFFAS
    this.w_DESFAS = t_DESFAS
    this.w_DBFLVARI = t_DBFLVARI
    this.w_DBPERSCR = t_DBPERSCR
    this.w_DBVALSCR = t_DBVALSCR
    this.w_DBFLOMAG = t_DBFLOMAG
    this.w_MODUM2 = t_MODUM2
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DBCODCOM with this.w_DBCODCOM
    replace t_OPERA3 with this.w_OPERA3
    replace t_DBARTCOM with this.w_DBARTCOM
    replace t_TIPRIG with this.w_TIPRIG
    replace t_DTOBS1 with this.w_DTOBS1
    replace t_DESART with this.w_DESART
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_OPERAT with this.w_OPERAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_DBDESCOM with this.w_DBDESCOM
    replace t_DBUNIMIS with this.w_DBUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_DBQTADIS with this.w_DBQTADIS
    replace t_DBFLESPL with this.w_DBFLESPL
    replace t_DBPERSCA with this.w_DBPERSCA
    replace t_DBRECSCA with this.w_DBRECSCA
    replace t_DBPERSFR with this.w_DBPERSFR
    replace t_DBRECSFR with this.w_DBRECSFR
    replace t_DBPERRIC with this.w_DBPERRIC
    replace t_DBDATULT with this.w_DBDATULT
    replace t_DB__NOTE with this.w_DB__NOTE
    replace t_DBFLVARC with this.w_DBFLVARC
    replace t_DBINIVAL with this.w_DBINIVAL
    replace t_DBFINVAL with this.w_DBFINVAL
    replace t_DBCOEUM1 with this.w_DBCOEUM1
    replace t_DBRIFFAS with this.w_DBRIFFAS
    replace t_DESFAS with this.w_DESFAS
    replace t_DBFLVARI with this.w_DBFLVARI
    replace t_DBPERSCR with this.w_DBPERSCR
    replace t_DBVALSCR with this.w_DBVALSCR
    replace t_DBFLOMAG with this.w_DBFLOMAG
    replace t_MODUM2 with this.w_MODUM2
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsva_mkiPag1 as StdContainer
  Width  = 583
  height = 366
  stdWidth  = 583
  stdheight = 366
  resizeXpos=283
  resizeYpos=294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDBCODICE_1_1 as StdField with uid="KIDIKRLIGV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DBCODICE", cQueryName = "DBCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice del kit: dovr� essere associato all'articolo gestito a kit",;
    HelpContextID = 264832123,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=75, Top=6, InputMask=replicate('X',20)

  add object oDBDESCRI_1_2 as StdField with uid="QWYICCNFWF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del kit",;
    HelpContextID = 179246207,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=232, Top=6, InputMask=replicate('X',40)


  add object oObj_1_16 as cp_runprogram with uid="ZXOIPZZCYZ",left=207, top=401, width=202,height=22,;
    caption='GSAR_BEL',;
   bGlobalFont=.t.,;
    prg="GSAR_BEL('D')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 92539470

  add object oDBNOTAGG_1_19 as StdMemo with uid="TCLBLEFAUA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DBNOTAGG", cQueryName = "DBNOTAGG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note aggiuntive",;
    HelpContextID = 120998787,;
   bGlobalFont=.t.,;
    Height=70, Width=503, Left=75, Top=33


  add object LegQta as cp_calclbl with uid="CNMWASNEBB",left=75, top=106, width=503,height=42,;
    caption='LegQta',;
   bGlobalFont=.t.,;
    caption="LegQta",;
    nPag=1;
    , HelpContextID = 181233590


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=18, top=153, width=550,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Posiz.",Field2="DBCODCOM",Label2="Articolo imballo",Field3="DBDESCOM",Label3="Descrizione",Field4="DBQTADIS",Label4="Quantit� (*)",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 183715974

  add object oStr_1_11 as StdString with uid="HGKCEJRURI",Visible=.t., Left=25, Top=6,;
    Alignment=1, Width=49, Height=18,;
    Caption="Kit:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="GMJWIQYXFU",Visible=.t., Left=0, Top=32,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=8,top=172,;
    width=546+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=9,top=173,width=545+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDBCODCOM_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsva_mkiBodyRow as CPBodyRowCnt
  Width=536
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="HLMPAMRDEF",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di elaborazione del componente associato al kit",;
    HelpContextID = 117047914,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oDBCODCOM_2_2 as StdTrsField with uid="JXLDNFGVBT",rtseq=11,rtrep=.t.,;
    cFormVar="w_DBCODCOM",value=space(20),;
    ToolTipText = "Codice imballo",;
    HelpContextID = 164168835,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Articolo non di tipo imballo oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=51, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DBCODCOM"

  func oDBCODCOM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODCOM_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDBCODCOM_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDBCODCOM_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli imballo",'GSVA_MKI.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDBCODCOM_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DBCODCOM
    i_obj.ecpSave()
  endproc

  add object oDBDESCOM_2_14 as StdTrsField with uid="OLKSEPMULL",rtseq=23,rtrep=.t.,;
    cFormVar="w_DBDESCOM",value=space(40),;
    HelpContextID = 179246211,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=251, Left=200, Top=0, InputMask=replicate('X',40)

  add object oDBQTADIS_2_17 as StdTrsField with uid="SSRZORVMMZ",rtseq=26,rtrep=.t.,;
    cFormVar="w_DBQTADIS",value=0,;
    ToolTipText = "La quantit� indica il numero di articoli che contiene l'imballo espressi nella prima unit� di misura",;
    HelpContextID = 90250103,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=455, Top=0, cSayPict=[v_PQ(11)], cGetPict=[v_GQ(11)]

  func oDBQTADIS_2_17.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DBCODCOM))
    endwith
  endfunc

  func oDBQTADIS_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DBQTADIS<>0 AND (.w_FLFRAZ<>'S' OR .w_DBQTADIS=INT(.w_DBQTADIS)))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="DBDISKIT='I'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".DBCODICE=DISTBASE.DBCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsva_mki','DISMBASE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DBCODICE=DISMBASE.DBCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsva_mki
proc Stampa(obj)
   do gsar_ski with 'I'
endproc

* --- Fine Area Manuale
