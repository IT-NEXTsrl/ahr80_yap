* --- Container for functions
* --- START GETOBJXML
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getobjxml                                                       *
*              CALCOLA VALORI OGGETTI XML                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_76]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-23                                                      *
* Last revis.: 2018-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getobjxml
param pOBJ,pCODSTR,pSERIAL,pCODTAB,pSERIMP,pVerbose,pLog,pSEARCH,pIDCHLD

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NUM_ATTR
  m.w_NUM_ATTR=0
  private w_VALORE
  m.w_VALORE=space(100)
  private w_NODO
  m.w_NODO=space(100)
  private w_NUM_CHILD
  m.w_NUM_CHILD=0
  private w_OK
  m.w_OK=.f.
  private w_CODENT
  m.w_CODENT=space(15)
  private w_FLATAB
  m.w_FLATAB=space(15)
  private w_FL_AZI
  m.w_FL_AZI=space(1)
  private w_STFUNRIC
  m.w_STFUNRIC=space(30)
  private w_LFLATAB
  m.w_LFLATAB=space(15)
  private w_STTIPFIL
  m.w_STTIPFIL=space(1)
  private w_STSEPARA
  m.w_STSEPARA=space(1)
  private w_TABPAD
  m.w_TABPAD=space(30)
  private w_VALORET
  m.w_VALORET=space(100)
  private w_OKTAG
  m.w_OKTAG=.f.
  private w_CODELE
  m.w_CODELE=space(20)
  private w_INISEZ
  m.w_INISEZ=space(1)
  private w_CONCAT
  m.w_CONCAT=0
  private w_ELIDCHLD
  m.w_ELIDCHLD=0
  private w_CURSORE
  m.w_CURSORE=space(10)
  private w_ORDASS
  m.w_ORDASS=0
  private w_DFSEZINI
  m.w_DFSEZINI=0
  private w_DFSEZFIN
  m.w_DFSEZFIN=0
  private w_ELMEMDAT
  m.w_ELMEMDAT=space(1)
  private w_TIPELE
  m.w_TIPELE=space(1)
  private w_ELEROT
  m.w_ELEROT=space(20)
  private w_FLUTIL
  m.w_FLUTIL=space(1)
  private w_NODO_PADRE
  m.w_NODO_PADRE=space(100)
  private w_LEN
  m.w_LEN=0
  private w_POS
  m.w_POS=0
  private w_CARIMP
  m.w_CARIMP=space(1)
  private w_ALLIMP
  m.w_ALLIMP=space(1)
  private w_ELLUNMAX
  m.w_ELLUNMAX=0
  private w_ELEXPRE1
  m.w_ELEXPRE1=space(254)
  private w_MASK
  m.w_MASK=space(50)
  private w_SEPARA
  m.w_SEPARA=space(1)
  private w_PARTEIN
  m.w_PARTEIN=0
  private w_PHNAME
  m.w_PHNAME=space(30)
  private w_TIPCAM
  m.w_TIPCAM=space(1)
  private w_CODTAB
  m.w_CODTAB=space(30)
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_INDICE
  m.w_INDICE=0
  private w_DECIMAL
  m.w_DECIMAL=0
  private w_LENGHT
  m.w_LENGHT=0
  private w_SEGNO
  m.w_SEGNO=space(1)
  private w_TAB_GES
  m.w_TAB_GES=.f.
  private NoName
  m.NoName=space(0)
  private w_CAMPI
  m.w_CAMPI=space(30)
  private w_RIGADOC
  m.w_RIGADOC=0
  private w_TABPAD
  m.w_TABPAD=space(30)
  private w_RIGAPAD
  m.w_RIGAPAD=0
  private w_LENSEZ
  m.w_LENSEZ=0
  private w_LENARRSEZ
  m.w_LENARRSEZ=0
  private w_TRFLALLT
  m.w_TRFLALLT=space(1)
  private w_CODTRA
  m.w_CODTRA=space(10)
  private w_CODRAG
  m.w_CODRAG=space(10)
  private w_CODENT
  m.w_CODENT=space(10)
  private w_BATCH
  m.w_BATCH=space(30)
  private w_TIPTRA
  m.w_TIPTRA=space(1)
  private w_CODCON
  m.w_CODCON=space(15)
  private w_TIPCON
  m.w_TIPCON=space(1)
  private w_PRVALCAR
  m.w_PRVALCAR=space(10)
  private w_TAB_TIP
  m.w_TAB_TIP=space(8)
  private w_CAMPO_TIP
  m.w_CAMPO_TIP=space(8)
  private w_TMP_VAL
  m.w_TMP_VAL=space(30)
  private w_MVSERIAL
  m.w_MVSERIAL=space(10)
  private w_OBJ_CHILD
  m.w_OBJ_CHILD = .null.
* --- WorkFile variables
  private XDC_FIELDS_idx
  XDC_FIELDS_idx=0
  private VAELEMEN_idx
  VAELEMEN_idx=0
  private VATRASCO_idx
  VATRASCO_idx=0
  private VASTRUTT_idx
  VASTRUTT_idx=0
  private TRS_DETT_idx
  TRS_DETT_idx=0
  private TRS_MAST_idx
  TRS_MAST_idx=0
  private CONTI_idx
  CONTI_idx=0
  private TMP_TAGELE_idx
  TMP_TAGELE_idx=0
  private FLATMAST_idx
  FLATMAST_idx=0
  private FLATDETT_idx
  FLATDETT_idx=0
  private VAPREDEF_idx
  VAPREDEF_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getobjxml"
if vartype(__getobjxml_hook__)='O'
  __getobjxml_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getobjxml('+Transform(pOBJ)+','+Transform(pCODSTR)+','+Transform(pSERIAL)+','+Transform(pCODTAB)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+','+Transform(pSEARCH)+','+Transform(pIDCHLD)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getobjxml')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getobjxml_OpenTables()
  getobjxml_Page_1()
endif
cp_CloseFuncTables()
if used('_Curs_TMP_TAGELE')
  use in _Curs_TMP_TAGELE
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getobjxml('+Transform(pOBJ)+','+Transform(pCODSTR)+','+Transform(pSERIAL)+','+Transform(pCODTAB)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+','+Transform(pSEARCH)+','+Transform(pIDCHLD)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getobjxml')
Endif
*--- Activity log
if vartype(__getobjxml_hook__)='O'
  __getobjxml_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getobjxml_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Estrapola valori (attributi,valore nodo) dall'oggetto passato e dai relativi figli
  * --- Oggetto padre
  * --- Codice struttura
  * --- Seriale documento
  * --- Tabella primo elemento 
  * --- Seriale importazione
  * --- flag verbose
  * --- Log errori
  * --- No ricerca elemento
  * --- Id figlio di padre di tipo scelta
  m.pIDCHLD = IIF(TYPE("pIDCHLD")="N", m.pIDCHLD, 0)
  m.w_OK = .T.
  if Type("pOBJ")="O"
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[VASTRUTT_idx,2],.t.,VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STCODENT,STFLATAB,STFUNRIC,STSEPARA,STTIPFIL"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(m.pCODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STCODENT,STFLATAB,STFUNRIC,STSEPARA,STTIPFIL;
        from (i_cTable) where;
            STCODICE = m.pCODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      m.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      m.w_STFUNRIC = NVL(cp_ToDate(_read_.STFUNRIC),cp_NullValue(_read_.STFUNRIC))
      m.w_STSEPARA = NVL(cp_ToDate(_read_.STSEPARA),cp_NullValue(_read_.STSEPARA))
      m.w_STTIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from FLATMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[FLATMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[FLATMAST_idx,2],.t.,FLATMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FTFL_AZI"+;
        " from "+i_cTable+" FLATMAST where ";
            +"FTCODICE = "+cp_ToStrODBC(m.w_FLATAB);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FTFL_AZI;
        from (i_cTable) where;
            FTCODICE = m.w_FLATAB;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_FL_AZI = NVL(cp_ToDate(_read_.FTFL_AZI),cp_NullValue(_read_.FTFL_AZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    m.w_LFLATAB = alltrim(m.w_FLATAB)
    m.w_FLATAB = IIF(m.w_FL_AZI="S","xxx"+Alltrim(m.w_FLATAB),alltrim(m.w_FLATAB))
    if Type("pOBJ.Attributes.length") ="N"
      if m.pOBJ.Attributes.length>0
        m.w_NUM_ATTR = 0
        do while m.w_NUM_ATTR<m.pOBJ.Attributes.Length
          m.w_NODO = m.pOBJ.Attributes.item(m.w_NUM_ATTR).basename
          m.w_VALORE = m.pOBJ.Attributes.item(m.w_NUM_ATTR).text
          getobjxml_Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          m.w_NUM_ATTR = m.w_NUM_ATTR + 1
        enddo
      endif
    endif
    if m.pOBJ.HasChildNodes
      * --- Ha ulteriori figli riapplico la funzione ricorsivamente
      m.w_NUM_CHILD = 0
      do while m.w_NUM_CHILD<m.pOBJ.childnodes.Length
        m.w_OBJ_CHILD = m.pOBJ.childnodes.item(m.w_NUM_CHILD)
        m.w_NODO = m.w_OBJ_CHILD.basename
        if Not Empty(m.w_NODO)
          if (m.w_OBJ_CHILD.HasChildNodes or Type("m.w_OBJ_CHILD.Attributes.length") ="N") 
            m.w_TABPAD = Space(30)
            if Empty(m.pCODTAB)
              * --- Read from VAELEMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[VAELEMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ELTABIMP"+;
                  " from "+i_cTable+" VAELEMEN where ";
                      +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                      +" and ELVALTXT = "+cp_ToStrODBC(m.w_NODO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ELTABIMP;
                  from (i_cTable) where;
                      ELCODSTR = m.pCODSTR;
                      and ELVALTXT = m.w_NODO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                m.w_TABPAD = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_rows>1
                m.w_TABPAD = Space(30)
              endif
            else
              m.w_TABPAD = m.pCODTAB
            endif
             
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_FUN="m.w_OK = "+ALLTRIM(m.w_STFUNRIC)+" ( m.w_OBJ_CHILD, ALLTRIM(m.pCODSTR),ALLTRIM(m.pSERIAL),ALLTRIM(m.w_TABPAD),"+; 
 "ALLTRIM(m.pSERIMP),m.pVERBOSE,m.pLOG,m.pSearch)" 
 &L_FUN 
 on error &L_OldError 
            if L_err
              addMsg("%0Errore nell'esecuzione della funzione ricorsiva: %1" ,m.pLOG,Message()+Chr(13)+Message(1))
              m.w_OK = .F.
            endif
            if Not m.w_OK
              i_retcode = 'stop'
              i_retval = .f.
              return
            endif
          else
            m.w_VALORE = m.w_OBJ_CHILD.Xml
            m.w_VALORET = m.w_OBJ_CHILD.Text
            m.w_NODO = m.w_OBJ_CHILD.BaseName
            getobjxml_Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          m.w_VALORE = m.w_OBJ_CHILD.Xml
          m.w_VALORET = m.w_OBJ_CHILD.Text
          m.w_NODO = m.pOBJ.BaseName
          getobjxml_Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        m.w_NUM_CHILD = m.w_NUM_CHILD + 1
      enddo
    else
      m.w_NODO = m.pOBJ.BaseName
      * --- Utilizzo xml perch� il text toglie gli spazi a sinistra
      m.w_VALORE = m.pOBJ.Xml
      getobjxml_Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    i_retcode = 'stop'
    i_retval = .t.
    return
  else
    i_retcode = 'stop'
    i_retval = .f.
    return
  endif
endproc


procedure getobjxml_Page_2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Ricavo elemento dal nome base del nodo
  m.w_OKTAG = .F.
  m.w_CODTAB = m.pCODTAB
  m.w_CURSORE = sys(2015)
  if Not m.pSEARCH
    * --- Select from TMP_TAGELE
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2],.t.,TMP_TAGELE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_TAGELE ";
          +" where FLUTILI='N' AND TAGELE="+cp_ToStrODBC(m.w_NODO)+" AND TABELLA="+cp_ToStrODBC(m.w_CODTAB)+"";
          +" order by INDEN";
           ,"_Curs_TMP_TAGELE")
    else
      select * from (i_cTable);
       where FLUTILI="N" AND TAGELE=m.w_NODO AND TABELLA=m.w_CODTAB;
       order by INDEN;
        into cursor _Curs_TMP_TAGELE
    endif
    if used('_Curs_TMP_TAGELE')
      select _Curs_TMP_TAGELE
      locate for 1=1
      do while not(eof())
      m.w_ELIDCHLD = _Curs_TMP_TAGELE.ELIDCHLD
      if m.pIDCHLD=0 OR m.pIDCHLD = m.w_ELIDCHLD
        m.w_CODELE = _Curs_TMP_TAGELE.CODICE
        m.w_TIPELE = _Curs_TMP_TAGELE.TIPELE
        m.w_INISEZ = _Curs_TMP_TAGELE.INISEZ
        m.w_CODTAB = _Curs_TMP_TAGELE.TABELLA
        m.w_CAMPO = _Curs_TMP_TAGELE.ELCAMIMP
        m.w_TIPTRA = _Curs_TMP_TAGELE.ELTIPTR2
        m.w_BATCH = _Curs_TMP_TAGELE.EL_BATC2
        m.w_SEGNO = _Curs_TMP_TAGELE.EL_SEGNO
        m.w_CONCAT = _Curs_TMP_TAGELE.NUCONC
        m.w_ORDASS = Nvl(_Curs_TMP_TAGELE.INDEN,0)
        m.w_DFSEZINI = NVL(_Curs_TMP_TAGELE.DFSEZINI,0)
        m.w_DFSEZFIN = NVL(_Curs_TMP_TAGELE.DFSEZFIN,0)
        m.w_ELMEMDAT = NVL(_Curs_TMP_TAGELE.ELMEMDAT,0)
        m.w_OKTAG = .T.
        Exit
      endif
        select _Curs_TMP_TAGELE
        continue
      enddo
      use
    endif
    if m.w_OKTAG
      * --- Write into TMP_TAGELE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,TAGELE"
        do vq_exec with 'GSVA_QOA',createobject('cp_getfuncvar'),.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 set ";
        +"TMP_TAGELE.FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +Iif(Empty(i_ccchkf),"",",TMP_TAGELE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_TAGELE.CODICE = t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = t2.TAGELE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set (";
            +"FLUTILI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".TAGELE = "+i_cQueryTable+".TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Se esco dalla select senza aver trovato un elemento tag 
      *     siginifica che ho utilizzato tutti e sono ritornato all'elemento iniziale
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ELCAMIMP,ELTABIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELINISEZ,ELCODICE,ELCARIMP,ELALLIMP"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and ELVALTXT = "+cp_ToStrODBC(m.w_NODO);
              +" and ELINISEZ = "+cp_ToStrODBC("S");
              +" and ELTABIMP = "+cp_ToStrODBC(m.pCODTAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ELCAMIMP,ELTABIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELINISEZ,ELCODICE,ELCARIMP,ELALLIMP;
          from (i_cTable) where;
              ELCODSTR = m.pCODSTR;
              and ELVALTXT = m.w_NODO;
              and ELINISEZ = "S";
              and ELTABIMP = m.pCODTAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_CAMPO = NVL(cp_ToDate(_read_.ELCAMIMP),cp_NullValue(_read_.ELCAMIMP))
        m.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
        m.w_TIPTRA = NVL(cp_ToDate(_read_.ELTIPTR2),cp_NullValue(_read_.ELTIPTR2))
        m.w_BATCH = NVL(cp_ToDate(_read_.EL_BATC2),cp_NullValue(_read_.EL_BATC2))
        m.w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
        m.w_INISEZ = NVL(cp_ToDate(_read_.ELINISEZ),cp_NullValue(_read_.ELINISEZ))
        m.w_CODELE = NVL(cp_ToDate(_read_.ELCODICE),cp_NullValue(_read_.ELCODICE))
        m.w_CARIMP = NVL(cp_ToDate(_read_.ELCARIMP),cp_NullValue(_read_.ELCARIMP))
        m.w_ALLIMP = NVL(cp_ToDate(_read_.ELALLIMP),cp_NullValue(_read_.ELALLIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  else
    m.w_NODO = SUBSTR(m.w_NODO,2)
    m.w_CAMPO = Space(30)
     
 Cursore=m.w_CURSORE 
 Do vq_exec with "..\VEFA\EXE\QUERY\GSVA2QTA",createobject("cp_getfuncvar"),Cursore,"",.f.,.t. 
 SELECT (Cursore) 
 go top 
 Scan
    m.w_ELIDCHLD = &cursore..ELIDCHLD
    if m.pIDCHLD=0 OR m.pIDCHLD = m.w_ELIDCHLD
      m.w_CODELE = &cursore..CODICE
      m.w_TIPELE = &cursore..TIPELE
      m.w_CODTAB = &cursore..TABELLA
      m.w_CAMPO = &cursore..ELCAMIMP
      m.w_TIPTRA = &cursore..ELTIPTR2
      m.w_BATCH = &cursore..EL_BATC2
      m.w_SEGNO = &cursore..EL_SEGNO
      m.w_CONCAT = &cursore..NUCONC
      m.w_DFSEZINI = NVL(&cursore..DFSEZINI,0)
      m.w_DFSEZFIN = NVL(&cursore..DFSEZFIN,0)
      m.w_ELMEMDAT = NVL(&cursore..ELMEMDAT," ")
      m.w_OKTAG = .T.
      Exit
    endif
     
 endscan 
 use in (cursore)
    m.w_FLUTIL = "N"
    m.w_ELEROT = Space(20)
    if Not Empty(Nvl(m.w_CAMPO," "))
      m.w_NODO_PADRE = Substr(m.pOBJ.parentNode.NodeName,2)
      * --- Read from TMP_TAGELE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2],.t.,TMP_TAGELE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INISEZ,FLUTILI,ELEROT"+;
          " from "+i_cTable+" TMP_TAGELE where ";
              +"CODICE = "+cp_ToStrODBC(m.w_NODO_PADRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INISEZ,FLUTILI,ELEROT;
          from (i_cTable) where;
              CODICE = m.w_NODO_PADRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_INISEZ = NVL(cp_ToDate(_read_.INISEZ),cp_NullValue(_read_.INISEZ))
        m.w_FLUTIL = NVL(cp_ToDate(_read_.FLUTILI),cp_NullValue(_read_.FLUTILI))
        m.w_ELEROT = NVL(cp_ToDate(_read_.ELEROT),cp_NullValue(_read_.ELEROT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Valorizzo elemento di rottura
      if m.w_FLUTIL="S"
        if m.w_ELEROT=m.w_CODELE
          m.w_INISEZ = "S"
        else
          m.w_INISEZ = "N"
        endif
      else
        m.w_ELEROT = IIF(m.w_INISEZ="S",m.w_CODELE,Space(20))
      endif
    endif
  endif
  if m.w_INISEZ="S" and Not Empty(m.w_CAMPO)
    m.w_POS = cp_ROUND(ASCAN(ArProg,Alltrim(m.w_CODTAB))/2,0)
    if m.w_POS>0
      m.w_INDICE = ArProg[m.w_POS,2] + 1
    else
      * --- Inserisco prima occorrenza del campo
      m.w_LEN = int(Alen(ArProg)/2)
       
 DIMENSION ArProg(m.w_LEN+1,2) 
 ArProg[m.w_LEN+1,1]=m.w_CODTAB
      m.w_INDICE = 1
      m.w_POS = m.w_LEN+1
    endif
    * --- Aggiorno array progressivi 
    ArProg[m.w_POS,2] =m.w_INDICE
    * --- smarco elementi tag utilizzati della tabella
    if Not Empty(m.w_CODTAB)
      * --- Write into TMP_TAGELE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
      i_ccchkf=''
      func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("N"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf ;
        +" where ";
            +"TABELLA = "+cp_ToStrODBC(m.w_CODTAB);
               )
      else
        update (i_cTable) set;
            FLUTILI = "N";
            &i_ccchkf. ;
         where;
            TABELLA = m.w_CODTAB;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  else
    if Not Empty(m.w_CODTAB)
      m.w_POS = cp_ROUND(ASCAN(ArProg,Alltrim(m.w_CODTAB))/2,0)
      if m.w_POS>0
        m.w_INDICE = ArProg[m.w_POS,2]
      endif
    endif
  endif
  * --- Marco elemento tag utilizzato
  if Not m.pSEARCH
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(m.w_CODELE);
          +" and TAGELE = "+cp_ToStrODBC(m.w_NODO);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          &i_ccchkf. ;
       where;
          CODICE = m.w_CODELE;
          and TAGELE = m.w_NODO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  else
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
      +",ELEROT = "+cp_NullLink(cp_ToStrODBC(m.w_ELEROT),'TMP_TAGELE','ELEROT');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(m.w_NODO_PADRE);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          ,ELEROT = m.w_ELEROT;
          &i_ccchkf. ;
       where;
          CODICE = m.w_NODO_PADRE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endif
  if Not Empty(Nvl(m.w_CAMPO," ")) and Not Empty(Nvl(m.w_CODTAB," "))
    * --- Read from FLATDETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[FLATDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[FLATDETT_idx,2],.t.,FLATDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FTFLDDIM,FTFLDDEC,FTFLDTYP"+;
        " from "+i_cTable+" FLATDETT where ";
            +"FTCODICE = "+cp_ToStrODBC(m.w_LFLATAB);
            +" and FTTABNAM = "+cp_ToStrODBC(m.w_CODTAB);
            +" and FTFLDNAM = "+cp_ToStrODBC(m.w_CAMPO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FTFLDDIM,FTFLDDEC,FTFLDTYP;
        from (i_cTable) where;
            FTCODICE = m.w_LFLATAB;
            and FTTABNAM = m.w_CODTAB;
            and FTFLDNAM = m.w_CAMPO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_LENGHT = NVL(cp_ToDate(_read_.FTFLDDIM),cp_NullValue(_read_.FTFLDDIM))
      m.w_DECIMAL = NVL(cp_ToDate(_read_.FTFLDDEC),cp_NullValue(_read_.FTFLDDEC))
      m.w_TIPCAM = NVL(cp_ToDate(_read_.FTFLDTYP),cp_NullValue(_read_.FTFLDTYP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if m.w_TIPCAM $ "C-N"
      m.w_VALORE = m.w_VALORET
    endif
    if VarType( m.pLog ) ="O"
      AddMsgNL("Import tabella %2 campo: %1", m.pLog ,ALLTRIM(m.w_CAMPO),ALLTRIM(m.w_CODTAB))
      Inkey(0.001,"H")
    endif
    do case
      case m.w_ALLIMP="D"
        m.w_VALORE = LTRIM(m.w_VALORE, 1 , m.w_CARIMP )
      case m.w_ALLIMP="S"
        m.w_VALORE = RTRIM(m.w_VALORE, 1 , m.w_CARIMP )
    endcase
    getobjxml_Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not m.pSEARCH
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EL__MASK,ELEXPRE1,ELLUNMAX"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and ELVALTXT = "+cp_ToStrODBC(m.w_NODO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EL__MASK,ELEXPRE1,ELLUNMAX;
          from (i_cTable) where;
              ELCODSTR = m.pCODSTR;
              and ELVALTXT = m.w_NODO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_MASK = NVL(cp_ToDate(_read_.EL__MASK),cp_NullValue(_read_.EL__MASK))
        m.w_ELEXPRE1 = NVL(cp_ToDate(_read_.ELEXPRE1),cp_NullValue(_read_.ELEXPRE1))
        m.w_ELLUNMAX = NVL(cp_ToDate(_read_.ELLUNMAX),cp_NullValue(_read_.ELLUNMAX))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EL__MASK,ELEXPRE1,ELLUNMAX"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and ELCODICE = "+cp_ToStrODBC(m.w_NODO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EL__MASK,ELEXPRE1,ELLUNMAX;
          from (i_cTable) where;
              ELCODSTR = m.pCODSTR;
              and ELCODICE = m.w_NODO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_MASK = NVL(cp_ToDate(_read_.EL__MASK),cp_NullValue(_read_.EL__MASK))
        m.w_ELEXPRE1 = NVL(cp_ToDate(_read_.ELEXPRE1),cp_NullValue(_read_.ELEXPRE1))
        m.w_ELLUNMAX = NVL(cp_ToDate(_read_.ELLUNMAX),cp_NullValue(_read_.ELLUNMAX))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case m.w_TIPCAM="D"
        * --- Formato Date
        if at("T",m.w_VALORE)>0
          expr=cp_chartodate(SUBSTR(m.w_VALORE,1,at("T",m.w_VALORE)-1),"yy-mm-dd")
        else
          * --- No formato xml applico formato data in base alla lunghezza del campo
          if Not empty(m.w_MASK)
            expr=cp_chartodate(SUBSTR(m.w_valore,AT("G",m.w_MASK),OCCURS("G",m.w_MASK))+"-"+SUBSTR(m.w_valore,AT("M",m.w_MASK),; 
 OCCURS("M",m.w_MASK))+"-"+SUBSTR(m.w_valore,AT("A",m.w_MASK),OCCURS("A",m.w_MASK)))
          else
            expr=cp_chartodate(SUBSTR(m.w_VALORE,1,m.w_LENGHT),"yyyymmdd")
          endif
        endif
      case m.w_TIPCAM="N"
        * --- Numerici
        m.w_SEPARA = ICASE(m.w_STSEPARA="V",",",m.w_STSEPARA="P","."," ")
        if occur(m.w_SEPARA,m.w_VALORE) =0
          * --- Non ho separatore nel dato del file
          if Not empty(m.w_MASK) and m.w_STSEPARA="N"
            * --- Elimino dalla maschera parte non 9
            m.w_MASK = substr(m.w_MASK,at("9",m.w_MASK))
            if AT(".",m.w_MASK)>0
              m.w_DECIMAL = len(SUBSTR(alltrim(m.w_mask),AT(".",m.w_MASK)+1))
            else
              * --- Ho una maschera che non prevede decimali
              m.w_DECIMAL = 0
            endif
          else
            * --- Non ho specificato una maschera
            m.w_DECIMAL = 0
          endif
          * --- Costruisco importo in base ai decimali ricavati
          m.w_VALORE = SUBSTR(m.w_VALORE,1,LEN(m.w_VALORE)-m.w_DECIMAL) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
        else
          * --- Ho il separatore nel file non devo ricostruire il dato
          m.w_VALORE = STRTRAN(m.w_VALORE,".",",")
          m.w_DECIMAL = len(SUBSTR(m.w_VALORE,AT(",",m.w_VALORE)+1))
        endif
        * --- Determino la parte intera
        *     se non ho separatore � data dal minimo tra lunghezza massima del campo e quella del valore del file
        *     se ho il separatore la ricavo in funzione della  lunghezza dei decimali e dalla posizione del separatore
        m.w_PARTEIN = iif(At(",",m.w_VALORE)<>0,Min(m.w_LENGHT-m.w_DECIMAL,At(",",m.w_VALORE)-1),Min(m.w_LENGHT,Len(m.w_VALORE)))
        if Len(m.w_VALORE)<>At(",",m.w_VALORE)
          if SUBSTR(m.w_VALORE,1,1)="-"
            m.w_VALORE = "-"+right(SUBSTR(m.w_VALORE,1,At(",",m.w_VALORE)-1) ,m.w_PARTEIN-1) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
          else
            m.w_VALORE = right(SUBSTR(m.w_VALORE,1,At(",",m.w_VALORE)-1) ,m.w_PARTEIN) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
          endif
        endif
        do case
          case m.w_SEGNO="P"
            expr=Abs(val(SUBSTR(m.w_VALORE,1,m.w_LENGHT)))
          case m.w_SEGNO="M"
            * --- eseguo troncamento parte intera-1 in base al valore del database
            *     eseguo troncamento parte decimale in base al valore del database
            expr=-Abs(val(m.w_VALORE))
          otherwise
            * --- Devo mantenere il segno che arriva dal file
            expr=val(m.w_VALORE)
        endcase
        expr=Nvl(expr,0)
      case m.w_TIPCAM="C"
         
 expr=SUBSTR(m.w_VALORE,1,m.w_LENGHT)
        if Empty(EXPR)
          if i_dcx.IsFieldNullable(Alltrim(m.w_CODTAB),Alltrim(m.w_CAMPO))
            EXPR=.NULL.
          endif
        else
          * --- Spezzo il dato in base alla lunghezza del campo di destinazione
          EXPR=Left(Nvl(EXPR," "),m.w_LENGHT)
        endif
      otherwise
         
 expr=Nvl(m.w_VALORE," ")
    endcase
    if m.w_ELMEMDAT= "S"
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_test="vartype(g_"+Alltrim(m.w_CAMPO)+")" 
 L_resu=&L_test 
 
      if L_resu="U"
         
 L_VAR="Public  g_"+Alltrim(m.w_CAMPO) 
 &L_VAR
      endif
      L_VAR="g_"+Alltrim(m.w_CAMPO) + " =  EXPR" 
 &L_VAR
      on error &L_OldError 
      if L_err
        addMsg("%0Errore nell'esecuzione della funzione ricorsiva: %1" ,m.pLOG,Message()+Chr(13)+Message(1))
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    else
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_test="vartype(g_"+Alltrim(m.w_CAMPO)+")" 
 L_resu=&L_test 
 
      if L_resu<>"U"
         
 L_VAR="g_"+Alltrim(m.w_CAMPO)
        m.w_VALORE = &L_VAR
      endif
      on error &L_OldError 
      if L_err
        addMsg("%0Errore nell'esecuzione della funzione ricorsiva: %1" ,m.pLOG,Message()+Chr(13)+Message(1))
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    endif
    if Not Empty(m.w_ELEXPRE1)
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t.
      val=m.w_ELEXPRE1 
 EXPR=&val
      on error &L_OldError 
      if L_err
        addMsg("%0Errore nell'esecuzione espressione di import : %1%0%2" ,m.pLOG,Alltrim(m.w_ELEXPRE1),Message()+Chr(13)+Message(1))
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    endif
    Dimension Arrwrite[1,2]
     
 ArrWrite[1,1]=m.w_campo 
 ArrWrite[1,2]=expr
    m.w_TAB_GES = .T.
    m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
    NC="@@"
    do case
      case m.w_CODTAB="DOC_MAST"
        Dimension ArrCampi[3,2],ArrVal[1,2] 
 ArrCampi[1,1]="MVSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="FT___UID" 
 ArrCampi[2,2]=m.pSERIMP 
 ArrCampi[3,1]=m.w_CAMPO 
 ArrCampi[3,2]=EXPR
         
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL
        if m.w_INISEZ="S"
          m.w_CAMPI = "MVSERIAL"
           
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL
          m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
          NC=READTABLE(m.w_PHNAME,m.w_CAMPI,@ARRVAL,,,)
          if Reccount((Nc))>0
            m.w_INISEZ = "N"
          endif
        endif
      case m.w_CODTAB="DOC_DETT"
        if m.w_CAMPO="CPROWORD"
           
 Dimension ArrCampi[5,2] 
 ArrCampi[1,1]="MVSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="CPROWNUM" 
 ArrCampi[2,2]=m.w_INDICE 
 ArrCampi[3,1]="MVNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]=m.w_CAMPO 
 ArrCampi[4,2]=EXPR 
 ArrCampi[5,1]="FT___UID" 
 ArrCampi[5,2]=m.pSERIMP
        else
           
 Dimension ArrCampi[6,2] 
 ArrCampi[1,1]="MVSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="CPROWNUM" 
 ArrCampi[2,2]=m.w_INDICE 
 ArrCampi[3,1]="MVNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]="CPROWORD" 
 ArrCampi[4,2]=m.w_INDICE*10 
 ArrCampi[5,1]=m.w_CAMPO 
 ArrCampi[5,2]=EXPR 
 ArrCampi[6,1]="FT___UID" 
 ArrCampi[6,2]=m.pSERIMP
        endif
         
 Dimension ArrVal[2,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="CPROWNUM" 
 ArrVal[2,2]=m.w_INDICE
      case m.w_CODTAB="DOC_RATE"
         
 Dimension ArrCampi[4,2] 
 ArrCampi[1,1]="RSSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="RSNUMRAT" 
 ArrCampi[2,2]=m.w_INDICE 
 ArrCampi[3,1]=m.w_CAMPO 
 ArrCampi[3,2]=EXPR 
 ArrCampi[4,1]="FT___UID" 
 ArrCampi[4,2]=m.pSERIMP
         
 Dimension ArrVal[2,2] 
 ArrVal[1,1]="RSSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="RSNUMRAT" 
 ArrVal[2,2]=m.w_INDICE
      case m.w_CODTAB="MOVIMATR" AND g_APPLICATION="ad hoc ENTERPRISE"
        * --- Devo leggere ultimo cprownum inserito in doc_dett
        * --- Cerco ultima riga docdett caricata
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DOC_DETT")
         
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL
        MAXROW=READTABLE(m.w_PHNAME,"MAX(CPROWNUM) AS CPROWNUM",@ARRVAL,,,)
        m.w_RIGADOC = &MAXROW..CPROWNUM
        if Used((MAXROW))
           
 Select (MAXROW) 
 use
        endif
        do case
          case m.w_CAMPO="MTKEYSAL"
             
 Dimension ArrCampi[6,2] 
 ArrCampi[1,1]="MTSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="MTROWNUM" 
 ArrCampi[2,2]=m.w_RIGADOC 
 ArrCampi[3,1]="MTNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]="MTCODMAT" 
 ArrCampi[4,2]=SPACE(40) 
 ArrCampi[5,1]=m.w_CAMPO 
 ArrCampi[5,2]=EXPR 
 ArrCampi[6,1]="FT___UID" 
 ArrCampi[6,2]=m.pSERIMP
            m.w_CAMPI = "MTCODMAT,MTKEYSAL"
          case m.w_CAMPO="MTCODMAT"
             
 Dimension ArrCampi[6,2] 
 ArrCampi[1,1]="MTSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="MTROWNUM" 
 ArrCampi[2,2]=m.w_RIGADOC 
 ArrCampi[3,1]="MTNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]="MTKEYSAL" 
 ArrCampi[4,2]=SPACE(20) 
 ArrCampi[5,1]=m.w_CAMPO 
 ArrCampi[5,2]=EXPR 
 ArrCampi[6,1]="FT___UID" 
 ArrCampi[6,2]=m.pSERIMP
            m.w_CAMPI = "MTCODMAT,MTKEYSAL"
          otherwise
             
 Dimension ArrCampi[7,2] 
 ArrCampi[1,1]="MTSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="MTROWNUM" 
 ArrCampi[2,2]=m.w_RIGADOC 
 ArrCampi[3,1]="MTNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]="MTKEYSAL" 
 ArrCampi[4,2]=SPACE(20) 
 ArrCampi[5,1]="MTCODMAT" 
 ArrCampi[5,2]=SPACE(40) 
 ArrCampi[6,1]=m.w_CAMPO 
 ArrCampi[6,2]=EXPR 
 ArrCampi[7,1]="FT___UID" 
 ArrCampi[7,2]=m.pSERIMP
        endcase
         
 Dimension ArrVal[3,2] 
 ArrVal[1,1]="MTSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="MTROWNUM" 
 ArrVal[2,2]=m.w_RIGADOC 
 ArrVal[3,1]="MTNUMRIF" 
 ArrVal[3,2]=-20
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
        NC=READTABLE(m.w_PHNAME,m.w_CAMPI,@ARRVAL,,,)
        if Reccount((Nc))=m.w_INDICE
           
 Dimension ArrVal[5,2] 
 ArrVal[1,1]="MTSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="MTROWNUM" 
 ArrVal[2,2]=m.w_RIGADOC 
 ArrVal[3,1]="MTNUMRIF" 
 ArrVal[3,2]=-20 
 ArrVal[4,1]="MTCODMAT" 
 ArrVal[4,2]=&NC..MTCODMAT 
 ArrVal[5,1]="MTKEYSAL" 
 ArrVal[5,2]=&NC..MTKEYSAL
        endif
        * --- Chiudo cursore read
        if Used((NC))
           
 Select (NC) 
 use
        endif
      case m.w_CODTAB="MOVILOTT" AND g_APPLICATION="ad hoc ENTERPRISE"
        IDXTAB=cp_ROUND(ASCAN(ArProg,"DOC_DETT")/2,0)
        m.w_RIGADOC = IIF(IDXTAB>0,ARPROG[IDXTAB,2],1)
         
 Dimension ArrCampi[6,2] 
 ArrCampi[1,1]="MLSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="MLROWORD" 
 ArrCampi[2,2]=m.w_RIGADOC 
 ArrCampi[3,1]="MLNUMRIF" 
 ArrCampi[3,2]=-20 
 ArrCampi[4,1]="CPROWNUM" 
 ArrCampi[4,2]=m.w_INDICE 
 ArrCampi[5,1]=m.w_CAMPO 
 ArrCampi[5,2]=EXPR 
 ArrCampi[6,1]="FT___UID" 
 ArrCampi[6,2]=m.pSERIMP
         
 Dimension ArrVal[4,2] 
 ArrVal[1,1]="MLSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="MLROWORD" 
 ArrVal[2,2]=m.w_RIGADOC 
 ArrVal[3,1]="MLNUMRIF" 
 ArrVal[3,2]=-20 
 ArrVal[4,1]="CPROWNUM" 
 ArrVal[4,2]=m.w_INDICE
      case m.w_CODTAB="ABBLOTUL" AND g_APPLICATION="ad hoc ENTERPRISE"
        IDXTAB=cp_ROUND(ASCAN(ArProg,"DOC_DETT")/2,0)
        m.w_RIGADOC = IIF(IDXTAB>0,ARPROG[IDXTAB,2],1)
        m.w_TABPAD = Gettabpad(m.pCODSTR, m.w_CODTAB, m.w_CODELE)
        IDXTAB=cp_ROUND(ASCAN(ArProg,m.w_TABPAD)/2,0)
        m.w_RIGAPAD = IIF(IDXTAB>0,ARPROG[IDXTAB,2],1)
         
 Dimension ArrCampi(8,2) 
 ArrCampi[1,1]="ABSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="ABROWDOC" 
 ArrCampi[2,2]=m.w_RIGADOC 
 ArrCampi[3,1]="ABROWORD" 
 ArrCampi[3,2]=m.w_RIGAPAD 
 ArrCampi[4,1]="ABNUMRIF"
         
 Dimension ArrVal(6,2) 
 ArrVal[1,1]="ABSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="ABROWDOC" 
 ArrVal[2,2]=m.w_RIGADOC 
 ArrVal[3,1]="ABROWORD" 
 ArrVal[3,2]=m.w_RIGAPAD 
 ArrVal[4,1]="ABNUMRIF"
        do case
          case m.w_TABPAD="MOVIMATR"
             
 Dimension ReadArr[3,2] 
 ReadVal[1,1]="MTSERIAL" 
 ReadVal[1,2]=m.pSERIAL 
 ReadVal[2,1]="MTROWNUM" 
 ReadVal[2,2]=m.w_RIGADOC 
 ReadVal[3,1]="MTNUMRIF" 
 ReadVal[3,2]=-20
            NC=READTABLE(getclonename(m.w_TABPAD),m.w_CAMPI,@READARR,,,)
             
 ArrCampi[4,2]=-40 
 ArrCampi[5,1]="ABCODMAT" 
 ArrCampi[5,2]=&NC..MTCODMAT 
 ArrCampi[6,1]="CPROWNUM" 
 ArrCampi[6,2]=m.w_INDICE 
 ArrCampi[7,1]=m.w_CAMPO 
 ArrCampi[7,2]=EXPR 
 ArrCampi[8,1]="FT___UID" 
 ArrCampi[8,2]=m.pSERIMP
             
 ArrVal[4,2]=-40 
 ArrVal[5,1]="ABCODMAT" 
 ArrVal[5,2]=&NC..MTCODMAT 
 ArrVal[6,1]="CPROWNUM" 
 ArrCampi[6,2]=m.w_INDICE
            if Used((NC))
              * --- Chiudo cursore
               
 Select (NC) 
 use
            endif
          case m.w_TABPAD="MOVILOTT"
             
 ArrVal[4,2]=-30 
 ArrVal[5,1]="ABCODMAT" 
 ArrVal[5,2]=SPACE(40) 
 ArrVal[6,1]="CPROWNUM" 
 ArrVal[6,2]=m.w_INDICE
             
 ArrCampi[4,2]=-30 
 ArrCampi[5,1]="ABCODMAT" 
 ArrCampi[5,2]=SPACE(40) 
 ArrCampi[6,1]="CPROWNUM" 
 ArrCampi[6,2]=m.w_INDICE 
 ArrCampi[7,1]=m.w_CAMPO 
 ArrCampi[7,2]=EXPR 
 ArrCampi[8,1]="FT___UID" 
 ArrCampi[8,2]=m.pSERIMP
          case m.w_TABPAD="DOC_DETT"
             
 ArrVal[4,2]=-20 
 ArrVal[5,1]="ABCODMAT" 
 ArrVal[5,2]=SPACE(40) 
 ArrVal[6,1]="CPROWNUM" 
 ArrVal[6,2]=m.w_INDICE
             
 ArrCampi[4,2]=-20 
 ArrCampi[5,1]="ABCODMAT" 
 ArrCampi[5,2]=SPACE(40) 
 ArrCampi[6,1]="CPROWNUM" 
 ArrCampi[6,2]=m.w_INDICE 
 ArrCampi[7,1]=m.w_CAMPO 
 ArrCampi[7,2]=EXPR 
 ArrCampi[8,1]="FT___UID" 
 ArrCampi[8,2]=m.pSERIMP
        endcase
      otherwise
        AddMsgNL("Tabella %1, non gestita" , m.pLOG,Alltrim(m.w_CODTAB))
        m.w_TAB_GES = .F.
        i_retcode = 'stop'
        i_retval = .f.
        return
    endcase
    if m.w_TAB_GES
      if m.w_INISEZ="S"
        m.w_INISEZ = "N"
        if Not Empty(INSERTTABLE( m.w_PHNAME , @ArrCampi ,m.pLOG, m.pVerbose ))
          AddMsgNL("Errore in inserimento archivio %1, errore %2" , m.pLOG,m.w_CODTAB , Message()+Chr(13)+Message(1))
           
 i_Error=MSG_UPDATE_ERROR
          i_retcode = 'stop'
          i_retval = .f.
          return
        else
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Inserito campo: %1 della tabella:%2 con valore: %3" , m.pLOG,m.w_CAMPO,m.w_CODTAB , Cp_ToStrOdbc(EXPR))
          endif
        endif
      else
        m.w_LENSEZ = 0
        if m.w_CONCAT>0 and Type("ArrWrite[1,2]")="C"
          * --- Elemento da concatenare 
          NC=READTABLE(m.w_PHNAME,m.w_CAMPO,@ARRVAL,,,)
          if USED((Nc)) AND Reccount((Nc))>0
            m.w_LENSEZ = ASCAN(g_ARRSEZ,Alltrim(m.w_CAMPO),1,-1,1,15)
             
 VAL=ALLTRIM((NC))+"."+ALLTRIM(m.w_CAMPO) 
 VAL=&VAL
            if m.w_TIPCAM="C"
              if m.w_LENSEZ>0
                ArrWrite[1,2]=Left(Substr(Nvl(VAL,""),1,g_ARRSEZ(m.w_LENSEZ,2))+ArrWrite[1,2],m.w_LENGHT)
              else
                ArrWrite[1,2]=Nvl(VAL,"")+ArrWrite[1,2]
              endif
            else
              if m.w_LENSEZ>0
                ArrWrite[1,2]=Substr(Nvl(VAL,""),1,g_ARRSEZ(m.w_LENSEZ,2))+ArrWrite[1,2]
              else
                ArrWrite[1,2]=Nvl(VAL,"")+ArrWrite[1,2]
              endif
            endif
            * --- Aggiorno array sezioni
            if m.w_LENSEZ>0
              g_ARRSEZ(m.w_LENSEZ,1)=Alltrim(m.w_CAMPO) 
 g_ARRSEZ(m.w_LENSEZ,2)=g_ARRSEZ(m.w_LENSEZ,2) + m.w_DFSEZFIN-m.w_DFSEZINI+1
            else
              m.w_LENARRSEZ = ALEN(g_ARRSEZ,1) 
              if ALEN(g_ARRSEZ,1)>1
                m.w_LENARRSEZ = m.w_LENARRSEZ + 1
                Dimension g_ARRSEZ(m.w_LENARRSEZ,2)
              endif
               
 g_ARRSEZ(m.w_LENARRSEZ,1)=Alltrim(m.w_CAMPO) 
 g_ARRSEZ(m.w_LENARRSEZ,2)=m.w_DFSEZFIN-m.w_DFSEZINI+1
            endif
            * --- Chiudo cursore read
            if Used((NC))
               
 Select (NC) 
 use
            endif
          endif
        endif
        if Not Empty( WRITETABLE( m.w_PHNAME , @ArrWrite , @ArrVal , m.pLOG,m.pVerbose ) )
          AddMsgNL("Errore in scrittura archivio %1, errore %2" , m.pLOG,m.w_CODTAB , Message()+Chr(13)+Message(1))
           
 i_Error=MSG_UPDATE_ERROR
          i_retcode = 'stop'
          i_retval = .f.
          return
        else
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Scritto campo: %1 della tabella:%2 con valore: %3" , m.pLOG,m.w_CAMPO,m.w_CODTAB , m.w_valore)
          endif
        endif
      endif
    endif
  endif
endproc


procedure getobjxml_Page_3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Applico eventuali trascodifiche prerviste nell'elemento
  m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DOC_MAST")
   
 Dimension ArrTest[1,2] 
 ArrTest[1,1]="MVSERIAL" 
 ArrTest[1,2]=m.pSERIAL
  CURSOR=READTABLE(m.w_PHNAME,"MVCODCON,MVTIPCON",@ARRTEST,,,)
  do case
    case m.w_TIPTRA="I"
      * --- Da intestatario
      m.w_TAB_TIP = "DOC_MAST"
      m.w_CAMPO_TIP = "MVTIPCON"
      * --- Read from VAPREDEF
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAPREDEF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAPREDEF_idx,2],.t.,VAPREDEF_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRVALCAR"+;
          " from "+i_cTable+" VAPREDEF where ";
              +"PRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and PRCODTAB = "+cp_ToStrODBC(m.w_TAB_TIP);
              +" and PR_CAMPO = "+cp_ToStrODBC(m.w_CAMPO_TIP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRVALCAR;
          from (i_cTable) where;
              PRCODSTR = m.pCODSTR;
              and PRCODTAB = m.w_TAB_TIP;
              and PR_CAMPO = m.w_CAMPO_TIP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_PRVALCAR = NVL(cp_ToDate(_read_.PRVALCAR),cp_NullValue(_read_.PRVALCAR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      m.w_TIPCON = Nvl(&CURSOR..MVTIPCON,m.w_PRVALCAR)
      m.w_CODCON = &CURSOR..MVCODCON
      if Not empty(m.w_CODCON) 
        if Not empty(m.w_CODCON)
          * --- Cerco trascodifica specifica dell'intestatario
          * --- Read from VATRASCO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[VATRASCO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODTRI"+;
              " from "+i_cTable+" VATRASCO where ";
                  +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                  +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
                  +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
                  +" and TRCODCON = "+cp_ToStrODBC(m.w_CODCON);
                  +" and TRTIPCON = "+cp_ToStrODBC(m.w_TIPCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODTRI;
              from (i_cTable) where;
                  TRCODSTR = m.pCODSTR;
                  and TRCODENT = m.w_CODENT;
                  and TRCODELE = m.w_CODELE;
                  and TRCODCON = m.w_CODCON;
                  and TRTIPCON = m.w_TIPCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[CONTI_idx,2],.t.,CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODGRU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(m.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(m.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODGRU;
              from (i_cTable) where;
                  ANTIPCON = m.w_TIPCON;
                  and ANCODICE = m.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODRAG = NVL(cp_ToDate(_read_.ANCODGRU),cp_NullValue(_read_.ANCODGRU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Empty(m.w_CODTRA) and Not Empty(m.w_CODRAG)
          * --- Se non trovo trascodifica specifica dell'intestatario
          *     cerco quella del gruppo
          * --- Read from VATRASCO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[VATRASCO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODTRI"+;
              " from "+i_cTable+" VATRASCO where ";
                  +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                  +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
                  +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
                  +" and TRCODRAG = "+cp_ToStrODBC(m.w_CODRAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODTRI;
              from (i_cTable) where;
                  TRCODSTR = m.pCODSTR;
                  and TRCODENT = m.w_CODENT;
                  and TRCODELE = m.w_CODELE;
                  and TRCODRAG = m.w_CODRAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(m.w_CODTRA)
          * --- Read from TRS_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[TRS_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[TRS_MAST_idx,2],.t.,TRS_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRFLALLT"+;
              " from "+i_cTable+" TRS_MAST where ";
                  +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRFLALLT;
              from (i_cTable) where;
                  TRCODICE = m.w_CODTRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Applico alltrim  se previsto
          if m.w_TRFLALLT$ "I-T"
            m.w_VALORE = ALLTRIM(m.w_VALORE)
          endif
          * --- Read from TRS_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[TRS_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[TRS_DETT_idx,2],.t.,TRS_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODINT"+;
              " from "+i_cTable+" TRS_DETT where ";
                  +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                  +" and TRCODEXT = "+cp_ToStrODBC(m.w_VALORE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODINT;
              from (i_cTable) where;
                  TRCODICE = m.w_CODTRA;
                  and TRCODEXT = m.w_VALORE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS<>0
             
 EXPR=m.w_TMP_VAL
            m.w_VALORE = EXPR
            if VarType( m.pLog ) ="O" and m.pVerbose
              AddMsgNL("Applicazione trascodifica %1, valore %2 associata all'intestatario", m.pLog ,ALLTRIM(m.w_CODTRA), ALLTRIM(m.w_TMP_VAL))
            endif
          endif
        endif
      endif
    case m.w_TIPTRA="S"
      * --- Da struttura
      * --- Read from VATRASCO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VATRASCO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRCODTRI"+;
          " from "+i_cTable+" VATRASCO where ";
              +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
              +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRCODTRI;
          from (i_cTable) where;
              TRCODSTR = m.pCODSTR;
              and TRCODENT = m.w_CODENT;
              and TRCODELE = m.w_CODELE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(m.w_CODTRA)
        * --- Read from TRS_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[TRS_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TRS_MAST_idx,2],.t.,TRS_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRFLALLT"+;
            " from "+i_cTable+" TRS_MAST where ";
                +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRFLALLT;
            from (i_cTable) where;
                TRCODICE = m.w_CODTRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Applico alltrim  se previsto
        if m.w_TRFLALLT$ "I-T"
          m.w_VALORE = ALLTRIM(m.w_VALORE)
        endif
        * --- Read from TRS_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[TRS_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TRS_DETT_idx,2],.t.,TRS_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRCODINT"+;
            " from "+i_cTable+" TRS_DETT where ";
                +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                +" and TRCODEXT = "+cp_ToStrODBC(m.w_VALORE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRCODINT;
            from (i_cTable) where;
                TRCODICE = m.w_CODTRA;
                and TRCODEXT = m.w_VALORE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS<>0
           
 EXPR=m.w_TMP_VAL
          m.w_VALORE = EXPR
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Applicazione trascodifica %1, valore %2 associata alla struttura", m.pLog ,ALLTRIM(m.w_CODTRA), ALLTRIM(m.w_TMP_VAL))
          endif
        endif
      endif
    case m.w_TIPTRA="B" and Not empty(m.w_BATCH)
      * --- Da routine
      if VarType( m.pLog ) ="O" and m.pVerbose
        AddMsgNL("Applicazione trascodifica da routine %1", m.pLog ,ALLTRIM(m.w_BATCH))
      endif
      m.w_MVSERIAL = m.pSERIAL
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 Oggetto=createobject("cp_getfuncvar") 
 Batch=ALLTRIM(m.w_BATCH) 
 Batch=IIF(AT("(",Batch)>0, Batch, Batch+"(Oggetto)") 
 VALTRAS=&Batch 
 EXPR=IIF(Not empty(VALTRAS),VALTRAS,m.w_VALORE)
      m.w_VALORE = EXPR
      on error &L_OldError 
  endcase
  if Used((CURSOR))
     
 Select (CURSOR) 
 use
  endif
endproc


  function getobjxml_OpenTables()
    dimension i_cWorkTables[max(1,11)]
    i_cWorkTables[1]='XDC_FIELDS'
    i_cWorkTables[2]='VAELEMEN'
    i_cWorkTables[3]='VATRASCO'
    i_cWorkTables[4]='VASTRUTT'
    i_cWorkTables[5]='TRS_DETT'
    i_cWorkTables[6]='TRS_MAST'
    i_cWorkTables[7]='CONTI'
    i_cWorkTables[8]='TMP_TAGELE'
    i_cWorkTables[9]='FLATMAST'
    i_cWorkTables[10]='FLATDETT'
    i_cWorkTables[11]='VAPREDEF'
    return(cp_OpenFuncTables(11))
* --- END GETOBJXML
* --- START GETOBJXML_1
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getobjxml_1                                                     *
*              CALCOLA VALORI OGGETTI XML                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_76]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-23                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getobjxml_1
param pOBJ,pCODSTR,pSERIAL,pCODTAB,pSERIMP,pVerbose,pLog,pSEARCH,pIDCHLD

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NUM_ATTR
  m.w_NUM_ATTR=0
  private w_VALORE
  m.w_VALORE=space(100)
  private w_NODO
  m.w_NODO=space(100)
  private w_NUM_CHILD
  m.w_NUM_CHILD=0
  private w_OK
  m.w_OK=.f.
  private w_CODENT
  m.w_CODENT=space(15)
  private w_FLATAB
  m.w_FLATAB=space(15)
  private w_FL_AZI
  m.w_FL_AZI=space(1)
  private w_STFUNRIC
  m.w_STFUNRIC=space(30)
  private w_TABPAD
  m.w_TABPAD=space(30)
  private w_OKTAG
  m.w_OKTAG=.f.
  private w_CODELE
  m.w_CODELE=space(20)
  private w_INISEZ
  m.w_INISEZ=space(1)
  private w_CONCAT
  m.w_CONCAT=0
  private w_ELIDCHLD
  m.w_ELIDCHLD=0
  private w_CURSORE
  m.w_CURSORE=space(10)
  private w_ORDASS
  m.w_ORDASS=0
  private w_DFSEZINI
  m.w_DFSEZINI=0
  private w_DFSEZFIN
  m.w_DFSEZFIN=0
  private w_TIPELE
  m.w_TIPELE=space(1)
  private w_ELEROT
  m.w_ELEROT=space(20)
  private w_FLUTIL
  m.w_FLUTIL=space(1)
  private w_NODO_PADRE
  m.w_NODO_PADRE=space(100)
  private w_LEN
  m.w_LEN=0
  private w_POS
  m.w_POS=0
  private w_CARIMP
  m.w_CARIMP=space(1)
  private w_ALLIMP
  m.w_ALLIMP=space(1)
  private w_MASK
  m.w_MASK=space(50)
  private w_PHNAME
  m.w_PHNAME=space(30)
  private w_TIPCAM
  m.w_TIPCAM=space(1)
  private w_CODTAB
  m.w_CODTAB=space(30)
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_INDICE
  m.w_INDICE=0
  private w_DECIMAL
  m.w_DECIMAL=0
  private w_LENGHT
  m.w_LENGHT=0
  private w_SEGNO
  m.w_SEGNO=space(1)
  private w_TAB_GES
  m.w_TAB_GES=.f.
  private w_CAMPI
  m.w_CAMPI=space(30)
  private w_RIGDEL2
  m.w_RIGDEL2=0
  private w_CAMPI
  m.w_CAMPI=space(30)
  private w_RIGDEL2
  m.w_RIGDEL2=0
  private w_RIGDEL3
  m.w_RIGDEL3=0
  private w_TRFLALLT
  m.w_TRFLALLT=space(1)
  private w_CODTRA
  m.w_CODTRA=space(10)
  private w_CODRAG
  m.w_CODRAG=space(10)
  private w_CODENT
  m.w_CODENT=space(10)
  private w_BATCH
  m.w_BATCH=space(30)
  private w_TIPTRA
  m.w_TIPTRA=space(1)
  private w_CODCON
  m.w_CODCON=space(15)
  private w_TIPCON
  m.w_TIPCON=space(1)
  private w_TMP_VAL
  m.w_TMP_VAL=space(30)
  private w_OBJ_CHILD
  m.w_OBJ_CHILD = .null.
* --- WorkFile variables
  private XDC_FIELDS_idx
  XDC_FIELDS_idx=0
  private VAELEMEN_idx
  VAELEMEN_idx=0
  private VATRASCO_idx
  VATRASCO_idx=0
  private VASTRUTT_idx
  VASTRUTT_idx=0
  private TRS_DETT_idx
  TRS_DETT_idx=0
  private TRS_MAST_idx
  TRS_MAST_idx=0
  private CONTI_idx
  CONTI_idx=0
  private TMP_TAGELE_idx
  TMP_TAGELE_idx=0
  private FLATMAST_idx
  FLATMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getobjxml_1"
if vartype(__getobjxml_1_hook__)='O'
  __getobjxml_1_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getobjxml_1('+Transform(pOBJ)+','+Transform(pCODSTR)+','+Transform(pSERIAL)+','+Transform(pCODTAB)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+','+Transform(pSEARCH)+','+Transform(pIDCHLD)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getobjxml_1')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getobjxml_1_OpenTables()
  getobjxml_1_Page_1()
endif
cp_CloseFuncTables()
if used('_Curs_TMP_TAGELE')
  use in _Curs_TMP_TAGELE
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getobjxml_1('+Transform(pOBJ)+','+Transform(pCODSTR)+','+Transform(pSERIAL)+','+Transform(pCODTAB)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+','+Transform(pSEARCH)+','+Transform(pIDCHLD)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getobjxml_1')
Endif
*--- Activity log
if vartype(__getobjxml_1_hook__)='O'
  __getobjxml_1_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getobjxml_1_Page_1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Estrapola valori (attributi,valore nodo) dall'oggetto passato e dai relativi figli
  * --- Oggetto padre
  * --- Codice struttura
  * --- Seriale documento
  * --- Tabella primo elemento 
  * --- Seriale importazione
  * --- flag verbose
  * --- Log errori
  * --- No ricerca elemento
  * --- Id figlio di padre di tipo scelta
  m.pIDCHLD = IIF(TYPE("pIDCHLD")="N", m.pIDCHLD, 0)
  m.w_OK = .T.
  if Type("pOBJ")="O"
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[VASTRUTT_idx,2],.t.,VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STCODENT,STFLATAB,STFUNRIC"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(m.pCODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STCODENT,STFLATAB,STFUNRIC;
        from (i_cTable) where;
            STCODICE = m.pCODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      m.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      m.w_STFUNRIC = NVL(cp_ToDate(_read_.STFUNRIC),cp_NullValue(_read_.STFUNRIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from FLATMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[FLATMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[FLATMAST_idx,2],.t.,FLATMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FTFL_AZI"+;
        " from "+i_cTable+" FLATMAST where ";
            +"FTCODICE = "+cp_ToStrODBC(m.w_FLATAB);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FTFL_AZI;
        from (i_cTable) where;
            FTCODICE = m.w_FLATAB;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_FL_AZI = NVL(cp_ToDate(_read_.FTFL_AZI),cp_NullValue(_read_.FTFL_AZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    m.w_FLATAB = IIF(m.w_FL_AZI="S","xxx"+Alltrim(m.w_FLATAB),alltrim(m.w_FLATAB))
    if Type("pOBJ.Attributes.length") ="N"
      if m.pOBJ.Attributes.length>0
        m.w_NUM_ATTR = 0
        do while m.w_NUM_ATTR<m.pOBJ.Attributes.Length
          m.w_NODO = m.pOBJ.Attributes.item(m.w_NUM_ATTR).basename
          m.w_VALORE = m.pOBJ.Attributes.item(m.w_NUM_ATTR).text
          getobjxml_1_Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          m.w_NUM_ATTR = m.w_NUM_ATTR + 1
        enddo
      endif
    endif
    if m.pOBJ.HasChildNodes
      * --- Ha ulteriori figli riapplico la funzione ricorsivamente
      m.w_NUM_CHILD = 0
      do while m.w_NUM_CHILD<m.pOBJ.childnodes.Length
        m.w_OBJ_CHILD = m.pOBJ.childnodes.item(m.w_NUM_CHILD)
        m.w_NODO = m.w_OBJ_CHILD.basename
        if Not Empty(m.w_NODO)
          if (m.w_OBJ_CHILD.HasChildNodes or Type("m.w_OBJ_CHILD.Attributes.length") ="N") 
            m.w_TABPAD = Space(30)
            if Empty(m.pCODTAB)
              * --- Read from VAELEMEN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[VAELEMEN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ELTABIMP"+;
                  " from "+i_cTable+" VAELEMEN where ";
                      +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                      +" and ELVALTXT = "+cp_ToStrODBC(m.w_NODO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ELTABIMP;
                  from (i_cTable) where;
                      ELCODSTR = m.pCODSTR;
                      and ELVALTXT = m.w_NODO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                m.w_TABPAD = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              m.w_TABPAD = m.pCODTAB
            endif
             
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 L_FUN="m.w_OK = "+ALLTRIM(m.w_STFUNRIC)+" ( m.w_OBJ_CHILD, ALLTRIM(m.pCODSTR),ALLTRIM(m.pSERIAL),ALLTRIM(m.w_TABPAD),"+; 
 "ALLTRIM(m.pSERIMP),m.pVERBOSE,m.pLOG,m.pSearch)" 
 &L_FUN 
 on error &L_OldError 
            if L_err
              addMsg("%0Errore nell'esecuzione della funzione ricorsiva: %1" ,m.pLOG,Message()+Chr(13)+Message(1))
              m.w_OK = .F.
            endif
            if Not m.w_OK
              i_retcode = 'stop'
              i_retval = .f.
              return
            endif
          else
            m.w_VALORE = m.w_OBJ_CHILD.Text
            m.w_NODO = m.w_OBJ_CHILD.BaseName
            getobjxml_1_Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          m.w_VALORE = m.w_OBJ_CHILD.Text
          m.w_NODO = m.pOBJ.BaseName
          getobjxml_1_Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        m.w_NUM_CHILD = m.w_NUM_CHILD + 1
      enddo
    else
      m.w_NODO = m.pOBJ.BaseName
      m.w_VALORE = m.pOBJ.Text
      getobjxml_1_Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    i_retcode = 'stop'
    i_retval = .t.
    return
  else
    i_retcode = 'stop'
    i_retval = .f.
    return
  endif
endproc


procedure getobjxml_1_Page_2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Ricavo elemento dal nome base del nodo
  m.w_OKTAG = .F.
  m.w_CODTAB = m.pCODTAB
  m.w_CURSORE = sys(2015)
  if Not m.pSEARCH
    * --- Select from TMP_TAGELE
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2],.t.,TMP_TAGELE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_TAGELE ";
          +" where FLUTILI='N' AND TAGELE="+cp_ToStrODBC(m.w_NODO)+" AND TABELLA="+cp_ToStrODBC(m.w_CODTAB)+"";
          +" order by INDEN";
           ,"_Curs_TMP_TAGELE")
    else
      select * from (i_cTable);
       where FLUTILI="N" AND TAGELE=m.w_NODO AND TABELLA=m.w_CODTAB;
       order by INDEN;
        into cursor _Curs_TMP_TAGELE
    endif
    if used('_Curs_TMP_TAGELE')
      select _Curs_TMP_TAGELE
      locate for 1=1
      do while not(eof())
      m.w_ELIDCHLD = _Curs_TMP_TAGELE.ELIDCHLD
      if m.pIDCHLD=0 OR m.pIDCHLD = m.w_ELIDCHLD
        m.w_CODELE = _Curs_TMP_TAGELE.CODICE
        m.w_TIPELE = _Curs_TMP_TAGELE.TIPELE
        m.w_INISEZ = _Curs_TMP_TAGELE.INISEZ
        m.w_CODTAB = _Curs_TMP_TAGELE.TABELLA
        m.w_CAMPO = _Curs_TMP_TAGELE.ELCAMIMP
        m.w_TIPTRA = _Curs_TMP_TAGELE.ELTIPTR2
        m.w_BATCH = _Curs_TMP_TAGELE.EL_BATC2
        m.w_SEGNO = _Curs_TMP_TAGELE.EL_SEGNO
        m.w_CONCAT = _Curs_TMP_TAGELE.NUCONC
        m.w_ORDASS = Nvl(_Curs_TMP_TAGELE.INDEN,0)
        m.w_DFSEZINI = NVL(_Curs_TMP_TAGELE.DFSEZINI,0)
        m.w_DFSEZFIN = NVL(_Curs_TMP_TAGELE.DFSEZFIN,0)
        m.w_OKTAG = .T.
        Exit
      endif
        select _Curs_TMP_TAGELE
        continue
      enddo
      use
    endif
    if m.w_OKTAG
      * --- Write into TMP_TAGELE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,TAGELE"
        do vq_exec with 'GSVA_QOA',createobject('cp_getfuncvar'),.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE, "+i_cQueryTable+" _t2 set ";
        +"TMP_TAGELE.FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +Iif(Empty(i_ccchkf),"",",TMP_TAGELE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_TAGELE.CODICE = t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = t2.TAGELE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set (";
            +"FLUTILI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_TAGELE.CODICE = _t2.CODICE";
                +" and "+"TMP_TAGELE.TAGELE = _t2.TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_TAGELE set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".TAGELE = "+i_cQueryTable+".TAGELE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Se esco dalla select senza aver trovato un elemento tag 
      *     siginifica che ho utilizzato tutti e sono ritornato all'elemento iniziale
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ELCAMIMP,ELTABIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELINISEZ,ELCODICE,ELCARIMP,ELALLIMP"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and ELVALTXT = "+cp_ToStrODBC(m.w_NODO);
              +" and ELINISEZ = "+cp_ToStrODBC("S");
              +" and ELTABIMP = "+cp_ToStrODBC(m.pCODTAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ELCAMIMP,ELTABIMP,ELTIPTR2,EL_BATC2,EL_SEGNO,ELINISEZ,ELCODICE,ELCARIMP,ELALLIMP;
          from (i_cTable) where;
              ELCODSTR = m.pCODSTR;
              and ELVALTXT = m.w_NODO;
              and ELINISEZ = "S";
              and ELTABIMP = m.pCODTAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_CAMPO = NVL(cp_ToDate(_read_.ELCAMIMP),cp_NullValue(_read_.ELCAMIMP))
        m.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
        m.w_TIPTRA = NVL(cp_ToDate(_read_.ELTIPTR2),cp_NullValue(_read_.ELTIPTR2))
        m.w_BATCH = NVL(cp_ToDate(_read_.EL_BATC2),cp_NullValue(_read_.EL_BATC2))
        m.w_SEGNO = NVL(cp_ToDate(_read_.EL_SEGNO),cp_NullValue(_read_.EL_SEGNO))
        m.w_INISEZ = NVL(cp_ToDate(_read_.ELINISEZ),cp_NullValue(_read_.ELINISEZ))
        m.w_CODELE = NVL(cp_ToDate(_read_.ELCODICE),cp_NullValue(_read_.ELCODICE))
        m.w_CARIMP = NVL(cp_ToDate(_read_.ELCARIMP),cp_NullValue(_read_.ELCARIMP))
        m.w_ALLIMP = NVL(cp_ToDate(_read_.ELALLIMP),cp_NullValue(_read_.ELALLIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  else
    m.w_NODO = SUBSTR(m.w_NODO,2)
    m.w_CAMPO = Space(30)
     
 Cursore=m.w_CURSORE 
 Do vq_exec with "..\VEFA\EXE\QUERY\GSVA2QTA",createobject("cp_getfuncvar"),Cursore,"",.f.,.t. 
 SELECT (Cursore) 
 go top 
 Scan
    m.w_ELIDCHLD = &cursore..ELIDCHLD
    if m.pIDCHLD=0 OR m.pIDCHLD = m.w_ELIDCHLD
      m.w_CODELE = &cursore..CODICE
      m.w_TIPELE = &cursore..TIPELE
      m.w_CODTAB = &cursore..TABELLA
      m.w_CAMPO = &cursore..ELCAMIMP
      m.w_TIPTRA = &cursore..ELTIPTR2
      m.w_BATCH = &cursore..EL_BATC2
      m.w_SEGNO = &cursore..EL_SEGNO
      m.w_CONCAT = &cursore..NUCONC
      m.w_DFSEZINI = NVL(&cursore..DFSEZINI,0)
      m.w_DFSEZFIN = NVL(&cursore..DFSEZFIN,0)
      m.w_OKTAG = .T.
      Exit
    endif
     
 endscan 
 use in (cursore)
    m.w_FLUTIL = "N"
    m.w_ELEROT = Space(20)
    if Not Empty(Nvl(m.w_CAMPO," "))
      m.w_NODO_PADRE = Substr(m.pOBJ.parentNode.NodeName,2)
      * --- Read from TMP_TAGELE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[TMP_TAGELE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2],.t.,TMP_TAGELE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INISEZ,FLUTILI,ELEROT"+;
          " from "+i_cTable+" TMP_TAGELE where ";
              +"CODICE = "+cp_ToStrODBC(m.w_NODO_PADRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INISEZ,FLUTILI,ELEROT;
          from (i_cTable) where;
              CODICE = m.w_NODO_PADRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_INISEZ = NVL(cp_ToDate(_read_.INISEZ),cp_NullValue(_read_.INISEZ))
        m.w_FLUTIL = NVL(cp_ToDate(_read_.FLUTILI),cp_NullValue(_read_.FLUTILI))
        m.w_ELEROT = NVL(cp_ToDate(_read_.ELEROT),cp_NullValue(_read_.ELEROT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Valorizzo elemento di rottura
      if m.w_FLUTIL="S"
        if m.w_ELEROT=m.w_CODELE
          m.w_INISEZ = "S"
        else
          m.w_INISEZ = "N"
        endif
      else
        m.w_ELEROT = IIF(m.w_INISEZ="S",m.w_CODELE,Space(20))
      endif
    endif
  endif
  if m.w_DFSEZINI>0 AND m.w_DFSEZFIN>0
    * --- Se concatenazione ricavo informazione non in base 
    *     ma in base all'intervallo indicato nel formato
    m.w_VALORE = SUBSTR(m.w_VALORE, m.w_DFSEZINI,m.w_DFSEZFIN-m.w_DFSEZINI+1)
  endif
  if m.w_INISEZ="S"
    m.w_POS = cp_ROUND(ASCAN(ArProg,Alltrim(m.w_CODTAB))/2,0)
    if m.w_POS>0
      m.w_INDICE = ArProg[m.w_POS,2] + 1
    else
      * --- Inserisco prima occorrenza del campo
      m.w_LEN = int(Alen(ArProg)/2)
       
 DIMENSION ArProg(m.w_LEN+1,2) 
 ArProg[m.w_LEN+1,1]=m.w_CODTAB
      m.w_INDICE = 1
      m.w_POS = m.w_LEN+1
    endif
    * --- Aggiorno array progressivi 
    ArProg[m.w_POS,2] =m.w_INDICE
    * --- smarco elementi tag utilizzati della tabella
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("N"),'TMP_TAGELE','FLUTILI');
          +i_ccchkf ;
      +" where ";
          +"TABELLA = "+cp_ToStrODBC(m.w_CODTAB);
             )
    else
      update (i_cTable) set;
          FLUTILI = "N";
          &i_ccchkf. ;
       where;
          TABELLA = m.w_CODTAB;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  else
    if Not Empty(m.w_CODTAB)
      m.w_POS = cp_ROUND(ASCAN(ArProg,Alltrim(m.w_CODTAB))/2,0)
      if m.w_POS>0
        m.w_INDICE = ArProg[m.w_POS,2]
      endif
    endif
  endif
  * --- Marco elemento tag utilizzato
  if Not m.pSEARCH
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(m.w_CODELE);
          +" and TAGELE = "+cp_ToStrODBC(m.w_NODO);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          &i_ccchkf. ;
       where;
          CODICE = m.w_CODELE;
          and TAGELE = m.w_NODO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  else
    * --- Write into TMP_TAGELE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[TMP_TAGELE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TMP_TAGELE_idx,2])
    i_ccchkf=''
    func_SetCCCHKVarsWrite(@i_ccchkf,TMP_TAGELE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLUTILI = "+cp_NullLink(cp_ToStrODBC("S"),'TMP_TAGELE','FLUTILI');
      +",ELEROT = "+cp_NullLink(cp_ToStrODBC(m.w_ELEROT),'TMP_TAGELE','ELEROT');
          +i_ccchkf ;
      +" where ";
          +"CODICE = "+cp_ToStrODBC(m.w_NODO_PADRE);
             )
    else
      update (i_cTable) set;
          FLUTILI = "S";
          ,ELEROT = m.w_ELEROT;
          &i_ccchkf. ;
       where;
          CODICE = m.w_NODO_PADRE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endif
  if Not Empty(Nvl(m.w_CAMPO," ")) and Not Empty(Nvl(m.w_CODTAB," "))
    m.w_LENGHT = I_DCX.GEtfieldlen(m.w_CODTAB,I_DCX.GETfieldidx(m.w_CODTAB,m.w_CAMPO))
    m.w_DECIMAL = I_DCX.GEtfielddec(m.w_CODTAB,I_DCX.GETfieldidx(m.w_CODTAB,m.w_CAMPO))
    m.w_TIPCAM = I_DCX.GEtfieldtype(m.w_CODTAB,I_DCX.GETfieldidx(m.w_CODTAB,m.w_CAMPO))
    if VarType( m.pLog ) ="O"
      AddMsgNL("Import tabella %2 campo: %1", m.pLog ,ALLTRIM(m.w_CAMPO),ALLTRIM(m.w_CODTAB))
    endif
    do case
      case m.w_ALLIMP="D"
        m.w_VALORE = LTRIM(m.w_VALORE, 1 , m.w_CARIMP )
      case m.w_ALLIMP="S"
        m.w_VALORE = RTRIM(m.w_VALORE, 1 , m.w_CARIMP )
    endcase
    getobjxml_1_Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if m.w_TIPCAM $ "D-N" 
      * --- Read from VAELEMEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EL__MASK"+;
          " from "+i_cTable+" VAELEMEN where ";
              +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and ELCODICE = "+cp_ToStrODBC(m.w_NODO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EL__MASK;
          from (i_cTable) where;
              ELCODSTR = m.pCODSTR;
              and ELCODICE = m.w_NODO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_MASK = NVL(cp_ToDate(_read_.EL__MASK),cp_NullValue(_read_.EL__MASK))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case m.w_TIPCAM="D"
        * --- Formato Date
        if at("T",m.w_VALORE)>0
          expr=cp_chartodate(SUBSTR(m.w_VALORE,1,at("T",m.w_VALORE)-1),"yy-mm-dd")
        else
          * --- No formato xml applico formato data in base alla lunghezza del campo
          if Not empty(m.w_MASK)
            expr=cp_chartodate(SUBSTR(m.w_valore,AT("G",m.w_MASK),OCCURS("G",m.w_MASK))+"-"+SUBSTR(m.w_valore,AT("M",m.w_MASK),; 
 OCCURS("M",m.w_MASK))+"-"+SUBSTR(m.w_valore,AT("A",m.w_MASK),OCCURS("A",m.w_MASK)))
          else
            expr=cp_chartodate(SUBSTR(m.w_VALORE,1,m.w_LENGHT),"yyyymmdd")
          endif
        endif
      case m.w_TIPCAM="N"
        * --- Numerici
        m.w_VALORE = SUBSTR(m.w_VALORE,1,LEN(m.w_VALORE)-m.w_DECIMAL) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
        if SUBSTR(m.w_VALORE,1,1)="-"
          m.w_VALORE = "-"+right(m.w_VALORE,(m.w_LENGHT-m.w_DECIMAL)-1) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
        else
          m.w_VALORE = right(m.w_VALORE,(m.w_LENGHT-m.w_DECIMAL)) + ","+SUBSTR(m.w_VALORE,LEN(m.w_VALORE)-m.w_DECIMAL+1,m.w_DECIMAL)
        endif
        do case
          case m.w_SEGNO="P"
            expr=Abs(val(m.w_VALORE))
          case m.w_SEGNO="M"
            expr=-Abs(val(SUBSTR(m.w_VALORE,1,m.w_LENGHT)))
          otherwise
            expr=val(SUBSTR(m.w_VALORE,1,m.w_LENGHT))
        endcase
      case m.w_TIPCAM="C"
         
 expr=SUBSTR(m.w_VALORE,1,m.w_LENGHT)
        if Empty(EXPR)
          cp_ReadXdc()
          if i_dcx.IsFieldNullable(Alltrim(m.w_CODTAB),Alltrim(m.w_CAMPO))
            EXPR=.NULL.
          endif
        else
          * --- Spezzo il dato in base alla lunghezza del campo di destinazione
          EXPR=Left(EXPR,m.w_LENGHT)
        endif
      otherwise
         
 expr=m.w_VALORE
    endcase
    Dimension Arrwrite[1,2]
     
 ArrWrite[1,1]=m.w_campo 
 ArrWrite[1,2]=expr
    m.w_TAB_GES = .T.
    m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
    NC="@@"
    do case
      case m.w_CODTAB="DELFOR_1"
        Dimension ArrCampi[3,2],ArrVal[1,2] 
 ArrCampi[1,1]="DFSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="FT___UID" 
 ArrCampi[2,2]=m.pSERIMP 
 ArrCampi[3,1]=m.w_CAMPO 
 ArrCampi[3,2]=EXPR
         
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL
      case m.w_CODTAB="DELFOR_2"
         
 Dimension ArrCampi[4,2] 
 ArrCampi[1,1]="DFSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="CPROWNUM" 
 ArrCampi[2,2]=m.w_INDICE 
 ArrCampi[3,1]=m.w_CAMPO 
 ArrCampi[3,2]=EXPR 
 ArrCampi[4,1]="FT___UID" 
 ArrCampi[4,2]=m.pSERIMP
         
 Dimension ArrVal[2,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="CPROWNUM" 
 ArrVal[2,2]=m.w_INDICE
      case m.w_CODTAB="DELFOR_3"
        * --- Devo leggere ultimo cprownum inserito in DELFOR_2
        * --- Cerco ultima riga docdett caricata
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DELFOR_2")
         
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL
        MAXROW=READTABLE(m.w_PHNAME,"MAX(CPROWNUM) AS CPROWNUM",@ARRVAL,,,)
        m.w_RIGDEL2 = &MAXROW..CPROWNUM
        if Used((MAXROW))
           
 Select (MAXROW) 
 use
        endif
         
 Dimension ArrCampi[5,2] 
 ArrCampi[1,1]="DFSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="DFROWORD" 
 ArrCampi[2,2]=m.w_RIGDEL2 
 ArrCampi[3,1]="CPROWNUM" 
 ArrCampi[3,2]=m.w_INDICE 
 ArrCampi[4,1]=m.w_CAMPO 
 ArrCampi[4,2]=EXPR 
 ArrCampi[5,1]="FT___UID" 
 ArrCampi[5,2]=m.pSERIMP
         
 Dimension ArrVal[3,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="DFROWORD" 
 ArrVal[2,2]=m.w_RIGDEL2 
 ArrVal[3,1]="CPROWNUM" 
 ArrVal[3,2]=m.w_INDICE
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
      case m.w_CODTAB="DELFOR_4"
        * --- Devo leggere ultimo cprownum inserito in DELFOR_2
        * --- Cerco ultima riga docdett caricata
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DELFOR_2")
         
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL
        MAXROW=READTABLE(m.w_PHNAME,"MAX(CPROWNUM) AS CPROWNUM",@ARRVAL,,,)
        m.w_RIGDEL2 = &MAXROW..CPROWNUM
        if Used((MAXROW))
           
 Select (MAXROW) 
 use
        endif
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DELFOR_3")
         
 Dimension ArrVal[2,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="DFROWORD" 
 ArrVal[2,2]=m.w_RIGDEL2
        MAXROW=READTABLE(m.w_PHNAME,"MAX(CPROWNUM) AS CPROWNUM",@ARRVAL,,,)
        m.w_RIGDEL3 = &MAXROW..CPROWNUM
        if Used((MAXROW))
           
 Select (MAXROW) 
 use
        endif
         
 Dimension ArrCampi[6,2] 
 ArrCampi[1,1]="DFSERIAL" 
 ArrCampi[1,2]=m.pSERIAL 
 ArrCampi[2,1]="DFROWORD" 
 ArrCampi[2,2]=m.w_RIGDEL2 
 ArrCampi[3,1]="DFROWOR2" 
 ArrCampi[3,2]=m.w_RIGDEL3 
 ArrCampi[4,1]="CPROWNUM" 
 ArrCampi[4,2]=m.w_INDICE 
 ArrCampi[5,1]=m.w_CAMPO 
 ArrCampi[5,2]=EXPR 
 ArrCampi[6,1]="FT___UID" 
 ArrCampi[6,2]=m.pSERIMP
         
 Dimension ArrVal[4,2] 
 ArrVal[1,1]="DFSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="DFROWORD" 
 ArrVal[2,2]=m.w_RIGDEL2 
 ArrVal[3,1]="DFROWOR2" 
 ArrVal[3,2]=m.w_RIGDEL3 
 ArrVal[4,1]="CPROWNUM" 
 ArrVal[4,2]=m.w_INDICE
        m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),m.w_CODTAB)
      otherwise
        AddMsgNL("Tabella %1, non gestita" , m.pLOG,Alltrim(m.w_CODTAB))
        m.w_TAB_GES = .F.
        i_retcode = 'stop'
        i_retval = .f.
        return
    endcase
    if m.w_TAB_GES
      if m.w_INISEZ="S"
        m.w_INISEZ = "N"
        if Not Empty(INSERTTABLE( m.w_PHNAME , @ArrCampi ,m.pLOG, m.pVerbose ))
          AddMsgNL("Errore in inserimento archivio %1, errore %2" , m.pLOG,m.w_CODTAB , Message()+Chr(13)+Message(1))
           
 i_Error=MSG_UPDATE_ERROR
          i_retcode = 'stop'
          i_retval = .f.
          return
        else
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Inserito campo: %1 della tabella:%2 con valore: %3" , m.pLOG,m.w_CAMPO,m.w_CODTAB , Cp_ToStrOdbc(EXPR))
          endif
        endif
      else
        if m.w_CONCAT>0
          * --- Elemento da concatenare 
          NC=READTABLE(m.w_PHNAME,m.w_CAMPO,@ARRVAL,,,)
          if USED((Nc)) AND Reccount((Nc))>0
             
 VAL=ALLTRIM((NC))+"."+ALLTRIM(m.w_CAMPO) 
 VAL=&VAL 
 ArrWrite[1,2]=Alltrim(Nvl(VAL,""))+ArrWrite[1,2]
            * --- Chiudo cursore read
            if Used((NC))
               
 Select (NC) 
 use
            endif
          endif
        endif
        if Not Empty( WRITETABLE( m.w_PHNAME , @ArrWrite , @ArrVal , m.pLOG,m.pVerbose ) )
          AddMsgNL("Errore in scrittura archivio %1, errore %2" , m.pLOG,m.w_CODTAB , Message()+Chr(13)+Message(1))
           
 i_Error=MSG_UPDATE_ERROR
          i_retcode = 'stop'
          i_retval = .f.
          return
        else
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Scritto campo: %1 della tabella:%2 con valore: %3" , m.pLOG,m.w_CAMPO,m.w_CODTAB , m.w_valore)
          endif
        endif
      endif
    endif
  endif
endproc


procedure getobjxml_1_Page_3
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Applico eventuali trascodifiche prerviste nell'elemento
  m.w_PHNAME = GetETPhName(alltrim(m.w_FLATAB),"DELFOR_1")
   
 Dimension ArrTest[1,2] 
 ArrTest[1,1]="DFSERIAL" 
 ArrTest[1,2]=m.pSERIAL
  CURSOR=READTABLE(m.w_PHNAME,"DFBUYIDE",@ARRTEST,,,)
  do case
    case m.w_TIPTRA="I"
      * --- Da intestatario
      m.w_TIPCON = "C"
      m.w_CODCON = &CURSOR..DFBUYIDE
      if Not empty(m.w_CODCON) 
        if Not empty(m.w_CODCON)
          * --- Cerco trascodifica specifica dell'intestatario
          * --- Read from VATRASCO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[VATRASCO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODTRI"+;
              " from "+i_cTable+" VATRASCO where ";
                  +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                  +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
                  +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
                  +" and TRCODCON = "+cp_ToStrODBC(m.w_CODCON);
                  +" and TRTIPCON = "+cp_ToStrODBC(m.w_TIPCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODTRI;
              from (i_cTable) where;
                  TRCODSTR = m.pCODSTR;
                  and TRCODENT = m.w_CODENT;
                  and TRCODELE = m.w_CODELE;
                  and TRCODCON = m.w_CODCON;
                  and TRTIPCON = m.w_TIPCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[CONTI_idx,2],.t.,CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODGRU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(m.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(m.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODGRU;
              from (i_cTable) where;
                  ANTIPCON = m.w_TIPCON;
                  and ANCODICE = m.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODRAG = NVL(cp_ToDate(_read_.ANCODGRU),cp_NullValue(_read_.ANCODGRU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Empty(m.w_CODTRA) and Not Empty(m.w_CODRAG)
          * --- Se non trovo trascodifica specifica dell'intestatario
          *     cerco quella del gruppo
          * --- Read from VATRASCO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[VATRASCO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODTRI"+;
              " from "+i_cTable+" VATRASCO where ";
                  +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
                  +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
                  +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
                  +" and TRCODRAG = "+cp_ToStrODBC(m.w_CODRAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODTRI;
              from (i_cTable) where;
                  TRCODSTR = m.pCODSTR;
                  and TRCODENT = m.w_CODENT;
                  and TRCODELE = m.w_CODELE;
                  and TRCODRAG = m.w_CODRAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Not Empty(m.w_CODTRA)
          * --- Read from TRS_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[TRS_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[TRS_MAST_idx,2],.t.,TRS_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRFLALLT"+;
              " from "+i_cTable+" TRS_MAST where ";
                  +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRFLALLT;
              from (i_cTable) where;
                  TRCODICE = m.w_CODTRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Applico alltrim  se previsto
          if m.w_TRFLALLT$ "I-T"
            m.w_VALORE = ALLTRIM(m.w_VALORE)
          endif
          * --- Read from TRS_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[TRS_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[TRS_DETT_idx,2],.t.,TRS_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TRCODINT"+;
              " from "+i_cTable+" TRS_DETT where ";
                  +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                  +" and TRCODEXT = "+cp_ToStrODBC(m.w_VALORE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TRCODINT;
              from (i_cTable) where;
                  TRCODICE = m.w_CODTRA;
                  and TRCODEXT = m.w_VALORE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            m.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS<>0
             
 EXPR=m.w_TMP_VAL
            m.w_VALORE = EXPR
            if VarType( m.pLog ) ="O" and m.pVerbose
              AddMsgNL("Applicazione trascodifica %1, valore %2 associata all'intestatario", m.pLog ,ALLTRIM(m.w_CODTRA), ALLTRIM(m.w_TMP_VAL))
            endif
          endif
        endif
      endif
    case m.w_TIPTRA="S"
      * --- Da struttura
      * --- Read from VATRASCO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[VATRASCO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[VATRASCO_idx,2],.t.,VATRASCO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TRCODTRI"+;
          " from "+i_cTable+" VATRASCO where ";
              +"TRCODSTR = "+cp_ToStrODBC(m.pCODSTR);
              +" and TRCODENT = "+cp_ToStrODBC(m.w_CODENT);
              +" and TRCODELE = "+cp_ToStrODBC(m.w_CODELE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TRCODTRI;
          from (i_cTable) where;
              TRCODSTR = m.pCODSTR;
              and TRCODENT = m.w_CODENT;
              and TRCODELE = m.w_CODELE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_CODTRA = NVL(cp_ToDate(_read_.TRCODTRI),cp_NullValue(_read_.TRCODTRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(m.w_CODTRA)
        * --- Read from TRS_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[TRS_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TRS_MAST_idx,2],.t.,TRS_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRFLALLT"+;
            " from "+i_cTable+" TRS_MAST where ";
                +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRFLALLT;
            from (i_cTable) where;
                TRCODICE = m.w_CODTRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_TRFLALLT = NVL(cp_ToDate(_read_.TRFLALLT),cp_NullValue(_read_.TRFLALLT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Applico alltrim  se previsto
        if m.w_TRFLALLT$ "I-T"
          m.w_VALORE = ALLTRIM(m.w_VALORE)
        endif
        * --- Read from TRS_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[TRS_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[TRS_DETT_idx,2],.t.,TRS_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TRCODINT"+;
            " from "+i_cTable+" TRS_DETT where ";
                +"TRCODICE = "+cp_ToStrODBC(m.w_CODTRA);
                +" and TRCODEXT = "+cp_ToStrODBC(m.w_VALORE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TRCODINT;
            from (i_cTable) where;
                TRCODICE = m.w_CODTRA;
                and TRCODEXT = m.w_VALORE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          m.w_TMP_VAL = NVL(cp_ToDate(_read_.TRCODINT),cp_NullValue(_read_.TRCODINT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_ROWS<>0
           
 EXPR=m.w_TMP_VAL
          m.w_VALORE = EXPR
          if VarType( m.pLog ) ="O" and m.pVerbose
            AddMsgNL("Applicazione trascodifica %1, valore %2 associata alla struttura", m.pLog ,ALLTRIM(m.w_CODTRA), ALLTRIM(m.w_TMP_VAL))
          endif
        endif
      endif
    case m.w_TIPTRA="B" and Not empty(m.w_BATCH)
      * --- Da routine
      if VarType( m.pLog ) ="O" and m.pVerbose
        AddMsgNL("Applicazione trascodifica da routine %1", m.pLog ,ALLTRIM(m.w_BATCH))
      endif
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 Batch=ALLTRIM(m.w_BATCH) 
 Batch=IIF(AT("(",Batch)>0, Batch, Batch+"(This)") 
 VALTRAS=&Batch 
 EXPR=IIF(Not empty(VALTRAS),VALTRAS,EXPR)
      m.w_VALORE = EXPR
      on error &L_OldError 
  endcase
  if Used((CURSOR))
     
 Select (CURSOR) 
 use
  endif
endproc


  function getobjxml_1_OpenTables()
    dimension i_cWorkTables[max(1,9)]
    i_cWorkTables[1]='XDC_FIELDS'
    i_cWorkTables[2]='VAELEMEN'
    i_cWorkTables[3]='VATRASCO'
    i_cWorkTables[4]='VASTRUTT'
    i_cWorkTables[5]='TRS_DETT'
    i_cWorkTables[6]='TRS_MAST'
    i_cWorkTables[7]='CONTI'
    i_cWorkTables[8]='TMP_TAGELE'
    i_cWorkTables[9]='FLATMAST'
    return(cp_OpenFuncTables(9))
* --- END GETOBJXML_1
* --- START CALCINDEN
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calcinden                                                       *
*              CALCOLA INDENTATURA ELEMENTI                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-20                                                      *
* Last revis.: 2006-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calcinden
param pCODSTR,pROOT,pCODELE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_INDENT
  m.w_INDENT=space(254)
  private w_PADRE
  m.w_PADRE=space(20)
  private w_ORDINE
  m.w_ORDINE=0
  private w_LOOP
  m.w_LOOP=space(10)
* --- WorkFile variables
  private VAELEMEN_idx
  VAELEMEN_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calcinden"
if vartype(__calcinden_hook__)='O'
  __calcinden_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calcinden('+Transform(pCODSTR)+','+Transform(pROOT)+','+Transform(pCODELE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcinden')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calcinden_OpenTables()
  calcinden_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calcinden('+Transform(pCODSTR)+','+Transform(pROOT)+','+Transform(pCODELE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calcinden')
Endif
*--- Activity log
if vartype(__calcinden_hook__)='O'
  __calcinden_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calcinden_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato elemento calcola indentatura rispetto ad un elemento ROOT
  m.w_PADRE = m.pCODELE
  m.w_LOOP = 0
  do while m.w_PADRE<>m.pROOT and m.w_LOOP<1000
    * --- Read from VAELEMEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[VAELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ELCODPAD,ELORDINE"+;
        " from "+i_cTable+" VAELEMEN where ";
            +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
            +" and ELCODICE = "+cp_ToStrODBC(m.w_PADRE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ELCODPAD,ELORDINE;
        from (i_cTable) where;
            ELCODSTR = m.pCODSTR;
            and ELCODICE = m.w_PADRE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_PADRE = NVL(cp_ToDate(_read_.ELCODPAD),cp_NullValue(_read_.ELCODPAD))
      m.w_ORDINE = NVL(cp_ToDate(_read_.ELORDINE),cp_NullValue(_read_.ELORDINE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if m.w_PADRE<>m.pROOT
      m.w_INDENT = ALLTRIM(STR(m.w_ORDINE)) 
    endif
    m.w_LOOP = m.w_LOOP + 1
  enddo
  i_retcode = 'stop'
  i_retval = VAL(m.w_INDENT)
  return
endproc


  function calcinden_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='VAELEMEN'
    return(cp_OpenFuncTables(1))
* --- END CALCINDEN
* --- START CERSTRUPRE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cerstrupre                                                      *
*              CERCA STRUTTUTA PREDEFINITA                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-14                                                      *
* Last revis.: 2006-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func cerstrupre
param w_ORDERBY

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODSTR
  m.w_CODSTR=space(10)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "cerstrupre"
if vartype(__cerstrupre_hook__)='O'
  __cerstrupre_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'cerstrupre('+Transform(w_ORDERBY)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cerstrupre')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
cerstrupre_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'cerstrupre('+Transform(w_ORDERBY)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'cerstrupre')
Endif
*--- Activity log
if vartype(__cerstrupre_hook__)='O'
  __cerstrupre_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure cerstrupre_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Determina Struttura Predefinita
  * --- Select from gsva_qst
  do vq_exec with 'gsva_qst',createobject('cp_getfuncvar'),'_Curs_gsva_qst','',.f.,.t.
  if used('_Curs_gsva_qst')
    select _Curs_gsva_qst
    locate for 1=1
    do while not(eof())
    m.w_CODSTR = Nvl(_Curs_gsva_qst.STCODICE,SPACE(10))
    exit
      select _Curs_gsva_qst
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_CODSTR
  return
endproc


* --- END CERSTRUPRE
* --- START CREACURFILE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creacurfile                                                     *
*              CREA CURSORE FILE PER IMPORT EDI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-18                                                      *
* Last revis.: 2007-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creacurfile
param pPATH,pMASK,pCODSTR,pORDSTR,pSUBDIR,pCURSORE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CURSORE
  m.w_CURSORE=space(30)
  private w_NUMFIL
  m.w_NUMFIL=0
  private w_NUMSUB
  m.w_NUMSUB=space(5)
  private w_LOOP
  m.w_LOOP=0
  private w_LOOPS
  m.w_LOOPS=0
  private w_FILE
  m.w_FILE=space(254)
  private w_DATFIL
  m.w_DATFIL=ctod("  /  /  ")
  private w_OLDDIM
  m.w_OLDDIM=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creacurfile"
if vartype(__creacurfile_hook__)='O'
  __creacurfile_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creacurfile('+Transform(pPATH)+','+Transform(pMASK)+','+Transform(pCODSTR)+','+Transform(pORDSTR)+','+Transform(pSUBDIR)+','+Transform(pCURSORE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creacurfile')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
creacurfile_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creacurfile('+Transform(pPATH)+','+Transform(pMASK)+','+Transform(pCODSTR)+','+Transform(pORDSTR)+','+Transform(pSUBDIR)+','+Transform(pCURSORE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creacurfile')
Endif
*--- Activity log
if vartype(__creacurfile_hook__)='O'
  __creacurfile_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creacurfile_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Crea cursore con elenco file EDI da processare  e relativa struttura
  * --- Path directory di estrapolazione file
  * --- Maschera di ricerca
  * --- Struttura associata
  * --- Ordinamento strutture import
  * --- Flag ricerca in sottodirectory
  *     N = Non ricercare nelle sotodirectory
  *     S = Ricerca nelle sottodirectory
  * --- Cursore da utilizzare (Se non specificato viene creato nuovo)
  m.pSUBDIR = IIF(VARTYPE(m.pSUBDIR)="C",m.pSUBDIR,"N")
  m.pCURSORE = IIF(VARTYPE(m.pCURSORE)="C", m.pCURSORE, "")
  if EMPTY(NVL(m.pCURSORE, " "))
    m.w_CURSORE = Sys(2015)
    CREATE CURSOR (m.w_CURSORE) (FILE C(254),CODSTR C(10) , ORDSTR N(3,0), DATA D(8))
  else
    m.w_CURSORE = m.pCURSORE
  endif
  * --- Ciclo sulla directory
   
 L_SearchDefa= m.pPATH 
 L_OldPath = Sys(5)+Sys(2003) 
 Set Default To &L_SearchDefa 
  m.w_NUMFIL = ADIR(arrayAll,ALLTRIM(m.pMASK)) 
  m.w_LOOP = 0
  do while m.w_LOOP<m.w_NUMFIL
    m.w_LOOP = m.w_LOOP + 1
    m.w_FILE = ADDBS(Alltrim(m.pPATH)) + Alltrim(Arrayall[m.w_LOOP,1])
    m.w_DATFIL = Arrayall[m.w_LOOP,3]
    if Len(m.w_FILE)<=200
      INSERT INTO (m.w_CURSORE) (FILE,CODSTR,ORDSTR,DATA) VALUES (m.w_FILE,ALLTRIM(m.pCODSTR), m.pORDSTR, m.w_DATFIL)
    endif
  enddo
  if m.pSUBDIR="S"
    DIMENSION ArrDir(1,1)
    ArrDir[1,1]=L_SearchDefa
    m.w_LOOP = 0
    do while m.w_LOOP < ALEN(ArrDir,1)
      m.w_LOOP = m.w_LOOP + 1
      SET DEFAULT TO &ArrDir[m.w_LOOP,1]
      m.w_NUMSUB = ADIR(ArrTmp,"","D")
      m.w_LOOPS = 0
      do while m.w_LOOPS < m.w_NUMSUB
        m.w_LOOPS = m.w_LOOPS + 1
        if m.w_LOOPS = 1
          * --- Ridimensiono vettore per ospitare le nuove directory (elimino 2 per directory . e ..)
          m.w_OLDDIM = ALEN(ArrDir,1)
          DIMENSION ArrDir(m.w_OLDDIM+m.w_NUMSUB-2,1)
        endif
        if ALLTRIM(ArrTmp(m.w_LOOPS,1))<>"." AND ALLTRIM(ArrTmp(m.w_LOOPS,1))<>".."
          ArrDir(m.w_OLDDIM+m.w_LOOPS-2,1)=ADDBS(ALLTRIM(SYS(5)+SYS(2003)))+ADDBS(ALLTRIM(ArrTmp(m.w_LOOPS,1)))
        endif
      enddo
    enddo
    * --- Ripristino puntamento directory di partenza
    Set Default to &L_OldPath
    * --- Copio l'array delle sottodirectory nel cursore appoggio ed elimino la directory principale (gi� processata)
    CREATE CURSOR SubDir (Dir C(254))
    INSERT INTO SubDir FROM ARRAY ArrDir
    DELETE FROM SubDIR WHERE ALLTRIM(Dir)==ALLTRIM(ArrDir[1,1])
    * --- Riordino il cursore per processare le sottodirectory in fila
    SELECT * FROM SubDir WHERE NOT DELETED() GROUP BY Dir ORDER BY Dir INTO CURSOR SubDir
    if USED("SubDir")
      SELECT "SubDir"
      GO TOP
      SCAN
      creacurfile(ALLTRIM(Dir) , m.pMASK, m.pCODSTR, m.pORDSTR, "N", m.w_CURSORE)
      ENDSCAN
      USE IN "SubDir"
    endif
  else
    * --- Ripristino puntamento directory di partenza
    Set Default to &L_OldPath
  endif
  * --- Ritorno cursore creato
  i_retcode = 'stop'
  i_retval = m.w_CURSORE
  return
endproc


* --- END CREACURFILE
* --- START CREAELENELE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creaelenele                                                     *
*              CREA ARRAY ELEMENTI COLLEGATI                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-22                                                      *
* Last revis.: 2015-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creaelenele
param pCODSTR,pELEMEN,pARRAY,pRICORS,pazione

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODELE
  m.w_CODELE=space(20)
  private w_DIMENARR
  m.w_DIMENARR=0
  private w_CURSORE
  m.w_CURSORE=space(10)
  private w_CODTAB
  m.w_CODTAB=space(30)
  private w_QUALIF
  m.w_QUALIF=space(254)
  private w_OKQUAL
  m.w_OKQUAL=.f.
  private w_ELFUNSCE
  m.w_ELFUNSCE=space(50)
  private w_TIPO
  m.w_TIPO=space(1)
  private pELIDCHLD
  m.pELIDCHLD=0
  private w_VALTXT
  m.w_VALTXT=space(254)
  private w_OKQUAL1
  m.w_OKQUAL1=.f.
* --- WorkFile variables
  private VAELEMEN_idx
  VAELEMEN_idx=0
  private VASTRUTT_idx
  VASTRUTT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creaelenele"
if vartype(__creaelenele_hook__)='O'
  __creaelenele_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creaelenele('+Transform(pCODSTR)+','+Transform(pELEMEN)+','+Transform(pARRAY)+','+Transform(pRICORS)+','+Transform(pazione)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creaelenele')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if creaelenele_OpenTables()
  creaelenele_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creaelenele('+Transform(pCODSTR)+','+Transform(pELEMEN)+','+Transform(pARRAY)+','+Transform(pRICORS)+','+Transform(pazione)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creaelenele')
Endif
*--- Activity log
if vartype(__creaelenele_hook__)='O'
  __creaelenele_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creaelenele_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Determina Array elementi  riga file
  * --- T determina Tabella  associata
  *     E calcola array elementi associati
  *     Q calcola array elementi associati e riporta valore qualificatore 
  m.w_CURSORE = sys(2015)
  m.w_OKQUAL = .F.
  if m.pRICORS<100
    * --- Read from VAELEMEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[VAELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[VAELEMEN_idx,2],.t.,VAELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ELTABIMP,EL__TIPO,ELFUNSCE,ELVALTXT"+;
        " from "+i_cTable+" VAELEMEN where ";
            +"ELCODSTR = "+cp_ToStrODBC(m.pCODSTR);
            +" and ELCODICE = "+cp_ToStrODBC(m.pELEMEN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ELTABIMP,EL__TIPO,ELFUNSCE,ELVALTXT;
        from (i_cTable) where;
            ELCODSTR = m.pCODSTR;
            and ELCODICE = m.pELEMEN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_CODTAB = NVL(cp_ToDate(_read_.ELTABIMP),cp_NullValue(_read_.ELTABIMP))
      m.w_TIPO = NVL(cp_ToDate(_read_.EL__TIPO),cp_NullValue(_read_.EL__TIPO))
      m.w_ELFUNSCE = NVL(cp_ToDate(_read_.ELFUNSCE),cp_NullValue(_read_.ELFUNSCE))
      m.w_VALTXT = NVL(cp_ToDate(_read_.ELVALTXT),cp_NullValue(_read_.ELVALTXT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if m.pAZIONE="T" AND NOT EMPTY(m.w_CODTAB)
      i_retcode = 'stop'
      i_retval = m.w_CODTAB
      return
    endif
    if Not Empty(pARRAY[int(Alen(m.pARRAY)/2),1])
       
 Dimension m.pARRAY(int(Alen(m.pARRAY)/2) +1,2)
    endif
    * --- Nel caso del padre non memorizzo qualificatore perch� non significativo
     
 pARRAY[int(Alen(m.pARRAY)/2),1]=IIF(Not Empty(m.w_VALTXT),m.pELEMEN,"") 
 pARRAY[int(Alen(m.pARRAY)/2),2]=""
    m.pELIDCHLD = 0
    if m.w_TIPO="S" and !EMPTY(NVL(m.w_ELFUNSCE , " "))
      L_ELFUNSCE= m.w_ELFUNSCE
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t.
      m.pELIDCHLD = &L_ELFUNSCE
      on error &L_OldError 
      if L_Err
        m.pELIDCHLD = -1
      endif
    endif
    LOCAL TestMacro
    cursore=m.w_CURSORE
    Do vq_exec with "..\VEFA\EXE\QUERY\GSVA2BIM",createobject("cp_getfuncvar"),Cursore,"",.f.,.t. 
 SELECT (Cursore) 
 go top 
 Scan
    m.w_CODELE = &cursore .ELCODICE
    m.w_QUALIF = &cursore .QUALIF
    if m.pAZIONE$ "E-Q" 
      TestMacro=NVL(&cursore .TIPO," ")
      if TestMacro="S"
        if m.pAZIONE="Q"
          if Not empty(m.w_QUALIF)
            if Not Empty(pARRAY[int(Alen(m.pARRAY)/2),1])
              * --- Creo posizione nell'array per scrivere il dato
               
 Dimension m.pARRAY(int(Alen(m.pARRAY)/2)+1,2)
            endif
             
 pARRAY[int(Alen(m.pARRAY)/2),1]=m.w_CODELE 
 pARRAY[int(Alen(m.pARRAY)/2),2]=m.w_QUALIF
            m.w_OKQUAL = .T.
          endif
        else
          * --- Scrivo il dato solo se elemento di tipo tag
          if Not Empty(pARRAY[int(Alen(m.pARRAY)/2),1])
            * --- Creo posizione nell'array per scrivere il dato
             
 Dimension m.pARRAY(int(Alen(m.pARRAY)/2)+1,2)
          endif
           
 pARRAY[int(Alen(m.pARRAY)/2),1]=m.w_CODELE 
 pARRAY[int(Alen(m.pARRAY)/2),2]=""
          m.w_OKQUAL = .T.
        endif
      else
        * --- Determino figli
        m.w_OKQUAL1 = creaelenele(m.pCODSTR,m.w_codele,@m.pARRAY,m.pRICORS+1,m.pAZIONE)
        m.w_OKQUAL = IIF(m.w_OKQUAL,m.w_OKQUAL,m.w_OKQUAL1)
         
 cursore=m.w_CURSORE 
 SELECT (cursore)
      endif
    else
      m.w_CODTAB = creaelenele(m.pCODSTR,m.w_codele,@m.pARRAY,m.pRICORS+1,"T")
       
 cursore=m.w_CURSORE 
 SELECT (cursore)
      if NOT EMPTY(m.w_CODTAB)
        i_retcode = 'stop'
        i_retval = m.w_CODTAB
        return
      endif
    endif
    EndScan 
 use in (cursore)
  endif
  if m.pAZIONE$ "E-Q" 
    i_retcode = 'stop'
    i_retval = m.w_OKQUAL
    return
  else
    i_retcode = 'stop'
    i_retval = " "
    return
  endif
endproc


  function creaelenele_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='VAELEMEN'
    i_cWorkTables[2]='VASTRUTT'
    return(cp_OpenFuncTables(2))
* --- END CREAELENELE
* --- START CREATABEDI
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creatabedi                                                      *
*              CREA TABELLE PER GENERAZIONE EDI                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-31                                                      *
* Last revis.: 2014-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creatabedi
param pCODSTR,pCODTAB,pARRKEY

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LOOP
  m.w_LOOP=0
  private w_ARCHIVIO
  m.w_ARCHIVIO=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_POSKEY
  m.w_POSKEY=0
  private w_CODENT
  m.w_CODENT=space(10)
  private w_CODTAB
  m.w_CODTAB=space(30)
  private w_FLATAB
  m.w_FLATAB=space(15)
  private w_FL_AZI
  m.w_FL_AZI=space(1)
  private w_TABCOLL
  m.w_TABCOLL=space(30)
  private w_LINK
  m.w_LINK=space(254)
  private w_OKJOIN
  m.w_OKJOIN=.f.
  private w_WHERE
  m.w_WHERE=space(254)
  private w_EXPRES
  m.w_EXPRES=space(254)
  private w_INDICE
  m.w_INDICE=0
* --- WorkFile variables
  private VASTRUTT_idx
  VASTRUTT_idx=0
  private ENT_DETT_idx
  ENT_DETT_idx=0
  private FLATMAST_idx
  FLATMAST_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creatabedi"
if vartype(__creatabedi_hook__)='O'
  __creatabedi_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creatabedi('+Transform(pCODSTR)+','+Transform(pCODTAB)+','+Transform(pARRKEY)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatabedi')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if creatabedi_OpenTables()
  creatabedi_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_gsar1bvs')
  use in _Curs_gsar1bvs
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creatabedi('+Transform(pCODSTR)+','+Transform(pCODTAB)+','+Transform(pARRKEY)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatabedi')
Endif
*--- Activity log
if vartype(__creatabedi_hook__)='O'
  __creatabedi_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creatabedi_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione popola tabella temporanea creata ne batch
  *     chiamante con i dati ricavati in funzione del filtro sulla tabella principale 
  *     passato attraverso array e le relazioni definite nell'entit� associata
  * --- codice struttura utilizzato
  * --- codice Tabella
  * --- Array campi chiave da utilizzare come filtro
  * --- Read from VASTRUTT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[VASTRUTT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[VASTRUTT_idx,2],.t.,VASTRUTT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "STCODENT,STFLATAB"+;
      " from "+i_cTable+" VASTRUTT where ";
          +"STCODICE = "+cp_ToStrODBC(m.pCODSTR);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      STCODENT,STFLATAB;
      from (i_cTable) where;
          STCODICE = m.pCODSTR;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
    m.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Read from FLATMAST
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[FLATMAST_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[FLATMAST_idx,2],.t.,FLATMAST_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "FTFL_AZI"+;
      " from "+i_cTable+" FLATMAST where ";
          +"FTCODICE = "+cp_ToStrODBC(m.w_FLATAB);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      FTFL_AZI;
      from (i_cTable) where;
          FTCODICE = m.w_FLATAB;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_FL_AZI = NVL(cp_ToDate(_read_.FTFL_AZI),cp_NullValue(_read_.FTFL_AZI))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  m.w_FLATAB = IIF(m.w_FL_AZI="S","xxx"+Alltrim(m.w_FLATAB),ALLTRIM(m.w_FLATAB))
  m.w_LOOP = 0
  m.w_POSKEY = 1
  =cp_ReadXdc()
  m.w_ARCHIVIO = alltrim(m.pCODTAB)
  m.w_IDXTABLE = cp_OpenTable( m.w_ARCHIVIO ,.T.)
  m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_ARCHIVIO ,1 ) )
  sel=""
  do while m.w_POSKEY<>0
    m.w_POSKEY = At( "," , m.w_CHIAVE )
    if m.w_POSKEY>0
      m.w_CAMPO = Left( m.w_CHIAVE , m.w_POSKEY -1 )
    else
      m.w_CAMPO = Alltrim( m.w_CHIAVE )
    endif
    m.w_CHIAVE = SUBSTR(m.w_CHIAVE,m.w_POSKEY+1,LEN(m.w_CHIAVE)-m.w_POSKEY)
    sel=sel + Alltrim(m.pCODTAB)+"."+Alltrim(m.w_CAMPO)+","
  enddo
   
 L_FONTE_IDX=Cp_OpenTable( m.pCODTAB) 
 Tab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) +" " +m.pCODTAB 
 L_nConn = i_TableProp[L_FONTE_IDX,3]
   
 ccmdsql=""
  if Type("pARRKEY[1]")="C" AND m.w_IDXTABLE<>0
    * --- Solo se esiste almeno una dimensione
    m.w_LOOP = 0
    do while m.w_LOOP< INT(ALEN( m.pARRKEY ) /3)
      m.w_LOOP = m.w_LOOP+1
      m.w_CAMPO = pARRKEY[m.w_LOOP,2]
      if Not Empty(m.w_CAMPO)
         
 ccmdsql=ccmdsql+IIF( m.w_LOOP=1, " where "," and " )+ alltrim(pARRKEY[m.w_LOOP,3]) + "."+ Alltrim(m.w_CAMPO) +" = "+ Cp_tostrodbc(pARRKEY[m.w_LOOP,1])
      endif
    enddo
    m.w_CODTAB = m.pCODTAB
    m.w_TABCOLL = m.pCODTAB
    m.w_OKJOIN = .F.
     
 CcmdJoin="" 
 cWhere=""
    m.w_LOOP = 0
    * --- Costruisco frase di join sostituendo le tabelle collegate con il relativo  legame
    *     se legame valorizzato
    do while NOT EMPTY(m.w_TABCOLL) AND m.w_LOOP<100
      * --- Read from ENT_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[ENT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[ENT_DETT_idx,2],.t.,ENT_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ENTABCOL,EN__LINK,ENFILTRO"+;
          " from "+i_cTable+" ENT_DETT where ";
              +"ENCODICE = "+cp_ToStrODBC(m.w_CODENT);
              +" and EN_TABLE = "+cp_ToStrODBC(m.w_CODTAB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ENTABCOL,EN__LINK,ENFILTRO;
          from (i_cTable) where;
              ENCODICE = m.w_CODENT;
              and EN_TABLE = m.w_CODTAB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_TABCOLL = NVL(cp_ToDate(_read_.ENTABCOL),cp_NullValue(_read_.ENTABCOL))
        m.w_LINK = NVL(cp_ToDate(_read_.EN__LINK),cp_NullValue(_read_.EN__LINK))
        m.w_WHERE = NVL(cp_ToDate(_read_.ENFILTRO),cp_NullValue(_read_.ENFILTRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not empty(m.w_LINK) and Not empty(m.w_TABCOLL)
         
 L_FONTE_IDX=Cp_OpenTable( m.w_CODTAB) 
 CodTab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 L_FONTE_IDX=Cp_OpenTable( m.w_TABCOLL) 
 TabColl=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2])
        CondJoin="( "+ ALLtrim(TABCOLL) +" "+ ALLtrim(m.w_TABCOLL)+ " Inner Join " + Alltrim(CODTAB) +" "+Alltrim(m.w_CODTAB)+ " On "+ Alltrim(m.w_LINK) +" )"
        if Empty(CcmdJoin)
           
 CcmdJoin=CondJoin
        else
          CcmdJoin=STRTRAN(CcmdJoin,ALLtrim(CODTAB) +" "+ ALLtrim(m.w_CODTAB),CondJoin)
        endif
        m.w_OKJOIN = .T.
      endif
      if Not empty(m.w_WHERE) AND m.w_CODTAB=m.pCODTAB
        * --- Considero eventuale condizione di filtro solo della tabella corrente
         
 ccmdsql= "" 
 cWhere= Alltrim(m.pCodTab) + "  Where " + m.w_WHERE
      endif
      m.w_CODTAB = m.w_TABCOLL
      m.w_LOOP = m.w_LOOP + 1
    enddo
    * --- Select from gsar1bvs
    do vq_exec with 'gsar1bvs',createobject('cp_getfuncvar'),'_Curs_gsar1bvs','',.f.,.t.
    if used('_Curs_gsar1bvs')
      select _Curs_gsar1bvs
      locate for 1=1
      do while not(eof())
      if _Curs_gsar1bvs.EL__TIPO="V"
        if OCCUR(Alltrim(_Curs_gsar1bvs.EL_CAMPO) ,sel)=0
          sel= sel + Alltrim(_Curs_gsar1bvs.EL_CAMPO) + ","
        endif
      endif
      * --- Estrappolo campi dalla formula
      m.w_EXPRES = Nvl(_Curs_gsar1bvs.ELEXPRES," ")
      if Not Empty(w_express)
        m.w_INDICE = 0
        do while occurs(ALLTRIM(m.pCODTAB)+".",m.w_EXPRES)>0 AND m.w_INDICE<100
           
 campo=SUBSTR(m.w_EXPRES,rat(m.pCODTAB,m.w_EXPRES)+LEN(m.pCODTAB)+1,8)
          * --- Verifico esistenza del campo calcolato
          if I_DCX.GETfieldidx(Alltrim(m.pCODTAB),Alltrim(CAMPO)) <>0
            if OCCUR(campo,sel)=0
               
 sel= sel + Alltrim(CAMPO)+ ","
            endif
          endif
          m.w_EXPRES = STRTRAN(m.w_EXPRES,Alltrim(m.pCODTAB)+"."+Alltrim(CAMPO),"")
          m.w_INDICE = m.w_INDICE + 1
        enddo
      endif
        select _Curs_gsar1bvs
        continue
      enddo
      use
    endif
    if m.w_OKJOIN
      Tab=""
       
 ccmdsql=CcmdJoin + " " + ccmdsql
    endif
    * --- elimino ultima virgola
    if Not empty(sel)
      sel=LEFT(Alltrim(sel),LEN(sel)-1) 
    else
      sel=" * "
    endif
    * --- Popolo la tabella tempornea con il risultato della select costruita dall'entit�
     
 fisname=GetETPhName(ALLTRIM(m.w_FLATAB),m.pCODTAB)
     
 cmsql="Insert Into "+ alltrim(fisname) + " ( "+ alltrim(sel) +" ) Select " + sel + " from " + tab + ccmdsql 
 cp_SQL(L_nConn, cmsql, fisname )
    if Not Empty(cWhere)
       
 cWhere=strtran(cWhere,Alltrim(m.pCODTAB),fisname) 
 cmsql="Delete from Not( "+ alltrim(cWhere) +" )" 
 cp_SQL(L_nConn, cmsql, fisname )
    endif
    i_retcode = 'stop'
    i_retval = Not L_Err
    return
  else
    i_retcode = 'stop'
    i_retval = .f.
    return
  endif
endproc


  function creatabedi_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='VASTRUTT'
    i_cWorkTables[2]='ENT_DETT'
    i_cWorkTables[3]='FLATMAST'
    return(cp_OpenFuncTables(3))
* --- END CREATABEDI
* --- START CREATUPDCUR
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: creatupdcur                                                     *
*              CREA \MODIFCA CURSORE CON CAMPI CHIAVE                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-31                                                      *
* Last revis.: 2008-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func creatupdcur
param pCURSOR,pCAMPO,pAZIONE

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LOOP
  m.w_LOOP=0
  private w_ARCHIVIO
  m.w_ARCHIVIO=space(30)
  private w_IDXTABLE
  m.w_IDXTABLE=0
  private w_CHIAVE
  m.w_CHIAVE=space(40)
  private w_CAMPO
  m.w_CAMPO=space(30)
  private w_POSKEY
  m.w_POSKEY=0
  private w_LUNG
  m.w_LUNG=0
  private w_TIPO
  m.w_TIPO=space(1)
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "creatupdcur"
if vartype(__creatupdcur_hook__)='O'
  __creatupdcur_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'creatupdcur('+Transform(pCURSOR)+','+Transform(pCAMPO)+','+Transform(pAZIONE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatupdcur')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
creatupdcur_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'creatupdcur('+Transform(pCURSOR)+','+Transform(pCAMPO)+','+Transform(pAZIONE)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'creatupdcur')
Endif
*--- Activity log
if vartype(__creatupdcur_hook__)='O'
  __creatupdcur_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure creatupdcur_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- codice CURSORE
  * --- Campo da aggiungetre al cursore
  * --- Tipo azione 
  *     U update
  *     C create
  if Vartype("pCURSOR")="U"
    m.pCURSOR = " "
  endif
  if Vartype("pCAMPO")="U"
    m.pCAMPO = " "
  endif
  if Vartype("pAZIONE")="U"
    m.pAZIONE = "C"
  endif
  if Not Empty(m.pCURSOR)
    if m.pAZIONE="C"
      if Not USED(Alltrim(m.pCURSOR))
        m.w_LOOP = 0
        m.w_POSKEY = 1
        =cp_ReadXdc()
        m.w_ARCHIVIO = alltrim(m.pCURSOR)
        m.w_IDXTABLE = cp_OpenTable( m.w_ARCHIVIO ,.T.)
        m.w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( m.w_ARCHIVIO ,1 ) )
         
 L_FONTE_IDX=Cp_OpenTable( m.pCURSOR) 
 Tab=cp_setazi( I_TABLEPROP[L_FONTE_IDX,2]) 
 L_nConn = i_TableProp[L_FONTE_IDX,3]
         
 ccmdsql="Create Cursor " + Alltrim(m.pCURSOR) + " ( "
        if m.w_IDXTABLE<>0
          * --- Solo se esiste almeno una dimensione
          do while  m.w_POSKEY<>0 and m.w_loop<100
            m.w_POSKEY = At( "," , m.w_CHIAVE )
            if m.w_POSKEY>0
              m.w_CAMPO = Left( m.w_CHIAVE , m.w_POSKEY -1 )
            else
              m.w_CAMPO = Alltrim( m.w_CHIAVE )
            endif
            m.w_CHIAVE = SUBSTR(m.w_CHIAVE,m.w_POSKEY+1,LEN(m.w_CHIAVE)-m.w_POSKEY)
            m.w_LOOP = m.w_LOOP+1
            * --- Read from XDC_FIELDS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[XDC_FIELDS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[XDC_FIELDS_idx,2],.t.,XDC_FIELDS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "FLTYPE,FLLENGHT"+;
                " from "+i_cTable+" XDC_FIELDS where ";
                    +"TBNAME = "+cp_ToStrODBC(m.pCURSOR);
                    +" and FLNAME = "+cp_ToStrODBC(m.w_CAMPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                FLTYPE,FLLENGHT;
                from (i_cTable) where;
                    TBNAME = m.pCURSOR;
                    and FLNAME = m.w_CAMPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              m.w_TIPO = NVL(cp_ToDate(_read_.FLTYPE),cp_NullValue(_read_.FLTYPE))
              m.w_LUNG = NVL(cp_ToDate(_read_.FLLENGHT),cp_NullValue(_read_.FLLENGHT))
              use
              if i_Rows=0
              i_retcode = 'stop'
              i_retval = .f.
              return
              endif
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
             
 ccmdsql=ccmdsql+ Alltrim(m.w_CAMPO) + " "+ alltrim(m.w_TIPO)+"("+Alltrim(str(m.w_lung))+") ,"
          enddo
          * --- elimino ultima virgola e aggiungo parentesi chiusa
           
 ccmdsql=LEFT(Alltrim(ccmdsql),LEN(ccmdsql)-1) 
 ccmdsql=ccmdsql+")"
        else
          i_retcode = 'stop'
          i_retval = .f.
          return
        endif
      else
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    else
      if USED(Alltrim(m.pCURSOR)) AND Type(Alltrim(m.PCURSOR) + "."+ Alltrim(m.pCAMPO))="U"
        if Not Empty(m.pCAMPO)
          if I_DCX.GETfieldidx(Alltrim(m.pCURSOR),Alltrim(m.pCAMPO)) <>0
            m.w_LUNG = I_DCX.GEtfieldlen(Alltrim(m.pCURSOR),I_DCX.GETfieldidx(Alltrim(m.pCURSOR),Alltrim(m.pCAMPO)))
            m.w_TIPO = I_DCX.GEtfieldtype(Alltrim(m.pCURSOR),I_DCX.GETfieldidx(Alltrim(m.pCURSOR),Alltrim(m.pCAMPO)))
             
 ccmdsql="ALTER table " + Alltrim(m.pCURSOR) + " ADD COLUMN " + Alltrim(m.pCAMPO) + " "+ alltrim(m.w_TIPO)+"("+Alltrim(str(m.w_lung))+") "
          else
            i_retcode = 'stop'
            i_retval = .f.
            return
          endif
        else
          i_retcode = 'stop'
          i_retval = .f.
          return
        endif
      else
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    endif
  else
    i_retcode = 'stop'
    i_retval = .f.
    return
  endif
   
 L_Err=.t. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.f. 
 &ccmdsql 
 on error &L_OldError 
  i_retcode = 'stop'
  i_retval = L_Err
  return
endproc


* --- END CREATUPDCUR
* --- START DELRECIMP
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: delrecimp                                                       *
*              ELIMINA RECORD IMPORTAZIONE SU TABELLE EDI                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-31                                                      *
* Last revis.: 2007-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func delrecimp
param pARCHI,pSERIMP,pVerbose,pLog

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODTAB
  m.w_CODTAB=space(30)
  private w_RESULT
  m.w_RESULT=space(1)
  private w_PHNAME
  m.w_PHNAME=space(30)
  private w_TABFLAT
  m.w_TABFLAT=space(30)
* --- WorkFile variables
  private FLATDETT_idx
  FLATDETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "delrecimp"
if vartype(__delrecimp_hook__)='O'
  __delrecimp_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'delrecimp('+Transform(pARCHI)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'delrecimp')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if delrecimp_OpenTables()
  delrecimp_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_FLATDETT')
  use in _Curs_FLATDETT
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'delrecimp('+Transform(pARCHI)+','+Transform(pSERIMP)+','+Transform(pVerbose)+','+Transform(pLog)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'delrecimp')
Endif
*--- Activity log
if vartype(__delrecimp_hook__)='O'
  __delrecimp_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure delrecimp_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Questa funzione esegue cancellazione record legati a seriale
  *     importazione passato come parametro su tutte le tabelle EDI-TEMP-TABLE
  *     collegate alla tabella piatta passata per parametro
  * --- Seriale importazione
  * --- flag verbose
  * --- Log errori
  m.w_RESULT = 0
  * --- Definisco array cancellazione
   
 Dimension ArrVal[1,2] 
 ArrVal[1,1]="FT___UID" 
 ArrVal[1,2]=m.pSERIMP
  m.w_TABFLAT = IIF(SUBSTR(m.pARCHI,1,3)="xxx",SUBSTR(m.pARCHI,4),m.pARCHI)
  * --- Select from FLATDETT
  i_nConn=i_TableProp[FLATDETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[FLATDETT_idx,2],.t.,FLATDETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select FTTABNAM,FTCODICE from "+i_cTable+" FLATDETT ";
        +" where FTCODICE="+cp_ToStrODBC(m.w_TABFLAT)+"";
        +" group by FTCODICE,FTTABNAM";
        +" order by FTTABNAM";
         ,"_Curs_FLATDETT")
  else
    select FTTABNAM,FTCODICE from (i_cTable);
     where FTCODICE=m.w_TABFLAT;
     group by FTCODICE,FTTABNAM;
     order by FTTABNAM;
      into cursor _Curs_FLATDETT
  endif
  if used('_Curs_FLATDETT')
    select _Curs_FLATDETT
    locate for 1=1
    do while not(eof())
    m.w_CODTAB = _Curs_FLATDETT.FTTABNAM
    * --- Try
    delrecimp_Try_035A26C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      m.w_RESULT = -1
      AddMsgNL("Errore nella cancellazione tabella %1%0 errore:%2 ", m.pLog , Alltrim(m.w_CODTAB) ,Message())
    endif
    * --- End
      select _Curs_FLATDETT
      continue
    enddo
    use
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc
  proc delrecimp_Try_035A26C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  m.w_PHNAME = GetETPhName(m.pARCHI,m.w_CODTAB)
  DELETETABLE( m.w_PHNAME , @ArrVal , m.pLog ,m.pVerbose) 
  if VarType( m.pLog ) ="O" And m.pVerbose
    AddMsgNL("Eseguita cancellazione tabella %1 ", m.pLog , Alltrim(m.w_CODTAB))
  endif
    return


  function delrecimp_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='FLATDETT'
    return(cp_OpenFuncTables(1))
* --- END DELRECIMP
* --- START GETCHILDUNILOG
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getchildunilog                                                  *
*              SCELTA TIPO FIGLIO UNITA' LOGISTICHE                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-30                                                      *
* Last revis.: 2007-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getchildunilog
param pCODRIC,pCODMAG

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODART
  m.w_CODART=space(20)
  private w_ARMAGPRE
  m.w_ARMAGPRE=space(5)
  private w_ARFLLOTT
  m.w_ARFLLOTT=space(1)
  private w_ARGESMAT
  m.w_ARGESMAT=space(1)
  private w_MGFLUBIC
  m.w_MGFLUBIC=space(1)
* --- WorkFile variables
  private KEY_ARTI_idx
  KEY_ARTI_idx=0
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private MAGAZZIN_idx
  MAGAZZIN_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getchildunilog"
if vartype(__getchildunilog_hook__)='O'
  __getchildunilog_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getchildunilog('+Transform(pCODRIC)+','+Transform(pCODMAG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getchildunilog')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getchildunilog_OpenTables()
  getchildunilog_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getchildunilog('+Transform(pCODRIC)+','+Transform(pCODMAG)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getchildunilog')
Endif
*--- Activity log
if vartype(__getchildunilog_hook__)='O'
  __getchildunilog_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getchildunilog_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.pCODRIC = ALLTRIM(m.pCODRIC)
  m.pCODMAG = ALLTRIM(m.pCODMAG)
  * --- Try
  getchildunilog_Try_035B0208()
  * --- Catch
  if !empty(i_Error)
    i_ErrMsg=i_Error
    i_Error=''
    i_retcode = 'stop'
    i_retval = -1
    return
  endif
  * --- End
endproc
  proc getchildunilog_Try_035B0208()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Read from KEY_ARTI
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[KEY_ARTI_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[KEY_ARTI_idx,2],.t.,KEY_ARTI_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "CACODART"+;
      " from "+i_cTable+" KEY_ARTI where ";
          +"CACODICE = "+cp_ToStrODBC(m.pCODRIC);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      CACODART;
      from (i_cTable) where;
          CACODICE = m.pCODRIC;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
    use
    if i_Rows=0
    i_retcode = 'stop'
    i_retval = -1
    return
    endif
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Try
  getchildunilog_Try_035A55E8()
  * --- Catch
  if !empty(i_Error)
    i_ErrMsg=i_Error
    i_Error=''
    i_retcode = 'stop'
    i_retval = -1
    return
  endif
  * --- End
    return
  proc getchildunilog_Try_035A55E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Read from ART_ICOL
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[ART_ICOL_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[ART_ICOL_idx,2],.t.,ART_ICOL_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "ARGESMAT,ARFLLOTT,ARMAGPRE"+;
      " from "+i_cTable+" ART_ICOL where ";
          +"ARCODART = "+cp_ToStrODBC(m.w_CODART);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      ARGESMAT,ARFLLOTT,ARMAGPRE;
      from (i_cTable) where;
          ARCODART = m.w_CODART;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_ARGESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
    m.w_ARFLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
    m.w_ARMAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
    use
    if i_Rows=0
    i_retcode = 'stop'
    i_retval = -1
    return
    endif
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if m.w_ARGESMAT="S"
    i_retcode = 'stop'
    i_retval = 3
    return
  else
    if m.w_ARFLLOTT="S"
      i_retcode = 'stop'
      i_retval = 2
      return
    else
      if EMPTY(m.pCODMAG)
        m.pCODMAG = m.w_ARMAGPRE
      endif
      * --- Try
      getchildunilog_Try_035B0778()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        i_retcode = 'stop'
        i_retval = -1
        return
      endif
      * --- End
    endif
  endif
    return
  proc getchildunilog_Try_035B0778()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  m.w_MGFLUBIC = "N"
  * --- Read from MAGAZZIN
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[MAGAZZIN_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[MAGAZZIN_idx,2],.t.,MAGAZZIN_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "MGFLUBIC"+;
      " from "+i_cTable+" MAGAZZIN where ";
          +"MGCODMAG = "+cp_ToStrODBC(m.pCODMAG);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      MGFLUBIC;
      from (i_cTable) where;
          MGCODMAG = m.pCODMAG;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_MGFLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
    use
    if i_Rows=0
    i_retcode = 'stop'
    i_retval = -1
    return
    endif
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if m.w_MGFLUBIC="S"
    i_retcode = 'stop'
    i_retval = 2
    return
  else
    i_retcode = 'stop'
    i_retval = 1
    return
  endif
    return


  function getchildunilog_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='KEY_ARTI'
    i_cWorkTables[2]='ART_ICOL'
    i_cWorkTables[3]='MAGAZZIN'
    return(cp_OpenFuncTables(3))
* --- END GETCHILDUNILOG
* --- START GETCLONENAME
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getclonename                                                    *
*              DETERMINA NOME TABELLA MIRROR                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-12                                                      *
* Last revis.: 2008-06-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getclonename
param pTable

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_PHNAME
  m.w_PHNAME=space(50)
  private w_IDXTABLE
  m.w_IDXTABLE=0
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getclonename"
if vartype(__getclonename_hook__)='O'
  __getclonename_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getclonename('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getclonename')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
getclonename_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getclonename('+Transform(pTable)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getclonename')
Endif
*--- Activity log
if vartype(__getclonename_hook__)='O'
  __getclonename_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getclonename_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il nome logico di una tabella restituisce il nome della corrispondente
  *     tabella di clone.
  *     Es. da CONTI in VF_DEMOCONTI
  *     
  m.w_IDXTABLE = cp_OpenTable( m.pTable ,.T.)
  m.w_PHNAME = cp_SetAzi(i_TableProp[ m.w_IDXTABLE ,2]) 
  i_retcode = 'stop'
  i_retval = "VF_"+Alltrim( m.w_PHNAME )
  return
endproc


* --- END GETCLONENAME
* --- START GETFLDEVA
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: getfldeva                                                       *
*              LEGGE DATI DOCUMENTO EVASO                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-15                                                      *
* Last revis.: 2006-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func getfldeva
param pSERIAL,pROWNUM,pTABELLA,pCAMPO,pTIPO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_SERRIF
  m.w_SERRIF=space(10)
  private w_ROWRIF
  m.w_ROWRIF=0
* --- WorkFile variables
  private DOC_DETT_idx
  DOC_DETT_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "getfldeva"
if vartype(__getfldeva_hook__)='O'
  __getfldeva_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'getfldeva('+Transform(pSERIAL)+','+Transform(pROWNUM)+','+Transform(pTABELLA)+','+Transform(pCAMPO)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getfldeva')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if getfldeva_OpenTables()
  getfldeva_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'getfldeva('+Transform(pSERIAL)+','+Transform(pROWNUM)+','+Transform(pTABELLA)+','+Transform(pCAMPO)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'getfldeva')
Endif
*--- Activity log
if vartype(__getfldeva_hook__)='O'
  __getfldeva_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure getfldeva_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Determina valore del campo passato dal documento evaso
  *     Cercandolo in DOC_DETT e  RAG_FATT
  do case
    case m.pTIPO="N"
      TIPO=0
    case m.pTIPO="D"
      TIPO=CP_CHARTODATE("    -  -  ")
    otherwise
      TIPO=" "
  endcase
  * --- Read from DOC_DETT
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[DOC_DETT_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[DOC_DETT_idx,2],.t.,DOC_DETT_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "MVSERRIF,MVROWRIF"+;
      " from "+i_cTable+" DOC_DETT where ";
          +"MVSERIAL = "+cp_ToStrODBC(m.pSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(m.pROWNUM);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      MVSERRIF,MVROWRIF;
      from (i_cTable) where;
          MVSERIAL = m.pSERIAL;
          and CPROWNUM = m.pROWNUM;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_SERRIF = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
    m.w_ROWRIF = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
    use
    if i_Rows=0
    i_retcode = 'stop'
    i_retval = TIPO
    return
    endif
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  do case
    case m.pTABELLA="DOC_MAST"
      Dimension ArrVal[1,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL
    case m.pTABELLA="DOC_DETT"
      Dimension ArrVal[2,2] 
 ArrVal[1,1]="MVSERIAL" 
 ArrVal[1,2]=m.pSERIAL 
 ArrVal[2,1]="CPROWNUM" 
 ArrVal[2,2]=m.pROWNUM
  endcase
  if Not empty(m.pTABELLA)
    NC=READTABLE(m.pTABELLA,m.pCAMPO,@ARRVAL,,,)
    if Reccount((Nc))<>0
       
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 CAMPO=Alltrim(NC)+"."+Alltrim(m.pCAMPO) 
 VALORE=&CAMPO 
 on error &L_OldError 
      if Not L_Err
        TIPO=VALORE
      endif
    endif
  endif
  * --- Chiudo cursore read
  if Used((NC))
     
 Select (NC) 
 use
  endif
  i_retcode = 'stop'
  i_retval = TIPO
  return
endproc


  function getfldeva_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='DOC_DETT'
    return(cp_OpenFuncTables(1))
* --- END GETFLDEVA
