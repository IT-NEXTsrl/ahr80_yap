* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_ast                                                        *
*              Struttura                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2017-01-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_ast"))

* --- Class definition
define class tgsva_ast as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 658
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-01-09"
  HelpContextID=109736553
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Constant Properties
  VASTRUTT_IDX = 0
  ENT_MAST_IDX = 0
  VAELEMEN_IDX = 0
  VAFORMAT_IDX = 0
  CAR_SPEC_IDX = 0
  FLATMAST_IDX = 0
  ENT_DETT_IDX = 0
  cFile = "VASTRUTT"
  cKeySelect = "STCODICE"
  cKeyWhere  = "STCODICE=this.w_STCODICE"
  cKeyWhereODBC = '"STCODICE="+cp_ToStrODBC(this.w_STCODICE)';

  cKeyWhereODBCqualified = '"VASTRUTT.STCODICE="+cp_ToStrODBC(this.w_STCODICE)';

  cPrg = "gsva_ast"
  cComment = "Struttura"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_STCODICE = space(10)
  o_STCODICE = space(10)
  w_STSEPARA = space(1)
  w_STDESCRI = space(30)
  w_STINDENT = 0
  w_STFLPRED = space(1)
  w_STCODENT = space(15)
  o_STCODENT = space(15)
  w_DESCRI = space(50)
  w_ENTITA = space(15)
  w_EN_TABLE = space(30)
  w_STELROOT = space(20)
  w_DESELE = space(30)
  w_STFORTAG = space(20)
  w_DESFOR = space(30)
  w_STTIPFIL = space(1)
  o_STTIPFIL = space(1)
  w_STFLESAL = space(1)
  w_STFLGCBI = space(1)
  w_TIPO = space(1)
  w_STNOMFIL = space(150)
  w_STCODNOR = space(10)
  w_STFLATAB = space(15)
  o_STFLATAB = space(15)
  w_OLDFLAT = space(10)
  w_STLUNTAG = 0
  w_STFLTYPE = space(1)
  o_STFLTYPE = space(1)
  w_STFILXSD = space(254)
  o_STFILXSD = space(254)
  w_STTIPFIN = space(1)
  o_STTIPFIN = space(1)
  w_STCARFIN = space(1)
  w_STCARSEP = space(1)
  o_STCARSEP = space(1)
  w_STCARSEC = space(1)
  w_STLUNSEG = 0
  w_DESCAR = space(35)
  w_OLDLUN = 0
  w_STFLPROV = space(1)
  w_TIPENT = space(1)
  w_FLDES = space(50)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_STCRISTR = space(100)
  w_STBACXML = space(30)
  w_STDRIVER = space(30)
  w_STFUNRIC = space(30)
  w_STTIPGES = space(1)
  o_STTIPGES = space(1)
  w_STMODMSK = space(30)
  w_STFLIMPO = space(1)
  o_STFLIMPO = space(1)
  w_STORDIMP = 0
  w_STBACIMP = space(30)
  w_STTIPFIN = space(1)

  * --- Children pointers
  GSVA_MVN = .NULL.
  GSVA_MPF = .NULL.
  GSVA_MVP = .NULL.
  w_BTNREP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VASTRUTT','gsva_ast')
    stdPageFrame::Init()
    *set procedure to GSVA_MVN additive
    *set procedure to GSVA_MPF additive
    *set procedure to GSVA_MVP additive
    with this
      .Pages(1).addobject("oPag","tgsva_astPag1","gsva_ast",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Struttura")
      .Pages(1).HelpContextID = 181260888
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVA_MVN
    *release procedure GSVA_MPF
    *release procedure GSVA_MVP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_BTNREP = this.oPgFrm.Pages(1).oPag.BTNREP
      DoDefault()
    proc Destroy()
      this.w_BTNREP = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ENT_MAST'
    this.cWorkTables[2]='VAELEMEN'
    this.cWorkTables[3]='VAFORMAT'
    this.cWorkTables[4]='CAR_SPEC'
    this.cWorkTables[5]='FLATMAST'
    this.cWorkTables[6]='ENT_DETT'
    this.cWorkTables[7]='VASTRUTT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VASTRUTT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VASTRUTT_IDX,3]
  return

  function CreateChildren()
    this.GSVA_MVN = CREATEOBJECT('stdLazyChild',this,'GSVA_MVN')
    this.GSVA_MPF = CREATEOBJECT('stdLazyChild',this,'GSVA_MPF')
    this.GSVA_MVP = CREATEOBJECT('stdLazyChild',this,'GSVA_MVP')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVA_MVN)
      this.GSVA_MVN.DestroyChildrenChain()
      this.GSVA_MVN=.NULL.
    endif
    if !ISNULL(this.GSVA_MPF)
      this.GSVA_MPF.DestroyChildrenChain()
      this.GSVA_MPF=.NULL.
    endif
    if !ISNULL(this.GSVA_MVP)
      this.GSVA_MVP.DestroyChildrenChain()
      this.GSVA_MVP=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVA_MVN.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSVA_MPF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSVA_MVP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVA_MVN.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSVA_MPF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSVA_MVP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVA_MVN.NewDocument()
    this.GSVA_MPF.NewDocument()
    this.GSVA_MVP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVA_MVN.SetKey(;
            .w_STCODICE,"VNCODICE";
            )
      this.GSVA_MPF.SetKey(;
            .w_STCODICE,"PFCODSTR";
            )
      this.GSVA_MVP.SetKey(;
            .w_STCODICE,"PRCODSTR";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVA_MVN.ChangeRow(this.cRowID+'      1',1;
             ,.w_STCODICE,"VNCODICE";
             )
      .GSVA_MPF.ChangeRow(this.cRowID+'      1',1;
             ,.w_STCODICE,"PFCODSTR";
             )
      .GSVA_MVP.ChangeRow(this.cRowID+'      1',1;
             ,.w_STCODICE,"PRCODSTR";
             )
      .WriteTo_GSVA_MVP()
    endwith
    return

procedure WriteTo_GSVA_MVP()
  if at('gsva_mvp',lower(this.GSVA_MVP.class))<>0
    if this.GSVA_MVP.cnt.w_FLATAB<>this.w_STFLATAB
      this.GSVA_MVP.cnt.w_FLATAB = this.w_STFLATAB
      this.GSVA_MVP.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_STCODICE = NVL(STCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VASTRUTT where STCODICE=KeySet.STCODICE
    *
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VASTRUTT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VASTRUTT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VASTRUTT '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'STCODICE',this.w_STCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCRI = space(50)
        .w_EN_TABLE = space(30)
        .w_DESELE = space(30)
        .w_DESFOR = space(30)
        .w_TIPO = space(1)
        .w_DESCAR = space(35)
        .w_TIPENT = space(1)
        .w_FLDES = space(50)
        .w_STCODICE = NVL(STCODICE,space(10))
        .w_STSEPARA = NVL(STSEPARA,space(1))
        .w_STDESCRI = NVL(STDESCRI,space(30))
        .w_STINDENT = NVL(STINDENT,0)
        .w_STFLPRED = NVL(STFLPRED,space(1))
        .w_STCODENT = NVL(STCODENT,space(15))
          if link_1_9_joined
            this.w_STCODENT = NVL(ENCODICE109,NVL(this.w_STCODENT,space(15)))
            this.w_DESCRI = NVL(ENDESCRI109,space(50))
            this.w_TIPENT = NVL(ENTIPENT109,space(1))
          else
          .link_1_9('Load')
          endif
        .w_ENTITA = .w_STCODENT
          .link_1_11('Load')
        .w_STELROOT = NVL(STELROOT,space(20))
          if link_1_13_joined
            this.w_STELROOT = NVL(ELCODICE113,NVL(this.w_STELROOT,space(20)))
            this.w_DESELE = NVL(ELDESCRI113,space(30))
            this.w_TIPO = NVL(EL__TIPO113,space(1))
          else
          .link_1_13('Load')
          endif
        .w_STFORTAG = NVL(STFORTAG,space(20))
          if link_1_16_joined
            this.w_STFORTAG = NVL(FOCODICE116,NVL(this.w_STFORTAG,space(20)))
            this.w_DESFOR = NVL(FODESCRI116,space(30))
          else
          .link_1_16('Load')
          endif
        .w_STTIPFIL = NVL(STTIPFIL,space(1))
        .w_STFLESAL = NVL(STFLESAL,space(1))
        .w_STFLGCBI = NVL(STFLGCBI,space(1))
        .w_STNOMFIL = NVL(STNOMFIL,space(150))
        .w_STCODNOR = NVL(STCODNOR,space(10))
          .link_1_25('Load')
        .w_STFLATAB = NVL(STFLATAB,space(15))
          if link_1_26_joined
            this.w_STFLATAB = NVL(FTCODICE126,NVL(this.w_STFLATAB,space(15)))
            this.w_FLDES = NVL(FTDESCRI126,space(50))
          else
          .link_1_26('Load')
          endif
        .w_OLDFLAT = .w_STFLATAB
        .w_STLUNTAG = NVL(STLUNTAG,0)
        .w_STFLTYPE = NVL(STFLTYPE,space(1))
        .w_STFILXSD = NVL(STFILXSD,space(254))
        .w_STTIPFIN = NVL(STTIPFIN,space(1))
        .w_STCARFIN = NVL(STCARFIN,space(1))
        .w_STCARSEP = NVL(STCARSEP,space(1))
        .w_STCARSEC = NVL(STCARSEC,space(1))
        .w_STLUNSEG = NVL(STLUNSEG,0)
        .w_OLDLUN = .w_STLUNTAG
        .w_STFLPROV = NVL(STFLPROV,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .w_STCRISTR = NVL(STCRISTR,space(100))
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_STBACXML = NVL(STBACXML,space(30))
        .w_STDRIVER = NVL(STDRIVER,space(30))
        .w_STFUNRIC = NVL(STFUNRIC,space(30))
        .w_STTIPGES = NVL(STTIPGES,space(1))
        .w_STMODMSK = NVL(STMODMSK,space(30))
        .w_STFLIMPO = NVL(STFLIMPO,space(1))
        .w_STORDIMP = NVL(STORDIMP,0)
        .w_STBACIMP = NVL(STBACIMP,space(30))
        .w_STTIPFIN = NVL(STTIPFIN,space(1))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(.w_STFLPROV='S')
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(iif(.w_STFLPROV='N', ah_msgformat("<Confermata>"), ah_msgformat("<Provvisoria>")),0)
        cp_LoadRecExtFlds(this,'VASTRUTT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STCODICE = space(10)
      .w_STSEPARA = space(1)
      .w_STDESCRI = space(30)
      .w_STINDENT = 0
      .w_STFLPRED = space(1)
      .w_STCODENT = space(15)
      .w_DESCRI = space(50)
      .w_ENTITA = space(15)
      .w_EN_TABLE = space(30)
      .w_STELROOT = space(20)
      .w_DESELE = space(30)
      .w_STFORTAG = space(20)
      .w_DESFOR = space(30)
      .w_STTIPFIL = space(1)
      .w_STFLESAL = space(1)
      .w_STFLGCBI = space(1)
      .w_TIPO = space(1)
      .w_STNOMFIL = space(150)
      .w_STCODNOR = space(10)
      .w_STFLATAB = space(15)
      .w_OLDFLAT = space(10)
      .w_STLUNTAG = 0
      .w_STFLTYPE = space(1)
      .w_STFILXSD = space(254)
      .w_STTIPFIN = space(1)
      .w_STCARFIN = space(1)
      .w_STCARSEP = space(1)
      .w_STCARSEC = space(1)
      .w_STLUNSEG = 0
      .w_DESCAR = space(35)
      .w_OLDLUN = 0
      .w_STFLPROV = space(1)
      .w_TIPENT = space(1)
      .w_FLDES = space(50)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_STCRISTR = space(100)
      .w_STBACXML = space(30)
      .w_STDRIVER = space(30)
      .w_STFUNRIC = space(30)
      .w_STTIPGES = space(1)
      .w_STMODMSK = space(30)
      .w_STFLIMPO = space(1)
      .w_STORDIMP = 0
      .w_STBACIMP = space(30)
      .w_STTIPFIN = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_STSEPARA = 'N'
        .DoRTCalc(3,6,.f.)
          if not(empty(.w_STCODENT))
          .link_1_9('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_ENTITA = .w_STCODENT
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_ENTITA))
          .link_1_11('Full')
          endif
        .DoRTCalc(9,10,.f.)
          if not(empty(.w_STELROOT))
          .link_1_13('Full')
          endif
        .DoRTCalc(11,12,.f.)
          if not(empty(.w_STFORTAG))
          .link_1_16('Full')
          endif
          .DoRTCalc(13,13,.f.)
        .w_STTIPFIL = 'G'
        .DoRTCalc(15,19,.f.)
          if not(empty(.w_STCODNOR))
          .link_1_25('Full')
          endif
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_STFLATAB))
          .link_1_26('Full')
          endif
        .w_OLDFLAT = .w_STFLATAB
        .w_STLUNTAG = 0
        .w_STFLTYPE = 'E'
          .DoRTCalc(24,24,.f.)
        .w_STTIPFIN = iif(.w_STTIPFIL <>'L','A','F')
        .w_STCARFIN = iif(.w_STTIPFIL <>'E' or .w_STTIPFIN <>'T',' ',.w_STCARFIN)
        .w_STCARSEP = icase(.w_STTIPFIN='A','+',.w_STTIPFIL <>'E',' ',.w_STCARSEP)
        .w_STCARSEC = icase(.w_STTIPFIN='A',':',.w_STTIPFIL <>'E' or Empty(.w_STCARSEP),' ',.w_STCARSEC)
        .w_STLUNSEG = iif(.w_STTIPFIL<>'L' or .w_STTIPFIN <>'A',0,.w_STLUNSEG)
          .DoRTCalc(30,30,.f.)
        .w_OLDLUN = .w_STLUNTAG
        .w_STFLPROV = 'S'
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
          .DoRTCalc(33,38,.f.)
        .w_STCRISTR = IIF(Not Empty(.w_STFILXSD),cifracnf(Alltrim(.w_STCODICE)+juststem(.w_STFILXSD),'C'),' ')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_STBACXML = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BXM',.w_STBACXML)
        .w_STDRIVER = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BDR',.w_STDRIVER)
        .w_STFUNRIC = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GETOBJXML',.w_STFUNRIC)
        .w_STTIPGES = 'R'
        .w_STMODMSK = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' and .w_STTIPGES='R' ,'GSVA_BI3',.w_STMODMSK)
          .DoRTCalc(45,45,.f.)
        .w_STORDIMP = IIF(.w_STFLIMPO='N', 0, .w_STORDIMP)
        .w_STBACIMP = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BCD',.w_STBACIMP)
        .w_STTIPFIN = iif(.w_STTIPFIL <>'L','A','F')
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(.w_STFLPROV='S')
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(iif(.w_STFLPROV='N', ah_msgformat("<Confermata>"), ah_msgformat("<Provvisoria>")),0)
      endif
    endwith
    cp_BlankRecExtFlds(this,'VASTRUTT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSTCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oSTSEPARA_1_2.enabled = i_bVal
      .Page1.oPag.oSTDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oSTINDENT_1_4.enabled = i_bVal
      .Page1.oPag.oSTFLPRED_1_5.enabled = i_bVal
      .Page1.oPag.oSTCODENT_1_9.enabled = i_bVal
      .Page1.oPag.oSTELROOT_1_13.enabled = i_bVal
      .Page1.oPag.oSTFORTAG_1_16.enabled = i_bVal
      .Page1.oPag.oSTTIPFIL_1_18.enabled = i_bVal
      .Page1.oPag.oSTFLESAL_1_19.enabled = i_bVal
      .Page1.oPag.oSTFLGCBI_1_20.enabled = i_bVal
      .Page1.oPag.oSTNOMFIL_1_23.enabled = i_bVal
      .Page1.oPag.oSTCODNOR_1_25.enabled = i_bVal
      .Page1.oPag.oSTFLATAB_1_26.enabled = i_bVal
      .Page1.oPag.oSTLUNTAG_1_28.enabled = i_bVal
      .Page1.oPag.oSTFLTYPE_1_29.enabled = i_bVal
      .Page1.oPag.oSTFILXSD_1_30.enabled = i_bVal
      .Page1.oPag.oSTTIPFIN_1_31.enabled = i_bVal
      .Page1.oPag.oSTCARFIN_1_32.enabled = i_bVal
      .Page1.oPag.oSTCARSEP_1_33.enabled = i_bVal
      .Page1.oPag.oSTCARSEC_1_34.enabled = i_bVal
      .Page1.oPag.oSTLUNSEG_1_35.enabled = i_bVal
      .Page1.oPag.oSTBACXML_1_58.enabled = i_bVal
      .Page1.oPag.oSTDRIVER_1_59.enabled = i_bVal
      .Page1.oPag.oSTFUNRIC_1_60.enabled = i_bVal
      .Page1.oPag.oSTTIPGES_1_61.enabled = i_bVal
      .Page1.oPag.oSTMODMSK_1_62.enabled = i_bVal
      .Page1.oPag.oSTFLIMPO_1_63.enabled = i_bVal
      .Page1.oPag.oSTORDIMP_1_64.enabled = i_bVal
      .Page1.oPag.oSTBACIMP_1_68.enabled = i_bVal
      .Page1.oPag.oSTTIPFIN_1_69.enabled = i_bVal
      .Page1.oPag.oBtn_1_66.enabled = .Page1.oPag.oBtn_1_66.mCond()
      .Page1.oPag.oBtn_1_70.enabled = .Page1.oPag.oBtn_1_70.mCond()
      .Page1.oPag.oBtn_1_71.enabled = .Page1.oPag.oBtn_1_71.mCond()
      .Page1.oPag.BTNREP.enabled = i_bVal
      .Page1.oPag.oObj_1_56.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSTCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSTCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSVA_MVN.SetStatus(i_cOp)
    this.GSVA_MPF.SetStatus(i_cOp)
    this.GSVA_MVP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'VASTRUTT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVA_MVN.SetChildrenStatus(i_cOp)
  *  this.GSVA_MPF.SetChildrenStatus(i_cOp)
  *  this.GSVA_MVP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODICE,"STCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STSEPARA,"STSEPARA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDESCRI,"STDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STINDENT,"STINDENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLPRED,"STFLPRED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODENT,"STCODENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STELROOT,"STELROOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFORTAG,"STFORTAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPFIL,"STTIPFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLESAL,"STFLESAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLGCBI,"STFLGCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STNOMFIL,"STNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODNOR,"STCODNOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLATAB,"STFLATAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STLUNTAG,"STLUNTAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLTYPE,"STFLTYPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFILXSD,"STFILXSD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPFIN,"STTIPFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCARFIN,"STCARFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCARSEP,"STCARSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCARSEC,"STCARSEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STLUNSEG,"STLUNSEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLPROV,"STFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCRISTR,"STCRISTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STBACXML,"STBACXML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDRIVER,"STDRIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFUNRIC,"STFUNRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPGES,"STTIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STMODMSK,"STMODMSK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STFLIMPO,"STFLIMPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STORDIMP,"STORDIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STBACIMP,"STBACIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STTIPFIN,"STTIPFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    i_lTable = "VASTRUTT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VASTRUTT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVA_SST with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VASTRUTT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VASTRUTT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VASTRUTT')
        i_extval=cp_InsertValODBCExtFlds(this,'VASTRUTT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(STCODICE,STSEPARA,STDESCRI,STINDENT,STFLPRED"+;
                  ",STCODENT,STELROOT,STFORTAG,STTIPFIL,STFLESAL"+;
                  ",STFLGCBI,STNOMFIL,STCODNOR,STFLATAB,STLUNTAG"+;
                  ",STFLTYPE,STFILXSD,STTIPFIN,STCARFIN,STCARSEP"+;
                  ",STCARSEC,STLUNSEG,STFLPROV,UTCC,UTCV"+;
                  ",UTDC,UTDV,STCRISTR,STBACXML,STDRIVER"+;
                  ",STFUNRIC,STTIPGES,STMODMSK,STFLIMPO,STORDIMP"+;
                  ",STBACIMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_STCODICE)+;
                  ","+cp_ToStrODBC(this.w_STSEPARA)+;
                  ","+cp_ToStrODBC(this.w_STDESCRI)+;
                  ","+cp_ToStrODBC(this.w_STINDENT)+;
                  ","+cp_ToStrODBC(this.w_STFLPRED)+;
                  ","+cp_ToStrODBCNull(this.w_STCODENT)+;
                  ","+cp_ToStrODBCNull(this.w_STELROOT)+;
                  ","+cp_ToStrODBCNull(this.w_STFORTAG)+;
                  ","+cp_ToStrODBC(this.w_STTIPFIL)+;
                  ","+cp_ToStrODBC(this.w_STFLESAL)+;
                  ","+cp_ToStrODBC(this.w_STFLGCBI)+;
                  ","+cp_ToStrODBC(this.w_STNOMFIL)+;
                  ","+cp_ToStrODBCNull(this.w_STCODNOR)+;
                  ","+cp_ToStrODBCNull(this.w_STFLATAB)+;
                  ","+cp_ToStrODBC(this.w_STLUNTAG)+;
                  ","+cp_ToStrODBC(this.w_STFLTYPE)+;
                  ","+cp_ToStrODBC(this.w_STFILXSD)+;
                  ","+cp_ToStrODBC(this.w_STTIPFIN)+;
                  ","+cp_ToStrODBC(this.w_STCARFIN)+;
                  ","+cp_ToStrODBC(this.w_STCARSEP)+;
                  ","+cp_ToStrODBC(this.w_STCARSEC)+;
                  ","+cp_ToStrODBC(this.w_STLUNSEG)+;
                  ","+cp_ToStrODBC(this.w_STFLPROV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_STCRISTR)+;
                  ","+cp_ToStrODBC(this.w_STBACXML)+;
                  ","+cp_ToStrODBC(this.w_STDRIVER)+;
                  ","+cp_ToStrODBC(this.w_STFUNRIC)+;
                  ","+cp_ToStrODBC(this.w_STTIPGES)+;
                  ","+cp_ToStrODBC(this.w_STMODMSK)+;
                  ","+cp_ToStrODBC(this.w_STFLIMPO)+;
                  ","+cp_ToStrODBC(this.w_STORDIMP)+;
                  ","+cp_ToStrODBC(this.w_STBACIMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VASTRUTT')
        i_extval=cp_InsertValVFPExtFlds(this,'VASTRUTT')
        cp_CheckDeletedKey(i_cTable,0,'STCODICE',this.w_STCODICE)
        INSERT INTO (i_cTable);
              (STCODICE,STSEPARA,STDESCRI,STINDENT,STFLPRED,STCODENT,STELROOT,STFORTAG,STTIPFIL,STFLESAL,STFLGCBI,STNOMFIL,STCODNOR,STFLATAB,STLUNTAG,STFLTYPE,STFILXSD,STTIPFIN,STCARFIN,STCARSEP,STCARSEC,STLUNSEG,STFLPROV,UTCC,UTCV,UTDC,UTDV,STCRISTR,STBACXML,STDRIVER,STFUNRIC,STTIPGES,STMODMSK,STFLIMPO,STORDIMP,STBACIMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_STCODICE;
                  ,this.w_STSEPARA;
                  ,this.w_STDESCRI;
                  ,this.w_STINDENT;
                  ,this.w_STFLPRED;
                  ,this.w_STCODENT;
                  ,this.w_STELROOT;
                  ,this.w_STFORTAG;
                  ,this.w_STTIPFIL;
                  ,this.w_STFLESAL;
                  ,this.w_STFLGCBI;
                  ,this.w_STNOMFIL;
                  ,this.w_STCODNOR;
                  ,this.w_STFLATAB;
                  ,this.w_STLUNTAG;
                  ,this.w_STFLTYPE;
                  ,this.w_STFILXSD;
                  ,this.w_STTIPFIN;
                  ,this.w_STCARFIN;
                  ,this.w_STCARSEP;
                  ,this.w_STCARSEC;
                  ,this.w_STLUNSEG;
                  ,this.w_STFLPROV;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_STCRISTR;
                  ,this.w_STBACXML;
                  ,this.w_STDRIVER;
                  ,this.w_STFUNRIC;
                  ,this.w_STTIPGES;
                  ,this.w_STMODMSK;
                  ,this.w_STFLIMPO;
                  ,this.w_STORDIMP;
                  ,this.w_STBACIMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VASTRUTT_IDX,i_nConn)
      *
      * update VASTRUTT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VASTRUTT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " STSEPARA="+cp_ToStrODBC(this.w_STSEPARA)+;
             ",STDESCRI="+cp_ToStrODBC(this.w_STDESCRI)+;
             ",STINDENT="+cp_ToStrODBC(this.w_STINDENT)+;
             ",STFLPRED="+cp_ToStrODBC(this.w_STFLPRED)+;
             ",STCODENT="+cp_ToStrODBCNull(this.w_STCODENT)+;
             ",STELROOT="+cp_ToStrODBCNull(this.w_STELROOT)+;
             ",STFORTAG="+cp_ToStrODBCNull(this.w_STFORTAG)+;
             ",STTIPFIL="+cp_ToStrODBC(this.w_STTIPFIL)+;
             ",STFLESAL="+cp_ToStrODBC(this.w_STFLESAL)+;
             ",STFLGCBI="+cp_ToStrODBC(this.w_STFLGCBI)+;
             ",STNOMFIL="+cp_ToStrODBC(this.w_STNOMFIL)+;
             ",STCODNOR="+cp_ToStrODBCNull(this.w_STCODNOR)+;
             ",STFLATAB="+cp_ToStrODBCNull(this.w_STFLATAB)+;
             ",STLUNTAG="+cp_ToStrODBC(this.w_STLUNTAG)+;
             ",STFLTYPE="+cp_ToStrODBC(this.w_STFLTYPE)+;
             ",STFILXSD="+cp_ToStrODBC(this.w_STFILXSD)+;
             ",STTIPFIN="+cp_ToStrODBC(this.w_STTIPFIN)+;
             ",STCARFIN="+cp_ToStrODBC(this.w_STCARFIN)+;
             ",STCARSEP="+cp_ToStrODBC(this.w_STCARSEP)+;
             ",STCARSEC="+cp_ToStrODBC(this.w_STCARSEC)+;
             ",STLUNSEG="+cp_ToStrODBC(this.w_STLUNSEG)+;
             ",STFLPROV="+cp_ToStrODBC(this.w_STFLPROV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",STCRISTR="+cp_ToStrODBC(this.w_STCRISTR)+;
             ",STBACXML="+cp_ToStrODBC(this.w_STBACXML)+;
             ",STDRIVER="+cp_ToStrODBC(this.w_STDRIVER)+;
             ",STFUNRIC="+cp_ToStrODBC(this.w_STFUNRIC)+;
             ",STTIPGES="+cp_ToStrODBC(this.w_STTIPGES)+;
             ",STMODMSK="+cp_ToStrODBC(this.w_STMODMSK)+;
             ",STFLIMPO="+cp_ToStrODBC(this.w_STFLIMPO)+;
             ",STORDIMP="+cp_ToStrODBC(this.w_STORDIMP)+;
             ",STBACIMP="+cp_ToStrODBC(this.w_STBACIMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VASTRUTT')
        i_cWhere = cp_PKFox(i_cTable  ,'STCODICE',this.w_STCODICE  )
        UPDATE (i_cTable) SET;
              STSEPARA=this.w_STSEPARA;
             ,STDESCRI=this.w_STDESCRI;
             ,STINDENT=this.w_STINDENT;
             ,STFLPRED=this.w_STFLPRED;
             ,STCODENT=this.w_STCODENT;
             ,STELROOT=this.w_STELROOT;
             ,STFORTAG=this.w_STFORTAG;
             ,STTIPFIL=this.w_STTIPFIL;
             ,STFLESAL=this.w_STFLESAL;
             ,STFLGCBI=this.w_STFLGCBI;
             ,STNOMFIL=this.w_STNOMFIL;
             ,STCODNOR=this.w_STCODNOR;
             ,STFLATAB=this.w_STFLATAB;
             ,STLUNTAG=this.w_STLUNTAG;
             ,STFLTYPE=this.w_STFLTYPE;
             ,STFILXSD=this.w_STFILXSD;
             ,STTIPFIN=this.w_STTIPFIN;
             ,STCARFIN=this.w_STCARFIN;
             ,STCARSEP=this.w_STCARSEP;
             ,STCARSEC=this.w_STCARSEC;
             ,STLUNSEG=this.w_STLUNSEG;
             ,STFLPROV=this.w_STFLPROV;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,STCRISTR=this.w_STCRISTR;
             ,STBACXML=this.w_STBACXML;
             ,STDRIVER=this.w_STDRIVER;
             ,STFUNRIC=this.w_STFUNRIC;
             ,STTIPGES=this.w_STTIPGES;
             ,STMODMSK=this.w_STMODMSK;
             ,STFLIMPO=this.w_STFLIMPO;
             ,STORDIMP=this.w_STORDIMP;
             ,STBACIMP=this.w_STBACIMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVA_MVN : Saving
      this.GSVA_MVN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_STCODICE,"VNCODICE";
             )
      this.GSVA_MVN.mReplace()
      * --- GSVA_MPF : Saving
      this.GSVA_MPF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_STCODICE,"PFCODSTR";
             )
      this.GSVA_MPF.mReplace()
      * --- GSVA_MVP : Saving
      this.GSVA_MVP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_STCODICE,"PRCODSTR";
             )
      this.GSVA_MVP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsva_ast
    this.Notifyevent('Aggiorna')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVA_MVN : Deleting
    this.GSVA_MVN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_STCODICE,"VNCODICE";
           )
    this.GSVA_MVN.mDelete()
    * --- GSVA_MPF : Deleting
    this.GSVA_MPF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_STCODICE,"PFCODSTR";
           )
    this.GSVA_MPF.mDelete()
    * --- GSVA_MVP : Deleting
    this.GSVA_MVP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_STCODICE,"PRCODSTR";
           )
    this.GSVA_MVP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VASTRUTT_IDX,i_nConn)
      *
      * delete VASTRUTT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'STCODICE',this.w_STCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_STCODENT<>.w_STCODENT
            .w_ENTITA = .w_STCODENT
          .link_1_11('Full')
        endif
        .DoRTCalc(9,20,.t.)
        if .o_STCODICE<>.w_STCODICE
            .w_OLDFLAT = .w_STFLATAB
        endif
        if .o_STTIPFIL<>.w_STTIPFIL
            .w_STLUNTAG = 0
        endif
        .DoRTCalc(23,24,.t.)
        if .o_STTIPFIL<>.w_STTIPFIL
            .w_STTIPFIN = iif(.w_STTIPFIL <>'L','A','F')
        endif
        if .o_STTIPFIL<>.w_STTIPFIL.or. .o_STTIPFIN<>.w_STTIPFIN
            .w_STCARFIN = iif(.w_STTIPFIL <>'E' or .w_STTIPFIN <>'T',' ',.w_STCARFIN)
        endif
        if .o_STTIPFIN<>.w_STTIPFIN.or. .o_STTIPFIL<>.w_STTIPFIL
            .w_STCARSEP = icase(.w_STTIPFIN='A','+',.w_STTIPFIL <>'E',' ',.w_STCARSEP)
        endif
        if .o_STTIPFIL<>.w_STTIPFIL.or. .o_STCARSEP<>.w_STCARSEP.or. .o_STTIPFIN<>.w_STTIPFIN
            .w_STCARSEC = icase(.w_STTIPFIN='A',':',.w_STTIPFIL <>'E' or Empty(.w_STCARSEP),' ',.w_STCARSEC)
        endif
        if .o_STTIPFIL<>.w_STTIPFIL.or. .o_STTIPFIN<>.w_STTIPFIN
            .w_STLUNSEG = iif(.w_STTIPFIL<>'L' or .w_STTIPFIN <>'A',0,.w_STLUNSEG)
        endif
        .DoRTCalc(30,30,.t.)
        if .o_STCODICE<>.w_STCODICE
            .w_OLDLUN = .w_STLUNTAG
        endif
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .DoRTCalc(32,38,.t.)
        if .o_STFILXSD<>.w_STFILXSD
            .w_STCRISTR = IIF(Not Empty(.w_STFILXSD),cifracnf(Alltrim(.w_STCODICE)+juststem(.w_STFILXSD),'C'),' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        if .o_STFLTYPE<>.w_STFLTYPE
            .w_STBACXML = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BXM',.w_STBACXML)
        endif
        if .o_STFLTYPE<>.w_STFLTYPE
            .w_STDRIVER = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BDR',.w_STDRIVER)
        endif
        if .o_STFLTYPE<>.w_STFLTYPE
            .w_STFUNRIC = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GETOBJXML',.w_STFUNRIC)
        endif
        .DoRTCalc(43,43,.t.)
        if .o_STTIPGES<>.w_STTIPGES.or. .o_STFLTYPE<>.w_STFLTYPE
            .w_STMODMSK = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' and .w_STTIPGES='R' ,'GSVA_BI3',.w_STMODMSK)
        endif
        .DoRTCalc(45,45,.t.)
        if .o_STFLIMPO<>.w_STFLIMPO
            .w_STORDIMP = IIF(.w_STFLIMPO='N', 0, .w_STORDIMP)
        endif
        if  .o_STFLATAB<>.w_STFLATAB
          .WriteTo_GSVA_MVP()
        endif
        if .o_STFLTYPE<>.w_STFLTYPE
            .w_STBACIMP = ICASE(.w_STFLTYPE= 'O','',.cFunction='Load' ,'GSVA_BCD',.w_STBACIMP)
        endif
        if .o_STTIPFIL<>.w_STTIPFIL
            .w_STTIPFIN = iif(.w_STTIPFIL <>'L','A','F')
        endif
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(.w_STFLPROV='S')
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(iif(.w_STFLPROV='N', ah_msgformat("<Confermata>"), ah_msgformat("<Provvisoria>")),0)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(.w_STFLPROV='S')
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(iif(.w_STFLPROV='N', ah_msgformat("<Confermata>"), ah_msgformat("<Provvisoria>")),0)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTCODENT_1_9.enabled = this.oPgFrm.Page1.oPag.oSTCODENT_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.enabled = this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.mCond()
    this.oPgFrm.Page1.oPag.oSTBACXML_1_58.enabled = this.oPgFrm.Page1.oPag.oSTBACXML_1_58.mCond()
    this.oPgFrm.Page1.oPag.oSTDRIVER_1_59.enabled = this.oPgFrm.Page1.oPag.oSTDRIVER_1_59.mCond()
    this.oPgFrm.Page1.oPag.oSTFUNRIC_1_60.enabled = this.oPgFrm.Page1.oPag.oSTFUNRIC_1_60.mCond()
    this.oPgFrm.Page1.oPag.oSTTIPGES_1_61.enabled = this.oPgFrm.Page1.oPag.oSTTIPGES_1_61.mCond()
    this.oPgFrm.Page1.oPag.oSTMODMSK_1_62.enabled = this.oPgFrm.Page1.oPag.oSTMODMSK_1_62.mCond()
    this.oPgFrm.Page1.oPag.oSTORDIMP_1_64.enabled = this.oPgFrm.Page1.oPag.oSTORDIMP_1_64.mCond()
    this.oPgFrm.Page1.oPag.oSTBACIMP_1_68.enabled = this.oPgFrm.Page1.oPag.oSTBACIMP_1_68.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_65.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_65.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_67.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_67.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTFLESAL_1_19.visible=!this.oPgFrm.Page1.oPag.oSTFLESAL_1_19.mHide()
    this.oPgFrm.Page1.oPag.oSTFLGCBI_1_20.visible=!this.oPgFrm.Page1.oPag.oSTFLGCBI_1_20.mHide()
    this.oPgFrm.Page1.oPag.oSTFLATAB_1_26.visible=!this.oPgFrm.Page1.oPag.oSTFLATAB_1_26.mHide()
    this.oPgFrm.Page1.oPag.oSTLUNTAG_1_28.visible=!this.oPgFrm.Page1.oPag.oSTLUNTAG_1_28.mHide()
    this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.visible=!this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.mHide()
    this.oPgFrm.Page1.oPag.oSTTIPFIN_1_31.visible=!this.oPgFrm.Page1.oPag.oSTTIPFIN_1_31.mHide()
    this.oPgFrm.Page1.oPag.oSTCARFIN_1_32.visible=!this.oPgFrm.Page1.oPag.oSTCARFIN_1_32.mHide()
    this.oPgFrm.Page1.oPag.oSTCARSEP_1_33.visible=!this.oPgFrm.Page1.oPag.oSTCARSEP_1_33.mHide()
    this.oPgFrm.Page1.oPag.oSTCARSEC_1_34.visible=!this.oPgFrm.Page1.oPag.oSTCARSEC_1_34.mHide()
    this.oPgFrm.Page1.oPag.oSTLUNSEG_1_35.visible=!this.oPgFrm.Page1.oPag.oSTLUNSEG_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oFLDES_1_46.visible=!this.oPgFrm.Page1.oPag.oFLDES_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_67.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_67.mHide()
    this.oPgFrm.Page1.oPag.oSTTIPFIN_1_69.visible=!this.oPgFrm.Page1.oPag.oSTTIPFIN_1_69.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_70.visible=!this.oPgFrm.Page1.oPag.oBtn_1_70.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_71.visible=!this.oPgFrm.Page1.oPag.oBtn_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNREP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsva_ast
    this.w_BTNREP.visible= IIF (this.w_EN_TABLE<>'DIS_TINT',.F.,.T.)
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=STCODENT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_lTable = "ENT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2], .t., this.ENT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODENT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZE',True,'ENT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ENCODICE like "+cp_ToStrODBC(trim(this.w_STCODENT)+"%");

          i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',trim(this.w_STCODENT))
          select ENCODICE,ENDESCRI,ENTIPENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODENT)==trim(_Link_.ENCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODENT) and !this.bDontReportError
            deferred_cp_zoom('ENT_MAST','*','ENCODICE',cp_AbsName(oSource.parent,'oSTCODENT_1_9'),i_cWhere,'GSAR_BZE',"Entita",'gsvavmen.ENT_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                     +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1))
            select ENCODICE,ENDESCRI,ENTIPENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODENT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI,ENTIPENT";
                   +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(this.w_STCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_STCODENT)
            select ENCODICE,ENDESCRI,ENTIPENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODENT = NVL(_Link_.ENCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ENDESCRI,space(50))
      this.w_TIPENT = NVL(_Link_.ENTIPENT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_STCODENT = space(15)
      endif
      this.w_DESCRI = space(50)
      this.w_TIPENT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPENT='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, entit� di tipo incongrunte")
        endif
        this.w_STCODENT = space(15)
        this.w_DESCRI = space(50)
        this.w_TIPENT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)
      cp_ShowWarn(i_cKey,this.ENT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODENT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ENT_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.ENCODICE as ENCODICE109"+ ",link_1_9.ENDESCRI as ENDESCRI109"+ ",link_1_9.ENTIPENT as ENTIPENT109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on VASTRUTT.STCODENT=link_1_9.ENCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and VASTRUTT.STCODENT=link_1_9.ENCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ENTITA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTITA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTITA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENPRINCI,ENCODICE,EN_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(this.w_ENTITA);
                   +" and ENPRINCI="+cp_ToStrODBC('S');
                   +" and ENCODICE="+cp_ToStrODBC(this.w_STCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENPRINCI','S';
                       ,'ENCODICE',this.w_STCODENT;
                       ,'ENCODICE',this.w_ENTITA)
            select ENPRINCI,ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTITA = NVL(_Link_.ENCODICE,space(15))
      this.w_EN_TABLE = NVL(_Link_.EN_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ENTITA = space(15)
      endif
      this.w_EN_TABLE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENPRINCI,1)+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.ENCODICE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTITA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STELROOT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STELROOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_STELROOT)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STCODICE);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_STCODICE;
                     ,'ELCODICE',trim(this.w_STELROOT))
          select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STELROOT)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStrODBC(trim(this.w_STELROOT)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STCODICE);

            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStr(trim(this.w_STELROOT)+"%");
                   +" and ELCODSTR="+cp_ToStr(this.w_STCODICE);

            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STELROOT) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oSTELROOT_1_13'),i_cWhere,'GSVA_AEL',"Elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STCODICE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Elemento inesistente oppure non di tipo fisso o variabile")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_STCODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STELROOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_STELROOT);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_STCODICE;
                       ,'ELCODICE',this.w_STELROOT)
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STELROOT = NVL(_Link_.ELCODICE,space(20))
      this.w_DESELE = NVL(_Link_.ELDESCRI,space(30))
      this.w_TIPO = NVL(_Link_.EL__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_STELROOT = space(20)
      endif
      this.w_DESELE = space(30)
      this.w_TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO $ 'T-G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Elemento inesistente oppure non di tipo fisso o variabile")
        endif
        this.w_STELROOT = space(20)
        this.w_DESELE = space(30)
        this.w_TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STELROOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VAELEMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ELCODICE as ELCODICE113"+ ",link_1_13.ELDESCRI as ELDESCRI113"+ ",link_1_13.EL__TIPO as EL__TIPO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on VASTRUTT.STELROOT=link_1_13.ELCODICE"+" and VASTRUTT.STCODICE=link_1_13.ELCODSTR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and VASTRUTT.STELROOT=link_1_13.ELCODICE(+)"'+'+" and VASTRUTT.STCODICE=link_1_13.ELCODSTR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STFORTAG
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_lTable = "VAFORMAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2], .t., this.VAFORMAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STFORTAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AFO',True,'VAFORMAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_STFORTAG)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_STCODICE);

          i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODSTR,FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODSTR',this.w_STCODICE;
                     ,'FOCODICE',trim(this.w_STFORTAG))
          select FOCODSTR,FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODSTR,FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STFORTAG)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStrODBC(trim(this.w_STFORTAG)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_STCODICE);

            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStr(trim(this.w_STFORTAG)+"%");
                   +" and FOCODSTR="+cp_ToStr(this.w_STCODICE);

            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STFORTAG) and !this.bDontReportError
            deferred_cp_zoom('VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(oSource.parent,'oSTFORTAG_1_16'),i_cWhere,'GSVA_AFO',"Formati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STCODICE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FOCODSTR="+cp_ToStrODBC(this.w_STCODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',oSource.xKey(1);
                       ,'FOCODICE',oSource.xKey(2))
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STFORTAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_STFORTAG);
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_STCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',this.w_STCODICE;
                       ,'FOCODICE',this.w_STFORTAG)
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STFORTAG = NVL(_Link_.FOCODICE,space(20))
      this.w_DESFOR = NVL(_Link_.FODESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_STFORTAG = space(20)
      endif
      this.w_DESFOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODSTR,1)+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.VAFORMAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STFORTAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VAFORMAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.FOCODICE as FOCODICE116"+ ",link_1_16.FODESCRI as FODESCRI116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on VASTRUTT.STFORTAG=link_1_16.FOCODICE"+" and VASTRUTT.STCODICE=link_1_16.FOCODSTR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and VASTRUTT.STFORTAG=link_1_16.FOCODICE(+)"'+'+" and VASTRUTT.STCODICE=link_1_16.FOCODSTR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STCODNOR
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAR_SPEC_IDX,3]
    i_lTable = "CAR_SPEC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAR_SPEC_IDX,2], .t., this.CAR_SPEC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAR_SPEC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODNOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCS',True,'CAR_SPEC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_STCODNOR)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_STCODNOR))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODNOR)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODNOR) and !this.bDontReportError
            deferred_cp_zoom('CAR_SPEC','*','CACODICE',cp_AbsName(oSource.parent,'oSTCODNOR_1_25'),i_cWhere,'GSAR_MCS',"Caratteri speciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODNOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_STCODNOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_STCODNOR)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODNOR = NVL(_Link_.CACODICE,space(10))
      this.w_DESCAR = NVL(_Link_.CADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_STCODNOR = space(10)
      endif
      this.w_DESCAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAR_SPEC_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAR_SPEC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODNOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STFLATAB
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLATMAST_IDX,3]
    i_lTable = "FLATMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLATMAST_IDX,2], .t., this.FLATMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLATMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STFLATAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_MFT',True,'FLATMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FTCODICE like "+cp_ToStrODBC(trim(this.w_STFLATAB)+"%");

          i_ret=cp_SQL(i_nConn,"select FTCODICE,FTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FTCODICE',trim(this.w_STFLATAB))
          select FTCODICE,FTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STFLATAB)==trim(_Link_.FTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STFLATAB) and !this.bDontReportError
            deferred_cp_zoom('FLATMAST','*','FTCODICE',cp_AbsName(oSource.parent,'oSTFLATAB_1_26'),i_cWhere,'GSVA_MFT',"Tabelle piatte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',oSource.xKey(1))
            select FTCODICE,FTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STFLATAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE,FTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FTCODICE="+cp_ToStrODBC(this.w_STFLATAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',this.w_STFLATAB)
            select FTCODICE,FTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STFLATAB = NVL(_Link_.FTCODICE,space(15))
      this.w_FLDES = NVL(_Link_.FTDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_STFLATAB = space(15)
      endif
      this.w_FLDES = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLATMAST_IDX,2])+'\'+cp_ToStr(_Link_.FTCODICE,1)
      cp_ShowWarn(i_cKey,this.FLATMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STFLATAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FLATMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FLATMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.FTCODICE as FTCODICE126"+ ",link_1_26.FTDESCRI as FTDESCRI126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on VASTRUTT.STFLATAB=link_1_26.FTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and VASTRUTT.STFLATAB=link_1_26.FTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTCODICE_1_1.value==this.w_STCODICE)
      this.oPgFrm.Page1.oPag.oSTCODICE_1_1.value=this.w_STCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSTSEPARA_1_2.RadioValue()==this.w_STSEPARA)
      this.oPgFrm.Page1.oPag.oSTSEPARA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESCRI_1_3.value==this.w_STDESCRI)
      this.oPgFrm.Page1.oPag.oSTDESCRI_1_3.value=this.w_STDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSTINDENT_1_4.value==this.w_STINDENT)
      this.oPgFrm.Page1.oPag.oSTINDENT_1_4.value=this.w_STINDENT
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLPRED_1_5.RadioValue()==this.w_STFLPRED)
      this.oPgFrm.Page1.oPag.oSTFLPRED_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODENT_1_9.value==this.w_STCODENT)
      this.oPgFrm.Page1.oPag.oSTCODENT_1_9.value=this.w_STCODENT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSTELROOT_1_13.value==this.w_STELROOT)
      this.oPgFrm.Page1.oPag.oSTELROOT_1_13.value=this.w_STELROOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_14.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_14.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFORTAG_1_16.value==this.w_STFORTAG)
      this.oPgFrm.Page1.oPag.oSTFORTAG_1_16.value=this.w_STFORTAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_17.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_17.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPFIL_1_18.RadioValue()==this.w_STTIPFIL)
      this.oPgFrm.Page1.oPag.oSTTIPFIL_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLESAL_1_19.RadioValue()==this.w_STFLESAL)
      this.oPgFrm.Page1.oPag.oSTFLESAL_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLGCBI_1_20.RadioValue()==this.w_STFLGCBI)
      this.oPgFrm.Page1.oPag.oSTFLGCBI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTNOMFIL_1_23.value==this.w_STNOMFIL)
      this.oPgFrm.Page1.oPag.oSTNOMFIL_1_23.value=this.w_STNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODNOR_1_25.value==this.w_STCODNOR)
      this.oPgFrm.Page1.oPag.oSTCODNOR_1_25.value=this.w_STCODNOR
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLATAB_1_26.value==this.w_STFLATAB)
      this.oPgFrm.Page1.oPag.oSTFLATAB_1_26.value=this.w_STFLATAB
    endif
    if not(this.oPgFrm.Page1.oPag.oSTLUNTAG_1_28.value==this.w_STLUNTAG)
      this.oPgFrm.Page1.oPag.oSTLUNTAG_1_28.value=this.w_STLUNTAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLTYPE_1_29.RadioValue()==this.w_STFLTYPE)
      this.oPgFrm.Page1.oPag.oSTFLTYPE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.value==this.w_STFILXSD)
      this.oPgFrm.Page1.oPag.oSTFILXSD_1_30.value=this.w_STFILXSD
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPFIN_1_31.RadioValue()==this.w_STTIPFIN)
      this.oPgFrm.Page1.oPag.oSTTIPFIN_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCARFIN_1_32.value==this.w_STCARFIN)
      this.oPgFrm.Page1.oPag.oSTCARFIN_1_32.value=this.w_STCARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCARSEP_1_33.value==this.w_STCARSEP)
      this.oPgFrm.Page1.oPag.oSTCARSEP_1_33.value=this.w_STCARSEP
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCARSEC_1_34.value==this.w_STCARSEC)
      this.oPgFrm.Page1.oPag.oSTCARSEC_1_34.value=this.w_STCARSEC
    endif
    if not(this.oPgFrm.Page1.oPag.oSTLUNSEG_1_35.value==this.w_STLUNSEG)
      this.oPgFrm.Page1.oPag.oSTLUNSEG_1_35.value=this.w_STLUNSEG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAR_1_39.value==this.w_DESCAR)
      this.oPgFrm.Page1.oPag.oDESCAR_1_39.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDES_1_46.value==this.w_FLDES)
      this.oPgFrm.Page1.oPag.oFLDES_1_46.value=this.w_FLDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSTBACXML_1_58.value==this.w_STBACXML)
      this.oPgFrm.Page1.oPag.oSTBACXML_1_58.value=this.w_STBACXML
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDRIVER_1_59.value==this.w_STDRIVER)
      this.oPgFrm.Page1.oPag.oSTDRIVER_1_59.value=this.w_STDRIVER
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFUNRIC_1_60.value==this.w_STFUNRIC)
      this.oPgFrm.Page1.oPag.oSTFUNRIC_1_60.value=this.w_STFUNRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPGES_1_61.RadioValue()==this.w_STTIPGES)
      this.oPgFrm.Page1.oPag.oSTTIPGES_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTMODMSK_1_62.value==this.w_STMODMSK)
      this.oPgFrm.Page1.oPag.oSTMODMSK_1_62.value=this.w_STMODMSK
    endif
    if not(this.oPgFrm.Page1.oPag.oSTFLIMPO_1_63.RadioValue()==this.w_STFLIMPO)
      this.oPgFrm.Page1.oPag.oSTFLIMPO_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTORDIMP_1_64.value==this.w_STORDIMP)
      this.oPgFrm.Page1.oPag.oSTORDIMP_1_64.value=this.w_STORDIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oSTBACIMP_1_68.value==this.w_STBACIMP)
      this.oPgFrm.Page1.oPag.oSTBACIMP_1_68.value=this.w_STBACIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oSTTIPFIN_1_69.RadioValue()==this.w_STTIPFIN)
      this.oPgFrm.Page1.oPag.oSTTIPFIN_1_69.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'VASTRUTT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_STCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_STCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_STCODENT)) or not(.w_TIPENT='V'))  and (.cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTCODENT_1_9.SetFocus()
            i_bnoObbl = !empty(.w_STCODENT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, entit� di tipo incongrunte")
          case   not(.w_TIPO $ 'T-G')  and not(empty(.w_STELROOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTELROOT_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Elemento inesistente oppure non di tipo fisso o variabile")
          case   (empty(.w_STFLATAB))  and not(Not Empty(.w_STFILXSD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTFLATAB_1_26.SetFocus()
            i_bnoObbl = !empty(.w_STFLATAB)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_STCARFIN))  and not(.w_STTIPFIL <>'E' or .w_STTIPFIN <>'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTCARFIN_1_32.SetFocus()
            i_bnoObbl = !empty(.w_STCARFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_STCARSEP))  and not(.w_STTIPFIL <>'E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTCARSEP_1_33.SetFocus()
            i_bnoObbl = !empty(.w_STCARSEP)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVA_MVN.CheckForm()
      if i_bres
        i_bres=  .GSVA_MVN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSVA_MPF.CheckForm()
      if i_bres
        i_bres=  .GSVA_MPF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSVA_MVP.CheckForm()
      if i_bres
        i_bres=  .GSVA_MVP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STCODICE = this.w_STCODICE
    this.o_STCODENT = this.w_STCODENT
    this.o_STTIPFIL = this.w_STTIPFIL
    this.o_STFLATAB = this.w_STFLATAB
    this.o_STFLTYPE = this.w_STFLTYPE
    this.o_STFILXSD = this.w_STFILXSD
    this.o_STTIPFIN = this.w_STTIPFIN
    this.o_STCARSEP = this.w_STCARSEP
    this.o_STTIPGES = this.w_STTIPGES
    this.o_STFLIMPO = this.w_STFLIMPO
    * --- GSVA_MVN : Depends On
    this.GSVA_MVN.SaveDependsOn()
    * --- GSVA_MPF : Depends On
    this.GSVA_MPF.SaveDependsOn()
    * --- GSVA_MVP : Depends On
    this.GSVA_MVP.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=this.w_STFLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile modificare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsva_astPag1 as StdContainer
  Width  = 654
  height = 532
  stdWidth  = 654
  stdheight = 532
  resizeXpos=427
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTCODICE_1_1 as StdField with uid="SFRGXMMMZS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_STCODICE", cQueryName = "STCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 118036331,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=159, Top=10, InputMask=replicate('X',10)

  func oSTCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_STELROOT)
        bRes2=.link_1_13('Full')
      endif
      if .not. empty(.w_STFORTAG)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oSTSEPARA_1_2 as StdCombo with uid="HRCIQUWNGA",rtseq=2,rtrep=.f.,left=544,top=10,width=103,height=21;
    , ToolTipText = "Separatore applicato nei campi\espressioni numerici";
    , HelpContextID = 264247143;
    , cFormVar="w_STSEPARA",RowSource=""+"Punto,"+"Virgola,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTSEPARA_1_2.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'V',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oSTSEPARA_1_2.GetRadio()
    this.Parent.oContained.w_STSEPARA = this.RadioValue()
    return .t.
  endfunc

  func oSTSEPARA_1_2.SetRadio()
    this.Parent.oContained.w_STSEPARA=trim(this.Parent.oContained.w_STSEPARA)
    this.value = ;
      iif(this.Parent.oContained.w_STSEPARA=='P',1,;
      iif(this.Parent.oContained.w_STSEPARA=='V',2,;
      iif(this.Parent.oContained.w_STSEPARA=='N',3,;
      0)))
  endfunc

  add object oSTDESCRI_1_3 as StdField with uid="RYDWVCYLVO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_STDESCRI", cQueryName = "STDESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della struttura",;
    HelpContextID = 32450415,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=160, Top=39, InputMask=replicate('X',30)

  add object oSTINDENT_1_4 as StdField with uid="MOOFSSSBUF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STINDENT", cQueryName = "STINDENT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di spazi utilizzati per unit� di indentazione",;
    HelpContextID = 217548934,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=544, Top=39, cSayPict="'9999999999'", cGetPict="'9999999999'"

  add object oSTFLPRED_1_5 as StdCheck with uid="RGLZTDYHJS",rtseq=5,rtrep=.f.,left=291, top=9, caption="Predefinita",;
    HelpContextID = 12994410,;
    cFormVar="w_STFLPRED", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTFLPRED_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTFLPRED_1_5.GetRadio()
    this.Parent.oContained.w_STFLPRED = this.RadioValue()
    return .t.
  endfunc

  func oSTFLPRED_1_5.SetRadio()
    this.Parent.oContained.w_STFLPRED=trim(this.Parent.oContained.w_STFLPRED)
    this.value = ;
      iif(this.Parent.oContained.w_STFLPRED=='S',1,;
      0)
  endfunc

  add object oSTCODENT_1_9 as StdField with uid="WUTYRXYGAM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STCODENT", cQueryName = "STCODENT",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, entit� di tipo incongrunte",;
    ToolTipText = "Codice entit� associata alla struttura",;
    HelpContextID = 217507974,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=159, Top=68, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ENT_MAST", cZoomOnZoom="GSAR_BZE", oKey_1_1="ENCODICE", oKey_1_2="this.w_STCODENT"

  func oSTCODENT_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oSTCODENT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
      if .not. empty(.w_ENTITA)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSTCODENT_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODENT_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENT_MAST','*','ENCODICE',cp_AbsName(this.parent,'oSTCODENT_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZE',"Entita",'gsvavmen.ENT_MAST_VZM',this.parent.oContained
  endproc
  proc oSTCODENT_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ENCODICE=this.parent.oContained.w_STCODENT
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_10 as StdField with uid="IHCPJONAWR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione entit�",;
    HelpContextID = 131991350,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=68, InputMask=replicate('X',50)

  add object oSTELROOT_1_13 as StdField with uid="YUPEAIFUXW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_STELROOT", cQueryName = "STELROOT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Elemento inesistente oppure non di tipo fisso o variabile",;
    ToolTipText = "Elemento root della struttura",;
    HelpContextID = 35244166,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=159, Top=97, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_STCODICE", oKey_2_1="ELCODICE", oKey_2_2="this.w_STELROOT"

  func oSTELROOT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTELROOT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTELROOT_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_STCODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_STCODICE)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oSTELROOT_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AEL',"Elementi",'',this.parent.oContained
  endproc
  proc oSTELROOT_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_STCODICE
     i_obj.w_ELCODICE=this.parent.oContained.w_STELROOT
     i_obj.ecpSave()
  endproc

  add object oDESELE_1_14 as StdField with uid="NZDIXNTIBB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 58722102,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=326, Top=97, InputMask=replicate('X',30)

  add object oSTFORTAG_1_16 as StdField with uid="XREAFNIAIX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_STFORTAG", cQueryName = "STFORTAG",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Formato da applicare ai TAG (di tipo gruppo di TAG) legati alla struttura",;
    HelpContextID = 48842605,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=159, Top=126, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAFORMAT", cZoomOnZoom="GSVA_AFO", oKey_1_1="FOCODSTR", oKey_1_2="this.w_STCODICE", oKey_2_1="FOCODICE", oKey_2_2="this.w_STFORTAG"

  func oSTFORTAG_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTFORTAG_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTFORTAG_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAFORMAT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_STCODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStr(this.Parent.oContained.w_STCODICE)
    endif
    do cp_zoom with 'VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(this.parent,'oSTFORTAG_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AFO',"Formati",'',this.parent.oContained
  endproc
  proc oSTFORTAG_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AFO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FOCODSTR=w_STCODICE
     i_obj.w_FOCODICE=this.parent.oContained.w_STFORTAG
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_17 as StdField with uid="MBDCKEJWTJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 11601718,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=326, Top=126, InputMask=replicate('X',30)


  add object oSTTIPFIL_1_18 as StdCombo with uid="JUMAMWAFQE",rtseq=14,rtrep=.f.,left=159,top=152,width=152,height=21;
    , HelpContextID = 188471438;
    , cFormVar="w_STTIPFIL",RowSource=""+"XML,"+"Lunghezza variabile,"+"Lunghezza fissa,"+"Generico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTTIPFIL_1_18.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    iif(this.value =4,'G',;
    space(1))))))
  endfunc
  func oSTTIPFIL_1_18.GetRadio()
    this.Parent.oContained.w_STTIPFIL = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPFIL_1_18.SetRadio()
    this.Parent.oContained.w_STTIPFIL=trim(this.Parent.oContained.w_STTIPFIL)
    this.value = ;
      iif(this.Parent.oContained.w_STTIPFIL=='X',1,;
      iif(this.Parent.oContained.w_STTIPFIL=='E',2,;
      iif(this.Parent.oContained.w_STTIPFIL=='L',3,;
      iif(this.Parent.oContained.w_STTIPFIL=='G',4,;
      0))))
  endfunc

  add object oSTFLESAL_1_19 as StdCheck with uid="HGCZIBIOMC",rtseq=15,rtrep=.f.,left=327, top=152, caption="Elimina spazi",;
    ToolTipText = "Se attivo elimina spazi sul valore esportato",;
    HelpContextID = 18237298,;
    cFormVar="w_STFLESAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTFLESAL_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTFLESAL_1_19.GetRadio()
    this.Parent.oContained.w_STFLESAL = this.RadioValue()
    return .t.
  endfunc

  func oSTFLESAL_1_19.SetRadio()
    this.Parent.oContained.w_STFLESAL=trim(this.Parent.oContained.w_STFLESAL)
    this.value = ;
      iif(this.Parent.oContained.w_STFLESAL=='S',1,;
      0)
  endfunc

  func oSTFLESAL_1_19.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL='L')
    endwith
  endfunc

  add object oSTFLGCBI_1_20 as StdCheck with uid="QJQQFPNZBW",rtseq=16,rtrep=.f.,left=465, top=152, caption="Genera file CBI",;
    HelpContextID = 20334447,;
    cFormVar="w_STFLGCBI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTFLGCBI_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTFLGCBI_1_20.GetRadio()
    this.Parent.oContained.w_STFLGCBI = this.RadioValue()
    return .t.
  endfunc

  func oSTFLGCBI_1_20.SetRadio()
    this.Parent.oContained.w_STFLGCBI=trim(this.Parent.oContained.w_STFLGCBI)
    this.value = ;
      iif(this.Parent.oContained.w_STFLGCBI=='S',1,;
      0)
  endfunc

  func oSTFLGCBI_1_20.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL<>'X')
    endwith
  endfunc

  add object oSTNOMFIL_1_23 as StdField with uid="ENYEWLJBPR",rtseq=18,rtrep=.f.,;
    cFormVar = "w_STNOMFIL", cQueryName = "STNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Nome file generazione EDI",;
    HelpContextID = 191248526,;
   bGlobalFont=.t.,;
    Height=21, Width=435, Left=159, Top=178, InputMask=replicate('X',150)


  add object oLinkPC_1_24 as StdButton with uid="YWZQEWXOGH",left=597, top=155, width=48,height=45,;
    CpPicture="BMP\PREDEFINITO.BMP", caption="", nPag=1;
    , ToolTipText = "Variabili relative al nome del file";
    , HelpContextID = 240444654;
    , caption='\<Var. file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_24.Click()
      this.Parent.oContained.GSVA_MVN.LinkPCClick()
    endproc

  add object oSTCODNOR_1_25 as StdField with uid="WYVPBITTEL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_STCODNOR", cQueryName = "STCODNOR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice normalizzazione",;
    HelpContextID = 66513032,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=159, Top=205, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAR_SPEC", cZoomOnZoom="GSAR_MCS", oKey_1_1="CACODICE", oKey_1_2="this.w_STCODNOR"

  func oSTCODNOR_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODNOR_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODNOR_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAR_SPEC','*','CACODICE',cp_AbsName(this.parent,'oSTCODNOR_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCS',"Caratteri speciali",'',this.parent.oContained
  endproc
  proc oSTCODNOR_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_STCODNOR
     i_obj.ecpSave()
  endproc

  add object oSTFLATAB_1_26 as StdField with uid="XMVLLNKXLC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_STFLATAB", cQueryName = "STFLATAB",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tabella piatta associata",;
    HelpContextID = 30820200,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=159, Top=232, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="FLATMAST", cZoomOnZoom="GSVA_MFT", oKey_1_1="FTCODICE", oKey_1_2="this.w_STFLATAB"

  func oSTFLATAB_1_26.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_STFILXSD))
    endwith
  endfunc

  func oSTFLATAB_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTFLATAB_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTFLATAB_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLATMAST','*','FTCODICE',cp_AbsName(this.parent,'oSTFLATAB_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_MFT',"Tabelle piatte",'',this.parent.oContained
  endproc
  proc oSTFLATAB_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSVA_MFT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FTCODICE=this.parent.oContained.w_STFLATAB
     i_obj.ecpSave()
  endproc

  add object oSTLUNTAG_1_28 as StdField with uid="GZYBMTAREX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_STLUNTAG", cQueryName = "STLUNTAG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza elementi tipo ID primario",;
    HelpContextID = 45066093,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=159, Top=259

  func oSTLUNTAG_1_28.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL<>'L')
    endwith
  endfunc


  add object oSTFLTYPE_1_29 as StdCombo with uid="EFTPWAZSCN",rtseq=23,rtrep=.f.,left=548,top=259,width=99,height=21;
    , ToolTipText = "Definisce se la struttura � valida per import, export o entrambe (Import/export)";
    , HelpContextID = 134629227;
    , cFormVar="w_STFLTYPE",RowSource=""+"Entrambe,"+"Import,"+"Export", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTFLTYPE_1_29.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oSTFLTYPE_1_29.GetRadio()
    this.Parent.oContained.w_STFLTYPE = this.RadioValue()
    return .t.
  endfunc

  func oSTFLTYPE_1_29.SetRadio()
    this.Parent.oContained.w_STFLTYPE=trim(this.Parent.oContained.w_STFLTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_STFLTYPE=='E',1,;
      iif(this.Parent.oContained.w_STFLTYPE=='I',2,;
      iif(this.Parent.oContained.w_STFLTYPE=='O',3,;
      0)))
  endfunc

  add object oSTFILXSD_1_30 as StdField with uid="CCHCOLOEON",rtseq=24,rtrep=.f.,;
    cFormVar = "w_STFILXSD", cQueryName = "STFILXSD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File schema XSD",;
    HelpContextID = 109266794,;
   bGlobalFont=.t.,;
    Height=21, Width=467, Left=159, Top=289, InputMask=replicate('X',254)

  func oSTFILXSD_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STTIPFIL='X')
    endwith
   endif
  endfunc

  func oSTFILXSD_1_30.mHide()
    with this.Parent.oContained
      return (.w_EN_TABLE<>'DIS_TINT')
    endwith
  endfunc


  add object oSTTIPFIN_1_31 as StdCombo with uid="ABYHLQPTDN",rtseq=25,rtrep=.f.,left=101,top=350,width=114,height=21;
    , ToolTipText = "Tipo fine riga, ad esempio, Fine riga CR+LF per file di tipo Euritmo";
    , HelpContextID = 188471436;
    , cFormVar="w_STTIPFIN",RowSource=""+"Fine riga CR+LF,"+"Fine riga LF,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTTIPFIN_1_31.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'R',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oSTTIPFIN_1_31.GetRadio()
    this.Parent.oContained.w_STTIPFIN = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPFIN_1_31.SetRadio()
    this.Parent.oContained.w_STTIPFIN=trim(this.Parent.oContained.w_STTIPFIN)
    this.value = ;
      iif(this.Parent.oContained.w_STTIPFIN=='F',1,;
      iif(this.Parent.oContained.w_STTIPFIN=='R',2,;
      iif(this.Parent.oContained.w_STTIPFIN=='A',3,;
      0)))
  endfunc

  func oSTTIPFIN_1_31.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'L')
    endwith
  endfunc

  add object oSTCARFIN_1_32 as StdField with uid="WRPXMIPXNA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_STCARFIN", cQueryName = "STCARFIN",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Carattere speciale che identifica il fine riga",;
    HelpContextID = 186968204,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=217, Top=350, InputMask=replicate('X',1)

  func oSTCARFIN_1_32.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E' or .w_STTIPFIN <>'T')
    endwith
  endfunc

  add object oSTCARSEP_1_33 as StdField with uid="PDBFZBUQLR",rtseq=27,rtrep=.f.,;
    cFormVar = "w_STCARSEP", cQueryName = "STCARSEP",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Primo carattere separatore dei dati, ad esempio, + per file di tipo Edifact",;
    HelpContextID = 31135606,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=365, Top=350, InputMask=replicate('X',1)

  func oSTCARSEP_1_33.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E')
    endwith
  endfunc

  add object oSTCARSEC_1_34 as StdField with uid="WMECZNKMRB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_STCARSEC", cQueryName = "STCARSEC",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Secondo carattere separatore dei dati, ad esempio, : per file di tipo Edifact",;
    HelpContextID = 31135593,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=394, Top=350, InputMask=replicate('X',1)

  func oSTCARSEC_1_34.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E' or Empty(.w_STCARSEP))
    endwith
  endfunc

  add object oSTLUNSEG_1_35 as StdField with uid="GNRYHOCNZR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_STLUNSEG", cQueryName = "STLUNSEG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza del segmento per file a lunghezza fissa senza fine riga",;
    HelpContextID = 28288877,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=365, Top=350

  func oSTLUNSEG_1_35.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL<>'L' or .w_STTIPFIN <>'A')
    endwith
  endfunc

  add object oDESCAR_1_39 as StdField with uid="BMZXTDLBPE",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 265160502,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=319, Top=205, InputMask=replicate('X',35)

  add object oFLDES_1_46 as StdField with uid="EAKZECWPGS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_FLDES", cQueryName = "FLDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 17883562,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=284, Top=232, InputMask=replicate('X',50)

  func oFLDES_1_46.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_STFILXSD))
    endwith
  endfunc


  add object BTNREP as cp_askfile with uid="ISHGMEOMJK",left=629, top=292, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_STFILXSD",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file xsd";
    , HelpContextID = 109535530


  add object oObj_1_56 as cp_runprogram with uid="DMZWSQMXTD",left=9, top=569, width=221,height=19,;
    caption='gsva_bst(T)',;
   bGlobalFont=.t.,;
    prg="gsva_bst('T')",;
    cEvent = "w_STFLATAB Changed",;
    nPag=1;
    , HelpContextID = 30494298


  add object oObj_1_57 as cp_runprogram with uid="CYSQQCLZZI",left=8, top=546, width=221,height=19,;
    caption='gsva_bst(C)',;
   bGlobalFont=.t.,;
    prg="gsva_bst('C')",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 30489946

  add object oSTBACXML_1_58 as StdField with uid="LOPGAHXBOT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_STBACXML", cQueryName = "STBACXML",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Routine per eseguire creazione file xml al primo step in fase di import",;
    HelpContextID = 169146510,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=101, Top=376, InputMask=replicate('X',30)

  func oSTBACXML_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc

  add object oSTDRIVER_1_59 as StdField with uid="YNLYNLSJFH",rtseq=41,rtrep=.f.,;
    cFormVar = "w_STDRIVER", cQueryName = "STDRIVER",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Routine driver utilizzato per popolare tabelle EDI",;
    HelpContextID = 73148280,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=101, Top=402, InputMask=replicate('X',30)

  func oSTDRIVER_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc

  add object oSTFUNRIC_1_60 as StdField with uid="RMPCTBNAFH",rtseq=42,rtrep=.f.,;
    cFormVar = "w_STFUNRIC", cQueryName = "STFUNRIC",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Funzione ricorsiva per valorizzazione tabelle EDI",;
    HelpContextID = 256948375,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=272, Top=402, InputMask=replicate('X',30)

  func oSTFUNRIC_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc


  add object oSTTIPGES_1_61 as StdCombo with uid="RWWCRECZYI",rtseq=43,rtrep=.f.,left=101,top=432,width=133,height=21;
    , ToolTipText = "Tipo gestione associata maschera\routine";
    , HelpContextID = 96741241;
    , cFormVar="w_STTIPGES",RowSource=""+"Maschera,"+"Routine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTTIPGES_1_61.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oSTTIPGES_1_61.GetRadio()
    this.Parent.oContained.w_STTIPGES = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPGES_1_61.SetRadio()
    this.Parent.oContained.w_STTIPGES=trim(this.Parent.oContained.w_STTIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_STTIPGES=='M',1,;
      iif(this.Parent.oContained.w_STTIPGES=='R',2,;
      0))
  endfunc

  func oSTTIPGES_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc

  add object oSTMODMSK_1_62 as StdField with uid="WNRCNNVKJP",rtseq=44,rtrep=.f.,;
    cFormVar = "w_STMODMSK", cQueryName = "STMODMSK",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Permette di specificare la gestione associata alla struttura per la modifica dei dati presenti nelle tabelle EDI",;
    HelpContextID = 185186161,;
   bGlobalFont=.t.,;
    Height=21, Width=229, Left=238, Top=432, InputMask=replicate('X',30)

  func oSTMODMSK_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc

  add object oSTFLIMPO_1_63 as StdCheck with uid="DKAENOZVRK",rtseq=45,rtrep=.f.,left=101, top=482, caption="Import massivo",;
    ToolTipText = "Se attivo la struttura viene utilizzata per l'import massivo",;
    HelpContextID = 190203765,;
    cFormVar="w_STFLIMPO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTFLIMPO_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTFLIMPO_1_63.GetRadio()
    this.Parent.oContained.w_STFLIMPO = this.RadioValue()
    return .t.
  endfunc

  func oSTFLIMPO_1_63.SetRadio()
    this.Parent.oContained.w_STFLIMPO=trim(this.Parent.oContained.w_STFLIMPO)
    this.value = ;
      iif(this.Parent.oContained.w_STFLIMPO=='S',1,;
      0)
  endfunc

  add object oSTORDIMP_1_64 as StdField with uid="OWQSTTKBPJ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_STORDIMP", cQueryName = "STORDIMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Definisce la priorit� delle strutture durante l'import massivo",;
    HelpContextID = 150153354,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=368, Top=482, cSayPict='"999"', cGetPict='"999"'

  func oSTORDIMP_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLIMPO='S')
    endwith
   endif
  endfunc


  add object oLinkPC_1_65 as StdButton with uid="AFICGXDOPF",left=419, top=482, width=48,height=45,;
    CpPicture="BMP\FILEFIND.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle gestione dei pattern file associati";
    , HelpContextID = 44917622;
    , caption='\<File ass.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_65.Click()
      this.Parent.oContained.GSVA_MPF.LinkPCClick()
    endproc

  func oLinkPC_1_65.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_STFLIMPO='S')
      endwith
    endif
  endfunc


  add object oBtn_1_66 as StdButton with uid="OHLTUWPPVO",left=545, top=482, width=48,height=45,;
    CpPicture="BMP\TREEVIEW.BMP", caption="", nPag=1;
    , ToolTipText = "Premere accedere alla treeview";
    , HelpContextID = 172053933;
    , caption='\<Treeview';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        GSVA_BST(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_66.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( .cFunction='Query')
      endwith
    endif
  endfunc


  add object oLinkPC_1_67 as StdButton with uid="WMMAFJKEOV",left=600, top=482, width=48,height=45,;
    CpPicture="BMP\PREDEFINITO.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai valori predefiniti";
    , HelpContextID = 195750139;
    , caption='\<Val. pred.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_67.Click()
      this.Parent.oContained.GSVA_MVP.LinkPCClick()
      this.Parent.oContained.WriteTo_GSVA_MVP()
    endproc

  func oLinkPC_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_STCODENT) AND .w_STFLTYPE<>'O')
      endwith
    endif
  endfunc

  func oLinkPC_1_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_STCODENT))
     endwith
    endif
  endfunc

  add object oSTBACIMP_1_68 as StdField with uid="OHAXYRJRFV",rtseq=47,rtrep=.f.,;
    cFormVar = "w_STBACIMP", cQueryName = "STBACIMP",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Routine che esegue ultimo step dell'import, da tabelle EDI a archivi adhoc ",;
    HelpContextID = 152369290,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=101, Top=457, InputMask=replicate('X',30)

  func oSTBACIMP_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O')
    endwith
   endif
  endfunc


  add object oSTTIPFIN_1_69 as StdCombo with uid="WIIWBSYLLT",rtseq=48,rtrep=.f.,left=101,top=350,width=115,height=21;
    , ToolTipText = "Tipo fine riga, ad esempio, Apice per file di tipo Edifact";
    , HelpContextID = 188471436;
    , cFormVar="w_STTIPFIN",RowSource=""+"Fine riga CR+LF,"+"Fine riga LF,"+"Apice (Edifact),"+"Altro carattere", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTTIPFIN_1_69.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'R',;
    iif(this.value =3,'A',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oSTTIPFIN_1_69.GetRadio()
    this.Parent.oContained.w_STTIPFIN = this.RadioValue()
    return .t.
  endfunc

  func oSTTIPFIN_1_69.SetRadio()
    this.Parent.oContained.w_STTIPFIN=trim(this.Parent.oContained.w_STTIPFIN)
    this.value = ;
      iif(this.Parent.oContained.w_STTIPFIN=='F',1,;
      iif(this.Parent.oContained.w_STTIPFIN=='R',2,;
      iif(this.Parent.oContained.w_STTIPFIN=='A',3,;
      iif(this.Parent.oContained.w_STTIPFIN=='T',4,;
      0))))
  endfunc

  func oSTTIPFIN_1_69.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E')
    endwith
  endfunc


  add object oBtn_1_70 as StdButton with uid="HEKRVHWIZY",left=600, top=350, width=48,height=45,;
    CpPicture="BMP\BLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la struttura";
    , HelpContextID = 17821206;
    , caption='\<Blocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_70.Click()
      with this.Parent.oContained
        GSVA_BST(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_70.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_STFLPROV='S' and .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_70.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STFLPROV='N' or EMPTY(.w_STCODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_71 as StdButton with uid="CUYDSWIVKH",left=600, top=350, width=48,height=45,;
    CpPicture="BMP\SBLOCCA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rendere provvisoria la struttura";
    , HelpContextID = 52147494;
    , caption='\<Sblocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_71.Click()
      with this.Parent.oContained
        GSVA_BST(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_STFLPROV='N' and .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STFLPROV='S' or EMPTY(.w_STCODICE))
     endwith
    endif
  endfunc


  add object oObj_1_72 as cp_trafficlight with uid="FNKBUUREYN",left=487, top=358, width=24,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 68261094


  add object oObj_1_76 as cp_calclbl with uid="ATYVMNITUS",left=515, top=361, width=79,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 68261094

  add object oStr_1_6 as StdString with uid="WVEBUTUYDM",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=144, Height=18,;
    Caption="Codice struttura:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="AJHWPQUHSS",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=144, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="TGDEWGFPHX",Visible=.t., Left=6, Top=67,;
    Alignment=1, Width=144, Height=18,;
    Caption="Entit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CTYCNPDATW",Visible=.t., Left=6, Top=97,;
    Alignment=1, Width=144, Height=18,;
    Caption="Elemento radice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ASWDVUTUJW",Visible=.t., Left=6, Top=127,;
    Alignment=1, Width=144, Height=18,;
    Caption="Formato gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="GSFRKHOOWE",Visible=.t., Left=6, Top=178,;
    Alignment=1, Width=144, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QLEBIMVOHE",Visible=.t., Left=6, Top=153,;
    Alignment=1, Width=144, Height=18,;
    Caption="Tipo file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GXCNRQQGLQ",Visible=.t., Left=435, Top=13,;
    Alignment=1, Width=107, Height=18,;
    Caption="Separatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="IIBMOEEBEZ",Visible=.t., Left=6, Top=208,;
    Alignment=1, Width=144, Height=18,;
    Caption="Normalizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="MRKGUEBSOU",Visible=.t., Left=402, Top=40,;
    Alignment=1, Width=140, Height=18,;
    Caption="Indentatura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="WHEIBUHNWQ",Visible=.t., Left=6, Top=261,;
    Alignment=1, Width=144, Height=18,;
    Caption="Lung. ID primario:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL<>'L')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="KNLGVPXTFE",Visible=.t., Left=6, Top=233,;
    Alignment=1, Width=144, Height=18,;
    Caption="Cod. tab. piatte:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (Not Empty(.w_STFILXSD))
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="QDAJQJKCVC",Visible=.t., Left=410, Top=261,;
    Alignment=1, Width=132, Height=17,;
    Caption="Tipologia struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="XQLKSSYJWT",Visible=.t., Left=52, Top=292,;
    Alignment=1, Width=107, Height=18,;
    Caption="File schema XSD:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_EN_TABLE<>'DIS_TINT')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="MOGLYWXBYU",Visible=.t., Left=491, Top=325,;
    Alignment=0, Width=59, Height=16,;
    Caption="Stato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="EYRHMOULKX",Visible=.t., Left=242, Top=352,;
    Alignment=1, Width=116, Height=18,;
    Caption="Lung. segmento:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL<>'L' or .w_STTIPFIN <>'A')
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="XQVTONIYMS",Visible=.t., Left=6, Top=433,;
    Alignment=1, Width=94, Height=18,;
    Caption="Gest. terzo step:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="JHHUCUUFZQ",Visible=.t., Left=12, Top=405,;
    Alignment=1, Width=88, Height=18,;
    Caption="Secondo step:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="ZBJXJYBKZZ",Visible=.t., Left=-31, Top=323,;
    Alignment=1, Width=219, Height=18,;
    Caption="Parametri importazione file EDI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_81 as StdString with uid="CTCPMYKBAL",Visible=.t., Left=21, Top=460,;
    Alignment=1, Width=79, Height=18,;
    Caption="Quarto step:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="WOVMVTEPKW",Visible=.t., Left=34, Top=379,;
    Alignment=1, Width=66, Height=18,;
    Caption="Primo step:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="PBOHTYQTIX",Visible=.t., Left=235, Top=405,;
    Alignment=1, Width=37, Height=18,;
    Caption="Funz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="SITIDJGXNI",Visible=.t., Left=260, Top=484,;
    Alignment=1, Width=106, Height=18,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="SSHXRTQJDY",Visible=.t., Left=277, Top=352,;
    Alignment=1, Width=85, Height=18,;
    Caption="Separatore/i.:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E')
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="DHAQWGPTRM",Visible=.t., Left=4, Top=352,;
    Alignment=1, Width=96, Height=18,;
    Caption="Tipo fine riga:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E' and .w_STTIPFIL <>'L')
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="IHMVWCCYVX",Visible=.t., Left=387, Top=352,;
    Alignment=0, Width=6, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.w_STTIPFIL <>'E' or Empty(.w_STCARSEP))
    endwith
  endfunc

  add object oBox_1_74 as StdBox with uid="LRWKUJZAEY",left=489, top=340, width=164,height=2

  add object oBox_1_79 as StdBox with uid="FCNMDUGNQH",left=12, top=340, width=459,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_ast','VASTRUTT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".STCODICE=VASTRUTT.STCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
