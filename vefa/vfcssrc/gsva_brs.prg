* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_brs                                                        *
*              Ricostruzione saldi imballi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-05                                                      *
* Last revis.: 2009-06-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSerial,pDelete,pCodese
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_brs",oParentObject,m.pSerial,m.pDelete,m.pCodese)
return(i_retval)

define class tgsva_brs as StdBatch
  * --- Local variables
  pSerial = space(10)
  pDelete = space(1)
  pCodese = space(4)
  w_SERIAL = space(10)
  w_DELETE = space(1)
  w_CODESE = space(5)
  * --- WorkFile variables
  SAL_IMBA_idx=0
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'aggiornamento saldi imballi.
    *     Lanciato dai documenti alla Delete Start o Update Start per cancellazione/modifica e da GSAR_BEA per caricamento
    *     Da GSVA_KRS se ricostruzione Saldi 
    *     Lanciato anche da funzione FLR_DOCUM della logistica remota
    *     
    *     Parametri
    *     pSerial: seriale documento del quale bisogna aggiornare i saldi imballi. 
    *     pDelete : 'D' cancellazione del documento di esplosione; 'C' caricamento
    *     pCodese: esercizio di storicizzazione. Passato solo da batch GSSR_BSD
    *     
    *     Nell'aggiornamento saldi globale pSerial � vuoto 
    *     Nella storicizzazione documenti vale "STORICO"
    this.w_SERIAL = this.pSerial
    this.w_DELETE = this.pDelete
    if this.w_SERIAL = "STORICO"
      this.w_CODESE = this.pCodese
      * --- Lanciato da storicizzazione documenti GSSR_BSD
      *     Decremento la quantit� in linea dei documenti storicizzati e aumento quella fuori linea
      * --- Write into SAL_IMBA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SICODART,SITIPCON,SICODCON"
        do vq_exec with 'gsva1ssr',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_IMBA_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SIQTASTO = SAL_IMBA.SIQTASTO+_t2.SIQTASTO";
            +i_ccchkf;
            +" from "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 set ";
            +"SAL_IMBA.SIQTASTO = SAL_IMBA.SIQTASTO+_t2.SIQTASTO";
            +Iif(Empty(i_ccchkf),"",",SAL_IMBA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SAL_IMBA.SICODART = t2.SICODART";
                +" and "+"SAL_IMBA.SITIPCON = t2.SITIPCON";
                +" and "+"SAL_IMBA.SICODCON = t2.SICODCON";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set (";
            +"SIQTASTO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SIQTASTO+t2.SIQTASTO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set ";
            +"SIQTASTO = SAL_IMBA.SIQTASTO+_t2.SIQTASTO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SICODART = "+i_cQueryTable+".SICODART";
                +" and "+i_cTable+".SITIPCON = "+i_cQueryTable+".SITIPCON";
                +" and "+i_cTable+".SICODCON = "+i_cQueryTable+".SICODCON";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SIQTASTO = (select "+i_cTable+".SIQTASTO+SIQTASTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      if Empty(this.w_SERIAL)
        Ah_Msg("Inizio ricostruzione...")
        * --- Ricostruzione saldi globale. 
        *     1) Salvo il valore storicizzato in una tabellina temporanea
        *     2) Elimino tutti i saldi prima di inserirli
        *     3) Dopo averli inseriti reinserisco la quantit� storicizzata
        * --- Create temporary table TMPVEND1
        i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\vefa\exe\query\gsva4qsi',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPVEND1_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Delete from SAL_IMBA
        i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if Not Empty(this.w_SERIAL)
        * --- Aggiorno il saldo gi� esistente. 
        *     Questa Write viene eseguita solo per l'aggiornamento del singolo documento:
        *     cancellazione o caricamento singolo
        * --- Write into SAL_IMBA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SICODART,SITIPCON,SICODCON"
          do vq_exec with 'gsva3qsi',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_IMBA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTAESI";
              +i_ccchkf;
              +" from "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 set ";
              +"SAL_IMBA.SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTAESI";
              +Iif(Empty(i_ccchkf),"",",SAL_IMBA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SAL_IMBA.SICODART = t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = t2.SICODCON";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set (";
              +"SIQTAESI";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SIQTAESI+t2.SIQTAESI";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set ";
              +"SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTAESI";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SICODART = "+i_cQueryTable+".SICODART";
                  +" and "+i_cTable+".SITIPCON = "+i_cQueryTable+".SITIPCON";
                  +" and "+i_cTable+".SICODCON = "+i_cQueryTable+".SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SIQTAESI = (select "+i_cTable+".SIQTAESI+SIQTAESI from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_DELETE="C"
        * --- Inserimento dei nuovi record nei saldi imballi
        * --- Insert into SAL_IMBA
        i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsva2qsi",this.SAL_IMBA_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if Empty(this.w_SERIAL)
          * --- Reinserisco i record presenti nei saldi solo per il valore storicizzato ma che non hanno
          *     pi� movimenti. Se non li reinserissi sparirebbero dalla tabella
          * --- Insert into SAL_IMBA
          i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsva7qsi",this.SAL_IMBA_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      if Empty(this.w_SERIAL)
        * --- Risommo le quantit� storicizzate che precedentemente avevo salvato in TMPVEND1
        * --- Write into SAL_IMBA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SAL_IMBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SAL_IMBA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SICODART,SITIPCON,SICODCON"
          do vq_exec with 'gsva5qsi',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SAL_IMBA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTASTO";
              +",SIQTASTO = _t2.SIQTASTO";
              +i_ccchkf;
              +" from "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA, "+i_cQueryTable+" _t2 set ";
              +"SAL_IMBA.SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTASTO";
              +",SAL_IMBA.SIQTASTO = _t2.SIQTASTO";
              +Iif(Empty(i_ccchkf),"",",SAL_IMBA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="SAL_IMBA.SICODART = t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = t2.SICODCON";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set (";
              +"SIQTAESI,";
              +"SIQTASTO";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"SIQTAESI+t2.SIQTASTO,";
              +"t2.SIQTASTO";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="SAL_IMBA.SICODART = _t2.SICODART";
                  +" and "+"SAL_IMBA.SITIPCON = _t2.SITIPCON";
                  +" and "+"SAL_IMBA.SICODCON = _t2.SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SAL_IMBA set ";
              +"SIQTAESI = SAL_IMBA.SIQTAESI+_t2.SIQTASTO";
              +",SIQTASTO = _t2.SIQTASTO";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SICODART = "+i_cQueryTable+".SICODART";
                  +" and "+i_cTable+".SITIPCON = "+i_cQueryTable+".SITIPCON";
                  +" and "+i_cTable+".SICODCON = "+i_cQueryTable+".SICODCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SIQTAESI = (select "+i_cTable+".SIQTAESI+SIQTASTO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SIQTASTO = (select SIQTASTO from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if Empty(this.w_SERIAL)
        * --- Drop temporary table TMPVEND1
        i_nIdx=cp_GetTableDefIdx('TMPVEND1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND1')
        endif
      endif
      if Empty(this.w_SERIAL) And Type("this.oParentObject")="O" and Lower(this.oParentObject.Class)="tgsva_krs"
        Ah_Msg("Fine elaborazione")
        This.oParentObject.ecpQuit()
      endif
    endif
  endproc


  proc Init(oParentObject,pSerial,pDelete,pCodese)
    this.pSerial=pSerial
    this.pDelete=pDelete
    this.pCodese=pCodese
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='SAL_IMBA'
    this.cWorkTables[2]='*TMPVEND1'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSerial,pDelete,pCodese"
endproc
