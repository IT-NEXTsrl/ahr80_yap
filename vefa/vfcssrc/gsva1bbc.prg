* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva1bbc                                                        *
*              Lancia schede di calcolo                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_54]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-24                                                      *
* Last revis.: 2006-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva1bbc",oParentObject)
return(i_retval)

define class tgsva1bbc as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_AZIONE = space(1)
  w_CLSERIAL = space(10)
  w_SERIAL = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio funzionalit� legate a schde di calcolo, da men� contestuale
    *     Lanciato da GSva_kbc 
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_AZIONE = IIF(Type("g_omenu.okey")<>"U",g_oMenu.cBatchType,"A")
      this.w_SERIAL = Nvl(g_oMenu.oKey(1,3),"")
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_SERIAL = &cCurs..CLSERIAL
      this.w_AZIONE = "S"
    endif
    this.w_OBJECT = "GSVA_MSL"
    OpenGest(this.w_AZIONE,this.w_OBJECT, "CLSERIAL",this.w_SERIAL)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
