* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bcl                                                        *
*              VISUAL ZOOM INTESTATARIO                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-04                                                      *
* Last revis.: 2006-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bcl",oParentObject,m.pAZIO)
return(i_retval)

define class tgsva_bcl as StdBatch
  * --- Local variables
  pAZIO = space(1)
  w_ANDESCRI = space(40)
  w_GRDESCRI = space(40)
  w_ANCODICE = space(15)
  w_GRCODICE = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  GRU_INTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da gestione trascodifiche GSVA_MTR
    do case
      case this.pAZIO="V"
        vx_exec("..\VEFA\EXE\QUERY\gsva_mtr.vzm",this)
        this.oParentObject.w_DESINT = this.w_ANDESCRI
        this.oParentObject.w_TRCODCON = this.w_ANCODICE
      case this.pAZIO="C"
        do case
          case this.oParentObject.w_TRTIPCON $ "C-F"
            if Not Empty(this.oParentObject.w_TRTIPCON) 
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE,ANDESCRI"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TRTIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_TRCODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE,ANDESCRI;
                  from (i_cTable) where;
                      ANTIPCON = this.oParentObject.w_TRTIPCON;
                      and ANCODICE = this.oParentObject.w_TRCODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TRCODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                this.oParentObject.w_DESINT = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
                use
                if i_Rows=0
                  ah_ErrorMsg("Attenzione, intestatario non ammesso")
                endif
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          case this.oParentObject.w_TRTIPCON ="G"
            if Not Empty(this.oParentObject.w_TRCODRAG) 
              * --- Read from GRU_INTE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GRU_INTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GRU_INTE_idx,2],.t.,this.GRU_INTE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "GRCODICE,GRDESCRI"+;
                  " from "+i_cTable+" GRU_INTE where ";
                      +"GRCODICE = "+cp_ToStrODBC(this.oParentObject.w_TRCODRAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  GRCODICE,GRDESCRI;
                  from (i_cTable) where;
                      GRCODICE = this.oParentObject.w_TRCODRAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_TRCODRAG = NVL(cp_ToDate(_read_.GRCODICE),cp_NullValue(_read_.GRCODICE))
                this.oParentObject.w_DESINT = NVL(cp_ToDate(_read_.GRDESCRI),cp_NullValue(_read_.GRDESCRI))
                use
                if i_Rows=0
                  ah_ErrorMsg("Attenzione, gruppo intestatari non ammesso")
                endif
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
        endcase
      case this.pAZIO="G"
        vx_exec("..\VEFA\EXE\QUERY\gsva1mtr.vzm",this)
        this.oParentObject.w_DESINT = this.w_GRDESCRI
        this.oParentObject.w_TRCODRAG = this.w_GRCODICE
    endcase
  endproc


  proc Init(oParentObject,pAZIO)
    this.pAZIO=pAZIO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='GRU_INTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIO"
endproc
