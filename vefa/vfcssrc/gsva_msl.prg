* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_msl                                                        *
*              Scheda di calcolo listini                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_111]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-12-16                                                      *
* Last revis.: 2014-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_msl"))

* --- Class definition
define class tgsva_msl as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 782
  Height = 435+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-15"
  HelpContextID=171281815
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=66

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CLLSMAST_IDX = 0
  CLLSDETT_IDX = 0
  LISTINI_IDX = 0
  INVENTAR_IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  MAGAZZIN_IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "CLLSMAST"
  cFileDetail = "CLLSDETT"
  cKeySelect = "CLSERIAL"
  cKeyWhere  = "CLSERIAL=this.w_CLSERIAL"
  cKeyDetail  = "CLSERIAL=this.w_CLSERIAL"
  cKeyWhereODBC = '"CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';

  cKeyDetailWhereODBC = '"CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CLLSDETT.CLSERIAL="+cp_ToStrODBC(this.w_CLSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CLLSDETT.CPROWNUM '
  cPrg = "gsva_msl"
  cComment = "Scheda di calcolo listini"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  o_AZIENDA = space(5)
  w_DATACAM = ctod('  /  /  ')
  w_CLSERIAL = space(10)
  o_CLSERIAL = space(10)
  w_CLDESCRI = space(50)
  w_CLGRUMER = space(5)
  o_CLGRUMER = space(5)
  w_CLCODART = space(20)
  o_CLCODART = space(20)
  w_CLVALINI = ctod('  /  /  ')
  w_CLVALFIN = ctod('  /  /  ')
  o_CLVALFIN = ctod('  /  /  ')
  w_CLPRIORI = 0
  w_CLBASSEL = space(2)
  o_CLBASSEL = space(2)
  w_LIDESCRI = space(40)
  w_CLBASMAG = space(5)
  w_CLTIPUMI = space(1)
  w_VALUTA = space(10)
  o_VALUTA = space(10)
  w_CLAGGLIS = space(5)
  w_CLAGGCAO = 0
  w_CLAGGDIN = ctod('  /  /  ')
  w_CLAGGDFI = ctod('  /  /  ')
  w_CLAGGESI = space(1)
  w_CLAGGSCO = space(1)
  w_CLAGGSC1 = 0
  w_CLAGGSC2 = 0
  w_CLAGGSC3 = 0
  w_CLAGGSC4 = 0
  w_FLSCON = space(1)
  w_CLBASLIS = space(5)
  w_LIDESCR2 = space(40)
  w_VALRIF = space(10)
  o_VALRIF = space(10)
  w_CLCAORIF = 0
  w_DECTOT = 0
  w_CAOVAL = 0
  w_CAOVAL2 = 0
  w_DECTOT2 = 0
  w_CLBASESE = space(4)
  w_CLBASINV = space(6)
  w_DATINV = ctod('  /  /  ')
  w_VALNAZ = space(3)
  o_VALNAZ = space(3)
  w_CLCAONAZ = 0
  w_DESART = space(40)
  w_DESGRU = space(35)
  w_CLNUMELA = 0
  w_CLULTORA = 0
  w_CLULTMIN = 0
  w_VALPIC = space(3)
  o_VALPIC = space(3)
  w_DECTOP = 0
  w_CALCPICT = 0
  w_VALOFIN = 0
  w_CLCALSEL = space(1)
  o_CLCALSEL = space(1)
  w_CLRICAR1 = 0
  w_CLMOLTIP = 0
  w_CLMOLTI2 = 0
  w_CLRICVAL = 0
  w_CLARROT1 = 0
  w_CLVALOR1 = 0
  w_CLARROT2 = 0
  w_CLVALOR2 = 0
  w_CLARROT3 = 0
  w_CLVALOR3 = 0
  w_CLARROT4 = 0
  w_CLFLDASI = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CLDATRIF = ctod('  /  /  ')
  w_NORIGHE = .F.
  w_CLORISCA = space(1)
  w_OBSART = ctod('  /  /  ')
  w_CAONAZ2 = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_Codazi = this.W_Codazi
  op_CLSERIAL = this.W_CLSERIAL
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CLLSMAST','gsva_msl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mslPag1","gsva_msl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 103087849
      .Pages(2).addobject("oPag","tgsva_mslPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri di calcolo")
      .Pages(2).HelpContextID = 206811284
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLSERIAL_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='INVENTAR'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='CLLSMAST'
    this.cWorkTables[9]='CLLSDETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CLLSMAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CLLSMAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CLSERIAL = NVL(CLSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CLLSMAST where CLSERIAL=KeySet.CLSERIAL
    *
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2],this.bLoadRecFilter,this.CLLSMAST_IDX,"gsva_msl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CLLSMAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CLLSMAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CLLSDETT.","CLLSMAST.")
      i_cTable = i_cTable+' CLLSMAST '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_DATACAM = i_datsys
        .w_LIDESCRI = space(40)
        .w_VALUTA = space(10)
        .w_FLSCON = space(1)
        .w_LIDESCR2 = space(40)
        .w_VALRIF = space(10)
        .w_DECTOT = 0
        .w_CAOVAL2 = 0
        .w_DECTOT2 = 0
        .w_DATINV = ctod("  /  /  ")
        .w_VALNAZ = space(3)
        .w_DECTOP = 0
        .w_NORIGHE = .F.
        .w_CLSERIAL = NVL(CLSERIAL,space(10))
        .op_CLSERIAL = .w_CLSERIAL
        .w_CLDESCRI = NVL(CLDESCRI,space(50))
        .w_CLVALINI = NVL(cp_ToDate(CLVALINI),ctod("  /  /  "))
        .w_CLVALFIN = NVL(cp_ToDate(CLVALFIN),ctod("  /  /  "))
        .w_CLPRIORI = NVL(CLPRIORI,0)
        .w_CLBASSEL = NVL(CLBASSEL,space(2))
        .w_CLBASMAG = NVL(CLBASMAG,space(5))
          * evitabile
          *.link_1_16('Load')
        .w_CLTIPUMI = NVL(CLTIPUMI,space(1))
          .link_1_18('Load')
        .w_CLAGGLIS = NVL(CLAGGLIS,space(5))
          if link_1_19_joined
            this.w_CLAGGLIS = NVL(LSCODLIS119,NVL(this.w_CLAGGLIS,space(5)))
            this.w_LIDESCRI = NVL(LSDESLIS119,space(40))
            this.w_VALUTA = NVL(LSVALLIS119,space(10))
            this.w_FLSCON = NVL(LSFLSCON119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_CLAGGCAO = NVL(CLAGGCAO,0)
        .w_CLAGGDIN = NVL(cp_ToDate(CLAGGDIN),ctod("  /  /  "))
        .w_CLAGGDFI = NVL(cp_ToDate(CLAGGDFI),ctod("  /  /  "))
        .w_CLAGGESI = NVL(CLAGGESI,space(1))
        .w_CLAGGSCO = NVL(CLAGGSCO,space(1))
        .w_CLAGGSC1 = NVL(CLAGGSC1,0)
        .w_CLAGGSC2 = NVL(CLAGGSC2,0)
        .w_CLAGGSC3 = NVL(CLAGGSC3,0)
        .w_CLAGGSC4 = NVL(CLAGGSC4,0)
        .w_CLBASLIS = NVL(CLBASLIS,space(5))
          if link_1_36_joined
            this.w_CLBASLIS = NVL(LSCODLIS136,NVL(this.w_CLBASLIS,space(5)))
            this.w_LIDESCR2 = NVL(LSDESLIS136,space(40))
            this.w_VALRIF = NVL(LSVALLIS136,space(10))
          else
          .link_1_36('Load')
          endif
          .link_1_38('Load')
        .w_CLCAORIF = NVL(CLCAORIF,0)
        .w_CAOVAL = GETCAM(.w_VALUTA, I_DATSYS)
        .w_CLBASESE = NVL(CLBASESE,space(4))
          .link_1_45('Load')
        .w_CLBASINV = NVL(CLBASINV,space(6))
          .link_1_48('Load')
          .link_1_51('Load')
        .w_CLCAONAZ = NVL(CLCAONAZ,0)
        .w_CLNUMELA = NVL(CLNUMELA,0)
        .w_CLULTORA = NVL(CLULTORA,0)
        .w_CLULTMIN = NVL(CLULTMIN,0)
        .w_VALPIC = .w_VALUTA
          .link_4_1('Load')
        .w_CALCPICT = DEFPCUNI(.w_DECTOP)
        .w_VALOFIN = MAX(.w_CLVALOR1, .w_CLVALOR2, .w_CLVALOR3)
        .w_CLCALSEL = NVL(CLCALSEL,space(1))
        .w_CLRICAR1 = NVL(CLRICAR1,0)
        .w_CLMOLTIP = NVL(CLMOLTIP,0)
        .w_CLMOLTI2 = NVL(CLMOLTI2,0)
        .w_CLRICVAL = NVL(CLRICVAL,0)
        .w_CLARROT1 = NVL(CLARROT1,0)
        .w_CLVALOR1 = NVL(CLVALOR1,0)
        .w_CLARROT2 = NVL(CLARROT2,0)
        .w_CLVALOR2 = NVL(CLVALOR2,0)
        .w_CLARROT3 = NVL(CLARROT3,0)
        .w_CLVALOR3 = NVL(CLVALOR3,0)
        .w_CLARROT4 = NVL(CLARROT4,0)
        .w_CLFLDASI = NVL(CLFLDASI,space(1))
        .w_OBTEST = .w_CLVALFIN
        .w_CLDATRIF = NVL(cp_ToDate(CLDATRIF),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .w_CLORISCA = NVL(CLORISCA,space(1))
        .w_CAONAZ2 = GETCAM(.w_VALNAZ, i_DatSys)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CLLSMAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CLLSDETT where CLSERIAL=KeySet.CLSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsva_msl
      * --- Setta Ordine per Gr.Merceologico, Articolo
      i_cOrder = 'order by CLCODART, CLGRUMER '
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CLLSDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSDETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CLLSDETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CLLSDETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CLLSDETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
        select * from (i_cTable) CLLSDETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESART = space(40)
          .w_DESGRU = space(35)
          .w_OBSART = ctod("  /  /  ")
          .w_CPROWNUM = CPROWNUM
          .w_CLGRUMER = NVL(CLGRUMER,space(5))
          if link_2_1_joined
            this.w_CLGRUMER = NVL(GMCODICE201,NVL(this.w_CLGRUMER,space(5)))
            this.w_DESGRU = NVL(GMDESCRI201,space(35))
          else
          .link_2_1('Load')
          endif
          .w_CLCODART = NVL(CLCODART,space(20))
          if link_2_2_joined
            this.w_CLCODART = NVL(ARCODART202,NVL(this.w_CLCODART,space(20)))
            this.w_DESART = NVL(ARDESART202,space(40))
            this.w_OBSART = NVL(cp_ToDate(ARDTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CAOVAL = GETCAM(.w_VALUTA, I_DATSYS)
        .w_VALPIC = .w_VALUTA
        .w_CALCPICT = DEFPCUNI(.w_DECTOP)
        .w_VALOFIN = MAX(.w_CLVALOR1, .w_CLVALOR2, .w_CLVALOR3)
        .w_OBTEST = .w_CLVALFIN
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .w_CAONAZ2 = GETCAM(.w_VALNAZ, i_DatSys)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_AZIENDA=space(5)
      .w_DATACAM=ctod("  /  /  ")
      .w_CLSERIAL=space(10)
      .w_CLDESCRI=space(50)
      .w_CLGRUMER=space(5)
      .w_CLCODART=space(20)
      .w_CLVALINI=ctod("  /  /  ")
      .w_CLVALFIN=ctod("  /  /  ")
      .w_CLPRIORI=0
      .w_CLBASSEL=space(2)
      .w_LIDESCRI=space(40)
      .w_CLBASMAG=space(5)
      .w_CLTIPUMI=space(1)
      .w_VALUTA=space(10)
      .w_CLAGGLIS=space(5)
      .w_CLAGGCAO=0
      .w_CLAGGDIN=ctod("  /  /  ")
      .w_CLAGGDFI=ctod("  /  /  ")
      .w_CLAGGESI=space(1)
      .w_CLAGGSCO=space(1)
      .w_CLAGGSC1=0
      .w_CLAGGSC2=0
      .w_CLAGGSC3=0
      .w_CLAGGSC4=0
      .w_FLSCON=space(1)
      .w_CLBASLIS=space(5)
      .w_LIDESCR2=space(40)
      .w_VALRIF=space(10)
      .w_CLCAORIF=0
      .w_DECTOT=0
      .w_CAOVAL=0
      .w_CAOVAL2=0
      .w_DECTOT2=0
      .w_CLBASESE=space(4)
      .w_CLBASINV=space(6)
      .w_DATINV=ctod("  /  /  ")
      .w_VALNAZ=space(3)
      .w_CLCAONAZ=0
      .w_DESART=space(40)
      .w_DESGRU=space(35)
      .w_CLNUMELA=0
      .w_CLULTORA=0
      .w_CLULTMIN=0
      .w_VALPIC=space(3)
      .w_DECTOP=0
      .w_CALCPICT=0
      .w_VALOFIN=0
      .w_CLCALSEL=space(1)
      .w_CLRICAR1=0
      .w_CLMOLTIP=0
      .w_CLMOLTI2=0
      .w_CLRICVAL=0
      .w_CLARROT1=0
      .w_CLVALOR1=0
      .w_CLARROT2=0
      .w_CLVALOR2=0
      .w_CLARROT3=0
      .w_CLVALOR3=0
      .w_CLARROT4=0
      .w_CLFLDASI=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CLDATRIF=ctod("  /  /  ")
      .w_NORIGHE=.f.
      .w_CLORISCA=space(1)
      .w_OBSART=ctod("  /  /  ")
      .w_CAONAZ2=0
      if .cFunction<>"Filter"
        .w_AZIENDA = i_CODAZI
        .w_DATACAM = i_datsys
        .DoRTCalc(3,4,.f.)
        .w_CLGRUMER = SPACE(5)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CLGRUMER))
         .link_2_1('Full')
        endif
        .w_CLCODART = SPACE(20)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CLCODART))
         .link_2_2('Full')
        endif
        .w_CLVALINI = i_DATSYS
        .DoRTCalc(8,9,.f.)
        .w_CLBASSEL = 'LI'
        .DoRTCalc(11,11,.f.)
        .w_CLBASMAG = IIF(LEFT(.w_CLBASSEL,1)='U',.w_CLBASMAG, SPACE(5))
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CLBASMAG))
         .link_1_16('Full')
        endif
        .w_CLTIPUMI = IIF(LEFT(.w_CLBASSEL,1)='L' AND NOT EMPTY(.w_CLTIPUMI), .w_CLTIPUMI, 'P')
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_VALUTA))
         .link_1_18('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CLAGGLIS))
         .link_1_19('Full')
        endif
        .w_CLAGGCAO = GETCAM(.w_VALUTA, IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('  -  -  '))), .w_DATACAM, .w_CLDATRIF), 7)
        .DoRTCalc(17,18,.f.)
        .w_CLAGGESI = ' '
        .w_CLAGGSCO = 'F'
        .DoRTCalc(21,25,.f.)
        .w_CLBASLIS = IIF(LEFT(.w_CLBASSEL,1)='L',.w_CLBASLIS, SPACE(5))
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CLBASLIS))
         .link_1_36('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_VALRIF))
         .link_1_38('Full')
        endif
        .w_CLCAORIF = GETCAM(.w_VALRIF, .w_CLDATRIF, 7)
        .DoRTCalc(30,30,.f.)
        .w_CAOVAL = GETCAM(.w_VALUTA, I_DATSYS)
        .DoRTCalc(32,33,.f.)
        .w_CLBASESE = IIF(LEFT(.w_CLBASSEL,1)='I',.w_CLBASESE, SPACE(4))
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CLBASESE))
         .link_1_45('Full')
        endif
        .w_CLBASINV = IIF(LEFT(.w_CLBASSEL,1)='I',.w_CLBASINV, SPACE(6))
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CLBASINV))
         .link_1_48('Full')
        endif
        .DoRTCalc(36,37,.f.)
        if not(empty(.w_VALNAZ))
         .link_1_51('Full')
        endif
        .w_CLCAONAZ = GETCAM(.w_VALNAZ, IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('  -  -  '))), .w_DATACAM, .w_CLDATRIF), 7)
        .DoRTCalc(39,43,.f.)
        .w_VALPIC = .w_VALUTA
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_VALPIC))
         .link_4_1('Full')
        endif
        .DoRTCalc(45,45,.f.)
        .w_CALCPICT = DEFPCUNI(.w_DECTOP)
        .w_VALOFIN = MAX(.w_CLVALOR1, .w_CLVALOR2, .w_CLVALOR3)
        .w_CLCALSEL = 'R'
        .w_CLRICAR1 = IIF(.w_CLCALSEL='R',.w_CLRICAR1,0)
        .w_CLMOLTIP = IIF(.w_CLCALSEL='M',.w_CLMOLTIP,0)
        .w_CLMOLTI2 = IIF(.w_CLCALSEL='M',.w_CLMOLTI2,0)
        .w_CLRICVAL = IIF(.w_CLCALSEL$'RM',.w_CLRICVAL,0)
        .DoRTCalc(53,59,.f.)
        .w_CLFLDASI = IIF(LEFT(.w_CLBASSEL,1)='L',.w_CLFLDASI, SPACE(1))
        .w_OBTEST = .w_CLVALFIN
        .w_CLDATRIF = IIF(LEFT(.w_CLBASSEL,1)='L',IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('    /  /  '))), .w_DATACAM,.w_CLDATRIF), CP_CHARTODATE('    /  /  ') )
        .w_NORIGHE = .F.
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .w_CLORISCA = IIF(LEFT(.w_CLBASSEL,1)='L', 'S', ' ')
        .DoRTCalc(65,65,.f.)
        .w_CAONAZ2 = GETCAM(.w_VALNAZ, i_DatSys)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CLLSMAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCLSERIAL_1_3.enabled = i_bVal
      .Page1.oPag.oCLDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oCLVALINI_1_9.enabled = i_bVal
      .Page1.oPag.oCLVALFIN_1_11.enabled = i_bVal
      .Page1.oPag.oCLPRIORI_1_12.enabled = i_bVal
      .Page1.oPag.oCLBASSEL_1_14.enabled = i_bVal
      .Page1.oPag.oCLBASMAG_1_16.enabled = i_bVal
      .Page1.oPag.oCLTIPUMI_1_17.enabled = i_bVal
      .Page1.oPag.oCLAGGLIS_1_19.enabled = i_bVal
      .Page1.oPag.oCLAGGCAO_1_20.enabled = i_bVal
      .Page1.oPag.oCLAGGDIN_1_21.enabled = i_bVal
      .Page1.oPag.oCLAGGDFI_1_22.enabled = i_bVal
      .Page1.oPag.oCLAGGESI_1_25.enabled = i_bVal
      .Page1.oPag.oCLAGGSCO_1_26.enabled = i_bVal
      .Page1.oPag.oCLAGGSC1_1_27.enabled = i_bVal
      .Page1.oPag.oCLAGGSC2_1_28.enabled = i_bVal
      .Page1.oPag.oCLAGGSC3_1_29.enabled = i_bVal
      .Page1.oPag.oCLAGGSC4_1_31.enabled = i_bVal
      .Page1.oPag.oCLBASLIS_1_36.enabled = i_bVal
      .Page1.oPag.oCLCAORIF_1_39.enabled = i_bVal
      .Page1.oPag.oCLBASESE_1_45.enabled = i_bVal
      .Page1.oPag.oCLBASINV_1_48.enabled = i_bVal
      .Page1.oPag.oCLCAONAZ_1_52.enabled = i_bVal
      .Page2.oPag.oCLCALSEL_4_10.enabled = i_bVal
      .Page2.oPag.oCLRICAR1_4_12.enabled = i_bVal
      .Page2.oPag.oCLMOLTIP_4_15.enabled = i_bVal
      .Page2.oPag.oCLMOLTI2_4_16.enabled = i_bVal
      .Page2.oPag.oCLRICVAL_4_17.enabled = i_bVal
      .Page2.oPag.oCLARROT1_4_19.enabled = i_bVal
      .Page2.oPag.oCLVALOR1_4_20.enabled = i_bVal
      .Page2.oPag.oCLARROT2_4_21.enabled = i_bVal
      .Page2.oPag.oCLVALOR2_4_22.enabled = i_bVal
      .Page2.oPag.oCLARROT3_4_23.enabled = i_bVal
      .Page2.oPag.oCLVALOR3_4_24.enabled = i_bVal
      .Page2.oPag.oCLARROT4_4_25.enabled = i_bVal
      .Page1.oPag.oCLFLDASI_1_57.enabled = i_bVal
      .Page1.oPag.oCLDATRIF_1_59.enabled = i_bVal
      .Page1.oPag.oCLORISCA_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCLSERIAL_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCLSERIAL_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CLLSMAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SCLLS","i_Codazi,w_CLSERIAL")
      .op_Codazi = .w_Codazi
      .op_CLSERIAL = .w_CLSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLSERIAL,"CLSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDESCRI,"CLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALINI,"CLVALINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALFIN,"CLVALFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLPRIORI,"CLPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLBASSEL,"CLBASSEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLBASMAG,"CLBASMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLTIPUMI,"CLTIPUMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGLIS,"CLAGGLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGCAO,"CLAGGCAO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGDIN,"CLAGGDIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGDFI,"CLAGGDFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGESI,"CLAGGESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGSCO,"CLAGGSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGSC1,"CLAGGSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGSC2,"CLAGGSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGSC3,"CLAGGSC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLAGGSC4,"CLAGGSC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLBASLIS,"CLBASLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCAORIF,"CLCAORIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLBASESE,"CLBASESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLBASINV,"CLBASINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCAONAZ,"CLCAONAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLNUMELA,"CLNUMELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLULTORA,"CLULTORA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLULTMIN,"CLULTMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLCALSEL,"CLCALSEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLRICAR1,"CLRICAR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLMOLTIP,"CLMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLMOLTI2,"CLMOLTI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLRICVAL,"CLRICVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLARROT1,"CLARROT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALOR1,"CLVALOR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLARROT2,"CLARROT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALOR2,"CLVALOR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLARROT3,"CLARROT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLVALOR3,"CLVALOR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLARROT4,"CLARROT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLFLDASI,"CLFLDASI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLDATRIF,"CLDATRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CLORISCA,"CLORISCA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
    i_lTable = "CLLSMAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CLLSMAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVA_SSC with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CLGRUMER C(5);
      ,t_CLCODART C(20);
      ,t_DESART C(40);
      ,t_DESGRU C(35);
      ,CPROWNUM N(10);
      ,t_OBSART D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mslbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLGRUMER_2_1.controlsource=this.cTrsName+'.t_CLGRUMER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODART_2_2.controlsource=this.cTrsName+'.t_CLCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_3.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESGRU_2_4.controlsource=this.cTrsName+'.t_DESGRU'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(84)
    this.AddVLine(342)
    this.AddVLine(498)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLGRUMER_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SCLLS","i_Codazi,w_CLSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CLLSMAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CLLSMAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CLLSMAST')
        local i_cFld
        i_cFld=" "+;
                  "(CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN,CLPRIORI"+;
                  ",CLBASSEL,CLBASMAG,CLTIPUMI,CLAGGLIS,CLAGGCAO"+;
                  ",CLAGGDIN,CLAGGDFI,CLAGGESI,CLAGGSCO,CLAGGSC1"+;
                  ",CLAGGSC2,CLAGGSC3,CLAGGSC4,CLBASLIS,CLCAORIF"+;
                  ",CLBASESE,CLBASINV,CLCAONAZ,CLNUMELA,CLULTORA"+;
                  ",CLULTMIN,CLCALSEL,CLRICAR1,CLMOLTIP,CLMOLTI2"+;
                  ",CLRICVAL,CLARROT1,CLVALOR1,CLARROT2,CLVALOR2"+;
                  ",CLARROT3,CLVALOR3,CLARROT4,CLFLDASI,CLDATRIF"+;
                  ",CLORISCA"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CLSERIAL)+;
                    ","+cp_ToStrODBC(this.w_CLDESCRI)+;
                    ","+cp_ToStrODBC(this.w_CLVALINI)+;
                    ","+cp_ToStrODBC(this.w_CLVALFIN)+;
                    ","+cp_ToStrODBC(this.w_CLPRIORI)+;
                    ","+cp_ToStrODBC(this.w_CLBASSEL)+;
                    ","+cp_ToStrODBCNull(this.w_CLBASMAG)+;
                    ","+cp_ToStrODBC(this.w_CLTIPUMI)+;
                    ","+cp_ToStrODBCNull(this.w_CLAGGLIS)+;
                    ","+cp_ToStrODBC(this.w_CLAGGCAO)+;
                    ","+cp_ToStrODBC(this.w_CLAGGDIN)+;
                    ","+cp_ToStrODBC(this.w_CLAGGDFI)+;
                    ","+cp_ToStrODBC(this.w_CLAGGESI)+;
                    ","+cp_ToStrODBC(this.w_CLAGGSCO)+;
                    ","+cp_ToStrODBC(this.w_CLAGGSC1)+;
                    ","+cp_ToStrODBC(this.w_CLAGGSC2)+;
                    ","+cp_ToStrODBC(this.w_CLAGGSC3)+;
                    ","+cp_ToStrODBC(this.w_CLAGGSC4)+;
                    ","+cp_ToStrODBCNull(this.w_CLBASLIS)+;
                    ","+cp_ToStrODBC(this.w_CLCAORIF)+;
                    ","+cp_ToStrODBCNull(this.w_CLBASESE)+;
                    ","+cp_ToStrODBCNull(this.w_CLBASINV)+;
                    ","+cp_ToStrODBC(this.w_CLCAONAZ)+;
                    ","+cp_ToStrODBC(this.w_CLNUMELA)+;
                    ","+cp_ToStrODBC(this.w_CLULTORA)+;
                    ","+cp_ToStrODBC(this.w_CLULTMIN)+;
                    ","+cp_ToStrODBC(this.w_CLCALSEL)+;
                    ","+cp_ToStrODBC(this.w_CLRICAR1)+;
                    ","+cp_ToStrODBC(this.w_CLMOLTIP)+;
                    ","+cp_ToStrODBC(this.w_CLMOLTI2)+;
                    ","+cp_ToStrODBC(this.w_CLRICVAL)+;
                    ","+cp_ToStrODBC(this.w_CLARROT1)+;
                    ","+cp_ToStrODBC(this.w_CLVALOR1)+;
                    ","+cp_ToStrODBC(this.w_CLARROT2)+;
                    ","+cp_ToStrODBC(this.w_CLVALOR2)+;
                    ","+cp_ToStrODBC(this.w_CLARROT3)+;
                    ","+cp_ToStrODBC(this.w_CLVALOR3)+;
                    ","+cp_ToStrODBC(this.w_CLARROT4)+;
                    ","+cp_ToStrODBC(this.w_CLFLDASI)+;
                    ","+cp_ToStrODBC(this.w_CLDATRIF)+;
                    ","+cp_ToStrODBC(this.w_CLORISCA)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CLLSMAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CLLSMAST')
        cp_CheckDeletedKey(i_cTable,0,'CLSERIAL',this.w_CLSERIAL)
        INSERT INTO (i_cTable);
              (CLSERIAL,CLDESCRI,CLVALINI,CLVALFIN,CLPRIORI,CLBASSEL,CLBASMAG,CLTIPUMI,CLAGGLIS,CLAGGCAO,CLAGGDIN,CLAGGDFI,CLAGGESI,CLAGGSCO,CLAGGSC1,CLAGGSC2,CLAGGSC3,CLAGGSC4,CLBASLIS,CLCAORIF,CLBASESE,CLBASINV,CLCAONAZ,CLNUMELA,CLULTORA,CLULTMIN,CLCALSEL,CLRICAR1,CLMOLTIP,CLMOLTI2,CLRICVAL,CLARROT1,CLVALOR1,CLARROT2,CLVALOR2,CLARROT3,CLVALOR3,CLARROT4,CLFLDASI,CLDATRIF,CLORISCA &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CLSERIAL;
                  ,this.w_CLDESCRI;
                  ,this.w_CLVALINI;
                  ,this.w_CLVALFIN;
                  ,this.w_CLPRIORI;
                  ,this.w_CLBASSEL;
                  ,this.w_CLBASMAG;
                  ,this.w_CLTIPUMI;
                  ,this.w_CLAGGLIS;
                  ,this.w_CLAGGCAO;
                  ,this.w_CLAGGDIN;
                  ,this.w_CLAGGDFI;
                  ,this.w_CLAGGESI;
                  ,this.w_CLAGGSCO;
                  ,this.w_CLAGGSC1;
                  ,this.w_CLAGGSC2;
                  ,this.w_CLAGGSC3;
                  ,this.w_CLAGGSC4;
                  ,this.w_CLBASLIS;
                  ,this.w_CLCAORIF;
                  ,this.w_CLBASESE;
                  ,this.w_CLBASINV;
                  ,this.w_CLCAONAZ;
                  ,this.w_CLNUMELA;
                  ,this.w_CLULTORA;
                  ,this.w_CLULTMIN;
                  ,this.w_CLCALSEL;
                  ,this.w_CLRICAR1;
                  ,this.w_CLMOLTIP;
                  ,this.w_CLMOLTI2;
                  ,this.w_CLRICVAL;
                  ,this.w_CLARROT1;
                  ,this.w_CLVALOR1;
                  ,this.w_CLARROT2;
                  ,this.w_CLVALOR2;
                  ,this.w_CLARROT3;
                  ,this.w_CLVALOR3;
                  ,this.w_CLARROT4;
                  ,this.w_CLFLDASI;
                  ,this.w_CLDATRIF;
                  ,this.w_CLORISCA;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CLLSDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSDETT_IDX,2])
      *
      * insert into CLLSDETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CLSERIAL,CLGRUMER,CLCODART,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CLSERIAL)+","+cp_ToStrODBCNull(this.w_CLGRUMER)+","+cp_ToStrODBCNull(this.w_CLCODART)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CLSERIAL',this.w_CLSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CLSERIAL,this.w_CLGRUMER,this.w_CLCODART,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CLLSMAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CLLSMAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CLDESCRI="+cp_ToStrODBC(this.w_CLDESCRI)+;
             ",CLVALINI="+cp_ToStrODBC(this.w_CLVALINI)+;
             ",CLVALFIN="+cp_ToStrODBC(this.w_CLVALFIN)+;
             ",CLPRIORI="+cp_ToStrODBC(this.w_CLPRIORI)+;
             ",CLBASSEL="+cp_ToStrODBC(this.w_CLBASSEL)+;
             ",CLBASMAG="+cp_ToStrODBCNull(this.w_CLBASMAG)+;
             ",CLTIPUMI="+cp_ToStrODBC(this.w_CLTIPUMI)+;
             ",CLAGGLIS="+cp_ToStrODBCNull(this.w_CLAGGLIS)+;
             ",CLAGGCAO="+cp_ToStrODBC(this.w_CLAGGCAO)+;
             ",CLAGGDIN="+cp_ToStrODBC(this.w_CLAGGDIN)+;
             ",CLAGGDFI="+cp_ToStrODBC(this.w_CLAGGDFI)+;
             ",CLAGGESI="+cp_ToStrODBC(this.w_CLAGGESI)+;
             ",CLAGGSCO="+cp_ToStrODBC(this.w_CLAGGSCO)+;
             ",CLAGGSC1="+cp_ToStrODBC(this.w_CLAGGSC1)+;
             ",CLAGGSC2="+cp_ToStrODBC(this.w_CLAGGSC2)+;
             ",CLAGGSC3="+cp_ToStrODBC(this.w_CLAGGSC3)+;
             ",CLAGGSC4="+cp_ToStrODBC(this.w_CLAGGSC4)+;
             ",CLBASLIS="+cp_ToStrODBCNull(this.w_CLBASLIS)+;
             ",CLCAORIF="+cp_ToStrODBC(this.w_CLCAORIF)+;
             ",CLBASESE="+cp_ToStrODBCNull(this.w_CLBASESE)+;
             ",CLBASINV="+cp_ToStrODBCNull(this.w_CLBASINV)+;
             ",CLCAONAZ="+cp_ToStrODBC(this.w_CLCAONAZ)+;
             ",CLNUMELA="+cp_ToStrODBC(this.w_CLNUMELA)+;
             ",CLULTORA="+cp_ToStrODBC(this.w_CLULTORA)+;
             ",CLULTMIN="+cp_ToStrODBC(this.w_CLULTMIN)+;
             ",CLCALSEL="+cp_ToStrODBC(this.w_CLCALSEL)+;
             ",CLRICAR1="+cp_ToStrODBC(this.w_CLRICAR1)+;
             ",CLMOLTIP="+cp_ToStrODBC(this.w_CLMOLTIP)+;
             ",CLMOLTI2="+cp_ToStrODBC(this.w_CLMOLTI2)+;
             ",CLRICVAL="+cp_ToStrODBC(this.w_CLRICVAL)+;
             ",CLARROT1="+cp_ToStrODBC(this.w_CLARROT1)+;
             ",CLVALOR1="+cp_ToStrODBC(this.w_CLVALOR1)+;
             ",CLARROT2="+cp_ToStrODBC(this.w_CLARROT2)+;
             ",CLVALOR2="+cp_ToStrODBC(this.w_CLVALOR2)+;
             ",CLARROT3="+cp_ToStrODBC(this.w_CLARROT3)+;
             ",CLVALOR3="+cp_ToStrODBC(this.w_CLVALOR3)+;
             ",CLARROT4="+cp_ToStrODBC(this.w_CLARROT4)+;
             ",CLFLDASI="+cp_ToStrODBC(this.w_CLFLDASI)+;
             ",CLDATRIF="+cp_ToStrODBC(this.w_CLDATRIF)+;
             ",CLORISCA="+cp_ToStrODBC(this.w_CLORISCA)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CLLSMAST')
          i_cWhere = cp_PKFox(i_cTable  ,'CLSERIAL',this.w_CLSERIAL  )
          UPDATE (i_cTable) SET;
              CLDESCRI=this.w_CLDESCRI;
             ,CLVALINI=this.w_CLVALINI;
             ,CLVALFIN=this.w_CLVALFIN;
             ,CLPRIORI=this.w_CLPRIORI;
             ,CLBASSEL=this.w_CLBASSEL;
             ,CLBASMAG=this.w_CLBASMAG;
             ,CLTIPUMI=this.w_CLTIPUMI;
             ,CLAGGLIS=this.w_CLAGGLIS;
             ,CLAGGCAO=this.w_CLAGGCAO;
             ,CLAGGDIN=this.w_CLAGGDIN;
             ,CLAGGDFI=this.w_CLAGGDFI;
             ,CLAGGESI=this.w_CLAGGESI;
             ,CLAGGSCO=this.w_CLAGGSCO;
             ,CLAGGSC1=this.w_CLAGGSC1;
             ,CLAGGSC2=this.w_CLAGGSC2;
             ,CLAGGSC3=this.w_CLAGGSC3;
             ,CLAGGSC4=this.w_CLAGGSC4;
             ,CLBASLIS=this.w_CLBASLIS;
             ,CLCAORIF=this.w_CLCAORIF;
             ,CLBASESE=this.w_CLBASESE;
             ,CLBASINV=this.w_CLBASINV;
             ,CLCAONAZ=this.w_CLCAONAZ;
             ,CLNUMELA=this.w_CLNUMELA;
             ,CLULTORA=this.w_CLULTORA;
             ,CLULTMIN=this.w_CLULTMIN;
             ,CLCALSEL=this.w_CLCALSEL;
             ,CLRICAR1=this.w_CLRICAR1;
             ,CLMOLTIP=this.w_CLMOLTIP;
             ,CLMOLTI2=this.w_CLMOLTI2;
             ,CLRICVAL=this.w_CLRICVAL;
             ,CLARROT1=this.w_CLARROT1;
             ,CLVALOR1=this.w_CLVALOR1;
             ,CLARROT2=this.w_CLARROT2;
             ,CLVALOR2=this.w_CLVALOR2;
             ,CLARROT3=this.w_CLARROT3;
             ,CLVALOR3=this.w_CLVALOR3;
             ,CLARROT4=this.w_CLARROT4;
             ,CLFLDASI=this.w_CLFLDASI;
             ,CLDATRIF=this.w_CLDATRIF;
             ,CLORISCA=this.w_CLORISCA;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CLCODART)  or NOT EMPTY(t_CLGRUMER) ) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CLLSDETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CLLSDETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CLLSDETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CLLSDETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CLGRUMER="+cp_ToStrODBCNull(this.w_CLGRUMER)+;
                     ",CLCODART="+cp_ToStrODBCNull(this.w_CLCODART)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CLGRUMER=this.w_CLGRUMER;
                     ,CLCODART=this.w_CLCODART;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CLCODART)  or NOT EMPTY(t_CLGRUMER) ) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CLLSDETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CLLSDETT_IDX,2])
        *
        * delete CLLSDETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
        *
        * delete CLLSMAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CLCODART)  or NOT EMPTY(t_CLGRUMER) ) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CLLSMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLLSMAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CLCODART<>.w_CLCODART
          .w_CLGRUMER = SPACE(5)
          .link_2_1('Full')
        endif
        if .o_CLGRUMER<>.w_CLGRUMER
          .w_CLCODART = SPACE(20)
          .link_2_2('Full')
        endif
        .DoRTCalc(7,9,.t.)
        if .o_CLSERIAL<>.w_CLSERIAL
          .w_CLBASSEL = 'LI'
        endif
        .DoRTCalc(11,11,.t.)
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLBASMAG = IIF(LEFT(.w_CLBASSEL,1)='U',.w_CLBASMAG, SPACE(5))
          .link_1_16('Full')
        endif
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLTIPUMI = IIF(LEFT(.w_CLBASSEL,1)='L' AND NOT EMPTY(.w_CLTIPUMI), .w_CLTIPUMI, 'P')
        endif
          .link_1_18('Full')
        .DoRTCalc(15,15,.t.)
        if .o_VALUTA<>.w_VALUTA
          .w_CLAGGCAO = GETCAM(.w_VALUTA, IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('  -  -  '))), .w_DATACAM, .w_CLDATRIF), 7)
        endif
        .DoRTCalc(17,25,.t.)
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLBASLIS = IIF(LEFT(.w_CLBASSEL,1)='L',.w_CLBASLIS, SPACE(5))
          .link_1_36('Full')
        endif
        .DoRTCalc(27,27,.t.)
          .link_1_38('Full')
        if .o_VALRIF<>.w_VALRIF
          .w_CLCAORIF = GETCAM(.w_VALRIF, .w_CLDATRIF, 7)
        endif
        .DoRTCalc(30,30,.t.)
          .w_CAOVAL = GETCAM(.w_VALUTA, I_DATSYS)
        .DoRTCalc(32,33,.t.)
        if .o_AZIENDA<>.w_AZIENDA.or. .o_CLBASSEL<>.w_CLBASSEL
          .w_CLBASESE = IIF(LEFT(.w_CLBASSEL,1)='I',.w_CLBASESE, SPACE(4))
          .link_1_45('Full')
        endif
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLBASINV = IIF(LEFT(.w_CLBASSEL,1)='I',.w_CLBASINV, SPACE(6))
          .link_1_48('Full')
        endif
        .DoRTCalc(36,36,.t.)
          .link_1_51('Full')
        if .o_VALNAZ<>.w_VALNAZ
          .w_CLCAONAZ = GETCAM(.w_VALNAZ, IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('  -  -  '))), .w_DATACAM, .w_CLDATRIF), 7)
        endif
        .DoRTCalc(39,43,.t.)
        if .o_VALUTA<>.w_VALUTA
          .w_VALPIC = .w_VALUTA
          .link_4_1('Full')
        endif
        .DoRTCalc(45,45,.t.)
        if .o_VALPIC<>.w_VALPIC
          .w_CALCPICT = DEFPCUNI(.w_DECTOP)
        endif
          .w_VALOFIN = MAX(.w_CLVALOR1, .w_CLVALOR2, .w_CLVALOR3)
        .DoRTCalc(48,48,.t.)
        if .o_CLCALSEL<>.w_CLCALSEL
          .w_CLRICAR1 = IIF(.w_CLCALSEL='R',.w_CLRICAR1,0)
        endif
        if .o_CLCALSEL<>.w_CLCALSEL
          .w_CLMOLTIP = IIF(.w_CLCALSEL='M',.w_CLMOLTIP,0)
        endif
        if .o_CLCALSEL<>.w_CLCALSEL
          .w_CLMOLTI2 = IIF(.w_CLCALSEL='M',.w_CLMOLTI2,0)
        endif
        if .o_CLCALSEL<>.w_CLCALSEL
          .w_CLRICVAL = IIF(.w_CLCALSEL$'RM',.w_CLRICVAL,0)
        endif
        .DoRTCalc(53,59,.t.)
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLFLDASI = IIF(LEFT(.w_CLBASSEL,1)='L',.w_CLFLDASI, SPACE(1))
        endif
        if .o_CLVALFIN<>.w_CLVALFIN
          .w_OBTEST = .w_CLVALFIN
        endif
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLDATRIF = IIF(LEFT(.w_CLBASSEL,1)='L',IIF(EMPTY(NVL(.w_CLDATRIF, CP_CHARTODATE('    /  /  '))), .w_DATACAM,.w_CLDATRIF), CP_CHARTODATE('    /  /  ') )
        endif
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .DoRTCalc(63,63,.t.)
        if .o_CLBASSEL<>.w_CLBASSEL
          .w_CLORISCA = IIF(LEFT(.w_CLBASSEL,1)='L', 'S', ' ')
        endif
        .DoRTCalc(65,65,.t.)
        if .o_VALNAZ<>.w_VALNAZ
          .w_CAONAZ2 = GETCAM(.w_VALNAZ, i_DatSys)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_Codazi<>.w_Codazi
           cp_AskTableProg(this,i_nConn,"SCLLS","i_Codazi,w_CLSERIAL")
          .op_CLSERIAL = .w_CLSERIAL
        endif
        .op_Codazi = .w_Codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OBSART with this.w_OBSART
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCLSERIAL_1_3.enabled = this.oPgFrm.Page1.oPag.oCLSERIAL_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.enabled = this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.enabled = this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.enabled = this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.enabled = this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.enabled = this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.mCond()
    this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.enabled = this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.enabled = this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.mCond()
    this.oPgFrm.Page2.oPag.oCLRICAR1_4_12.enabled = this.oPgFrm.Page2.oPag.oCLRICAR1_4_12.mCond()
    this.oPgFrm.Page2.oPag.oCLMOLTIP_4_15.enabled = this.oPgFrm.Page2.oPag.oCLMOLTIP_4_15.mCond()
    this.oPgFrm.Page2.oPag.oCLMOLTI2_4_16.enabled = this.oPgFrm.Page2.oPag.oCLMOLTI2_4_16.mCond()
    this.oPgFrm.Page2.oPag.oCLRICVAL_4_17.enabled = this.oPgFrm.Page2.oPag.oCLRICVAL_4_17.mCond()
    this.oPgFrm.Page2.oPag.oCLARROT2_4_21.enabled = this.oPgFrm.Page2.oPag.oCLARROT2_4_21.mCond()
    this.oPgFrm.Page2.oPag.oCLVALOR2_4_22.enabled = this.oPgFrm.Page2.oPag.oCLVALOR2_4_22.mCond()
    this.oPgFrm.Page2.oPag.oCLARROT3_4_23.enabled = this.oPgFrm.Page2.oPag.oCLARROT3_4_23.mCond()
    this.oPgFrm.Page2.oPag.oCLVALOR3_4_24.enabled = this.oPgFrm.Page2.oPag.oCLVALOR3_4_24.mCond()
    this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.enabled = this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.mCond()
    this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.enabled = this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCODART_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCLCODART_2_2.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.visible=!this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCLTIPUMI_1_17.visible=!this.oPgFrm.Page1.oPag.oCLTIPUMI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGCAO_1_20.visible=!this.oPgFrm.Page1.oPag.oCLAGGCAO_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.visible=!this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.visible=!this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.visible=!this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.visible=!this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.visible=!this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCLBASLIS_1_36.visible=!this.oPgFrm.Page1.oPag.oCLBASLIS_1_36.mHide()
    this.oPgFrm.Page1.oPag.oLIDESCR2_1_37.visible=!this.oPgFrm.Page1.oPag.oLIDESCR2_1_37.mHide()
    this.oPgFrm.Page1.oPag.oVALRIF_1_38.visible=!this.oPgFrm.Page1.oPag.oVALRIF_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.visible=!this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oCLBASESE_1_45.visible=!this.oPgFrm.Page1.oPag.oCLBASESE_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oCLBASINV_1_48.visible=!this.oPgFrm.Page1.oPag.oCLBASINV_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDATINV_1_49.visible=!this.oPgFrm.Page1.oPag.oDATINV_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oVALNAZ_1_51.visible=!this.oPgFrm.Page1.oPag.oVALNAZ_1_51.mHide()
    this.oPgFrm.Page1.oPag.oCLCAONAZ_1_52.visible=!this.oPgFrm.Page1.oPag.oCLCAONAZ_1_52.mHide()
    this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.visible=!this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.mHide()
    this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.visible=!this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.mHide()
    this.oPgFrm.Page1.oPag.oCLORISCA_1_65.visible=!this.oPgFrm.Page1.oPag.oCLORISCA_1_65.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsva_msl
    This.oPgFrm.Pages[2].opag.uienable(.T.)
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CLGRUMER
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CLGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CLGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCLGRUMER_2_1'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CLGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CLGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.GMCODICE as GMCODICE201"+ ","+cp_TransLinkFldName('link_2_1.GMDESCRI')+" as GMDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CLLSDETT.CLGRUMER=link_2_1.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CLLSDETT.CLGRUMER=link_2_1.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLCODART
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CLCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CLCODART))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCLCODART_2_2'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CLCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CLCODART)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_OBSART = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_OBSART = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.ARCODART as ARCODART202"+ ",link_2_2.ARDESART as ARDESART202"+ ",link_2_2.ARDTOBSO as ARDTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on CLLSDETT.CLCODART=link_2_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and CLLSDETT.CLCODART=link_2_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLBASMAG
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLBASMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CLBASMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CLBASMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLBASMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLBASMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCLBASMAG_1_16'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLBASMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CLBASMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CLBASMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLBASMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CLBASMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLBASMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(10))
      this.w_DECTOT = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(10)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAGGLIS
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAGGLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CLAGGLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CLAGGLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAGGLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_CLAGGLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_CLAGGLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLAGGLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCLAGGLIS_1_19'),i_cWhere,'GSAR_ALI',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAGGLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CLAGGLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CLAGGLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAGGLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_LIDESCRI = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(10))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLAGGLIS = space(5)
      endif
      this.w_LIDESCRI = space(40)
      this.w_VALUTA = space(10)
      this.w_FLSCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAGGLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.LSCODLIS as LSCODLIS119"+ ",link_1_19.LSDESLIS as LSDESLIS119"+ ",link_1_19.LSVALLIS as LSVALLIS119"+ ",link_1_19.LSFLSCON as LSFLSCON119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on CLLSMAST.CLAGGLIS=link_1_19.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and CLLSMAST.CLAGGLIS=link_1_19.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CLBASLIS
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLBASLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CLBASLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CLBASLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLBASLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLBASLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCLBASLIS_1_36'),i_cWhere,'GSAR_ALI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLBASLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CLBASLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CLBASLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLBASLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_LIDESCR2 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALRIF = NVL(_Link_.LSVALLIS,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CLBASLIS = space(5)
      endif
      this.w_LIDESCR2 = space(40)
      this.w_VALRIF = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLBASLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.LSCODLIS as LSCODLIS136"+ ",link_1_36.LSDESLIS as LSDESLIS136"+ ",link_1_36.LSVALLIS as LSVALLIS136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on CLLSMAST.CLBASLIS=link_1_36.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and CLLSMAST.CLBASLIS=link_1_36.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALRIF
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALRIF)
            select VACODVAL,VADECUNI,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALRIF = NVL(_Link_.VACODVAL,space(10))
      this.w_DECTOT2 = NVL(_Link_.VADECUNI,0)
      this.w_CAOVAL2 = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALRIF = space(10)
      endif
      this.w_DECTOT2 = 0
      this.w_CAOVAL2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLBASESE
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLBASESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CLBASESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CLBASESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLBASESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLBASESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCLBASESE_1_45'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLBASESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CLBASESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CLBASESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLBASESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CLBASESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLBASESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLBASINV
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLBASINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_CLBASINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_CLBASESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_CLBASESE;
                     ,'INNUMINV',trim(this.w_CLBASINV))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLBASINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLBASINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oCLBASINV_1_48'),i_cWhere,'gsma_ain',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLBASESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_CLBASESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLBASINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_CLBASINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_CLBASESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_CLBASESE;
                       ,'INNUMINV',this.w_CLBASINV)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLBASINV = NVL(_Link_.INNUMINV,space(6))
      this.w_DATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLBASINV = space(6)
      endif
      this.w_DATINV = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLBASINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALPIC
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPIC)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPIC = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALPIC = space(3)
      endif
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCLSERIAL_1_3.value==this.w_CLSERIAL)
      this.oPgFrm.Page1.oPag.oCLSERIAL_1_3.value=this.w_CLSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDESCRI_1_4.value==this.w_CLDESCRI)
      this.oPgFrm.Page1.oPag.oCLDESCRI_1_4.value=this.w_CLDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLVALINI_1_9.value==this.w_CLVALINI)
      this.oPgFrm.Page1.oPag.oCLVALINI_1_9.value=this.w_CLVALINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLVALFIN_1_11.value==this.w_CLVALFIN)
      this.oPgFrm.Page1.oPag.oCLVALFIN_1_11.value=this.w_CLVALFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLPRIORI_1_12.value==this.w_CLPRIORI)
      this.oPgFrm.Page1.oPag.oCLPRIORI_1_12.value=this.w_CLPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLBASSEL_1_14.RadioValue()==this.w_CLBASSEL)
      this.oPgFrm.Page1.oPag.oCLBASSEL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESCRI_1_15.value==this.w_LIDESCRI)
      this.oPgFrm.Page1.oPag.oLIDESCRI_1_15.value=this.w_LIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.value==this.w_CLBASMAG)
      this.oPgFrm.Page1.oPag.oCLBASMAG_1_16.value=this.w_CLBASMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCLTIPUMI_1_17.RadioValue()==this.w_CLTIPUMI)
      this.oPgFrm.Page1.oPag.oCLTIPUMI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_18.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_18.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGLIS_1_19.value==this.w_CLAGGLIS)
      this.oPgFrm.Page1.oPag.oCLAGGLIS_1_19.value=this.w_CLAGGLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGCAO_1_20.value==this.w_CLAGGCAO)
      this.oPgFrm.Page1.oPag.oCLAGGCAO_1_20.value=this.w_CLAGGCAO
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGDIN_1_21.value==this.w_CLAGGDIN)
      this.oPgFrm.Page1.oPag.oCLAGGDIN_1_21.value=this.w_CLAGGDIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGDFI_1_22.value==this.w_CLAGGDFI)
      this.oPgFrm.Page1.oPag.oCLAGGDFI_1_22.value=this.w_CLAGGDFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGESI_1_25.RadioValue()==this.w_CLAGGESI)
      this.oPgFrm.Page1.oPag.oCLAGGESI_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.RadioValue()==this.w_CLAGGSCO)
      this.oPgFrm.Page1.oPag.oCLAGGSCO_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.value==this.w_CLAGGSC1)
      this.oPgFrm.Page1.oPag.oCLAGGSC1_1_27.value=this.w_CLAGGSC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.value==this.w_CLAGGSC2)
      this.oPgFrm.Page1.oPag.oCLAGGSC2_1_28.value=this.w_CLAGGSC2
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.value==this.w_CLAGGSC3)
      this.oPgFrm.Page1.oPag.oCLAGGSC3_1_29.value=this.w_CLAGGSC3
    endif
    if not(this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.value==this.w_CLAGGSC4)
      this.oPgFrm.Page1.oPag.oCLAGGSC4_1_31.value=this.w_CLAGGSC4
    endif
    if not(this.oPgFrm.Page1.oPag.oCLBASLIS_1_36.value==this.w_CLBASLIS)
      this.oPgFrm.Page1.oPag.oCLBASLIS_1_36.value=this.w_CLBASLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESCR2_1_37.value==this.w_LIDESCR2)
      this.oPgFrm.Page1.oPag.oLIDESCR2_1_37.value=this.w_LIDESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALRIF_1_38.value==this.w_VALRIF)
      this.oPgFrm.Page1.oPag.oVALRIF_1_38.value=this.w_VALRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.value==this.w_CLCAORIF)
      this.oPgFrm.Page1.oPag.oCLCAORIF_1_39.value=this.w_CLCAORIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCLBASESE_1_45.value==this.w_CLBASESE)
      this.oPgFrm.Page1.oPag.oCLBASESE_1_45.value=this.w_CLBASESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCLBASINV_1_48.value==this.w_CLBASINV)
      this.oPgFrm.Page1.oPag.oCLBASINV_1_48.value=this.w_CLBASINV
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINV_1_49.value==this.w_DATINV)
      this.oPgFrm.Page1.oPag.oDATINV_1_49.value=this.w_DATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_51.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_51.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCLCAONAZ_1_52.value==this.w_CLCAONAZ)
      this.oPgFrm.Page1.oPag.oCLCAONAZ_1_52.value=this.w_CLCAONAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oVALOFIN_4_7.value==this.w_VALOFIN)
      this.oPgFrm.Page2.oPag.oVALOFIN_4_7.value=this.w_VALOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCLCALSEL_4_10.RadioValue()==this.w_CLCALSEL)
      this.oPgFrm.Page2.oPag.oCLCALSEL_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLRICAR1_4_12.value==this.w_CLRICAR1)
      this.oPgFrm.Page2.oPag.oCLRICAR1_4_12.value=this.w_CLRICAR1
    endif
    if not(this.oPgFrm.Page2.oPag.oCLMOLTIP_4_15.value==this.w_CLMOLTIP)
      this.oPgFrm.Page2.oPag.oCLMOLTIP_4_15.value=this.w_CLMOLTIP
    endif
    if not(this.oPgFrm.Page2.oPag.oCLMOLTI2_4_16.value==this.w_CLMOLTI2)
      this.oPgFrm.Page2.oPag.oCLMOLTI2_4_16.value=this.w_CLMOLTI2
    endif
    if not(this.oPgFrm.Page2.oPag.oCLRICVAL_4_17.value==this.w_CLRICVAL)
      this.oPgFrm.Page2.oPag.oCLRICVAL_4_17.value=this.w_CLRICVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARROT1_4_19.value==this.w_CLARROT1)
      this.oPgFrm.Page2.oPag.oCLARROT1_4_19.value=this.w_CLARROT1
    endif
    if not(this.oPgFrm.Page2.oPag.oCLVALOR1_4_20.value==this.w_CLVALOR1)
      this.oPgFrm.Page2.oPag.oCLVALOR1_4_20.value=this.w_CLVALOR1
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARROT2_4_21.value==this.w_CLARROT2)
      this.oPgFrm.Page2.oPag.oCLARROT2_4_21.value=this.w_CLARROT2
    endif
    if not(this.oPgFrm.Page2.oPag.oCLVALOR2_4_22.value==this.w_CLVALOR2)
      this.oPgFrm.Page2.oPag.oCLVALOR2_4_22.value=this.w_CLVALOR2
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARROT3_4_23.value==this.w_CLARROT3)
      this.oPgFrm.Page2.oPag.oCLARROT3_4_23.value=this.w_CLARROT3
    endif
    if not(this.oPgFrm.Page2.oPag.oCLVALOR3_4_24.value==this.w_CLVALOR3)
      this.oPgFrm.Page2.oPag.oCLVALOR3_4_24.value=this.w_CLVALOR3
    endif
    if not(this.oPgFrm.Page2.oPag.oCLARROT4_4_25.value==this.w_CLARROT4)
      this.oPgFrm.Page2.oPag.oCLARROT4_4_25.value=this.w_CLARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.RadioValue()==this.w_CLFLDASI)
      this.oPgFrm.Page1.oPag.oCLFLDASI_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.value==this.w_CLDATRIF)
      this.oPgFrm.Page1.oPag.oCLDATRIF_1_59.value=this.w_CLDATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCLORISCA_1_65.RadioValue()==this.w_CLORISCA)
      this.oPgFrm.Page1.oPag.oCLORISCA_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLGRUMER_2_1.value==this.w_CLGRUMER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLGRUMER_2_1.value=this.w_CLGRUMER
      replace t_CLGRUMER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLGRUMER_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODART_2_2.value==this.w_CLCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODART_2_2.value=this.w_CLCODART
      replace t_CLCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCLCODART_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_3.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_3.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESGRU_2_4.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESGRU_2_4.value=this.w_DESGRU
      replace t_DESGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESGRU_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'CLLSMAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CLVALINI) or not(((empty(.w_CLVALFIN)) OR  (.w_CLVALINI<=.w_CLVALFIN))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLVALINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CLVALINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   (empty(.w_CLVALFIN) or not(((.w_CLVALFIN>=.w_CLVALINI) or (empty(.w_CLVALINI)))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLVALFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CLVALFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   (empty(.w_CLBASMAG))  and not(LEFT(.w_CLBASSEL,1)<>'U')  and (LEFT(.w_CLBASSEL,1)='U')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLBASMAG_1_16.SetFocus()
            i_bnoObbl = !empty(.w_CLBASMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLAGGLIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLAGGLIS_1_19.SetFocus()
            i_bnoObbl = !empty(.w_CLAGGLIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLAGGCAO))  and not(EMPTY(.w_VALUTA) OR .w_CAOVAL<>0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLAGGCAO_1_20.SetFocus()
            i_bnoObbl = !empty(.w_CLAGGCAO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLAGGDIN) or not(((empty(.w_CLAGGDFI)) OR  (.w_CLAGGDIN<=.w_CLAGGDFI))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLAGGDIN_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CLAGGDIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   (empty(.w_CLAGGDFI) or not(((.w_CLAGGDFI>=.w_CLAGGDIN) or (empty(.w_CLAGGDIN)))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLAGGDFI_1_22.SetFocus()
            i_bnoObbl = !empty(.w_CLAGGDFI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   (empty(.w_CLBASLIS))  and not(!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLBASLIS_1_36.SetFocus()
            i_bnoObbl = !empty(.w_CLBASLIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLBASESE))  and not(LEFT(.w_CLBASSEL,1)<>'I')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLBASESE_1_45.SetFocus()
            i_bnoObbl = !empty(.w_CLBASESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLBASINV))  and not(LEFT(.w_CLBASSEL,1)<>'I')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLBASINV_1_48.SetFocus()
            i_bnoObbl = !empty(.w_CLBASINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CLVALOR2>.w_CLVALOR1 OR .w_CLVALOR2=0)  and (.w_CLVALOR1<>0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCLVALOR2_4_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CLVALOR3>.w_CLVALOR2 OR  .w_CLVALOR3=0)  and (.w_CLVALOR1<>0 AND .w_CLVALOR2<>0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCLVALOR3_4_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CLDATRIF))  and not(!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))  and (.w_CLBASSEL='LI' OR .w_CLBASSEL='LN')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCLDATRIF_1_59.SetFocus()
            i_bnoObbl = !empty(.w_CLDATRIF)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsva_msl
      this.NotifyEvent('CheckRighe')
      If i_bRes And this.w_NORIGHE
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Impossibile salvare la scheda senza righe di dettaglio")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_CLCODART)  or NOT EMPTY(.w_CLGRUMER) 
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZIENDA = this.w_AZIENDA
    this.o_CLSERIAL = this.w_CLSERIAL
    this.o_CLGRUMER = this.w_CLGRUMER
    this.o_CLCODART = this.w_CLCODART
    this.o_CLVALFIN = this.w_CLVALFIN
    this.o_CLBASSEL = this.w_CLBASSEL
    this.o_VALUTA = this.w_VALUTA
    this.o_VALRIF = this.w_VALRIF
    this.o_VALNAZ = this.w_VALNAZ
    this.o_VALPIC = this.w_VALPIC
    this.o_CLCALSEL = this.w_CLCALSEL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CLCODART)  or NOT EMPTY(t_CLGRUMER) )
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CLGRUMER=space(5)
      .w_CLCODART=space(20)
      .w_DESART=space(40)
      .w_DESGRU=space(35)
      .w_OBSART=ctod("  /  /  ")
      .DoRTCalc(1,4,.f.)
        .w_CLGRUMER = SPACE(5)
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_CLGRUMER))
        .link_2_1('Full')
      endif
        .w_CLCODART = SPACE(20)
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_CLCODART))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(7,66,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CLGRUMER = t_CLGRUMER
    this.w_CLCODART = t_CLCODART
    this.w_DESART = t_DESART
    this.w_DESGRU = t_DESGRU
    this.w_OBSART = t_OBSART
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CLGRUMER with this.w_CLGRUMER
    replace t_CLCODART with this.w_CLCODART
    replace t_DESART with this.w_DESART
    replace t_DESGRU with this.w_DESGRU
    replace t_OBSART with this.w_OBSART
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsva_mslPag1 as StdContainer
  Width  = 778
  height = 435
  stdWidth  = 778
  stdheight = 435
  resizeXpos=435
  resizeYpos=371
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLSERIAL_1_3 as StdField with uid="VNYHEBXAVE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CLSERIAL", cQueryName = "CLSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 123728270,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=154, Top=7, cSayPict="'9999999999'", cGetPict="'9999999999'", InputMask=replicate('X',10)

  func oCLSERIAL_1_3.mCond()
    with this.Parent.oContained
      return (.F.)
    endwith
  endfunc

  add object oCLDESCRI_1_4 as StdField with uid="EBHEVGXNHW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CLDESCRI", cQueryName = "CLDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione scheda",;
    HelpContextID = 223404433,;
   bGlobalFont=.t.,;
    Height=21, Width=527, Left=243, Top=7, InputMask=replicate('X',50)

  add object oCLVALINI_1_9 as StdField with uid="IXHXBCFDGL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CLVALINI", cQueryName = "CLVALINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data inizio validit� scheda",;
    HelpContextID = 138165871,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=154, Top=35

  func oCLVALINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_CLVALFIN)) OR  (.w_CLVALINI<=.w_CLVALFIN)))
    endwith
    return bRes
  endfunc

  add object oCLVALFIN_1_11 as StdField with uid="XOANNTAKZM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CLVALFIN", cQueryName = "CLVALFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data fine validit� scheda",;
    HelpContextID = 87834228,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=517, Top=35

  func oCLVALFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_CLVALFIN>=.w_CLVALINI) or (empty(.w_CLVALINI))))
    endwith
    return bRes
  endfunc

  add object oCLPRIORI_1_12 as StdField with uid="LQUNZZGRZM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CLPRIORI", cQueryName = "CLPRIORI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ordine di elaborazione della scheda di calcolo",;
    HelpContextID = 31662481,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=736, Top=35, cSayPict='"999"', cGetPict='"999"'


  add object oCLBASSEL_1_14 as StdCombo with uid="NXXGHSDBAM",rtseq=10,rtrep=.f.,left=154,top=61,width=263,height=21;
    , ToolTipText = "Criterio di aggiornamento";
    , HelpContextID = 223674766;
    , cFormVar="w_CLBASSEL",RowSource=""+"Prezzo di listino,"+"Prezzo di listino al netto degli sconti,"+"Ultimo costo,"+"Costo medio ponderato annuo,"+"Costo medio ponderato periodo,"+"LIFO continuo,"+"LIFO scatti,"+"FIFO,"+"Costo standard,"+"Ultimo costo dei saldi,"+"Ultimo prezzo dei saldi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLBASSEL_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLBASSEL,&i_cF..t_CLBASSEL),this.value)
    return(iif(xVal =1,'LI',;
    iif(xVal =2,'LN',;
    iif(xVal =3,'IU',;
    iif(xVal =4,'IA',;
    iif(xVal =5,'IP',;
    iif(xVal =6,'IL',;
    iif(xVal =7,'IS',;
    iif(xVal =8,'IF',;
    iif(xVal =9,'CS',;
    iif(xVal =10,'UC',;
    iif(xVal =11,'UP',;
    space(2)))))))))))))
  endfunc
  func oCLBASSEL_1_14.GetRadio()
    this.Parent.oContained.w_CLBASSEL = this.RadioValue()
    return .t.
  endfunc

  func oCLBASSEL_1_14.ToRadio()
    this.Parent.oContained.w_CLBASSEL=trim(this.Parent.oContained.w_CLBASSEL)
    return(;
      iif(this.Parent.oContained.w_CLBASSEL=='LI',1,;
      iif(this.Parent.oContained.w_CLBASSEL=='LN',2,;
      iif(this.Parent.oContained.w_CLBASSEL=='IU',3,;
      iif(this.Parent.oContained.w_CLBASSEL=='IA',4,;
      iif(this.Parent.oContained.w_CLBASSEL=='IP',5,;
      iif(this.Parent.oContained.w_CLBASSEL=='IL',6,;
      iif(this.Parent.oContained.w_CLBASSEL=='IS',7,;
      iif(this.Parent.oContained.w_CLBASSEL=='IF',8,;
      iif(this.Parent.oContained.w_CLBASSEL=='CS',9,;
      iif(this.Parent.oContained.w_CLBASSEL=='UC',10,;
      iif(this.Parent.oContained.w_CLBASSEL=='UP',11,;
      0))))))))))))
  endfunc

  func oCLBASSEL_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLIDESCRI_1_15 as StdField with uid="RXVXYNLWBV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LIDESCRI", cQueryName = "LIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223405057,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=87, Top=107, InputMask=replicate('X',40)

  add object oCLBASMAG_1_16 as StdField with uid="JANDRGCUUY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CLBASMAG", cQueryName = "CLBASMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 55902611,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=519, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CLBASMAG"

  func oCLBASMAG_1_16.mCond()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)='U')
    endwith
  endfunc

  func oCLBASMAG_1_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'U')
    endwith
    endif
  endfunc

  func oCLBASMAG_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLBASMAG_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLBASMAG_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCLBASMAG_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCLBASMAG_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CLBASMAG
    i_obj.ecpSave()
  endproc


  add object oCLTIPUMI_1_17 as StdCombo with uid="SWVMEMHUHQ",rtseq=13,rtrep=.f.,left=519,top=62,width=120,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima o seconda)";
    , HelpContextID = 75767407;
    , cFormVar="w_CLTIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLTIPUMI_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLTIPUMI,&i_cF..t_CLTIPUMI),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oCLTIPUMI_1_17.GetRadio()
    this.Parent.oContained.w_CLTIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oCLTIPUMI_1_17.ToRadio()
    this.Parent.oContained.w_CLTIPUMI=trim(this.Parent.oContained.w_CLTIPUMI)
    return(;
      iif(this.Parent.oContained.w_CLTIPUMI=='P',1,;
      iif(this.Parent.oContained.w_CLTIPUMI=='S',2,;
      0)))
  endfunc

  func oCLTIPUMI_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLTIPUMI_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
    endif
  endfunc

  add object oVALUTA_1_18 as StdField with uid="TPXUMDLBLL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 254831530,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=386, Top=107, InputMask=replicate('X',10), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCLAGGLIS_1_19 as StdField with uid="YYLHSPYBQC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CLAGGLIS", cQueryName = "CLAGGLIS",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Listino da aggiornare",;
    HelpContextID = 183561849,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=10, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CLAGGLIS"

  func oCLAGGLIS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAGGLIS_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAGGLIS_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCLAGGLIS_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'',this.parent.oContained
  endproc
  proc oCLAGGLIS_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CLAGGLIS
    i_obj.ecpSave()
  endproc

  add object oCLAGGCAO_1_20 as StdField with uid="OWXFAZVKCL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CLAGGCAO", cQueryName = "CLAGGCAO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio da applicare se listino non in valuta di conto",;
    HelpContextID = 235868555,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=437, Top=107

  func oCLAGGCAO_1_20.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_VALUTA) OR .w_CAOVAL<>0)
    endwith
    endif
  endfunc

  add object oCLAGGDIN_1_21 as StdField with uid="NAKZXEYMVF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CLAGGDIN", cQueryName = "CLAGGDIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data inizio validit� listino da aggiornare",;
    HelpContextID = 49344116,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=605, Top=107

  func oCLAGGDIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((empty(.w_CLAGGDFI)) OR  (.w_CLAGGDIN<=.w_CLAGGDFI)))
    endwith
    return bRes
  endfunc

  add object oCLAGGDFI_1_22 as StdField with uid="VIQOENQMYH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CLAGGDFI", cQueryName = "CLAGGDFI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data fine validit� listino da aggiornare",;
    HelpContextID = 219091345,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=701, Top=107

  func oCLAGGDFI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_CLAGGDFI>=.w_CLAGGDIN) or (empty(.w_CLAGGDIN))))
    endwith
    return bRes
  endfunc

  add object oCLAGGESI_1_25 as StdCheck with uid="EFTHWJITBZ",rtseq=19,rtrep=.f.,left=10, top=135, caption="Solo esistenti",;
    ToolTipText = "Solo prezzi presenti in listino da aggiornare",;
    HelpContextID = 202314129,;
    cFormVar="w_CLAGGESI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLAGGESI_1_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLAGGESI,&i_cF..t_CLAGGESI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCLAGGESI_1_25.GetRadio()
    this.Parent.oContained.w_CLAGGESI = this.RadioValue()
    return .t.
  endfunc

  func oCLAGGESI_1_25.ToRadio()
    this.Parent.oContained.w_CLAGGESI=trim(this.Parent.oContained.w_CLAGGESI)
    return(;
      iif(this.Parent.oContained.w_CLAGGESI=='S',1,;
      0))
  endfunc

  func oCLAGGESI_1_25.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCLAGGSCO_1_26 as StdCombo with uid="ZMHDNOOEJW",rtseq=20,rtrep=.f.,left=111,top=135,width=143,height=21;
    , ToolTipText = "Copia (C) o forza (F) sconto";
    , HelpContextID = 235868555;
    , cFormVar="w_CLAGGSCO",RowSource=""+"Forza sconti,"+"Copia sconti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLAGGSCO_1_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLAGGSCO,&i_cF..t_CLAGGSCO),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oCLAGGSCO_1_26.GetRadio()
    this.Parent.oContained.w_CLAGGSCO = this.RadioValue()
    return .t.
  endfunc

  func oCLAGGSCO_1_26.ToRadio()
    this.Parent.oContained.w_CLAGGSCO=trim(this.Parent.oContained.w_CLAGGSCO)
    return(;
      iif(this.Parent.oContained.w_CLAGGSCO=='F',1,;
      iif(this.Parent.oContained.w_CLAGGSCO=='C',2,;
      0)))
  endfunc

  func oCLAGGSCO_1_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLAGGSCO_1_26.mCond()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)='L')
    endwith
  endfunc

  func oCLAGGSCO_1_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSCON<>'S')
    endwith
    endif
  endfunc

  add object oCLAGGSC1_1_27 as StdField with uid="UZVAEZUALP",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CLAGGSC1", cQueryName = "CLAGGSC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 235868585,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=542, Top=135, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLAGGSC1_1_27.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>0)
    endwith
  endfunc

  func oCLAGGSC1_1_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLAGGSCO<>'F'  OR .w_FLSCON<>'S')
    endwith
    endif
  endfunc

  add object oCLAGGSC2_1_28 as StdField with uid="KKZZXFLXUB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CLAGGSC2", cQueryName = "CLAGGSC2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 235868584,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=600, Top=135, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLAGGSC2_1_28.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>1)
    endwith
  endfunc

  func oCLAGGSC2_1_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLAGGSCO<>'F'  OR .w_FLSCON<>'S')
    endwith
    endif
  endfunc

  add object oCLAGGSC3_1_29 as StdField with uid="BEKJJFYBWU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CLAGGSC3", cQueryName = "CLAGGSC3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 235868583,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=658, Top=135, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLAGGSC3_1_29.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>2)
    endwith
  endfunc

  func oCLAGGSC3_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLAGGSCO<>'F'  OR .w_FLSCON<>'S')
    endwith
    endif
  endfunc

  add object oCLAGGSC4_1_31 as StdField with uid="MVNKKOFNQU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CLAGGSC4", cQueryName = "CLAGGSC4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 235868582,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=715, Top=135, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLAGGSC4_1_31.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>3)
    endwith
  endfunc

  func oCLAGGSC4_1_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLAGGSCO<>'F'  OR .w_FLSCON<>'S')
    endwith
    endif
  endfunc

  add object oCLBASLIS_1_36 as StdField with uid="QFWFQEJQLH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CLBASLIS", cQueryName = "CLBASLIS",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di listino di riferimento",;
    HelpContextID = 195755641,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=10, Top=188, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CLBASLIS"

  func oCLBASLIS_1_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
    endif
  endfunc

  func oCLBASLIS_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLBASLIS_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLBASLIS_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCLBASLIS_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"",'',this.parent.oContained
  endproc
  proc oCLBASLIS_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CLBASLIS
    i_obj.ecpSave()
  endproc

  add object oLIDESCR2_1_37 as StdField with uid="ZZIKQWFXTQ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_LIDESCR2", cQueryName = "LIDESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223405080,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=87, Top=188, InputMask=replicate('X',40)

  func oLIDESCR2_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'L')
    endwith
    endif
  endfunc

  add object oVALRIF_1_38 as StdField with uid="KQGJHONHBO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_VALRIF", cQueryName = "VALRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 182676394,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=386, Top=188, InputMask=replicate('X',10), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALRIF"

  func oVALRIF_1_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'L')
    endwith
    endif
  endfunc

  func oVALRIF_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCLCAORIF_1_39 as StdField with uid="GNDCYPZCOI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CLCAORIF", cQueryName = "CLCAORIF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio del listino di riferimento",;
    HelpContextID = 23793260,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=437, Top=188

  func oCLCAORIF_1_39.mCond()
    with this.Parent.oContained
      return (.w_CAOVAL2=0)
    endwith
  endfunc

  func oCLCAORIF_1_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_VALRIF) OR .w_CAOVAL2<>0)
    endwith
    endif
  endfunc

  add object oCLBASESE_1_45 as StdField with uid="STRIIMDDWF",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CLBASESE", cQueryName = "CLBASESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio relativo all'inventario da considerare come valorizzazione iniziale",;
    HelpContextID = 190120341,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=87, Top=188, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CLBASESE"

  func oCLBASESE_1_45.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
    endif
  endfunc

  func oCLBASESE_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
      if .not. empty(.w_CLBASINV)
        bRes2=.link_1_48('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCLBASESE_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLBASESE_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCLBASESE_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oCLBASESE_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_CLBASESE
    i_obj.ecpSave()
  endproc

  add object oCLBASINV_1_48 as StdField with uid="MFOWBWWDNQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CLBASINV", cQueryName = "CLBASINV",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'inventario base di calcolo",;
    HelpContextID = 145423996,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=242, Top=188, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_CLBASESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_CLBASINV"

  func oCLBASINV_1_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
    endif
  endfunc

  func oCLBASINV_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLBASINV_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLBASINV_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_CLBASESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_CLBASESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oCLBASINV_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventari",'',this.parent.oContained
  endproc
  proc oCLBASINV_1_48.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_CLBASESE
     i_obj.w_INNUMINV=this.parent.oContained.w_CLBASINV
    i_obj.ecpSave()
  endproc

  add object oDATINV_1_49 as StdField with uid="LODYKYZADK",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DATINV", cQueryName = "DATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti dell'inventario prec.",;
    HelpContextID = 177990858,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=605, Top=188

  func oDATINV_1_49.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
    endif
  endfunc

  add object oVALNAZ_1_51 as StdField with uid="YRWKYNHLNK",rtseq=37,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 124218282,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=386, Top=188, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALNAZ"

  func oVALNAZ_1_51.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
    endif
  endfunc

  func oVALNAZ_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCLCAONAZ_1_52 as StdField with uid="RMKCZQEOTJ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CLCAONAZ", cQueryName = "CLCAONAZ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio dell'inventario di riferimento",;
    HelpContextID = 43315584,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=437, Top=188

  func oCLCAONAZ_1_52.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I' OR .w_CAONAZ2<>0)
    endwith
    endif
  endfunc

  add object oCLFLDASI_1_57 as StdCheck with uid="BUDVJYBYPR",rtseq=60,rtrep=.f.,left=509, top=188, caption="Data di sistema",;
    ToolTipText = "Se attivo, utilizza la data di sistema come data di validit� del listino di riferimento",;
    HelpContextID = 3785105,;
    cFormVar="w_CLFLDASI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLFLDASI_1_57.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLFLDASI,&i_cF..t_CLFLDASI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCLFLDASI_1_57.GetRadio()
    this.Parent.oContained.w_CLFLDASI = this.RadioValue()
    return .t.
  endfunc

  func oCLFLDASI_1_57.ToRadio()
    this.Parent.oContained.w_CLFLDASI=trim(this.Parent.oContained.w_CLFLDASI)
    return(;
      iif(this.Parent.oContained.w_CLFLDASI=='S',1,;
      0))
  endfunc

  func oCLFLDASI_1_57.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLFLDASI_1_57.mCond()
    with this.Parent.oContained
      return (.w_CLBASSEL='LI' OR .w_CLBASSEL='LN')
    endwith
  endfunc

  func oCLFLDASI_1_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
    endif
  endfunc

  add object oCLDATRIF_1_59 as StdField with uid="GRSYOIRVEG",rtseq=62,rtrep=.f.,;
    cFormVar = "w_CLDATRIF", cQueryName = "CLDATRIF",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di selezione del listino di riferimento",;
    HelpContextID = 29040236,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=701, Top=188

  func oCLDATRIF_1_59.mCond()
    with this.Parent.oContained
      return (.w_CLBASSEL='LI' OR .w_CLBASSEL='LN')
    endwith
  endfunc

  func oCLDATRIF_1_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
    endif
  endfunc


  add object oObj_1_61 as cp_runprogram with uid="FBHIKALFKA",left=-4, top=446, width=227,height=23,;
    caption='GSVA_BRP(R)',;
   bGlobalFont=.t.,;
    prg="GSVA_BRP('R')",;
    cEvent = "CheckRighe",;
    nPag=1;
    , HelpContextID = 227595722


  add object oObj_1_62 as cp_runprogram with uid="MBFXUHBLVN",left=-2, top=470, width=226,height=21,;
    caption='GSVA_BRP(N)',;
   bGlobalFont=.t.,;
    prg="GSVA_BRP('N')",;
    cEvent = "w_CLCODART Changed",;
    nPag=1;
    , HelpContextID = 227596746


  add object oObj_1_63 as cp_runprogram with uid="ZKOMECJMYE",left=-4, top=493, width=230,height=19,;
    caption='"GSVA_BRP(N)',;
   bGlobalFont=.t.,;
    prg="GSVA_BRP('D')",;
    cEvent = "w_CLVALFIN Changed",;
    nPag=1;
    , HelpContextID = 36760472

  add object oCLORISCA_1_65 as StdCheck with uid="JYJLMMDVUV",rtseq=64,rtrep=.f.,left=264, top=135, caption="Scaglioni di origine",;
    ToolTipText = "Attivo: cancella gli scaglioni di origine e inserisce quelli dello scaglione di riferimento; disattivo: mantiene gli scaglioni del listino da agg. se esiste, altrimenti viene calcolato uno scaglione",;
    HelpContextID = 232993177,;
    cFormVar="w_CLORISCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLORISCA_1_65.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLORISCA,&i_cF..t_CLORISCA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCLORISCA_1_65.GetRadio()
    this.Parent.oContained.w_CLORISCA = this.RadioValue()
    return .t.
  endfunc

  func oCLORISCA_1_65.ToRadio()
    this.Parent.oContained.w_CLORISCA=trim(this.Parent.oContained.w_CLORISCA)
    return(;
      iif(this.Parent.oContained.w_CLORISCA=='S',1,;
      0))
  endfunc

  func oCLORISCA_1_65.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCLORISCA_1_65.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'L')
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=220, width=759,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CLGRUMER",Label1="Gr. merc.",Field2="DESGRU",Label2="Descrizione",Field3="CLCODART",Label3="Articolo/Servizio",Field4="DESART",Label4="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49498246

  add object oStr_1_5 as StdString with uid="LFGZHKZTCU",Visible=.t., Left=12, Top=85,;
    Alignment=0, Width=255, Height=18,;
    Caption="Listino da aggiornare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="ZJHQAOXOWD",Visible=.t., Left=512, Top=108,;
    Alignment=1, Width=91, Height=18,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CKQGSSDDPW",Visible=.t., Left=3, Top=61,;
    Alignment=1, Width=147, Height=18,;
    Caption="Criterio di aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="HAYJOAYVZM",Visible=.t., Left=420, Top=61,;
    Alignment=1, Width=95, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'U')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="KPWJOZGUUK",Visible=.t., Left=5, Top=35,;
    Alignment=1, Width=145, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TNKKFPAXNQ",Visible=.t., Left=375, Top=37,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MGGWUWYUUY",Visible=.t., Left=677, Top=108,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="IADITUBGLN",Visible=.t., Left=412, Top=137,;
    Alignment=1, Width=128, Height=18,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_CLAGGSCO<>'F' OR .w_FLSCON<>'S')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="IWFOKWSFCP",Visible=.t., Left=12, Top=166,;
    Alignment=0, Width=179, Height=18,;
    Caption="Listino di riferimento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="AKYQGNDRDB",Visible=.t., Left=12, Top=165,;
    Alignment=0, Width=179, Height=18,;
    Caption="Inventario di riferimento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="TFRBMQHZDR",Visible=.t., Left=635, Top=188,;
    Alignment=1, Width=65, Height=18,;
    Caption="Valido al:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (!(.w_CLBASSEL='LI' OR .w_CLBASSEL='LN' ))
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="XQSDXGRUDQ",Visible=.t., Left=17, Top=188,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="NAKCCEPVZW",Visible=.t., Left=157, Top=188,;
    Alignment=1, Width=83, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="QYGKEVIZPZ",Visible=.t., Left=562, Top=188,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (LEFT(.w_CLBASSEL,1)<>'I')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="UGWAEGAHIO",Visible=.t., Left=592, Top=38,;
    Alignment=1, Width=142, Height=18,;
    Caption="Ordine di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="MOJAGWDFKZ",Visible=.t., Left=5, Top=8,;
    Alignment=1, Width=145, Height=19,;
    Caption="Codice scheda:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_23 as StdBox with uid="GBPGTOYOYY",left=6, top=100, width=765,height=1

  add object oBox_1_33 as StdBox with uid="YMAPFMZRKS",left=6, top=182, width=765,height=1

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=239,;
    width=755+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=240,width=754+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRUMERC|ART_ICOL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRUMERC'
        oDropInto=this.oBodyCol.oRow.oCLGRUMER_2_1
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oCLCODART_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsva_mslPag2 as StdContainer
    Width  = 778
    height = 435
    stdWidth  = 778
    stdheight = 435
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVALOFIN_4_7 as StdField with uid="ZZMRLWXAIF",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132748374,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=639, Top=133, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"


  add object oCLCALSEL_4_10 as StdCombo with uid="QAHBGGRECH",rtseq=48,rtrep=.f.,left=120,top=53,width=205,height=21;
    , ToolTipText = "Parametro di ricalcolo selezionato (classe di ricalcolo / calcolato/ricarico prezzi articolo per listino)";
    , HelpContextID = 231010702;
    , cFormVar="w_CLCALSEL",RowSource=""+"Classe di ricarico,"+"Ricarico a percentuale fissa,"+"Ricarico con moltiplicatore,"+"Ricarico prezzi articolo,"+"Vendita imposta", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCLCALSEL_4_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CLCALSEL,&i_cF..t_CLCALSEL),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'R',;
    iif(xVal =3,'M',;
    iif(xVal =4,'P',;
    iif(xVal =5,'V',;
    space(1)))))))
  endfunc
  func oCLCALSEL_4_10.GetRadio()
    this.Parent.oContained.w_CLCALSEL = this.RadioValue()
    return .t.
  endfunc

  func oCLCALSEL_4_10.ToRadio()
    this.Parent.oContained.w_CLCALSEL=trim(this.Parent.oContained.w_CLCALSEL)
    return(;
      iif(this.Parent.oContained.w_CLCALSEL=='C',1,;
      iif(this.Parent.oContained.w_CLCALSEL=='R',2,;
      iif(this.Parent.oContained.w_CLCALSEL=='M',3,;
      iif(this.Parent.oContained.w_CLCALSEL=='P',4,;
      iif(this.Parent.oContained.w_CLCALSEL=='V',5,;
      0))))))
  endfunc

  func oCLCALSEL_4_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCLRICAR1_4_12 as StdField with uid="FCSXRAEEZF",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CLRICAR1", cQueryName = "CLRICAR1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ricarico",;
    HelpContextID = 4981161,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=120, Top=80, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCLRICAR1_4_12.mCond()
    with this.Parent.oContained
      return (.w_CLCALSEL='R')
    endwith
  endfunc

  add object oCLMOLTIP_4_15 as StdField with uid="AGAJHOBYMP",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CLMOLTIP", cQueryName = "CLMOLTIP",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^ moltiplicatore",;
    HelpContextID = 55160438,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=120, Top=108, cSayPict='"999999999999999.99"', cGetPict='"999999999999999.99"'

  func oCLMOLTIP_4_15.mCond()
    with this.Parent.oContained
      return (.w_CLCALSEL='M')
    endwith
  endfunc

  add object oCLMOLTI2_4_16 as StdField with uid="NEEDQOPNEZ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CLMOLTI2", cQueryName = "CLMOLTI2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^ moltiplicatore",;
    HelpContextID = 55160408,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=120, Top=136, cSayPict='"999999999999999.99"', cGetPict='"999999999999999.99"'

  func oCLMOLTI2_4_16.mCond()
    with this.Parent.oContained
      return (.w_CLCALSEL='M')
    endwith
  endfunc

  add object oCLRICVAL_4_17 as StdField with uid="COXPFVNYXY",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CLRICVAL", cQueryName = "CLRICVAL",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ricalcolo in valore del listino",;
    HelpContextID = 189530510,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=120, Top=164, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oCLRICVAL_4_17.mCond()
    with this.Parent.oContained
      return (.w_CLCALSEL$'RM')
    endwith
  endfunc

  add object oCLARROT1_4_19 as StdField with uid="UXLTQEUJWG",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CLARROT1", cQueryName = "CLARROT1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 22286761,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=340, Top=53, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oCLVALOR1_4_20 as StdField with uid="VCDCNKMUCX",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CLVALOR1", cQueryName = "CLVALOR1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 29606313,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=501, Top=53, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oCLARROT2_4_21 as StdField with uid="RSLXERXGNU",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CLARROT2", cQueryName = "CLARROT2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 22286760,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=340, Top=80, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oCLARROT2_4_21.mCond()
    with this.Parent.oContained
      return (.w_CLVALOR1<>0)
    endwith
  endfunc

  add object oCLVALOR2_4_22 as StdField with uid="IFLZKFIOAG",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CLVALOR2", cQueryName = "CLVALOR2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 29606312,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=501, Top=80, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oCLVALOR2_4_22.mCond()
    with this.Parent.oContained
      return (.w_CLVALOR1<>0)
    endwith
  endfunc

  func oCLVALOR2_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CLVALOR2>.w_CLVALOR1 OR .w_CLVALOR2=0)
    endwith
    return bRes
  endfunc

  add object oCLARROT3_4_23 as StdField with uid="OWSSMYOLGG",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CLARROT3", cQueryName = "CLARROT3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 22286759,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=340, Top=107, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oCLARROT3_4_23.mCond()
    with this.Parent.oContained
      return (.w_CLVALOR1<>0 AND .w_CLVALOR2<>0)
    endwith
  endfunc

  add object oCLVALOR3_4_24 as StdField with uid="VAVOQTAVGC",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CLVALOR3", cQueryName = "CLVALOR3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 29606311,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=501, Top=107, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oCLVALOR3_4_24.mCond()
    with this.Parent.oContained
      return (.w_CLVALOR1<>0 AND .w_CLVALOR2<>0)
    endwith
  endfunc

  func oCLVALOR3_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CLVALOR3>.w_CLVALOR2 OR  .w_CLVALOR3=0)
    endwith
    return bRes
  endfunc

  add object oCLARROT4_4_25 as StdField with uid="URZGWVCVYL",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CLARROT4", cQueryName = "CLARROT4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 22286758,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=340, Top=134, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oStr_4_4 as StdString with uid="JEFECLEULP",Visible=.t., Left=340, Top=23,;
    Alignment=0, Width=146, Height=15,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_5 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=504, Top=23,;
    Alignment=0, Width=146, Height=15,;
    Caption="Per importi fino a"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_6 as StdString with uid="HFEZTPBADH",Visible=.t., Left=487, Top=134,;
    Alignment=1, Width=149, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_8 as StdString with uid="EUXSGWMCFH",Visible=.t., Left=6, Top=80,;
    Alignment=1, Width=112, Height=18,;
    Caption="Perc. ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_4_9 as StdString with uid="LCHQFIKVVD",Visible=.t., Left=6, Top=108,;
    Alignment=1, Width=112, Height=18,;
    Caption="1� moltiplicatore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="JOCJINPMUD",Visible=.t., Left=6, Top=53,;
    Alignment=1, Width=112, Height=18,;
    Caption="Tipo di ricalcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="KXGRJBJXYB",Visible=.t., Left=8, Top=23,;
    Alignment=0, Width=180, Height=15,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_18 as StdString with uid="DRUIDCWMWY",Visible=.t., Left=6, Top=136,;
    Alignment=1, Width=112, Height=18,;
    Caption="2� moltiplicatore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="IWJLTAOQGP",Visible=.t., Left=6, Top=164,;
    Alignment=1, Width=112, Height=18,;
    Caption="Ricalcolo in valore:"  ;
  , bGlobalFont=.t.

  add object oBox_4_14 as StdBox with uid="WAAYDYUIYN",left=7, top=43, width=762,height=1
enddefine

* --- Defining Body row
define class tgsva_mslBodyRow as CPBodyRowCnt
  Width=745
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCLGRUMER_2_1 as StdTrsField with uid="ZWOQTOKVIL",rtseq=5,rtrep=.t.,;
    cFormVar="w_CLGRUMER",value=space(5),;
    HelpContextID = 52670856,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=70, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CLGRUMER"

  func oCLGRUMER_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLGRUMER_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCLGRUMER_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCLGRUMER_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oCLGRUMER_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CLGRUMER
    i_obj.ecpSave()
  endproc

  add object oCLCODART_2_2 as StdTrsField with uid="YPIQQLULRM",rtseq=6,rtrep=.t.,;
    cFormVar="w_CLCODART",value=space(20),;
    HelpContextID = 3600774,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=330, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CLCODART"

  func oCLCODART_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_CLGRUMER))
    endwith
  endfunc

  func oCLCODART_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLCODART_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCLCODART_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCLCODART_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oCLCODART_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CLCODART
    i_obj.ecpSave()
  endproc

  add object oDESART_2_3 as StdTrsField with uid="FNKLODYYEI",rtseq=39,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 207878346,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=256, Left=484, Top=0, InputMask=replicate('X',40)

  add object oDESGRU_2_4 as StdTrsField with uid="CDGQQQTCPR",rtseq=40,rtrep=.t.,;
    cFormVar="w_DESGRU",value=space(35),enabled=.f.,;
    HelpContextID = 190707914,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=255, Left=70, Top=0, InputMask=replicate('X',35)
  add object oLast as LastKeyMover
  * ---
  func oCLGRUMER_2_1.When()
    return(.t.)
  proc oCLGRUMER_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCLGRUMER_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_msl','CLLSMAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CLSERIAL=CLLSMAST.CLSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
