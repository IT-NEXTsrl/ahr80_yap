* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bds                                                        *
*              DUPLICAZIONE STRUTTURA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_37]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-14                                                      *
* Last revis.: 2016-06-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bds",oParentObject)
return(i_retval)

define class tgsva_bds as StdBatch
  * --- Local variables
  w_DESCRI = space(35)
  w_CURSORNA = space(10)
  w_RESULT = 0
  w_ELCODICE = space(10)
  w_ELCODPAD = space(10)
  w_PRCODSTR = space(10)
  w_PRCODTAB = space(10)
  w_PR_CAMPO = space(10)
  w_FOCODSTR = space(10)
  w_FOCODICE = space(10)
  w_DFCODSTR = space(10)
  w_DFCODFOR = space(10)
  w_CPROWNUM = 0
  w_STCRISTR = space(0)
  w_VNCODVAR = space(10)
  * --- WorkFile variables
  VAFORMAT_idx=0
  VASTRUTT_idx=0
  VAELEMEN_idx=0
  VADETTFO_idx=0
  VATRASCO_idx=0
  VAPREDEF_idx=0
  TMP_STR_idx=0
  TMP_ELEMEN_idx=0
  VAVARNOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSVA_KDS
    this.w_CURSORNA = SYS(2015)
    * --- Duplico le strutture
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STDESCRI"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STDESCRI;
        from (i_cTable) where;
            STCODICE = this.oParentObject.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESCRI = NVL(cp_ToDate(_read_.STDESCRI),cp_NullValue(_read_.STDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS<>0 and !ah_YesNo("Attenzione struttura %1 gi� presente, continuare?",,"",Alltrim(this.oParentObject.w_CODSTR))
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_02E6DD50
    bErr_02E6DD50=bTrsErr
    this.Try_02E6DD50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Attenzione errore nella duplicazione:%0%1",,"",Left(Alltrim(message()),254))
    endif
    bTrsErr=bTrsErr or bErr_02E6DD50
    * --- End
  endproc
  proc Try_02E6DD50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_Msg("Preparazione dati")
    this.w_RESULT = GSAR_BPT(this, this.w_CURSORNA, this.oParentObject.w_STRORI, this.oParentObject.w_ELEMORIG)
    if this.w_RESULT<0
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=MSG_TRANSACTION_ERROR
    endif
    ah_Msg("Duplicazione strutture")
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            STCODICE = this.oParentObject.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows=0
      * --- Insert into VASTRUTT
      i_nConn=i_TableProp[this.VASTRUTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VASTRUTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"STCODICE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'VASTRUTT','STCODICE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'STCODICE',this.oParentObject.w_CODSTR)
        insert into (i_cTable) (STCODICE &i_ccchkf. );
           values (;
             this.oParentObject.w_CODSTR;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Select from VASTRUTT
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" VASTRUTT ";
          +" where STCODICE="+cp_ToStrODBC(this.oParentObject.w_STRORI)+"";
           ,"_Curs_VASTRUTT")
    else
      select * from (i_cTable);
       where STCODICE=this.oParentObject.w_STRORI;
        into cursor _Curs_VASTRUTT
    endif
    if used('_Curs_VASTRUTT')
      select _Curs_VASTRUTT
      locate for 1=1
      do while not(eof())
      * --- Preparo array con nomi campi
      this.w_STCRISTR = IIF(Not Empty(Nvl(_Curs_VASTRUTT.STFILXSD," ")),cifracnf(Alltrim(this.oParentObject.w_CODSTR)+juststem(_Curs_VASTRUTT.STFILXSD),"C")," ")
      AFIELDS(_tmpFlds_)
      this.w_CPROWNUM = ALEN(_tmpFlds_,1)
      * --- Preparo array con valori campi
      COPY TO ARRAY "_tmpVal_"
      DIMENSION AppFlds(this.w_CPROWNUM,2)
      do while this.w_CPROWNUM>0
        * --- Nome campo
        AppFlds(this.w_CPROWNUM,1)=_tmpFlds_(this.w_CPROWNUM,1)
        * --- Valore campo
        if AppFlds(this.w_CPROWNUM,1)="STCODICE"
          AppFlds(this.w_CPROWNUM,2)=this.oParentObject.w_CODSTR
        else
          AppFlds(this.w_CPROWNUM,2)=_tmpVal_(1,this.w_CPROWNUM)
        endif
        this.w_CPROWNUM = this.w_CPROWNUM - 1
      enddo
      DIMENSION AppFilter(1,2)
      AppFilter(1,1)="STCODICE"
      AppFilter(1,2)=this.oParentObject.w_CODSTR
      if !EMPTY(writetable("VASTRUTT",@AppFlds,@AppFilter,.NULL.,.F.,"",i_Rows))
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
        select _Curs_VASTRUTT
        continue
      enddo
      use
    endif
    * --- Tolgo flag predefinita dalla struttura di origine
    if this.oParentObject.w_PREDEF="S"
      * --- Write into VASTRUTT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VASTRUTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VASTRUTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"STFLPRED ="+cp_NullLink(cp_ToStrODBC("N"),'VASTRUTT','STFLPRED');
            +i_ccchkf ;
        +" where ";
            +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_STRORI);
               )
      else
        update (i_cTable) set;
            STFLPRED = "N";
            &i_ccchkf. ;
         where;
            STCODICE = this.oParentObject.w_STRORI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Write into VASTRUTT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VASTRUTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"STDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESNEW),'VASTRUTT','STDESCRI');
      +",STFORTAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FORTAG),'VASTRUTT','STFORTAG');
      +",STNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STNOMFIL),'VASTRUTT','STNOMFIL');
      +",STFLPRED ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PREDEF),'VASTRUTT','STFLPRED');
      +",STCRISTR ="+cp_NullLink(cp_ToStrODBC(this.w_STCRISTR),'VASTRUTT','STCRISTR');
          +i_ccchkf ;
      +" where ";
          +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODSTR);
             )
    else
      update (i_cTable) set;
          STDESCRI = this.oParentObject.w_DESNEW;
          ,STFORTAG = this.oParentObject.w_FORTAG;
          ,STNOMFIL = this.oParentObject.w_STNOMFIL;
          ,STFLPRED = this.oParentObject.w_PREDEF;
          ,STCRISTR = this.w_STCRISTR;
          &i_ccchkf. ;
       where;
          STCODICE = this.oParentObject.w_CODSTR;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_CPROWNUM = 0
    ah_Msg("Duplicazione variabili nome file")
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VAVARNOM_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VAVARNOM_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where VNCODICE="+cp_ToStrODBC(this.oParentObject.w_STRORI)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"VNCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','VNCODICE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          VNCODICE = this.oParentObject.w_CODSTR;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TMP_ELEMEN
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2],.t.,this.TMP_ELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ELEMEN ";
           ,"_Curs_TMP_ELEMEN")
    else
      select * from (i_cTable);
        into cursor _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      select _Curs_TMP_ELEMEN
      locate for 1=1
      do while not(eof())
      this.w_PRCODSTR = _Curs_TMP_ELEMEN.VNCODICE
      this.w_VNCODVAR = _Curs_TMP_ELEMEN.VNCODVAR
      * --- Try
      local bErr_02EA9D30
      bErr_02EA9D30=bTrsErr
      this.Try_02EA9D30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02EA9D30
      * --- End
        select _Curs_TMP_ELEMEN
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    ah_Msg("Duplicazione valori predefiniti")
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VAPREDEF_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VAPREDEF_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where PRCODSTR="+cp_ToStrODBC(this.oParentObject.w_STRORI)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','PRCODSTR');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          PRCODSTR = this.oParentObject.w_CODSTR;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TMP_ELEMEN
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2],.t.,this.TMP_ELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ELEMEN ";
           ,"_Curs_TMP_ELEMEN")
    else
      select * from (i_cTable);
        into cursor _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      select _Curs_TMP_ELEMEN
      locate for 1=1
      do while not(eof())
      this.w_PRCODSTR = _Curs_TMP_ELEMEN.PRCODSTR
      this.w_PRCODTAB = _Curs_TMP_ELEMEN.PRCODTAB
      this.w_PR_CAMPO = _Curs_TMP_ELEMEN.PR_CAMPO
      * --- Try
      local bErr_02E843C8
      bErr_02E843C8=bTrsErr
      this.Try_02E843C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02E843C8
      * --- End
        select _Curs_TMP_ELEMEN
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    * --- Duplico i Formati
    ah_Msg("Duplicazione formati")
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VAFORMAT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VAFORMAT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where FOCODSTR="+cp_ToStrODBC(this.oParentObject.w_STRORI)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FOCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','FOCODSTR');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          FOCODSTR = this.oParentObject.w_CODSTR;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TMP_ELEMEN
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2],.t.,this.TMP_ELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ELEMEN ";
           ,"_Curs_TMP_ELEMEN")
    else
      select * from (i_cTable);
        into cursor _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      select _Curs_TMP_ELEMEN
      locate for 1=1
      do while not(eof())
      this.w_FOCODSTR = _Curs_TMP_ELEMEN.FOCODSTR
      this.w_FOCODICE = _Curs_TMP_ELEMEN.FOCODICE
      * --- Try
      local bErr_02EA51A0
      bErr_02EA51A0=bTrsErr
      this.Try_02EA51A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02EA51A0
      * --- End
        select _Curs_TMP_ELEMEN
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VADETTFO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VADETTFO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where DFCODSTR="+cp_ToStrODBC(this.oParentObject.w_STRORI)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DFCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','DFCODSTR');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          DFCODSTR = this.oParentObject.w_CODSTR;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TMP_ELEMEN
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2],.t.,this.TMP_ELEMEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_ELEMEN ";
           ,"_Curs_TMP_ELEMEN")
    else
      select * from (i_cTable);
        into cursor _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      select _Curs_TMP_ELEMEN
      locate for 1=1
      do while not(eof())
      this.w_DFCODSTR = _Curs_TMP_ELEMEN.DFCODSTR
      this.w_DFCODFOR = _Curs_TMP_ELEMEN.DFCODFOR
      this.w_CPROWNUM = _Curs_TMP_ELEMEN.CPROWNUM
      * --- Try
      local bErr_02E846C8
      bErr_02E846C8=bTrsErr
      this.Try_02E846C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_02E846C8
      * --- End
        select _Curs_TMP_ELEMEN
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    * --- Duplico gli elementi
    SELECT (this.w_CURSORNA)
    GO TOP
    SCAN
    this.w_ELCODICE = ELCODICE
    this.w_ELCODPAD = ELCODPAD
    if !EMPTY(this.oParentObject.w_ELEMDEST) AND this.w_ELCODICE=this.oParentObject.w_ELEMORIG
      this.w_ELCODPAD = this.oParentObject.w_ELEMDEST
    endif
    ah_Msg("Duplicazione elemento %1", .T., .F., .F., ALLTRIM(this.w_ELCODICE))
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VAELEMEN_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where ELCODSTR="+cp_ToStrODBC(this.oParentObject.w_STRORI)+" AND ELCODICE="+cp_ToStrODBC(this.w_ELCODICE)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ELCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','ELCODSTR');
      +",ELCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODICE),'TMP_ELEMEN','ELCODICE');
      +",ELCODPAD ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODPAD),'TMP_ELEMEN','ELCODPAD');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          ELCODSTR = this.oParentObject.w_CODSTR;
          ,ELCODICE = this.w_ELCODICE;
          ,ELCODPAD = this.w_ELCODPAD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into VAELEMEN
    i_nConn=i_TableProp[this.VAELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.VAELEMEN_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    ah_Msg("Duplicazione trascodifiche elemento %1", .T., .F., .F., ALLTRIM(this.w_ELCODICE))
    * --- Create temporary table TMP_ELEMEN
    i_nIdx=cp_AddTableDef('TMP_ELEMEN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.VATRASCO_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where TRCODSTR="+cp_ToStrODBC(this.oParentObject.w_STRORI)+" AND TRCODELE="+cp_ToStrODBC(this.w_ELCODICE)+"";
          )
    this.TMP_ELEMEN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_ELEMEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_ELEMEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_ELEMEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRCODSTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODSTR),'TMP_ELEMEN','TRCODSTR');
      +",TRCODELE ="+cp_NullLink(cp_ToStrODBC(this.w_ELCODICE),'TMP_ELEMEN','TRCODELE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          TRCODSTR = this.oParentObject.w_CODSTR;
          ,TRCODELE = this.w_ELCODICE;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into VATRASCO
    i_nConn=i_TableProp[this.VATRASCO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VATRASCO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.VATRASCO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_ELEMEN
    i_nIdx=cp_GetTableDefIdx('TMP_ELEMEN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ELEMEN')
    endif
    SELECT (this.w_CURSORNA)
    ENDSCAN
    if USED(this.w_CURSORNA)
      SELECT (this.w_CURSORNA)
      USE
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Duplicazione eseguita con successo",64)
    return
  proc Try_02EA9D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VAVARNOM
    i_nConn=i_TableProp[this.VAVARNOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAVARNOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where VNCODICE="+cp_ToStrODBC(this.w_PRCODSTR)+" AND VNCODVAR="+cp_ToStrODBC(this.w_VNCODVAR)+"",this.VAVARNOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02E843C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VAPREDEF
    i_nConn=i_TableProp[this.VAPREDEF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAPREDEF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where PRCODSTR="+cp_ToStrODBC(this.w_PRCODSTR)+" AND PRCODTAB="+cp_ToStrODBC(this.w_PRCODTAB)+" AND PR_CAMPO="+cp_ToStrODBC(this.w_PR_CAMPO)+"",this.VAPREDEF_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02EA51A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VAFORMAT
    i_nConn=i_TableProp[this.VAFORMAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAFORMAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where FOCODSTR="+cp_ToStrODBC(this.w_FOCODSTR)+" AND FOCODICE="+cp_ToStrODBC(this.w_FOCODICE)+"",this.VAFORMAT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_02E846C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VADETTFO
    i_nConn=i_TableProp[this.VADETTFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VADETTFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_ELEMEN_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where DFCODSTR="+cp_ToStrODBC(this.w_DFCODSTR)+" AND DFCODFOR="+cp_ToStrODBC(this.w_DFCODFOR)+" AND CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+"",this.VADETTFO_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='VAFORMAT'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='VAELEMEN'
    this.cWorkTables[4]='VADETTFO'
    this.cWorkTables[5]='VATRASCO'
    this.cWorkTables[6]='VAPREDEF'
    this.cWorkTables[7]='*TMP_STR'
    this.cWorkTables[8]='*TMP_ELEMEN'
    this.cWorkTables[9]='VAVARNOM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_VASTRUTT')
      use in _Curs_VASTRUTT
    endif
    if used('_Curs_TMP_ELEMEN')
      use in _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      use in _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      use in _Curs_TMP_ELEMEN
    endif
    if used('_Curs_TMP_ELEMEN')
      use in _Curs_TMP_ELEMEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
