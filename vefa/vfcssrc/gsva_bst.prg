* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bst                                                        *
*              CAMBIA STATO ALLA STRUTTURA                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-17                                                      *
* Last revis.: 2010-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bst",oParentObject,m.pEXEC)
return(i_retval)

define class tgsva_bst as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_STATO = space(1)
  w_OLDCOD = space(10)
  w_PROG = .NULL.
  w_DESCRI = space(40)
  * --- WorkFile variables
  VASTRUTT_idx=0
  VAELEMEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da Gsva_AST 
    *     cambia stato alla struttura
    * --- B-S: cambia stato alla struttura
    *     C: aggiorna lunghezza Tag negli elementi
    *     V: visualizza treeview
    *     T : cambio tabella piatta
    do case
      case this.pEXEC$ "B-S"
        if EMPTY(this.oParentObject.w_STCODICE)
          ah_errorMsg("Nessuna struttura selezionata")
        else
          this.w_STATO = iif(this.pEXEC="B","N","S")
          this.oParentObject.w_STFLPROV = iif(this.pEXEC="B","N","S")
          this.w_OLDCOD = this.oParentObject.w_STCODICE
          * --- Write into VASTRUTT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VASTRUTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VASTRUTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"STFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'VASTRUTT','STFLPROV');
                +i_ccchkf ;
            +" where ";
                +"STCODICE = "+cp_ToStrODBC(this.oParentObject.w_STCODICE);
                   )
          else
            update (i_cTable) set;
                STFLPROV = this.w_STATO;
                &i_ccchkf. ;
             where;
                STCODICE = this.oParentObject.w_STCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          SetMainCaption()
          this.oParentObject.Loadrec()
        endif
      case this.pEXEC="V"
        if EMPTY(this.oParentObject.w_STCODICE)
          ah_errorMsg("Nessuna struttura selezionata")
        else
          ah_msg("Apertura visualizzazione struttura...")
          this.w_PROG = GSVA_KVS()
          if this.w_PROG.bSec1
            = setvaluelinked ( "M" , this.w_PROG , "w_CODSTR" , this.oParentObject.w_STCODICE)
            = setvaluelinked ( "M" , this.w_PROG , "w_ROOT" ,this.w_PROG.w_LROOT,this.oParentObject.w_STCODICE)
            * --- Read from VAELEMEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VAELEMEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ELDESCRI"+;
                " from "+i_cTable+" VAELEMEN where ";
                    +"ELCODSTR = "+cp_ToStrODBC(this.oParentObject.w_STCODICE);
                    +" and ELCODICE = "+cp_ToStrODBC(this.oParentObject.w_STELROOT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ELDESCRI;
                from (i_cTable) where;
                    ELCODSTR = this.oParentObject.w_STCODICE;
                    and ELCODICE = this.oParentObject.w_STELROOT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCRI = NVL(cp_ToDate(_read_.ELDESCRI),cp_NullValue(_read_.ELDESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_PROG.w_DESELE = this.w_DESCRI
            ah_msg("Elaborazione visualizzazione struttura...")
            this.w_PROG.NotiFyEvent("Reload")     
          else
            ah_errorMsg("Impossibile accedere alla visualizzazione struttura")
          endif
        endif
      case this.pEXEC="C"
        if this.oParentObject.w_OLDLUN<>this.oParentObject.w_STLUNTAG and this.oparentobject.cfunction="Edit" and this.oParentObject.w_STTIPFIL="L"
          if Ah_yesno("Attenzione la modifica della lunghezza del tag determinerÓ l'aggiornamento della lunghezza degli elementi TAG%0Si desidera continuare?")
            * --- Write into VAELEMEN
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.VAELEMEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="ELCODSTR,ELCODICE"
              do vq_exec with 'gsva_bst',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.VAELEMEN_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
                      +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ELLUNMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STLUNTAG),'VAELEMEN','ELLUNMAX');
                  +i_ccchkf;
                  +" from "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
                      +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN, "+i_cQueryTable+" _t2 set ";
              +"VAELEMEN.ELLUNMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STLUNTAG),'VAELEMEN','ELLUNMAX');
                  +Iif(Empty(i_ccchkf),"",",VAELEMEN.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="VAELEMEN.ELCODSTR = t2.ELCODSTR";
                      +" and "+"VAELEMEN.ELCODICE = t2.ELCODICE";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set (";
                  +"ELLUNMAX";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STLUNTAG),'VAELEMEN','ELLUNMAX')+"";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="VAELEMEN.ELCODSTR = _t2.ELCODSTR";
                      +" and "+"VAELEMEN.ELCODICE = _t2.ELCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" VAELEMEN set ";
              +"ELLUNMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STLUNTAG),'VAELEMEN','ELLUNMAX');
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".ELCODSTR = "+i_cQueryTable+".ELCODSTR";
                      +" and "+i_cTable+".ELCODICE = "+i_cQueryTable+".ELCODICE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ELLUNMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STLUNTAG),'VAELEMEN','ELLUNMAX');
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=Ah_msgformat("Transazione abbandonata")
          endif
        endif
      case this.pEXEC="T"
        if !Ah_Yesno("Attenzione tale modifica potrebbe rendere incongruenti informazioni legate agli elementi della struttura%0Si desidera continuare ugualmente?")
          this.oParentObject.w_STFLATAB = this.oParentObject.w_OLDFLAT
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAELEMEN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
