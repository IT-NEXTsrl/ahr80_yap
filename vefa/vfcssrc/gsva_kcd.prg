* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kcd                                                        *
*              Conferma documenti provvisori generati da piano di spedizione   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-05                                                      *
* Last revis.: 2012-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kcd",oParentObject))

* --- Class definition
define class tgsva_kcd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 447
  Height = 166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-08"
  HelpContextID=99250793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsva_kcd"
  cComment = "Conferma documenti provvisori generati da piano di spedizione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PSNUMDOC = 0
  w_PSALFDOC = space(10)
  o_PSALFDOC = space(10)
  w_PSDATDOC = ctod('  /  /  ')
  o_PSDATDOC = ctod('  /  /  ')
  w_PSDATDIV = ctod('  /  /  ')
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVANNDOC = space(4)
  w_MVPRD = space(2)
  w_OKAGG = .F.
  w_PSCODMAG = space(5)
  w_CODMAT = space(5)
  w_PSSTATUS = space(1)
  w_PSPRD = space(2)
  w_MVDATDOC = ctod('  /  /  ')
  w_DEFALF = space(5)
  w_TDPRD = space(5)
  w_MVDATDIV = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kcdPag1","gsva_kcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPSNUMDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PSNUMDOC=0
      .w_PSALFDOC=space(10)
      .w_PSDATDOC=ctod("  /  /  ")
      .w_PSDATDIV=ctod("  /  /  ")
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVANNDOC=space(4)
      .w_MVPRD=space(2)
      .w_OKAGG=.f.
      .w_PSCODMAG=space(5)
      .w_CODMAT=space(5)
      .w_PSSTATUS=space(1)
      .w_PSPRD=space(2)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_DEFALF=space(5)
      .w_TDPRD=space(5)
      .w_MVDATDIV=ctod("  /  /  ")
      .w_MVNUMDOC=oParentObject.w_MVNUMDOC
      .w_MVALFDOC=oParentObject.w_MVALFDOC
      .w_MVANNDOC=oParentObject.w_MVANNDOC
      .w_MVPRD=oParentObject.w_MVPRD
      .w_OKAGG=oParentObject.w_OKAGG
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_DEFALF=oParentObject.w_DEFALF
      .w_TDPRD=oParentObject.w_TDPRD
      .w_MVDATDIV=oParentObject.w_MVDATDIV
          .DoRTCalc(1,1,.f.)
        .w_PSALFDOC = .w_DEFALF
        .w_PSDATDOC = i_datsys
          .DoRTCalc(4,4,.f.)
        .w_MVNUMDOC = .w_PSNUMDOC
        .w_MVALFDOC = .w_PSALFDOC
        .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
        .w_MVPRD = .w_PSPRD
        .w_OKAGG = .T.
        .w_PSCODMAG = SPACE(5)
        .w_CODMAT = SPACE(5)
        .w_PSSTATUS = 'N'
        .w_PSPRD = .w_TDPRD
        .w_MVDATDOC = .w_PSDATDOC
          .DoRTCalc(15,16,.f.)
        .w_MVDATDIV = .w_PSDATDIV
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVNUMDOC=.w_MVNUMDOC
      .oParentObject.w_MVALFDOC=.w_MVALFDOC
      .oParentObject.w_MVANNDOC=.w_MVANNDOC
      .oParentObject.w_MVPRD=.w_MVPRD
      .oParentObject.w_OKAGG=.w_OKAGG
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_DEFALF=.w_DEFALF
      .oParentObject.w_TDPRD=.w_TDPRD
      .oParentObject.w_MVDATDIV=.w_MVDATDIV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
            .w_MVNUMDOC = .w_PSNUMDOC
            .w_MVALFDOC = .w_PSALFDOC
            .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
            .w_MVPRD = .w_PSPRD
        .DoRTCalc(9,10,.t.)
            .w_CODMAT = SPACE(5)
        if .o_PSALFDOC<>.w_PSALFDOC.or. .o_PSDATDOC<>.w_PSDATDOC
          .Calculate_JWOQQUFHID()
        endif
        .DoRTCalc(12,13,.t.)
            .w_MVDATDOC = .w_PSDATDOC
        .DoRTCalc(15,16,.t.)
            .w_MVDATDIV = .w_PSDATDIV
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_JWOQQUFHID()
    with this
          * --- Inizializzazione primo numero documento da generare - GSVA_BPS(INITNUMDOC)
          gsva_bps(this;
              ,"INITNUMDOC";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_JWOQQUFHID()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSNUMDOC_1_1.value==this.w_PSNUMDOC)
      this.oPgFrm.Page1.oPag.oPSNUMDOC_1_1.value=this.w_PSNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOC_1_2.value==this.w_PSALFDOC)
      this.oPgFrm.Page1.oPag.oPSALFDOC_1_2.value=this.w_PSALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDOC_1_3.value==this.w_PSDATDOC)
      this.oPgFrm.Page1.oPag.oPSDATDOC_1_3.value=this.w_PSDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDIV_1_7.value==this.w_PSDATDIV)
      this.oPgFrm.Page1.oPag.oPSDATDIV_1_7.value=this.w_PSDATDIV
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PSDATDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATDOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_PSDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PSALFDOC = this.w_PSALFDOC
    this.o_PSDATDOC = this.w_PSDATDOC
    return

enddefine

* --- Define pages as container
define class tgsva_kcdPag1 as StdContainer
  Width  = 443
  height = 166
  stdWidth  = 443
  stdheight = 166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSNUMDOC_1_1 as StdField with uid="KSKQRDNYNC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PSNUMDOC", cQueryName = "PSNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero di fattura da generare",;
    HelpContextID = 213924295,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=209, Top=11, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPSALFDOC_1_2 as StdField with uid="PERYBOHTMB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PSALFDOC", cQueryName = "PSALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 221907399,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=11, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oPSDATDOC_1_3 as StdField with uid="KZDSRMLIGZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PSDATDOC", cQueryName = "PSDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione delle fatture differite",;
    HelpContextID = 207935943,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=209, Top=43

  add object oPSDATDIV_1_7 as StdField with uid="MLFEFEKNXD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PSDATDIV", cQueryName = "PSDATDIV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 207935924,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=209, Top=75


  add object oBtn_1_8 as StdButton with uid="XNWYCGISAT",left=324, top=111, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 99222042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="JCFFQZETBZ",left=376, top=111, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91933370;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="VWMRLRBKBQ",Visible=.t., Left=72, Top=45,;
    Alignment=1, Width=135, Height=18,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EXKTVGEXZR",Visible=.t., Left=327, Top=11,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VJZYILQUBP",Visible=.t., Left=10, Top=14,;
    Alignment=1, Width=197, Height=18,;
    Caption="Primo numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="IKKQXXPMEO",Visible=.t., Left=10, Top=78,;
    Alignment=1, Width=197, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kcd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
