* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bps                                                        *
*              Gestione piani di spedizione                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-12                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bps",oParentObject,m.pOPER)
return(i_retval)

define class tgsva_bps as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PADRE = .NULL.
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVPRD = space(2)
  w_MVANNDOC = space(4)
  w_DTOBSO = ctod("  /  /  ")
  w_OBSMAG = .f.
  w_OBSMAT = .f.
  w_TPSERDOC = space(10)
  w_SER_CONF = space(10)
  w_ORDERBY = space(254)
  w_nAttIdx = 0
  w_SERIALE = space(10)
  w_LROWNUM = 0
  w_BGEN_ERR = .f.
  w_BGENWARN = .f.
  w_OBJDOC = .NULL.
  w_GDRETVAL = space(254)
  w_OLDSERIA = space(10)
  w_CPCCCHK = space(10)
  w_ATNUMDOC = 0
  w_ATDATDOC = ctod("  /  /  ")
  w_TMDESART = space(40)
  w_ATROWNUM = 0
  w_ATROWORD = 0
  w_DPROWNUM = 0
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_MVFLCASC = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLRISE = space(1)
  w_MVSEREVA = space(10)
  w_ATSERIAL = space(10)
  w_ARDATINT = space(1)
  w_NUMROWOK = 0
  w_ATCODART = space(20)
  w_ATCODMAG = space(3)
  w_ATQTAUM1 = 0
  w_NUMDOCOK = 0
  w_SERTOSKIP = space(10)
  w_ORFLRISC = space(1)
  w_ATTIDLOG = 0
  w_LCODIVA = space(5)
  w_OLROWORD = 0
  w_OLROWNUM = 0
  w_IDXOLACC = 0
  w_IDXDATAG = 0
  w_TOTDATAG = 0
  w_STRDATAG = space(2)
  w_ASCODMAG = space(5)
  w_ASFLRISE = space(1)
  w_ASFLORDI = space(1)
  w_ASFLIMPE = space(1)
  w_ASCODMAT = space(5)
  w_ASF2RISE = space(1)
  w_ASF2ORDI = space(1)
  w_ASF2IMPE = space(1)
  w_ASQTASAL = 0
  w_ASKEYSAL = space(40)
  w_ASSERIAL = space(10)
  w_ASROWNUM = 0
  w_FLIMPE = space(1)
  w_FLORDI = space(1)
  w_ASNUMDDT = 0
  w_PSCHKCOL = space(5)
  w_PSTSTNUR = 0
  w_KEYPRPIASPE = space(100)
  w_ATFLSCAF = space(1)
  w_ARMAGPRE = space(5)
  w_ANMAGTER = space(5)
  w_FLPPRO = space(1)
  w_CFLPP = space(1)
  w_RSSERIAL = space(10)
  w_DCFLEVAS = space(1)
  w_CAMBATTU = 0
  w_TOTDOCEL = 0
  w_ATTNUROW = 0
  w_OKAGG = .f.
  w_DEFALF = space(5)
  w_TDPRD = space(5)
  w_ATFLSCAF = space(1)
  w_MVDATDIV = ctod("  /  /  ")
  w_FLAGGDAD = space(1)
  w_ASCODART = space(20)
  w_ASCODCOM = space(15)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_REPSEC = space(1)
  w_TIPOIN = space(5)
  w_CLIFOR = space(15)
  w_CATDOC = space(5)
  w_FLVEAC = space(1)
  w_CODESE = space(4)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_MVRIFODL = space(10)
  w_CODAGE = space(5)
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(5)
  w_CATCOM = space(3)
  w_CODDES = space(5)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_MVRIFFAD = space(10)
  w_SERDOC = space(10)
  w_DT = space(35)
  w_ATFLFIDO = space(1)
  w_ATVALFID = 0
  w_ATFLBLVE = space(1)
  w_ATCODCLI = space(20)
  w_ATTOTDOC = 0
  w_FIDATELA = ctod("  /  /  ")
  w_FIIMPPAP = 0
  w_FIIMPESC = 0
  w_FIIMPESO = 0
  w_FIIMPORD = 0
  w_FIIMPDDT = 0
  w_FIIMPFAT = 0
  w_ATDECTOT = 0
  w_ATFIDRES = 0
  w_LMDATDEL = ctod("  /  /  ")
  w_ATFLDISP = space(1)
  w_DAPRI_01 = 0
  w_DAPRI_02 = 0
  w_DAPRI_03 = 0
  w_DAPRI_04 = 0
  w_DAPRI_05 = 0
  w_DAPRI_06 = 0
  w_DAORD_01 = space(1)
  w_DAORD_02 = space(1)
  w_DAORD_03 = space(1)
  w_DAORD_04 = space(1)
  w_DAORD_05 = space(1)
  w_DAORD_06 = space(1)
  w_IDXFLDAN = 0
  w_IDXFLDAT = 0
  w_PRIATTFL = 0
  w_DO_ORDER = .f.
  w_DATAGGPO = space(254)
  w_DATAGGAN = space(254)
  * --- WorkFile variables
  DOC_MAST_idx=0
  MAGAZZIN_idx=0
  TMPPIASP_idx=0
  VACONPIA_idx=0
  GENERDOC_idx=0
  LOG_GEND_idx=0
  DET_PIAS_idx=0
  DOC_DETT_idx=0
  VOCIIVA_idx=0
  DATI_AGG_idx=0
  SALDIART_idx=0
  DOC_COLL_idx=0
  PIA_SPED_idx=0
  CAM_AGAZ_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    LOCAL TestMacro
    * --- Non � necessario definire come locali le variabili per ospitare i valori dei campi del documento
    *     Vengono create automaticamente in base ai campi del cursore.
    *     Se definite vengono comunque utilizzate senza errori
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="SELCONFPRE"
        this.w_SER_CONF = ""
        * --- Select from ..\vefa\exe\query\gsva_bps
        do vq_exec with '..\vefa\exe\query\gsva_bps',this,'_Curs__d__d__vefa_exe_query_gsva_bps','',.f.,.t.
        if used('_Curs__d__d__vefa_exe_query_gsva_bps')
          select _Curs__d__d__vefa_exe_query_gsva_bps
          locate for 1=1
          do while not(eof())
          this.w_SER_CONF = CPSERIAL
          exit
            select _Curs__d__d__vefa_exe_query_gsva_bps
            continue
          enddo
          use
        endif
        i_RetVal = this.w_SER_CONF
      case this.pOPER=="INITNUMDOC"
        this.w_MVNUMDOC = 0
        do case
          case this.oParentObject.w_PSSTATUS="S" AND EMPTY(this.oParentObject.w_PSALFDOC)
            this.oParentObject.w_PSALFDOC = "PR"
          case this.oParentObject.w_PSSTATUS="N" AND this.oParentObject.w_PSALFDOC=="PR"
            this.oParentObject.w_PSALFDOC = SPACE(10)
        endcase
        this.w_MVALFDOC = this.oParentObject.w_PSALFDOC
        this.w_MVPRD = this.oParentObject.w_PSPRD
        this.w_MVANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_AskTableProg(this, i_Conn, "PRDOC", "i_CODAZI,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
        this.oParentObject.w_PSNUMDOC = this.w_MVNUMDOC
        if ! Empty(this.oParentObject.w_PSCODMAG)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_PSCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_PSCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTOBSO = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_OBTEST>=this.w_DTOBSO AND NOT EMPTY(this.w_DTOBSO)
            this.w_OBSMAG = .t.
          endif
        endif
        this.w_DTOBSO = cp_CharToDate("  -  -    ")
        if ! Empty(this.oParentObject.w_CODMAT)
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDTOBSO"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDTOBSO;
              from (i_cTable) where;
                  MGCODMAG = this.oParentObject.w_CODMAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DTOBSO = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_OBTEST>=this.w_DTOBSO AND NOT EMPTY(this.w_DTOBSO)
            this.w_OBSMAT = .t.
          endif
        endif
        do case
          case this.w_OBSMAG and this.w_OBSMAT
            ah_errormsg("Magazzino principale e collegato obsoleti",48)
          case this.w_OBSMAG
            ah_errormsg("Magazzino principale obsoleto",48)
          case this.w_OBSMAT
            ah_errormsg("Magazzino collegato obsoleto",48)
        endcase
      case this.pOPER=="SELEZDOCUM"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Richiamo la maschera di selezione documenti
        *     In questo modo evito errori in costruzione zoom e posso
        *     utilizzare le variabili caller in modo appropriato
        DO GSVA_KPS WITH this.w_PADRE
      case this.pOPER=="GETXCHKDOC"
        vq_Exec("..\VEFA\EXE\QUERY\GSVA2BPS.VQR", this.w_PADRE.oParentObject, "TmpDocSel")
        SELECT (this.oParentObject.w_ZOOMMAST.cCursor)
        GO TOP
        UPDATE (this.oParentObject.w_ZOOMMAST.cCursor) SET XCHK=1 WHERE TPSERDOC IN (SELECT TPSERDOC FROM "TmpDocSel")
        GO TOP
        USE IN SELECT("TmpDocSel")
      case this.pOPER=="SETXCHKDOC"
        SELECT (this.oParentObject.w_ZOOMMAST.cCursor)
        GO TOP
        LOCATE FOR XCHK=1
        if !FOUND()
          this.oParentObject.w_FLDOCSEL = .F.
        endif
        GO TOP
        SCAN
        this.w_TPSERDOC = TPSERDOC
        * --- Write into TMPPIASP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPIASP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPIASP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPIASP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TPFLELAB ="+cp_NullLink(cp_ToStrODBC(IIF(XCHK=1, "S", "N")),'TMPPIASP','TPFLELAB');
              +i_ccchkf ;
          +" where ";
              +"TPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CURSSEDO);
              +" and TPSERDOC = "+cp_ToStrODBC(this.w_TPSERDOC);
                 )
        else
          update (i_cTable) set;
              TPFLELAB = IIF(XCHK=1, "S", "N");
              &i_ccchkf. ;
           where;
              TPSERIAL = this.oParentObject.w_CURSSEDO;
              and TPSERDOC = this.w_TPSERDOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        SELECT (this.oParentObject.w_ZOOMMAST.cCursor)
        ENDSCAN
      case this.pOPER=="RESETDATA"
        this.Page_3(.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="ELABDATA"
        this.w_SERIALE = SPACE(10)
        this.w_LROWNUM = 0
        this.w_BGEN_ERR = .F.
        this.w_BGENWARN = .F.
        this.w_TOTDATAG = 6
        i_Conn=i_TableProp[this.GENERDOC_IDX, 3]
        * --- begin transaction
        cp_BeginTrs()
        cp_NextTableProg(this,i_Conn,"SEGED","i_codazi,w_SERIALE")
        * --- commit
        cp_EndTrs(.t.)
        * --- Insert into GENERDOC
        i_nConn=i_TableProp[this.GENERDOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GENERDOC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GENERDOC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"GDSERIAL"+",GDDATGEN"+",GDPARAME"+",GDTIPOEL"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",GDKEYRIF"+",GDFLSIMU"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'GENERDOC','GDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(DATE()),'GENERDOC','GDDATGEN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PADRE.class),'GENERDOC','GDPARAME');
          +","+cp_NullLink(cp_ToStrODBC("E"),'GENERDOC','GDTIPOEL');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'GENERDOC','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'GENERDOC','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'GENERDOC','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'GENERDOC','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSSERIAL),'GENERDOC','GDKEYRIF');
          +","+cp_NullLink(cp_ToStrODBC("N"),'GENERDOC','GDFLSIMU');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'GDSERIAL',this.w_SERIALE,'GDDATGEN',DATE(),'GDPARAME',this.w_PADRE.class,'GDTIPOEL',"E",'UTCC',i_CODUTE,'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0,'UTDV',cp_CharToDate("  -  -    "),'GDKEYRIF',this.oParentObject.w_PSSERIAL,'GDFLSIMU',"N")
          insert into (i_cTable) (GDSERIAL,GDDATGEN,GDPARAME,GDTIPOEL,UTCC,UTDC,UTCV,UTDV,GDKEYRIF,GDFLSIMU &i_ccchkf. );
             values (;
               this.w_SERIALE;
               ,DATE();
               ,this.w_PADRE.class;
               ,"E";
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,this.oParentObject.w_PSSERIAL;
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.Page_5("L",ah_MsgFormat("Estrazione e ordinamento dati"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Eseguo l'estrazione dei documenti (Se non sono stati selezionati manualmente)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_5("L",ah_MsgFormat("Estrazione campi di rottura"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_nAttIdx = 1
        * --- Select from VACONPIA
        i_nConn=i_TableProp[this.VACONPIA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VACONPIA_idx,2],.t.,this.VACONPIA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPNOMCAM  from "+i_cTable+" VACONPIA ";
              +" where CPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PSSERCOR)+" AND CPFLROTT = 'S'";
              +" order by CPROWORD";
               ,"_Curs_VACONPIA")
        else
          select CPNOMCAM from (i_cTable);
           where CPSERIAL = this.oParentObject.w_PSSERCOR AND CPFLROTT = "S";
           order by CPROWORD;
            into cursor _Curs_VACONPIA
        endif
        if used('_Curs_VACONPIA')
          select _Curs_VACONPIA
          locate for 1=1
          do while not(eof())
          DIMENSION aCamRott[ this.w_nAttIdx ]
          aCamRott[ this.w_nAttIdx ] = ALLTRIM(_Curs_VACONPIA.CPNOMCAM)
          this.w_nAttIdx = this.w_nAttIdx + 1
            select _Curs_VACONPIA
            continue
          enddo
          use
        endif
        DIMENSION aDescriDatAgg[ this.w_TOTDATAG ]
        local L_MACRO
        this.w_IDXDATAG = 1
        * --- Se la tabella non � stata caricata il nome non viene sostituito nei messaggi, inizializzo per sicurezza
        do while this.w_IDXDATAG <= this.w_TOTDATAG
          aDescriDatAgg[ this.w_IDXDATAG ] = ah_MsgFormat("Campo %1", ALLTRIM(STR(this.w_IDXDATAG) ) )
          this.w_IDXDATAG = this.w_IDXDATAG + 1
        enddo
        this.w_IDXDATAG = 1
        * --- Select from DATI_AGG
        i_nConn=i_TableProp[this.DATI_AGG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DATI_AGG_idx,2],.t.,this.DATI_AGG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DATI_AGG ";
              +" where DASERIAL = '0000000001'";
               ,"_Curs_DATI_AGG")
        else
          select * from (i_cTable);
           where DASERIAL = "0000000001";
            into cursor _Curs_DATI_AGG
        endif
        if used('_Curs_DATI_AGG')
          select _Curs_DATI_AGG
          locate for 1=1
          do while not(eof())
          do while this.w_IDXDATAG <= this.w_TOTDATAG
            this.w_STRDATAG = RIGHT("00"+ALLTRIM(STR(this.w_IDXDATAG)),2)
            L_MACRO = "DACAM_" + this.w_STRDATAG
            aDescriDatAgg[ this.w_IDXDATAG ] = ALLTRIM(EVL(NVL(&L_MACRO , " "), ah_MsgFormat("Campo %1", ALLTRIM(STR(this.w_IDXDATAG) ) ) ) )
            this.w_IDXDATAG = this.w_IDXDATAG + 1
          enddo
            select _Curs_DATI_AGG
            continue
          enddo
          use
        endif
        release L_MACRO
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        * --- Esecuzione query estrazione dettaglio documenti da generare
        this.Page_5("L",ah_MsgFormat("Estrazione documenti da elaborare"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Try
        local bErr_029C3168
        bErr_029C3168=bTrsErr
        this.Try_029C3168()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.Page_5("E",ah_MsgFormat("Errore durante l'esecuzione della query di estrazione dei dati. Verificare campi di rottura/ordinamento e le necessarie corrispondenze nella query d'estrazione dati"))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_029C3168
        * --- End
        this.oParentObject.w_FLDOCSEL = .F.
        Release aCamRott, aChkRott, aFldsList, aMsgDatAgg, aDescriDatAgg
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        USE IN SELECT( this.oParentObject.w_CURSSEDO )
        this.Page_5("L",ah_MsgFormat("Fine elaborazione"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_BGEN_ERR OR this.w_BGENWARN
          if Ah_YesNo( "Generati n. %1 documenti%0Elaborazione completata con errori e/o avvisi%0Si desidera visualizzare il log di generazione?",, ALLTRIM(STR(this.w_NUMDOCOK)))
            this.w_PADRE.w_GDSERIAL = this.w_SERIALE
            this.w_PADRE.NotifyEvent("ShowLog")     
          endif
        else
          Ah_ErrorMsg( "Operazione completata%0N. %1 documenti generati", 64,, ALLTRIM(STR(this.w_NUMDOCOK)))
        endif
        if this.oParentObject.w_STADOCIM="S" AND this.w_NUMDOCOK > 0
          this.w_PADRE.NotifyEvent("PrintDoc")     
        endif
      case this.pOPER=="PRINTDOC"
        this.w_DATAIN = this.oParentObject.w_PSDATDOC
        this.w_DATA1 = this.oParentObject.w_PSDATDOC
        this.w_DATA2 = this.oParentObject.w_PSDATDOC
        this.w_DATAFI = this.oParentObject.w_PSDATDOC
        this.w_REPSEC = this.oParentObject.w_STREPSEC
        this.w_TIPOIN = ""
        this.w_CLIFOR = ""
        this.w_CATDOC = this.oParentObject.w_MVCLADOC
        this.w_FLVEAC = this.oParentObject.w_MVFLVEAC
        this.w_MVRIFODL = ""
        this.w_CODAGE = ""
        this.w_SERIE1 = ""
        this.w_SERIE2 = ""
        this.w_LINGUA = ""
        this.w_CATEGO = ""
        this.w_CODVET = ""
        this.w_CODZON = ""
        this.w_CATCOM = ""
        this.w_CODDES = ""
        this.w_CODPAG = ""
        this.w_NOSBAN = ""
        this.w_MVRIFFAD = ""
        this.w_SERDOC = REPL("X",10)
        this.w_DT = this.oParentObject.w_DESDOC
        this.AddProperty( "w_MVSERIAL", "" )
        GSVE_BRD(this,"P")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="CHKDOCTOGE"
        this.w_PSCHKCOL = ""
        if !EMPTY(NVL(this.oParentObject.w_PSTIPDOC, " "))
          * --- Read from DOC_COLL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2],.t.,this.DOC_COLL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DCCOLLEG"+;
              " from "+i_cTable+" DOC_COLL where ";
                  +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_PSTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DCCOLLEG;
              from (i_cTable) where;
                  DCCODICE = this.oParentObject.w_PSTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PSCHKCOL = NVL(cp_ToDate(_read_.DCCOLLEG),cp_NullValue(_read_.DCCOLLEG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.bUpdateParentObject=.F.
        i_retcode = 'stop'
        i_retval = !EMPTY(this.w_PSCHKCOL)
        return
      case this.pOPER=="RIPRISPROG"
        i_Conn=i_TableProp[this.PIA_SPED_IDX, 3]
        this.w_PSTSTNUR = this.oParentObject.w_PSNUMREG
        this.oParentObject.w_PSNUMREG = 0
        cp_AskTableProg(this.w_PADRE,i_Conn,"PRPIASPE","i_codazi,w_PSANNREG,w_PSNUMREG")
        if this.w_PSTSTNUR = this.oParentObject.w_PSNUMREG - 1
          * --- Aggiorno il progressivo all'ultimo piano di spedizione inserito
          this.w_KEYPRPIASPE = "prog\PRPIASPE\"+cp_ToStrODBC(alltrim(i_CODAZI))+"\"+cp_ToStrODBC(this.oParentObject.w_PSANNREG)
          * --- Try
          local bErr_03F56580
          bErr_03F56580=bTrsErr
          this.Try_03F56580()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_03F56580
          * --- End
        endif
      case this.pOPER=="CHKDOCORIG"
        this.w_PSCHKCOL = ""
        if !EMPTY(NVL(this.oParentObject.w_PSTIPORI, " ")) AND !EMPTY(NVL(this.oParentObject.w_TDCAUMAG, " "))
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLRISE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_TDCAUMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLRISE;
              from (i_cTable) where;
                  CMCODICE = this.oParentObject.w_TDCAUMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PSCHKCOL = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.bUpdateParentObject=.F.
        i_retcode = 'stop'
        i_retval = EMPTY(this.w_PSCHKCOL)
        return
      case this.pOPER=="CONFDOCGEN"
        this.w_ASNUMDDT = 0
        this.w_NUMDOCOK = 0
        this.w_OKAGG = .F.
        this.w_DEFALF = this.oParentObject.w_TDALFDOC
        this.w_TDPRD = this.oParentObject.w_PSPRD
        * --- Visualizzazione maschera per definizione primo numero documento definitivo da assegnare
        do GSVA_KCD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_OKAGG
          * --- Select from ..\vefa\exe\query\gsva8bps
          do vq_exec with '..\vefa\exe\query\gsva8bps',this,'_Curs__d__d__vefa_exe_query_gsva8bps','',.f.,.t.
          if used('_Curs__d__d__vefa_exe_query_gsva8bps')
            select _Curs__d__d__vefa_exe_query_gsva8bps
            locate for 1=1
            do while not(eof())
            this.w_ASNUMDDT = this.w_ASNUMDDT + 1
            this.w_ATSERIAL = DPSERDOC
            this.w_ATFLFIDO = ANFLFIDO
            this.w_ATFLBLVE = ANFLBLVE
            this.w_ATCODCLI = ALLTRIM(NVL(ANCODICE, " "))
            this.w_ATTOTDOC = IIF(MVAFLOM1="X", MVAIMPN1,0) + IIF(MVAFLOM2="X", MVAIMPN2,0) + IIF(MVAFLOM3="X", MVAIMPN3,0)
            this.w_ATTOTDOC = this.w_ATTOTDOC + IIF(MVAFLOM4="X", MVAIMPN4,0) + IIF(MVAFLOM5="X", MVAIMPN5,0) + IIF(MVAFLOM6="X", MVAIMPN6,0)
            this.w_ATTOTDOC = this.w_ATTOTDOC - MVSPEBOL
            this.w_FIDATELA = FIDATELA
            this.w_FIIMPPAP = FIIMPPAP
            this.w_FIIMPESC = FIIMPESC
            this.w_FIIMPESO = FIIMPESO
            this.w_FIIMPORD = FIIMPORD
            this.w_FIIMPDDT = FIIMPDDT
            this.w_FIIMPFAT = FIIMPFAT
            this.w_ATVALFID = NVL(ANVALFID, 0)
            this.w_ATDECTOT = NVL(VADECTOT, 2)
            this.w_ATFLSCAF = NVL(MVFLSCAF, "N")
            * --- Try
            local bErr_03F6DA58
            bErr_03F6DA58=bTrsErr
            this.Try_03F6DA58()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Errore in conferma documento%0%1",,, i_ErrMsg )
            endif
            bTrsErr=bTrsErr or bErr_03F6DA58
            * --- End
            release L_ARRKEY, bTrsOk
              select _Curs__d__d__vefa_exe_query_gsva8bps
              continue
            enddo
            use
          endif
          if this.w_ASNUMDDT > 0
            if this.w_NUMDOCOK>=this.w_ASNUMDDT
              * --- Write into PIA_SPED
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PIA_SPED_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PIA_SPED_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PIA_SPED_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PSSTATUS ="+cp_NullLink(cp_ToStrODBC("N"),'PIA_SPED','PSSTATUS');
                    +i_ccchkf ;
                +" where ";
                    +"PSSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PSSERIAL);
                       )
              else
                update (i_cTable) set;
                    PSSTATUS = "N";
                    &i_ccchkf. ;
                 where;
                    PSSERIAL = this.oParentObject.w_PSSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_ATSERIAL = this.oParentObject.w_PSSERIAL
            this.w_PADRE.ecpFilter()     
            this.w_PADRE.w_PSSERIAL = this.w_ATSERIAL
            this.w_PADRE.ecpSave()     
            ah_ErrorMsg("Confermati %1 documenti su %2",48+IIF(this.w_NUMDOCOK>=this.w_ASNUMDDT , 16, 0 ),, ALLTRIM(STR(this.w_NUMDOCOK)), ALLTRIM(STR(this.w_ASNUMDDT)) )
          else
            ah_ErrorMsg("Tutti i documenti generati dal piano di spedizione ancora esistenti sono gi� stati confermati precedentemente", 64)
          endif
        else
          ah_ErrorMsg("Operazione interrotta come richiesto", 64)
        endif
    endcase
  endproc
  proc Try_029C3168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    vq_Exec("..\VEFA\EXE\QUERY\GSVA4BPS.VQR", this, this.oParentObject.w_CURSSEDO )
    this.w_NUMDOCOK = 0
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_03F56580()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PSTSTNUR = this.w_PSTSTNUR - 1
    sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_PSTSTNUR)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_KEYPRPIASPE))
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03F6DA58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Memorizzo il progressivo assegnato al documento confermato
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC", .T.)
    this.w_FLAGGDAD = IIF(this.w_ATFLSCAF<>"S" AND !EMPTY(this.w_MVDATDIV), "=", " ")
    * --- Aggiorno i campi della testata
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_cOp5=cp_SetTrsOp(this.w_FLAGGDAD,'MVDATDIV','this.w_MVDATDIV',this.w_MVDATDIV,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +",MVALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +",MVFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLPROV');
      +",MVDATDIV ="+cp_NullLink(i_cOp5,'DOC_MAST','MVDATDIV');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          MVNUMDOC = this.w_MVNUMDOC;
          ,MVALFDOC = this.w_MVALFDOC;
          ,MVDATDOC = this.w_MVDATDOC;
          ,MVFLPROV = "N";
          ,MVDATDIV = &i_cOp5.;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorno i campi di dettaglio
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMFLCASC),'DOC_DETT','MVFLCASC');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMF2CASC),'DOC_DETT','MVF2CASC');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMFLORDI),'DOC_DETT','MVFLORDI');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMF2ORDI),'DOC_DETT','MVF2ORDI');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMF2IMPE),'DOC_DETT','MVF2IMPE');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMFLRISE),'DOC_DETT','MVFLRISE');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CMF2RISE),'DOC_DETT','MVF2RISE');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          MVFLCASC = this.oParentObject.w_CMFLCASC;
          ,MVF2CASC = this.oParentObject.w_CMF2CASC;
          ,MVFLORDI = this.oParentObject.w_CMFLORDI;
          ,MVF2ORDI = this.oParentObject.w_CMF2ORDI;
          ,MVFLIMPE = this.oParentObject.w_CMFLIMPE;
          ,MVF2IMPE = this.oParentObject.w_CMF2IMPE;
          ,MVFLRISE = this.oParentObject.w_CMFLRISE;
          ,MVF2RISE = this.oParentObject.w_CMF2RISE;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    Dimension L_ARRKEY(1,2)
    L_ARRKEY[1,1]="MVSERIAL"
    L_ARRKEY[1,2]=this.w_ATSERIAL
    Public bTrsOk
    bTrsOk=.t.
    this.w_GDRETVAL = ""
    VQ_EXEC("..\vefa\exe\query\GSVA9BPS.VQR", this, "TRS_ADD")
    if USED("TRS_ADD")
      * --- Aggiungo la propriet� per non definire la variabile locale che potrebbe dare errore nell'elaborazione dei documenti
      this.AddProperty( "w_MESS", "" )
      bTrsOk = GSAR_BDA(This, "S", This)
      if !bTrsOk
        this.w_GDRETVAL = ah_MsgFormat("Controllo disponibilit�%0%1", this.w_MESS )
      endif
    endif
    if this.oParentObject.w_TDFLCRIS ="S" AND this.oParentObject.w_TDFLRISC $ "SD" AND this.w_ATFLFIDO="S"
      if this.w_ATFLBLVE="S"
        this.w_GDRETVAL = ah_msgFormat("Il cliente %1 ha attivato il blocco vendite%0Impossibile confermare", this.w_ATCODCLI )
      else
        this.w_ATFIDRES = (this.w_ATVALFID-(this.w_FIIMPPAP+this.w_FIIMPESC+this.w_FIIMPESO+this.w_FIIMPORD+this.w_FIIMPDDT+this.w_FIIMPFAT) ) - this.w_ATTOTDOC 
        if this.w_ATFIDRES < 0
          this.w_GDRETVAL = ah_msgFormat("Superato massimo importo fido cliente%0Residuo: %1 alla data elaborazione: %2", ALLTRIM(STR(this.w_ATFIDRES,18+this.w_ATDECTOT,this.w_ATDECTOT)), DTOC(this.w_FIDATELA) )
        endif
      endif
    endif
    if EMPTY(this.w_GDRETVAL) and bTrsOk
      * --- Controlli finali sul documento
      this.w_GDRETVAL = GSAR_BAD(This,"U",@l_arrkey,.NULL.,@bTrsOk)
    endif
    if EMPTY(this.w_GDRETVAL) and bTrsOk
      * --- Verifiche e aggiornamenti tabelle collegate
      this.w_GDRETVAL = flr_docum(@l_arrkey, "POST", "U", @bTrsOk)
    endif
    if EMPTY(this.w_GDRETVAL) and bTrsOk
      * --- Aggiorno i riferimenti presenti nel dettaglio documenti generati
      if this.w_ATFLSCAF<>"S"
        GSAR_BRD(this,this.w_ATSERIAL, .F., .F., .F., .F., .T., .T. )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Write into DET_PIAS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DET_PIAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_PIAS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DPNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DET_PIAS','DPNUMDOC');
        +",DPALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DET_PIAS','DPALFDOC');
        +",DPDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DET_PIAS','DPDATDOC');
            +i_ccchkf ;
        +" where ";
            +"DPSERDOC = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            DPNUMDOC = this.w_MVNUMDOC;
            ,DPALFDOC = this.w_MVALFDOC;
            ,DPDATDOC = this.w_MVDATDOC;
            &i_ccchkf. ;
         where;
            DPSERDOC = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_MVNUMDOC = 0
      this.w_NUMDOCOK = this.w_NUMDOCOK + 1
    else
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_GDRETVAL
      i_Error=i_TrsMsg
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione tabella documenti da elaborare
    if !this.oParentObject.w_FLDOCSEL
      this.Pag10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Try
      local bErr_04041358
      bErr_04041358=bTrsErr
      this.Try_04041358()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.pOPER=="ELABDATA"
          this.Page_5("E",ah_MsgFormat("Errore durante l'esecuzione della query di estrazione dei dati. Verificare campi di rottura/ordinamento e le necessarie corrispondenze nella query d'estrazione dati"))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          ah_ErrorMsg("Errore durante l'esecuzione della query di estrazione dei dati. Verificare campi di rottura/ordinamento e le necessarie corrispondenze nella query d'estrazione dati")
        endif
      endif
      bTrsErr=bTrsErr or bErr_04041358
      * --- End
    endif
    this.w_ORDERBY = ""
  endproc
  proc Try_04041358()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMPPIASP
    i_nConn=i_TableProp[this.TMPPIASP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPIASP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\vefa\exe\query\gsva3bps",this.TMPPIASP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    param pDELHISTO
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Delete from TMPPIASP
    i_nConn=i_TableProp[this.TMPPIASP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPIASP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"TPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CURSSEDO);
             )
    else
      delete from (i_cTable) where;
            TPSERIAL = this.oParentObject.w_CURSSEDO;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if m.pDELHISTO
      this.w_LMDATDEL = i_DatSys - 30
      * --- Delete from TMPPIASP
      i_nConn=i_TableProp[this.TMPPIASP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPIASP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"TP__DATE <= "+cp_ToStrODBC(this.w_LMDATDEL);
               )
      else
        delete from (i_cTable) where;
              TP__DATE <= this.w_LMDATDEL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED( this.oParentObject.w_CURSSEDO )
      SELECT( this.oParentObject.w_CURSSEDO )
      GO TOP
      if RECCOUNT() > 0
        this.w_ATNUMDOC = this.oParentObject.w_PSNUMDOC
        this.w_ATDATDOC = this.oParentObject.w_PSDATDOC
        this.w_MVF2CASC = this.oParentObject.w_CMF2CASC
        this.w_MVF2IMPE = this.oParentObject.w_CMF2IMPE
        this.w_MVF2ORDI = this.oParentObject.w_CMF2ORDI
        this.w_MVF2RISE = this.oParentObject.w_CMF2RISE
        this.w_MVFLCASC = this.oParentObject.w_CMFLCASC
        this.w_MVFLIMPE = this.oParentObject.w_CMFLIMPE
        this.w_MVFLORDI = this.oParentObject.w_CMFLORDI
        this.w_MVFLRISE = this.oParentObject.w_CMFLRISE
        this.Page_5("L",ah_MsgFormat("Inizio elaborazione documenti"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Preparazione array per rotture
        *     Aggiungo 6 posizioni per i campi dei dati aggiuntivi
        DIMENSION aChkRott[ ALEN(aCamRott, 1) + 6, 2 ]
        DIMENSION aMsgDatAgg[ this.w_TOTDATAG ]
        aChkRott = ""
        this.w_OLDSERIA = ""
        this.w_DPROWNUM = 0
        AFIELDS( aFldsList, this.oParentObject.w_CURSSEDO )
        this.w_nAttIdx = 1
        do while this.w_nAttIdx <= ALEN( aFldsList, 1)
          if !PEMSTATUS( this, "w_" + ALLTRIM( aFldsList[ this.w_nAttIdx, 1 ] ), 5)
            this.AddProperty( "w_" + ALLTRIM( aFldsList[ this.w_nAttIdx, 1 ] ), ICASE(aFldsList[ this.w_nAttIdx, 2 ] $"D-T", cp_CharToDate("  -  -    "), aFldsList[ this.w_nAttIdx, 2 ]="N", 0, "" ) )
          endif
          this.w_nAttIdx = this.w_nAttIdx + 1
        enddo
        CREATE CURSOR "DocToDel" (MVSERIAL C(10), CPROWNUM N(5,0) )
        if this.oParentObject.w_PSFLDISP <> "N"
          this.w_OLDSERIA = ""
          this.w_SERTOSKIP = ""
          this.Page_5("L",ah_MsgFormat("Inizio verifica disponibilit�"))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          VQ_EXEC("..\VEFA\EXE\QUERY\GSVA6BPS.VQR", this, "tmpsaldi" )
          WRCURSOR("tmpsaldi")
          * --- Cursore d'appoggio per memorizzare le righe e i documenti da eliminare, la cancellazione diretta
          *     nel cursore d'origine provoca l'errata iterazione dello stesso durante la scan
          SELECT( this.oParentObject.w_CURSSEDO )
          GO TOP
          SCAN FOR EVL(NVL(ARFLDISP, " ") , "N" ) <> "N"
          if EMPTY(this.w_SERTOSKIP) OR MVSERIAL<>this.w_SERTOSKIP
            this.w_SERTOSKIP = ""
            if this.w_OLDSERIA <> MVSERIAL
              SELECT "tmpsaldi"
              GO TOP
              UPDATE "tmpsaldi" SET SLQTAPER = SLQTAPER - QTACONSU, QTACONSU = 0
              SELECT( this.oParentObject.w_CURSSEDO )
              this.w_OLDSERIA = MVSERIAL
            endif
            this.w_ATCODART = MVCODART
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARFLDISP"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_ATCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARFLDISP;
                from (i_cTable) where;
                    ARCODART = this.w_ATCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ATFLDISP = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_ARMAGPRE = ARMAGPRE
            this.w_ANMAGTER = ANMAGTER
            do case
              case this.oParentObject.w_TDFLMGPR="D"
                this.w_ATCODMAG = EVL(NVL(MVCODMAG, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG ), this.oParentObject.w_AZMAGAZI ) )
              case this.oParentObject.w_TDFLMGPR="O"
                this.w_ATCODMAG = EVL(NVL(MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG), this.oParentObject.w_AZMAGAZI ) ) )
              case this.oParentObject.w_TDFLMGPR="F"
                this.w_ATCODMAG = EVL(this.oParentObject.w_PSCODMAG, EVL(this.oParentObject.w_TDCODMAG, this.oParentObject.w_AZMAGAZI ) )
              case this.oParentObject.w_TDFLMGPR="I"
                this.w_ATCODMAG = EVL(NVL(this.w_ANMAGTER, " "), EVL(NVL(MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG), this.oParentObject.w_AZMAGAZI ) ) ))
              case this.oParentObject.w_TDFLMGPR="P"
                this.w_ATCODMAG = EVL(NVL(MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG ), this.oParentObject.w_AZMAGAZI ) ) )
            endcase
            this.w_ATQTAUM1 = MVQTAUM1
            this.w_ATROWNUM = CPROWNUM
            this.w_ATROWORD = CPROWORD
            this.w_ATFLSCAF = NVL(MVFLSCAF, "N")
            SELECT "tmpsaldi"
            GO TOP
            LOCATE FOR SLCODICE = this.w_ATCODART AND SLCODMAG = this.w_ATCODMAG
            if !FOUND() OR this.w_ATQTAUM1 > SLQTAPER - SLQTRPER - QTACONSU
              this.Page_5("W",ah_MsgFormat("Documento %1 riga %2. Per l'articolo %3 la quantit� richiesta (%4) non � disponibile nel magazzino %5",ALLTRIM(STR(this.w_ATNUMDOC)),ALLTRIM(STR(this.w_ATROWORD)),ALLTRIM(this.w_ATCODART),ALLTRIM(STR(this.w_ATQTAUM1)),ALLTRIM(this.w_ATCODMAG)),this.w_OLDSERIA,this.w_ATROWNUM)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case this.oParentObject.w_PSFLDISP = "D" OR this.w_ATFLSCAF="S"
                  if EVL(ARFLDISP, "S")="S"
                    if this.oParentObject.w_PSFLDISP = "D"
                      this.Page_5("E",ah_MsgFormat("Documento %1 scartato per controllo saldi su intero documento",ALLTRIM(STR(this.w_ATNUMDOC))),this.w_OLDSERIA,this.w_ATROWNUM)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    else
                      this.Page_5("E",ah_MsgFormat("Documento %1 scartato per flag scadenze fissate attivo",ALLTRIM(STR(this.w_ATNUMDOC))),this.w_OLDSERIA,this.w_ATROWNUM)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                    * --- Azzero le quantit� consumate dal documento corrente
                    SELECT "tmpsaldi"
                    GO TOP
                    UPDATE "tmpsaldi" SET QTACONSU = 0
                    * --- Memorizzo la necessit� di eliminare tutte le righe del documento corrente
                    INSERT INTO "DocToDel" VALUES ( this.w_OLDSERIA, -1)
                    this.w_SERTOSKIP = this.w_OLDSERIA
                  endif
                case this.oParentObject.w_PSFLDISP = "S"
                  if EVL(this.w_ATFLDISP, "S")="S"
                    * --- Eliminazione righe documento corrente
                    INSERT INTO "DocToDel" VALUES ( this.w_OLDSERIA, this.w_ATROWNUM)
                    this.Page_5("W",ah_MsgFormat("Documento %1 la riga %2 non verr� importata",ALLTRIM(STR(this.w_ATNUMDOC)),ALLTRIM(STR(this.w_ATROWORD))),this.w_OLDSERIA,this.w_ATROWNUM)
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
              endcase
              SELECT( this.oParentObject.w_CURSSEDO )
            else
              if FOUND()
                SELECT "tmpsaldi"
                REPLACE QTACONSU WITH QTACONSU + this.w_ATQTAUM1 IN "tmpsaldi" 
              endif
            endif
            SELECT( this.oParentObject.w_CURSSEDO )
          endif
          ENDSCAN
          this.w_ATROWNUM = 0
          this.w_OLDSERIA = ""
        endif
        if this.oParentObject.w_TDMINVEN $ "B-A" AND this.oParentObject.w_TDRIOTOT="R"
          SELECT( this.oParentObject.w_CURSSEDO )
          GO TOP
          SCAN FOR NVL(ARMINVEN,0)>MVQTAUM1
          this.w_OLDSERIA = MVSERIAL
          this.w_ATROWNUM = CPROWNUM
          if this.oParentObject.w_TDMINVEN="B"
            this.Page_5("W",ah_MsgFormat("Documento %1 la riga %2 non verr� importata, quantit� venduta (%3 %4) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%3 %5)",ALLTRIM(STR(this.w_ATNUMDOC)),ALLTRIM(STR(CPROWORD)),ALLTRIM(ARUNMIS1),ALLTRIM(STR(MVQTAUM1,12,3)),ALLTRIM(STR(ARMINVEN,12,3))),MVSERIAL,CPROWNUM)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            INSERT INTO "DocToDel" VALUES ( this.w_OLDSERIA, this.w_ATROWNUM)
          else
            this.Page_5("W",ah_MsgFormat("Documento %1 la riga %2 con quantit� venduta (%3 %4) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%3 %5)",ALLTRIM(STR(this.w_ATNUMDOC)),ALLTRIM(STR(CPROWORD)),ALLTRIM(ARUNMIS1),ALLTRIM(STR(MVQTAUM1,12,3)),ALLTRIM(STR(ARMINVEN,12,3))),MVSERIAL,CPROWNUM)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          SELECT( this.oParentObject.w_CURSSEDO )
          ENDSCAN
        endif
        SELECT "DocToDel"
        GO TOP
        SCAN
        if DocToDel.cprownum = -1
          DELETE FROM (this.oParentObject.w_CURSSEDO ) WHERE MVSERIAL = DocToDel.MVSERIAL
        else
          DELETE FROM (this.oParentObject.w_CURSSEDO ) WHERE MVSERIAL = DocToDel.MVSERIAL AND CPROWNUM = DocToDel.CPROWNUM
        endif
        SELECT "DocToDel"
        ENDSCAN
        USE IN SELECT("tmpsaldi")
        USE IN SELECT("DocToDel")
        this.Page_5("L",ah_MsgFormat("Inizio scrittura documenti"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- vettore per riferimento rige contributi accessori
        this.w_TOTDOCEL = 0
        this.w_ATTNUROW = 0
        Public aRifConAcc[1,2]
        SELECT( this.oParentObject.w_CURSSEDO )
        GO TOP
        SCAN
        this.w_ATTNUROW = this.w_ATTNUROW + 1
        if this.Page_Verifica_Rotture()
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OBJDOC = CREATEOBJECT("DocumWriter",This,"DOC_MAST")
          this.w_OBJDOC.cRetLogWarn = "S"
          this.w_OBJDOC.bEvasDoc = .T.
          release aRifConAcc
          Public aRifConAcc[1,2]
          this.w_OBJDOC.bNOcalcFido = EVL(NVL(this.oParentObject.w_PSFLGFID, " "), "N") = "N"
          this.w_OBJDOC.AddPropFido()     
          this.w_OBJDOC.w_CAONAZ = g_PERVAL
          this.w_OBJDOC.w_FLRISC = this.oParentObject.w_TDFLRISC
          DIMENSION aWhere(1,2)
          aWhere(1,1) = "VACODVAL"
          aWhere(1,2) = g_PERVAL
          this.w_OBJDOC.w_DECTOP = this.w_OBJDOC.ReadTable("VALUTE", "VADECTOT", @aWhere )
          SELECT( this.oParentObject.w_CURSSEDO )
          DIMENSION aWhere(2,2)
          aWhere(1,1) = "ANTIPCON"
          aWhere(1,2) = MVTIPCON
          aWhere(2,1) = "ANCODICE"
          aWhere(2,2) = MVCODCON
          this.w_OBJDOC.w_MAXFID = this.w_OBJDOC.ReadTable("CONTI", "ANVALFID", @aWhere )
          release aWhere
          this.w_OLDSERIA = ""
          this.w_ATROWNUM = 0
          this.w_NUMROWOK = 0
          this.w_ORFLRISC = NVL(TDFLRISC, "N")
          this.w_OBJDOC.w_MVTIPDOC = this.oParentObject.w_PSTIPDOC
          DIMENSION aWhere(2,2)
          aWhere(1,1) = "DCCODICE"
          aWhere(1,2) = this.w_OBJDOC.w_MVTIPDOC
          aWhere(2,1) = "DCCOLLEG"
          aWhere(2,2) = MVTIPDOC
          this.w_DCFLEVAS = NVL(this.w_OBJDOC.ReadTable("DOC_COLL", "DCFLEVAS", @aWhere ), " ")
          release aWhere
          this.w_OBJDOC.w_MVFLPROV = this.oParentObject.w_PSSTATUS
          this.w_OBJDOC.w_MVCLADOC = this.oParentObject.w_MVCLADOC
          this.w_OBJDOC.w_MVFLVEAC = this.oParentObject.w_MVFLVEAC
          this.w_OBJDOC.w_MVFLACCO = this.oParentObject.w_MVFLACCO
          if this.oParentObject.w_MVFLACCO = "S"
            this.w_OBJDOC.w_MVDATTRA = this.oParentObject.w_PSDATTRA
            this.w_OBJDOC.w_MVORATRA = this.oParentObject.w_PSORATRA
            this.w_OBJDOC.w_MVMINTRA = this.oParentObject.w_PSMINTRA
            this.w_OBJDOC.w_MVNOTAGG = this.oParentObject.w_PSNOTAGG
          endif
          this.w_OBJDOC.w_MVFLINTE = this.oParentObject.w_MVFLINTE
          this.w_OBJDOC.w_MVTIPCON = MVTIPCON
          this.w_OBJDOC.w_MVCODCON = MVCODCON
          this.w_OBJDOC.w_MVNUMDOC = this.w_ATNUMDOC
          this.w_OBJDOC.w_MVALFDOC = this.oParentObject.w_PSALFDOC
          this.w_OBJDOC.w_MVDATDOC = this.oParentObject.w_PSDATDOC
          this.w_OBJDOC.w_MVDATCIV = this.oParentObject.w_PSDATDOC
          this.w_OBJDOC.w_MVANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
          this.w_OBJDOC.w_MVPRD = this.oParentObject.w_PSPRD
          this.w_OBJDOC.w_MV__NOTE = NVL(MV__NOTE, "")
          this.w_OBJDOC.w_MVCODAGE = EVL(NVL(MVCODAGE, " "), .NULL.)
          this.w_OBJDOC.w_MVCODAG2 = EVL(NVL(MVCODAG2, " "), .NULL.)
          this.w_OBJDOC.w_MVCODIVE = EVL(NVL(MVCODIVE, " "), .NULL.)
          this.w_OBJDOC.w_MVRIFDIC = EVL(NVL(MVRIFDIC, " "), .NULL.)
          this.w_OBJDOC.w_MVIVAARR = EVL(NVL(MVIVAARR, " "), .NULL.)
          this.w_OBJDOC.w_MVCODVE3 = EVL(NVL(MVCODVE3, " "), .NULL.)
          this.w_OBJDOC.w_MVCODVE2 = EVL(NVL(MVCODVE2, " "), .NULL.)
          this.w_OBJDOC.w_MVCODPOR = EVL(NVL(MVCODPOR, " "), .NULL.)
          this.w_OBJDOC.w_MVCODSPE = EVL(NVL(MVCODSPE, " "), .NULL.)
          this.w_OBJDOC.w_MVCODORN = EVL(NVL(MVCODORN, " "), .NULL.)
          this.w_OBJDOC.w_MVCODASP = EVL(NVL(MVCODASP, " "), .NULL.)
          this.w_OBJDOC.w_MVCODSED = EVL(NVL(MVCODSED, " "), .NULL.)
          this.w_OBJDOC.w_MVCODPAG = NVL(MVCODPAG, " ")
          this.w_OBJDOC.w_MVTCOLIS = ""
          this.w_OBJDOC.w_UTDV = cp_CharToDate("  -  -    ")
          this.w_OBJDOC.w_MVSERIAL = SPACE(10)
          if EMPTY(this.w_OBJDOC.w_MVCODDES)
            if EMPTY(NVL(MVCODDES, " "))
              DIMENSION aWhere(4,2)
              aWhere(1,1) = "DDTIPCON"
              aWhere(1,2) = this.w_OBJDOC.w_MVTIPCON
              aWhere(2,1) = "DDCODICE"
              aWhere(2,2) = this.w_OBJDOC.w_MVCODCON
              aWhere(3,1) = "DDTIPRIF"
              aWhere(3,2) = "CO"
              aWhere(4,1) = "DDPREDEF"
              aWhere(4,2) = "S"
              this.w_OBJDOC.w_MVCODDES = this.w_OBJDOC.ReadTable("DES_DIVE", "DDCODDES", @aWhere )
            else
              this.w_OBJDOC.w_MVCODDES = MVCODDES
            endif
            if EMPTY(this.w_OBJDOC.w_MVCODDES)
              DIMENSION aWhere(3,2)
              aWhere(1,1) = "DDTIPCON"
              aWhere(1,2) = this.w_OBJDOC.w_MVTIPCON
              aWhere(2,1) = "DDCODICE"
              aWhere(2,2) = this.w_OBJDOC.w_MVCODCON
              aWhere(3,1) = "DDCODDES"
              aWhere(3,2) = this.w_OBJDOC.w_MVCODDES
              this.w_OBJDOC.w_MVCODVET = this.w_OBJDOC.ReadTable("DES_DIVE", "DDCODVET", @aWhere )
              this.w_OBJDOC.w_MVCODPOR = this.w_OBJDOC.ReadTable("DES_DIVE", "DDCODPOR", @aWhere )
              this.w_OBJDOC.w_MVCODSPE = this.w_OBJDOC.ReadTable("DES_DIVE", "DDCODSPE", @aWhere )
            endif
          endif
          if EMPTY(this.w_OBJDOC.w_MVCODBA2)
            DIMENSION aWhere(2,2)
            aWhere(1,1) = "ANTIPCON"
            aWhere(1,2) = this.w_OBJDOC.w_MVTIPCON
            aWhere(2,1) = "ANCODICE"
            aWhere(2,2) = this.w_OBJDOC.w_MVCODCON
            this.w_OBJDOC.w_MVCODBA2 = EVL(NVL(MVCODBA2, " "), this.w_OBJDOC.ReadTable("CONTI", "ANCODBA2", @aWhere ) )
            this.w_OBJDOC.w_MVCODBAN = EVL(NVL(MVCODBAN, " "), this.w_OBJDOC.ReadTable("CONTI", "ANCODBAN", @aWhere ) )
          endif
          if EMPTY(this.w_OBJDOC.w_MVFLVABD)
            DIMENSION aWhere(1,2)
            aWhere(1,1) = "AZCODAZI"
            aWhere(1,2) = i_CODAZI
            this.w_OBJDOC.w_MVFLVABD = this.w_OBJDOC.ReadTable("AZIENDA", "AZFLVEBD", @aWhere )
          endif
          if EMPTY(this.w_OBJDOC.w_MVCODVAL)
            this.w_OBJDOC.w_MVCODVAL = EVL(NVL(MVCODVAL, " "), g_PERVAL)
          endif
          this.w_OBJDOC.w_MVCATOPE = "OP"
          this.w_OBJDOC.w_MVFLSEND = "N"
          this.w_OBJDOC.w_MV__ANNO = YEAR(this.w_OBJDOC.w_MVDATDOC)
          this.w_OBJDOC.w_MVTIPDIS = "N"
          this.w_OBJDOC.w_MVSTFILCB = "1"
          this.w_OBJDOC.w_MV__MESE = MONTH(this.w_OBJDOC.w_MVDATDOC)
          this.w_OBJDOC.w_MVFLGINC = "N"
          if Not Empty(NVL(this.w_OBJDOC.w_MVCAUCON, " "))
            DIMENSION ArrWhere(1,2)
            ArrWhere(1,1) = "CCCODICE"
            ArrWhere(1,2) = this.w_OBJDOC.w_MVCAUCON
            this.w_CFLPP = this.w_PADRE.ReadTable( "CAU_CONT" , "CCFLPPRO" , @ArrWhere )
            Release ArrWhere
          else
            this.w_CFLPP = ""
          endif
          this.w_FLPPRO = IIF(this.oParentObject.w_DFLPP="P", this.w_CFLPP, this.oParentObject.w_DFLPP)
          this.w_OBJDOC.w_MVDATREG = EVL(NVL(this.w_OBJDOC.w_MVDATREG, " "), this.oParentObject.w_PSDATDOC)
          this.w_OBJDOC.w_MVCODESE = g_CODESE
          this.w_OBJDOC.w_MVANNPRO = CALPRO(this.w_OBJDOC.w_MVDATREG,this.w_OBJDOC.w_MVCODESE,this.w_FLPPRO)
          this.w_OBJDOC.w_MVRIFESP = ""
          this.w_OBJDOC.w_MVFLSCAF = NVL(MVFLSCAF, " ")
          this.w_OBJDOC.w_MVFLFOSC = NVL(MVFLFOSC, " ")
          this.w_OBJDOC.w_MVSCOCL1 = NVL(MVSCOCL1, 0)
          this.w_OBJDOC.w_MVSCOCL2 = NVL(MVSCOCL2, 0)
          this.w_OBJDOC.w_MVSCOPAG = NVL(MVSCOPAG, 0)
          if this.w_OBJDOC.w_MVFLFOSC="S"
            this.w_OBJDOC.w_MVSCONTI = NVL(MVSCONTI, 0)
          endif
          if this.w_OBJDOC.w_MVFLSCAF="S"
            * --- Scadenze fissate carico il dettaglio delle rate
            this.w_RSSERIAL = MVSERIAL
            this.w_OBJDOC.w_DOC_RATE.FillFromQuery("..\VEFA\EXE\QUERY\GSVA7BPS.VQR")     
          endif
          this.w_OBJDOC.w_MVFLRINC = NVL(MVFLRINC, " ")
          this.w_OBJDOC.w_MVSPEINC = EVL(NVL(MVSPEINC, 0), CALSPEINC( this.w_OBJDOC.w_MVFLVEAC, this.w_OBJDOC.w_MVCODPAG, this.w_OBJDOC.w_MVCODVAL, this.w_OBJDOC.w_MVTIPCON, this.w_OBJDOC.w_MVCODCON, this.oParentObject.w_TDFLSPIN ) )
          this.w_OBJDOC.w_MVFLRIMB = NVL(MVFLRIMB, " ")
          this.w_OBJDOC.w_MVSPEIMB = NVL(MVSPEIMB, 0)
          this.w_OBJDOC.w_MVFLRTRA = NVL(MVFLRTRA, " ")
          this.w_OBJDOC.w_MVSPETRA = NVL(MVSPETRA, 0)
          this.w_OBJDOC.w_MVDATDIV = NVL(MVDATDIV, cp_CharToDate("  -  -    "))
          this.w_OBJDOC.w_MVCODVET = NVL(MVCODVET, " " )
          this.w_OBJDOC.w_MVCODPOR = NVL(MVCODPOR, " " )
          this.w_OBJDOC.w_MVCODSPE = NVL(MVCODSPE, " " )
          this.w_OBJDOC.w_MVCONCON = NVL(MVCONCON, " " )
          this.w_OBJDOC.w_MVDESDOC = NVL(MVDESDOC, " " )
          this.w_OBJDOC.w_MVIVAINC = NVL(MVIVAINC, " " )
          this.w_OBJDOC.w_MVIVATRA = NVL(MVIVATRA, " " )
          this.w_OBJDOC.w_MVIVAIMB = NVL(MVIVAIMB, " " )
          this.w_OBJDOC.w_MVIVABOL = NVL(MVIVABOL, " " )
          this.w_OBJDOC.w_MVIVACAU = NVL(MVIVACAU, " " )
          if NVL(MVCAOVAL, 1) <> 1
            * --- Verifico il cambio, eventualmente do il messaggio
            if this.oParentObject.w_AZFLCAMB = "S"
              this.w_OBJDOC.w_MVCAOVAL = NVL(MVCAOVAL, 1)
            else
              this.w_CAMBATTU = GETCAM(this.w_OBJDOC.w_MVCODVAL, this.w_OBJDOC.w_MVDATDOC, 0)
              if this.w_CAMBATTU=0
                this.Page_5("W",ah_MsgFormat("Verificare cambio documento%0Cambio inesistente"))
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              if this.w_CAMBATTU<>0 AND this.w_CAMBATTU<>NVL(MVCAOVAL, 1)
                this.Page_5("W",ah_MsgFormat("Verificare cambio documento%0Valore originale %1 valore documento generato %2",ALLTRIM(STR(MVCAOVAL,12,7)),ALLTRIM(STR(this.w_CAMBATTU,12,7))))
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
          this.w_OBJDOC.w_MVALFEST = this.oParentObject.w_MVALFEST
          this.w_OBJDOC.w_MVTIPOPE = NVL(MVTIPOPE, " ")
          this.w_OBJDOC.w_MVASPEST = NVL(this.oParentObject.w_TDASPETT, "")
          this.w_OBJDOC.w_MVNUMCOR = NVL(MVNUMCOR, "")
          * --- Azzero numero documento, dopo il primo gi� impostato dalla maschera gli altri li devo ricalcolare
          this.w_ATNUMDOC = 0
          * --- Azzero le notifiche dei messaggi dei dati aggiuntivi
          do while this.w_IDXDATAG <= this.w_TOTDATAG
            aMsgDatAgg[this.w_IDXDATAG ] = .F.
            this.w_IDXDATAG = this.w_IDXDATAG + 1
          enddo
          this.w_ATTNUROW = 1
        endif
        * --- I dati aggiuntivi bench� siano di testata non possono essere inizializzati nel solo caso di rottura documento
        *     perch� occorre gestire i casi del primo documento con valore vuoto e i successivi con campo valorizato e
        *     tipo rottura a Si, in questo caso occorre valorizzare il documento di destinazione con il primo valore
        *     non vuoto
        local L_MACRO
        this.w_IDXDATAG = 1
        do while this.w_IDXDATAG <= this.w_TOTDATAG
          this.w_STRDATAG = RIGHT("00"+ALLTRIM(STR(this.w_IDXDATAG)),2)
          * --- Se la rottura � "si" o "no" se il campo era vuoto e adesso deve essere valorizzato
          *     e non ho dato ancora il messaggio aggiungo al log il messaggio d'attenzione
          L_MACRO = "this.oParentObject.w_TDFLIA" + this.w_STRDATAG + " = 'S' OR this.oParentObject.w_TDFLIA" + this.w_STRDATAG + " = 'N'"
          TestMacro=&L_MACRO
          if TestMacro
            L_MACRO = "EMPTY(NVL(this.w_OBJDOC.w_MVAGG_" + this.w_STRDATAG + ", ' ')) AND !EMPTY(NVL(MVAGG_" + this.w_STRDATAG + ", ' ')) AND !aMsgDatAgg[this.w_IDXDATAG] AND this.w_ATTNUROW > 1"
            TestMacro=&L_MACRO
            if TestMacro
              aMsgDatAgg[this.w_IDXDATAG ] = .T.
              this.Page_5("W",ah_MsgFormat("I documenti selezionati hanno valori differenti per il campo aggiuntivo %1%0Verr� utilizzato il valore del primo documento con dato aggiuntivo valorizzato",aDescriDatAgg[this.w_IDXDATAG]))
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          L_MACRO = "this.oParentObject.w_TDFLIA" + this.w_STRDATAG + " = 'S' "
          TestMacro=&L_MACRO
          if TestMacro
            L_MACRO = "this.w_OBJDOC.w_MVAGG_" + this.w_STRDATAG + " = EVL(NVL( this.w_OBJDOC.w_MVAGG_"+ this.w_STRDATAG +" , ' '), MVAGG_" + this.w_STRDATAG + ")"
            &L_MACRO
          endif
          this.w_IDXDATAG = this.w_IDXDATAG + 1
        enddo
        release L_MACRO
        if NVL(MVFLCAPA, " N")="S"
          this.w_OBJDOC.w_MVACCPRE = this.w_OBJDOC.w_MVACCPRE + NVL(MVACCONT, 0 )
        endif
        local L_MACRO, L_MACFLDVAL
        this.w_nAttIdx = 1
        do while this.w_nAttIdx <= ALEN( aFldsList, 1)
          L_MACFLDVAL = ALLTRIM( this.oParentObject.w_CURSSEDO ) + "." + aFldsList[ this.w_nAttIdx, 1 ]
          L_MACRO = "This.w_" + ALLTRIM( aFldsList[ this.w_nAttIdx, 1 ] ) + " = &L_MACFLDVAL"
          &L_MACRO
          this.w_nAttIdx = this.w_nAttIdx + 1
        enddo
        if EMPTY( this.w_OLDSERIA) OR MVSERIAL <> this.w_OLDSERIA
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_OLDSERIA = MVSERIAL
        this.w_OBJDOC.w_DOC_DETT.AppendBlank()     
        * --- Inizializzo i valori con le variabili create localmente con i campi del cursore
        this.w_OBJDOC.w_DOC_DETT.SetObjectValues(This)     
        this.w_ATROWNUM = this.w_ATROWNUM - 1
        this.w_OLROWNUM = this.w_OBJDOC.w_DOC_DETT.CPROWNUM
        this.w_OBJDOC.w_DOC_DETT.CPROWNUM = this.w_ATROWNUM
        if VARTYPE(aRifConAcc[ALEN(aRifConAcc,1),1])="N"
          Public aRifConAcc[ALEN(aRifConAcc,1)+1,2]
        endif
        aRifConAcc[ALEN(aRifConAcc,1),1] = this.w_OLROWNUM
        aRifConAcc[ALEN(aRifConAcc,1),2] = this.w_ATROWNUM
        this.w_OLROWORD = this.w_OBJDOC.w_DOC_DETT.CPROWORD
        this.w_OBJDOC.w_DOC_DETT.CPROWORD = this.w_ATROWNUM * -10
        DIMENSION aWhere(1,2)
        aWhere(1,1) = "ARCODART"
        aWhere(1,2) = this.w_OBJDOC.w_DOC_DETT.MVCODART
        this.w_ARMAGPRE = this.w_OBJDOC.ReadTable("ART_ICOL", "ARMAGPRE", @aWhere )
        release aWhere
        DIMENSION aWhere(2,2)
        aWhere(1,1) = "ANTIPCON"
        aWhere(1,2) = this.w_OBJDOC.w_MVTIPCON
        aWhere(2,1) = "ANCODICE"
        aWhere(2,2) = this.w_OBJDOC.w_MVCODCON
        this.w_ANMAGTER = this.w_OBJDOC.ReadTable("CONTI", "ANMAGTER", @aWhere )
        release aWhere
        if MVTIPRIG $ "F-M-D"
          this.w_OBJDOC.w_DOC_DETT.MVCODMAG = SPACE(5)
        else
          do case
            case this.oParentObject.w_TDFLMGPR="D"
              this.w_OBJDOC.w_DOC_DETT.MVCODMAG = EVL(NVL(this.w_OBJDOC.w_DOC_DETT.MVCODMAG, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG ), this.oParentObject.w_AZMAGAZI ) )
            case this.oParentObject.w_TDFLMGPR="O"
              this.w_OBJDOC.w_DOC_DETT.MVCODMAG = EVL(NVL(this.w_OBJDOC.w_DOC_DETT.MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG), this.oParentObject.w_AZMAGAZI ) ) )
            case this.oParentObject.w_TDFLMGPR="F"
              this.w_OBJDOC.w_DOC_DETT.MVCODMAG = EVL(this.oParentObject.w_PSCODMAG, EVL(this.oParentObject.w_TDCODMAG, this.oParentObject.w_AZMAGAZI ) )
            case this.oParentObject.w_TDFLMGPR="I"
              this.w_OBJDOC.w_DOC_DETT.MVCODMAG = EVL(NVL(this.w_ANMAGTER, " "), EVL(NVL(this.w_OBJDOC.w_DOC_DETT.MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG), this.oParentObject.w_AZMAGAZI ) ) ))
            case this.oParentObject.w_TDFLMGPR="P"
              this.w_OBJDOC.w_DOC_DETT.MVCODMAG = EVL(NVL(MVCODMAG, " "), EVL(NVL(this.w_ARMAGPRE, " "), EVL(EVL(this.oParentObject.w_PSCODMAG, this.oParentObject.w_TDCODMAG ), this.oParentObject.w_AZMAGAZI ) ) )
          endcase
        endif
        this.w_OBJDOC.w_DOC_DETT.MVCAUMAG = this.oParentObject.w_MVTCAMAG
        this.w_OBJDOC.w_DOC_DETT.MVFLCASC = this.w_MVFLCASC
        this.w_OBJDOC.w_DOC_DETT.MVFLIMPE = this.w_MVFLIMPE
        this.w_OBJDOC.w_DOC_DETT.MVFLORDI = this.w_MVFLORDI
        this.w_OBJDOC.w_DOC_DETT.MVFLRISE = this.w_MVFLRISE
        this.w_OBJDOC.w_DOC_DETT.MVF2CASC = this.w_MVF2CASC
        this.w_OBJDOC.w_DOC_DETT.MVF2IMPE = this.w_MVF2IMPE
        this.w_OBJDOC.w_DOC_DETT.MVF2ORDI = this.w_MVF2ORDI
        this.w_OBJDOC.w_DOC_DETT.MVF2RISE = this.w_MVF2RISE
        this.w_OBJDOC.w_DOC_DETT.MVNAZPRO = ANNAZION
        this.w_OBJDOC.w_DOC_DETT.MVFLELGM = this.oParentObject.w_MVFLELGM
        this.w_OBJDOC.w_DOC_DETT.MVFLARIF = IIF(this.w_DCFLEVAS<>"S", "+", " ")
        this.w_OBJDOC.w_DOC_DETT.MVFLERIF = IIF(this.w_DCFLEVAS<>"S", "S", " ")
        this.w_OBJDOC.w_DOC_DETT.MVQTAMOV = MVQTAMOV - MVQTAEVA
        this.w_OBJDOC.w_DOC_DETT.MVQTAUM1 = MVQTAUM1 - MVQTAEV1
        this.w_OBJDOC.w_DOC_DETT.MVPREZZO = IIF(this.oParentObject.w_TDPRZDES="S" AND this.w_OBJDOC.w_MVFLSCAF<>"S" , 0, MVPREZZO - MVIMPEVA )
        this.w_OBJDOC.w_DOC_DETT.MVQTAEVA = 0
        this.w_OBJDOC.w_DOC_DETT.MVQTAEV1 = 0
        this.w_OBJDOC.w_DOC_DETT.MVIMPEVA = 0
        this.w_OBJDOC.w_DOC_DETT.MVQTAIMP = MVQTAMOV - MVQTAEVA
        this.w_OBJDOC.w_DOC_DETT.MVQTAIM1 = MVQTAUM1 - MVQTAEV1
        this.w_OBJDOC.w_DOC_DETT.MVDATEVA = cp_CharToDate("  -  -    ")
        this.w_OBJDOC.w_DOC_DETT.MVLOTMAT = EVL(NVL(MVLOTMAT, " ") , .NULL. )
        this.w_OBJDOC.w_DOC_DETT.MVCODUBI = NVL(MVCODUBI, "")
        this.w_OBJDOC.w_DOC_DETT.MVLOTMAG = IIF( Empty( NVL(this.w_OBJDOC.w_DOC_DETT.MVCODLOT, " ") ) And Empty( NVL(this.w_OBJDOC.w_DOC_DETT.MVCODUBI, " ") ) , SPACE(5) , this.w_OBJDOC.w_DOC_DETT.MVCODMAG )
        this.w_OBJDOC.w_DOC_DETT.MVCONTRA = EVL(NVL(MVCONTRA, " "), .NULL. )
        this.w_OBJDOC.w_DOC_DETT.MVMC_PER = EVL(NVL(MVMC_PER, " "), .NULL. )
        this.w_OBJDOC.w_DOC_DETT.MVCODCLA = NVL(ARCODCLA, SPACE(3) )
        this.w_OBJDOC.w_DOC_DETT.MVSERRIF = MVSERIAL
        this.w_OBJDOC.w_DOC_DETT.MVROWRIF = CPROWNUM
        this.w_OBJDOC.w_DOC_DETT.MVSERIAL = SPACE(10)
        this.w_OBJDOC.w_DOC_DETT.MVRIFESC = SPACE(10)
        this.w_OBJDOC.w_DOC_DETT.MVTIPPRO = NVL(MVTIPPRO, " " )
        this.w_OBJDOC.w_DOC_DETT.MVTIPPR2 = NVL(MVTIPPR2, " " )
        this.w_OBJDOC.w_DOC_DETT.MVEFFEVA = cp_ChartoDate("  -  -    ")
        if !EMPTY(NVL(this.w_OBJDOC.w_DOC_DETT.MVRIFCAC, 0))
          * --- Ricerco il vecchio riferimento e reimposto il valore con il nuovo riferimento
          this.w_IDXOLACC = ASCAN(aRifConAcc, this.w_OBJDOC.w_DOC_DETT.MVRIFCAC, 1, -1, 1, 14)
          if this.w_IDXOLACC > 0
            this.w_OBJDOC.w_DOC_DETT.MVRIFCAC = aRifConAcc[this.w_IDXOLACC, 2]
          endif
        endif
        if (this.oParentObject.w_FLRICIVA AND ( EMPTY(this.oParentObject.w_RIIVDTIN) OR this.w_OBJDOC.w_MVDATEST>=this.oParentObject.w_RIIVDTIN) AND (EMPTY(this.oParentObject.w_RIIVDTFI) OR this.w_OBJDOC.w_MVDATEST<=this.oParentObject.w_RIIVDTFI) AND ( EMPTY(this.oParentObject.w_RIIVLSIV) OR this.w_OBJDOC.w_DOC_DETT.MVCODIVA+","$this.oParentObject.w_RIIVLSIV))
          this.w_LCODIVA = CALCODIV(this.w_OBJDOC.w_DOC_DETT.MVCODART, this.w_OBJDOC.w_MVTIPCON, iif(!empty(this.w_OBJDOC.w_MVCODORN),this.w_OBJDOC.w_MVCODORN,this.w_OBJDOC.w_MVCODCON), this.w_OBJDOC.w_MVTIPOPE, this.w_OBJDOC.w_MVDATREG)
          if NOT EMPTY(NVL(this.w_LCODIVA," "))
            this.Page_5("W",ah_msgformat("Riga %1: codice I.V.A. ricalcolato. Codice I.V.A. precedente: %2, nuovo codice I.V.A. %3",alltrim(str(this.w_OLROWORD)),alltrim(this.w_OBJDOC.w_DOC_DETT.MVCODIVA),alltrim(this.w_LCODIVA)),MVSERIAL,CPROWNUM)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_OBJDOC.w_DOC_DETT.MVCODIVA = this.w_LCODIVA
          endif
        endif
        * --- Salvo la riga corrente
        this.w_OBJDOC.w_DOC_DETT.SaveCurrentRecord()     
        this.w_NUMROWOK = this.w_NUMROWOK + IIF(this.w_OBJDOC.w_DOC_DETT.MVTIPRIG<>"D", 1, 0)
        SELECT( this.oParentObject.w_CURSSEDO )
        ENDSCAN
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.Page_5("W",ah_MsgFormat("Nessun documento da elaborare"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_5
    param pTIPOERR,pMESSAGE,pSERORI,pROWORI,pSERDES,pROWDES
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LROWNUM = this.w_LROWNUM + 1
    * --- Insert into LOG_GEND
    i_nConn=i_TableProp[this.LOG_GEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_GEND_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LGSERIAL"+",CPROWNUM"+",LGTIPRIG"+",LGMESSAG"+",LGSERORI"+",LGROWORI"+",LGSERDES"+",LGROWDES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'LOG_GEND','LGSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LROWNUM),'LOG_GEND','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(m.pTIPOERR),'LOG_GEND','LGTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(m.pMESSAGE),'LOG_GEND','LGMESSAG');
      +","+cp_NullLink(cp_ToStrODBC(EVL(m.pSERORI, .NULL.)),'LOG_GEND','LGSERORI');
      +","+cp_NullLink(cp_ToStrODBC(EVL(m.pROWORI, 0)),'LOG_GEND','LGROWORI');
      +","+cp_NullLink(cp_ToStrODBC(EVL(m.pSERDES, .NULL.)),'LOG_GEND','LGSERDES');
      +","+cp_NullLink(cp_ToStrODBC(EVL(m.pROWDES, 0)),'LOG_GEND','LGROWDES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LGSERIAL',this.w_SERIALE,'CPROWNUM',this.w_LROWNUM,'LGTIPRIG',m.pTIPOERR,'LGMESSAG',m.pMESSAGE,'LGSERORI',EVL(m.pSERORI, .NULL.),'LGROWORI',EVL(m.pROWORI, 0),'LGSERDES',EVL(m.pSERDES, .NULL.),'LGROWDES',EVL(m.pROWDES, 0))
      insert into (i_cTable) (LGSERIAL,CPROWNUM,LGTIPRIG,LGMESSAG,LGSERORI,LGROWORI,LGSERDES,LGROWDES &i_ccchkf. );
         values (;
           this.w_SERIALE;
           ,this.w_LROWNUM;
           ,m.pTIPOERR;
           ,m.pMESSAGE;
           ,EVL(m.pSERORI, .NULL.);
           ,EVL(m.pROWORI, 0);
           ,EVL(m.pSERDES, .NULL.);
           ,EVL(m.pROWDES, 0);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_BGEN_ERR = this.w_BGEN_ERR OR m.pTIPOERR=="E"
    this.w_BGENWARN = this.w_BGENWARN OR m.pTIPOERR=="W"
  endproc


  procedure Page_Verifica_Rotture
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private w_BNEWDOC
    m.w_BNEWDOC = .f.
    m.w_BNEWDOC = .F.
    Local L_Macro
    this.w_nAttIdx = 1
    do while this.w_nAttIdx <= ALEN( aChkRott, 1)
      aChkRott[ this.w_nAttIdx , 1 ] = aChkRott[ this.w_nAttIdx , 2 ]
      if this.w_nAttIdx<=ALEN(aCamRott, 1)
        if AT(".", aCamRott[ this.w_nAttIdx ] ) > 0
          L_MACRO = SUBSTR( aCamRott[ this.w_nAttIdx ], RAT(".", aCamRott[ this.w_nAttIdx ]) +1 )
        else
          L_MACRO = aCamRott[ this.w_nAttIdx ]
        endif
      else
        * --- Recupero il numero del campo aggiuntivo da verificare (Da 1 a 6)
        this.w_IDXDATAG = this.w_nAttIdx - ALEN(aCamRott, 1)
        this.w_STRDATAG = RIGHT("00"+ALLTRIM(STR(this.w_IDXDATAG)),2)
        L_MACRO = "this.oParentObject.w_TDFLIA"+this.w_STRDATAG+"<>'S' OR this.oParentObject.w_TDFLRA"+this.w_STRDATAG+"='N' "
        TestMacro=&L_MACRO
        if TestMacro
          * --- Non devo importare o non eseguo rottura, valorizzo il secondo campo uguale al primo
          L_MACRO = "aChkRott[ this.w_nAttIdx , 2 ]"
        else
          L_MACRO = "this.oParentObject.w_TDFLRA"+this.w_STRDATAG+"='S' "
          TestMacro=&L_MACRO
          if TestMacro
            * --- Rottura standard (non tassativa)
            * --- Preparo il valore per memorizzare nel secondo campo il valore attuale, se il primo era vuoto risulter� comunque
            *     uguale, altrimenti se necessario verr� eseguita la rottura
            L_MACRO = "NVL(MVAGG_"+this.w_STRDATAG+", ' ')"
            TestMacro= !EMPTY(aChkRott[ this.w_nAttIdx , 1 ]) AND EMPTY(&L_MACRO)
            if TestMacro
              * --- Il documento precedente aveva il valore pieno, quello attuale ha il valore vuoto, per evitare la rottura
              *     devo mettere come valore attuale il valore del documento precedente
              L_MACRO = "aChkRott[ this.w_nAttIdx , 2 ]"
            else
              if EMPTY(aChkRott[ this.w_nAttIdx , 1 ])
                * --- Il documento precedente aveva il valore vuoto, quello attuale no per evitare la rottura
                *     devo reimpostare nel vecchio documento il valore attuale
                aChkRott[ this.w_nAttIdx , 1 ] = &L_MACRO
              endif
            endif
          else
            * --- Rottura tassativa, preparo sempre il valore corrente perch� qualsiasi valore differente (anche vuoto)
            *     deve eseguire la rottura
            L_MACRO = "MVAGG_"+this.w_STRDATAG
          endif
        endif
      endif
      if L_MACRO=="@@DATIAGGPOSTORDER@@"
        aChkRott[ this.w_nAttIdx , 2 ] = aChkRott[ this.w_nAttIdx , 1 ]
      else
        aChkRott[ this.w_nAttIdx , 2 ] = NVL( &L_MACRO, "")
      endif
      this.w_nAttIdx = this.w_nAttIdx + 1
    enddo
    this.w_nAttIdx = 1
    do while !m.w_BNEWDOC AND this.w_nAttIdx <= ALEN( aChkRott, 1)
      do case
        case VARTYPE(aChkRott[ this.w_nAttIdx , 1 ] ) <> VARTYPE( aChkRott[ this.w_nAttIdx , 2 ] )
          m.w_BNEWDOC = .T.
        case VARTYPE(aChkRott[ this.w_nAttIdx , 1 ] ) $ "C-M"
          m.w_BNEWDOC = !lower(aChkRott[ this.w_nAttIdx , 1 ]) == lower(aChkRott[ this.w_nAttIdx , 2 ])
        otherwise
          m.w_BNEWDOC = !aChkRott[ this.w_nAttIdx , 1 ] = aChkRott[ this.w_nAttIdx , 2 ]
      endcase
      if m.w_BNEWDOC AND VARTYPE(this.w_OBJDOC) == "O"
        if this.w_nAttIdx<=ALEN(aCamRott, 1)
          this.Page_5("L",ah_MsgFormat("Individuata rottura documento campo %3, valore precedente (%1), valore attuale (%2)",cp_ToStrOdbc(aChkRott[this.w_nAttIdx,1]),cp_ToStrOdbc(aChkRott[this.w_nAttIdx,2]),ALLTRIM(aCamRott[this.w_nAttIdx])))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_IDXDATAG = this.w_nAttIdx - ALEN(aCamRott, 1)
          this.Page_5("L",ah_MsgFormat("Individuata rottura documento campo aggiuntivo %3, valore precedente (%1), valore attuale (%2)",cp_ToStrOdbc(aChkRott[this.w_nAttIdx,1]),cp_ToStrOdbc(aChkRott[this.w_nAttIdx,2]),aDescriDatAgg[this.w_IDXDATAG]))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      this.w_nAttIdx = this.w_nAttIdx + 1
    enddo
    release L_Macro
    return(m.w_BNEWDOC)
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riga descrittiva
    if this.oParentObject.w_FLNSRI = "S"
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(MVNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(MVALFDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(MVDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= "" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
       
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]=ALLTRIM(MVTIPDOC) 
 ARPARAM2[2]=ALLTRIM(STR(MVNUMDOC,15))+IIF(EMPTY(MVALFDOC),"", "/"+Alltrim(MVALFDOC))+SPACE(1)+DTOC(CP_TODATE(MVDATDOC))
      * --- Calcolo nuova riga descrittiva sul temporaneo di appoggio
      this.w_TMDESART = CALRIFDES("nsd",ANCODLIN, this.oParentObject.w_TDMODRIF, @ARPARAM, NVL(TDDESRIF, " "), @ARPARAM2)
      this.Page_8(this.w_TMDESART,this.oParentObject.w_TPNSRI)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_FLVSRI = "S" AND !EMPTY(NVL(MVNUMEST, 0 ) )
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= "" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
       
 DIMENSION ARPARAM2[2] 
 ARPARAM2[1]="N" 
 ARPARAM2[2]=ALLTRIM(STR(MVNUMEST,15))+IIF(EMPTY(MVALFEST),"", "/"+Alltrim(MVALFEST)) +SPACE(1)+DTOC(MVDATEST) 
      * --- Scrive nuova Riga Descrittiva sul Temporaneo di Appoggio
      this.w_TMDESART = CALRIFDES("vsd",ANCODLIN, this.oParentObject.w_TDMODRIF, @ARPARAM,"", @ARPARAM2)
      this.Page_8(this.w_TMDESART,this.oParentObject.w_TPVSRI)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_TDFLRIDE = "S"
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(MVNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=Alltrim(MVALFDOC) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(MVDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATPRO" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(MVCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(MVCODAGE) 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]= "" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=""
      this.w_TMDESART = IIF(!EMPTY(NVL(MV__NOTE, " ")), "R.(" +ALLTRIM(MVTIPDOC)+") "+ALLTRIM(MV__NOTE), CALRIFDES("dsd",ANCODLIN, this.oParentObject.w_TDMODRIF, @ARPARAM, ,.F.) )
      if !EMPTY(this.w_TMDESART) AND this.oParentObject.w_MVCLADOC<>"OR"
        * --- Se il dato � stato inserito dai dati di testata nel trasferimento 
        *     del valore al relativo campo vengono eliminati gli spazi
        this.w_TMDESART = this.w_TMDESART+" "
      endif
      if !EMPTY(ALLTRIM(this.w_TMDESART))
        this.Page_8(this.w_TMDESART,this.oParentObject.w_TPVSRI)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_8
    param pDESRIG,pCLADOC
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJDOC.w_DOC_DETT.AppendBlank()     
    this.w_ATROWNUM = this.w_ATROWNUM - 1
    this.w_OBJDOC.w_DOC_DETT.CPROWNUM = this.w_ATROWNUM
    this.w_OBJDOC.w_DOC_DETT.CPROWORD = this.w_ATROWNUM * -10
    this.w_OBJDOC.w_DOC_DETT.MVTIPRIG = "D"
    this.w_OBJDOC.w_DOC_DETT.MVCODICE = g_ARTDES
    if len(alltrim(m.pDESRIG)) >40
      * --- Se la lunghezza del Riferimento descrittivo � maggiore di 30 caratteri,
      *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
      *     dello spazio nei primi 30 caratteri.
      *     Il resto viene inserito nella descrizione supplementare.
      this.w_OBJDOC.w_DOC_DETT.MVDESART = LEFT(m.pDESRIG, RATC(" ", LEFT(m.pDESRIG,40)))
      this.w_OBJDOC.w_DOC_DETT.MVDESSUP = SUBSTR(m.pDESRIG,(RATC(" ",LEFT(m.pDESRIG,40))+1))
    else
      this.w_OBJDOC.w_DOC_DETT.MVDESART = m.pDESRIG
    endif
    if !EMPTY(this.w_OBJDOC.w_DOC_DETT.MVCODICE)
      this.w_OBJDOC.w_DOC_DETT.MVCODART = g_ARTDES
      DIMENSION aWhere(1,2)
      aWhere(1,1) = "ARCODART"
      aWhere(1,2) = this.w_OBJDOC.w_DOC_DETT.MVCODART
      this.w_ARDATINT = this.w_OBJDOC.ReadTable("ART_ICOL", "ARDATINT", @aWhere )
      this.w_OBJDOC.w_DOC_DETT.MVFLTRAS = IIF( this.w_ARDATINT="S","Z",IIF( this.w_ARDATINT="F"," ",IIF(this.w_ARDATINT="I","I","S")))
      this.w_OBJDOC.w_DOC_DETT.MVCODCLA = m.pCLADOC
    endif
    this.w_OBJDOC.w_DOC_DETT.MVUNIMIS = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVQTAMOV = 0
    this.w_OBJDOC.w_DOC_DETT.MVPREZZO = 0
    this.w_OBJDOC.w_DOC_DETT.MVCODCLA = ""
    this.w_OBJDOC.w_DOC_DETT.MVCODIVA = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVCONTRA = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVFLTRAS = " "
    this.w_OBJDOC.w_DOC_DETT.MVTIPCOL = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVCODCOM = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVROWRIF = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVFLRESC = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVTIPATT = "A"
    this.w_OBJDOC.w_DOC_DETT.MVCODCOS = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVFLRIAP = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVCODATT = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVFLRVCL = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVMC_PER = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVDATOAI = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVAIRPOR = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVCODRES = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVLOTMAT = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVLOTMAG = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVCODCOS = .NULL.
    this.w_OBJDOC.w_DOC_DETT.MVRIGPRE = "N"
    this.w_OBJDOC.w_DOC_DETT.SaveCurrentRecord()     
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if VARTYPE(this.w_OBJDOC) == "O"
      if this.w_NUMROWOK > 0
        * --- Calcolo i seriali prima di chiamare la creazione del documento.
        *     Se il seriale � vuoto la generazione ricalcola tutti i progressivi.
        *     Il numero documento non deve essere ricalcolato in quanto
        *     � possibile specificare da che nuemro partire nella maschera
        *     di configurazione del piano di spedizione.
        this.w_TOTDOCEL = this.w_TOTDOCEL + 1
        * --- begin transaction
        cp_BeginTrs()
        i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
        cp_NextTableProg(this.w_OBJDOC, i_Conn, "SEDOC", "i_CODAZI,w_MVSERIAL", .T. )
        if this.w_OBJDOC.w_MVANNPRO<>" "
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          cp_NextTableProg(this.w_OBJDOC, i_Conn, "PRPRO", "i_codazi,w_MVANNPRO,w_MVPRP,w_MVALFEST,w_MVNUMEST", .T.)
        endif
        if !empty(this.w_OBJDOC.w_MVNUMDOC) OR (this.w_OBJDOC.w_MVFLVEAC="V" OR (this.w_OBJDOC.w_MVFLVEAC="A" AND (this.w_OBJDOC.w_MVPRD="DV" OR this.w_OBJDOC.w_MVPRD="IV")))
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          cp_NextTableProg(this.w_OBJDOC, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC", .T.)
          if (this.w_OBJDOC.w_MVFLVEAC="V" OR (this.w_OBJDOC.w_MVFLVEAC="A" AND (this.w_OBJDOC.w_MVPRD="DV" OR this.w_OBJDOC.w_MVPRD="IV")))
            this.w_OBJDOC.w_MVNUMREG = this.w_MVNUMDOC
          endif
        endif
        this.w_GDRETVAL = ""
        this.w_GDRETVAL = this.w_OBJDOC.CreateDocument()
        if this.w_OBJDOC.nTotLogMsg > 0
          this.w_ATTIDLOG = 1
          do while this.w_ATTIDLOG <= this.w_OBJDOC.nTotLogMsg
            this.Page_5(this.w_OBJDOC.aLogRetMsg[this.w_ATTIDLOG,1],this.w_OBJDOC.aLogRetMsg[this.w_ATTIDLOG,2])
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_OBJDOC.aLogRetMsg[ this.w_ATTIDLOG , 1]=="E" AND EMPTY(this.w_GDRETVAL)
              this.w_GDRETVAL = ah_MsgFormat("Impossibile generare documento. Verificare log di generazione")
            endif
            this.w_ATTIDLOG = this.w_ATTIDLOG + 1
          enddo
          this.w_OBJDOC.ResetLogMsg()     
        endif
        if EMPTY(this.w_GDRETVAL)
          this.w_MVSEREVA = this.w_OBJDOC.w_MVSERIAL
          this.w_DPROWNUM = this.w_DPROWNUM + 1
          * --- Try
          local bErr_04211108
          bErr_04211108=bTrsErr
          this.Try_04211108()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.Page_5("E",MESSAGE())
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04211108
          * --- End
        else
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.Page_5("E",this.w_GDRETVAL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.Page_5("E",ah_MsgFormat("Documento non generabile perch� senza righe di dettaglio o con sole righe descrittive"),this.w_OLDSERIA)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_OBJDOC = .NULL.
    endif
  endproc
  proc Try_04211108()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DET_PIAS
    i_nConn=i_TableProp[this.DET_PIAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_PIAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPSERIAL"+",CPROWNUM"+",CPROWORD"+",DPSERDOC"+",DPNUMDOC"+",DPALFDOC"+",DPDATDOC"+",DPTIPCLF"+",DPCODCLF"+",DPPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSSERIAL),'DET_PIAS','DPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DPROWNUM),'DET_PIAS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DPROWNUM * 10),'DET_PIAS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSEREVA),'DET_PIAS','DPSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJDOC.w_MVNUMDOC),'DET_PIAS','DPNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJDOC.w_MVALFDOC),'DET_PIAS','DPALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJDOC.w_MVDATDOC),'DET_PIAS','DPDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJDOC.w_MVTIPCON),'DET_PIAS','DPTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJDOC.w_MVCODCON),'DET_PIAS','DPCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(this.w_OBJDOC.w_MVFLVEAC)+ALLTRIM(this.w_OBJDOC.w_MVCLADOC)),'DET_PIAS','DPPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.oParentObject.w_PSSERIAL,'CPROWNUM',this.w_DPROWNUM,'CPROWORD',this.w_DPROWNUM * 10,'DPSERDOC',this.w_MVSEREVA,'DPNUMDOC',this.w_OBJDOC.w_MVNUMDOC,'DPALFDOC',this.w_OBJDOC.w_MVALFDOC,'DPDATDOC',this.w_OBJDOC.w_MVDATDOC,'DPTIPCLF',this.w_OBJDOC.w_MVTIPCON,'DPCODCLF',this.w_OBJDOC.w_MVCODCON,'DPPARAME',ALLTRIM(this.w_OBJDOC.w_MVFLVEAC)+ALLTRIM(this.w_OBJDOC.w_MVCLADOC))
      insert into (i_cTable) (DPSERIAL,CPROWNUM,CPROWORD,DPSERDOC,DPNUMDOC,DPALFDOC,DPDATDOC,DPTIPCLF,DPCODCLF,DPPARAME &i_ccchkf. );
         values (;
           this.oParentObject.w_PSSERIAL;
           ,this.w_DPROWNUM;
           ,this.w_DPROWNUM * 10;
           ,this.w_MVSEREVA;
           ,this.w_OBJDOC.w_MVNUMDOC;
           ,this.w_OBJDOC.w_MVALFDOC;
           ,this.w_OBJDOC.w_MVDATDOC;
           ,this.w_OBJDOC.w_MVTIPCON;
           ,this.w_OBJDOC.w_MVCODCON;
           ,ALLTRIM(this.w_OBJDOC.w_MVFLVEAC)+ALLTRIM(this.w_OBJDOC.w_MVCLADOC);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.Page_5("L",ah_MsgFormat("Documento %1 generato correttamente",ALLTRIM(STR(this.w_OBJDOC.w_MVNUMDOC))),"",0,this.w_MVSEREVA)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NUMDOCOK = this.w_NUMDOCOK + 1
    * --- Aggiornamento righe di log con seriale temporaneo "XXXXXXXXXX"
    * --- Write into LOG_GEND
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.LOG_GEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.LOG_GEND_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LGSERDES ="+cp_NullLink(cp_ToStrODBC(this.w_MVSEREVA),'LOG_GEND','LGSERDES');
          +i_ccchkf ;
      +" where ";
          +"LGSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
          +" and LGSERDES = "+cp_ToStrODBC("XXXXXXXXXX");
             )
    else
      update (i_cTable) set;
          LGSERDES = this.w_MVSEREVA;
          &i_ccchkf. ;
       where;
          LGSERIAL = this.w_SERIALE;
          and LGSERDES = "XXXXXXXXXX";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_DCFLEVAS<>"S"
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM"
        do vq_exec with '..\vefa\exe\query\gsva5bps',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLEVAS = _t2.MVFLEVAS";
            +",MVIMPEVA = _t2.MVIMPEVA";
            +",MVQTAEVA = _t2.MVQTAEVA";
            +",MVQTAEV1 = _t2.MVQTAEV1";
            +",MVEFFEVA = _t2.MVEFFEVA";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
            +"DOC_DETT.MVFLEVAS = _t2.MVFLEVAS";
            +",DOC_DETT.MVIMPEVA = _t2.MVIMPEVA";
            +",DOC_DETT.MVQTAEVA = _t2.MVQTAEVA";
            +",DOC_DETT.MVQTAEV1 = _t2.MVQTAEV1";
            +",DOC_DETT.MVEFFEVA = _t2.MVEFFEVA";
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVFLEVAS,";
            +"MVIMPEVA,";
            +"MVQTAEVA,";
            +"MVQTAEV1,";
            +"MVEFFEVA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.MVFLEVAS,";
            +"t2.MVIMPEVA,";
            +"t2.MVQTAEVA,";
            +"t2.MVQTAEV1,";
            +"t2.MVEFFEVA";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
            +"MVFLEVAS = _t2.MVFLEVAS";
            +",MVIMPEVA = _t2.MVIMPEVA";
            +",MVQTAEVA = _t2.MVQTAEVA";
            +",MVQTAEV1 = _t2.MVQTAEV1";
            +",MVEFFEVA = _t2.MVEFFEVA";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVIMPEVA = (select MVIMPEVA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVQTAEVA = (select MVQTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVQTAEV1 = (select MVQTAEV1 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVEFFEVA = (select MVEFFEVA from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_ASNUMDDT = -20
    * --- Select from ..\vefa\exe\query\gsva5bps
    do vq_exec with '..\vefa\exe\query\gsva5bps',this,'_Curs__d__d__vefa_exe_query_gsva5bps','',.f.,.t.
    if used('_Curs__d__d__vefa_exe_query_gsva5bps')
      select _Curs__d__d__vefa_exe_query_gsva5bps
      locate for 1=1
      do while not(eof())
      this.w_ASSERIAL = MVSERIAL
      this.w_ASROWNUM = CPROWNUM
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_ASSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ASROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_ASNUMDDT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVCODART,MVCODCOM;
          from (i_cTable) where;
              MVSERIAL = this.w_ASSERIAL;
              and CPROWNUM = this.w_ASROWNUM;
              and MVNUMRIF = this.w_ASNUMDDT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ASCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
        this.w_ASFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
        this.w_ASFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
        this.w_ASFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
        this.w_ASCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
        this.w_ASF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
        this.w_ASF2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
        this.w_ASF2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
        this.w_ASQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
        this.w_ASKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
        this.w_ASCODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
        this.w_ASCODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_ASFLRISE) Or Not Empty(this.w_ASF2RISE)
        * --- Documento non Accettato (movimenta riservato)
        this.Page_5("E",ah_MsgFormat("Impossibile generare doc.: %1%0Movimenta il riservato",ALLTRIM(STR(this.w_OBJDOC.w_MVNUMDOC))),"",0,this.w_MVSEREVA)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Raise
        i_Error=ah_MsgFormat("Impossibile generare doc.: %1%0Movimenta il riservato", ALLTRIM(STR(this.w_OBJDOC.w_MVNUMDOC)) )
        return
      else
        if NOT EMPTY(this.w_ASFLORDI+this.w_ASFLIMPE+this.w_ASF2ORDI+this.w_ASF2IMPE) AND this.w_DCFLEVAS<>"S"
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_ASCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_ASCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_ASCODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_ASCODCOM
            endif
          endif
          * --- Aggiorno saldi MVCODMAG
          this.w_FLIMPE = ICASE(this.w_ASFLIMPE="+","-", this.w_ASFLIMPE="-","+"," ")
          this.w_FLORDI = ICASE(this.w_ASFLORDI="+","-", this.w_ASFLORDI="-","+"," ")
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_ASKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_ASCODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_ASKEYSAL;
                and SLCODMAG = this.w_ASCODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_SALCOM="S"
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_ASKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_ASCODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_ASKEYSAL;
                  and SCCODMAG = this.w_ASCODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
          * --- Aggiorno saldi MVCODMAT
          this.w_FLIMPE = ICASE(this.w_ASF2IMPE="+","-", this.w_ASF2IMPE="-","+"," ")
          this.w_FLORDI = ICASE(this.w_ASF2ORDI="+","-", this.w_ASF2ORDI="-","+"," ")
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = w_MVKEYSAL;
                and SLCODMAG = w_MVCODMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_SALCOM="S"
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_ASQTASAL',this.w_ASQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(w_MVKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(w_MVCODMAT);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = w_MVKEYSAL;
                  and SCCODMAG = w_MVCODMAT;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
          this.w_ASQTASAL = 0
        endif
      endif
        select _Curs__d__d__vefa_exe_query_gsva5bps
        continue
      enddo
      use
    endif
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM"
      do vq_exec with '..\vefa\exe\query\gsva5bps',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTASAL = _t2.MVQTASAL";
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVQTASAL = _t2.MVQTASAL";
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVQTASAL";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.MVQTASAL";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVQTASAL = _t2.MVQTASAL";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTASAL = (select MVQTASAL from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ORDERBY = ""
    * --- Select from VACONPIA
    i_nConn=i_TableProp[this.VACONPIA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VACONPIA_idx,2],.t.,this.VACONPIA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CPNOMCAM, CPFLORDI  from "+i_cTable+" VACONPIA ";
          +" where CPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PSSERCOR)+" AND CPFLORDI <> 'N'";
          +" order by CPROWORD";
           ,"_Curs_VACONPIA")
    else
      select CPNOMCAM, CPFLORDI from (i_cTable);
       where CPSERIAL = this.oParentObject.w_PSSERCOR AND CPFLORDI <> "N";
       order by CPROWORD;
        into cursor _Curs_VACONPIA
    endif
    if used('_Curs_VACONPIA')
      select _Curs_VACONPIA
      locate for 1=1
      do while not(eof())
      this.w_ORDERBY = this.w_ORDERBY + ALLTRIM(_Curs_VACONPIA.CPNOMCAM) + IIF(_Curs_VACONPIA.CPFLORDI="A", "", " DESC" ) + ","
        select _Curs_VACONPIA
        continue
      enddo
      use
    endif
    * --- Read from DATI_AGG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DATI_AGG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DATI_AGG_idx,2],.t.,this.DATI_AGG_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DAPRI_01,DAPRI_02,DAPRI_03,DAPRI_04,DAPRI_05,DAPRI_06,DAORD_01,DAORD_02,DAORD_03,DAORD_04,DAORD_05,DAORD_06"+;
        " from "+i_cTable+" DATI_AGG where ";
            +"DASERIAL = "+cp_ToStrODBC("0000000001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DAPRI_01,DAPRI_02,DAPRI_03,DAPRI_04,DAPRI_05,DAPRI_06,DAORD_01,DAORD_02,DAORD_03,DAORD_04,DAORD_05,DAORD_06;
        from (i_cTable) where;
            DASERIAL = "0000000001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DAPRI_01 = NVL(cp_ToDate(_read_.DAPRI_01),cp_NullValue(_read_.DAPRI_01))
      this.w_DAPRI_02 = NVL(cp_ToDate(_read_.DAPRI_02),cp_NullValue(_read_.DAPRI_02))
      this.w_DAPRI_03 = NVL(cp_ToDate(_read_.DAPRI_03),cp_NullValue(_read_.DAPRI_03))
      this.w_DAPRI_04 = NVL(cp_ToDate(_read_.DAPRI_04),cp_NullValue(_read_.DAPRI_04))
      this.w_DAPRI_05 = NVL(cp_ToDate(_read_.DAPRI_05),cp_NullValue(_read_.DAPRI_05))
      this.w_DAPRI_06 = NVL(cp_ToDate(_read_.DAPRI_06),cp_NullValue(_read_.DAPRI_06))
      this.w_DAORD_01 = NVL(cp_ToDate(_read_.DAORD_01),cp_NullValue(_read_.DAORD_01))
      this.w_DAORD_02 = NVL(cp_ToDate(_read_.DAORD_02),cp_NullValue(_read_.DAORD_02))
      this.w_DAORD_03 = NVL(cp_ToDate(_read_.DAORD_03),cp_NullValue(_read_.DAORD_03))
      this.w_DAORD_04 = NVL(cp_ToDate(_read_.DAORD_04),cp_NullValue(_read_.DAORD_04))
      this.w_DAORD_05 = NVL(cp_ToDate(_read_.DAORD_05),cp_NullValue(_read_.DAORD_05))
      this.w_DAORD_06 = NVL(cp_ToDate(_read_.DAORD_06),cp_NullValue(_read_.DAORD_06))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DATAGGPO = ""
    this.w_DATAGGAN = ""
    this.w_IDXFLDAN = 1
    this.w_DO_ORDER = "P" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "D" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "S" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    this.w_DO_ORDER = this.w_DO_ORDER OR "F" $ ALLTRIM(this.w_DAORD_01)+ALLTRIM(this.w_DAORD_02)+ALLTRIM(this.w_DAORD_03)+ALLTRIM(this.w_DAORD_04)+ALLTRIM(this.w_DAORD_05)+ALLTRIM(this.w_DAORD_06)
    if this.w_DO_ORDER
      private L_MACRO
      do while this.w_IDXFLDAN<=6
        * --- Ricerco il campo con priorit� pi� alta
        this.w_IDXFLDAT = 1
        L_MACRO = "this.w_DAPRI_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
        this.w_PRIATTFL = &L_MACRO
        do while this.w_PRIATTFL <> this.w_IDXFLDAN
          this.w_IDXFLDAT = this.w_IDXFLDAT + 1
          L_MACRO = "this.w_DAPRI_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
          this.w_PRIATTFL = &L_MACRO
        enddo
        L_MACRO = "this.oParentobject.w_TDFLIA"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
        TestMacro=&L_MACRO
        if TestMacro = "S"
          L_MACRO = "this.w_DAORD_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)
          * --- Se il campo aggiuntivo non deve essere ignorato per gli ordinamenti lo metto nelle
          *     variabili dedicate per farlo anticipare o seguire gli ordinamenti standard
          do case
            case &L_MACRO = "P"
              this.w_DATAGGAN = this.w_DATAGGAN + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+", "
            case &L_MACRO = "D"
              this.w_DATAGGAN = this.w_DATAGGAN + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+" DESC, "
            case &L_MACRO = "S"
              this.w_DATAGGPO = this.w_DATAGGPO + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+", "
            case &L_MACRO = "F"
              this.w_DATAGGPO = this.w_DATAGGPO + "MVAGG_"+RIGHT("00"+ALLTRIM(STR(this.w_IDXFLDAT)), 2)+" DESC, "
          endcase
        endif
        this.w_IDXFLDAN = this.w_IDXFLDAN + 1
      enddo
      release L_MACRO
    endif
    this.w_ORDERBY = LEFT(EVL(this.w_ORDERBY, ","), LEN(EVL(this.w_ORDERBY, ",")) -1 )
    this.w_ORDERBY = this.w_DATAGGAN + this.w_ORDERBY
    this.w_ORDERBY = STRTRAN(this.w_ORDERBY, "@@DATIAGGPOSTORDER@@", this.w_DATAGGPO, 1, 1, 2) 
    this.w_ORDERBY = STRTRAN(this.w_ORDERBY, ",,", ",", 1, -1, 2) 
    this.w_ORDERBY = STRTRAN(this.w_ORDERBY, ", ,", ",", 1, -1, 2) 
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='TMPPIASP'
    this.cWorkTables[4]='VACONPIA'
    this.cWorkTables[5]='GENERDOC'
    this.cWorkTables[6]='LOG_GEND'
    this.cWorkTables[7]='DET_PIAS'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='VOCIIVA'
    this.cWorkTables[10]='DATI_AGG'
    this.cWorkTables[11]='SALDIART'
    this.cWorkTables[12]='DOC_COLL'
    this.cWorkTables[13]='PIA_SPED'
    this.cWorkTables[14]='CAM_AGAZ'
    this.cWorkTables[15]='ART_ICOL'
    this.cWorkTables[16]='SALDICOM'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs__d__d__vefa_exe_query_gsva_bps')
      use in _Curs__d__d__vefa_exe_query_gsva_bps
    endif
    if used('_Curs_VACONPIA')
      use in _Curs_VACONPIA
    endif
    if used('_Curs_DATI_AGG')
      use in _Curs_DATI_AGG
    endif
    if used('_Curs__d__d__vefa_exe_query_gsva8bps')
      use in _Curs__d__d__vefa_exe_query_gsva8bps
    endif
    if used('_Curs__d__d__vefa_exe_query_gsva5bps')
      use in _Curs__d__d__vefa_exe_query_gsva5bps
    endif
    if used('_Curs_VACONPIA')
      use in _Curs_VACONPIA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
