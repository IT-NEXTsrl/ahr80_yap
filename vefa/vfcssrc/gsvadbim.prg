* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsvadbim                                                        *
*              IMPORT FILE EDI DETTAGLIO                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-22                                                      *
* Last revis.: 2012-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsvadbim",oParentObject)
return(i_retval)

define class tgsvadbim as StdBatch
  * --- Local variables
  w_SERGDOC = space(10)
  w_LGSERORI = space(10)
  w_LGROWORI = 0
  w_LGTIPRIG = space(1)
  w_LGMESSAG = space(254)
  * --- WorkFile variables
  LOG_GEND_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_SERGDOC = this.oParentObject.oParentObject.w_SERGDOC
    if !EMPTY(NVL(this.w_SERGDOC, " "))
      * --- Select from LOG_GEND
      i_nConn=i_TableProp[this.LOG_GEND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LGTIPRIG ,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM  from "+i_cTable+" LOG_GEND ";
            +" where LGSERIAL="+cp_ToStrODBC(this.w_SERGDOC)+" AND LGTIPRIG<>'L'";
            +" group by LGTIPRIG,LGSERORI, LGROWORI, LGMESSAG, CPROWNUM";
            +" order by CPROWNUM";
             ,"_Curs_LOG_GEND")
      else
        select LGTIPRIG ,LGMESSAG,LGSERORI,LGROWORI,CPROWNUM from (i_cTable);
         where LGSERIAL=this.w_SERGDOC AND LGTIPRIG<>"L";
         group by LGTIPRIG,LGSERORI, LGROWORI, LGMESSAG, CPROWNUM;
         order by CPROWNUM;
          into cursor _Curs_LOG_GEND
      endif
      if used('_Curs_LOG_GEND')
        select _Curs_LOG_GEND
        locate for 1=1
        do while not(eof())
        this.w_LGSERORI = _Curs_LOG_GEND.LGSERORI
        this.w_LGROWORI = _Curs_LOG_GEND.LGROWORI
        this.w_LGTIPRIG = _Curs_LOG_GEND.LGTIPRIG
        if NVL(this.w_LGROWORI, 0) >0
          this.w_LGMESSAG = ALLTRIM(_Curs_LOG_GEND.LGMESSAG)
          UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET ERRORMSG = IIF(EMPTY(NVL(ERRORMSG, " ")), "", ERRORMSG+CHR(10))+this.w_LGMESSAG WHERE MVSERIAL=this.w_LGSERORI AND CPROWNUM=this.w_LGROWORI
        endif
          select _Curs_LOG_GEND
          continue
        enddo
        use
      endif
      UPDATE (this.oParentObject.w_ZOOMDOC.cCURSOR) SET TIPREC=IIF(EMPTY(NVL(ERRORMSG, " ")), "O", "E")
      SELECT (this.oParentObject.w_ZOOMDOC.cCURSOR)
      GO TOP
      this.oParentObject.w_ERRORLOG = ALLTRIM(ERRORMSG)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOG_GEND'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_LOG_GEND')
      use in _Curs_LOG_GEND
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
