* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kds                                                        *
*              Duplicazione struttura                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-14                                                      *
* Last revis.: 2008-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kds",oParentObject))

* --- Class definition
define class tgsva_kds as StdForm
  Top    = 15
  Left   = 11

  * --- Standard Properties
  Width  = 729
  Height = 262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-12-22"
  HelpContextID=82473577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  VASTRUTT_IDX = 0
  ENT_MAST_IDX = 0
  VAELEMEN_IDX = 0
  VAFORMAT_IDX = 0
  cPrg = "gsva_kds"
  cComment = "Duplicazione struttura"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_STRORI = space(10)
  o_STRORI = space(10)
  w_CODSTR = space(10)
  o_CODSTR = space(10)
  w_DESNEW = space(30)
  w_PREDEF = space(1)
  w_ELEMORIG = space(20)
  w_ELEMDEST = space(20)
  w_CODENTOR = space(10)
  w_CODENTDE = space(10)
  w_CODENT = space(15)
  w_CODROOT = space(20)
  w_FORTAG = space(20)
  w_STNOMFIL = space(150)
  w_DESSTR = space(30)
  w_DESENT = space(50)
  w_CHKORSTR = space(10)
  w_STELROOT = space(20)
  w_STNOMFOR = space(150)
  w_STNOMFDE = space(150)
  w_FORTAGOR = space(20)
  w_FORTAGDE = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kdsPag1","gsva_kds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTRORI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='ENT_MAST'
    this.cWorkTables[3]='VAELEMEN'
    this.cWorkTables[4]='VAFORMAT'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STRORI=space(10)
      .w_CODSTR=space(10)
      .w_DESNEW=space(30)
      .w_PREDEF=space(1)
      .w_ELEMORIG=space(20)
      .w_ELEMDEST=space(20)
      .w_CODENTOR=space(10)
      .w_CODENTDE=space(10)
      .w_CODENT=space(15)
      .w_CODROOT=space(20)
      .w_FORTAG=space(20)
      .w_STNOMFIL=space(150)
      .w_DESSTR=space(30)
      .w_DESENT=space(50)
      .w_CHKORSTR=space(10)
      .w_STELROOT=space(20)
      .w_STNOMFOR=space(150)
      .w_STNOMFDE=space(150)
      .w_FORTAGOR=space(20)
      .w_FORTAGDE=space(20)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_STRORI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODSTR))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_ELEMORIG = IIF(EMPTY(.w_CHKORSTR) OR EMPTY(.w_ELEMORIG), .w_STELROOT, .w_ELEMORIG)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ELEMORIG))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ELEMDEST))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,8,.f.)
        .w_CODENT = IIF(EMPTY(.w_CHKORSTR), .w_CODENTOR, .w_CODENTDE)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODENT))
          .link_1_9('Full')
        endif
        .w_CODROOT = .w_STELROOT
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODROOT))
          .link_1_10('Full')
        endif
        .w_FORTAG = IIF(EMPTY(.w_CHKORSTR), .w_FORTAGOR, .w_FORTAGDE)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_FORTAG))
          .link_1_11('Full')
        endif
        .w_STNOMFIL = IIF(EMPTY(.w_CHKORSTR), .w_STNOMFOR, .w_STNOMFDE)
    endwith
    this.DoRTCalc(13,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CODSTR<>.w_CODSTR
            .w_ELEMORIG = IIF(EMPTY(.w_CHKORSTR) OR EMPTY(.w_ELEMORIG), .w_STELROOT, .w_ELEMORIG)
          .link_1_5('Full')
        endif
        .DoRTCalc(6,8,.t.)
        if .o_STRORI<>.w_STRORI.or. .o_CODSTR<>.w_CODSTR
            .w_CODENT = IIF(EMPTY(.w_CHKORSTR), .w_CODENTOR, .w_CODENTDE)
          .link_1_9('Full')
        endif
        if .o_STRORI<>.w_STRORI
            .w_CODROOT = .w_STELROOT
          .link_1_10('Full')
        endif
        if .o_STRORI<>.w_STRORI.or. .o_CODSTR<>.w_CODSTR
            .w_FORTAG = IIF(EMPTY(.w_CHKORSTR), .w_FORTAGOR, .w_FORTAGDE)
          .link_1_11('Full')
        endif
        if .o_STRORI<>.w_STRORI.or. .o_CODSTR<>.w_CODSTR
            .w_STNOMFIL = IIF(EMPTY(.w_CHKORSTR), .w_STNOMFOR, .w_STNOMFDE)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDESNEW_1_3.enabled = this.oPgFrm.Page1.oPag.oDESNEW_1_3.mCond()
    this.oPgFrm.Page1.oPag.oELEMORIG_1_5.enabled = this.oPgFrm.Page1.oPag.oELEMORIG_1_5.mCond()
    this.oPgFrm.Page1.oPag.oELEMDEST_1_6.enabled = this.oPgFrm.Page1.oPag.oELEMDEST_1_6.mCond()
    this.oPgFrm.Page1.oPag.oFORTAG_1_11.enabled = this.oPgFrm.Page1.oPag.oFORTAG_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSTNOMFIL_1_12.enabled = this.oPgFrm.Page1.oPag.oSTNOMFIL_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=STRORI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_STRORI)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_STRORI))
          select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRORI)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRORI) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oSTRORI_1_1'),i_cWhere,'GSVA_AST',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_STRORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_STRORI)
            select STCODICE,STDESCRI,STFLPRED,STCODENT,STELROOT,STFORTAG,STNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRORI = NVL(_Link_.STCODICE,space(10))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_PREDEF = NVL(_Link_.STFLPRED,space(1))
      this.w_CODENTOR = NVL(_Link_.STCODENT,space(10))
      this.w_STELROOT = NVL(_Link_.STELROOT,space(20))
      this.w_FORTAGOR = NVL(_Link_.STFORTAG,space(20))
      this.w_STNOMFOR = NVL(_Link_.STNOMFIL,space(150))
    else
      if i_cCtrl<>'Load'
        this.w_STRORI = space(10)
      endif
      this.w_DESSTR = space(30)
      this.w_PREDEF = space(1)
      this.w_CODENTOR = space(10)
      this.w_STELROOT = space(20)
      this.w_FORTAGOR = space(20)
      this.w_STNOMFOR = space(150)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSTR
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AST',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_CODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_CODSTR))
          select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oCODSTR_1_2'),i_cWhere,'GSVA_AST',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_CODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_CODSTR)
            select STCODICE,STDESCRI,STELROOT,STCODENT,STNOMFIL,STFORTAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_CODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_DESNEW = NVL(_Link_.STDESCRI,space(30))
      this.w_ELEMDEST = NVL(_Link_.STELROOT,space(20))
      this.w_CHKORSTR = NVL(_Link_.STCODICE,space(10))
      this.w_CODENTDE = NVL(_Link_.STCODENT,space(10))
      this.w_STNOMFDE = NVL(_Link_.STNOMFIL,space(150))
      this.w_FORTAGDE = NVL(_Link_.STFORTAG,space(20))
    else
      this.w_DESNEW = space(30)
      this.w_ELEMDEST = space(20)
      this.w_CHKORSTR = space(10)
      this.w_CODENTDE = space(10)
      this.w_STNOMFDE = space(150)
      this.w_FORTAGDE = space(20)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELEMORIG
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELEMORIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ELEMORIG)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STRORI);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_STRORI;
                     ,'ELCODICE',trim(this.w_ELEMORIG))
          select ELCODSTR,ELCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELEMORIG)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELEMORIG) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oELEMORIG_1_5'),i_cWhere,'GSVA_AEL',"Elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STRORI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_STRORI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELEMORIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ELEMORIG);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STRORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_STRORI;
                       ,'ELCODICE',this.w_ELEMORIG)
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELEMORIG = NVL(_Link_.ELCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ELEMORIG = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELEMORIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELEMDEST
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELEMDEST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ELEMDEST)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_CHKORSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_CHKORSTR;
                     ,'ELCODICE',trim(this.w_ELEMDEST))
          select ELCODSTR,ELCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELEMDEST)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELEMDEST) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oELEMDEST_1_6'),i_cWhere,'GSVA_AEL',"Elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CHKORSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_CHKORSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELEMDEST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ELEMDEST);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_CHKORSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_CHKORSTR;
                       ,'ELCODICE',this.w_ELEMDEST)
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELEMDEST = NVL(_Link_.ELCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ELEMDEST = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELEMDEST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODENT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_lTable = "ENT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2], .t., this.ENT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODENT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODENT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,ENDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ENCODICE="+cp_ToStrODBC(this.w_CODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_CODENT)
            select ENCODICE,ENDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODENT = NVL(_Link_.ENCODICE,space(15))
      this.w_DESENT = NVL(_Link_.ENDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODENT = space(15)
      endif
      this.w_DESENT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)
      cp_ShowWarn(i_cKey,this.ENT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODENT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODROOT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODROOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODROOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_CODROOT);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_STRORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_STRORI;
                       ,'ELCODICE',this.w_CODROOT)
            select ELCODSTR,ELCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODROOT = NVL(_Link_.ELCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODROOT = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODROOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORTAG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_lTable = "VAFORMAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2], .t., this.VAFORMAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORTAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_AFO',True,'VAFORMAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_FORTAG)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_STRORI);

          i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODSTR,FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODSTR',this.w_STRORI;
                     ,'FOCODICE',trim(this.w_FORTAG))
          select FOCODSTR,FOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODSTR,FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORTAG)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORTAG) and !this.bDontReportError
            deferred_cp_zoom('VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(oSource.parent,'oFORTAG_1_11'),i_cWhere,'GSVA_AFO',"Formati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STRORI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FOCODSTR,FOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FOCODSTR="+cp_ToStrODBC(this.w_STRORI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',oSource.xKey(1);
                       ,'FOCODICE',oSource.xKey(2))
            select FOCODSTR,FOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORTAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_FORTAG);
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_STRORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',this.w_STRORI;
                       ,'FOCODICE',this.w_FORTAG)
            select FOCODSTR,FOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORTAG = NVL(_Link_.FOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FORTAG = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODSTR,1)+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.VAFORMAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORTAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTRORI_1_1.value==this.w_STRORI)
      this.oPgFrm.Page1.oPag.oSTRORI_1_1.value=this.w_STRORI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSTR_1_2.value==this.w_CODSTR)
      this.oPgFrm.Page1.oPag.oCODSTR_1_2.value=this.w_CODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNEW_1_3.value==this.w_DESNEW)
      this.oPgFrm.Page1.oPag.oDESNEW_1_3.value=this.w_DESNEW
    endif
    if not(this.oPgFrm.Page1.oPag.oPREDEF_1_4.RadioValue()==this.w_PREDEF)
      this.oPgFrm.Page1.oPag.oPREDEF_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELEMORIG_1_5.value==this.w_ELEMORIG)
      this.oPgFrm.Page1.oPag.oELEMORIG_1_5.value=this.w_ELEMORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oELEMDEST_1_6.value==this.w_ELEMDEST)
      this.oPgFrm.Page1.oPag.oELEMDEST_1_6.value=this.w_ELEMDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCODENT_1_9.value==this.w_CODENT)
      this.oPgFrm.Page1.oPag.oCODENT_1_9.value=this.w_CODENT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODROOT_1_10.value==this.w_CODROOT)
      this.oPgFrm.Page1.oPag.oCODROOT_1_10.value=this.w_CODROOT
    endif
    if not(this.oPgFrm.Page1.oPag.oFORTAG_1_11.value==this.w_FORTAG)
      this.oPgFrm.Page1.oPag.oFORTAG_1_11.value=this.w_FORTAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTNOMFIL_1_12.value==this.w_STNOMFIL)
      this.oPgFrm.Page1.oPag.oSTNOMFIL_1_12.value=this.w_STNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_15.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_15.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESENT_1_18.value==this.w_DESENT)
      this.oPgFrm.Page1.oPag.oDESENT_1_18.value=this.w_DESENT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DESNEW))  and (!EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESNEW_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DESNEW)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ELEMORIG))  and (Not Empty(.w_STRORI) and !EMPTY(.w_CHKORSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELEMORIG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ELEMORIG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ELEMDEST))  and (Not Empty(.w_CODSTR) and !EMPTY(.w_CHKORSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELEMDEST_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ELEMDEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODENT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODENT_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CODENT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FORTAG))  and (Not Empty(.w_STRORI) and !EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORTAG_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FORTAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_STNOMFIL))  and (Not Empty(.w_STRORI) and !EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTNOMFIL_1_12.SetFocus()
            i_bnoObbl = !empty(.w_STNOMFIL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STRORI = this.w_STRORI
    this.o_CODSTR = this.w_CODSTR
    return

enddefine

* --- Define pages as container
define class tgsva_kdsPag1 as StdContainer
  Width  = 725
  height = 262
  stdWidth  = 725
  stdheight = 262
  resizeXpos=350
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTRORI_1_1 as StdField with uid="BQZEUOSBON",rtseq=1,rtrep=.f.,;
    cFormVar = "w_STRORI", cQueryName = "STRORI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura originaria",;
    HelpContextID = 108394714,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=223, Top=6, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_STRORI"

  func oSTRORI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_ELEMORIG)
        bRes2=.link_1_5('Full')
      endif
      if .not. empty(.w_CODROOT)
        bRes2=.link_1_10('Full')
      endif
      if .not. empty(.w_FORTAG)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oSTRORI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRORI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oSTRORI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Strutture",'',this.parent.oContained
  endproc
  proc oSTRORI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_STRORI
     i_obj.ecpSave()
  endproc

  add object oCODSTR_1_2 as StdField with uid="ITVDRFKDOP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODSTR", cQueryName = "CODSTR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Nuovo codice struttura",;
    HelpContextID = 223534810,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=223, Top=34, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", cZoomOnZoom="GSVA_AST", oKey_1_1="STCODICE", oKey_1_2="this.w_CODSTR"

  func oCODSTR_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSTR_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSTR_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oCODSTR_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AST',"Strutture",'',this.parent.oContained
  endproc
  proc oCODSTR_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AST()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_STCODICE=this.parent.oContained.w_CODSTR
     i_obj.ecpSave()
  endproc

  add object oDESNEW_1_3 as StdField with uid="KKDXWOMCBA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESNEW", cQueryName = "DESNEW",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 155646154,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=323, Top=34, InputMask=replicate('X',30)

  func oDESNEW_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
    endwith
   endif
  endfunc

  add object oPREDEF_1_4 as StdCheck with uid="ZNSWLKRPAA",rtseq=4,rtrep=.f.,left=553, top=35, caption="Predefinita",;
    ToolTipText = "Se attivo imposta la nuova struttura come predefinita",;
    HelpContextID = 173132554,;
    cFormVar="w_PREDEF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPREDEF_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPREDEF_1_4.GetRadio()
    this.Parent.oContained.w_PREDEF = this.RadioValue()
    return .t.
  endfunc

  func oPREDEF_1_4.SetRadio()
    this.Parent.oContained.w_PREDEF=trim(this.Parent.oContained.w_PREDEF)
    this.value = ;
      iif(this.Parent.oContained.w_PREDEF=='S',1,;
      0)
  endfunc

  add object oELEMORIG_1_5 as StdField with uid="RNJXKIHPGW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELEMORIG", cQueryName = "ELEMORIG",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Elemento origine d'inizio duplicazione",;
    HelpContextID = 229167475,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=223, Top=62, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_STRORI", oKey_2_1="ELCODICE", oKey_2_2="this.w_ELEMORIG"

  func oELEMORIG_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_STRORI) and !EMPTY(.w_CHKORSTR))
    endwith
   endif
  endfunc

  func oELEMORIG_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oELEMORIG_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELEMORIG_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_STRORI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_STRORI)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oELEMORIG_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AEL',"Elementi",'',this.parent.oContained
  endproc
  proc oELEMORIG_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_STRORI
     i_obj.w_ELCODICE=this.parent.oContained.w_ELEMORIG
     i_obj.ecpSave()
  endproc

  add object oELEMDEST_1_6 as StdField with uid="INVGROHQMP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ELEMDEST", cQueryName = "ELEMDEST",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Elemento di destinazione a cui accodare la struttuar duplicata",;
    HelpContextID = 78065306,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=223, Top=90, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_CHKORSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_ELEMDEST"

  func oELEMDEST_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_CODSTR) and !EMPTY(.w_CHKORSTR))
    endwith
   endif
  endfunc

  func oELEMDEST_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oELEMDEST_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELEMDEST_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_CHKORSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_CHKORSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oELEMDEST_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AEL',"Elementi",'',this.parent.oContained
  endproc
  proc oELEMDEST_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_CHKORSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_ELEMDEST
     i_obj.ecpSave()
  endproc

  add object oCODENT_1_9 as StdField with uid="FMWTQMCHFG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODENT", cQueryName = "CODENT",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice entit� associata alla struttura",;
    HelpContextID = 197189338,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=223, Top=118, InputMask=replicate('X',15), cLinkFile="ENT_MAST", cZoomOnZoom="GSAR_BZE", oKey_1_1="ENCODICE", oKey_1_2="this.w_CODENT"

  func oCODENT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODROOT_1_10 as StdField with uid="WKVUEJMVNR",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODROOT", cQueryName = "CODROOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Elemento radice",;
    HelpContextID = 257696038,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=223, Top=147, InputMask=replicate('X',20), cLinkFile="VAELEMEN", cZoomOnZoom="GSVA_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_STRORI", oKey_2_1="ELCODICE", oKey_2_2="this.w_CODROOT"

  func oCODROOT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oFORTAG_1_11 as StdField with uid="LBAGIRRDWI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FORTAG", cQueryName = "FORTAG",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Formato da applicare agli elementi di tipo fisso",;
    HelpContextID = 159448746,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=553, Top=147, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAFORMAT", cZoomOnZoom="GSVA_AFO", oKey_1_1="FOCODSTR", oKey_1_2="this.w_STRORI", oKey_2_1="FOCODICE", oKey_2_2="this.w_FORTAG"

  func oFORTAG_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_STRORI) and !EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
    endwith
   endif
  endfunc

  func oFORTAG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORTAG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORTAG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAFORMAT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_STRORI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStr(this.Parent.oContained.w_STRORI)
    endif
    do cp_zoom with 'VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(this.parent,'oFORTAG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_AFO',"Formati",'',this.parent.oContained
  endproc
  proc oFORTAG_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSVA_AFO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FOCODSTR=w_STRORI
     i_obj.w_FOCODICE=this.parent.oContained.w_FORTAG
     i_obj.ecpSave()
  endproc

  add object oSTNOMFIL_1_12 as StdField with uid="DEGEIOSTWF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_STNOMFIL", cQueryName = "STNOMFIL",;
    bObbl = .t. , nPag = 1, value=space(150), bMultilanguage =  .f.,;
    ToolTipText = "Nome file generazione EDI",;
    HelpContextID = 104449906,;
   bGlobalFont=.t.,;
    Height=21, Width=484, Left=223, Top=178, InputMask=replicate('X',150)

  func oSTNOMFIL_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_STRORI) and !EMPTY(.w_CODSTR) AND EMPTY(.w_CHKORSTR))
    endwith
   endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="FRSHLUKCSC",left=605, top=209, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Duplica struttura";
    , HelpContextID = 82453018;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSVA_BDS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODSTR) AND .w_CODSTR<> .w_STRORI AND Not Empty(.w_STRORI))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="YEDHIQOSNF",left=659, top=209, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 240246790;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSTR_1_15 as StdField with uid="EDRRWFASFK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 223475914,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=323, Top=6, InputMask=replicate('X',30)

  add object oDESENT_1_18 as StdField with uid="VWSDFQKWUT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESENT", cQueryName = "DESENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 197130442,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=343, Top=118, InputMask=replicate('X',50)

  add object oStr_1_16 as StdString with uid="CIQCDBRVMO",Visible=.t., Left=8, Top=35,;
    Alignment=1, Width=211, Height=18,;
    Caption="Struttura destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="UVTJRLDTRY",Visible=.t., Left=8, Top=8,;
    Alignment=1, Width=211, Height=18,;
    Caption="Struttura origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KONGWGHTUY",Visible=.t., Left=8, Top=120,;
    Alignment=1, Width=211, Height=18,;
    Caption="Entit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IKMQYDQVVR",Visible=.t., Left=8, Top=149,;
    Alignment=1, Width=211, Height=18,;
    Caption="Elemento radice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UUEFKPXKPU",Visible=.t., Left=388, Top=149,;
    Alignment=1, Width=164, Height=18,;
    Caption="Formato gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="OFVOFQLTIL",Visible=.t., Left=8, Top=179,;
    Alignment=1, Width=211, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CYEPISGLOR",Visible=.t., Left=8, Top=64,;
    Alignment=1, Width=211, Height=18,;
    Caption="Elemento padre origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KADBUYPOZK",Visible=.t., Left=8, Top=92,;
    Alignment=1, Width=211, Height=18,;
    Caption="Elemento padre destinazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kds','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
