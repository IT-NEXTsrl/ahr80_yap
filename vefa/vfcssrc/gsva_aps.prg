* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_aps                                                        *
*              Piani di spedizione                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-09-12                                                      *
* Last revis.: 2012-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_aps"))

* --- Class definition
define class tgsva_aps as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 780
  Height = 527+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-07"
  HelpContextID=160068201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=135

  * --- Constant Properties
  PIA_SPED_IDX = 0
  TIP_DOCU_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  VACONPIA_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  GENERDOC_IDX = 0
  AZIENDA_IDX = 0
  DOC_COLL_IDX = 0
  cFile = "PIA_SPED"
  cKeySelect = "PSSERIAL"
  cKeyWhere  = "PSSERIAL=this.w_PSSERIAL"
  cKeyWhereODBC = '"PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cKeyWhereODBCqualified = '"PIA_SPED.PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cPrg = "gsva_aps"
  cComment = "Piani di spedizione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_PSSERIAL = space(10)
  o_PSSERIAL = space(10)
  w_PSNUMREG = 0
  w_PSDATREG = ctod('  /  /  ')
  o_PSDATREG = ctod('  /  /  ')
  w_PSSTATUS = space(1)
  o_PSSTATUS = space(1)
  w_PSANNREG = space(4)
  w_PSCODESE = space(4)
  w_PSDESCRI = space(40)
  w_PSTIPDOC = space(5)
  o_PSTIPDOC = space(5)
  w_PSNUMDOC = 0
  w_PSALFDOC = space(10)
  o_PSALFDOC = space(10)
  w_PSDATDOC = ctod('  /  /  ')
  o_PSDATDOC = ctod('  /  /  ')
  w_PSSERCOR = space(10)
  w_PSCODMAG = space(5)
  w_PSCLAORI = space(2)
  o_PSCLAORI = space(2)
  w_PSTIPORI = space(5)
  w_PSEVAINI = ctod('  /  /  ')
  w_PSEVAFIN = ctod('  /  /  ')
  w_PSFLDISP = space(1)
  w_PSCODVAL = space(3)
  w_PSFLGFID = space(1)
  w_PSCODCLF = space(15)
  o_PSCODCLF = space(15)
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_PSCODDES = space(5)
  w_PSCODZON = space(3)
  w_PSCODAGE = space(5)
  w_PSDATTRA = ctod('  /  /  ')
  w_PSORATRA = space(2)
  w_PSMINTRA = space(2)
  w_PSNOTAGG = space(40)
  w_STADOCIM = space(1)
  w_STREPSEC = space(1)
  w_DESDOC = space(35)
  w_MVFLVEAC = space(1)
  w_PSPRD = space(2)
  w_MVCLADOC = space(2)
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVTCAMAG = space(5)
  w_MVTFRAGG = space(1)
  w_DOCAUCON = space(5)
  w_STAMPA = 0
  w_FLIMAC = space(1)
  w_TDMAGDEF = space(1)
  w_TDFLAPCA1 = space(1)
  w_MVALFEST = space(10)
  w_DFLPP = space(1)
  w_FLPACK = space(1)
  w_FLNSRI = space(1)
  w_TPNSRI = space(3)
  w_FLDTPR = space(1)
  w_TPVSRI = space(3)
  w_FLVSRI = space(1)
  w_CODMAT = space(5)
  w_ASSCES = space(1)
  w_EMERIC = space(1)
  w_CMDES = space(35)
  w_CAUCOL = space(5)
  w_DESORI = space(35)
  w_DESVAL = space(35)
  w_PSTIPCLF = space(1)
  w_DESCLF = space(40)
  w_NOMDES = space(80)
  w_DESZON = space(35)
  w_DESAGE = space(35)
  w_CPDESCRI = space(30)
  w_CPCODCAU = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DTCOBSO = ctod('  /  /  ')
  w_CLAORI = space(2)
  w_ASSCES1 = space(1)
  w_DTMOBSO = ctod('  /  /  ')
  w_CATCON = space(5)
  w_LINGUA = space(3)
  w_DESOBSO = ctod('  /  /  ')
  w_CURSSEDO = space(10)
  w_FLDOCSEL = .F.
  w_TDMODRIF = space(5)
  w_TDDESRIF = space(18)
  w_CMFLRISE = space(1)
  w_CMFLCASC = space(1)
  w_CMFLORDI = space(1)
  w_CMFLIMPE = space(1)
  w_CMF2CASC = space(1)
  w_CMF2ORDI = space(1)
  w_CMF2IMPE = space(1)
  w_CMF2RISE = space(1)
  w_MVFLELGM = space(1)
  w_TDFLRIDE = space(1)
  w_GDSERIAL = space(10)
  w_GDKEYRIF = space(10)
  w_TDFLRISC = space(1)
  w_TDFLCRIS = space(1)
  w_AZFLFIDO = space(1)
  w_TDFLAPCA = space(1)
  w_TDCHKUCA = space(1)
  w_RIIVLSIV = space(0)
  w_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_TDESCCL1 = space(3)
  w_TDESCCL2 = space(3)
  w_TDESCCL3 = space(3)
  w_TDESCCL4 = space(3)
  w_TDESCCL5 = space(3)
  w_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_MGDESMAG = space(30)
  w_TDFLANAL = space(1)
  w_TDFLCOMM = space(1)
  w_DDTIPCON = space(1)
  w_DDCODCON = space(15)
  w_TDMINVEN = space(1)
  w_TDRIOTOT = space(1)
  w_AZMAGAZI = space(5)
  w_TDCODMAG = space(5)
  w_TDFLMGPR = space(1)
  w_TDCAUMAG = space(5)
  w_TDRICNOM = space(1)
  w_TDORDAPE = space(1)
  w_AZFLCAMB = space(1)
  w_TDPRZDES = space(1)
  w_TDASPETT = space(30)
  w_TDALFDOC = space(5)
  w_TDFLSPIN = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PSSERIAL = this.W_PSSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PSANNREG = this.W_PSANNREG
  op_PSNUMREG = this.W_PSNUMREG

  * --- Children pointers
  GSVA_MPS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PIA_SPED','gsva_aps')
    stdPageFrame::Init()
    *set procedure to GSVA_MPS additive
    with this
      .Pages(1).addobject("oPag","tgsva_apsPag1","gsva_aps",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano di spedizione")
      .Pages(1).HelpContextID = 227362488
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVA_MPS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='VACONPIA'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='DES_DIVE'
    this.cWorkTables[8]='ZONE'
    this.cWorkTables[9]='AGENTI'
    this.cWorkTables[10]='GENERDOC'
    this.cWorkTables[11]='AZIENDA'
    this.cWorkTables[12]='DOC_COLL'
    this.cWorkTables[13]='PIA_SPED'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(13))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PIA_SPED_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PIA_SPED_IDX,3]
  return

  function CreateChildren()
    this.GSVA_MPS = CREATEOBJECT('stdLazyChild',this,'GSVA_MPS')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVA_MPS)
      this.GSVA_MPS.DestroyChildrenChain()
      this.GSVA_MPS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVA_MPS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVA_MPS.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVA_MPS.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVA_MPS.SetKey(;
            .w_PSSERIAL,"DPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVA_MPS.ChangeRow(this.cRowID+'      1',1;
             ,.w_PSSERIAL,"DPSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PSSERIAL = NVL(PSSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PIA_SPED where PSSERIAL=KeySet.PSSERIAL
    *
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PIA_SPED')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PIA_SPED.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PIA_SPED '
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLRICIVA = .f.
        .w_STADOCIM = 'S'
        .w_STREPSEC = 'S'
        .w_DESDOC = space(35)
        .w_MVFLVEAC = space(1)
        .w_PSPRD = space(2)
        .w_MVCLADOC = space(2)
        .w_MVFLACCO = space(1)
        .w_MVFLINTE = space(1)
        .w_MVTCAMAG = space(5)
        .w_MVTFRAGG = space(1)
        .w_DOCAUCON = space(5)
        .w_STAMPA = 0
        .w_FLIMAC = space(1)
        .w_TDMAGDEF = space(1)
        .w_TDFLAPCA1 = space(1)
        .w_MVALFEST = space(10)
        .w_DFLPP = space(1)
        .w_FLPACK = space(1)
        .w_FLNSRI = space(1)
        .w_TPNSRI = space(3)
        .w_FLDTPR = space(1)
        .w_TPVSRI = space(3)
        .w_FLVSRI = space(1)
        .w_ASSCES = space(1)
        .w_EMERIC = space(1)
        .w_CMDES = space(35)
        .w_CAUCOL = space(5)
        .w_DESORI = space(35)
        .w_DESVAL = space(35)
        .w_DESCLF = space(40)
        .w_NOMDES = space(80)
        .w_DESZON = space(35)
        .w_DESAGE = space(35)
        .w_CPDESCRI = space(30)
        .w_CPCODCAU = space(5)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DTCOBSO = ctod("  /  /  ")
        .w_CLAORI = space(2)
        .w_ASSCES1 = space(1)
        .w_DTMOBSO = ctod("  /  /  ")
        .w_CATCON = space(5)
        .w_LINGUA = space(3)
        .w_DESOBSO = ctod("  /  /  ")
        .w_CURSSEDO = SYS(2015)
        .w_FLDOCSEL = .F.
        .w_TDMODRIF = space(5)
        .w_TDDESRIF = space(18)
        .w_CMFLRISE = space(1)
        .w_CMFLCASC = space(1)
        .w_CMFLORDI = space(1)
        .w_CMFLIMPE = space(1)
        .w_CMF2CASC = space(1)
        .w_CMF2ORDI = space(1)
        .w_CMF2IMPE = space(1)
        .w_CMF2RISE = space(1)
        .w_MVFLELGM = space(1)
        .w_TDFLRIDE = space(1)
        .w_GDSERIAL = space(10)
        .w_TDFLRISC = space(1)
        .w_TDFLCRIS = space(1)
        .w_AZFLFIDO = space(1)
        .w_TDFLAPCA = space(1)
        .w_TDCHKUCA = space(1)
        .w_RIIVLSIV = space(0)
        .w_RIIVDTIN = ctod("  /  /  ")
        .w_RIIVDTFI = ctod("  /  /  ")
        .w_TDESCCL1 = space(3)
        .w_TDESCCL2 = space(3)
        .w_TDESCCL3 = space(3)
        .w_TDESCCL4 = space(3)
        .w_TDESCCL5 = space(3)
        .w_TDFLIA01 = space(1)
        .w_TDFLIA02 = space(1)
        .w_TDFLIA03 = space(1)
        .w_TDFLIA04 = space(1)
        .w_TDFLIA05 = space(1)
        .w_TDFLIA06 = space(1)
        .w_TDFLRA01 = space(1)
        .w_TDFLRA02 = space(1)
        .w_TDFLRA03 = space(1)
        .w_TDFLRA04 = space(1)
        .w_TDFLRA05 = space(1)
        .w_TDFLRA06 = space(1)
        .w_MGDESMAG = space(30)
        .w_TDFLANAL = space(1)
        .w_TDFLCOMM = space(1)
        .w_TDMINVEN = space(1)
        .w_TDRIOTOT = space(1)
        .w_AZMAGAZI = space(5)
        .w_TDCODMAG = space(5)
        .w_TDFLMGPR = space(1)
        .w_TDCAUMAG = space(5)
        .w_TDRICNOM = space(1)
        .w_TDORDAPE = space(1)
        .w_AZFLCAMB = space(1)
        .w_TDPRZDES = space(1)
        .w_TDASPETT = space(30)
        .w_TDALFDOC = space(5)
        .w_TDFLSPIN = space(1)
        .w_CODAZI = i_CODAZI
          .link_1_1('Load')
        .w_PSSERIAL = NVL(PSSERIAL,space(10))
        .op_PSSERIAL = .w_PSSERIAL
        .w_PSNUMREG = NVL(PSNUMREG,0)
        .op_PSNUMREG = .w_PSNUMREG
        .w_PSDATREG = NVL(cp_ToDate(PSDATREG),ctod("  /  /  "))
        .w_PSSTATUS = NVL(PSSTATUS,space(1))
        .w_PSANNREG = NVL(PSANNREG,space(4))
        .op_PSANNREG = .w_PSANNREG
        .w_PSCODESE = NVL(PSCODESE,space(4))
        .w_PSDESCRI = NVL(PSDESCRI,space(40))
        .w_PSTIPDOC = NVL(PSTIPDOC,space(5))
          if link_1_12_joined
            this.w_PSTIPDOC = NVL(TDTIPDOC112,NVL(this.w_PSTIPDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC112,space(35))
            this.w_MVFLVEAC = NVL(TDFLVEAC112,space(1))
            this.w_PSPRD = NVL(TDPRODOC112,space(2))
            this.w_MVCLADOC = NVL(TDCATDOC112,space(2))
            this.w_MVFLACCO = NVL(TDFLACCO112,space(1))
            this.w_MVFLINTE = NVL(TDFLINTE112,space(1))
            this.w_MVTCAMAG = NVL(TDCAUMAG112,space(5))
            this.w_MVTFRAGG = NVL(TFFLRAGG112,space(1))
            this.w_DOCAUCON = NVL(TDCAUCON112,space(5))
            this.w_STAMPA = NVL(TDPRGSTA112,0)
            this.w_PSALFDOC = NVL(TDALFDOC112,space(10))
            this.w_MVALFEST = NVL(TDSERPRO112,space(10))
            this.w_DFLPP = NVL(TDFLPPRO112,space(1))
            this.w_FLPACK = NVL(TDFLPACK112,space(1))
            this.w_FLNSRI = NVL(TDFLNSRI112,space(1))
            this.w_TPNSRI = NVL(TDTPNDOC112,space(3))
            this.w_FLDTPR = NVL(TDFLDTPR112,space(1))
            this.w_TPVSRI = NVL(TDTPVDOC112,space(3))
            this.w_FLVSRI = NVL(TDFLVSRI112,space(1))
            this.w_TDCODMAG = NVL(TDCODMAG112,space(5))
            this.w_CODMAT = NVL(TDCODMAT112,space(5))
            this.w_ASSCES = NVL(TDASSCES112,space(1))
            this.w_EMERIC = NVL(TDEMERIC112,space(1))
            this.w_FLIMAC = NVL(TDFLIMAC112,space(1))
            this.w_TDFLAPCA1 = NVL(TDFLAPCA112,space(1))
            this.w_TDMODRIF = NVL(TDMODRIF112,space(5))
            this.w_TDDESRIF = NVL(TDDESRIF112,space(18))
            this.w_TDFLRIDE = NVL(TDFLRIDE112,space(1))
            this.w_TDFLRISC = NVL(TDFLRISC112,space(1))
            this.w_TDFLCRIS = NVL(TDFLCRIS112,space(1))
            this.w_TDCHKUCA = NVL(TDCHKUCA112,space(1))
            this.w_TDFLAPCA = NVL(TDFLAPCA112,space(1))
            this.w_TDESCCL1 = NVL(TDESCCL1112,space(3))
            this.w_TDESCCL2 = NVL(TDESCCL2112,space(3))
            this.w_TDESCCL3 = NVL(TDESCCL3112,space(3))
            this.w_TDESCCL4 = NVL(TDESCCL4112,space(3))
            this.w_TDESCCL5 = NVL(TDESCCL5112,space(3))
            this.w_TDFLIA01 = NVL(TDFLIA01112,space(1))
            this.w_TDFLIA02 = NVL(TDFLIA02112,space(1))
            this.w_TDFLIA03 = NVL(TDFLIA03112,space(1))
            this.w_TDFLIA04 = NVL(TDFLIA04112,space(1))
            this.w_TDFLIA05 = NVL(TDFLIA05112,space(1))
            this.w_TDFLIA06 = NVL(TDFLIA06112,space(1))
            this.w_TDFLRA01 = NVL(TDFLRA01112,space(1))
            this.w_TDFLRA02 = NVL(TDFLRA02112,space(1))
            this.w_TDFLRA03 = NVL(TDFLRA03112,space(1))
            this.w_TDFLRA04 = NVL(TDFLRA04112,space(1))
            this.w_TDFLRA05 = NVL(TDFLRA05112,space(1))
            this.w_TDFLRA06 = NVL(TDFLRA06112,space(1))
            this.w_TDFLANAL = NVL(TDFLANAL112,space(1))
            this.w_TDFLCOMM = NVL(TDFLCOMM112,space(1))
            this.w_TDMINVEN = NVL(TDMINVEN112,space(1))
            this.w_TDRIOTOT = NVL(TDRIOTOT112,space(1))
            this.w_TDFLMGPR = NVL(TDFLMGPR112,space(1))
            this.w_TDRICNOM = NVL(TDRICNOM112,space(1))
            this.w_TDPRZDES = NVL(TDPRZDES112,space(1))
            this.w_TDASPETT = NVL(TDASPETT112,space(30))
            this.w_TDALFDOC = NVL(TDALFDOC112,space(5))
            this.w_TDFLSPIN = NVL(TDFLSPIN112,space(1))
          else
          .link_1_12('Load')
          endif
        .w_PSNUMDOC = NVL(PSNUMDOC,0)
        .w_PSALFDOC = NVL(PSALFDOC,space(10))
        .w_PSDATDOC = NVL(cp_ToDate(PSDATDOC),ctod("  /  /  "))
        .w_PSSERCOR = NVL(PSSERCOR,space(10))
          .link_1_16('Load')
        .w_PSCODMAG = NVL(PSCODMAG,space(5))
          if link_1_17_joined
            this.w_PSCODMAG = NVL(MGCODMAG117,NVL(this.w_PSCODMAG,space(5)))
            this.w_DTMOBSO = NVL(cp_ToDate(MGDTOBSO117),ctod("  /  /  "))
            this.w_MGDESMAG = NVL(MGDESMAG117,space(30))
          else
          .link_1_17('Load')
          endif
        .w_PSCLAORI = NVL(PSCLAORI,space(2))
        .w_PSTIPORI = NVL(PSTIPORI,space(5))
          if link_1_19_joined
            this.w_PSTIPORI = NVL(TDTIPDOC119,NVL(this.w_PSTIPORI,space(5)))
            this.w_DESORI = NVL(TDDESDOC119,space(35))
            this.w_CLAORI = NVL(TDCATDOC119,space(2))
            this.w_ASSCES1 = NVL(TDASSCES119,space(1))
            this.w_TDCAUMAG = NVL(TDCAUMAG119,space(5))
            this.w_TDORDAPE = NVL(TDORDAPE119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_PSEVAINI = NVL(cp_ToDate(PSEVAINI),ctod("  /  /  "))
        .w_PSEVAFIN = NVL(cp_ToDate(PSEVAFIN),ctod("  /  /  "))
        .w_PSFLDISP = NVL(PSFLDISP,space(1))
        .w_PSCODVAL = NVL(PSCODVAL,space(3))
          if link_1_23_joined
            this.w_PSCODVAL = NVL(VACODVAL123,NVL(this.w_PSCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL123,space(35))
          else
          .link_1_23('Load')
          endif
        .w_PSFLGFID = NVL(PSFLGFID,space(1))
        .w_PSCODCLF = NVL(PSCODCLF,space(15))
          if link_1_25_joined
            this.w_PSCODCLF = NVL(ANCODICE125,NVL(this.w_PSCODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI125,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO125),ctod("  /  /  "))
            this.w_CATCON = NVL(ANCATCON125,space(5))
            this.w_LINGUA = NVL(ANCODLIN125,space(3))
          else
          .link_1_25('Load')
          endif
        .w_PSCODDES = NVL(PSCODDES,space(5))
          .link_1_27('Load')
        .w_PSCODZON = NVL(PSCODZON,space(3))
          .link_1_28('Load')
        .w_PSCODAGE = NVL(PSCODAGE,space(5))
          .link_1_29('Load')
        .w_PSDATTRA = NVL(cp_ToDate(PSDATTRA),ctod("  /  /  "))
        .w_PSORATRA = NVL(PSORATRA,space(2))
        .w_PSMINTRA = NVL(PSMINTRA,space(2))
        .w_PSNOTAGG = NVL(PSNOTAGG,space(40))
          .link_1_50('Load')
        .w_CODMAT = IIF( .cFunction='Query',Space(5), .w_PSCODMAG )
          .link_1_65('Load')
          .link_1_70('Load')
        .w_PSTIPCLF = NVL(PSTIPCLF,space(1))
        .w_OBTEST = .w_PSDATDOC
        .w_GDKEYRIF = .w_PSSERIAL
          .link_1_133('Load')
        .w_DDTIPCON = .w_PSTIPCLF
        .w_DDCODCON = .w_PSCODCLF
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PIA_SPED')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_131.enabled = this.oPgFrm.Page1.oPag.oBtn_1_131.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_163.enabled = this.oPgFrm.Page1.oPag.oBtn_1_163.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_180.enabled = this.oPgFrm.Page1.oPag.oBtn_1_180.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_PSSERIAL = space(10)
      .w_PSNUMREG = 0
      .w_PSDATREG = ctod("  /  /  ")
      .w_PSSTATUS = space(1)
      .w_PSANNREG = space(4)
      .w_PSCODESE = space(4)
      .w_PSDESCRI = space(40)
      .w_PSTIPDOC = space(5)
      .w_PSNUMDOC = 0
      .w_PSALFDOC = space(10)
      .w_PSDATDOC = ctod("  /  /  ")
      .w_PSSERCOR = space(10)
      .w_PSCODMAG = space(5)
      .w_PSCLAORI = space(2)
      .w_PSTIPORI = space(5)
      .w_PSEVAINI = ctod("  /  /  ")
      .w_PSEVAFIN = ctod("  /  /  ")
      .w_PSFLDISP = space(1)
      .w_PSCODVAL = space(3)
      .w_PSFLGFID = space(1)
      .w_PSCODCLF = space(15)
      .w_FLRICIVA = .f.
      .w_PSCODDES = space(5)
      .w_PSCODZON = space(3)
      .w_PSCODAGE = space(5)
      .w_PSDATTRA = ctod("  /  /  ")
      .w_PSORATRA = space(2)
      .w_PSMINTRA = space(2)
      .w_PSNOTAGG = space(40)
      .w_STADOCIM = space(1)
      .w_STREPSEC = space(1)
      .w_DESDOC = space(35)
      .w_MVFLVEAC = space(1)
      .w_PSPRD = space(2)
      .w_MVCLADOC = space(2)
      .w_MVFLACCO = space(1)
      .w_MVFLINTE = space(1)
      .w_MVTCAMAG = space(5)
      .w_MVTFRAGG = space(1)
      .w_DOCAUCON = space(5)
      .w_STAMPA = 0
      .w_FLIMAC = space(1)
      .w_TDMAGDEF = space(1)
      .w_TDFLAPCA1 = space(1)
      .w_MVALFEST = space(10)
      .w_DFLPP = space(1)
      .w_FLPACK = space(1)
      .w_FLNSRI = space(1)
      .w_TPNSRI = space(3)
      .w_FLDTPR = space(1)
      .w_TPVSRI = space(3)
      .w_FLVSRI = space(1)
      .w_CODMAT = space(5)
      .w_ASSCES = space(1)
      .w_EMERIC = space(1)
      .w_CMDES = space(35)
      .w_CAUCOL = space(5)
      .w_DESORI = space(35)
      .w_DESVAL = space(35)
      .w_PSTIPCLF = space(1)
      .w_DESCLF = space(40)
      .w_NOMDES = space(80)
      .w_DESZON = space(35)
      .w_DESAGE = space(35)
      .w_CPDESCRI = space(30)
      .w_CPCODCAU = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DTCOBSO = ctod("  /  /  ")
      .w_CLAORI = space(2)
      .w_ASSCES1 = space(1)
      .w_DTMOBSO = ctod("  /  /  ")
      .w_CATCON = space(5)
      .w_LINGUA = space(3)
      .w_DESOBSO = ctod("  /  /  ")
      .w_CURSSEDO = space(10)
      .w_FLDOCSEL = .f.
      .w_TDMODRIF = space(5)
      .w_TDDESRIF = space(18)
      .w_CMFLRISE = space(1)
      .w_CMFLCASC = space(1)
      .w_CMFLORDI = space(1)
      .w_CMFLIMPE = space(1)
      .w_CMF2CASC = space(1)
      .w_CMF2ORDI = space(1)
      .w_CMF2IMPE = space(1)
      .w_CMF2RISE = space(1)
      .w_MVFLELGM = space(1)
      .w_TDFLRIDE = space(1)
      .w_GDSERIAL = space(10)
      .w_GDKEYRIF = space(10)
      .w_TDFLRISC = space(1)
      .w_TDFLCRIS = space(1)
      .w_AZFLFIDO = space(1)
      .w_TDFLAPCA = space(1)
      .w_TDCHKUCA = space(1)
      .w_RIIVLSIV = space(0)
      .w_RIIVDTIN = ctod("  /  /  ")
      .w_RIIVDTFI = ctod("  /  /  ")
      .w_TDESCCL1 = space(3)
      .w_TDESCCL2 = space(3)
      .w_TDESCCL3 = space(3)
      .w_TDESCCL4 = space(3)
      .w_TDESCCL5 = space(3)
      .w_TDFLIA01 = space(1)
      .w_TDFLIA02 = space(1)
      .w_TDFLIA03 = space(1)
      .w_TDFLIA04 = space(1)
      .w_TDFLIA05 = space(1)
      .w_TDFLIA06 = space(1)
      .w_TDFLRA01 = space(1)
      .w_TDFLRA02 = space(1)
      .w_TDFLRA03 = space(1)
      .w_TDFLRA04 = space(1)
      .w_TDFLRA05 = space(1)
      .w_TDFLRA06 = space(1)
      .w_MGDESMAG = space(30)
      .w_TDFLANAL = space(1)
      .w_TDFLCOMM = space(1)
      .w_DDTIPCON = space(1)
      .w_DDCODCON = space(15)
      .w_TDMINVEN = space(1)
      .w_TDRIOTOT = space(1)
      .w_AZMAGAZI = space(5)
      .w_TDCODMAG = space(5)
      .w_TDFLMGPR = space(1)
      .w_TDCAUMAG = space(5)
      .w_TDRICNOM = space(1)
      .w_TDORDAPE = space(1)
      .w_AZFLCAMB = space(1)
      .w_TDPRZDES = space(1)
      .w_TDASPETT = space(30)
      .w_TDALFDOC = space(5)
      .w_TDFLSPIN = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_PSDATREG = i_DATSYS
        .w_PSSTATUS = 'N'
        .w_PSANNREG = STR(YEAR(.w_PSDATREG),4,0)
        .w_PSCODESE = g_CODESE
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_PSTIPDOC))
          .link_1_12('Full')
          endif
          .DoRTCalc(10,11,.f.)
        .w_PSDATDOC = i_datsys
        .w_PSSERCOR = IIF(EMPTY(.w_PSTIPDOC), "", gsva_bps(this, "SELCONFPRE") )
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_PSSERCOR))
          .link_1_16('Full')
          endif
        .w_PSCODMAG = IIF(.w_TDFLMGPR='F', .w_TDCODMAG, SPACE(5) )
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_PSCODMAG))
          .link_1_17('Full')
          endif
        .w_PSCLAORI = 'OR'
        .w_PSTIPORI = SPACE(5)
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_PSTIPORI))
          .link_1_19('Full')
          endif
          .DoRTCalc(17,18,.f.)
        .w_PSFLDISP = 'N'
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_PSCODVAL))
          .link_1_23('Full')
          endif
        .w_PSFLGFID = IIF(.w_PSSTATUS<>'S', .w_AZFLFIDO, 'N')
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_PSCODCLF))
          .link_1_25('Full')
          endif
          .DoRTCalc(23,23,.f.)
        .w_PSCODDES = IIF(EMPTY(.w_PSCODCLF), "", .w_PSCODDES)
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_PSCODDES))
          .link_1_27('Full')
          endif
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_PSCODZON))
          .link_1_28('Full')
          endif
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_PSCODAGE))
          .link_1_29('Full')
          endif
        .w_PSDATTRA = IIF(.w_MVFLACCO='S', .w_PSDATREG, cp_CharToDate('  -  -  '))
          .DoRTCalc(28,30,.f.)
        .w_STADOCIM = 'S'
        .w_STREPSEC = 'S'
        .DoRTCalc(33,39,.f.)
          if not(empty(.w_MVTCAMAG))
          .link_1_50('Full')
          endif
          .DoRTCalc(40,53,.f.)
        .w_CODMAT = IIF( .cFunction='Query',Space(5), .w_PSCODMAG )
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_CODMAT))
          .link_1_65('Full')
          endif
        .DoRTCalc(55,58,.f.)
          if not(empty(.w_CAUCOL))
          .link_1_70('Full')
          endif
          .DoRTCalc(59,60,.f.)
        .w_PSTIPCLF = 'C'
          .DoRTCalc(62,67,.f.)
        .w_OBTEST = .w_PSDATDOC
          .DoRTCalc(69,76,.f.)
        .w_CURSSEDO = SYS(2015)
        .w_FLDOCSEL = .F.
          .DoRTCalc(79,91,.f.)
        .w_GDKEYRIF = .w_PSSERIAL
        .DoRTCalc(92,92,.f.)
          if not(empty(.w_GDKEYRIF))
          .link_1_133('Full')
          endif
          .DoRTCalc(93,120,.f.)
        .w_DDTIPCON = .w_PSTIPCLF
        .w_DDCODCON = .w_PSCODCLF
      endif
    endwith
    cp_BlankRecExtFlds(this,'PIA_SPED')
    this.DoRTCalc(123,135,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_131.enabled = this.oPgFrm.Page1.oPag.oBtn_1_131.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_163.enabled = this.oPgFrm.Page1.oPag.oBtn_1_163.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_180.enabled = this.oPgFrm.Page1.oPag.oBtn_1_180.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEPIASPE","i_codazi,w_PSSERIAL")
      cp_AskTableProg(this,i_nConn,"PRPIASPE","i_codazi,w_PSANNREG,w_PSNUMREG")
      .op_codazi = .w_codazi
      .op_PSSERIAL = .w_PSSERIAL
      .op_codazi = .w_codazi
      .op_PSANNREG = .w_PSANNREG
      .op_PSNUMREG = .w_PSNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPSNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oPSDATREG_1_4.enabled = i_bVal
      .Page1.oPag.oPSSTATUS_1_5.enabled = i_bVal
      .Page1.oPag.oPSDESCRI_1_10.enabled = i_bVal
      .Page1.oPag.oPSTIPDOC_1_12.enabled = i_bVal
      .Page1.oPag.oPSNUMDOC_1_13.enabled = i_bVal
      .Page1.oPag.oPSALFDOC_1_14.enabled = i_bVal
      .Page1.oPag.oPSDATDOC_1_15.enabled = i_bVal
      .Page1.oPag.oPSSERCOR_1_16.enabled = i_bVal
      .Page1.oPag.oPSCODMAG_1_17.enabled = i_bVal
      .Page1.oPag.oPSCLAORI_1_18.enabled = i_bVal
      .Page1.oPag.oPSTIPORI_1_19.enabled = i_bVal
      .Page1.oPag.oPSEVAINI_1_20.enabled = i_bVal
      .Page1.oPag.oPSEVAFIN_1_21.enabled = i_bVal
      .Page1.oPag.oPSFLDISP_1_22.enabled = i_bVal
      .Page1.oPag.oPSCODVAL_1_23.enabled = i_bVal
      .Page1.oPag.oPSFLGFID_1_24.enabled = i_bVal
      .Page1.oPag.oPSCODCLF_1_25.enabled = i_bVal
      .Page1.oPag.oFLRICIVA_1_26.enabled = i_bVal
      .Page1.oPag.oPSCODDES_1_27.enabled = i_bVal
      .Page1.oPag.oPSCODZON_1_28.enabled = i_bVal
      .Page1.oPag.oPSCODAGE_1_29.enabled = i_bVal
      .Page1.oPag.oPSDATTRA_1_30.enabled = i_bVal
      .Page1.oPag.oPSORATRA_1_31.enabled = i_bVal
      .Page1.oPag.oPSMINTRA_1_32.enabled = i_bVal
      .Page1.oPag.oPSNOTAGG_1_33.enabled = i_bVal
      .Page1.oPag.oSTADOCIM_1_34.enabled = i_bVal
      .Page1.oPag.oSTREPSEC_1_35.enabled = i_bVal
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_37.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = i_bVal
      .Page1.oPag.oBtn_1_131.enabled = .Page1.oPag.oBtn_1_131.mCond()
      .Page1.oPag.oBtn_1_163.enabled = .Page1.oPag.oBtn_1_163.mCond()
      .Page1.oPag.oBtn_1_180.enabled = .Page1.oPag.oBtn_1_180.mCond()
      if i_cOp = "Query"
        .Page1.oPag.oPSNUMREG_1_3.enabled = .t.
      endif
    endwith
    this.GSVA_MPS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PIA_SPED',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVA_MPS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSERIAL,"PSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMREG,"PSNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATREG,"PSDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSTATUS,"PSSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSANNREG,"PSANNREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODESE,"PSCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDESCRI,"PSDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPDOC,"PSTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMDOC,"PSNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSALFDOC,"PSALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATDOC,"PSDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSERCOR,"PSSERCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODMAG,"PSCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCLAORI,"PSCLAORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPORI,"PSTIPORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSEVAINI,"PSEVAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSEVAFIN,"PSEVAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSFLDISP,"PSFLDISP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODVAL,"PSCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSFLGFID,"PSFLGFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODCLF,"PSCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODDES,"PSCODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODZON,"PSCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODAGE,"PSCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATTRA,"PSDATTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSORATRA,"PSORATRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMINTRA,"PSMINTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNOTAGG,"PSNOTAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPCLF,"PSTIPCLF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
    i_lTable = "PIA_SPED"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PIA_SPED_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PIA_SPED_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEPIASPE","i_codazi,w_PSSERIAL")
          cp_NextTableProg(this,i_nConn,"PRPIASPE","i_codazi,w_PSANNREG,w_PSNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PIA_SPED
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PIA_SPED')
        i_extval=cp_InsertValODBCExtFlds(this,'PIA_SPED')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PSSERIAL,PSNUMREG,PSDATREG,PSSTATUS,PSANNREG"+;
                  ",PSCODESE,PSDESCRI,PSTIPDOC,PSNUMDOC,PSALFDOC"+;
                  ",PSDATDOC,PSSERCOR,PSCODMAG,PSCLAORI,PSTIPORI"+;
                  ",PSEVAINI,PSEVAFIN,PSFLDISP,PSCODVAL,PSFLGFID"+;
                  ",PSCODCLF,PSCODDES,PSCODZON,PSCODAGE,PSDATTRA"+;
                  ",PSORATRA,PSMINTRA,PSNOTAGG,PSTIPCLF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PSNUMREG)+;
                  ","+cp_ToStrODBC(this.w_PSDATREG)+;
                  ","+cp_ToStrODBC(this.w_PSSTATUS)+;
                  ","+cp_ToStrODBC(this.w_PSANNREG)+;
                  ","+cp_ToStrODBC(this.w_PSCODESE)+;
                  ","+cp_ToStrODBC(this.w_PSDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_PSTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_PSNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_PSALFDOC)+;
                  ","+cp_ToStrODBC(this.w_PSDATDOC)+;
                  ","+cp_ToStrODBCNull(this.w_PSSERCOR)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODMAG)+;
                  ","+cp_ToStrODBC(this.w_PSCLAORI)+;
                  ","+cp_ToStrODBCNull(this.w_PSTIPORI)+;
                  ","+cp_ToStrODBC(this.w_PSEVAINI)+;
                  ","+cp_ToStrODBC(this.w_PSEVAFIN)+;
                  ","+cp_ToStrODBC(this.w_PSFLDISP)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODVAL)+;
                  ","+cp_ToStrODBC(this.w_PSFLGFID)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODCLF)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODDES)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODAGE)+;
                  ","+cp_ToStrODBC(this.w_PSDATTRA)+;
                  ","+cp_ToStrODBC(this.w_PSORATRA)+;
                  ","+cp_ToStrODBC(this.w_PSMINTRA)+;
                  ","+cp_ToStrODBC(this.w_PSNOTAGG)+;
                  ","+cp_ToStrODBC(this.w_PSTIPCLF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PIA_SPED')
        i_extval=cp_InsertValVFPExtFlds(this,'PIA_SPED')
        cp_CheckDeletedKey(i_cTable,0,'PSSERIAL',this.w_PSSERIAL)
        INSERT INTO (i_cTable);
              (PSSERIAL,PSNUMREG,PSDATREG,PSSTATUS,PSANNREG,PSCODESE,PSDESCRI,PSTIPDOC,PSNUMDOC,PSALFDOC,PSDATDOC,PSSERCOR,PSCODMAG,PSCLAORI,PSTIPORI,PSEVAINI,PSEVAFIN,PSFLDISP,PSCODVAL,PSFLGFID,PSCODCLF,PSCODDES,PSCODZON,PSCODAGE,PSDATTRA,PSORATRA,PSMINTRA,PSNOTAGG,PSTIPCLF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PSSERIAL;
                  ,this.w_PSNUMREG;
                  ,this.w_PSDATREG;
                  ,this.w_PSSTATUS;
                  ,this.w_PSANNREG;
                  ,this.w_PSCODESE;
                  ,this.w_PSDESCRI;
                  ,this.w_PSTIPDOC;
                  ,this.w_PSNUMDOC;
                  ,this.w_PSALFDOC;
                  ,this.w_PSDATDOC;
                  ,this.w_PSSERCOR;
                  ,this.w_PSCODMAG;
                  ,this.w_PSCLAORI;
                  ,this.w_PSTIPORI;
                  ,this.w_PSEVAINI;
                  ,this.w_PSEVAFIN;
                  ,this.w_PSFLDISP;
                  ,this.w_PSCODVAL;
                  ,this.w_PSFLGFID;
                  ,this.w_PSCODCLF;
                  ,this.w_PSCODDES;
                  ,this.w_PSCODZON;
                  ,this.w_PSCODAGE;
                  ,this.w_PSDATTRA;
                  ,this.w_PSORATRA;
                  ,this.w_PSMINTRA;
                  ,this.w_PSNOTAGG;
                  ,this.w_PSTIPCLF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PIA_SPED_IDX,i_nConn)
      *
      * update PIA_SPED
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PIA_SPED')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PSNUMREG="+cp_ToStrODBC(this.w_PSNUMREG)+;
             ",PSDATREG="+cp_ToStrODBC(this.w_PSDATREG)+;
             ",PSSTATUS="+cp_ToStrODBC(this.w_PSSTATUS)+;
             ",PSANNREG="+cp_ToStrODBC(this.w_PSANNREG)+;
             ",PSCODESE="+cp_ToStrODBC(this.w_PSCODESE)+;
             ",PSDESCRI="+cp_ToStrODBC(this.w_PSDESCRI)+;
             ",PSTIPDOC="+cp_ToStrODBCNull(this.w_PSTIPDOC)+;
             ",PSNUMDOC="+cp_ToStrODBC(this.w_PSNUMDOC)+;
             ",PSALFDOC="+cp_ToStrODBC(this.w_PSALFDOC)+;
             ",PSDATDOC="+cp_ToStrODBC(this.w_PSDATDOC)+;
             ",PSSERCOR="+cp_ToStrODBCNull(this.w_PSSERCOR)+;
             ",PSCODMAG="+cp_ToStrODBCNull(this.w_PSCODMAG)+;
             ",PSCLAORI="+cp_ToStrODBC(this.w_PSCLAORI)+;
             ",PSTIPORI="+cp_ToStrODBCNull(this.w_PSTIPORI)+;
             ",PSEVAINI="+cp_ToStrODBC(this.w_PSEVAINI)+;
             ",PSEVAFIN="+cp_ToStrODBC(this.w_PSEVAFIN)+;
             ",PSFLDISP="+cp_ToStrODBC(this.w_PSFLDISP)+;
             ",PSCODVAL="+cp_ToStrODBCNull(this.w_PSCODVAL)+;
             ",PSFLGFID="+cp_ToStrODBC(this.w_PSFLGFID)+;
             ",PSCODCLF="+cp_ToStrODBCNull(this.w_PSCODCLF)+;
             ",PSCODDES="+cp_ToStrODBCNull(this.w_PSCODDES)+;
             ",PSCODZON="+cp_ToStrODBCNull(this.w_PSCODZON)+;
             ",PSCODAGE="+cp_ToStrODBCNull(this.w_PSCODAGE)+;
             ",PSDATTRA="+cp_ToStrODBC(this.w_PSDATTRA)+;
             ",PSORATRA="+cp_ToStrODBC(this.w_PSORATRA)+;
             ",PSMINTRA="+cp_ToStrODBC(this.w_PSMINTRA)+;
             ",PSNOTAGG="+cp_ToStrODBC(this.w_PSNOTAGG)+;
             ",PSTIPCLF="+cp_ToStrODBC(this.w_PSTIPCLF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PIA_SPED')
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        UPDATE (i_cTable) SET;
              PSNUMREG=this.w_PSNUMREG;
             ,PSDATREG=this.w_PSDATREG;
             ,PSSTATUS=this.w_PSSTATUS;
             ,PSANNREG=this.w_PSANNREG;
             ,PSCODESE=this.w_PSCODESE;
             ,PSDESCRI=this.w_PSDESCRI;
             ,PSTIPDOC=this.w_PSTIPDOC;
             ,PSNUMDOC=this.w_PSNUMDOC;
             ,PSALFDOC=this.w_PSALFDOC;
             ,PSDATDOC=this.w_PSDATDOC;
             ,PSSERCOR=this.w_PSSERCOR;
             ,PSCODMAG=this.w_PSCODMAG;
             ,PSCLAORI=this.w_PSCLAORI;
             ,PSTIPORI=this.w_PSTIPORI;
             ,PSEVAINI=this.w_PSEVAINI;
             ,PSEVAFIN=this.w_PSEVAFIN;
             ,PSFLDISP=this.w_PSFLDISP;
             ,PSCODVAL=this.w_PSCODVAL;
             ,PSFLGFID=this.w_PSFLGFID;
             ,PSCODCLF=this.w_PSCODCLF;
             ,PSCODDES=this.w_PSCODDES;
             ,PSCODZON=this.w_PSCODZON;
             ,PSCODAGE=this.w_PSCODAGE;
             ,PSDATTRA=this.w_PSDATTRA;
             ,PSORATRA=this.w_PSORATRA;
             ,PSMINTRA=this.w_PSMINTRA;
             ,PSNOTAGG=this.w_PSNOTAGG;
             ,PSTIPCLF=this.w_PSTIPCLF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVA_MPS : Saving
      this.GSVA_MPS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PSSERIAL,"DPSERIAL";
             )
      this.GSVA_MPS.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVA_MPS : Deleting
    this.GSVA_MPS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PSSERIAL,"DPSERIAL";
           )
    this.GSVA_MPS.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PIA_SPED_IDX,i_nConn)
      *
      * delete PIA_SPED
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PIA_SPED_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PIA_SPED_IDX,2])
    if i_bUpd
      with this
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,5,.t.)
            .w_PSANNREG = STR(YEAR(.w_PSDATREG),4,0)
            .w_PSCODESE = g_CODESE
        .DoRTCalc(8,12,.t.)
        if .o_PSTIPDOC<>.w_PSTIPDOC
            .w_PSSERCOR = IIF(EMPTY(.w_PSTIPDOC), "", gsva_bps(this, "SELCONFPRE") )
          .link_1_16('Full')
        endif
        if .o_PSTIPDOC<>.w_PSTIPDOC
            .w_PSCODMAG = IIF(.w_TDFLMGPR='F', .w_TDCODMAG, SPACE(5) )
          .link_1_17('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_PSCLAORI<>.w_PSCLAORI
            .w_PSTIPORI = SPACE(5)
          .link_1_19('Full')
        endif
        .DoRTCalc(17,20,.t.)
        if .o_PSSTATUS<>.w_PSSTATUS
            .w_PSFLGFID = IIF(.w_PSSTATUS<>'S', .w_AZFLFIDO, 'N')
        endif
        .DoRTCalc(22,23,.t.)
        if .o_PSCODCLF<>.w_PSCODCLF
            .w_PSCODDES = IIF(EMPTY(.w_PSCODCLF), "", .w_PSCODDES)
          .link_1_27('Full')
        endif
        .DoRTCalc(25,26,.t.)
        if .o_PSDATREG<>.w_PSDATREG.or. .o_PSTIPDOC<>.w_PSTIPDOC
            .w_PSDATTRA = IIF(.w_MVFLACCO='S', .w_PSDATREG, cp_CharToDate('  -  -  '))
        endif
        .DoRTCalc(28,38,.t.)
          .link_1_50('Full')
        .DoRTCalc(40,53,.t.)
        if .o_PSSERIAL<>.w_PSSERIAL
            .w_CODMAT = IIF( .cFunction='Query',Space(5), .w_PSCODMAG )
          .link_1_65('Full')
        endif
        .DoRTCalc(55,57,.t.)
          .link_1_70('Full')
        .DoRTCalc(59,60,.t.)
            .w_PSTIPCLF = 'C'
        .DoRTCalc(62,67,.t.)
        if .o_PSDATDOC<>.w_PSDATDOC
            .w_OBTEST = .w_PSDATDOC
        endif
        if .o_PSTIPDOC<>.w_PSTIPDOC.or. .o_PSALFDOC<>.w_PSALFDOC.or. .o_PSDATDOC<>.w_PSDATDOC.or. .o_PSSTATUS<>.w_PSSTATUS
          .Calculate_JWOQQUFHID()
        endif
        .DoRTCalc(69,91,.t.)
            .w_GDKEYRIF = .w_PSSERIAL
          .link_1_133('Full')
        if .o_FLRICIVA<>.w_FLRICIVA
          .Calculate_WUICMSNHXF()
        endif
        if .o_PSTIPDOC<>.w_PSTIPDOC
          .Calculate_BPFLDBXRVN()
        endif
        .DoRTCalc(93,120,.t.)
            .w_DDTIPCON = .w_PSTIPCLF
            .w_DDCODCON = .w_PSCODCLF
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEPIASPE","i_codazi,w_PSSERIAL")
          .op_PSSERIAL = .w_PSSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_PSANNREG<>.w_PSANNREG
           cp_AskTableProg(this,i_nConn,"PRPIASPE","i_codazi,w_PSANNREG,w_PSNUMREG")
          .op_PSNUMREG = .w_PSNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_PSANNREG = .w_PSANNREG
      endwith
      this.DoRTCalc(123,135,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_JWOQQUFHID()
    with this
          * --- Inizializzazione primo numero documento da generare - GSVA_BPS(INITNUMDOC)
          gsva_bps(this;
              ,"INITNUMDOC";
             )
    endwith
  endproc
  proc Calculate_YYTKGJXUPN()
    with this
          * --- Elimina i dati temporanei eventualmente creati - GSVA_BPS(RESETDATA)
          gsva_bps(this;
              ,"RESETDATA";
             )
    endwith
  endproc
  proc Calculate_XXOJDZUAOE()
    with this
          * --- Elabora il piano di spedizione - GSVA_BPS(ELABDATA)
          gsva_bps(this;
              ,"ELABDATA";
             )
    endwith
  endproc
  proc Calculate_NKJWGPNEVF()
    with this
          * --- Visualizza il log della generazione - GSVE_KLE
          GSVE_KLE(this;
             )
    endwith
  endproc
  proc Calculate_WUICMSNHXF()
    with this
          * --- Apertura maschera conferma - GSVE_BFI(OPEN)
          .w_FLRICIVA = GSVE_BFI(this, "OPEN")
    endwith
  endproc
  proc Calculate_NQHUTZPBAT()
    with this
          * --- Stampa documenti generati - GSVA_BPS(PRINTDOC)
          GSVA_BPS(this;
              ,'PRINTDOC';
             )
    endwith
  endproc
  proc Calculate_BPFLDBXRVN()
    with this
          * --- Valorizza descrizione magazzino su inserimento doc. da generare
          .w_PSCODMAG = .w_PSCODMAG
          .link_1_17('Full')
    endwith
  endproc
  proc Calculate_BICOEBZKEY()
    with this
          * --- Ripristna progressivo piano di spedizione - GSVA_BPS(RIPRISPROG)
          gsva_bps(this;
              ,"RIPRISPROG";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPSEVAINI_1_20.enabled = this.oPgFrm.Page1.oPag.oPSEVAINI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPSEVAFIN_1_21.enabled = this.oPgFrm.Page1.oPag.oPSEVAFIN_1_21.mCond()
    this.oPgFrm.Page1.oPag.oPSCODVAL_1_23.enabled = this.oPgFrm.Page1.oPag.oPSCODVAL_1_23.mCond()
    this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.enabled = this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPSCODCLF_1_25.enabled = this.oPgFrm.Page1.oPag.oPSCODCLF_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPSCODDES_1_27.enabled = this.oPgFrm.Page1.oPag.oPSCODDES_1_27.mCond()
    this.oPgFrm.Page1.oPag.oPSCODZON_1_28.enabled = this.oPgFrm.Page1.oPag.oPSCODZON_1_28.mCond()
    this.oPgFrm.Page1.oPag.oPSCODAGE_1_29.enabled = this.oPgFrm.Page1.oPag.oPSCODAGE_1_29.mCond()
    this.oPgFrm.Page1.oPag.oPSDATTRA_1_30.enabled = this.oPgFrm.Page1.oPag.oPSDATTRA_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPSORATRA_1_31.enabled = this.oPgFrm.Page1.oPag.oPSORATRA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oPSMINTRA_1_32.enabled = this.oPgFrm.Page1.oPag.oPSMINTRA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oPSNOTAGG_1_33.enabled = this.oPgFrm.Page1.oPag.oPSNOTAGG_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.enabled = this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_163.enabled = this.oPgFrm.Page1.oPag.oBtn_1_163.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.visible=!this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.mHide()
    this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.visible=!this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_116.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_116.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_131.visible=!this.oPgFrm.Page1.oPag.oBtn_1_131.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_163.visible=!this.oPgFrm.Page1.oPag.oBtn_1_163.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_180.visible=!this.oPgFrm.Page1.oPag.oBtn_1_180.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Done")
          .Calculate_YYTKGJXUPN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_XXOJDZUAOE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ShowLog")
          .Calculate_NKJWGPNEVF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("PrintDoc")
          .Calculate_NQHUTZPBAT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted")
          .Calculate_BICOEBZKEY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLFIDO,AZMAGAZI,AZFLCAMB";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLFIDO,AZMAGAZI,AZFLCAMB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZFLFIDO = NVL(_Link_.AZFLFIDO,space(1))
      this.w_AZMAGAZI = NVL(_Link_.AZMAGAZI,space(5))
      this.w_AZFLCAMB = NVL(_Link_.AZFLCAMB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZFLFIDO = space(1)
      this.w_AZMAGAZI = space(5)
      this.w_AZFLCAMB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSTIPDOC
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PSTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PSTIPDOC))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPSTIPDOC_1_12'),i_cWhere,'',"Causali documenti",'GSVA1APS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PSTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PSTIPDOC)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_PSPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_DOCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_STAMPA = NVL(_Link_.TDPRGSTA,0)
      this.w_PSALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_MVALFEST = NVL(_Link_.TDSERPRO,space(10))
      this.w_DFLPP = NVL(_Link_.TDFLPPRO,space(1))
      this.w_FLPACK = NVL(_Link_.TDFLPACK,space(1))
      this.w_FLNSRI = NVL(_Link_.TDFLNSRI,space(1))
      this.w_TPNSRI = NVL(_Link_.TDTPNDOC,space(3))
      this.w_FLDTPR = NVL(_Link_.TDFLDTPR,space(1))
      this.w_TPVSRI = NVL(_Link_.TDTPVDOC,space(3))
      this.w_FLVSRI = NVL(_Link_.TDFLVSRI,space(1))
      this.w_TDCODMAG = NVL(_Link_.TDCODMAG,space(5))
      this.w_CODMAT = NVL(_Link_.TDCODMAT,space(5))
      this.w_ASSCES = NVL(_Link_.TDASSCES,space(1))
      this.w_EMERIC = NVL(_Link_.TDEMERIC,space(1))
      this.w_FLIMAC = NVL(_Link_.TDFLIMAC,space(1))
      this.w_TDFLAPCA1 = NVL(_Link_.TDFLAPCA,space(1))
      this.w_TDMODRIF = NVL(_Link_.TDMODRIF,space(5))
      this.w_TDDESRIF = NVL(_Link_.TDDESRIF,space(18))
      this.w_TDFLRIDE = NVL(_Link_.TDFLRIDE,space(1))
      this.w_TDFLRISC = NVL(_Link_.TDFLRISC,space(1))
      this.w_TDFLCRIS = NVL(_Link_.TDFLCRIS,space(1))
      this.w_TDCHKUCA = NVL(_Link_.TDCHKUCA,space(1))
      this.w_TDFLAPCA = NVL(_Link_.TDFLAPCA,space(1))
      this.w_TDESCCL1 = NVL(_Link_.TDESCCL1,space(3))
      this.w_TDESCCL2 = NVL(_Link_.TDESCCL2,space(3))
      this.w_TDESCCL3 = NVL(_Link_.TDESCCL3,space(3))
      this.w_TDESCCL4 = NVL(_Link_.TDESCCL4,space(3))
      this.w_TDESCCL5 = NVL(_Link_.TDESCCL5,space(3))
      this.w_TDFLIA01 = NVL(_Link_.TDFLIA01,space(1))
      this.w_TDFLIA02 = NVL(_Link_.TDFLIA02,space(1))
      this.w_TDFLIA03 = NVL(_Link_.TDFLIA03,space(1))
      this.w_TDFLIA04 = NVL(_Link_.TDFLIA04,space(1))
      this.w_TDFLIA05 = NVL(_Link_.TDFLIA05,space(1))
      this.w_TDFLIA06 = NVL(_Link_.TDFLIA06,space(1))
      this.w_TDFLRA01 = NVL(_Link_.TDFLRA01,space(1))
      this.w_TDFLRA02 = NVL(_Link_.TDFLRA02,space(1))
      this.w_TDFLRA03 = NVL(_Link_.TDFLRA03,space(1))
      this.w_TDFLRA04 = NVL(_Link_.TDFLRA04,space(1))
      this.w_TDFLRA05 = NVL(_Link_.TDFLRA05,space(1))
      this.w_TDFLRA06 = NVL(_Link_.TDFLRA06,space(1))
      this.w_TDFLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_TDFLCOMM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_TDMINVEN = NVL(_Link_.TDMINVEN,space(1))
      this.w_TDRIOTOT = NVL(_Link_.TDRIOTOT,space(1))
      this.w_TDFLMGPR = NVL(_Link_.TDFLMGPR,space(1))
      this.w_TDRICNOM = NVL(_Link_.TDRICNOM,space(1))
      this.w_TDPRZDES = NVL(_Link_.TDPRZDES,space(1))
      this.w_TDASPETT = NVL(_Link_.TDASPETT,space(30))
      this.w_TDALFDOC = NVL(_Link_.TDALFDOC,space(5))
      this.w_TDFLSPIN = NVL(_Link_.TDFLSPIN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PSTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_MVFLVEAC = space(1)
      this.w_PSPRD = space(2)
      this.w_MVCLADOC = space(2)
      this.w_MVFLACCO = space(1)
      this.w_MVFLINTE = space(1)
      this.w_MVTCAMAG = space(5)
      this.w_MVTFRAGG = space(1)
      this.w_DOCAUCON = space(5)
      this.w_STAMPA = 0
      this.w_PSALFDOC = space(10)
      this.w_MVALFEST = space(10)
      this.w_DFLPP = space(1)
      this.w_FLPACK = space(1)
      this.w_FLNSRI = space(1)
      this.w_TPNSRI = space(3)
      this.w_FLDTPR = space(1)
      this.w_TPVSRI = space(3)
      this.w_FLVSRI = space(1)
      this.w_TDCODMAG = space(5)
      this.w_CODMAT = space(5)
      this.w_ASSCES = space(1)
      this.w_EMERIC = space(1)
      this.w_FLIMAC = space(1)
      this.w_TDFLAPCA1 = space(1)
      this.w_TDMODRIF = space(5)
      this.w_TDDESRIF = space(18)
      this.w_TDFLRIDE = space(1)
      this.w_TDFLRISC = space(1)
      this.w_TDFLCRIS = space(1)
      this.w_TDCHKUCA = space(1)
      this.w_TDFLAPCA = space(1)
      this.w_TDESCCL1 = space(3)
      this.w_TDESCCL2 = space(3)
      this.w_TDESCCL3 = space(3)
      this.w_TDESCCL4 = space(3)
      this.w_TDESCCL5 = space(3)
      this.w_TDFLIA01 = space(1)
      this.w_TDFLIA02 = space(1)
      this.w_TDFLIA03 = space(1)
      this.w_TDFLIA04 = space(1)
      this.w_TDFLIA05 = space(1)
      this.w_TDFLIA06 = space(1)
      this.w_TDFLRA01 = space(1)
      this.w_TDFLRA02 = space(1)
      this.w_TDFLRA03 = space(1)
      this.w_TDFLRA04 = space(1)
      this.w_TDFLRA05 = space(1)
      this.w_TDFLRA06 = space(1)
      this.w_TDFLANAL = space(1)
      this.w_TDFLCOMM = space(1)
      this.w_TDMINVEN = space(1)
      this.w_TDRIOTOT = space(1)
      this.w_TDFLMGPR = space(1)
      this.w_TDRICNOM = space(1)
      this.w_TDPRZDES = space(1)
      this.w_TDASPETT = space(30)
      this.w_TDALFDOC = space(5)
      this.w_TDFLSPIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVFLVEAC='V' AND .w_ASSCES<>'S' AND .w_EMERIC = 'V' AND .w_MVCLADOC='DT' AND .w_TDRICNOM<>'A' AND GSVA_BPS( this, "CHKDOCTOGE")
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PSTIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_MVFLVEAC = space(1)
        this.w_PSPRD = space(2)
        this.w_MVCLADOC = space(2)
        this.w_MVFLACCO = space(1)
        this.w_MVFLINTE = space(1)
        this.w_MVTCAMAG = space(5)
        this.w_MVTFRAGG = space(1)
        this.w_DOCAUCON = space(5)
        this.w_STAMPA = 0
        this.w_PSALFDOC = space(10)
        this.w_MVALFEST = space(10)
        this.w_DFLPP = space(1)
        this.w_FLPACK = space(1)
        this.w_FLNSRI = space(1)
        this.w_TPNSRI = space(3)
        this.w_FLDTPR = space(1)
        this.w_TPVSRI = space(3)
        this.w_FLVSRI = space(1)
        this.w_TDCODMAG = space(5)
        this.w_CODMAT = space(5)
        this.w_ASSCES = space(1)
        this.w_EMERIC = space(1)
        this.w_FLIMAC = space(1)
        this.w_TDFLAPCA1 = space(1)
        this.w_TDMODRIF = space(5)
        this.w_TDDESRIF = space(18)
        this.w_TDFLRIDE = space(1)
        this.w_TDFLRISC = space(1)
        this.w_TDFLCRIS = space(1)
        this.w_TDCHKUCA = space(1)
        this.w_TDFLAPCA = space(1)
        this.w_TDESCCL1 = space(3)
        this.w_TDESCCL2 = space(3)
        this.w_TDESCCL3 = space(3)
        this.w_TDESCCL4 = space(3)
        this.w_TDESCCL5 = space(3)
        this.w_TDFLIA01 = space(1)
        this.w_TDFLIA02 = space(1)
        this.w_TDFLIA03 = space(1)
        this.w_TDFLIA04 = space(1)
        this.w_TDFLIA05 = space(1)
        this.w_TDFLIA06 = space(1)
        this.w_TDFLRA01 = space(1)
        this.w_TDFLRA02 = space(1)
        this.w_TDFLRA03 = space(1)
        this.w_TDFLRA04 = space(1)
        this.w_TDFLRA05 = space(1)
        this.w_TDFLRA06 = space(1)
        this.w_TDFLANAL = space(1)
        this.w_TDFLCOMM = space(1)
        this.w_TDMINVEN = space(1)
        this.w_TDRIOTOT = space(1)
        this.w_TDFLMGPR = space(1)
        this.w_TDRICNOM = space(1)
        this.w_TDPRZDES = space(1)
        this.w_TDASPETT = space(30)
        this.w_TDALFDOC = space(5)
        this.w_TDFLSPIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 60 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+60<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.TDTIPDOC as TDTIPDOC112"+ ",link_1_12.TDDESDOC as TDDESDOC112"+ ",link_1_12.TDFLVEAC as TDFLVEAC112"+ ",link_1_12.TDPRODOC as TDPRODOC112"+ ",link_1_12.TDCATDOC as TDCATDOC112"+ ",link_1_12.TDFLACCO as TDFLACCO112"+ ",link_1_12.TDFLINTE as TDFLINTE112"+ ",link_1_12.TDCAUMAG as TDCAUMAG112"+ ",link_1_12.TFFLRAGG as TFFLRAGG112"+ ",link_1_12.TDCAUCON as TDCAUCON112"+ ",link_1_12.TDPRGSTA as TDPRGSTA112"+ ",link_1_12.TDALFDOC as TDALFDOC112"+ ",link_1_12.TDSERPRO as TDSERPRO112"+ ",link_1_12.TDFLPPRO as TDFLPPRO112"+ ",link_1_12.TDFLPACK as TDFLPACK112"+ ",link_1_12.TDFLNSRI as TDFLNSRI112"+ ",link_1_12.TDTPNDOC as TDTPNDOC112"+ ",link_1_12.TDFLDTPR as TDFLDTPR112"+ ",link_1_12.TDTPVDOC as TDTPVDOC112"+ ",link_1_12.TDFLVSRI as TDFLVSRI112"+ ",link_1_12.TDCODMAG as TDCODMAG112"+ ",link_1_12.TDCODMAT as TDCODMAT112"+ ",link_1_12.TDASSCES as TDASSCES112"+ ",link_1_12.TDEMERIC as TDEMERIC112"+ ",link_1_12.TDFLIMAC as TDFLIMAC112"+ ",link_1_12.TDFLAPCA as TDFLAPCA112"+ ",link_1_12.TDMODRIF as TDMODRIF112"+ ",link_1_12.TDDESRIF as TDDESRIF112"+ ",link_1_12.TDFLRIDE as TDFLRIDE112"+ ",link_1_12.TDFLRISC as TDFLRISC112"+ ",link_1_12.TDFLCRIS as TDFLCRIS112"+ ",link_1_12.TDCHKUCA as TDCHKUCA112"+ ",link_1_12.TDFLAPCA as TDFLAPCA112"+ ",link_1_12.TDESCCL1 as TDESCCL1112"+ ",link_1_12.TDESCCL2 as TDESCCL2112"+ ",link_1_12.TDESCCL3 as TDESCCL3112"+ ",link_1_12.TDESCCL4 as TDESCCL4112"+ ",link_1_12.TDESCCL5 as TDESCCL5112"+ ",link_1_12.TDFLIA01 as TDFLIA01112"+ ",link_1_12.TDFLIA02 as TDFLIA02112"+ ",link_1_12.TDFLIA03 as TDFLIA03112"+ ",link_1_12.TDFLIA04 as TDFLIA04112"+ ",link_1_12.TDFLIA05 as TDFLIA05112"+ ",link_1_12.TDFLIA06 as TDFLIA06112"+ ",link_1_12.TDFLRA01 as TDFLRA01112"+ ",link_1_12.TDFLRA02 as TDFLRA02112"+ ",link_1_12.TDFLRA03 as TDFLRA03112"+ ",link_1_12.TDFLRA04 as TDFLRA04112"+ ",link_1_12.TDFLRA05 as TDFLRA05112"+ ",link_1_12.TDFLRA06 as TDFLRA06112"+ ",link_1_12.TDFLANAL as TDFLANAL112"+ ",link_1_12.TDFLCOMM as TDFLCOMM112"+ ",link_1_12.TDMINVEN as TDMINVEN112"+ ",link_1_12.TDRIOTOT as TDRIOTOT112"+ ",link_1_12.TDFLMGPR as TDFLMGPR112"+ ",link_1_12.TDRICNOM as TDRICNOM112"+ ",link_1_12.TDPRZDES as TDPRZDES112"+ ",link_1_12.TDASPETT as TDASPETT112"+ ",link_1_12.TDALFDOC as TDALFDOC112"+ ",link_1_12.TDFLSPIN as TDFLSPIN112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on PIA_SPED.PSTIPDOC=link_1_12.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+60
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and PIA_SPED.PSTIPDOC=link_1_12.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+60
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSSERCOR
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VACONPIA_IDX,3]
    i_lTable = "VACONPIA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2], .t., this.VACONPIA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSSERCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVA_MCP',True,'VACONPIA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CPSERIAL like "+cp_ToStrODBC(trim(this.w_PSSERCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select CPSERIAL,CPDESCRI,CPCODCAU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CPSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CPSERIAL',trim(this.w_PSSERCOR))
          select CPSERIAL,CPDESCRI,CPCODCAU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CPSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSSERCOR)==trim(_Link_.CPSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSSERCOR) and !this.bDontReportError
            deferred_cp_zoom('VACONPIA','*','CPSERIAL',cp_AbsName(oSource.parent,'oPSSERCOR_1_16'),i_cWhere,'GSVA_MCP',"Configurazioni ordinamento/rottura",'GSVA_APS.VACONPIA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPSERIAL,CPDESCRI,CPCODCAU";
                     +" from "+i_cTable+" "+i_lTable+" where CPSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPSERIAL',oSource.xKey(1))
            select CPSERIAL,CPDESCRI,CPCODCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSSERCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPSERIAL,CPDESCRI,CPCODCAU";
                   +" from "+i_cTable+" "+i_lTable+" where CPSERIAL="+cp_ToStrODBC(this.w_PSSERCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPSERIAL',this.w_PSSERCOR)
            select CPSERIAL,CPDESCRI,CPCODCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSSERCOR = NVL(_Link_.CPSERIAL,space(10))
      this.w_CPDESCRI = NVL(_Link_.CPDESCRI,space(30))
      this.w_CPCODCAU = NVL(_Link_.CPCODCAU,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PSSERCOR = space(10)
      endif
      this.w_CPDESCRI = space(30)
      this.w_CPCODCAU = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CPCODCAU) OR .w_CPCODCAU=.w_PSTIPDOC
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Configurazione inesistente oppure con causale documento differente da quella seleziona per la generazione del piano di spedizione")
        endif
        this.w_PSSERCOR = space(10)
        this.w_CPDESCRI = space(30)
        this.w_CPCODCAU = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VACONPIA_IDX,2])+'\'+cp_ToStr(_Link_.CPSERIAL,1)
      cp_ShowWarn(i_cKey,this.VACONPIA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSSERCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSCODMAG
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PSCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PSCODMAG))
          select MGCODMAG,MGDTOBSO,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPSCODMAG_1_17'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDTOBSO,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PSCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PSCODMAG)
            select MGCODMAG,MGDTOBSO,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DTMOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_MGDESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODMAG = space(5)
      endif
      this.w_DTMOBSO = ctod("  /  /  ")
      this.w_MGDESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.MGCODMAG as MGCODMAG117"+ ",link_1_17.MGDTOBSO as MGDTOBSO117"+ ",link_1_17.MGDESMAG as MGDESMAG117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on PIA_SPED.PSCODMAG=link_1_17.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and PIA_SPED.PSCODMAG=link_1_17.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSTIPORI
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSTIPORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PSTIPORI)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PSTIPORI))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSTIPORI)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSTIPORI) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPSTIPORI_1_19'),i_cWhere,'',"Causali documenti",'GSVA2APS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSTIPORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PSTIPORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PSTIPORI)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDASSCES,TDCAUMAG,TDORDAPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSTIPORI = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_CLAORI = NVL(_Link_.TDCATDOC,space(2))
      this.w_ASSCES1 = NVL(_Link_.TDASSCES,space(1))
      this.w_TDCAUMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_TDORDAPE = NVL(_Link_.TDORDAPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PSTIPORI = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_CLAORI = space(2)
      this.w_ASSCES1 = space(1)
      this.w_TDCAUMAG = space(5)
      this.w_TDORDAPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLAORI=.w_PSCLAORI AND .w_ASSCES1<>'S' AND .w_TDORDAPE<>'S' AND GSVA_BPS( this, "CHKDOCORIG")
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente, con gestione del riservato o con gestione cespiti attivata")
        endif
        this.w_PSTIPORI = space(5)
        this.w_DESORI = space(35)
        this.w_CLAORI = space(2)
        this.w_ASSCES1 = space(1)
        this.w_TDCAUMAG = space(5)
        this.w_TDORDAPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSTIPORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.TDTIPDOC as TDTIPDOC119"+ ",link_1_19.TDDESDOC as TDDESDOC119"+ ",link_1_19.TDCATDOC as TDCATDOC119"+ ",link_1_19.TDASSCES as TDASSCES119"+ ",link_1_19.TDCAUMAG as TDCAUMAG119"+ ",link_1_19.TDORDAPE as TDORDAPE119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on PIA_SPED.PSTIPORI=link_1_19.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and PIA_SPED.PSTIPORI=link_1_19.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODVAL
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PSCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PSCODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PSCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PSCODVAL)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPSCODVAL_1_23'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PSCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PSCODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.VACODVAL as VACODVAL123"+ ",link_1_23.VADESVAL as VADESVAL123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on PIA_SPED.PSCODVAL=link_1_23.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and PIA_SPED.PSCODVAL=link_1_23.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODCLF
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PSTIPCLF;
                     ,'ANCODICE',trim(this.w_PSCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PSTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPSCODCLF_1_25'),i_cWhere,'GSAR_ACL',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PSTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PSTIPCLF;
                       ,'ANCODICE',this.w_PSCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANCATCON,ANCODLIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CATCON = NVL(_Link_.ANCATCON,space(5))
      this.w_LINGUA = NVL(_Link_.ANCODLIN,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CATCON = space(5)
      this.w_LINGUA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_PSCODCLF = space(15)
        this.w_DESCLF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CATCON = space(5)
        this.w_LINGUA = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.ANCODICE as ANCODICE125"+ ",link_1_25.ANDESCRI as ANDESCRI125"+ ",link_1_25.ANDTOBSO as ANDTOBSO125"+ ",link_1_25.ANCATCON as ANCATCON125"+ ",link_1_25.ANCODLIN as ANCODLIN125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on PIA_SPED.PSCODCLF=link_1_25.ANCODICE"+" and PIA_SPED.PSTIPCLF=link_1_25.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and PIA_SPED.PSCODCLF=link_1_25.ANCODICE(+)"'+'+" and PIA_SPED.PSTIPCLF=link_1_25.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODDES
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_PSCODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_PSTIPCLF;
                     ,'DDCODICE',this.w_PSCODCLF;
                     ,'DDCODDES',trim(this.w_PSCODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oPSCODDES_1_27'),i_cWhere,'',"Destinazioni diverse",'GSVE_BZD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PSTIPCLF<>oSource.xKey(1);
           .or. this.w_PSCODCLF<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice sede inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_PSCODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_PSTIPCLF;
                       ,'DDCODICE',this.w_PSCODCLF;
                       ,'DDCODDES',this.w_PSCODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(80))
      this.w_DESOBSO = NVL(cp_ToDate(_Link_.DDDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODDES = space(5)
      endif
      this.w_NOMDES = space(80)
      this.w_DESOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSCODZON
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PSCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PSCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPSCODZON_1_28'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PSCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PSCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleta")
        endif
        this.w_PSCODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSCODAGE
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PSCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PSCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPSCODAGE_1_29'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PSCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PSCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PSCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCAMAG
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MVTCAMAG)
            select CMCODICE,CMDESCRI,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLELGM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_CMDES = NVL(_Link_.CMDESCRI,space(35))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_CMFLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_CMFLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_CMFLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_CMFLRISE = NVL(_Link_.CMFLRISE,space(1))
      this.w_MVFLELGM = NVL(_Link_.CMFLELGM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCAMAG = space(5)
      endif
      this.w_CMDES = space(35)
      this.w_CAUCOL = space(5)
      this.w_CMFLCASC = space(1)
      this.w_CMFLORDI = space(1)
      this.w_CMFLIMPE = space(1)
      this.w_CMFLRISE = space(1)
      this.w_MVFLELGM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAT
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAT)
            select MGCODMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DTCOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAT = space(5)
      endif
      this.w_DTCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCOL
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUCOL)
            select CMCODICE,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_CMF2CASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_CMF2ORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_CMF2IMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_CMF2RISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCOL = space(5)
      endif
      this.w_CMF2CASC = space(1)
      this.w_CMF2ORDI = space(1)
      this.w_CMF2IMPE = space(1)
      this.w_CMF2RISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GDKEYRIF
  func Link_1_133(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GENERDOC_IDX,3]
    i_lTable = "GENERDOC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2], .t., this.GENERDOC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GDKEYRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GDKEYRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDKEYRIF,GDSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where GDKEYRIF="+cp_ToStrODBC(this.w_GDKEYRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDKEYRIF',this.w_GDKEYRIF)
            select GDKEYRIF,GDSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GDKEYRIF = NVL(_Link_.GDKEYRIF,space(10))
      this.w_GDSERIAL = NVL(_Link_.GDSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GDKEYRIF = space(10)
      endif
      this.w_GDSERIAL = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])+'\'+cp_ToStr(_Link_.GDKEYRIF,1)
      cp_ShowWarn(i_cKey,this.GENERDOC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GDKEYRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSNUMREG_1_3.value==this.w_PSNUMREG)
      this.oPgFrm.Page1.oPag.oPSNUMREG_1_3.value=this.w_PSNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATREG_1_4.value==this.w_PSDATREG)
      this.oPgFrm.Page1.oPag.oPSDATREG_1_4.value=this.w_PSDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSSTATUS_1_5.RadioValue()==this.w_PSSTATUS)
      this.oPgFrm.Page1.oPag.oPSSTATUS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSANNREG_1_6.value==this.w_PSANNREG)
      this.oPgFrm.Page1.oPag.oPSANNREG_1_6.value=this.w_PSANNREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODESE_1_8.value==this.w_PSCODESE)
      this.oPgFrm.Page1.oPag.oPSCODESE_1_8.value=this.w_PSCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDESCRI_1_10.value==this.w_PSDESCRI)
      this.oPgFrm.Page1.oPag.oPSDESCRI_1_10.value=this.w_PSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSTIPDOC_1_12.value==this.w_PSTIPDOC)
      this.oPgFrm.Page1.oPag.oPSTIPDOC_1_12.value=this.w_PSTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMDOC_1_13.value==this.w_PSNUMDOC)
      this.oPgFrm.Page1.oPag.oPSNUMDOC_1_13.value=this.w_PSNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOC_1_14.value==this.w_PSALFDOC)
      this.oPgFrm.Page1.oPag.oPSALFDOC_1_14.value=this.w_PSALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.value==this.w_PSDATDOC)
      this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.value=this.w_PSDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSSERCOR_1_16.value==this.w_PSSERCOR)
      this.oPgFrm.Page1.oPag.oPSSERCOR_1_16.value=this.w_PSSERCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODMAG_1_17.value==this.w_PSCODMAG)
      this.oPgFrm.Page1.oPag.oPSCODMAG_1_17.value=this.w_PSCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCLAORI_1_18.RadioValue()==this.w_PSCLAORI)
      this.oPgFrm.Page1.oPag.oPSCLAORI_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSTIPORI_1_19.value==this.w_PSTIPORI)
      this.oPgFrm.Page1.oPag.oPSTIPORI_1_19.value=this.w_PSTIPORI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSEVAINI_1_20.value==this.w_PSEVAINI)
      this.oPgFrm.Page1.oPag.oPSEVAINI_1_20.value=this.w_PSEVAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSEVAFIN_1_21.value==this.w_PSEVAFIN)
      this.oPgFrm.Page1.oPag.oPSEVAFIN_1_21.value=this.w_PSEVAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPSFLDISP_1_22.RadioValue()==this.w_PSFLDISP)
      this.oPgFrm.Page1.oPag.oPSFLDISP_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODVAL_1_23.value==this.w_PSCODVAL)
      this.oPgFrm.Page1.oPag.oPSCODVAL_1_23.value=this.w_PSCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.RadioValue()==this.w_PSFLGFID)
      this.oPgFrm.Page1.oPag.oPSFLGFID_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODCLF_1_25.value==this.w_PSCODCLF)
      this.oPgFrm.Page1.oPag.oPSCODCLF_1_25.value=this.w_PSCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRICIVA_1_26.RadioValue()==this.w_FLRICIVA)
      this.oPgFrm.Page1.oPag.oFLRICIVA_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODDES_1_27.value==this.w_PSCODDES)
      this.oPgFrm.Page1.oPag.oPSCODDES_1_27.value=this.w_PSCODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODZON_1_28.value==this.w_PSCODZON)
      this.oPgFrm.Page1.oPag.oPSCODZON_1_28.value=this.w_PSCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODAGE_1_29.value==this.w_PSCODAGE)
      this.oPgFrm.Page1.oPag.oPSCODAGE_1_29.value=this.w_PSCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATTRA_1_30.value==this.w_PSDATTRA)
      this.oPgFrm.Page1.oPag.oPSDATTRA_1_30.value=this.w_PSDATTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSORATRA_1_31.value==this.w_PSORATRA)
      this.oPgFrm.Page1.oPag.oPSORATRA_1_31.value=this.w_PSORATRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSMINTRA_1_32.value==this.w_PSMINTRA)
      this.oPgFrm.Page1.oPag.oPSMINTRA_1_32.value=this.w_PSMINTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNOTAGG_1_33.value==this.w_PSNOTAGG)
      this.oPgFrm.Page1.oPag.oPSNOTAGG_1_33.value=this.w_PSNOTAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.RadioValue()==this.w_STADOCIM)
      this.oPgFrm.Page1.oPag.oSTADOCIM_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTREPSEC_1_35.RadioValue()==this.w_STREPSEC)
      this.oPgFrm.Page1.oPag.oSTREPSEC_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_44.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_44.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTCAMAG_1_50.value==this.w_MVTCAMAG)
      this.oPgFrm.Page1.oPag.oMVTCAMAG_1_50.value=this.w_MVTCAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDES_1_69.value==this.w_CMDES)
      this.oPgFrm.Page1.oPag.oCMDES_1_69.value=this.w_CMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_75.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_75.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_84.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_84.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_88.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_88.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_90.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_90.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZON_1_91.value==this.w_DESZON)
      this.oPgFrm.Page1.oPag.oDESZON_1_91.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_92.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_92.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCPDESCRI_1_100.value==this.w_CPDESCRI)
      this.oPgFrm.Page1.oPag.oCPDESCRI_1_100.value=this.w_CPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMGDESMAG_1_160.value==this.w_MGDESMAG)
      this.oPgFrm.Page1.oPag.oMGDESMAG_1_160.value=this.w_MGDESMAG
    endif
    cp_SetControlsValueExtFlds(this,'PIA_SPED')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PSTIPDOC)) or not(.w_MVFLVEAC='V' AND .w_ASSCES<>'S' AND .w_EMERIC = 'V' AND .w_MVCLADOC='DT' AND .w_TDRICNOM<>'A' AND GSVA_BPS( this, "CHKDOCTOGE")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSTIPDOC_1_12.SetFocus()
            i_bnoObbl = !empty(.w_PSTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PSNUMDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSNUMDOC_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PSNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PSSTATUS<>'S' OR !EMPTY(.w_PSALFDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSALFDOC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la serie dei documenti provvisori")
          case   not(CALCESER(.w_PSDATDOC, 'ZZZZ')=.w_PSCODESE)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATDOC_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data registrazione fuori dall'esercizio del documento")
          case   ((empty(.w_PSSERCOR)) or not(EMPTY(.w_CPCODCAU) OR .w_CPCODCAU=.w_PSTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSSERCOR_1_16.SetFocus()
            i_bnoObbl = !empty(.w_PSSERCOR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Configurazione inesistente oppure con causale documento differente da quella seleziona per la generazione del piano di spedizione")
          case   (empty(.w_PSCLAORI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCLAORI_1_18.SetFocus()
            i_bnoObbl = !empty(.w_PSCLAORI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CLAORI=.w_PSCLAORI AND .w_ASSCES1<>'S' AND .w_TDORDAPE<>'S' AND GSVA_BPS( this, "CHKDOCORIG"))  and not(empty(.w_PSTIPORI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSTIPORI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente, con gestione del riservato o con gestione cespiti attivata")
          case   ((empty(.w_PSEVAINI)) or not(EMPTY(.w_PSEVAFIN) OR .w_PSEVAINI<=.w_PSEVAFIN))  and (!.w_FLDOCSEL)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSEVAINI_1_20.SetFocus()
            i_bnoObbl = !empty(.w_PSEVAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data d'inizio evasione deve essere minore o uguale della data di fine evasione")
          case   ((empty(.w_PSEVAFIN)) or not(EMPTY(.w_PSEVAINI) OR .w_PSEVAINI<=.w_PSEVAFIN))  and (!.w_FLDOCSEL)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSEVAFIN_1_21.SetFocus()
            i_bnoObbl = !empty(.w_PSEVAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine evasione deve essere maggiore o uguale della data d'inizio evasione")
          case   (empty(.w_PSFLDISP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSFLDISP_1_22.SetFocus()
            i_bnoObbl = !empty(.w_PSFLDISP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and (!.w_FLDOCSEL)  and not(empty(.w_PSCODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODCLF_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (!.w_FLDOCSEL)  and not(empty(.w_PSCODZON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODZON_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (!.w_FLDOCSEL)  and not(empty(.w_PSCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODAGE_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   ((empty(.w_PSDATTRA)) or not(.w_PSDATTRA>=.w_PSDATDOC))  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATTRA_1_30.SetFocus()
            i_bnoObbl = !empty(.w_PSDATTRA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data trasporto minore della data documento")
          case   not(VAL(.w_PSORATRA)<24)  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSORATRA_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Ora trasporto non corretta")
          case   not(VAL(.w_PSMINTRA)<60)  and (.w_MVFLACCO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSMINTRA_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Minuto trasporto non correto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVA_MPS.CheckForm()
      if i_bres
        i_bres=  .GSVA_MPS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PSSERIAL = this.w_PSSERIAL
    this.o_PSDATREG = this.w_PSDATREG
    this.o_PSSTATUS = this.w_PSSTATUS
    this.o_PSTIPDOC = this.w_PSTIPDOC
    this.o_PSALFDOC = this.w_PSALFDOC
    this.o_PSDATDOC = this.w_PSDATDOC
    this.o_PSCLAORI = this.w_PSCLAORI
    this.o_PSCODCLF = this.w_PSCODCLF
    this.o_FLRICIVA = this.w_FLRICIVA
    * --- GSVA_MPS : Depends On
    this.GSVA_MPS.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsva_apsPag1 as StdContainer
  Width  = 776
  height = 527
  stdWidth  = 776
  stdheight = 527
  resizeXpos=444
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSNUMREG_1_3 as StdField with uid="BZDKZZTDLL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PSNUMREG", cQueryName = "PSNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo del piano di spedizione",;
    HelpContextID = 228574781,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=127, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oPSDATREG_1_4 as StdField with uid="DECRIHRDLZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PSDATREG", cQueryName = "PSDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 234563133,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=295, Top=12


  add object oPSSTATUS_1_5 as StdCombo with uid="XCVSSYYXDS",rtseq=5,rtrep=.f.,left=610,top=12,width=123,height=21;
    , ToolTipText = "Status dei documenti da generare: provvisori o confermati";
    , HelpContextID = 249501257;
    , cFormVar="w_PSSTATUS",RowSource=""+"Confermati,"+"Provvisori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSSTATUS_1_5.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPSSTATUS_1_5.GetRadio()
    this.Parent.oContained.w_PSSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oPSSTATUS_1_5.SetRadio()
    this.Parent.oContained.w_PSSTATUS=trim(this.Parent.oContained.w_PSSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_PSSTATUS=='N',1,;
      iif(this.Parent.oContained.w_PSSTATUS=='S',2,;
      0))
  endfunc

  add object oPSANNREG_1_6 as StdField with uid="NCQPPVOBUP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PSANNREG", cQueryName = "PSANNREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 229111357,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=196, Top=12, InputMask=replicate('X',4)

  add object oPSCODESE_1_8 as StdField with uid="ZWCNXCXZEO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PSCODESE", cQueryName = "PSCODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio dei documenti da generare",;
    HelpContextID = 595515,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=452, Top=12, InputMask=replicate('X',4)

  add object oPSDESCRI_1_10 as StdField with uid="SNDULHGGPW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PSDESCRI", cQueryName = "PSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note descrittive del piano di spedizione",;
    HelpContextID = 250553919,;
   bGlobalFont=.t.,;
    Height=21, Width=343, Left=127, Top=38, InputMask=replicate('X',40)

  add object oPSTIPDOC_1_12 as StdField with uid="LUDMJCCSGZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PSTIPDOC", cQueryName = "PSTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documenti da generare",;
    HelpContextID = 3922375,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PSTIPDOC"

  func oPSTIPDOC_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSTIPDOC_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSTIPDOC_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPSTIPDOC_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSVA1APS.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPSNUMDOC_1_13 as StdField with uid="UODTXQGIGB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PSNUMDOC", cQueryName = "PSNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero documento da generare a disposizione",;
    HelpContextID = 6306247,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=127, Top=89, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  add object oPSALFDOC_1_14 as StdField with uid="NQIGUHWGUA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PSALFDOC", cQueryName = "PSALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la serie dei documenti provvisori",;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 14289351,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=255, Top=90, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPSALFDOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSSTATUS<>'S' OR !EMPTY(.w_PSALFDOC))
    endwith
    return bRes
  endfunc

  add object oPSDATDOC_1_15 as StdField with uid="AHGHYNCDLY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PSDATDOC", cQueryName = "PSDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data registrazione fuori dall'esercizio del documento",;
    ToolTipText = "Data di registrazione dei documenti da generare",;
    HelpContextID = 317895,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=393, Top=90

  func oPSDATDOC_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CALCESER(.w_PSDATDOC, 'ZZZZ')=.w_PSCODESE)
    endwith
    return bRes
  endfunc

  add object oPSSERCOR_1_16 as StdField with uid="SLHDPZWBSJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PSSERCOR", cQueryName = "PSSERCOR",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Configurazione inesistente oppure con causale documento differente da quella seleziona per la generazione del piano di spedizione",;
    ToolTipText = "Configurazione campi di ordinamento/rottura da utilizzare per la generazione del piano di spedizione",;
    HelpContextID = 18868664,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=127, Top=114, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VACONPIA", cZoomOnZoom="GSVA_MCP", oKey_1_1="CPSERIAL", oKey_1_2="this.w_PSSERCOR"

  func oPSSERCOR_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSSERCOR_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSSERCOR_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VACONPIA','*','CPSERIAL',cp_AbsName(this.parent,'oPSSERCOR_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVA_MCP',"Configurazioni ordinamento/rottura",'GSVA_APS.VACONPIA_VZM',this.parent.oContained
  endproc
  proc oPSSERCOR_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSVA_MCP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CPSERIAL=this.parent.oContained.w_PSSERCOR
     i_obj.ecpSave()
  endproc

  add object oPSCODMAG_1_17 as StdField with uid="QEESXZQFJT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PSCODMAG", cQueryName = "PSCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino da utilizzare per i documenti da generare (se vuoto magazzino riga d'origine)",;
    HelpContextID = 134813245,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=127, Top=165, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PSCODMAG"

  func oPSCODMAG_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODMAG_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODMAG_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPSCODMAG_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oPSCODMAG_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PSCODMAG
     i_obj.ecpSave()
  endproc


  add object oPSCLAORI_1_18 as StdCombo with uid="GDGXRZGMCT",rtseq=15,rtrep=.f.,left=127,top=215,width=134,height=21;
    , ToolTipText = "Categoria di appartenenza dei documenti di origine";
    , HelpContextID = 165025343;
    , cFormVar="w_PSCLAORI",RowSource=""+"Ordine,"+"Documento interno", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPSCLAORI_1_18.RadioValue()
    return(iif(this.value =1,'OR',;
    iif(this.value =2,'DI',;
    space(2))))
  endfunc
  func oPSCLAORI_1_18.GetRadio()
    this.Parent.oContained.w_PSCLAORI = this.RadioValue()
    return .t.
  endfunc

  func oPSCLAORI_1_18.SetRadio()
    this.Parent.oContained.w_PSCLAORI=trim(this.Parent.oContained.w_PSCLAORI)
    this.value = ;
      iif(this.Parent.oContained.w_PSCLAORI=='OR',1,;
      iif(this.Parent.oContained.w_PSCLAORI=='DI',2,;
      0))
  endfunc

  add object oPSTIPORI_1_19 as StdField with uid="CZVVQCBSPL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PSTIPORI", cQueryName = "PSTIPORI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente, con gestione del riservato o con gestione cespiti attivata",;
    ToolTipText = "Causale documento di origine",;
    HelpContextID = 180627007,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=359, Top=215, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PSTIPORI"

  func oPSTIPORI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSTIPORI_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSTIPORI_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPSTIPORI_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSVA2APS.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oPSEVAINI_1_20 as StdField with uid="BUOWERNCRH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PSEVAINI", cQueryName = "PSEVAINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data d'inizio evasione deve essere minore o uguale della data di fine evasione",;
    ToolTipText = "Data di inizio prevista evasione",;
    HelpContextID = 203409857,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=127, Top=272

  func oPSEVAINI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSEVAINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_PSEVAFIN) OR .w_PSEVAINI<=.w_PSEVAFIN)
    endwith
    return bRes
  endfunc

  add object oPSEVAFIN_1_21 as StdField with uid="YMOQLEITIN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PSEVAFIN", cQueryName = "PSEVAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine evasione deve essere maggiore o uguale della data d'inizio evasione",;
    ToolTipText = "Data di fine prevista evasione",;
    HelpContextID = 253741500,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=328, Top=272

  func oPSEVAFIN_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSEVAFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_PSEVAINI) OR .w_PSEVAINI<=.w_PSEVAFIN)
    endwith
    return bRes
  endfunc


  add object oPSFLDISP_1_22 as StdCombo with uid="GIYWBOFKFX",rtseq=19,rtrep=.f.,left=610,top=272,width=123,height=21;
    , ToolTipText = "Controllo sulla disponibilitÓ dei documenti di origine";
    , HelpContextID = 67520070;
    , cFormVar="w_PSFLDISP",RowSource=""+"No,"+"Singola riga,"+"Intero documento", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPSFLDISP_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oPSFLDISP_1_22.GetRadio()
    this.Parent.oContained.w_PSFLDISP = this.RadioValue()
    return .t.
  endfunc

  func oPSFLDISP_1_22.SetRadio()
    this.Parent.oContained.w_PSFLDISP=trim(this.Parent.oContained.w_PSFLDISP)
    this.value = ;
      iif(this.Parent.oContained.w_PSFLDISP=='N',1,;
      iif(this.Parent.oContained.w_PSFLDISP=='S',2,;
      iif(this.Parent.oContained.w_PSFLDISP=='D',3,;
      0)))
  endfunc

  add object oPSCODVAL_1_23 as StdField with uid="TFISTWDSDJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PSCODVAL", cQueryName = "PSCODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice eventuale valuta di selezione (vuoto = no selezione)",;
    HelpContextID = 17372738,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=296, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PSCODVAL"

  func oPSCODVAL_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSCODVAL_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODVAL_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODVAL_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPSCODVAL_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oPSCODVAL_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PSCODVAL
     i_obj.ecpSave()
  endproc

  add object oPSFLGFID_1_24 as StdCheck with uid="XRCYWQCZZQ",rtseq=21,rtrep=.f.,left=610, top=300, caption="Controllo fido",;
    ToolTipText = "Se attivo viene effettuato il controllo fido",;
    HelpContextID = 248101318,;
    cFormVar="w_PSFLGFID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPSFLGFID_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPSFLGFID_1_24.GetRadio()
    this.Parent.oContained.w_PSFLGFID = this.RadioValue()
    return .t.
  endfunc

  func oPSFLGFID_1_24.SetRadio()
    this.Parent.oContained.w_PSFLGFID=trim(this.Parent.oContained.w_PSFLGFID)
    this.value = ;
      iif(this.Parent.oContained.w_PSFLGFID=='S',1,;
      0)
  endfunc

  func oPSFLGFID_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSSTATUS<>'S')
    endwith
   endif
  endfunc

  func oPSFLGFID_1_24.mHide()
    with this.Parent.oContained
      return (NVL(.w_AZFLFIDO,' ')<>'S')
    endwith
  endfunc

  add object oPSCODCLF_1_25 as StdField with uid="MMRKZEQIQI",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PSCODCLF", cQueryName = "PSCODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Eventuale cliente di selezione (vuoto = no selezione)",;
    HelpContextID = 32958916,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=127, Top=321, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PSTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_PSCODCLF"

  func oPSCODCLF_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSCODCLF_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
      if .not. empty(.w_PSCODDES)
        bRes2=.link_1_27('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPSCODCLF_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODCLF_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PSTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PSTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPSCODCLF_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"Clienti",'',this.parent.oContained
  endproc
  proc oPSCODCLF_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PSTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_PSCODCLF
     i_obj.ecpSave()
  endproc

  add object oFLRICIVA_1_26 as StdCheck with uid="KJZMSHAWAR",rtseq=23,rtrep=.f.,left=610, top=326, caption="Ricalcola codici I.V.A.",;
    ToolTipText = "Ricalcola codici I.V.A.",;
    HelpContextID = 66322071,;
    cFormVar="w_FLRICIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRICIVA_1_26.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLRICIVA_1_26.GetRadio()
    this.Parent.oContained.w_FLRICIVA = this.RadioValue()
    return .t.
  endfunc

  func oFLRICIVA_1_26.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLRICIVA==.T.,1,;
      0)
  endfunc

  add object oPSCODDES_1_27 as StdField with uid="NORTUNABJC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PSCODDES", cQueryName = "PSCODDES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice sede inesistente oppure obsoleto",;
    ToolTipText = "Eventuale destinazione particolare di selezione (vuoto = no selezione)",;
    HelpContextID = 252253769,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=346, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_PSTIPCLF", oKey_2_1="DDCODICE", oKey_2_2="this.w_PSCODCLF", oKey_3_1="DDCODDES", oKey_3_2="this.w_PSCODDES"

  func oPSCODDES_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_PSCODCLF) AND !.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSCODDES_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODDES_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODDES_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PSTIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_PSCODCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_PSTIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_PSCODCLF)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oPSCODDES_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni diverse",'GSVE_BZD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oPSCODZON_1_28 as StdField with uid="PFOBPYHWFQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PSCODZON", cQueryName = "PSCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleta",;
    ToolTipText = "Codice zona di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 183953852,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=371, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PSCODZON"

  func oPSCODZON_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSCODZON_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODZON_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODZON_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPSCODZON_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oPSCODZON_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_PSCODZON
     i_obj.ecpSave()
  endproc

  add object oPSCODAGE_1_29 as StdField with uid="SJEHZJFWZE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PSCODAGE", cQueryName = "PSCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice eventuale agente di selezione (vuoto = no selezione)",;
    HelpContextID = 201922107,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=395, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PSCODAGE"

  func oPSCODAGE_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_FLDOCSEL)
    endwith
   endif
  endfunc

  func oPSCODAGE_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODAGE_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODAGE_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPSCODAGE_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPSCODAGE_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PSCODAGE
     i_obj.ecpSave()
  endproc

  add object oPSDATTRA_1_30 as StdField with uid="OQSEVZJAYI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PSDATTRA", cQueryName = "PSDATTRA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data trasporto minore della data documento",;
    ToolTipText = "Data del trasporto",;
    HelpContextID = 268117559,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=127, Top=419

  func oPSDATTRA_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oPSDATTRA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSDATTRA>=.w_PSDATDOC)
    endwith
    return bRes
  endfunc

  add object oPSORATRA_1_31 as StdField with uid="FPJPZQFLMT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PSORATRA", cQueryName = "PSORATRA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Ora trasporto non corretta",;
    ToolTipText = "Ora del trasporto",;
    HelpContextID = 249353783,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=261, Top=419, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPSORATRA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oPSORATRA_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PSORATRA)<24)
    endwith
    return bRes
  endfunc

  add object oPSMINTRA_1_32 as StdField with uid="MDMRISDJVA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PSMINTRA", cQueryName = "PSMINTRA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Minuto trasporto non correto",;
    ToolTipText = "Minuto del trasporto",;
    HelpContextID = 262387255,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=294, Top=419, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oPSMINTRA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  func oPSMINTRA_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PSMINTRA)<60)
    endwith
    return bRes
  endfunc

  add object oPSNOTAGG_1_33 as StdField with uid="XKUKGNZGSY",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PSNOTAGG", cQueryName = "PSNOTAGG",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive sul trasporto",;
    HelpContextID = 218744381,;
   bGlobalFont=.t.,;
    Height=21, Width=344, Left=383, Top=419, InputMask=replicate('X',40)

  func oPSNOTAGG_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVFLACCO='S')
    endwith
   endif
  endfunc

  add object oSTADOCIM_1_34 as StdCheck with uid="MWDUROXYAH",rtseq=31,rtrep=.f.,left=127, top=444, caption="Stampa immediata",;
    ToolTipText = "Se attivo: stampa immediata dei documenti al termine della elaborazione",;
    HelpContextID = 246282099,;
    cFormVar="w_STADOCIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADOCIM_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTADOCIM_1_34.GetRadio()
    this.Parent.oContained.w_STADOCIM = this.RadioValue()
    return .t.
  endfunc

  func oSTADOCIM_1_34.SetRadio()
    this.Parent.oContained.w_STADOCIM=trim(this.Parent.oContained.w_STADOCIM)
    this.value = ;
      iif(this.Parent.oContained.w_STADOCIM=='S',1,;
      0)
  endfunc

  func oSTADOCIM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oSTADOCIM_1_34.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc


  add object oSTREPSEC_1_35 as StdCombo with uid="ILFBHAZQOE",rtseq=32,rtrep=.f.,left=127,top=468,width=170,height=21;
    , ToolTipText = "Stampa report secondari";
    , HelpContextID = 247465833;
    , cFormVar="w_STREPSEC",RowSource=""+"Tutti i report secondari,"+"Nessun report secondario,"+"Escludi report opzionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTREPSEC_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oSTREPSEC_1_35.GetRadio()
    this.Parent.oContained.w_STREPSEC = this.RadioValue()
    return .t.
  endfunc

  func oSTREPSEC_1_35.SetRadio()
    this.Parent.oContained.w_STREPSEC=trim(this.Parent.oContained.w_STREPSEC)
    this.value = ;
      iif(this.Parent.oContained.w_STREPSEC=='S',1,;
      iif(this.Parent.oContained.w_STREPSEC=='N',2,;
      iif(this.Parent.oContained.w_STREPSEC=='C',3,;
      0)))
  endfunc


  add object oBtn_1_36 as StdButton with uid="YLZOHLFLUQ",left=608, top=477, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza dettaglio dei documenti da generare in base alle selezioni effettuate";
    , HelpContextID = 55919767;
    , tabstop=.f., caption='\<Sel. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSVA_BPS(this.Parent.oContained,"SELEZDOCUM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_PSTIPDOC) AND !EMPTY(.w_PSEVAINI) AND !EMPTY(.w_PSEVAFIN) AND .cFunction='Load' AND (.w_PSSTATUS<>'S' OR NOT EMPTY(.w_PSALFDOC)))
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Load')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="AFWBALVSOS",left=660, top=477, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare il piano di spedizione";
    , HelpContextID = 160047642;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_PSTIPDOC))
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="BKXLMQLDYX",left=713, top=477, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 254096966;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESDOC_1_44 as StdField with uid="AVQVRVQXVT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22083786,;
   bGlobalFont=.t.,;
    Height=21, Width=280, Left=190, Top=63, InputMask=replicate('X',35)

  add object oMVTCAMAG_1_50 as StdField with uid="DZPFWMUSUF",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MVTCAMAG", cQueryName = "MVTCAMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale magazzino utilizzata nei documenti da generare",;
    HelpContextID = 130951437,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=127, Top=139, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MVTCAMAG"

  func oMVTCAMAG_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCMDES_1_69 as StdField with uid="TPICUTCHJC",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CMDES", cQueryName = "CMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68215002,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=212, Top=139, InputMask=replicate('X',35)

  add object oDESORI_1_75 as StdField with uid="QCOQLVDJLL",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 185989322,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=424, Top=215, InputMask=replicate('X',35)

  add object oDESVAL_1_84 as StdField with uid="ANDACSFIJD",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 153024714,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=191, Top=296, InputMask=replicate('X',35)

  add object oDESCLF_1_88 as StdField with uid="QIJPAOXSCI",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243398858,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=262, Top=321, InputMask=replicate('X',40)

  add object oNOMDES_1_90 as StdField with uid="TCCVZGINZX",rtseq=63,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 32591402,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=192, Top=346, InputMask=replicate('X',80)

  add object oDESZON_1_91 as StdField with uid="NBVIFUXLYP",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 104528074,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=192, Top=371, InputMask=replicate('X',35)

  add object oDESAGE_1_92 as StdField with uid="XDYRFETZIW",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 265550026,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=192, Top=395, InputMask=replicate('X',35)

  add object oCPDESCRI_1_100 as StdField with uid="MCTSHDYPRW",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CPDESCRI", cQueryName = "CPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 250552943,;
   bGlobalFont=.t.,;
    Height=21, Width=238, Left=232, Top=114, InputMask=replicate('X',30)


  add object oLinkPC_1_116 as StdButton with uid="QRELQMQLNR",left=608, top=477, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per accedere al dettaglio dei documenti generati";
    , HelpContextID = 112507380;
    , caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_116.Click()
      this.Parent.oContained.GSVA_MPS.LinkPCClick()
    endproc

  func oLinkPC_1_116.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' OR ( .cFunction='Query' AND EMPTY(.w_PSSERIAL) ))
     endwith
    endif
  endfunc


  add object oBtn_1_131 as StdButton with uid="HCRBKHSUQT",left=556, top=477, width=48,height=45,;
    CpPicture="BMP\LOG.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log di generazione documenti";
    , HelpContextID = 159616586;
    , tabstop=.f., caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_131.Click()
      this.parent.oContained.NotifyEvent("ShowLog")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_131.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' OR EMPTY(.w_GDSERIAL))
     endwith
    endif
  endfunc

  add object oMGDESMAG_1_160 as StdField with uid="DRLIMKZJCI",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MGDESMAG", cQueryName = "MGDESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 149887501,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=212, Top=165, InputMask=replicate('X',30)


  add object oBtn_1_163 as StdButton with uid="ELNSICJVGH",left=504, top=477, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa dei documenti associati";
    , HelpContextID = 250156838;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_163.Click()
      this.parent.oContained.NotifyEvent("PrintDoc")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_163.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND !EMPTY(.w_PSSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_163.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' OR ( .cFunction='Query' AND EMPTY(.w_PSSERIAL) ))
     endwith
    endif
  endfunc


  add object oBtn_1_180 as StdButton with uid="TQSULRUZKM",left=685, top=38, width=48,height=45,;
    CpPicture="BMP\DOCCON.BMP", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Premere per rendere confermati i documenti generati in stato provvisorio";
    , HelpContextID = 13442681;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_180.Click()
      with this.Parent.oContained
        GSVA_BPS(this.Parent.oContained,"CONFDOCGEN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_180.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' OR EMPTY(.w_PSSERIAL) OR .w_PSSTATUS<>'S')
     endwith
    endif
  endfunc

  add object oStr_1_7 as StdString with uid="PRZSDNZXPW",Visible=.t., Left=183, Top=14,;
    Alignment=2, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="HGULSMLVJU",Visible=.t., Left=376, Top=13,;
    Alignment=1, Width=72, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RVYOMPLFKL",Visible=.t., Left=6, Top=40,;
    Alignment=1, Width=119, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="ADNOSAUSNF",Visible=.t., Left=6, Top=116,;
    Alignment=1, Width=119, Height=15,;
    Caption="Config. ord./rott.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VKLRGNVTME",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=119, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="IXEYFKGVWL",Visible=.t., Left=245, Top=13,;
    Alignment=1, Width=41, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="NIRZZUDSML",Visible=.t., Left=510, Top=15,;
    Alignment=1, Width=94, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QTYOCOTXYA",Visible=.t., Left=6, Top=63,;
    Alignment=1, Width=119, Height=15,;
    Caption="Doc.da generare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="LCSEPCFTKX",Visible=.t., Left=6, Top=139,;
    Alignment=1, Width=119, Height=18,;
    Caption="Cau. magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="XEFBATLSRU",Visible=.t., Left=6, Top=166,;
    Alignment=1, Width=119, Height=15,;
    Caption="Cod. magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="GHYXZDSXKF",Visible=.t., Left=6, Top=92,;
    Alignment=1, Width=119, Height=15,;
    Caption="N.1░doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="DQTWNZDYRF",Visible=.t., Left=248, Top=92,;
    Alignment=0, Width=5, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="EETJHZNNYS",Visible=.t., Left=342, Top=93,;
    Alignment=1, Width=48, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="HYMUOGQIDC",Visible=.t., Left=10, Top=193,;
    Alignment=0, Width=128, Height=15,;
    Caption="Documenti di origine"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="RSVFVERYEY",Visible=.t., Left=6, Top=215,;
    Alignment=1, Width=119, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="NBHPRBWMEX",Visible=.t., Left=270, Top=215,;
    Alignment=1, Width=87, Height=15,;
    Caption="Documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="XUIKEXYDMC",Visible=.t., Left=9, Top=242,;
    Alignment=0, Width=157, Height=15,;
    Caption="Ulteriori selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="ANOFGUNPTU",Visible=.t., Left=6, Top=274,;
    Alignment=1, Width=119, Height=15,;
    Caption="Da data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="NBHEXDELSU",Visible=.t., Left=214, Top=274,;
    Alignment=1, Width=111, Height=15,;
    Caption="A data evasione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="HSWTTNIOUV",Visible=.t., Left=6, Top=296,;
    Alignment=1, Width=119, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="ENEVDGMHDK",Visible=.t., Left=457, Top=275,;
    Alignment=1, Width=148, Height=15,;
    Caption="Controllo disponibilitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="CDRIOADYUB",Visible=.t., Left=6, Top=321,;
    Alignment=1, Width=119, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="FHJYWKXVJD",Visible=.t., Left=6, Top=346,;
    Alignment=1, Width=119, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="HVNWQTSPXY",Visible=.t., Left=6, Top=371,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="JUVCOQRTOR",Visible=.t., Left=6, Top=395,;
    Alignment=1, Width=119, Height=15,;
    Caption="1^Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="KQNZOJESMF",Visible=.t., Left=6, Top=421,;
    Alignment=1, Width=119, Height=15,;
    Caption="Data trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="ZEPWHGOMEW",Visible=.t., Left=210, Top=421,;
    Alignment=1, Width=43, Height=15,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="OCBWCPKAEE",Visible=.t., Left=329, Top=421,;
    Alignment=1, Width=50, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="JHWQMZVEXT",Visible=.t., Left=284, Top=421,;
    Alignment=1, Width=7, Height=15,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_164 as StdString with uid="YQPOXLZVJK",Visible=.t., Left=6, Top=468,;
    Alignment=1, Width=119, Height=15,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_79 as StdBox with uid="JWOOBKGTTZ",left=7, top=208, width=735,height=1

  add object oBox_1_81 as StdBox with uid="DBNNLFCUNI",left=6, top=257, width=735,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_aps','PIA_SPED','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PSSERIAL=PIA_SPED.PSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
