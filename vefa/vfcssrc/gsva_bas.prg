* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bas                                                        *
*              Variazione listino prezzi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_456]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-05                                                      *
* Last revis.: 2008-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bas",oParentObject,m.pOPER)
return(i_retval)

define class tgsva_bas as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_FLAGGIO = space(1)
  w_MESS = space(10)
  w_OK = .f.
  w_CLSERIAL = space(10)
  w_CODICE = space(20)
  * --- WorkFile variables
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  RIC_PREZ_idx=0
  TMPART_idx=0
  TMPZOOM2_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera aggiorna Listini Prezzi (GSVA_KAS)
    * --- Inizializzazione variabili globali.
    this.w_FLAGGIO = this.oparentobject.w_FLAGGIO
    do case
      case this.pOPER="A"
        if AH_YESNO("Attenzione, saranno aggiornati massivamente i listini secondo le selezioni effettuate. Si desidera continuare?")
          * --- Create temporary table TMPZOOM2
          i_nIdx=cp_AddTableDef('TMPZOOM2') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSVA2KAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPZOOM2_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.oParentObject.w_FLSELE = 0
          ND = this.oParentObject.w_Zoom.cCursor
          select (ND) 
          GO TOP 
 SCAN FOR XCHK=1
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          this.oParentObject.w_FLSELE = 1
          * --- Insert into TMPZOOM2
          i_nConn=i_TableProp[this.TMPZOOM2_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM2_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPZOOM2_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CLSERIAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CLSERIAL),'TMPZOOM2','CLSERIAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CLSERIAL',this.w_CLSERIAL)
            insert into (i_cTable) (CLSERIAL &i_ccchkf. );
               values (;
                 this.w_CLSERIAL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
          if this.oParentObject.w_FLSELE=1
            * --- Create temporary table TMPART
            i_nIdx=cp_AddTableDef('TMPART') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('GSVA_KAS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPART_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Faccio una read fittizia per avere i_rows
            * --- Read from TMPART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPART_idx,2],.t.,this.TMPART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CODART"+;
                " from "+i_cTable+" TMPART where ";
                    +"1 = "+cp_ToStrODBC(1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CODART;
                from (i_cTable) where;
                    1 = 1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODICE = NVL(cp_ToDate(_read_.CODART),cp_NullValue(_read_.CODART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_rows>0
              GSAR_BAG(this,"M")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if !this.w_OK 
                * --- Se non ha aggiornato /inserito niente lo segnalo
                ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
              endif
            else
              ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
            endif
            this.oParentObject.w_FLSELE = 0
            this.oParentObject.w_SELEZI = "D"
            This.oparentobject.NotifyEvent("Lanciazoom")
          else
            ah_ErrorMsg("Non ci sono righe selezionate per l'aggiornamento")
            i_retcode = 'stop'
            return
          endif
          * --- creo la tabella
        endif
      case this.pOPER="S"
        * --- Seleziona/Deseleziona Tutto
        ND = this.oParentObject.w_Zoom.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &ND SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &ND SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOPER="D"
        * --- Drop temporary table TMPZOOM2
        i_nIdx=cp_GetTableDefIdx('TMPZOOM2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPZOOM2')
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='LIS_SCAG'
    this.cWorkTables[2]='LIS_TINI'
    this.cWorkTables[3]='RIC_PREZ'
    this.cWorkTables[4]='*TMPART'
    this.cWorkTables[5]='*TMPZOOM2'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
