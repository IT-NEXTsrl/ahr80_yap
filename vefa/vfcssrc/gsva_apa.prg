* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_apa                                                        *
*              Parametri EDI                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-18                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsva_apa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsva_apa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsva_apa")
  return

* --- Class definition
define class tgsva_apa as StdPCForm
  Width  = 638
  Height = 266
  Top    = 10
  Left   = 10
  cComment = "Parametri EDI"
  cPrg = "gsva_apa"
  HelpContextID=160068201
  add object cnt as tcgsva_apa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsva_apa as PCContext
  w_PACODAZI = space(5)
  w_PAPATHED = space(200)
  w_PADIRFIL = space(200)
  w_PADIRSPO = space(200)
  w_PADIRXML = space(200)
  w_PAGENERR = space(1)
  w_PACRIPEV = space(1)
  w_PAQTAMIN = 0
  w_PAGIOLIM = 0
  w_PASCADEN = space(1)
  w_LPATH = space(1)
  proc Save(oFrom)
    this.w_PACODAZI = oFrom.w_PACODAZI
    this.w_PAPATHED = oFrom.w_PAPATHED
    this.w_PADIRFIL = oFrom.w_PADIRFIL
    this.w_PADIRSPO = oFrom.w_PADIRSPO
    this.w_PADIRXML = oFrom.w_PADIRXML
    this.w_PAGENERR = oFrom.w_PAGENERR
    this.w_PACRIPEV = oFrom.w_PACRIPEV
    this.w_PAQTAMIN = oFrom.w_PAQTAMIN
    this.w_PAGIOLIM = oFrom.w_PAGIOLIM
    this.w_PASCADEN = oFrom.w_PASCADEN
    this.w_LPATH = oFrom.w_LPATH
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PACODAZI = this.w_PACODAZI
    oTo.w_PAPATHED = this.w_PAPATHED
    oTo.w_PADIRFIL = this.w_PADIRFIL
    oTo.w_PADIRSPO = this.w_PADIRSPO
    oTo.w_PADIRXML = this.w_PADIRXML
    oTo.w_PAGENERR = this.w_PAGENERR
    oTo.w_PACRIPEV = this.w_PACRIPEV
    oTo.w_PAQTAMIN = this.w_PAQTAMIN
    oTo.w_PAGIOLIM = this.w_PAGIOLIM
    oTo.w_PASCADEN = this.w_PASCADEN
    oTo.w_LPATH = this.w_LPATH
    PCContext::Load(oTo)
enddefine

define class tcgsva_apa as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 638
  Height = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-15"
  HelpContextID=160068201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  PAR_VEFA_IDX = 0
  AZIENDA_IDX = 0
  cFile = "PAR_VEFA"
  cKeySelect = "PACODAZI"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cKeyWhereODBCqualified = '"PAR_VEFA.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cPrg = "gsva_apa"
  cComment = "Parametri EDI"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODAZI = space(5)
  w_PAPATHED = space(200)
  o_PAPATHED = space(200)
  w_PADIRFIL = space(200)
  o_PADIRFIL = space(200)
  w_PADIRSPO = space(200)
  o_PADIRSPO = space(200)
  w_PADIRXML = space(200)
  o_PADIRXML = space(200)
  w_PAGENERR = space(1)
  w_PACRIPEV = space(1)
  w_PAQTAMIN = 0
  w_PAGIOLIM = 0
  w_PASCADEN = space(1)
  w_LPATH = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_apaPag1","gsva_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 101620746
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPAPATHED_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PAR_VEFA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_VEFA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_VEFA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsva_apa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_VEFA where PACODAZI=KeySet.PACODAZI
    *
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_VEFA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_VEFA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_VEFA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LPATH = .f.
        .w_PACODAZI = NVL(PACODAZI,space(5))
          * evitabile
          *.link_1_1('Load')
        .w_PAPATHED = NVL(PAPATHED,space(200))
        .w_PADIRFIL = NVL(PADIRFIL,space(200))
        .w_PADIRSPO = NVL(PADIRSPO,space(200))
        .w_PADIRXML = NVL(PADIRXML,space(200))
        .w_PAGENERR = NVL(PAGENERR,space(1))
        .w_PACRIPEV = NVL(PACRIPEV,space(1))
        .w_PAQTAMIN = NVL(PAQTAMIN,0)
        .w_PAGIOLIM = NVL(PAGIOLIM,0)
        .w_PASCADEN = NVL(PASCADEN,space(1))
        cp_LoadRecExtFlds(this,'PAR_VEFA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PACODAZI = space(5)
      .w_PAPATHED = space(200)
      .w_PADIRFIL = space(200)
      .w_PADIRSPO = space(200)
      .w_PADIRXML = space(200)
      .w_PAGENERR = space(1)
      .w_PACRIPEV = space(1)
      .w_PAQTAMIN = 0
      .w_PAGIOLIM = 0
      .w_PASCADEN = space(1)
      .w_LPATH = .f.
      if .cFunction<>"Filter"
        .w_PACODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PACODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,6,.f.)
        .w_PACRIPEV = 'I'
          .DoRTCalc(8,9,.f.)
        .w_PASCADEN = 'I'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_VEFA')
    this.DoRTCalc(11,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPAPATHED_1_2.enabled = i_bVal
      .Page1.oPag.oPADIRFIL_1_4.enabled = i_bVal
      .Page1.oPag.oPADIRSPO_1_6.enabled = i_bVal
      .Page1.oPag.oPADIRXML_1_8.enabled = i_bVal
      .Page1.oPag.oPAGENERR_1_10.enabled = i_bVal
      .Page1.oPag.oPACRIPEV_1_11.enabled = i_bVal
      .Page1.oPag.oPAQTAMIN_1_12.enabled = i_bVal
      .Page1.oPag.oPAGIOLIM_1_13.enabled = i_bVal
      .Page1.oPag.oPASCADEN_1_14.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'PAR_VEFA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPATHED,"PAPATHED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADIRFIL,"PADIRFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADIRSPO,"PADIRSPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADIRXML,"PADIRXML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGENERR,"PAGENERR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACRIPEV,"PACRIPEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAQTAMIN,"PAQTAMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGIOLIM,"PAGIOLIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASCADEN,"PASCADEN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_VEFA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_VEFA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_VEFA')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_VEFA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODAZI,PAPATHED,PADIRFIL,PADIRSPO,PADIRXML"+;
                  ",PAGENERR,PACRIPEV,PAQTAMIN,PAGIOLIM,PASCADEN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PACODAZI)+;
                  ","+cp_ToStrODBC(this.w_PAPATHED)+;
                  ","+cp_ToStrODBC(this.w_PADIRFIL)+;
                  ","+cp_ToStrODBC(this.w_PADIRSPO)+;
                  ","+cp_ToStrODBC(this.w_PADIRXML)+;
                  ","+cp_ToStrODBC(this.w_PAGENERR)+;
                  ","+cp_ToStrODBC(this.w_PACRIPEV)+;
                  ","+cp_ToStrODBC(this.w_PAQTAMIN)+;
                  ","+cp_ToStrODBC(this.w_PAGIOLIM)+;
                  ","+cp_ToStrODBC(this.w_PASCADEN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_VEFA')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_VEFA')
        cp_CheckDeletedKey(i_cTable,0,'PACODAZI',this.w_PACODAZI)
        INSERT INTO (i_cTable);
              (PACODAZI,PAPATHED,PADIRFIL,PADIRSPO,PADIRXML,PAGENERR,PACRIPEV,PAQTAMIN,PAGIOLIM,PASCADEN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PAPATHED;
                  ,this.w_PADIRFIL;
                  ,this.w_PADIRSPO;
                  ,this.w_PADIRXML;
                  ,this.w_PAGENERR;
                  ,this.w_PACRIPEV;
                  ,this.w_PAQTAMIN;
                  ,this.w_PAGIOLIM;
                  ,this.w_PASCADEN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_VEFA_IDX,i_nConn)
      *
      * update PAR_VEFA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_VEFA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PAPATHED="+cp_ToStrODBC(this.w_PAPATHED)+;
             ",PADIRFIL="+cp_ToStrODBC(this.w_PADIRFIL)+;
             ",PADIRSPO="+cp_ToStrODBC(this.w_PADIRSPO)+;
             ",PADIRXML="+cp_ToStrODBC(this.w_PADIRXML)+;
             ",PAGENERR="+cp_ToStrODBC(this.w_PAGENERR)+;
             ",PACRIPEV="+cp_ToStrODBC(this.w_PACRIPEV)+;
             ",PAQTAMIN="+cp_ToStrODBC(this.w_PAQTAMIN)+;
             ",PAGIOLIM="+cp_ToStrODBC(this.w_PAGIOLIM)+;
             ",PASCADEN="+cp_ToStrODBC(this.w_PASCADEN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_VEFA')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        UPDATE (i_cTable) SET;
              PAPATHED=this.w_PAPATHED;
             ,PADIRFIL=this.w_PADIRFIL;
             ,PADIRSPO=this.w_PADIRSPO;
             ,PADIRXML=this.w_PADIRXML;
             ,PAGENERR=this.w_PAGENERR;
             ,PACRIPEV=this.w_PACRIPEV;
             ,PAQTAMIN=this.w_PAQTAMIN;
             ,PAGIOLIM=this.w_PAGIOLIM;
             ,PASCADEN=this.w_PASCADEN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_VEFA_IDX,i_nConn)
      *
      * delete PAR_VEFA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_VEFA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_VEFA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        if .o_PAPATHED<>.w_PAPATHED
          .Calculate_DJAIDTCSRD()
        endif
        if .o_PADIRXML<>.w_PADIRXML
          .Calculate_FORBCAQGLL()
        endif
        if .o_PADIRFIL<>.w_PADIRFIL
          .Calculate_PYPBRNDNYN()
        endif
        if .o_PADIRSPO<>.w_PADIRSPO
          .Calculate_HELKZIBCPZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SFONVUKBBV()
    with this
          * --- Update end
          GSAR_BAM(this;
              ,'modifica';
             )
          GSVA_BAP(this;
              ,'E';
              ,.w_PAPATHED;
             )
    endwith
  endproc
  proc Calculate_DJAIDTCSRD()
    with this
          * --- Controllo path export file
          .w_PAPATHED = IIF(right(alltrim(.w_PAPATHED),1)='\' or empty(.w_PAPATHED),.w_PAPATHED,alltrim(.w_PAPATHED)+iif(len(alltrim(.w_PAPATHED))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PAPATHED,'F')
    endwith
  endproc
  proc Calculate_FORBCAQGLL()
    with this
          * --- Controllo path generazione file XML
          .w_PADIRXML = IIF(right(alltrim(.w_PADIRXML),1)='\' or empty(.w_PADIRXML),.w_PADIRXML,alltrim(.w_PADIRXML)+iif(len(alltrim(.w_PADIRXML))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PADIRXML,'F')
    endwith
  endproc
  proc Calculate_PYPBRNDNYN()
    with this
          * --- Controllo path selezione file import
          .w_PADIRFIL = IIF(right(alltrim(.w_PADIRFIL),1)='\' or empty(.w_PADIRFIL),.w_PADIRFIL,alltrim(.w_PADIRFIL)+iif(len(alltrim(.w_PADIRFIL))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PADIRFIL,'F')
          .w_LPATH = chkpath(.w_PADIRFIL,.w_PADIRSPO)
    endwith
  endproc
  proc Calculate_HELKZIBCPZ()
    with this
          * --- Controllo path spostamento file
          .w_PADIRSPO = IIF(right(alltrim(.w_PADIRSPO),1)='\' or empty(.w_PADIRSPO),.w_PADIRSPO,alltrim(.w_PADIRSPO)+iif(len(alltrim(.w_PADIRSPO))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PADIRSPO,'F')
          .w_LPATH = chkpath(.w_PADIRSPO,.w_PADIRFIL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.enabled = this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.enabled = this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPASCADEN_1_14.enabled = this.oPgFrm.Page1.oPag.oPASCADEN_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPACRIPEV_1_11.visible=!this.oPgFrm.Page1.oPag.oPACRIPEV_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.visible=!this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.visible=!this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.mHide()
    this.oPgFrm.Page1.oPag.oPASCADEN_1_14.visible=!this.oPgFrm.Page1.oPag.oPASCADEN_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Update end") or lower(cEvent)==lower("Insert end")
          .Calculate_SFONVUKBBV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PACODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PACODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PACODAZI=i_codazi
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PACODAZI = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPAPATHED_1_2.value==this.w_PAPATHED)
      this.oPgFrm.Page1.oPag.oPAPATHED_1_2.value=this.w_PAPATHED
    endif
    if not(this.oPgFrm.Page1.oPag.oPADIRFIL_1_4.value==this.w_PADIRFIL)
      this.oPgFrm.Page1.oPag.oPADIRFIL_1_4.value=this.w_PADIRFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oPADIRSPO_1_6.value==this.w_PADIRSPO)
      this.oPgFrm.Page1.oPag.oPADIRSPO_1_6.value=this.w_PADIRSPO
    endif
    if not(this.oPgFrm.Page1.oPag.oPADIRXML_1_8.value==this.w_PADIRXML)
      this.oPgFrm.Page1.oPag.oPADIRXML_1_8.value=this.w_PADIRXML
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGENERR_1_10.RadioValue()==this.w_PAGENERR)
      this.oPgFrm.Page1.oPag.oPAGENERR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACRIPEV_1_11.RadioValue()==this.w_PACRIPEV)
      this.oPgFrm.Page1.oPag.oPACRIPEV_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.value==this.w_PAQTAMIN)
      this.oPgFrm.Page1.oPag.oPAQTAMIN_1_12.value=this.w_PAQTAMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.value==this.w_PAGIOLIM)
      this.oPgFrm.Page1.oPag.oPAGIOLIM_1_13.value=this.w_PAGIOLIM
    endif
    if not(this.oPgFrm.Page1.oPag.oPASCADEN_1_14.RadioValue()==this.w_PASCADEN)
      this.oPgFrm.Page1.oPag.oPASCADEN_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_VEFA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PACODAZI=i_codazi)  and not(empty(.w_PACODAZI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODAZI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PAQTAMIN))  and not(1=1)  and (.w_PACRIPEV='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAQTAMIN_1_12.SetFocus()
            i_bnoObbl = !empty(.w_PAQTAMIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PAGIOLIM))  and not(1=1)  and (.w_PACRIPEV='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAGIOLIM_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PAGIOLIM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsva_apa
      if i_bRes
         IF (empty(.w_PADIRSPO) or empty(.w_PADIRXML) or empty(.w_PADIRFIL) ) AND Not empty(Alltrim(Nvl(.w_PADIRSPO,' ')+Nvl(.w_PADIRXML,' ')+Nvl(.w_PADIRFIL,' ')))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg = Ah_MsgFormat("Uno o pi� percorsi per l'import non sono correttamente valorizzati")
         ENDIF
      ENDIF
      if i_bRes
         IF not(empty(.w_PAPATHED) or directory(alltrim(.w_PAPATHED)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg = Ah_MsgFormat("La cartella impostata per la directory di salvataggio file EDI non esiste")
         ENDIF
      ENDIF
      if i_bRes
         IF not(empty(.w_PADIRXML) or directory(alltrim(.w_PADIRXML)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg =Ah_MsgFormat( "La cartella impostata per la directory di generazione file XML intermedi non esiste")
         ENDIF
      ENDIF
      if i_bRes
         IF not(empty(.w_PADIRFIL) or directory(alltrim(.w_PADIRFIL)) and Upper(ALLTRIM(.w_PADIRFIL)) <> Upper(ALLTRIM(.w_PADIRSPO)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg =Ah_MsgFormat( "La cartella impostata per la directory di selezione file import non esiste o coincide con la directory di spostamento file")
         ENDIF
      ENDIF
      if i_bRes
         IF not(empty(.w_PADIRSPO) or directory(alltrim(.w_PADIRSPO)) and Upper(ALLTRIM(.w_PADIRFIL)) <> Upper(ALLTRIM(.w_PADIRSPO)))
            i_bRes = .f.
          	i_bnoChk = .f.
            i_cErrorMsg = Ah_MsgFormat("La cartella impostata per la directory di spostamento file non esiste o coincide con la directory di selezione file import")
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PAPATHED = this.w_PAPATHED
    this.o_PADIRFIL = this.w_PADIRFIL
    this.o_PADIRSPO = this.w_PADIRSPO
    this.o_PADIRXML = this.w_PADIRXML
    return

enddefine

* --- Define pages as container
define class tgsva_apaPag1 as StdContainer
  Width  = 634
  height = 266
  stdWidth  = 634
  stdheight = 266
  resizeXpos=417
  resizeYpos=154
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAPATHED_1_2 as StdField with uid="PHYODTXVIA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PAPATHED", cQueryName = "PAPATHED",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory di salvataggio file EDI",;
    HelpContextID = 66835514,;
   bGlobalFont=.t.,;
    Height=21, Width=367, Left=233, Top=6, InputMask=replicate('X',200)


  add object oBtn_1_3 as StdButton with uid="XHJDNMZZCE",left=607, top=6, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il percorso";
    , HelpContextID = 159867178;
    , caption='...';
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSVA_BPA(this.Parent.oContained,"SELFI","PAPATHED")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPADIRFIL_1_4 as StdField with uid="MVLFMHDPPG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PADIRFIL", cQueryName = "PADIRFIL",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory di selezione file import",;
    HelpContextID = 31659074,;
   bGlobalFont=.t.,;
    Height=21, Width=367, Left=233, Top=37, InputMask=replicate('X',200)


  add object oBtn_1_5 as StdButton with uid="AVWVGKLLEM",left=607, top=40, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il percorso";
    , HelpContextID = 159867178;
    , caption='...';
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSVA_BPA(this.Parent.oContained,"SELFI","PADIRFIL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPADIRSPO_1_6 as StdField with uid="LGBHADMHRV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PADIRSPO", cQueryName = "PADIRSPO",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory di spostamento file",;
    HelpContextID = 18672571,;
   bGlobalFont=.t.,;
    Height=21, Width=367, Left=233, Top=68, InputMask=replicate('X',200)


  add object oBtn_1_7 as StdButton with uid="CBEBOSAAWU",left=607, top=71, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il percorso";
    , HelpContextID = 159867178;
    , caption='...';
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSVA_BPA(this.Parent.oContained,"SELFI","PADIRSPO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPADIRXML_1_8 as StdField with uid="BLZKFGICWB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PADIRXML", cQueryName = "PADIRXML",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Directory di generazione file XML intermedi",;
    HelpContextID = 203221950,;
   bGlobalFont=.t.,;
    Height=21, Width=367, Left=233, Top=95, InputMask=replicate('X',200)


  add object oBtn_1_9 as StdButton with uid="VGBUGGQCIZ",left=607, top=98, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il percorso";
    , HelpContextID = 159867178;
    , caption='...';
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSVA_BPA(this.Parent.oContained,"SELFI","PADIRXML")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPAGENERR_1_10 as StdCombo with uid="MTKSOJHEAR",rtseq=6,rtrep=.f.,left=233,top=123,width=223,height=21;
    , ToolTipText = "Permette di specificare in che modo devono essere gestiti gli errori nella funzione Import massivo file EDI";
    , HelpContextID = 257997752;
    , cFormVar="w_PAGENERR",RowSource=""+"Annulla elaborazione corrente,"+"Annulla elaborazione struttura corrente,"+"Annulla elaborazione file corrente,"+"Annulla solo documenti errati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAGENERR_1_10.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oPAGENERR_1_10.GetRadio()
    this.Parent.oContained.w_PAGENERR = this.RadioValue()
    return .t.
  endfunc

  func oPAGENERR_1_10.SetRadio()
    this.Parent.oContained.w_PAGENERR=trim(this.Parent.oContained.w_PAGENERR)
    this.value = ;
      iif(this.Parent.oContained.w_PAGENERR=='E',1,;
      iif(this.Parent.oContained.w_PAGENERR=='S',2,;
      iif(this.Parent.oContained.w_PAGENERR=='N',3,;
      iif(this.Parent.oContained.w_PAGENERR=='C',4,;
      0))))
  endfunc


  add object oPACRIPEV_1_11 as StdCombo with uid="AUPGYEUDHP",rtseq=7,rtrep=.f.,left=233,top=152,width=184,height=21;
    , ToolTipText = "Criterio di definizione date di prevista evasione";
    , HelpContextID = 190579788;
    , cFormVar="w_PACRIPEV",RowSource=""+"per inizio periodo,"+"per fine periodo,"+"split giorni periodo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPACRIPEV_1_11.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'F',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oPACRIPEV_1_11.GetRadio()
    this.Parent.oContained.w_PACRIPEV = this.RadioValue()
    return .t.
  endfunc

  func oPACRIPEV_1_11.SetRadio()
    this.Parent.oContained.w_PACRIPEV=trim(this.Parent.oContained.w_PACRIPEV)
    this.value = ;
      iif(this.Parent.oContained.w_PACRIPEV=='I',1,;
      iif(this.Parent.oContained.w_PACRIPEV=='F',2,;
      iif(this.Parent.oContained.w_PACRIPEV=='S',3,;
      0)))
  endfunc

  func oPACRIPEV_1_11.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oPAQTAMIN_1_12 as StdField with uid="FYIHOHPPQV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PAQTAMIN", cQueryName = "PAQTAMIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� minima per giorno nel caso di ripartizione giorni periodo",;
    HelpContextID = 132047940,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=543, Top=152, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oPAQTAMIN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACRIPEV='S')
    endwith
   endif
  endfunc

  func oPAQTAMIN_1_12.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oPAGIOLIM_1_13 as StdField with uid="PBMXVAUSWP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PAGIOLIM", cQueryName = "PAGIOLIM",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "giorni entro i quali eseguire split quantit�",;
    HelpContextID = 129188931,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=233, Top=181, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oPAGIOLIM_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACRIPEV='S')
    endwith
   endif
  endfunc

  func oPAGIOLIM_1_13.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc


  add object oPASCADEN_1_14 as StdCombo with uid="MDVNEJWQGN",rtseq=10,rtrep=.f.,left=442,top=181,width=184,height=21;
    , ToolTipText = "Tipo di scadenza per determinare giorno di fine split";
    , HelpContextID = 248382532;
    , cFormVar="w_PASCADEN",RowSource=""+"Data inizio pianificazione,"+"Fine mese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPASCADEN_1_14.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPASCADEN_1_14.GetRadio()
    this.Parent.oContained.w_PASCADEN = this.RadioValue()
    return .t.
  endfunc

  func oPASCADEN_1_14.SetRadio()
    this.Parent.oContained.w_PASCADEN=trim(this.Parent.oContained.w_PASCADEN)
    this.value = ;
      iif(this.Parent.oContained.w_PASCADEN=='I',1,;
      iif(this.Parent.oContained.w_PASCADEN=='F',2,;
      0))
  endfunc

  func oPASCADEN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACRIPEV='S' AND .w_PAGIOLIM>0)
    endwith
   endif
  endfunc

  func oPASCADEN_1_14.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc


  add object oBtn_1_15 as StdButton with uid="SXVVCXGXSY",left=525, top=214, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 160047642;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="MTRWQEGJEF",left=578, top=214, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 162652166;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_17 as StdString with uid="WCHKOWCQEW",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=218, Height=18,;
    Caption="Directory export file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MXZLANNDSM",Visible=.t., Left=11, Top=40,;
    Alignment=1, Width=218, Height=18,;
    Caption="Directory di selezione file import:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LKPZYGIKAF",Visible=.t., Left=11, Top=70,;
    Alignment=1, Width=218, Height=18,;
    Caption="Directory di spostamento file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="THYZWVAXCK",Visible=.t., Left=11, Top=126,;
    Alignment=1, Width=218, Height=18,;
    Caption="Modalit� gestione errori:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JZHUOWYDIT",Visible=.t., Left=11, Top=99,;
    Alignment=1, Width=218, Height=18,;
    Caption="Directory file XML intermedi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UFYBSMKRQO",Visible=.t., Left=11, Top=154,;
    Alignment=1, Width=218, Height=18,;
    Caption="Criterio date di prevista evasione:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="KETOFPYAZH",Visible=.t., Left=431, Top=154,;
    Alignment=1, Width=110, Height=18,;
    Caption="Quantit� minima:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="NFVBURLSJF",Visible=.t., Left=11, Top=183,;
    Alignment=1, Width=218, Height=18,;
    Caption="Giorni applicazione split:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="NLNAHTLGTW",Visible=.t., Left=323, Top=183,;
    Alignment=1, Width=116, Height=18,;
    Caption="Inizio scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_apa','PAR_VEFA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=PAR_VEFA.PACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsva_apa
func chkpath(cpath,cspath)
   if Upper(ALLTRIM(cpath)) = Upper(ALLTRIM(cspath)) And !Empty(cpath)AND !Empty(cspath)
     ah_errormsg("Le directory di selezione file import e spostamento file non devono coincidere")
     return .f.
   endif
   return .t.
endfunc
* --- Fine Area Manuale
