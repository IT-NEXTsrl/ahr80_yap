* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bbc                                                        *
*              Elabora basi di calcolo da documenti                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_322]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-24                                                      *
* Last revis.: 2014-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bbc",oParentObject,m.pOper)
return(i_retval)

define class tgsva_bbc as StdBatch
  * --- Local variables
  pOper = space(1)
  w_PERCRIC = 0
  w_PREZZOBASE = 0
  w_CALC = space(1)
  w_CRITER = space(2)
  w_CLBASLIS = space(5)
  w_CLBASLIS = space(5)
  w_CLDATRIF = ctod("  /  /  ")
  w_CLBASMAG = space(5)
  w_CLBASESE = space(4)
  w_CLBASINV = space(6)
  w_CLCALSEL = space(1)
  w_CLRICAR1 = 0
  w_CLCAONAZ = 0
  w_CLAGGCAO = 0
  w_CLCAORIF = 0
  w_CLAGGSCO = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_MESS = space(10)
  w_PERCENT = 0
  w_LORNET = space(1)
  w_LORNET1 = space(1)
  w_CODRIC = space(5)
  w_CLAGGLIS = space(5)
  w_CLBASLIS = space(5)
  w_VALUTA = space(3)
  w_CLRICVAL = 0
  w_CLARROT1 = 0
  w_CLVALOR1 = 0
  w_CLARROT2 = 0
  w_CLVALOR2 = 0
  w_CLARROT3 = 0
  w_CLVALOR3 = 0
  w_CLARROT4 = 0
  w_CLMOLTIP = 0
  w_CLMOLTI2 = 0
  w_CLAGGDIN = ctod("  /  /  ")
  w_CLAGGDFI = ctod("  /  /  ")
  w_ROWNUM = 0
  w_AGGNUM = 0
  w_SCANUM = 0
  w_TROV = .f.
  w_PREZZOFI = 0
  w_PREZZOBA = 0
  w_VARPERC = 0
  w_VARPERC2 = 0
  w_VARPERC3 = 0
  w_DELETED = .f.
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_PADRE = .NULL.
  w_LORNET1 = space(1)
  w_CAMBIO = 0
  w_CLSERIAL = space(10)
  w_LIQUANTI = 0
  w_ROWRIF = 0
  w_LIQUANTRIF = 0
  w_CLAGGESI = space(1)
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_OK = .f.
  w_PRIMA = .f.
  w_ORISCA = space(1)
  w_DESCRI = space(50)
  w_DECTOT = 0
  w_CLTIPUMI = space(1)
  w_UM3 = space(3)
  w_CLUNIMIS = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTUM = 0
  w_MOLTUM3 = 0
  w_PREZZORIG = 0
  w_ROSSO = 0
  w_GIALLO = 0
  w_BLU = 0
  w_BIANCO = 0
  w_PREZZOUM = 0
  w_NEWPREZZO = 0
  w_OLDPREZZO = 0
  w_LIPREZZO = 0
  w_PREZZOPRO = 0
  w_NEWRICAR = 0
  w_OLDRICAR = 0
  w_OLDRICAR2 = 0
  w_OLDRICAR3 = 0
  w_NEWRICAR2 = 0
  w_NEWRICAR3 = 0
  w_RICAR2 = 0
  w_OLDRICAR2 = 0
  w_RICAR3 = 0
  w_FXCHK = 0
  w_BASUMLIS = 0
  w_MESS = space(10)
  w_AGGNUM = 0
  w_QUANTI = 0
  w_CLORISCA = space(1)
  w_UMLISAGG = space(3)
  w_QTA = 0
  w_PREZZOB = 0
  w_PREZZOBP = 0
  w_QTARIF = 0
  w_PREZZORIF = 0
  w_SCONT1RIF = 0
  w_SCONT2RIF = 0
  w_SCONT3RIF = 0
  w_SCONT4RIF = 0
  w_PREZZOBRIF = 0
  w_PREZZOFIRIF = 0
  w_NSCONT1RIF = 0
  w_NSCONT2RIF = 0
  w_NSCONT3RIF = 0
  w_NSCONT4RIF = 0
  w_ULTORA = 0
  w_ULTMIN = 0
  w_CTRL_CHIAVE = .NULL.
  w_ERR = .f.
  w_PREZDOC = 0
  w_GEST = .NULL.
  w_FLSCORDOC = space(1)
  w_IVADOC = 0
  w_DECUNI = 0
  w_LISIVA = space(1)
  w_LISVAL = space(3)
  w_VALDOC = space(3)
  w_CAMLIS = 0
  w_CAMDOC = 0
  w_LISDOC = space(5)
  w_FLIVALIS = space(1)
  w_APPO = space(10)
  * --- WorkFile variables
  TMPLIST_idx=0
  ART_ICOL_idx=0
  VOCIIVA_idx=0
  ESERCIZI_idx=0
  LISTINI_idx=0
  VALUTE_idx=0
  TMPZOOM_idx=0
  TMPSCAG_idx=0
  CLA_RICA_idx=0
  RIC_PREZ_idx=0
  LIS_TINI_idx=0
  TMPSCAGRIF_idx=0
  LIS_SCAG_idx=0
  PAR_RIOR_idx=0
  CLLSMAST_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OK = .F.
    if this.pOper <>"O"
      this.w_PADRE = this.oparentobject
      ND = this.oParentObject.w_Zoom.cCursor
      this.oParentObject.w_CHIAVE = LEFT(this.oParentObject.w_CHIAVE + SPACE(33), 33)
    endif
    do case
      case this.pOper="E"
        if EMPTY(this.oParentObject.w_CHIAVE) OR EMPTY(this.oParentObject.w_CODART) OR EMPTY(this.oParentObject.w_DATRIF)
          This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
          if This.TMPZOOM_idx<>0
            * --- Delete from TMPZOOM
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"1 = "+cp_ToStrODBC(1);
                     )
            else
              delete from (i_cTable) where;
                    1 = 1;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            this.oParentObject.w_ZOOM.cCpQueryName = "..\VEFA\EXE\QUERY\GSVA_BBC"
            this.oParentobject.NotifyEvent("Lanciazoom")
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          This.TMPLIST_idx=cp_GetTableDefIdx("TMPLIST")
          if This.TMPLIST_idx<>0
            * --- Delete from TMPLIST
            i_nConn=i_TableProp[this.TMPLIST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"1 = "+cp_ToStrODBC(1);
                     )
            else
              delete from (i_cTable) where;
                    1 = 1;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        else
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODRIC,ARCODIVA,AROPERAT,ARMOLTIP"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODRIC,ARCODIVA,AROPERAT,ARMOLTIP;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
            this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
            this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.w_MOLTUM = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_CODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Create temporary table TMPLIST
          i_nIdx=cp_AddTableDef('TMPLIST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSVA4MSL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPLIST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.w_PADRE.NotifyEvent("Lanciazoom")     
          * --- Create temporary table TMPZOOM
          i_nIdx=cp_AddTableDef('TMPZOOM') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSVA5MSL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPZOOM_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Select from TMPLIST
          i_nConn=i_TableProp[this.TMPLIST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPLIST_idx,2],.t.,this.TMPLIST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPLIST ";
                 ,"_Curs_TMPLIST")
          else
            select * from (i_cTable);
              into cursor _Curs_TMPLIST
          endif
          if used('_Curs_TMPLIST')
            select _Curs_TMPLIST
            locate for 1=1
            do while not(eof())
            this.w_PREZZOBASE = 0
            this.w_ROWRIF = 0
            this.w_PERCRIC = 0
            this.w_PREZZOFI = 0
            this.w_LISCONT1 = 0
            this.w_LISCONT2 = 0
            this.w_LISCONT3 = 0
            this.w_LISCONT4 = 0
            this.w_LIQUANTI = 0
            this.w_LIQUANTRIF = 0
            this.w_VARPERC = 0
            this.w_VARPERC2 = 0
            this.w_VARPERC3 = 0
            this.w_CRITER = NVL(_Curs_TMPLIST.CLBASSEL,SPACE(2))
            this.w_CLBASLIS = NVL(_Curs_TMPLIST.CLBASLIS,SPACE(5))
            this.w_CLDATRIF = NVL(_Curs_TMPLIST.CLDATRIF, CP_CHARTODATE("  -  -    "))
            this.w_CLBASMAG = NVL(_Curs_TMPLIST.CLBASMAG,SPACE(5))
            this.w_CLBASESE = NVL(_Curs_TMPLIST.CLBASESE,SPACE(4))
            this.w_CLBASINV = NVL(_Curs_TMPLIST.CLBASINV,SPACE(6))
            this.w_CLCALSEL = NVL(_Curs_TMPLIST.CLCALSEL,SPACE(1))
            this.w_CLRICAR1 = NVL(_Curs_TMPLIST.CLRICAR1,0)
            this.w_CLCAONAZ = NVL(_Curs_TMPLIST.CLCAONAZ,0)
            this.w_CLAGGCAO = NVL(_Curs_TMPLIST.CLAGGCAO,0)
            this.w_CLCAORIF = NVL(_Curs_TMPLIST.CLCAORIF,0)
            this.w_CLAGGSCO = NVL(_Curs_TMPLIST.CLAGGSCO,SPACE(1))
            this.w_CLAGGLIS = NVL(_Curs_TMPLIST.CLAGGLIS,SPACE(5))
            this.w_CLRICVAL = NVL(_Curs_TMPLIST.CLRICVAL,0)
            this.w_CLARROT1 = NVL(_Curs_TMPLIST.CLARROT1,0)
            this.w_CLVALOR1 = NVL(_Curs_TMPLIST.CLVALOR1,0)
            this.w_CLARROT2 = NVL(_Curs_TMPLIST.CLARROT2,0)
            this.w_CLVALOR2 = NVL(_Curs_TMPLIST.CLVALOR2,0)
            this.w_CLARROT3 = NVL(_Curs_TMPLIST.CLARROT3,0)
            this.w_CLVALOR3 = NVL(_Curs_TMPLIST.CLVALOR3,0)
            this.w_CLARROT4 = NVL(_Curs_TMPLIST.CLARROT4,0)
            this.w_CLMOLTIP = NVL(_Curs_TMPLIST.CLMOLTIP,0)
            this.w_CLMOLTI2 = NVL(_Curs_TMPLIST.CLMOLTI2,0)
            this.w_CLAGGDIN = NVL(_Curs_TMPLIST.CLAGGDIN, CP_CHARTODATE("  -  -    "))
            this.w_CLAGGDFI = NVL(_Curs_TMPLIST.CLAGGDFI, CP_CHARTODATE("  -  -    "))
            this.w_CLSERIAL = NVL(_Curs_TMPLIST.CLSERIAL, SPACE(10))
            this.w_ORISCA = NVL(_Curs_TMPLIST.CLORISCA, SPACE(1))
            this.w_DESCRI = NVL(_Curs_TMPLIST.DESCRI,SPACE(50))
            this.w_CLTIPUMI = NVL(_Curs_TMPLIST.CLTIPUMI,"P")
            DIMENSION pArr[3]
            this.w_CALC = BASPRZLI( this.oParentObject.w_CODART , this.w_CRITER , this.w_CLBASLIS, this.w_CLDATRIF , this.oParentObject.w_QTAMOV , this.w_CLBASMAG, this.w_CLBASESE, this.w_CLBASINV, @pArr, this.oParentObject.w_UNISEL)
            * --- Determina il prezzo base 
            this.w_PREZZOBASE = pArr[1]
            * --- Determina  il rownum del listino di riferimento (quando il criterio � prezzo di listino)
            this.w_CLUNIMIS = pArr[3]
            if this.w_CLTIPUMI="P"
              if NOT EMPTY(this.w_CLUNIMIS) AND this.w_CLUNIMIS<>this.oParentObject.w_UNMIS1
                * --- l'um della base di calcolo � diversa da quella sulla maschera e non � la principale quindi escludo questa base di calcolo
                this.w_PREZZOBASE = -1
              endif
            else
              if  EMPTY(this.w_CLUNIMIS) OR this.w_CLUNIMIS<>this.oParentObject.w_UNMIS2
                * --- l'um della base di calcolo � diversa  da quella sulla maschera quindi escludo questa base di calcolo
                this.w_PREZZOBASE = -1
              endif
            endif
            if EMPTY(this.w_CLUNIMIS)
              * --- L'origine non � da listino e quindi il prezzo � espresso nella 1UM dell'articolo
              this.w_CLUNIMIS = this.oParentObject.w_UNMIS1
            endif
            if this.w_PREZZOBASE<>-1
              this.w_ROWRIF = pArr[2]
              * --- leggo i flag scorpori dal listino da aggiornare
              * --- Read from LISTINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LISTINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LSIVALIS,LSVALLIS,LSDESLIS"+;
                  " from "+i_cTable+" LISTINI where ";
                      +"LSCODLIS = "+cp_ToStrODBC(this.w_CLAGGLIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LSIVALIS,LSVALLIS,LSDESLIS;
                  from (i_cTable) where;
                      LSCODLIS = this.w_CLAGGLIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LORNET = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                this.w_VALUTA = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                w_LIDESCRI = NVL(cp_ToDate(_read_.LSDESLIS),cp_NullValue(_read_.LSDESLIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_DECTOT = GETVALUT(this.w_VALUTA, "VADECUNI")
              if LEFT(this.w_CRITER,1)="L"
                * --- leggo i flag scorpori dal listino di base se il criterio � da listino
                * --- Read from LISTINI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.LISTINI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "LSIVALIS"+;
                    " from "+i_cTable+" LISTINI where ";
                        +"LSCODLIS = "+cp_ToStrODBC(this.w_CLBASLIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    LSIVALIS;
                    from (i_cTable) where;
                        LSCODLIS = this.w_CLBASLIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_LORNET1 = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.w_PERCENT = 0
              do case
                case this.w_CLCALSEL="P"
                  * --- Read from RIC_PREZ
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.RIC_PREZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.RIC_PREZ_idx,2],.t.,this.RIC_PREZ_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LIPERRIC"+;
                      " from "+i_cTable+" RIC_PREZ where ";
                          +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                          +" and LICODLIS = "+cp_ToStrODBC(this.w_CLAGGLIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LIPERRIC;
                      from (i_cTable) where;
                          LICODART = this.oParentObject.w_CODART;
                          and LICODLIS = this.w_CLAGGLIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_PERCENT = NVL(cp_ToDate(_read_.LIPERRIC),cp_NullValue(_read_.LIPERRIC))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                case this.w_CLCALSEL="C"
                  * --- Read from CLA_RICA
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CLA_RICA_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CLA_RICA_idx,2],.t.,this.CLA_RICA_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CRPERCEN"+;
                      " from "+i_cTable+" CLA_RICA where ";
                          +"CRCODICE = "+cp_ToStrODBC(this.w_CODRIC);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CRPERCEN;
                      from (i_cTable) where;
                          CRCODICE = this.w_CODRIC;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_PERCENT = NVL(cp_ToDate(_read_.CRPERCEN),cp_NullValue(_read_.CRPERCEN))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
              endcase
              * --- Controllo che tipo di ricalcolo � stato utilizzato.
              do case
                case this.w_CLCALSEL$"CP"
                  this.w_PERCRIC = this.w_PERCENT
                case this.w_CLCALSEL="R"
                  this.w_PERCRIC = this.w_CLRICAR1
              endcase
              if LEFT(this.w_CRITER,1)$"IUC"
                * --- Per inventario, ultimo costo/prezzo e costo standard il listino � al netto
                this.w_LORNET1 = "N"
              endif
              do case
                case LEFT(this.w_CRITER,1)="I"
                  this.w_CAMBIO = this.w_CLCAONAZ
                case LEFT(this.w_CRITER,1)$"UC"
                  this.w_CAMBIO = g_CAOVAL
                case LEFT(this.w_CRITER,1)$"L"
                  this.w_CAMBIO = this.w_CLCAORIF
              endcase
              if this.w_CLAGGSCO="F" 
                * --- forzo gli sconti della scheda di calcolo
                this.w_LISCONT1 = NVL(_Curs_TMPLIST.CLAGGSC1,0)
                this.w_LISCONT2 = NVL(_Curs_TMPLIST.CLAGGSC2,0)
                this.w_LISCONT3 = NVL(_Curs_TMPLIST.CLAGGSC3, 0)
                this.w_LISCONT4 = NVL(_Curs_TMPLIST.CLAGGSC4, 0)
              endif
              this.w_PREZZORIG = this.w_PREZZOBASE
              this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOBASE, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CLAGGCAO,this.w_CAMBIO,this.w_DECTOT, this.w_CLRICVAL, this.w_CLARROT1, this.w_CLVALOR1, this.w_CLARROT2, this.w_CLVALOR2, this.w_CLARROT3, this.w_CLVALOR3, this.w_CLARROT4 ,this.w_CLMOLTIP, this.w_CLMOLTI2)
              * --- Se ho pi� listini identici prende il primo che il server
              *     gli restituisce
              * --- Read from LIS_TINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LIS_TINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPROWNUM"+;
                  " from "+i_cTable+" LIS_TINI where ";
                      +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                      +" and LICODLIS = "+cp_ToStrODBC(this.w_CLAGGLIS);
                      +" and LIDATATT = "+cp_ToStrODBC(this.w_CLAGGDIN);
                      +" and LIDATDIS = "+cp_ToStrODBC(this.w_CLAGGDFI);
                      +" and LIUNIMIS = "+cp_ToStrODBC(this.w_CLUNIMIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPROWNUM;
                  from (i_cTable) where;
                      LICODART = this.oParentObject.w_CODART;
                      and LICODLIS = this.w_CLAGGLIS;
                      and LIDATATT = this.w_CLAGGDIN;
                      and LIDATDIS = this.w_CLAGGDFI;
                      and LIUNIMIS = this.w_CLUNIMIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_rows>0
                * --- se il listino da aggiornare esiste
                * --- Leggo gli Scaglioni del Listino di Origine
                * --- Create temporary table TMPSCAG
                i_nIdx=cp_AddTableDef('TMPSCAG') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('GSVA6MSL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPSCAG_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Select from TMPSCAG
                i_nConn=i_TableProp[this.TMPSCAG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPSCAG_idx,2],.t.,this.TMPSCAG_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPSCAG ";
                      +" where ORDINE>="+cp_ToStrODBC(this.oParentObject.w_QTAMOV)+"";
                      +" order by ORDINE";
                       ,"_Curs_TMPSCAG")
                else
                  select * from (i_cTable);
                   where ORDINE>=this.oParentObject.w_QTAMOV;
                   order by ORDINE;
                    into cursor _Curs_TMPSCAG
                endif
                if used('_Curs_TMPSCAG')
                  select _Curs_TMPSCAG
                  locate for 1=1
                  do while not(eof())
                  * --- Trovo il prezzo dello scaglione da aggiornare
                  this.w_PREZZOBA = cp_ROUND(NVL(_Curs_TMPSCAG.LIPREZZO,0),5)
                  * --- la quantit� mi serve per aggiornare la riga degli scaglioni (infatti � la chiave della riga)
                  this.w_LIQUANTI = NVL(_Curs_TMPSCAG.LIQUANTI,0)
                  * --- Tengo la prima riga valida
                  exit
                    select _Curs_TMPSCAG
                    continue
                  enddo
                  use
                endif
              else
                * --- il listino da aggiornare non esiste
                this.w_PREZZOBA = 0
                this.w_VARPERC = 0
                this.w_VARPERC2 = 0
                this.w_LIQUANTI = -1
              endif
              if LEFT(this.w_CRITER,1)="L"
                * --- se il criterio � da listino trovo la quantit� del listino di riferimento (� chiave, mi serve per l'aggiornamento)
                * --- Create temporary table TMPSCAGRIF
                i_nIdx=cp_AddTableDef('TMPSCAGRIF') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('GSVA7MSL',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMPSCAGRIF_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Select from TMPSCAGRIF
                i_nConn=i_TableProp[this.TMPSCAGRIF_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPSCAGRIF_idx,2],.t.,this.TMPSCAGRIF_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPSCAGRIF ";
                      +" where ORDINE>="+cp_ToStrODBC(this.oParentObject.w_QTAMOV)+"";
                      +" order by ORDINE";
                       ,"_Curs_TMPSCAGRIF")
                else
                  select * from (i_cTable);
                   where ORDINE>=this.oParentObject.w_QTAMOV;
                   order by ORDINE;
                    into cursor _Curs_TMPSCAGRIF
                endif
                if used('_Curs_TMPSCAGRIF')
                  select _Curs_TMPSCAGRIF
                  locate for 1=1
                  do while not(eof())
                  * --- la quantit� mi serve per aggiornare la riga degli scaglioni (infatti � la chiave della riga)
                  this.w_LIQUANTRIF = NVL(_Curs_TMPSCAGRIF.LIQUANTI,0)
                  if this.w_CLAGGSCO="C"
                    * --- copio gli sconti del listino di riferimento
                    this.w_LISCONT1 = NVL(_Curs_TMPSCAGRIF.LISCONT1,0)
                    this.w_LISCONT2 = NVL(_Curs_TMPSCAGRIF.LISCONT2,0)
                    this.w_LISCONT3 = NVL(_Curs_TMPSCAGRIF.LISCONT3,0)
                    this.w_LISCONT4 = NVL(_Curs_TMPSCAGRIF.LISCONT4,0)
                  endif
                  * --- Tengo la prima riga valida
                  exit
                    select _Curs_TMPSCAGRIF
                    continue
                  enddo
                  use
                endif
              else
                this.w_LIQUANTRIF = -1
              endif
              if this.w_PREZZOBA<>0
                * --- il listino non esiste quindi la percentuale di ricarico andrebbe all'inifinito. Metto 0 per convenzione
                this.w_VARPERC = cp_ROUND(((this.w_PREZZOBASE - this.w_PREZZOBA)*100/this.w_PREZZOBA),5)
                this.w_VARPERC2 = cp_ROUND(((this.w_PREZZOFI -this.w_PREZZOBA)*100/this.w_PREZZOBA),5)
              else
                this.w_VARPERC = 0
                this.w_VARPERC2 = 0
              endif
              if this.w_PREZZOBASE<>0
                this.w_VARPERC3 = cp_ROUND(((this.w_PREZZOFI -this.w_PREZZOBASE)*100/this.w_PREZZOBASE),5)
              else
                this.w_VARPERC3 = 0
              endif
              this.w_ROSSO = RGB(255,0,0)
              this.w_GIALLO = RGB(255,255,0)
              this.w_BLU = RGB(108,240,247)
              this.w_BIANCO = RGB(255,255,255)
              this.w_PREZZOUM = 0
              if this.oParentObject.w_UNIMOV<>this.oParentObject.w_UNISEL AND this.oParentObject.w_ORIGINE="D"
                * --- se l'unit� di misura del documento differisce dall'UM del listino del listino converto il prezzo
                *     di quest'ultimo anche in tale UM
                this.w_PREZZOUM = UM2UM(this.oParentObject.w_CODART,this.w_PREZZOFI, this.oParentObject.w_UNIMOV, this.oParentObject.w_UNISEL, this.oParentObject.w_UNMIS1, this.oParentObject.w_UNMIS2, this.w_OPERAT, this.w_MOLTUM)
              else
                this.w_PREZZOUM = this.w_PREZZOFI
              endif
              * --- Insert into TMPZOOM
              i_nConn=i_TableProp[this.TMPZOOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPZOOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CLSERIAL"+",DESCRI"+",CLAGGLIS"+",CLDESCRI"+",PREZZOFI"+",VARPERC3"+",LIPREZZO"+",VARPERC2"+",VARPERC"+",PREZZOBASE"+",OLDPREZZO"+",OLDVARPERC3"+",OLDVARPERC2"+",NUMROW"+",LIQUANTI"+",ROWRIF"+",LIQUANTRIF"+",FXCHK"+",OPR"+",OV3"+",OV2"+",ORISCA"+",ERRATO"+",ROSSO"+",GIALLO"+",BLU"+",BIANCO"+",PREZZOUM"+",LIUNIMIS"+",BASUMLIS"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_CLSERIAL),'TMPZOOM','CLSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TMPZOOM','DESCRI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CLAGGLIS),'TMPZOOM','CLAGGLIS');
                +","+cp_NullLink(cp_ToStrODBC(w_LIDESCRI),'TMPZOOM','CLDESCRI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'TMPZOOM','PREZZOFI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC3),'TMPZOOM','VARPERC3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOBA),'TMPZOOM','LIPREZZO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC2),'TMPZOOM','VARPERC2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC),'TMPZOOM','VARPERC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOBASE),'TMPZOOM','PREZZOBASE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'TMPZOOM','OLDPREZZO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC3),'TMPZOOM','OLDVARPERC3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC2),'TMPZOOM','OLDVARPERC2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'TMPZOOM','NUMROW');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LIQUANTI),'TMPZOOM','LIQUANTI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF),'TMPZOOM','ROWRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_LIQUANTRIF),'TMPZOOM','LIQUANTRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FXCHK),'TMPZOOM','FXCHK');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'TMPZOOM','OPR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC3),'TMPZOOM','OV3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VARPERC2),'TMPZOOM','OV2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ORISCA),'TMPZOOM','ORISCA');
                +","+cp_NullLink(cp_ToStrODBC(SPACE(1)),'TMPZOOM','ERRATO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROSSO),'TMPZOOM','ROSSO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_GIALLO),'TMPZOOM','GIALLO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BLU),'TMPZOOM','BLU');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BIANCO),'TMPZOOM','BIANCO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOUM),'TMPZOOM','PREZZOUM');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UNISEL),'TMPZOOM','LIUNIMIS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOBASE),'TMPZOOM','BASUMLIS');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CLSERIAL',this.w_CLSERIAL,'DESCRI',this.w_DESCRI,'CLAGGLIS',this.w_CLAGGLIS,'CLDESCRI',w_LIDESCRI,'PREZZOFI',this.w_PREZZOFI,'VARPERC3',this.w_VARPERC3,'LIPREZZO',this.w_PREZZOBA,'VARPERC2',this.w_VARPERC2,'VARPERC',this.w_VARPERC,'PREZZOBASE',this.w_PREZZOBASE,'OLDPREZZO',this.w_PREZZOFI,'OLDVARPERC3',this.w_VARPERC3)
                insert into (i_cTable) (CLSERIAL,DESCRI,CLAGGLIS,CLDESCRI,PREZZOFI,VARPERC3,LIPREZZO,VARPERC2,VARPERC,PREZZOBASE,OLDPREZZO,OLDVARPERC3,OLDVARPERC2,NUMROW,LIQUANTI,ROWRIF,LIQUANTRIF,FXCHK,OPR,OV3,OV2,ORISCA,ERRATO,ROSSO,GIALLO,BLU,BIANCO,PREZZOUM,LIUNIMIS,BASUMLIS &i_ccchkf. );
                   values (;
                     this.w_CLSERIAL;
                     ,this.w_DESCRI;
                     ,this.w_CLAGGLIS;
                     ,w_LIDESCRI;
                     ,this.w_PREZZOFI;
                     ,this.w_VARPERC3;
                     ,this.w_PREZZOBA;
                     ,this.w_VARPERC2;
                     ,this.w_VARPERC;
                     ,this.w_PREZZOBASE;
                     ,this.w_PREZZOFI;
                     ,this.w_VARPERC3;
                     ,this.w_VARPERC2;
                     ,this.w_ROWNUM;
                     ,this.w_LIQUANTI;
                     ,this.w_ROWRIF;
                     ,this.w_LIQUANTRIF;
                     ,this.w_FXCHK;
                     ,this.w_PREZZOFI;
                     ,this.w_VARPERC3;
                     ,this.w_VARPERC2;
                     ,this.w_ORISCA;
                     ,SPACE(1);
                     ,this.w_ROSSO;
                     ,this.w_GIALLO;
                     ,this.w_BLU;
                     ,this.w_BIANCO;
                     ,this.w_PREZZOUM;
                     ,this.oParentObject.w_UNISEL;
                     ,this.w_PREZZOBASE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.oParentObject.w_UMBAS = this.w_CLUNIMIS
            endif
              select _Curs_TMPLIST
              continue
            enddo
            use
          endif
          if This.TMPZOOM_idx<>0
            this.oParentObject.w_ZOOM.cCpQueryName = "..\VEFA\EXE\QUERY\GSVA_BBC"
            this.oParentobject.NotifyEvent("Lanciazoom")
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.oParentObject.w_BASCALC = this.w_PREZZORIG
          if (this.oParentObject.w_BASCALC=-1 OR this.w_PREZZOBASE=-1) and not empty(this.oParentObject.w_CHIAVE) 
            AH_ERRORMSG("La base di calcolo utilizzata non � valida in quanto non esiste listino per le selezioni presenti o non � valorizzata dall'inventario")
            this.oParentObject.w_BASCALC = 0
          endif
          this.oParentObject.w_CALCOLA = "P"
        endif
      case this.pOper="R"
        if this.oParentObject.w_VARIATI or this.oParentObject.o_BASCALC<>this.oParentObject.w_BASCALC
          * --- recupero l'indice perch� la tabella � stata creata al lancio precedente del batch
          This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
          select (nd) 
          go top 
 scan
          this.w_LIPREZZO = NVL(LIPREZZO,0)
          this.w_PREZZOPRO = NVL(PREZZOFI,0)
          this.w_OLDPREZZO = NVL(OLDPREZZO,0)
          this.w_RICAR3 = NVL(VARPERC3,0)
          this.w_OLDRICAR3 = NVL(OLDVARPERC3,0)
          this.w_RICAR2 = NVL(VARPERC2,0)
          this.w_OLDRICAR2 = NVL(OLDVARPERC2,0)
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          this.w_FXCHK = NVL(FXCHK,0)
          this.w_PREZZOUM = NVL(PREZZOUM,0)
          this.w_BASUMLIS = NVL(BASUMLIS,0)
          if this.oParentObject.o_BASCALC<>this.oParentObject.w_BASCALC AND this.oParentObject.w_BASCALC<>0
            * --- se modifico la %ricarico (fra base calcolo e prezzo proposto)
            if this.oParentObject.o_BASCALC<>0
              this.w_BASUMLIS = this.w_BASUMLIS * (this.oParentObject.w_BASCALC/this.oParentObject.o_BASCALC)
              this.w_NEWPREZZO = cp_ROUND(this.w_BASUMLIS*(1 + this.w_RICAR3/100),5)
              if this.w_BASUMLIS<>0
                this.w_NEWRICAR3 = cp_ROUND((this.w_NEWPREZZO - this.w_BASUMLIS)*100/this.w_BASUMLIS,5)
              else
                this.w_NEWRICAR3 = 0
              endif
              if this.w_OLDPREZZO<>0
                this.w_PREZZOUM = cp_ROUND(this.w_PREZZOUM * (this.w_NEWPREZZO/this.w_OLDPREZZO), 5)
              else
                this.w_PREZZOUM = 0
              endif
            else
              this.w_BASUMLIS = 0
              this.w_NEWPREZZO = 0
              this.w_NEWRICAR3 = 0
              this.w_PREZZOUM = 0
            endif
            if this.w_LIPREZZO<>0
              this.w_NEWRICAR2 = cp_ROUND((this.w_NEWPREZZO - this.w_LIPREZZO)*100/this.w_LIPREZZO,5)
              this.w_NEWRICAR = cp_ROUND((this.oParentObject.w_BASCALC - this.w_LIPREZZO)*100/this.w_LIPREZZO,5)
            else
              this.w_NEWRICAR2 = 0
              this.w_NEWRICAR = 0
            endif
            * --- Write into TMPZOOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PREZZOFI ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','PREZZOFI');
              +",OLDPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','OLDPREZZO');
              +",VARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','VARPERC2');
              +",FXCHK ="+cp_NullLink(cp_ToStrODBC(this.w_FXCHK),'TMPZOOM','FXCHK');
              +",OLDVARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','OLDVARPERC2');
              +",VARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','VARPERC3');
              +",OLDVARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','OLDVARPERC3');
              +",VARPERC ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR),'TMPZOOM','VARPERC');
              +",PREZZOUM ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOUM),'TMPZOOM','PREZZOUM');
              +",BASUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_BASUMLIS),'TMPZOOM','BASUMLIS');
                  +i_ccchkf ;
              +" where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                     )
            else
              update (i_cTable) set;
                  PREZZOFI = this.w_NEWPREZZO;
                  ,OLDPREZZO = this.w_NEWPREZZO;
                  ,VARPERC2 = this.w_NEWRICAR2;
                  ,FXCHK = this.w_FXCHK;
                  ,OLDVARPERC2 = this.w_NEWRICAR2;
                  ,VARPERC3 = this.w_NEWRICAR3;
                  ,OLDVARPERC3 = this.w_NEWRICAR3;
                  ,VARPERC = this.w_NEWRICAR;
                  ,PREZZOUM = this.w_PREZZOUM;
                  ,BASUMLIS = this.w_BASUMLIS;
                  &i_ccchkf. ;
               where;
                  CLSERIAL = this.w_CLSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.w_PREZZOPRO<> this.w_OLDPREZZO
            * --- se modifico il prezzo proposto
            if this.w_OLDPREZZO<>0
              this.w_PREZZOUM = cp_ROUND(this.w_PREZZOUM * (this.w_PREZZOPRO/this.w_OLDPREZZO), 5)
            else
              this.w_PREZZOUM = 0
            endif
            if this.oParentObject.w_BASCALC<>0 AND this.w_BASUMLIS<>0
              this.w_NEWRICAR3 = cp_ROUND((this.w_PREZZOPRO - this.w_BASUMLIS)*100/this.w_BASUMLIS,5)
            else
              this.w_NEWRICAR3 = 0
            endif
            if this.w_LIPREZZO<>0
              this.w_NEWRICAR2 = cp_ROUND((this.w_PREZZOPRO - this.w_LIPREZZO)*100/this.w_LIPREZZO,5)
              this.w_NEWRICAR = cp_ROUND((this.w_BASUMLIS- this.w_LIPREZZO)*100/this.w_LIPREZZO,5)
            else
              this.w_NEWRICAR2 = 0
              this.w_NEWRICAR = 0
            endif
            * --- Write into TMPZOOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"VARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','VARPERC3');
              +",OLDVARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','OLDVARPERC3');
              +",VARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','VARPERC2');
              +",FXCHK ="+cp_NullLink(cp_ToStrODBC(this.w_FXCHK),'TMPZOOM','FXCHK');
              +",OLDVARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','OLDVARPERC2');
              +",VARPERC ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR),'TMPZOOM','VARPERC');
              +",PREZZOFI ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'TMPZOOM','PREZZOFI');
              +",OLDPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'TMPZOOM','OLDPREZZO');
              +",PREZZOUM ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOUM),'TMPZOOM','PREZZOUM');
              +",BASUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_BASUMLIS),'TMPZOOM','BASUMLIS');
                  +i_ccchkf ;
              +" where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                     )
            else
              update (i_cTable) set;
                  VARPERC3 = this.w_NEWRICAR3;
                  ,OLDVARPERC3 = this.w_NEWRICAR3;
                  ,VARPERC2 = this.w_NEWRICAR2;
                  ,FXCHK = this.w_FXCHK;
                  ,OLDVARPERC2 = this.w_NEWRICAR2;
                  ,VARPERC = this.w_NEWRICAR;
                  ,PREZZOFI = this.w_PREZZOPRO;
                  ,OLDPREZZO = this.w_PREZZOPRO;
                  ,PREZZOUM = this.w_PREZZOUM;
                  ,BASUMLIS = this.w_BASUMLIS;
                  &i_ccchkf. ;
               where;
                  CLSERIAL = this.w_CLSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.w_RICAR3<>this.w_OLDRICAR3
            * --- se modifico la %ricarico (fra base calcolo e prezzo proposto)
            this.w_NEWPREZZO = cp_ROUND(this.w_BASUMLIS*(1 + this.w_RICAR3/100),5)
            if this.w_OLDPREZZO<>0
              this.w_PREZZOUM = cp_ROUND(this.w_PREZZOUM * (this.w_NEWPREZZO/this.w_OLDPREZZO), 5)
            else
              this.w_PREZZOUM = 0
            endif
            if this.w_LIPREZZO<>0
              this.w_NEWRICAR2 = cp_ROUND((this.w_NEWPREZZO - this.w_LIPREZZO)*100/this.w_LIPREZZO,5)
            else
              this.w_NEWRICAR2 = 0
            endif
            * --- Write into TMPZOOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PREZZOFI ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','PREZZOFI');
              +",OLDPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','OLDPREZZO');
              +",VARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','VARPERC2');
              +",FXCHK ="+cp_NullLink(cp_ToStrODBC(this.w_FXCHK),'TMPZOOM','FXCHK');
              +",OLDVARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR2),'TMPZOOM','OLDVARPERC2');
              +",VARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_RICAR3),'TMPZOOM','VARPERC3');
              +",OLDVARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_RICAR3),'TMPZOOM','OLDVARPERC3');
              +",PREZZOUM ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOUM),'TMPZOOM','PREZZOUM');
              +",BASUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_BASUMLIS),'TMPZOOM','BASUMLIS');
                  +i_ccchkf ;
              +" where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                     )
            else
              update (i_cTable) set;
                  PREZZOFI = this.w_NEWPREZZO;
                  ,OLDPREZZO = this.w_NEWPREZZO;
                  ,VARPERC2 = this.w_NEWRICAR2;
                  ,FXCHK = this.w_FXCHK;
                  ,OLDVARPERC2 = this.w_NEWRICAR2;
                  ,VARPERC3 = this.w_RICAR3;
                  ,OLDVARPERC3 = this.w_RICAR3;
                  ,PREZZOUM = this.w_PREZZOUM;
                  ,BASUMLIS = this.w_BASUMLIS;
                  &i_ccchkf. ;
               where;
                  CLSERIAL = this.w_CLSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.w_RICAR2<>this.w_OLDRICAR2 AND this.oParentObject.w_BASCALC<>0
            * --- se modifico la %ricarico (fra prezzo attuale e prezzo proposto)
            this.w_NEWPREZZO = cp_ROUND(this.w_LIPREZZO*(1 + this.w_RICAR2/100),5)
            if this.w_OLDPREZZO<>0
              this.w_PREZZOUM = cp_ROUND(this.w_PREZZOUM * (this.w_NEWPREZZO/this.w_OLDPREZZO), 5)
            else
              this.w_PREZZOUM = 0
            endif
            if this.w_BASUMLIS<>0
              this.w_NEWRICAR3 = cp_ROUND((this.w_NEWPREZZO - this.w_BASUMLIS)*100/this.w_BASUMLIS,5)
            else
              this.w_NEWRICAR3 = 0
            endif
            * --- Write into TMPZOOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PREZZOFI ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','PREZZOFI');
              +",OLDPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPREZZO),'TMPZOOM','OLDPREZZO');
              +",VARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_RICAR2),'TMPZOOM','VARPERC2');
              +",FXCHK ="+cp_NullLink(cp_ToStrODBC(this.w_FXCHK),'TMPZOOM','FXCHK');
              +",OLDVARPERC2 ="+cp_NullLink(cp_ToStrODBC(this.w_RICAR2),'TMPZOOM','OLDVARPERC2');
              +",VARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','VARPERC3');
              +",OLDVARPERC3 ="+cp_NullLink(cp_ToStrODBC(this.w_NEWRICAR3),'TMPZOOM','OLDVARPERC3');
              +",PREZZOUM ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOUM),'TMPZOOM','PREZZOUM');
              +",BASUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_BASUMLIS),'TMPZOOM','BASUMLIS');
                  +i_ccchkf ;
              +" where ";
                  +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                     )
            else
              update (i_cTable) set;
                  PREZZOFI = this.w_NEWPREZZO;
                  ,OLDPREZZO = this.w_NEWPREZZO;
                  ,VARPERC2 = this.w_RICAR2;
                  ,FXCHK = this.w_FXCHK;
                  ,OLDVARPERC2 = this.w_RICAR2;
                  ,VARPERC3 = this.w_NEWRICAR3;
                  ,OLDVARPERC3 = this.w_NEWRICAR3;
                  ,PREZZOUM = this.w_PREZZOUM;
                  ,BASUMLIS = this.w_BASUMLIS;
                  &i_ccchkf. ;
               where;
                  CLSERIAL = this.w_CLSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          ENDSCAN
          this.oParentobject.NotifyEvent("Lanciazoom")
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOper="A"
        if this.oParentObject.w_FLAGGIO="S"
          this.w_MESS = "Saranno aggiornati i listini e le basi di calcolo secondo le selezioni effettuate. Si desidera continuare?"
        else
          this.w_MESS = "Saranno aggiornati i listini secondo le selezioni effettuate. Si desidera continuare?"
        endif
        if Ah_YesNo(this.w_MESS)
          This.TMPLIST_idx=cp_GetTableDefIdx("TMPLIST")
          This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODRIC,ARCODIVA"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODRIC,ARCODIVA;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
            this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_CODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          select (ND) 
          GO TOP 
 SCAN FOR XCHK=1
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          this.oParentObject.w_FLSELE = 1
          * --- aggiorna il campo fittizio
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FXCHK ="+cp_NullLink(cp_ToStrODBC(1),'TMPZOOM','FXCHK');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   )
          else
            update (i_cTable) set;
                FXCHK = 1;
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ENDSCAN
          if this.oParentObject.w_FLSELE=1
            * --- Try
            local bErr_02AC3E20
            bErr_02AC3E20=bTrsErr
            this.Try_02AC3E20()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Errore durante l'inserimento")
            endif
            bTrsErr=bTrsErr or bErr_02AC3E20
            * --- End
          else
            ah_ErrorMsg("Non ci sono righe selezionate per l'aggiornamento")
            i_retcode = 'stop'
            return
          endif
          this.oParentobject.NotifyEvent("w_CHIAVE Changed")
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_SELEZI = "D"
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="S"
        * --- Seleziona/Deseleziona Tutto
        this.w_ERR = .F.
        This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
        if this.oParentObject.w_SELEZI="S"
          if this.oParentObject.w_CALCOLA="P"
            UPDATE &ND SET XCHK = 1
            UPDATE &ND SET FXCHK = 1
            this.oParentObject.w_FLSELE = 1
            * --- Write into TMPZOOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPZOOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FXCHK ="+cp_NullLink(cp_ToStrODBC(1),'TMPZOOM','FXCHK');
                  +i_ccchkf ;
              +" where ";
                  +"1 = "+cp_ToStrODBC(1);
                     )
            else
              update (i_cTable) set;
                  FXCHK = 1;
                  &i_ccchkf. ;
               where;
                  1 = 1;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            SELECT (ND)
            go top
            SCAN
            this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
            if NVL(ORISCA," ")<>"S"
              this.w_ERR = .T.
              Record=recno()
              REPLACE ERRATO WITH "S" FOR NVL(ORISCA," ")<>"S"
              GOTO (Record )
              * --- Write into TMPZOOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPZOOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ERRATO ="+cp_NullLink(cp_ToStrODBC("S"),'TMPZOOM','ERRATO');
                    +i_ccchkf ;
                +" where ";
                    +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                       )
              else
                update (i_cTable) set;
                    ERRATO = "S";
                    &i_ccchkf. ;
                 where;
                    CLSERIAL = this.w_CLSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              Record=recno()
              REPLACE XCHK WITH 1 FOR NVL(ORISCA," ")="S"
              REPLACE FXCHK WITH 1 FOR NVL(ORISCA," ")="S"
              REPLACE ERRATO WITH " " FOR NVL(ORISCA," ")="S"
              GOTO (Record )
              this.oParentObject.w_FLSELE = 1
              * --- Write into TMPZOOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPZOOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"FXCHK ="+cp_NullLink(cp_ToStrODBC(1),'TMPZOOM','FXCHK');
                +",ERRATO ="+cp_NullLink(cp_ToStrODBC(" "),'TMPZOOM','ERRATO');
                    +i_ccchkf ;
                +" where ";
                    +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                       )
              else
                update (i_cTable) set;
                    FXCHK = 1;
                    ,ERRATO = " ";
                    &i_ccchkf. ;
                 where;
                    CLSERIAL = this.w_CLSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            SELECT (ND)
            ENDSCAN
            this.oParentObject.w_Zoom.refresh()
            if this.w_ERR
              Ah_ErrorMsg("Attenzione sono presenti basi di calcolo incongruenti con il tipo di aggiornamento impostato")
            endif
          endif
        else
          UPDATE &ND SET XCHK = 0
          UPDATE &ND SET FXCHK = 0
          this.oParentObject.w_FLSELE = 0
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FXCHK ="+cp_NullLink(cp_ToStrODBC(0),'TMPZOOM','FXCHK');
                +i_ccchkf ;
            +" where ";
                +"1 = "+cp_ToStrODBC(1);
                   )
          else
            update (i_cTable) set;
                FXCHK = 0;
                &i_ccchkf. ;
             where;
                1 = 1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pOper="D"
        * --- Drop temporary table TMPSCAGRIF
        i_nIdx=cp_GetTableDefIdx('TMPSCAGRIF')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPSCAGRIF')
        endif
        * --- Drop temporary table TMPSCAG
        i_nIdx=cp_GetTableDefIdx('TMPSCAG')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPSCAG')
        endif
        * --- Drop temporary table TMPLIST
        i_nIdx=cp_GetTableDefIdx('TMPLIST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPLIST')
        endif
        * --- Drop temporary table TMPZOOM
        i_nIdx=cp_GetTableDefIdx('TMPZOOM')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPZOOM')
        endif
      case this.pOper="P"
        this.oParentObject.w_CHIAVE = SPACE(33)
        this.oParentobject.NotifyEvent("w_CHIAVE Changed")
        this.w_CTRL_CHIAVE = This.oparentobject.GetCtrl("w_CHIAVE")
        this.w_CTRL_CHIAVE.Popola()     
        this.oParentObject.w_CALCOLA = "P"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="C"
        This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
        this.w_ERR = .F.
        SELECT (ND)
        if this.oParentObject.w_CALCOLA="C"
          GO TOP 
 SCAN FOR NVL(ORISCA," ")<>"S"
          this.w_ERR = .T.
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          UPDATE &ND SET XCHK = 0 where NVL(ORISCA," ")<>"S"
          * --- aggiorna il campo fittizio
          UPDATE &ND SET FXCHK = 0 where NVL(ORISCA," ")<>"S"
          UPDATE &ND SET ERRATO = "S" where NVL(ORISCA," ")<>"S"
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FXCHK ="+cp_NullLink(cp_ToStrODBC(0),'TMPZOOM','FXCHK');
            +",ERRATO ="+cp_NullLink(cp_ToStrODBC("S"),'TMPZOOM','ERRATO');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   )
          else
            update (i_cTable) set;
                FXCHK = 0;
                ,ERRATO = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ENDSCAN
          if this.w_ERR
            Ah_ErrorMsg("Attenzione sono presenti basi di calcolo incongruenti con il tipo di aggiornamento impostato")
          endif
        else
          GO TOP 
 SCAN 
          UPDATE &ND SET ERRATO = " "
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ERRATO ="+cp_NullLink(cp_ToStrODBC(" "),'TMPZOOM','ERRATO');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   )
          else
            update (i_cTable) set;
                ERRATO = " ";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ENDSCAN
        endif
      case this.pOper="Z"
        This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
        SELECT (ND)
        if NVL(ORISCA," ")<>"S" and this.oParentObject.w_CALCOLA="C"
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          AA=ALLTRIM(STR(this.oParentObject.w_Zoom.grd.ColumnCount))
          this.oParentObject.w_Zoom.grd.Column&AA..chk.Value = 0
          UPDATE &ND SET XCHK = 0 where CLSERIAL=this.w_CLSERIAL
          * --- aggiorna il campo fittizio
          UPDATE &ND SET FXCHK = 0 where CLSERIAL=this.w_CLSERIAL
          UPDATE &ND SET ERRATO = "S" where CLSERIAL=this.w_CLSERIAL
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FXCHK ="+cp_NullLink(cp_ToStrODBC(0),'TMPZOOM','FXCHK');
            +",ERRATO ="+cp_NullLink(cp_ToStrODBC("S"),'TMPZOOM','ERRATO');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   )
          else
            update (i_cTable) set;
                FXCHK = 0;
                ,ERRATO = "S";
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          Ah_ErrorMsg("Base di calcolo incongruente con il tipo di aggiornamento impostato: impossibile selezionarla")
        endif
      case this.pOper="U"
        * --- Aggiorna prezzo unitario 
        * --- VAR. LOCALI
        this.w_GEST = this.oParentObject.oParentObject
        this.w_FLSCORDOC = this.w_GEST.w_MVFLSCOR
        this.w_IVADOC = this.w_GEST.w_PERIVA
        this.w_DECUNI = this.w_GEST.w_DECUNI
        this.w_LISDOC = this.w_GEST.w_MVTCOLIS
        this.w_VALDOC = this.w_GEST.w_MVCODVAL
        this.w_CAMDOC = this.w_GEST.w_MVCAOVAL
        this.w_FLIVALIS = this.w_GEST.w_IVALIS
        if Ah_YesNo("Sar� aggiornato il prezzo di riga del documento.Si desidera continuare?")
          This.TMPZOOM_idx=cp_GetTableDefIdx("TMPZOOM")
          select (ND) 
          GO TOP 
 SCAN FOR XCHK=1
          this.w_CLSERIAL = NVL(CLSERIAL,SPACE(10))
          this.oParentObject.w_FLSELE = 1
          * --- aggiorna il campo fittizio
          * --- Write into TMPZOOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZOOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZOOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FXCHK ="+cp_NullLink(cp_ToStrODBC(1),'TMPZOOM','FXCHK');
                +i_ccchkf ;
            +" where ";
                +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                   )
          else
            update (i_cTable) set;
                FXCHK = 1;
                &i_ccchkf. ;
             where;
                CLSERIAL = this.w_CLSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ENDSCAN
          if this.oParentObject.w_FLSELE=1
            if this.oParentObject.w_LISAGG<>this.w_LISDOC
              * --- Read from LISTINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LISTINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LSIVALIS,LSVALLIS"+;
                  " from "+i_cTable+" LISTINI where ";
                      +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_LISAGG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LSIVALIS,LSVALLIS;
                  from (i_cTable) where;
                      LSCODLIS = this.oParentObject.w_LISAGG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LISIVA = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                this.w_LISVAL = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_LISVAL<>this.w_VALDOC
                * --- Read from CLLSMAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CLLSMAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CLLSMAST_idx,2],.t.,this.CLLSMAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CLAGGCAO"+;
                    " from "+i_cTable+" CLLSMAST where ";
                        +"CLSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CLAGGCAO;
                    from (i_cTable) where;
                        CLSERIAL = this.oParentObject.w_SERIAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CAMLIS = NVL(cp_ToDate(_read_.CLAGGCAO),cp_NullValue(_read_.CLAGGCAO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if this.w_FLSCORDOC<>"S" AND this.w_LISIVA="L" AND this.w_IVADOC<>0
                this.w_PREZDOC = NEWPRZLI(this.oParentObject.w_PREZZOOM, 0, this.w_FLIVALIS, this.w_LISIVA,this.w_IVADOC, this.w_CAMLIS,this.w_CAMDOC,this.w_DECUNI)
              else
                this.w_PREZDOC = NEWPRZLI(this.oParentObject.w_PREZZOOM, 0, "" , "",this.w_IVADOC, this.w_CAMLIS,this.w_CAMDOC,this.w_DECUNI)
              endif
              this.w_GEST.w_MVPREZZO = this.w_PREZDOC
            else
              this.w_GEST.w_MVPREZZO = this.oParentObject.w_PREZZOOM
            endif
            this.w_GEST.NotifyEvent("w_MVPREZZO Changed")     
            * --- Esegue i Calcoli
            this.w_GEST.mCalc(.T.)     
            this.w_GEST.SaveDependsOn()     
            this.w_PADRE.ecpquit()     
          else
            ah_ErrorMsg("Non ci sono righe selezionate per l'aggiornamento")
          endif
        endif
      case this.pOper="O"
        g_omenu.oparentobject.notifyevent("Aggiorna")
    endcase
  endproc
  proc Try_02AC3E20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from GSVA8MSL
    do vq_exec with 'GSVA8MSL',this,'_Curs_GSVA8MSL','',.f.,.t.
    if used('_Curs_GSVA8MSL')
      select _Curs_GSVA8MSL
      locate for 1=1
      do while not(eof())
      this.w_TROV = .F.
      this.w_PRIMA = .F.
      this.w_CLBASLIS = SPACE(5)
      this.w_CLDATRIF = CP_CHARTODATE("  -  -    ")
      this.w_CLCALSEL = SPACE(1)
      this.w_CLRICAR1 = 0
      this.w_CLCAONAZ = 0
      this.w_CLAGGCAO = 0
      this.w_CLCAORIF = 0
      this.w_CLAGGSCO = SPACE(1)
      this.w_CLAGGLIS = SPACE(5)
      this.w_CLRICVAL = 0
      this.w_CLMOLTIP = 0
      this.w_CLMOLTI2 = 0
      this.w_CAMBIO = 0
      this.w_CLSERIAL = NVL(_Curs_GSVA8MSL.CLSERIAL, SPACE(10))
      this.w_CRITER = NVL(_Curs_GSVA8MSL.CLBASSEL, SPACE(2))
      this.w_CLAGGESI = NVL(_Curs_GSVA8MSL.CLAGGESI, SPACE(1))
      this.w_ROWNUM = NVL(_Curs_GSVA8MSL.NUMROW,0)
      this.w_ROWRIF = NVL(_Curs_GSVA8MSL.ROWRIF,0)
      this.w_CLAGGLIS = NVL(_Curs_GSVA8MSL.CLAGGLIS,SPACE(5))
      this.w_CLAGGDIN = NVL(_Curs_GSVA8MSL.CLAGGDIN, CP_CHARTODATE("  -  -    "))
      this.w_CLAGGDFI = NVL(_Curs_GSVA8MSL.CLAGGDFI, CP_CHARTODATE("  -  -    "))
      this.w_PREZZOPRO = NVL(_Curs_GSVA8MSL.PREZZOFI,0)
      this.w_BASUMLIS = NVL(_Curs_GSVA8MSL.BASUMLIS,0)
      this.w_UMLISAGG = NVL(LIUNIMIS, SPACE(3))
      this.w_LIPREZZO = NVL(_Curs_GSVA8MSL.LIPREZZO,0)
      this.w_CLAGGSCO = NVL(_Curs_GSVA8MSL.CLAGGSCO," ")
      this.w_CLAGGCAO = NVL(_Curs_GSVA8MSL.CLAGGCAO,0)
      this.w_CLARROT1 = NVL(_Curs_GSVA8MSL.CLARROT1,0)
      this.w_CLVALOR1 = NVL(_Curs_GSVA8MSL.CLVALOR1,0)
      this.w_CLARROT2 = NVL(_Curs_GSVA8MSL.CLARROT2,0)
      this.w_CLVALOR2 = NVL(_Curs_GSVA8MSL.CLVALOR2,0)
      this.w_CLARROT3 = NVL(_Curs_GSVA8MSL.CLARROT3,0)
      this.w_CLVALOR3 = NVL(_Curs_GSVA8MSL.CLVALOR3,0)
      this.w_CLARROT4 = NVL(_Curs_GSVA8MSL.CLARROT4,0)
      this.w_CLORISCA = NVL(_Curs_GSVA8MSL.CLORISCA," ")
      if this.w_CLAGGSCO<>"C" 
        this.w_LISCONT1 = NVL(_Curs_GSVA8MSL.CLAGGSC1,0)
        this.w_LISCONT2 = NVL(_Curs_GSVA8MSL.CLAGGSC2,0)
        this.w_LISCONT3 = NVL(_Curs_GSVA8MSL.CLAGGSC3, 0)
        this.w_LISCONT4 = NVL(_Curs_GSVA8MSL.CLAGGSC4, 0)
      endif
      * --- leggo la quantit� dello scaglione del listino da aggiornare
      this.w_LIQUANTI = NVL(_Curs_GSVA8MSL.LIQUANTI,0)
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSIVALIS,LSVALLIS,LSDESLIS"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.w_CLAGGLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSIVALIS,LSVALLIS,LSDESLIS;
          from (i_cTable) where;
              LSCODLIS = this.w_CLAGGLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LORNET = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
        this.w_VALUTA = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
        w_LIDESCRI = NVL(cp_ToDate(_read_.LSDESLIS),cp_NullValue(_read_.LSDESLIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DECTOT = GETVALUT(this.w_VALUTA, "VADECUNI")
      if LEFT(this.w_CRITER,1)="L" 
        * --- leggo la quantit� dello scaglione del listino di  riferimento
        this.w_LIQUANTRIF = NVL(_Curs_GSVA8MSL.LIQUANTRIF,0)
      else
        this.w_LIQUANTRIF = 0
      endif
      if this.w_CLAGGESI<>"S" AND this.w_ROWNUM=0
        * --- Se listino non esistente allora determino il primo CPROWNUM libero e lo assegno a w_AGGNUM
        * --- Select from LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) As ULTROW  from "+i_cTable+" LIS_TINI ";
              +" where LICODART="+cp_ToStrODBC(this.oParentObject.w_CODART)+"";
               ,"_Curs_LIS_TINI")
        else
          select MAX(CPROWNUM) As ULTROW from (i_cTable);
           where LICODART=this.oParentObject.w_CODART;
            into cursor _Curs_LIS_TINI
        endif
        if used('_Curs_LIS_TINI')
          select _Curs_LIS_TINI
          locate for 1=1
          do while not(eof())
          this.w_AGGNUM = Nvl( _Curs_LIS_TINI.ULTROW ,0 )
            select _Curs_LIS_TINI
            continue
          enddo
          use
        endif
        this.w_AGGNUM = this.w_AGGNUM + 1
        * --- Insert into LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+",LIUNIMIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_TINI','LICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_TINI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLAGGLIS),'LIS_TINI','LICODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLAGGDIN),'LIS_TINI','LIDATATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CLAGGDFI),'LIS_TINI','LIDATDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_UMLISAGG),'LIS_TINI','LIUNIMIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'CPROWNUM',this.w_AGGNUM,'LICODLIS',this.w_CLAGGLIS,'LIDATATT',this.w_CLAGGDIN,'LIDATDIS',this.w_CLAGGDFI,'LIUNIMIS',this.w_UMLISAGG)
          insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS,LIUNIMIS &i_ccchkf. );
             values (;
               this.oParentObject.w_CODART;
               ,this.w_AGGNUM;
               ,this.w_CLAGGLIS;
               ,this.w_CLAGGDIN;
               ,this.w_CLAGGDFI;
               ,this.w_UMLISAGG;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore inserimento listini'
          return
        endif
      endif
      if this.w_ROWNUM<>0
        * --- Select from LIS_SCAG
        i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_SCAG ";
              +" where LICODART="+cp_ToStrODBC(this.oParentObject.w_CODART)+" AND LIROWNUM="+cp_ToStrODBC(this.w_ROWNUM)+"";
               ,"_Curs_LIS_SCAG")
        else
          select * from (i_cTable);
           where LICODART=this.oParentObject.w_CODART AND LIROWNUM=this.w_ROWNUM;
            into cursor _Curs_LIS_SCAG
        endif
        if used('_Curs_LIS_SCAG')
          select _Curs_LIS_SCAG
          locate for 1=1
          do while not(eof())
          this.w_QTA = _Curs_LIS_SCAG.LIQUANTI
          * --- testo se gli scaglioni esistono
          * --- Se gi� esistono aggiorno scaglioni
          this.w_TROV = .T.
          if this.w_QTA<>this.w_LIQUANTI
            this.w_PREZZOBP = _Curs_LIS_SCAG.LIPREZZO
            if LEFT(this.w_CRITER,1)="L"
              this.w_SCONT1 = NVL(_Curs_LIS_SCAG.LISCONT1,0)
              this.w_SCONT2 = NVL(_Curs_LIS_SCAG.LISCONT2,0)
              this.w_SCONT3 = NVL(_Curs_LIS_SCAG.LISCONT3,0)
              this.w_SCONT4 = NVL(_Curs_LIS_SCAG.LISCONT4,0)
              if this.w_CLAGGSCO="C"
                this.w_LISCONT1 = this.w_SCONT1
                this.w_LISCONT2 = this.w_SCONT2
                this.w_LISCONT3 = this.w_SCONT3
                this.w_LISCONT4 = this.w_SCONT4
              endif
              if this.w_CRITER="LN"
                * --- Se la base di calcolo � al netto degli sconti li applico
                this.w_PREZZOBP = cp_ROUND(this.w_PREZZOBP * (1+this.w_SCONT1/100)*(1+this.w_SCONT2/100)*(1+this.w_SCONT3/100)*(1+this.w_SCONT4/100),5)
              endif
            endif
            if this.oParentObject.w_CALCOLA="P"
              * --- faccio la proporzione
              this.w_PREZZOB = this.w_PREZZOPRO * (this.w_PREZZOBP/this.w_LIPREZZO)
              * --- Eseguo gli arrotondamenti a seconda del valore
              this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOB, 0, "L","L",0,0,0,this.w_DECTOT, 0, this.w_CLARROT1, this.w_CLVALOR1, this.w_CLARROT2, this.w_CLVALOR2, this.w_CLARROT3, this.w_CLVALOR3, this.w_CLARROT4)
            else
              * --- riapplico la base su tutti gli scaglioni
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PREZZOFI = NEWPRZLI(this.w_PREZZOBP, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CLAGGCAO,this.w_CAMBIO,this.w_DECTOT, this.w_CLRICVAL, this.w_CLARROT1, this.w_CLVALOR1, this.w_CLARROT2, this.w_CLVALOR2, this.w_CLARROT3, this.w_CLVALOR3, this.w_CLARROT4 ,this.w_CLMOLTIP, this.w_CLMOLTI2)
            endif
          else
            this.w_PREZZOFI = this.w_PREZZOPRO
            this.w_QTA = this.w_LIQUANTI
          endif
          * --- Write into LIS_SCAG
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
            +",LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                +i_ccchkf ;
            +" where ";
                +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and LIQUANTI = "+cp_ToStrODBC(this.w_QTA);
                   )
          else
            update (i_cTable) set;
                LISCONT1 = this.w_LISCONT1;
                ,LISCONT2 = this.w_LISCONT2;
                ,LISCONT3 = this.w_LISCONT3;
                ,LISCONT4 = this.w_LISCONT4;
                ,LIPREZZO = this.w_PREZZOFI;
                &i_ccchkf. ;
             where;
                LICODART = this.oParentObject.w_CODART;
                and LIROWNUM = this.w_ROWNUM;
                and LIQUANTI = this.w_QTA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_OK = .T.
            select _Curs_LIS_SCAG
            continue
          enddo
          use
        endif
        if !this.w_TROV and this.w_PREZZOPRO<>0
          * --- la riga non � presente in LIS_SCAG ma esiste in LIS_TINI (prezzo a zero)
          *     Pertanto non posso aggiornarla ma solo inserirla
          this.w_PREZZOFI = this.w_PREZZOPRO
          this.w_QTA = this.w_LIQUANTI
          if this.w_CLAGGSCO="C"
            this.w_LISCONT1 = 0
            this.w_LISCONT2 = 0
            this.w_LISCONT3 = 0
            this.w_LISCONT4 = 0
          endif
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+",LIPREZZO"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QTA),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QTA,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4,'LIPREZZO',this.w_PREZZOFI)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QTA;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 ,this.w_PREZZOFI;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_OK = .T.
        endif
      endif
      if (this.w_AGGNUM<>0 or (this.w_ROWNUM<>0 AND !this.w_TROV)) OR this.oParentObject.w_FLAGGIO="S"
        if LEFT(this.w_CRITER,1)="L" AND this.w_CLORISCA="S"
          * --- Select from LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2],.t.,this.LIS_SCAG_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" LIS_SCAG ";
                +" where LICODART="+cp_ToStrODBC(this.oParentObject.w_CODART)+" AND LIROWNUM="+cp_ToStrODBC(this.w_ROWRIF)+"";
                 ,"_Curs_LIS_SCAG")
          else
            select * from (i_cTable);
             where LICODART=this.oParentObject.w_CODART AND LIROWNUM=this.w_ROWRIF;
              into cursor _Curs_LIS_SCAG
          endif
          if used('_Curs_LIS_SCAG')
            select _Curs_LIS_SCAG
            locate for 1=1
            do while not(eof())
            this.w_QTARIF = _Curs_LIS_SCAG.LIQUANTI
            this.w_PREZZORIF = _Curs_LIS_SCAG.LIPREZZO
            this.w_NSCONT1RIF = _Curs_LIS_SCAG.LISCONT1
            this.w_NSCONT2RIF = _Curs_LIS_SCAG.LISCONT2
            this.w_NSCONT3RIF = _Curs_LIS_SCAG.LISCONT3
            this.w_NSCONT4RIF = _Curs_LIS_SCAG.LISCONT4
            if this.w_CRITER="LN"
              * --- Se la base di calcolo � al netto degli sconti li applico
              this.w_PREZZORIF = cp_ROUND(this.w_PREZZORIF * (1+this.w_NSCONT1RIF/100)*(1+this.w_NSCONT2RIF/100)*(1+this.w_NSCONT3RIF/100)*(1+this.w_NSCONT4RIF/100),5)
            endif
            if this.w_CLAGGSCO="C"
              this.w_SCONT1RIF = this.w_NSCONT1RIF
              this.w_SCONT2RIF = this.w_NSCONT2RIF
              this.w_SCONT3RIF = this.w_NSCONT3RIF
              this.w_SCONT4RIF = this.w_NSCONT4RIF
            else
              this.w_SCONT1RIF = this.w_LISCONT1
              this.w_SCONT2RIF = this.w_LISCONT2
              this.w_SCONT3RIF = this.w_LISCONT3
              this.w_SCONT4RIF = this.w_LISCONT4
            endif
            if this.w_QTARIF<>this.w_LIQUANTRIF
              if this.oParentObject.w_CALCOLA="P"
                * --- faccio la proporzione
                this.w_PREZZOBRIF = this.w_PREZZOPRO * (this.w_PREZZORIF/this.oParentObject.w_BASCALC)
                * --- Eseguo gli arrotondamenti a seconda del valore
                this.w_PREZZOFIRIF = NEWPRZLI(this.w_PREZZOBRIF, 0, "L","L",0,0,0,this.w_DECTOT, 0, this.w_CLARROT1, this.w_CLVALOR1, this.w_CLARROT2, this.w_CLVALOR2, this.w_CLARROT3, this.w_CLVALOR3, this.w_CLARROT4)
              else
                * --- riapplico la base su tutti gli scaglioni
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_PREZZOFIRIF = NEWPRZLI(this.w_PREZZORIF, this.w_PERCRIC, this.w_LORNET,this.w_LORNET1,this.w_PERIVA, this.w_CLAGGCAO,this.w_CAMBIO,this.w_DECTOT, this.w_CLRICVAL, this.w_CLARROT1, this.w_CLVALOR1, this.w_CLARROT2, this.w_CLVALOR2, this.w_CLARROT3, this.w_CLVALOR3, this.w_CLARROT4 ,this.w_CLMOLTIP, this.w_CLMOLTI2)
              endif
              if this.w_PREZZOFIRIF=0 AND this.w_PREZZOPRO<>0
                this.w_PREZZOFIRIF = this.w_PREZZOPRO
              endif
            else
              this.w_PREZZOFIRIF = this.w_PREZZOPRO
              this.w_QTARIF = this.w_LIQUANTRIF
            endif
            * --- scrivo il listino da creare con gli scaglioni del listino di riferimento
            if this.w_PREZZOFIRIF<>0 AND (this.w_AGGNUM<>0 or (this.w_ROWNUM<>0 AND !this.w_TROV)) 
              * --- Insert into LIS_SCAG
              i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+",LIPREZZO"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_SCAG','LICODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_SCAG','LIROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QTARIF),'LIS_SCAG','LIQUANTI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT1RIF),'LIS_SCAG','LISCONT1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT2RIF),'LIS_SCAG','LISCONT2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT3RIF),'LIS_SCAG','LISCONT3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT4RIF),'LIS_SCAG','LISCONT4');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFIRIF),'LIS_SCAG','LIPREZZO');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'LIROWNUM',this.w_AGGNUM,'LIQUANTI',this.w_QTARIF,'LISCONT1',this.w_SCONT1RIF,'LISCONT2',this.w_SCONT2RIF,'LISCONT3',this.w_SCONT3RIF,'LISCONT4',this.w_SCONT4RIF,'LIPREZZO',this.w_PREZZOFIRIF)
                insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO &i_ccchkf. );
                   values (;
                     this.oParentObject.w_CODART;
                     ,this.w_AGGNUM;
                     ,this.w_QTARIF;
                     ,this.w_SCONT1RIF;
                     ,this.w_SCONT2RIF;
                     ,this.w_SCONT3RIF;
                     ,this.w_SCONT4RIF;
                     ,this.w_PREZZOFIRIF;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            if this.oParentObject.w_FLAGGIO="S"
              * --- aggiorno anche li listino di riferimento
              * --- se l'um � diversa riproporziono
              if this.w_UMLISAGG<>this.oParentObject.w_UMBAS
                this.w_PREZZOFIRIF = this.w_PREZZOPRO * (this.w_BASUMLIS/this.oParentObject.w_BASCALC)
              endif
              * --- Write into LIS_SCAG
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_SCONT1RIF),'LIS_SCAG','LISCONT1');
                +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_SCONT2RIF),'LIS_SCAG','LISCONT2');
                +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_SCONT3RIF),'LIS_SCAG','LISCONT3');
                +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_SCONT4RIF),'LIS_SCAG','LISCONT4');
                +",LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFIRIF),'LIS_SCAG','LIPREZZO');
                    +i_ccchkf ;
                +" where ";
                    +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                    +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
                    +" and LIQUANTI = "+cp_ToStrODBC(this.w_QTARIF);
                       )
              else
                update (i_cTable) set;
                    LISCONT1 = this.w_SCONT1RIF;
                    ,LISCONT2 = this.w_SCONT2RIF;
                    ,LISCONT3 = this.w_SCONT3RIF;
                    ,LISCONT4 = this.w_SCONT4RIF;
                    ,LIPREZZO = this.w_PREZZOFIRIF;
                    &i_ccchkf. ;
                 where;
                    LICODART = this.oParentObject.w_CODART;
                    and LIROWNUM = this.w_ROWRIF;
                    and LIQUANTI = this.w_QTARIF;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_TROV = .T.
              select _Curs_LIS_SCAG
              continue
            enddo
            use
          endif
          if !this.w_TROV
            if this.w_CLAGGSCO<>"F"
              this.w_SCONT1RIF = this.w_LISCONT1
              this.w_SCONT2RIF = this.w_LISCONT2
              this.w_SCONT3RIF = this.w_LISCONT3
              this.w_SCONT4RIF = this.w_LISCONT4
            endif
            * --- Insert into LIS_SCAG
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+",LIPREZZO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_SCAG','LICODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_SCAG','LIROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LIQUANTI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT1RIF),'LIS_SCAG','LISCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT2RIF),'LIS_SCAG','LISCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT3RIF),'LIS_SCAG','LISCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT4RIF),'LIS_SCAG','LISCONT4');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'LIS_SCAG','LIPREZZO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'LIROWNUM',this.w_AGGNUM,'LIQUANTI',0,'LISCONT1',this.w_SCONT1RIF,'LISCONT2',this.w_SCONT2RIF,'LISCONT3',this.w_SCONT3RIF,'LISCONT4',this.w_SCONT4RIF,'LIPREZZO',this.w_PREZZOPRO)
              insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODART;
                   ,this.w_AGGNUM;
                   ,0;
                   ,this.w_SCONT1RIF;
                   ,this.w_SCONT2RIF;
                   ,this.w_SCONT3RIF;
                   ,this.w_SCONT4RIF;
                   ,this.w_PREZZOPRO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if this.oParentObject.w_FLAGGIO="S"
              if this.w_UMLISAGG<>this.oParentObject.w_UMBAS
                this.w_PREZZOPRO = this.w_PREZZOPRO * (this.w_BASUMLIS/this.oParentObject.w_BASCALC)
              endif
              * --- aggiorno anche li listino di riferimento
              * --- Insert into LIS_SCAG
              i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+",LIPREZZO"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_SCAG','LICODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF),'LIS_SCAG','LIROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LIQUANTI');
                +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT1');
                +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT2');
                +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT3');
                +","+cp_NullLink(cp_ToStrODBC(0),'LIS_SCAG','LISCONT4');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'LIS_SCAG','LIPREZZO');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'LIROWNUM',this.w_ROWRIF,'LIQUANTI',0,'LISCONT1',0,'LISCONT2',0,'LISCONT3',0,'LISCONT4',0,'LIPREZZO',this.w_PREZZOPRO)
                insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LISCONT1,LISCONT2,LISCONT3,LISCONT4,LIPREZZO &i_ccchkf. );
                   values (;
                     this.oParentObject.w_CODART;
                     ,this.w_ROWRIF;
                     ,0;
                     ,0;
                     ,0;
                     ,0;
                     ,0;
                     ,this.w_PREZZOPRO;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
        else
          * --- inserisco solo una riga
          if this.w_PREZZOPRO<>0 AND (this.w_AGGNUM<>0 or (this.w_ROWNUM<>0 AND !this.w_TROV)) 
            if this.w_CLORISCA<>"S"
              this.w_LIQUANTRIF = 0
            endif
            * --- Insert into LIS_SCAG
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODART),'LIS_SCAG','LICODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AGGNUM),'LIS_SCAG','LIROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LIQUANTRIF),'LIS_SCAG','LIQUANTI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'LIS_SCAG','LIPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LICODART',this.oParentObject.w_CODART,'LIROWNUM',this.w_AGGNUM,'LIQUANTI',this.w_LIQUANTRIF,'LIPREZZO',this.w_PREZZOPRO,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
              insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CODART;
                   ,this.w_AGGNUM;
                   ,this.w_LIQUANTRIF;
                   ,this.w_PREZZOPRO;
                   ,this.w_LISCONT1;
                   ,this.w_LISCONT2;
                   ,this.w_LISCONT3;
                   ,this.w_LISCONT4;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          if this.oParentObject.w_FLAGGIO="S" AND LEFT(this.w_CRITER,1)="C"
            * --- aggiorno il costo standard
            * --- Write into PAR_RIOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_RIOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRCOSSTA ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOPRO),'PAR_RIOR','PRCOSSTA');
                  +i_ccchkf ;
              +" where ";
                  +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                     )
            else
              update (i_cTable) set;
                  PRCOSSTA = this.w_PREZZOPRO;
                  &i_ccchkf. ;
               where;
                  PRCODART = this.oParentObject.w_CODART;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        this.w_OK = .T.
      endif
      if this.w_OK
        this.w_ULTORA = HOUR(DATETIME( ))
        this.w_ULTMIN = MINUTE(DATETIME( ))
        * --- Write into CLLSMAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CLLSMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLLSMAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CLLSMAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CLULTORA ="+cp_NullLink(cp_ToStrODBC(this.w_ULTORA),'CLLSMAST','CLULTORA');
          +",CLULTMIN ="+cp_NullLink(cp_ToStrODBC(this.w_ULTMIN),'CLLSMAST','CLULTMIN');
              +i_ccchkf ;
          +" where ";
              +"CLSERIAL = "+cp_ToStrODBC(this.w_CLSERIAL);
                 )
        else
          update (i_cTable) set;
              CLULTORA = this.w_ULTORA;
              ,CLULTMIN = this.w_ULTMIN;
              &i_ccchkf. ;
           where;
              CLSERIAL = this.w_CLSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorno ora e minuto dell'ultimo lancio della scheda di calcolo
      endif
        select _Curs_GSVA8MSL
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_OK
      ah_ErrorMsg("Elaborazione terminata")
    else
      ah_ErrorMsg("Per le impostazioni effettuate non esistono listini-prezzi da aggiornare")
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica la formattazione delle colonne editabili
    SELECT (ND)
    FOR I=1 TO this.oParentObject.w_ZOOM.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    this.w_APPO = UPPER(this.oParentObject.w_ZOOM.grd.Column&NC..ControlSource)
    if this.w_APPO = "PREZZOFI" OR this.w_APPO = "VARPERC3" OR this.w_APPO = "VARPERC2" OR this.w_APPO="PREZZOUM"
      this.oParentObject.w_ZOOM.grd.Column&NC..Format = "KR"
      this.oParentObject.w_ZOOM.grd.Column&NC..Enabled = .T.
    endif
    if this.w_APPO= "PREZZOUM" 
      if this.oParentObject.w_ORIGINE<>"D"
        this.oParentObject.w_ZOOM.grd.Column&NC..Visible = .F.
        this.oParentObject.w_ZOOM.grd.Column&NC..Width = 0
      else
        this.oParentObject.w_ZOOM.grd.Column&NC..Visible = .T.
        this.oParentObject.w_ZOOM.grd.Column&NC..Width = 108
      endif
    endif
    ENDFOR
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !this.w_PRIMA
      this.w_CLBASLIS = NVL(_Curs_GSVA8MSL.CLBASLIS,SPACE(5))
      this.w_CLDATRIF = NVL(_Curs_GSVA8MSL.CLDATRIF, CP_CHARTODATE("  -  -    "))
      this.w_CLCALSEL = NVL(_Curs_GSVA8MSL.CLCALSEL,SPACE(1))
      this.w_CLRICAR1 = NVL(_Curs_GSVA8MSL.CLRICAR1,0)
      this.w_CLCAONAZ = NVL(_Curs_GSVA8MSL.CLCAONAZ,0)
      this.w_CLAGGCAO = NVL(_Curs_GSVA8MSL.CLAGGCAO,0)
      this.w_CLCAORIF = NVL(_Curs_GSVA8MSL.CLCAORIF,0)
      this.w_CLAGGSCO = NVL(_Curs_GSVA8MSL.CLAGGSCO,SPACE(1))
      this.w_CLAGGLIS = NVL(_Curs_GSVA8MSL.CLAGGLIS,SPACE(5))
      this.w_CLRICVAL = NVL(_Curs_GSVA8MSL.CLRICVAL,0)
      this.w_CLMOLTIP = NVL(_Curs_GSVA8MSL.CLMOLTIP,0)
      this.w_CLMOLTI2 = NVL(_Curs_GSVA8MSL.CLMOLTI2,0)
      this.w_CAMBIO = this.w_CLCAORIF
      * --- leggo i flag scorpori dal listino da aggiornare
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSIVALIS,LSVALLIS"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.w_CLAGGLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSIVALIS,LSVALLIS;
          from (i_cTable) where;
              LSCODLIS = this.w_CLAGGLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LORNET = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
        this.w_VALUTA = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_DECTOT = GETVALUT(this.w_VALUTA, "VADECUNI")
      * --- leggo i flag scorpori dal listino di base se il criterio � da listino
      * --- Read from LISTINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LISTINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LSIVALIS"+;
          " from "+i_cTable+" LISTINI where ";
              +"LSCODLIS = "+cp_ToStrODBC(this.w_CLBASLIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LSIVALIS;
          from (i_cTable) where;
              LSCODLIS = this.w_CLBASLIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LORNET1 = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PERCENT = 0
      do case
        case this.w_CLCALSEL="P"
          * --- Read from RIC_PREZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIC_PREZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIC_PREZ_idx,2],.t.,this.RIC_PREZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LIPERRIC"+;
              " from "+i_cTable+" RIC_PREZ where ";
                  +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
                  +" and LICODLIS = "+cp_ToStrODBC(this.w_CLBASLIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LIPERRIC;
              from (i_cTable) where;
                  LICODART = this.oParentObject.w_CODART;
                  and LICODLIS = this.w_CLBASLIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERCENT = NVL(cp_ToDate(_read_.LIPERRIC),cp_NullValue(_read_.LIPERRIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case this.w_CLCALSEL="C"
          * --- Read from CLA_RICA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CLA_RICA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLA_RICA_idx,2],.t.,this.CLA_RICA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CRPERCEN"+;
              " from "+i_cTable+" CLA_RICA where ";
                  +"CRCODICE = "+cp_ToStrODBC(this.w_CODRIC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CRPERCEN;
              from (i_cTable) where;
                  CRCODICE = this.w_CODRIC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERCENT = NVL(cp_ToDate(_read_.CRPERCEN),cp_NullValue(_read_.CRPERCEN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
      * --- Controllo che tipo di ricalcolo � stato utilizzato.
      do case
        case this.w_CLCALSEL$"CP"
          this.w_PERCRIC = this.w_PERCENT
        case this.w_CLCALSEL="R"
          this.w_PERCRIC = this.w_CLRICAR1
      endcase
      this.w_PRIMA = .T.
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='*TMPLIST'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='*TMPZOOM'
    this.cWorkTables[8]='*TMPSCAG'
    this.cWorkTables[9]='CLA_RICA'
    this.cWorkTables[10]='RIC_PREZ'
    this.cWorkTables[11]='LIS_TINI'
    this.cWorkTables[12]='*TMPSCAGRIF'
    this.cWorkTables[13]='LIS_SCAG'
    this.cWorkTables[14]='PAR_RIOR'
    this.cWorkTables[15]='CLLSMAST'
    this.cWorkTables[16]='KEY_ARTI'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_TMPLIST')
      use in _Curs_TMPLIST
    endif
    if used('_Curs_TMPSCAG')
      use in _Curs_TMPSCAG
    endif
    if used('_Curs_TMPSCAGRIF')
      use in _Curs_TMPSCAGRIF
    endif
    if used('_Curs_GSVA8MSL')
      use in _Curs_GSVA8MSL
    endif
    if used('_Curs_LIS_TINI')
      use in _Curs_LIS_TINI
    endif
    if used('_Curs_LIS_SCAG')
      use in _Curs_LIS_SCAG
    endif
    if used('_Curs_LIS_SCAG')
      use in _Curs_LIS_SCAG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
