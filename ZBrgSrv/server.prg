*--- Server Bridge Service
*--- tnPort Porta su cui mettersi in ascolto, se non passata utilizza la porta 63333
Lparameters tnPort

ON SHUTDOWN CLEAR EVENTS

#Define VER "1.0.2" 
#Define EOT Chr(4) && End of Transmission sign
#Define NIM_ADD 0
#Define NIM_MODIFY 1
#Define NIM_DELETE 2
#Define NIF_MESSAGE 1
#Define NIF_ICON 2
#Define NIF_TIP 4
#Define NOTIFYICONDATA_SIZE 88
#Define MAX_PATH 260
#Define GWL_WNDPROC -4
#Define IMAGE_ICON 1
#Define LR_LOADFROMFILE 0x0010
#Define LR_DEFAULTSIZE 0x0040

#Define WM_MOUSEMOVE 0x0200
#Define WM_LBUTTONDOWN 0x0201
#Define WM_LBUTTONUP 0x0202
#Define WM_RBUTTONDOWN 0x0204
#Define WM_RBUTTONUP 0x0205
#Define WM_MBUTTONDOWN 0x0207
#Define WM_MBUTTONUP 0x0208
#Define WM_LBUTTONDBLCLK 0x203

* Activating Windows Calculator
#DEFINE SW_SHOWNORMAL 1
DECLARE INTEGER WinExec IN kernel32;
	STRING lpCmdLine, INTEGER nCmdShow
		
DECLARE INTEGER SetForegroundWindow IN user32 INTEGER hwnd

DECLARE INTEGER FindWindow IN user32;
	STRING lpClassName, STRING lpWindowName
	
Clear
If Vartype(tnPort) = "C"
	tnPort=VAL(tnPort)
ELSE 
	tnPort = 63333
ENDIF
*--- Verifico che il servizio non sia gi� attivo
hwnd = FindWindow (.NULL., "Zucchetti Bridge Service"+SPACE(10))
IF hwnd <> 0
	MESSAGEBOX("Il servizio � gi� attivo", 64, "Zucchetti Bridge Service")
	= SetForegroundWindow (hwnd)
	RETURN
ENDIF
oldDate=SET("DATE")
SET DATE ITALIAN 
oForm = Createobject('ServerForm',tnPort)
Read Events
ON SHUTDOWN 
Release oForm
SET DATE &oldDate
Return

Define Class ServerForm As Form
	Height = 250
	Width = 375
	ShowWindow = 2
	DoCreate = .T.
	AutoCenter = .T.
	BorderStyle = 2
	Caption = "Zucchetti Bridge Service"+SPACE(10)
	MaxButton = .F.
	DrawStyle = 0
	Icon = "NETHOOD.ICO"
	Name = "zbrgsrv"

	nProtocol = 0
	nPort = 0 && Valorizzare con una porta non utilizzata
	nstat = 0
	 
	Add Object taskbaricon As TaskbarStatus

	Add Object editout As EditBox With ;
		Enabled = .T., ;
		Height = 169, ;
		Left = 12, ;
		ReadOnly = .T., ;
		Top = 24, ;
		Width = 349, ;
		Name = "EditOut",;
		Visible = .T.

	Add Object btn_avvia As CommandButton With ;
		Top = 216, ;
		Left = 12, ;
		Height = 25, ;
		Width = 85, ;
		Caption = "\<Avvia", ;
		Name = "btn_avvia"

	Add Object btn_arresta As CommandButton With ;
		Top = 216, ;
		Left = 108, ;
		Height = 25, ;
		Width = 85, ;
		Caption = "A\<rresta", ;
		Name = "btn_arresta"

	Add Object btn_esci As CommandButton With ;
		Top = 216, ;
		Left = 204, ;
		Height = 25, ;
		Width = 85, ;
		Caption = "\<Esci", ;
		Name = "btn_esci"

	Add Object btn_help As CommandButton With ;
		Top = 216, ;
		Left = 324, ;
		Height = 25, ;
		Width = 36, ;
		Caption = "\<?", ;
		Name = "btn_help"

	Procedure editout.Refresh
		With This
			.SelStart = Len(.Text)
		Endwith
	Endproc

	Procedure ShowIcon(nMode)
		* shows or hides icon in the systray
		With This.taskbaricon
			If nMode = 0
				.DeleteIcon
			Else
				.baloon = "Zucchetti Bridge Service "+Iif(Thisform.oSvrListener.bActivate, "(Attivo)", "(Disattivo)")
				.InitIcon(Thisform.Icon)
			Endif
		Endwith
	Endproc

	Procedure taskbaricon.OnUdfMessage
		Parameters wParam As Integer, Lparam As Integer
		DoDefault()
		If Lparam=WM_LBUTTONDBLCLK
			IF Thisform.Visible
				Thisform.WindowState=1
				Thisform.Visible=.F.
			ELSE 
				Thisform.Visible=.T.
				Thisform.WindowState=0
			ENDIF 	
		Endif
	Endproc

	Procedure Resize()
		If This.WindowState= 1
			This.Visible=.F.
		Endif
		DoDefault()
	Endproc

	Procedure btn_avvia.Click
		Thisform.oSvrListener.bActivate=.T.
		Thisform.ShowIcon(1)
		With Thisform.editout
			.Value= .Value + 'Info Time: '+Ttoc(Datetime())+ Chr(13) + Chr(10)
			.Value = .Value + 'Info ' + 'Start Server' + Chr(13) + Chr(10)
			.Refresh()
		Endwith
	Endproc

	Procedure btn_arresta.Click
		Thisform.oSvrListener.bActivate=.F.
		Thisform.ShowIcon(1)
		With Thisform.editout
			.Value= .Value + 'Info Time: '+Ttoc(Datetime())+ Chr(13) + Chr(10)
			.Value = .Value + 'Info ' + 'Stop Server' + Chr(13) + Chr(10)
			.Refresh()
		Endwith
	Endproc

	Procedure btn_esci.Click
		If Messagebox("Terminare la procedura?",68,"Zucchetti Bridge Service")=6
			Clear Events
		Endif
	Endproc

	Procedure btn_help.Click
		Local Msg
		Msg="Zucchetti Bridge Service   Rel. "+VER+Chr(13)+Chr(13)
		Msg=Msg+"Server per l'esecuzione di programmi in remoto."+Chr(13)+Chr(13)
		Msg=Msg+"Copyright (C) by Zucchetti S.p.a."+Chr(13)
		Msg=Msg+"Tutti i diritti riservati."+Chr(13)+Chr(13)
		Msg=Msg+"--------------------"+Chr(13)
		Msg=Msg+"  Protocollo TCP"+Chr(13)
		Msg=Msg+"  Porta: " +Transform(Thisform.nPort)+Chr(13)
		Msg=Msg+"  Stato: " +Iif(Thisform.oSvrListener.bActivate, "Attivo", "Disattivo")+Chr(13)
		Messagebox(Msg, 64, "Informazioni su Zucchetti Bridge Service")
	Endproc

	Procedure Load
		Sys(2333,0)
		_vfp.AutoYield = .F.
		Return
	Endproc

	Procedure Unload
		_vfp.AutoYield = .T.
		Return
	Endproc

	Procedure QueryUnload
		Nodefault
		This.WindowState = 1
		Return .F.
	Endproc

	Procedure Init
		Lparameters tnPort
		This.nPort = tnPort
		This.AddObject('oSvrListener', 'TcpServerL')
		This.oSvrListener.Listen()
		With Thisform.editout
			.Value= .Value + 'Info Time: '+Ttoc(Datetime())+ Chr(13) + Chr(10)
			.Value = .Value + 'Info ' + 'Start Server' + Chr(13) + Chr(10)
			.Refresh()
		Endwith
		_Screen.Visible=.F.
		This.Show()
		This.WindowState = 1
		This.ShowIcon(1)
		Return
	Endproc
Enddefine

Define Class TcpServer As TcpServerL
	cReceiveBuffer=""
	nCreateTime=0
	Procedure Init
		Lparameters tnRequestID
		Local llRetVal
		This.nCreateTime = Seconds()
		This.Accept(tnRequestID)
		Thisform.nstat = This.State
		Thisform.Refresh()
		Return
	Endproc

	Procedure DataArrival
		Lparameters tnByteCount
		*--- Dichiara una variabile per i dati in ingresso.
		*--- Richiama il metodo GetData.
		Local lcBuffer
		lcBuffer = Space(tnByteCount)
		This.GetData( @lcBuffer, , tnByteCount )
		If At( EOT, lcBuffer ) = 0 && Non ho ancora finito
			This.cReceiveBuffer = This.cReceiveBuffer + lcBuffer
		Else
			This.cReceiveBuffer = This.cReceiveBuffer + Left( lcBuffer, At( EOT, lcBuffer ) -1 )
			*--- Decripto il comando
			cCommand=Cifracnf(This.cReceiveBuffer, 'D')
			*--- Eseguo la stringa inviata
			L_errsav=on('ERROR')
			Messaggio=''
			ON ERROR Messaggio=Message()
				Execscript(cCommand)
			on error &L_errsav

			With Thisform.editout
				IF not empty(Messaggio)
					.Value = .Value + 'Error ' + 'Executed command: '+ Chr(13) + Chr(10) + cCommand + Chr(13) + Chr(10)
				ELSE 
					.Value = .Value + 'Info ' + 'Executed command: '+ Chr(13) + Chr(10) + cCommand + Chr(13) + Chr(10)
				ENDIF 
				.Refresh()
			Endwith
			*--- Chiudo la connessione
			This.Close()
			With Thisform.editout
				.Value= .Value + 'Info Time: '+Ttoc(Datetime())+ Chr(13) + Chr(10)
				.Value = .Value + 'Info ' + 'Close connection ' + Chr(13) + Chr(10)+ Chr(13) + Chr(10)
				.Refresh()
			Endwith
			This.cReceiveBuffer = ''
		Endif
		Thisform.Refresh()
	Endproc

	Procedure Close
		*--- Chiudo la connesione
		This.Object.Close()
	Endproc
Enddefine
*--- Server TCP Listener
Define Class TcpServerL As OleControl
	OleClass="MSWinsock.Winsock"
	bActivate=.T. 	&&Se a true accetta la connessione, utilizzato per disabilitare temporaneamente l'ascolto

	Procedure Init
		This.protocol = Thisform.nProtocol
		This.localport = Thisform.nPort
	Endproc

	Procedure ConnectionRequest
		Lparameter tnRequestID
		If This.bActivate=.T.
			For Each lControl In Thisform.Controls
				DoEvents
				If Lower(lControl.Class) != 'TcpServer' Or ;
						((Seconds()-lControl.nCreateTime) < 5 Or lControl.State = 7)
					Loop
				Endif
				*--- Rimozione socket chiuso
				With Thisform.editout
					.Value = .Value + 'Info ' + 'Remove ' + lControl.Name + Chr(13) + Chr(10)
					.Refresh()
				Endwith
				Thisform.RemoveObject(lControl.Name )
			Endfor
			*--- Creo un socket con un nome univoco
			Thisform.AddObject( Sys(2015), 'TcpServer', m.tnRequestID )
			*--- Segnalo la creazione di una nuova connessione
			With Thisform.editout
				.Value= .Value + 'Info Time: '+ Ttoc(Datetime())+ Chr(13) + Chr(10)
				.Value = .Value + 'Info ' + 'Connection added ID '+ ;
					STR(m.tnRequestID) + Chr(13) + Chr(10)
				.Refresh()
			Endwith
			Thisform.Refresh()
			Return
		Endif
	Endproc
Enddefine

Define Class TaskbarStatus As Custom
	hWindow=0
	AppID=1
	MessageID=0x4001
	hOrigProc=0
	hIcon=0
	baloon=""
	LastMouseX=0
	LastMouseY=0

	Procedure Init
		This.Declare
		This.hWindow = Thisform.HWnd
		This.hOrigProc = GetWindowLong(This.hWindow, GWL_WNDPROC)

	Procedure Destroy
		This.DeleteIcon

	Procedure DeleteIcon
		If This.hIcon <> 0
			This.SetIcon(NIM_DELETE)
			= DestroyIcon(This.hIcon)
			This.hIcon = 0

			If Version(5) >= 900  && VFP9+
				= Unbindevents(This.hWindow, This.MessageID)
			Endif
		Endif

	Procedure InitIcon(hIcon)
		This.DeleteIcon

		Do Case
			Case Vartype(m.hIcon)="N"
				This.hIcon = m.hIcon
			Case Vartype(m.hIcon)="C"
				This.hIcon = This.LoadIcon(m.hIcon)
			Otherwise
				Return
		Endcase
		This.SetIcon(NIM_ADD)

	Procedure SetIcon(cAction)
		Local cBuffer

		cBuffer = num2dword(NOTIFYICONDATA_SIZE) +;
			num2dword(This.hWindow) +;
			num2dword(This.AppID) +;
			num2dword(NIF_ICON + NIF_MESSAGE + NIF_TIP) +;
			num2dword(This.MessageID) +;
			num2dword(This.hIcon) +;
			PADR(This.baloon, 64, Chr(0))

		If Shell_NotifyIcon(m.cAction, @cBuffer) <> 0
			If Version(5) >= 900  && VFP9+
				= Bindevent(This.hWindow, This.MessageID,;
					THIS, "HookedWindowProc")
			Endif
		Endif

	Procedure GetMousePos(nX, nY)
		* retrieves position of the cursor in screen coordinates
		Local cBuffer
		cBuffer = Replicate(Chr(0), 8)
		= GetCursorPos(@cBuffer)
		nX = buf2dword(Substr(cBuffer, 1,4))
		nY = buf2dword(Substr(cBuffer, 5,4))

	Procedure LoadIcon(cIconFile)
		* loads icon from a file
		Local hIcon
		Try
			hIcon = LoadImage(0, m.cIconFile, IMAGE_ICON,;
				0,0, LR_LOADFROMFILE+LR_DEFAULTSIZE)
		Catch
			hIcon=0
		Endtry
		Return m.hIcon

	Procedure GetDefaultIcon
		* loads application icon
		Local cBuffer, nBufsize, nIconIndex, hIcon
		cBuffer = Replicate(Chr(0), MAX_PATH)
		nBufsize = GetModuleFileName (0, @cBuffer, MAX_PATH)
		cBuffer = Left(cBuffer, nBufsize)
		nIconIndex = 0  && works as a reference only
		hIcon = ExtractAssociatedIcon(0, cBuffer, @nIconIndex)
		Return m.hIcon

	Procedure OnUdfMessage(wParam As Integer, Lparam As Integer)
		* you will probably want to subclass this
		Local nX, nY
		Store 0 To nX, nY
		This.GetMousePos(@nX, @nY)
		This.LastMouseX=m.nX
		This.LastMouseY=m.nY

	Procedure HookedWindowProc(hWindow As Integer,;
			nMsgID As Integer, wParam As Integer, Lparam As Integer)
		* requires VFP9, otherwise ignored
		* note that input parameters are predefined and should not be changed
		* see WindowProc function for details

		Local nReturn
		nReturn=0

		Do Case
			Case nMsgID=This.MessageID
				This.OnUdfMessage(wParam, Lparam)

			Otherwise
				* pass control to the original window procedure
				nReturn = CallWindowProc(This.hOrigProc, This.hWindow,;
					m.nMsgID, m.wParam, m.lParam)
		Endcase
		Return nReturn

	Protected Procedure Declare
		Declare Integer DestroyIcon In user32 Integer hIcon
		Declare Integer SetForegroundWindow In user32 Integer hWindow
		Declare Integer GetCursorPos In user32 String @ lpPoint
		Declare Integer Shell_NotifyIcon In shell32 Integer dwMsg, String @lpdata
		Declare Integer GetWindowLong In user32 Integer hWindow, Integer nIndex

		Declare Integer ExtractAssociatedIcon In shell32;
			INTEGER hInst, String lpIconPath, Integer @lpiIcon

		Declare Integer GetModuleFileName In kernel32;
			INTEGER hModule, String @lpFilename, Integer nSize

		Declare Integer CallWindowProc In user32;
			INTEGER lpPrevWndFunc, Integer hWindow, Long Msg,;
			INTEGER wParam, Integer Lparam

		Declare Integer LoadImage In user32;
			INTEGER hinst, String lpszName, Integer uType,;
			INTEGER cxDesired, Integer cyDesired, Integer fuLoad
Enddefine

Function buf2dword(cBuffer)
	Return Asc(Substr(cBuffer, 1,1)) + ;
		BitLShift(Asc(Substr(cBuffer, 2,1)),  8) +;
		BitLShift(Asc(Substr(cBuffer, 3,1)), 16) +;
		BitLShift(Asc(Substr(cBuffer, 4,1)), 24)

Function num2dword(nValue)
	#Define m0  0x0100
	#Define m1  0x010000
	#Define m2  0x01000000
	If nValue < 0
		nValue = 0x100000000 + nValue
	Endif
	Local b0, b1, b2, b3
	b3 = Int(nValue/m2)
	b2 = Int((nValue - b3*m2)/m1)
	b1 = Int((nValue - b3*m2 - b2*m1)/m0)
	b0 = Mod(nValue, m0)
	Return Chr(b0)+Chr(b1)+Chr(b2)+Chr(b3)

	*--- Funzione per criptare/Decriptare il messaggio
Func Cifracnf
	Param pCnf,pTipo
	Private i_retcode,i_retval,i_Error
	i_retcode = 'go'
	i_retval = ''
	i_Error = ''
	* --- Local variables
	Private w_RESULT
	w_RESULT=Space(200)
	Private w_TempS
	w_TempS=Space(200)
	Private w_RESTO
	w_RESTO=0
	Private w_FIVECHAR
	w_FIVECHAR=Space(5)
	Private w_LOOP
	w_LOOP=0
	cifracnf_Pag1()
	Return(i_retval)

	*--- Cifratura
Procedure cifracnf_Pag1()
	Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
	* --- Funzione per cifrare / decifrare la stringa di connessione
	*     Utilizzata per impedire agli utente l'accesso ai dati criptando la stringa di connessione
	*     sul file CNF
	*     Parametri
	*     pCnf - stringa da cifrare / decifrare
	*     pTipo - Operazione ('C' Cifra, 'D' Decifra)
	w_RESULT = ""
	w_TempS = Alltrim( m.pCnf )
	* --- Rendo lunga la stringa sempre un multiplo di 5 se cripto
	If m.pTipo="C"
		w_RESTO = Mod( Len( m.w_TempS ) , 5 )
		If m.w_RESTO<>0
			w_TempS = m.w_TempS + Replicate(" ",5 - m.w_RESTO )
		Endif
	Else
		* --- Se decripto verifico che la lunghezza sia un multiplo di 5
		w_RESTO = Mod( Len( m.w_TempS ) , 5 )
		If m.w_RESTO<>0
			* --- non restituisco un messaggio perch� altrimenti, causa errori di utilizzo,
			*     lo darei anche all'avvio.
			*
			*     Cos� facendo lo visualizzo nella maschera di cifratura..
			i_retval = "Attenzione la stringa cifrata non � valida. Impossibile decifrare."
			Return
		Endif
	Endif
	* --- Vettore per trasposizioni...

	Dimension A_Sec[5]
	A_Sec[1]=3
	A_Sec[2]=1
	A_Sec[3]=5
	A_Sec[4]=2
	A_Sec[5]=4
	Do While Len ( m.w_TempS )>0
		w_FIVECHAR = Left( m.w_TempS , 5 )
		If m.pTipo="C"
			cifracnf_Pag2()
			If i_retcode='stop' Or !Empty(i_Error)
				Return
			Endif
		Else
			cifracnf_Pag3()
			If i_retcode='stop' Or !Empty(i_Error)
				Return
			Endif
		Endif
		w_TempS = Substr( m.w_TempS,6,Len( m.w_TempS ) )
	Enddo
	If m.pTipo="D"
		* --- Se decripto elimino gli spazi
		w_RESULT = Alltrim( m.w_RESULT )
	Endif
	i_retval = m.w_RESULT
	Return
Endproc


Procedure cifracnf_Pag2()
	Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
	* --- Cifra5
	*     Cifra i 5 caratteri contenuti in w_FIVECHAR e li mette in w_RESULT
	w_LOOP = 1
	Do While m.w_LOOP<=5
		w_RESULT = m.w_RESULT + Chr(Asc(Substr( m.w_FIVECHAR , A_Sec[ m.w_LOOP ],1))+A_Sec[ m.w_LOOP ])
		w_LOOP = m.w_LOOP + 1
	Enddo
Endproc


Procedure cifracnf_Pag3()
	Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
	* --- DeCifra5
	*     Decifra i 5 caratteri contenuti in w_FIVECHAR e li mette in w_RESULT
	w_LOOP = 1
	Do While m.w_LOOP<=5
		w_RESULT = m.w_RESULT + Chr(Asc(Substr( m.w_FIVECHAR ,Ascan(A_Sec, m.w_LOOP ,1),1))-A_Sec[Ascan(A_Sec, m.w_LOOP ,1)])
		w_LOOP = m.w_LOOP + 1
	Enddo
Endproc
