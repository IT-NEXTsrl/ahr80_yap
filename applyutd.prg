Public loghnd, ANS
ANS=6

loghnd = Fcreate("utd.LOG")
Do apply_utd_proc
Fclose(loghnd)
Messagebox("Nel file UTD.TXT � riportato l'elenco dei file modificati","APPLYUTD")

Procedure apply_utd_proc()
    Local filenumber,i,j,k,namearray,dirnumber, updfile, occorr
    filenumber = Adir(alistfile , "*.*def")
    PRGR=0
    oldPRGR=-1
    For i=1 To filenumber
    	namefile = alistfile[i,1]
    	If Inlist(Upper(Justext(namefile)),"MSTDEF","DTLDEF","MDTDEF")
    		gcString = Filetostr(namefile)
    		updfile = .F.
    		occorr=1
    		possearch=Atc("name='UTD", gcString)
    		PRGR=Int(i*100/filenumber)
    		If PRGR<>oldPRGR
    			Wait Window Sys(2003)+" -> "+Str(PRGR,3,0)+"%" Nowait
    			oldPRGR=PRGR
    		Endif
    		Do While possearch>0
    			inipos=Ratc("Icpitem.CPItem",Left(gcString,possearch))
    			finpos=Atc("Icpitem.CPItem",Substr(gcString,inipos+1))
    			myitem=Substrc(gcString,inipos,finpos)
    			If Atc("name='UTDC",myitem)>0 Or Atc("name='UTDV",myitem)>0
				    * cambio da Date a DateTime
    				NewItem=Strtran(myitem,"obj_type='D","obj_type='T")
    				NewItem=Strtran(NewItem,"video_len=8","video_len=14")
    				NewItem=Strtran(NewItem,"video_len=9","video_len=14")
    				If myitem<>NewItem
    					If ANS=6
    						ANS=Messagebox("Trovato: " + myitem + Chr(13) + " Continuo controllo?", 3 ,namefile)
    					Endif
    					If ANS=2
    						Return
    					Endif
    					gcString=Strtran(gcString,myitem,NewItem)
    					updfile = .T.
    				Endif
    			Endif
    			occorr = occorr+1
    			possearch=Atc("name='UTD", gcString, occorr)
    		Enddo
    		If updfile Then
    			If File(namefile+".bak")
    				Delete File (namefile+".bak")
    			Endif
    			Rename (namefile) To (namefile+".bak")
    			Strtofile(gcString,namefile)
    			Fputs(loghnd, Fullpath(namefile))
    		Endif
    	Endif
    Endfor

    * file di analisi
    Local newline
    newline=Chr(13)+Chr(10)
    Dimension astrconv(4)
    astrconv[1]="len=8"+newline  + "name='UTDC"+newline + "type='D"
    astrconv[2]="len=8"+newline  + "name='UTDV"+newline + "type='D"
    astrconv[3]="len=8"+newline  + "name='UTDC"+newline + "repeated=True"+newline + "type='D"
    astrconv[4]="len=8"+newline  + "name='UTDV"+newline + "repeated=True"+newline + "type='D"

    filenumber = Adir(alistfile , "*.shelve")
    For i=1 To filenumber
    	namefile = alistfile[i,1]
    	gcString = Filetostr(namefile)
    	updfile = .F.
    	For k=1 To 4
    		myitem=astrconv[k]
    		possearch=Atc(myitem, gcString)
    		If possearch>0
			    * cambio da Date a DateTime
    			NewItem=Strtran(myitem,"type='D","type='T")
    			NewItem=Strtran(NewItem,"len=8","len=14")
				gcString=Strtran(gcString,myitem,NewItem)
				updfile = .T.
    		Endif
    	Endfor
    	If updfile Then
    		If File(namefile+".bak")
    			Delete File (namefile+".bak")
    		Endif
    		Rename (namefile) To (namefile+".bak")
    		Strtofile(gcString,namefile)
	    	* aggiorno timestamp su analisi
    		namefile=Juststem(namefile)
    		gcString=Filetostr(namefile)
    		possearch=Ratc("timestamp='",gcString)
    		gcString=Left(gcString,possearch+10) + Ttoc(Datetime(),1) + Substr(gcString,possearch+25)
    		If File(namefile+".bak")
    			Delete File (namefile+".bak")
    		Endif
    		Rename (namefile) To (namefile+".bak")
    		Strtofile(gcString,namefile)
    		Fputs(loghnd, Fullpath(namefile))
    	Endif
    Endfor

    * chiamata ricorsiva alle sottocartelle
    namearray=Sys(2015)
    dirnumber = Adir(&namearray , "", "D")
    For j=1 To dirnumber
    	If Left(&namearray[j,1],1)<>'.'
    		Cd &namearray[j,1]
    		apply_utd_proc()
    		Cd ..\
    		If ANS=2
    			Return
    		Endif
    	Endif
    Endfor
    Endproc
