* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gles_bdi                                                        *
*              Generazione disco INTRA                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgles_bdi",oParentObject)
return(i_retval)

define class tgles_bdi as StdBatch
  * --- Local variables
  Trovati = .f.
  Scritti = 0
  UltimaSez = space(2)
  Handle = 0
  RecordComu = space(10)
  RecordSpec = space(10)
  TipoRiep = space(10)
  SpazioLib = 0
  Errori = .f.
  UltimoTipo = space(2)
  Messaggio = space(10)
  w_PIvaDele = space(11)
  w_NUMDIS = 0
  w_NUMELE = 0
  w_MESS = space(30)
  w_AGGIPROG = .f.
  w_Sez1RigC = 0
  w_Sez1RigA = 0
  w_Sez1ImpC = 0
  w_Sez1ImpA = 0
  w_Sez2RigC = 0
  w_Sez2RigA = 0
  w_Sez2ImpC = 0
  w_Sez2ImpA = 0
  w_AGGIORNA = space(1)
  w_DATEMU = ctod("  /  /  ")
  w_BLOCK = .f.
  w_CAMB = 0
  w_VALOR = space(3)
  w_PIDATREG = ctod("  /  /  ")
  w_DECOR = 0
  w_NOMENC = space(8)
  w_UNIMIS = space(3)
  w_ARRSUP = 0
  Gruppo = space(10)
  w_ANPARIVA = space(10)
  GruppoExpr = space(10)
  w_NAISOCLF = space(10)
  Record = 0
  w_NAISODES = space(10)
  w_PIIMPNAZ = 0
  w_NAISOPRO = space(10)
  w_PIIMPVAL = 0
  w_NAISOORI = space(10)
  w_PIMASNET = 0
  w_SPMODSPE = space(10)
  w_PIQTASUP = 0
  w_PICONDCO = space(1)
  w_PIVALSTA = 0
  w_TIPOMOV = space(2)
  w_PINOMENC = space(8)
  w_PINATTRA = space(3)
  w_TIPAC = space(1)
  w_TOTNAZ = 0
  w_IESELSTA = 0
  w_IESENAZ = 0
  w_Sez1Righe = 0
  w_Sez2Righe = 0
  w_Sez1Impor = 0
  w_Sez2Impor = 0
  SaveReco = space(10)
  w_ImpoGrup = 0
  w_Sez2Impapp = space(10)
  w_PORORI = space(10)
  w_PORDES = space(10)
  w_REGSTA = space(1)
  w_ISOORI = space(3)
  w_CODISO = space(3)
  w_PROSPA = space(2)
  w_NOMENCLA = space(0)
  w_IMPORTO = 0
  w_IMPVAL = space(18)
  w_VALSTA = space(18)
  w_SEP = space(1)
  w_oERRORLOG = .NULL.
  w_AGGCESS = 0
  w_AGGACQU = 0
  Confermato = space(10)
  OldDefault = space(10)
  NewDefault = space(10)
  * --- WorkFile variables
  VALUTE_idx=0
  AZIENDA_idx=0
  NOMENCLA_idx=0
  AZDATINT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili della maschera
    * --- Variabili locali
    this.oParentObject.w_NProgEle = iif(this.oParentObject.w_NProgEle=0,1,this.oParentObject.w_NProgEle)
    this.Trovati = .f.
    this.Scritti = 0
    this.UltimaSez = "  "
    this.Handle = -1
    this.RecordComu = ""
    this.RecordSpec = ""
    this.TipoRiep = ""
    this.SpazioLib = 0
    this.Errori = .f.
    this.UltimoTipo = "  "
    this.Messaggio = ""
    this.w_PIvaDele = space(11)
    this.w_NUMDIS = 1
    this.w_NUMELE = 0
    this.w_MESS = "Si desidera stampare l'etichetta?"
    this.w_AGGIPROG = .F.
    this.w_Sez1RigC = 0
    this.w_Sez1RigA = 0
    this.w_Sez1ImpC = 0
    this.w_Sez1ImpA = 0
    this.w_Sez2RigC = 0
    this.w_Sez2RigA = 0
    this.w_Sez2ImpC = 0
    this.w_Sez2ImpA = 0
    this.w_AGGIORNA = "N"
    this.w_BLOCK = .F.
    this.w_ARRSUP = IIF(this.oParentObject.w_VALUTA = g_PERVAL, 0, 0.00000001)
    * --- Controllo selezioni obbligatorie
    if this.oParentObject.w_TIPOGENE<>"C" AND this.oParentObject.w_TIPOACQU="T" AND this.oParentObject.w_ANNORIFE>="2003"
      ah_errormsg("Gli elenchi relativi agli acquisti non possono avere periodicit� trimestrale",48)
      i_retcode = 'stop'
      return
    endif
    if empty(this.oParentObject.w_PERIODO)
      ah_errormsg("Specificare il periodo di riferimento",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPOGENE<>"A" and !(this.oParentObject.w_Periodo=1 or (this.oParentObject.w_Periodo<5 and this.oParentObject.w_TipoCess="T") or (this.oParentObject.w_Periodo<13 and this.oParentObject.w_TipoCess="M"))
      ah_errormsg("Il periodo di riferimento delle cessioni non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_TIPOGENE<>"C" and !(this.oParentObject.w_Periodo=1 or (this.oParentObject.w_Periodo<5 and this.oParentObject.w_TipoAcqu="T") or (this.oParentObject.w_Periodo<13 and this.oParentObject.w_TipoAcqu="M"))
      ah_errormsg("Il periodo di riferimento degli acquisti non � valido",48)
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_AZINPECE>=this.oParentObject.w_PERIODO AND (this.oParentObject.w_TIPOGENE="E" OR this.oParentObject.w_TIPOGENE="C")
      if not ah_YesNo("Periodo cessioni gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_AZINPEAC>=this.oParentObject.w_PERIODO AND (this.oParentObject.w_TIPOGENE="E" OR this.oParentObject.w_TIPOGENE="A")
      if not ah_YesNo("Periodo acquisti gi� generato. Si desidera procedere?")
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_SEP = ";"
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Scrittura file ascii
    * --- Creazione file e verifica supporto
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.Handle > -1
      * --- Crea un cursore temporaneo ordinato per tipo operazione (Ordine: AC - RA - CE -RE)
      vq_exec("query\GLES_BDI",this,"EleIntra")
      * --- Le rettifiche nello stesso periodo sono da considerare Acquisti/Cessioni (se in meno, in negativo)
      SELECT IIF(PITIPMOV $ "AC-CE", LEFT(PITIPMOV, 1), IIF(PITIPMOV="RC", "C", "A")) AS SEZIONE, ;
      IIF(PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), IIF(PITIPMOV="RA", "AC", "CE"), PITIPMOV) AS PITIPMOV, ;
      IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO)), "    ", PIANNRET) AS PIANNRET, ;
      IIF(PITIPMOV $ "AC-CE" OR (PITIPMOV $ "RC-RA" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO)), 99*0, PIPERRET) AS PIPERRET, ;
      ANPARIVA,PINUMREG,PIDATREG,PINATTRA, PINOMENC, IIF(PICONDCO="N", " ", PICONDCO) AS PICONDCO, NAISOPRO,NAISOORI, NAISODES, PIPROORI, PIPRODES,PISERIAL, SPMODSPE, ;
      PIIMPNAZ * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), -1, 1) AS PIIMPNAZ, ;
      PIIMPVAL * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), -1, 1) AS PIIMPVAL, ;
      PIMASNET * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), -1, 1) AS PIMASNET, ;
      PIQTASUP * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), -1, 1) AS PIQTASUP, ;
      PIVALSTA * IIF(PITIPMOV $ "RC-RA" AND NVL(PITIPRET," ")="-" AND NVL(PIANNRET,"    ")=this.oParentObject.w_ANNORIFE AND PIPERRET=IIF(PITIPMOV="RA", this.oParentObject.w_PERIODO, this.oParentObject.w_PERIODO), -1, 1) AS PIVALSTA, ;
      PIPORORI AS PIPORORI, PIPORDES AS PIPORDES, TTREGSTA AS TTREGSTA;
      FROM EleIntra ;
      INTO CURSOR EleIntra order by 1, 2, 3, 4, 5, 8, 9, 10, 17, 11, 13, 12, 14,15,23,24,25 
      SELECT EleIntra
      GO TOP
      * --- Scrittura righe di dettaglio
      do while .not. eof()
        this.UltimaSez = Sezione
        * --- Quando cambia il tipo movimento azzera il contatore di riga
        if PITIPMOV <> this.UltimoTipo
          this.UltimoTipo = PITIPMOV
        endif
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT,VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALOR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT,VACAOVAL;
            from (i_cTable) where;
                VACODVAL = this.w_VALOR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECOR = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          this.w_CAMB = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Impostazione della variabile che indica se sono stati trovati dati da scrivere
        this.Trovati = .T.
        select EleIntra
        skip
      enddo
    endif
    this.Handle = fclose( this.Handle )
    if .not. this.Errori
      if this.Trovati
        if this.w_BLOCK
          ah_errormsg("Generazione abortita",48)
          ERASE (this.oParentObject.w_NomeFile)
        else
          ah_errormsg("Generazione terminata",48)
          this.w_AGGIPROG = .T.
        endif
      else
        ah_errormsg("Non esistono dati da elaborare",48)
        ERASE (this.oParentObject.w_NomeFile)
      endif
    else
      ah_errormsg("Generazione incompleta. Ripetere l'operazione",48)
    endif
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
    if this.w_AGGIPROG
      if ah_yesno("Si desidera aggiornare il progressivo disco e i progressivi periodo?")
        do case
          case this.oParentObject.w_TIPOGENE="C"
            this.oParentObject.w_PERIODO = IIF(this.oParentObject.w_TipoCess="A" or (this.oParentObject.w_PERIODO=4 and this.oParentObject.w_TipoCess="T") or (this.oParentObject.w_PERIODO=12 and this.oParentObject.w_TipoCess="M"),0,this.oParentObject.w_PERIODO)
            this.w_AGGCESS = this.oParentObject.w_PERIODO
            this.w_AGGACQU = this.oParentObject.w_AZINPEAC
          case this.oParentObject.w_TIPOGENE="A" 
            this.oParentObject.w_PERIODO = IIF(this.oParentObject.w_TipoAcqu="A" or (this.oParentObject.w_PERIODO=4 and this.oParentObject.w_TipoAcqu="T") or (this.oParentObject.w_PERIODO=12 and this.oParentObject.w_TipoAcqu="M"),0,this.oParentObject.w_PERIODO)
            this.w_AGGCESS = this.oParentObject.w_AZINPECE
            this.w_AGGACQU = this.oParentObject.w_PERIODO
            * --- Calcolo Ultima Data generazione Elenchi (da scrivere nei dati Azienda)
          case this.oParentObject.w_TIPOGENE="E" 
            this.oParentObject.w_PERIODO = IIF(this.oParentObject.w_TipoCess="A" or (this.oParentObject.w_PERIODO=4 and this.oParentObject.w_TipoCess="T") or (this.oParentObject.w_PERIODO=12 and this.oParentObject.w_TipoCess="M"),0,this.oParentObject.w_PERIODO)
            this.oParentObject.w_PERIODO = IIF(this.oParentObject.w_TipoAcqu="A" or (this.oParentObject.w_PERIODO=4 and this.oParentObject.w_TipoAcqu="T") or (this.oParentObject.w_PERIODO=12 and this.oParentObject.w_TipoAcqu="M"),0,this.oParentObject.w_PERIODO)
            this.w_AGGCESS = this.oParentObject.w_PERIODO
            this.w_AGGACQU = this.oParentObject.w_PERIODO
        endcase
        this.w_AGGIORNA = "S"
        this.oParentObject.w_NProgEle = this.oParentObject.w_NProgEle + 1
        * --- Write into AZDATINT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZDATINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZDATINT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZDATINT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ITINPEAC ="+cp_NullLink(cp_ToStrODBC(this.w_AGGACQU),'AZDATINT','ITINPEAC');
          +",ITINPECE ="+cp_NullLink(cp_ToStrODBC(this.w_AGGCESS),'AZDATINT','ITINPECE');
              +i_ccchkf ;
          +" where ";
              +"ITCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              ITINPEAC = this.w_AGGACQU;
              ,ITINPECE = this.w_AGGCESS;
              &i_ccchkf. ;
           where;
              ITCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZNPRGEL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NProgEle),'AZIENDA','AZNPRGEL');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZNPRGEL = this.oParentObject.w_NProgEle;
              &i_ccchkf. ;
           where;
              AZCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        AH_ERRORMSG("Aggiornamento terminato",48)
      endif
    endif
    * --- Chiusura Cursore
    if used( "ERROR" )
      select ERROR
      use
    endif
    if used( "ELEINTRA" )
      select eleintra
      use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione File Ascii
    * --- Variabili
    this.Confermato = .T.
    * --- Richiesta azzeramento file esistente
    if file( this.oParentObject.w_NomeFile )
      if NOT AH_YESNO("File esistente. Ricreo?")
        this.Confermato = .F.
        this.w_NUMDIS = this.w_NUMDIS+1
      endif
    endif
    * --- Creazione file ascii
    if this.Confermato
      this.Handle = fcreate( this.oParentObject.w_NomeFile, 0 )
      if this.Handle < 0
        ah_errormsg("Impossibile creare il file %1",48,"",this.oParentObject.w_NomeFile)
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Dettaglio
    * --- Gestione dei campi che possono assumere il valore NULL
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(3) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(3) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    this.w_SPMODSPE = iif( isnull( SPMODSPE ), space(2) , alltrim(SPMODSPE) )
    this.w_PICONDCO = IIF(EMPTY(NVL(PICONDCO," ")) OR PICONDCO="N", SPACE(1), ALLTRIM(PICONDCO))
    do case
      case this.w_PICONDCO="1"
        this.w_PICONDCO = "EXW"
      case this.w_PICONDCO="2"
        this.w_PICONDCO = "FCA"
      case this.w_PICONDCO="3"
        this.w_PICONDCO = "FAS"
      case this.w_PICONDCO="4"
        this.w_PICONDCO = "FOB"
      case this.w_PICONDCO="5"
        this.w_PICONDCO = "CFR"
      case this.w_PICONDCO="6"
        this.w_PICONDCO = "CIF"
      case this.w_PICONDCO="7"
        this.w_PICONDCO = "CPT"
      case this.w_PICONDCO="8"
        this.w_PICONDCO = "CIP"
      case this.w_PICONDCO="9"
        this.w_PICONDCO = "DAF"
      case this.w_PICONDCO="W"
        this.w_PICONDCO = "DES"
      case this.w_PICONDCO="Y"
        this.w_PICONDCO = "DEQ"
      case this.w_PICONDCO="Z"
        this.w_PICONDCO = "DDU"
      case this.w_PICONDCO="K"
        this.w_PICONDCO = "DDP"
      case this.w_PICONDCO="X"
        this.w_PICONDCO = "XXX"
    endcase
    this.w_REGSTA = NVL(TTREGSTA," ")
    * --- Se il periodo � annuale la nomenclatura non viene riportata (il campo � ripempito con 8 zeri)
    this.w_PINOMENC = alltrim( NVL(PINOMENC,SPACE(8)))
    * --- Dichiarazione Mensile, Trimestrale, Annuale e Rettifiche
    this.w_PINATTRA = alltrim( NVL(PINATTRA,SPACE(3)))
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se inferiori all'unita' mette 1
    this.w_PIQTASUP = IIF(cp_ROUND(this.w_PIQTASUP, 0)<1 AND this.w_PIQTASUP<>0, 1, cp_ROUND(this.w_PIQTASUP, 0))
    this.w_PIMASNET = IIF(cp_ROUND(this.w_PIMASNET, 0)<1 AND this.w_PIMASNET<>0, 1, cp_ROUND(this.w_PIMASNET, 0))
    if this.oParentObject.w_VALUTA = g_PERVAL
      this.w_PIIMPNAZ = cp_ROUND(this.w_PIIMPNAZ, 2)
      this.w_PIIMPVAL = cp_ROUND(this.w_PIIMPVAL, 2)
      this.w_PIVALSTA = cp_ROUND(this.w_PIVALSTA, 2)
    endif
    * --- Raggruppamento righe
    * --- Non � stato gestito a livello di query perch�:
    * --- 1) Varia in base alla periodicit� di presentazione degli elenchi.
    * --- 2) Un'azienda pu� avere periodicit� differenti fra acquisti e cessioni.
    * --- 3) Le rettifiche devono essere raggruppate
    * --- Controllo cambio fisso
    if PITIPMOV $ "AC-RA" and this.w_PIIMPVAL <> 0 and this.w_CAMB<>0
      this.w_PIIMPVAL = 0
    endif
    * --- Composizione record (parte specifica)
    this.RecordSpec = ""
    if this.UltimaSez = "A"
      this.w_CODISO = left(NVL(this.w_NAISOPRO,SPACE(3))+space(3), 3)
      this.w_PROSPA = left( NVL(PIPRODES,SPACE(2))+space(2), 2)
      if this.w_SPMODSPE="1" OR this.w_SPMODSPE="4"
        this.w_PORDES = ALLTRIM(NVL(PIPORDES, SPACE(10)))
      else
        this.w_PORDES = SPACE(10)
      endif
      this.w_ISOORI = ALLTRIM(NVL(this.w_NAISOORI,SPACE(3)))
      this.w_IMPVAL = right( repl("0",13) + alltrim(str( cp_round((this.w_PIIMPVAL)-this.w_ARRSUP, 0) ,13,2)) , 13)
    else
      this.w_CODISO = left(NVL(this.w_NAISODES,SPACE(3))+space(3), 3)
      this.w_PROSPA = left( NVL(PIPROORI,SPACE(2))+space(2), 2)
      if this.w_SPMODSPE="1" OR this.w_SPMODSPE="4"
        this.w_PORDES = ALLTRIM(NVL(PIPORORI, SPACE(10)))
      else
        this.w_PORDES = SPACE(10)
      endif
      this.w_IMPVAL = SPACE(18)
    endif
    if this.oParentObject.w_TIPOACQU="A" and PITIPMOV = "AC" or this.oParentObject.w_TIPOCESS="A" and PITIPMOV = "CE"
      this.w_NOMENCLA = "00000000"
      this.w_PINOMENC = SPACE(8)
    else
      this.w_NOMENCLA = right( repl("0",8) + alltrim( NVL(PINOMENC,SPACE(8))) , 8)
    endif
    if this.oParentObject.w_VALUTA=g_PERVAL
      this.w_IMPORTO = right( repl("0",13) + alltrim(str( cp_round((this.w_PIIMPNAZ)-this.w_ARRSUP,2),13,2)) , 13)
      this.w_VALSTA = right( repl("0",13) + alltrim(str( cp_round((this.w_PIVALSTA)-this.w_ARRSUP,2),13,2)) , 13)
    else
      this.w_IMPORTO = right( repl("0",13) + alltrim(str( cp_round((this.w_PIIMPNAZ/1000)-this.w_ARRSUP,0),13,0)) , 13)
      this.w_VALSTA = right( repl("0",13) + alltrim(str( cp_round((this.w_PIVALSTA/1000)-this.w_ARRSUP,0),13,0)) , 13)
    endif
    * --- Controllo su Natura Transazione
    if EMPTY(this.w_PINATTRA)
      this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: natura della transazione non trovata - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
      SELECT EleIntra
    endif
    if !empty (NVL(PINOMENC,SPACE(8))) and ((this.oParentObject.w_TIPOACQU = "M" and PITIPMOV = "AC") or (this.oParentObject.w_TIPOCESS = "M" and PITIPMOV = "CE"))
      this.w_NOMENC = NVL(PINOMENC,SPACE(8))
      * --- Read from NOMENCLA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.NOMENCLA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2],.t.,this.NOMENCLA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NMUNISUP"+;
          " from "+i_cTable+" NOMENCLA where ";
              +"NMCODICE = "+cp_ToStrODBC(this.w_NOMENC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NMUNISUP;
          from (i_cTable) where;
              NMCODICE = this.w_NOMENC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UNIMIS = NVL(cp_ToDate(_read_.NMUNISUP),cp_NullValue(_read_.NMUNISUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PIQTASUP=0 and !EMPTY(this.w_UNIMIS) and this.w_UNIMIS<>"ZZZ"
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: quantit� relativa all'unit� di misura supplementare uguale a zero - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
    endif
    * --- Controllo sul codice ISO dello stato di provenienza/destinazione della merce
    if (this.UltimaSez="C" .and. this.oParentObject.w_TipoCess="M") .or. (this.UltimaSez="A" .and. this.oParentObject.w_TipoAcqu="M")
      * --- Controllo su Nomenclatura
      if EMPTY(this.w_PINOMENC) 
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice nomenclatura non trovato - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
      if EMPTY(this.w_CODISO) 
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice ISO mancante. - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
      * --- Controllo sulla provincia spagnola di provenienza/destinazione
      if EMPTY(this.w_PROSPA)
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: provincia di provenienza/destinazione mancante. - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
      * --- Controllo sull massa netta
      if this.w_PIMASNET=0
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: massa netta uguale a 0. - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
    endif
    * --- Controllo sul porto/areoporto di provenienza/destinazione
    if EMPTY(this.w_PORDES) AND (this.w_SPMODSPE="1" OR this.w_SPMODSPE="4")
      this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: porto/aeroporto di provenienza/destinazione mancante. - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
      SELECT EleIntra
    endif
    * --- Controllo sul regime statistico
    if EMPTY(this.w_REGSTA)
      this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: regime statistico mancante. - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
      SELECT EleIntra
    endif
    * --- Controllo sull'importo
    if PITIPMOV $ "AC-CE" and (this.w_PIIMPNAZ < 0 or this.w_PIVALSTA < 0)
      this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: � presente almeno un importo negativo",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
      SELECT EleIntra
      this.w_BLOCK = .T.
    endif
    * --- Dichiarazione Mensile
    if (this.UltimaSez="C" AND this.oParentObject.w_TipoCess="M") OR (this.UltimaSez="A" AND this.oParentObject.w_TipoAcqu="M")
      * --- Valore Statistico : soltanto messaggio di warning
      if this.w_PIVALSTA=0 AND ((this.UltimaSez="C" AND this.oParentObject.w_DTOBBL $ "CE") OR (this.UltimaSez="A" AND this.oParentObject.w_DTOBBL $ "AE")) 
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: valore statistico uguale a zero - WARNING",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
      endif
      * --- Verifica che la periodicit� della rettifica acquisti con competenza maggiore o uguale al 2003 non sia trimestrale
      if NVL(PITIPMOV,SPACE(2))="RA" AND (NVL(PIANNRET,SPACE(4))>="2003" AND NOT EMPTY(NVL(PIANNRET,SPACE(4)))) AND NVL(PITIPPER," " )="T" 
        this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: periodicit� trimestrale per gli acquisti non utilizzabile",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
        SELECT EleIntra
        this.w_BLOCK = .T.
      endif
      if .not. (PITIPMOV $ "RC-RA")
        * --- Se non e' una rettifica -  Controllo dati obbligatori
        if EMPTY(NVL(this.w_PICONDCO,"")) and ((this.UltimaSez="C" and this.oParentObject.w_DTOBBL $ "CE") or (this.UltimaSez="A" and this.oParentObject.w_DTOBBL $ "AE")) 
          * --- Condizione di consegna
          this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice condizione di consegna non trovato",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
          SELECT EleIntra
          this.w_BLOCK = .T.
        endif
        if empty(this.w_SPMODSPE) and ((this.UltimaSez="C" and this.oParentObject.w_DTOBBL $ "CE") or (this.UltimaSez="A" and this.oParentObject.w_DTOBBL $ "AE"))
          * --- Modalit� di trasporto
          this.w_oERRORLOG.AddMsgLog("Reg. %1 del %2: codice modalit� di trasporto non trovato",ltrim(str(NVL(PINUMREG,0),6,0)),dtoc(NVL(CP_TODATE(PIDATREG),cp_CharToDate("  -  -  "))))     
          SELECT EleIntra
          this.w_BLOCK = .T.
        endif
      endif
    endif
    this.RecordSpec = this.RecordSpec+ ALLTRIM(this.w_CODISO)+ this.w_SEP + ALLTRIM(this.w_PROSPA) + this.w_SEP+ ALLTRIM(this.w_PICONDCO) + this.w_SEP + right( "  " + alltrim( NVL(PINATTRA,SPACE(3))) , 2) + this.w_SEP 
    this.RecordSpec = this.RecordSpec + right( "0"+alltrim(this.w_SPMODSPE), 1) + this.w_SEP+ ALLTRIM(this.w_PORDES) + this.w_SEP+ ALLTRIM(this.w_NOMENCLA)+ this.w_SEP + ALLTRIM(this.w_ISOORI)+ this.w_SEP+ ALLTRIM(this.w_REGSTA)+this.w_SEP
    this.RecordSpec = this.RecordSpec+ ALLTRIM(STR(this.w_PIMASNET))+this.w_SEP+ ALLTRIM(STR(this.w_PIQTASUP)) + this.w_SEP+ ALLTRIM(this.w_IMPORTO)+this.w_SEP+ ALLTRIM(this.w_VALSTA)
    * --- Scrittura
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Scritti = fputs( this.Handle, this.RecordSpec )
    * --- Verifica
    if this.Scritti <> len(this.RecordSpec)+2
      ah_errormsg("Attenzione: verificare record dettaglio sezione %1 numero %2",48,"",this.UltimaSez,str(NumRiga,5,0))
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica spazio su disco
    * --- Il controllo dello spazio libero viene realizzato solo se l'unita' e' stata mappata.
    if substr(this.oParentObject.w_NomeFile,2,1)=":"
      * --- Memorizza la posizione corrente
      this.OldDefault = sys(5)+sys(2003)
      * --- Si posiziona sul disco di destinazione
      this.NewDefault = left(this.oParentObject.w_NomeFile,2)
      set default to (this.NewDefault)
      this.SpazioLib = diskspace()
      * --- Cicla fino a quando viene selezionato un device con spazio sufficiente
      this.SpazioLib = 0
      do while this.SpazioLib <= 10000
        * --- Legge lo spazio libero
        this.SpazioLib = diskspace()
        * --- Richiede la selezione di un nuovo disco
        if this.SpazioLib <= 10000
          * --- Chiusura File Ascii su disco corrente
          this.Handle = fclose( this.Handle )
          * --- Richiesta nuovo disco
          if .not. AH_YESNO("Spazio su disco esaurito.%0Inserire un nuovo disco formattato.%0Proseguo?")
            this.Errori = .T.
            i_retcode = 'stop'
            return
          else
            * --- Creazione File Ascii sul nuovo disco
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      enddo
      * --- Torna nella posizione di partenza
      set default to (this.OldDefault)
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.Gruppo = ""
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Azzero le variabili
    STORE 0 TO this.w_PIIMPNAZ,this.w_PIIMPVAL,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA,this.w_IESENAZ,this.w_IESELSTA
    this.w_TIPAC = PITIPMOV
    * --- Raggruppamento
    this.Record = recno()
    do while (.not. eof()) .and. this.Gruppo = eval( this.GruppoExpr ) and this.w_TIPAC=PITIPMOV
      * --- Totalizza i dati del record corrente
      this.Record = recno()
      this.w_PIIMPNAZ = this.w_PIIMPNAZ + NVL(PIIMPNAZ, 0)
      this.w_PIVALSTA = this.w_PIVALSTA + NVL(PIVALSTA, 0)
      this.w_PIIMPVAL = this.w_PIIMPVAL + NVL(PIIMPVAL, 0)
      this.w_PIMASNET = this.w_PIMASNET + NVL(PIMASNET, 0)
      this.w_PIQTASUP = this.w_PIQTASUP + NVL(PIQTASUP, 0)
      skip
      this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
      this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
      this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
      this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
    enddo
    * --- Si posiziona sull'ultimo record che apparteneva all'intervallo di raggruppamento
    goto this.Record
    this.w_ANPARIVA = iif( isnull( ANPARIVA ), space(12) , alltrim(ANPARIVA) )
    this.w_NAISODES = iif( isnull( NAISODES ), space(2) , alltrim(NAISODES) )
    this.w_NAISOPRO = iif( isnull( NAISOPRO ), space(2) , alltrim(NAISOPRO) )
    this.w_NAISOORI = iif( isnull( NAISOORI ), space(2) , alltrim(NAISOORI) )
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Stringa di raggruppamento
    if PITIPMOV $ "CE-RC"
      if this.oParentObject.w_TIPOCESS = "M"
        this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
        this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISODES+NVL(PIPROORI,SPACE(2)) +NVL(PIPORORI, SPACE(10)) + NVL(TTREGSTA," ")
        this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
        this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
        this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
        this.GruppoExpr = this.GruppoExpr+"+this.w_NAISODES+NVL(PIPROORI,SPACE(2))+NVL(PIPORORI, SPACE(10))+NVL(TTREGSTA,' ')"
        this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
      else
        * --- Cessioni trimestrali o annuali (se annuali il codice nomenclatura viene eliminato dal raggruppamento)
        if this.oParentObject.w_TIPOCESS="A"
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+ SPACE(8) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+ SPACE(8)"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="CE"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='CE',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      endif
    else
      * --- Acquisti
      if this.oParentObject.w_TIPOACQU = "M"
        this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))+NVL(PICONDCO,SPACE(1))
        this.Gruppo = this.Gruppo+NVL(SPMODSPE,SPACE(2))+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2)) + NVL(PIPORDES, SPACE(10))+ NVL(TTREGSTA," ")
        this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
        this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
        this.GruppoExpr = this.GruppoExpr+"+NVL(PICONDCO,SPACE(1))+NVL(SPMODSPE,SPACE(2))"
        this.GruppoExpr = this.GruppoExpr+"+this.w_NAISOPRO+this.w_NAISOORI+NVL(PIPRODES,SPACE(2))+ NVL(PIPORDES, SPACE(10))+NVL(TTREGSTA,' ')"
        this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
      else
        * --- Acquisti trimestrali o annuali (se annuali il codice nomenclatura viene eliminato dal raggruppamento)
        if this.oParentObject.w_TIPOACQU="A"
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+ SPACE(8) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3)) + SPACE(8)"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        else
          this.Gruppo = this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8)) 
          this.Gruppo = this.Gruppo+IIF(PITIPMOV="AC"," ", NVL(PITIPRET," ") + NVL(PIANNRET," ") + STR(NVL(PIPERRET,0)))
          this.GruppoExpr = "this.w_ANPARIVA+NVL(PINATTRA,SPACE(3))+NVL(PINOMENC,SPACE(8))"
          this.GruppoExpr = this.GruppoExpr+"+IIF(PITIPMOV='AC',' ', NVL(PITIPRET,' ') + NVL(PIANNRET,' ') + STR(NVL(PIPERRET,0)))"
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='NOMENCLA'
    this.cWorkTables[4]='AZDATINT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
