* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gles_kls                                                        *
*              Generazione disco INTRA                                         *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_36]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgles_kls",oParentObject))

* --- Class definition
define class tgles_kls as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 493
  Height = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-02"
  HelpContextID=216622041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  AZDATINT_IDX = 0
  cPrg = "gles_kls"
  cComment = "Generazione disco INTRA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ReadAzie = space(5)
  w_ReadAzieDatInt = space(5)
  w_TIPOGENE = space(1)
  o_TIPOGENE = space(1)
  w_ANNORIFE = space(4)
  w_TIPOCESS = space(1)
  o_TIPOCESS = space(1)
  w_TIPOACQU = space(1)
  o_TIPOACQU = space(1)
  w_TIPO = space(10)
  w_NOMEFILE = space(40)
  w_NProgEle = 0
  w_AZINPECE = 0
  w_AZINPEAC = 0
  w_AZAICFPI = space(12)
  w_DECIM = 0
  w_AZNPRGEL = 0
  w_AZLOCALI = space(30)
  w_DTOBBL = space(1)
  w_TIPOGEN = space(1)
  w_PERIODO = 0
  o_PERIODO = 0
  w_VALUTA = space(3)
  w_MESINI = 0
  w_MESFIN = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgles_klsPag1","gles_kls",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOGENE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='AZDATINT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ReadAzie=space(5)
      .w_ReadAzieDatInt=space(5)
      .w_TIPOGENE=space(1)
      .w_ANNORIFE=space(4)
      .w_TIPOCESS=space(1)
      .w_TIPOACQU=space(1)
      .w_TIPO=space(10)
      .w_NOMEFILE=space(40)
      .w_NProgEle=0
      .w_AZINPECE=0
      .w_AZINPEAC=0
      .w_AZAICFPI=space(12)
      .w_DECIM=0
      .w_AZNPRGEL=0
      .w_AZLOCALI=space(30)
      .w_DTOBBL=space(1)
      .w_TIPOGEN=space(1)
      .w_PERIODO=0
      .w_VALUTA=space(3)
      .w_MESINI=0
      .w_MESFIN=0
        .w_ReadAzie = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ReadAzie))
          .link_1_1('Full')
        endif
        .w_ReadAzieDatInt = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ReadAzieDatInt))
          .link_1_2('Full')
        endif
        .w_TIPOGENE = 'C'
        .w_ANNORIFE = str(year(i_datsys),4,0)
          .DoRTCalc(5,6,.f.)
        .w_TIPO = IIF(.w_TIPOGENE='C',.w_TIPOCESS,.w_TIPOACQU)
        .w_NOMEFILE = left(sys(5)+sys(2003)+"\SCAMBI.CEE"+space(40),40)
        .w_NProgEle = IIF(.w_AZNPRGEL=0,1,.w_AZNPRGEL)
          .DoRTCalc(10,16,.f.)
        .w_TIPOGEN = IIF(.w_TIPOGENE='C',.w_TIPOCESS, .w_TIPOACQU)
        .w_PERIODO = IIF(.w_TIPOGENE='C',.w_AZINPECE+1,.w_AZINPEAC+1)
        .w_VALUTA = g_PERVAL
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_VALUTA))
          .link_1_34('Full')
        endif
        .w_MESINI = iif( .w_TIPO="A", 1, iif( .w_TIPO="M", .w_PERIODO, (.w_PERIODO*3)-2 ))
        .w_MESFIN = iif( .w_TIPO="A", 12, iif( .w_TIPO="M", .w_PERIODO, (.w_PERIODO*3) ))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,6,.t.)
        if .o_TIPOGENE<>.w_TIPOGENE.or. .o_TIPOCESS<>.w_TIPOCESS.or. .o_TIPOACQU<>.w_TIPOACQU
            .w_TIPO = IIF(.w_TIPOGENE='C',.w_TIPOCESS,.w_TIPOACQU)
        endif
        .DoRTCalc(8,16,.t.)
        if .o_TIPOGENE<>.w_TIPOGENE.or. .o_TIPOACQU<>.w_TIPOACQU.or. .o_TIPOCESS<>.w_TIPOCESS
            .w_TIPOGEN = IIF(.w_TIPOGENE='C',.w_TIPOCESS, .w_TIPOACQU)
        endif
        if .o_TIPOGENE<>.w_TIPOGENE
            .w_PERIODO = IIF(.w_TIPOGENE='C',.w_AZINPECE+1,.w_AZINPEAC+1)
        endif
            .w_VALUTA = g_PERVAL
          .link_1_34('Full')
        if .o_PERIODO<>.w_PERIODO
            .w_MESINI = iif( .w_TIPO="A", 1, iif( .w_TIPO="M", .w_PERIODO, (.w_PERIODO*3)-2 ))
        endif
        if .o_PERIODO<>.w_PERIODO
            .w_MESFIN = iif( .w_TIPO="A", 12, iif( .w_TIPO="M", .w_PERIODO, (.w_PERIODO*3) ))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadAzie
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzie) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzie)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZAICFPI,AZNPRGEL,AZLOCAZI,AZDTOBBL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_ReadAzie);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_ReadAzie)
            select AZCODAZI,AZAICFPI,AZNPRGEL,AZLOCAZI,AZDTOBBL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzie = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZAICFPI = NVL(_Link_.AZAICFPI,space(12))
      this.w_AZNPRGEL = NVL(_Link_.AZNPRGEL,0)
      this.w_AZLOCALI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_DTOBBL = NVL(_Link_.AZDTOBBL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzie = space(5)
      endif
      this.w_AZAICFPI = space(12)
      this.w_AZNPRGEL = 0
      this.w_AZLOCALI = space(30)
      this.w_DTOBBL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzie Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ReadAzieDatInt
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZDATINT_IDX,3]
    i_lTable = "AZDATINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2], .t., this.AZDATINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzieDatInt) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzieDatInt)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODAZI,ITAITIPE,ITAITIPV,ITINPECE,ITINPEAC";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODAZI="+cp_ToStrODBC(this.w_ReadAzieDatInt);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODAZI',this.w_ReadAzieDatInt)
            select ITCODAZI,ITAITIPE,ITAITIPV,ITINPECE,ITINPEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzieDatInt = NVL(_Link_.ITCODAZI,space(5))
      this.w_TIPOCESS = NVL(_Link_.ITAITIPE,space(1))
      this.w_TIPOACQU = NVL(_Link_.ITAITIPV,space(1))
      this.w_AZINPECE = NVL(_Link_.ITINPECE,0)
      this.w_AZINPEAC = NVL(_Link_.ITINPEAC,0)
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzieDatInt = space(5)
      endif
      this.w_TIPOCESS = space(1)
      this.w_TIPOACQU = space(1)
      this.w_AZINPECE = 0
      this.w_AZINPEAC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])+'\'+cp_ToStr(_Link_.ITCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZDATINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzieDatInt Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIM = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECIM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOGENE_1_3.RadioValue()==this.w_TIPOGENE)
      this.oPgFrm.Page1.oPag.oTIPOGENE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNORIFE_1_10.value==this.w_ANNORIFE)
      this.oPgFrm.Page1.oPag.oANNORIFE_1_10.value=this.w_ANNORIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_17.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_17.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oNProgEle_1_19.value==this.w_NProgEle)
      this.oPgFrm.Page1.oPag.oNProgEle_1_19.value=this.w_NProgEle
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOGEN_1_32.RadioValue()==this.w_TIPOGEN)
      this.oPgFrm.Page1.oPag.oTIPOGEN_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_33.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_33.value=this.w_PERIODO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANNORIFE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNORIFE_1_10.SetFocus()
            i_bnoObbl = !empty(.w_ANNORIFE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOMEFILE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_17.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NProgEle))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNProgEle_1_19.SetFocus()
            i_bnoObbl = !empty(.w_NProgEle)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIODO)) or not(.w_PERIODO=1.or.(.w_PERIODO<5.and..w_TIPO="T").or.(.w_PERIODO<13.and..w_TIPO="M")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIODO_1_33.SetFocus()
            i_bnoObbl = !empty(.w_PERIODO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOGENE = this.w_TIPOGENE
    this.o_TIPOCESS = this.w_TIPOCESS
    this.o_TIPOACQU = this.w_TIPOACQU
    this.o_PERIODO = this.w_PERIODO
    return

enddefine

* --- Define pages as container
define class tgles_klsPag1 as StdContainer
  Width  = 489
  height = 212
  stdWidth  = 489
  stdheight = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOGENE_1_3 as StdRadio with uid="WTAXYMMMUQ",rtseq=3,rtrep=.f.,left=13, top=31, width=169,height=36;
    , cFormVar="w_TIPOGENE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTIPOGENE_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Cessioni UE (INTRA 1)"
      this.Buttons(1).HelpContextID = 52761845
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Acquisti UE (INTRA 2)"
      this.Buttons(2).HelpContextID = 52761845
      this.Buttons(2).Top=17
      this.SetAll("Width",167)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTIPOGENE_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTIPOGENE_1_3.GetRadio()
    this.Parent.oContained.w_TIPOGENE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOGENE_1_3.SetRadio()
    this.Parent.oContained.w_TIPOGENE=trim(this.Parent.oContained.w_TIPOGENE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOGENE=='C',1,;
      iif(this.Parent.oContained.w_TIPOGENE=='A',2,;
      0))
  endfunc

  add object oANNORIFE_1_10 as StdField with uid="JPHLTXUKOY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANNORIFE", cQueryName = "ANNORIFE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 25874139,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=235, Top=27, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oNOMEFILE_1_17 as StdField with uid="GDRPKGIZEW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 255803221,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=115, Top=124, InputMask=replicate('X',40)


  add object oBtn_1_18 as StdButton with uid="CAKWZHWZEA",left=403, top=124, width=22,height=21,;
    caption="...", nPag=1;
    , HelpContextID = 216421018;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .w_NOMEFILE=left(Cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+"SCAMBI.CEE"+space(40),40)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNProgEle_1_19 as StdField with uid="RPGEKYDXDM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NProgEle", cQueryName = "NProgEle",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo dischetto",;
    HelpContextID = 16969269,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=115, Top=154, cSayPict='"999999"', cGetPict='"999999"'


  add object oTIPOGEN_1_32 as StdCombo with uid="WPWTKXDLJT",rtseq=17,rtrep=.f.,left=193,top=73,width=140,height=21, enabled=.f.;
    , HelpContextID = 52761914;
    , cFormVar="w_TIPOGEN",RowSource=""+"Mensile,"+"Trimestrale,"+"Annuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOGEN_1_32.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oTIPOGEN_1_32.GetRadio()
    this.Parent.oContained.w_TIPOGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPOGEN_1_32.SetRadio()
    this.Parent.oContained.w_TIPOGEN=trim(this.Parent.oContained.w_TIPOGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOGEN=='M',1,;
      iif(this.Parent.oContained.w_TIPOGEN=='T',2,;
      iif(this.Parent.oContained.w_TIPOGEN=='A',3,;
      0)))
  endfunc

  add object oPERIODO_1_33 as StdField with uid="GBVQCADMDC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 61536634,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=353, Top=73, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERIODO_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PERIODO=1.or.(.w_PERIODO<5.and..w_TIPO="T").or.(.w_PERIODO<13.and..w_TIPO="M"))
    endwith
    return bRes
  endfunc


  add object oBtn_1_37 as StdButton with uid="ZMIZZNTUDJ",left=377, top=158, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il disco INTRA";
    , HelpContextID = 216593290;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        do GLES_BDI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="JWIBHYOYEX",left=427, top=158, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 209304618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="HGRSVJGZQO",Visible=.t., Left=11, Top=7,;
    Alignment=2, Width=171, Height=15,;
    Caption="Opzioni di generazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="JOPNWTQRFX",Visible=.t., Left=190, Top=7,;
    Alignment=2, Width=288, Height=15,;
    Caption="Periodo di riferimento e dati di presentazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="MVZOTOFYWQ",Visible=.t., Left=192, Top=28,;
    Alignment=1, Width=41, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KOQZOJPYCJ",Visible=.t., Left=193, Top=52,;
    Alignment=0, Width=140, Height=18,;
    Caption="Modello INTRA 1"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="KYRJTUBUTL",Visible=.t., Left=344, Top=55,;
    Alignment=0, Width=85, Height=15,;
    Caption="Periodo"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WLLELXIRZP",Visible=.t., Left=11, Top=126,;
    Alignment=1, Width=102, Height=15,;
    Caption="Nome del file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CYFYQAWLHQ",Visible=.t., Left=7, Top=103,;
    Alignment=2, Width=473, Height=15,;
    Caption="Supporto di destinazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="PAJZGLSGNS",Visible=.t., Left=11, Top=154,;
    Alignment=1, Width=102, Height=15,;
    Caption="N. progr. disco:"  ;
  , bGlobalFont=.t.

  add object oBox_1_5 as StdBox with uid="QDJEKYZMXO",left=7, top=4, width=179,height=99

  add object oBox_1_6 as StdBox with uid="IARKIXEXEA",left=10, top=24, width=174,height=2

  add object oBox_1_8 as StdBox with uid="IBUWOEFTQD",left=187, top=4, width=295,height=99

  add object oBox_1_9 as StdBox with uid="PJQPPJKDJQ",left=188, top=24, width=291,height=1

  add object oBox_1_30 as StdBox with uid="TVOKZTPCRT",left=7, top=101, width=475,height=108

  add object oBox_1_31 as StdBox with uid="WDMFELDQSP",left=7, top=120, width=473,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gles_kls','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
