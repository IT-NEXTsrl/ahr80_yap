<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link del tag Plan -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <HTML>
    <HEAD>
      <STYLE>
        BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
        TABLE {font-family:Times New Roman; color:black; background:white}
        A:hover {color:red}
        .Row1 {color:white; border-right-width:1px; border-right-style:solid; border-right-color:black;
               border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:black}
        .Row2 {border-right-width:1px; border-right-style:solid; border-right-color:black}
        .Row3 {border-top-width:1px; border-top-style:solid; border-top-color:black}
        .prjsum {font-family:Times New Roman; border-right: 1px solid rgb(0,0,0); background:#DBDBDB}
        .prjsum2 {font-family:Times New Roman; background:#DBDBDB}
      </STYLE>
    </HEAD>
    <BODY>
      <xsl:apply-templates select="Plan"/>
      <xsl:apply-templates select="Plan/PlanTotalizer"/>
      <xsl:if test="Plan/PlanAddins">
        <P ALIGN="CENTER"><B>Other plans</B></P>
      </xsl:if>
      <xsl:if test="Plan/PlanMain">
        <P ALIGN="CENTER">
          <A HREF="{Plan/PlanMain}_Main{$default_extension}">
            <xsl:value-of select="Plan/PlanMain"/>
          </A>
        </P>
      </xsl:if>
      <xsl:for-each select="Plan/PlanAddins/Module[.!=/Plan/PlanName]">
        <P ALIGN="CENTER">
          <A HREF="{.}_Main{$default_extension}">
            <xsl:value-of select="."/>
          </A>
        </P>
      </xsl:for-each>
    </BODY>
  </HTML>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <xsl:if test="PlanMain">
    <TABLE BORDER="0" WIDTH="100%" CELLSPACING="0" CELLPADDING="4">
      <TR BGCOLOR="#6495ED">
        <TD class="Row1"><B>Design file</B></TD>
        <TD class="Row1"><B>Title</B></TD>
        <TD class="Row1"><B>Author</B></TD>
        <TD class="Row1"><B>User</B></TD>
        <TD class="Row1" WIDTH="6%"><B>Version</B></TD>
        <TD class="Row1" WIDTH="8%"><B>Created</B></TD>
        <TD class="Row1" WIDTH="10%" STYLE="BORDER-RIGHT: 0px"><B>Last revision</B></TD>
      </TR>
      <TR BGCOLOR="#E8FAFF">
        <TD class="Row2"><xsl:apply-templates select="PlanName"/></TD>
        <TD class="Row2"><xsl:apply-templates select="PlanSubtitle"/></TD>
        <TD class="Row2"><xsl:apply-templates select="PlanAuthor"/></TD>
        <TD class="Row2"><xsl:apply-templates select="PlanUser"/></TD>
        <TD class="Row2"><xsl:apply-templates select="PlanVersion"/></TD>
        <TD class="Row2"><xsl:apply-templates select="PlanCreated"/></TD>
        <TD class="Row2" STYLE="BORDER-RIGHT: 0px"><xsl:apply-templates select="PlanLastrevision"/></TD>
      </TR>
      <xsl:if test="PlanNote!=''">
        <TR>
          <TD class="Row3" ALIGN="RIGHT" VALIGN="TOP"><B><I>Description:</I></B></TD>
          <TD class="Row3" COLSPAN="5"><xsl:apply-templates select="PlanNote"/></TD>
          <TD class="Row3">&#160;</TD>
        </TR>
      </xsl:if>
    </TABLE>
  </xsl:if>

  <TABLE BORDER="0" WIDTH="100%" CELLSPACING="0">
    <xsl:if test="not(PlanMain)">
      <xsl:if test="PlanTitle!=''">
        <TR>
          <TD WIDTH="100%" COLSPAN="10" HEIGHT="50" VALIGN="BOTTOM" ALIGN="CENTER">
            <FONT SIZE="7" COLOR="#000080"><B><xsl:value-of select="PlanTitle"/></B></FONT>
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="PlanSubtitle!=''">
        <TR>
          <TD WIDTH="100%" COLSPAN="10" HEIGHT="45" VALIGN="TOP" ALIGN="CENTER">
            <FONT SIZE="5" COLOR="#000080"><B><xsl:value-of select="PlanSubtitle"/></B></FONT>
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="PlanVersion!=''">
        <TR>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
            <B>Version:</B>
          </TD>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
            <xsl:value-of select="PlanVersion"/>
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="PlanAuthor!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Author:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="PlanAuthor"/>
        </TD>
      </TR>
      </xsl:if>
      <xsl:if test="PlanCreated!=''">
      <TR>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
          <B>Created:</B>
        </TD>
        <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
          <xsl:value-of select="PlanCreated"/>
        </TD>
      </TR>
      </xsl:if>
      <xsl:if test="PlanLastrevision!=''">
        <TR>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
            <B>Last modify:</B>
          </TD>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
            <xsl:value-of select="PlanLastrevision"/>
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="PlanUser!=''">
        <TR>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="RIGHT" VALIGN="TOP" HEIGHT="25">
            <B>User:</B>
          </TD>
          <TD WIDTH="50%" COLSPAN="5" ALIGN="LEFT" VALIGN="TOP">
            <xsl:value-of select="PlanUser"/>
          </TD>
        </TR>
      </xsl:if>
      <xsl:if test="PlanNote!=''">
        <TR><TD WIDTH="80%" COLSPAN="10">
          <TABLE BORDER="0" CELLSPACING="20" ALIGN="CENTER">
            <TR><TD xsl:use-attribute-sets="global-notes">
              <xsl:apply-templates select="PlanNote"/>
            </TD></TR>
          </TABLE>
        </TD></TR>
      </xsl:if>
    </xsl:if>

    <TR><TD>&#160;</TD></TR>
    <TR>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="RIGHT">
        <A HREF="{PlanName}_Diagrams{$default_extension}">
          <IMG BORDER="0" SRC="Diagrams.gif" ALT="Show main project plan" WIDTH="60" HEIGHT="60"/>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="LEFT">
        <A HREF="{PlanName}_Diagrams{$default_extension}">
          <FONT SIZE="5">Diagrams</FONT>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="RIGHT" >
        <A HREF="{PlanName}_Entities{$default_extension}">
          <IMG BORDER="0" SRC="Entities.gif" ALT="Show entities list" WIDTH="60" HEIGHT="60"/>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="LEFT">
        <A HREF="{PlanName}_Entities{$default_extension}">
          <FONT SIZE="5">Entities</FONT>
       </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="RIGHT" >
        <A HREF="{PlanName}_TablesSummary{$default_extension}">
          <IMG BORDER="0" SRC="TablesSummary.gif"  ALT="Show tables attributes" WIDTH="60" HEIGHT="60"/>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="LEFT">
        <A HREF="{PlanName}_TablesSummary{$default_extension}">
          <FONT SIZE="5">Tables summary</FONT>
       </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="RIGHT" >
        <A HREF="{PlanName}_Tables{$default_extension}">
          <IMG BORDER="0" SRC="Tables.gif" ALT="Show table field details" WIDTH="60" HEIGHT="60"/>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="LEFT">
        <A HREF="{PlanName}_Tables{$default_extension}">
          <FONT SIZE="5">Tables</FONT>
        </A>
      </TD>

      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="RIGHT" >
        <A HREF="{PlanName}_DataDictionary{$default_extension}">
          <IMG BORDER="0" SRC="DataDictionary.gif" ALT="Show all project fields"/>
        </A>
      </TD>
      <TD WIDTH="10%" VALIGN="MIDDLE" ALIGN="LEFT">
        <A HREF="{PlanName}_DataDictionary{$default_extension}">
          <FONT SIZE="5">Data dictionary</FONT>
        </A>
      </TD>
    </TR>
  </TABLE>
</xsl:template>


<xsl:template match="PlanTotalizer">
  <P align="center">
    <TABLE border="0" cellspacing="0" cellpadding="8">
      <TR>
        <TD bgcolor="#DBDBDB" colspan="4" align="center">
          <font color="#000080"><B>Project summary</B></font>
        </TD>
      </TR>
      <TR>
        <TD width="20%" bgcolor="#DBDBDB"></TD>
        <TD width="40%" bgcolor="#000080" style="border-right: 1px solid rgb(0,0,0)" align="center">
          <SPAN style="color: rgb(255,255,255)"><B>Entities</B></SPAN>
        </TD>
        <TD width="20%" bgcolor="#000080" style="border-right: 1px solid rgb(0,0,0)" align="center">
          <SPAN style="color: rgb(255,255,255)"><B>Tables</B></SPAN>
        </TD>
        <TD width="20%" bgcolor="#000080" align="center">
          <SPAN style="color: rgb(255,255,255)"><B>Fields</B></SPAN>
        </TD>
      </TR>
      <TR>
        <TD bgcolor="#FFFF00"><B>Total nr.</B></TD>
        <TD bgcolor="#FFFF00" style="border-right: 1px solid rgb(0,0,0)" align="center">
           <xsl:value-of select="TotEntities"/>
        </TD>
        <TD bgcolor="#FFFF00" style="border-right: 1px solid rgb(0,0,0)" align="center">
           <xsl:value-of select="TotTables"/>
        </TD>
        <TD bgcolor="#FFFF00" align="center">
           <xsl:value-of select="TotFields"/>
        </TD>
      </TR>
      <xsl:if test="Entities/*">
        <TR>
          <TD bgcolor="#DBDBDB"></TD>
          <TD bgcolor="#DBDBDB">
            <TABLE border="0" width="100%" cellspacing="0">
              <TR>
                <TD style="border-right: 1px solid rgb(0,0,0); border-bottom: 2px solid rgb(0,0,0)" bgcolor="#DBDBDB">
                  <font face="Times New Roman" size="2"><B>Entity type</B></font>
                </TD>
                <TD style="border-bottom: 2px solid rgb(0,0,0)" bgcolor="#DBDBDB" align="right">
                  <font face="Times New Roman" size="2"><B>Q.ty</B></font>
                </TD>
              </TR>
              <xsl:for-each select="Entities/*">
                <TR>
                  <TD class="prjsum" align="left"><xsl:value-of select="local-name(.)"/></TD>
                  <TD class="prjsum2" align="right"><xsl:value-of select="."/></TD>
                </TR>
              </xsl:for-each>
            </TABLE>
          </TD>
          <TD bgcolor="#DBDBDB"></TD>
          <TD bgcolor="#DBDBDB"></TD>
        </TR>
      </xsl:if>
    </TABLE>
  </P>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="PlanName | PlanSubtitle | PlanAuthor | PlanUser |
                     PlanVersion | PlanCreated | PlanLastrevision">
  <xsl:call-template name="ValueOrNotAvailable"/>
</xsl:template>


</xsl:stylesheet>
