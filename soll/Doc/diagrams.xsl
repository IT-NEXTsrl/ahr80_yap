<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link del tag Plan -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Plan"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <HTML>
  <HEAD>
    <STYLE>
      BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
      TABLE {font-family:Times New Roman; color:black; background:white}
      A:hover {color:red}
    </STYLE>
  </HEAD>
  <BODY>
  <P ALIGN="CENTER"><FONT COLOR="#000080" SIZE="6"><B>Diagrams</B></FONT></P>
  <xsl:apply-templates select="PlanImagelist/JPG"/>
  <TABLE BORDER="0" WIDTH="100%" CELLSPACING="0">
    <TR>
      <TD ALIGN="CENTER" WIDTH="50%">
        <A HREF="{PlanName}_Main{$default_extension}">Project overview</A>
      </TD>
      <TD ALIGN="CENTER" WIDTH="50%">
        <A HREF="{PlanName}_EntitiesRecap{$default_extension}">Entities summary</A>
      </TD>
    </TR>
  </TABLE>
  </BODY>
  </HTML>
</xsl:template>


<xsl:template match="JPG">
  <xsl:variable name="name" select="normalize-space(text())"/>
  <xsl:variable name="level">
    <xsl:choose>
      <xsl:when test=".='TOP-LEVEL'"/>
      <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="x" select="@x"/>
  <xsl:variable name="y" select="@y"/>
  <A NAME="{$name}">
    <FONT SIZE="5"><U><xsl:value-of select="$name"/></U></FONT>
  </A>
  <P>
  <TABLE BORDER="0" WIDTH="100%">
    <TR><TD>
      <TABLE BORDER="0" WIDTH="100%">
        <TR>
          <TD WIDTH="10%">&#160;</TD>
          <TD WIDTH="80%">
            <xsl:apply-templates
             select="/Plan/PlanItemlist/PlanItem[ItemName=$name]/ItemGroupnote"/>
          </TD>
          <TD WIDTH="10%">&#160;</TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR>
      <TD ALIGN="CENTER">
      <xsl:variable name="punct">;: #&amp;&lt;&gt;&quot;&apos;|\/*?</xsl:variable>
      <MAP NAME="Level_{$name}">
        <xsl:for-each select="/Plan/PlanItemlist/PlanItem[ItemGroup=$level and ItemObj!='String']">
          <xsl:variable name="link">
            <xsl:choose>
              <xsl:when test="ItemObj='Group'">Diagrams</xsl:when>
              <xsl:otherwise>Entities</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <AREA HREF="{/Plan/PlanName}_{$link}{$default_extension}#{ItemName}"
                SHAPE="RECT" TITLE="Go to '{ItemName}'"
                COORDS="{@x1 - $x},{@y1 - $y},{@x2 - $x},{@y2 - $y}"/>
        </xsl:for-each>
      </MAP>
      <IMG SRC="{/Plan/PlanName}_{translate($name,$punct,'')}.jpg"
           BORDER="0" ALIGN="MIDDLE" USEMAP="#Level_{$name}"/>
      </TD>
    </TR>
    <TR><TD>&#160;</TD></TR>
    <TR>
      <TD ALIGN="CENTER">
      <TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
        <TR BGCOLOR="#6495ED">
          <TD><FONT COLOR="WHITE"><B>Entity</B></FONT></TD>
          <TD><FONT COLOR="WHITE"><B>Class</B></FONT></TD>
          <TD><FONT COLOR="WHITE"><B>Program</B></FONT></TD>
          <TD><FONT COLOR="WHITE"><B>Created</B></FONT></TD>
          <TD><FONT COLOR="WHITE"><B>File</B></FONT></TD>
          <TD><FONT COLOR="WHITE"><B>Menu</B></FONT></TD>
        </TR>
        <xsl:for-each select="/Plan/PlanItemlist/PlanItem[ItemGroup=$level and ItemObj!='String']">
          <xsl:sort select="ItemObj"/>
          <xsl:sort select="ItemName"/>
          <xsl:variable name="link">
            <xsl:choose>
              <xsl:when test="ItemObj='Group'">Diagrams</xsl:when>
              <xsl:otherwise>Entities</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <TR BGCOLOR="#F4FBFF">
            <TD>
              <A HREF="{/Plan/PlanName}_{$link}{$default_extension}#{ItemName}">
                <xsl:value-of select="ItemName"/>
              </A>
            </TD>
            <TD><xsl:value-of select="ItemObj"/></TD>
            <TD><xsl:apply-templates select="ItemProcname"/></TD>
            <TD><xsl:apply-templates select="ItemDatecreate"/></TD>
            <TD>
              <xsl:if test="ItemTbl!=''">
                <A HREF="{/Plan/PlanName}_TablesSummary{$default_extension}#Table_{ItemTbl}">
                  <xsl:value-of select="ItemTbl"/>
                </A>
              </xsl:if>
              <BR/>
              <xsl:if test="ItemMastertbl!=''">
                <A HREF="{/Plan/PlanName}_TablesSummary{$default_extension}#Table_{ItemMastertbl}">
                  <xsl:value-of select="ItemMastertbl"/>
                </A>
              </xsl:if>
            </TD>
            <TD ALIGN="CENTER"><xsl:apply-templates select="ItemMenu"/></TD>
          </TR>
        </xsl:for-each>
      </TABLE>
      </TD>
    </TR>
  </TABLE>
  <P/>
  </P>
</xsl:template>


<!-- Gestione checkbox TRUE/FALSE -->

<xsl:template match="ItemMenu">
  <xsl:call-template name="LogicalCheckBox"/>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="ItemProcname | ItemDatecreate">
  <xsl:call-template name="ValueOrNotAvailable">
    <xsl:with-param name="NA" select="'&#160;'"/>
  </xsl:call-template>
</xsl:template>


</xsl:stylesheet>
