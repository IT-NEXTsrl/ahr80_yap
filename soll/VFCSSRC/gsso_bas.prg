* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bas                                                        *
*              Aggiorna status                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_165]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-23                                                      *
* Last revis.: 2001-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bas",oParentObject)
return(i_retval)

define class tgsso_bas as StdBatch
  * --- Local variables
  w_DATULINC = ctod("  /  /  ")
  w_CODCON = space(15)
  w_SERIAL = space(10)
  w_TIPCON = space(1)
  w_CODVAL = space(3)
  w_DATINC = ctod("  /  /  ")
  w_CONTROL = 0
  w_IMPINC = 0
  w_MODPAG = space(10)
  w_RESIDUO = 0
  w_LEN = 0
  w_DATSCA = ctod("  /  /  ")
  w_USCITA = space(1)
  w_DESCRI = space(40)
  w_STATUS = space(1)
  w_NUMREG = 0
  w_TIPO = space(2)
  w_DATREG = ctod("  /  /  ")
  w_COMPET = space(4)
  w_SERRIF = space(10)
  w_TIPAGG = space(1)
  w_PROG = .NULL.
  w_ROWS = 0
  w_OSERCONT = space(10)
  w_SEGNO_RES = space(1)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_CONTA = 0
  w_mess = space(100)
  w_NumInc = 0
  w_IncTot = 0
  w_mess = space(100)
  * --- WorkFile variables
  INC_AVVE_idx=0
  TMP_GESPIAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Flag Aggiornamento Status (da GSSO_KSA)
    this.w_LEN = 0
    this.w_USCITA = "N"
    * --- Legge Incassi Avvenuti per Contenzioso
    *     
    *     Creo un temporaneo con i contenziosi da valutare con il totale partite associate
    *     al contenzioso
    ah_Msg("Verifica incassi...",.T.)
    if this.oParentObject.w_AGGINC="S"
      this.w_TIPAGG = "S"
      this.w_CONTA = 0
      * --- Select from GSSO2INC
      do vq_exec with 'GSSO2INC',this,'_Curs_GSSO2INC','',.f.,.t.
      if used('_Curs_GSSO2INC')
        select _Curs_GSSO2INC
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl( _Curs_GSSO2INC.CPROWNUM , 0 )
          select _Curs_GSSO2INC
          continue
        enddo
        use
      endif
      if this.w_CONTA>0
        do gsso_sia with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_CONTA = 0
    endif
    ah_Msg("Lettura incassi...",.T.)
    * --- Create temporary table TMP_GESPIAN
    i_nIdx=cp_AddTableDef('TMP_GESPIAN') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSSO_QAS_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_GESPIAN_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_ROWS = 1
    do while this.w_ROWS<>0
      * --- Aggiornamento status, elimina eventuali contenziosi che hanno 
      *     in comune partite di saldo pre 2.2 (senza riferimenti).
      *     
      *     Quindi per escludere il caso in cui due partite di saldo identiche 
      *     vanno in due contenziosi diversi di tipo no effetto che sono entrambi 
      *     successivamente saldati da partite identiche.
      ah_Msg("'Esclude contenziosi 'doppi' pre 2.2...",.T.) 
      * --- Delete from TMP_GESPIAN
      i_nConn=i_TableProp[this.TMP_GESPIAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_GESPIAN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".COSERIAL = "+i_cQueryTable+".COSERIAL";
      
        do vq_exec with 'GSSO_QAS_5',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Al termine della prima cancellazione verifico quanti contenziosi ho eliminato.
      *     
      *     Se nessuno ho terminato altrimenti devo ripetere la query nel caso abbia
      *     pi� di 2 contenziosi legati a pi� di due partite di saldo identiche
      this.w_ROWS = i_ROWS
    enddo
    * --- Cursore Incassi da passare alla Stampa Finale
     
 CREATE CURSOR INCAVVE (COSERIAL C(10), COCODCON C(15),ANDESCRI C(40),CONUMREG N(6),COCOMPET C(4),; 
 COSTATUS C(2), CO__TIPO C(1), CODATREG D(10), COCODVAL C(3), IADATINC D(10),IAIMPINC N(18,4),IATIPPAG C(10) )
    * --- Lancio la query per recuperare gli incassi successivi ai vari
    *     contenziosi..
    this.w_OSERCONT = Repl("#",15)
    this.w_NumInc = 0
    this.w_IncTot = 0
    * --- Try
    local bErr_032982D8
    bErr_032982D8=bTrsErr
    this.Try_032982D8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Elaborazione annullata - errore: %1",,"", Message() )
    endif
    bTrsErr=bTrsErr or bErr_032982D8
    * --- End
    * --- Drop temporary table TMP_GESPIAN
    i_nIdx=cp_GetTableDefIdx('TMP_GESPIAN')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_GESPIAN')
    endif
    * --- Verifico se aprire la maschera di conferma chiusura contenziosi
    * --- Select from GSSO_QAS_4
    do vq_exec with 'GSSO_QAS_4',this,'_Curs_GSSO_QAS_4','',.f.,.t.
    if used('_Curs_GSSO_QAS_4')
      select _Curs_GSSO_QAS_4
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl( _Curs_GSSO_QAS_4.CONTA , 0 )
        select _Curs_GSSO_QAS_4
        continue
      enddo
      use
    endif
    if this.w_CONTA>0
      * --- Apro la maschera per la chiusura contenziosi (w_USCITA valorizzato
      *     differentemente da 'C' se maschera confermata)
      this.w_USCITA = "F"
      this.w_TIPAGG = "N"
      do gsso_sia with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Test abbandono Elaborazione
      do case
        case RECCOUNT("INCAVVE") =0 AND this.w_USCITA="C" 
          ah_ErrorMsg("Fino alla data impostata non ci sono contenziosi da aggiornare",,"")
        case RECCOUNT("INCAVVE") >0 and this.w_USCITA $ "C-F"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_USCITA="F"
            this.w_mess = "Elaborazione interrotta"
            ah_ErrorMsg(this.w_MESS,,"")
          else
            this.w_mess = "Elaborazione terminata, inseriti incassi %1 su %2 da inserire"
            ah_ErrorMsg(this.w_MESS,,"", alltrim(str(this.w_NumInc)), alltrim(str(this.w_IncTot)) )
          endif
      endcase
    else
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_mess = "Elaborazione terminata, inseriti incassi %1 su %2 da inserire"
      ah_ErrorMsg(this.w_MESS,,"", alltrim(str(this.w_NumInc)), alltrim(str(this.w_IncTot)) )
    endif
  endproc
  proc Try_032982D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from GSSOAQAS
    do vq_exec with 'GSSOAQAS',this,'_Curs_GSSOAQAS','',.f.,.t.
    if used('_Curs_GSSOAQAS')
      select _Curs_GSSOAQAS
      locate for 1=1
      do while not(eof())
      if this.w_OSERCONT<>_Curs_GSSOAQAS.COSERIAL
        if this.w_OSERCONT<>Repl("#",15)
          * --- Scrivo il residuo nel piede degli incassi avvenuti
          this.w_RESIDUO = IIF( IIF( this.w_SEGNO_RES="D" , -1 , 1 ) * this.w_RESIDUO<0 , 0 , this.w_RESIDUO )
          * --- Write into INC_AVVE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.INC_AVVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_AVVE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IARESINC ="+cp_NullLink(cp_ToStrODBC(this.w_RESIDUO),'INC_AVVE','IARESINC');
                +i_ccchkf ;
            +" where ";
                +"IASERIAL = "+cp_ToStrODBC(this.w_OSERCONT);
                   )
          else
            update (i_cTable) set;
                IARESINC = this.w_RESIDUO;
                &i_ccchkf. ;
             where;
                IASERIAL = this.w_OSERCONT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_LEN = Nvl( _Curs_GSSOAQAS.NUMROW , 0 )
        * --- Determino il segno dell'eventuale residuo, il residuo deve avere lo
        *     stesso segno delle partite del contenziosi. Ho quindi due casi:
        *     a) Residuo in segno discordante => lo pongo a zero
        *     b) Residuo stessi segno lo mantengo
        *     
        *     questa verifica lo svolgo al termine dell'esame di tutti gli incassi
        *     Residuo calcolato come totale partita legate al contenzioso con segno - 
        *     totale incassi presi direttamente dal database
        this.w_RESIDUO = Nvl( _Curs_GSSOAQAS.RESIDUO,0)
        this.w_SEGNO_RES = IIF ( Nvl( _Curs_GSSOAQAS.TOTPAR , 0 )<0 , "D","A" )
        this.w_DESCRI = _Curs_GSSOAQAS.ANDESCRI
        this.w_CODVAL = _Curs_GSSOAQAS.COCODVAL
        this.w_NUMREG = _Curs_GSSOAQAS.CONUMREG
        this.w_COMPET = _Curs_GSSOAQAS.COCOMPET
        this.w_TIPO = _Curs_GSSOAQAS.CO__TIPO
        this.w_STATUS = _Curs_GSSOAQAS.COSTATUS
        this.w_DATREG = _Curs_GSSOAQAS.CODATREG
        this.w_CODCON = _Curs_GSSOAQAS.COCODCON
        this.w_OSERCONT = _Curs_GSSOAQAS.COSERIAL
        ah_Msg("Incassi avvenuti contenzioso %1",.T.,.F.,.F., this.w_OSERCONT)
      endif
      this.w_DATINC = CP_TODATE(_Curs_GSSOAQAS.PNDATREG)
      this.w_MODPAG = _Curs_GSSOAQAS.PTMODPAG
      this.w_IMPINC = IIF(NVL(_Curs_GSSOAQAS.PT_SEGNO," ")="D",-NVL(_Curs_GSSOAQAS.PTTOTIMP,0),NVL(_Curs_GSSOAQAS.PTTOTIMP,0))
      this.w_RESIDUO = this.w_RESIDUO - this.w_IMPINC
      this.w_PTSERIAL = _Curs_GSSOAQAS.PTSERIAL
      this.w_PTROWORD = _Curs_GSSOAQAS.PTROWORD
      this.w_CPROWNUM = _Curs_GSSOAQAS.CPROWNUM
      this.w_LEN = this.w_LEN+1
      this.w_IncTot = this.w_IncTot + 1
      * --- Insert into INC_AVVE
      i_nConn=i_TableProp[this.INC_AVVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.INC_AVVE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IASERIAL"+",CPROWNUM"+",IADATINC"+",IAIMPINC"+",IATIPPAG"+",IASERRIF"+",IAORDRIF"+",IANUMRIF"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OSERCONT),'INC_AVVE','IASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LEN),'INC_AVVE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATINC),'INC_AVVE','IADATINC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPINC),'INC_AVVE','IAIMPINC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'INC_AVVE','IATIPPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'INC_AVVE','IASERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'INC_AVVE','IAORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'INC_AVVE','IANUMRIF');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IASERIAL',this.w_OSERCONT,'CPROWNUM',this.w_LEN,'IADATINC',this.w_DATINC,'IAIMPINC',this.w_IMPINC,'IATIPPAG',this.w_MODPAG,'IASERRIF',this.w_PTSERIAL,'IAORDRIF',this.w_PTROWORD,'IANUMRIF',this.w_CPROWNUM)
        insert into (i_cTable) (IASERIAL,CPROWNUM,IADATINC,IAIMPINC,IATIPPAG,IASERRIF,IAORDRIF,IANUMRIF &i_ccchkf. );
           values (;
             this.w_OSERCONT;
             ,this.w_LEN;
             ,this.w_DATINC;
             ,this.w_IMPINC;
             ,this.w_MODPAG;
             ,this.w_PTSERIAL;
             ,this.w_PTROWORD;
             ,this.w_CPROWNUM;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in inserimento incassi'
        return
      endif
      this.w_NumInc = this.w_NumInc + 1
       
 SELECT INCAVVE 
 APPEND BLANK 
 REPLACE INCAVVE.COSERIAL WITH this.w_OSERCONT 
 REPLACE INCAVVE.ANDESCRI WITH this.w_DESCRI 
 REPLACE INCAVVE.COCODCON WITH this.w_CODCON 
 REPLACE INCAVVE.CONUMREG WITH this.w_NUMREG 
 REPLACE INCAVVE.COCOMPET WITH this.w_COMPET 
 REPLACE INCAVVE.CO__TIPO WITH this.w_TIPO 
 REPLACE INCAVVE.COSTATUS WITH this.w_STATUS 
 REPLACE INCAVVE.CODATREG WITH this.w_DATREG 
 REPLACE INCAVVE.COCODVAL WITH this.w_CODVAL 
 REPLACE INCAVVE.IADATINC WITH this.w_DATINC 
 REPLACE INCAVVE.IAIMPINC WITH this.w_IMPINC 
 REPLACE INCAVVE.IATIPPAG WITH this.w_MODPAG
        select _Curs_GSSOAQAS
        continue
      enddo
      use
    endif
    * --- Scrivo il risiduo nel piede degli incassi avvenuti
    this.w_RESIDUO = IIF( IIF( this.w_SEGNO_RES="D" , -1 , 1 ) * this.w_RESIDUO<0 , 0 , this.w_RESIDUO )
    * --- Write into INC_AVVE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INC_AVVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_AVVE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IARESINC ="+cp_NullLink(cp_ToStrODBC(this.w_RESIDUO),'INC_AVVE','IARESINC');
          +i_ccchkf ;
      +" where ";
          +"IASERIAL = "+cp_ToStrODBC(this.w_OSERCONT);
             )
    else
      update (i_cTable) set;
          IARESINC = this.w_RESIDUO;
          &i_ccchkf. ;
       where;
          IASERIAL = this.w_OSERCONT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo Stampa  Incassi
    if RECCOUNT("INCAVVE") >0
      SELECT * FROM INCAVVE INTO CURSOR __TMP__ ORDER BY 1, 3
      SELECT __TMP__
      GO TOP
      ah_Msg("Stampa in corso",.T.)
      CP_CHPRN("..\SOLL\EXE\QUERY\GSSO1INC.FRX", " ", this)
      if used("__TMP__")
        SELECT __TMP__
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='INC_AVVE'
    this.cWorkTables[2]='*TMP_GESPIAN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSSO2INC')
      use in _Curs_GSSO2INC
    endif
    if used('_Curs_GSSOAQAS')
      use in _Curs_GSSOAQAS
    endif
    if used('_Curs_GSSO_QAS_4')
      use in _Curs_GSSO_QAS_4
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
