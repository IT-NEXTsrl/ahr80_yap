* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bef                                                        *
*              Elimina files                                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-27                                                      *
* Last revis.: 2012-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bef",oParentObject)
return(i_retval)

define class tgsso_bef as StdBatch
  * --- Local variables
  w_MSG = space(100)
  w_FILEA = space(100)
  w_DTARIC = ctod("  /  /  ")
  w_NOMSUP = space(20)
  w_ERR = space(1)
  * --- WorkFile variables
  LOG_REBA_idx=0
  FLU_RITO_idx=0
  FLU_ESRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Files 
    this.w_ERR = "N"
    this.w_DTARIC = this.oparentobject.w_DTARIC
    this.w_NOMSUP = this.oparentobject.w_NOMSUP
    this.w_FILEA = this.oparentobject.w_FILEA
    * --- Try
    local bErr_0347BDA0
    bErr_0347BDA0=bTrsErr
    this.Try_0347BDA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if this.w_ERR="S"
        AH_ERRORMSG(this.w_MSG,48,"")
      else
        AH_ERRORMSG("Impossibile eliminare i flussi con riferimento al file %1",48,"",IIF(NOT EMPTY(this.w_MSG),CHR(13)+ALLTRIM(this.w_MSG),""))
      endif
    endif
    bTrsErr=bTrsErr or bErr_0347BDA0
    * --- End
  endproc
  proc Try_0347BDA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do case
      case this.oParentObject.w_GESTIONE="F" and NOT EMPTY(this.oParentObject.w_FILEF)
        VQ_EXEC("..\SOLL\EXE\QUERY\GSSOFBEF.VQR",this,"chkuser")
        if used("chkuser")
          if reccount("chkuser")>0
            this.w_MSG = "Sono presenti dei flussi elaborati"
            * --- Raise
            i_Error=AH_MSGFORMAT ("Impossibile eliminare il file perch� ha dei flussi elaborati")
            return
          endif
          Select chkuser 
 Use
        endif
        * --- Delete from FLU_RITO
        i_nConn=i_TableProp[this.FLU_RITO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"FLNOMFIL = "+cp_ToStrODBC(this.oParentObject.w_FILEF);
                 )
        else
          delete from (i_cTable) where;
                FLNOMFIL = this.oParentObject.w_FILEF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from FLU_ESRI
        i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"FRNOMFIL = "+cp_ToStrODBC(this.oParentObject.w_FILEF);
                 )
        else
          delete from (i_cTable) where;
                FRNOMFIL = this.oParentObject.w_FILEF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from LOG_REBA
        i_nConn=i_TableProp[this.LOG_REBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOG_REBA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ERNOMFIL = "+cp_ToStrODBC(this.oParentObject.w_FILEF);
                 )
        else
          delete from (i_cTable) where;
                ERNOMFIL = this.oParentObject.w_FILEF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
    * --- commit
    cp_EndTrs(.t.)
    AH_ERRORMSG("Il file ed i flussi in esso contenuti sono stati eliminati",48,"")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOG_REBA'
    this.cWorkTables[2]='FLU_RITO'
    this.cWorkTables[3]='FLU_ESRI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
