* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mfr                                                        *
*              Flussi operativi di ritorno                                     *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2012-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_mfr"))

* --- Class definition
define class tgsso_mfr as StdTrsForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 803
  Height = 284+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-13"
  HelpContextID=46765417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  FLU_RITO_IDX = 0
  cFile = "FLU_RITO"
  cKeySelect = "FLNOMSUP,FLDTARIC,FLNOMFIL"
  cKeyWhere  = "FLNOMSUP=this.w_FLNOMSUP and FLDTARIC=this.w_FLDTARIC and FLNOMFIL=this.w_FLNOMFIL"
  cKeyDetail  = "FLNOMSUP=this.w_FLNOMSUP and FLDTARIC=this.w_FLDTARIC and FLNOMFIL=this.w_FLNOMFIL"
  cKeyWhereODBC = '"FLNOMSUP="+cp_ToStrODBC(this.w_FLNOMSUP)';
      +'+" and FLDTARIC="+cp_ToStrODBC(this.w_FLDTARIC,"D")';
      +'+" and FLNOMFIL="+cp_ToStrODBC(this.w_FLNOMFIL)';

  cKeyDetailWhereODBC = '"FLNOMSUP="+cp_ToStrODBC(this.w_FLNOMSUP)';
      +'+" and FLDTARIC="+cp_ToStrODBC(this.w_FLDTARIC,"D")';
      +'+" and FLNOMFIL="+cp_ToStrODBC(this.w_FLNOMFIL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"FLU_RITO.FLNOMSUP="+cp_ToStrODBC(this.w_FLNOMSUP)';
      +'+" and FLU_RITO.FLDTARIC="+cp_ToStrODBC(this.w_FLDTARIC,"D")';
      +'+" and FLU_RITO.FLNOMFIL="+cp_ToStrODBC(this.w_FLNOMFIL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'FLU_RITO.CPROWNUM '
  cPrg = "gsso_mfr"
  cComment = "Flussi operativi di ritorno"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FLNOMSUP = space(20)
  w_FLDTARIC = ctod('  /  /  ')
  w_FLNOMFIL = space(100)
  w_FLDATIEL = space(200)
  o_FLDATIEL = space(200)
  w_FLERRORE = space(1)
  w_FLFLGACC = space(1)
  w_FLNUMDIS = space(10)
  w_FLAGELA = 0
  w_TOTELA = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FLU_RITO','gsso_mfr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_mfrPag1","gsso_mfr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Flusso operativo di ritorno")
      .Pages(1).HelpContextID = 187165757
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLNOMSUP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FLU_RITO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FLU_RITO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FLU_RITO_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_FLNOMSUP = NVL(FLNOMSUP,space(20))
      .w_FLDTARIC = NVL(FLDTARIC,ctod("  /  /  "))
      .w_FLNOMFIL = NVL(FLNOMFIL,space(100))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsso_mfr
    this.w_FLDTARIC=IIF(EMPTY(this.w_FLDTARIC),i_DATSYS,this.w_FLDTARIC)
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from FLU_RITO where FLNOMSUP=KeySet.FLNOMSUP
    *                            and FLDTARIC=KeySet.FLDTARIC
    *                            and FLNOMFIL=KeySet.FLNOMFIL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2],this.bLoadRecFilter,this.FLU_RITO_IDX,"gsso_mfr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FLU_RITO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FLU_RITO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FLU_RITO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FLNOMSUP',this.w_FLNOMSUP  ,'FLDTARIC',this.w_FLDTARIC  ,'FLNOMFIL',this.w_FLNOMFIL  )
      select * from (i_cTable) FLU_RITO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTELA = 0
        .w_FLNOMSUP = NVL(FLNOMSUP,space(20))
        .w_FLDTARIC = NVL(cp_ToDate(FLDTARIC),ctod("  /  /  "))
        .w_FLNOMFIL = NVL(FLNOMFIL,space(100))
        .w_FLERRORE = NVL(FLERRORE,space(1))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'FLU_RITO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTELA = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_FLDATIEL = NVL(FLDATIEL,space(200))
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
          .w_FLFLGACC = NVL(FLFLGACC,space(1))
          .w_FLNUMDIS = NVL(FLNUMDIS,space(10))
        .w_FLAGELA = IIF(.w_FLFLGACC='S',1,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTELA = .w_TOTELA+.w_FLAGELA
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_9.enabled = .oPgFrm.Page1.oPag.oBtn_1_9.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_10.enabled = .oPgFrm.Page1.oPag.oBtn_1_10.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_FLNOMSUP=space(20)
      .w_FLDTARIC=ctod("  /  /  ")
      .w_FLNOMFIL=space(100)
      .w_FLDATIEL=space(200)
      .w_FLERRORE=space(1)
      .w_FLFLGACC=space(1)
      .w_FLNUMDIS=space(10)
      .w_FLAGELA=0
      .w_TOTELA=0
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
        .DoRTCalc(1,7,.f.)
        .w_FLAGELA = IIF(.w_FLFLGACC='S',1,0)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'FLU_RITO')
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFLNOMSUP_1_1.enabled = i_bVal
      .Page1.oPag.oFLDTARIC_1_2.enabled = i_bVal
      .Page1.oPag.oFLNOMFIL_1_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      .Page1.oPag.oBtn_1_10.enabled = .Page1.oPag.oBtn_1_10.mCond()
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_2_2.enabled = i_bVal
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFLNOMSUP_1_1.enabled = .f.
        .Page1.oPag.oFLDTARIC_1_2.enabled = .f.
        .Page1.oPag.oFLNOMFIL_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFLNOMSUP_1_1.enabled = .t.
        .Page1.oPag.oFLDTARIC_1_2.enabled = .t.
        .Page1.oPag.oFLNOMFIL_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'FLU_RITO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOMSUP,"FLNOMSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLDTARIC,"FLDTARIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLNOMFIL,"FLNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FLERRORE,"FLERRORE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
    i_lTable = "FLU_RITO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FLU_RITO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsso_SCF with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FLDATIEL C(200);
      ,t_FLFLGACC N(3);
      ,t_FLNUMDIS C(10);
      ,CPROWNUM N(10);
      ,t_FLAGELA N(3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_mfrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLDATIEL_2_1.controlsource=this.cTrsName+'.t_FLDATIEL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.controlsource=this.cTrsName+'.t_FLFLGACC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLNUMDIS_2_4.controlsource=this.cTrsName+'.t_FLNUMDIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(561)
    this.AddVLine(686)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDATIEL_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
      *
      * insert into FLU_RITO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FLU_RITO')
        i_extval=cp_InsertValODBCExtFlds(this,'FLU_RITO')
        i_cFldBody=" "+;
                  "(FLNOMSUP,FLDTARIC,FLNOMFIL,FLDATIEL,FLERRORE"+;
                  ",FLFLGACC,FLNUMDIS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FLNOMSUP)+","+cp_ToStrODBC(this.w_FLDTARIC)+","+cp_ToStrODBC(this.w_FLNOMFIL)+","+cp_ToStrODBC(this.w_FLDATIEL)+","+cp_ToStrODBC(this.w_FLERRORE)+;
             ","+cp_ToStrODBC(this.w_FLFLGACC)+","+cp_ToStrODBC(this.w_FLNUMDIS)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FLU_RITO')
        i_extval=cp_InsertValVFPExtFlds(this,'FLU_RITO')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FLNOMSUP',this.w_FLNOMSUP,'FLDTARIC',this.w_FLDTARIC,'FLNOMFIL',this.w_FLNOMFIL)
        INSERT INTO (i_cTable) (;
                   FLNOMSUP;
                  ,FLDTARIC;
                  ,FLNOMFIL;
                  ,FLDATIEL;
                  ,FLERRORE;
                  ,FLFLGACC;
                  ,FLNUMDIS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_FLNOMSUP;
                  ,this.w_FLDTARIC;
                  ,this.w_FLNOMFIL;
                  ,this.w_FLDATIEL;
                  ,this.w_FLERRORE;
                  ,this.w_FLFLGACC;
                  ,this.w_FLNUMDIS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_FLDATIEL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'FLU_RITO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " FLERRORE="+cp_ToStrODBC(this.w_FLERRORE)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'FLU_RITO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  FLERRORE=this.w_FLERRORE;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_FLDATIEL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update FLU_RITO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'FLU_RITO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " FLDATIEL="+cp_ToStrODBC(this.w_FLDATIEL)+;
                     ",FLERRORE="+cp_ToStrODBC(this.w_FLERRORE)+;
                     ",FLFLGACC="+cp_ToStrODBC(this.w_FLFLGACC)+;
                     ",FLNUMDIS="+cp_ToStrODBC(this.w_FLNUMDIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'FLU_RITO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      FLDATIEL=this.w_FLDATIEL;
                     ,FLERRORE=this.w_FLERRORE;
                     ,FLFLGACC=this.w_FLFLGACC;
                     ,FLNUMDIS=this.w_FLNUMDIS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- gsso_mfr
    this.NotifyEvent('DeleteLog')
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_FLDATIEL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete FLU_RITO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_FLDATIEL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
        .DoRTCalc(1,7,.t.)
          .w_TOTELA = .w_TOTELA-.w_flagela
          .w_FLAGELA = IIF(.w_FLFLGACC='S',1,0)
          .w_TOTELA = .w_TOTELA+.w_flagela
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FLAGELA with this.w_FLAGELA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oFLNOMSUP_1_1.value==this.w_FLNOMSUP)
      this.oPgFrm.Page1.oPag.oFLNOMSUP_1_1.value=this.w_FLNOMSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDTARIC_1_2.value==this.w_FLDTARIC)
      this.oPgFrm.Page1.oPag.oFLDTARIC_1_2.value=this.w_FLDTARIC
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOMFIL_1_3.value==this.w_FLNOMFIL)
      this.oPgFrm.Page1.oPag.oFLNOMFIL_1_3.value=this.w_FLNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLERRORE_1_7.RadioValue()==this.w_FLERRORE)
      this.oPgFrm.Page1.oPag.oFLERRORE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDATIEL_2_1.value==this.w_FLDATIEL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDATIEL_2_1.value=this.w_FLDATIEL
      replace t_FLDATIEL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLDATIEL_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.RadioValue()==this.w_FLFLGACC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.SetRadio()
      replace t_FLFLGACC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNUMDIS_2_4.value==this.w_FLNUMDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNUMDIS_2_4.value=this.w_FLNUMDIS
      replace t_FLNUMDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLNUMDIS_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'FLU_RITO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FLNOMSUP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oFLNOMSUP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FLNOMSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_FLDATIEL));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_FLDATIEL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLDATIEL = this.w_FLDATIEL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_FLDATIEL))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=1=0
    if !i_bRes
      cp_ErrorMsg("Flusso operativo elaborato non � consentita la cancellazione","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FLDATIEL=space(200)
      .w_FLFLGACC=space(1)
      .w_FLNUMDIS=space(10)
      .w_FLAGELA=0
        .oPgFrm.Page1.oPag.oObj_2_2.Calculate()
      .DoRTCalc(1,7,.f.)
        .w_FLAGELA = IIF(.w_FLFLGACC='S',1,0)
    endwith
    this.DoRTCalc(9,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FLDATIEL = t_FLDATIEL
    this.w_FLFLGACC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.RadioValue(.t.)
    this.w_FLNUMDIS = t_FLNUMDIS
    this.w_FLAGELA = t_FLAGELA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FLDATIEL with this.w_FLDATIEL
    replace t_FLFLGACC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLFLGACC_2_3.ToRadio()
    replace t_FLNUMDIS with this.w_FLNUMDIS
    replace t_FLAGELA with this.w_FLAGELA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTELA = .w_TOTELA-.w_flagela
        .SetControlsValue()
      endwith
  EndProc
  func CanDelete()
    local i_res
    i_res=this.w_TOTELA=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Flusso operativo elaborato non � consentita la cancellazione"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_mfrPag1 as StdContainer
  Width  = 799
  height = 284
  stdWidth  = 799
  stdheight = 284
  resizeXpos=365
  resizeYpos=144
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLNOMSUP_1_1 as StdField with uid="BJNCVUVGPD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FLNOMSUP", cQueryName = "FLNOMSUP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome supporto flusso di ritorno",;
    HelpContextID = 89824166,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=230, Top=8, InputMask=replicate('X',20)

  add object oFLDTARIC_1_2 as StdField with uid="WGXMORKMLB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FLDTARIC", cQueryName = "FLNOMSUP,FLDTARIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione flusso di ritorno",;
    HelpContextID = 60750745,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=554, Top=8

  add object oFLNOMFIL_1_3 as StdField with uid="HALKSXAOVZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLNOMFIL", cQueryName = "FLNOMSUP,FLDTARIC,FLNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome file flusso di ritorno",;
    HelpContextID = 140155810,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=230, Top=35, InputMask=replicate('X',100)

  add object oFLERRORE_1_7 as StdCheck with uid="PHEECYWOSW",rtseq=5,rtrep=.f.,left=406, top=35, caption="Tracciato errato", enabled=.f.,;
    ToolTipText = "Tracciato errato",;
    HelpContextID = 28117915,;
    cFormVar="w_FLERRORE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLERRORE_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLERRORE,&i_cF..t_FLERRORE),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFLERRORE_1_7.GetRadio()
    this.Parent.oContained.w_FLERRORE = this.RadioValue()
    return .t.
  endfunc

  func oFLERRORE_1_7.ToRadio()
    this.Parent.oContained.w_FLERRORE=trim(this.Parent.oContained.w_FLERRORE)
    return(;
      iif(this.Parent.oContained.w_FLERRORE=='S',1,;
      0))
  endfunc

  func oFLERRORE_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_8 as cp_runprogram with uid="HDFMWSYSWP",left=-3, top=293, width=187,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsso_BDL(w_FLNOMSUP,w_FLDTARIC,w_FLNOMFIL,'F')",;
    cEvent = "DeleteLog",;
    nPag=1;
    , HelpContextID = 131232230


  add object oBtn_1_9 as StdButton with uid="ATWJDENOIZ",left=670, top=10, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare gli esiti ricevuti";
    , HelpContextID = 44180502;
    , Caption='\<Esiti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_MER', 'FRNOMSUP', .w_FLNOMSUP, 'FRDTARIC', .w_FLDTARIC, 'FRNOMFIL', .w_FLNOMFIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    with this.Parent.oContained
      return (not empty(nvl(.w_FLNOMSUP,'')))
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="JKDXEKYIKS",left=723, top=10, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log";
    , HelpContextID = 44180502;
    , Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSSO_BAE(this.Parent.oContained,.w_FLNOMSUP,.w_FLDTARIC,.w_FLNOMFIL,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLERRORE<>'S')
    endwith
   endif
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="EXGNTTCBBN",left=-4, top=317, width=266,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSO_BER(w_FLNOMSUP,w_FLDTARIC,w_FLNOMFIL,'E')",;
    cEvent = "DeleteLog",;
    nPag=1;
    , HelpContextID = 131232230


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=67, width=790,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="FLDATIEL",Label1="Campo dati record di ritorno",Field2="FLFLGACC",Label2="",Field3="FLNUMDIS",Label3="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 889978

  add object oStr_1_4 as StdString with uid="JANPERCFJY",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=220, Height=19,;
    Caption="Nome supporto flusso di ritorno:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="NJXTCDDCWP",Visible=.t., Left=402, Top=8,;
    Alignment=1, Width=149, Height=18,;
    Caption="Data creazione ricezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XOOAQFWHXO",Visible=.t., Left=153, Top=35,;
    Alignment=1, Width=69, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=86,;
    width=786+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=87,width=785+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_2 as cp_runprogram with uid="NFXYBBCDLD",width=187,height=20,;
   left=192, top=293,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSO_BVR('FL')",;
    cEvent = "w_FLDATIEL Changed",;
    nPag=2;
    , HelpContextID = 131232230

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsso_mfrBodyRow as CPBodyRowCnt
  Width=776
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFLDATIEL_2_1 as StdTrsField with uid="TBMPSJJNCE",rtseq=4,rtrep=.t.,;
    cFormVar="w_FLDATIEL",value=space(200),;
    ToolTipText = "Campo dati record di ritorno",;
    HelpContextID = 196869026,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=555, Left=-2, Top=0, InputMask=replicate('X',200)

  add object oFLFLGACC_2_3 as StdTrsCheck with uid="GWUFIRYFOW",rtrep=.t.,;
    cFormVar="w_FLFLGACC", enabled=.f., caption="Flusso elaborato",;
    ToolTipText = "Flag elaborato flusso di ritorno",;
    HelpContextID = 49748889,;
    Left=558, Top=0, Width=121,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oFLFLGACC_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLFLGACC,&i_cF..t_FLFLGACC),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFLFLGACC_2_3.GetRadio()
    this.Parent.oContained.w_FLFLGACC = this.RadioValue()
    return .t.
  endfunc

  func oFLFLGACC_2_3.ToRadio()
    this.Parent.oContained.w_FLFLGACC=trim(this.Parent.oContained.w_FLFLGACC)
    return(;
      iif(this.Parent.oContained.w_FLFLGACC=='S',1,;
      0))
  endfunc

  func oFLFLGACC_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFLNUMDIS_2_4 as StdTrsField with uid="PPCYGRGNWG",rtseq=7,rtrep=.t.,;
    cFormVar="w_FLNUMDIS",value=space(10),enabled=.f.,;
    HelpContextID = 106994601,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=90, Left=681, Top=0, InputMask=replicate('X',10)
  add object oLast as LastKeyMover
  * ---
  func oFLDATIEL_2_1.When()
    return(.t.)
  proc oFLDATIEL_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFLDATIEL_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mfr','FLU_RITO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FLNOMSUP=FLU_RITO.FLNOMSUP";
  +" and "+i_cAliasName2+".FLDTARIC=FLU_RITO.FLDTARIC";
  +" and "+i_cAliasName2+".FLNOMFIL=FLU_RITO.FLNOMFIL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
