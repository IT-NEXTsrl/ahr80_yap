* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bic                                                        *
*              Chiude insoluti\mancati pagamenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_72]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-23                                                      *
* Last revis.: 2010-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bic",oParentObject,m.pPARAM,m.pSERIAL)
return(i_retval)

define class tgsso_bic as StdBatch
  * --- Local variables
  pPARAM = space(4)
  pSERIAL = space(10)
  w_SERIAL = space(10)
  w_STATUS = space(1)
  w_ROWNUM = space(10)
  w_NumCon = 0
  w_ConTot = 0
  w_mess = space(100)
  w_PROG = .NULL.
  w_IMPINC = 0
  * --- WorkFile variables
  CON_TENZ_idx=0
  VALUTE_idx=0
  INC_AVVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ND = this.oParentObject.w_Conins.cCursor
    do case
      case this.pPARAM="SELE"
        * --- Evento w_SELEZI Changed
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if USED(this.oParentObject.w_Conins.cCursor)
          * --- Selezionati gli insoluti da chiudere
          if this.oParentObject.w_SELEZI = "S"
            * --- Seleziona Tutto
            UPDATE (ND) SET XCHK=1 Where XCHK=0
          else
            * --- deseleziona Tutto
            UPDATE (ND) SET XCHK=0 WHERE XCHK=1
          endif
        endif
      case this.pPARAM="STAT"
        * --- Chisura degli insoluti con residuo nullo selezionati
        Select Distinct COSERIAL,CPROWNUM,IAIMPINC FROM (ND) WHERE XCHK=1 INTO CURSOR TEMP NOFILTER
        this.w_STATUS = "CI"
        this.w_NumCon = 0
        this.w_ConTot = 0
        * --- Try
        local bErr_03558518
        bErr_03558518=bTrsErr
        this.Try_03558518()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Aggiornamento status annullato - errore: %1",,"", Message() )
        endif
        bTrsErr=bTrsErr or bErr_03558518
        * --- End
        if this.w_Numcon>0
          if this.oParentObject.w_TIPAGG="N"
            this.w_mess = "Aggiornati %1 contenziosi su %2 contenziosi da aggiornare"
          else
            this.w_mess = "Aggiornati %1 incassi su %2 incassi da aggiornare"
          endif
          ah_ErrorMsg(this.w_MESS,,"", Alltrim(str(this.w_NumCon)), Alltrim(str(this.w_ConTot)) )
        endif
        * --- Comunque vada la transazione elimino il cursore...
        if used("TEMP")
          select TEMP
          use
        endif
      case this.pPARAM="DETT"
        * --- Questo oggetto sar� definito come Documento
        NC=DEFPIC(this.oParentObject.w_DECTOT)
        this.w_PROG = GSSO_MCO()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_COSERIAL = this.pSERIAL
        this.w_PROG.QueryKeySet("COSERIAL='"+this.pSERIAL+ "'","")     
        this.w_PROG.LoadRecWarn()     
    endcase
  endproc
  proc Try_03558518()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_TIPAGG="N"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 Select Temp 
 Go Top 
 Scan
    this.w_SERIAL = COSERIAL
    this.w_ConTot = this.w_ConTot+1
    * --- Write into CON_TENZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COSTATUS ="+cp_NullLink(cp_ToStrODBC("CI"),'CON_TENZ','COSTATUS');
          +i_ccchkf ;
      +" where ";
          +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          COSTATUS = "CI";
          &i_ccchkf. ;
       where;
          COSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NumCon = this.w_NumCon+1
    * --- Aggiorno Status nel Cursore x la  stampa
     
 Select INCAVVE 
 REPLACE INCAVVE.COSTATUS WITH this.w_STATUS For COSERIAL=this.w_SERIAL 
 Select Temp 
 EndScan
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 Select Temp 
 Go Top 
 Scan
    this.w_SERIAL = COSERIAL
    this.w_ROWNUM = CPROWNUM
    this.w_IMPINC = Nvl(IAIMPINC,0)
    this.w_ConTot = this.w_ConTot+1
    * --- Delete from INC_AVVE
    i_nConn=i_TableProp[this.INC_AVVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IASERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
             )
    else
      delete from (i_cTable) where;
            IASERIAL = this.w_SERIAL;
            and CPROWNUM = this.w_ROWNUM;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Write into INC_AVVE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.INC_AVVE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_AVVE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IARESINC =IARESINC+ "+cp_ToStrODBC(this.w_IMPINC);
          +i_ccchkf ;
      +" where ";
          +"IASERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          IARESINC = IARESINC + this.w_IMPINC;
          &i_ccchkf. ;
       where;
          IASERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into CON_TENZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COSTATUS ="+cp_NullLink(cp_ToStrODBC("PE"),'CON_TENZ','COSTATUS');
          +i_ccchkf ;
      +" where ";
          +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          COSTATUS = "PE";
          &i_ccchkf. ;
       where;
          COSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_NumCon = this.w_NumCon+1
     
 EndScan
  endproc


  proc Init(oParentObject,pPARAM,pSERIAL)
    this.pPARAM=pPARAM
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CON_TENZ'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='INC_AVVE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM,pSERIAL"
endproc
