* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bi3                                                        *
*              Zoom contenzioso                                                *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-08                                                      *
* Last revis.: 2001-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bi3",oParentObject,m.pPARAM)
return(i_retval)

define class tgsso_bi3 as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciato all'init dalla maschera GSSO_SI1 per poter riempire il cursore per lo zoom
    this.w_ZOOM = this.oParentObject.w_CONPAR
    do case
      case this.pPARAM="INIT"
        * --- Cancello il contenuto dello zoom
        DELETE FROM ( this.w_ZOOM.cCursor )
        SELECT TEMP
        scan
        scatter memvar
        SELECT ( this.w_ZOOM.cCursor )
        append blank
        gather memvar
        SELECT TEMP
        endscan
        * --- Chiudo i cursori
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
      case this.pPARAM="FINI"
        SELECT* FROM ( this.w_ZOOM.cCursor ) INTO CURSOR TEMP
      case this.pPARAM="EXIT"
        UPDATE ( this.w_ZOOM.cCursor ) SET XCHK = 0 WHERE XCHK=1
        * --- Chiudo la Maschera
        This.oParentObject.oParentObject.w_cont="F"
        This.bUpdateParentObject=.f.
        This.oParentObject.bUpdated=.f.
        This.oParentObject.ecpQuit()
      case this.pPARAM="CONT"
        * --- Controllo numero di partite selezionate
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        SELECT count(* ) AS CONT FROM ( this.w_ZOOM.cCursor ) where XCHK=1 INTO CURSOR CONTA
        SELECT CONTA
        if CONT >1
          ah_ErrorMsg("Selezionare una sola partita",,"")
          This.oParentObject.oParentObject.w_cont="F"
        else
          if CONT= 0
            ah_ErrorMsg("Nessuna partita selezionata",,"")
            This.oParentObject.oParentObject.w_cont="F"
          else
            This.oParentObject.oParentObject.w_cont="T"
          endif
        endif
        if USED("CONTA")
          SELECT CONTA
          USE
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
