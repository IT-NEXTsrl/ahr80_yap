* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mia                                                        *
*              Incassi avvenuti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_71]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-19                                                      *
* Last revis.: 2011-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsso_mia")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsso_mia")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsso_mia")
  return

* --- Class definition
define class tgsso_mia as StdPCForm
  Width  = 376
  Height = 231
  Top    = 122
  Left   = 16
  cComment = "Incassi avvenuti"
  cPrg = "gsso_mia"
  HelpContextID=264869225
  add object cnt as tcgsso_mia
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsso_mia as PCContext
  w_IASERIAL = space(10)
  w_RIGA = space(10)
  w_IADATINC = space(8)
  w_IAIMPINC = 0
  w_IATIPPAG = space(10)
  w_TOTINC = 0
  w_IARESINC = 0
  w_IANUMRIF = 0
  w_IAORDRIF = 0
  w_IASERRIF = space(10)
  proc Save(i_oFrom)
    this.w_IASERIAL = i_oFrom.w_IASERIAL
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_IADATINC = i_oFrom.w_IADATINC
    this.w_IAIMPINC = i_oFrom.w_IAIMPINC
    this.w_IATIPPAG = i_oFrom.w_IATIPPAG
    this.w_TOTINC = i_oFrom.w_TOTINC
    this.w_IARESINC = i_oFrom.w_IARESINC
    this.w_IANUMRIF = i_oFrom.w_IANUMRIF
    this.w_IAORDRIF = i_oFrom.w_IAORDRIF
    this.w_IASERRIF = i_oFrom.w_IASERRIF
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_IASERIAL = this.w_IASERIAL
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_IADATINC = this.w_IADATINC
    i_oTo.w_IAIMPINC = this.w_IAIMPINC
    i_oTo.w_IATIPPAG = this.w_IATIPPAG
    i_oTo.w_TOTINC = this.w_TOTINC
    i_oTo.w_IARESINC = this.w_IARESINC
    i_oTo.w_IANUMRIF = this.w_IANUMRIF
    i_oTo.w_IAORDRIF = this.w_IAORDRIF
    i_oTo.w_IASERRIF = this.w_IASERRIF
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsso_mia as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 376
  Height = 231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-28"
  HelpContextID=264869225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  INC_AVVE_IDX = 0
  MOD_PAGA_IDX = 0
  cFile = "INC_AVVE"
  cKeySelect = "IASERIAL"
  cKeyWhere  = "IASERIAL=this.w_IASERIAL"
  cKeyDetail  = "IASERIAL=this.w_IASERIAL"
  cKeyWhereODBC = '"IASERIAL="+cp_ToStrODBC(this.w_IASERIAL)';

  cKeyDetailWhereODBC = '"IASERIAL="+cp_ToStrODBC(this.w_IASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"INC_AVVE.IASERIAL="+cp_ToStrODBC(this.w_IASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INC_AVVE.CPROWNUM '
  cPrg = "gsso_mia"
  cComment = "Incassi avvenuti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IASERIAL = space(10)
  w_RIGA = space(10)
  w_IADATINC = ctod('  /  /  ')
  w_IAIMPINC = 0
  w_IATIPPAG = space(10)
  w_TOTINC = 0
  w_IARESINC = 0
  w_IANUMRIF = 0
  w_IAORDRIF = 0
  w_IASERRIF = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_miaPag1","gsso_mia",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='MOD_PAGA'
    this.cWorkTables[2]='INC_AVVE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INC_AVVE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INC_AVVE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsso_mia'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from INC_AVVE where IASERIAL=KeySet.IASERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2],this.bLoadRecFilter,this.INC_AVVE_IDX,"gsso_mia")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INC_AVVE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INC_AVVE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INC_AVVE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IASERIAL',this.w_IASERIAL  )
      select * from (i_cTable) INC_AVVE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTINC = 0
        .w_IASERIAL = NVL(IASERIAL,space(10))
        .w_IARESINC = NVL(IARESINC,0)
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'INC_AVVE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTINC = 0
      scan
        with this
          .w_RIGA = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_IADATINC = NVL(cp_ToDate(IADATINC),ctod("  /  /  "))
          .w_IAIMPINC = NVL(IAIMPINC,0)
          .w_IATIPPAG = NVL(IATIPPAG,space(10))
          * evitabile
          *.link_2_4('Load')
          .w_IANUMRIF = NVL(IANUMRIF,0)
          .w_IAORDRIF = NVL(IAORDRIF,0)
          .w_IASERRIF = NVL(IASERRIF,space(10))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTINC = .w_TOTINC+.w_IAIMPINC
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_IASERIAL=space(10)
      .w_RIGA=space(10)
      .w_IADATINC=ctod("  /  /  ")
      .w_IAIMPINC=0
      .w_IATIPPAG=space(10)
      .w_TOTINC=0
      .w_IARESINC=0
      .w_IANUMRIF=0
      .w_IAORDRIF=0
      .w_IASERRIF=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        if not(empty(.w_IATIPPAG))
         .link_2_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INC_AVVE')
    this.DoRTCalc(6,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBtn_1_5.enabled = i_bVal
      .Page1.oPag.oObj_3_3.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INC_AVVE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IASERIAL,"IASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IARESINC,"IARESINC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_RIGA C(10);
      ,t_IADATINC D(8);
      ,t_IAIMPINC N(18,4);
      ,t_IATIPPAG C(10);
      ,CPROWNUM N(10);
      ,t_IANUMRIF N(3);
      ,t_IAORDRIF N(4);
      ,t_IASERRIF C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_miabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.controlsource=this.cTrsName+'.t_RIGA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIADATINC_2_2.controlsource=this.cTrsName+'.t_IADATINC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIAIMPINC_2_3.controlsource=this.cTrsName+'.t_IAIMPINC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIATIPPAG_2_4.controlsource=this.cTrsName+'.t_IATIPPAG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(97)
    this.AddVLine(238)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2])
      *
      * insert into INC_AVVE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INC_AVVE')
        i_extval=cp_InsertValODBCExtFlds(this,'INC_AVVE')
        i_cFldBody=" "+;
                  "(IASERIAL,IADATINC,IAIMPINC,IATIPPAG,IARESINC"+;
                  ",IANUMRIF,IAORDRIF,IASERRIF,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IASERIAL)+","+cp_ToStrODBC(this.w_IADATINC)+","+cp_ToStrODBC(this.w_IAIMPINC)+","+cp_ToStrODBCNull(this.w_IATIPPAG)+","+cp_ToStrODBC(this.w_IARESINC)+;
             ","+cp_ToStrODBC(this.w_IANUMRIF)+","+cp_ToStrODBC(this.w_IAORDRIF)+","+cp_ToStrODBC(this.w_IASERRIF)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INC_AVVE')
        i_extval=cp_InsertValVFPExtFlds(this,'INC_AVVE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'IASERIAL',this.w_IASERIAL)
        INSERT INTO (i_cTable) (;
                   IASERIAL;
                  ,IADATINC;
                  ,IAIMPINC;
                  ,IATIPPAG;
                  ,IARESINC;
                  ,IANUMRIF;
                  ,IAORDRIF;
                  ,IASERRIF;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IASERIAL;
                  ,this.w_IADATINC;
                  ,this.w_IAIMPINC;
                  ,this.w_IATIPPAG;
                  ,this.w_IARESINC;
                  ,this.w_IANUMRIF;
                  ,this.w_IAORDRIF;
                  ,this.w_IASERRIF;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty(t_IADATINC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'INC_AVVE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " IARESINC="+cp_ToStrODBC(this.w_IARESINC)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'INC_AVVE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  IARESINC=this.w_IARESINC;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty(t_IADATINC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INC_AVVE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'INC_AVVE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IADATINC="+cp_ToStrODBC(this.w_IADATINC)+;
                     ",IAIMPINC="+cp_ToStrODBC(this.w_IAIMPINC)+;
                     ",IATIPPAG="+cp_ToStrODBCNull(this.w_IATIPPAG)+;
                     ",IARESINC="+cp_ToStrODBC(this.w_IARESINC)+;
                     ",IANUMRIF="+cp_ToStrODBC(this.w_IANUMRIF)+;
                     ",IAORDRIF="+cp_ToStrODBC(this.w_IAORDRIF)+;
                     ",IASERRIF="+cp_ToStrODBC(this.w_IASERRIF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'INC_AVVE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IADATINC=this.w_IADATINC;
                     ,IAIMPINC=this.w_IAIMPINC;
                     ,IATIPPAG=this.w_IATIPPAG;
                     ,IARESINC=this.w_IARESINC;
                     ,IANUMRIF=this.w_IANUMRIF;
                     ,IAORDRIF=this.w_IAORDRIF;
                     ,IASERRIF=this.w_IASERRIF;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty(t_IADATINC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete INC_AVVE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty(t_IADATINC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INC_AVVE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_AVVE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_2_4('Full')
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_IANUMRIF with this.w_IANUMRIF
      replace t_IAORDRIF with this.w_IAORDRIF
      replace t_IASERRIF with this.w_IASERRIF
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IATIPPAG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IATIPPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IATIPPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_IATIPPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_IATIPPAG)
            select MPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IATIPPAG = NVL(_Link_.MPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IATIPPAG = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IATIPPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTINC_3_1.value==this.w_TOTINC)
      this.oPgFrm.Page1.oPag.oTOTINC_3_1.value=this.w_TOTINC
    endif
    if not(this.oPgFrm.Page1.oPag.oIARESINC_3_2.value==this.w_IARESINC)
      this.oPgFrm.Page1.oPag.oIARESINC_3_2.value=this.w_IARESINC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value==this.w_RIGA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value=this.w_RIGA
      replace t_RIGA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATINC_2_2.value==this.w_IADATINC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATINC_2_2.value=this.w_IADATINC
      replace t_IADATINC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATINC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAIMPINC_2_3.value==this.w_IAIMPINC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAIMPINC_2_3.value=this.w_IAIMPINC
      replace t_IAIMPINC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAIMPINC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIATIPPAG_2_4.value==this.w_IATIPPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIATIPPAG_2_4.value=this.w_IATIPPAG
      replace t_IATIPPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIATIPPAG_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'INC_AVVE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if Not Empty(.w_IADATINC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty(t_IADATINC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_RIGA=space(10)
      .w_IADATINC=ctod("  /  /  ")
      .w_IAIMPINC=0
      .w_IATIPPAG=space(10)
      .w_IANUMRIF=0
      .w_IAORDRIF=0
      .w_IASERRIF=space(10)
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_IATIPPAG))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(6,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_RIGA = t_RIGA
    this.w_IADATINC = t_IADATINC
    this.w_IAIMPINC = t_IAIMPINC
    this.w_IATIPPAG = t_IATIPPAG
    this.w_IANUMRIF = t_IANUMRIF
    this.w_IAORDRIF = t_IAORDRIF
    this.w_IASERRIF = t_IASERRIF
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_RIGA with this.w_RIGA
    replace t_IADATINC with this.w_IADATINC
    replace t_IAIMPINC with this.w_IAIMPINC
    replace t_IATIPPAG with this.w_IATIPPAG
    replace t_IANUMRIF with this.w_IANUMRIF
    replace t_IAORDRIF with this.w_IAORDRIF
    replace t_IASERRIF with this.w_IASERRIF
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTINC = .w_TOTINC-.w_iaimpinc
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsso_miaPag1 as StdContainer
  Width  = 372
  height = 231
  stdWidth  = 372
  stdheight = 231
  resizeXpos=282
  resizeYpos=112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_4 as StdButton with uid="ROOUPFXFYC",left=264, top=177, width=48,height=45,;
    CpPicture="bmp\SoldiT.bmp", caption="", nPag=1;
    , HelpContextID = 87159418;
    , TabStop=.f.,Caption='Incasso';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_IASERRIF, .w_IAORDRIF, .w_IANUMRIF,0,0,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_IASERRIF))
    endwith
  endfunc

  func oBtn_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_IASERRIF))
    endwith
   endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="ABKZYHHENU",left=315, top=177, width=48,height=45,;
    CpPicture="bmp\abbina.bmp", caption="", nPag=1;
    , HelpContextID = 125439738;
    , TabStop=.f.,Caption='Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      do GSSO_KIN with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=15, width=355,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="IADATINC",Label1="Data incasso",Field2="IAIMPINC",Label2="Importo incassato",Field3="IATIPPAG",Label3="Tipo pagamento",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51221626

  add object oStr_1_2 as StdString with uid="UUJMPEMRRE",Visible=.t., Left=9, Top=176,;
    Alignment=1, Width=75, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="ZBZDDYGJRY",Visible=.t., Left=9, Top=202,;
    Alignment=1, Width=75, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=37,;
    width=351+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=38,width=350+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTINC_3_1 as StdField with uid="KPTNSHRSKQ",rtseq=6,rtrep=.f.,;
    cFormVar="w_TOTINC",value=0,enabled=.f.,;
    ToolTipText = "Totale incassato",;
    HelpContextID = 127598794,;
    cQueryName = "TOTINC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=88, Top=173, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  add object oIARESINC_3_2 as StdField with uid="RGYQHHDPEZ",rtseq=7,rtrep=.f.,;
    cFormVar="w_IARESINC",value=0,enabled=.f.,;
    ToolTipText = "Residuo incassi",;
    HelpContextID = 21966647,;
    cQueryName = "IARESINC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=140, Left=88, Top=200, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  add object oObj_3_3 as cp_runprogram with uid="EAUIQMSHZE",width=227,height=23,;
   left=95, top=252,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsso_boo",;
    cEvent = "Record Updated,Row deleted",;
    nPag=3;
    , ToolTipText = "Aggiorna residuo";
    , HelpContextID = 86871578
enddefine

* --- Defining Body row
define class tgsso_miaBodyRow as CPBodyRowCnt
  Width=341
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oRIGA_2_1 as StdTrsField with uid="FRCIHFDHET",rtseq=2,rtrep=.t.,;
    cFormVar="w_RIGA",value=space(10),;
    HelpContextID = 260298474,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=2, Width=3, Left=3, Top=0, InputMask=replicate('X',10), BackStyle=0

  add object oIADATINC_2_2 as StdTrsField with uid="TZBJTRZYLM",rtseq=3,rtrep=.t.,;
    cFormVar="w_IADATINC",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data incasso",;
    HelpContextID = 21237559,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=88, Left=-2, Top=-3

  add object oIAIMPINC_2_3 as StdTrsField with uid="WXSARCMQKW",rtseq=4,rtrep=.t.,;
    cFormVar="w_IAIMPINC",value=0,enabled=.f.,;
    ToolTipText = "Importo incassato",;
    HelpContextID = 24624951,;
    cTotal = "this.Parent.oContained.w_totinc", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=138, Left=89, Top=-3, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  add object oIATIPPAG_2_4 as StdTrsField with uid="NBGSJHCPVP",rtseq=5,rtrep=.t.,;
    cFormVar="w_IATIPPAG",value=space(10),enabled=.f.,;
    ToolTipText = "Tipo pagamento",;
    HelpContextID = 175836979,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=106, Left=230, Top=-3, InputMask=replicate('X',10), cLinkFile="MOD_PAGA", cZoomOnZoom="gsar_amp", oKey_1_1="MPCODICE", oKey_1_2="this.w_IATIPPAG"

  func oIATIPPAG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oRIGA_2_1.When()
    return(.t.)
  proc oRIGA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRIGA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mia','INC_AVVE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IASERIAL=INC_AVVE.IASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
