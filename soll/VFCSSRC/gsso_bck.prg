* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bck                                                        *
*              Controlli finali da contenzioso                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-25                                                      *
* Last revis.: 2001-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bck",oParentObject,m.pParam)
return(i_retval)

define class tgsso_bck as StdBatch
  * --- Local variables
  pParam = space(1)
  w_APPO = 0
  w_RECO = 0
  w_nabsrow = 0
  w_nRelrow = 0
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_OSERRIF = space(10)
  w_OORDRIF = 0
  w_ONUMRIF = 0
  w_MESS = space(100)
  w_PADRE = .NULL.
  w_OK = .f.
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli finali nella Manutenzione Contenzioso Lanciato da Gsso_MCO
    *     C: Esegue Controlli Finali
    *     S: Esegue controlli al cambiare dello status
    this.w_OK = .t.
    this.w_APPO = 0
    this.w_PADRE = this.oParentObject
    this.w_PADRE.Markpos()     
    this.w_PADRE.FirstRow()     
    do case
      case this.pParam="C"
        do while Not this.w_PADRE.Eof_Trs()
          if this.w_PADRE.FullRow() 
            if this.oParentObject.w_CODATINT <> this.oParentObject.w_DATINT 
              this.w_SERRIF =  this.w_PADRE.Get("t_COSERRIF") 
              this.w_ORDRIF =  this.w_PADRE.Get("t_COORDRIF") 
              this.w_NUMRIF =  this.w_PADRE.Get("t_CONUMRIF") 
              if this.oParentObject.w_CO__TIPO="M"
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTDATINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATINT),'PAR_TITE','PTDATINT');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                      +" and PTROWORD = "+cp_ToStrODBC(this.w_ORDRIF);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIF);
                         )
                else
                  update (i_cTable) set;
                      PTDATINT = this.oParentObject.w_CODATINT;
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.w_SERRIF;
                      and PTROWORD = this.w_ORDRIF;
                      and CPROWNUM = this.w_NUMRIF;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Leggo riferimenti partita di origine
                * --- Read from PAR_TITE
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PTSERRIF,PTORDRIF,PTNUMRIF"+;
                    " from "+i_cTable+" PAR_TITE where ";
                        +"PTSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                        +" and PTROWORD = "+cp_ToStrODBC(this.w_ORDRIF);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIF);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PTSERRIF,PTORDRIF,PTNUMRIF;
                    from (i_cTable) where;
                        PTSERIAL = this.w_SERRIF;
                        and PTROWORD = this.w_ORDRIF;
                        and CPROWNUM = this.w_NUMRIF;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_OSERRIF = NVL(cp_ToDate(_read_.PTSERRIF),cp_NullValue(_read_.PTSERRIF))
                  this.w_OORDRIF = NVL(cp_ToDate(_read_.PTORDRIF),cp_NullValue(_read_.PTORDRIF))
                  this.w_ONUMRIF = NVL(cp_ToDate(_read_.PTNUMRIF),cp_NullValue(_read_.PTNUMRIF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Aggiorno data nella partita specifica di contabilizzazione dell'insoluto
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTDATINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATINT),'PAR_TITE','PTDATINT');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CORIFCON);
                      +" and PTSERRIF = "+cp_ToStrODBC(this.w_OSERRIF);
                      +" and PTORDRIF = "+cp_ToStrODBC(this.w_OORDRIF);
                      +" and PTNUMRIF = "+cp_ToStrODBC(this.w_ONUMRIF);
                         )
                else
                  update (i_cTable) set;
                      PTDATINT = this.oParentObject.w_CODATINT;
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.oParentObject.w_CORIFCON;
                      and PTSERRIF = this.w_OSERRIF;
                      and PTORDRIF = this.w_OORDRIF;
                      and PTNUMRIF = this.w_ONUMRIF;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            this.w_APPO = this.w_APPO +1
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.Repos()     
        if this.w_APPO=0
          this.w_OK = .F.
          this.w_MESS = ah_Msgformat("Attenzione nessuna partita inserita")
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pParam="S"
        if this.oParentObject.w_OLDSTATUS<>"PE" AND this.oParentObject.w_COSTATUS="PE"
          this.oParentObject.GSSO_MIA.MarkPos()     
          this.oParentObject.GSSO_MIA.FirstRow()     
          do while Not this.oParentObject.GSSO_MIA.Eof_Trs()
            this.w_SERRIF = this.oParentObject.GSSO_MIA.Get("t_IASERRIF") 
            if Nvl(this.oParentObject.GSSO_MIA.Rowstatus(),"N") <>"A" and Empty(this.w_SERRIF)
              this.w_MESS = "Attenzione, per riaprire il sollecito � necessario eliminare incassi avvenuti non correttamente abbinati"
              ah_ErrorMsg(this.w_MESS,,"")
              this.oParentObject.w_COSTATUS = this.oParentObject.w_OLDSTATUS
              EXIT
            endif
            this.oParentObject.GSSO_MIA.NextRow()     
          enddo
          this.oParentObject.GSSO_MIA.RePos()     
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
