* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bsr                                                        *
*              Creazione cursori stampe Remote Banking                         *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-11                                                      *
* Last revis.: 2012-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bsr",oParentObject)
return(i_retval)

define class tgsso_bsr as StdBatch
  * --- Local variables
  w_POSINIZ = 0
  w_POSFIN = 0
  w_INTERVIN = space(3)
  w_INTERVFI = space(3)
  w_ESTREMI = space(80)
  w_CARATTERE = space(1)
  w_PRIMOEXT = .f.
  w_DESCERR = space(200)
  w_DESCERRF = space(254)
  w_DATIEL = space(200)
  w_SOTTOLIN = space(200)
  w_APPSOTLI = space(200)
  w_NUMRIGA = 0
  w_NOMSUP = space(20)
  w_NOMFIL = space(100)
  w_DTARIC = ctod("  /  /  ")
  w_NUMERROR = 0
  w_TIPOFLUS = space(1)
  * --- WorkFile variables
  LOG_REBA_idx=0
  FLU_RITO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampe archivi di Remote Banking (da GSSO_SCF)
    this.w_DESCERRF = ""
    this.w_NUMERROR = 0
    this.w_POSINIZ = 0
    this.w_POSFIN = 0
    this.w_INTERVIN = ""
    this.w_INTERVFI = ""
    this.w_SOTTOLIN = ""
    this.w_APPSOTLI = ""
    this.w_ESTREMI = SPACE(80)
    * --- Variabili utilizzate nel report 
    L_DATCREIN=this.oParentObject.w_DATCREIN
    L_DATCREFI=this.oParentObject.w_DATCREFI
    L_STATO=iif(this.oParentObject.w_TIPOCONF="C","Corretti",iif(this.oParentObject.w_TIPOCONF="E","Errati","Corretti/errati"))
    * --- Diversifico la creazione del cursore a seconda della stampa richiesta (da maschere diverse)
    L_NOMSUPIN=this.oParentObject.w_NOMSUPINF
    L_NOMSUPFI=this.oParentObject.w_NOMSUPFIF
    ah_msg("Creazione cursore stampa flussi operativi di ritorno")
    vq_exec("..\soll\exe\query\gsso_sor.vqr",this,"DATISTA")
    L_TIPOSTA="FLUS"
    if this.oParentObject.w_TIPOCONF="C"
      L_ERRORI=.F.
      Select * from DATISTA into cursor __TMP__
      CP_CHPRN("..\soll\exe\QUERY\gsso_SCF.FRX","",this.oParentObject)
    else
      L_ERRORI=.T.
      Select DATISTA 
 Go Top
      SCAN 
      this.w_DESCERR = ""
      if CRERRORE="S"
        this.w_TIPOFLUS = "F"
        this.w_NOMSUP = CRNOMSUP
        this.w_NOMFIL = CRNOMFIL
        this.w_DTARIC = CRDTARIC
        J=1
        * --- Select from LOG_REBA
        i_nConn=i_TableProp[this.LOG_REBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOG_REBA_idx,2],.t.,this.LOG_REBA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" LOG_REBA ";
              +" where ERNOMSUP="+cp_ToStrODBC(this.w_NOMSUP)+" AND ERDTARIC="+cp_ToStrODBC(this.w_DTARIC)+" AND   ERNOMFIL="+cp_ToStrODBC(this.w_NOMFIL)+" AND ERTIPIMP="+cp_ToStrODBC(this.w_TIPOFLUS)+"";
               ,"_Curs_LOG_REBA")
        else
          select * from (i_cTable);
           where ERNOMSUP=this.w_NOMSUP AND ERDTARIC=this.w_DTARIC AND   ERNOMFIL=this.w_NOMFIL AND ERTIPIMP=this.w_TIPOFLUS;
            into cursor _Curs_LOG_REBA
        endif
        if used('_Curs_LOG_REBA')
          select _Curs_LOG_REBA
          locate for 1=1
          do while not(eof())
          this.w_DESCERR = ERDESERR
          this.w_NUMRIGA = ERNUMRIG
          * --- Leggo la stringa relativa ai dati elaborati
          * --- Read from FLU_RITO
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.FLU_RITO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2],.t.,this.FLU_RITO_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FLDATIEL"+;
              " from "+i_cTable+" FLU_RITO where ";
                  +"FLNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
                  +" and FLDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
                  +" and FLNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIGA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FLDATIEL;
              from (i_cTable) where;
                  FLNOMSUP = this.w_NOMSUP;
                  and FLDTARIC = this.w_DTARIC;
                  and FLNOMFIL = this.w_NOMFIL;
                  and CPROWNUM = this.w_NUMRIGA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DATIEL = NVL(cp_ToDate(_read_.FLDATIEL),cp_NullValue(_read_.FLDATIEL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_LOG_REBA
            continue
          enddo
          use
        endif
      endif
      Select DATISTA
      if EMPTY(this.w_DESCERR)
        this.w_DESCERR = AH_MSGFORMAT ("Il tracciato � corretto ma risulta associato ad altri tracciati errati")
      endif
      REPLACE DESCERR WITH this.w_DESCERR 
      REPLACE SOTTOLIN WITH this.w_SOTTOLIN 
      REPLACE NUMRIGAE WITH this.w_NUMRIGA
      REPLACE CRDATIEL WITH this.w_DATIEL
      * --- Riazzero le varibili utilizzate per la determinazione dell'intervallo in cui � contenuto l'errore
      this.w_INTERVIN = ""
      this.w_INTERVFI = ""
      this.w_SOTTOLIN = ""
      this.w_DESCERRF = ""
      this.w_DATIEL = ""
      this.w_NUMRIGA = 0
      this.w_APPSOTLI = ""
      endscan
      Select * from DATISTA into cursor __TMP__
      CP_CHPRN("..\soll\exe\QUERY\gsso_SCF.FRX","",this.oParentObject)
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice per sottolineare la parte errata
    this.w_PRIMOEXT = .T.
    this.w_INTERVIN = ""
    this.w_INTERVFI = ""
    this.w_POSINIZ = AT("<Posizione",this.w_DESCERR)+10
    this.w_POSFIN = AT(">",this.w_DESCERR)
    if this.w_POSINIZ<>10 AND this.w_POSFIN<>0
      * --- Ricavo la sottostringa in cui � contenuto l'intervallo dei caratteri errati
      this.w_ESTREMI = SUBSTR(this.w_DESCERR,this.w_POSINIZ,this.w_POSFIN-this.w_POSINIZ+1)
      I=1
      this.w_CARATTERE = SUBSTR(this.w_ESTREMI,I,1)
      * --- Estraggo l'estremo o gli estremi in cui � contenuto l'errore
      do while this.w_CARATTERE <> ">"
        do case
          case INLIST(this.w_CARATTERE, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" )
            if this.w_PRIMOEXT
              this.w_INTERVIN = this.w_INTERVIN+this.w_CARATTERE
            else
              this.w_INTERVFI = this.w_INTERVFI+this.w_CARATTERE
            endif
          case INLIST(this.w_CARATTERE,"-")
            this.w_PRIMOEXT = .F.
        endcase
        I=I+1
        * --- Prendo il carattere successivo
        this.w_CARATTERE = SUBSTR(this.w_ESTREMI,I,1)
      enddo
      if EMPTY(this.w_INTERVFI)
        VOLTE=1
      else
        VOLTE=VAL(this.w_INTERVFI)-VAL(this.w_INTERVIN)+1
      endif
      * --- Stringa di sottolineatura errori di appoggio
      this.w_APPSOTLI = this.w_SOTTOLIN
      this.w_SOTTOLIN = SPACE(VAL(this.w_INTERVIN)-1)+REPL("�",VOLTE)
      * --- Creo la stringa delle sottolineature degli errori
      this.w_SOTTOLIN = this.w_APPSOTLI+IIF(J=1,this.w_SOTTOLIN,SUBSTR(this.w_SOTTOLIN,LEN(this.w_APPSOTLI)+1))
      J=J+1
    else
      AH_ERRORMSG("Impossibile determinare la posizione dell'errore per 'nome supporto': %1","","",this.w_NOMSUP)
    endif
    this.w_DESCERRF = this.w_DESCERRF+alltrim(this.w_DESCERR)+chr(13)
    this.w_DESCERR = this.w_DESCERRF
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LOG_REBA'
    this.cWorkTables[2]='FLU_RITO'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_LOG_REBA')
      use in _Curs_LOG_REBA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
