* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bdl                                                        *
*              Cancella supporto da log rem.ban.                               *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-23                                                      *
* Last revis.: 2012-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NOMSUP,w_DTARIC,w_NOMFIL,w_TIPIMP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bdl",oParentObject,m.w_NOMSUP,m.w_DTARIC,m.w_NOMFIL,m.w_TIPIMP)
return(i_retval)

define class tgsso_bdl as StdBatch
  * --- Local variables
  w_NOMSUP = space(20)
  w_DTARIC = ctod("  /  /  ")
  w_NOMFIL = space(100)
  w_TIPIMP = space(1)
  * --- WorkFile variables
  LOG_REBA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione Log Errori associato anagrafica remote banking
    if NOT EMPTY(NVL(this.w_NOMSUP,space(20))) AND NOT EMPTY(NVL(this.w_NOMFIL,space(100))) AND NOT EMPTY(NVL(this.w_DTARIC,cp_CharToDate("  -  -    "))) AND (this.w_TIPIMP="C" OR this.w_TIPIMP="R" OR this.w_TIPIMP="F")
      * --- Delete from LOG_REBA
      i_nConn=i_TableProp[this.LOG_REBA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_REBA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"ERNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
              +" and ERDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
              +" and ERTIPIMP = "+cp_ToStrODBC(this.w_TIPIMP);
              +" and ERNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
               )
      else
        delete from (i_cTable) where;
              ERNOMSUP = this.w_NOMSUP;
              and ERDTARIC = this.w_DTARIC;
              and ERTIPIMP = this.w_TIPIMP;
              and ERNOMFIL = this.w_NOMFIL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_NOMSUP,w_DTARIC,w_NOMFIL,w_TIPIMP)
    this.w_NOMSUP=w_NOMSUP
    this.w_DTARIC=w_DTARIC
    this.w_NOMFIL=w_NOMFIL
    this.w_TIPIMP=w_TIPIMP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOG_REBA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NOMSUP,w_DTARIC,w_NOMFIL,w_TIPIMP"
endproc
