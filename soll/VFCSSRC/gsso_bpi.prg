* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bpi                                                        *
*              Gestione piano contenziosi                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_404]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2014-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bpi",oParentObject,m.pPARAM)
return(i_retval)

define class tgsso_bpi as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_CODCON = space(15)
  w_SCAINI = ctod("  /  /  ")
  w_SCAFIN = ctod("  /  /  ")
  w_TIPPAG = space(2)
  w_TIPCON = space(1)
  w_TIPEFF = space(1)
  w_COCODVAL = space(3)
  w_COCAOVAL = 0
  w_COSERIAL = space(10)
  w_CONUMREG = 0
  w_CO__TIPO = space(1)
  w_COTIPCON = space(1)
  w_CPROWORD = 0
  w_COCODCON = space(15)
  w_CODATREG = ctod("  /  /  ")
  w_COVALNAZ = space(3)
  w_COCODRAG = space(10)
  w_COCOMPET = space(4)
  w_CONTRO = space(15)
  w_COSTATUS = space(2)
  w_CONUMLIV = 0
  w_COSERRIF = space(10)
  w_COORDRIF = 0
  w_CONUMRIF = 0
  w_COCODBAN = space(15)
  w_CODRAG = space(10)
  w_NUMCON = 0
  w_NOTE = space(0)
  w_AZIONE = space(8)
  w_CPROWNUM = 0
  w_RIFOCN = space(10)
  w_CODAZI = space(5)
  w_CODCLI = space(15)
  w_COUNT = 0
  w_NUMDIS = space(10)
  w_PTNUMDIS = space(10)
  w_PTSERIAL = space(10)
  w_DATSCA1 = ctod("  /  /  ")
  w_NUMPAR1 = space(31)
  w_DATSCA = ctod("  /  /  ")
  w_FLINDI = space(1)
  w_OK = space(1)
  w_ADDSPE = space(1)
  w_ADDINT = space(1)
  w_ADDSOL = space(1)
  w_IMPSPE = 0
  w_PERINT = 0
  w_GPSAINTC = space(1)
  w_PAGMOR = space(5)
  w_DURMOR = 0
  w_DATMOR = ctod("  /  /  ")
  w_LPAGMOR = space(5)
  w_GESCON = space(5)
  w_PAGPAR = space(5)
  w_ANSPRINT = space(1)
  w_ANSAGINT = 0
  w_NSBANC = space(15)
  w_BANAPP = space(10)
  w_FLBANC = space(1)
  w_DATVAL = ctod("  /  /  ")
  w_LSEGNO = space(1)
  w_LSEGNO = space(1)
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_LTOTIMP = 0
  w_LDATSCA = ctod("  /  /  ")
  w_LNUMPAR = space(31)
  w_PTFLRAGG = space(1)
  w_TIPO = space(1)
  w_DESSPE = space(1)
  w_RIFDIS1 = space(10)
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_ISERIAL = space(10)
  w_IROWORD = 0
  w_IROWNUM = 0
  w_DATINT = ctod("  /  /  ")
  w_OKRIF = .f.
  w_BANPRE = space(15)
  w_PTCODRAG = space(10)
  w_TCONTRO = space(15)
  w_TCODBAN = space(15)
  w_PTNUMCOR = space(25)
  w_FNSBANC = space(15)
  w_PCDESSUP = space(0)
  w_ZNUMPAR = space(31)
  w_ZDATSCA = ctod("  /  /  ")
  w_ZSEGNO = space(8)
  w_ZTOTIMP = 0
  w_ZNUMDOC = 0
  w_ZALFDOC = space(10)
  w_ZDATDOC = ctod("  /  /  ")
  w_ZPTSER = space(10)
  w_ZPTROW = 0
  w_ZROWNUM = 0
  w_ZCAOVAL = 0
  w_ZPTNUMDIS = space(10)
  w_ZCODCON = space(15)
  w_ZCODVAL = space(5)
  w_ZFLRAGG = space(1)
  w_ZTIPPAG = space(2)
  w_ZDINUMERO = 0
  w_ZGESCON = space(1)
  w_ZPAGPAR = space(5)
  w_ZSPRINT = space(1)
  w_ZSAGINT = 0
  w_ZBANRIF = space(15)
  w_ZNUMDIS = space(10)
  w_ZFLINDI = space(1)
  w_ZBANNOS = space(15)
  w_ZBANAPP = space(10)
  w_ZFLBANC = space(1)
  w_ZOFLRAGG = space(1)
  w_ZPTDESRIG = space(50)
  w_ZPTSERRIF = space(10)
  w_ZPTROWRIF = 0
  w_ZNUMRIF = 0
  w_ZDATREG = ctod("  /  /  ")
  w_ZPCODRAG = space(10)
  w_ZNUMCOR = space(25)
  w_ZANDESCRI = space(60)
  w_RTIPCON = space(1)
  w_RCODCON = space(15)
  w_RDATSCA = ctod("  /  /  ")
  w_RNUMDIS = space(10)
  w_LNUMDOC = 0
  w_LALFDOC = space(10)
  w_LDATDOC = ctod("  /  /  ")
  w_LSERIAL = space(10)
  w_OK = .f.
  w_LDATREG = ctod("  /  /  ")
  w_DREG = ctod("  /  /  ")
  * --- WorkFile variables
  CONDTENZ_idx=0
  CONTI_idx=0
  CON_TENZ_idx=0
  DIS_TINT_idx=0
  PAR_CONT_idx=0
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  TMP_PIANOCONT_idx=0
  SCA_VARI_idx=0
  TMP_GESPIAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Maschera gestione Piano Insoluti (da GSSO_KPI - Eventi OK, w_SELEZI Changed)
    this.w_TIPO = This.oparentobject.oparentobject.w_GP__TIPO
    this.w_TIPEFF = This.oparentobject.oparentobject.w_GPTIPEFF
    this.w_BANPRE = This.oparentobject.oparentobject.w_GPBANPRE
    do case
      case this.pParam="PIAN"
        * --- Assegno valori per l'inserimento degli insoluti
        this.w_COCODRAG = This.oparentobject.oparentobject.w_GPCODRAG
        this.w_COCOMPET = This.oparentobject.oparentobject.w_GPCOMPET
        if EMPTY(this.w_COCODRAG) OR EMPTY(this.w_COCOMPET)
          ah_ErrorMsg("Piano insoluti non definito",,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PAGMOR = This.oparentobject.oparentobject.w_PAGMOR
        this.w_DURMOR = This.oparentobject.oparentobject.w_DURMOR
        this.w_DESSPE = This.oparentobject.oparentobject.w_GPDESSPE
        this.w_CO__TIPO = this.w_TIPO
        this.w_COTIPCON = "C"
        this.w_CODATREG = this.oParentObject.w_DATREG
        this.w_COVALNAZ = g_PERVAL
        this.w_COSTATUS = "PE"
        this.w_CONUMLIV = 0
        this.w_CODAZI = i_CODAZI
        * --- Leggo Contropartita di riapertura
        * --- Read from PAR_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PCCONTRO,PCDESSUP"+;
            " from "+i_cTable+" PAR_CONT where ";
                +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PCCONTRO,PCDESSUP;
            from (i_cTable) where;
                PCCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONTRO = NVL(cp_ToDate(_read_.PCCONTRO),cp_NullValue(_read_.PCCONTRO))
          this.w_PCDESSUP = NVL(cp_ToDate(_read_.PCDESSUP),cp_NullValue(_read_.PCDESSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_COUNT = 0
        this.w_ADDSPE = This.oparentobject.oparentobject.w_GPADDSPE
        this.w_ADDINT = This.oparentobject.oparentobject.w_GPADDINT
        this.w_ADDSOL = This.oparentobject.oparentobject.w_GPADDSOL
        this.w_GPSAINTC = This.oparentobject.oparentobject.w_GPSAINTC
        this.w_IMPSPE = This.oparentobject.oparentobject.w_GPIMPSPE
        this.w_DATVAL = NVL(This.oparentobject.oparentobject.w_GPDATVAL,cp_CharToDate("  -  -    "))
         
 Select * From ( this.oParentObject.w_CONPAR.cCursor ) Where XCHK=1 Into Cursor Temp NoFilter 
 Select Temp 
 Go Top
        do while not Eof( "Temp" )
          this.w_NOTE = SPACE(0)
          this.w_COCODCON = TEMP.PTCODCON
          this.w_GESCON = NVL(TEMP.ANGESCON, "N")
          this.w_PAGPAR = NVL(TEMP.ANPAGPAR, "     ")
          this.w_ANSPRINT = NVL(TEMP.ANSPRINT, "N")
          this.w_ANSAGINT = NVL(TEMP.ANSAGINT, 0)
          this.w_COCODVAL = TEMP.PTCODVAL
          this.w_COCAOVAL = This.oparentobject.oparentobject.w_GPCAOVAL
          this.w_SERRIF = NVL(TEMP.PTSERRIF,SPACE(10))
          this.w_ORDRIF = NVL(TEMP.PTORDRIF,0)
          this.w_NUMRIF = NVL(TEMP.PTNUMRIF,0)
          this.w_FLINDI = NVL(FLINDI," ")
          this.w_TIPPAG = NVL(TEMP.MPTIPPAG,"  ")
          this.w_LTIPCON = this.w_COTIPCON
          this.w_LCODCON = this.w_COCODCON
          this.w_LTOTIMP = IIF(this.w_TIPO="M",NVL(TEMP.PTTOTIMP,0),0)
          this.w_PTCODRAG = IIF(this.w_FLINDI<>"S",NVL(TEMP.PTCODRAG,Space(10)),Space(10))
          * --- Gestione Autonumber Contenzioso
          this.w_COSERIAL = SPACE(10)
          this.w_CONUMREG = 0
          i_Conn=i_TableProp[this.CON_TENZ_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEINS", "i_codazi,w_COSERIAL")
          cp_NextTableProg(this, i_Conn, "PRINS", "i_codazi,w_COCOMPET,w_CONUMREG")
          Wait Clear
          * --- Inserisco nuovo insoluto
          * --- Se ho attivato il flag "Saggio da anagrafica cliente", il cliente non ha settato 
          *     il flag "Spread su saggio di mora" ed ha un tasso di interesse diverso da 0
          *     applico questo tasso sul contenzioso
          if this.w_Gpsaintc="S" And this.w_Ansprint="N" And this.w_Ansagint<>0
            this.w_PERINT = this.w_Ansagint
          else
            this.w_PERINT = This.oparentobject.oparentobject.w_GPPERINT
          endif
          this.w_COUNT = this.w_COUNT+1
          * --- Se Tipo Effetto 
          *     Applico Ns banca della distinta se esite una distinta associata altrimenti applico 
          *     Ns banca sulla partita, se vuota applico Ns dai parametri contenzioso
          *     Se tipo no effetto
          *     Applico Ns banco della partita se vuota Ns banca dei parametri
          this.w_NSBANC = NVL(TEMP.PTBANNOS,SPACE(15))
          this.w_CONTRO = IIF(this.w_TIPO="M",this.w_NSBANC,this.w_CONTRO)
          if this.w_TIPEFF="E"
            if NOT EMPTY(NVL(this.w_BANPRE, SPACE(15)))
              this.w_TCODBAN = this.w_BANPRE
              this.w_COCODBAN = this.w_BANPRE
            else
              this.w_TCODBAN = IIF(Not Empty(NVL(TEMP.BANRIF, SPACE(15))),NVL(TEMP.BANRIF, SPACE(15)),this.w_CONTRO)
              this.w_TCONTRO = SPACE(15)
              this.w_COCODBAN = IIF(Not Empty(NVL(TEMP.BANRIF, SPACE(15))),NVL(TEMP.BANRIF, SPACE(15)),this.w_CONTRO)
            endif
          else
            this.w_TCODBAN = SPACE(15)
            this.w_TCONTRO = this.w_CONTRO
            this.w_COCODBAN = this.w_CONTRO
          endif
          this.w_DATINT = CP_TODATE(TEMP.PTDATSCA)
          * --- Insert into CON_TENZ
          i_nConn=i_TableProp[this.CON_TENZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TENZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"COSERIAL"+",CONUMREG"+",COCOMPET"+",CODATREG"+",COTIPCON"+",COCODCON"+",COCATPAG"+",COVALNAZ"+",COCODVAL"+",COSTATUS"+",CONUMLIV"+",COCODRAG"+",CO__TIPO"+",COCAOVAL"+",COADDSPE"+",COADDINT"+",COADDSOL"+",COIMPSPE"+",COPERINT"+",COBANPRE"+",CODESSPE"+",COTIPEFF"+",COCONTRO"+",CODATINT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_COSERIAL),'CON_TENZ','COSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONUMREG),'CON_TENZ','CONUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCOMPET),'CON_TENZ','COCOMPET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODATREG),'CON_TENZ','CODATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COTIPCON),'CON_TENZ','COTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCODCON),'CON_TENZ','COCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPPAG),'CON_TENZ','COCATPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COVALNAZ),'CON_TENZ','COVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCODVAL),'CON_TENZ','COCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COSTATUS),'CON_TENZ','COSTATUS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CONUMLIV),'CON_TENZ','CONUMLIV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCODRAG),'CON_TENZ','COCODRAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CO__TIPO),'CON_TENZ','CO__TIPO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCAOVAL),'CON_TENZ','COCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ADDSPE),'CON_TENZ','COADDSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ADDINT),'CON_TENZ','COADDINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ADDSOL),'CON_TENZ','COADDSOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSPE),'CON_TENZ','COIMPSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PERINT),'CON_TENZ','COPERINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TCODBAN),'CON_TENZ','COBANPRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSPE),'CON_TENZ','CODESSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPEFF),'CON_TENZ','COTIPEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TCONTRO),'CON_TENZ','COCONTRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATINT),'CON_TENZ','CODATINT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'COSERIAL',this.w_COSERIAL,'CONUMREG',this.w_CONUMREG,'COCOMPET',this.w_COCOMPET,'CODATREG',this.w_CODATREG,'COTIPCON',this.w_COTIPCON,'COCODCON',this.w_COCODCON,'COCATPAG',this.w_TIPPAG,'COVALNAZ',this.w_COVALNAZ,'COCODVAL',this.w_COCODVAL,'COSTATUS',this.w_COSTATUS,'CONUMLIV',this.w_CONUMLIV,'COCODRAG',this.w_COCODRAG)
            insert into (i_cTable) (COSERIAL,CONUMREG,COCOMPET,CODATREG,COTIPCON,COCODCON,COCATPAG,COVALNAZ,COCODVAL,COSTATUS,CONUMLIV,COCODRAG,CO__TIPO,COCAOVAL,COADDSPE,COADDINT,COADDSOL,COIMPSPE,COPERINT,COBANPRE,CODESSPE,COTIPEFF,COCONTRO,CODATINT &i_ccchkf. );
               values (;
                 this.w_COSERIAL;
                 ,this.w_CONUMREG;
                 ,this.w_COCOMPET;
                 ,this.w_CODATREG;
                 ,this.w_COTIPCON;
                 ,this.w_COCODCON;
                 ,this.w_TIPPAG;
                 ,this.w_COVALNAZ;
                 ,this.w_COCODVAL;
                 ,this.w_COSTATUS;
                 ,this.w_CONUMLIV;
                 ,this.w_COCODRAG;
                 ,this.w_CO__TIPO;
                 ,this.w_COCAOVAL;
                 ,this.w_ADDSPE;
                 ,this.w_ADDINT;
                 ,this.w_ADDSOL;
                 ,this.w_IMPSPE;
                 ,this.w_PERINT;
                 ,this.w_TCODBAN;
                 ,this.w_DESSPE;
                 ,this.w_TIPEFF;
                 ,this.w_TCONTRO;
                 ,this.w_DATINT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          * --- Leggo nelle distinte la banca di presentazione
          this.w_DATSCA = CP_TODATE(TEMP.PTDATSCA)
          this.w_LDATSCA = this.w_DATSCA
          this.w_NUMDIS = NVL(TEMP.NUMDIS," ")
          this.w_PTNUMDIS = NVL(TEMP.PTNUMDIS," ")
          this.w_CODCLI = TEMP.PTCODCON
          this.w_BANAPP = NVL(TEMP.PTBANAPP,SPACE(10))
          this.w_FLBANC = NVL(TEMP.FLBANC," ")
          this.w_PTNUMCOR = NVL(TEMP.PTNUMCOR," ")
          this.w_FNSBANC = iCase(this.w_FLBANC="A",this.w_BANAPP,this.w_FLBANC="S",NVL(TEMP.PTBANNOS,SPACE(15)),Space(10))
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Select Temp
          * --- Scrivo i riferimenti
          if Not Empty(this.w_PCDESSUP)
            * --- Array elenco parametri per descrizioni di riga e testata
             
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_CODATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_COCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]=alltrim(this.w_NOTE) 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(this.w_LNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_DATSCA) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_COTIPCON
            this.w_NOTE = CALDESPA(this.w_PCDESSUP,@ARPARAM,.t.)
          endif
          this.w_DATVAL = IIF(NOT EMPTY(this.w_DATVAL),this.w_DATVAL,this.w_DATSCA)
          * --- Write into CON_TENZ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CON_TENZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CON_TENZ','CO__NOTE');
            +",CODATVAL ="+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CON_TENZ','CODATVAL');
                +i_ccchkf ;
            +" where ";
                +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
                   )
          else
            update (i_cTable) set;
                CO__NOTE = this.w_NOTE;
                ,CODATVAL = this.w_DATVAL;
                &i_ccchkf. ;
             where;
                COSERIAL = this.w_COSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Aggiornamento parametri contenzioso nell' Anagrafica Clienti 
          do case
            case this.w_GESCON="B"
              * --- Inserisci Blocco contenzioso
              * --- Write into CONTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ANFLGCON ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCON');
                    +i_ccchkf ;
                +" where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_COTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_COCODCON);
                       )
              else
                update (i_cTable) set;
                    ANFLGCON = "S";
                    &i_ccchkf. ;
                 where;
                    ANTIPCON = this.w_COTIPCON;
                    and ANCODICE = this.w_COCODCON;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_GESCON="M"
              * --- Aggiorno Data Moratoria
              this.w_DATMOR = this.w_CODATREG+this.w_DURMOR
              this.w_LPAGMOR = IIF(EMPTY(this.w_PAGPAR), this.w_PAGMOR, this.w_PAGPAR)
              * --- Write into CONTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ANPAGPAR ="+cp_NullLink(cp_ToStrODBC(this.w_LPAGMOR),'CONTI','ANPAGPAR');
                +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(this.w_DATMOR),'CONTI','ANDATMOR');
                    +i_ccchkf ;
                +" where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_COTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_COCODCON);
                       )
              else
                update (i_cTable) set;
                    ANPAGPAR = this.w_LPAGMOR;
                    ,ANDATMOR = this.w_DATMOR;
                    &i_ccchkf. ;
                 where;
                    ANTIPCON = this.w_COTIPCON;
                    and ANCODICE = this.w_COCODCON;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
          endcase
          if Not Eof("Temp")
             
 Select Temp 
 Skip
          endif
        enddo
        if RECCOUNT("TEMP")>0
          * --- Conferma il Piano
          This.oParentObject.oParentObject.NotifyEvent("Legge")
          This.oparentobject.oparentobject.ecpSave()
          * --- Chiudo la Maschera
          This.oParentObject.ecpQuit()
        else
          ah_ErrorMsg("Non ci sono insoluti da generare",,"")
        endif
        * --- Chiudo il Cursore
        if used("TEMP")
          select TEMP
          use
        endif
      case this.Pparam="INIT"
        * --- Inizializzo contenuto cursore zoom
        this.w_TIPCON = This.oparentobject.oparentobject.w_GPTIPCON
        this.w_SCAINI = CP_TODATE(This.oparentobject.oparentobject.w_GPSCAINI)
        this.w_SCAFIN = CP_TODATE(This.oparentobject.oparentobject.w_GPSCAFIN)
        this.w_CODCON = NVL(This.oparentobject.oparentobject.w_GPCODCON,SPACE(15))
        this.w_BANPRE = NVL(This.oparentobject.oparentobject.w_GPBANPRE,SPACE(15))
        this.w_TIPPAG = This.oparentobject.oparentobject.w_TIPPAG
        if this.w_TIPO="I"
          if this.w_TIPEFF="E"
            * --- Create temporary table TMP_GESPIAN
            i_nIdx=cp_AddTableDef('TMP_GESPIAN') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\SOLL\EXE\QUERY\GSSOC1QIN',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMP_GESPIAN_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            Vq_Exec("..\SOLL\EXE\Query\GSSO_GPI.vqr",This,"SOLL")
            * --- Drop temporary table TMP_GESPIAN
            i_nIdx=cp_GetTableDefIdx('TMP_GESPIAN')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMP_GESPIAN')
            endif
          else
            Vq_Exec("..\SOLL\EXE\Query\GSSO2GPI.vqr",This,"SOLL")
          endif
        else
          * --- Creo nel temporaneo l'elenco delle partite aperte
          * --- Create temporary table TMP_PIANOCONT
          i_nIdx=cp_AddTableDef('TMP_PIANOCONT') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\SOLL\EXE\QUERY\GSSO_BPI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMP_PIANOCONT_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Se cerco Mancato pagamento filtro solo le scadenze Aperte.
          Vq_Exec("..\SOLL\EXE\Query\GSSOMGPI.vqr",This,"SOLL")
          * --- Drop temporary table TMP_PIANOCONT
          i_nIdx=cp_GetTableDefIdx('TMP_PIANOCONT')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP_PIANOCONT')
          endif
        endif
        * --- Adeguo cursore per applicare batch di calcolo del residuo
         
 Select PTDATSCA AS DATSCA,PTNUMPAR AS NUMPAR,PT_SEGNO AS SEGNO,PTTOTIMP AS TOTIMP,PTNUMDOC,; 
 PTALFDOC,PTDATDOC,PTSERIAL,PTROWORD,CPROWNUM,PTCAOVAL AS CAOVAL,PTNUMDIS,PTCODCON AS CODCON,PTCODVAL AS CODVAL,; 
 FLRAGG,MPTIPPAG AS TIPPAG,DINUMERO,ANGESCON,ANPAGPAR,ANSPRINT,ANSAGINT,; 
 BANRIF,NUMDIS,FLINDI AS OFLINDI,PTBANNOS AS BANNOS,; 
 PTBANAPP AS BANAPP,FLBANC,PTFLRAGG AS OFLRAGG,PTDESRIG,NUMPRO,PTSERRIF,PTORDRIF,PTNUMRIF,; 
 CAOAPE,IMPDAR,IMPAVE,PTFLINDI AS FLINDI,NFLRAGG AS PTFLRAGG,TIPCON,FLCRSA,MODPAG,TIPDOC,DATREG,PTCODRAG,PTNUMCOR,ANDESCRI From SOLL Into Cursor SOLL NoFilter
        =wrcursor("SOLL")
        if this.w_TIPO="M"
          GSTE_BCP(this,"S","SOLL"," " , , , .T. )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Per disporre di un insieme di dati ordinabili utilizzo un temporaneo che 
          *     riempio riga per riga con il contenuto del risultato dell'elaborazione
          *     partite aperte, come primo passo lo creo tramite una query vuota copia di
          *     GSSOMGPI
        endif
        * --- Create temporary table TMP_PIANOCONT
        i_nIdx=cp_AddTableDef('TMP_PIANOCONT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\SOLL\EXE\QUERY\GSSONGPI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_PIANOCONT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
         
 Select Soll 
 Go Top
        do while .not. eof("SOLL")
          * --- Escludo dalle partite aperte quelle con pagamento diverso o non di creazione
          if ( (TIPPAG=this.w_TIPPAG OR EMPTY(this.w_TIPPAG)) AND FLCRSA="C" ) OR this.w_TIPO="I"
            this.w_ZNUMPAR = Soll.Numpar
            this.w_ZDATSCA = CP_TODATE(Soll.Datsca)
            this.w_ZSEGNO = "D"
            this.w_ZTOTIMP = IIF(this.w_TIPO="I",Nvl(Soll.Totimp,0),IIF(Soll.segno="A",-Abs(Nvl(Soll.Totimp,0)),Abs(Nvl(Soll.Totimp,0))))
            this.w_ZNUMDOC = Nvl(Soll.ptnumdoc,0)
            this.w_ZALFDOC = Nvl(Soll.PTALFDOC,Space(10))
            this.w_ZDATDOC = cp_todate(Soll.PTDATDOC)
            this.w_ZPTSER = Soll.Ptserial
            this.w_ZPTROW = Soll.ptroword
            this.w_ZROWNUM = Soll.CpRowNum
            this.w_ZCAOVAL = Nvl(Soll.Caoval,0)
            this.w_ZPTNUMDIS = Nvl(Soll.Ptnumdis,Space(10))
            this.w_ZCODCON = Soll.CodCon
            this.w_ZCODVAL = Soll.CodVal
            this.w_ZFLRAGG = Nvl(Soll.Flragg," ")
            this.w_ZTIPPAG = Nvl(Soll.Tippag,"  ")
            this.w_ZDINUMERO = Nvl(Soll.Dinumero,0)
            this.w_ZGESCON = Nvl(Soll.Angescon," ")
            this.w_ZSPRINT = Nvl(Soll.Ansprint,"N")
            this.w_ZSAGINT = Nvl(Soll.Ansagint,0)
            this.w_ZPAGPAR = Nvl(Soll.Anpagpar,Space(5))
            this.w_ZBANRIF = Nvl(Soll.Banrif,Space(15))
            this.w_ZNUMDIS = Nvl(Soll.NumDis,Space(10))
            this.w_ZFLINDI = Nvl(Soll.Oflindi," ")
            this.w_ZBANNOS = Nvl(Soll.Bannos,Space(15))
            this.w_ZBANAPP = Nvl(Soll.Banapp,Space(10))
            this.w_ZFLBANC = Nvl(Soll.Flbanc," ")
            this.w_ZOFLRAGG = Nvl(Soll.Oflragg," ")
            this.w_ZPTDESRIG = Soll.Ptdesrig
            this.w_ZPTSERRIF = Nvl(Soll.Ptserrif,Space(10))
            this.w_ZPTROWRIF = Nvl(Soll.Ptordrif,0)
            this.w_ZNUMRIF = Nvl(Soll.Ptnumrif,0)
            this.w_ZDATREG = CP_TODATE(Soll.Datreg)
            this.w_ZPCODRAG = Nvl(Soll.Ptcodrag,"          ")
            this.w_ZNUMCOR = Nvl(Soll.PTNUMCOR,SPACE(25))
            this.w_ZANDESCRI = Nvl(Soll.ANDESCRI,SPACE(60))
            * --- Inserisco nel temporaneo
            * --- Insert into TMP_PIANOCONT
            i_nConn=i_TableProp[this.TMP_PIANOCONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PIANOCONT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PIANOCONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PTNUMPAR"+",PTDATSCA"+",PT_SEGNO"+",PTTOTIMP"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTCAOVAL"+",PTNUMDIS"+",PTCODCON"+",PTCODVAL"+",FLRAGG"+",MPTIPPAG"+",DINUMERO"+",ANGESCON"+",ANPAGPAR"+",BANRIF"+",NUMDIS"+",FLINDI"+",PTBANNOS"+",PTBANAPP"+",FLBANC"+",PTFLRAGG"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",DATREG"+",PTCODRAG"+",PTNUMCOR"+",ANDESCRI"+",ANSPRINT"+",ANSAGINT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_ZNUMPAR),'TMP_PIANOCONT','PTNUMPAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZDATSCA),'TMP_PIANOCONT','PTDATSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZSEGNO),'TMP_PIANOCONT','PT_SEGNO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZTOTIMP),'TMP_PIANOCONT','PTTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZNUMDOC),'TMP_PIANOCONT','PTNUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZALFDOC),'TMP_PIANOCONT','PTALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZDATDOC),'TMP_PIANOCONT','PTDATDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTSER),'TMP_PIANOCONT','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTROW),'TMP_PIANOCONT','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZROWNUM),'TMP_PIANOCONT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZCAOVAL),'TMP_PIANOCONT','PTCAOVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTNUMDIS),'TMP_PIANOCONT','PTNUMDIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZCODCON),'TMP_PIANOCONT','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZCODVAL),'TMP_PIANOCONT','PTCODVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZFLRAGG),'TMP_PIANOCONT','FLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZTIPPAG),'TMP_PIANOCONT','MPTIPPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZDINUMERO),'TMP_PIANOCONT','DINUMERO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZGESCON),'TMP_PIANOCONT','ANGESCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPAGPAR),'TMP_PIANOCONT','ANPAGPAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZBANRIF),'TMP_PIANOCONT','BANRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZNUMDIS),'TMP_PIANOCONT','NUMDIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZFLINDI),'TMP_PIANOCONT','FLINDI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZBANNOS),'TMP_PIANOCONT','PTBANNOS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZBANAPP),'TMP_PIANOCONT','PTBANAPP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZFLBANC),'TMP_PIANOCONT','FLBANC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZOFLRAGG),'TMP_PIANOCONT','PTFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTDESRIG),'TMP_PIANOCONT','PTDESRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTSERRIF),'TMP_PIANOCONT','PTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPTROWRIF),'TMP_PIANOCONT','PTORDRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZNUMRIF),'TMP_PIANOCONT','PTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZDATREG),'TMP_PIANOCONT','DATREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZPCODRAG),'TMP_PIANOCONT','PTCODRAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZNUMCOR),'TMP_PIANOCONT','PTNUMCOR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZANDESCRI),'TMP_PIANOCONT','ANDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZSPRINT),'TMP_PIANOCONT','ANSPRINT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ZSAGINT),'TMP_PIANOCONT','ANSAGINT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PTNUMPAR',this.w_ZNUMPAR,'PTDATSCA',this.w_ZDATSCA,'PT_SEGNO',this.w_ZSEGNO,'PTTOTIMP',this.w_ZTOTIMP,'PTNUMDOC',this.w_ZNUMDOC,'PTALFDOC',this.w_ZALFDOC,'PTDATDOC',this.w_ZDATDOC,'PTSERIAL',this.w_ZPTSER,'PTROWORD',this.w_ZPTROW,'CPROWNUM',this.w_ZROWNUM,'PTCAOVAL',this.w_ZCAOVAL,'PTNUMDIS',this.w_ZPTNUMDIS)
              insert into (i_cTable) (PTNUMPAR,PTDATSCA,PT_SEGNO,PTTOTIMP,PTNUMDOC,PTALFDOC,PTDATDOC,PTSERIAL,PTROWORD,CPROWNUM,PTCAOVAL,PTNUMDIS,PTCODCON,PTCODVAL,FLRAGG,MPTIPPAG,DINUMERO,ANGESCON,ANPAGPAR,BANRIF,NUMDIS,FLINDI,PTBANNOS,PTBANAPP,FLBANC,PTFLRAGG,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,DATREG,PTCODRAG,PTNUMCOR,ANDESCRI,ANSPRINT,ANSAGINT &i_ccchkf. );
                 values (;
                   this.w_ZNUMPAR;
                   ,this.w_ZDATSCA;
                   ,this.w_ZSEGNO;
                   ,this.w_ZTOTIMP;
                   ,this.w_ZNUMDOC;
                   ,this.w_ZALFDOC;
                   ,this.w_ZDATDOC;
                   ,this.w_ZPTSER;
                   ,this.w_ZPTROW;
                   ,this.w_ZROWNUM;
                   ,this.w_ZCAOVAL;
                   ,this.w_ZPTNUMDIS;
                   ,this.w_ZCODCON;
                   ,this.w_ZCODVAL;
                   ,this.w_ZFLRAGG;
                   ,this.w_ZTIPPAG;
                   ,this.w_ZDINUMERO;
                   ,this.w_ZGESCON;
                   ,this.w_ZPAGPAR;
                   ,this.w_ZBANRIF;
                   ,this.w_ZNUMDIS;
                   ,this.w_ZFLINDI;
                   ,this.w_ZBANNOS;
                   ,this.w_ZBANAPP;
                   ,this.w_ZFLBANC;
                   ,this.w_ZOFLRAGG;
                   ,this.w_ZPTDESRIG;
                   ,this.w_ZPTSERRIF;
                   ,this.w_ZPTROWRIF;
                   ,this.w_ZNUMRIF;
                   ,this.w_ZDATREG;
                   ,this.w_ZPCODRAG;
                   ,this.w_ZNUMCOR;
                   ,this.w_ZANDESCRI;
                   ,this.w_ZSPRINT;
                   ,this.w_ZSAGINT;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          if Not Eof( "SOLL" )
             
 Select Soll 
 Skip
          endif
        enddo
        if Used("SOLL")
          Select SOLL
          Use
        endif
        if this.w_TIPO="M"
          ah_Msg("Elimino partite in precedenti insoluti contabilizzati e/o gi� in insoluto",.T.)
          * --- Delete from TMP_PIANOCONT
          i_nConn=i_TableProp[this.TMP_PIANOCONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_PIANOCONT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
                  +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with '..\SOLL\EXE\QUERY\GSSO11QIN',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        this.oParentObject.w_CONPAR.cCpQueryName = "..\SOLL\EXE\Query\GSSO10QIN"
        This.oParentObject.notifyevent("Esegui")
      case this.Pparam="SELE"
        UPDATE ( this.oParentObject.w_CONPAR.cCursor ) SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
      case this.Pparam="DONE"
        * --- Drop temporary table TMP_PIANOCONT
        i_nIdx=cp_GetTableDefIdx('TMP_PIANOCONT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PIANOCONT')
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determino le partite raggruppate
    this.w_RTIPCON = this.w_COTIPCON
    this.w_RCODCON = this.w_CODCLI
    this.w_RDATSCA = this.w_DATSCA
    this.w_RNUMDIS = IIF(this.w_FLINDI="S",Space(10),this.w_NUMDIS)
    * --- Select from GSSORPAR
    do vq_exec with 'GSSORPAR',this,'_Curs_GSSORPAR','',.f.,.t.
    if used('_Curs_GSSORPAR')
      select _Curs_GSSORPAR
      locate for 1=1
      do while not(eof())
      this.w_SERRIF = NVL(_Curs_GSSORPAR.PTSERRIF,SPACE(10))
      this.w_ORDRIF = NVL(_Curs_GSSORPAR.PTORDRIF,0)
      this.w_NUMRIF = NVL(_Curs_GSSORPAR.PTNUMRIF,0)
      if this.w_FLINDI<>"S"
        this.w_PTSERIAL = this.w_PTNUMDIS
      else
        * --- Filtro sulla distinta nel caso di Contabilizzazione indiretta
        this.w_PTSERIAL = _Curs_GSSORPAR.PTSERIAL
        this.w_PTCODRAG = NVL(TEMP.PTCODRAG,Space(10))
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNRIFDIS"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNRIFDIS;
            from (i_cTable) where;
                PNSERIAL = this.w_PTSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RIFDIS1 = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if VAL(this.w_RIFDIS1)<0
          * --- Filtro sulla distinta nel caso di Contabilizzazione indiretta
          this.w_DATSCA1 = CP_TODATE(_Curs_GSSORPAR.PTDATSCA)
          this.w_NUMPAR1 = _Curs_GSSORPAR.PTNUMPAR
          * --- Cerco partita fittizia generata dal caricamento di una distinta
          if EMPTY(this.w_SERRIF)
            * --- Gestito pregresso privo di riferimenti
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                  +" where PTROWORD=-2 AND PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR1)+" AND PTDATSCA="+cp_ToStrODBC(this.w_DATSCA1)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCLI)+" AND PTTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+"";
                   ,"_Curs_PAR_TITE")
            else
              select * from (i_cTable);
               where PTROWORD=-2 AND PTNUMPAR=this.w_NUMPAR1 AND PTDATSCA=this.w_DATSCA1 AND PTCODCON=this.w_CODCLI AND PTTIPCON=this.w_COTIPCON;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              * --- Il codice raggruppamento �  valorizzato solo nelle partite da distinta ptroword =-2 devo quindi eseguire
              *     in questo punto il filtro 
              if Nvl(_Curs_PAR_TITE.PTCODRAG,Space(10))=this.w_PTCODRAG
                this.w_PTSERIAL = _Curs_PAR_TITE.PTSERIAL
                exit
              endif
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
          else
            * --- Risalgo alla partita di creazione fittizzia con PTROWORD=-3
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTROWORD,CPROWNUM"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTROWORD = "+cp_ToStrODBC(-3);
                    +" and PTSERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                    +" and PTORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTROWORD,CPROWNUM;
                from (i_cTable) where;
                    PTROWORD = -3;
                    and PTSERRIF = this.w_SERRIF;
                    and PTORDRIF = this.w_ORDRIF;
                    and PTNUMRIF = this.w_NUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ISERIAL = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.w_IROWORD = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
              this.w_IROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Determino partita della distinta con PTROWORD=-2
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                  +" where PTROWORD=-2 AND PTSERRIF="+cp_ToStrODBC(this.w_ISERIAL)+" AND PTORDRIF="+cp_ToStrODBC(this.w_IROWORD)+" AND PTNUMRIF="+cp_ToStrODBC(this.w_IROWNUM)+"";
                   ,"_Curs_PAR_TITE")
            else
              select * from (i_cTable);
               where PTROWORD=-2 AND PTSERRIF=this.w_ISERIAL AND PTORDRIF=this.w_IROWORD AND PTNUMRIF=this.w_IROWNUM;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              * --- Il codice raggruppamento �  valorizzato solo nelle partite da distinta ptroword =-2 devo quindi eseguire
              *     in questo punto il filtro 
              if Nvl(_Curs_PAR_TITE.PTCODRAG,Space(10))=this.w_PTCODRAG
                this.w_PTSERIAL = _Curs_PAR_TITE.PTSERIAL
                exit
              endif
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
          endif
        else
          this.w_PTSERIAL = SPACE(10)
        endif
      endif
      if this.w_PTSERIAL=this.w_PTNUMDIS
        this.w_CPROWNUM = this.w_CPROWNUM+1
        this.w_CPROWORD = this.w_CPROWORD+10
        this.w_CONUMRIF = _Curs_GSSORPAR.CPROWNUM
        this.w_COSERRIF = _Curs_GSSORPAR.PTSERIAL
        this.w_COORDRIF = _Curs_GSSORPAR.PTROWORD
        this.w_LNUMPAR = _Curs_GSSORPAR.PTNUMPAR
        this.w_LSEGNO = _Curs_GSSORPAR.PT_SEGNO
        this.w_PTFLRAGG = NVL(_Curs_GSSORPAR.PTFLRAGG," ")
        this.w_OKRIF = .F.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.w_OKRIF
          if NOT EMPTY(_Curs_GSSORPAR.PTDATDOC)
            this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(_Curs_GSSORPAR.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(_Curs_GSSORPAR.PTALFDOC,Space(10))), " / "+Alltrim(_Curs_GSSORPAR.PTALFDOC), ""), DTOC(_Curs_GSSORPAR.PTDATDOC) )
          else
            this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(NVL(_Curs_GSSORPAR.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(_Curs_GSSORPAR.PTALFDOC,Space(10))), " / "+Alltrim(_Curs_GSSORPAR.PTALFDOC), ""))
          endif
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
        select _Curs_GSSORPAR
        continue
      enddo
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riferimenti delle partite di origine nel caso fossero state raggruppate manualmente nella Manutenzione partite.
    this.w_LSEGNO = "D"
    this.w_LNUMDOC = 0
    this.w_LALFDOC = Space(10)
    this.w_LDATDOC = CP_TODATE("01-01-1960")
    this.w_LSERIAL = "ZZZYYYXXXK"
    this.w_OK = .F.
    if EMPTY(this.w_SERRIF)
      * --- Partite appartenenti al pregresso - prendo la prima
      vq_exec("..\SOLL\EXE\QUERY\GSSORBPI.VQR",this,"PAR_TITE")
      if RECCOUNT() >0
        GO TOP
        this.w_LSERIAL = PTSERIAL
        this.w_PTFLRAGG = IIF(this.w_TIPEFF="A",NVL(PTFLRAGG," "),this.w_PTFLRAGG)
        if this.w_PTFLRAGG="S"
          if this.w_LSERIAL<>"ZZZYYYXXXK"
            vq_exec("..\SOLL\EXE\QUERY\GSSOSBPI.VQR",this,"APPO")
          endif
        endif
      endif
      if USED("PAR_TITE")
        select PAR_TITE
        USE
      endif
    else
      vq_exec("..\SOLL\EXE\QUERY\GSSOSQIN.VQR",this,"APPO")
    endif
    if USED("APPO")
      if RECCOUNT("APPO") >0
        SELECT APPO
        GO TOP
        SCAN
        if (this.w_LNUMDOC <> Nvl(PTNUMDOC,0)) OR (this.w_LALFDOC <> Nvl(PTALFDOC,Space(10))) 
          this.w_OK = .T.
        else
          this.w_OK = .F.
        endif
        this.w_LNUMDOC = NVL(PTNUMDOC,0)
        this.w_LALFDOC = NVL(PTALFDOC,Space(10))
        this.w_LDATDOC = CP_TODATE(PTDATDOC)
        if this.w_OK
          this.w_OKRIF = .T.
          if NOT EMPTY(this.w_LDATDOC)
            this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(this.w_LNUMDOC,15)) + IIF(NOT EMPTY(this.w_LALFDOC), " / "+Alltrim(this.w_LALFDOC), ""), DTOC(this.w_LDATDOC) )
          else
            this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(this.w_LNUMDOC,15)) + IIF(NOT EMPTY(this.w_LALFDOC), " / "+Alltrim(this.w_LALFDOC), "") )
          endif
        endif
        ENDSCAN
      endif
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if TEMP.FLRAGG$"ST" AND this.w_TIPEFF<>"A" AND this.w_TIPO="I"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_CPROWNUM = this.w_CPROWNUM+1
      this.w_CPROWORD = this.w_CPROWORD+10
      this.w_LNUMPAR = TEMP.PTNUMPAR
      this.w_LSEGNO = TEMP.PT_SEGNO
      this.w_PTFLRAGG = NVL(TEMP.PTFLRAGG," ")
      this.w_OKRIF = .F.
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not this.w_OKRIF
        if NOT EMPTY(CP_TODATE(temp.PTDATDOC))
          this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(temp.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(temp.PTALFDOC," ")), " / "+Alltrim(temp.PTALFDOC), ""), DTOC(CP_TODATE(temp.PTDATDOC)) )
        else
          this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(NVL(temp.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(temp.PTALFDOC," ")), " / "+Alltrim(temp.PTALFDOC), "") )
        endif
      endif
      * --- Determino riferimenti dell'ultima partita di incasso
      *     associata alla partita nel contenzioso (solo se insoluto da mancato pagamento)
      *     Negli altri casi i campi li ho gia nelle query
      if this.w_TIPO="M"
        this.w_LDATREG = cp_CharToDate("  /  /   ")
        this.w_COSERRIF = iif(Not Empty(this.w_SERRIF),this.w_SERRIF,TEMP.PTSERIAL)
        this.w_COORDRIF = iif(Not Empty(this.w_ORDRIF),this.w_ORDRIF,TEMP.PTROWORD)
        this.w_CONUMRIF = iif(Not Empty(this.w_NUMRIF),this.w_NUMRIF,TEMP.CPROWNUM)
        * --- Select from GSSO4GPI
        do vq_exec with 'GSSO4GPI',this,'_Curs_GSSO4GPI','',.f.,.t.
        if used('_Curs_GSSO4GPI')
          select _Curs_GSSO4GPI
          locate for 1=1
          do while not(eof())
          do case
            case this.w_COORDRIF>0
              * --- Read from PNT_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PNDATREG"+;
                  " from "+i_cTable+" PNT_MAST where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.w_COSERRIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PNDATREG;
                  from (i_cTable) where;
                      PNSERIAL = this.w_COSERRIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case this.w_COORDRIF=-1
              * --- Read from SCA_VARI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.SCA_VARI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2],.t.,this.SCA_VARI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SCDATREG"+;
                  " from "+i_cTable+" SCA_VARI where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_COSERRIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SCDATREG;
                  from (i_cTable) where;
                      SCCODICE = this.w_COSERRIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DREG = NVL(cp_ToDate(_read_.SCDATREG),cp_NullValue(_read_.SCDATREG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
          endcase
          if this.w_DREG> this.w_LDATREG
            this.w_LDATREG = this.w_DREG
            this.w_COSERRIF = _Curs_GSSO4GPI.PTSERIAL
            this.w_COORDRIF = _Curs_GSSO4GPI.PTROWORD
            this.w_CONUMRIF = _Curs_GSSO4GPI.CPROWNUM
          endif
            select _Curs_GSSO4GPI
            continue
          enddo
          use
        endif
      else
        this.w_COSERRIF = TEMP.PTSERIAL
        this.w_COORDRIF = TEMP.PTROWORD
        this.w_CONUMRIF = TEMP.CPROWNUM
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco nel dettaglio del contenzioso i dati delle partite
    * --- Insert into CONDTENZ
    i_nConn=i_TableProp[this.CONDTENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONDTENZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COSERIAL"+",CPROWNUM"+",COSERRIF"+",COORDRIF"+",CONUMRIF"+",COCODBAN"+",CPROWORD"+",COTOTIMP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COSERIAL),'CONDTENZ','COSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CONDTENZ','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COSERRIF),'CONDTENZ','COSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COORDRIF),'CONDTENZ','COORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONUMRIF),'CONDTENZ','CONUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COCODBAN),'CONDTENZ','COCODBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CONDTENZ','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LTOTIMP),'CONDTENZ','COTOTIMP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COSERIAL',this.w_COSERIAL,'CPROWNUM',this.w_CPROWNUM,'COSERRIF',this.w_COSERRIF,'COORDRIF',this.w_COORDRIF,'CONUMRIF',this.w_CONUMRIF,'COCODBAN',this.w_COCODBAN,'CPROWORD',this.w_CPROWORD,'COTOTIMP',this.w_LTOTIMP)
      insert into (i_cTable) (COSERIAL,CPROWNUM,COSERRIF,COORDRIF,CONUMRIF,COCODBAN,CPROWORD,COTOTIMP &i_ccchkf. );
         values (;
           this.w_COSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_COSERRIF;
           ,this.w_COORDRIF;
           ,this.w_CONUMRIF;
           ,this.w_COCODBAN;
           ,this.w_CPROWORD;
           ,this.w_LTOTIMP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CONDTENZ'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CON_TENZ'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='PAR_CONT'
    this.cWorkTables[6]='PAR_TITE'
    this.cWorkTables[7]='PNT_MAST'
    this.cWorkTables[8]='*TMP_PIANOCONT'
    this.cWorkTables[9]='SCA_VARI'
    this.cWorkTables[10]='*TMP_GESPIAN'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSSORPAR')
      use in _Curs_GSSORPAR
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_GSSO4GPI')
      use in _Curs_GSSO4GPI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
