* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bik                                                        *
*              Chiude insoluti\mancati pagamenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_114]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-23                                                      *
* Last revis.: 2009-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bik",oParentObject,m.pPARAM)
return(i_retval)

define class tgsso_bik as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_PROG = .NULL.
  w_PADRE = .NULL.
  w_SERIAL = space(10)
  w_STATUS = space(1)
  w_SEGNO_RES = space(1)
  w_IASERIAL = space(10)
  w_NUMPAR = 0
  w_TOTPAR = 0
  w_OBJCT = .NULL.
  * --- WorkFile variables
  CON_TENZ_idx=0
  VALUTE_idx=0
  TMP_GESPIAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -SELE:   Evento w_SELEZI Changed Seleziona/Deselezione la Righe Dettaglio (se consentito)
    *     -CREA:  Inserisce righe incassi nel caso di abbinamento manuale
    *     -INIT:     Crea zoom incassi filtrato per contenzioso, nel caso di bottone abbina manuale
    ND = this.oParentObject.w_CONINS.cCursor
    do case
      case this.pPARAM="SELE"
        * --- Evento w_SELEZI Changed
        *     Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if USED(this.oParentObject.w_Conins.cCursor)
          * --- Selezionati gli insoluti da chiudere
          if this.oParentObject.w_SELEZI = "S"
            * --- Seleziona Tutto
            UPDATE (ND) SET XCHK=1 Where XCHK=0
          else
            * --- deseleziona Tutto
            UPDATE (ND) SET XCHK=0 WHERE XCHK=1
          endif
        endif
      case this.pPARAM="CREA"
        Select (ND)
        COUNT FOR xChk=1 TO this.w_NUMPAR
        if this.w_NUMPAR = 0
          ah_ErrorMsg("Attenzione: selezionare almeno un insoluto da chiudere","!","")
        else
          * --- Chisura degli insoluti con residuo nullo selezionati
          *     Inserisce righe incassi nel caso di abbinamento manuale
          this.w_PADRE = This.oparentobject.oparentobject.oparentobject
          this.w_PROG = This.oparentobject.oparentobject
          this.w_PROG.Markpos()     
           
 Select PNDATREG,PTMODPAG,PTSERIAL,PTROWORD,CPROWNUM,PTTOTIMP,TOTPAR,PT_SEGNO,RESIDUO; 
 FROM (ND) WHERE XCHK=1 INTO CURSOR TEMP NOFILTER
          this.w_PROG.w_IARESINC = IIF(this.w_PROG.w_IARESINC=0,Nvl( TEMP.RESIDUO,0),this.w_PROG.w_IARESINC)
          this.w_TOTPAR = Nvl( TEMP.TOTPAR,0)
           
 Select TEMP 
 Go Top 
 Scan
          this.w_PROG.Addrow()     
          this.w_SEGNO_RES = IIF ( Nvl( TEMP.TOTPAR , 0 )<0 , "D","A" )
          this.w_PROG.w_IADATINC = CP_TODATE(TEMP.PNDATREG)
          this.w_PROG.w_IASERRIF = TEMP.PTSERIAL
          this.w_PROG.w_IAORDRIF = TEMP.PTROWORD
          this.w_PROG.w_IANUMRIF = TEMP.CPROWNUM
          this.w_PROG.w_IAIMPINC = IIF(NVL(TEMP.PT_SEGNO," ")="D",-NVL(TEMP.PTTOTIMP,0),NVL(TEMP.PTTOTIMP,0))
          this.w_PROG.w_IATIPPAG = TEMP.PTMODPAG
          this.w_PROG.w_IARESINC = this.w_PROG.w_IARESINC-this.w_PROG.w_IAIMPINC
          this.w_PROG.Saverow()     
          endscan
          this.w_PROG.w_TOTINC = this.w_TOTPAR- this.w_PROG.w_IARESINC
          if this.w_PROG.w_IARESINC=0 AND this.w_PADRE.w_COSTATUS<>"CI"
            if ah_YesNo("Attenzione, insoluto incassato totalmente: aggiorno status?")
              this.w_OBJCT = this.w_PADRE.GetCtrl( "w_COSTATUS" )
              this.w_OBJCT.value = 2
              this.w_PADRE.w_COSTATUS = "CI"
              this.w_PADRE.bHeaderUpdated = .T.
            endif
          endif
          this.w_PROG.Repos()     
          if USED("TEMP")
             
 Select TEMP 
 Use
          endif
        endif
      case this.pPARAM="INIT"
        * --- Crea zoom incassi filtrato per contenzioso, nel caso di bottone abbina manuale
        this.w_PADRE = This.oparentobject.oparentobject
        this.w_PROG = This.oparentobject.oparentobject.oparentobject
        this.w_IASERIAL = this.w_PROG.w_COSERIAL
        * --- Create temporary table TMP_GESPIAN
        i_nIdx=cp_AddTableDef('TMP_GESPIAN') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSSO_QAS_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_GESPIAN_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_PROG = This.oparentobject
        this.oParentObject.w_CONINS.cCpQueryName = "GSSOAQAS"
        this.w_PROG.Notifyevent("Esegui")     
        this.w_PADRE.Markpos()     
        this.w_PADRE.Exec_Select("_Tmp_Sq_","Nvl(t_IASERRIF,Space(10))+Str(nvl(t_IAORDRIF,0),4)+Str(nvl(t_IANUMRIF,0),3) As CHIAVE"," i_srv='A' And Not Deleted()  ","","","")     
        this.w_PADRE.Repos()     
         
 Delete From (ND) Where Nvl(PTSERIAL,Space(10))+Str(nvl(PTROWORD,0),4)+Str(nvl(CPROWNUM,0),3) ; 
 In (Select CHIAVE From _Tmp_Sq_ ) or Nvl(COSERIAL,Space(10))<>this.w_IASERIAL
        this.oParentObject.w_CONINS.cCpQueryName = "..\SOLL\EXE\QUERY\GSSO_KIN"
         
 Select (ND) 
 Go Top
        if USED("_Tmp_Sq_")
           
 Select _Tmp_Sq_ 
 Use
        endif
        * --- Drop temporary table TMP_GESPIAN
        i_nIdx=cp_GetTableDefIdx('TMP_GESPIAN')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_GESPIAN')
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CON_TENZ'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='*TMP_GESPIAN'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
