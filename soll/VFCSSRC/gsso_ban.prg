* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_ban                                                        *
*              Riassegna banca di presentazione negli insoluti                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-07-23                                                      *
* Last revis.: 2001-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_ban",oParentObject,m.pParam)
return(i_retval)

define class tgsso_ban as StdBatch
  * --- Local variables
  pParam = space(4)
  w_NabsRow = 0
  w_NRelRow = 0
  w_RECO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se pParametro = BANC   ARiassegna la stessa banca\Contropartita di Riapertura per ogni riga dell'insoluto
    * --- Se pParametro = DELE azzera temporaneo al variare del campo COTIPEFF
    * --- Lanciato da GSSO_MCO
    do case
      case this.pParam="BANC"
        * --- Aggiorna Contropartita di Riapertura e Conto Banche nel dettaglio
        this.w_RECO = RECCOUNT(this.oParentObject.cTrsName)
        this.w_NabsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_NRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
        SELECT (this.oParentObject.cTrsName)
        GO TOP
        SCAN FOR NOT EMPTY(NVL(t_CONUMRIF,SPACE(15)))
        * --- Legge i Dati del Temporaneo
        this.oParentObject.WorkFromTrs()
        this.oParentObject.SaveDependsOn()
        this.oParentObject.w_COCODBAN = IIF(this.oParentObject.w_COTIPEFF="E",this.oParentObject.w_COBANPRE,this.oParentObject.w_COCONTRO)
        this.oParentObject.w_CODBAN = this.oParentObject.w_COBANPRE
        * --- Carica il Temporaneo dei Dati
        this.oParentObject.TrsFromWork()
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        ENDSCAN
      case this.pParam="DELE"
        * --- Sbianca Temporaneo al campiare del tipo effetto.
        SELECT (this.oParentObject.cTrsName)
        GO TOP
        DELETE ALL
    endcase
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SaveDependsOn()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NabsRow
    .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRelRow
    EndWith
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
