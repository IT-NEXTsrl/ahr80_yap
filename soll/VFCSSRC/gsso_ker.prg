* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_ker                                                        *
*              Aggiorna scadenze errate a esito                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-05                                                      *
* Last revis.: 2013-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_ker",oParentObject))

* --- Class definition
define class tgsso_ker as StdForm
  Top    = 6
  Left   = 9

  * --- Standard Properties
  Width  = 678
  Height = 517
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-24"
  HelpContextID=65639785
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  DIS_TINT_IDX = 0
  COC_MAST_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsso_ker"
  cComment = "Aggiorna scadenze errate a esito"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FRTIPCON = space(1)
  w_FRCODCON = space(15)
  w_FRDATSCA = ctod('  /  /  ')
  w_FRTOTIMP = space(10)
  w_FRNUMEFF = space(10)
  w_FRRIFDIS = space(10)
  w_FRFLGERR = space(1)
  w_TIPCLF = space(1)
  w_FRCFISDE = space(16)
  w_CODCLF = space(15)
  w_CODFIS = space(16)
  w_DATSCA = ctod('  /  /  ')
  w_IMPSCA = 0
  w_DINUMDIS = space(10)
  w_NUMEFF = 0
  w_DESCRI = space(40)
  w_PARTSN = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_NUMERO = 0
  w_ANNO = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_TIPDIS = space(2)
  w_NEWNUMEFF = 0
  w_NEWIMPSCA = 0
  w_NEWDATSCA = ctod('  /  /  ')
  w_NEWTIPCLF = space(1)
  w_NEWCODCLF = space(15)
  w_NEWDESCRI = space(40)
  w_NEWCODFIS = space(15)
  w_NEWNUMDIS = space(10)
  w_NEWNUMERO = 0
  w_NEWANNO = space(4)
  w_NEWDATDIS = ctod('  /  /  ')
  w_NEWDIBANRIF = space(15)
  w_FRNUMCOR = space(15)
  w_NEWPARIVA = space(12)
  w_NEWDIBADES = space(35)
  w_BADESCRI = space(35)
  w_FRTIPFLU = space(2)
  w_PERFIS = space(1)
  w_DATOBSN = ctod('  /  /  ')
  w_NOFLGBEN = space(1)
  w_NEWBCODFIS = space(16)
  w_NEWBSOGGET = space(2)
  w_NEWBPARIVA = space(12)
  w_GESPAR = space(1)
  w_FLPAR = space(1)
  w_ANDESCRI = space(40)
  w_DINUMERO = 0
  w_DI__ANNO = space(4)
  w_DIDATDIS = ctod('  /  /  ')
  w_SelEff = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_kerPag1","gsso_ker",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCLF_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SelEff = this.oPgFrm.Pages(1).oPag.SelEff
    DoDefault()
    proc Destroy()
      this.w_SelEff = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DIS_TINT'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='OFF_NOMI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FRTIPCON=space(1)
      .w_FRCODCON=space(15)
      .w_FRDATSCA=ctod("  /  /  ")
      .w_FRTOTIMP=space(10)
      .w_FRNUMEFF=space(10)
      .w_FRRIFDIS=space(10)
      .w_FRFLGERR=space(1)
      .w_TIPCLF=space(1)
      .w_FRCFISDE=space(16)
      .w_CODCLF=space(15)
      .w_CODFIS=space(16)
      .w_DATSCA=ctod("  /  /  ")
      .w_IMPSCA=0
      .w_DINUMDIS=space(10)
      .w_NUMEFF=0
      .w_DESCRI=space(40)
      .w_PARTSN=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_NUMERO=0
      .w_ANNO=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_TIPDIS=space(2)
      .w_NEWNUMEFF=0
      .w_NEWIMPSCA=0
      .w_NEWDATSCA=ctod("  /  /  ")
      .w_NEWTIPCLF=space(1)
      .w_NEWCODCLF=space(15)
      .w_NEWDESCRI=space(40)
      .w_NEWCODFIS=space(15)
      .w_NEWNUMDIS=space(10)
      .w_NEWNUMERO=0
      .w_NEWANNO=space(4)
      .w_NEWDATDIS=ctod("  /  /  ")
      .w_NEWDIBANRIF=space(15)
      .w_FRNUMCOR=space(15)
      .w_NEWPARIVA=space(12)
      .w_NEWDIBADES=space(35)
      .w_BADESCRI=space(35)
      .w_FRTIPFLU=space(2)
      .w_PERFIS=space(1)
      .w_DATOBSN=ctod("  /  /  ")
      .w_NOFLGBEN=space(1)
      .w_NEWBCODFIS=space(16)
      .w_NEWBSOGGET=space(2)
      .w_NEWBPARIVA=space(12)
      .w_GESPAR=space(1)
      .w_FLPAR=space(1)
      .w_ANDESCRI=space(40)
      .w_DINUMERO=0
      .w_DI__ANNO=space(4)
      .w_DIDATDIS=ctod("  /  /  ")
      .w_FRTIPCON=oParentObject.w_FRTIPCON
      .w_FRCODCON=oParentObject.w_FRCODCON
      .w_FRDATSCA=oParentObject.w_FRDATSCA
      .w_FRTOTIMP=oParentObject.w_FRTOTIMP
      .w_FRNUMEFF=oParentObject.w_FRNUMEFF
      .w_FRRIFDIS=oParentObject.w_FRRIFDIS
      .w_FRFLGERR=oParentObject.w_FRFLGERR
      .w_FRCFISDE=oParentObject.w_FRCFISDE
      .w_PARTSN=oParentObject.w_PARTSN
      .w_FRNUMCOR=oParentObject.w_FRNUMCOR
      .w_BADESCRI=oParentObject.w_BADESCRI
      .w_FRTIPFLU=oParentObject.w_FRTIPFLU
      .w_ANDESCRI=oParentObject.w_ANDESCRI
      .w_DINUMERO=oParentObject.w_DINUMERO
      .w_DI__ANNO=oParentObject.w_DI__ANNO
      .w_DIDATDIS=oParentObject.w_DIDATDIS
      .oPgFrm.Page1.oPag.SelEff.Calculate()
          .DoRTCalc(1,7,.f.)
        .w_TIPCLF = .w_FRTIPCON
          .DoRTCalc(9,9,.f.)
        .w_CODCLF = .w_FRCODCON
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODCLF))
          .link_1_11('Full')
        endif
        .w_CODFIS = .w_FRCFISDE
        .w_DATSCA = .w_FRDATSCA
        .w_IMPSCA = .w_FRTOTIMP
        .w_DINUMDIS = .w_FRRIFDIS
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DINUMDIS))
          .link_1_15('Full')
        endif
        .w_NUMEFF = .w_FRNUMEFF
          .DoRTCalc(16,18,.f.)
        .w_OBTEST = this.oParentObject .w_FRDTARIC
        .w_NUMERO = this.oParentObject .w_DINUMERO
        .w_ANNO = this.oParentObject .w_DI__ANNO
          .DoRTCalc(22,22,.f.)
        .w_TIPDIS = icase(.w_FRTIPFLU='IB', 'RB', .w_FRTIPFLU='IR', 'RI', .w_FRTIPFLU='IM', 'MA', '  ')
        .w_NEWNUMEFF = .w_SelEff.getVar('NUMEFF')
        .w_NEWIMPSCA = .w_SelEff.getVar('TOTIMP')
        .w_NEWDATSCA = cp_todate(.w_SelEff.getVar('DATSCA'))
        .w_NEWTIPCLF = .w_SelEff.getVar('TIPCON')
        .w_NEWCODCLF = .w_SelEff.getVar('CODCON')
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_NEWCODCLF))
          .link_1_39('Full')
        endif
          .DoRTCalc(29,30,.f.)
        .w_NEWNUMDIS = .w_SelEff.getVar('PTSERIAL')
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_NEWNUMDIS))
          .link_1_45('Full')
        endif
          .DoRTCalc(32,34,.f.)
        .w_NEWDIBANRIF = .w_SelEff.getVar('BANNOS')
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_NEWDIBANRIF))
          .link_1_55('Full')
        endif
    endwith
    this.DoRTCalc(36,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gsso_ker
    this.NotifyEvent('SalvaEffetto')
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FRTIPCON=.w_FRTIPCON
      .oParentObject.w_FRCODCON=.w_FRCODCON
      .oParentObject.w_FRDATSCA=.w_FRDATSCA
      .oParentObject.w_FRTOTIMP=.w_FRTOTIMP
      .oParentObject.w_FRNUMEFF=.w_FRNUMEFF
      .oParentObject.w_FRRIFDIS=.w_FRRIFDIS
      .oParentObject.w_FRFLGERR=.w_FRFLGERR
      .oParentObject.w_FRCFISDE=.w_FRCFISDE
      .oParentObject.w_PARTSN=.w_PARTSN
      .oParentObject.w_FRNUMCOR=.w_FRNUMCOR
      .oParentObject.w_BADESCRI=.w_BADESCRI
      .oParentObject.w_FRTIPFLU=.w_FRTIPFLU
      .oParentObject.w_ANDESCRI=.w_ANDESCRI
      .oParentObject.w_DINUMERO=.w_DINUMERO
      .oParentObject.w_DI__ANNO=.w_DI__ANNO
      .oParentObject.w_DIDATDIS=.w_DIDATDIS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.SelEff.Calculate()
        .DoRTCalc(1,13,.t.)
          .link_1_15('Full')
        .DoRTCalc(15,23,.t.)
            .w_NEWNUMEFF = .w_SelEff.getVar('NUMEFF')
            .w_NEWIMPSCA = .w_SelEff.getVar('TOTIMP')
            .w_NEWDATSCA = cp_todate(.w_SelEff.getVar('DATSCA'))
            .w_NEWTIPCLF = .w_SelEff.getVar('TIPCON')
            .w_NEWCODCLF = .w_SelEff.getVar('CODCON')
          .link_1_39('Full')
        .DoRTCalc(29,30,.t.)
            .w_NEWNUMDIS = .w_SelEff.getVar('PTSERIAL')
          .link_1_45('Full')
        .DoRTCalc(32,34,.t.)
            .w_NEWDIBANRIF = .w_SelEff.getVar('BANNOS')
          .link_1_55('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(36,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.SelEff.Calculate()
    endwith
  return

  proc Calculate_NZAJVLTKZO()
    with this
          * --- Elimina distinta
          .w_DINUMDIS = space(10)
          .w_NUMERO = 0
          .w_ANNO = space(4)
          .w_DATDIS = cp_chartodate('  -  -    ')
    endwith
  endproc
  proc Calculate_SOBGOIKJTI()
    with this
          * --- Inizializza variabili nuove
          .w_NEWTIPCLF = .w_TIPCLF
          .w_NEWCODCLF = .w_CODCLF
          .w_NEWDATSCA = cp_CharToDate('  -  -    ')
          .w_NEWIMPSCA = 0
          .w_NEWNUMEFF = 0
          .w_NEWNUMDIS = space(10)
    endwith
  endproc
  proc Calculate_GWSMUELXYD()
    with this
          * --- Salva effetto
          .w_FRTIPCON = .w_NEWTIPCLF
          .w_FRCODCON = .w_NEWCODCLF
          .w_FRDATSCA = .w_NEWDATSCA
          .w_FRTOTIMP = .w_NEWIMPSCA
          .w_FRNUMEFF = .w_NEWNUMEFF
          .w_FRRIFDIS = .w_NEWNUMDIS
          .w_FRNUMCOR = .w_NEWDIBANRIF
          .w_FRFLGERR = ''
          .w_BADESCRI = .w_NEWDIBADES
          .w_FRCFISDE = iif(.w_FRTIPFLU='IB' AND .w_PERFIS='S',iif(empty(nvl(.w_NEWCODFIS,'')),.w_NEWPARIVA,.w_NEWCODFIS), iif(empty(nvl(.w_NEWPARIVA,'')),.w_NEWCODFIS,.w_NEWPARIVA) )
          .w_PARTSN = .w_FLPAR
          .w_ANDESCRI = .w_NEWDESCRI
          .w_DINUMERO = .w_NEWNUMERO
          .w_DI__ANNO = .w_NEWANNO
          .w_DIDATDIS = .w_NEWDATDIS
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.SelEff.Event(cEvent)
        if lower(cEvent)==lower("EliminaDistinta")
          .Calculate_NZAJVLTKZO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_SOBGOIKJTI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SalvaEffetto")
          .Calculate_GWSMUELXYD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLF
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_11'),i_cWhere,'',"CLIENTE / FORNITORE / GENERICO",'CONTISALD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto o non gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_GESPAR = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_GESPAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_GESPAR='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto o non gestito a partite")
        endif
        this.w_CODCLF = space(15)
        this.w_DESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_GESPAR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DINUMDIS
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_lTable = "DIS_TINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2], .t., this.DIS_TINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DINUMDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DINUMDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DINUMDIS',this.w_DINUMDIS)
            select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DINUMDIS = NVL(_Link_.DINUMDIS,space(10))
      this.w_ANNO = NVL(_Link_.DI__ANNO,space(4))
      this.w_NUMERO = NVL(_Link_.DINUMERO,0)
      this.w_DATDIS = NVL(cp_ToDate(_Link_.DIDATDIS),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DINUMDIS = space(10)
      endif
      this.w_ANNO = space(4)
      this.w_NUMERO = 0
      this.w_DATDIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])+'\'+cp_ToStr(_Link_.DINUMDIS,1)
      cp_ShowWarn(i_cKey,this.DIS_TINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DINUMDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWCODCLF
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODFIS,ANPARIVA,ANPERFIS,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_NEWCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NEWTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_NEWTIPCLF;
                       ,'ANCODICE',this.w_NEWCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODFIS,ANPARIVA,ANPERFIS,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_NEWDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_NEWCODFIS = NVL(_Link_.ANCODFIS,space(15))
      this.w_NEWPARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_PERFIS = NVL(_Link_.ANPERFIS,space(1))
      this.w_FLPAR = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWCODCLF = space(15)
      endif
      this.w_NEWDESCRI = space(40)
      this.w_NEWCODFIS = space(15)
      this.w_NEWPARIVA = space(12)
      this.w_PERFIS = space(1)
      this.w_FLPAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWNUMDIS
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_lTable = "DIS_TINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2], .t., this.DIS_TINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWNUMDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWNUMDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS,DIBANRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DINUMDIS="+cp_ToStrODBC(this.w_NEWNUMDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DINUMDIS',this.w_NEWNUMDIS)
            select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS,DIBANRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWNUMDIS = NVL(_Link_.DINUMDIS,space(10))
      this.w_NEWANNO = NVL(_Link_.DI__ANNO,space(4))
      this.w_NEWNUMERO = NVL(_Link_.DINUMERO,0)
      this.w_NEWDATDIS = NVL(cp_ToDate(_Link_.DIDATDIS),ctod("  /  /  "))
      this.w_NEWDIBANRIF = NVL(_Link_.DIBANRIF,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NEWNUMDIS = space(10)
      endif
      this.w_NEWANNO = space(4)
      this.w_NEWNUMERO = 0
      this.w_NEWDATDIS = ctod("  /  /  ")
      this.w_NEWDIBANRIF = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])+'\'+cp_ToStr(_Link_.DINUMDIS,1)
      cp_ShowWarn(i_cKey,this.DIS_TINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWNUMDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWDIBANRIF
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWDIBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWDIBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NEWDIBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NEWDIBANRIF)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWDIBANRIF = NVL(_Link_.BACODBAN,space(15))
      this.w_NEWDIBADES = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NEWDIBANRIF = space(15)
      endif
      this.w_NEWDIBADES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWDIBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCLF_1_9.RadioValue()==this.w_TIPCLF)
      this.oPgFrm.Page1.oPag.oTIPCLF_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_11.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_11.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIS_1_12.value==this.w_CODFIS)
      this.oPgFrm.Page1.oPag.oCODFIS_1_12.value=this.w_CODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_13.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_13.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPSCA_1_14.value==this.w_IMPSCA)
      this.oPgFrm.Page1.oPag.oIMPSCA_1_14.value=this.w_IMPSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMEFF_1_16.value==this.w_NUMEFF)
      this.oPgFrm.Page1.oPag.oNUMEFF_1_16.value=this.w_NUMEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_17.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_17.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_26.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_26.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_28.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_28.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_31.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_31.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWNUMEFF_1_35.value==this.w_NEWNUMEFF)
      this.oPgFrm.Page1.oPag.oNEWNUMEFF_1_35.value=this.w_NEWNUMEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWIMPSCA_1_36.value==this.w_NEWIMPSCA)
      this.oPgFrm.Page1.oPag.oNEWIMPSCA_1_36.value=this.w_NEWIMPSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWDATSCA_1_37.value==this.w_NEWDATSCA)
      this.oPgFrm.Page1.oPag.oNEWDATSCA_1_37.value=this.w_NEWDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWTIPCLF_1_38.RadioValue()==this.w_NEWTIPCLF)
      this.oPgFrm.Page1.oPag.oNEWTIPCLF_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWCODCLF_1_39.value==this.w_NEWCODCLF)
      this.oPgFrm.Page1.oPag.oNEWCODCLF_1_39.value=this.w_NEWCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWDESCRI_1_40.value==this.w_NEWDESCRI)
      this.oPgFrm.Page1.oPag.oNEWDESCRI_1_40.value=this.w_NEWDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWNUMERO_1_47.value==this.w_NEWNUMERO)
      this.oPgFrm.Page1.oPag.oNEWNUMERO_1_47.value=this.w_NEWNUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWANNO_1_49.value==this.w_NEWANNO)
      this.oPgFrm.Page1.oPag.oNEWANNO_1_49.value=this.w_NEWANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWDATDIS_1_51.value==this.w_NEWDATDIS)
      this.oPgFrm.Page1.oPag.oNEWDATDIS_1_51.value=this.w_NEWDATDIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_GESPAR='S')  and not(empty(.w_CODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLF_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto o non gestito a partite")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_kerPag1 as StdContainer
  Width  = 674
  height = 517
  stdWidth  = 674
  stdheight = 517
  resizeXpos=438
  resizeYpos=271
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object SelEff as cp_zoombox with uid="XHFQXMAIKX",left=3, top=115, width=661,height=276,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSSO_KER",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,cTable="PAR_TITE",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnLoad=.t.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Init, AggiornaZoom",;
    nPag=1;
    , HelpContextID = 112357862


  add object oTIPCLF_1_9 as StdCombo with uid="CRGINLCTVV",rtseq=8,rtrep=.f.,left=50,top=3,width=76,height=21;
    , HelpContextID = 148981450;
    , cFormVar="w_TIPCLF",RowSource=""+"Fornitore,"+"Generico,"+"Cliente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLF_1_9.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'G',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oTIPCLF_1_9.GetRadio()
    this.Parent.oContained.w_TIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLF_1_9.SetRadio()
    this.Parent.oContained.w_TIPCLF=trim(this.Parent.oContained.w_TIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLF=='F',1,;
      iif(this.Parent.oContained.w_TIPCLF=='G',2,;
      iif(this.Parent.oContained.w_TIPCLF=='C',3,;
      0)))
  endfunc

  func oTIPCLF_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLF)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLF_1_11 as StdField with uid="OTXZEIRDAX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto o non gestito a partite",;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 149029338,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=132, Top=3, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CLIENTE / FORNITORE / GENERICO",'CONTISALD.CONTI_VZM',this.parent.oContained
  endproc

  add object oCODFIS_1_12 as StdField with uid="JNVDKXDOCU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODFIS", cQueryName = "CODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale da filtrare",;
    HelpContextID = 202310106,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=132, Top=30, InputMask=replicate('X',16)

  add object oDATSCA_1_13 as StdField with uid="FUXVZXIRCY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza o data raggruppamento dell'effetto da filtrare",;
    HelpContextID = 241242058,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=58

  add object oIMPSCA_1_14 as StdField with uid="QSDVONLJVX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IMPSCA", cQueryName = "IMPSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo scadenza da filtrare",;
    HelpContextID = 241255290,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=365, Top=58, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oNUMEFF_1_16 as StdField with uid="XKJWISAPQT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NUMEFF", cQueryName = "NUMEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero effetto da filtrare",;
    HelpContextID = 155151146,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=547, Top=87

  add object oDESCRI_1_17 as StdField with uid="MOJGVGXQQC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 92347338,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=267, Top=3, InputMask=replicate('X',40)


  add object oBtn_1_24 as StdButton with uid="QCIRTXRCWZ",left=611, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 194645754;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .NotifyEvent('AggiornaZoom')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_26 as StdField with uid="ZTJCGWMRVF",rtseq=20,rtrep=.t.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta da filtrare",;
    HelpContextID = 260008746,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=132, Top=86, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO_1_28 as StdField with uid="OMSJMYNPMA",rtseq=21,rtrep=.t.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento distinta da filtrare",;
    HelpContextID = 60121850,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=204, Top=86, InputMask=replicate('X',4)


  add object oBtn_1_29 as StdButton with uid="YXNUTKQXOS",left=255, top=86, width=20,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare una distinta";
    , HelpContextID = 65438762;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      vx_exec("QUERY\GSSO_KCS.VZM",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATDIS_1_31 as StdField with uid="MJIZLIFULF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta",;
    HelpContextID = 202379210,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=326, Top=86


  add object oBtn_1_33 as StdButton with uid="ALRXZTWAFI",left=408, top=86, width=20,height=22,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare la distinta da filtrare";
    , HelpContextID = 65438762;
  , bGlobalFont=.t.

    proc oBtn_1_33.Click()
      with this.Parent.oContained
        .NotifyEvent('EliminaDistinta')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNEWNUMEFF_1_35 as StdField with uid="PTPAOJVHOU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NEWNUMEFF", cQueryName = "NEWNUMEFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 247081340,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=544, Top=462

  add object oNEWIMPSCA_1_36 as StdField with uid="ZPHLLNGOKG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NEWIMPSCA", cQueryName = "NEWIMPSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo scadenza",;
    HelpContextID = 20261161,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=362, Top=430, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oNEWDATSCA_1_37 as StdField with uid="JKMRSQISJW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NEWDATSCA", cQueryName = "NEWDATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza o data raggruppamento dell'effetto",;
    HelpContextID = 74459433,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=129, Top=430


  add object oNEWTIPCLF_1_38 as StdCombo with uid="SPHHFXFCDK",rtseq=27,rtrep=.f.,left=47,top=399,width=76,height=21, enabled=.f.;
    , HelpContextID = 16787842;
    , cFormVar="w_NEWTIPCLF",RowSource=""+"Fornitore,"+"Generico,"+"Cliente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNEWTIPCLF_1_38.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'G',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oNEWTIPCLF_1_38.GetRadio()
    this.Parent.oContained.w_NEWTIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oNEWTIPCLF_1_38.SetRadio()
    this.Parent.oContained.w_NEWTIPCLF=trim(this.Parent.oContained.w_NEWTIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_NEWTIPCLF=='F',1,;
      iif(this.Parent.oContained.w_NEWTIPCLF=='G',2,;
      iif(this.Parent.oContained.w_NEWTIPCLF=='C',3,;
      0)))
  endfunc

  func oNEWTIPCLF_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_NEWCODCLF)
        bRes2=.link_1_39('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNEWCODCLF_1_39 as StdField with uid="OXQJRHDKFO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NEWCODCLF", cQueryName = "NEWCODCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto o non gestito a partite",;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 89074050,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=129, Top=399, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_NEWTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_NEWCODCLF"

  proc oNEWCODCLF_1_39.mAfter
    with this.Parent.oContained
      .w_CODCLF=IIF(.w_TIPCLF$'CF',CALCZER(.w_CODCLF),.w_CODCLF)
    endwith
  endproc

  func oNEWCODCLF_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNEWDESCRI_1_40 as StdField with uid="ALBOLXXRBE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NEWDESCRI", cQueryName = "NEWDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61876664,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=264, Top=399, InputMask=replicate('X',40)

  add object oNEWNUMERO_1_47 as StdField with uid="FPQRTVILVD",rtseq=32,rtrep=.t.,;
    cFormVar = "w_NEWNUMERO", cQueryName = "NEWNUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta da filtrare",;
    HelpContextID = 247081496,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=129, Top=462, cSayPict='"999999"', cGetPict='"999999"'

  add object oNEWANNO_1_49 as StdField with uid="VBMWRHKFDI",rtseq=33,rtrep=.t.,;
    cFormVar = "w_NEWANNO", cQueryName = "NEWANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento distinta da filtrare",;
    HelpContextID = 12770090,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=201, Top=462, InputMask=replicate('X',4)

  add object oNEWDATDIS_1_51 as StdField with uid="RLDWITWSFV",rtseq=34,rtrep=.f.,;
    cFormVar = "w_NEWDATDIS", cQueryName = "NEWDATDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta",;
    HelpContextID = 74459727,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=323, Top=462


  add object oBtn_1_52 as StdButton with uid="BWTKCLQRSS",left=611, top=399, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 25306134;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_53 as StdButton with uid="SDNJNHCASI",left=557, top=399, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare il riferimento all'effetto nell'esito";
    , HelpContextID = 194645754;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_NEWNUMEFF))
      endwith
    endif
  endfunc

  add object oStr_1_21 as StdString with uid="RAIXRZTTCC",Visible=.t., Left=8, Top=58,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LXBXVCUMTN",Visible=.t., Left=438, Top=87,;
    Alignment=1, Width=103, Height=18,;
    Caption="Num. effetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="YUVIESJNFU",Visible=.t., Left=227, Top=58,;
    Alignment=1, Width=132, Height=18,;
    Caption="Importo scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NRYUQNGMIR",Visible=.t., Left=6, Top=86,;
    Alignment=1, Width=120, Height=18,;
    Caption="Rif.distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="JSBQUBCPDE",Visible=.t., Left=193, Top=86,;
    Alignment=0, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LIGMQNIGIT",Visible=.t., Left=281, Top=86,;
    Alignment=1, Width=38, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="JJPQDNZOGQ",Visible=.t., Left=409, Top=462,;
    Alignment=1, Width=129, Height=18,;
    Caption="Num. effetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="IJRJZWOLWK",Visible=.t., Left=11, Top=432,;
    Alignment=1, Width=112, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="XXPKKHFMXE",Visible=.t., Left=225, Top=432,;
    Alignment=1, Width=131, Height=18,;
    Caption="Importo scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VMONAKEFWF",Visible=.t., Left=8, Top=462,;
    Alignment=1, Width=115, Height=18,;
    Caption="Rif.distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="GMTTFJVPND",Visible=.t., Left=190, Top=462,;
    Alignment=0, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OOKPGWHCLA",Visible=.t., Left=250, Top=462,;
    Alignment=1, Width=66, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="UCDYHSHFMF",Visible=.t., Left=9, Top=30,;
    Alignment=1, Width=117, Height=18,;
    Caption="Codice fisc.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_ker','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
