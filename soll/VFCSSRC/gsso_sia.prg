* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_sia                                                        *
*              Conferma contenziosi chiusi con incasso                         *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_81]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-08                                                      *
* Last revis.: 2007-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_sia",oParentObject))

* --- Class definition
define class tgsso_sia as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 570
  Height = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-10-18"
  HelpContextID=258577769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsso_sia"
  cComment = "Conferma contenziosi chiusi con incasso"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SELEZI = space(1)
  w_PARAM = space(4)
  w_SERIALE = space(10)
  w_DECTOT = 0
  w_USCITA = space(1)
  w_TIPAGG = space(1)
  w_CONINS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_siaPag1","gsso_sia",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CONINS = this.oPgFrm.Pages(1).oPag.CONINS
    DoDefault()
    proc Destroy()
      this.w_CONINS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsso_sia
    this.ccomment=IIF(This.oParentobject.w_TIPAGG='N',this.ccomment,'Conferma incassi da eliminare')
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SELEZI=space(1)
      .w_PARAM=space(4)
      .w_SERIALE=space(10)
      .w_DECTOT=0
      .w_USCITA=space(1)
      .w_TIPAGG=space(1)
      .w_USCITA=oParentObject.w_USCITA
      .w_TIPAGG=oParentObject.w_TIPAGG
      .oPgFrm.Page1.oPag.CONINS.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_PARAM = 'DETT'
        .w_SERIALE = Nvl(.w_CONINS.getVar('COSERIAL'),Space(10))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_DECTOT = Nvl(.w_CONINS.getVar('DECTOT'),0)
        .w_USCITA = 'C'
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_USCITA=.w_USCITA
      .oParentObject.w_TIPAGG=.w_TIPAGG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CONINS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_SERIALE = Nvl(.w_CONINS.getVar('COSERIAL'),Space(10))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
            .w_DECTOT = Nvl(.w_CONINS.getVar('DECTOT'),0)
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CONINS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CONINS.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_4.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_siaPag1 as StdContainer
  Width  = 566
  height = 334
  stdWidth  = 566
  stdheight = 334
  resizeXpos=352
  resizeYpos=180
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CONINS as cp_szoombox with uid="TUGLPHAHRT",left=4, top=3, width=557,height=277,;
    caption='CONINS',;
   bGlobalFont=.t.,;
    bOptions=.f.,cZoomFile="GSSO_INC",cTable="CON_TENZ",bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSSO_MCO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 121332186


  add object oBtn_1_2 as StdButton with uid="NUBHVPSUDL",left=508, top=283, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'operazione";
    , HelpContextID = 251260346;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="SHRFOKIYMQ",left=454, top=284, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere insoluti selezionati";
    , HelpContextID = 258549018;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_4 as StdRadio with uid="QEGLKWTHQX",rtseq=1,rtrep=.f.,left=15, top=285, width=128,height=34;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 8358618
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 8358618
      this.Buttons(2).Top=16
      this.SetAll("Width",126)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_4.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_4.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="QTFCFYGRYT",left=3, top=356, width=249,height=22,;
    caption='GSSO_BIC(SELE)',;
   bGlobalFont=.t.,;
    prg="gsso_bic('SELE')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 159672279


  add object oObj_1_6 as cp_runprogram with uid="FOYWUNBZIS",left=319, top=333, width=201,height=22,;
    caption='GSSO_BIC(STAT)',;
   bGlobalFont=.t.,;
    prg="gsso_bic('STAT')",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 144603095


  add object oBtn_1_9 as StdButton with uid="FRSHLUKCSC",left=145, top=283, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza contenzioso associato alla riga selezionata";
    , HelpContextID = 31320929;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSSO_BIC(this.Parent.oContained,.w_PARAM,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oObj_1_10 as cp_runprogram with uid="ZCVRZQTAZY",left=3, top=334, width=314,height=22,;
    caption='GSSO_BIC(w_PARAM,W_SERIALE)',;
   bGlobalFont=.t.,;
    prg="gsso_bic(w_PARAM,w_SERIALE)",;
    cEvent = "w_CONINS selected",;
    nPag=1;
    , HelpContextID = 168984521


  add object oObj_1_13 as cp_runprogram with uid="MBSDDYREVN",left=262, top=359, width=249,height=22,;
    caption='GSSO_BIC(SELE)',;
   bGlobalFont=.t.,;
    prg="GSSO_BIC(w_PARAM,w_SERIALE)",;
    cEvent = "w_conins selected",;
    nPag=1;
    , HelpContextID = 159672279
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_sia','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
