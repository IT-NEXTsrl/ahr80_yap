* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bri                                                        *
*              Inserimento causali Remote Banking                              *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-28                                                      *
* Last revis.: 2012-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bri",oParentObject)
return(i_retval)

define class tgsso_bri as StdBatch
  * --- Local variables
  * --- WorkFile variables
  CAU_REBA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'inserimento delle causali remote banking, con Carica/Salva dati esterni, invocato da 'GSUT_BCO('Carica')'
    * --- Try
    local bErr_03472610
    bErr_03472610=bTrsErr
    this.Try_03472610()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORE = .T.
    endif
    bTrsErr=bTrsErr or bErr_03472610
    * --- End
  endproc
  proc Try_03472610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_034791C0
    bErr_034791C0=bTrsErr
    this.Try_034791C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_03472C40
      bErr_03472C40=bTrsErr
      this.Try_03472C40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not AH_YESNO("Errore di scrittura in codice: %1 Continuo?","",this.oParentObject.w_CODCAUR)
          * --- Raise
          i_Error="Import fallito"
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_03472C40
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_034791C0
    * --- End
    return
  proc Try_034791C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_REBA
    i_nConn=i_TableProp[this.CAU_REBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_REBA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_REBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODCAU"+",CADESCRI"+",CACAUTES"+",CATIPMOV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCAUR),'CAU_REBA','CACODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCCAU),'CAU_REBA','CADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAUTESR),'CAU_REBA','CACAUTES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPMOVR),'CAU_REBA','CATIPMOV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODCAU',this.oParentObject.w_CODCAUR,'CADESCRI',this.oParentObject.w_DESCCAU,'CACAUTES',this.oParentObject.w_CAUTESR,'CATIPMOV',this.oParentObject.w_TIPMOVR)
      insert into (i_cTable) (CACODCAU,CADESCRI,CACAUTES,CATIPMOV &i_ccchkf. );
         values (;
           this.oParentObject.w_CODCAUR;
           ,this.oParentObject.w_DESCCAU;
           ,this.oParentObject.w_CAUTESR;
           ,this.oParentObject.w_TIPMOVR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03472C40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_msg("Scrivo: codice: %1", .t.,.f.,.f.,this.oParentObject.w_CODCAUR)
    * --- Write into CAU_REBA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_REBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_REBA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_REBA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CADESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESCCAU),'CAU_REBA','CADESCRI');
      +",CATIPMOV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPMOVR),'CAU_REBA','CATIPMOV');
          +i_ccchkf ;
      +" where ";
          +"CACODCAU = "+cp_ToStrODBC(this.oParentObject.w_CODCAUR);
             )
    else
      update (i_cTable) set;
          CADESCRI = this.oParentObject.w_DESCCAU;
          ,CATIPMOV = this.oParentObject.w_TIPMOVR;
          &i_ccchkf. ;
       where;
          CACODCAU = this.oParentObject.w_CODCAUR;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAU_REBA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
