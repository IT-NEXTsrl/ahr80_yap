* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bfs                                                        *
*              Ricezione flussi di ritorno su piu file                         *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-13                                                      *
* Last revis.: 2015-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bfs",oParentObject,m.pParam)
return(i_retval)

define class tgsso_bfs as StdBatch
  * --- Local variables
  pParam = space(1)
  w_SHOWERROR = .f.
  w_MESS = space(200)
  w_ERRORE = .f.
  w_ORIGPATH = space(200)
  w_COPATHTE = space(200)
  w_PATH = space(200)
  w_HOMEDIR = space(200)
  w_ENDFILE = .f.
  w_MESS = space(200)
  w_FILENUMBER = 0
  w_ZOOM = space(10)
  w_NUMEROFILE = 0
  w_SCARTATO = .f.
  w_CAMPOFOCUS = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lettura flusso di ritorno lanciata dalla maschera gsso_kfr  
    *     La routine ha 2 funzioni  :
    *     1) - Se viene invocata passandogli il parametro 'O' allora viene richiesto all utente di specificare una directori 
    *     da cui saranno estratti i file che saranno visualizzati nella maschera gsso_kfr 
    *     2) - Se viene invocata con parametri <>'O' allora effettua l import di tutti i file selezionati (all interno della maschera 
    *     gsso_kfr ) 
    this.w_SCARTATO = .F.
    this.w_NUMEROFILE = 0
    this.w_SHOWERROR = .F.
    this.w_ERRORE = .F.
    this.w_COPATHTE = this.oparentobject.w_COPATHTE
    this.w_PATH = this.oparentobject.w_PATH
    this.w_MESS = ""
    this.w_ZOOM = this.oParentObject.w_ZoomFile
    if USED("__TMP__")
      Select __TMP__ 
 Use
    endif
    CREATE CURSOR __TMP__ (TITOLO C(100) , NOMSUP C(20), DTARIC D(8), NOMFIL C(100), NUMRIG N(10), MSG M, TIPIMP C(10))
    NC = this.w_ZOOM.cCursor
    if this.pParam="I"
      if EMPTY (alltrim(NVL(this.oParentObject.w_DIRECTORI," ")))
        i_retcode = 'stop'
        return
      endif
      * --- Pulisce il cursore della maschera GSRB_KFR prima di inserirvi nuovi dati   
      ZAP IN ( this.w_ZOOM.cCursor )
      delete from (NC) where 1=1
      * --- Si posiziona sulla directory di default (specificata nella maschera di input GSRB_KFR)
      this.w_HOMEDIR = sys(5)+sys(2003)
      Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
      CD (alltrim(this.oParentObject.w_DIRECTORI))
      ON ERROR
      if NOT EMPTY(Messaggio)
        Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
        cd (this.w_HOMEDIR)
        i_retcode = 'stop'
        return
      endif
      * --- Visualizza la directori selezionata all interno della maschera GSRB_KFR
      this.oParentObject.w_DIR = this.oParentObject.w_DIRECTORI
      * --- Si posiziona sulla directory selezionata (per leggere i file tramite l istruzione ADIR (...)
      *     Legge i nomi dei file  
      *     Rispristina la directory di lavoro 
      cd (this.oparentobject.w_DIR) 
 this.w_FILENUMBER = ADIR(NOMIFILE) && Create array 
 cd (this.w_HOMEDIR) 
 
      if this.w_FILENUMBER=0
        i_retcode = 'stop'
        return
      endif
      * --- Crea un cursore e vi inserisce tutti i file della directory selezionata 
      CREATE CURSOR CursFILE (DATIEL C(200), SIZE N(18), MODIFICATO D, TIME C(20), ATTRIBUTI c(20), DIR c(200),XCHK N(1))
      APPEND FROM ARRAY NOMIFILE
      update CursFILE set DIR=this.oparentobject.w_DIR where 1=1
      * --- LA routine GSRB1KFS riemepie lo zoom della maschera GSSO_KFR scartando i file che sono stati imoprtati in precedenti sessioni di lavoro
      GSSO1BFS (this,"CALC")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    if this.pParam="O"
      * --- Si posiziona sulla directory di default (specificata nella maschera di input GSSO_KFR)
      this.w_HOMEDIR = sys(5)+sys(2003)
      Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
      CD (iif(EMPTY(this.oParentObject.w_DIRECTORI),this.w_COPATHTE,SUBSTR(this.oParentObject.w_DIRECTORI,1,RATC("\",this.oParentObject.w_DIRECTORI))))
      ON ERROR
      if NOT EMPTY(Messaggio)
        Ah_ErrorMSG(Messaggio,48)
        Messaggio = "" 
 ON ERROR Messaggio = "Path errato"
      endif
      this.oParentObject.w_DIR = cp_GetDIR()
      this.w_CAMPOFOCUS = THIS.OPARENTOBJECT.GETCTRL("w_DIRECTORI")
      this.w_CAMPOFOCUS.SetFocus()     
      if EMPTY(this.oParentObject.w_DIR)
        cd (this.w_HOMEDIR)
        ZAP IN ( this.w_ZOOM.cCursor )
        delete from (NC) where 1=1
        this.oParentObject.w_DIRECTORI = ""
        i_retcode = 'stop'
        return
      else
        * --- Pulisce il cursore della maschera GSSO_KFR prima di inserirvi nuovi dati  
        ZAP IN ( this.w_ZOOM.cCursor )
        delete from (NC) where 1=1
      endif
      * --- Visualizza la directori selezionata all interno della maschera GSSO_KFR
      this.oParentObject.w_DIRECTORI = this.oParentObject.w_DIR
      * --- Si posiziona sulla directory selezionata (per leggere i file tramite l istruzione ADIR (...)
      *     Legge i nomi dei file  
      *     Rispristina la directory di lavoro 
      cd (this.oparentobject.w_DIR) 
 this.w_FILENUMBER = ADIR(NOMIFILE) && Create array 
 cd (this.w_HOMEDIR) 
 
      if this.w_FILENUMBER=0
        i_retcode = 'stop'
        return
      endif
      * --- Crea un cursore e vi inserisce tutti i file della directory selezionata 
      CREATE CURSOR CursFILE (DATIEL C(200), SIZE N(18), MODIFICATO D, TIME C(20), ATTRIBUTI c(20), DIR c(200),XCHK N(1))
      APPEND FROM ARRAY NOMIFILE
      update CursFILE set DIR=this.oparentobject.w_DIR where 1=1
      * --- LA routine GSSO1KFS riemepie lo zoom della maschera GSSO_KFR scartando i file che sono stati imoprtati in precedenti sessioni di lavoro
      GSSO1BFS (this,"CALC")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Se l utente non ha selezionato nessun path (non ci sono file selezionati) e non esiste il cursore appoggio CursFILE allora viene visualizzato un messaggio di errore
    if NOT (USED("CursFILE"))
      Messaggio = "Non � stato selezionato alcun file"
      Ah_ErrorMSG(Messaggio,48)
      i_retcode = 'stop'
      return
    endif
    if EMPTY (alltrim(NVL(this.oParentObject.w_DIRECTORI," ")))
      i_retcode = 'stop'
      return
    endif
    * --- Per ogni file selezionato dall utente viene invocata la routine che effettua l import
    * --- Cursore CursErrSta riempito dal batch gsrb_bfr contenente gli errori dei file di importazione
    CREATE CURSOR CursErrSta (NOMSUP C(20), DTARIC D(8), NOMFIL C(100), NUMRIG N(10), MSG C(200), TIPIMP C(1))
    SELECT (NC) 
 SCAN FOR XCHK=1
    this.w_PATH = trim(DIR)+trim(NOME)
    this.w_NUMEROFILE = this.w_NUMEROFILE + 1
    * --- Il nome del file corrente su cui effettuare l import visene passato alla routine GSSO_BFR tramite la variabile w_PATH  
    this.oparentobject.w_PATH=this.w_PATH
    this.w_MESS = ""
    if justext(this.w_PATH)="XML"
      GSSO1BFR (this,"C")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      GSSO_BFR (this,"C")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- I messaggi di errore vengono visualizzati nello ZOOM della maschera GSSO_KFR
    if this.w_SHOWERROR=.T.
      REPLACE ERRORE with this.w_MESS
      REPLACE ATTRIBUTI with "err"
      this.w_zoom.grd.refresh()
      this.w_ERRORE = .T.
      this.w_SHOWERROR = .F.
    else
      REPLACE ERRORE with AH_MSGFORMAT ("Import eseguito")
    endif
    ENDSCAN
    * --- Ordian il cursore di stampa rispetto al tipo di impotazione
    SELECT * from __TMP__ into cursor __TMP1__ 
 SELECT __TMP__ 
 use 
 SELECT * from __TMP1__ order by TIPIMP,NOMFIL into cursor __TMP__ 
 SELECT __TMP__ 
 go top
    if RECCOUNT()>0
      if AH_YESNO( AH_MSGFORMAT ( "Attenzione:%0Si sono verificate delle anomalie%1%0Si vuole visualizzare il report di riepilogo?" , IIF (this.w_SCARTATO," e alcuni supporti sono stati scartati",".")))
        CP_CHPRN("..\SOLL\EXE\QUERY\GSSO_BFR.FRX","",this.oParentObject)
      endif
    endif
    if USED("__TMP1__")
      Select __TMP1__ 
 Use
    endif
    * --- Se durante l elaborazione si sono verificati degli errori viene  visualizzato un messaggio di errore 
    if this.w_ERRORE=.T.
      SELECT CursErrSta 
 go top
      if RECCOUNT()>0
        if AH_YESNO("Si sono verificati errori durante l'import di alcuni file. I file che hanno causato gli errori sono evidenziati in rosso. %0Si vuole eseguire la stampa?")
          select * from CursErrSta into cursor __TMP__
          CP_CHPRN("..\SOLL\EXE\QUERY\GSSO_BFS.FRX","",this.oParentObject)
        endif
      else
        this.w_MESS = "Si sono verificati errori durante l'import di alcuni file. I file che hanno causato gli errori sono evidenziati in rosso."
        ah_ErrorMsg(this.w_MESS,48,"")
      endif
    else
      if this.w_NUMEROFILE>0
        this.w_MESS = "Importazione terminata"
        ah_ErrorMsg(this.w_MESS,48,"")
      endif
    endif
    if USED("__TMP__")
      Select __TMP__ 
 Use
    endif
    if USED("CursErrSta")
      Select CursErrSta 
 Use
    endif
    if (this.w_NUMEROFILE=0)
      Messaggio = "Non � stato selezionato alcun file"
      Ah_ErrorMSG(Messaggio,48,"")
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
