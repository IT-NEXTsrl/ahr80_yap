* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_sso                                                        *
*              Stampa solleciti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-05-19                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_sso",oParentObject))

* --- Class definition
define class tgsso_sso as StdForm
  Top    = 2
  Left   = 68

  * --- Standard Properties
  Width  = 643
  Height = 530
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=177629847
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  LINGUE_IDX = 0
  TES_CONT_IDX = 0
  PAR_CONT_IDX = 0
  AGENTI_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsso_sso"
  cComment = "Stampa solleciti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_NOMPRG = space(10)
  w_CODAZI = space(5)
  w_TIPCON = space(1)
  w_MODSPE = space(2)
  o_MODSPE = space(2)
  w_TIPINS = space(1)
  o_TIPINS = space(1)
  w_CATPAG = space(2)
  w_STATUS = space(2)
  w_NUMLIV = 0
  o_NUMLIV = 0
  w_SOLOZERO = space(1)
  w_FILINS = space(1)
  w_CODVAL = space(3)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_FILCAO = 0
  w_FLEURO = space(1)
  w_CODLIN = space(3)
  w_DATINV = ctod('  /  /  ')
  o_DATINV = ctod('  /  /  ')
  w_NUMERO = space(6)
  w_MEZSPE = space(2)
  w_FLGAUTO = space(1)
  w_FLGRAGG = space(1)
  o_FLGRAGG = space(1)
  w_SELEZI = space(1)
  w_FILPAG = space(2)
  w_DESVAL = space(35)
  w_DESLIN = space(30)
  w_LIVTES = 0
  w_OGGTES = space(50)
  w_TESTES = space(0)
  w_PIETES = space(0)
  w_GIOPAR = 0
  w_NOMTES = space(50)
  w_LINGUA = space(3)
  w_COSERIAL = space(10)
  w_DESCRI = space(40)
  w_EMAIL = space(254)
  w_SPELET = 0
  w_SPERAC = 0
  w_SPERAR = 0
  w_SPEMAI = 0
  w_SPETEL = 0
  w_VALPAR = space(3)
  w_TIPTES = space(1)
  w_DECTOT = 0
  w_DECPAR = 0
  w_DIFFDAY = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_SPEFAX = 0
  w_SPEPEC = 0
  w_EMPEC = space(254)
  w_ZoomInso = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_ssoPag1","gsso_sso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPINS_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomInso = this.oPgFrm.Pages(1).oPag.ZoomInso
    DoDefault()
    proc Destroy()
      this.w_ZoomInso = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='LINGUE'
    this.cWorkTables[3]='TES_CONT'
    this.cWorkTables[4]='PAR_CONT'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='CONTI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSSO_BZ2(this,"S")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsso_sso
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NOMPRG=space(10)
      .w_CODAZI=space(5)
      .w_TIPCON=space(1)
      .w_MODSPE=space(2)
      .w_TIPINS=space(1)
      .w_CATPAG=space(2)
      .w_STATUS=space(2)
      .w_NUMLIV=0
      .w_SOLOZERO=space(1)
      .w_FILINS=space(1)
      .w_CODVAL=space(3)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_FILCAO=0
      .w_FLEURO=space(1)
      .w_CODLIN=space(3)
      .w_DATINV=ctod("  /  /  ")
      .w_NUMERO=space(6)
      .w_MEZSPE=space(2)
      .w_FLGAUTO=space(1)
      .w_FLGRAGG=space(1)
      .w_SELEZI=space(1)
      .w_FILPAG=space(2)
      .w_DESVAL=space(35)
      .w_DESLIN=space(30)
      .w_LIVTES=0
      .w_OGGTES=space(50)
      .w_TESTES=space(0)
      .w_PIETES=space(0)
      .w_GIOPAR=0
      .w_NOMTES=space(50)
      .w_LINGUA=space(3)
      .w_COSERIAL=space(10)
      .w_DESCRI=space(40)
      .w_EMAIL=space(254)
      .w_SPELET=0
      .w_SPERAC=0
      .w_SPERAR=0
      .w_SPEMAI=0
      .w_SPETEL=0
      .w_VALPAR=space(3)
      .w_TIPTES=space(1)
      .w_DECTOT=0
      .w_DECPAR=0
      .w_DIFFDAY=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_SPEFAX=0
      .w_SPEPEC=0
      .w_EMPEC=space(254)
        .w_NOMPRG = IIF( .w_FLGRAGG='S', 'GSSORSSO', 'GSSO_SSO' )
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
        .w_TIPCON = 'C'
          .DoRTCalc(4,4,.f.)
        .w_TIPINS = 'T'
        .w_CATPAG = 'XX'
        .w_STATUS = 'PE'
          .DoRTCalc(8,8,.f.)
        .w_SOLOZERO = 'N'
        .w_FILINS = IIF(.w_TIPINS='T', ' ', .w_TIPINS)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODVAL))
          .link_1_12('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODINI))
          .link_1_13('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODFIN))
          .link_1_14('Full')
        endif
        .DoRTCalc(14,16,.f.)
        if not(empty(.w_CODLIN))
          .link_1_17('Full')
        endif
        .w_DATINV = i_datsys
        .w_NUMERO = SPACE(6)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_NUMERO))
          .link_1_19('Full')
        endif
        .w_MEZSPE = IIF(NOT EMPTY(NVL(.w_MODSPE,' ')),.w_MODSPE,.w_MEZSPE)
        .w_FLGAUTO = IIF(.w_TIPINS='T','S','')
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate(.w_NOMPRG)
          .DoRTCalc(21,22,.f.)
        .w_FILPAG = IIF(.w_CATPAG='XX', '  ', .w_CATPAG)
          .DoRTCalc(24,32,.f.)
        .w_COSERIAL = Nvl(.w_ZoomInso.getVar('COSERIAL'),Space(10))
        .w_DESCRI = Nvl(.w_ZoomInso.getVar('ANDESCRI'),Space(40))
      .oPgFrm.Page1.oPag.ZoomInso.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .DoRTCalc(35,41,.f.)
        if not(empty(.w_VALPAR))
          .link_1_59('Full')
        endif
        .w_TIPTES = IIF(.w_TIPINS='T',' ',.w_TIPINS)
          .DoRTCalc(43,44,.f.)
        .w_DIFFDAY = .w_DATINV-.w_GIOPAR
        .w_OBTEST = I_DATSYS
    endwith
    this.DoRTCalc(47,51,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FLGRAGG<>.w_FLGRAGG
            .w_NOMPRG = IIF( .w_FLGRAGG='S', 'GSSORSSO', 'GSSO_SSO' )
        endif
          .link_1_2('Full')
        .DoRTCalc(3,8,.t.)
        if .o_NUMLIV<>.w_NUMLIV
            .w_SOLOZERO = 'N'
        endif
            .w_FILINS = IIF(.w_TIPINS='T', ' ', .w_TIPINS)
        .DoRTCalc(11,17,.t.)
        if .o_TIPINS<>.w_TIPINS.or. .o_NUMLIV<>.w_NUMLIV
            .w_NUMERO = SPACE(6)
          .link_1_19('Full')
        endif
        if .o_MODSPE<>.w_MODSPE
            .w_MEZSPE = IIF(NOT EMPTY(NVL(.w_MODSPE,' ')),.w_MODSPE,.w_MEZSPE)
        endif
        if .o_TIPINS<>.w_TIPINS
            .w_FLGAUTO = IIF(.w_TIPINS='T','S','')
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(.w_NOMPRG)
        .DoRTCalc(21,22,.t.)
            .w_FILPAG = IIF(.w_CATPAG='XX', '  ', .w_CATPAG)
        .DoRTCalc(24,32,.t.)
            .w_COSERIAL = Nvl(.w_ZoomInso.getVar('COSERIAL'),Space(10))
            .w_DESCRI = Nvl(.w_ZoomInso.getVar('ANDESCRI'),Space(40))
        .oPgFrm.Page1.oPag.ZoomInso.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .DoRTCalc(35,40,.t.)
          .link_1_59('Full')
            .w_TIPTES = IIF(.w_TIPINS='T',' ',.w_TIPINS)
        .DoRTCalc(43,44,.t.)
        if .o_DATINV<>.w_DATINV
            .w_DIFFDAY = .w_DATINV-.w_GIOPAR
        endif
            .w_OBTEST = I_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(47,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.ZoomInso.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNUMLIV_1_9.enabled = this.oPgFrm.Page1.oPag.oNUMLIV_1_9.mCond()
    this.oPgFrm.Page1.oPag.oSOLOZERO_1_10.enabled = this.oPgFrm.Page1.oPag.oSOLOZERO_1_10.mCond()
    this.oPgFrm.Page1.oPag.oFLEURO_1_16.enabled = this.oPgFrm.Page1.oPag.oFLEURO_1_16.mCond()
    this.oPgFrm.Page1.oPag.oNUMERO_1_19.enabled = this.oPgFrm.Page1.oPag.oNUMERO_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFLGAUTO_1_21.enabled = this.oPgFrm.Page1.oPag.oFLGAUTO_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLEURO_1_16.visible=!this.oPgFrm.Page1.oPag.oFLEURO_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomInso.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_lTable = "PAR_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2], .t., this.PAR_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCMODINV,PCINTSOLL,PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCVALIMP,PCSPETEL,PCSPEPEC";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCMODINV,PCINTSOLL,PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCVALIMP,PCSPETEL,PCSPEPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(5))
      this.w_MODSPE = NVL(_Link_.PCMODINV,space(2))
      this.w_GIOPAR = NVL(_Link_.PCINTSOLL,0)
      this.w_SPELET = NVL(_Link_.PCSPELET,0)
      this.w_SPERAC = NVL(_Link_.PCSPERAC,0)
      this.w_SPERAR = NVL(_Link_.PCSPERAR,0)
      this.w_SPEFAX = NVL(_Link_.PCSPEFAX,0)
      this.w_SPEMAI = NVL(_Link_.PCSPEMAI,0)
      this.w_VALPAR = NVL(_Link_.PCVALIMP,space(3))
      this.w_SPETEL = NVL(_Link_.PCSPETEL,0)
      this.w_SPEPEC = NVL(_Link_.PCSPEPEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_MODSPE = space(2)
      this.w_GIOPAR = 0
      this.w_SPELET = 0
      this.w_SPERAC = 0
      this.w_SPERAR = 0
      this.w_SPEFAX = 0
      this.w_SPEMAI = 0
      this.w_VALPAR = space(3)
      this.w_SPETEL = 0
      this.w_SPEPEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_CODVAL)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_12'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_FILCAO = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
      this.w_FILCAO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_1_13'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_codini<=.w_codfin) or (empty(.w_codfin))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_CODINI = space(15)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_14'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_codini<=.w_codfin) or (empty(.w_codini))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
        endif
        this.w_CODFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_CODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_CODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oCODLIN_1_17'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_CODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_CODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMERO
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_lTable = "TES_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2], .t., this.TES_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSSO_ATC',True,'TES_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCNUMERO like "+cp_ToStrODBC(trim(this.w_NUMERO)+"%");

          i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCNUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCNUMERO',trim(this.w_NUMERO))
          select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCNUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMERO)==trim(_Link_.TCNUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMERO) and !this.bDontReportError
            deferred_cp_zoom('TES_CONT','*','TCNUMERO',cp_AbsName(oSource.parent,'oNUMERO_1_19'),i_cWhere,'GSSO_ATC',"Testi contenziosi",'GSSO_SSS.TES_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN";
                     +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',oSource.xKey(1))
            select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN";
                   +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',this.w_NUMERO)
            select TCNUMERO,TCNOMTES,TCNUMLIV,TCOGGTES,TCTESDOC,TCPIEDOC,TCTIPTES,TCCODLIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.TCNUMERO,space(6))
      this.w_NOMTES = NVL(_Link_.TCNOMTES,space(50))
      this.w_LIVTES = NVL(_Link_.TCNUMLIV,0)
      this.w_OGGTES = NVL(_Link_.TCOGGTES,space(50))
      this.w_TESTES = NVL(_Link_.TCTESDOC,space(0))
      this.w_PIETES = NVL(_Link_.TCPIEDOC,space(0))
      this.w_TIPTES = NVL(_Link_.TCTIPTES,space(1))
      this.w_LINGUA = NVL(_Link_.TCCODLIN,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(6)
      endif
      this.w_NOMTES = space(50)
      this.w_LIVTES = 0
      this.w_OGGTES = space(50)
      this.w_TESTES = space(0)
      this.w_PIETES = space(0)
      this.w_TIPTES = space(1)
      this.w_LINGUA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_LIVTES>=.w_NUMLIV AND (.w_TIPINS<>'T' AND .w_TIPTES=.w_TIPINS OR .w_TIPINS='T' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Testo inesistente o livello, tipo incongruente")
        endif
        this.w_NUMERO = space(6)
        this.w_NOMTES = space(50)
        this.w_LIVTES = 0
        this.w_OGGTES = space(50)
        this.w_TESTES = space(0)
        this.w_PIETES = space(0)
        this.w_TIPTES = space(1)
        this.w_LINGUA = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])+'\'+cp_ToStr(_Link_.TCNUMERO,1)
      cp_ShowWarn(i_cKey,this.TES_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALPAR
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPAR)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPAR = NVL(_Link_.VACODVAL,space(3))
      this.w_DECPAR = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALPAR = space(3)
      endif
      this.w_DECPAR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPINS_1_5.RadioValue()==this.w_TIPINS)
      this.oPgFrm.Page1.oPag.oTIPINS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATPAG_1_6.RadioValue()==this.w_CATPAG)
      this.oPgFrm.Page1.oPag.oCATPAG_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATUS_1_7.RadioValue()==this.w_STATUS)
      this.oPgFrm.Page1.oPag.oSTATUS_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMLIV_1_9.value==this.w_NUMLIV)
      this.oPgFrm.Page1.oPag.oNUMLIV_1_9.value=this.w_NUMLIV
    endif
    if not(this.oPgFrm.Page1.oPag.oSOLOZERO_1_10.RadioValue()==this.w_SOLOZERO)
      this.oPgFrm.Page1.oPag.oSOLOZERO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_12.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_12.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_13.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_13.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_14.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_14.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEURO_1_16.RadioValue()==this.w_FLEURO)
      this.oPgFrm.Page1.oPag.oFLEURO_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIN_1_17.value==this.w_CODLIN)
      this.oPgFrm.Page1.oPag.oCODLIN_1_17.value=this.w_CODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINV_1_18.value==this.w_DATINV)
      this.oPgFrm.Page1.oPag.oDATINV_1_18.value=this.w_DATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_19.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_19.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oMEZSPE_1_20.RadioValue()==this.w_MEZSPE)
      this.oPgFrm.Page1.oPag.oMEZSPE_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGAUTO_1_21.RadioValue()==this.w_FLGAUTO)
      this.oPgFrm.Page1.oPag.oFLGAUTO_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGRAGG_1_22.RadioValue()==this.w_FLGRAGG)
      this.oPgFrm.Page1.oPag.oFLGRAGG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_23.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_29.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_29.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_1_30.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_1_30.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMTES_1_36.value==this.w_NOMTES)
      this.oPgFrm.Page1.oPag.oNOMTES_1_36.value=this.w_NOMTES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_68.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_68.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_69.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_69.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_codini<=.w_codfin) or (empty(.w_codfin)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   not((.w_codini<=.w_codfin) or (empty(.w_codini)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale")
          case   (empty(.w_DATINV))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINV_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DATINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMERO)) or not(.w_LIVTES>=.w_NUMLIV AND (.w_TIPINS<>'T' AND .w_TIPTES=.w_TIPINS OR .w_TIPINS='T' )))  and (.w_FLGAUTO<>'S' AND .w_TIPINS<>'T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_19.SetFocus()
            i_bnoObbl = !empty(.w_NUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Testo inesistente o livello, tipo incongruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MODSPE = this.w_MODSPE
    this.o_TIPINS = this.w_TIPINS
    this.o_NUMLIV = this.w_NUMLIV
    this.o_DATINV = this.w_DATINV
    this.o_FLGRAGG = this.w_FLGRAGG
    return

enddefine

* --- Define pages as container
define class tgsso_ssoPag1 as StdContainer
  Width  = 639
  height = 530
  stdWidth  = 639
  stdheight = 530
  resizeXpos=376
  resizeYpos=324
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPINS_1_5 as StdCombo with uid="QQYZKXWLFT",rtseq=5,rtrep=.f.,left=76,top=5,width=136,height=21;
    , ToolTipText = "Tipo contenziosi: insoluti, mancati pagamenti o tutti";
    , HelpContextID = 221988554;
    , cFormVar="w_TIPINS",RowSource=""+"Insoluti,"+"Mancati pagamenti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPINS_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPINS_1_5.GetRadio()
    this.Parent.oContained.w_TIPINS = this.RadioValue()
    return .t.
  endfunc

  func oTIPINS_1_5.SetRadio()
    this.Parent.oContained.w_TIPINS=trim(this.Parent.oContained.w_TIPINS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPINS=='I',1,;
      iif(this.Parent.oContained.w_TIPINS=='M',2,;
      iif(this.Parent.oContained.w_TIPINS=='T',3,;
      0)))
  endfunc


  add object oCATPAG_1_6 as StdCombo with uid="CWOBMSTBNT",rtseq=6,rtrep=.f.,left=287,top=5,width=130,height=21;
    , ToolTipText = "Selezione categoria di pagamento";
    , HelpContextID = 168038362;
    , cFormVar="w_CATPAG",RowSource=""+"Cambiale/tratta,"+"Rimessa diretta,"+"Bonifico,"+"Anticipazioni/M.AV.,"+"R.I.D.,"+"Ric. bancaria/RiBa,"+"Compensazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATPAG_1_6.RadioValue()
    return(iif(this.value =1,'CA',;
    iif(this.value =2,'RD',;
    iif(this.value =3,'BO',;
    iif(this.value =4,'MA',;
    iif(this.value =5,'RI',;
    iif(this.value =6,'RB',;
    iif(this.value =7,'CC',;
    iif(this.value =8,'XX',;
    space(2))))))))))
  endfunc
  func oCATPAG_1_6.GetRadio()
    this.Parent.oContained.w_CATPAG = this.RadioValue()
    return .t.
  endfunc

  func oCATPAG_1_6.SetRadio()
    this.Parent.oContained.w_CATPAG=trim(this.Parent.oContained.w_CATPAG)
    this.value = ;
      iif(this.Parent.oContained.w_CATPAG=='CA',1,;
      iif(this.Parent.oContained.w_CATPAG=='RD',2,;
      iif(this.Parent.oContained.w_CATPAG=='BO',3,;
      iif(this.Parent.oContained.w_CATPAG=='MA',4,;
      iif(this.Parent.oContained.w_CATPAG=='RI',5,;
      iif(this.Parent.oContained.w_CATPAG=='RB',6,;
      iif(this.Parent.oContained.w_CATPAG=='CC',7,;
      iif(this.Parent.oContained.w_CATPAG=='XX',8,;
      0))))))))
  endfunc


  add object oSTATUS_1_7 as StdCombo with uid="LWNTRXCKMH",rtseq=7,rtrep=.f.,left=471,top=5,width=148,height=21;
    , ToolTipText = "Status della pratica";
    , HelpContextID = 213986266;
    , cFormVar="w_STATUS",RowSource=""+"Pendente,"+"Chiuso con incasso,"+"Chiuso senza incasso,"+"Pratica legale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATUS_1_7.RadioValue()
    return(iif(this.value =1,'PE',;
    iif(this.value =2,'CI',;
    iif(this.value =3,'CS',;
    iif(this.value =4,'PL',;
    space(2))))))
  endfunc
  func oSTATUS_1_7.GetRadio()
    this.Parent.oContained.w_STATUS = this.RadioValue()
    return .t.
  endfunc

  func oSTATUS_1_7.SetRadio()
    this.Parent.oContained.w_STATUS=trim(this.Parent.oContained.w_STATUS)
    this.value = ;
      iif(this.Parent.oContained.w_STATUS=='PE',1,;
      iif(this.Parent.oContained.w_STATUS=='CI',2,;
      iif(this.Parent.oContained.w_STATUS=='CS',3,;
      iif(this.Parent.oContained.w_STATUS=='PL',4,;
      0))))
  endfunc


  add object oBtn_1_8 as StdButton with uid="CVKSDOIMAT",left=584, top=81, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la ricerca sui solleciti";
    , HelpContextID = 182318826;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .notifyevent('CalcZoom')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMLIV_1_9 as StdField with uid="LRWLECSCSH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMLIV", cQueryName = "NUMLIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo livello insoluto (0=no selezione)",;
    HelpContextID = 176712490,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=76, Top=33

  func oNUMLIV_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SOLOZERO<>'S')
    endwith
   endif
  endfunc

  add object oSOLOZERO_1_10 as StdCheck with uid="TZEROTIWPW",rtseq=9,rtrep=.f.,left=112, top=32, caption="Solo livello 0",;
    ToolTipText = "Se attivo filtra i solleciti di livello 0",;
    HelpContextID = 175472779,;
    cFormVar="w_SOLOZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOLOZERO_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOLOZERO_1_10.GetRadio()
    this.Parent.oContained.w_SOLOZERO = this.RadioValue()
    return .t.
  endfunc

  func oSOLOZERO_1_10.SetRadio()
    this.Parent.oContained.w_SOLOZERO=trim(this.Parent.oContained.w_SOLOZERO)
    this.value = ;
      iif(this.Parent.oContained.w_SOLOZERO=='S',1,;
      0)
  endfunc

  func oSOLOZERO_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMLIV=0)
    endwith
   endif
  endfunc

  add object oCODVAL_1_12 as StdField with uid="EPCCJKLKZX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione (vuoto=no selezione)",;
    HelpContextID = 83821018,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=287, Top=33, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oCODINI_1_13 as StdField with uid="GQTJNZBAEG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    HelpContextID = 121373146,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=76, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODINI_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_14 as StdField with uid="UAWBFKFCOA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale",;
    HelpContextID = 42926554,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=76, Top=85, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODFIN_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oFLEURO_1_16 as StdCheck with uid="XKNGRAJSLT",rtseq=15,rtrep=.f.,left=248, top=111, caption="Importi in Euro",;
    ToolTipText = "Se attivo: gli importi delle scadenze in valuta EMU saranno stampati convertiti in Euro",;
    HelpContextID = 15725738,;
    cFormVar="w_FLEURO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEURO_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLEURO_1_16.GetRadio()
    this.Parent.oContained.w_FLEURO = this.RadioValue()
    return .t.
  endfunc

  func oFLEURO_1_16.SetRadio()
    this.Parent.oContained.w_FLEURO=trim(this.Parent.oContained.w_FLEURO)
    this.value = ;
      iif(this.Parent.oContained.w_FLEURO=='S',1,;
      0)
  endfunc

  func oFLEURO_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODVAL<>g_PERVAL AND (EMPTY(.w_CODVAL) OR .w_FILCAO<>0))
    endwith
   endif
  endfunc

  func oFLEURO_1_16.mHide()
    with this.Parent.oContained
      return (NOT (.w_CODVAL<>g_PERVAL AND (EMPTY(.w_CODVAL) OR .w_FILCAO<>0)))
    endwith
  endfunc

  add object oCODLIN_1_17 as StdField with uid="YOAMZHLKWP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODLIN", cQueryName = "CODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della lingua del testo da stampare",;
    HelpContextID = 42533338,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=72, Top=136, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_CODLIN"

  func oCODLIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oCODLIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oCODLIN_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_CODLIN
     i_obj.ecpSave()
  endproc

  add object oDATINV_1_18 as StdField with uid="GPGXXYTNQG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATINV", cQueryName = "DATINV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di invio del sollecito",;
    HelpContextID = 171642826,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=540, Top=136

  add object oNUMERO_1_19 as StdField with uid="ZCAJNMRQAO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Testo inesistente o livello, tipo incongruente",;
    ToolTipText = "Numero del testo da stampare",;
    HelpContextID = 16739114,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=72, Top=160, cSayPict="'999999'", cGetPict="'999999'", InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="TES_CONT", cZoomOnZoom="GSSO_ATC", oKey_1_1="TCNUMERO", oKey_1_2="this.w_NUMERO"

  func oNUMERO_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGAUTO<>'S' AND .w_TIPINS<>'T')
    endwith
   endif
  endfunc

  func oNUMERO_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMERO_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMERO_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TES_CONT','*','TCNUMERO',cp_AbsName(this.parent,'oNUMERO_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSSO_ATC',"Testi contenziosi",'GSSO_SSS.TES_CONT_VZM',this.parent.oContained
  endproc
  proc oNUMERO_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSSO_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCNUMERO=this.parent.oContained.w_NUMERO
     i_obj.ecpSave()
  endproc


  add object oMEZSPE_1_20 as StdCombo with uid="OZPAUFNJYM",rtseq=19,rtrep=.f.,left=503,top=160,width=116,height=21;
    , ToolTipText = "Mezzo di spedizione del testo";
    , HelpContextID = 185641786;
    , cFormVar="w_MEZSPE",RowSource=""+"Raccomandata,"+"FAX,"+"Raccomandata A.R.,"+"Lettera,"+"E-mail,"+"Telefono,"+"PEC", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMEZSPE_1_20.RadioValue()
    return(iif(this.value =1,'RA',;
    iif(this.value =2,'FA',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'LE',;
    iif(this.value =5,'EM',;
    iif(this.value =6,'TE',;
    iif(this.value =7,'PE',;
    space(2)))))))))
  endfunc
  func oMEZSPE_1_20.GetRadio()
    this.Parent.oContained.w_MEZSPE = this.RadioValue()
    return .t.
  endfunc

  func oMEZSPE_1_20.SetRadio()
    this.Parent.oContained.w_MEZSPE=trim(this.Parent.oContained.w_MEZSPE)
    this.value = ;
      iif(this.Parent.oContained.w_MEZSPE=='RA',1,;
      iif(this.Parent.oContained.w_MEZSPE=='FA',2,;
      iif(this.Parent.oContained.w_MEZSPE=='AR',3,;
      iif(this.Parent.oContained.w_MEZSPE=='LE',4,;
      iif(this.Parent.oContained.w_MEZSPE=='EM',5,;
      iif(this.Parent.oContained.w_MEZSPE=='TE',6,;
      iif(this.Parent.oContained.w_MEZSPE=='PE',7,;
      0)))))))
  endfunc

  add object oFLGAUTO_1_21 as StdCheck with uid="MZOQAGABHZ",rtseq=20,rtrep=.f.,left=48, top=183, caption="Selezione automatica testo",;
    HelpContextID = 198431914,;
    cFormVar="w_FLGAUTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGAUTO_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLGAUTO_1_21.GetRadio()
    this.Parent.oContained.w_FLGAUTO = this.RadioValue()
    return .t.
  endfunc

  func oFLGAUTO_1_21.SetRadio()
    this.Parent.oContained.w_FLGAUTO=trim(this.Parent.oContained.w_FLGAUTO)
    this.value = ;
      iif(this.Parent.oContained.w_FLGAUTO=='S',1,;
      0)
  endfunc

  func oFLGAUTO_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPINS<>'T')
    endwith
   endif
  endfunc

  add object oFLGRAGG_1_22 as StdCheck with uid="ZHFJGAILIG",rtseq=21,rtrep=.f.,left=426, top=184, caption="Raggruppamento per cliente",;
    HelpContextID = 100477782,;
    cFormVar="w_FLGRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGRAGG_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLGRAGG_1_22.GetRadio()
    this.Parent.oContained.w_FLGRAGG = this.RadioValue()
    return .t.
  endfunc

  func oFLGRAGG_1_22.SetRadio()
    this.Parent.oContained.w_FLGRAGG=trim(this.Parent.oContained.w_FLGRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_FLGRAGG=='S',1,;
      0)
  endfunc

  add object oSELEZI_1_23 as StdRadio with uid="EVWZMNCSKG",rtseq=22,rtrep=.f.,left=10, top=450, width=138,height=36;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_23.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 109021914
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 109021914
      this.Buttons(2).Top=17
      this.SetAll("Width",136)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_23.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_23.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_24 as cp_outputCombo with uid="ZLERXIVPWT",left=237, top=450, width=393,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 181243418


  add object oBtn_1_25 as StdButton with uid="KQLSQFRVJN",left=534, top=476, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i solleciti selezionati";
    , HelpContextID = 217451482;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSSO_BZ2(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_datinv) and not empty(.w_orep))
      endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="EWTIKYIINT",left=233, top=476, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla dettaglio dell'insoluto";
    , HelpContextID = 131982449;
    , Caption='\<Dettaglio';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSSO_BZ1(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_COSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="IJQZSGQUFG",left=584, top=476, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184947270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESVAL_1_29 as StdField with uid="AJHPWHTSQW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 83762122,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=338, Top=33, InputMask=replicate('X',35)

  add object oDESLIN_1_30 as StdField with uid="BOHROBATVQ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 42474442,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=119, Top=136, InputMask=replicate('X',30)

  add object oNOMTES_1_36 as StdField with uid="NELHAHLVQC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NOMTES", cQueryName = "NOMTES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 230715690,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=143, Top=160, InputMask=replicate('X',50)


  add object ZoomInso as cp_szoombox with uid="QADFPNYNON",left=5, top=204, width=629,height=242,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CON_TENZ",cZoomFile="GSSO_SSS",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "CalcZoom",;
    nPag=1;
    , HelpContextID = 181243418


  add object oObj_1_53 as cp_runprogram with uid="BAYTHJPPRE",left=7, top=541, width=171,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSO_BZ1('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 181243418

  add object oDESINI_1_68 as StdField with uid="XPMJLQQCIQ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121314250,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=197, Top=59, InputMask=replicate('X',40)

  add object oDESFIN_1_69 as StdField with uid="ZABGWMZVYD",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42867658,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=197, Top=85, InputMask=replicate('X',40)

  add object oStr_1_38 as StdString with uid="KIFPAZEAEA",Visible=.t., Left=41, Top=5,;
    Alignment=1, Width=31, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="VHYWPWHNYC",Visible=.t., Left=218, Top=5,;
    Alignment=1, Width=65, Height=15,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KQDMVAGAEQ",Visible=.t., Left=418, Top=5,;
    Alignment=1, Width=50, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="VXPHUCWGQQ",Visible=.t., Left=34, Top=34,;
    Alignment=1, Width=38, Height=15,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="DGMUXCPMIN",Visible=.t., Left=218, Top=34,;
    Alignment=1, Width=65, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="FWWIJPMBVU",Visible=.t., Left=10, Top=136,;
    Alignment=1, Width=61, Height=15,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="ILDTXNYRCG",Visible=.t., Left=10, Top=160,;
    Alignment=1, Width=61, Height=15,;
    Caption="Testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="AYKOAFEAMV",Visible=.t., Left=413, Top=136,;
    Alignment=1, Width=122, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="WZVKLIDHKX",Visible=.t., Left=451, Top=160,;
    Alignment=1, Width=49, Height=15,;
    Caption="Mezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="EEEFLOCRTZ",Visible=.t., Left=6, Top=112,;
    Alignment=0, Width=210, Height=18,;
    Caption="Selezione testo sollecito"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="XQAYGSRVZL",Visible=.t., Left=150, Top=450,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="YMIURECZME",Visible=.t., Left=14, Top=59,;
    Alignment=1, Width=58, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="TCDAKCIQPW",Visible=.t., Left=23, Top=85,;
    Alignment=1, Width=49, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_52 as StdBox with uid="HBSLODPCIN",left=4, top=131, width=629,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_sso','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
