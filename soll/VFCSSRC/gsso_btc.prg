* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_btc                                                        *
*              Controlli in cancellazione testi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-08-30                                                      *
* Last revis.: 2004-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_btc",oParentObject)
return(i_retval)

define class tgsso_btc as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_MESS = space(100)
  w_NUMREG = 0
  w_DATREG = ctod("  /  /  ")
  w_COMPET = space(4)
  * --- WorkFile variables
  TES_CONT_idx=0
  DET_SOLL_idx=0
  CON_TENZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Controllo Testi Utilizzati nel dettaglio Solleciti
    *     Lanciato da GSSO_ATC
    * --- Read from DET_SOLL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DET_SOLL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_SOLL_idx,2],.t.,this.DET_SOLL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DSSERIAL"+;
        " from "+i_cTable+" DET_SOLL where ";
            +"DSRIFTES = "+cp_ToStrODBC(this.oParentObject.w_TCNUMERO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DSSERIAL;
        from (i_cTable) where;
            DSRIFTES = this.oParentObject.w_TCNUMERO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SERIAL = NVL(cp_ToDate(_read_.DSSERIAL),cp_NullValue(_read_.DSSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CON_TENZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCOMPET,CONUMREG,CODATREG"+;
        " from "+i_cTable+" CON_TENZ where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCOMPET,CONUMREG,CODATREG;
        from (i_cTable) where;
            COSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMPET = NVL(cp_ToDate(_read_.COCOMPET),cp_NullValue(_read_.COCOMPET))
      this.w_NUMREG = NVL(cp_ToDate(_read_.CONUMREG),cp_NullValue(_read_.CONUMREG))
      this.w_DATREG = NVL(cp_ToDate(_read_.CODATREG),cp_NullValue(_read_.CODATREG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(this.w_SERIAL)
      this.w_MESS = ah_Msgformat("Attenzione, testo utilizzato nel contenzioso di competenza %1%0Num. %2 del %3, impossibile cancellare", alltrim(this.w_compet) , alltrim(str(this.w_numreg)), dtoc(this.w_datreg) )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TES_CONT'
    this.cWorkTables[2]='DET_SOLL'
    this.cWorkTables[3]='CON_TENZ'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
