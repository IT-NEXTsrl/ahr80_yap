* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bvr                                                        *
*              Variazione di riga                                              *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-14                                                      *
* Last revis.: 2015-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bvr",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsso_bvr as StdBatch
  * --- Local variables
  w_PARAM = space(2)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    AH_ERRORMSG("Impossibile variare il contenuto della riga",48)
    do case
      case this.w_PARAM="CR"
        this.oParentObject.w_CRDATIEL=this.oParentObject.o_CRDATIEL
      case this.w_PARAM="RE"
        this.oParentObject.w_REDATIEL=this.oParentObject.o_REDATIEL
      case this.w_PARAM="FL"
        this.oParentObject.w_FLDATIEL=this.oParentObject.o_FLDATIEL
    endcase
  endproc


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
