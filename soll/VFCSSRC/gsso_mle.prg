* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mle                                                        *
*              Log errori ricezione flussi                                     *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2012-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_mle"))

* --- Class definition
define class tgsso_mle as StdTrsForm
  Top    = 8
  Left   = 21

  * --- Standard Properties
  Width  = 760
  Height = 355+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-16"
  HelpContextID=214537577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LOG_REBA_IDX = 0
  cFile = "LOG_REBA"
  cKeySelect = "ERNOMSUP,ERDTARIC,ERTIPIMP,ERNOMFIL"
  cKeyWhere  = "ERNOMSUP=this.w_ERNOMSUP and ERDTARIC=this.w_ERDTARIC and ERTIPIMP=this.w_ERTIPIMP and ERNOMFIL=this.w_ERNOMFIL"
  cKeyDetail  = "ERNOMSUP=this.w_ERNOMSUP and ERDTARIC=this.w_ERDTARIC and ERTIPIMP=this.w_ERTIPIMP and ERNOMFIL=this.w_ERNOMFIL"
  cKeyWhereODBC = '"ERNOMSUP="+cp_ToStrODBC(this.w_ERNOMSUP)';
      +'+" and ERDTARIC="+cp_ToStrODBC(this.w_ERDTARIC,"D")';
      +'+" and ERTIPIMP="+cp_ToStrODBC(this.w_ERTIPIMP)';
      +'+" and ERNOMFIL="+cp_ToStrODBC(this.w_ERNOMFIL)';

  cKeyDetailWhereODBC = '"ERNOMSUP="+cp_ToStrODBC(this.w_ERNOMSUP)';
      +'+" and ERDTARIC="+cp_ToStrODBC(this.w_ERDTARIC,"D")';
      +'+" and ERTIPIMP="+cp_ToStrODBC(this.w_ERTIPIMP)';
      +'+" and ERNOMFIL="+cp_ToStrODBC(this.w_ERNOMFIL)';

  cKeyWhereODBCqualified = '"LOG_REBA.ERNOMSUP="+cp_ToStrODBC(this.w_ERNOMSUP)';
      +'+" and LOG_REBA.ERDTARIC="+cp_ToStrODBC(this.w_ERDTARIC,"D")';
      +'+" and LOG_REBA.ERTIPIMP="+cp_ToStrODBC(this.w_ERTIPIMP)';
      +'+" and LOG_REBA.ERNOMFIL="+cp_ToStrODBC(this.w_ERNOMFIL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsso_mle"
  cComment = "Log errori ricezione flussi"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ERNOMSUP = space(20)
  w_ERDTARIC = ctod('  /  /  ')
  w_ERNUMRIG = 0
  o_ERNUMRIG = 0
  w_ERDESERR = space(200)
  w_ERTIPIMP = space(1)
  w_CAMRIG = space(200)
  w_ERNOMFIL = space(100)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'LOG_REBA','gsso_mle')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_mlePag1","gsso_mle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log errore ricezione flusso")
      .Pages(1).HelpContextID = 237337588
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oERNOMSUP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='LOG_REBA'
    * --- Area Manuale = Open Work Table
    * --- gsso_mle
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.DynamicBackColor= ;
       "RGB(255,128,128)"
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LOG_REBA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LOG_REBA_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ERNOMSUP = NVL(ERNOMSUP,space(20))
      .w_ERDTARIC = NVL(ERDTARIC,ctod("  /  /  "))
      .w_ERTIPIMP = NVL(ERTIPIMP,space(1))
      .w_ERNOMFIL = NVL(ERNOMFIL,space(100))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsso_mle
    this.w_ERDTARIC=IIF(EMPTY(this.w_ERDTARIC),i_DATSYS,this.w_ERDTARIC)
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LOG_REBA where ERNOMSUP=KeySet.ERNOMSUP
    *                            and ERDTARIC=KeySet.ERDTARIC
    *                            and ERTIPIMP=KeySet.ERTIPIMP
    *                            and ERNOMFIL=KeySet.ERNOMFIL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2],this.bLoadRecFilter,this.LOG_REBA_IDX,"gsso_mle")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LOG_REBA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LOG_REBA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LOG_REBA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ERNOMSUP',this.w_ERNOMSUP  ,'ERDTARIC',this.w_ERDTARIC  ,'ERTIPIMP',this.w_ERTIPIMP  ,'ERNOMFIL',this.w_ERNOMFIL  )
      select * from (i_cTable) LOG_REBA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ERNOMSUP = NVL(ERNOMSUP,space(20))
        .w_ERDTARIC = NVL(cp_ToDate(ERDTARIC),ctod("  /  /  "))
        .w_ERTIPIMP = NVL(ERTIPIMP,space(1))
        .w_ERNOMFIL = NVL(ERNOMFIL,space(100))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'LOG_REBA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ERNUMRIG = NVL(ERNUMRIG,0)
          .w_ERDESERR = NVL(ERDESERR,space(200))
        .w_CAMRIG = iif(.w_ERTIPIMP='D',LookTab("AVV_RIB","AVDATIEL","AVNOMSUP",.w_ERNOMSUP,"AVDTARIC",.w_ERDTARIC,"AVNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),  IIF (.w_ERTIPIMP='C',LookTab("CON_FRIC","CRDATIEL","CRNOMSUP",.w_ERNOMSUP,"CRDTARIC",.w_ERDTARIC,"CRNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),iif(.w_ERTIPIMP='R',LookTab("REN_DICO","REDATIEL","RENOMSUP",.w_ERNOMSUP,"REDTARIC",.w_ERDTARIC,"RENOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),LookTab("FLU_RITO","FLDATIEL","FLNOMSUP",.w_ERNOMSUP,"FLDTARIC",.w_ERDTARIC,"FLNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG))))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ERNOMSUP=space(20)
      .w_ERDTARIC=ctod("  /  /  ")
      .w_ERNUMRIG=0
      .w_ERDESERR=space(200)
      .w_ERTIPIMP=space(1)
      .w_CAMRIG=space(200)
      .w_ERNOMFIL=space(100)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_ERDTARIC = i_datsys
        .DoRTCalc(3,5,.f.)
        .w_CAMRIG = iif(.w_ERTIPIMP='D',LookTab("AVV_RIB","AVDATIEL","AVNOMSUP",.w_ERNOMSUP,"AVDTARIC",.w_ERDTARIC,"AVNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),  IIF (.w_ERTIPIMP='C',LookTab("CON_FRIC","CRDATIEL","CRNOMSUP",.w_ERNOMSUP,"CRDTARIC",.w_ERDTARIC,"CRNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),iif(.w_ERTIPIMP='R',LookTab("REN_DICO","REDATIEL","RENOMSUP",.w_ERNOMSUP,"REDTARIC",.w_ERDTARIC,"RENOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),LookTab("FLU_RITO","FLDATIEL","FLNOMSUP",.w_ERNOMSUP,"FLDTARIC",.w_ERDTARIC,"FLNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'LOG_REBA')
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oERTIPIMP_1_6.enabled = !i_bVal
      .Page1.oPag.oERNOMSUP_1_1.enabled = i_bVal
      .Page1.oPag.oERDTARIC_1_2.enabled = i_bVal
      .Page1.oPag.oERNOMFIL_1_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oERNOMSUP_1_1.enabled = .f.
        .Page1.oPag.oERDTARIC_1_2.enabled = .f.
        .Page1.oPag.oERNOMFIL_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oERNOMSUP_1_1.enabled = .t.
        .Page1.oPag.oERDTARIC_1_2.enabled = .t.
        .Page1.oPag.oERNOMFIL_1_8.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'LOG_REBA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ERNOMSUP,"ERNOMSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ERDTARIC,"ERDTARIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ERTIPIMP,"ERTIPIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ERNOMFIL,"ERNOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
    i_lTable = "LOG_REBA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.LOG_REBA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ERNUMRIG N(4);
      ,t_ERDESERR C(200);
      ,t_CAMRIG C(200);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_mlebodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oERNUMRIG_2_1.controlsource=this.cTrsName+'.t_ERNUMRIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oERDESERR_2_2.controlsource=this.cTrsName+'.t_ERDESERR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAMRIG_2_3.controlsource=this.cTrsName+'.t_CAMRIG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(67)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERNUMRIG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
      *
      * insert into LOG_REBA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LOG_REBA')
        i_extval=cp_InsertValODBCExtFlds(this,'LOG_REBA')
        i_cFldBody=" "+;
                  "(ERNOMSUP,ERDTARIC,ERNUMRIG,ERDESERR,ERTIPIMP"+;
                  ",ERNOMFIL,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ERNOMSUP)+","+cp_ToStrODBC(this.w_ERDTARIC)+","+cp_ToStrODBC(this.w_ERNUMRIG)+","+cp_ToStrODBC(this.w_ERDESERR)+","+cp_ToStrODBC(this.w_ERTIPIMP)+;
             ","+cp_ToStrODBC(this.w_ERNOMFIL)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LOG_REBA')
        i_extval=cp_InsertValVFPExtFlds(this,'LOG_REBA')
        cp_CheckDeletedKey(i_cTable,0,'ERNOMSUP',this.w_ERNOMSUP,'ERDTARIC',this.w_ERDTARIC,'ERTIPIMP',this.w_ERTIPIMP,'ERNOMFIL',this.w_ERNOMFIL)
        INSERT INTO (i_cTable) (;
                   ERNOMSUP;
                  ,ERDTARIC;
                  ,ERNUMRIG;
                  ,ERDESERR;
                  ,ERTIPIMP;
                  ,ERNOMFIL;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ERNOMSUP;
                  ,this.w_ERDTARIC;
                  ,this.w_ERNUMRIG;
                  ,this.w_ERDESERR;
                  ,this.w_ERTIPIMP;
                  ,this.w_ERNOMFIL;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (nvl(t_ERNUMRIG,0)<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LOG_REBA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LOG_REBA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (nvl(t_ERNUMRIG,0)<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LOG_REBA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LOG_REBA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ERNUMRIG="+cp_ToStrODBC(this.w_ERNUMRIG)+;
                     ",ERDESERR="+cp_ToStrODBC(this.w_ERDESERR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LOG_REBA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ERNUMRIG=this.w_ERNUMRIG;
                     ,ERDESERR=this.w_ERDESERR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (nvl(t_ERNUMRIG,0)<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LOG_REBA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (nvl(t_ERNUMRIG,0)<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_ERNUMRIG<>.w_ERNUMRIG
          .w_CAMRIG = iif(.w_ERTIPIMP='D',LookTab("AVV_RIB","AVDATIEL","AVNOMSUP",.w_ERNOMSUP,"AVDTARIC",.w_ERDTARIC,"AVNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),  IIF (.w_ERTIPIMP='C',LookTab("CON_FRIC","CRDATIEL","CRNOMSUP",.w_ERNOMSUP,"CRDTARIC",.w_ERDTARIC,"CRNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),iif(.w_ERTIPIMP='R',LookTab("REN_DICO","REDATIEL","RENOMSUP",.w_ERNOMSUP,"REDTARIC",.w_ERDTARIC,"RENOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),LookTab("FLU_RITO","FLDATIEL","FLNOMSUP",.w_ERNOMSUP,"FLDTARIC",.w_ERDTARIC,"FLNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG))))
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oERNOMSUP_1_1.value==this.w_ERNOMSUP)
      this.oPgFrm.Page1.oPag.oERNOMSUP_1_1.value=this.w_ERNOMSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oERDTARIC_1_2.value==this.w_ERDTARIC)
      this.oPgFrm.Page1.oPag.oERDTARIC_1_2.value=this.w_ERDTARIC
    endif
    if not(this.oPgFrm.Page1.oPag.oERTIPIMP_1_6.RadioValue()==this.w_ERTIPIMP)
      this.oPgFrm.Page1.oPag.oERTIPIMP_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oERNOMFIL_1_8.value==this.w_ERNOMFIL)
      this.oPgFrm.Page1.oPag.oERNOMFIL_1_8.value=this.w_ERNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERNUMRIG_2_1.value==this.w_ERNUMRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERNUMRIG_2_1.value=this.w_ERNUMRIG
      replace t_ERNUMRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERNUMRIG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERDESERR_2_2.value==this.w_ERDESERR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERDESERR_2_2.value=this.w_ERDESERR
      replace t_ERDESERR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oERDESERR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAMRIG_2_3.value==this.w_CAMRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAMRIG_2_3.value=this.w_CAMRIG
      replace t_CAMRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAMRIG_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'LOG_REBA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ERNOMSUP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oERNOMSUP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ERNOMSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ERNOMFIL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oERNOMFIL_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ERNOMFIL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (nvl(t_ERNUMRIG,0)<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if nvl(.w_ERNUMRIG,0)<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ERNUMRIG = this.w_ERNUMRIG
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(nvl(t_ERNUMRIG,0)<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ERNUMRIG=0
      .w_ERDESERR=space(200)
      .w_CAMRIG=space(200)
      .DoRTCalc(1,5,.f.)
        .w_CAMRIG = iif(.w_ERTIPIMP='D',LookTab("AVV_RIB","AVDATIEL","AVNOMSUP",.w_ERNOMSUP,"AVDTARIC",.w_ERDTARIC,"AVNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),  IIF (.w_ERTIPIMP='C',LookTab("CON_FRIC","CRDATIEL","CRNOMSUP",.w_ERNOMSUP,"CRDTARIC",.w_ERDTARIC,"CRNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),iif(.w_ERTIPIMP='R',LookTab("REN_DICO","REDATIEL","RENOMSUP",.w_ERNOMSUP,"REDTARIC",.w_ERDTARIC,"RENOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG),LookTab("FLU_RITO","FLDATIEL","FLNOMSUP",.w_ERNOMSUP,"FLDTARIC",.w_ERDTARIC,"FLNOMFIL",.w_ERNOMFIL,"CPROWNUM",.w_ERNUMRIG))))
    endwith
    this.DoRTCalc(7,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ERNUMRIG = t_ERNUMRIG
    this.w_ERDESERR = t_ERDESERR
    this.w_CAMRIG = t_CAMRIG
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ERNUMRIG with this.w_ERNUMRIG
    replace t_ERDESERR with this.w_ERDESERR
    replace t_CAMRIG with this.w_CAMRIG
    if i_srv='A'
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg('MSG_CANNOT_ADD')
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_mlePag1 as StdContainer
  Width  = 756
  height = 359
  stdWidth  = 756
  stdheight = 359
  resizeXpos=239
  resizeYpos=190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oERNOMSUP_1_1 as StdField with uid="BJNCVUVGPD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ERNOMSUP", cQueryName = "ERNOMSUP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome supporto",;
    HelpContextID = 190488982,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=138, Top=14, InputMask=replicate('X',20)

  add object oERDTARIC_1_2 as StdField with uid="WGXMORKMLB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ERDTARIC", cQueryName = "ERNOMSUP,ERDTARIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione",;
    HelpContextID = 107019895,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=457, Top=14


  add object oERTIPIMP_1_6 as StdCombo with uid="MGFSCNEHYE",rtseq=5,rtrep=.f.,left=545,top=14,width=143,height=21, enabled=.f.;
    , HelpContextID = 242941546;
    , cFormVar="w_ERTIPIMP",RowSource=""+"Flusso di ritorno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oERTIPIMP_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ERTIPIMP,&i_cF..t_ERTIPIMP),this.value)
    return(iif(xVal =1,'F',;
    space(1)))
  endfunc
  func oERTIPIMP_1_6.GetRadio()
    this.Parent.oContained.w_ERTIPIMP = this.RadioValue()
    return .t.
  endfunc

  func oERTIPIMP_1_6.ToRadio()
    this.Parent.oContained.w_ERTIPIMP=trim(this.Parent.oContained.w_ERTIPIMP)
    return(;
      iif(this.Parent.oContained.w_ERTIPIMP=='F',1,;
      0))
  endfunc

  func oERTIPIMP_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oBtn_1_7 as StdButton with uid="JKDXEKYIKS",left=698, top=15, width=48,height=45,;
    CpPicture="bmp\Visualflu.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i movimenti";
    , HelpContextID = 144843798;
    , Caption='\<Vis. Mov.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        gsso_BLE(this.Parent.oContained,.w_ERNOMSUP,.w_ERDTARIC,.w_ERNOMFIL,.w_ERTIPIMP)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_ERNOMSUP))
    endwith
   endif
  endfunc

  add object oERNOMFIL_1_8 as StdField with uid="VSWAGHTZWE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ERNOMFIL", cQueryName = "ERNOMSUP,ERDTARIC,ERTIPIMP,ERNOMFIL",;
    bObbl = .t. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome file importazione",;
    HelpContextID = 27614830,;
   bGlobalFont=.t.,;
    Height=21, Width=549, Left=138, Top=41, InputMask=replicate('X',100)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=24, top=69, width=723,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="ERNUMRIG",Label1="N.riga",Field2="ERDESERR",Label2="Descrizione errore / riga gestione",Field3="CAMRIG",Label3="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101553274

  add object oStr_1_4 as StdString with uid="JANPERCFJY",Visible=.t., Left=17, Top=14,;
    Alignment=1, Width=117, Height=19,;
    Caption="Nome supporto:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="NJXTCDDCWP",Visible=.t., Left=305, Top=14,;
    Alignment=1, Width=149, Height=18,;
    Caption="Data creazione ricezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XDZFOQPQDT",Visible=.t., Left=4, Top=42,;
    Alignment=1, Width=132, Height=18,;
    Caption="Nome file importato:"  ;
  , bGlobalFont=.t.

  add object oBox_1_3 as StdBox with uid="KNJEYQHQLR",left=67, top=70, width=1,height=289

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=14,top=88,;
    width=719+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=15,top=89,width=718+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsso_mleBodyRow as CPBodyRowCnt
  Width=709
  Height=int(fontmetric(1,"Arial",9,"")*2*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oERNUMRIG_2_1 as StdTrsField with uid="ZQUBQVSQPT",rtseq=3,rtrep=.t.,;
    cFormVar="w_ERNUMRIG",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 94330483,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0

  add object oERDESERR_2_2 as StdTrsField with uid="TBMPSJJNCE",rtseq=4,rtrep=.t.,;
    cFormVar="w_ERDESERR",value=space(200),;
    ToolTipText = "Descrizione errore",;
    HelpContextID = 229638552,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=622, Left=43, Top=0, InputMask=replicate('X',200), tabstop = .f., readonly = .t., BackStyle=0

  add object oCAMRIG_2_3 as StdTrsField with uid="GNXORJSKSH",rtseq=6,rtrep=.t.,;
    cFormVar="w_CAMRIG",value=space(200),;
    ToolTipText = "Campo dati record",;
    HelpContextID = 253591590,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=642, Left=62, Top=19, InputMask=replicate('X',200), tabstop = .f., readonly = .t.
  add object oLast as LastKeyMover
  * ---
  func oERNUMRIG_2_1.When()
    return(.t.)
  proc oERNUMRIG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oERNUMRIG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mle','LOG_REBA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ERNOMSUP=LOG_REBA.ERNOMSUP";
  +" and "+i_cAliasName2+".ERDTARIC=LOG_REBA.ERDTARIC";
  +" and "+i_cAliasName2+".ERTIPIMP=LOG_REBA.ERTIPIMP";
  +" and "+i_cAliasName2+".ERNOMFIL=LOG_REBA.ERNOMFIL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
