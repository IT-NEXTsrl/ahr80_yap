* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_scf                                                        *
*              Stampa controllo flussi                                         *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-11                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_scf",oParentObject))

* --- Class definition
define class tgsso_scf as StdForm
  Top    = 93
  Left   = 36

  * --- Standard Properties
  Width  = 623
  Height = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-23"
  HelpContextID=90805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  FLU_RITO_IDX = 0
  cPrg = "gsso_scf"
  cComment = "Stampa controllo flussi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOCONF = space(1)
  o_TIPOCONF = space(1)
  w_TIPOSTA = space(1)
  o_TIPOSTA = space(1)
  w_NOMSUPINF = space(20)
  w_NOMSUPFIF = space(20)
  w_DATCREIN = ctod('  /  /  ')
  w_DATCREFI = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_scfPag1","gsso_scf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOCONF_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FLU_RITO'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSso_BSR with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOCONF=space(1)
      .w_TIPOSTA=space(1)
      .w_NOMSUPINF=space(20)
      .w_NOMSUPFIF=space(20)
      .w_DATCREIN=ctod("  /  /  ")
      .w_DATCREFI=ctod("  /  /  ")
        .w_TIPOCONF = 'C'
        .w_TIPOSTA = 'F'
        .w_NOMSUPINF = space(20)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_NOMSUPINF))
          .link_1_4('Full')
        endif
        .w_NOMSUPFIF = space(20)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_NOMSUPFIF))
          .link_1_5('Full')
        endif
    endwith
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_TIPOSTA<>.w_TIPOSTA.or. .o_TIPOCONF<>.w_TIPOCONF
            .w_NOMSUPINF = space(20)
          .link_1_4('Full')
        endif
        if .o_TIPOSTA<>.w_TIPOSTA.or. .o_TIPOCONF<>.w_TIPOCONF
            .w_NOMSUPFIF = space(20)
          .link_1_5('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NOMSUPINF
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_lTable = "FLU_RITO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2], .t., this.FLU_RITO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMSUPINF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsso_MFR',True,'FLU_RITO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNOMSUP like "+cp_ToStrODBC(trim(this.w_NOMSUPINF)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNOMSUP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNOMSUP',trim(this.w_NOMSUPINF))
          select FLNOMSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNOMSUP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMSUPINF)==trim(_Link_.FLNOMSUP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMSUPINF) and !this.bDontReportError
            deferred_cp_zoom('FLU_RITO','*','FLNOMSUP',cp_AbsName(oSource.parent,'oNOMSUPINF_1_4'),i_cWhere,'gsso_MFR',"Flussi operativi di ritorno",'gsso2SCF.FLU_RITO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                     +" from "+i_cTable+" "+i_lTable+" where FLNOMSUP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMSUP',oSource.xKey(1))
            select FLNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMSUPINF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                   +" from "+i_cTable+" "+i_lTable+" where FLNOMSUP="+cp_ToStrODBC(this.w_NOMSUPINF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMSUP',this.w_NOMSUPINF)
            select FLNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMSUPINF = NVL(_Link_.FLNOMSUP,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_NOMSUPINF = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_NOMSUPINF <= .w_NOMSUPFIF) OR EMPTY(.w_NOMSUPFIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il nome del supporto iniziale � maggiore di quello finale")
        endif
        this.w_NOMSUPINF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])+'\'+cp_ToStr(_Link_.FLNOMSUP,1)
      cp_ShowWarn(i_cKey,this.FLU_RITO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMSUPINF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMSUPFIF
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_lTable = "FLU_RITO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2], .t., this.FLU_RITO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMSUPFIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsso_MFR',True,'FLU_RITO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNOMSUP like "+cp_ToStrODBC(trim(this.w_NOMSUPFIF)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNOMSUP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNOMSUP',trim(this.w_NOMSUPFIF))
          select FLNOMSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNOMSUP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMSUPFIF)==trim(_Link_.FLNOMSUP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMSUPFIF) and !this.bDontReportError
            deferred_cp_zoom('FLU_RITO','*','FLNOMSUP',cp_AbsName(oSource.parent,'oNOMSUPFIF_1_5'),i_cWhere,'gsso_MFR',"Flussi operativi di ritorno",'gsso2SCF.FLU_RITO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                     +" from "+i_cTable+" "+i_lTable+" where FLNOMSUP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMSUP',oSource.xKey(1))
            select FLNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMSUPFIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMSUP";
                   +" from "+i_cTable+" "+i_lTable+" where FLNOMSUP="+cp_ToStrODBC(this.w_NOMSUPFIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMSUP',this.w_NOMSUPFIF)
            select FLNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMSUPFIF = NVL(_Link_.FLNOMSUP,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_NOMSUPFIF = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_NOMSUPINF <= .w_NOMSUPFIF) OR EMPTY(.w_NOMSUPINF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il nome del supporto finale � minore di quello iniziale")
        endif
        this.w_NOMSUPFIF = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])+'\'+cp_ToStr(_Link_.FLNOMSUP,1)
      cp_ShowWarn(i_cKey,this.FLU_RITO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMSUPFIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOCONF_1_1.RadioValue()==this.w_TIPOCONF)
      this.oPgFrm.Page1.oPag.oTIPOCONF_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTA_1_2.RadioValue()==this.w_TIPOSTA)
      this.oPgFrm.Page1.oPag.oTIPOSTA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMSUPINF_1_4.value==this.w_NOMSUPINF)
      this.oPgFrm.Page1.oPag.oNOMSUPINF_1_4.value=this.w_NOMSUPINF
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMSUPFIF_1_5.value==this.w_NOMSUPFIF)
      this.oPgFrm.Page1.oPag.oNOMSUPFIF_1_5.value=this.w_NOMSUPFIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREIN_1_6.value==this.w_DATCREIN)
      this.oPgFrm.Page1.oPag.oDATCREIN_1_6.value=this.w_DATCREIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREFI_1_8.value==this.w_DATCREFI)
      this.oPgFrm.Page1.oPag.oDATCREFI_1_8.value=this.w_DATCREFI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_NOMSUPINF <= .w_NOMSUPFIF) OR EMPTY(.w_NOMSUPFIF))  and not(empty(.w_NOMSUPINF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMSUPINF_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome del supporto iniziale � maggiore di quello finale")
          case   not((.w_NOMSUPINF <= .w_NOMSUPFIF) OR EMPTY(.w_NOMSUPINF))  and not(empty(.w_NOMSUPFIF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMSUPFIF_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome del supporto finale � minore di quello iniziale")
          case   not((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREFI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore di quella iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOCONF = this.w_TIPOCONF
    this.o_TIPOSTA = this.w_TIPOSTA
    return

enddefine

* --- Define pages as container
define class tgsso_scfPag1 as StdContainer
  Width  = 619
  height = 112
  stdWidth  = 619
  stdheight = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOCONF_1_1 as StdCombo with uid="UVJRMFGIJF",rtseq=1,rtrep=.f.,left=137,top=6,width=101,height=21;
    , ToolTipText = "Stato della conferma ricezione (corretta/errata)";
    , HelpContextID = 31803012;
    , cFormVar="w_TIPOCONF",RowSource=""+"Corretto,"+"Errato,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCONF_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPOCONF_1_1.GetRadio()
    this.Parent.oContained.w_TIPOCONF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCONF_1_1.SetRadio()
    this.Parent.oContained.w_TIPOCONF=trim(this.Parent.oContained.w_TIPOCONF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCONF=='C',1,;
      iif(this.Parent.oContained.w_TIPOCONF=='E',2,;
      iif(this.Parent.oContained.w_TIPOCONF=='T',3,;
      0)))
  endfunc


  add object oTIPOSTA_1_2 as StdCombo with uid="HXCYWADIYZ",rtseq=2,rtrep=.f.,left=377,top=6,width=198,height=21, enabled=.f.;
    , ToolTipText = "Selezione del tipo di stampa da eseguire";
    , HelpContextID = 68860214;
    , cFormVar="w_TIPOSTA",RowSource=""+"Flussi operativi di ritorno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTA_1_2.RadioValue()
    return(iif(this.value =1,'F',;
    space(1)))
  endfunc
  func oTIPOSTA_1_2.GetRadio()
    this.Parent.oContained.w_TIPOSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTA_1_2.SetRadio()
    this.Parent.oContained.w_TIPOSTA=trim(this.Parent.oContained.w_TIPOSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTA=='F',1,;
      0)
  endfunc

  add object oNOMSUPINF_1_4 as StdField with uid="KAPITWNLDO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NOMSUPINF", cQueryName = "NOMSUPINF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome del supporto iniziale � maggiore di quello finale",;
    ToolTipText = "Nome del supporto iniziale",;
    HelpContextID = 264334460,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=137, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FLU_RITO", cZoomOnZoom="gsso_MFR", oKey_1_1="FLNOMSUP", oKey_1_2="this.w_NOMSUPINF"

  func oNOMSUPINF_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMSUPINF_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMSUPINF_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLU_RITO','*','FLNOMSUP',cp_AbsName(this.parent,'oNOMSUPINF_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'gsso_MFR',"Flussi operativi di ritorno",'gsso2SCF.FLU_RITO_VZM',this.parent.oContained
  endproc
  proc oNOMSUPINF_1_4.mZoomOnZoom
    local i_obj
    i_obj=gsso_MFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FLNOMSUP=this.parent.oContained.w_NOMSUPINF
     i_obj.ecpSave()
  endproc

  add object oNOMSUPFIF_1_5 as StdField with uid="RJUAURRDNH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NOMSUPFIF", cQueryName = "NOMSUPFIF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome del supporto finale � minore di quello iniziale",;
    ToolTipText = "Nome del supporto finale",;
    HelpContextID = 264334465,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=441, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FLU_RITO", cZoomOnZoom="gsso_MFR", oKey_1_1="FLNOMSUP", oKey_1_2="this.w_NOMSUPFIF"

  func oNOMSUPFIF_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMSUPFIF_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMSUPFIF_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLU_RITO','*','FLNOMSUP',cp_AbsName(this.parent,'oNOMSUPFIF_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsso_MFR',"Flussi operativi di ritorno",'gsso2SCF.FLU_RITO_VZM',this.parent.oContained
  endproc
  proc oNOMSUPFIF_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsso_MFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FLNOMSUP=this.parent.oContained.w_NOMSUPFIF
     i_obj.ecpSave()
  endproc

  add object oDATCREIN_1_6 as StdField with uid="ROYQUKZJZX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATCREIN", cQueryName = "DATCREIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data creazione iniziale",;
    HelpContextID = 184618876,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=137, Top=63

  func oDATCREIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREFI))
    endwith
    return bRes
  endfunc

  add object oDATCREFI_1_8 as StdField with uid="VPRQRKDJWB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATCREFI", cQueryName = "DATCREFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore di quella iniziale",;
    ToolTipText = "Data creazione finale",;
    HelpContextID = 83816575,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=352, Top=63

  func oDATCREFI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREIN))
    endwith
    return bRes
  endfunc


  add object oBtn_1_10 as StdButton with uid="OSXGXXPXXC",left=507, top=61, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 140310;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        do gsso_BSR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="BWTKCLQRSS",left=558, top=61, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 140310;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="PCACAIFDMJ",Visible=.t., Left=7, Top=39,;
    Alignment=1, Width=128, Height=18,;
    Caption="Da nome supporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="OZHAPFJMZN",Visible=.t., Left=23, Top=67,;
    Alignment=1, Width=112, Height=18,;
    Caption="Da data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RWOUOWBKBH",Visible=.t., Left=250, Top=67,;
    Alignment=1, Width=98, Height=18,;
    Caption="A data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JLZEGUAYPD",Visible=.t., Left=317, Top=38,;
    Alignment=1, Width=123, Height=18,;
    Caption="A nome supporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="PRFZRUDLEF",Visible=.t., Left=91, Top=9,;
    Alignment=1, Width=44, Height=17,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="PTQVYNWRMY",Visible=.t., Left=278, Top=8,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_scf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
