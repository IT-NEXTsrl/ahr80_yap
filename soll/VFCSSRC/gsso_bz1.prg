* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bz1                                                        *
*              Gestione stampa solleciti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-05-19                                                      *
* Last revis.: 1998-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bz1",oParentObject,m.pOPER)
return(i_retval)

define class tgsso_bz1 as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_ZOOM = space(10)
  w_CONTEN = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- gestioni di Contorno Maschera Stampa Solleciti (da GSSO_SSS)
    * --- Parametro Operazione: D =Dettaglio, S =Seleziona/Deseleziona Tutto
    * --- Questo oggetto sar� definito come Gestione Contenziosi
    do case
      case this.pOPER="D"
        * --- Visualizza il Contenzioso dell'Oggettino Zoom selezionato nella Maschera
        if EMPTY(this.oParentObject.w_COSERIAL)
          ah_ErrorMsg("Selezionare una riga",,"")
          i_retcode = 'stop'
          return
        endif
        * --- definisco l'oggetto come appartenente alla classe Gestione Contenziosi
        this.w_CONTEN = GSSO_MCO()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTEN.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- inizializzo la chiave della Prima Nota
        this.w_CONTEN.w_COSERIAL = this.oParentObject.w_COSERIAL
        * --- creo il curosre delle solo chiavi
        this.w_CONTEN.QueryKeySet("COSERIAL='"+this.oParentObject.w_COSERIAL+ "'","")     
        * --- mi metto in interrogazione
        this.w_CONTEN.LoadRec()     
      case this.pOPER="S"
        * --- Seleziona / Deseleziona tutti i contenziosi presenti nell'oggettino Zoom della maschera
        this.w_ZOOM = this.oParentObject.w_ZoomInso
        UPDATE ( this.w_ZOOM.cCursor ) SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
