* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_api                                                        *
*              Piano contenziosi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_273]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-15                                                      *
* Last revis.: 2015-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_api"))

* --- Class definition
define class tgsso_api as StdForm
  Top    = 4
  Left   = 10

  * --- Standard Properties
  Width  = 699
  Height = 517+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-13"
  HelpContextID=108423831
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Constant Properties
  GES_PIAN_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  PAR_CONT_IDX = 0
  COC_MAST_IDX = 0
  cFile = "GES_PIAN"
  cKeySelect = "GPCODRAG,GPCOMPET"
  cKeyWhere  = "GPCODRAG=this.w_GPCODRAG and GPCOMPET=this.w_GPCOMPET"
  cKeyWhereODBC = '"GPCODRAG="+cp_ToStrODBC(this.w_GPCODRAG)';
      +'+" and GPCOMPET="+cp_ToStrODBC(this.w_GPCOMPET)';

  cKeyWhereODBCqualified = '"GES_PIAN.GPCODRAG="+cp_ToStrODBC(this.w_GPCODRAG)';
      +'+" and GES_PIAN.GPCOMPET="+cp_ToStrODBC(this.w_GPCOMPET)';

  cPrg = "gsso_api"
  cComment = "Piano contenziosi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPAR = space(5)
  w_IMPSPE1 = 0
  w_GPCODRAG = space(10)
  o_GPCODRAG = space(10)
  w_GPCOMPET = space(4)
  w_GPDATREG = ctod('  /  /  ')
  o_GPDATREG = ctod('  /  /  ')
  w_GP__TIPO = space(1)
  o_GP__TIPO = space(1)
  w_CONTENZ = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_GPCODVAL = space(3)
  o_GPCODVAL = space(3)
  w_GPCAOVAL = 0
  w_GPTIPEFF = space(1)
  w_GPADDINT = space(1)
  o_GPADDINT = space(1)
  w_GPPERINT = 0
  w_GPSAINTC = space(1)
  w_GPSCAINI = ctod('  /  /  ')
  w_GPADDSOL = space(1)
  w_GPSCAFIN = ctod('  /  /  ')
  w_GPADDSPE = space(1)
  o_GPADDSPE = space(1)
  w_GPDATVAL = ctod('  /  /  ')
  w_GPDESSPE = space(1)
  w_GPTIPCON = space(1)
  w_DATVAL = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_SIMVAL = space(5)
  w_CAMBIO1 = 0
  w_SCAINI = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_SCAFIN = ctod('  /  /  ')
  w_CAOVAL = 0
  w_CAMBIO = 0
  w_SELEZI = space(1)
  w_TIPO = space(10)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = 0
  w_VALPAR1 = space(3)
  w_VALPAR = space(3)
  w_DEC = 0
  w_PAGMOR = space(5)
  w_DURMOR = 0
  w_DATOBSO = ctod('  /  /  ')
  w_DATREG = space(10)
  w_FASE = 0
  w_GPCODCON = space(15)
  w_DESCON = space(40)
  w_GPCATPAG = space(2)
  o_GPCATPAG = space(2)
  w_GPBANSEL = space(15)
  w_GPBANPRE = space(15)
  w_GPRIFDIS = space(10)
  w_STATUS = space(2)
  w_VALCLF = space(3)
  w_CALCPIP = 0
  w_GPIMPSPE = 0
  w_CODCON = space(15)
  w_RIFDIS = space(10)
  w_CATPAG = space(2)
  w_CONTRO = space(15)
  w_TIPPAG = space(10)
  w_DINUMERO = 0
  w_DATDIS = ctod('  /  /  ')
  w_TIPSBF = space(1)
  w_DESBAN = space(35)
  w_DESPRE = space(35)
  w_BANSEL = space(15)
  w_CONASS = space(10)
  w_NUMERO = 0
  w_CODAGE = space(5)
  w_DESCON = space(10)
  w_FLNOTE = space(1)
  w_DESAGE = space(10)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_GPCOMPET = this.W_GPCOMPET
  op_GPCODRAG = this.W_GPCODRAG
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GES_PIAN','gsso_api')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_apiPag1","gsso_api",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano")
      .Pages(1).HelpContextID = 232450294
      .Pages(2).addobject("oPag","tgsso_apiPag2","gsso_api",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati aggiuntivi")
      .Pages(2).HelpContextID = 151884755
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGPCODRAG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
      DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PAR_CONT'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='GES_PIAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GES_PIAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GES_PIAN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GPCODRAG = NVL(GPCODRAG,space(10))
      .w_GPCOMPET = NVL(GPCOMPET,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GES_PIAN where GPCODRAG=KeySet.GPCODRAG
    *                            and GPCOMPET=KeySet.GPCOMPET
    *
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GES_PIAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GES_PIAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GES_PIAN '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GPCODRAG',this.w_GPCODRAG  ,'GPCOMPET',this.w_GPCOMPET  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_IMPSPE1 = 0
        .w_DATVAL = i_DATSYS
        .w_SIMVAL = space(5)
        .w_CAMBIO1 = 0
        .w_SELEZI = space(1)
        .w_DECTOT = 0
        .w_VALPAR1 = space(3)
        .w_DEC = 0
        .w_PAGMOR = space(5)
        .w_DURMOR = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_FASE = 0
        .w_DESCON = space(40)
        .w_STATUS = space(2)
        .w_VALCLF = space(3)
        .w_CONTRO = space(15)
        .w_DINUMERO = 0
        .w_DATDIS = ctod("  /  /  ")
        .w_TIPSBF = space(1)
        .w_DESBAN = space(35)
        .w_NUMERO = 0
        .w_CODAGE = space(5)
        .w_DESCON = space(10)
        .w_FLNOTE = space(1)
        .w_DESAGE = space(10)
        .w_READPAR = i_CODAZI
          .link_1_1('Load')
        .w_GPCODRAG = NVL(GPCODRAG,space(10))
        .op_GPCODRAG = .w_GPCODRAG
        .w_GPCOMPET = NVL(GPCOMPET,space(4))
        .op_GPCOMPET = .w_GPCOMPET
        .w_GPDATREG = NVL(cp_ToDate(GPDATREG),ctod("  /  /  "))
        .w_GP__TIPO = NVL(GP__TIPO,space(1))
        .w_CONTENZ = .w_GP__TIPO
        .w_OBTEST = .w_GPDATREG
        .w_GPCODVAL = NVL(GPCODVAL,space(3))
          if link_1_10_joined
            this.w_GPCODVAL = NVL(VACODVAL110,NVL(this.w_GPCODVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL110,space(5))
            this.w_CAMBIO1 = NVL(VACAOVAL110,0)
            this.w_DECTOT = NVL(VADECTOT110,0)
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO110),ctod("  /  /  "))
          else
          .link_1_10('Load')
          endif
        .w_GPCAOVAL = NVL(GPCAOVAL,0)
        .w_GPTIPEFF = NVL(GPTIPEFF,space(1))
        .w_GPADDINT = NVL(GPADDINT,space(1))
        .w_GPPERINT = NVL(GPPERINT,0)
        .w_GPSAINTC = NVL(GPSAINTC,space(1))
        .w_GPSCAINI = NVL(cp_ToDate(GPSCAINI),ctod("  /  /  "))
        .w_GPADDSOL = NVL(GPADDSOL,space(1))
        .w_GPSCAFIN = NVL(cp_ToDate(GPSCAFIN),ctod("  /  /  "))
        .w_GPADDSPE = NVL(GPADDSPE,space(1))
        .w_GPDATVAL = NVL(cp_ToDate(GPDATVAL),ctod("  /  /  "))
        .w_GPDESSPE = NVL(GPDESSPE,space(1))
        .w_GPTIPCON = NVL(GPTIPCON,space(1))
        .w_TIPCON = .w_GPTIPCON
        .w_SCAINI = .w_GPSCAINI
        .w_CODVAL = .w_GPCODVAL
        .w_SCAFIN = .w_GPSCAFIN
        .w_CAOVAL = .w_GPCAOVAL
        .w_CAMBIO = .w_GPCAOVAL
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_TIPO = .w_GP__TIPO
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1),.w_VALPAR1,g_PERVAL)
          .link_1_47('Load')
        .w_DATREG = .w_GPDATREG
        .w_GPCODCON = NVL(GPCODCON,space(15))
          .link_2_1('Load')
        .w_GPCATPAG = NVL(GPCATPAG,space(2))
        .w_GPBANSEL = NVL(GPBANSEL,space(15))
          if link_2_4_joined
            this.w_GPBANSEL = NVL(BACODBAN204,NVL(this.w_GPBANSEL,space(15)))
            this.w_DESBAN = NVL(BADESCRI204,space(35))
            this.w_GPBANPRE = NVL(BACONASS204,space(15))
            this.w_TIPSBF = NVL(BACONSBF204,space(1))
            this.w_CONASS = NVL(BACONASS204,space(10))
          else
          .link_2_4('Load')
          endif
        .w_GPBANPRE = NVL(GPBANPRE,space(15))
          if link_2_5_joined
            this.w_GPBANPRE = NVL(BACODBAN205,NVL(this.w_GPBANPRE,space(15)))
            this.w_DESPRE = NVL(BADESCRI205,space(35))
          else
          .link_2_5('Load')
          endif
        .w_GPRIFDIS = NVL(GPRIFDIS,space(10))
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .w_CALCPIP = DEFPIP(.w_DEC)
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .w_GPIMPSPE = NVL(GPIMPSPE,0)
        .w_CODCON = .w_GPCODCON
        .w_RIFDIS = .w_GPRIFDIS
        .w_CATPAG = IIF(.w_GPCATPAG='TT', '  ', .w_GPCATPAG)
        .w_TIPPAG = IIF(.w_CATPAG='TT','  ',.w_CATPAG)
        .w_DESPRE = LOOKTAB('COC_MAST', 'BADESCRI', 'BACODBAN', .w_GPBANPRE)
        .w_BANSEL = .w_GPBANSEL
        .w_CONASS = IIF(.w_TIPSBF='S', .w_CONASS, .w_BANSEL)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GES_PIAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR = space(5)
      .w_IMPSPE1 = 0
      .w_GPCODRAG = space(10)
      .w_GPCOMPET = space(4)
      .w_GPDATREG = ctod("  /  /  ")
      .w_GP__TIPO = space(1)
      .w_CONTENZ = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_GPCODVAL = space(3)
      .w_GPCAOVAL = 0
      .w_GPTIPEFF = space(1)
      .w_GPADDINT = space(1)
      .w_GPPERINT = 0
      .w_GPSAINTC = space(1)
      .w_GPSCAINI = ctod("  /  /  ")
      .w_GPADDSOL = space(1)
      .w_GPSCAFIN = ctod("  /  /  ")
      .w_GPADDSPE = space(1)
      .w_GPDATVAL = ctod("  /  /  ")
      .w_GPDESSPE = space(1)
      .w_GPTIPCON = space(1)
      .w_DATVAL = ctod("  /  /  ")
      .w_TIPCON = space(1)
      .w_SIMVAL = space(5)
      .w_CAMBIO1 = 0
      .w_SCAINI = ctod("  /  /  ")
      .w_CODVAL = space(3)
      .w_SCAFIN = ctod("  /  /  ")
      .w_CAOVAL = 0
      .w_CAMBIO = 0
      .w_SELEZI = space(1)
      .w_TIPO = space(10)
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_VALPAR1 = space(3)
      .w_VALPAR = space(3)
      .w_DEC = 0
      .w_PAGMOR = space(5)
      .w_DURMOR = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_DATREG = space(10)
      .w_FASE = 0
      .w_GPCODCON = space(15)
      .w_DESCON = space(40)
      .w_GPCATPAG = space(2)
      .w_GPBANSEL = space(15)
      .w_GPBANPRE = space(15)
      .w_GPRIFDIS = space(10)
      .w_STATUS = space(2)
      .w_VALCLF = space(3)
      .w_CALCPIP = 0
      .w_GPIMPSPE = 0
      .w_CODCON = space(15)
      .w_RIFDIS = space(10)
      .w_CATPAG = space(2)
      .w_CONTRO = space(15)
      .w_TIPPAG = space(10)
      .w_DINUMERO = 0
      .w_DATDIS = ctod("  /  /  ")
      .w_TIPSBF = space(1)
      .w_DESBAN = space(35)
      .w_DESPRE = space(35)
      .w_BANSEL = space(15)
      .w_CONASS = space(10)
      .w_NUMERO = 0
      .w_CODAGE = space(5)
      .w_DESCON = space(10)
      .w_FLNOTE = space(1)
      .w_DESAGE = space(10)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_READPAR = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_READPAR))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_GPCOMPET = g_CODESE
        .w_GPDATREG = i_datsys
        .w_GP__TIPO = 'I'
        .w_CONTENZ = .w_GP__TIPO
        .w_OBTEST = .w_GPDATREG
        .w_GPCODVAL = g_PERVAL
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_GPCODVAL))
          .link_1_10('Full')
          endif
        .w_GPCAOVAL =  GETCAM(.w_GPCODVAL, .w_DATVAL, 7)
        .w_GPTIPEFF = 'E'
        .w_GPADDINT = 'N'
        .w_GPPERINT = IIF(.w_GPADDINT='S', .w_GPPERINT, 0)
        .w_GPSAINTC = 'N'
        .w_GPSCAINI = .w_GPDATREG-60
        .w_GPADDSOL = 'S'
        .w_GPSCAFIN = .w_GPDATREG
        .w_GPADDSPE = IIF(.w_GP__TIPO='I','S',' ')
          .DoRTCalc(19,19,.f.)
        .w_GPDESSPE = 'C'
        .w_GPTIPCON = 'C'
        .w_DATVAL = i_DATSYS
        .w_TIPCON = .w_GPTIPCON
          .DoRTCalc(24,25,.f.)
        .w_SCAINI = .w_GPSCAINI
        .w_CODVAL = .w_GPCODVAL
        .w_SCAFIN = .w_GPSCAFIN
        .w_CAOVAL = .w_GPCAOVAL
        .w_CAMBIO = .w_GPCAOVAL
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
          .DoRTCalc(31,31,.f.)
        .w_TIPO = .w_GP__TIPO
          .DoRTCalc(33,33,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(35,35,.f.)
        .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1),.w_VALPAR1,g_PERVAL)
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_VALPAR))
          .link_1_47('Full')
          endif
          .DoRTCalc(37,40,.f.)
        .w_DATREG = .w_GPDATREG
        .w_FASE = 0
        .DoRTCalc(43,43,.f.)
          if not(empty(.w_GPCODCON))
          .link_2_1('Full')
          endif
          .DoRTCalc(44,44,.f.)
        .w_GPCATPAG = 'TT'
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_GPBANSEL))
          .link_2_4('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_GPBANPRE))
          .link_2_5('Full')
          endif
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
          .DoRTCalc(48,50,.f.)
        .w_CALCPIP = DEFPIP(.w_DEC)
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .w_GPIMPSPE = IIF(.w_GPADDSPE='S',IIF(.w_IMPSPE1<>0,.w_IMPSPE1,.w_GPIMPSPE),0)
        .w_CODCON = .w_GPCODCON
        .w_RIFDIS = .w_GPRIFDIS
        .w_CATPAG = IIF(.w_GPCATPAG='TT', '  ', .w_GPCATPAG)
          .DoRTCalc(56,56,.f.)
        .w_TIPPAG = IIF(.w_CATPAG='TT','  ',.w_CATPAG)
          .DoRTCalc(58,61,.f.)
        .w_DESPRE = LOOKTAB('COC_MAST', 'BADESCRI', 'BACODBAN', .w_GPBANPRE)
        .w_BANSEL = .w_GPBANSEL
        .w_CONASS = IIF(.w_TIPSBF='S', .w_CONASS, .w_BANSEL)
      endif
    endwith
    cp_BlankRecExtFlds(this,'GES_PIAN')
    this.DoRTCalc(65,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsso_api
     Local L_Object
     L_Object=This.GetCtrl('w_GPADDINT')
     L_Object.Value=1
     * --- Refresh dello Zoom
    if this.cFunction='Load'
       this.NotifyEvent('Legge')
    endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"GRINS","i_codazi,w_GPCOMPET,w_GPCODRAG")
      .op_codazi = .w_codazi
      .op_GPCOMPET = .w_GPCOMPET
      .op_GPCODRAG = .w_GPCODRAG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGPCODRAG_1_3.enabled = i_bVal
      .Page1.oPag.oGPCOMPET_1_4.enabled = i_bVal
      .Page1.oPag.oGPDATREG_1_6.enabled = i_bVal
      .Page1.oPag.oGP__TIPO_1_7.enabled = i_bVal
      .Page1.oPag.oGPCODVAL_1_10.enabled = i_bVal
      .Page1.oPag.oGPCAOVAL_1_11.enabled = i_bVal
      .Page1.oPag.oGPTIPEFF_1_12.enabled = i_bVal
      .Page1.oPag.oGPADDINT_1_13.enabled = i_bVal
      .Page1.oPag.oGPPERINT_1_14.enabled = i_bVal
      .Page1.oPag.oGPSAINTC_1_15.enabled = i_bVal
      .Page1.oPag.oGPSCAINI_1_16.enabled = i_bVal
      .Page1.oPag.oGPADDSOL_1_17.enabled = i_bVal
      .Page1.oPag.oGPSCAFIN_1_18.enabled = i_bVal
      .Page1.oPag.oGPADDSPE_1_19.enabled = i_bVal
      .Page1.oPag.oGPDATVAL_1_20.enabled = i_bVal
      .Page1.oPag.oGPDESSPE_1_21.enabled = i_bVal
      .Page2.oPag.oGPCODCON_2_1.enabled = i_bVal
      .Page2.oPag.oGPCATPAG_2_3.enabled = i_bVal
      .Page2.oPag.oGPBANSEL_2_4.enabled = i_bVal
      .Page2.oPag.oGPBANPRE_2_5.enabled = i_bVal
      .Page2.oPag.oGPRIFDIS_2_7.enabled = i_bVal
      .Page1.oPag.oGPIMPSPE_1_64.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = .Page1.oPag.oBtn_1_23.mCond()
      .Page1.oPag.oBtn_1_34.enabled = i_bVal
      .Page1.oPag.oBtn_1_54.enabled = i_bVal
      .Page1.oPag.oBtn_1_55.enabled = i_bVal
      .Page2.oPag.oBtn_2_6.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page2.oPag.oObj_2_13.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGPCODRAG_1_3.enabled = .f.
        .Page1.oPag.oGPCOMPET_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGPCODRAG_1_3.enabled = .t.
        .Page1.oPag.oGPCOMPET_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GES_PIAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODRAG,"GPCODRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCOMPET,"GPCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATREG,"GPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GP__TIPO,"GP__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODVAL,"GPCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCAOVAL,"GPCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPEFF,"GPTIPEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPADDINT,"GPADDINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPERINT,"GPPERINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSAINTC,"GPSAINTC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSCAINI,"GPSCAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPADDSOL,"GPADDSOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSCAFIN,"GPSCAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPADDSPE,"GPADDSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATVAL,"GPDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDESSPE,"GPDESSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPCON,"GPTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODCON,"GPCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCATPAG,"GPCATPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPBANSEL,"GPBANSEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPBANPRE,"GPBANPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPRIFDIS,"GPRIFDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPIMPSPE,"GPIMPSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
    i_lTable = "GES_PIAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GES_PIAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GES_PIAN_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"GRINS","i_codazi,w_GPCOMPET,w_GPCODRAG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GES_PIAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GES_PIAN')
        i_extval=cp_InsertValODBCExtFlds(this,'GES_PIAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GPCODRAG,GPCOMPET,GPDATREG,GP__TIPO,GPCODVAL"+;
                  ",GPCAOVAL,GPTIPEFF,GPADDINT,GPPERINT,GPSAINTC"+;
                  ",GPSCAINI,GPADDSOL,GPSCAFIN,GPADDSPE,GPDATVAL"+;
                  ",GPDESSPE,GPTIPCON,GPCODCON,GPCATPAG,GPBANSEL"+;
                  ",GPBANPRE,GPRIFDIS,GPIMPSPE,UTCC,UTCV"+;
                  ",UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GPCODRAG)+;
                  ","+cp_ToStrODBC(this.w_GPCOMPET)+;
                  ","+cp_ToStrODBC(this.w_GPDATREG)+;
                  ","+cp_ToStrODBC(this.w_GP__TIPO)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODVAL)+;
                  ","+cp_ToStrODBC(this.w_GPCAOVAL)+;
                  ","+cp_ToStrODBC(this.w_GPTIPEFF)+;
                  ","+cp_ToStrODBC(this.w_GPADDINT)+;
                  ","+cp_ToStrODBC(this.w_GPPERINT)+;
                  ","+cp_ToStrODBC(this.w_GPSAINTC)+;
                  ","+cp_ToStrODBC(this.w_GPSCAINI)+;
                  ","+cp_ToStrODBC(this.w_GPADDSOL)+;
                  ","+cp_ToStrODBC(this.w_GPSCAFIN)+;
                  ","+cp_ToStrODBC(this.w_GPADDSPE)+;
                  ","+cp_ToStrODBC(this.w_GPDATVAL)+;
                  ","+cp_ToStrODBC(this.w_GPDESSPE)+;
                  ","+cp_ToStrODBC(this.w_GPTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODCON)+;
                  ","+cp_ToStrODBC(this.w_GPCATPAG)+;
                  ","+cp_ToStrODBCNull(this.w_GPBANSEL)+;
                  ","+cp_ToStrODBCNull(this.w_GPBANPRE)+;
                  ","+cp_ToStrODBC(this.w_GPRIFDIS)+;
                  ","+cp_ToStrODBC(this.w_GPIMPSPE)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GES_PIAN')
        i_extval=cp_InsertValVFPExtFlds(this,'GES_PIAN')
        cp_CheckDeletedKey(i_cTable,0,'GPCODRAG',this.w_GPCODRAG,'GPCOMPET',this.w_GPCOMPET)
        INSERT INTO (i_cTable);
              (GPCODRAG,GPCOMPET,GPDATREG,GP__TIPO,GPCODVAL,GPCAOVAL,GPTIPEFF,GPADDINT,GPPERINT,GPSAINTC,GPSCAINI,GPADDSOL,GPSCAFIN,GPADDSPE,GPDATVAL,GPDESSPE,GPTIPCON,GPCODCON,GPCATPAG,GPBANSEL,GPBANPRE,GPRIFDIS,GPIMPSPE,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GPCODRAG;
                  ,this.w_GPCOMPET;
                  ,this.w_GPDATREG;
                  ,this.w_GP__TIPO;
                  ,this.w_GPCODVAL;
                  ,this.w_GPCAOVAL;
                  ,this.w_GPTIPEFF;
                  ,this.w_GPADDINT;
                  ,this.w_GPPERINT;
                  ,this.w_GPSAINTC;
                  ,this.w_GPSCAINI;
                  ,this.w_GPADDSOL;
                  ,this.w_GPSCAFIN;
                  ,this.w_GPADDSPE;
                  ,this.w_GPDATVAL;
                  ,this.w_GPDESSPE;
                  ,this.w_GPTIPCON;
                  ,this.w_GPCODCON;
                  ,this.w_GPCATPAG;
                  ,this.w_GPBANSEL;
                  ,this.w_GPBANPRE;
                  ,this.w_GPRIFDIS;
                  ,this.w_GPIMPSPE;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GES_PIAN_IDX,i_nConn)
      *
      * update GES_PIAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GES_PIAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GPDATREG="+cp_ToStrODBC(this.w_GPDATREG)+;
             ",GP__TIPO="+cp_ToStrODBC(this.w_GP__TIPO)+;
             ",GPCODVAL="+cp_ToStrODBCNull(this.w_GPCODVAL)+;
             ",GPCAOVAL="+cp_ToStrODBC(this.w_GPCAOVAL)+;
             ",GPTIPEFF="+cp_ToStrODBC(this.w_GPTIPEFF)+;
             ",GPADDINT="+cp_ToStrODBC(this.w_GPADDINT)+;
             ",GPPERINT="+cp_ToStrODBC(this.w_GPPERINT)+;
             ",GPSAINTC="+cp_ToStrODBC(this.w_GPSAINTC)+;
             ",GPSCAINI="+cp_ToStrODBC(this.w_GPSCAINI)+;
             ",GPADDSOL="+cp_ToStrODBC(this.w_GPADDSOL)+;
             ",GPSCAFIN="+cp_ToStrODBC(this.w_GPSCAFIN)+;
             ",GPADDSPE="+cp_ToStrODBC(this.w_GPADDSPE)+;
             ",GPDATVAL="+cp_ToStrODBC(this.w_GPDATVAL)+;
             ",GPDESSPE="+cp_ToStrODBC(this.w_GPDESSPE)+;
             ",GPTIPCON="+cp_ToStrODBC(this.w_GPTIPCON)+;
             ",GPCODCON="+cp_ToStrODBCNull(this.w_GPCODCON)+;
             ",GPCATPAG="+cp_ToStrODBC(this.w_GPCATPAG)+;
             ",GPBANSEL="+cp_ToStrODBCNull(this.w_GPBANSEL)+;
             ",GPBANPRE="+cp_ToStrODBCNull(this.w_GPBANPRE)+;
             ",GPRIFDIS="+cp_ToStrODBC(this.w_GPRIFDIS)+;
             ",GPIMPSPE="+cp_ToStrODBC(this.w_GPIMPSPE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GES_PIAN')
        i_cWhere = cp_PKFox(i_cTable  ,'GPCODRAG',this.w_GPCODRAG  ,'GPCOMPET',this.w_GPCOMPET  )
        UPDATE (i_cTable) SET;
              GPDATREG=this.w_GPDATREG;
             ,GP__TIPO=this.w_GP__TIPO;
             ,GPCODVAL=this.w_GPCODVAL;
             ,GPCAOVAL=this.w_GPCAOVAL;
             ,GPTIPEFF=this.w_GPTIPEFF;
             ,GPADDINT=this.w_GPADDINT;
             ,GPPERINT=this.w_GPPERINT;
             ,GPSAINTC=this.w_GPSAINTC;
             ,GPSCAINI=this.w_GPSCAINI;
             ,GPADDSOL=this.w_GPADDSOL;
             ,GPSCAFIN=this.w_GPSCAFIN;
             ,GPADDSPE=this.w_GPADDSPE;
             ,GPDATVAL=this.w_GPDATVAL;
             ,GPDESSPE=this.w_GPDESSPE;
             ,GPTIPCON=this.w_GPTIPCON;
             ,GPCODCON=this.w_GPCODCON;
             ,GPCATPAG=this.w_GPCATPAG;
             ,GPBANSEL=this.w_GPBANSEL;
             ,GPBANPRE=this.w_GPBANPRE;
             ,GPRIFDIS=this.w_GPRIFDIS;
             ,GPIMPSPE=this.w_GPIMPSPE;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GES_PIAN_IDX,i_nConn)
      *
      * delete GES_PIAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GPCODRAG',this.w_GPCODRAG  ,'GPCOMPET',this.w_GPCOMPET  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
    if i_bUpd
      with this
            .w_READPAR = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_GP__TIPO<>.w_GP__TIPO
            .w_CONTENZ = .w_GP__TIPO
        endif
            .w_OBTEST = .w_GPDATREG
        .DoRTCalc(9,9,.t.)
        if .o_GPCODVAL<>.w_GPCODVAL
            .w_GPCAOVAL =  GETCAM(.w_GPCODVAL, .w_DATVAL, 7)
        endif
        if .o_GP__TIPO<>.w_GP__TIPO
            .w_GPTIPEFF = 'E'
        endif
        .DoRTCalc(12,12,.t.)
        if .o_GPADDINT<>.w_GPADDINT
            .w_GPPERINT = IIF(.w_GPADDINT='S', .w_GPPERINT, 0)
        endif
        .DoRTCalc(14,14,.t.)
        if .o_GPDATREG<>.w_GPDATREG
            .w_GPSCAINI = .w_GPDATREG-60
        endif
        .DoRTCalc(16,16,.t.)
        if .o_GPDATREG<>.w_GPDATREG
            .w_GPSCAFIN = .w_GPDATREG
        endif
        if .o_GP__TIPO<>.w_GP__TIPO
            .w_GPADDSPE = IIF(.w_GP__TIPO='I','S',' ')
        endif
        .DoRTCalc(19,20,.t.)
            .w_GPTIPCON = 'C'
        .DoRTCalc(22,22,.t.)
            .w_TIPCON = .w_GPTIPCON
        .DoRTCalc(24,25,.t.)
            .w_SCAINI = .w_GPSCAINI
            .w_CODVAL = .w_GPCODVAL
            .w_SCAFIN = .w_GPSCAFIN
            .w_CAOVAL = .w_GPCAOVAL
            .w_CAMBIO = .w_GPCAOVAL
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .DoRTCalc(31,31,.t.)
            .w_TIPO = .w_GP__TIPO
        .DoRTCalc(33,33,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(35,35,.t.)
            .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1),.w_VALPAR1,g_PERVAL)
          .link_1_47('Full')
        .DoRTCalc(37,40,.t.)
        if .o_GPDATREG<>.w_GPDATREG
            .w_DATREG = .w_GPDATREG
        endif
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .DoRTCalc(42,50,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPIP = DEFPIP(.w_DEC)
        endif
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        if .o_GPADDSPE<>.w_GPADDSPE.or. .o_GP__TIPO<>.w_GP__TIPO
            .w_GPIMPSPE = IIF(.w_GPADDSPE='S',IIF(.w_IMPSPE1<>0,.w_IMPSPE1,.w_GPIMPSPE),0)
        endif
            .w_CODCON = .w_GPCODCON
            .w_RIFDIS = .w_GPRIFDIS
        if .o_GPCATPAG<>.w_GPCATPAG
            .w_CATPAG = IIF(.w_GPCATPAG='TT', '  ', .w_GPCATPAG)
        endif
        .DoRTCalc(56,56,.t.)
        if .o_GPCATPAG<>.w_GPCATPAG
            .w_TIPPAG = IIF(.w_CATPAG='TT','  ',.w_CATPAG)
        endif
        .DoRTCalc(58,61,.t.)
            .w_DESPRE = LOOKTAB('COC_MAST', 'BADESCRI', 'BACODBAN', .w_GPBANPRE)
            .w_BANSEL = .w_GPBANSEL
            .w_CONASS = IIF(.w_TIPSBF='S', .w_CONASS, .w_BANSEL)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_GPCOMPET<>.w_GPCOMPET
           cp_AskTableProg(this,i_nConn,"GRINS","i_codazi,w_GPCOMPET,w_GPCODRAG")
          .op_GPCODRAG = .w_GPCODRAG
        endif
        .op_codazi = .w_codazi
        .op_GPCOMPET = .w_GPCOMPET
      endwith
      this.DoRTCalc(65,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGP__TIPO_1_7.enabled = this.oPgFrm.Page1.oPag.oGP__TIPO_1_7.mCond()
    this.oPgFrm.Page1.oPag.oGPCAOVAL_1_11.enabled = this.oPgFrm.Page1.oPag.oGPCAOVAL_1_11.mCond()
    this.oPgFrm.Page1.oPag.oGPADDINT_1_13.enabled = this.oPgFrm.Page1.oPag.oGPADDINT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oGPPERINT_1_14.enabled = this.oPgFrm.Page1.oPag.oGPPERINT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oGPSAINTC_1_15.enabled = this.oPgFrm.Page1.oPag.oGPSAINTC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oGPADDSPE_1_19.enabled = this.oPgFrm.Page1.oPag.oGPADDSPE_1_19.mCond()
    this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.enabled = this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.mCond()
    this.oPgFrm.Page1.oPag.oGPIMPSPE_1_64.enabled = this.oPgFrm.Page1.oPag.oGPIMPSPE_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGPTIPEFF_1_12.visible=!this.oPgFrm.Page1.oPag.oGPTIPEFF_1_12.mHide()
    this.oPgFrm.Page1.oPag.oGPDATVAL_1_20.visible=!this.oPgFrm.Page1.oPag.oGPDATVAL_1_20.mHide()
    this.oPgFrm.Page1.oPag.oGPDESSPE_1_21.visible=!this.oPgFrm.Page1.oPag.oGPDESSPE_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_54.visible=!this.oPgFrm.Page1.oPag.oBtn_1_54.mHide()
    this.oPgFrm.Page2.oPag.oGPBANSEL_2_4.visible=!this.oPgFrm.Page2.oPag.oGPBANSEL_2_4.mHide()
    this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.visible=!this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_6.visible=!this.oPgFrm.Page2.oPag.oBtn_2_6.mHide()
    this.oPgFrm.Page2.oPag.oGPRIFDIS_2_7.visible=!this.oPgFrm.Page2.oPag.oGPRIFDIS_2_7.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oDESBAN_2_23.visible=!this.oPgFrm.Page2.oPag.oDESBAN_2_23.mHide()
    this.oPgFrm.Page2.oPag.oDESPRE_2_24.visible=!this.oPgFrm.Page2.oPag.oDESPRE_2_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_lTable = "PAR_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2], .t., this.PAR_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCVALIMP,PCSPEINS,PCPAGMOR,PCDURMOR,PCCONTRO";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_READPAR)
            select PCCODAZI,PCVALIMP,PCSPEINS,PCPAGMOR,PCDURMOR,PCCONTRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PCCODAZI,space(5))
      this.w_VALPAR1 = NVL(_Link_.PCVALIMP,space(3))
      this.w_IMPSPE1 = NVL(_Link_.PCSPEINS,0)
      this.w_PAGMOR = NVL(_Link_.PCPAGMOR,space(5))
      this.w_DURMOR = NVL(_Link_.PCDURMOR,0)
      this.w_CONTRO = NVL(_Link_.PCCONTRO,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(5)
      endif
      this.w_VALPAR1 = space(3)
      this.w_IMPSPE1 = 0
      this.w_PAGMOR = space(5)
      this.w_DURMOR = 0
      this.w_CONTRO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_GPCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_GPCODVAL))
          select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oGPCODVAL_1_10'),i_cWhere,'GSAR_AVL',"Codici valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_GPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_GPCODVAL)
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_CAMBIO1 = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_CAMBIO1 = 0
      this.w_DECTOT = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GPCODVAL = space(3)
        this.w_SIMVAL = space(5)
        this.w_CAMBIO1 = 0
        this.w_DECTOT = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.VACODVAL as VACODVAL110"+ ",link_1_10.VASIMVAL as VASIMVAL110"+ ",link_1_10.VACAOVAL as VACAOVAL110"+ ",link_1_10.VADECTOT as VADECTOT110"+ ",link_1_10.VADTOBSO as VADTOBSO110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on GES_PIAN.GPCODVAL=link_1_10.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and GES_PIAN.GPCODVAL=link_1_10.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALPAR
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPAR)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPAR = NVL(_Link_.VACODVAL,space(3))
      this.w_DEC = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALPAR = space(3)
      endif
      this.w_DEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODCON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_GPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GPTIPCON;
                     ,'ANCODICE',trim(this.w_GPCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_GPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_GPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_GPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oGPCODCON_2_1'),i_cWhere,'GSAR_BZC',"Anagrafica clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GPTIPCON;
                       ,'ANCODICE',this.w_GPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(10))
      this.w_VALCLF = NVL(_Link_.ANCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODCON = space(15)
      endif
      this.w_DESCON = space(10)
      this.w_VALCLF = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPBANSEL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPBANSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_GPBANSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONASS,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_GPBANSEL))
          select BACODBAN,BADESCRI,BACONASS,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPBANSEL)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPBANSEL) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oGPBANSEL_2_4'),i_cWhere,'GSTE_ACB',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONASS,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACONASS,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPBANSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONASS,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_GPBANSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_GPBANSEL)
            select BACODBAN,BADESCRI,BACONASS,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPBANSEL = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(35))
      this.w_GPBANPRE = NVL(_Link_.BACONASS,space(15))
      this.w_TIPSBF = NVL(_Link_.BACONSBF,space(1))
      this.w_CONASS = NVL(_Link_.BACONASS,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GPBANSEL = space(15)
      endif
      this.w_DESBAN = space(35)
      this.w_GPBANPRE = space(15)
      this.w_TIPSBF = space(1)
      this.w_CONASS = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPBANSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.BACODBAN as BACODBAN204"+ ",link_2_4.BADESCRI as BADESCRI204"+ ",link_2_4.BACONASS as BACONASS204"+ ",link_2_4.BACONSBF as BACONSBF204"+ ",link_2_4.BACONASS as BACONASS204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on GES_PIAN.GPBANSEL=link_2_4.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and GES_PIAN.GPBANSEL=link_2_4.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPBANPRE
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPBANPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_GPBANPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_GPBANPRE))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPBANPRE)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPBANPRE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oGPBANPRE_2_5'),i_cWhere,'GSTE_ACB',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPBANPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_GPBANPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_GPBANPRE)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPBANPRE = NVL(_Link_.BACODBAN,space(15))
      this.w_DESPRE = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GPBANPRE = space(15)
      endif
      this.w_DESPRE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPBANPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.BACODBAN as BACODBAN205"+ ",link_2_5.BADESCRI as BADESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on GES_PIAN.GPBANPRE=link_2_5.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and GES_PIAN.GPBANPRE=link_2_5.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGPCODRAG_1_3.value==this.w_GPCODRAG)
      this.oPgFrm.Page1.oPag.oGPCODRAG_1_3.value=this.w_GPCODRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCOMPET_1_4.value==this.w_GPCOMPET)
      this.oPgFrm.Page1.oPag.oGPCOMPET_1_4.value=this.w_GPCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATREG_1_6.value==this.w_GPDATREG)
      this.oPgFrm.Page1.oPag.oGPDATREG_1_6.value=this.w_GPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oGP__TIPO_1_7.RadioValue()==this.w_GP__TIPO)
      this.oPgFrm.Page1.oPag.oGP__TIPO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODVAL_1_10.value==this.w_GPCODVAL)
      this.oPgFrm.Page1.oPag.oGPCODVAL_1_10.value=this.w_GPCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCAOVAL_1_11.value==this.w_GPCAOVAL)
      this.oPgFrm.Page1.oPag.oGPCAOVAL_1_11.value=this.w_GPCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPEFF_1_12.RadioValue()==this.w_GPTIPEFF)
      this.oPgFrm.Page1.oPag.oGPTIPEFF_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPADDINT_1_13.RadioValue()==this.w_GPADDINT)
      this.oPgFrm.Page1.oPag.oGPADDINT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPERINT_1_14.value==this.w_GPPERINT)
      this.oPgFrm.Page1.oPag.oGPPERINT_1_14.value=this.w_GPPERINT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSAINTC_1_15.RadioValue()==this.w_GPSAINTC)
      this.oPgFrm.Page1.oPag.oGPSAINTC_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSCAINI_1_16.value==this.w_GPSCAINI)
      this.oPgFrm.Page1.oPag.oGPSCAINI_1_16.value=this.w_GPSCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPADDSOL_1_17.RadioValue()==this.w_GPADDSOL)
      this.oPgFrm.Page1.oPag.oGPADDSOL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSCAFIN_1_18.value==this.w_GPSCAFIN)
      this.oPgFrm.Page1.oPag.oGPSCAFIN_1_18.value=this.w_GPSCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPADDSPE_1_19.RadioValue()==this.w_GPADDSPE)
      this.oPgFrm.Page1.oPag.oGPADDSPE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATVAL_1_20.value==this.w_GPDATVAL)
      this.oPgFrm.Page1.oPag.oGPDATVAL_1_20.value=this.w_GPDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESSPE_1_21.RadioValue()==this.w_GPDESSPE)
      this.oPgFrm.Page1.oPag.oGPDESSPE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_26.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_26.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oGPCODCON_2_1.value==this.w_GPCODCON)
      this.oPgFrm.Page2.oPag.oGPCODCON_2_1.value=this.w_GPCODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_2.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_2.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oGPCATPAG_2_3.RadioValue()==this.w_GPCATPAG)
      this.oPgFrm.Page2.oPag.oGPCATPAG_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGPBANSEL_2_4.value==this.w_GPBANSEL)
      this.oPgFrm.Page2.oPag.oGPBANSEL_2_4.value=this.w_GPBANSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.value==this.w_GPBANPRE)
      this.oPgFrm.Page2.oPag.oGPBANPRE_2_5.value=this.w_GPBANPRE
    endif
    if not(this.oPgFrm.Page2.oPag.oGPRIFDIS_2_7.value==this.w_GPRIFDIS)
      this.oPgFrm.Page2.oPag.oGPRIFDIS_2_7.value=this.w_GPRIFDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oGPIMPSPE_1_64.value==this.w_GPIMPSPE)
      this.oPgFrm.Page1.oPag.oGPIMPSPE_1_64.value=this.w_GPIMPSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_23.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_23.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRE_2_24.value==this.w_DESPRE)
      this.oPgFrm.Page2.oPag.oDESPRE_2_24.value=this.w_DESPRE
    endif
    cp_SetControlsValueExtFlds(this,'GES_PIAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GPCODRAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCODRAG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_GPCODRAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPCOMPET))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCOMPET_1_4.SetFocus()
            i_bnoObbl = !empty(.w_GPCOMPET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_GPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPCODVAL)) or not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCODVAL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_GPCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPCAOVAL))  and (.w_CAMBIO1=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCAOVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_GPCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPPERINT)) or not(.w_GPADDINT<>'S' OR .w_GPPERINT<>0))  and (.w_GPADDINT='S' AND (.w_FASE=1 Or .cFunction='Load' ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPPERINT_1_14.SetFocus()
            i_bnoObbl = !empty(.w_GPPERINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GPSCAINI<=.w_GPSCAFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPSCAFIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale inferiore alla data iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsso_api
      if .w_FASE=0 AND .cFunction='Load'
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg = Ah_MsgFormat("Per registrare la prima volta premere il bottone <Conferma>")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GPCODRAG = this.w_GPCODRAG
    this.o_GPDATREG = this.w_GPDATREG
    this.o_GP__TIPO = this.w_GP__TIPO
    this.o_GPCODVAL = this.w_GPCODVAL
    this.o_GPADDINT = this.w_GPADDINT
    this.o_GPADDSPE = this.w_GPADDSPE
    this.o_DECTOT = this.w_DECTOT
    this.o_GPCATPAG = this.w_GPCATPAG
    return

  func CanAdd()
    local i_res
    i_res=not empty(this.w_VALPAR1) AND not empty(this.w_PAGMOR) AND  not empty(this.w_CONTRO)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione compilare parametri contenzioso"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_apiPag1 as StdContainer
  Width  = 695
  height = 517
  stdWidth  = 695
  stdheight = 517
  resizeYpos=331
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPCODRAG_1_3 as StdField with uid="EMQSAACPSZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GPCODRAG", cQueryName = "GPCODRAG",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice raggruppamento inserito automaticamente",;
    HelpContextID = 218754989,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=89, Left=94, Top=18, InputMask=replicate('X',10)

  add object oGPCOMPET_1_4 as StdField with uid="WURLRTRTUC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GPCOMPET", cQueryName = "GPCODRAG,GPCOMPET",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 73797702,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=36, Left=202, Top=18, InputMask=replicate('X',4)

  add object oGPDATREG_1_6 as StdField with uid="XXJUJGLRND",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GPDATREG", cQueryName = "GPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 33816659,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=284, Top=18


  add object oGP__TIPO_1_7 as StdCombo with uid="LPQGHLBSKN",rtseq=6,rtrep=.f.,left=536,top=19,width=152,height=21;
    , ToolTipText = "Tipologia solleciti I= insoluti M=mancati pagamenti";
    , HelpContextID = 85700533;
    , cFormVar="w_GP__TIPO",RowSource=""+"Insoluti,"+"Mancati pagamenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGP__TIPO_1_7.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oGP__TIPO_1_7.GetRadio()
    this.Parent.oContained.w_GP__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oGP__TIPO_1_7.SetRadio()
    this.Parent.oContained.w_GP__TIPO=trim(this.Parent.oContained.w_GP__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_GP__TIPO=='I',1,;
      iif(this.Parent.oContained.w_GP__TIPO=='M',2,;
      0))
  endfunc

  func oGP__TIPO_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oGPCODVAL_1_10 as StdField with uid="WNYSJGBVIR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_GPCODVAL", cQueryName = "GPCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione",;
    HelpContextID = 17428402,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=94, Top=46, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_GPCODVAL"

  func oGPCODVAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODVAL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODVAL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oGPCODVAL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Codici valute",'',this.parent.oContained
  endproc
  proc oGPCODVAL_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_GPCODVAL
     i_obj.ecpSave()
  endproc

  add object oGPCAOVAL_1_11 as StdField with uid="IWSVUBWRSN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GPCAOVAL", cQueryName = "GPCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "cambio di riferimento per la generazione dei movimenti di insoluto",;
    HelpContextID = 28045234,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=284, Top=46

  func oGPCAOVAL_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMBIO1=0)
    endwith
   endif
  endfunc


  add object oGPTIPEFF_1_12 as StdCombo with uid="JQLLCNOVBY",rtseq=11,rtrep=.f.,left=536,top=46,width=152,height=21;
    , ToolTipText = "Tipologia effetti E= partite chiuse provenienti da distinte A= partite chiuse non da distinte";
    , HelpContextID = 255524948;
    , cFormVar="w_GPTIPEFF",RowSource=""+"Solo effetti,"+"Nessun effetto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPTIPEFF_1_12.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oGPTIPEFF_1_12.GetRadio()
    this.Parent.oContained.w_GPTIPEFF = this.RadioValue()
    return .t.
  endfunc

  func oGPTIPEFF_1_12.SetRadio()
    this.Parent.oContained.w_GPTIPEFF=trim(this.Parent.oContained.w_GPTIPEFF)
    this.value = ;
      iif(this.Parent.oContained.w_GPTIPEFF=='E',1,;
      iif(this.Parent.oContained.w_GPTIPEFF=='A',2,;
      0))
  endfunc

  func oGPTIPEFF_1_12.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc


  add object oGPADDINT_1_13 as StdCombo with uid="JFCCFBSRWL",rtseq=12,rtrep=.f.,left=95,top=80,width=163,height=21;
    , ToolTipText = "Determina l'origine del saggio di interesse da applicare: tabella saggio interessi di mora / saggio forzato";
    , HelpContextID = 67030970;
    , cFormVar="w_GPADDINT",RowSource=""+"Non applicati,"+"Saggio di mora,"+"Saggio forzato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPADDINT_1_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oGPADDINT_1_13.GetRadio()
    this.Parent.oContained.w_GPADDINT = this.RadioValue()
    return .t.
  endfunc

  func oGPADDINT_1_13.SetRadio()
    this.Parent.oContained.w_GPADDINT=trim(this.Parent.oContained.w_GPADDINT)
    this.value = ;
      iif(this.Parent.oContained.w_GPADDINT=='N',1,;
      iif(this.Parent.oContained.w_GPADDINT=='M',2,;
      iif(this.Parent.oContained.w_GPADDINT=='S',3,;
      0)))
  endfunc

  func oGPADDINT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FASE=1 Or .cFunction='Load')
    endwith
   endif
  endfunc

  add object oGPPERINT_1_14 as StdField with uid="YNIWUNAJTT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GPPERINT", cQueryName = "GPPERINT",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale interessi",;
    HelpContextID = 81838010,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=284, Top=79, cSayPict='"999.99"', cGetPict='"999.99"'

  func oGPPERINT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPADDINT='S' AND (.w_FASE=1 Or .cFunction='Load' ))
    endwith
   endif
  endfunc

  func oGPPERINT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPADDINT<>'S' OR .w_GPPERINT<>0)
    endwith
    return bRes
  endfunc

  add object oGPSAINTC_1_15 as StdCheck with uid="FBVXQFODBT",rtseq=14,rtrep=.f.,left=371, top=79, caption="Saggio da anagrafica cliente",;
    ToolTipText = "Check editabile se il tasso da applicare � forzato. Se attivo nella stampa solleciti viene applicato il tasso di mora definito in anagrafica cliente in sostituzione del saggio forzato",;
    HelpContextID = 112398423,;
    cFormVar="w_GPSAINTC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPSAINTC_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPSAINTC_1_15.GetRadio()
    this.Parent.oContained.w_GPSAINTC = this.RadioValue()
    return .t.
  endfunc

  func oGPSAINTC_1_15.SetRadio()
    this.Parent.oContained.w_GPSAINTC=trim(this.Parent.oContained.w_GPSAINTC)
    this.value = ;
      iif(this.Parent.oContained.w_GPSAINTC=='S',1,;
      0)
  endfunc

  func oGPSAINTC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPADDINT='S' And (.w_FASE=1 Or .cFunction='Load' ))
    endwith
   endif
  endfunc

  add object oGPSCAINI_1_16 as StdField with uid="TUDVXUEDHI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GPSCAINI", cQueryName = "GPSCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza di inizio selezione",;
    HelpContextID = 63893423,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=612, Top=116

  add object oGPADDSOL_1_17 as StdCheck with uid="PPVUJHGYNE",rtseq=16,rtrep=.f.,left=95, top=116, caption="Addebito spese solleciti",;
    ToolTipText = "Se attivo abilita addebito spese per l'invio dei solleciti",;
    HelpContextID = 234803122,;
    cFormVar="w_GPADDSOL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPADDSOL_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGPADDSOL_1_17.GetRadio()
    this.Parent.oContained.w_GPADDSOL = this.RadioValue()
    return .t.
  endfunc

  func oGPADDSOL_1_17.SetRadio()
    this.Parent.oContained.w_GPADDSOL=trim(this.Parent.oContained.w_GPADDSOL)
    this.value = ;
      iif(this.Parent.oContained.w_GPADDSOL=='S',1,;
      0)
  endfunc

  add object oGPSCAFIN_1_18 as StdField with uid="PFGDMAVCDU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GPSCAFIN", cQueryName = "GPSCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale inferiore alla data iniziale",;
    ToolTipText = "Data scadenza di fine selezione",;
    HelpContextID = 254873676,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=612, Top=155

  func oGPSCAFIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPSCAINI<=.w_GPSCAFIN)
    endwith
    return bRes
  endfunc

  add object oGPADDSPE_1_19 as StdCheck with uid="DRKNZTFWIG",rtseq=18,rtrep=.f.,left=95, top=155, caption="Addebito spese bancarie",;
    ToolTipText = "Se attivo abilita addebito spese bancarie per ogni contenzioso",;
    HelpContextID = 234803115,;
    cFormVar="w_GPADDSPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPADDSPE_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGPADDSPE_1_19.GetRadio()
    this.Parent.oContained.w_GPADDSPE = this.RadioValue()
    return .t.
  endfunc

  func oGPADDSPE_1_19.SetRadio()
    this.Parent.oContained.w_GPADDSPE=trim(this.Parent.oContained.w_GPADDSPE)
    this.value = ;
      iif(this.Parent.oContained.w_GPADDSPE=='S',1,;
      0)
  endfunc

  func oGPADDSPE_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GP__TIPO='I')
    endwith
   endif
  endfunc

  add object oGPDATVAL_1_20 as StdField with uid="FHQTYKOHQX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GPDATVAL", cQueryName = "GPDATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data valuta per la contabilizzazione movimenti di C\C",;
    HelpContextID = 33292210,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=612, Top=192

  func oGPDATVAL_1_20.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S' OR .w_GP__TIPO='M')
    endwith
  endfunc


  add object oGPDESSPE_1_21 as StdCombo with uid="ISAQCPZDKX",rtseq=20,rtrep=.f.,left=95,top=192,width=163,height=21;
    , ToolTipText = "Se C= addebito spese direttamente sul cliente se S= sul relativo costo";
    , HelpContextID = 250609579;
    , cFormVar="w_GPDESSPE",RowSource=""+"Assegna spese al cliente,"+"Assegna spese al costo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPDESSPE_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oGPDESSPE_1_21.GetRadio()
    this.Parent.oContained.w_GPDESSPE = this.RadioValue()
    return .t.
  endfunc

  func oGPDESSPE_1_21.SetRadio()
    this.Parent.oContained.w_GPDESSPE=trim(this.Parent.oContained.w_GPDESSPE)
    this.value = ;
      iif(this.Parent.oContained.w_GPDESSPE=='S',1,;
      iif(this.Parent.oContained.w_GPDESSPE=='C',2,;
      0))
  endfunc

  func oGPDESSPE_1_21.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M' OR .w_GPADDSPE<>'S')
    endwith
  endfunc


  add object oBtn_1_23 as StdButton with uid="TBVXGJUURX",left=644, top=225, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Lancia stampa dettagliata del piano";
    , HelpContextID = 250213414;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSSO_BPD(this.Parent.oContained,"STAM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_GPCODRAG))
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load')
     endwith
    endif
  endfunc

  add object oSIMVAL_1_26 as StdField with uid="DSCCQRSBUM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 115444006,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=148, Top=46, InputMask=replicate('X',5)


  add object oBtn_1_34 as StdButton with uid="GJBOCVKTWI",left=539, top=470, width=48,height=45,;
    CpPicture="BMP\ABBINA.BMP", caption="", nPag=1;
    , ToolTipText = "Digitare per abbinare insoluti al piano";
    , HelpContextID = 247853318;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        do gsso_kpi with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction = 'Edit' AND NOT EMPTY(.w_GPCODRAG) AND .w_FASE=1)
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Edit' OR .w_FASE<>1)
     endwith
    endif
  endfunc


  add object oObj_1_39 as cp_runprogram with uid="JPJOPVARNV",left=800, top=269, width=200,height=25,;
    caption='GSSO_BPD(DELE)',;
   bGlobalFont=.t.,;
    prg="Gsso_bpd('DELE')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 207325482


  add object oBtn_1_54 as StdButton with uid="RVSXCPRHTD",left=591, top=470, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare i dati inseriti";
    , HelpContextID = 255049351;
    , tabstop=.f., Caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSSO_BCP(this.Parent.oContained,.w_GPCODRAG, .w_GPCOMPET)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Load' AND (.w_GPADDINT<>'S' OR .w_GPPERINT<>0))
      endwith
    endif
  endfunc

  func oBtn_1_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Load')
     endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="EERZOILSBK",left=644, top=470, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 115741254;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object CalcZoom as cp_szoombox with uid="AGEVNPTYPM",left=1, top=277, width=691,height=183,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSSO_AIN",cTable="CON_TENZ",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSSO_MCO",;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 250449434

  add object oGPIMPSPE_1_64 as StdField with uid="JUPISSFQJE",rtseq=52,rtrep=.f.,;
    cFormVar = "w_GPIMPSPE", cQueryName = "GPIMPSPE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese bancarie espresso nella valuta dei parametri",;
    HelpContextID = 248008619,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=286, Top=155, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oGPIMPSPE_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPADDSPE='S' AND .w_GP__TIPO='I')
    endwith
   endif
  endfunc

  add object oStr_1_5 as StdString with uid="QANLFSPHPQ",Visible=.t., Left=189, Top=18,;
    Alignment=0, Width=18, Height=21,;
    Caption="/"  ;
    , FontName = "Arial", FontSize = 11, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="LVEKFFIWLD",Visible=.t., Left=252, Top=18,;
    Alignment=1, Width=30, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ACQRGLEFSS",Visible=.t., Left=25, Top=47,;
    Alignment=1, Width=67, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="EFPTDRMSAC",Visible=.t., Left=491, Top=118,;
    Alignment=1, Width=118, Height=18,;
    Caption="Da data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="DESUMZXRAE",Visible=.t., Left=491, Top=156,;
    Alignment=1, Width=118, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="SWBQBGDJOA",Visible=.t., Left=1, Top=18,;
    Alignment=1, Width=91, Height=18,;
    Caption="Codice piano:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="JDVEIHZSUT",Visible=.t., Left=208, Top=47,;
    Alignment=1, Width=74, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="DRXNUDKNRG",Visible=.t., Left=264, Top=84,;
    Alignment=0, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="TJKFVFPCPE",Visible=.t., Left=7, Top=250,;
    Alignment=0, Width=150, Height=18,;
    Caption="Elenco insoluti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="PIYGSIUIFG",Visible=.t., Left=469, Top=18,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="KYCXRAUJVT",Visible=.t., Left=508, Top=194,;
    Alignment=1, Width=101, Height=18,;
    Caption="Data valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S'  OR .w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="JJMKUBAYKI",Visible=.t., Left=469, Top=46,;
    Alignment=1, Width=63, Height=18,;
    Caption="Effetti:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="OKJNUCOANV",Visible=.t., Left=1, Top=193,;
    Alignment=1, Width=91, Height=18,;
    Caption="Destin. spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M' OR .w_GPADDSPE<>'S')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="LIYNVUZKGX",Visible=.t., Left=25, Top=81,;
    Alignment=1, Width=67, Height=18,;
    Caption="Interessi:"  ;
  , bGlobalFont=.t.

  add object oBox_1_59 as StdBox with uid="UOTSCPZXIN",left=3, top=273, width=686,height=2
enddefine
define class tgsso_apiPag2 as StdContainer
  Width  = 695
  height = 517
  stdWidth  = 695
  stdheight = 517
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPCODCON_2_1 as StdField with uid="LFYRUKPNJD",rtseq=43,rtrep=.f.,;
    cFormVar = "w_GPCODCON", cQueryName = "GPCODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente di selezione (vuoto=no selezione)",;
    HelpContextID = 235532212,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=121, Top=25, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GPCODCON"

  func oGPCODCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODCON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODCON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oGPCODCON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti",'',this.parent.oContained
  endproc
  proc oGPCODCON_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_GPCODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_2 as StdField with uid="OFYHQVRQDN",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente",;
    HelpContextID = 162456630,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=259, Top=25, InputMask=replicate('X',40)


  add object oGPCATPAG_2_3 as StdCombo with uid="WINCMPJCPC",rtseq=45,rtrep=.f.,left=121,top=57,width=112,height=21;
    , HelpContextID = 201060269;
    , cFormVar="w_GPCATPAG",RowSource=""+"Ric.bancarie/RiBa,"+"Rimesse dirette,"+"R.I.D.,"+"Anticipazioni/M.AV.,"+"Bonifici,"+"Cambiali / tratte,"+"Compensazione,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oGPCATPAG_2_3.RadioValue()
    return(iif(this.value =1,'RB',;
    iif(this.value =2,'RD',;
    iif(this.value =3,'RI',;
    iif(this.value =4,'MA',;
    iif(this.value =5,'BO',;
    iif(this.value =6,'CA',;
    iif(this.value =7,'CC',;
    iif(this.value =8,'TT',;
    space(2))))))))))
  endfunc
  func oGPCATPAG_2_3.GetRadio()
    this.Parent.oContained.w_GPCATPAG = this.RadioValue()
    return .t.
  endfunc

  func oGPCATPAG_2_3.SetRadio()
    this.Parent.oContained.w_GPCATPAG=trim(this.Parent.oContained.w_GPCATPAG)
    this.value = ;
      iif(this.Parent.oContained.w_GPCATPAG=='RB',1,;
      iif(this.Parent.oContained.w_GPCATPAG=='RD',2,;
      iif(this.Parent.oContained.w_GPCATPAG=='RI',3,;
      iif(this.Parent.oContained.w_GPCATPAG=='MA',4,;
      iif(this.Parent.oContained.w_GPCATPAG=='BO',5,;
      iif(this.Parent.oContained.w_GPCATPAG=='CA',6,;
      iif(this.Parent.oContained.w_GPCATPAG=='CC',7,;
      iif(this.Parent.oContained.w_GPCATPAG=='TT',8,;
      0))))))))
  endfunc

  add object oGPBANSEL_2_4 as StdField with uid="YEEAPDHKDH",rtseq=46,rtrep=.f.,;
    cFormVar = "w_GPBANSEL", cQueryName = "GPBANSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice banca di presentazione di selezione (vuoto=no selezione)",;
    HelpContextID = 23339086,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=121, Top=88, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_GPBANSEL"

  func oGPBANSEL_2_4.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  func oGPBANSEL_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPBANSEL_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPBANSEL_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oGPBANSEL_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'',this.parent.oContained
  endproc
  proc oGPBANSEL_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_GPBANSEL
     i_obj.ecpSave()
  endproc

  add object oGPBANPRE_2_5 as StdField with uid="GHVKJKPOTQ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_GPBANPRE", cQueryName = "GPBANPRE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Banca per insoluti",;
    HelpContextID = 194764715,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=121, Top=122, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_GPBANPRE"

  func oGPBANPRE_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GP__TIPO='I' AND NOT EMPTY(.w_GPBANSEL))
    endwith
   endif
  endfunc

  func oGPBANPRE_2_5.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  func oGPBANPRE_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPBANPRE_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPBANPRE_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oGPBANPRE_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'',this.parent.oContained
  endproc
  proc oGPBANPRE_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_GPBANPRE
     i_obj.ecpSave()
  endproc


  add object oBtn_2_6 as StdButton with uid="XRFDSBJDJK",left=183, top=160, width=20,height=20,;
    caption="...", nPag=2;
    , ToolTipText = "Selezione distinte";
    , HelpContextID = 108624854;
  , bGlobalFont=.t.

    proc oBtn_2_6.Click()
      do gste_kds with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (empty(.w_GPCODCON))
      endwith
    endif
  endfunc

  func oBtn_2_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GP__TIPO='M')
     endwith
    endif
  endfunc

  add object oGPRIFDIS_2_7 as StdField with uid="HIYAZERGPO",rtseq=48,rtrep=.f.,;
    cFormVar = "w_GPRIFDIS", cQueryName = "GPRIFDIS",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento distinta di selezione (vuota=nessuna selezione)",;
    HelpContextID = 14360647,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=216, Top=160, InputMask=replicate('X',10)

  func oGPRIFDIS_2_7.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc


  add object oObj_2_13 as cp_runprogram with uid="FLHYXLPKQB",left=-6, top=550, width=175,height=25,;
    caption='GSSO_BPD(LOAD)',;
   bGlobalFont=.t.,;
    prg="Gsso_bpd('LOAD')",;
    cEvent = "Load",;
    nPag=2;
    , HelpContextID = 205599018

  add object oDESBAN_2_23 as StdField with uid="CHGQEOEBLM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 147711030,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=259, Top=88, InputMask=replicate('X',35)

  func oDESBAN_2_23.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oDESPRE_2_24 as StdField with uid="JOQYFGUMUL",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 15459382,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=259, Top=122, InputMask=replicate('X',35)

  func oDESPRE_2_24.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="CHJTHMUJFH",Visible=.t., Left=5, Top=25,;
    Alignment=1, Width=111, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="TDTLKBEFLW",Visible=.t., Left=5, Top=57,;
    Alignment=1, Width=111, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="TOMABAEAGM",Visible=.t., Left=123, Top=160,;
    Alignment=0, Width=58, Height=18,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="IWARIAIHOS",Visible=.t., Left=5, Top=160,;
    Alignment=1, Width=111, Height=18,;
    Caption="Distinte:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="KNZVPSZDRH",Visible=.t., Left=5, Top=88,;
    Alignment=1, Width=111, Height=18,;
    Caption="Banca di pres.:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="XBQZAIPIDE",Visible=.t., Left=5, Top=122,;
    Alignment=1, Width=111, Height=18,;
    Caption="Banca insoluti:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (.w_GP__TIPO='M')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_api','GES_PIAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GPCODRAG=GES_PIAN.GPCODRAG";
  +" and "+i_cAliasName2+".GPCOMPET=GES_PIAN.GPCOMPET";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
