* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mds                                                        *
*              Dettaglio solleciti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-19                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsso_mds")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsso_mds")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsso_mds")
  return

* --- Class definition
define class tgsso_mds as StdPCForm
  Width  = 691
  Height = 408
  Top    = 6
  Left   = 5
  cComment = "Dettaglio solleciti"
  cPrg = "gsso_mds"
  HelpContextID=80319849
  add object cnt as tcgsso_mds
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsso_mds as PCContext
  w_DSSERIAL = space(10)
  w_DSNUMLIV = 0
  w_NOMPRG = space(10)
  w_DSRIFTES = space(6)
  w_DSMEZSPE = space(2)
  w_DSDATINV = space(8)
  w_DSIMPINT = 0
  w_DSIMPSPE = 0
  w_TOTINT = 0
  w_TOTSPE = 0
  w_TOTADD = 0
  w_FLEURO = space(1)
  w_CODAZI = space(10)
  w_SPELET = 0
  w_SPEFAX = 0
  w_SPEMAI = 0
  w_SPERAC = 0
  w_SPERAR = 0
  w_MEZSPE = space(2)
  w_DECTOT = 0
  w_DATINV = space(8)
  w_VALPAR = space(3)
  w_SPETEL = 0
  w_SPEPEC = 0
  proc Save(i_oFrom)
    this.w_DSSERIAL = i_oFrom.w_DSSERIAL
    this.w_DSNUMLIV = i_oFrom.w_DSNUMLIV
    this.w_NOMPRG = i_oFrom.w_NOMPRG
    this.w_DSRIFTES = i_oFrom.w_DSRIFTES
    this.w_DSMEZSPE = i_oFrom.w_DSMEZSPE
    this.w_DSDATINV = i_oFrom.w_DSDATINV
    this.w_DSIMPINT = i_oFrom.w_DSIMPINT
    this.w_DSIMPSPE = i_oFrom.w_DSIMPSPE
    this.w_TOTINT = i_oFrom.w_TOTINT
    this.w_TOTSPE = i_oFrom.w_TOTSPE
    this.w_TOTADD = i_oFrom.w_TOTADD
    this.w_FLEURO = i_oFrom.w_FLEURO
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_SPELET = i_oFrom.w_SPELET
    this.w_SPEFAX = i_oFrom.w_SPEFAX
    this.w_SPEMAI = i_oFrom.w_SPEMAI
    this.w_SPERAC = i_oFrom.w_SPERAC
    this.w_SPERAR = i_oFrom.w_SPERAR
    this.w_MEZSPE = i_oFrom.w_MEZSPE
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_DATINV = i_oFrom.w_DATINV
    this.w_VALPAR = i_oFrom.w_VALPAR
    this.w_SPETEL = i_oFrom.w_SPETEL
    this.w_SPEPEC = i_oFrom.w_SPEPEC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DSSERIAL = this.w_DSSERIAL
    i_oTo.w_DSNUMLIV = this.w_DSNUMLIV
    i_oTo.w_NOMPRG = this.w_NOMPRG
    i_oTo.w_DSRIFTES = this.w_DSRIFTES
    i_oTo.w_DSMEZSPE = this.w_DSMEZSPE
    i_oTo.w_DSDATINV = this.w_DSDATINV
    i_oTo.w_DSIMPINT = this.w_DSIMPINT
    i_oTo.w_DSIMPSPE = this.w_DSIMPSPE
    i_oTo.w_TOTINT = this.w_TOTINT
    i_oTo.w_TOTSPE = this.w_TOTSPE
    i_oTo.w_TOTADD = this.w_TOTADD
    i_oTo.w_FLEURO = this.w_FLEURO
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_SPELET = this.w_SPELET
    i_oTo.w_SPEFAX = this.w_SPEFAX
    i_oTo.w_SPEMAI = this.w_SPEMAI
    i_oTo.w_SPERAC = this.w_SPERAC
    i_oTo.w_SPERAR = this.w_SPERAR
    i_oTo.w_MEZSPE = this.w_MEZSPE
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_DATINV = this.w_DATINV
    i_oTo.w_VALPAR = this.w_VALPAR
    i_oTo.w_SPETEL = this.w_SPETEL
    i_oTo.w_SPEPEC = this.w_SPEPEC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsso_mds as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 691
  Height = 408
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-14"
  HelpContextID=80319849
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_SOLL_IDX = 0
  TES_CONT_IDX = 0
  PAR_CONT_IDX = 0
  cFile = "DET_SOLL"
  cKeySelect = "DSSERIAL"
  cKeyWhere  = "DSSERIAL=this.w_DSSERIAL"
  cKeyDetail  = "DSSERIAL=this.w_DSSERIAL"
  cKeyWhereODBC = '"DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';

  cKeyDetailWhereODBC = '"DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DET_SOLL.DSSERIAL="+cp_ToStrODBC(this.w_DSSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DET_SOLL.CPROWNUM '
  cPrg = "gsso_mds"
  cComment = "Dettaglio solleciti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DSSERIAL = space(10)
  w_DSNUMLIV = 0
  w_NOMPRG = space(10)
  w_DSRIFTES = space(6)
  w_DSMEZSPE = space(2)
  w_DSDATINV = ctod('  /  /  ')
  w_DSIMPINT = 0
  w_DSIMPSPE = 0
  w_TOTINT = 0
  w_TOTSPE = 0
  w_TOTADD = 0
  w_FLEURO = space(1)
  w_CODAZI = space(10)
  w_SPELET = 0
  w_SPEFAX = 0
  w_SPEMAI = 0
  w_SPERAC = 0
  w_SPERAR = 0
  w_MEZSPE = space(2)
  w_DECTOT = 0
  w_DATINV = ctod('  /  /  ')
  w_VALPAR = space(3)
  w_SPETEL = 0
  w_SPEPEC = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_mdsPag1","gsso_mds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TES_CONT'
    this.cWorkTables[2]='PAR_CONT'
    this.cWorkTables[3]='DET_SOLL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_SOLL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_SOLL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsso_mds'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_SOLL where DSSERIAL=KeySet.DSSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2],this.bLoadRecFilter,this.DET_SOLL_IDX,"gsso_mds")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_SOLL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_SOLL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_SOLL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DSSERIAL',this.w_DSSERIAL  )
      select * from (i_cTable) DET_SOLL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NOMPRG = 'GSSO_SSO'
        .w_TOTSPE = 0
        .w_FLEURO = space(1)
        .w_SPELET = 0
        .w_SPEFAX = 0
        .w_SPEMAI = 0
        .w_SPERAC = 0
        .w_SPERAR = 0
        .w_MEZSPE = space(2)
        .w_DECTOT = 0
        .w_DATINV = ctod("  /  /  ")
        .w_VALPAR = space(3)
        .w_SPETEL = 0
        .w_SPEPEC = 0
        .w_DSSERIAL = NVL(DSSERIAL,space(10))
        .w_TOTINT = .w_DSIMPINT
        .w_TOTADD = .w_TOTINT+.w_TOTSPE
        .w_CODAZI = i_CODAZI
          .link_1_4('Load')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_SOLL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTSPE = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DSNUMLIV = NVL(DSNUMLIV,0)
          .w_DSRIFTES = NVL(DSRIFTES,space(6))
          * evitabile
          *.link_2_2('Load')
          .w_DSMEZSPE = NVL(DSMEZSPE,space(2))
          .w_DSDATINV = NVL(cp_ToDate(DSDATINV),ctod("  /  /  "))
          .w_DSIMPINT = NVL(DSIMPINT,0)
          .w_DSIMPSPE = NVL(DSIMPSPE,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTSPE = .w_TOTSPE+.w_DSIMPSPE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TOTINT = .w_DSIMPINT
        .w_TOTADD = .w_TOTINT+.w_TOTSPE
        .w_CODAZI = i_CODAZI
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_7.enabled = .oPgFrm.Page1.oPag.oBtn_2_7.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DSSERIAL=space(10)
      .w_DSNUMLIV=0
      .w_NOMPRG=space(10)
      .w_DSRIFTES=space(6)
      .w_DSMEZSPE=space(2)
      .w_DSDATINV=ctod("  /  /  ")
      .w_DSIMPINT=0
      .w_DSIMPSPE=0
      .w_TOTINT=0
      .w_TOTSPE=0
      .w_TOTADD=0
      .w_FLEURO=space(1)
      .w_CODAZI=space(10)
      .w_SPELET=0
      .w_SPEFAX=0
      .w_SPEMAI=0
      .w_SPERAC=0
      .w_SPERAR=0
      .w_MEZSPE=space(2)
      .w_DECTOT=0
      .w_DATINV=ctod("  /  /  ")
      .w_VALPAR=space(3)
      .w_SPETEL=0
      .w_SPEPEC=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_NOMPRG = 'GSSO_SSO'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_DSRIFTES))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,8,.f.)
        .w_TOTINT = .w_DSIMPINT
        .DoRTCalc(10,10,.f.)
        .w_TOTADD = .w_TOTINT+.w_TOTSPE
        .DoRTCalc(12,12,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODAZI))
         .link_1_4('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_SOLL')
    this.DoRTCalc(14,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled = this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_7.enabled = .Page1.oPag.oBtn_2_7.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_SOLL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DSSERIAL,"DSSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DSNUMLIV N(2);
      ,t_DSRIFTES C(6);
      ,t_DSMEZSPE N(3);
      ,t_DSDATINV D(8);
      ,t_DSIMPINT N(18,4);
      ,t_DSIMPSPE N(18,4);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_mdsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMLIV_2_1.controlsource=this.cTrsName+'.t_DSNUMLIV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSRIFTES_2_2.controlsource=this.cTrsName+'.t_DSRIFTES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.controlsource=this.cTrsName+'.t_DSMEZSPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSDATINV_2_4.controlsource=this.cTrsName+'.t_DSDATINV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPINT_2_5.controlsource=this.cTrsName+'.t_DSIMPINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPSPE_2_6.controlsource=this.cTrsName+'.t_DSIMPSPE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(48)
    this.AddVLine(139)
    this.AddVLine(223)
    this.AddVLine(317)
    this.AddVLine(463)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMLIV_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2])
      *
      * insert into DET_SOLL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_SOLL')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_SOLL')
        i_cFldBody=" "+;
                  "(DSSERIAL,DSNUMLIV,DSRIFTES,DSMEZSPE,DSDATINV"+;
                  ",DSIMPINT,DSIMPSPE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DSSERIAL)+","+cp_ToStrODBC(this.w_DSNUMLIV)+","+cp_ToStrODBCNull(this.w_DSRIFTES)+","+cp_ToStrODBC(this.w_DSMEZSPE)+","+cp_ToStrODBC(this.w_DSDATINV)+;
             ","+cp_ToStrODBC(this.w_DSIMPINT)+","+cp_ToStrODBC(this.w_DSIMPSPE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_SOLL')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_SOLL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DSSERIAL',this.w_DSSERIAL)
        INSERT INTO (i_cTable) (;
                   DSSERIAL;
                  ,DSNUMLIV;
                  ,DSRIFTES;
                  ,DSMEZSPE;
                  ,DSDATINV;
                  ,DSIMPINT;
                  ,DSIMPSPE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DSSERIAL;
                  ,this.w_DSNUMLIV;
                  ,this.w_DSRIFTES;
                  ,this.w_DSMEZSPE;
                  ,this.w_DSDATINV;
                  ,this.w_DSIMPINT;
                  ,this.w_DSIMPSPE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DSRIFTES))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_SOLL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_SOLL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DSRIFTES))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_SOLL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_SOLL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DSNUMLIV="+cp_ToStrODBC(this.w_DSNUMLIV)+;
                     ",DSRIFTES="+cp_ToStrODBCNull(this.w_DSRIFTES)+;
                     ",DSMEZSPE="+cp_ToStrODBC(this.w_DSMEZSPE)+;
                     ",DSDATINV="+cp_ToStrODBC(this.w_DSDATINV)+;
                     ",DSIMPINT="+cp_ToStrODBC(this.w_DSIMPINT)+;
                     ",DSIMPSPE="+cp_ToStrODBC(this.w_DSIMPSPE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_SOLL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DSNUMLIV=this.w_DSNUMLIV;
                     ,DSRIFTES=this.w_DSRIFTES;
                     ,DSMEZSPE=this.w_DSMEZSPE;
                     ,DSDATINV=this.w_DSDATINV;
                     ,DSIMPINT=this.w_DSIMPINT;
                     ,DSIMPSPE=this.w_DSIMPSPE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DSRIFTES))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_SOLL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DSRIFTES))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_SOLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_SOLL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
          .w_TOTINT = .w_DSIMPINT
        .DoRTCalc(10,10,.t.)
          .w_TOTADD = .w_TOTINT+.w_TOTSPE
        .DoRTCalc(12,12,.t.)
          .w_CODAZI = i_CODAZI
          .link_1_4('Full')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate(.w_NOMPRG)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_7.visible=!this.oPgFrm.Page1.oPag.oBtn_2_7.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DSRIFTES
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_lTable = "TES_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2], .t., this.TES_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DSRIFTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TES_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCNUMERO like "+cp_ToStrODBC(trim(this.w_DSRIFTES)+"%");

          i_ret=cp_SQL(i_nConn,"select TCNUMERO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCNUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCNUMERO',trim(this.w_DSRIFTES))
          select TCNUMERO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCNUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DSRIFTES)==trim(_Link_.TCNUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DSRIFTES) and !this.bDontReportError
            deferred_cp_zoom('TES_CONT','*','TCNUMERO',cp_AbsName(oSource.parent,'oDSRIFTES_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO";
                     +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',oSource.xKey(1))
            select TCNUMERO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DSRIFTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO";
                   +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(this.w_DSRIFTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',this.w_DSRIFTES)
            select TCNUMERO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DSRIFTES = NVL(_Link_.TCNUMERO,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_DSRIFTES = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])+'\'+cp_ToStr(_Link_.TCNUMERO,1)
      cp_ShowWarn(i_cKey,this.TES_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DSRIFTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_lTable = "PAR_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2], .t., this.PAR_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCVALIMP,PCSPETEL,PCSPEPEC";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_CODAZI)
            select PCCODAZI,PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCVALIMP,PCSPETEL,PCSPEPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PCCODAZI,space(10))
      this.w_SPELET = NVL(_Link_.PCSPELET,0)
      this.w_SPERAC = NVL(_Link_.PCSPERAC,0)
      this.w_SPERAR = NVL(_Link_.PCSPERAR,0)
      this.w_SPEFAX = NVL(_Link_.PCSPEFAX,0)
      this.w_SPEMAI = NVL(_Link_.PCSPEMAI,0)
      this.w_VALPAR = NVL(_Link_.PCVALIMP,space(3))
      this.w_SPETEL = NVL(_Link_.PCSPETEL,0)
      this.w_SPEPEC = NVL(_Link_.PCSPEPEC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_SPELET = 0
      this.w_SPERAC = 0
      this.w_SPERAR = 0
      this.w_SPEFAX = 0
      this.w_SPEMAI = 0
      this.w_VALPAR = space(3)
      this.w_SPETEL = 0
      this.w_SPEPEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTINT_3_1.value==this.w_TOTINT)
      this.oPgFrm.Page1.oPag.oTOTINT_3_1.value=this.w_TOTINT
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSPE_3_2.value==this.w_TOTSPE)
      this.oPgFrm.Page1.oPag.oTOTSPE_3_2.value=this.w_TOTSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTADD_3_4.value==this.w_TOTADD)
      this.oPgFrm.Page1.oPag.oTOTADD_3_4.value=this.w_TOTADD
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMLIV_2_1.value==this.w_DSNUMLIV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMLIV_2_1.value=this.w_DSNUMLIV
      replace t_DSNUMLIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSNUMLIV_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSRIFTES_2_2.value==this.w_DSRIFTES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSRIFTES_2_2.value=this.w_DSRIFTES
      replace t_DSRIFTES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSRIFTES_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.RadioValue()==this.w_DSMEZSPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.SetRadio()
      replace t_DSMEZSPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSDATINV_2_4.value==this.w_DSDATINV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSDATINV_2_4.value=this.w_DSDATINV
      replace t_DSDATINV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSDATINV_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPINT_2_5.value==this.w_DSIMPINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPINT_2_5.value=this.w_DSIMPINT
      replace t_DSIMPINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPINT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPSPE_2_6.value==this.w_DSIMPSPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPSPE_2_6.value=this.w_DSIMPSPE
      replace t_DSIMPSPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSIMPSPE_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_SOLL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_DSRIFTES))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DSRIFTES)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DSNUMLIV=0
      .w_DSRIFTES=space(6)
      .w_DSMEZSPE=space(2)
      .w_DSDATINV=ctod("  /  /  ")
      .w_DSIMPINT=0
      .w_DSIMPSPE=0
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_DSRIFTES))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(5,24,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DSNUMLIV = t_DSNUMLIV
    this.w_DSRIFTES = t_DSRIFTES
    this.w_DSMEZSPE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.RadioValue(.t.)
    this.w_DSDATINV = t_DSDATINV
    this.w_DSIMPINT = t_DSIMPINT
    this.w_DSIMPSPE = t_DSIMPSPE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DSNUMLIV with this.w_DSNUMLIV
    replace t_DSRIFTES with this.w_DSRIFTES
    replace t_DSMEZSPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDSMEZSPE_2_3.ToRadio()
    replace t_DSDATINV with this.w_DSDATINV
    replace t_DSIMPINT with this.w_DSIMPINT
    replace t_DSIMPSPE with this.w_DSIMPSPE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTSPE = .w_TOTSPE-.w_dsimpspe
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsso_mdsPag1 as StdContainer
  Width  = 687
  height = 408
  stdWidth  = 687
  stdheight = 408
  resizeXpos=588
  resizeYpos=160
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_15 as cp_outputCombo with uid="VDNLZAUBWY",left=288, top=382, width=391,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    cDefSep="",cNoDefSep="",FontBold=.f.,FontItalic=.t.,Font="Arial",FontSize=8,;
    nPag=1;
    , HelpContextID = 97677798


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=13, width=609,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DSNUMLIV",Label1="Livello",Field2="DSRIFTES",Label2="Testo inviato",Field3="DSMEZSPE",Label3="A mezzo",Field4="DSDATINV",Label4="Data invio",Field5="DSIMPINT",Label5="Importo interessi",Field6="DSIMPSPE",Label6="Spese notificate",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235771002

  add object oStr_1_14 as StdString with uid="VHTGDBMZLM",Visible=.t., Left=131, Top=382,;
    Alignment=1, Width=156, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=32,;
    width=605+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=33,width=604+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TES_CONT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TES_CONT'
        oDropInto=this.oBodyCol.oRow.oDSRIFTES_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_7 as StdButton with uid="XVFSKZRQBE",width=48,height=45,;
   left=631, top=35,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere ristampare i solleciti";
    , HelpContextID = 61469734;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        GSSO_BZ2(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(.cFunction<>'Load' AND Not Empty(.w_OREP) And .w_dsnumliv >0 And Not Empty(.w_dsriftes) And Not Empty(.w_dsmezspe) And Not Empty(.w_dsdatinv)))
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTINT_3_1 as StdField with uid="OAQDOOSNBF",rtseq=9,rtrep=.f.,;
    cFormVar="w_TOTINT",value=0,enabled=.f.,;
    ToolTipText = "Ultimi interessi calcolati",;
    HelpContextID = 194707658,;
    cQueryName = "TOTINT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=291, Top=331, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTSPE_3_2 as StdField with uid="FIJIUKFGJX",rtseq=10,rtrep=.f.,;
    cFormVar="w_TOTSPE",value=0,enabled=.f.,;
    ToolTipText = "Totale spese spedizione",;
    HelpContextID = 175177930,;
    cQueryName = "TOTSPE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=433, Top=331, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTADD_3_4 as StdField with uid="VDAFDFULUY",rtseq=11,rtrep=.f.,;
    cFormVar="w_TOTADD",value=0,enabled=.f.,;
    ToolTipText = "Totale spese spedizione + ultimi interessi calcolati",;
    HelpContextID = 205717706,;
    cQueryName = "TOTADD",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=291, Top=356, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oStr_3_3 as StdString with uid="MSNSASMOFG",Visible=.t., Left=131, Top=335,;
    Alignment=1, Width=156, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="JDYTEXLLLZ",Visible=.t., Left=131, Top=359,;
    Alignment=1, Width=156, Height=18,;
    Caption="Totale spese addebitate:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsso_mdsBodyRow as CPBodyRowCnt
  Width=595
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDSNUMLIV_2_1 as StdTrsField with uid="TUNXASYSWX",rtseq=2,rtrep=.t.,;
    cFormVar="w_DSNUMLIV",value=0,;
    ToolTipText = "N.livello",;
    HelpContextID = 207659660,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=36, Left=-2, Top=0, cSayPict=["99"], cGetPict=["99"]

  add object oDSRIFTES_2_2 as StdTrsField with uid="CXXMARADRM",rtseq=4,rtrep=.t.,;
    cFormVar="w_DSRIFTES",value=space(6),;
    ToolTipText = "Numero testo inviato",;
    HelpContextID = 65331849,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=88, Left=37, Top=0, InputMask=replicate('X',6), cLinkFile="TES_CONT", oKey_1_1="TCNUMERO", oKey_1_2="this.w_DSRIFTES"

  func oDSRIFTES_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDSRIFTES_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oDSMEZSPE_2_3 as StdTrsCombo with uid="MDEEEEQNJS",rtrep=.t.,;
    cFormVar="w_DSMEZSPE", RowSource=""+"Raccomandata,"+"FAX,"+"Lettera,"+"Raccom. A.R.,"+"E-mail,"+"Telefono,"+"PEC" , ;
    ToolTipText = "Spedito a mezzo",;
    HelpContextID = 199191941,;
    Height=21, Width=76, Left=128, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDSMEZSPE_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DSMEZSPE,&i_cF..t_DSMEZSPE),this.value)
    return(iif(xVal =1,'RA',;
    iif(xVal =2,'FA',;
    iif(xVal =3,'LE',;
    iif(xVal =4,'AR',;
    iif(xVal =5,'EM',;
    iif(xVal =6,'TE',;
    iif(xVal =7,'PE',;
    space(2)))))))))
  endfunc
  func oDSMEZSPE_2_3.GetRadio()
    this.Parent.oContained.w_DSMEZSPE = this.RadioValue()
    return .t.
  endfunc

  func oDSMEZSPE_2_3.ToRadio()
    this.Parent.oContained.w_DSMEZSPE=trim(this.Parent.oContained.w_DSMEZSPE)
    return(;
      iif(this.Parent.oContained.w_DSMEZSPE=='RA',1,;
      iif(this.Parent.oContained.w_DSMEZSPE=='FA',2,;
      iif(this.Parent.oContained.w_DSMEZSPE=='LE',3,;
      iif(this.Parent.oContained.w_DSMEZSPE=='AR',4,;
      iif(this.Parent.oContained.w_DSMEZSPE=='EM',5,;
      iif(this.Parent.oContained.w_DSMEZSPE=='TE',6,;
      iif(this.Parent.oContained.w_DSMEZSPE=='PE',7,;
      0))))))))
  endfunc

  func oDSMEZSPE_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDSDATINV_2_4 as StdTrsField with uid="UOCDZYXOQF",rtseq=6,rtrep=.t.,;
    cFormVar="w_DSDATINV",value=ctod("  /  /  "),;
    HelpContextID = 105119092,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=80, Left=217, Top=0

  add object oDSIMPINT_2_5 as StdTrsField with uid="YZPFPJCAAW",rtseq=7,rtrep=.t.,;
    cFormVar="w_DSIMPINT",value=0,;
    ToolTipText = "Importo interessi",;
    HelpContextID = 108506486,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=138, Left=311, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oDSIMPSPE_2_6 as StdTrsField with uid="VSXKDEOUKZ",rtseq=8,rtrep=.t.,;
    cFormVar="w_DSIMPSPE",value=0,;
    ToolTipText = "Spese notificate",;
    HelpContextID = 209169797,;
    cTotal = "this.Parent.oContained.w_totspe", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=138, Left=452, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)], Width=130
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDSNUMLIV_2_1.When()
    return(.t.)
  proc oDSNUMLIV_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDSNUMLIV_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mds','DET_SOLL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DSSERIAL=DET_SOLL.DSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
