* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_be2                                                        *
*              Apro la riga con la descrizione del promemoria contabile        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-08                                                      *
* Last revis.: 2012-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_be2",oParentObject)
return(i_retval)

define class tgsso_be2 as StdBatch
  * --- Local variables
  w_CPROWNUM = 0
  * --- WorkFile variables
  LOG_REBA_idx=0
  FLU_ESRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apro la riga dell'errore
    * --- Select from gsso_be2
    do vq_exec with 'gsso_be2',this,'_Curs_gsso_be2','',.f.,.t.
    if used('_Curs_gsso_be2')
      select _Curs_gsso_be2
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = CPROWNUM
        select _Curs_gsso_be2
        continue
      enddo
      use
    endif
    if this.w_CPROWNUM<>0
      OpenGest("A", "GSSO_MER", "FRNOMSUP", this.oParentObject.w_NOMSUP, "FRDTARIC", this.oParentObject.w_DTARIC, "FRNOMFIL", this.oParentObject.w_NOMFIL,"CPROWNUM",this.w_CPROWNUM)
    else
      ah_errormsg("Non � stata trovata la riga di riferimento del promemoria contabile")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LOG_REBA'
    this.cWorkTables[2]='FLU_ESRI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gsso_be2')
      use in _Curs_gsso_be2
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
