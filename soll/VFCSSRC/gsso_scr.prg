* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_scr                                                        *
*              Stampa causali Remote Banking                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-24                                                      *
* Last revis.: 2012-08-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_scr",oParentObject))

* --- Class definition
define class tgsso_scr as StdForm
  Top    = 52
  Left   = 50

  * --- Standard Properties
  Width  = 479
  Height = 151
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-21"
  HelpContextID=90805609
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  CAU_REBA_IDX = 0
  cPrg = "gsso_scr"
  cComment = "Stampa causali Remote Banking"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICEIN = space(5)
  w_CODICEFI = space(5)
  w_DESCCODI = space(60)
  w_DESCCODF = space(60)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_scrPag1","gsso_scr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICEIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAU_REBA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICEIN=space(5)
      .w_CODICEFI=space(5)
      .w_DESCCODI=space(60)
      .w_DESCCODF=space(60)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODICEIN))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICEFI))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICEIN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_lTable = "CAU_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2], .t., this.CAU_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICEIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsrb_ARB',True,'CAU_REBA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODCAU like "+cp_ToStrODBC(trim(this.w_CODICEIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODCAU',trim(this.w_CODICEIN))
          select CACODCAU,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICEIN)==trim(_Link_.CACODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICEIN) and !this.bDontReportError
            deferred_cp_zoom('CAU_REBA','*','CACODCAU',cp_AbsName(oSource.parent,'oCODICEIN_1_1'),i_cWhere,'gsrb_ARB',"Causali Remote Banking",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',oSource.xKey(1))
            select CACODCAU,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICEIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(this.w_CODICEIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',this.w_CODICEIN)
            select CACODCAU,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICEIN = NVL(_Link_.CACODCAU,space(5))
      this.w_DESCCODI = NVL(_Link_.CADESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODICEIN = space(5)
      endif
      this.w_DESCCODI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODICEIN < .w_CODICEFI) or empty(.w_CODICEFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore del codice finale o incogruente")
        endif
        this.w_CODICEIN = space(5)
        this.w_DESCCODI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])+'\'+cp_ToStr(_Link_.CACODCAU,1)
      cp_ShowWarn(i_cKey,this.CAU_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICEIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICEFI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_lTable = "CAU_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2], .t., this.CAU_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICEFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsrb_ARB',True,'CAU_REBA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODCAU like "+cp_ToStrODBC(trim(this.w_CODICEFI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODCAU',trim(this.w_CODICEFI))
          select CACODCAU,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICEFI)==trim(_Link_.CACODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICEFI) and !this.bDontReportError
            deferred_cp_zoom('CAU_REBA','*','CACODCAU',cp_AbsName(oSource.parent,'oCODICEFI_1_2'),i_cWhere,'gsrb_ARB',"Causali Remote Banking",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',oSource.xKey(1))
            select CACODCAU,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICEFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(this.w_CODICEFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',this.w_CODICEFI)
            select CACODCAU,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICEFI = NVL(_Link_.CACODCAU,space(5))
      this.w_DESCCODF = NVL(_Link_.CADESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODICEFI = space(5)
      endif
      this.w_DESCCODF = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODICEFI > .w_CODICEIN) or empty(.w_CODICEIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale minore del codice iniziale o incogruente")
        endif
        this.w_CODICEFI = space(5)
        this.w_DESCCODF = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])+'\'+cp_ToStr(_Link_.CACODCAU,1)
      cp_ShowWarn(i_cKey,this.CAU_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICEFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICEIN_1_1.value==this.w_CODICEIN)
      this.oPgFrm.Page1.oPag.oCODICEIN_1_1.value=this.w_CODICEIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICEFI_1_2.value==this.w_CODICEFI)
      this.oPgFrm.Page1.oPag.oCODICEFI_1_2.value=this.w_CODICEFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCODI_1_9.value==this.w_DESCCODI)
      this.oPgFrm.Page1.oPag.oDESCCODI_1_9.value=this.w_DESCCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCODF_1_10.value==this.w_DESCCODF)
      this.oPgFrm.Page1.oPag.oDESCCODF_1_10.value=this.w_DESCCODF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CODICEIN < .w_CODICEFI) or empty(.w_CODICEFI))  and not(empty(.w_CODICEIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICEIN_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore del codice finale o incogruente")
          case   not((.w_CODICEFI > .w_CODICEIN) or empty(.w_CODICEIN))  and not(empty(.w_CODICEFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICEFI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale minore del codice iniziale o incogruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_scrPag1 as StdContainer
  Width  = 475
  height = 151
  stdWidth  = 475
  stdheight = 151
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICEIN_1_1 as StdField with uid="BTMZJSVUVB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODICEIN", cQueryName = "CODICEIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore del codice finale o incogruente",;
    ToolTipText = "Codice causale Remote Banking iniziale",;
    HelpContextID = 68419188,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=14, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_REBA", cZoomOnZoom="gsrb_ARB", oKey_1_1="CACODCAU", oKey_1_2="this.w_CODICEIN"

  func oCODICEIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICEIN_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICEIN_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_REBA','*','CACODCAU',cp_AbsName(this.parent,'oCODICEIN_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gsrb_ARB',"Causali Remote Banking",'',this.parent.oContained
  endproc
  proc oCODICEIN_1_1.mZoomOnZoom
    local i_obj
    i_obj=gsrb_ARB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODCAU=this.parent.oContained.w_CODICEIN
     i_obj.ecpSave()
  endproc

  add object oCODICEFI_1_2 as StdField with uid="KZAGKDFLKT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICEFI", cQueryName = "CODICEFI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale minore del codice iniziale o incogruente",;
    ToolTipText = "Codice causale Remote Banking finale",;
    HelpContextID = 68419183,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_REBA", cZoomOnZoom="gsrb_ARB", oKey_1_1="CACODCAU", oKey_1_2="this.w_CODICEFI"

  func oCODICEFI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICEFI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICEFI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_REBA','*','CACODCAU',cp_AbsName(this.parent,'oCODICEFI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsrb_ARB',"Causali Remote Banking",'',this.parent.oContained
  endproc
  proc oCODICEFI_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsrb_ARB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODCAU=this.parent.oContained.w_CODICEFI
     i_obj.ecpSave()
  endproc


  add object oObj_1_5 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=71, width=362,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87192038


  add object oBtn_1_6 as StdButton with uid="OSXGXXPXXC",left=368, top=99, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 140310;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="BWTKCLQRSS",left=419, top=99, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 140310;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCCODI_1_9 as StdField with uid="IQIGLFNDKM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCCODI", cQueryName = "DESCCODI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 235857023,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=171, Top=14, InputMask=replicate('X',60)

  add object oDESCCODF_1_10 as StdField with uid="DGKDYQJBAA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCCODF", cQueryName = "DESCCODF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 235857020,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=171, Top=43, InputMask=replicate('X',60)

  add object oStr_1_3 as StdString with uid="JAMJVCFHSJ",Visible=.t., Left=38, Top=17,;
    Alignment=1, Width=64, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="MIHQIFMXWO",Visible=.t., Left=41, Top=46,;
    Alignment=1, Width=61, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JXQZRHYFJX",Visible=.t., Left=7, Top=72,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_scr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
