* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bfr                                                        *
*              Ricezione flussi di ritorno                                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2015-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bfr",oParentObject,m.pParam)
return(i_retval)

define class tgsso_bfr as StdBatch
  * --- Local variables
  pParam = space(1)
  w_ORIGPATH = space(200)
  w_ENDFILE = .f.
  w_NOMSUP = space(20)
  w_OPER = space(2)
  w_TIPIMP = space(1)
  w_DTARIC = ctod("  /  /  ")
  w_NOMFIL = space(100)
  w_DATIEL = space(200)
  w_NUMRIG = 0
  w_NUMPRO = 0
  w_SERIALKEY = space(10)
  w_NUMDIS = space(10)
  w_ERROR = .f.
  w_ERRORE = space(1)
  w_NUMREC = 0
  w_ABIRIC = space(5)
  w_ABIMIT = space(5)
  w_CODCAU = space(2)
  w_CAUABI = space(5)
  w_NUMEFF = 0
  w_CPROWNUM = 0
  w_CODSIA = space(5)
  w_CODABI = space(5)
  w_CODCAB = space(5)
  w_CONCOR = space(12)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODBEN = space(15)
  w_NOMSUPOR = space(20)
  w_TIPDIS = space(2)
  w_DTCFRB = ctod("  /  /  ")
  w_NUMCOR = space(12)
  w_RECSPE = .f.
  w_READEFF = 0
  w_READDIS = space(10)
  w_OLDREC = 0
  w_FLGRIC = space(1)
  w_FORNUNICO = space(1)
  w_FORNITORE = space(15)
  w_BACODBAN = space(15)
  w_BANCA = space(15)
  w_TEMP = space(15)
  w_DIMENSIONE = 0
  w_DTASCA = ctod("  /  /  ")
  w_IMPORTO = 0
  w_CODFIS = space(16)
  w_ULTIMOTRECORD = .f.
  w_NRIBA = 0
  w_AVSERIAL = space(10)
  w_CREDITORE = space(90)
  w_RIFOP = space(100)
  w_NUMAVV = space(40)
  w_DTACRE = ctod("  /  /  ")
  w_AVVSCAVIS = space(1)
  w_AVSEGNO = space(1)
  w_AVSIAFOR = space(5)
  w_BLOCCANTE = space(1)
  w_TITOLO = space(100)
  w_ERRAVV = space(1)
  w_APPO = space(100)
  w_NUMCONTI = 0
  w_TIPRID = space(1)
  w_TIPRID1 = space(1)
  w_CODCON1 = space(15)
  w_NEWFLUSSO = space(2)
  w_POSREC = 0
  w_TIPOEFF = space(1)
  w_RECPOS = 0
  w_ALTRAAZIENDA = .f.
  w_IDAZI = space(20)
  w_CHECKDATIEL = space(200)
  w_CODFIS1 = space(16)
  w_PIVA1 = space(32)
  w_MESSAGGIO = space(0)
  w_CODICILETTI = space(0)
  w_CONTA = 0
  w_OLDDIS = space(10)
  w_CATIPMOV = space(3)
  w_NOSELECT = .f.
  w_TOTSPE = 0
  w_DATVAL = ctod("  /  /  ")
  w_DATCOM = ctod("  /  /  ")
  w_NUMRECZ = 0
  w_TOTIMP = 0
  w_ERROREF = space(1)
  w_FIRRIG = 0
  w_LASRIG = 0
  w_FRNUMREC = 0
  w_FRRIFPRO = space(12)
  w_FRCFISDE = space(16)
  w_FRDATSCA = ctod("  /  /  ")
  w_PTDATSCA = ctod("  /  /  ")
  w_PTDATRAG = ctod("  /  /  ")
  w_FRNUMCOR = space(15)
  w_FRRIGPRO = space(1)
  w_ERR_CLI = .f.
  w_PROM_CONT = .f.
  w_PIVA = space(32)
  w_SAGINT = 0
  w_ADDINT = space(1)
  w_CODAZI = space(5)
  w_MANDATI = .f.
  w_NUMREC = 0
  w_BACODABI = space(5)
  w_BACODCAB = space(5)
  w_BACONCOR = space(12)
  w_BACONCOL = space(15)
  * --- WorkFile variables
  FLU_RITO_idx=0
  DIS_TINT_idx=0
  PAR_TITE_idx=0
  CONTI_idx=0
  COC_MAST_idx=0
  LOG_REBA_idx=0
  AZIENDA_idx=0
  CAU_REBA_idx=0
  FLU_ESRI_idx=0
  OFF_NOMI_idx=0
  PAR_CONT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lettura flusso di ritorno lanciata dalla maschera gsrb_KFR 
    this.w_NUMRIG = 0
    this.w_NUMPRO = 0
    this.w_ERROR = .F.
    this.w_RECSPE = .F.
    this.w_CONTA = 0
    * --- Nuove variabili per scrittura direttamente recordo flussi
    this.w_PROM_CONT = .F.
    this.w_BLOCCANTE = "N"
    this.w_AVSEGNO = ""
    this.w_AVSIAFOR = ""
    this.w_AVVSCAVIS = ""
    this.w_ULTIMOTRECORD = .F.
    this.w_NRIBA = 1
    this.w_CODFIS = ""
    this.w_IMPORTO = 0
    this.w_DTASCA = ""
    this.w_RIFOP = ""
    this.w_DIMENSIONE = 200
    this.w_TEMP = ""
    this.w_FORNUNICO = ""
    this.w_BACODBAN = ""
    this.w_FORNITORE = ""
    * --- Legge Partita iva e cod fisccale dell azienda
    this.w_CODFIS1 = ""
    this.w_PIVA1 = ""
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODFIS1 = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PIVA1 = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODFIS1 = IIF ( NOT EMPTY ( this.w_CODFIS1 ) ,UPPER(ALLTRIM (this.w_CODFIS1)) , space (16) )
    this.w_PIVA1 = IIF ( NOT EMPTY ( this.w_PIVA1 ) ,UPPER(ALLTRIM(this.w_PIVA1)) , space(16) )
    * --- Carica da file di testo il contenuto della tabella Selezionata
    if NOT file( this.oParentObject.w_PATH )
      * --- Se il file non esiste
      this.oParentObject.w_SHOWERROR = .T.
      this.oParentObject.w_MESS = AH_MSGFORMAT ("Il file selezionato non esiste")
      i_retcode = 'stop'
      return
    endif
    do gsso_BRC with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    CREATE CURSOR CursErr (NOMSUP C(20), DTARIC D(8), NOMFIL C(100), NUMRIG N(10), MSG C(200), TIPIMP C(1))
    CREATE CURSOR CursTEMPO (DATIEL C(200)) 
 APPEND FROM (this.oParentObject.w_PATH) TYPE SDF
    SELECT CursTempo 
 GO TOP
    this.w_DATIEL = SUBSTR(DATIEL,1,200)
    this.w_NOMSUP = SUBSTR(this.w_DATIEL,20,20)
    l_SUPP = SUBSTR(this.w_DATIEL,20,20)
    this.w_DTARIC = cp_CharToDate(SUBSTR(this.w_DATIEL,14,2)+"-"+SUBSTR(this.w_DATIEL,16,2)+"-"+SUBSTR(this.w_DATIEL,18,2))
    l_RIC=cp_CharToDate(SUBSTR(this.w_DATIEL,14,2)+"-"+SUBSTR(this.w_DATIEL,16,2)+"-"+SUBSTR(this.w_DATIEL,18,2))
    this.w_NOMFIL = SUBSTR( LTRIM( SUBSTR(this.oParentObject.w_PATH, RAT("\",this.oParentObject.w_PATH)+1 ) ), 1, 100)
    this.w_OPER = SUBSTR(DATIEL,2,2)
    * --- Inserisco nell'archivio
    if NOT this.w_ERROR
      * --- Try
      local bErr_0323D340
      bErr_0323D340=bTrsErr
      this.Try_0323D340()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_ERROR = .T.
      endif
      bTrsErr=bTrsErr or bErr_0323D340
      * --- End
    endif
    if this.w_ERROR AND EMPTY(this.oParentObject.w_MESS)
      this.oParentObject.w_MESS = AH_MSGFORMAT ("Errore nell'importazione dei dati! %1",message())
      this.oParentObject.w_SHOWERROR = .T.
    endif
    this.oParentObject.w_PATH = SUBSTR(this.oParentObject.w_PATH,1,RATC("\",this.oParentObject.w_PATH))
    if USED("CursErr")
      Select CursErr 
 Use
    endif
    if USED("CursTempo")
      SELECT CursTempo 
 USE
    endif
    if USED("CursCausaliRem")
      SELECT CursCausaliRem 
 USE
    endif
  endproc
  proc Try_0323D340()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT CursTEMPO 
 GO TOP
    SCAN FOR NOT EMPTY(SUBSTR(DATIEL,2,2))
    * --- Se il tracciato contiene molti record di testata allora rilegge il nome del supporto e la data di ricezione
    this.w_DATIEL = SUBSTR(DATIEL,1,200)
    this.w_NEWFLUSSO = SUBSTR(this.w_DATIEL,2,2)
    if this.w_NEWFLUSSO=this.w_OPER
      this.w_NOMSUP = SUBSTR(this.w_DATIEL,20,20)
      this.w_DTARIC = cp_CharToDate(SUBSTR(this.w_DATIEL,14,2)+"-"+SUBSTR(this.w_DATIEL,16,2)+"-"+SUBSTR(this.w_DATIEL,18,2))
      this.w_ULTIMOTRECORD = .F.
    endif
    this.w_NUMRIG = this.w_NUMRIG + 1
    do case
      case this.w_OPER="IB" OR this.w_OPER="IM" OR this.w_OPER="IR"
        if this.w_NUMRIG=1 OR SUBSTR(DATIEL,2,2)="EF" OR SUBSTR(DATIEL,2,2)="IB" OR SUBSTR(DATIEL,2,2)="IM" OR SUBSTR(DATIEL,2,2)="IR"
          this.w_TIPIMP = "F"
          * --- Controlli preliminari sul numero di record
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NUMPRO<>VAL(SUBSTR(this.w_DATIEL,4,7)) AND NOT(SUBSTR(this.w_DATIEL,2,2)$"EF/IB/IM/IR")
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT( "Il numero progressivo disposizione non corrisponde al record elaborato") ,this.w_TIPIMP)
        endif
        this.w_ERROREF = iif(this.w_ERRORE="S", this.w_ERRORE, this.w_ERROREF)
        * --- Insert into FLU_RITO
        i_nConn=i_TableProp[this.FLU_RITO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FLU_RITO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"FLNOMSUP"+",FLDTARIC"+",FLNOMFIL"+",CPROWNUM"+",FLFLGACC"+",FLDATIEL"+",FLERRORE"+",FLNUMDIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NOMSUP),'FLU_RITO','FLNOMSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DTARIC),'FLU_RITO','FLDTARIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOMFIL),'FLU_RITO','FLNOMFIL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'FLU_RITO','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(" "),'FLU_RITO','FLFLGACC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATIEL),'FLU_RITO','FLDATIEL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ERROREF),'FLU_RITO','FLERRORE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDIS),'FLU_RITO','FLNUMDIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'FLNOMSUP',this.w_NOMSUP,'FLDTARIC',this.w_DTARIC,'FLNOMFIL',this.w_NOMFIL,'CPROWNUM',this.w_NUMRIG,'FLFLGACC'," ",'FLDATIEL',this.w_DATIEL,'FLERRORE',this.w_ERROREF,'FLNUMDIS',this.w_NUMDIS)
          insert into (i_cTable) (FLNOMSUP,FLDTARIC,FLNOMFIL,CPROWNUM,FLFLGACC,FLDATIEL,FLERRORE,FLNUMDIS &i_ccchkf. );
             values (;
               this.w_NOMSUP;
               ,this.w_DTARIC;
               ,this.w_NOMFIL;
               ,this.w_NUMRIG;
               ," ";
               ,this.w_DATIEL;
               ,this.w_ERROREF;
               ,this.w_NUMDIS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      otherwise
        this.oParentObject.w_SHOWERROR = .T.
        this.oParentObject.w_MESS = AH_MSGFORMAT ("Errore: tipologia file non riconosciuta!")
        * --- Raise
        i_Error=AH_MSGFORMAT ("Tipologia non riconosciuta")
        return
    endcase
    if this.w_ENDFILE
      this.w_NUMRIG = 0
      this.w_NUMPRO = 0
      this.w_ENDFILE = .F.
      do case
        case this.w_OPER="IB" OR this.w_OPER="IM" OR this.w_OPER="IR"
          * --- Write into FLU_RITO
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.FLU_RITO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_RITO_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FLERRORE ="+cp_NullLink(cp_ToStrODBC(this.w_ERROREF),'FLU_RITO','FLERRORE');
                +i_ccchkf ;
            +" where ";
                +"FLNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
                +" and FLDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
                +" and FLNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
                   )
          else
            update (i_cTable) set;
                FLERRORE = this.w_ERROREF;
                &i_ccchkf. ;
             where;
                FLNOMSUP = this.w_NOMSUP;
                and FLDTARIC = this.w_DTARIC;
                and FLNOMFIL = this.w_NOMFIL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    this.oParentObject.w_MESS = "Importazione flusso operativo di ritorno terminata"
    SELECT CursErr
    if RECCOUNT()>0
      this.oParentObject.w_SHOWERROR = .T.
      this.oParentObject.w_MESS = AH_MSGFORMAT (this.oParentObject.w_MESS ) +" "+ AH_MSGFORMAT ("Attenzione si sono verificati degli errori in fase di importazione.Controllare la tabella di log.")
      this.w_NUMRIG = 0
      SELECT CursErr 
 GO TOP
      SCAN
      this.w_NUMRIG = this.w_NUMRIG + 1
      * --- Insert into LOG_REBA
      i_nConn=i_TableProp[this.LOG_REBA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_REBA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_REBA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ERNOMSUP"+",ERDTARIC"+",CPROWNUM"+",ERNUMRIG"+",ERDESERR"+",ERTIPIMP"+",ERNOMFIL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(NOMSUP),'LOG_REBA','ERNOMSUP');
        +","+cp_NullLink(cp_ToStrODBC(DTARIC),'LOG_REBA','ERDTARIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'LOG_REBA','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(NUMRIG),'LOG_REBA','ERNUMRIG');
        +","+cp_NullLink(cp_ToStrODBC(MSG),'LOG_REBA','ERDESERR');
        +","+cp_NullLink(cp_ToStrODBC(TIPIMP),'LOG_REBA','ERTIPIMP');
        +","+cp_NullLink(cp_ToStrODBC(NOMFIL),'LOG_REBA','ERNOMFIL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ERNOMSUP',NOMSUP,'ERDTARIC',DTARIC,'CPROWNUM',this.w_NUMRIG,'ERNUMRIG',NUMRIG,'ERDESERR',MSG,'ERTIPIMP',TIPIMP,'ERNOMFIL',NOMFIL)
        insert into (i_cTable) (ERNOMSUP,ERDTARIC,CPROWNUM,ERNUMRIG,ERDESERR,ERTIPIMP,ERNOMFIL &i_ccchkf. );
           values (;
             NOMSUP;
             ,DTARIC;
             ,this.w_NUMRIG;
             ,NUMRIG;
             ,MSG;
             ,TIPIMP;
             ,NOMFIL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore Insert Log'
        return
      endif
      * --- CursErrSta cursore contenente gli errori dei file di importazione gestito da gsrb_bfs
      Scatter memvar 
 insert into CursErrSta from memvar 
 Select CursErr
      ENDSCAN
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli preliminari
    if SUBSTR(this.w_DATIEL,2,2)<>"EF"
      SELECT CursTempo
      this.w_DATIEL = SUBSTR(DATIEL,1,200)
      this.w_NOMSUP = SUBSTR(this.w_DATIEL,20,20)
      this.w_DTARIC = cp_CharToDate(SUBSTR(this.w_DATIEL,14,2)+"-"+SUBSTR(this.w_DATIEL,16,2)+"-"+SUBSTR(this.w_DATIEL,18,2))
      this.w_NOMFIL = SUBSTR( LTRIM( SUBSTR(this.oParentObject.w_PATH, RAT("\",this.oParentObject.w_PATH)+1 ) ), 1, 100)
      this.w_OPER = SUBSTR(DATIEL,2,2)
      this.w_MANDATI = .F.
      this.w_NUMRIG = 1
      SELECT CursTempo
      this.w_NUMREC = RECNO()
      SELECT CursTempo 
 LOCATE FOR SUBSTR(DATIEL,2,2)="EF" REST
      * --- controllo esistenza recod di coda
      if FOUND()
        * --- controllo mittente/ricevente/data creazione/nome supporto
        if SUBSTR(this.w_DATIEL,4,5)<>SUBSTR(DATIEL,4,5)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ( "Il codice mittente iniziale <Posizione 4-8> non corrisponde al finale"),this.w_TIPIMP)
        endif
        this.w_ABIMIT = SUBSTR(this.w_DATIEL,4,5)
        if SUBSTR(this.w_DATIEL,9,5)<>SUBSTR(DATIEL,9,5)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ( "Il codice SIA della banca ricevente presente nel record di testa <Posizione 9-13> non corrisponde a quello del record di coda" ) ,this.w_TIPIMP)
        endif
        this.w_ABIRIC = SUBSTR(this.w_DATIEL,9,5)
        if SUBSTR(this.w_DATIEL,14,6)<>SUBSTR(DATIEL,14,6)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ("La data creazione iniziale <Posizione 14-19> non corrisponde alla finale"),this.w_TIPIMP)
        endif
        if SUBSTR(this.w_DATIEL,20,20)<>SUBSTR(DATIEL,20,20)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ("Il nome supporto iniziale <Posizione 20-39> non corrisponde al finale" ),this.w_TIPIMP)
        endif
      else
        this.w_ERRORE = "S"
        if this.w_OPER="AV"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ("Non esiste un record di coda con intestazione 'EF'"),this.w_TIPIMP)
        else
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,1, AH_MSGFORMAT ("Non esiste record di coda" ),this.w_TIPIMP)
        endif
      endif
      SELECT CursTempo 
 GO this.w_NUMREC
      SELECT CursTempo 
 LOCATE FOR SUBSTR(DATIEL,2,2)="10" AND this.w_OPER="IR" AND ( SUBSTR(DATIEL,29,5)="16000" OR SUBSTR(DATIEL,29,5)="57000")
      if FOUND()
        this.w_PROM_CONT = .T.
      else
        this.w_PROM_CONT = .F.
      endif
    else
      * --- controllo numero di records
      this.w_NUMREC = RECNO()
      if this.w_OPER<>"CN"
        if this.w_OPER="AV" AND VAL(SUBSTR(this.w_DATIEL,83,7))<>this.w_NUMRIG
          INSERT INTO __TMP__ (TITOLO,NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_TITOLO,this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il numero di records <Record EF - Posizione 83-89> non corrisponde al numero definito nel file" ),this.w_TIPIMP)
        else
          if VAL(SUBSTR(this.w_DATIEL,83,7))<>this.w_NUMRIG
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ( "Il numero di records <Posizione 83-89> non corrisponde al numero definito nel file" ),this.w_TIPIMP)
          endif
        endif
      endif
      this.w_ENDFILE = .T.
    endif
    SELECT CursTempo 
 GO this.w_NUMREC
    this.w_DATIEL = SUBSTR(DATIEL,1,200)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli records
    do case
      case (SUBSTR(this.w_DATIEL,2,2)="14" OR (SUBSTR(this.w_DATIEL,2,2)="10" AND this.w_OPER="IR" AND SUBSTR(this.w_DATIEL,29,5)<>"16000" AND SUBSTR(this.w_DATIEL,29,5)<>"57000") )
        this.w_NUMDIS = space(10)
        this.w_NUMPRO = this.w_NUMPRO + 1
        this.w_FIRRIG = this.w_NUMRIG
        if this.w_NUMPRO=1
          this.w_CODSIA = SUBSTR(this.w_DATIEL,92,5)
        endif
        if this.w_CODSIA<>SUBSTR(this.w_DATIEL,92,5)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il codice SIA <Posizione 92-96> � diverso da quello definito inizialmente (%1)",SUBSTR(this.w_DATIEL,92,5)),this.w_TIPIMP)
        endif
        if this.w_ABIMIT<>SUBSTR(this.w_DATIEL,70,5)
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ( "Il codice ABI della banca assuntrice <Posizione 70-74> � diverso da quello di testata (%1)",SUBSTR(this.w_DATIEL,70,5)),this.w_TIPIMP)
        endif
        * --- Riferimenti alla banca assuntrice
        this.w_CODABI = trim(SUBSTR(this.w_DATIEL,70,5))
        this.w_CODCAB = trim(SUBSTR(this.w_DATIEL,75,5))
        this.w_NUMCOR = right(replicate("0", 12)+alltrim(SUBSTR(this.w_DATIEL,80,12)), 12)
        this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,58,12)
        * --- --
        this.w_FRNUMREC = VAL(SUBSTR(this.w_DATIEL,4,7))
        this.w_NUMREC = VAL(SUBSTR(this.w_DATIEL,4,7))
        this.w_CAUABI = SUBSTR(this.w_DATIEL,29,5)
        Select CursCausaliRem
        LOCATE FOR CODCAU=this.w_CAUABI
        if NOT FOUND()
          this.w_CAUABI = space(5)
        endif
        this.w_CATIPMOV = space(3)
        this.w_TIPCON = "C"
        this.w_CODCON = SUBSTR(this.w_DATIEL,98,15)
        this.w_TOTIMP = VAL(SUBSTR(this.w_DATIEL,34,13))/100
        * --- Effettuo una lettura sulle causali di remote banking per verificare la tipologia
        *     movimento. Se distinta di richiamo, il codice cliente non � obbligatorio.
        * --- Read from CAU_REBA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAU_REBA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_REBA_idx,2],.t.,this.CAU_REBA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CATIPMOV"+;
            " from "+i_cTable+" CAU_REBA where ";
                +"CACODCAU = "+cp_ToStrODBC(this.w_CAUABI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CATIPMOV;
            from (i_cTable) where;
                CACODCAU = this.w_CAUABI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CATIPMOV = NVL(cp_ToDate(_read_.CATIPMOV),cp_NullValue(_read_.CATIPMOV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.w_OPER="IB" AND NOT (this.w_CAUABI="42010")
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ( "La causale record <Posizione 29-33> (%1) non corrisponde al tipo file o non � gestita",SUBSTR(this.w_DATIEL,29,5)),this.w_TIPIMP)
          case this.w_OPER="IM" AND NOT (this.w_CAUABI="07010")
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("La causale record <Posizione 29-33> (%1) non corrisponde al tipo file o non � gestita",SUBSTR(this.w_DATIEL,29,5)),this.w_TIPIMP)
          case this.w_OPER="IR" AND NOT (this.w_CAUABI $ "50001/50003/50004/50007/50009")
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("La causale record <Posizione 29-33> (%1) non corrisponde al tipo file o non � gestita",SUBSTR(this.w_DATIEL,29,5)),this.w_TIPIMP)
        endcase
        if this.w_OPER="IB" AND SUBSTR(this.w_DATIEL,97,1)<>"4"
          this.w_ERRORE = "S"
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il tipo codice <Posizione 97> � diverso da 4"),this.w_TIPIMP)
        endif
        if this.w_OPER="IB" OR this.w_OPER="IM" OR this.w_OPER="IR"
          this.w_TIPRID = SUBSTR(this.w_DATIEL,97,1)
          this.w_TIPCON = "C"
          if SUBSTR(this.w_DATIEL,2,2)="10" AND this.w_OPER="IR" AND this.w_TIPRID<>"4"
            * --- Si verifica se il flusso contiene alla posizione 98-113 il codice del cliente oppure il codice RID del cliente.
            *     Potrebbe contenere anche il codice del debitore/creditore oppure il codice RID associato al debitore/creditore diverso
            this.w_CODCON = SUBSTR(this.w_DATIEL,98,16)
            * --- Verifichiamo se il codice conto specificato nel tracciato della banca corrisponde
            *     all'identificativo RID presente nella tabella Conti, inoltre dobbiamo
            *     verificare che corrisponda il tipo identificativo 
            this.w_CODCON = SUBSTR(this.w_DATIEL,98,16)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODICE"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                    +" and ANIBARID = "+cp_ToStrODBC(this.w_CODCON);
                    +" and ANTIIDRI = "+cp_ToStrODBC(this.w_TIPRID);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODICE;
                from (i_cTable) where;
                    ANTIPCON = this.w_TIPCON;
                    and ANIBARID = this.w_CODCON;
                    and ANTIIDRI = this.w_TIPRID;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if EMPTY(this.w_CODCON)
              this.w_CODCON = SUBSTR(this.w_DATIEL,98,16)
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANIBARID = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANIBARID = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if EMPTY(this.w_CODCON)
                this.w_ERRORE = "S"
                INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il codice cliente <Posizione 98-113> non esiste (%1)",SUBSTR(this.w_DATIEL,98,16)),this.w_TIPIMP)
              else
                * --- Se � presente il codice conto, ma non � corretto il tipo identificativo, segnalo
                *     all'utente l'errore
                this.w_ERRORE = "S"
                INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il tipo codice <Posizione 97> non esiste (%1)",SUBSTR(this.w_DATIEL,97,1)),this.w_TIPIMP)
              endif
            endif
          else
            this.w_CODCON = SUBSTR(this.w_DATIEL,98,15)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODICE"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODICE;
                from (i_cTable) where;
                    ANTIPCON = this.w_TIPCON;
                    and ANCODICE = this.w_CODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Nel caso sia attivo il modulo di DocFinance il codice conto deve essere letto dalla 
            *     posizione 99 per 15 carattere dato che nella posizione 98 � presente
            *     il tipo conto
            if EMPTY(NVL(this.w_CODCON,space(15))) And g_ISDF="S"
              this.w_CODCON = SUBSTR(this.w_DATIEL,99,15)
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANCODICE;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCON = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if EMPTY(NVL(this.w_CODCON,space(15)))
              this.w_ERRORE = "S"
              INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il codice cliente  <Posizione 98-113> non esiste (%1)",SUBSTR(this.w_DATIEL,98,15)),this.w_TIPIMP)
            endif
          endif
        endif
        this.w_FRDATSCA = cp_CharToDate(SUBSTR(this.w_DATIEL,23,2)+"-"+SUBSTR(this.w_DATIEL,25,2)+"-"+SUBSTR(this.w_DATIEL,27,2))
      case (SUBSTR(this.w_DATIEL,2,2)="51" AND (this.w_OPER="IB" OR this.w_OPER="IM")) OR (this.w_OPER="IR" AND SUBSTR(this.w_DATIEL,2,2)="50" AND this.w_PROM_CONT)
        this.w_NUMEFF = IIF (this.w_MANDATI ,0,VAL(SUBSTR(this.w_DATIEL,11,10)) )
        if this.w_OPER="IB" OR this.w_OPER="IM"
          this.w_NUMEFF = IIF (this.w_MANDATI ,0,VAL(SUBSTR(this.w_DATIEL,11,10)) )
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if EMPTY(this.w_NUMDIS)
            * --- Read from DIS_TINT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DINUMDIS"+;
                " from "+i_cTable+" DIS_TINT where ";
                    +"DINSCFRB = "+cp_ToStrODBC(this.w_NOMSUP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DINUMDIS;
                from (i_cTable) where;
                    DINSCFRB = this.w_NOMSUP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.w_TOTSPE = this.w_TOTSPE + IIF(this.w_OPER="IR",VAL(SUBSTR(this.w_DATIEL,91,5))/100,VAL(SUBSTR(this.w_DATIEL,87,5))/100)
        this.w_DATCOM = cp_CharToDate(SUBSTR(this.w_DATIEL,92,2)+"-"+SUBSTR(this.w_DATIEL,94,2)+SUBSTR(this.w_DATIEL,96,2))
        if this.w_OPER="IR"
          this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,102,12)
          this.w_DATVAL = cp_CharToDate(SUBSTR(this.w_DATIEL,96,2)+"-"+SUBSTR(this.w_DATIEL,98,2)+"-"+SUBSTR(this.w_DATIEL,100,2))
        else
          this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,98,12)
          this.w_DATVAL = cp_CharToDate(SUBSTR(this.w_DATIEL,92,2)+"-"+SUBSTR(this.w_DATIEL,94,2)+"-"+SUBSTR(this.w_DATIEL,96,2))
        endif
      case SUBSTR(this.w_DATIEL,2,2)="70"
        if this.w_OPER="IR"
          if NOT this.w_RECSPE
            this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,102,12)
            this.w_NUMEFF = IIF (this.w_MANDATI,0,VAL(SUBSTR(this.w_DATIEL,11,15)))
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,98,12)
            if EMPTY(this.w_NUMDIS)
              * --- Read from DIS_TINT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DIS_TINT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DINUMDIS"+;
                  " from "+i_cTable+" DIS_TINT where ";
                      +"DINSCFRB = "+cp_ToStrODBC(this.w_NOMSUP);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DINUMDIS;
                  from (i_cTable) where;
                      DINSCFRB = this.w_NOMSUP;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.w_RECSPE = .F.
          endif
        else
          if this.w_RECSPE AND EMPTY(this.w_NUMDIS)
            * --- Read from DIS_TINT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DINUMDIS"+;
                " from "+i_cTable+" DIS_TINT where ";
                    +"DINSCFRB = "+cp_ToStrODBC(this.w_NOMSUP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DINUMDIS;
                from (i_cTable) where;
                    DINSCFRB = this.w_NOMSUP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        this.w_NUMRECZ = this.w_NUMRECZ+1
        this.w_LASRIG = this.w_NUMRIG
        * --- Inserisco i dati anche nella tabella per la gestione degli esiti
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANSAGINT"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANSAGINT;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCON;
                and ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SAGINT = NVL(cp_ToDate(_read_.ANSAGINT),cp_NullValue(_read_.ANSAGINT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CODAZI = I_CODAZI
        * --- Read from PAR_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PCADDINT"+;
            " from "+i_cTable+" PAR_CONT where ";
                +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PCADDINT;
            from (i_cTable) where;
                PCCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ADDINT = NVL(cp_ToDate(_read_.PCADDINT),cp_NullValue(_read_.PCADDINT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Insert into FLU_ESRI
        i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FLU_ESRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"FRNOMSUP"+",FRDTARIC"+",FRNOMFIL"+",FRTIPFLU"+",CPROWNUM"+",FRRIFDIS"+",FRTIPCON"+",FRCODCON"+",FRCODCAU"+",FRNUMEFF"+",FRNUMCOR"+",FRTOTIMP"+",FRSPETOT"+",FRDATSPE"+",FRFLGFRZ"+",FRFLGELA"+",FRFLGERR"+",FRFIRRIG"+",FRLASRIG"+",FRNUMREC"+",FRCFISDE"+",FRRIFPRO"+",FRDATSCA"+",FRRIGPRO"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NOMSUP),'FLU_ESRI','FRNOMSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DTARIC),'FLU_ESRI','FRDTARIC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOMFIL),'FLU_ESRI','FRNOMFIL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OPER),'FLU_ESRI','FRTIPFLU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRECZ),'FLU_ESRI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDIS),'FLU_ESRI','FRRIFDIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'FLU_ESRI','FRTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'FLU_ESRI','FRCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CAUABI),'FLU_ESRI','FRCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMEFF),'FLU_ESRI','FRNUMEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRNUMCOR),'FLU_ESRI','FRNUMCOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'FLU_ESRI','FRTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TOTSPE),'FLU_ESRI','FRSPETOT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'FLU_ESRI','FRDATSPE');
          +","+cp_NullLink(cp_ToStrODBC(" "),'FLU_ESRI','FRFLGFRZ');
          +","+cp_NullLink(cp_ToStrODBC(" "),'FLU_ESRI','FRFLGELA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ERRORE),'FLU_ESRI','FRFLGERR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FIRRIG),'FLU_ESRI','FRFIRRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LASRIG),'FLU_ESRI','FRLASRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRNUMREC),'FLU_ESRI','FRNUMREC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRCFISDE),'FLU_ESRI','FRCFISDE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRRIFPRO),'FLU_ESRI','FRRIFPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRDATSCA),'FLU_ESRI','FRDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FRRIGPRO),'FLU_ESRI','FRRIGPRO');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'FRNOMSUP',this.w_NOMSUP,'FRDTARIC',this.w_DTARIC,'FRNOMFIL',this.w_NOMFIL,'FRTIPFLU',this.w_OPER,'CPROWNUM',this.w_NUMRECZ,'FRRIFDIS',this.w_NUMDIS,'FRTIPCON',this.w_TIPCON,'FRCODCON',this.w_CODCON,'FRCODCAU',this.w_CAUABI,'FRNUMEFF',this.w_NUMEFF,'FRNUMCOR',this.w_FRNUMCOR,'FRTOTIMP',this.w_TOTIMP)
          insert into (i_cTable) (FRNOMSUP,FRDTARIC,FRNOMFIL,FRTIPFLU,CPROWNUM,FRRIFDIS,FRTIPCON,FRCODCON,FRCODCAU,FRNUMEFF,FRNUMCOR,FRTOTIMP,FRSPETOT,FRDATSPE,FRFLGFRZ,FRFLGELA,FRFLGERR,FRFIRRIG,FRLASRIG,FRNUMREC,FRCFISDE,FRRIFPRO,FRDATSCA,FRRIGPRO &i_ccchkf. );
             values (;
               this.w_NOMSUP;
               ,this.w_DTARIC;
               ,this.w_NOMFIL;
               ,this.w_OPER;
               ,this.w_NUMRECZ;
               ,this.w_NUMDIS;
               ,this.w_TIPCON;
               ,this.w_CODCON;
               ,this.w_CAUABI;
               ,this.w_NUMEFF;
               ,this.w_FRNUMCOR;
               ,this.w_TOTIMP;
               ,this.w_TOTSPE;
               ,this.w_DATVAL;
               ," ";
               ," ";
               ,this.w_ERRORE;
               ,this.w_FIRRIG;
               ,this.w_LASRIG;
               ,this.w_FRNUMREC;
               ,this.w_FRCFISDE;
               ,this.w_FRRIFPRO;
               ,this.w_FRDATSCA;
               ,this.w_FRRIGPRO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_NUMDIS = SPACE(10)
        this.w_NUMEFF = 0
        this.w_NOSELECT = .F.
        this.w_TOTSPE = 0
        this.w_DATVAL = cp_CharToDate("  -  -    ")
        this.w_ERRORE = " "
        this.w_ERROREF = iif(this.w_ERRORE="S", this.w_ERRORE, this.w_ERROREF)
        this.w_FRCFISDE = space(16)
        this.w_FRRIFPRO = space(12)
        this.w_FRDATSCA = cp_CharToDate("  -  -    ")
        this.w_FRRIGPRO = " "
      case SUBSTR(this.w_DATIEL,2,2)="10"
        this.w_NUMPRO = this.w_NUMPRO + 1
        this.w_CAUABI = SUBSTR(this.w_DATIEL,29,5)
        this.w_FRNUMREC = VAL(SUBSTR(this.w_DATIEL,4,7))
        this.w_FRRIFPRO = SUBSTR(this.w_DATIEL,58,12)
        this.w_DATVAL = cp_CharToDate(SUBSTR(this.w_DATIEL,17,2)+"-"+SUBSTR(this.w_DATIEL,19,2)+"-"+SUBSTR(this.w_DATIEL,21,2))
        this.w_FIRRIG = this.w_NUMRIG
        this.w_RECSPE = .T.
        this.w_TIPCON = space(1)
        this.w_CODCON = space(15)
        this.w_CODBEN = space(15)
        Select CursCausaliRem
        LOCATE FOR CODCAU=this.w_CAUABI
        if NOT FOUND()
          this.w_CAUABI = space(5)
        endif
        this.w_TOTIMP = 0
        this.w_TOTSPE = VAL(SUBSTR(this.w_DATIEL,34,13))/100
        this.w_FRRIGPRO = "S"
        do case
          case NOT (this.w_CAUABI $ "16000/37000/07000/57000")
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("La causale record <Posizione 29-33> (%1) non corrisponde al tipo file o non � gestita",SUBSTR(this.w_DATIEL,29,5)),this.w_TIPIMP)
        endcase
      case SUBSTR(this.w_DATIEL,2,2)="30"
        * --- da recuperare dal record 30
        this.w_FRCFISDE = SUBSTR(this.w_DATIEL,71,16)
      case SUBSTR(this.w_DATIEL,2,2)="17" AND this.w_OPER="IR"
        this.w_MANDATI = .T.
    endcase
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo informazioni effetto
    * --- Read from DIS_TINT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DINUMDIS,DIBANRIF"+;
        " from "+i_cTable+" DIS_TINT where ";
            +"DINSCFRB = "+cp_ToStrODBC(this.w_NOMSUP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DINUMDIS,DIBANRIF;
        from (i_cTable) where;
            DINSCFRB = this.w_NOMSUP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMDIS = NVL(cp_ToDate(_read_.DINUMDIS),cp_NullValue(_read_.DINUMDIS))
      this.w_FRNUMCOR = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not EMPTY(this.w_NUMDIS) and this.w_OPER="IR"
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_NUMDIS);
              +" and PTROWORD = "+cp_ToStrODBC(-2);
              +" and PTNUMEFF = "+cp_ToStrODBC(this.w_NUMEFF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              PTSERIAL = this.w_NUMDIS;
              and PTROWORD = -2;
              and PTNUMEFF = this.w_NUMEFF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Verifico se esistono distinte con lo stesso nome supporto
    if EMPTY(this.w_NUMDIS) OR this.w_CPROWNUM=0 and this.w_OPER="IR"
      * --- Verifico se esistono pi� partite in distinta associate allo stesso numero effetto, alla stessa
      *     tipologia di pagamento, allo stesso codice conto
      VQ_EXEC("QUERY\CONUMEFF.VQR",this,"CONUMEFF")
      Select Conumeff
      Go Top
      if Reccount("Conumeff")>0
        this.w_NUMREC = Reccount()
        if this.w_NUMREC>1
          if this.w_OPER="IR"
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Esistono pi� numeri effetto record 10 per data scadenza <Posizione 23-28> (%1) e importo <Posizione 34-46> (%2)",DTOC(this.w_FRDATSCA),ALLTRIM(STR(this.w_TOTIMP,15,2))),this.w_TIPIMP)
            Select Conumeff
            Go Top
          else
            LOCATE FOR this.w_FRDATSCA=ORDATA
            if NOT FOUND( )
              Select Conumeff
              Go Top
              this.w_ERRORE = "S"
              INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Esiste pi� volte lo stesso numero effetto <Posizione 11-20> tra quelli inviati dalla disposizione (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,10))),this.w_TIPIMP)
            endif
          endif
        else
          Select Conumeff
          Go Top
        endif
        this.w_PTDATSCA = PTDATSCA
        this.w_CPROWNUM = Conumeff.Cprownum
        this.w_NUMDIS = Conumeff.Ptserial
        this.w_NUMEFF = Conumeff.Ptnumeff
        this.w_FRNUMCOR = Nvl(Conumeff.DIBANRIF,"")
      else
        this.w_CPROWNUM = 0
      endif
      if USED("CONUMEFF")
        SELECT CONUMEFF 
 USE
      endif
    endif
    * --- Controllo dati partita
    if NOT (this.w_OPER="IR" AND VAL(SUBSTR(this.w_DATIEL,2,2))=50)
      VQ_EXEC("..\soll\exe\query\gsso_bfr.vqr",this,"ChkPartite")
      if USED("ChkPartite") AND (NOT this.w_MANDATI OR CPROWNUM>0)
        Select ChkPartite
        this.w_CPROWNUM = nvl(ChkPartite.Cprownum,0)
        this.w_PTDATSCA = ChkPartite.PTDATSCA
        this.w_ERR_CLI = .F.
        do case
          case NVL(this.w_CPROWNUM,0)=0 AND NOT this.w_MANDATI
            this.w_ERRORE = "S"
            if this.w_OPER="IR"
              if NOT this.w_MANDATI
                INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Non esiste il numero effetto <Posizione 11-25> tra quelli inviati dalla disposizione (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,15))),this.w_TIPIMP)
              endif
            else
              INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Non esiste il numero effetto <Posizione 11-20> tra quelli inviati dalla disposizione (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,10))),this.w_TIPIMP)
            endif
          case this.w_CODCON<>ChkPartite.PTCODCON
            this.w_ERRORE = "S"
            this.w_ERR_CLI = .T.
            * --- Controllo se la partita associata al numero effetto riguarda un debitore/creditore
            *     diverso
            if this.w_OPER="IR"
              INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il cliente/fornitore non corrisponde a quello indicato per questo numero effetto  <Posizione 11-25> (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,15))),this.w_TIPIMP)
            else
              INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Il cliente/fornitore non corrisponde a quello indicato per questo numero effetto  <Posizione 11-20> (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,10))),this.w_TIPIMP)
            endif
          case this.w_FRDATSCA<this.w_PTDATSCA
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP, this.w_DTARIC, this.w_NOMFIL, this.w_NUMRIG, AH_MSGFORMAT ("Non c'� corrispondenza tra la data scadenza del flusso <Posizione 23-28> (%1) e quella definita nella distinta %2", DTOC(this.w_FRDATSCA), DTOC(iif(empty(this.w_PTDATRAG), this.w_PTDATSCA, this.w_PTDATRAG))), this.w_TIPIMP)
          case this.w_TOTIMP<>ABS(ChkPartite.PTTOTIMP)
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP, this.w_DTARIC, this.w_NOMFIL, this.w_NUMRIG, AH_MSGFORMAT ("Non c'� corrispondenza tra l'importo dell'effetto del flusso (%1) e quello indicata nella distinta di origine (%2)", STR(this.w_TOTIMP,18,2), STR(ABS(ChkPartite.PTTOTIMP),18,2)), this.w_TIPIMP)
        endcase
        if NOT this.w_ERR_CLI
          this.w_TIPCON = ChkPartite.PTTIPCON
          this.w_CODCON = ChkPartite.PTCODCON
        endif
        Select ChkPartite 
 Use
      else
        this.w_CPROWNUM = 0
        this.w_ERRORE = "S"
        if this.w_OPER="IR"
          if NOT this.w_MANDATI
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Non esiste il numero effetto <Posizione 11-25> tra quelli inviati dalla disposizione (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,15))),this.w_TIPIMP)
          else
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Non esiste alcun effetto corrispondente a quello presente nel file "),this.w_TIPIMP)
          endif
        else
          INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP,this.w_DTARIC,this.w_NOMFIL,this.w_NUMRIG, AH_MSGFORMAT ("Non esiste il numero effetto <Posizione 11-20> tra quelli inviati dalla disposizione (%1)",ALLTRIM(SUBSTR(this.w_DATIEL,11,10))),this.w_TIPIMP)
        endif
      endif
    endif
    if not empty(this.w_FRNUMCOR)
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACODABI,BACODCAB,BACONCOR,BACONCOL"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_FRNUMCOR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACODABI,BACODCAB,BACONCOR,BACONCOL;
          from (i_cTable) where;
              BACODBAN = this.w_FRNUMCOR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_BACODABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
        this.w_BACODCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
        this.w_BACONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
        this.w_BACONCOL = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_BACODABI<>this.w_CODABI OR this.w_BACODCAB<>this.w_CODCAB 
        this.w_ERRORE = "S"
        INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP, this.w_DTARIC, this.w_NOMFIL, this.w_NUMRIG, AH_MSGFORMAT ("Il riferimento della banca assuntrice (%1 %2 %3) non corrisponde con quello della distinta (%4)", this.w_CODABI, this.w_CODCAB, this.w_NUMCOR, this.w_FRNUMCOR), this.w_TIPIMP)
        this.w_FRNUMCOR = space(15)
      else
        if (right(replicate("0", 12)+alltrim(this.w_BACONCOR),12)<>this.w_NUMCOR AND this.w_NUMCOR<>replicate("0", 12) AND right(replicate("0", 12)+alltrim(this.w_BACONCOR),12)<>replicate("0", 12)) 
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACONCOR"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_BACONCOL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACONCOR;
              from (i_cTable) where;
                  BACODBAN = this.w_BACONCOL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_BACONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if (right(replicate("0", 12)+alltrim(this.w_BACONCOR),12)<>this.w_NUMCOR AND this.w_NUMCOR<>replicate("0", 12) AND right(replicate("0", 12)+alltrim(this.w_BACONCOR),12)<>replicate("0", 12)) 
            this.w_ERRORE = "S"
            INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP, this.w_DTARIC, this.w_NOMFIL, this.w_NUMRIG, AH_MSGFORMAT ("Il riferimento della banca assuntrice (%1 %2 %3) non corrisponde con quello della distinta (%4)", this.w_CODABI, this.w_CODCAB, this.w_NUMCOR, this.w_FRNUMCOR), this.w_TIPIMP)
            this.w_FRNUMCOR = space(15)
          endif
        endif
      endif
    else
      this.w_ERRORE = "S"
      INSERT INTO CursErr (NOMSUP,DTARIC,NOMFIL,NUMRIG,MSG,TIPIMP) VALUES (this.w_NOMSUP, this.w_DTARIC, this.w_NOMFIL, this.w_NUMRIG, AH_MSGFORMAT ("Non � stata identificata la banca assuntrice"), this.w_TIPIMP)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='FLU_RITO'
    this.cWorkTables[2]='DIS_TINT'
    this.cWorkTables[3]='PAR_TITE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='LOG_REBA'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='CAU_REBA'
    this.cWorkTables[9]='FLU_ESRI'
    this.cWorkTables[10]='OFF_NOMI'
    this.cWorkTables[11]='PAR_CONT'
    return(this.OpenAllTables(11))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
