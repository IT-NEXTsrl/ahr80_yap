* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bin                                                        *
*              Contabilizzazione insoluti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_290]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-03                                                      *
* Last revis.: 2013-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bin",oParentObject)
return(i_retval)

define class tgsso_bin as StdBatch
  * --- Local variables
  w_oERRORLOG = .NULL.
  w_RAGCONT = 0
  w_NUMERO = 0
  w_COCOMPET = space(4)
  w_PTNUMDIS = space(10)
  w_PNCODVAL = space(3)
  w_IMPCLF = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_CODRAG = space(10)
  w_DIBANRIF = space(15)
  w_PNCAOVAL = 0
  w_IMPBAN = 0
  w_PTNUMPAR = space(31)
  w_PNTIPCLF = space(1)
  w_PNDATREG = ctod("  /  /  ")
  w_CAOVAR = 0
  w_PNCODESE = space(4)
  w_PNCODPAG = space(5)
  w_IMPDCA = 0
  w_MODPAG = space(10)
  w_PAGPAR = space(5)
  w_VALPAR = space(5)
  w_PTTIPCON = space(1)
  w_PNDATDOC = ctod("  /  /  ")
  w_PNCODUTE = 0
  w_IMPDCP = 0
  w_IMPSPE = 0
  w_PTCODCON = space(15)
  w_CODCON = space(15)
  w_CODBAN = space(15)
  w_PNNUMDOC = 0
  w_PNPRP = space(2)
  w_IMPBAN = 0
  w_PT_SEGNO = space(1)
  w_PNALFDOC = space(10)
  w_PNPRG = space(8)
  w_IMPCLF = 0
  w_PTTOTIMP = 0
  w_PNDESSUP = space(50)
  w_PTDESRIG = space(50)
  w_CAOVAC = 0
  w_IMPCOM = 0
  w_PTCODVAL = space(3)
  w_PNCODCAU = space(5)
  w_CAONAZ = 0
  w_IMPCOT = 0
  w_PTCAOVAL = 0
  w_PNIMPDAR = 0
  w_CODVAL = space(12)
  w_PTCAOAPE = 0
  w_PNIMPAVE = 0
  w_CAOVAL = 0
  w_CAOPAR = 0
  w_TRAG = 0
  w_PTMODPAG = space(10)
  w_PNNUMRER = 0
  w_PERVAL = space(3)
  w_TINS = 0
  w_PTIMPDOC = 0
  w_CPROWNUM = 0
  w_DATAPE = ctod("  /  /  ")
  w_NINS = 0
  w_PTNUMDOC = 0
  w_CPROWORD = 0
  w_DATCAM = ctod("  /  /  ")
  w_NRAG = 0
  w_PTALFDOC = space(10)
  w_PNNUMPRO = 0
  w_APPO = 0
  w_SQUAD = 0
  w_PTDATDOC = ctod("  /  /  ")
  w_PNTIPCON = space(1)
  w_APPO1 = 0
  w_UTCC = 0
  w_PTBANNOS = space(15)
  w_PNCODCON = space(15)
  w_EURVAL = 0
  w_UTDC = ctod("  /  /  ")
  w_PTBANAPP = space(10)
  w_PNVALNAZ = space(3)
  w_VAL1 = 0
  w_UTCV = 0
  w_PTFLCRSA = space(1)
  w_PNCOMPET = space(4)
  w_VAL2 = 0
  w_UTDV = ctod("  /  /  ")
  w_PTFLSOSP = space(1)
  w_PNSERIAL = space(10)
  w_PGEN = 0
  w_PTROWORD = 0
  w_PNFLPART = space(1)
  w_PARROW = 0
  w_MESS = space(10)
  w_PTROWNUM = 0
  w_PNTIPDOC = space(2)
  w_SALROW = 0
  w_CODAZI = space(5)
  w_PTNUMEFF = 0
  w_PNTIPREG = space(1)
  w_DATBLO = ctod("  /  /  ")
  w_PNNUMREG = 0
  w_STALIG = ctod("  /  /  ")
  w_PNALFPRO = space(10)
  w_TROV = .f.
  w_TIPCON = space(1)
  w_MRCODVOC = space(15)
  w_PNANNPRO = space(4)
  w_MRCODICE = space(15)
  w_PNFLSALI = space(1)
  w_FLSALI = space(1)
  w_MRPARAME = 0
  w_PNFLSALF = space(1)
  w_OKANAL = .f.
  w_FLSALF = space(1)
  w_MRTOTIMP = 0
  w_PNFLSALD = space(1)
  w_MR_SEGNO = space(1)
  w_PNINICOM = ctod("  /  /  ")
  w_CCFLANAL = space(1)
  w_PNFINCOM = ctod("  /  /  ")
  w_CCFLPART = space(1)
  w_CODESE = space(4)
  w_PNFLIVDF = space(1)
  w_TIPO = space(1)
  w_PERPVL = 0
  w_RTOT = 0
  w_TOTPAR = 0
  w_FLINDI = space(1)
  w_ROWCEN = 0
  w_SEZBIL = space(1)
  w_OFLINDI = space(1)
  w_OKANAL = .f.
  w_CONSUP = space(15)
  w_IMPCES = 0
  w_IMPTOT = 0
  w_CCTAGG = space(1)
  w_SPECLI = 0
  w_SPEPAR = 0
  w_DESSPE = space(1)
  w_OLDSER = space(10)
  w_COSERIAL = space(10)
  w_TIPEFF = space(1)
  w_NUMPRO = 0
  w_PNCODAGE = space(1)
  w_PNFLVABD = space(1)
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_FLERR = .f.
  w_NUMCOR = space(15)
  w_DATVAL = ctod("  /  /  ")
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_COSCOM = 0
  w_COMVAL = 0
  w_IMPCRE = 0
  w_IMPDEB = 0
  w_TIPCAU = space(1)
  w_CONBAN = space(15)
  w_CAUMOV = space(5)
  w_FLMOVC = space(1)
  w_FLCRDE = space(1)
  w_CALFES = space(3)
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_ROWNUM = 0
  w_BANNUM = 0
  w_BANORD = 0
  w_PTCODAGE = space(5)
  w_PTFLVABD = space(1)
  w_PTDATINT = ctod("  /  /  ")
  w_TESCAU = .f.
  w_DECTOT = 0
  w_BAVAL = space(3)
  w_SPECON = 0
  w_WSERIAL = space(10)
  w_MESS_ERR = space(200)
  w_PTDATREG = ctod("  /  /  ")
  w_PAGINS = space(5)
  w_FLINSO = space(1)
  w_PTNUMCOR = space(25)
  w_PNDESRIG = space(50)
  w_CCDESRIG = space(254)
  w_CODESCRI = space(50)
  w_CCDESSUP = space(254)
  w_PNNUMDOC = space(15)
  w_PNALFDOC = space(10)
  w_PNDATDOC = ctod("  /  /  ")
  w_COCODCON = space(15)
  w_COTIPCON = space(1)
  w_DFCONTIN = space(15)
  w_FLPDOC = space(1)
  w_PNANNDOC = space(4)
  * --- WorkFile variables
  AZIENDA_idx=0
  CAU_CONT_idx=0
  CCM_DETT_idx=0
  COC_MAST_idx=0
  CONTI_idx=0
  CONTROPA_idx=0
  CON_TENZ_idx=0
  ESERCIZI_idx=0
  PAG_2AME_idx=0
  PAR_CONT_idx=0
  PAR_TITE_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  CCC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Insoluti (da GSSO_KCO)
    * --- Controlli Preliminari - non occorre rimuovere i cursori in caso di fallito check perch� non li ho ancora creati
    do case
      case EMPTY(this.oParentObject.w_CONBANC)
        ah_ErrorMsg("Contropartita spese bancarie non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CAUINS)
        ah_ErrorMsg("Causale insoluto non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DIFCON)
        ah_ErrorMsg("Contropartita differenze di conversione non definita",,"")
        i_retcode = 'stop'
        return
    endcase
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Try
    local bErr_035FE880
    bErr_035FE880=bTrsErr
    this.Try_035FE880()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_errmsg,,"")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_035FE880
    * --- End
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_NRAG = 0
    this.w_NRAG = 0
    * --- Carica Gli Insoluti da Contabilizzare
    vq_exec("..\SOLL\EXE\QUERY\GSSO1QCO.VQR",this,"MOVIMAST")
    if RECCOUNT("MOVIMAST")>0
      * --- Inizia la Contabilizzazione
      * --- Elabora e Aggiorna Primanota
      ah_Msg("Inizio contabilizzazione insoluti...",.T.)
      this.w_NRAG = 0
      DIMENSION MES[1]
      SELECT MOVIMAST
      GO TOP
      SCAN FOR NOT EMPTY(NVL(COCODRAG,"")) 
      * --- Per ciascuna Riga, legge i dati di Testata
      this.w_RAGCONT = 0
      this.w_FLERR = .F.
      this.w_PNDATREG = CP_TODATE(CODATREG)
      this.w_CODESCRI = NVL(CODESCRI,"")
      * --- Se Ho pi� contenziosi nello stesso piano non valorizzo la data documento
      *     in testata di Primanota
      this.w_NUMERO = NVL(NUMERO,0)
      this.w_PNDATDOC = IIF(this.w_NUMERO>1,cp_CharToDate("    -  -  "),CP_TODATE(PTDATDOC))
      this.w_DATCAM = this.w_PNDATREG
      this.w_PNALFDOC = Space(10)
      this.w_PNCODCAU = this.oParentObject.w_CAUINS
      this.w_PNTIPDOC = "  "
      this.w_PNTIPREG = "N"
      this.w_PNNUMREG = 0
      this.w_PNFLIVDF = " "
      this.w_PNALFPRO = Space(10)
      this.w_CCFLANAL = " "
      this.w_PNTIPCLF = "N"
      this.w_PNCODVAL = NVL(COCODVAL, g_PERVAL)
      this.w_CAOVAC = NVL(COCAOVAL, 1)
      this.w_TIPEFF = Nvl ( COTIPEFF , "E" )
      this.w_CODRAG = COCODRAG
      this.w_PNDESRIG = NVL(CODESCRI,"")
      this.w_PNDESSUP = NVL(CODESCRI,"")
      this.w_PNNUMDOC = NVL(CONUMDOC,"")
      this.w_PNALFDOC = NVL(COALFDOC,"")
      this.w_PNDATDOC = CP_TODATE(CODATDOC)
      this.w_COCODCON = NVL(COCODCON,"")
      this.w_COTIPCON = NVL(COTIPCON,"")
      this.w_COCOMPET = Nvl ( COCOMPET , Space(4) )
      this.w_PNCAOVAL = this.w_CAOVAC
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      * --- Casale Contabile
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLANAL,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLIVDF,CCFLPART,CCFLPPRO,CCSERPRO,CCCONIVA,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLPDOC,CCDESRIG,CCDESSUP"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLANAL,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLIVDF,CCFLPART,CCFLPPRO,CCSERPRO,CCCONIVA,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLPDOC,CCDESRIG,CCDESSUP;
          from (i_cTable) where;
              CCCODICE = this.w_PNCODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCFLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
        this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
        this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
        this.w_PNFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
        this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
        this.oParentObject.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
        this.w_PNALFPRO = NVL(cp_ToDate(_read_.CCSERPRO),cp_NullValue(_read_.CCSERPRO))
        w_CCCONIVA = NVL(cp_ToDate(_read_.CCCONIVA),cp_NullValue(_read_.CCCONIVA))
        this.w_FLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
        this.w_FLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
        this.w_PNTIPCLF = NVL(cp_ToDate(_read_.CCFLRIFE),cp_NullValue(_read_.CCFLRIFE))
        this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
        this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
        this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PNANNPRO = CALPRO(this.w_PNDATREG, this.w_PNCODESE, this.oParentObject.w_FLPPRO)
      this.w_PNFLSALI = IIF(this.w_FLSALI="S", "+", " ")
      this.w_PNFLSALF = IIF(this.w_FLSALF="S", "+", " ")
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_CCFLANAL = IIF(g_PERCCR="S", this.w_CCFLANAL, " ")
      this.w_MESS = ah_Msgformat("%1 del %2", this.w_CODRAG, DTOC(CP_TODATE(this.w_PNDATREG)) )
      ah_Msg("Inizio contabilizzazione raggr. n. %1",.T.,.F.,.F., this.w_MESS )
      this.oParentObject.w_FLPPRO = "N"
      this.w_SQUAD = 0
      this.w_PNCODESE = this.w_CODESE
      * --- Legge dati collegati alla Causale Contabile
      this.w_FLSALI = " "
      this.w_FLSALF = " "
      this.w_PNVALNAZ = this.w_PERVAL
      this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_DATCAM, 0)
      this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
      this.w_PERPVL = g_PERPVL
      if this.w_PNDATREG<g_INIESE OR this.w_PNDATREG>g_FINESE
        * --- Se di un altro esercizio legge Codice e Divisa
        * --- Select from ESERCIZI
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE  from "+i_cTable+" ESERCIZI ";
              +" where ESINIESE<="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESFINESE>="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" ";
               ,"_Curs_ESERCIZI")
        else
          select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE from (i_cTable);
           where ESINIESE<=this.w_PNDATREG AND ESFINESE>=this.w_PNDATREG AND ESCODAZI=this.w_CODAZI ;
            into cursor _Curs_ESERCIZI
        endif
        if used('_Curs_ESERCIZI')
          select _Curs_ESERCIZI
          locate for 1=1
          do while not(eof())
          this.w_PNCODESE = ESCODESE
          this.w_PNVALNAZ = NVL(ESVALNAZ, this.w_PERVAL)
            select _Curs_ESERCIZI
            continue
          enddo
          use
        endif
        * --- Simbolo Valuta Nazionale e cambio Fisso EURO dell'esercizio Letto
        if this.w_PNVALNAZ<>this.w_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_PNVALNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERPVL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_DATCAM, 0)
          this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
        endif
      endif
       
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_COCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO,15)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_COTIPCON
      if Empty(this.w_PNDESSUP) and Not Empty(this.w_CCDESSUP)
        * --- Array elenco parametri per descrizioni di riga e testata
        this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
      endif
      this.w_PNCOMPET = this.w_PNCODESE
      do case
        case this.w_PNDATREG<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
          this.w_oERRORLOG.AddMsgLog("Raggr. n.: %1 del %2 inferiore a data ultima stampa L.G.", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG) )     
          this.w_FLERR = .T.
        case Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
          this.w_oERRORLOG.AddMsgLog("Raggr. n.: %1 del %2 %3", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG) , SUBSTR(CHKCONS("P",this.w_PNDATREG,"B","N"),44))     
          this.w_FLERR = .T.
        case this.w_PNCODESE<>G_CODESE
          this.w_oERRORLOG.AddMsgLog("Raggr. n.: %1 del %2 non di competenza dell'esercizio %3", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG) , g_CODESE)     
          this.w_FLERR = .T.
      endcase
      if Not this.w_FLERR
        this.w_OKANAL = .T.
        * --- Try
        local bErr_036A5A08
        bErr_036A5A08=bTrsErr
        this.Try_036A5A08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- La registrazione non � andata a buon fine cmq vado avanti ad esaminare il 
          *     cursore
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if i_ErrMsg<>"Registrazione annullata"
            this.w_oERRORLOG.AddMsgLog("Raggr. n.: %1 del %2", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG) )     
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(10), "Errore di generazione:%1 %2", Message(), Message(1))     
            this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
          endif
        endif
        bTrsErr=bTrsErr or bErr_036A5A08
        * --- End
      endif
      SELECT MOVIMAST
      * --- Aggiorna Totalizzatori Contabilizzato
      ENDSCAN
      this.w_MESS = "Operazione completata:%0N.%1 insoluti contabilizzati su %2 da contabilizzare%0%0%3%0Totale registrazioni di primanota generate %4%0%5"
      ah_ErrorMsg(this.w_MESS,,"", alltrim(str(this.w_NRAG)), ALLTRIM(STR(this.w_TRAG)), REPL("_",40), ALLTRIM(STR(this.w_PGEN)), REPL("_",40) )
    else
      this.w_MESS = "Per la selezione effettuata, non ci sono insoluti da contabilizzare"
      ah_ErrorMsg(this.w_MESS,,"")
    endif
    * --- Toglie <Blocco> per Primanota
    * --- Try
    local bErr_035FEF10
    bErr_035FEF10=bTrsErr
    this.Try_035FEF10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile rimuovere il blocco della prima nota. Rimuoverlo dai dati azienda",,"")
    endif
    bTrsErr=bTrsErr or bErr_035FEF10
    * --- End
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
    if used("MOVIMAST")
      select MOVIMAST
      use
    endif
    if used("MOVIDETT")
      select MOVIDETT
      use
    endif
    if USED("SPESE")
      SELECT SPESE
      USE
    endif
    if USED("PARTITE")
      SELECT PARTITE
      USE
    endif
  endproc
  proc Try_035FE880()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATBLO)
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.oParentObject.w_DATFIN;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_MESS = "Prima nota bloccata - verificare semaforo bollati in dati azienda"
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_036A5A08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cicla sui record Selezionati
    * --- begin transaction
    cp_BeginTrs()
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not this.w_FLERR
      if ( Not this.w_OKANAL ) OR this.w_SQUAD<>0 
        this.w_oERRORLOG.AddMsgLog("Verificare registrazione primanota: %1 del %2", STR(this.w_PNNUMRER,6,0), DTOC(this.w_PNDATDOC))     
        if this.w_OKANAL=.F.
          this.w_oERRORLOG.AddMsgLog("Collegamenti ai c.di costo mancanti")     
        endif
        if this.w_SQUAD<>0
          this.w_oERRORLOG.AddMsgLog("Errore di quadratura (%1)", ALLTRIM(STR(this.w_SQUAD)))     
        endif
        this.w_oERRORLOG.AddMsgLogNoTranslate(space(50))     
      endif
      if this.w_RAGCONT > 0
        this.w_PGEN = this.w_PGEN + 1
        this.w_NRAG = this.w_NRAG + this.w_RAGCONT
        * --- commit
        cp_EndTrs(.t.)
      else
        this.w_oERRORLOG.AddMsgLog("Registrazione incompleta, raggr. n.: %1 del %2 %0     Dettaglio non presente, verificare la data di registrazione nel contenzioso e l'esercizio in essere.", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG))     
        * --- Raise
        i_Error="Registrazione annullata"
        return
      endif
    else
      this.w_oERRORLOG.AddMsgLog("Raggr. n.: %1 del %2", this.w_CODRAG+"/"+this.w_COCOMPET, DTOC(this.w_PNDATREG) )     
      * --- Tipo errore bloccante
      this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(10)+MES[1])     
      this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
      * --- Raise
      i_Error="Registrazione annullata"
      return
    endif
    return
  proc Try_035FEF10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza variabili di Lavoro
    * --- Inizializza Valori
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNFLSALD = "+"
    this.w_PNCAOVAL = getcam(g_perval, i_datsys)
    this.w_OKANAL = .T.
    this.w_CCFLPART = " "
    this.w_CAONAZ = getcam(g_perval, i_datsys)
    this.oParentObject.w_FLPPRO = "N"
    this.w_PNTIPDOC = "  "
    this.w_PNANNPRO = SPACE(4)
    * --- Se .T. registrazione non confermata causa errore bloccante
    this.w_FLERR = .F.
    this.w_PNTIPREG = "N"
    this.w_PNCODESE = g_CODESE
    this.w_TINS = 0
    this.w_PNNUMREG = 0
    this.w_IMPDCA = 0
    this.w_NINS = 0
    this.w_PNALFPRO = Space(10)
    this.w_IMPDCP = 0
    this.w_SQUAD = 0
    this.w_TIPCON = "X"
    this.w_PGEN = 0
    this.w_CODCON = SPACE(15)
    this.w_OKANAL = .T.
    this.w_IMPCES = 0
    this.w_IMPBAN = 0
    this.w_TRAG = 0
    this.w_IMPCLF = 0
    this.w_SPEPAR = 0
    this.w_TRAG = 0
    this.w_APPO = 0
    this.w_SPECLI = 0
    this.w_TESCAU = .F.
    this.w_CODESE = g_CODESE
    this.w_CODAZI = i_CODAZI
    this.w_PERVAL = g_PERVAL
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    this.w_PERPVL = g_PERPVL
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNCODPAG = SPACE(5)
    this.w_BANNUM = 0
    this.w_BANORD = 0
    this.w_SPECON = 0
    this.w_MESS_ERR = SPACE(200)
    * --- Controllo che tutte le contropartite non siano obsolete alla data fine selezione
    this.w_TIPO = "G"
    if NOT EMPTY(this.w_CONBAN)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CONBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.w_CONBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartita spese bancarie obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CAUINS)
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDTOBSO,CCCAUMOV,CCFLMOVC,CCPAGINS,CCFLINSO"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAUINS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDTOBSO,CCCAUMOV,CCFLMOVC,CCPAGINS,CCFLINSO;
          from (i_cTable) where;
              CCCODICE = this.oParentObject.w_CAUINS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
        this.w_CAUMOV = NVL(cp_ToDate(_read_.CCCAUMOV),cp_NullValue(_read_.CCCAUMOV))
        this.w_FLMOVC = NVL(cp_ToDate(_read_.CCFLMOVC),cp_NullValue(_read_.CCFLMOVC))
        this.w_PAGINS = NVL(cp_ToDate(_read_.CCPAGINS),cp_NullValue(_read_.CCPAGINS))
        this.w_FLINSO = NVL(cp_ToDate(_read_.CCFLINSO),cp_NullValue(_read_.CCFLINSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Causale insoluto obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
      if this.w_FLMOVC="S" AND g_BANC="S"
        * --- Controlli Preliminari Sulla Causale dei Movimenti
        if EMPTY(this.w_CAUMOV)
          ah_ErrorMsg("Causale movimenti C\C non definita nella causale contabile",,"")
          i_retcode = 'stop'
          return
        else
          * --- Read from CCC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCC_MAST_idx,2],.t.,this.CCC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CATIPCON,CAFLCRDE"+;
              " from "+i_cTable+" CCC_MAST where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUMOV);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CATIPCON,CAFLCRDE;
              from (i_cTable) where;
                  CACODICE = this.w_CAUMOV;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPCAU = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
            this.w_FLCRDE = NVL(cp_ToDate(_read_.CAFLCRDE),cp_NullValue(_read_.CAFLCRDE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLCRDE="C"
            ah_ErrorMsg("La causale movimenti di C\C specificata nella causale contabile � a credito",,"")
            i_retcode = 'stop'
            return
          endif
        endif
      endif
    endif
    * --- Read from PAR_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCVALIMP,PCPAGMOR"+;
        " from "+i_cTable+" PAR_CONT where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCVALIMP,PCPAGMOR;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALPAR = NVL(cp_ToDate(_read_.PCVALIMP),cp_NullValue(_read_.PCVALIMP))
      this.w_PAGPAR = NVL(cp_ToDate(_read_.PCPAGMOR),cp_NullValue(_read_.PCPAGMOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PAGPAR = IIF(this.w_FLINSO="S" AND NOT EMPTY(NVL(this.w_PAGINS,"")), this.w_PAGINS, this.w_PAGPAR)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALPAR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALPAR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAOPAR = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili C\C
    * --- DocFinance
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola PNSERIAL e PNNUMRER
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_PNNUMPRO = 0
    this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
    this.w_PNPRP = "NN"
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    * --- Protocollo (se Gestito)
    if NOT EMPTY(this.w_PNANNPRO)
      cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNALFPRO"+",PNANNDOC"+",PNANNPRO"+",PNCAOVAL"+",PNCODCAU"+",PNCODESE"+",PNCODUTE"+",PNCODVAL"+",PNCOMIVA"+",PNCOMPET"+",PNDATREG"+",PNFLPROV"+",PNNUMPRO"+",PNNUMREG"+",PNNUMRER"+",PNPRD"+",PNPRG"+",PNPRP"+",PNRIFCES"+",PNSERIAL"+",PNTIPDOC"+",PNTIPREG"+",PNVALNAZ"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNTIPCLF"+",PNDESSUP"+",PNDATDOC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC("C"),'PNT_MAST','PNRIFCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNALFPRO',this.w_PNALFPRO,'PNANNDOC',this.w_PNANNDOC,'PNANNPRO',this.w_PNANNPRO,'PNCAOVAL',this.w_PNCAOVAL,'PNCODCAU',this.w_PNCODCAU,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNCODVAL',this.w_PNCODVAL,'PNCOMIVA',this.w_PNDATREG,'PNCOMPET',this.w_PNCOMPET,'PNDATREG',this.w_PNDATREG,'PNFLPROV',"N")
      insert into (i_cTable) (PNALFPRO,PNANNDOC,PNANNPRO,PNCAOVAL,PNCODCAU,PNCODESE,PNCODUTE,PNCODVAL,PNCOMIVA,PNCOMPET,PNDATREG,PNFLPROV,PNNUMPRO,PNNUMREG,PNNUMRER,PNPRD,PNPRG,PNPRP,PNRIFCES,PNSERIAL,PNTIPDOC,PNTIPREG,PNVALNAZ,UTCC,UTCV,UTDC,UTDV,PNTIPCLF,PNDESSUP,PNDATDOC &i_ccchkf. );
         values (;
           this.w_PNALFPRO;
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_PNCAOVAL;
           ,this.w_PNCODCAU;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNCODVAL;
           ,this.w_PNDATREG;
           ,this.w_PNCOMPET;
           ,this.w_PNDATREG;
           ,"N";
           ,this.w_PNNUMPRO;
           ,this.w_PNNUMREG;
           ,this.w_PNNUMRER;
           ,"NN";
           ,this.w_PNPRG;
           ,this.w_PNPRP;
           ,"C";
           ,this.w_PNSERIAL;
           ,this.w_PNTIPDOC;
           ,this.w_PNTIPREG;
           ,this.w_PNVALNAZ;
           ,this.w_UTCC;
           ,this.w_UTCV;
           ,this.w_UTDC;
           ,this.w_UTDV;
           ,this.w_PNTIPCLF;
           ,this.w_PNDESSUP;
           ,this.w_PNDATDOC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento PNT_MAST'
      return
    endif
    this.w_IMPCLF = 0
    this.w_APPO = 0
    this.w_EURVAL = -1
    this.w_PARROW = 0
    this.w_SALROW = 0
    * --- Scorre tutte le Partite Congruenti e Totalizza gli Importi
    vq_exec("..\SOLL\EXE\QUERY\GSSO_SPE.VQR",this,"SPESE")
    vq_exec("..\SOLL\EXE\QUERY\GSSO_QCO.VQR",this,"MOVIDETT")
    SELECT MOVIDETT
    GO TOP
    SCAN 
    * --- Aggiungo abbuoni se effetti non provenienti da distinte
    this.w_APPO = IIF(this.w_TIPEFF="A",NVL(TOTCLI,0)+NVL(TOTABB,0),NVL(TOTCLI,0))
    this.w_RAGCONT = this.w_RAGCONT + 1
    this.w_TRAG = this.w_TRAG + 1
    * --- Dettaglia Righe Cli/For
    if this.w_APPO<>0
      if this.w_PNCODVAL<>this.w_PNVALNAZ
        this.w_APPO = VAL2MON(this.w_APPO, this.w_PNCAOVAL, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
        this.w_APPO = cp_ROUND(this.w_APPO, this.w_PERPVL)
        SELECT MOVIDETT
      endif
      * --- Determino spese da associare a ogni riga al cliente
      this.w_CODCON = NVL(COCODCON,"")
      SELECT SPESE
      GO TOP
      SUM NVL(COIMPSPE, 0) FOR NVL(SPESE.COCODCON, " ")=this.w_CODCON AND NVL(CODESSPE," ")="S" TO this.w_SPECLI
      SELECT MOVIDETT
      if this.w_VALPAR<>this.w_PNVALNAZ
        this.w_APPO = VAL2MON(this.w_APPO, this.w_CAOPAR, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
        this.w_APPO = cp_ROUND(this.w_APPO, this.w_PERPVL) 
      endif
      if this.w_SPECLI<>0
        if this.w_VALPAR<>this.w_PNVALNAZ
          this.w_SPECLI = VAL2MON(this.w_SPECLI, this.w_CAOPAR, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
          this.w_SPECLI = cp_ROUND(this.w_SPECLI, this.w_PERPVL) 
        endif
        this.w_APPO = this.w_APPO+this.w_SPECLI
      endif
      this.w_PNTIPCON = "C" 
      this.w_PNCODCON = NVL(COCODCON,"")
      this.w_PNFLPART = "C"
      this.w_PNCODPAG = IIF(NOT EMPTY(this.w_PAGPAR),this.w_PAGPAR,NVL(CODPAG,""))
      this.w_PNCODAGE = NVL(PNCODAGE,SPACE(5))
      this.w_PNFLVABD = NVL(PNFLVABD," ")
      * --- Inserimento nel Dettaglio Primanota
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_APPO = 0
    SELECT MOVIDETT
    ENDSCAN
    this.w_PNFLPART = "N"
    this.w_PNTIPCON = "G"
    this.w_PNCODPAG = SPACE(5)
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    * --- Calcolo spese Bancarie dagli Insoluti
    this.w_IMPSPE = 0
    if RECCOUNT("SPESE")<>0
      SELECT SPESE
      GO TOP
      * --- Totalizza le Spese
      SUM NVL(COIMPSPE, 0) FOR NVL(CODESSPE," ")="C" TO this.w_IMPSPE
      if this.w_IMPSPE<>0
        * --- Converto le spese bancarie in valuta di conto
        if this.w_VALPAR<>this.w_PNVALNAZ
          this.w_IMPSPE = VAL2MON(this.w_IMPSPE, this.w_CAOPAR, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
          this.w_IMPSPE = cp_ROUND(this.w_IMPSPE, this.w_PERPVL)
        endif
        this.w_PNCODCON = this.oParentObject.w_CONBANC
        this.w_APPO = this.w_IMPSPE
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Righe Banche
    vq_exec("..\SOLL\EXE\QUERY\GSSO_BAN.VQR",this,"BANCHE")
    if RECCOUNT("BANCHE")<>0
      SELECT BANCHE
      GO TOP
      SCAN
      * --- Converto le spese bancarie in valuta di conto
      this.w_PNTIPCON = "G" 
      this.w_CODBAN = NVL(BANCHE,"") 
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACONCOL,BACALFES,BACODVAL"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_CODBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACONCOL,BACALFES,BACODVAL;
          from (i_cTable) where;
              BACODBAN = this.w_CODBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PNCODCON = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
        this.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
        this.w_BAVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_TIPEFF = "E"
      this.w_DFCONTIN = Space(15)
      if i_rows=0
        this.w_PNCODCON = this.w_CODBAN
        this.w_TIPEFF = "A"
      else
        * --- Controllo se � attivo il modulo DocFinance e la causale contabile
        *     non ha il flag "Movimenti di C/C"
        if g_ISDF="S" AND this.w_FLMOVC<>"S"
          GSDF_BDM (this, "CONTINS")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PNCODCON = IIF(EMPTY(this.w_DFCONTIN),this.w_PNCODCON,this.w_DFCONTIN)
        endif
      endif
      this.w_IMPCLF = IIF(this.w_TIPEFF="A",NVL(TOTBAN,0)+NVL(TOTABB,0),NVL(TOTBAN,0))
      if this.w_CODVAL<>this.w_PNVALNAZ
        this.w_IMPCLF = VAL2MON(this.w_IMPCLF, this.w_PNCAOVAL, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
        this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL) 
      endif
      this.w_APPO = 0
      * --- Determino spese da associare a ogni riga della banca 
      SELECT SPESE
      GO TOP
      SUM NVL(COIMPSPE, 0) FOR NVL(COCODBAN, " ")=this.w_CODBAN TO this.w_APPO
      if this.w_APPO<>0
        if this.w_VALPAR<>this.w_PNVALNAZ
          this.w_APPO = VAL2MON(this.w_APPO, this.w_CAOPAR, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
          this.w_APPO = cp_ROUND(this.w_APPO, this.w_PERPVL) 
        endif
        this.w_IMPCLF = this.w_IMPCLF + this.w_APPO
      endif
      SELECT BANCHE
      this.w_APPO = -this.w_IMPCLF
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if g_BANC="S" AND this.w_FLMOVC="S" AND this.w_TIPEFF="E"
        vq_exec("..\SOLL\EXE\QUERY\GSSO1BAN.VQR",this,"CONTIC")
        SELECT CONTIC
        GO TOP
        SCAN
        this.w_DATVAL = NVL(CODATVAL,cp_CharToDate("  -  -    "))
        this.w_CONBAN = this.w_CODBAN
        * --- Select from gscg_bmc
        do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
        if used('_Curs_gscg_bmc')
          select _Curs_gscg_bmc
          locate for 1=1
          do while not(eof())
          this.w_TIPCAU = NVL(_Curs_gscg_bmc.CATIPCON," ")
          this.w_FLCRDE = NVL(_Curs_gscg_bmc.CAFLCRDE," ")
          this.w_TESCAU = .T.
            select _Curs_gscg_bmc
            continue
          enddo
          use
        endif
        if this.w_TESCAU
          if this.w_PNCODVAL<>this.w_BAVAL
            if Not this.w_FLERR
              MES[1] = Ah_msgformat("Valuta conto corrente %1 non congruente alla valuta di registrazione dell'insoluto", ALLTRIM(this.w_CONBAN) )
              this.w_FLERR = .T.
            endif
          else
            this.w_SPECON = CONTIC.COIMPSPE
            if this.w_VALPAR<>this.w_PNVALNAZ
              this.w_SPECON = VAL2MON(this.w_SPECON, this.w_CAOPAR, this.w_CAONAZ, this.w_PNDATREG, this.w_PNVALNAZ)
              this.w_SPECON = cp_ROUND(this.w_SPECON, this.w_PERPVL) 
            endif
            this.w_DATVAL = IIF(NOT EMPTY(this.w_DATVAL),this.w_DATVAL,this.w_PTDATSCA)
            this.w_COSCOM = this.w_SPECON
            this.w_COMVAL = cp_ROUND(this.w_COSCOM*this.w_PNCAOVAL, this.w_DECTOT)
            this.w_IMPCLF = NVL(CONTIC.TOTBAN,0) +this.w_SPECON
            this.w_APPO = -this.w_IMPCLF
            this.w_IMPDEB = (IIF(this.w_APPO<0, ABS(this.w_APPO), 0)) - this.w_COMVAL
            this.w_IMPCRE = IIF(this.w_APPO>0, this.w_APPO+ABS(this.w_COMVAL), 0)
            this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
            this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
            this.w_CCFLCOMM = "+"
            this.w_ROWNUM = this.w_CPROWNUM
            this.w_BANNUM = this.w_BANNUM+1
            this.w_BANORD = this.w_BANORD+10
            * --- Insert into CCM_DETT
            i_nConn=i_TableProp[this.CCM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+",CCCOSCOM"+",CCCOMVAL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CCM_DETT','CCSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CCM_DETT','CCROWRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMOV),'CCM_DETT','CCCODCAU');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CONBAN),'CCM_DETT','CCNUMCOR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_BANNUM),'CCM_DETT','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_BANORD),'CCM_DETT','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_COSCOM),'CCM_DETT','CCCOSCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_COMVAL),'CCM_DETT','CCCOMVAL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_PNSERIAL,'CCROWRIF',this.w_ROWNUM,'CCCODCAU',this.w_CAUMOV,'CCNUMCOR',this.w_CONBAN,'CCDATVAL',this.w_DATVAL,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CPROWNUM',this.w_BANNUM,'CPROWORD',this.w_BANORD,'CCFLDEBI',this.w_CCFLDEBI,'CCFLCRED',this.w_CCFLCRED,'CCFLCOMM',this.w_CCFLCOMM)
              insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM,CCCOSCOM,CCCOMVAL &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_CAUMOV;
                   ,this.w_CONBAN;
                   ,this.w_DATVAL;
                   ,this.w_IMPCRE;
                   ,this.w_IMPDEB;
                   ,this.w_BANNUM;
                   ,this.w_BANORD;
                   ,this.w_CCFLDEBI;
                   ,this.w_CCFLCRED;
                   ,this.w_CCFLCOMM;
                   ,this.w_COSCOM;
                   ,this.w_COMVAL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Aggiorno Saldi C\C
            * --- Write into COC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.COC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"BASALCRE =BASALCRE+ "+cp_ToStrODBC(this.w_IMPCRE);
              +",BASALDEB =BASALDEB+ "+cp_ToStrODBC(this.w_PNIMPAVE);
                  +i_ccchkf ;
              +" where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                     )
            else
              update (i_cTable) set;
                  BASALCRE = BASALCRE + this.w_IMPCRE;
                  ,BASALDEB = BASALDEB + this.w_PNIMPAVE;
                  &i_ccchkf. ;
               where;
                  BACODBAN = this.w_CONBAN;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          if Not this.w_FLERR
            MES[1] = Ah_msgformat("Codice C\C: %1 non congruente alla causale movimenti C\C: %2 indicata nella causale contabile", ALLTRIM(this.w_CONBAN) , ALLTRIM(this.w_CAUMOV))
            this.w_FLERR = .T.
          endif
        endif
        SELECT CONTIC
        ENDSCAN
      endif
      SELECT BANCHE
      ENDSCAN
    endif
    * --- Eventuali Differenze di Conversione
    this.w_SQUAD = cp_ROUND(this.w_SQUAD, this.w_PERPVL)
    if this.w_SQUAD<>0 AND ((this.w_PNVALNAZ=g_perval AND ABS(this.w_SQUAD)<1) OR (this.w_PNVALNAZ=g_CODLIR AND ABS(this.w_SQUAD)<100))
      this.w_PNCODCON = this.oParentObject.w_DIFCON
      this.w_APPO = -this.w_SQUAD
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Aggiorna Riferimenti Contabilizzazione
    * --- Select from GSSOCQCO
    do vq_exec with 'GSSOCQCO',this,'_Curs_GSSOCQCO','',.f.,.t.
    if used('_Curs_GSSOCQCO')
      select _Curs_GSSOCQCO
      locate for 1=1
      do while not(eof())
      this.w_WSERIAL = _Curs_GSSOCQCO.COSERIAL
      * --- Write into CON_TENZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CORIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CON_TENZ','CORIFCON');
            +i_ccchkf ;
        +" where ";
            +"COCODRAG = "+cp_ToStrODBC(this.w_CODRAG);
            +" and CO__TIPO = "+cp_ToStrODBC("I");
            +" and COSERIAL = "+cp_ToStrODBC(this.w_WSERIAL);
               )
      else
        update (i_cTable) set;
            CORIFCON = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            COCODRAG = this.w_CODRAG;
            and CO__TIPO = "I";
            and COSERIAL = this.w_WSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSSOCQCO
        continue
      enddo
      use
    endif
    if USED("BANCHE")
      SELECT BANCHE
      USE
    endif
    if USED("CONTIC")
      SELECT CONTIC
      USE
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Dettaglio Primanota e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
    this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNFLSALI"+",PNCODPAG"+",PNCODAGE"+",PNFLVABD"+",PNFLZERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PNT_DETT','PNCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PNT_DETT','PNFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNFLSALD',this.w_PNFLSALD,'PNFLSALF'," ")
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNDESRIG,PNCAURIG,PNFLSALD,PNFLSALF,PNINICOM,PNFINCOM,PNFLSALI,PNCODPAG,PNCODAGE,PNFLVABD,PNFLZERO &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,this.w_PNFLSALD;
           ," ";
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ," ";
           ,this.w_PNCODPAG;
           ,this.w_PNCODAGE;
           ,this.w_PNFLVABD;
           ," ";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento PNT_DETT'
      return
    endif
    this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
    * --- Aggiorna Saldo
    * --- Try
    local bErr_03820238
    bErr_03820238=bTrsErr
    this.Try_03820238()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03820238
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
      +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          ,SLDARINI = &i_cOp3.;
          ,SLAVEINI = &i_cOp4.;
          ,SLDARFIN = &i_cOp5.;
          ,SLAVEFIN = &i_cOp6.;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Gestione Analitica
    if this.w_CCFLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S"
      this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPO)
      if Not EMPTY(this.w_MESS_ERR)
        ah_ErrorMsg(this.w_MESS_ERR,,"")
      endif
    endif
    * --- Inserimento delle Partite
    if this.w_PNTIPCON="C"
      * --- Salva Il Puntatore alla Riga P.N. per aggiornare alla Fine il Totale di Riga
      this.w_SALROW = this.w_CPROWNUM
      this.w_PTTIPCON = "C"
      this.w_PTCODCON = this.w_PNCODCON
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if Not EMPTY(this.w_CCDESRIG) and Empty(this.w_PNDESRIG)
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PTNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PTALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PTDATDOC) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_PTCODCON) 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PTCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(this.w_PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_PTDATSCA) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_PTTIPCON
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PNDESRIG = this.w_PNDESRIG;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_03820238()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive le Partite associate alla Reg. di Primanota
    * --- Partite da Insoluto (ATTENZIONE: Siamo Posizionati dentro il Cursore MOVIDETT)
    * --- Dati per Chusura Partite
    * --- Determino Il tipo di pagamento da inserire nelle partite ricavato dal codice pagamento dei parametri
    * --- Read from PAG_2AME
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAG_2AME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "P2MODPAG"+;
        " from "+i_cTable+" PAG_2AME where ";
            +"P2CODICE = "+cp_ToStrODBC(this.w_PAGPAR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        P2MODPAG;
        from (i_cTable) where;
            P2CODICE = this.w_PAGPAR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MODPAG = NVL(cp_ToDate(_read_.P2MODPAG),cp_NullValue(_read_.P2MODPAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    vq_exec("..\SOLL\EXE\QUERY\GSSO2QCO.VQR",this,"PARTITE")
    SELECT PARTITE
    GO TOP
    SCAN 
    this.w_PARROW = this.w_PARROW + 1
    this.w_PTNUMDOC = NVL(PTNUMDOC,0)
    this.w_PTROWORD = this.w_SALROW
    this.w_PTROWNUM = this.w_PARROW
    this.w_PTDATSCA = CP_TODATE(PTDATSCA)
    this.w_PTNUMPAR = PTNUMPAR
    this.w_PT_SEGNO = IIF(Nvl(PT_SEGNO," ")="D","A","D")
    * --- Aggiungo eventuali abbuoni per effetti non provenienti da distinte
    this.w_PTTOTIMP = IIF(this.w_TIPEFF="A",abs(NVL(PTTOTIMP,0)+NVL(PTTOTABB,0)),abs(NVL(PTTOTIMP,0)))
    this.w_PTDATDOC = CP_TODATE(PTDATDOC)
    this.w_PTCODVAL = PTCODVAL
    this.w_PTCAOVAL = this.w_PNCAOVAL
    this.w_PTCAOAPE = NVL(PTCAOAPE,0)
    this.w_PTMODPAG = NVL(this.w_MODPAG,SPACE(10))
    this.w_PTBANNOS = NVL(PTBANNOS,SPACE(15))
    this.w_PTBANAPP = NVL(PTBANAPP,SPACE(10))
    this.w_PTNUMEFF = 0
    this.w_PTNUMDIS = space(10)
    this.w_PTFLCRSA = "C"
    this.w_PTFLSOSP = " "
    this.w_PTALFDOC = NVL(PTALFDOC,SPACE(10))
    this.w_PTDESRIG = NVL(PTDESRIG, " ")
    this.w_PTCODAGE = NVL(PTCODAGE,SPACE(5))
    this.w_OSERIAL = NVL(PTSERRIF,SPACE(10))
    this.w_OROWORD = NVL(PTORDRIF,0)
    this.w_OROWNUM = NVL(PTNUMRIF,0)
    this.w_PTFLVABD = NVL(PTFLVABD, " ")
    this.w_PTDATINT = CP_TODATE(CODATINT)
    this.w_TIPEFF = Nvl ( COTIPEFF , "E" )
    this.w_PTDATREG = this.w_PNDATREG
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANNUMCOR"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_PTCODCON);
            +" and ANCODBAN = "+cp_ToStrODBC(this.w_PTBANAPP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANNUMCOR;
        from (i_cTable) where;
            ANTIPCON = this.w_PTTIPCON;
            and ANCODICE = this.w_PTCODCON;
            and ANCODBAN = this.w_PTBANAPP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PTNUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from gsso3qco
    do vq_exec with 'gsso3qco',this,'_Curs_gsso3qco','',.f.,.t.
    if used('_Curs_gsso3qco')
      select _Curs_gsso3qco
      locate for 1=1
      do while not(eof())
      if NOT EMPTY(NVL(_Curs_gsso3qco.PTDESRIG,""))
        this.w_PTDESRIG = _Curs_gsso3qco.PTDESRIG
      endif
        select _Curs_gsso3qco
        continue
      enddo
      use
    endif
    * --- Scrive la Partita di Primanota
    SELECT PARTITE
    this.w_NUMPRO = CERCPRO(this.w_PNDATREG,this.w_PTDATSCA,this.w_PTCODCON,this.w_PTTIPCON,this.w_PTCODVAL,this.w_PTNUMPAR) +1
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTTIPCON"+",PTCODCON"+",PTFLSOSP"+",PTDATAPE"+",PTBANNOS"+",PTFLRAGG"+",PTBANAPP"+",PTNUMEFF"+",PTFLIMPE"+",PTNUMDIS"+",PTFLINDI"+",PTNUMDOC"+",PTDATDOC"+",PTALFDOC"+",PTDESRIG"+",PTNUMPRO"+",PTCAOAPE"+",PTCODAGE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLVABD"+",PTDATINT"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'PAR_TITE','PTNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATINT),'PAR_TITE','PTDATINT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATREG),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_PTROWNUM,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTFLCRSA',this.w_PTFLCRSA,'PTTIPCON',this.w_PTTIPCON)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTTIPCON,PTCODCON,PTFLSOSP,PTDATAPE,PTBANNOS,PTFLRAGG,PTBANAPP,PTNUMEFF,PTFLIMPE,PTNUMDIS,PTFLINDI,PTNUMDOC,PTDATDOC,PTALFDOC,PTDESRIG,PTNUMPRO,PTCAOAPE,PTCODAGE,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,PTDATINT,PTDATREG,PTNUMCOR &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PTROWORD;
           ,this.w_PTROWNUM;
           ,this.w_PTDATSCA;
           ,this.w_PTNUMPAR;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTFLCRSA;
           ,this.w_PTTIPCON;
           ,this.w_PTCODCON;
           ,this.w_PTFLSOSP;
           ,this.w_PNDATREG;
           ,this.w_PTBANNOS;
           ," ";
           ,this.w_PTBANAPP;
           ,this.w_PTNUMEFF;
           ,"  ";
           ,this.w_PTNUMDIS;
           ," ";
           ,this.w_PTNUMDOC;
           ,this.w_PTDATDOC;
           ,this.w_PTALFDOC;
           ,this.w_PTDESRIG;
           ,this.w_NUMPRO;
           ,this.w_PTCAOAPE;
           ,this.w_PTCODAGE;
           ,this.w_OSERIAL;
           ,this.w_OROWORD;
           ,this.w_OROWNUM;
           ,this.w_PTFLVABD;
           ,this.w_PTDATINT;
           ,this.w_PTDATREG;
           ,this.w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento PAR_TITE'
      return
    endif
    this.w_DESSPE = NVL(CODESSPE,"C")
    this.w_COSERIAL = NVL(COSERIAL,SPACE(10))
    if this.w_DESSPE="S" AND this.w_OLDSER<>this.w_COSERIAL AND NOT EMPTY(this.w_COSERIAL)
      * --- Inserisce Riga relative alle spese se addebito spese su conto
      * --- Read from CON_TENZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COIMPSPE"+;
          " from "+i_cTable+" CON_TENZ where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COIMPSPE;
          from (i_cTable) where;
              COSERIAL = this.w_COSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SPEPAR = NVL(cp_ToDate(_read_.COIMPSPE),cp_NullValue(_read_.COIMPSPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_VALPAR<>this.w_PTCODVAL
        this.w_SPEPAR = VAL2MON(this.w_SPEPAR, this.w_CAOPAR, this.w_PTCAOVAL, this.w_PNDATREG, this.w_PTCODVAL)
        this.w_SPEPAR = cp_ROUND(this.w_SPEPAR, this.w_PERPVL) 
      endif
      if this.w_SPEPAR<>0
        this.w_PT_SEGNO = "D"
        this.w_PARROW = this.w_PARROW + 1
        this.w_PTROWNUM = this.w_PARROW
        * --- Insert into PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTTIPCON"+",PTCODCON"+",PTFLSOSP"+",PTDATAPE"+",PTBANNOS"+",PTFLRAGG"+",PTBANAPP"+",PTNUMEFF"+",PTFLIMPE"+",PTNUMDIS"+",PTFLINDI"+",PTNUMDOC"+",PTDATDOC"+",PTALFDOC"+",PTDESRIG"+",PTNUMPRO"+",PTCAOAPE"+",PTCODAGE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLVABD"+",PTDATINT"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SPEPAR),'PAR_TITE','PTTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
          +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'PAR_TITE','PTNUMPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATINT),'PAR_TITE','PTDATINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATREG),'PAR_TITE','PTDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_PTROWNUM,'PTDATSCA',this.w_PNDATREG,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_SPEPAR,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTFLCRSA',this.w_PTFLCRSA,'PTTIPCON',this.w_PTTIPCON)
          insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTTIPCON,PTCODCON,PTFLSOSP,PTDATAPE,PTBANNOS,PTFLRAGG,PTBANAPP,PTNUMEFF,PTFLIMPE,PTNUMDIS,PTFLINDI,PTNUMDOC,PTDATDOC,PTALFDOC,PTDESRIG,PTNUMPRO,PTCAOAPE,PTCODAGE,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,PTDATINT,PTDATREG,PTNUMCOR &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_PTROWORD;
               ,this.w_PTROWNUM;
               ,this.w_PNDATREG;
               ,this.w_PTNUMPAR;
               ,this.w_PT_SEGNO;
               ,this.w_SPEPAR;
               ,this.w_PTCODVAL;
               ,this.w_PTCAOVAL;
               ,this.w_PTMODPAG;
               ,this.w_PTFLCRSA;
               ,this.w_PTTIPCON;
               ,this.w_PTCODCON;
               ,this.w_PTFLSOSP;
               ,this.w_PNDATREG;
               ,this.w_PTBANNOS;
               ," ";
               ,this.w_PTBANAPP;
               ,this.w_PTNUMEFF;
               ,"  ";
               ,this.w_PTNUMDIS;
               ," ";
               ,this.w_PTNUMDOC;
               ,this.w_PTDATDOC;
               ,this.w_PTALFDOC;
               ,this.w_PTDESRIG;
               ,this.w_NUMPRO;
               ,this.w_PTCAOAPE;
               ,this.w_PTCODAGE;
               ,this.w_OSERIAL;
               ,this.w_OROWORD;
               ,this.w_OROWNUM;
               ,this.w_PTFLVABD;
               ,this.w_PTDATINT;
               ,this.w_PTDATREG;
               ,this.w_PTNUMCOR;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in inserimento PAR_TITE spese'
          return
        endif
      endif
    endif
    this.w_OLDSER = this.w_COSERIAL
    SELECT PARTITE
    ENDSCAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CCM_DETT'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='CONTROPA'
    this.cWorkTables[7]='CON_TENZ'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='PAG_2AME'
    this.cWorkTables[10]='PAR_CONT'
    this.cWorkTables[11]='PAR_TITE'
    this.cWorkTables[12]='PNT_DETT'
    this.cWorkTables[13]='PNT_MAST'
    this.cWorkTables[14]='SALDICON'
    this.cWorkTables[15]='VALUTE'
    this.cWorkTables[16]='CCC_MAST'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    if used('_Curs_GSSOCQCO')
      use in _Curs_GSSOCQCO
    endif
    if used('_Curs_gsso3qco')
      use in _Curs_gsso3qco
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
