* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso1bfs                                                        *
*              Funzionalita di gsso_kfs.mskdef                                 *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-13                                                      *
* Last revis.: 2012-08-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ARG1
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso1bfs",oParentObject,m.w_ARG1)
return(i_retval)

define class tgsso1bfs as StdBatch
  * --- Local variables
  w_POSI = 0
  w_PATH = space(200)
  w_DATIEL = space(200)
  w_FILE = space(200)
  w_OPER = space(2)
  w_NOMSUP = space(20)
  w_DTARIC = ctod("  /  /  ")
  w_ARG1 = space(20)
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocata dalla routine GSSO_BFS e dalla maschera GSSO_KFR per :
    *     - Riempire lo zoom della maschera GSSO_KFR
    *     - Per selezionare e deselezionare lo zoom della maschera GSSO_KFR  
    *     
    if this.w_ARG1=="CALC"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_ARG1=="SELE"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempie lo zoom della maschera GSSO_KFR  con i dati conteni nel cursore NOMIFILE (ossia i file della directori scelta dall utente)
    *     Vengono inseriti nello zoom solo i file che non sono mai stati importati (ossia non importati all interno delle tabelle  FLU_RITO REN_DICO CON_FRIC ) 
    this.w_ZOOM = this.oParentObject.oparentobject.w_ZoomFile
    NC = this.w_ZOOM.cCursor
    ZAP IN ( this.w_ZOOM.cCursor )
    * --- Pulisce il cursore della maschera GSSO_KFR prima di inserirvi nuovi dati  
    delete from (NC) where 1=1
    * --- Per ogni file presente nella directori selezionata dall utente si verifica che non sia gi� presente all interno del database 
    *     e in caso affermativo allora gli estremi del file vengono ricopiati all interno della maschera GSSO_KFR
    SELECT CursFILE 
 GO TOP 
 SCAN
    if NOT (".$$$" $ CursFILE.DATIEL)
      INSERT INTO (NC) Values (CursFILE.DATIEL, CursFILE.SIZE, CursFILE.MODIFICATO, CursFILE.TIME, "",CursFILE.DIR,"", CursFILE.XCHK)
    endif
    ENDSCAN
    if USED ("CursCARICO")
      SELECT CursCARICO 
 USE
    endif
    SELECT (NC)
    if reccount()>0
      go 1
    endif
    this.w_zoom.grd.refresh()
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona e deseleziona tutti gli elementi dello zoom della  masc hera GSSO_KFR
    this.w_ZOOM = this.oParentObject.w_ZoomFile
    NC = this.w_ZOOM.cCursor
    SELECT (NC)
    this.w_POSI = RECNO()
    if this.oParentObject.w_SELEZI="S"
      UPDATE &NC SET XCHK = 1
    else
      UPDATE &NC SET XCHK = 0
    endif
    if this.w_POSI>0 AND i_rows>0
      go this.w_POSI
    endif
    this.w_zoom.grd.refresh()
  endproc


  proc Init(oParentObject,w_ARG1)
    this.w_ARG1=w_ARG1
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ARG1"
endproc
