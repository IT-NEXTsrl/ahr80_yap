* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bz2                                                        *
*              Esegue stampa solleciti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_338]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-05-19                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PpARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bz2",oParentObject,m.PpARAM)
return(i_retval)

define class tgsso_bz2 as StdBatch
  * --- Local variables
  PpARAM = space(4)
  w_oERRORLOG = .NULL.
  w_TMPC = space(10)
  w_LPIETES = space(0)
  w_LTESTES = space(0)
  w_LOGGTES = space(50)
  w_ZOOM = space(10)
  w_TOTIMP = 0
  w_DATSCA = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_SPEPRE = 0
  w_IMPSPE = 0
  w_TOTSPE = 0
  w_INTERESS = 0
  w_CONT = 0
  w_DATINC = ctod("  /  /  ")
  w_INCASSI = 0
  w_PERINT = 0
  w_ROWNUM = 0
  w_FLGINT = space(1)
  w_IMPINT = 0
  w_LIVEL = 0
  w_DDCODE = space(15)
  w_LIVCON = 0
  w_DATREG = ctod("  /  /  ")
  w_NUMTES = space(6)
  w_TESCON = space(6)
  w_NOTTES = space(10)
  w_TIPO = space(1)
  w_OCODICE = space(15)
  w_SPEBAN = 0
  w_COSERIAL = space(10)
  w_CODATREG = ctod("  /  /  ")
  w_CO__TIPO = space(1)
  w_APPO = 0
  w_DATMAT = ctod("  /  /  ")
  w_SAGINT = 0
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_INTBCE = .f.
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ANNO = space(4)
  w_PERIOD = space(1)
  w_SAGRIF = 0
  w_MAGNDP = 0
  w_MAGDEP = 0
  w_DATINIMO = ctod("  /  /  ")
  w_DATFINMO = ctod("  /  /  ")
  w_GIORNI = 0
  w_TASSO = 0
  w_IMPORTO = 0
  w_FLVABD = space(1)
  w_INCAVV = .f.
  w_COCODCON = space(15)
  w_COCODVAL = space(3)
  w_CONUMLIV = space(3)
  w_COOGGTES = space(15)
  w_COTESTES = space(0)
  w_COPIETES = space(0)
  w_TOTPAR = 0
  w_CODAZI = space(5)
  w_LCODLIN = space(3)
  w_CODLIN = space(3)
  w_COCODLIN = space(3)
  w_CONUMREG = 0
  w_COCOMPET = space(4)
  w_NKO = 0
  w_NOK = 0
  w_NUMREC = 0
  w_ANSPRINT = space(1)
  w_ANSAGINT = 0
  w_CONTA = 0
  w_CODCON = space(15)
  w_TELFAX = space(18)
  w_ANDESCRI = space(40)
  w_MAIL = .f.
  * --- WorkFile variables
  CON_TENZ_idx=0
  DET_SOLL_idx=0
  TES_CONT_idx=0
  CONDTENZ_idx=0
  PAR_TITE_idx=0
  CONTI_idx=0
  INT_MORA_idx=0
  PAR_CONT_idx=0
  INC_AVVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la  Stampa dei Solleciti (da GSSO_SSS)
    * --- S:  Stampa Solleciti GSSO_SSS
    *     R: Ristampa Sollecit GSSO_MDS
    this.w_INTBCE = .F.
    this.w_INCAVV = .F.
    this.w_NKO = 0
    if this.PpARAM="S"
      this.w_ZOOM = this.oParentObject.w_ZoomInso
      NC = this.w_Zoom.cCursor
      * --- Per Ulteriore controllo
      this.oParentObject.w_FLEURO = IIF(NOT (this.oParentObject.w_CODVAL<>g_PERVAL AND (EMPTY(this.oParentObject.w_CODVAL) OR this.oParentObject.w_FILCAO<>0)), " ", this.oParentObject.w_FLEURO)
      if EMPTY(NVL(this.oParentObject.w_NUMERO," ")) AND this.oParentObject.w_FLGAUTO<>"S"
        ah_ErrorMsg("Testo da stampare non definito",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_OREP) 
        ah_ErrorMsg("Report di stampa non definito",,"")
        i_retcode = 'stop'
        return
      endif
      this.w_CONT = 0
      this.w_TESCON = SPACE(6)
      if this.oParentObject.w_FLGAUTO="S"
        do while (this.w_CONT<>100 AND EMPTY(this.w_TESCON))
          * --- Read from TES_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TES_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TES_CONT_idx,2],.t.,this.TES_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TCNUMERO"+;
              " from "+i_cTable+" TES_CONT where ";
                  +"TCNUMLIV = "+cp_ToStrODBC(this.w_CONT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TCNUMERO;
              from (i_cTable) where;
                  TCNUMLIV = this.w_CONT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TESCON = NVL(cp_ToDate(_read_.TCNUMERO),cp_NullValue(_read_.TCNUMERO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CONT = this.w_CONT+1
        enddo
        if EMPTY(this.w_TESCON)
          ah_ErrorMsg("Attenzione nessun testo caricato",,"")
          i_retcode = 'stop'
          return
        endif
      endif
      SELECT &NC
      GO TOP
      * --- Verifica se ci sono record Selezionati
      this.w_NUMREC = RECNO()
      COUNT FOR XCHK=1 TO this.w_CONTA
      if this.w_CONTA=0
        ah_ErrorMsg("Non ci sono contenziosi selezionati",,"")
        i_retcode = 'stop'
        return
      endif
      * --- Se seleziono un solo cliente determino i dati per invio Fax/Mail
      Select Distinct COCODCON From &NC Where Xchk=1 Into Cursor _Codcon_
      if RecCount( "_Codcon_" )= 1
        this.w_MAIL = .T.
         
 Select _CodCon_ 
 Go top
        this.w_CODCON = _CodCon_.COCODCON
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AN_EMAIL,ANDESCRI,AN_EMPEC"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC("C");
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AN_EMAIL,ANDESCRI,AN_EMPEC;
            from (i_cTable) where;
                ANTIPCON = "C";
                and ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
          this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          this.oParentObject.w_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
         
 Use in _CodCon_
        * --- Acquisisco gli indirizzi email associati al cliente
        GSAR_BRM(this,"E_M","C",this.w_CODCON,"SO","V")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
        i_CLIFORDES = "C" 
 i_CODDES = this.w_CODCON 
 i_TIPDES = "SO" 
 I_DEST=ALLTRIM(this.w_ANDESCRI)
        i_WEDEST = "C" + this.w_CODCON
      else
        i_EMAIL ="" 
 i_EMAIL_PEC ="" 
 i_CLIFORDES ="" 
 i_CODDES ="" 
 i_TIPDES = "" 
 I_DEST=""
        i_WEDEST = ""
        this.oParentObject.w_EMAIL = ""
        this.oParentObject.w_EMPEC = ""
         
 Select _CodCon_ 
 Use in _CodCon_
      endif
      * --- Predispone il Corsore di Stampa
       SELECT * FROM &NC WHERE XCHK=1 INTO CURSOR STAMPA ORDER BY COCODCON, COCODVAL, COSERIAL
      * --- Variabili da Passare al Report
      this.w_LPIETES = this.oParentObject.w_PIETES
      this.w_LTESTES = this.oParentObject.w_TESTES
      this.w_LOGGTES = this.oParentObject.w_OGGTES
      this.w_LCODLIN = this.oParentObject.w_LINGUA
      this.w_IMPSPE = 0
      CREATE CURSOR DATEST (CO__TIPO C(1), COSERIAL C(10), TESTES M(10), PIETES M(10), OGGTES C(50), NOTTES M(10))
    else
      this.w_CODAZI = I_CODAZI
      * --- CREO IL CURSORE DA PASSARE AL REPORT
      Vq_Exec("..\SOLL\EXE\Query\GSSO_QRS.vqr",This,"STAMPA")
      * --- Read from PAR_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCSPETEL,PCSPEPEC"+;
          " from "+i_cTable+" PAR_CONT where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCSPELET,PCSPERAC,PCSPERAR,PCSPEFAX,PCSPEMAI,PCSPETEL,PCSPEPEC;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_SPELET = NVL(cp_ToDate(_read_.PCSPELET),cp_NullValue(_read_.PCSPELET))
        this.oParentObject.w_SPERAC = NVL(cp_ToDate(_read_.PCSPERAC),cp_NullValue(_read_.PCSPERAC))
        this.oParentObject.w_SPERAR = NVL(cp_ToDate(_read_.PCSPERAR),cp_NullValue(_read_.PCSPERAR))
        this.oParentObject.w_SPEFAX = NVL(cp_ToDate(_read_.PCSPEFAX),cp_NullValue(_read_.PCSPEFAX))
        this.oParentObject.w_SPEMAI = NVL(cp_ToDate(_read_.PCSPEMAI),cp_NullValue(_read_.PCSPEMAI))
        this.oParentObject.w_SPETEL = NVL(cp_ToDate(_read_.PCSPETEL),cp_NullValue(_read_.PCSPETEL))
        this.oParentObject.w_SPEPEC = NVL(cp_ToDate(_read_.PCSPEPEC),cp_NullValue(_read_.PCSPEPEC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MEZSPE = this.oParentObject.w_DSMEZSPE
      this.oParentObject.w_DATINV = this.oParentObject.w_DSDATINV
      * --- Read from TES_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TES_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TES_CONT_idx,2],.t.,this.TES_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TCOGGTES,TCTESDOC,TCPIEDOC"+;
          " from "+i_cTable+" TES_CONT where ";
              +"TCNUMERO = "+cp_ToStrODBC(this.oParentObject.w_DSRIFTES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TCOGGTES,TCTESDOC,TCPIEDOC;
          from (i_cTable) where;
              TCNUMERO = this.oParentObject.w_DSRIFTES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LOGGTES = NVL(cp_ToDate(_read_.TCOGGTES),cp_NullValue(_read_.TCOGGTES))
        this.w_LTESTES = NVL(cp_ToDate(_read_.TCTESDOC),cp_NullValue(_read_.TCTESDOC))
        this.w_LPIETES = NVL(cp_ToDate(_read_.TCPIEDOC),cp_NullValue(_read_.TCPIEDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case this.oParentObject.w_MEZSPE="RA"
        this.w_IMPSPE = this.oParentObject.w_SPERAC
      case this.oParentObject.w_MEZSPE="LE"
        this.w_IMPSPE = this.oParentObject.w_SPELET
      case this.oParentObject.w_MEZSPE="AR"
        this.w_IMPSPE = this.oParentObject.w_SPERAR
      case this.oParentObject.w_MEZSPE="FA"
        this.w_IMPSPE = this.oParentObject.w_SPEFAX
      case this.oParentObject.w_MEZSPE="EM"
        this.w_IMPSPE = this.oParentObject.w_SPEMAI
      case this.oParentObject.w_MEZSPE="TE"
        this.w_IMPSPE = this.oParentObject.w_SPETEL
      case this.oParentObject.w_MEZSPE="PE"
        this.w_IMPSPE = this.oParentObject.w_SPEPEC
    endcase
    L_DATINV = this.oParentObject.w_DATINV
    L_MEZSPE = this.oParentObject.w_MEZSPE
    Select STAMPA
    WRCURSOR("STAMPA")
    Select STAMPA
    Go Top
    Scan
    this.w_COSERIAL = STAMPA.COSERIAL
    this.w_CO__TIPO = STAMPA.CO__TIPO
    this.w_TOTIMP = NVL(STAMPA.COTOTIMP, 0)
    this.w_FLGINT = NVL(STAMPA.FLGINT, " ")
    this.w_LCODLIN = NVL(STAMPA.ANCODLIN,SPACE(3))
    this.w_COCOMPET = NVL(STAMPA.COCOMPET,"")
    this.w_ANSPRINT = NVL(STAMPA.ANSPRINT,"N")
    this.w_ANSAGINT = NVL(STAMPA.ANSAGINT,0)
    * --- Calcolo Saggio di Interesse
    do case
      case this.w_FLGINT="S"
        * --- Flag Addebita Interessi: Saggio Concordato
        this.w_PERINT = STAMPA.COPERINT
      case this.w_FLGINT="M"
        * --- Flag Addebita Interessi: Saggio di Mora
        this.w_INTBCE = .T.
        this.w_PERINT = IIF(this.w_ANSPRINT="S",this.w_ANSAGINT,0)
      otherwise
        * --- Flag Addebita Interessi: Non Applicati
        this.w_PERINT = 0
    endcase
    * --- Valutazione Flag Beni Deperibili: Beni Deperibili se almeno una partita ha il flag attivo
    this.w_FLVABD = " "
    Vq_Exec("..\SOLL\EXE\Query\GSSO_FBD.vqr",This,"BENIDEP")
    this.w_FLVABD = NVL(BENIDEP.PTFLVABD, " ")
    if Used("BENIDEP")
      Select BENIDEP
      Use
    endif
    this.oParentObject.w_DECTOT = STAMPA.VADECTOT
    this.w_CODATREG = CP_TODATE(STAMPA.CODATREG)
    this.w_DATREG = CP_TODATE(STAMPA.CODATREG)
    this.w_SPEPRE = 0
    this.w_SPEBAN = 0
    * --- Calcolo Spese di Comunicazione Precedenti 
    Vq_Exec("..\SOLL\EXE\Query\GSSO_SSO.vqr",This,"SPESE")
    Select SPESE
    GO TOP
    this.w_SPEPRE = SPESE.DSIMPSPE
    SELECT STAMPA
    * --- Converto le spese nella valuta del contenzioso
    this.w_TOTSPE = this.w_IMPSPE
    if STAMPA.COCODVAL<>this.oParentObject.w_VALPAR
      this.w_SPEBAN = STAMPA.SPEBAN
      this.w_TOTSPE = VAL2VAL(this.w_IMPSPE,STAMPA.COCAOVAL,STAMPA.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),this.oParentObject.w_DECTOT)
      this.w_SPEBAN = VAL2VAL(this.w_SPEBAN,STAMPA.COCAOVAL,STAMPA.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),this.oParentObject.w_DECTOT)
      REPLACE STAMPA.SPEBAN WITH this.w_SPEBAN
    endif
    REPLACE STAMPA.SPEPRE WITH this.w_SPEPRE
    REPLACE STAMPA.COIMPSPE WITH this.w_TOTSPE
    this.w_DDCODE = STAMPA.COCODCON
    if Used("SPESE")
      Select SPESE
      Use
    endif
    * --- Calcolo Data Maturazione Interessi (campo obbligatorio, dovrebbe essere sempre valorizzata, altrimenti viene recuperata la Data Scadenza dalla partita)
    * --- Read from CON_TENZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TENZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CODATINT"+;
        " from "+i_cTable+" CON_TENZ where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CODATINT;
        from (i_cTable) where;
            COSERIAL = this.w_COSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATMAT = NVL(cp_ToDate(_read_.CODATINT),cp_NullValue(_read_.CODATINT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATMAT)
      * --- Read from CONDTENZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONDTENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2],.t.,this.CONDTENZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COSERRIF,COORDRIF,CONUMRIF"+;
          " from "+i_cTable+" CONDTENZ where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COSERRIF,COORDRIF,CONUMRIF;
          from (i_cTable) where;
              COSERIAL = this.w_COSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERRIF = NVL(cp_ToDate(_read_.COSERRIF),cp_NullValue(_read_.COSERRIF))
        this.w_ORDRIF = NVL(cp_ToDate(_read_.COORDRIF),cp_NullValue(_read_.COORDRIF))
        this.w_NUMRIF = NVL(cp_ToDate(_read_.CONUMRIF),cp_NullValue(_read_.CONUMRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_SERRIF) AND NOT EMPTY(this.w_ORDRIF) AND NOT EMPTY(this.w_NUMRIF)
        * --- Read from PAR_TITE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTDATSCA"+;
            " from "+i_cTable+" PAR_TITE where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_ORDRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTDATSCA;
            from (i_cTable) where;
                PTSERIAL = this.w_SERRIF;
                and PTROWORD = this.w_ORDRIF;
                and CPROWNUM = this.w_NUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATMAT = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if this.Pparam="S"
      this.w_INTERESS = 0
      this.w_INCASSI = 0
      Vq_Exec("..\SOLL\EXE\Query\GSSO1SSO.vqr",This,"INCASSI")
      if RECCOUNT("INCASSI") >0
        this.w_INCAVV = .T.
        SELECT INCASSI
        GO TOP
        SCAN
        this.w_INCASSI = 0
        this.w_DATINC = NVL(CP_TODATE(INCASSI.IADATINC), cp_CharToDate("  -  -  "))
        this.w_INCASSI = NVL(INCASSI.IAIMPINC, 0)
        if this.w_INTBCE
          * --- Se l'interesse � dato dalla Banca centrale Europea, il calcolo avviene semestre per semestre
          this.w_DATINI = this.w_DATMAT
          this.w_DATFIN = this.w_DATINC
          this.w_IMPORTO = this.w_INCASSI
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_INTERESS = this.w_INTERESS + cp_ROUND((((this.w_INCASSI) * (this.w_PERINT*(this.w_DATINC-this.w_DATMAT)))/36500),this.oParentObject.w_DECTOT)
        endif
        this.w_TOTIMP = this.w_TOTIMP-this.w_INCASSI
        SELECT INCASSI
        ENDSCAN
      endif
      SELECT STAMPA
      this.w_CONUMREG = NVL(STAMPA.CONUMREG,"")
      this.w_TOTIMP = iif((nvl(this.w_TOTIMP,0)=0 AND (NOT this.w_INCAVV)) ,NVL(STAMPA.COTOTIMP, 0),this.w_TOTIMP)
      this.w_DATREG = IIF(NOT EMPTY(this.w_DATREG),this.w_DATREG,this.w_CODATREG)
      if this.w_INTBCE
        * --- Se l'interesse � dato dalla Banca centrale Europea, il calcolo avviene semestre per semestre
        this.w_DATINI = this.w_DATMAT
        this.w_DATFIN = this.oParentObject.w_DATINV
        this.w_IMPORTO = this.w_TOTIMP
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_INTERESS = this.w_INTERESS + cp_ROUND((((this.w_TOTIMP) * (this.w_PERINT*(this.oParentObject.w_DATINV-this.w_DATMAT)))/36500),this.oParentObject.w_DECTOT)
      endif
      REPLACE STAMPA.INTERESS WITH this.w_INTERESS
      REPLACE STAMPA.COTOTIMP WITH this.w_TOTIMP
      * --- Seleziono il relativo Testo se attivata l'opzione automatica.
      if this.oParentObject.w_FLGAUTO="S"
        this.w_LIVCON = NVL(STAMPA.CONUMLIV,0)
        this.w_TIPO = NVL(STAMPA.CO__TIPO," ")
        this.w_LIVEL = 0
        Vq_Exec("..\SOLL\EXE\Query\GSSO2SSS.vqr",This,"TEMP")
        if NVL(temp.TCNUMLIV,0) >0
          this.w_LIVEL = TEMP.TCNUMLIV
          this.w_NUMTES = TEMP.TCNUMERO
          this.w_LTESTES = TEMP.TCTESDOC
          this.w_LPIETES = TEMP.TCPIEDOC
          this.w_LOGGTES = TEMP.TCOGGTES
          this.w_CODLIN = TEMP.TCCODLIN
        else
          * --- assegno a livel-1 per escludere i contenziosi con testo non determinabile
          this.w_LIVEL = -1
          this.w_NKO = this.w_NKO + 1
          * --- Crea Cursore errori
          if this.w_NKO=1
            * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
            this.w_oERRORLOG=createobject("AH_ErrorLog")
          endif
          if this.oParentObject.w_FLGAUTO="S"
            this.w_TMPC = "Contenzioso (%1) num. %2 del %3 - cliente: %4 - Contenzioso non stampato (testo non determinabile)"
          else
            this.w_TMPC = "Contenzioso (%1) num. %2 del %3 - cliente: %4 - Contenzioso non stampato (livello testo non corretto)"
          endif
          this.w_oERRORLOG.AddMsgLog(this.w_TMPC,this.w_CO__TIPO,str(this.w_CONUMREG,6,0)+"/"+this.w_COCOMPET,dtoc(this.w_CODATREG),this.w_DDCODE)     
        endif
        select STAMPA
        Replace STAMPA.CONUMLIV With this.w_LIVEL, STAMPA.NUMTES With this.w_NUMTES
      endif
      this.w_NOTTES = ""
      * --- Read from CON_TENZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CO__NOTE"+;
          " from "+i_cTable+" CON_TENZ where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CO__NOTE;
          from (i_cTable) where;
              COSERIAL = this.w_COSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOTTES = NVL(cp_ToDate(_read_.CO__NOTE),cp_NullValue(_read_.CO__NOTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      INSERT INTO DATEST (CO__TIPO, COSERIAL, TESTES, PIETES, OGGTES, NOTTES) ;
      VALUES (this.w_CO__TIPO, this.w_COSERIAL, this.w_LTESTES, this.w_LPIETES, this.w_LOGGTES, this.w_NOTTES)
    else
      * --- Sommo il Totale incassi leggendo dalla movimentazione INC_AVVE e filtrando per Data Invio
      * --- Select from INC_AVVE
      i_nConn=i_TableProp[this.INC_AVVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2],.t.,this.INC_AVVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" INC_AVVE ";
            +" where IASERIAL="+cp_ToStrODBC(this.oParentObject.w_DSSERIAL)+" AND IADATINC<="+cp_ToStrODBC(this.oParentObject.w_DSDATINV)+"";
             ,"_Curs_INC_AVVE")
      else
        select * from (i_cTable);
         where IASERIAL=this.oParentObject.w_DSSERIAL AND IADATINC<=this.oParentObject.w_DSDATINV;
          into cursor _Curs_INC_AVVE
      endif
      if used('_Curs_INC_AVVE')
        select _Curs_INC_AVVE
        locate for 1=1
        do while not(eof())
        this.w_TOTPAR = this.w_TOTPAR+ _Curs_INC_AVVE.IAIMPINC
          select _Curs_INC_AVVE
          continue
        enddo
        use
      endif
      * --- Leggo dai Parametri Contenzioso i campi delle Spese Notificate e a seconda della Modalit� di inoltro
      *     assegno il relativo valore.
      Select STAMPA 
 Go Top 
 Replace SPEPRE WITH this.oParentObject.w_DSIMPSPE 
 Replace COTOTIMP WITH Nvl(COTOTIMP,0)-this.w_TOTPAR 
 Replace NOMTES WITH this.oParentObject.w_DSRIFTES 
 Replace INTERESS WITH this.oParentObject.w_DSIMPINT 
 Replace OGGTES WITH this.w_LOGGTES 
 Replace TESTES WITH this.w_LTESTES 
 Replace PIETES WITH this.w_LPIETES
      this.oParentObject.w_FLEURO = IIF(NOT (STAMPA.COCODVAL<>g_PERVAL AND (EMPTY(Nvl(STAMPA.CODVAL,"   ")) OR Nvl(STAMPA.COCAOVAL,0)<>0)), " ", this.oParentObject.w_FLEURO)
      this.w_MAIL = .T.
      * --- Acquisisco gli indirizzi email associati al cliente
      GSAR_BRM(this,"E_M","C",STAMPA.COCODCON,"SO","V")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
      i_CLIFORDES = "C" 
 i_CODDES = STAMPA.COCODCON 
 i_TIPDES = "SO"
       
 i_WEDEST = "C" + STAMPA.COCODCON
    endif
    * --- eseguo la query per selezionare i dati dalla tabella destinazioni diverse
    * --- prima sede di avviso
    Vq_Exec("..\SOLL\EXE\Query\gsso_bz2.vqr",This,"TMP1")
    if RECCOUNT("TMP1") > 0
      Select STAMPA
      * --- Se ci sono i dati in TMP1, allora devo modificare i dati in __TMP__ con quelli di TMP1
      if !EMPTY(ALLTRIM(TMP1.DDNOMDES))
        Replace ANDESCRI With TMP1.DDNOMDES
      endif
      if !EMPTY(ALLTRIM(TMP1.DDINDIRI))
        Replace ANINDIRI With TMP1.DDINDIRI
      endif
      if !EMPTY(ALLTRIM(TMP1.DD___CAP))
        Replace AN___CAP With TMP1.DD___CAP
      endif
      if !EMPTY(ALLTRIM(TMP1.DDLOCALI))
        Replace ANLOCALI With TMP1.DDLOCALI
      endif
      if !EMPTY(ALLTRIM(TMP1.DDPROVIN))
        Replace ANPROVIN With TMP1.DDPROVIN
      endif
    else
      * --- altrimenti sede di pagamento
      Vq_Exec("..\SOLL\EXE\Query\gsso_bz3.vqr",This,"TMP2")
      if RECCOUNT("TMP2") > 0
        Select STAMPA
        * --- Se ci sono i dati in TMP2, allora devo modificare i dati in __TMP__ con quelli di TMP2
        if !EMPTY(ALLTRIM(TMP2.DDNOMDES))
          Replace ANDESCRI With TMP2.DDNOMDES
        endif
        if !EMPTY(ALLTRIM(TMP2.DDINDIRI))
          Replace ANINDIRI With TMP2.DDINDIRI
        endif
        if !EMPTY(ALLTRIM(TMP2.DD___CAP))
          Replace AN___CAP With TMP2.DD___CAP
        endif
        if !EMPTY(ALLTRIM(TMP2.DDLOCALI))
          Replace ANLOCALI With TMP2.DDLOCALI
        endif
        if !EMPTY(ALLTRIM(TMP2.DDPROVIN))
          Replace ANPROVIN With TMP2.DDPROVIN
        endif
      endif
    endif
    Endscan
    * --- Se w_NKO>0 esegue stampa mancanti
    if this.w_NKO>0
      if this.w_NKO=1
        this.w_TMPC = "� stato rilevato un contenzioso per il quale non � stato possibile %0determinare automaticamente il testo da stampare. %0Si vuole la stampa delle segnalazioni?"
      else
        this.w_TMPC = "Sono stati rilevati %1 contenziosi per i quali non � stato possibile %0determinare automaticamente il testo da stampare. %0Si vuole la stampa delle segnalazioni?"
      endif
      if ah_YesNo(this.w_TMPC,,alltrim(str(this.w_NKO,6,0)))
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione",.f.)     
      endif
    endif
    if Used("TMP1")
      Select TMP1
      Use
    endif
    if Used("TMP2")
      Select TMP2
      Use
    endif
    CURFIL = IIF(this.oParentObject.w_FLEURO="S", "TMP1", "__TMP__")
    if this.Pparam="S"
      * --- Esegue la Stampa esegue la join con il cursore contenente i memo
       
 SELECT STAMPA.*, DATEST.TESTES, DATEST.PIETES, DATEST.OGGTES, DATEST.NOTTES FROM STAMPA ; 
 LEFT OUTER JOIN DATEST ON (DATEST.CO__TIPO=STAMPA.CO__TIPO AND DATEST.COSERIAL=STAMPA.COSERIAL) where stampa.conumliv<>-1 ; 
 INTO CURSOR &CURFIL 
    else
       
 SELECT * FROM STAMPA INTO CURSOR &CURFIL 
    endif
    if this.oParentObject.w_FLEURO="S"
      * --- Converte gli Importi EMU in EURO
      Select TMP1
      WRCURSOR("TMP1")
      Select TMP1
      GO TOP
      SCAN FOR COCODVAL<>g_PERVAL
      REPLACE COIMPSPE WITH VAL2VAL(COIMPSPE,TMP1.COCAOVAL,TMP1.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),2)
      REPLACE COTOTIMP WITH VAL2VAL(COTOTIMP,TMP1.COCAOVAL,TMP1.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),2)
      REPLACE SPEBAN WITH VAL2VAL(SPEBAN,TMP1.COCAOVAL,TMP1.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),2)
      REPLACE SPEPRE WITH VAL2VAL(SPEPRE,TMP1.COCAOVAL,TMP1.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),2)
      REPLACE INTERESS WITH VAL2VAL(INTERESS,TMP1.COCAOVAL,TMP1.CODATREG,I_DATSYS,GETCAM(G_PERVAL,I_DATSYS),2)
      ENDSCAN
      Select * FROM TMP1 INTO CURSOR __TMP__ ORDER BY COCODCON, COCODVAL, COSERIAL
      if Used("TMP1")
        Select TMP1
        Use
      endif
    endif
    if Used("DATEST") 
      Select DATEST
      Use
    endif
    if Used("TEMP")
      Select TEMP
      Use
    endif
    if this.Pparam="S"
      Select __TMP__
      GO TOP
      if this.oParentObject.w_FLGRAGG="S"
        if this.oParentObject.w_FLGAUTO="S"
          * --- Sistemazione dei testi utilizzati
          Select __TMP__
          WRCURSOR("__TMP__")
           Index On (COCODCON+COCODVAL+STR(CONUMLIV)) Tag idx1
          Select __TMP__
          GO TOP
          * --- Creo un cursore avente il numero di livello pi� alto per ogni raggruppamento (COCODCON e COCODVAL)
          SELECT COCODCON, COCODVAL, MAX(CONUMLIV) AS CONUMLIV FROM __TMP__ INTO CURSOR RAGGRUPP GROUP BY COCODCON,COCODVAL
          Select RAGGRUPP
          GO TOP
          SCAN
          * --- Recupero il numero di livello pi� alto per ogni raggruppamento (COCODCON e COCODVAL)
          this.w_COCODCON = RAGGRUPP.COCODCON
          this.w_COCODVAL = RAGGRUPP.COCODVAL
          this.w_CONUMLIV = RAGGRUPP.CONUMLIV
          Select __TMP__
          GO TOP
          Seek(this.w_COCODCON+this.w_COCODVAL+STR(this.w_CONUMLIV))
          * --- Recupero i testi relativi al numero di livello pi� alto per ogni raggruppamento (COCODCON e COCODVAL)
          this.w_COOGGTES = __TMP__.OGGTES
          this.w_COTESTES = __TMP__.TESTES
          this.w_COPIETES = __TMP__.PIETES
          this.oParentObject.w_NUMERO = __TMP__.NUMTES
          GO TOP
          * --- Aggiorno i testi di ogni riga di ogni raggruppamento
          REPLACE OGGTES WITH this.w_COOGGTES, TESTES WITH this.w_COTESTES, PIETES WITH this.w_COPIETES, CONUMLIV WITH this.w_CONUMLIV ; 
 FOR COCODCON=this.w_COCODCON AND COCODVAL=this.w_COCODVAL
          Select RAGGRUPP
          ENDSCAN
        endif
      endif
    endif
    this.w_NOK = 0
    CP_CHPRN (this.oParentObject.w_OREP, " ", this)
    if this.w_MAIL
      i_FAXNO = ""
      i_EMAIL = ""
      i_EMAIL_PEC = ""
      i_WEDEST =""
      i_WEALLENAME = ""
      i_WEALLETITLE =""
      i_DEST = ""
      i_CODDES = ""
    endif
    Select __TMP__
    this.w_NOK = reccount("__TMP__")
    if this.Pparam="S" AND this.w_NOK > 0
      if Used("__TMP__") And ah_YesNo("Aggiorno la situazione dei solleciti?")
        this.w_OCODICE = "@@@@@@"
        SELECT STAMPA
        GO TOP
        SCAN FOR XCHK=1 AND NOT EMPTY(NVL(COSERIAL," ")) AND CONUMLIV<>-1
        * --- Per Ogni Contenzioso...
        if this.oParentObject.w_FLGAUTO="S"
          if this.oParentObject.w_FLGRAGG="S"
            * --- Recupero il livello dal cursore RAGGRUPP
            this.w_COCODCON = STAMPA.COCODCON
            this.w_COCODVAL = STAMPA.COCODVAL
            Select __TMP__
            GO TOP
            Seek(this.w_COCODCON+this.w_COCODVAL)
            this.oParentObject.w_NUMERO = __TMP__.NUMTES
            this.oParentObject.w_LIVTES = __TMP__.CONUMLIV
            SELECT STAMPA
          else
            this.oParentObject.w_LIVTES = STAMPA.CONUMLIV
            this.oParentObject.w_NUMERO = STAMPA.NUMTES
          endif
        endif
        this.w_SERIAL = COSERIAL
        this.w_TOTIMP = NVL(COTOTIMP, 0)
        this.w_PERINT = NVL(COPERINT, 0)
        this.w_DATSCA = NVL(CP_TODATE(CODATREG), cp_CharToDate("  -  -  "))
        this.w_IMPINT = NVL(INTERESS,0)
        this.w_IMPSPE = IIF(FLGSPE="S",COIMPSPE,0)
        if this.oParentObject.w_FLGRAGG="S" AND this.w_OCODICE=NVL(COCODCON, SPACE(15))
          * --- Se Raggruppa testi e non e' un nuovo cliente azzera le spese (solo sul primo)
          this.w_IMPSPE = 0
        endif
        this.w_OCODICE = NVL(COCODCON, SPACE(15))
        if this.oParentObject.w_DATINV>NVL(CODATULT, cp_CharToDate("  -  -  "))
          * --- Aggiorna la Data Ultimo Sollecito (se Maggiore)
          * --- Write into CON_TENZ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CON_TENZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CODATULT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINV),'CON_TENZ','CODATULT');
            +",CONUMLIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIVTES),'CON_TENZ','CONUMLIV');
                +i_ccchkf ;
            +" where ";
                +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                CODATULT = this.oParentObject.w_DATINV;
                ,CONUMLIV = this.oParentObject.w_LIVTES;
                &i_ccchkf. ;
             where;
                COSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Comunque aggiorno il livello in testata
          * --- Write into CON_TENZ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CON_TENZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CONUMLIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIVTES),'CON_TENZ','CONUMLIV');
                +i_ccchkf ;
            +" where ";
                +"COSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                CONUMLIV = this.oParentObject.w_LIVTES;
                &i_ccchkf. ;
             where;
                COSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Select from DET_SOLL
        i_nConn=i_TableProp[this.DET_SOLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_SOLL_idx,2],.t.,this.DET_SOLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" DET_SOLL ";
              +" where DSSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
               ,"_Curs_DET_SOLL")
        else
          select CPROWNUM from (i_cTable);
           where DSSERIAL=this.w_SERIAL;
            into cursor _Curs_DET_SOLL
        endif
        if used('_Curs_DET_SOLL')
          select _Curs_DET_SOLL
          locate for 1=1
          do while not(eof())
          this.w_ROWNUM = _Curs_DET_SOLL.CPROWNUM
            select _Curs_DET_SOLL
            continue
          enddo
          use
        endif
        this.w_ROWNUM = NVL(this.w_ROWNUM, 0) + 1
        * --- E aggiunge lo Stesso alla Lista
        if this.oParentObject.w_FLGAUTO="S"
          * --- Insert into DET_SOLL
          i_nConn=i_TableProp[this.DET_SOLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_SOLL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_SOLL_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DSSERIAL"+",CPROWNUM"+",DSNUMLIV"+",DSRIFTES"+",DSMEZSPE"+",DSDATINV"+",DSIMPINT"+",DSIMPSPE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DET_SOLL','DSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DET_SOLL','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIVTES),'DET_SOLL','DSNUMLIV');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMERO),'DET_SOLL','DSRIFTES');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MEZSPE),'DET_SOLL','DSMEZSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINV),'DET_SOLL','DSDATINV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPINT),'DET_SOLL','DSIMPINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSPE),'DET_SOLL','DSIMPSPE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DSSERIAL',this.w_SERIAL,'CPROWNUM',this.w_ROWNUM,'DSNUMLIV',this.oParentObject.w_LIVTES,'DSRIFTES',this.oParentObject.w_NUMERO,'DSMEZSPE',this.oParentObject.w_MEZSPE,'DSDATINV',this.oParentObject.w_DATINV,'DSIMPINT',this.w_IMPINT,'DSIMPSPE',this.w_IMPSPE)
            insert into (i_cTable) (DSSERIAL,CPROWNUM,DSNUMLIV,DSRIFTES,DSMEZSPE,DSDATINV,DSIMPINT,DSIMPSPE &i_ccchkf. );
               values (;
                 this.w_SERIAL;
                 ,this.w_ROWNUM;
                 ,this.oParentObject.w_LIVTES;
                 ,this.oParentObject.w_NUMERO;
                 ,this.oParentObject.w_MEZSPE;
                 ,this.oParentObject.w_DATINV;
                 ,this.w_IMPINT;
                 ,this.w_IMPSPE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into DET_SOLL
          i_nConn=i_TableProp[this.DET_SOLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_SOLL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_SOLL_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DSSERIAL"+",CPROWNUM"+",DSNUMLIV"+",DSRIFTES"+",DSMEZSPE"+",DSDATINV"+",DSIMPINT"+",DSIMPSPE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DET_SOLL','DSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DET_SOLL','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LIVTES),'DET_SOLL','DSNUMLIV');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMERO),'DET_SOLL','DSRIFTES');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MEZSPE),'DET_SOLL','DSMEZSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATINV),'DET_SOLL','DSDATINV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPINT),'DET_SOLL','DSIMPINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSPE),'DET_SOLL','DSIMPSPE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DSSERIAL',this.w_SERIAL,'CPROWNUM',this.w_ROWNUM,'DSNUMLIV',this.oParentObject.w_LIVTES,'DSRIFTES',this.oParentObject.w_NUMERO,'DSMEZSPE',this.oParentObject.w_MEZSPE,'DSDATINV',this.oParentObject.w_DATINV,'DSIMPINT',this.w_IMPINT,'DSIMPSPE',this.w_IMPSPE)
            insert into (i_cTable) (DSSERIAL,CPROWNUM,DSNUMLIV,DSRIFTES,DSMEZSPE,DSDATINV,DSIMPINT,DSIMPSPE &i_ccchkf. );
               values (;
                 this.w_SERIAL;
                 ,this.w_ROWNUM;
                 ,this.oParentObject.w_LIVTES;
                 ,this.oParentObject.w_NUMERO;
                 ,this.oParentObject.w_MEZSPE;
                 ,this.oParentObject.w_DATINV;
                 ,this.w_IMPINT;
                 ,this.w_IMPSPE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        SELECT &NC
        ENDSCAN
        this.oParentObject.NotifyEvent("CalcZoom")
      else
        SELECT &NC 
 GOTO (this.w_NUMREC)
      endif
      this.oParentObject.w_NUMERO = SPACE(6)
      this.oParentObject.w_NOMTES = SPACE(50)
    endif
    this.w_NKO = 0
    * --- RILASCIO DEI CURSORI
    if Used("TMP1")
      Select TMP1
      Use
    endif
    if Used("__TMP__")
      Select __TMP__
      Use
    endif
    if Used("STAMPA")
      Select STAMPA
      Use
    endif
    if Used("INCASSI")
      Select INCASSI
      Use
    endif
    if Used("RAGGRUPP") 
      Select RAGGRUPP
      Use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il tasso viene letto dall'anagrafica Saggio Interessi di Mora � necessario dettagliare gli interessi per ogni semestre
    this.w_ANNO = STR(YEAR(this.w_DATINI+1),4)
    this.w_PERIOD = IIF(MONTH(this.w_DATINI+1)<7, "P", "S")
    do while this.w_DATINI<this.w_DATFIN
      this.w_SAGRIF = 0
      this.w_MAGNDP = 0
      this.w_MAGDEP = 0
      * --- Read from INT_MORA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INT_MORA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INT_MORA_idx,2],.t.,this.INT_MORA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMSAGRIF,IMMAGNDP,IMMAGDEP"+;
          " from "+i_cTable+" INT_MORA where ";
              +"IM__ANNO = "+cp_ToStrODBC(this.w_ANNO);
              +" and IMPERIOD = "+cp_ToStrODBC(this.w_PERIOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMSAGRIF,IMMAGNDP,IMMAGDEP;
          from (i_cTable) where;
              IM__ANNO = this.w_ANNO;
              and IMPERIOD = this.w_PERIOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SAGRIF = NVL(cp_ToDate(_read_.IMSAGRIF),cp_NullValue(_read_.IMSAGRIF))
        this.w_MAGNDP = NVL(cp_ToDate(_read_.IMMAGNDP),cp_NullValue(_read_.IMMAGNDP))
        this.w_MAGDEP = NVL(cp_ToDate(_read_.IMMAGDEP),cp_NullValue(_read_.IMMAGDEP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Calcolo il numero di giorni per i quali devono essere calcolati gli interessi
      * --- Data inizio Mora
      this.w_DATINIMO = this.w_DATINI+1
      * --- Data fine Mora
      this.w_DATFINMO = IIF(this.w_PERIOD="P", cp_CharToDate("30-06-"+this.w_ANNO), cp_CharToDate("31-12-"+this.w_ANNO))
      if this.w_DATFIN<this.w_DATFINMO
        this.w_GIORNI = this.w_DATFIN - this.w_DATINI
        this.w_DATFINMO = this.w_DATFIN
      else
        this.w_GIORNI = this.w_DATFINMO - this.w_DATINI
      endif
      * --- Determinazione del tasso in funzione del flag Beni Deperibili
      this.w_TASSO = this.w_SAGRIF+IIF(this.w_FLVABD="S", this.w_MAGDEP, this.w_MAGNDP)+this.w_PERINT
      * --- Calcolo degli interessi
      this.w_INTERESS = this.w_INTERESS + cp_ROUND(((this.w_IMPORTO * (this.w_TASSO*this.w_GIORNI))/36500),this.oParentObject.w_DECTOT)
      this.w_DATINI = this.w_DATFINMO
      this.w_ANNO = STR(VAL(this.w_ANNO)+IIF(this.w_PERIOD="S", 1, 0), 4)
      this.w_PERIOD = IIF(this.w_PERIOD="S", "P", "S")
    enddo
  endproc


  proc Init(oParentObject,PpARAM)
    this.PpARAM=PpARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CON_TENZ'
    this.cWorkTables[2]='DET_SOLL'
    this.cWorkTables[3]='TES_CONT'
    this.cWorkTables[4]='CONDTENZ'
    this.cWorkTables[5]='PAR_TITE'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='INT_MORA'
    this.cWorkTables[8]='PAR_CONT'
    this.cWorkTables[9]='INC_AVVE'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_INC_AVVE')
      use in _Curs_INC_AVVE
    endif
    if used('_Curs_DET_SOLL')
      use in _Curs_DET_SOLL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PpARAM"
endproc
