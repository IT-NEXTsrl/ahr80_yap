* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_be1                                                        *
*              Apro la riga con la descrizion e dell'errore                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-08                                                      *
* Last revis.: 2012-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_be1",oParentObject)
return(i_retval)

define class tgsso_be1 as StdBatch
  * --- Local variables
  w_CPROWNUM = 0
  * --- WorkFile variables
  LOG_REBA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apro la riga dell'errore
    this.w_CPROWNUM = 0
    * --- Select from LOG_REBA
    i_nConn=i_TableProp[this.LOG_REBA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_REBA_idx,2],.t.,this.LOG_REBA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MIN(CPROWNUM) AS CPROWNUM  from "+i_cTable+" LOG_REBA ";
          +" where ERNOMSUP="+cp_ToStrODBC(this.oParentObject.w_FRNOMSUP)+" and ERDTARIC="+cp_ToStrODBC(this.oParentObject.w_FRDTARIC)+" and ERNOMFIL="+cp_ToStrODBC(this.oParentObject.w_FRNOMFIL)+" AND ERNUMRIG>="+cp_ToStrODBC(this.oParentObject.w_FRFIRRIG)+" and ERNUMRIG<="+cp_ToStrODBC(this.oParentObject.w_FRLASRIG)+"";
           ,"_Curs_LOG_REBA")
    else
      select MIN(CPROWNUM) AS CPROWNUM from (i_cTable);
       where ERNOMSUP=this.oParentObject.w_FRNOMSUP and ERDTARIC=this.oParentObject.w_FRDTARIC and ERNOMFIL=this.oParentObject.w_FRNOMFIL AND ERNUMRIG>=this.oParentObject.w_FRFIRRIG and ERNUMRIG<=this.oParentObject.w_FRLASRIG;
        into cursor _Curs_LOG_REBA
    endif
    if used('_Curs_LOG_REBA')
      select _Curs_LOG_REBA
      locate for 1=1
      do while not(eof())
      this.w_CPROWNUM = nvl(CPROWNUM,0)
        select _Curs_LOG_REBA
        continue
      enddo
      use
    endif
    OpenGest("A", "GSSO_MLE", "ERNOMSUP", this.oParentObject.w_FRNOMSUP, "ERDTARIC", this.oParentObject.w_FRDTARIC, "ERNOMFIL", this.oParentObject.w_FRNOMFIL, "CPROWNUM", this.w_CPROWNUM)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOG_REBA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_LOG_REBA')
      use in _Curs_LOG_REBA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
