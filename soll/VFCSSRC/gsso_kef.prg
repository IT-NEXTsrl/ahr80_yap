* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_kef                                                        *
*              Elimina flussi operativi di ritorno                             *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-27                                                      *
* Last revis.: 2012-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_kef",oParentObject))

* --- Class definition
define class tgsso_kef as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 497
  Height = 127
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-27"
  HelpContextID=65639785
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  FLU_RITO_IDX = 0
  cPrg = "gsso_kef"
  cComment = "Elimina flussi operativi di ritorno"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_GESTIONE = space(1)
  o_GESTIONE = space(1)
  w_FILEC = space(100)
  w_FILEF = space(100)
  w_FILER = space(100)
  w_FILEA = space(100)
  w_DTARIC = ctod('  /  /  ')
  w_NOMSUP = space(20)
  w_AVSERIAL = space(10)
  w_FILEB = space(100)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_kefPag1","gsso_kef",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILEC_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FLU_RITO'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GESTIONE=space(1)
      .w_FILEC=space(100)
      .w_FILEF=space(100)
      .w_FILER=space(100)
      .w_FILEA=space(100)
      .w_DTARIC=ctod("  /  /  ")
      .w_NOMSUP=space(20)
      .w_AVSERIAL=space(10)
      .w_FILEB=space(100)
        .w_GESTIONE = 'F'
        .w_FILEC = IIF(NOT(.w_GESTIONE='C'),space(100),.w_FILEC)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_FILEC))
          .link_1_6('Full')
        endif
        .w_FILEF = IIF(NOT(.w_GESTIONE='F'),space(100),.w_FILEF)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_FILEF))
          .link_1_7('Full')
        endif
        .w_FILER = IIF(NOT(.w_GESTIONE='R'),space(100),.w_FILER)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_FILER))
          .link_1_8('Full')
        endif
        .w_FILEA = IIF(NOT(.w_GESTIONE='R'),space(100),.w_FILER)
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_AVSERIAL))
          .link_1_12('Full')
        endif
        .w_FILEB = IIF(NOT(.w_GESTIONE='B'),space(100),.w_FILEB)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_FILEB))
          .link_1_13('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_GESTIONE<>.w_GESTIONE
            .w_FILEC = IIF(NOT(.w_GESTIONE='C'),space(100),.w_FILEC)
          .link_1_6('Full')
        endif
        if .o_GESTIONE<>.w_GESTIONE
            .w_FILEF = IIF(NOT(.w_GESTIONE='F'),space(100),.w_FILEF)
          .link_1_7('Full')
        endif
        if .o_GESTIONE<>.w_GESTIONE
            .w_FILER = IIF(NOT(.w_GESTIONE='R'),space(100),.w_FILER)
          .link_1_8('Full')
        endif
        if .o_GESTIONE<>.w_GESTIONE
            .w_FILEA = IIF(NOT(.w_GESTIONE='R'),space(100),.w_FILER)
        endif
        .DoRTCalc(6,8,.t.)
        if .o_GESTIONE<>.w_GESTIONE
            .w_FILEB = IIF(NOT(.w_GESTIONE='B'),space(100),.w_FILEB)
          .link_1_13('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFILEC_1_6.visible=!this.oPgFrm.Page1.oPag.oFILEC_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFILEF_1_7.visible=!this.oPgFrm.Page1.oPag.oFILEF_1_7.mHide()
    this.oPgFrm.Page1.oPag.oFILER_1_8.visible=!this.oPgFrm.Page1.oPag.oFILER_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFILEA_1_9.visible=!this.oPgFrm.Page1.oPag.oFILEA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oAVSERIAL_1_12.visible=!this.oPgFrm.Page1.oPag.oAVSERIAL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oFILEB_1_13.visible=!this.oPgFrm.Page1.oPag.oFILEB_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FILEC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_FRIC_IDX,3]
    i_lTable = "CON_FRIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_FRIC_IDX,2], .t., this.CON_FRIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_FRIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILEC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_FRIC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRNOMFIL like "+cp_ToStrODBC(trim(this.w_FILEC)+"%");

          i_ret=cp_SQL(i_nConn,"select CRNOMFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRNOMFIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRNOMFIL',trim(this.w_FILEC))
          select CRNOMFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRNOMFIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILEC)==trim(_Link_.CRNOMFIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILEC) and !this.bDontReportError
            deferred_cp_zoom('CON_FRIC','*','CRNOMFIL',cp_AbsName(oSource.parent,'oFILEC_1_6'),i_cWhere,'',"Elenco file conferma ricezione",'gsrb_kef.CON_FRIC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRNOMFIL";
                     +" from "+i_cTable+" "+i_lTable+" where CRNOMFIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRNOMFIL',oSource.xKey(1))
            select CRNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILEC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRNOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where CRNOMFIL="+cp_ToStrODBC(this.w_FILEC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRNOMFIL',this.w_FILEC)
            select CRNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILEC = NVL(_Link_.CRNOMFIL,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_FILEC = space(100)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_FRIC_IDX,2])+'\'+cp_ToStr(_Link_.CRNOMFIL,1)
      cp_ShowWarn(i_cKey,this.CON_FRIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILEC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILEF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLU_RITO_IDX,3]
    i_lTable = "FLU_RITO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2], .t., this.FLU_RITO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FLU_RITO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNOMFIL like "+cp_ToStrODBC(trim(this.w_FILEF)+"%");

          i_ret=cp_SQL(i_nConn,"select FLNOMFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FLNOMFIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FLNOMFIL',trim(this.w_FILEF))
          select FLNOMFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FLNOMFIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILEF)==trim(_Link_.FLNOMFIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILEF) and !this.bDontReportError
            deferred_cp_zoom('FLU_RITO','*','FLNOMFIL',cp_AbsName(oSource.parent,'oFILEF_1_7'),i_cWhere,'',"Elenco file flussi di ritorno",'gsrb_kef.FLU_RITO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMFIL";
                     +" from "+i_cTable+" "+i_lTable+" where FLNOMFIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMFIL',oSource.xKey(1))
            select FLNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FLNOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where FLNOMFIL="+cp_ToStrODBC(this.w_FILEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FLNOMFIL',this.w_FILEF)
            select FLNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILEF = NVL(_Link_.FLNOMFIL,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_FILEF = space(100)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLU_RITO_IDX,2])+'\'+cp_ToStr(_Link_.FLNOMFIL,1)
      cp_ShowWarn(i_cKey,this.FLU_RITO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILER
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REN_DICO_IDX,3]
    i_lTable = "REN_DICO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REN_DICO_IDX,2], .t., this.REN_DICO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REN_DICO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'REN_DICO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RENOMFIL like "+cp_ToStrODBC(trim(this.w_FILER)+"%");

          i_ret=cp_SQL(i_nConn,"select RENOMFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RENOMFIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RENOMFIL',trim(this.w_FILER))
          select RENOMFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RENOMFIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILER)==trim(_Link_.RENOMFIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILER) and !this.bDontReportError
            deferred_cp_zoom('REN_DICO','*','RENOMFIL',cp_AbsName(oSource.parent,'oFILER_1_8'),i_cWhere,'',"Elenco file rendicontazione",'gsrb_kef.REN_DICO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RENOMFIL";
                     +" from "+i_cTable+" "+i_lTable+" where RENOMFIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RENOMFIL',oSource.xKey(1))
            select RENOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RENOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where RENOMFIL="+cp_ToStrODBC(this.w_FILER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RENOMFIL',this.w_FILER)
            select RENOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILER = NVL(_Link_.RENOMFIL,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_FILER = space(100)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REN_DICO_IDX,2])+'\'+cp_ToStr(_Link_.RENOMFIL,1)
      cp_ShowWarn(i_cKey,this.REN_DICO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AVSERIAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AVV_RIB_IDX,3]
    i_lTable = "AVV_RIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AVV_RIB_IDX,2], .t., this.AVV_RIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AVV_RIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AVSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AVV_RIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AVSERIAL like "+cp_ToStrODBC(trim(this.w_AVSERIAL)+"%");

          i_ret=cp_SQL(i_nConn,"select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AVSERIAL',trim(this.w_AVSERIAL))
          select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AVSERIAL)==trim(_Link_.AVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AVSERIAL) and !this.bDontReportError
            deferred_cp_zoom('AVV_RIB','*','AVSERIAL',cp_AbsName(oSource.parent,'oAVSERIAL_1_12'),i_cWhere,'',"Elenco file avvisi RiBa passive",'gsrb_kef.AVV_RIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC";
                     +" from "+i_cTable+" "+i_lTable+" where AVSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AVSERIAL',oSource.xKey(1))
            select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AVSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC";
                   +" from "+i_cTable+" "+i_lTable+" where AVSERIAL="+cp_ToStrODBC(this.w_AVSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AVSERIAL',this.w_AVSERIAL)
            select AVSERIAL,AVNOMFIL,AVNOMSUP,AVDTARIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AVSERIAL = NVL(_Link_.AVSERIAL,space(10))
      this.w_FILEA = NVL(_Link_.AVNOMFIL,space(100))
      this.w_NOMSUP = NVL(_Link_.AVNOMSUP,space(20))
      this.w_DTARIC = NVL(cp_ToDate(_Link_.AVDTARIC),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AVSERIAL = space(10)
      endif
      this.w_FILEA = space(100)
      this.w_NOMSUP = space(20)
      this.w_DTARIC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AVV_RIB_IDX,2])+'\'+cp_ToStr(_Link_.AVSERIAL,1)
      cp_ShowWarn(i_cKey,this.AVV_RIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AVSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILEB
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BON_RICE_IDX,3]
    i_lTable = "BON_RICE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BON_RICE_IDX,2], .t., this.BON_RICE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BON_RICE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILEB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BON_RICE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BRNOMFIL like "+cp_ToStrODBC(trim(this.w_FILEB)+"%");

          i_ret=cp_SQL(i_nConn,"select BRNOMFIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BRNOMFIL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BRNOMFIL',trim(this.w_FILEB))
          select BRNOMFIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BRNOMFIL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILEB)==trim(_Link_.BRNOMFIL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILEB) and !this.bDontReportError
            deferred_cp_zoom('BON_RICE','*','BRNOMFIL',cp_AbsName(oSource.parent,'oFILEB_1_13'),i_cWhere,'',"Elenco bonifici ricevuti",'gsrb_kef.BON_RICE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BRNOMFIL";
                     +" from "+i_cTable+" "+i_lTable+" where BRNOMFIL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BRNOMFIL',oSource.xKey(1))
            select BRNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILEB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BRNOMFIL";
                   +" from "+i_cTable+" "+i_lTable+" where BRNOMFIL="+cp_ToStrODBC(this.w_FILEB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BRNOMFIL',this.w_FILEB)
            select BRNOMFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILEB = NVL(_Link_.BRNOMFIL,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_FILEB = space(100)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BON_RICE_IDX,2])+'\'+cp_ToStr(_Link_.BRNOMFIL,1)
      cp_ShowWarn(i_cKey,this.BON_RICE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILEB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGESTIONE_1_3.RadioValue()==this.w_GESTIONE)
      this.oPgFrm.Page1.oPag.oGESTIONE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEC_1_6.value==this.w_FILEC)
      this.oPgFrm.Page1.oPag.oFILEC_1_6.value=this.w_FILEC
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEF_1_7.value==this.w_FILEF)
      this.oPgFrm.Page1.oPag.oFILEF_1_7.value=this.w_FILEF
    endif
    if not(this.oPgFrm.Page1.oPag.oFILER_1_8.value==this.w_FILER)
      this.oPgFrm.Page1.oPag.oFILER_1_8.value=this.w_FILER
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEA_1_9.value==this.w_FILEA)
      this.oPgFrm.Page1.oPag.oFILEA_1_9.value=this.w_FILEA
    endif
    if not(this.oPgFrm.Page1.oPag.oAVSERIAL_1_12.value==this.w_AVSERIAL)
      this.oPgFrm.Page1.oPag.oAVSERIAL_1_12.value=this.w_AVSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEB_1_13.value==this.w_FILEB)
      this.oPgFrm.Page1.oPag.oFILEB_1_13.value=this.w_FILEB
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GESTIONE = this.w_GESTIONE
    return

enddefine

* --- Define pages as container
define class tgsso_kefPag1 as StdContainer
  Width  = 493
  height = 127
  stdWidth  = 493
  stdheight = 127
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="OSXGXXPXXC",left=381, top=76, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 25306134;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        do GSSO_BEF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_FILEC) OR not empty(.w_FILEF) OR not empty(.w_FILER)  OR not empty(.w_FILEA) OR not empty(.w_FILEB))
      endwith
    endif
  endfunc


  add object oBtn_1_2 as StdButton with uid="BWTKCLQRSS",left=432, top=76, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 25306134;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oGESTIONE_1_3 as StdCombo with uid="ZDYAGQBIAZ",rtseq=1,rtrep=.f.,left=125,top=18,width=161,height=21, enabled=.f.;
    , ToolTipText = "Gestione da selezionare per il file da eliminare";
    , HelpContextID = 6997;
    , cFormVar="w_GESTIONE",RowSource=""+"Flussi operativi di ritorno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGESTIONE_1_3.RadioValue()
    return(iif(this.value =1,'F',;
    space(1)))
  endfunc
  func oGESTIONE_1_3.GetRadio()
    this.Parent.oContained.w_GESTIONE = this.RadioValue()
    return .t.
  endfunc

  func oGESTIONE_1_3.SetRadio()
    this.Parent.oContained.w_GESTIONE=trim(this.Parent.oContained.w_GESTIONE)
    this.value = ;
      iif(this.Parent.oContained.w_GESTIONE=='F',1,;
      0)
  endfunc

  add object oFILEC_1_6 as StdField with uid="QHYLSQUVVO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FILEC", cQueryName = "FILEC",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File da eliminare",;
    HelpContextID = 258967466,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=125, Top=49, InputMask=replicate('X',100), bHasZoom = .t. , cLinkFile="CON_FRIC", oKey_1_1="CRNOMFIL", oKey_1_2="this.w_FILEC"

  func oFILEC_1_6.mHide()
    with this.Parent.oContained
      return (NOT(.w_GESTIONE='C'))
    endwith
  endfunc

  func oFILEC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILEC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILEC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_FRIC','*','CRNOMFIL',cp_AbsName(this.parent,'oFILEC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco file conferma ricezione",'gsrb_kef.CON_FRIC_VZM',this.parent.oContained
  endproc

  add object oFILEF_1_7 as StdField with uid="YQTCDXPMOX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FILEF", cQueryName = "FILEF",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File da eliminare",;
    HelpContextID = 255821738,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=125, Top=49, InputMask=replicate('X',100), bHasZoom = .t. , cLinkFile="FLU_RITO", oKey_1_1="FLNOMFIL", oKey_1_2="this.w_FILEF"

  func oFILEF_1_7.mHide()
    with this.Parent.oContained
      return (NOT(.w_GESTIONE='F'))
    endwith
  endfunc

  func oFILEF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILEF_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILEF_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLU_RITO','*','FLNOMFIL',cp_AbsName(this.parent,'oFILEF_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco file flussi di ritorno",'gsrb_kef.FLU_RITO_VZM',this.parent.oContained
  endproc

  add object oFILER_1_8 as StdField with uid="GOOQQGYQLE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FILER", cQueryName = "FILER",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File da eliminare",;
    HelpContextID = 243238826,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=125, Top=49, InputMask=replicate('X',100), bHasZoom = .t. , cLinkFile="REN_DICO", oKey_1_1="RENOMFIL", oKey_1_2="this.w_FILER"

  func oFILER_1_8.mHide()
    with this.Parent.oContained
      return (NOT(.w_GESTIONE='R'))
    endwith
  endfunc

  func oFILER_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILER_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILER_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REN_DICO','*','RENOMFIL',cp_AbsName(this.parent,'oFILER_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco file rendicontazione",'gsrb_kef.REN_DICO_VZM',this.parent.oContained
  endproc

  add object oFILEA_1_9 as StdField with uid="CLJMUZLMGD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FILEA", cQueryName = "FILEA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File da eliminare",;
    HelpContextID = 261064618,;
   bGlobalFont=.t.,;
    Height=21, Width=250, Left=229, Top=49, InputMask=replicate('X',100)

  func oFILEA_1_9.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'A')
    endwith
  endfunc

  add object oAVSERIAL_1_12 as StdField with uid="ECWWTTWTVW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AVSERIAL", cQueryName = "AVSERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 176223570,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=125, Top=49, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="AVV_RIB", oKey_1_1="AVSERIAL", oKey_1_2="this.w_AVSERIAL"

  func oAVSERIAL_1_12.mHide()
    with this.Parent.oContained
      return (.w_GESTIONE<>'A')
    endwith
  endfunc

  func oAVSERIAL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oAVSERIAL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAVSERIAL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AVV_RIB','*','AVSERIAL',cp_AbsName(this.parent,'oAVSERIAL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco file avvisi RiBa passive",'gsrb_kef.AVV_RIB_VZM',this.parent.oContained
  endproc

  add object oFILEB_1_13 as StdField with uid="YZRSDJJTYT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FILEB", cQueryName = "FILEB",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "File da eliminare",;
    HelpContextID = 260016042,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=125, Top=49, InputMask=replicate('X',100), bHasZoom = .t. , cLinkFile="BON_RICE", oKey_1_1="BRNOMFIL", oKey_1_2="this.w_FILEB"

  func oFILEB_1_13.mHide()
    with this.Parent.oContained
      return (NOT(.w_GESTIONE='B'))
    endwith
  endfunc

  func oFILEB_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILEB_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILEB_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BON_RICE','*','BRNOMFIL',cp_AbsName(this.parent,'oFILEB_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco bonifici ricevuti",'gsrb_kef.BON_RICE_VZM',this.parent.oContained
  endproc

  add object oStr_1_4 as StdString with uid="WNPCNCMZGJ",Visible=.t., Left=8, Top=19,;
    Alignment=1, Width=113, Height=18,;
    Caption="Scelta gestione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="TIHBPTDFTB",Visible=.t., Left=9, Top=49,;
    Alignment=1, Width=112, Height=18,;
    Caption="File da eliminare:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_kef','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
