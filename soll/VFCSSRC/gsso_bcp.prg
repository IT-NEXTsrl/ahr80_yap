* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bcp                                                        *
*              Conferma piano insoluti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_151]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-03                                                      *
* Last revis.: 2001-08-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODRAG,w_COMPET
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bcp",oParentObject,m.w_CODRAG,m.w_COMPET)
return(i_retval)

define class tgsso_bcp as StdBatch
  * --- Local variables
  w_CODRAG = space(10)
  w_COMPET = space(4)
  w_GPCODRAG = space(10)
  w_GPCOMPET = space(4)
  w_CODRAG1 = space(10)
  * --- WorkFile variables
  CONDTENZ_idx=0
  CON_TENZ_idx=0
  DET_SOLL_idx=0
  DIS_TINT_idx=0
  INC_AVVE_idx=0
  PAR_CONT_idx=0
  PAR_TITE_idx=0
  GES_PIAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Piano Insoluti (da GSSO_API)
    * --- Questo batch viene richiamato dal bottone di conferma presente sull'anagrafica
    * --- Memorizza il Piano che l'operatore sta' caricando e rientra in stato di variazione.
    do case
      case EMPTY(this.w_CODRAG)
        ah_ErrorMsg("Inserire il codice del piano",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.w_COMPET)
        ah_ErrorMsg("Inserire l'esercizio di competenza",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_GPDATREG)
        ah_ErrorMsg("Inserire la data di registrazione",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_GPCODVAL)
        ah_ErrorMsg("Inserire la valuta di riferimento",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_GPCAOVAL)
        ah_ErrorMsg("Inserire il valore del cambio",,"")
        i_retcode = 'stop'
        return
    endcase
    * --- Variabili
    this.w_GPCODRAG = this.w_CODRAG
    this.w_GPCOMPET = this.w_COMPET
    this.oParentObject.w_FASE = 1
    * --- Controllo Presenza del Piano
    * --- Read from GES_PIAN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.GES_PIAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GES_PIAN_idx,2],.t.,this.GES_PIAN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "GPCODRAG"+;
        " from "+i_cTable+" GES_PIAN where ";
            +"GPCODRAG = "+cp_ToStrODBC(this.w_CODRAG);
            +" and GPCOMPET = "+cp_ToStrODBC(this.w_COMPET);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        GPCODRAG;
        from (i_cTable) where;
            GPCODRAG = this.w_CODRAG;
            and GPCOMPET = this.w_COMPET;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODRAG1 = NVL(cp_ToDate(_read_.GPCODRAG),cp_NullValue(_read_.GPCODRAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      * --- Memorizzazione Distinta
      this.oParentobject.bUpdated=.t.
      this.oParentobject.NotifyEvent("Save")
      this.oParentobject.GotFocus()
      this.oParentobject.ecpSave()
      this.oParentobject.ecpQuery()
      * --- Ricalcolo Serial
      this.oParentObject.w_GPCODRAG = this.w_CODRAG
      this.oParentObject.w_GPCOMPET = this.w_COMPET
      * --- Creazione cursore delle chiavi
      this.oParentobject.QueryKeySet("GPCODRAG=" + cp_ToStrODBC(this.w_GPCODRAG) + " and GPCOMPET=" + cp_ToStrODBC(this.w_GPCOMPET) , "" )
      * --- Caricamento record ed accesso in modifica
      this.oParentobject.LoadRec()
      this.oParentobject.NotifyEvent("Edit")
      this.oParentobject.ecpEdit()
      this.oParentObject.w_FASE = 1
    else
      ah_ErrorMsg("Chiave gi� utilizzata",,"")
    endif
  endproc


  proc Init(oParentObject,w_CODRAG,w_COMPET)
    this.w_CODRAG=w_CODRAG
    this.w_COMPET=w_COMPET
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='CONDTENZ'
    this.cWorkTables[2]='CON_TENZ'
    this.cWorkTables[3]='DET_SOLL'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='INC_AVVE'
    this.cWorkTables[6]='PAR_CONT'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='GES_PIAN'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODRAG,w_COMPET"
endproc
