* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bi1                                                        *
*              Seleziona partite x insoluto                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_435]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-11-14                                                      *
* Last revis.: 2013-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bi1",oParentObject)
return(i_retval)

define class tgsso_bi1 as StdBatch
  * --- Local variables
  w_TIPCON = space(1)
  w_PTCODCON = space(15)
  w_SCAINI = ctod("  /  /  ")
  w_CODCON = space(15)
  w_SCAFIN = ctod("  /  /  ")
  w_CODVAL = space(3)
  w_BANCHE = space(15)
  w_TIPPAG = space(2)
  w_FLSALD = space(1)
  w_CONT = space(1)
  w_SERIAL = space(10)
  w_NUMDIS = space(10)
  w_PTDATSCA = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_PTNUMDOC = 0
  w_PTTOTIMP = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod("  /  /  ")
  w_PT_SEGNO = space(1)
  w_PTSERIAL = space(10)
  w_PTFLRAGG = space(1)
  w_FLANEV = space(1)
  w_PTROWORD = 0
  w_COLCOR = space(15)
  w_NabsRow = 0
  w_NRelRow = 0
  w_RECO = 0
  w_FLINDI = space(1)
  w_RNUMDIS = space(10)
  w_DATSCA1 = ctod("  /  /  ")
  w_NUMPAR1 = space(31)
  w_NSBANC = space(15)
  w_BANAPP = space(10)
  w_FLBANC = space(1)
  w_LSEGNO = space(1)
  w_INIT = .f.
  w_RIFDIS1 = space(10)
  w_CHIAVE = space(100)
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_SERIAL = space(10)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_OKRIF = .f.
  w_BANSEL = space(15)
  w_CONASS = space(15)
  w_FLGSCA = space(1)
  w_CODAZI = space(5)
  w_PCDESSUP = space(0)
  w_PTNUMCOR = space(25)
  w_RTIPCON = space(1)
  w_RCODCON = space(15)
  w_RDATSCA = ctod("  /  /  ")
  w_PTCODRAG = space(10)
  w_PTNUMDIS = space(10)
  w_FNSBANC = space(15)
  w_ISERIAL = space(10)
  w_IROWORD = 0
  w_IROWNUM = 0
  w_LNUMDOC = 0
  w_LALFDOC = space(10)
  w_LDATDOC = ctod("  /  /  ")
  w_LSERIAL = space(10)
  w_LSERRIF = space(10)
  w_LORDRIF = 0
  w_LNUMRIF = 0
  w_OK = .f.
  * --- WorkFile variables
  COC_MAST_idx=0
  DIS_TINT_idx=0
  OUT_PUTS_idx=0
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  TMP_GESPIAN_idx=0
  PAR_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona la Partita da Inserire in Insoluto (da GSSO_MCO)
    this.w_INIT = .T.
    this.w_CONT = "T"
    this.w_RECO = RECCOUNT(this.oParentObject.cTrsName)
    this.w_NabsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_NRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
    this.w_SERIAL = this.oParentObject.w_COSERIAL
    this.w_TIPCON = this.oParentObject.w_COTIPCON
    this.w_CODCON = this.oParentObject.w_COCODCON
    this.w_CODVAL = this.oParentObject.w_COCODVAL
    this.w_SCAINI = this.oParentObject.w_INISCA
    this.w_SCAFIN = this.oParentObject.w_FINSCA
    this.w_TIPPAG = this.oParentObject.w_COCATPAG
    this.w_BANSEL = SPACE(15)
    this.w_CONASS = SPACE(15)
    if this.oParentObject.w_CO__TIPO="I"
      * --- Se cerco Insoluti filtra solo le scadenze Saldate.
      * --- Se attivo il flag raggr. scadenze esegui il raggruppamento.
      if (this.oParentObject.w_FLAGSCA$"ST" AND this.oParentObject.w_COTIPEFF<>"A") OR this.oParentObject.w_COTIPEFF="E"
        * --- Create temporary table TMP_GESPIAN
        i_nIdx=cp_AddTableDef('TMP_GESPIAN') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\SOLL\EXE\QUERY\GSSOC1QIN',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_GESPIAN_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      if this.oParentObject.w_FLAGSCA$"ST" AND this.oParentObject.w_COTIPEFF<>"A"
        vq_exec("..\SOLL\EXE\QUERY\GSSO_GIN.VQR",this,"TEMP")
      else
        if this.oParentObject.w_COTIPEFF="E"
          vq_exec("..\SOLL\EXE\QUERY\GSSO_QIN.VQR",this,"TEMP")
        else
          vq_exec("..\SOLL\EXE\QUERY\GSSOCQIN.VQR",this,"TEMP")
        endif
      endif
      if (this.oParentObject.w_FLAGSCA$"ST" AND this.oParentObject.w_COTIPEFF<>"A") OR this.oParentObject.w_COTIPEFF="E"
        * --- Drop temporary table TMP_GESPIAN
        i_nIdx=cp_GetTableDefIdx('TMP_GESPIAN')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_GESPIAN')
        endif
      endif
    else
      * --- Se cerco Mancato pagamento filtro solo le scadenze Aperte.
      vq_exec("..\SOLL\EXE\QUERY\GSSO2QIN.VQR",this,"TEMP")
      * --- Adeguo cursore per applicare batch di calcolo del residuo
      SELECT PTDATSCA AS DATSCA,PTNUMPAR AS NUMPAR,TIPCON,PTCODCON AS CODCON,FLINDI AS OFLINDI,PTFLINDI AS FLINDI,;
      PTSERIAL,PTROWORD,CPROWNUM,PTFLRAGG,PTCODVAL AS CODVAL,NUMPRO,FLCRSA,PT_SEGNO AS SEGNO,PTTOTIMP AS TOTIMP,;
      MODPAG,BANRIF AS BANNOS,BANAPP,PTCAOVAL AS CAOVAL,CAOAPE,TIPPAG,PTSERRIF,PTORDRIF,PTNUMRIF,IMPDAR,IMPAVE,;
      PTDESRIG,DINUMERO,PTNUMDOC,PTALFDOC,PTDATDOC,PTNUMDIS,FLRAGG,TIPDOC FROM TEMP INTO CURSOR TEMP NOFILTER
      =wrcursor("TEMP")
      GSTE_BCP(this,"S","TEMP"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ripristino cursore di partenza
      SELECT NUMPAR AS PTNUMPAR,DATSCA AS PTDATSCA,"D" AS PT_SEGNO,IIF(SEGNO="A",-ABS(TOTIMP),ABS(TOTIMP)) AS PTTOTIMP,PTNUMDOC,;
      PTALFDOC,PTDATDOC,PTSERIAL,PTROWORD,CPROWNUM,CAOVAL AS PTCAOVAL,PTNUMDIS,CODCON AS PTCODCON,;
      CODVAL AS PTCODVAL,DINUMERO,BANNOS AS BANRIF,OFLINDI AS FLINDI,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,FLRAGG AS PTFLRAGG; 
      FROM TEMP WHERE TIPPAG=this.w_TIPPAG AND FLCRSA="C" INTO CURSOR TEMP1
      SELECT TEMP
      USE
      ah_Msg("Elimina partite saldate...",.T.)
      =wrcursor("TEMP1") 
      vq_exec("..\SOLL\EXE\QUERY\GSSO9QIN.VQR",this,"ELIM")
      if RECCOUNT("ELIM")<>0
        * --- Elimino:
        *     - partite di incasso associate a partite gi� inserite in Insoluti
        *     - partite  associate a partite gi� inserite in Mancati Pagamenti
        Cur=wrcursor("ELIM") 
         Index On (PTSERIAL+ALLTRIM(STR(NVL(PTROWORD,0)))+ALLTRIM(STR(NVL(CPROWNUM,0)))) Tag idx1
        SELECT TEMP1
        GO TOP 
        SCAN
        this.w_SERIAL = PTSERIAL
        this.w_ROWORD = PTROWORD
        this.w_ROWNUM = CPROWNUM
        SELECT ELIM
        if SEEK(this.w_SERIAL+ALLTRIM(STR(this.w_ROWORD))+ALLTRIM(STR(this.w_ROWNUM)))
          SELECT TEMP1
          DELETE
        endif
        ENDSCAN
        SELECT ELIM
        USE
      endif
      vq_exec("..\SOLL\EXE\QUERY\GSSO3QIN.VQR",this,"PARTITE")
      if RECCOUNT("PARTITE")<>0
        * --- Cerco partite di incasso per inserire nel contenzioso i riferimenti dell'ultima partita di incasso
        *     Se non trovo nessun incasso rimangono i riferimenti della partita di creazione.
        =wrcursor("PARTITE") 
         Index On (PTSERRIF+ALLTRIM(STR(NVL(PTORDRIF,0)))+ALLTRIM(STR(NVL(PTNUMRIF,0)))) Tag idx1
        SELECT TEMP1
        GO TOP 
        SCAN
        this.w_SERRIF = NVL(PTSERIAL,SPACE(10))
        this.w_ORDRIF = NVL(PTROWORD,0)
        this.w_NUMRIF = NVL(CPROWNUM,0)
        SELECT PARTITE
        if SEEK(this.w_SERRIF+ALLTRIM(STR(this.w_ORDRIF))+ALLTRIM(STR(this.w_NUMRIF)))
          this.w_SERIAL = PARTITE.PTSERIAL
          this.w_ROWORD = PARTITE.PTROWORD
          this.w_ROWNUM = PARTITE.CPROWNUM
          SELECT TEMP1
          REPLACE PTSERIAL WITH this.w_SERIAL
          REPLACE PTROWORD WITH this.w_ROWORD
          REPLACE CPROWNUM WITH this.w_ROWNUM
          REPLACE PTSERRIF WITH this.w_SERRIF
          REPLACE PTORDRIF WITH this.w_ORDRIF
          REPLACE PTNUMRIF WITH this.w_NUMRIF
        endif
        ENDSCAN
      endif
      SELECT PARTITE
      USE
      SELECT * FROM TEMP1 INTO CURSOR TEMP 
      SELECT TEMP1
      USE
    endif
    * --- Lancio la maschera che conterr� le partite valide
    do GSSO_SI1 with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    DELETE ALL
    this.oParentObject.w_TOTALE = 0
    if this.w_CONT="T"
      this.oParentObject.w_EDITTIPO = .F.
      SELECT * FROM TEMP WHERE XCHK=1 INTO CURSOR TEMP
      this.oParentObject.w_CO__NOTE = ""
      if this.oParentObject.w_FLAGSCA$"ST"
        * --- Verifico effettivamente se devo raggruppare oppure no
        *     nel caso in cui ho un'indiretta effetti non inserita in una distinta
        SELECT TEMP
        GO TOP
        this.w_FLGSCA = Nvl(FLGSCA," ")
      endif
      if this.oParentObject.w_FLAGSCA$"ST" AND this.oParentObject.w_CO__TIPO="I" AND this.oParentObject.w_COTIPEFF<>"A" AND this.w_FLGSCA$"ST"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      DIMENSION ARPARAM[12,2]
      this.w_CODAZI = i_CODAZI
      * --- Read from PAR_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PCDESSUP"+;
          " from "+i_cTable+" PAR_CONT where ";
              +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PCDESSUP;
          from (i_cTable) where;
              PCCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PCDESSUP = NVL(cp_ToDate(_read_.PCDESSUP),cp_NullValue(_read_.PCDESSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_PCDESSUP)
        * --- Array elenco parametri per descrizioni di riga e testata
         
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.oParentObject.w_NUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.oParentObject.w_ALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.oParentObject.w_DATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]="" 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.oParentObject.w_COCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]=alltrim(this.oParentObject.w_CO__NOTE) 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(this.oParentObject.w_NUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.oParentObject.w_DATSCA) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.oParentObject.w_COTIPCON
      endif
      this.oParentObject.w_CO__NOTE = CALDESPA(this.w_PCDESSUP,@ARPARAM,.t.)
      this.oParentObject.w_CODATINT = this.oParentObject.w_DATSCA
      if this.oParentObject.w_CO__TIPO="I" AND g_BANC="S"
        this.oParentObject.w_CODATVAL = this.oParentObject.w_DATSCA
      endif
    else
      this.oParentObject.w_EDITTIPO = .T.
    endif
    if USED("TEMP")
      SELECT TEMP
      USE
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SaveDependsOn()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NabsRow
    .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRelRow
    EndWith
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    GO TOP
    SCAN 
    SELECT (this.oParentObject.cTrsName)
    if this.w_INIT=.T.
      * --- Azzera il Transitorio
      if reccount()<>0
        ZAP
      endif
      this.w_INIT = .F.
    endif
    * --- Nuova Riga del Temporaneo
    this.oParentObject.InitRow()
    this.w_PTDATSCA = CP_TODATE(TEMP.PTDATSCA)
    this.w_NUMDIS = NVL(TEMP.PTNUMDIS," ")
    if this.oParentObject.w_COTIPEFF="E" AND this.w_FLGSCA$"ST"
      this.oParentObject.w_COCODBAN = NVL(TEMP.BANRIF," ")
      this.oParentObject.w_COBANPRE = NVL(TEMP.BANRIF," ")
    else
      * --- Se valorizzata privilegio la nostra banca rispetto a quella indicata nei Parametri
      if this.oParentObject.w_COTIPEFF="E" 
        this.oParentObject.w_COBANPRE = iif(not empty(NVL(TEMP.BANRIF," ")),NVL(TEMP.BANRIF," "),this.oParentObject.w_CONTRO)
        this.oParentObject.w_COCODBAN = iif(not empty(NVL(TEMP.BANRIF," ")),NVL(TEMP.BANRIF," "),this.oParentObject.w_CONTRO)
      else
        this.oParentObject.w_COCONTRO = this.oParentObject.w_CONTRO
        this.oParentObject.w_COCODBAN = this.oParentObject.w_CONTRO
      endif
    endif
    this.oParentObject.w_COSERRIF = TEMP.PTSERIAL
    this.oParentObject.w_COORDRIF = TEMP.PTROWORD
    this.oParentObject.w_NUMPAR = TEMP.PTNUMPAR
    this.oParentObject.w_CONUMRIF = TEMP.CPROWNUM
    this.oParentObject.w_NUMDOC = NVL(TEMP.PTNUMDOC,0)
    this.oParentObject.w_DATSCA = CP_TODATE(TEMP.PTDATSCA)
    this.oParentObject.w_ALFDOC = NVL(TEMP.PTALFDOC,Space(10))
    this.w_SERRIF = Nvl(TEMP.PTSERRIF,space(10))
    this.w_ORDRIF = Nvl(TEMP.PTORDRIF,0)
    this.w_NUMRIF = Nvl(TEMP.PTNUMRIF,0)
    if this.oParentObject.w_CO__TIPO="M"
      this.oParentObject.w_COTOTIMP = NVL(TEMP.PTTOTIMP,0)
    endif
    this.oParentObject.w_SEGNO = Nvl(TEMP.PT_SEGNO," ")
    this.oParentObject.w_TOTIMP = NVL(TEMP.PTTOTIMP,0)
    this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE+this.oParentObject.w_TOTIMP
    this.oParentObject.w_DATDOC = CP_TODATE(TEMP.PTDATDOC)
    this.w_LSEGNO = TEMP.PT_SEGNO
    this.w_PTFLRAGG = NVL(TEMP.PTFLRAGG," ")
    this.w_OKRIF = .F.
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not this.w_OKRIF
      if NOT EMPTY(this.oParentObject.w_DATDOC)
        this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(temp.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(temp.PTALFDOC," ")), " / "+Alltrim(temp.PTALFDOC), ""), DTOC(this.oParentObject.w_DATDOC) )
      else
        this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(NVL(temp.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(temp.PTALFDOC," ")), " / "+Alltrim(temp.PTALFDOC), "") )
      endif
    endif
    this.oParentObject.TrsFromWork
    SELECT TEMP
    endscan
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT TEMP
    GO TOP
    SCAN 
    this.w_FLINDI = NVL(FLINDI," ")
    if NOT EMPTY(NVL(TEMP.PTNUMDIS," "))
      * --- Propone il Conto presente sulla Distinta
      this.w_NUMDIS = NVL(TEMP.NUMDIS," ")
      this.w_PTDATSCA = CP_TODATE(TEMP.PTDATSCA)
      this.w_PTNUMDIS = NVL(TEMP.PTNUMDIS," ")
    endif
    this.w_BANCHE = NVL(TEMP.BANRIF," ")
    if this.oParentObject.w_COTIPEFF="E"
      this.oParentObject.w_COBANPRE = NVL(TEMP.BANRIF," ")
    else
      this.oParentObject.w_COCONTRO = this.oParentObject.w_CONTRO
      this.w_BANCHE = this.oParentObject.w_CONTRO
    endif
    this.w_NSBANC = NVL(TEMP.PTBANNOS,SPACE(15))
    this.w_BANAPP = NVL(TEMP.PTBANAPP,SPACE(10))
    this.w_FLBANC = NVL(TEMP.FLBANC," ")
    this.w_PTCODRAG = IIF(this.w_FLINDI<>"S",NVL(TEMP.PTCODRAG,Space(10)),Space(10))
    this.w_PTNUMCOR = NVL(TEMP.PTNUMCOR,SPACE(25))
    this.w_RTIPCON = this.w_TIPCON
    this.w_RCODCON = this.w_CODCON
    this.w_RDATSCA = this.w_PTDATSCA
    this.w_RNUMDIS = IIF(this.w_FLINDI="S",Space(10),NVL(TEMP.PTNUMDIS," "))
    this.w_FNSBANC = iCase(this.w_FLBANC="A",this.w_BANAPP,this.w_FLBANC="S",NVL(TEMP.PTBANNOS,SPACE(15)),Spcace(10))
    * --- Determina dettaglio partite raggruppate
    * --- Select from GSSORPAR
    do vq_exec with 'GSSORPAR',this,'_Curs_GSSORPAR','',.f.,.t.
    if used('_Curs_GSSORPAR')
      select _Curs_GSSORPAR
      locate for 1=1
      do while not(eof())
      this.w_SERRIF = NVL(_Curs_GSSORPAR.PTSERRIF,SPACE(10))
      this.w_ORDRIF = NVL(_Curs_GSSORPAR.PTORDRIF,0)
      this.w_NUMRIF = NVL(_Curs_GSSORPAR.PTNUMRIF,0)
      if this.w_FLINDI<>"S"
        this.w_PTSERIAL = this.w_RNUMDIS
      else
        * --- Filtro sulla distinta nel caso di Contabilizzazione indiretta
        this.w_PTSERIAL = _Curs_GSSORPAR.PTSERIAL
        this.w_PTCODRAG = NVL(TEMP.PTCODRAG,Space(10))
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNRIFDIS"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNRIFDIS;
            from (i_cTable) where;
                PNSERIAL = this.w_PTSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RIFDIS1 = NVL(cp_ToDate(_read_.PNRIFDIS),cp_NullValue(_read_.PNRIFDIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if VAL(this.w_RIFDIS1)<0
          this.w_DATSCA1 = CP_TODATE(_Curs_GSSORPAR.PTDATSCA)
          this.w_NUMPAR1 = _Curs_GSSORPAR.PTNUMPAR
          * --- Cerco partita fittizia generata dal caricamento di una distinta
          if EMPTY(this.w_SERRIF)
            * --- Gestito pregresso privo di riferimenti
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                  +" where PTROWORD=-2 AND PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR1)+" AND PTDATSCA="+cp_ToStrODBC(this.w_DATSCA1)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" AND PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+"";
                   ,"_Curs_PAR_TITE")
            else
              select * from (i_cTable);
               where PTROWORD=-2 AND PTNUMPAR=this.w_NUMPAR1 AND PTDATSCA=this.w_DATSCA1 AND PTCODCON=this.w_CODCON AND PTTIPCON=this.w_TIPCON;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              * --- Il codice raggruppamento �  valorizzato solo nelle partite da distinta ptroword =-2 devo quindi eseguire
              *     in questo punto il filtro 
              if Nvl(_Curs_PAR_TITE.PTCODRAG,Space(10))=this.w_PTCODRAG
                this.w_PTSERIAL = _Curs_PAR_TITE.PTSERIAL
                exit
              endif
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
          else
            * --- Risalgo alla partita di creazione fittizzia con PTROWORD=-3
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL,PTROWORD,CPROWNUM"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTROWORD = "+cp_ToStrODBC(-3);
                    +" and PTSERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                    +" and PTORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL,PTROWORD,CPROWNUM;
                from (i_cTable) where;
                    PTROWORD = -3;
                    and PTSERRIF = this.w_SERRIF;
                    and PTORDRIF = this.w_ORDRIF;
                    and PTNUMRIF = this.w_NUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ISERIAL = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              this.w_IROWORD = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
              this.w_IROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Determino partita della distinta con PTROWORD=-2
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                  +" where PTROWORD=-2 AND PTSERRIF="+cp_ToStrODBC(this.w_ISERIAL)+" AND PTORDRIF="+cp_ToStrODBC(this.w_IROWORD)+" AND PTNUMRIF="+cp_ToStrODBC(this.w_IROWNUM)+"";
                   ,"_Curs_PAR_TITE")
            else
              select * from (i_cTable);
               where PTROWORD=-2 AND PTSERRIF=this.w_ISERIAL AND PTORDRIF=this.w_IROWORD AND PTNUMRIF=this.w_IROWNUM;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              * --- Il codice raggruppamento �  valorizzato solo nelle partite da distinta ptroword =-2 devo quindi eseguire
              *     in questo punto il filtro 
              if Nvl(_Curs_PAR_TITE.PTCODRAG,Space(10))=this.w_PTCODRAG
                this.w_PTSERIAL = _Curs_PAR_TITE.PTSERIAL
                exit
              endif
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
          endif
        else
          this.w_PTSERIAL = SPACE(10)
        endif
      endif
      if this.w_PTSERIAL=this.w_PTNUMDIS
        SELECT (this.oParentObject.cTrsName)
        if this.w_INIT=.T.
          * --- Azzera il Transitorio
          if reccount()<>0
            ZAP
          endif
          this.w_INIT = .F.
        endif
        * --- Nuova Riga del Temporaneo
        this.oParentObject.InitRow()
        this.oParentObject.w_COSERRIF = _Curs_GSSORPAR.PTSERIAL
        this.oParentObject.w_COORDRIF = _Curs_GSSORPAR.PTROWORD
        this.oParentObject.w_CONUMRIF = _Curs_GSSORPAR.CPROWNUM
        this.oParentObject.w_NUMPAR = _Curs_GSSORPAR.PTNUMPAR
        this.oParentObject.w_NUMDOC = _Curs_GSSORPAR.PTNUMDOC
        this.oParentObject.w_ALFDOC = NVL(_Curs_GSSORPAR.PTALFDOC,Space(10))
        this.oParentObject.w_SEGNO = Nvl(_Curs_GSSORPAR.PT_SEGNO," ")
        * --- Nel  caso di partite non provenienti da  distinte controllo la presenza di abbuoni
        this.oParentObject.w_TOTIMP = IIF(this.oParentObject.w_COTIPEFF="A",(nvl(_Curs_GSSORPAR.PTTOTIMP,0)+ nvl(_Curs_GSSORPAR.PTTOTABB,0)),nvl(_Curs_GSSORPAR.PTTOTIMP,0))
        this.oParentObject.w_TOTIMP = IIF(this.oParentObject.w_SEGNO="A",this.oParentObject.w_TOTIMP,-this.oParentObject.w_TOTIMP)
        this.oParentObject.w_TOTPAR = nvl(_Curs_GSSORPAR.PTTOTIMP,0)
        this.oParentObject.w_TOTABB = nvl(_Curs_GSSORPAR.PTTOTABB,0)
        this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE+this.oParentObject.w_TOTIMP
        this.oParentObject.w_DATSCA = CP_TODATE(_Curs_GSSORPAR.PTDATSCA)
        this.oParentObject.w_DATDOC = CP_TODATE(_Curs_GSSORPAR.PTDATDOC)
        this.w_LSEGNO = _Curs_GSSORPAR.PT_SEGNO
        this.w_PTFLRAGG = _Curs_GSSORPAR.PTFLRAGG
        * --- Se raggruppata cerco riferimenti nelle partite di origine
        this.w_OKRIF = .F.
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.w_OKRIF
          if NOT EMPTY(this.oParentObject.w_DATDOC)
            this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(_Curs_GSSORPAR.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(_Curs_GSSORPAR.PTALFDOC," ")), " / "+Alltrim(_Curs_GSSORPAR.PTALFDOC), ""), DTOC(this.oParentObject.w_DATDOC) )
          else
            this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(NVL(_Curs_GSSORPAR.PTNUMDOC,0),15)) + IIF(NOT EMPTY(NVL(_Curs_GSSORPAR.PTALFDOC," ")), " / "+Alltrim(_Curs_GSSORPAR.PTALFDOC), "") )
          endif
        endif
        this.oParentObject.w_COCODBAN = this.w_BANCHE
        this.oParentObject.TrsFromWork
      endif
        select _Curs_GSSORPAR
        continue
      enddo
      use
    endif
    SELECT TEMP
    endscan
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riferimenti delle partite di origine nel caso fossero state raggruppate manualmente nella Manutenzione partite.
    this.w_LSEGNO = "D"
    this.w_LNUMDOC = 0
    this.w_LALFDOC = SPACE(10)
    this.w_LDATDOC = CP_TODATE("01-01-1960")
    this.w_LSERIAL = "ZZZYYYXXXK"
    this.w_OK = .F.
    if EMPTY(this.w_SERRIF)
      * --- Paertite appartenenti al pregresso
      vq_exec("..\SOLL\EXE\QUERY\GSSORQIN.VQR",this,"PAR_TITE")
      if RECCOUNT() >0
        GO TOP
        this.w_LSERIAL = PTSERIAL
        this.w_PTFLRAGG = IIF(this.oParentObject.w_COTIPEFF="A",NVL(PTFLRAGG," "),this.w_PTFLRAGG)
        if this.w_PTFLRAGG="S" 
          if this.w_LSERIAL<>"ZZZYYYXXXK"
            vq_exec("..\SOLL\EXE\QUERY\GSSOTQIN.VQR",this,"APPO")
          endif
        endif
      endif
      if USED("PAR_TITE")
        select PAR_TITE
        USE
      endif
    else
      vq_exec("..\SOLL\EXE\QUERY\GSSOSQIN.VQR",this,"APPO")
    endif
    if USED("APPO")
      if RECCOUNT("APPO") >0
        SELECT APPO
        GO TOP
        SCAN
        if (this.w_LNUMDOC <> Nvl(PTNUMDOC,0)) OR (this.w_LALFDOC <> Nvl(PTALFDOC,Space(10)))
          this.w_OK = .T.
        else
          this.w_OK = .F.
        endif
        this.w_LNUMDOC = NVL(PTNUMDOC,0)
        this.w_LALFDOC = NVL(PTALFDOC,SPACE(10))
        this.w_LDATDOC = CP_TODATE(PTDATDOC)
        if this.w_OK
          this.w_OKRIF = .T.
          if NOT EMPTY(this.w_LDATDOC)
            this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(this.w_LNUMDOC, 15))+ " " + IIF(NOT EMPTY(this.w_LALFDOC), " / "+Alltrim(this.w_LALFDOC), ""), DTOC(this.w_LDATDOC) )
          else
            this.oParentObject.w_CO__NOTE = this.oParentObject.w_CO__NOTE+ ah_Msgformat("Doc. num. %1%0", ALLTRIM(STR(this.w_LNUMDOC, 15))+ " " + IIF(NOT EMPTY(this.w_LALFDOC), " / "+Alltrim(this.w_LALFDOC), "") )
          endif
        endif
        ENDSCAN
      endif
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='DIS_TINT'
    this.cWorkTables[3]='OUT_PUTS'
    this.cWorkTables[4]='PAR_TITE'
    this.cWorkTables[5]='PNT_MAST'
    this.cWorkTables[6]='*TMP_GESPIAN'
    this.cWorkTables[7]='PAR_CONT'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSSORPAR')
      use in _Curs_GSSORPAR
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
