* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bds                                                        *
*              Zoom disitnte                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-16                                                      *
* Last revis.: 2001-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Pparam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bds",oParentObject,m.Pparam)
return(i_retval)

define class tgsso_bds as StdBatch
  * --- Local variables
  Pparam = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.Pparam="INIT"
        this.oparentobject.notifyevent("Esegui")
      case this.Pparam="SELE"
        this.oparentObject.ecpsave()
        i_retcode = 'stop'
        return
    endcase
  endproc


  proc Init(oParentObject,Pparam)
    this.Pparam=Pparam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Pparam"
endproc
