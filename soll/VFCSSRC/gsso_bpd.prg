* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bpd                                                        *
*              Gestione eventi da piano insoluti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_155]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2011-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bpd",oParentObject,m.pPARAM)
return(i_retval)

define class tgsso_bpd as StdBatch
  * --- Local variables
  pPARAM = space(4)
  w_CODCON = space(15)
  w_TIPCON = space(1)
  w_CODVAL = space(3)
  w_SCAINI = ctod("  /  /  ")
  w_SCAFIN = ctod("  /  /  ")
  w_TIPPAG = space(2)
  w_RIFDIS = space(10)
  w_COCODVAL = space(3)
  w_COCAOVAL = 0
  w_NUMCON = 0
  w_NOTE = space(0)
  w_APPO = space(10)
  w_STATUS = space(2)
  w_COCOMPET = space(4)
  w_AZIONE = space(8)
  w_CPROWNUM = 0
  w_RIFOCN = space(10)
  w_COCODRAG = space(10)
  w_COSERIAL = space(10)
  w_CONUMREG = 0
  w_CO__TIPO = space(1)
  w_COTIPCON = space(1)
  w_CPROWORD = 0
  w_COCODCON = space(15)
  w_CODATREG = ctod("  /  /  ")
  w_COVALNAZ = space(3)
  w_CODRAG = space(10)
  w_COMPET = space(4)
  w_LTIPO = space(1)
  w_COSTATUS = space(2)
  w_CONUMLIV = 0
  w_CODATULT = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_CODCLI = space(15)
  w_COUNT = 0
  w_INTSOLL = 0
  w_COSERRIF = space(10)
  w_COORDRIF = 0
  w_CONUMRIF = 0
  w_NUMDIS = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_COCODBAN = space(15)
  w_ZOOM = space(10)
  * --- WorkFile variables
  CONDTENZ_idx=0
  CON_TENZ_idx=0
  DET_SOLL_idx=0
  DIS_TINT_idx=0
  INC_AVVE_idx=0
  PAR_CONT_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Piano Insoluti (da GSO_API - Evento: Delete Start, Stampa)
    do case
      case this.Pparam="STAM"
        * --- Stampa del Piano Insoluti
        * --- Lancio Stampa Insoluti nel Piano
        this.w_CODRAG = this.oParentObject.w_GPCODRAG
        vq_exec("..\SOLL\EXE\QUERY\GSSO_AIN.VQR",this,"__TMP__")
        CP_CHPRN("..\SOLL\EXE\QUERY\GSSO_AIN.FRX", " ", this)
      case this.Pparam="DELE" AND this.oparentobject.cFunction="Query"
        * --- Controlli in Cancellazione
        if this.oparentobject.cFunction="Query"
          this.w_NUMCON = 0
          * --- Verifica esistenza di Documenti Contabilizzati
          if this.oParentObject.w_TIPO="I"
            * --- Select from CON_TENZ
            i_nConn=i_TableProp[this.CON_TENZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CORIFCON  from "+i_cTable+" CON_TENZ ";
                  +" where COCODRAG="+cp_ToStrODBC(this.oParentObject.w_GPCODRAG)+" AND COCOMPET="+cp_ToStrODBC(this.oParentObject.w_GPCOMPET)+" AND CO__TIPO="+cp_ToStrODBC(this.oParentObject.w_TIPO)+"";
                   ,"_Curs_CON_TENZ")
            else
              select CORIFCON from (i_cTable);
               where COCODRAG=this.oParentObject.w_GPCODRAG AND COCOMPET=this.oParentObject.w_GPCOMPET AND CO__TIPO=this.oParentObject.w_TIPO;
                into cursor _Curs_CON_TENZ
            endif
            if used('_Curs_CON_TENZ')
              select _Curs_CON_TENZ
              locate for 1=1
              do while not(eof())
              if NOT EMPTY(NVL(_Curs_CON_TENZ.CORIFCON," "))
                this.w_NUMCON = this.w_NUMCON + 1
              endif
                select _Curs_CON_TENZ
                continue
              enddo
              use
            endif
          endif
          * --- Test
          if this.w_NUMCON=0 OR this.oParentObject.w_TIPO="M"
            * --- Cancello Insoluti del Piano se non Contabilizzati
            * --- Select from CON_TENZ
            i_nConn=i_TableProp[this.CON_TENZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" CON_TENZ ";
                  +" where CO__TIPO="+cp_ToStrODBC(this.oParentObject.w_TIPO)+" AND COCODRAG= "+cp_ToStrODBC(this.oParentObject.w_GPCODRAG)+" AND COCOMPET="+cp_ToStrODBC(this.oParentObject.w_GPCOMPET)+"";
                   ,"_Curs_CON_TENZ")
            else
              select * from (i_cTable);
               where CO__TIPO=this.oParentObject.w_TIPO AND COCODRAG= this.oParentObject.w_GPCODRAG AND COCOMPET=this.oParentObject.w_GPCOMPET;
                into cursor _Curs_CON_TENZ
            endif
            if used('_Curs_CON_TENZ')
              select _Curs_CON_TENZ
              locate for 1=1
              do while not(eof())
              this.w_COSERIAL = _Curs_CON_TENZ.COSERIAL
              * --- Cancello eventuali Solleciti inviati
              * --- Delete from DET_SOLL
              i_nConn=i_TableProp[this.DET_SOLL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DET_SOLL_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"DSSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
                       )
              else
                delete from (i_cTable) where;
                      DSSERIAL = this.w_COSERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Cancello eventuali Incassi Avvenuti
              * --- Delete from INC_AVVE
              i_nConn=i_TableProp[this.INC_AVVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.INC_AVVE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"IASERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
                       )
              else
                delete from (i_cTable) where;
                      IASERIAL = this.w_COSERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Delete from CONDTENZ
              i_nConn=i_TableProp[this.CONDTENZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
                       )
              else
                delete from (i_cTable) where;
                      COSERIAL = this.w_COSERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Delete from CON_TENZ
              i_nConn=i_TableProp[this.CON_TENZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"COSERIAL = "+cp_ToStrODBC(this.w_COSERIAL);
                       )
              else
                delete from (i_cTable) where;
                      COSERIAL = this.w_COSERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
                select _Curs_CON_TENZ
                continue
              enddo
              use
            endif
            * --- Svuoto lo Zoom 
            SELECT (this.oParentObject.w_CalcZoom.cCursor)
            ZAP
            this.oParentObject.w_CalcZoom.refresh()
          else
            ah_ErrorMsg("Impossibile rimuovere il piano, esistono n.%1 insoluti contabilizzati",,"", ALLTRIM(STR(this.w_NUMCON)) )
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_Msgformat("Cancellazione sospesa")
          endif
        endif
      case this.Pparam="LOAD"
        * --- Carico lo Zoom solo se non sono in Caricamento
        if this.oParentObject.cFunction="Load"
          This.bUpdateParentObject=.f.
        endif
        this.w_ZOOM = this.oParentObject.w_CalcZoom
        This.OparentObject.NotifyEvent("Legge")
        * --- Setta Proprieta' Campi del Cursore
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource) 
          This.w_Zoom.grd.Column&NC..Enabled=.f.
        endif
        ENDFOR
        SELECT (this.w_ZOOM.cCursor)
        GO TOP
        this.w_ZOOM.refresh()
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CONDTENZ'
    this.cWorkTables[2]='CON_TENZ'
    this.cWorkTables[3]='DET_SOLL'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='INC_AVVE'
    this.cWorkTables[6]='PAR_CONT'
    this.cWorkTables[7]='PAR_TITE'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_CON_TENZ')
      use in _Curs_CON_TENZ
    endif
    if used('_Curs_CON_TENZ')
      use in _Curs_CON_TENZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
