* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_arb                                                        *
*              Causali remote banking                                          *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2012-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_arb"))

* --- Class definition
define class tgsso_arb as StdForm
  Top    = 11
  Left   = 16

  * --- Standard Properties
  Width  = 600
  Height = 97+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-16"
  HelpContextID=126457193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  CAU_REBA_IDX = 0
  cFile = "CAU_REBA"
  cKeySelect = "CACODCAU"
  cKeyWhere  = "CACODCAU=this.w_CACODCAU"
  cKeyWhereODBC = '"CACODCAU="+cp_ToStrODBC(this.w_CACODCAU)';

  cKeyWhereODBCqualified = '"CAU_REBA.CACODCAU="+cp_ToStrODBC(this.w_CACODCAU)';

  cPrg = "gsso_arb"
  cComment = "Causali remote banking"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CACODCAU = space(5)
  w_CADESCRI = space(60)
  w_CATIPMOV = space(3)
  w_FILRIC = space(1)
  w_CONRIC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAU_REBA','gsso_arb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_arbPag1","gsso_arb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causali remote banking")
      .Pages(1).HelpContextID = 101155032
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODCAU_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAU_REBA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAU_REBA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAU_REBA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CACODCAU = NVL(CACODCAU,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CAU_REBA where CACODCAU=KeySet.CACODCAU
    *
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAU_REBA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAU_REBA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAU_REBA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODCAU',this.w_CACODCAU  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CONRIC = space(1)
        .w_CACODCAU = NVL(CACODCAU,space(5))
        .w_CADESCRI = NVL(CADESCRI,space(60))
        .w_CATIPMOV = NVL(CATIPMOV,space(3))
        .w_FILRIC = IIF(.w_CATIPMOV='MAN', 'N', '')
        cp_LoadRecExtFlds(this,'CAU_REBA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODCAU = space(5)
      .w_CADESCRI = space(60)
      .w_CATIPMOV = space(3)
      .w_FILRIC = space(1)
      .w_CONRIC = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CATIPMOV = 'MAN'
        .w_FILRIC = IIF(.w_CATIPMOV='MAN', 'N', '')
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAU_REBA')
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODCAU_1_1.enabled = i_bVal
      .Page1.oPag.oCADESCRI_1_2.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODCAU_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODCAU_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CAU_REBA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODCAU,"CACODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCRI,"CADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPMOV,"CATIPMOV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    i_lTable = "CAU_REBA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAU_REBA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSSO_SCR with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CAU_REBA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAU_REBA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAU_REBA')
        i_extval=cp_InsertValODBCExtFlds(this,'CAU_REBA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CACODCAU,CADESCRI,CATIPMOV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CACODCAU)+;
                  ","+cp_ToStrODBC(this.w_CADESCRI)+;
                  ","+cp_ToStrODBC(this.w_CATIPMOV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAU_REBA')
        i_extval=cp_InsertValVFPExtFlds(this,'CAU_REBA')
        cp_CheckDeletedKey(i_cTable,0,'CACODCAU',this.w_CACODCAU)
        INSERT INTO (i_cTable);
              (CACODCAU,CADESCRI,CATIPMOV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CACODCAU;
                  ,this.w_CADESCRI;
                  ,this.w_CATIPMOV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CAU_REBA_IDX,i_nConn)
      *
      * update CAU_REBA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CAU_REBA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
             ",CATIPMOV="+cp_ToStrODBC(this.w_CATIPMOV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CAU_REBA')
        i_cWhere = cp_PKFox(i_cTable  ,'CACODCAU',this.w_CACODCAU  )
        UPDATE (i_cTable) SET;
              CADESCRI=this.w_CADESCRI;
             ,CATIPMOV=this.w_CATIPMOV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CAU_REBA_IDX,i_nConn)
      *
      * delete CAU_REBA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CACODCAU',this.w_CACODCAU  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_FILRIC = IIF(.w_CATIPMOV='MAN', 'N', '')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODCAU_1_1.value==this.w_CACODCAU)
      this.oPgFrm.Page1.oPag.oCACODCAU_1_1.value=this.w_CACODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_2.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_2.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPMOV_1_4.RadioValue()==this.w_CATIPMOV)
      this.oPgFrm.Page1.oPag.oCATIPMOV_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CAU_REBA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CACODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODCAU_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CACODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanAdd()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile aggiungere nuove causali Remote Banking"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=.f.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare causali Remote Banking"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_arbPag1 as StdContainer
  Width  = 596
  height = 97
  stdWidth  = 596
  stdheight = 97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODCAU_1_1 as StdField with uid="XFHXMVVVEF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODCAU", cQueryName = "CACODCAU",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale Remote Banking",;
    HelpContextID = 267788165,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=160, Top=12, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5)

  add object oCADESCRI_1_2 as StdField with uid="IILVCKKSNN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale Remote Banking",;
    HelpContextID = 15724655,;
   bGlobalFont=.t.,;
    Height=21, Width=431, Left=160, Top=42, InputMask=replicate('X',60)


  add object oCATIPMOV_1_4 as StdCombo with uid="JDCXIIDNEO",value=6,rtseq=3,rtrep=.f.,left=160,top=70,width=185,height=21, enabled=.f.;
    , ToolTipText = "Tipologia movimento da generare";
    , HelpContextID = 87756676;
    , cFormVar="w_CATIPMOV",RowSource=""+"Saldaconto,"+"Spese,"+"Richiamo,"+"Insoluto/mancato pagamento,"+"Disimpegna scadenza,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPMOV_1_4.RadioValue()
    return(iif(this.value =1,'SAL',;
    iif(this.value =2,'SPE',;
    iif(this.value =3,'RIC',;
    iif(this.value =4,'MAN',;
    iif(this.value =5,'DIS',;
    iif(this.value =6,'   ',;
    space(3))))))))
  endfunc
  func oCATIPMOV_1_4.GetRadio()
    this.Parent.oContained.w_CATIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oCATIPMOV_1_4.SetRadio()
    this.Parent.oContained.w_CATIPMOV=trim(this.Parent.oContained.w_CATIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPMOV=='SAL',1,;
      iif(this.Parent.oContained.w_CATIPMOV=='SPE',2,;
      iif(this.Parent.oContained.w_CATIPMOV=='RIC',3,;
      iif(this.Parent.oContained.w_CATIPMOV=='MAN',4,;
      iif(this.Parent.oContained.w_CATIPMOV=='DIS',5,;
      iif(this.Parent.oContained.w_CATIPMOV=='',6,;
      0))))))
  endfunc

  add object oStr_1_3 as StdString with uid="PHLIGVKYXX",Visible=.t., Left=11, Top=12,;
    Alignment=1, Width=147, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="ARDNNSLNJY",Visible=.t., Left=5, Top=42,;
    Alignment=1, Width=153, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FBDLYHFGDW",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=153, Height=18,;
    Caption="Tipologia movimento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_arb','CAU_REBA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODCAU=CAU_REBA.CACODCAU";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
