* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mer                                                        *
*              Esiti ricevuti                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-15                                                      *
* Last revis.: 2015-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_mer"))

* --- Class definition
define class tgsso_mer as StdTrsForm
  Top    = 10
  Left   = 7

  * --- Standard Properties
  Width  = 822
  Height = 466+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-16"
  HelpContextID=63542633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=50

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  FLU_ESRI_IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  CAU_REBA_IDX = 0
  DIS_TINT_IDX = 0
  LOG_REBA_IDX = 0
  CON_TENZ_IDX = 0
  GES_PIAN_IDX = 0
  PAR_CONT_IDX = 0
  cFile = "FLU_ESRI"
  cKeySelect = "FRNOMSUP,FRDTARIC,FRNOMFIL"
  cKeyWhere  = "FRNOMSUP=this.w_FRNOMSUP and FRDTARIC=this.w_FRDTARIC and FRNOMFIL=this.w_FRNOMFIL"
  cKeyDetail  = "FRNOMSUP=this.w_FRNOMSUP and FRDTARIC=this.w_FRDTARIC and FRNOMFIL=this.w_FRNOMFIL"
  cKeyWhereODBC = '"FRNOMSUP="+cp_ToStrODBC(this.w_FRNOMSUP)';
      +'+" and FRDTARIC="+cp_ToStrODBC(this.w_FRDTARIC,"D")';
      +'+" and FRNOMFIL="+cp_ToStrODBC(this.w_FRNOMFIL)';

  cKeyDetailWhereODBC = '"FRNOMSUP="+cp_ToStrODBC(this.w_FRNOMSUP)';
      +'+" and FRDTARIC="+cp_ToStrODBC(this.w_FRDTARIC,"D")';
      +'+" and FRNOMFIL="+cp_ToStrODBC(this.w_FRNOMFIL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"FLU_ESRI.FRNOMSUP="+cp_ToStrODBC(this.w_FRNOMSUP)';
      +'+" and FLU_ESRI.FRDTARIC="+cp_ToStrODBC(this.w_FRDTARIC,"D")';
      +'+" and FLU_ESRI.FRNOMFIL="+cp_ToStrODBC(this.w_FRNOMFIL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'FLU_ESRI.FRNUMREC'
  cPrg = "gsso_mer"
  cComment = "Esiti ricevuti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FRNOMSUP = space(20)
  w_FRNUMREC = 0
  w_FRTIPCON = space(1)
  w_FRCODCON = space(15)
  w_FRCODCAU = space(5)
  w_FRDATSCA = ctod('  /  /  ')
  w_FRNUMEFF = 0
  w_FRTOTIMP = 0
  w_FRSPETOT = 0
  w_FRFLGELA = space(1)
  w_FRFLGERR = space(1)
  w_FRFLGFRZ = space(1)
  w_FRNUMCOR = space(15)
  w_FRDATSPE = ctod('  /  /  ')
  w_FRRIFPRO = space(12)
  w_FRDESSUP = space(100)
  w_FRRIFDIS = space(10)
  w_FRRIFTES = space(10)
  w_FRRIFCON = space(10)
  w_FRDTARIC = ctod('  /  /  ')
  w_FRNOMFIL = space(100)
  w_FRTIPFLU = space(2)
  w_RIFTES = space(10)
  w_RIFCON = space(10)
  w_CADESCRI = space(60)
  w_DINUMERO = 0
  w_DI__ANNO = space(4)
  w_OBTEST = ctod('  /  /  ')
  w_ANDTOBSO = ctod('  /  /  ')
  w_ANDESCRI = space(40)
  w_FRFIRRIG = 0
  w_FRLASRIG = 0
  w_BADESCRI = space(35)
  w_PARTSN = space(1)
  w_DIDATDIS = ctod('  /  /  ')
  w_FLAGELA = 0
  w_TOTELA = 0
  w_CATIPMOV = space(3)
  w_ERNOMSUP = space(20)
  w_ERDTARIC = ctod('  /  /  ')
  w_ERNOMFIL = space(100)
  w_ERROWNUM = 0
  w_FRRIFINS = space(10)
  w_COMPET = space(4)
  w_RERNOMFIL = space(100)
  w_RERROWNUM = 0
  w_FRRIGPRO = space(1)
  w_FRCFISDE = space(16)
  w_SAGINT = 0
  w_SPRINT = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FLU_ESRI','gsso_mer')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_merPag1","gsso_mer",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Esito ricevuto")
      .Pages(1).HelpContextID = 173948383
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFRNOMSUP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='CAU_REBA'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='LOG_REBA'
    this.cWorkTables[6]='CON_TENZ'
    this.cWorkTables[7]='GES_PIAN'
    this.cWorkTables[8]='PAR_CONT'
    this.cWorkTables[9]='FLU_ESRI'
    * --- Area Manuale = Open Work Table
    * --- gsso_mer
       * colora le righe che sono errate
              This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
                "IIF(EMPTY(NVL(t_FRFLGERR,0)),RGB(255,255,255),RGB(255,128,128))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FLU_ESRI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FLU_ESRI_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_FRNOMSUP = NVL(FRNOMSUP,space(20))
      .w_FRDTARIC = NVL(FRDTARIC,ctod("  /  /  "))
      .w_FRNOMFIL = NVL(FRNOMFIL,space(100))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_18_joined
    link_2_18_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from FLU_ESRI where FRNOMSUP=KeySet.FRNOMSUP
    *                            and FRDTARIC=KeySet.FRDTARIC
    *                            and FRNOMFIL=KeySet.FRNOMFIL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2],this.bLoadRecFilter,this.FLU_ESRI_IDX,"gsso_mer")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FLU_ESRI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FLU_ESRI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FLU_ESRI '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_18_joined=this.AddJoinedLink_2_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FRNOMSUP',this.w_FRNOMSUP  ,'FRDTARIC',this.w_FRDTARIC  ,'FRNOMFIL',this.w_FRNOMFIL  )
      select * from (i_cTable) FLU_ESRI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTELA = 0
        .w_ERROWNUM = 0
        .w_FRNOMSUP = NVL(FRNOMSUP,space(20))
        .w_FRDTARIC = NVL(cp_ToDate(FRDTARIC),ctod("  /  /  "))
        .w_FRNOMFIL = NVL(FRNOMFIL,space(100))
        .w_FRTIPFLU = NVL(FRTIPFLU,space(2))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_ERNOMSUP = .w_FRNOMSUP
        .w_ERDTARIC = .w_FRDTARIC
        .w_ERNOMFIL = .w_FRNOMFIL
          .link_1_24('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'FLU_ESRI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTELA = 0
      scan
        with this
          .w_RIFTES = space(10)
          .w_RIFCON = space(10)
          .w_CADESCRI = space(60)
          .w_DINUMERO = 0
          .w_DI__ANNO = space(4)
          .w_ANDTOBSO = ctod("  /  /  ")
          .w_ANDESCRI = space(40)
          .w_BADESCRI = space(35)
          .w_PARTSN = space(1)
          .w_DIDATDIS = ctod("  /  /  ")
          .w_CATIPMOV = space(3)
          .w_RERROWNUM = 0
          .w_SAGINT = 0
          .w_SPRINT = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_FRNUMREC = NVL(FRNUMREC,0)
          .w_FRTIPCON = NVL(FRTIPCON,space(1))
          .w_FRCODCON = NVL(FRCODCON,space(15))
          .link_2_3('Load')
          .w_FRCODCAU = NVL(FRCODCAU,space(5))
          if link_2_4_joined
            this.w_FRCODCAU = NVL(CACODCAU204,NVL(this.w_FRCODCAU,space(5)))
            this.w_CADESCRI = NVL(CADESCRI204,space(60))
            this.w_CATIPMOV = NVL(CATIPMOV204,space(3))
          else
          .link_2_4('Load')
          endif
          .w_FRDATSCA = NVL(cp_ToDate(FRDATSCA),ctod("  /  /  "))
          .w_FRNUMEFF = NVL(FRNUMEFF,0)
          .w_FRTOTIMP = NVL(FRTOTIMP,0)
          .w_FRSPETOT = NVL(FRSPETOT,0)
          .w_FRFLGELA = NVL(FRFLGELA,space(1))
          .w_FRFLGERR = NVL(FRFLGERR,space(1))
          .w_FRFLGFRZ = NVL(FRFLGFRZ,space(1))
          .w_FRNUMCOR = NVL(FRNUMCOR,space(15))
          if link_2_12_joined
            this.w_FRNUMCOR = NVL(BACODBAN212,NVL(this.w_FRNUMCOR,space(15)))
            this.w_BADESCRI = NVL(BADESCRI212,space(35))
          else
          .link_2_12('Load')
          endif
          .w_FRDATSPE = NVL(cp_ToDate(FRDATSPE),ctod("  /  /  "))
          .w_FRRIFPRO = NVL(FRRIFPRO,space(12))
          .w_FRDESSUP = NVL(FRDESSUP,space(100))
          .w_FRRIFDIS = NVL(FRRIFDIS,space(10))
          if link_2_16_joined
            this.w_FRRIFDIS = NVL(DINUMDIS216,NVL(this.w_FRRIFDIS,space(10)))
            this.w_DI__ANNO = NVL(DI__ANNO216,space(4))
            this.w_DINUMERO = NVL(DINUMERO216,0)
            this.w_DIDATDIS = NVL(cp_ToDate(DIDATDIS216),ctod("  /  /  "))
          else
          .link_2_16('Load')
          endif
          .w_FRRIFTES = NVL(FRRIFTES,space(10))
          .w_FRRIFCON = NVL(FRRIFCON,space(10))
          if link_2_18_joined
            this.w_FRRIFCON = NVL(COSERIAL218,NVL(this.w_FRRIFCON,space(10)))
            this.w_RIFCON = NVL(COSERIAL218,space(10))
          else
          .link_2_18('Load')
          endif
        .w_OBTEST = .w_FRDTARIC
          .w_FRFIRRIG = NVL(FRFIRRIG,0)
          .w_FRLASRIG = NVL(FRLASRIG,0)
        .w_FLAGELA = IIF(.w_FRFLGELA='S',1,0)
          .w_FRRIFINS = NVL(FRRIFINS,space(10))
          * evitabile
          *.link_2_35('Load')
        .w_COMPET = g_CODESE
        .w_RERNOMFIL = .w_FRNOMFIL
          .link_2_37('Load')
          .w_FRRIGPRO = NVL(FRRIGPRO,space(1))
          .w_FRCFISDE = NVL(FRCFISDE,space(16))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTELA = .w_TOTELA+.w_FLAGELA
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_ERNOMSUP = .w_FRNOMSUP
        .w_ERDTARIC = .w_FRDTARIC
        .w_ERNOMFIL = .w_FRNOMFIL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_13.enabled = .oPgFrm.Page1.oPag.oBtn_1_13.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_14.enabled = .oPgFrm.Page1.oPag.oBtn_1_14.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_21.enabled = .oPgFrm.Page1.oPag.oBtn_1_21.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_39.enabled = .oPgFrm.Page1.oPag.oBtn_2_39.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_40.enabled = .oPgFrm.Page1.oPag.oBtn_2_40.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_41.enabled = .oPgFrm.Page1.oPag.oBtn_2_41.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_FRNOMSUP=space(20)
      .w_FRNUMREC=0
      .w_FRTIPCON=space(1)
      .w_FRCODCON=space(15)
      .w_FRCODCAU=space(5)
      .w_FRDATSCA=ctod("  /  /  ")
      .w_FRNUMEFF=0
      .w_FRTOTIMP=0
      .w_FRSPETOT=0
      .w_FRFLGELA=space(1)
      .w_FRFLGERR=space(1)
      .w_FRFLGFRZ=space(1)
      .w_FRNUMCOR=space(15)
      .w_FRDATSPE=ctod("  /  /  ")
      .w_FRRIFPRO=space(12)
      .w_FRDESSUP=space(100)
      .w_FRRIFDIS=space(10)
      .w_FRRIFTES=space(10)
      .w_FRRIFCON=space(10)
      .w_FRDTARIC=ctod("  /  /  ")
      .w_FRNOMFIL=space(100)
      .w_FRTIPFLU=space(2)
      .w_RIFTES=space(10)
      .w_RIFCON=space(10)
      .w_CADESCRI=space(60)
      .w_DINUMERO=0
      .w_DI__ANNO=space(4)
      .w_OBTEST=ctod("  /  /  ")
      .w_ANDTOBSO=ctod("  /  /  ")
      .w_ANDESCRI=space(40)
      .w_FRFIRRIG=0
      .w_FRLASRIG=0
      .w_BADESCRI=space(35)
      .w_PARTSN=space(1)
      .w_DIDATDIS=ctod("  /  /  ")
      .w_FLAGELA=0
      .w_TOTELA=0
      .w_CATIPMOV=space(3)
      .w_ERNOMSUP=space(20)
      .w_ERDTARIC=ctod("  /  /  ")
      .w_ERNOMFIL=space(100)
      .w_ERROWNUM=0
      .w_FRRIFINS=space(10)
      .w_COMPET=space(4)
      .w_RERNOMFIL=space(100)
      .w_RERROWNUM=0
      .w_FRRIGPRO=space(1)
      .w_FRCFISDE=space(16)
      .w_SAGINT=0
      .w_SPRINT=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_FRCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_FRCODCAU))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,13,.f.)
        if not(empty(.w_FRNUMCOR))
         .link_2_12('Full')
        endif
        .DoRTCalc(14,17,.f.)
        if not(empty(.w_FRRIFDIS))
         .link_2_16('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_FRRIFCON))
         .link_2_18('Full')
        endif
        .DoRTCalc(20,27,.f.)
        .w_OBTEST = .w_FRDTARIC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(29,35,.f.)
        .w_FLAGELA = IIF(.w_FRFLGELA='S',1,0)
        .DoRTCalc(37,38,.f.)
        .w_ERNOMSUP = .w_FRNOMSUP
        .w_ERDTARIC = .w_FRDTARIC
        .w_ERNOMFIL = .w_FRNOMFIL
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_ERNOMFIL))
         .link_1_24('Full')
        endif
        .DoRTCalc(42,43,.f.)
        if not(empty(.w_FRRIFINS))
         .link_2_35('Full')
        endif
        .w_COMPET = g_CODESE
        .w_RERNOMFIL = .w_FRNOMFIL
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_RERNOMFIL))
         .link_2_37('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'FLU_ESRI')
    this.DoRTCalc(46,50,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled = this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_40.enabled = this.oPgFrm.Page1.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_41.enabled = this.oPgFrm.Page1.oPag.oBtn_2_41.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFRNOMSUP_1_1.enabled = i_bVal
      .Page1.oPag.oFRSPETOT_2_8.enabled = i_bVal
      .Page1.oPag.oFRNUMCOR_2_12.enabled = i_bVal
      .Page1.oPag.oFRDATSPE_2_13.enabled = i_bVal
      .Page1.oPag.oFRDESSUP_2_15.enabled = i_bVal
      .Page1.oPag.oFRDTARIC_1_5.enabled = i_bVal
      .Page1.oPag.oFRNOMFIL_1_6.enabled = i_bVal
      .Page1.oPag.oFRTIPFLU_1_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_13.enabled = .Page1.oPag.oBtn_1_13.mCond()
      .Page1.oPag.oBtn_1_14.enabled = .Page1.oPag.oBtn_1_14.mCond()
      .Page1.oPag.oBtn_1_21.enabled = .Page1.oPag.oBtn_1_21.mCond()
      .Page1.oPag.oBtn_2_39.enabled = .Page1.oPag.oBtn_2_39.mCond()
      .Page1.oPag.oBtn_2_40.enabled = .Page1.oPag.oBtn_2_40.mCond()
      .Page1.oPag.oBtn_2_41.enabled = .Page1.oPag.oBtn_2_41.mCond()
      .Page1.oPag.oBtn_2_42.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFRNOMSUP_1_1.enabled = .f.
        .Page1.oPag.oFRDTARIC_1_5.enabled = .f.
        .Page1.oPag.oFRNOMFIL_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFRNOMSUP_1_1.enabled = .t.
        .Page1.oPag.oFRDTARIC_1_5.enabled = .t.
        .Page1.oPag.oFRNOMFIL_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'FLU_ESRI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRNOMSUP,"FRNOMSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRDTARIC,"FRDTARIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRNOMFIL,"FRNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRTIPFLU,"FRTIPFLU",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
    i_lTable = "FLU_ESRI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FLU_ESRI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSSO_SER with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_FRNUMREC N(10);
      ,t_FRTIPCON C(1);
      ,t_FRCODCON C(15);
      ,t_FRCODCAU C(5);
      ,t_FRDATSCA D(8);
      ,t_FRNUMEFF N(6);
      ,t_FRTOTIMP N(18,4);
      ,t_FRSPETOT N(18,4);
      ,t_FRFLGELA N(3);
      ,t_FRFLGERR N(3);
      ,t_FRFLGFRZ N(3);
      ,t_FRNUMCOR C(15);
      ,t_FRDATSPE D(8);
      ,t_FRRIFPRO C(12);
      ,t_FRDESSUP C(100);
      ,t_CADESCRI C(60);
      ,t_DINUMERO N(6);
      ,t_DI__ANNO C(4);
      ,t_ANDESCRI C(40);
      ,t_BADESCRI C(35);
      ,t_DIDATDIS D(8);
      ,t_FRRIGPRO N(3);
      ,t_FRCFISDE C(16);
      ,CPROWNUM N(10);
      ,t_FRRIFDIS C(10);
      ,t_FRRIFTES C(10);
      ,t_FRRIFCON C(10);
      ,t_RIFTES C(10);
      ,t_RIFCON C(10);
      ,t_OBTEST D(8);
      ,t_ANDTOBSO D(8);
      ,t_FRFIRRIG N(6);
      ,t_FRLASRIG N(6);
      ,t_PARTSN C(1);
      ,t_FLAGELA N(3);
      ,t_CATIPMOV C(3);
      ,t_FRRIFINS C(10);
      ,t_COMPET C(4);
      ,t_RERNOMFIL C(100);
      ,t_RERROWNUM N(4);
      ,t_SAGINT N(6,2);
      ,t_SPRINT C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_merbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMREC_2_1.controlsource=this.cTrsName+'.t_FRNUMREC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRTIPCON_2_2.controlsource=this.cTrsName+'.t_FRTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCON_2_3.controlsource=this.cTrsName+'.t_FRCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCAU_2_4.controlsource=this.cTrsName+'.t_FRCODCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRDATSCA_2_5.controlsource=this.cTrsName+'.t_FRDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMEFF_2_6.controlsource=this.cTrsName+'.t_FRNUMEFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRTOTIMP_2_7.controlsource=this.cTrsName+'.t_FRTOTIMP'
    this.oPgFRm.Page1.oPag.oFRSPETOT_2_8.controlsource=this.cTrsName+'.t_FRSPETOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.controlsource=this.cTrsName+'.t_FRFLGELA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.controlsource=this.cTrsName+'.t_FRFLGERR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.controlsource=this.cTrsName+'.t_FRFLGFRZ'
    this.oPgFRm.Page1.oPag.oFRNUMCOR_2_12.controlsource=this.cTrsName+'.t_FRNUMCOR'
    this.oPgFRm.Page1.oPag.oFRDATSPE_2_13.controlsource=this.cTrsName+'.t_FRDATSPE'
    this.oPgFRm.Page1.oPag.oFRRIFPRO_2_14.controlsource=this.cTrsName+'.t_FRRIFPRO'
    this.oPgFRm.Page1.oPag.oFRDESSUP_2_15.controlsource=this.cTrsName+'.t_FRDESSUP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCADESCRI_2_21.controlsource=this.cTrsName+'.t_CADESCRI'
    this.oPgFRm.Page1.oPag.oDINUMERO_2_22.controlsource=this.cTrsName+'.t_DINUMERO'
    this.oPgFRm.Page1.oPag.oDI__ANNO_2_24.controlsource=this.cTrsName+'.t_DI__ANNO'
    this.oPgFRm.Page1.oPag.oANDESCRI_2_27.controlsource=this.cTrsName+'.t_ANDESCRI'
    this.oPgFRm.Page1.oPag.oBADESCRI_2_30.controlsource=this.cTrsName+'.t_BADESCRI'
    this.oPgFRm.Page1.oPag.oDIDATDIS_2_32.controlsource=this.cTrsName+'.t_DIDATDIS'
    this.oPgFRm.Page1.oPag.oFRRIGPRO_2_43.controlsource=this.cTrsName+'.t_FRRIGPRO'
    this.oPgFRm.Page1.oPag.oFRCFISDE_2_44.controlsource=this.cTrsName+'.t_FRCFISDE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(93)
    this.AddVLine(220)
    this.AddVLine(279)
    this.AddVLine(443)
    this.AddVLine(518)
    this.AddVLine(579)
    this.AddVLine(723)
    this.AddVLine(749)
    this.AddVLine(775)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMREC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
      *
      * insert into FLU_ESRI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FLU_ESRI')
        i_extval=cp_InsertValODBCExtFlds(this,'FLU_ESRI')
        i_cFldBody=" "+;
                  "(FRNOMSUP,FRNUMREC,FRTIPCON,FRCODCON,FRCODCAU"+;
                  ",FRDATSCA,FRNUMEFF,FRTOTIMP,FRSPETOT,FRFLGELA"+;
                  ",FRFLGERR,FRFLGFRZ,FRNUMCOR,FRDATSPE,FRRIFPRO"+;
                  ",FRDESSUP,FRRIFDIS,FRRIFTES,FRRIFCON,FRDTARIC"+;
                  ",FRNOMFIL,FRTIPFLU,FRFIRRIG,FRLASRIG,FRRIFINS"+;
                  ",FRRIGPRO,FRCFISDE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_FRNOMSUP)+","+cp_ToStrODBC(this.w_FRNUMREC)+","+cp_ToStrODBC(this.w_FRTIPCON)+","+cp_ToStrODBCNull(this.w_FRCODCON)+","+cp_ToStrODBCNull(this.w_FRCODCAU)+;
             ","+cp_ToStrODBC(this.w_FRDATSCA)+","+cp_ToStrODBC(this.w_FRNUMEFF)+","+cp_ToStrODBC(this.w_FRTOTIMP)+","+cp_ToStrODBC(this.w_FRSPETOT)+","+cp_ToStrODBC(this.w_FRFLGELA)+;
             ","+cp_ToStrODBC(this.w_FRFLGERR)+","+cp_ToStrODBC(this.w_FRFLGFRZ)+","+cp_ToStrODBCNull(this.w_FRNUMCOR)+","+cp_ToStrODBC(this.w_FRDATSPE)+","+cp_ToStrODBC(this.w_FRRIFPRO)+;
             ","+cp_ToStrODBC(this.w_FRDESSUP)+","+cp_ToStrODBCNull(this.w_FRRIFDIS)+","+cp_ToStrODBC(this.w_FRRIFTES)+","+cp_ToStrODBCNull(this.w_FRRIFCON)+","+cp_ToStrODBC(this.w_FRDTARIC)+;
             ","+cp_ToStrODBC(this.w_FRNOMFIL)+","+cp_ToStrODBC(this.w_FRTIPFLU)+","+cp_ToStrODBC(this.w_FRFIRRIG)+","+cp_ToStrODBC(this.w_FRLASRIG)+","+cp_ToStrODBCNull(this.w_FRRIFINS)+;
             ","+cp_ToStrODBC(this.w_FRRIGPRO)+","+cp_ToStrODBC(this.w_FRCFISDE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FLU_ESRI')
        i_extval=cp_InsertValVFPExtFlds(this,'FLU_ESRI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'FRNOMSUP',this.w_FRNOMSUP,'FRDTARIC',this.w_FRDTARIC,'FRNOMFIL',this.w_FRNOMFIL)
        INSERT INTO (i_cTable) (;
                   FRNOMSUP;
                  ,FRNUMREC;
                  ,FRTIPCON;
                  ,FRCODCON;
                  ,FRCODCAU;
                  ,FRDATSCA;
                  ,FRNUMEFF;
                  ,FRTOTIMP;
                  ,FRSPETOT;
                  ,FRFLGELA;
                  ,FRFLGERR;
                  ,FRFLGFRZ;
                  ,FRNUMCOR;
                  ,FRDATSPE;
                  ,FRRIFPRO;
                  ,FRDESSUP;
                  ,FRRIFDIS;
                  ,FRRIFTES;
                  ,FRRIFCON;
                  ,FRDTARIC;
                  ,FRNOMFIL;
                  ,FRTIPFLU;
                  ,FRFIRRIG;
                  ,FRLASRIG;
                  ,FRRIFINS;
                  ,FRRIGPRO;
                  ,FRCFISDE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_FRNOMSUP;
                  ,this.w_FRNUMREC;
                  ,this.w_FRTIPCON;
                  ,this.w_FRCODCON;
                  ,this.w_FRCODCAU;
                  ,this.w_FRDATSCA;
                  ,this.w_FRNUMEFF;
                  ,this.w_FRTOTIMP;
                  ,this.w_FRSPETOT;
                  ,this.w_FRFLGELA;
                  ,this.w_FRFLGERR;
                  ,this.w_FRFLGFRZ;
                  ,this.w_FRNUMCOR;
                  ,this.w_FRDATSPE;
                  ,this.w_FRRIFPRO;
                  ,this.w_FRDESSUP;
                  ,this.w_FRRIFDIS;
                  ,this.w_FRRIFTES;
                  ,this.w_FRRIFCON;
                  ,this.w_FRDTARIC;
                  ,this.w_FRNOMFIL;
                  ,this.w_FRTIPFLU;
                  ,this.w_FRFIRRIG;
                  ,this.w_FRLASRIG;
                  ,this.w_FRRIFINS;
                  ,this.w_FRRIGPRO;
                  ,this.w_FRCFISDE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_FRNUMREC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'FLU_ESRI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " FRTIPFLU="+cp_ToStrODBC(this.w_FRTIPFLU)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'FLU_ESRI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  FRTIPFLU=this.w_FRTIPFLU;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_FRNUMREC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update FLU_ESRI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'FLU_ESRI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " FRNUMREC="+cp_ToStrODBC(this.w_FRNUMREC)+;
                     ",FRTIPCON="+cp_ToStrODBC(this.w_FRTIPCON)+;
                     ",FRCODCON="+cp_ToStrODBCNull(this.w_FRCODCON)+;
                     ",FRCODCAU="+cp_ToStrODBCNull(this.w_FRCODCAU)+;
                     ",FRDATSCA="+cp_ToStrODBC(this.w_FRDATSCA)+;
                     ",FRNUMEFF="+cp_ToStrODBC(this.w_FRNUMEFF)+;
                     ",FRTOTIMP="+cp_ToStrODBC(this.w_FRTOTIMP)+;
                     ",FRSPETOT="+cp_ToStrODBC(this.w_FRSPETOT)+;
                     ",FRFLGELA="+cp_ToStrODBC(this.w_FRFLGELA)+;
                     ",FRFLGERR="+cp_ToStrODBC(this.w_FRFLGERR)+;
                     ",FRFLGFRZ="+cp_ToStrODBC(this.w_FRFLGFRZ)+;
                     ",FRNUMCOR="+cp_ToStrODBCNull(this.w_FRNUMCOR)+;
                     ",FRDATSPE="+cp_ToStrODBC(this.w_FRDATSPE)+;
                     ",FRRIFPRO="+cp_ToStrODBC(this.w_FRRIFPRO)+;
                     ",FRDESSUP="+cp_ToStrODBC(this.w_FRDESSUP)+;
                     ",FRRIFDIS="+cp_ToStrODBCNull(this.w_FRRIFDIS)+;
                     ",FRRIFTES="+cp_ToStrODBC(this.w_FRRIFTES)+;
                     ",FRRIFCON="+cp_ToStrODBCNull(this.w_FRRIFCON)+;
                     ",FRTIPFLU="+cp_ToStrODBC(this.w_FRTIPFLU)+;
                     ",FRFIRRIG="+cp_ToStrODBC(this.w_FRFIRRIG)+;
                     ",FRLASRIG="+cp_ToStrODBC(this.w_FRLASRIG)+;
                     ",FRRIFINS="+cp_ToStrODBCNull(this.w_FRRIFINS)+;
                     ",FRRIGPRO="+cp_ToStrODBC(this.w_FRRIGPRO)+;
                     ",FRCFISDE="+cp_ToStrODBC(this.w_FRCFISDE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'FLU_ESRI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      FRNUMREC=this.w_FRNUMREC;
                     ,FRTIPCON=this.w_FRTIPCON;
                     ,FRCODCON=this.w_FRCODCON;
                     ,FRCODCAU=this.w_FRCODCAU;
                     ,FRDATSCA=this.w_FRDATSCA;
                     ,FRNUMEFF=this.w_FRNUMEFF;
                     ,FRTOTIMP=this.w_FRTOTIMP;
                     ,FRSPETOT=this.w_FRSPETOT;
                     ,FRFLGELA=this.w_FRFLGELA;
                     ,FRFLGERR=this.w_FRFLGERR;
                     ,FRFLGFRZ=this.w_FRFLGFRZ;
                     ,FRNUMCOR=this.w_FRNUMCOR;
                     ,FRDATSPE=this.w_FRDATSPE;
                     ,FRRIFPRO=this.w_FRRIFPRO;
                     ,FRDESSUP=this.w_FRDESSUP;
                     ,FRRIFDIS=this.w_FRRIFDIS;
                     ,FRRIFTES=this.w_FRRIFTES;
                     ,FRRIFCON=this.w_FRRIFCON;
                     ,FRTIPFLU=this.w_FRTIPFLU;
                     ,FRFIRRIG=this.w_FRFIRRIG;
                     ,FRLASRIG=this.w_FRLASRIG;
                     ,FRRIFINS=this.w_FRRIFINS;
                     ,FRRIGPRO=this.w_FRRIGPRO;
                     ,FRCFISDE=this.w_FRCFISDE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- gsso_mer
    this.NotifyEvent('DeleteFlusso')
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_FRNUMREC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete FLU_ESRI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_FRNUMREC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_2_3('Full')
        .DoRTCalc(5,16,.t.)
          .link_2_16('Full')
        .DoRTCalc(18,18,.t.)
          .link_2_18('Full')
        .DoRTCalc(20,27,.t.)
          .w_OBTEST = .w_FRDTARIC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(29,35,.t.)
          .w_TOTELA = .w_TOTELA-.w_flagela
          .w_FLAGELA = IIF(.w_FRFLGELA='S',1,0)
          .w_TOTELA = .w_TOTELA+.w_flagela
        .DoRTCalc(37,38,.t.)
          .w_ERNOMSUP = .w_FRNOMSUP
          .w_ERDTARIC = .w_FRDTARIC
          .w_ERNOMFIL = .w_FRNOMFIL
          .link_1_24('Full')
        .DoRTCalc(42,42,.t.)
          .link_2_35('Full')
          .w_COMPET = g_CODESE
          .w_RERNOMFIL = .w_FRNOMFIL
          .link_2_37('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(46,50,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FRRIFDIS with this.w_FRRIFDIS
      replace t_FRRIFTES with this.w_FRRIFTES
      replace t_FRRIFCON with this.w_FRRIFCON
      replace t_RIFTES with this.w_RIFTES
      replace t_RIFCON with this.w_RIFCON
      replace t_OBTEST with this.w_OBTEST
      replace t_ANDTOBSO with this.w_ANDTOBSO
      replace t_FRFIRRIG with this.w_FRFIRRIG
      replace t_FRLASRIG with this.w_FRLASRIG
      replace t_PARTSN with this.w_PARTSN
      replace t_FLAGELA with this.w_FLAGELA
      replace t_CATIPMOV with this.w_CATIPMOV
      replace t_FRRIFINS with this.w_FRRIFINS
      replace t_COMPET with this.w_COMPET
      replace t_RERNOMFIL with this.w_RERNOMFIL
      replace t_RERROWNUM with this.w_RERROWNUM
      replace t_SAGINT with this.w_SAGINT
      replace t_SPRINT with this.w_SPRINT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRCODCAU_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRCODCAU_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRDATSCA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRDATSCA_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRNUMEFF_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRNUMEFF_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRTOTIMP_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFRTOTIMP_2_7.mCond()
    this.oPgFrm.Page1.oPag.oFRSPETOT_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oFRSPETOT_2_8.mCond()
    this.oPgFrm.Page1.oPag.oFRDATSPE_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oFRDATSPE_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_39.enabled =this.oPgFrm.Page1.oPag.oBtn_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_40.enabled =this.oPgFrm.Page1.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_41.enabled =this.oPgFrm.Page1.oPag.oBtn_2_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_42.enabled =this.oPgFrm.Page1.oPag.oBtn_2_42.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_39.visible=!this.oPgFrm.Page1.oPag.oBtn_2_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_40.visible=!this.oPgFrm.Page1.oPag.oBtn_2_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_41.visible=!this.oPgFrm.Page1.oPag.oBtn_2_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_42.visible=!this.oPgFrm.Page1.oPag.oBtn_2_42.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FRCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN,ANSAGINT,ANSPRINT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FRCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FRTIPCON;
                       ,'ANCODICE',this.w_FRCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN,ANSAGINT,ANSPRINT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANDTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_SAGINT = NVL(_Link_.ANSAGINT,0)
      this.w_SPRINT = NVL(_Link_.ANSPRINT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_FRCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANDTOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
      this.w_SAGINT = 0
      this.w_SPRINT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO) And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto o non gestito a partite")
        endif
        this.w_FRCODCON = space(15)
        this.w_ANDESCRI = space(40)
        this.w_ANDTOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
        this.w_SAGINT = 0
        this.w_SPRINT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FRCODCAU
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_REBA_IDX,3]
    i_lTable = "CAU_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2], .t., this.CAU_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSRB_ARB',True,'CAU_REBA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODCAU like "+cp_ToStrODBC(trim(this.w_FRCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI,CATIPMOV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODCAU',trim(this.w_FRCODCAU))
          select CACODCAU,CADESCRI,CATIPMOV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FRCODCAU)==trim(_Link_.CACODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FRCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_REBA','*','CACODCAU',cp_AbsName(oSource.parent,'oFRCODCAU_2_4'),i_cWhere,'GSRB_ARB',"CAUSALI REMOTE BANKING",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI,CATIPMOV";
                     +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',oSource.xKey(1))
            select CACODCAU,CADESCRI,CATIPMOV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAU,CADESCRI,CATIPMOV";
                   +" from "+i_cTable+" "+i_lTable+" where CACODCAU="+cp_ToStrODBC(this.w_FRCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAU',this.w_FRCODCAU)
            select CACODCAU,CADESCRI,CATIPMOV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRCODCAU = NVL(_Link_.CACODCAU,space(5))
      this.w_CADESCRI = NVL(_Link_.CADESCRI,space(60))
      this.w_CATIPMOV = NVL(_Link_.CATIPMOV,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_FRCODCAU = space(5)
      endif
      this.w_CADESCRI = space(60)
      this.w_CATIPMOV = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])+'\'+cp_ToStr(_Link_.CACODCAU,1)
      cp_ShowWarn(i_cKey,this.CAU_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_REBA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_REBA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CACODCAU as CACODCAU204"+ ",link_2_4.CADESCRI as CADESCRI204"+ ",link_2_4.CATIPMOV as CATIPMOV204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on FLU_ESRI.FRCODCAU=link_2_4.CACODCAU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and FLU_ESRI.FRCODCAU=link_2_4.CACODCAU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=FRNUMCOR
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_FRNUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_FRNUMCOR))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FRNUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FRNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oFRNUMCOR_2_12'),i_cWhere,'GSTE_ACB',"CONTI BANCHE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_FRNUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_FRNUMCOR)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRNUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FRNUMCOR = space(15)
      endif
      this.w_BADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.BACODBAN as BACODBAN212"+ ",link_2_12.BADESCRI as BADESCRI212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on FLU_ESRI.FRNUMCOR=link_2_12.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and FLU_ESRI.FRNUMCOR=link_2_12.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=FRRIFDIS
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_lTable = "DIS_TINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2], .t., this.DIS_TINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRRIFDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRRIFDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DINUMDIS="+cp_ToStrODBC(this.w_FRRIFDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DINUMDIS',this.w_FRRIFDIS)
            select DINUMDIS,DI__ANNO,DINUMERO,DIDATDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRRIFDIS = NVL(_Link_.DINUMDIS,space(10))
      this.w_DI__ANNO = NVL(_Link_.DI__ANNO,space(4))
      this.w_DINUMERO = NVL(_Link_.DINUMERO,0)
      this.w_DIDATDIS = NVL(cp_ToDate(_Link_.DIDATDIS),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FRRIFDIS = space(10)
      endif
      this.w_DI__ANNO = space(4)
      this.w_DINUMERO = 0
      this.w_DIDATDIS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])+'\'+cp_ToStr(_Link_.DINUMDIS,1)
      cp_ShowWarn(i_cKey,this.DIS_TINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRRIFDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIS_TINT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.DINUMDIS as DINUMDIS216"+ ",link_2_16.DI__ANNO as DI__ANNO216"+ ",link_2_16.DINUMERO as DINUMERO216"+ ",link_2_16.DIDATDIS as DIDATDIS216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on FLU_ESRI.FRRIFDIS=link_2_16.DINUMDIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and FLU_ESRI.FRRIFDIS=link_2_16.DINUMDIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=FRRIFCON
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_lTable = "CON_TENZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2], .t., this.CON_TENZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRRIFCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRRIFCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_FRRIFCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_FRRIFCON)
            select COSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRRIFCON = NVL(_Link_.COSERIAL,space(10))
      this.w_RIFCON = NVL(_Link_.COSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_FRRIFCON = space(10)
      endif
      this.w_RIFCON = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TENZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRRIFCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_TENZ_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_18.COSERIAL as COSERIAL218"+ ",link_2_18.COSERIAL as COSERIAL218"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_18 on FLU_ESRI.FRRIFCON=link_2_18.COSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_18"
          i_cKey=i_cKey+'+" and FLU_ESRI.FRRIFCON=link_2_18.COSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ERNOMFIL
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_lTable = "LOG_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2], .t., this.LOG_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ERNOMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ERNOMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where ERNOMFIL="+cp_ToStrODBC(this.w_ERNOMFIL);
                   +" and ERNOMSUP="+cp_ToStrODBC(this.w_ERNOMSUP);
                   +" and ERDTARIC="+cp_ToStrODBC(this.w_ERDTARIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ERNOMSUP',this.w_ERNOMSUP;
                       ,'ERDTARIC',this.w_ERDTARIC;
                       ,'ERNOMFIL',this.w_ERNOMFIL)
            select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ERNOMFIL = NVL(_Link_.ERNOMFIL,space(100))
      this.w_ERROWNUM = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_ERNOMFIL = space(100)
      endif
      this.w_ERROWNUM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])+'\'+cp_ToStr(_Link_.ERNOMSUP,1)+'\'+cp_ToStr(_Link_.ERDTARIC,1)+'\'+cp_ToStr(_Link_.ERNOMFIL,1)
      cp_ShowWarn(i_cKey,this.LOG_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ERNOMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FRRIFINS
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GES_PIAN_IDX,3]
    i_lTable = "GES_PIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2], .t., this.GES_PIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRRIFINS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRRIFINS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODRAG";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODRAG="+cp_ToStrODBC(this.w_FRRIFINS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODRAG',this.w_FRRIFINS)
            select GPCODRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRRIFINS = NVL(_Link_.GPCODRAG,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_FRRIFINS = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GES_PIAN_IDX,2])+'\'+cp_ToStr(_Link_.GPCODRAG,1)
      cp_ShowWarn(i_cKey,this.GES_PIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRRIFINS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RERNOMFIL
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_lTable = "LOG_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2], .t., this.LOG_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RERNOMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RERNOMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where ERNOMFIL="+cp_ToStrODBC(this.w_RERNOMFIL);
                   +" and ERNOMSUP="+cp_ToStrODBC(this.w_ERNOMSUP);
                   +" and ERDTARIC="+cp_ToStrODBC(this.w_ERDTARIC);
                   +" and ERNOMFIL="+cp_ToStrODBC(this.w_FRNOMFIL);
                   +" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ERNOMSUP',this.w_ERNOMSUP;
                       ,'ERDTARIC',this.w_ERDTARIC;
                       ,'ERNOMFIL',this.w_FRNOMFIL;
                       ,'CPROWNUM',this.w_CPROWNUM;
                       ,'ERNOMFIL',this.w_RERNOMFIL)
            select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RERNOMFIL = NVL(_Link_.ERNOMFIL,space(100))
      this.w_RERROWNUM = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_RERNOMFIL = space(100)
      endif
      this.w_RERROWNUM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])+'\'+cp_ToStr(_Link_.ERNOMSUP,1)+'\'+cp_ToStr(_Link_.ERDTARIC,1)+'\'+cp_ToStr(_Link_.ERNOMFIL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)+'\'+cp_ToStr(_Link_.ERNOMFIL,1)
      cp_ShowWarn(i_cKey,this.LOG_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RERNOMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oFRNOMSUP_1_1.value==this.w_FRNOMSUP)
      this.oPgFrm.Page1.oPag.oFRNOMSUP_1_1.value=this.w_FRNOMSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oFRSPETOT_2_8.value==this.w_FRSPETOT)
      this.oPgFrm.Page1.oPag.oFRSPETOT_2_8.value=this.w_FRSPETOT
      replace t_FRSPETOT with this.oPgFrm.Page1.oPag.oFRSPETOT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRNUMCOR_2_12.value==this.w_FRNUMCOR)
      this.oPgFrm.Page1.oPag.oFRNUMCOR_2_12.value=this.w_FRNUMCOR
      replace t_FRNUMCOR with this.oPgFrm.Page1.oPag.oFRNUMCOR_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDATSPE_2_13.value==this.w_FRDATSPE)
      this.oPgFrm.Page1.oPag.oFRDATSPE_2_13.value=this.w_FRDATSPE
      replace t_FRDATSPE with this.oPgFrm.Page1.oPag.oFRDATSPE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRRIFPRO_2_14.value==this.w_FRRIFPRO)
      this.oPgFrm.Page1.oPag.oFRRIFPRO_2_14.value=this.w_FRRIFPRO
      replace t_FRRIFPRO with this.oPgFrm.Page1.oPag.oFRRIFPRO_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDESSUP_2_15.value==this.w_FRDESSUP)
      this.oPgFrm.Page1.oPag.oFRDESSUP_2_15.value=this.w_FRDESSUP
      replace t_FRDESSUP with this.oPgFrm.Page1.oPag.oFRDESSUP_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDTARIC_1_5.value==this.w_FRDTARIC)
      this.oPgFrm.Page1.oPag.oFRDTARIC_1_5.value=this.w_FRDTARIC
    endif
    if not(this.oPgFrm.Page1.oPag.oFRNOMFIL_1_6.value==this.w_FRNOMFIL)
      this.oPgFrm.Page1.oPag.oFRNOMFIL_1_6.value=this.w_FRNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFRTIPFLU_1_10.value==this.w_FRTIPFLU)
      this.oPgFrm.Page1.oPag.oFRTIPFLU_1_10.value=this.w_FRTIPFLU
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMERO_2_22.value==this.w_DINUMERO)
      this.oPgFrm.Page1.oPag.oDINUMERO_2_22.value=this.w_DINUMERO
      replace t_DINUMERO with this.oPgFrm.Page1.oPag.oDINUMERO_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDI__ANNO_2_24.value==this.w_DI__ANNO)
      this.oPgFrm.Page1.oPag.oDI__ANNO_2_24.value=this.w_DI__ANNO
      replace t_DI__ANNO with this.oPgFrm.Page1.oPag.oDI__ANNO_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_2_27.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_2_27.value=this.w_ANDESCRI
      replace t_ANDESCRI with this.oPgFrm.Page1.oPag.oANDESCRI_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_2_30.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_2_30.value=this.w_BADESCRI
      replace t_BADESCRI with this.oPgFrm.Page1.oPag.oBADESCRI_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATDIS_2_32.value==this.w_DIDATDIS)
      this.oPgFrm.Page1.oPag.oDIDATDIS_2_32.value=this.w_DIDATDIS
      replace t_DIDATDIS with this.oPgFrm.Page1.oPag.oDIDATDIS_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRRIGPRO_2_43.RadioValue()==this.w_FRRIGPRO)
      this.oPgFrm.Page1.oPag.oFRRIGPRO_2_43.SetRadio()
      replace t_FRRIGPRO with this.oPgFrm.Page1.oPag.oFRRIGPRO_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oFRCFISDE_2_44.value==this.w_FRCFISDE)
      this.oPgFrm.Page1.oPag.oFRCFISDE_2_44.value=this.w_FRCFISDE
      replace t_FRCFISDE with this.oPgFrm.Page1.oPag.oFRCFISDE_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMREC_2_1.value==this.w_FRNUMREC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMREC_2_1.value=this.w_FRNUMREC
      replace t_FRNUMREC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMREC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTIPCON_2_2.value==this.w_FRTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTIPCON_2_2.value=this.w_FRTIPCON
      replace t_FRTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTIPCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCON_2_3.value==this.w_FRCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCON_2_3.value=this.w_FRCODCON
      replace t_FRCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCAU_2_4.value==this.w_FRCODCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCAU_2_4.value=this.w_FRCODCAU
      replace t_FRCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCAU_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRDATSCA_2_5.value==this.w_FRDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRDATSCA_2_5.value=this.w_FRDATSCA
      replace t_FRDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRDATSCA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMEFF_2_6.value==this.w_FRNUMEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMEFF_2_6.value=this.w_FRNUMEFF
      replace t_FRNUMEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRNUMEFF_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTOTIMP_2_7.value==this.w_FRTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTOTIMP_2_7.value=this.w_FRTOTIMP
      replace t_FRTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRTOTIMP_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.RadioValue()==this.w_FRFLGELA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.SetRadio()
      replace t_FRFLGELA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.RadioValue()==this.w_FRFLGERR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.SetRadio()
      replace t_FRFLGERR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.RadioValue()==this.w_FRFLGFRZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.SetRadio()
      replace t_FRFLGFRZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESCRI_2_21.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESCRI_2_21.value=this.w_CADESCRI
      replace t_CADESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESCRI_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'FLU_ESRI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FRNOMSUP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oFRNOMSUP_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FRNOMSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ERNOMSUP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oERNOMSUP_1_22.SetFocus()
            i_bnoObbl = !empty(.w_ERNOMSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_FRNUMREC));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_ANDTOBSO>.w_OBTEST OR EMPTY(.w_ANDTOBSO) And .w_PARTSN='S') and not(empty(.w_FRCODCON)) and (NOT EMPTY(.w_FRNUMREC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCON_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto o non gestito a partite")
        case   empty(.w_FRCODCAU) and (.w_FRFLGERR='S') and (NOT EMPTY(.w_FRNUMREC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRCODCAU_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_FRNUMCOR) and (NOT EMPTY(.w_FRNUMREC))
          .oNewFocus=.oPgFrm.Page1.oPag.oFRNUMCOR_2_12
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if NOT EMPTY(.w_FRNUMREC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_FRNUMREC))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=NVL(t_FRFLGELA,0)<>1
    if !i_bRes
      cp_ErrorMsg("Impossibile eliminare riga elaborata","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_FRNUMREC=0
      .w_FRTIPCON=space(1)
      .w_FRCODCON=space(15)
      .w_FRCODCAU=space(5)
      .w_FRDATSCA=ctod("  /  /  ")
      .w_FRNUMEFF=0
      .w_FRTOTIMP=0
      .w_FRSPETOT=0
      .w_FRFLGELA=space(1)
      .w_FRFLGERR=space(1)
      .w_FRFLGFRZ=space(1)
      .w_FRNUMCOR=space(15)
      .w_FRDATSPE=ctod("  /  /  ")
      .w_FRRIFPRO=space(12)
      .w_FRDESSUP=space(100)
      .w_FRRIFDIS=space(10)
      .w_FRRIFTES=space(10)
      .w_FRRIFCON=space(10)
      .w_RIFTES=space(10)
      .w_RIFCON=space(10)
      .w_CADESCRI=space(60)
      .w_DINUMERO=0
      .w_DI__ANNO=space(4)
      .w_OBTEST=ctod("  /  /  ")
      .w_ANDTOBSO=ctod("  /  /  ")
      .w_ANDESCRI=space(40)
      .w_FRFIRRIG=0
      .w_FRLASRIG=0
      .w_BADESCRI=space(35)
      .w_PARTSN=space(1)
      .w_DIDATDIS=ctod("  /  /  ")
      .w_FLAGELA=0
      .w_CATIPMOV=space(3)
      .w_FRRIFINS=space(10)
      .w_COMPET=space(4)
      .w_RERNOMFIL=space(100)
      .w_RERROWNUM=0
      .w_FRRIGPRO=space(1)
      .w_FRCFISDE=space(16)
      .w_SAGINT=0
      .w_SPRINT=space(1)
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_FRCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_FRCODCAU))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,13,.f.)
      if not(empty(.w_FRNUMCOR))
        .link_2_12('Full')
      endif
      .DoRTCalc(14,17,.f.)
      if not(empty(.w_FRRIFDIS))
        .link_2_16('Full')
      endif
      .DoRTCalc(18,19,.f.)
      if not(empty(.w_FRRIFCON))
        .link_2_18('Full')
      endif
      .DoRTCalc(20,27,.f.)
        .w_OBTEST = .w_FRDTARIC
      .DoRTCalc(29,35,.f.)
        .w_FLAGELA = IIF(.w_FRFLGELA='S',1,0)
      .DoRTCalc(37,43,.f.)
      if not(empty(.w_FRRIFINS))
        .link_2_35('Full')
      endif
        .w_COMPET = g_CODESE
        .w_RERNOMFIL = .w_FRNOMFIL
      .DoRTCalc(45,45,.f.)
      if not(empty(.w_RERNOMFIL))
        .link_2_37('Full')
      endif
    endwith
    this.DoRTCalc(46,50,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_FRNUMREC = t_FRNUMREC
    this.w_FRTIPCON = t_FRTIPCON
    this.w_FRCODCON = t_FRCODCON
    this.w_FRCODCAU = t_FRCODCAU
    this.w_FRDATSCA = t_FRDATSCA
    this.w_FRNUMEFF = t_FRNUMEFF
    this.w_FRTOTIMP = t_FRTOTIMP
    this.w_FRSPETOT = t_FRSPETOT
    this.w_FRFLGELA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.RadioValue(.t.)
    this.w_FRFLGERR = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.RadioValue(.t.)
    this.w_FRFLGFRZ = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.RadioValue(.t.)
    this.w_FRNUMCOR = t_FRNUMCOR
    this.w_FRDATSPE = t_FRDATSPE
    this.w_FRRIFPRO = t_FRRIFPRO
    this.w_FRDESSUP = t_FRDESSUP
    this.w_FRRIFDIS = t_FRRIFDIS
    this.w_FRRIFTES = t_FRRIFTES
    this.w_FRRIFCON = t_FRRIFCON
    this.w_RIFTES = t_RIFTES
    this.w_RIFCON = t_RIFCON
    this.w_CADESCRI = t_CADESCRI
    this.w_DINUMERO = t_DINUMERO
    this.w_DI__ANNO = t_DI__ANNO
    this.w_OBTEST = t_OBTEST
    this.w_ANDTOBSO = t_ANDTOBSO
    this.w_ANDESCRI = t_ANDESCRI
    this.w_FRFIRRIG = t_FRFIRRIG
    this.w_FRLASRIG = t_FRLASRIG
    this.w_BADESCRI = t_BADESCRI
    this.w_PARTSN = t_PARTSN
    this.w_DIDATDIS = t_DIDATDIS
    this.w_FLAGELA = t_FLAGELA
    this.w_CATIPMOV = t_CATIPMOV
    this.w_FRRIFINS = t_FRRIFINS
    this.w_COMPET = t_COMPET
    this.w_RERNOMFIL = t_RERNOMFIL
    this.w_RERROWNUM = t_RERROWNUM
    this.w_FRRIGPRO = this.oPgFrm.Page1.oPag.oFRRIGPRO_2_43.RadioValue(.t.)
    this.w_FRCFISDE = t_FRCFISDE
    this.w_SAGINT = t_SAGINT
    this.w_SPRINT = t_SPRINT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_FRNUMREC with this.w_FRNUMREC
    replace t_FRTIPCON with this.w_FRTIPCON
    replace t_FRCODCON with this.w_FRCODCON
    replace t_FRCODCAU with this.w_FRCODCAU
    replace t_FRDATSCA with this.w_FRDATSCA
    replace t_FRNUMEFF with this.w_FRNUMEFF
    replace t_FRTOTIMP with this.w_FRTOTIMP
    replace t_FRSPETOT with this.w_FRSPETOT
    replace t_FRFLGELA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGELA_2_9.ToRadio()
    replace t_FRFLGERR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGERR_2_10.ToRadio()
    replace t_FRFLGFRZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFRFLGFRZ_2_11.ToRadio()
    replace t_FRNUMCOR with this.w_FRNUMCOR
    replace t_FRDATSPE with this.w_FRDATSPE
    replace t_FRRIFPRO with this.w_FRRIFPRO
    replace t_FRDESSUP with this.w_FRDESSUP
    replace t_FRRIFDIS with this.w_FRRIFDIS
    replace t_FRRIFTES with this.w_FRRIFTES
    replace t_FRRIFCON with this.w_FRRIFCON
    replace t_RIFTES with this.w_RIFTES
    replace t_RIFCON with this.w_RIFCON
    replace t_CADESCRI with this.w_CADESCRI
    replace t_DINUMERO with this.w_DINUMERO
    replace t_DI__ANNO with this.w_DI__ANNO
    replace t_OBTEST with this.w_OBTEST
    replace t_ANDTOBSO with this.w_ANDTOBSO
    replace t_ANDESCRI with this.w_ANDESCRI
    replace t_FRFIRRIG with this.w_FRFIRRIG
    replace t_FRLASRIG with this.w_FRLASRIG
    replace t_BADESCRI with this.w_BADESCRI
    replace t_PARTSN with this.w_PARTSN
    replace t_DIDATDIS with this.w_DIDATDIS
    replace t_FLAGELA with this.w_FLAGELA
    replace t_CATIPMOV with this.w_CATIPMOV
    replace t_FRRIFINS with this.w_FRRIFINS
    replace t_COMPET with this.w_COMPET
    replace t_RERNOMFIL with this.w_RERNOMFIL
    replace t_RERROWNUM with this.w_RERROWNUM
    replace t_FRRIGPRO with this.oPgFrm.Page1.oPag.oFRRIGPRO_2_43.ToRadio()
    replace t_FRCFISDE with this.w_FRCFISDE
    replace t_SAGINT with this.w_SAGINT
    replace t_SPRINT with this.w_SPRINT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTELA = .w_TOTELA-.w_flagela
        .SetControlsValue()
      endwith
  EndProc
  func CanDelete()
    local i_res
    i_res=this.w_TOTELA=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Esistono delle righe elaborate per cui non � consentita la cancellazione"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_merPag1 as StdContainer
  Width  = 818
  height = 466
  stdWidth  = 818
  stdheight = 466
  resizeXpos=189
  resizeYpos=212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFRNOMSUP_1_1 as StdField with uid="BJNCVUVGPD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FRNOMSUP", cQueryName = "FRNOMSUP",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome supporto flusso di ritorno",;
    HelpContextID = 73048486,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=227, Top=17, InputMask=replicate('X',20)

  add object oFRDTARIC_1_5 as StdField with uid="WGXMORKMLB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FRDTARIC", cQueryName = "FRNOMSUP,FRDTARIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione flusso di ritorno",;
    HelpContextID = 43975065,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=654, Top=17

  add object oFRNOMFIL_1_6 as StdField with uid="HALKSXAOVZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_FRNOMFIL", cQueryName = "FRNOMSUP,FRDTARIC,FRNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome file flusso di ritorno",;
    HelpContextID = 123380130,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=227, Top=42, InputMask=replicate('X',100)

  func oFRNOMFIL_1_6.mCond()
    with this.Parent.oContained
        if .nLastRow>1
          return (.f.)
        endif
    endwith
  endfunc

  func oFRNOMFIL_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_RERNOMFIL)
        bRes2=.link_2_37('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFRTIPFLU_1_10 as StdField with uid="LKDZHZXYZF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_FRTIPFLU", cQueryName = "FRTIPFLU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Tipo flusso",;
    HelpContextID = 142278229,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=227, Top=65, InputMask=replicate('X',2)


  add object oBtn_1_13 as StdButton with uid="ATWJDENOIZ",left=682, top=42, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il flusso di ritorno";
    , HelpContextID = 48311126;
    , Caption='\<Flusso';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_MFR', 'FLNOMSUP', .w_FRNOMSUP, 'FLDTARIC', .w_FRDTARIC, 'FLNOMFIL', .w_FRNOMFIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    with this.Parent.oContained
      return (not empty(nvl(.w_FRNOMSUP,'')))
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="JKDXEKYIKS",left=741, top=42, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log";
    , HelpContextID = 63091018;
    , Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSSO_BAE(this.Parent.oContained,.w_FRNOMSUP,.w_FRDTARIC,.w_FRNOMFIL,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    with this.Parent.oContained
      return (nvl(.w_ERROWNUM,0)<>0)
    endwith
  endfunc


  add object oObj_1_17 as cp_runprogram with uid="EXGNTTCBBN",left=2, top=484, width=266,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSO_BER(w_FRNOMSUP,w_FRDTARIC,w_FRNOMFIL,'F')",;
    cEvent = "DeleteFlusso",;
    nPag=1;
    , HelpContextID = 114455014


  add object oObj_1_18 as cp_runprogram with uid="HDFMWSYSWP",left=283, top=484, width=266,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsso_BDL(w_FRNOMSUP,w_FRDTARIC,w_FRNOMFIL,'F')",;
    cEvent = "DeleteFlusso",;
    nPag=1;
    , HelpContextID = 114455014


  add object oBtn_1_21 as StdButton with uid="IPAUOXIXUO",left=741, top=313, width=48,height=45,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il flusso di ritorno";
    , HelpContextID = 190951237;
    , Caption='\<Riga flus.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_MFR', 'FLNOMSUP', .w_FRNOMSUP, 'FLDTARIC', .w_FRDTARIC, 'FLNOMFIL', .w_FRNOMFIL, 'CPROWNUM', IIF(.w_FRTIPFLU='SD',.w_FRLASRIG,.w_FRFIRRIG))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    with this.Parent.oContained
      return (not empty(nvl(.w_FRNOMSUP,'')))
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=94, width=812,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=11,Field1="FRNUMREC",Label1="N.rec.",Field2="FRTIPCON",Label2="Tipo",Field3="FRCODCON",Label3="Codice conto",Field4="FRCODCAU",Label4="Causale esito",Field5="CADESCRI",Label5="Descrizione",Field6="FRDATSCA",Label6="Data scad.",Field7="FRNUMEFF",Label7="N.effetto",Field8="FRTOTIMP",Label8="Importo scadenza",Field9="FRFLGELA",Label9="Ela.",Field10="FRFLGERR",Label10="Err.",Field11="FRFLGFRZ",Label11="For.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252548218

  add object oStr_1_2 as StdString with uid="RGLAOCONVH",Visible=.t., Left=139, Top=66,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo flusso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="HYICLWGSGX",Visible=.t., Left=240, Top=392,;
    Alignment=1, Width=142, Height=18,;
    Caption="Spese addebitate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="KKQOHMVONM",Visible=.t., Left=8, Top=392,;
    Alignment=1, Width=139, Height=18,;
    Caption="Data valuta spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="JANPERCFJY",Visible=.t., Left=2, Top=17,;
    Alignment=1, Width=220, Height=19,;
    Caption="Nome supporto flusso di ritorno:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="NJXTCDDCWP",Visible=.t., Left=483, Top=17,;
    Alignment=1, Width=168, Height=18,;
    Caption="Data creazione ricezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XOOAQFWHXO",Visible=.t., Left=139, Top=42,;
    Alignment=1, Width=83, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TOIXUDWNYT",Visible=.t., Left=8, Top=418,;
    Alignment=1, Width=139, Height=18,;
    Caption="Promemoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZEDYKJTTRM",Visible=.t., Left=8, Top=445,;
    Alignment=1, Width=139, Height=18,;
    Caption="Descr. supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NWVFXODLGC",Visible=.t., Left=8, Top=339,;
    Alignment=1, Width=139, Height=18,;
    Caption="Descrizione conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="NRYUQNGMIR",Visible=.t., Left=8, Top=365,;
    Alignment=1, Width=139, Height=18,;
    Caption="Rif.distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QWNLJUXBVQ",Visible=.t., Left=4, Top=313,;
    Alignment=1, Width=143, Height=18,;
    Caption="Banca assuntrice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PJWBHDYTWI",Visible=.t., Left=288, Top=365,;
    Alignment=1, Width=94, Height=18,;
    Caption="Data distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="XJYBUGVNOP",Visible=.t., Left=450, Top=339,;
    Alignment=1, Width=88, Height=18,;
    Caption="Codice fisc.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=113,;
    width=808+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=114,width=807+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAU_REBA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oFRSPETOT_2_8.Refresh()
      this.Parent.oFRNUMCOR_2_12.Refresh()
      this.Parent.oFRDATSPE_2_13.Refresh()
      this.Parent.oFRRIFPRO_2_14.Refresh()
      this.Parent.oFRDESSUP_2_15.Refresh()
      this.Parent.oDINUMERO_2_22.Refresh()
      this.Parent.oDI__ANNO_2_24.Refresh()
      this.Parent.oANDESCRI_2_27.Refresh()
      this.Parent.oBADESCRI_2_30.Refresh()
      this.Parent.oDIDATDIS_2_32.Refresh()
      this.Parent.oFRRIGPRO_2_43.Refresh()
      this.Parent.oFRCFISDE_2_44.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAU_REBA'
        oDropInto=this.oBodyCol.oRow.oFRCODCAU_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oFRSPETOT_2_8 as StdTrsField with uid="HTUUANOZCG",rtseq=9,rtrep=.t.,;
    cFormVar="w_FRSPETOT",value=0,;
    ToolTipText = "Spese addebitate",;
    HelpContextID = 186912342,;
    cTotal="", bFixedPos=.t., cQueryName = "FRSPETOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=386, Top=392, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oFRSPETOT_2_8.mCond()
    with this.Parent.oContained
      return (.w_FRFLGELA<>'S')
    endwith
  endfunc

  add object oFRNUMCOR_2_12 as StdTrsField with uid="XMHGAMWLIV",rtseq=13,rtrep=.t.,;
    cFormVar="w_FRNUMCOR",value=space(15),;
    ToolTipText = "Banca assuntrice",;
    HelpContextID = 194993752,;
    cTotal="", bFixedPos=.t., cQueryName = "FRNUMCOR",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=152, Top=313, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_FRNUMCOR"

  func oFRNUMCOR_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFRNUMCOR_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oFRNUMCOR_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oFRNUMCOR_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"CONTI BANCHE",'',this.parent.oContained
  endproc
  proc oFRNUMCOR_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_FRNUMCOR
    i_obj.ecpSave()
  endproc

  add object oFRDATSPE_2_13 as StdTrsField with uid="WAZJJFRVQC",rtseq=14,rtrep=.t.,;
    cFormVar="w_FRDATSPE",value=ctod("  /  /  "),;
    ToolTipText = "Data valuta spese",;
    HelpContextID = 189005413,;
    cTotal="", bFixedPos=.t., cQueryName = "FRDATSPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=152, Top=392

  func oFRDATSPE_2_13.mCond()
    with this.Parent.oContained
      return (.w_FRFLGELA<>'S')
    endwith
  endfunc

  add object oFRRIFPRO_2_14 as StdTrsField with uid="LZWOKGOPDH",rtseq=15,rtrep=.t.,;
    cFormVar="w_FRRIFPRO",value=space(12),enabled=.f.,;
    ToolTipText = "Riferimento promemoria contabile",;
    HelpContextID = 14999973,;
    cTotal="", bFixedPos=.t., cQueryName = "FRRIFPRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=152, Top=418, InputMask=replicate('X',12)

  add object oFRDESSUP_2_15 as StdTrsField with uid="RPESPIUUXN",rtseq=16,rtrep=.t.,;
    cFormVar="w_FRDESSUP",value=space(100),;
    ToolTipText = "Descrizione supplementare",;
    HelpContextID = 78643622,;
    cTotal="", bFixedPos=.t., cQueryName = "FRDESSUP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=517, Left=152, Top=445, InputMask=replicate('X',100)

  add object oDINUMERO_2_22 as StdTrsField with uid="ZTJCGWMRVF",rtseq=26,rtrep=.t.,;
    cFormVar="w_DINUMERO",value=0,enabled=.f.,;
    ToolTipText = "Numero distinta",;
    HelpContextID = 161441659,;
    cTotal="", bFixedPos=.t., cQueryName = "DINUMERO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=152, Top=365, cSayPict=["999999"], cGetPict=["999999"]

  add object oDI__ANNO_2_24 as StdTrsField with uid="OMSJMYNPMA",rtseq=27,rtrep=.t.,;
    cFormVar="w_DI__ANNO",value=space(4),enabled=.f.,;
    ToolTipText = "Anno di riferimento distinta",;
    HelpContextID = 22304635,;
    cTotal="", bFixedPos=.t., cQueryName = "DI__ANNO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=221, Top=365, InputMask=replicate('X',4)

  add object oANDESCRI_2_27 as StdTrsField with uid="OZRQOCPFZR",rtseq=30,rtrep=.t.,;
    cFormVar="w_ANDESCRI",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione conto",;
    HelpContextID = 189792945,;
    cTotal="", bFixedPos=.t., cQueryName = "ANDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=152, Top=339, InputMask=replicate('X',40)

  add object oBADESCRI_2_30 as StdTrsField with uid="WPJWRVMDCA",rtseq=33,rtrep=.t.,;
    cFormVar="w_BADESCRI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione banca",;
    HelpContextID = 189796257,;
    cTotal="", bFixedPos=.t., cQueryName = "BADESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=301, Left=276, Top=313, InputMask=replicate('X',35)

  add object oDIDATDIS_2_32 as StdTrsField with uid="KFXUQALIOT",rtseq=35,rtrep=.t.,;
    cFormVar="w_DIDATDIS",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data distinta",;
    HelpContextID = 96204937,;
    cTotal="", bFixedPos=.t., cQueryName = "DIDATDIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=386, Top=365

  add object oBtn_2_39 as StdButton with uid="ZUJXFLWHLZ",width=48,height=45,;
   left=682, top=363,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare il contenzioso";
    , HelpContextID = 251543980;
    , Caption='\<Contenz.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_39.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_MCO', 'COSERIAL', .w_FRRIFCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_39.mCond()
    with this.Parent.oContained
      return (not empty(NVL(.w_RIFCON,'')))
    endwith
  endfunc

  func oBtn_2_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(NVL(.w_RIFCON,'')))
    endwith
   endif
  endfunc

  add object oBtn_2_40 as StdButton with uid="YVVPJYLBUU",width=48,height=45,;
   left=741, top=363,;
    CpPicture="bmp\log.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare il log";
    , HelpContextID = 27403286;
    , Caption='R\<iga log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      with this.Parent.oContained
        do GSSO_BE1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_40.mCond()
    with this.Parent.oContained
      return (.w_FRFLGERR='S')
    endwith
  endfunc

  func oBtn_2_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FRFLGERR<>'S')
    endwith
   endif
  endfunc

  add object oBtn_2_41 as StdButton with uid="ZVPVNYCPCU",width=48,height=45,;
   left=629, top=363,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare l'insoluto";
    , HelpContextID = 141364725;
    , Caption='I\<nsoluto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_API', 'GPCODRAG', .w_FRRIFINS,'GPCOMPET',.w_COMPET)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_41.mCond()
    with this.Parent.oContained
      return (not empty(NVL(.w_FRRIFINS,'')))
    endwith
  endfunc

  func oBtn_2_41.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(NVL(.w_FRRIFINS,'')))
    endwith
   endif
  endfunc

  add object oBtn_2_42 as StdButton with uid="XOCJTHTCLO",width=48,height=45,;
   left=741, top=414,;
    CpPicture="bmp\abbina.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per abbinare gli effetti";
    , HelpContextID = 110817642;
    , Caption='\<Abb.scad.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      do GSSO_KER with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_42.mCond()
    with this.Parent.oContained
      return (not empty(nvl(.w_FRNOMSUP,'')) and nvl(.w_FRFLGELA,' ')<>'S' and nvl(.w_FRFLGFRZ,' ')<>'S' and .cFunction='Edit' and .w_FRRIGPRO<>'S' and .Rowstatus()<>'A')
    endwith
  endfunc

  func oBtn_2_42.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (nvl(.w_FRFLGELA,' ')='S' OR nvl(.w_FRFLGFRZ,' ')='S')
    endwith
   endif
  endfunc

  add object oFRRIGPRO_2_43 as StdTrsCheck with uid="TDQYVGXEDG",rtrep=.t.,;
    cFormVar="w_FRRIGPRO", enabled=.f., caption="Riepilogativo contabile",;
    ToolTipText = "Riepilogativo contabile",;
    HelpContextID = 16048549,;
    Left=386, Top=418,;
    cTotal="", cQueryName = "FRRIGPRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oFRRIGPRO_2_43.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FRRIGPRO,&i_cF..t_FRRIGPRO),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFRRIGPRO_2_43.GetRadio()
    this.Parent.oContained.w_FRRIGPRO = this.RadioValue()
    return .t.
  endfunc

  func oFRRIGPRO_2_43.ToRadio()
    this.Parent.oContained.w_FRRIGPRO=trim(this.Parent.oContained.w_FRRIGPRO)
    return(;
      iif(this.Parent.oContained.w_FRRIGPRO=='S',1,;
      0))
  endfunc

  func oFRRIGPRO_2_43.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFRCFISDE_2_44 as StdTrsField with uid="ZSFRUUNVEH",rtseq=48,rtrep=.t.,;
    cFormVar="w_FRCFISDE",value=space(16),enabled=.f.,;
    ToolTipText = "Codice fiscale del debitore",;
    HelpContextID = 68219291,;
    cTotal="", bFixedPos=.t., cQueryName = "FRCFISDE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=544, Top=339, InputMask=replicate('X',16)

  add object oStr_2_23 as StdString with uid="JSBQUBCPDE",Visible=.t., Left=210, Top=365,;
    Alignment=0, Width=9, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsso_merBodyRow as CPBodyRowCnt
  Width=798
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oFRNUMREC_2_1 as StdTrsField with uid="NBAYANXOXU",rtseq=2,rtrep=.t.,;
    cFormVar="w_FRNUMREC",value=0,;
    ToolTipText = "Numero record",;
    HelpContextID = 56664473,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=56, Left=-2, Top=0, cSayPict=["9999999999"], cGetPict=["9999999999"], BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1)

  add object oFRTIPCON_2_2 as StdTrsField with uid="OLEQQLZVWP",rtseq=3,rtrep=.t.,;
    cFormVar="w_FRTIPCON",value=space(1),enabled=.f.,;
    ToolTipText = "Tipo conto",;
    HelpContextID = 192609884,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=60, Top=0, cSayPict=["X"], cGetPict=["X"], InputMask=replicate('X',1)

  func oFRTIPCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FRCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFRCODCON_2_3 as StdTrsField with uid="OPZILIKSKL",rtseq=4,rtrep=.t.,;
    cFormVar="w_FRCODCON",value=space(15),enabled=.f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 204869212,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto o non gestito a partite",;
   bGlobalFont=.t.,;
    Height=17, Width=123, Left=90, Top=0, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FRCODCON"

  func oFRCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oFRCODCAU_2_4 as StdTrsField with uid="ETWAZICXJK",rtseq=5,rtrep=.t.,;
    cFormVar="w_FRCODCAU",value=space(5),;
    ToolTipText = "Causale esito",;
    HelpContextID = 204869205,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=52, Left=218, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_REBA", cZoomOnZoom="GSRB_ARB", oKey_1_1="CACODCAU", oKey_1_2="this.w_FRCODCAU"

  func oFRCODCAU_2_4.mCond()
    with this.Parent.oContained
      return (.w_FRFLGERR='S')
    endwith
  endfunc

  func oFRCODCAU_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oFRCODCAU_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oFRCODCAU_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_REBA','*','CACODCAU',cp_AbsName(this.parent,'oFRCODCAU_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSRB_ARB',"CAUSALI REMOTE BANKING",'',this.parent.oContained
  endproc
  proc oFRCODCAU_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSRB_ARB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODCAU=this.parent.oContained.w_FRCODCAU
    i_obj.ecpSave()
  endproc

  add object oFRDATSCA_2_5 as StdTrsField with uid="EKUGYNMATT",rtseq=6,rtrep=.t.,;
    cFormVar="w_FRDATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data scadenza",;
    HelpContextID = 79430039,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=441, Top=0

  func oFRDATSCA_2_5.mCond()
    with this.Parent.oContained
      return (.w_FRFLGERR='S')
    endwith
  endfunc

  add object oFRNUMEFF_2_6 as StdTrsField with uid="SWXSHWTRFO",rtseq=7,rtrep=.t.,;
    cFormVar="w_FRNUMEFF",value=0,;
    ToolTipText = "Numero effetto",;
    HelpContextID = 106996124,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=517, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  func oFRNUMEFF_2_6.mCond()
    with this.Parent.oContained
      return (.w_FRFLGERR='S')
    endwith
  endfunc

  add object oFRTOTIMP_2_7 as StdTrsField with uid="GNOLSRMSCB",rtseq=8,rtrep=.t.,;
    cFormVar="w_FRTOTIMP",value=0,;
    ToolTipText = "Importo scadenza",;
    HelpContextID = 87359066,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=577, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oFRTOTIMP_2_7.mCond()
    with this.Parent.oContained
      return (.w_FRFLGERR='S')
    endwith
  endfunc

  add object oFRFLGELA_2_9 as StdTrsCheck with uid="FLMQUEUYVI",rtrep=.t.,;
    cFormVar="w_FRFLGELA", enabled=.f., caption="",;
    ToolTipText = "Elaborato",;
    HelpContextID = 168353385,;
    Left=721, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oFRFLGELA_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FRFLGELA,&i_cF..t_FRFLGELA),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFRFLGELA_2_9.GetRadio()
    this.Parent.oContained.w_FRFLGELA = this.RadioValue()
    return .t.
  endfunc

  func oFRFLGELA_2_9.ToRadio()
    this.Parent.oContained.w_FRFLGELA=trim(this.Parent.oContained.w_FRFLGELA)
    return(;
      iif(this.Parent.oContained.w_FRFLGELA=='S',1,;
      0))
  endfunc

  func oFRFLGELA_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFRFLGERR_2_10 as StdTrsCheck with uid="UXBXQRMJOL",rtrep=.t.,;
    cFormVar="w_FRFLGERR", enabled=.f., caption="",;
    ToolTipText = "Errato",;
    HelpContextID = 168353368,;
    Left=747, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oFRFLGERR_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FRFLGERR,&i_cF..t_FRFLGERR),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFRFLGERR_2_10.GetRadio()
    this.Parent.oContained.w_FRFLGERR = this.RadioValue()
    return .t.
  endfunc

  func oFRFLGERR_2_10.ToRadio()
    this.Parent.oContained.w_FRFLGERR=trim(this.Parent.oContained.w_FRFLGERR)
    return(;
      iif(this.Parent.oContained.w_FRFLGERR=='S',1,;
      0))
  endfunc

  func oFRFLGERR_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFRFLGFRZ_2_11 as StdTrsCheck with uid="RSPWIHKBRG",rtrep=.t.,;
    cFormVar="w_FRFLGFRZ",  caption="",;
    ToolTipText = "Forzato",;
    HelpContextID = 151576144,;
    Left=773, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oFRFLGFRZ_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FRFLGFRZ,&i_cF..t_FRFLGFRZ),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oFRFLGFRZ_2_11.GetRadio()
    this.Parent.oContained.w_FRFLGFRZ = this.RadioValue()
    return .t.
  endfunc

  func oFRFLGFRZ_2_11.ToRadio()
    this.Parent.oContained.w_FRFLGFRZ=trim(this.Parent.oContained.w_FRFLGFRZ)
    return(;
      iif(this.Parent.oContained.w_FRFLGFRZ=='S',1,;
      0))
  endfunc

  func oFRFLGFRZ_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCADESCRI_2_21 as StdTrsField with uid="JZVQBCKMUA",rtseq=25,rtrep=.t.,;
    cFormVar="w_CADESCRI",value=space(60),;
    ToolTipText = "Descrizione causale ABI",;
    HelpContextID = 189796241,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=157, Left=278, Top=0, InputMask=replicate('X',60)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oFRNUMREC_2_1.When()
    return(.t.)
  proc oFRNUMREC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oFRNUMREC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mer','FLU_ESRI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FRNOMSUP=FLU_ESRI.FRNOMSUP";
  +" and "+i_cAliasName2+".FRDTARIC=FLU_ESRI.FRDTARIC";
  +" and "+i_cAliasName2+".FRNOMFIL=FLU_ESRI.FRNOMFIL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
