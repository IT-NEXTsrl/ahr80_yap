* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bae                                                        *
*              Lancia movimentavione log errori                                *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-25                                                      *
* Last revis.: 2012-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bae",oParentObject,m.pNOMSUP,m.pDTARIC,m.pNOMFIL,m.pERTIPIMP)
return(i_retval)

define class tgsso_bae as StdBatch
  * --- Local variables
  pNOMSUP = space(20)
  pDTARIC = ctod("  /  /  ")
  pNOMFIL = space(100)
  pERTIPIMP = space(1)
  w_LOGERROR = .NULL.
  * --- WorkFile variables
  LOG_REBA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia il Log Errori Ricezione Flussi (da gsrb_MFR) 
    * --- Oggetto che sar� definito come Log Ricezione Flussi
    if not empty(this.pNOMSUP)
      * --- Definisco l'oggetto con il nome relativo alla classe del 'Log Ricezione Errori'
      this.w_LOGERROR = gsso_MLE()
      * --- Controllo se ha passato il test di accesso (apertura in interrogazione)
      if !(this.w_LOGERROR.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- Inizializzo la chiave del 'Log Errori Ricezioni Flussi'
      this.w_LOGERROR.w_ERNOMSUP = this.pNOMSUP
      this.w_LOGERROR.w_ERDTARIC = this.pDTARIC
      this.w_LOGERROR.w_ERNOMFIL = this.pNOMFIL
      this.w_LOGERROR.w_ERTIPIMP = this.pERTIPIMP
      * --- Creo il cursore della chiave
      this.w_LOGERROR.QueryKeySet("ERNOMSUP="+cp_ToStrODBC(this.pNOMSUP)+ " AND ERDTARIC="+cp_ToStrODBC(this.pDTARIC)+" AND ERNOMFIL="+cp_ToStrODBC(this.pNOMFIL)+" AND ERTIPIMP='"+this.pERTIPIMP+"'","")     
      * --- vado in interrogazione
      this.w_LOGERROR.LoadRecWarn()     
    endif
  endproc


  proc Init(oParentObject,pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP)
    this.pNOMSUP=pNOMSUP
    this.pDTARIC=pDTARIC
    this.pNOMFIL=pNOMFIL
    this.pERTIPIMP=pERTIPIMP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOG_REBA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP"
endproc
