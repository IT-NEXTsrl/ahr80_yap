* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bpr                                                        *
*              Gestione progressivi contenzioso                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_127]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-22                                                      *
* Last revis.: 2001-02-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Pparam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bpr",oParentObject,m.Pparam)
return(i_retval)

define class tgsso_bpr as StdBatch
  * --- Local variables
  Pparam = space(3)
  w_DATREG = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_BLOCON = space(1)
  w_MESS = space(10)
  w_CFUNC = space(10)
  w_CAMBIO = 0
  w_DATMOR = ctod("  /  /  ")
  w_LPAGMOR = space(5)
  * --- WorkFile variables
  CONTI_idx=0
  PNT_MAST_idx=0
  VALUTE_idx=0
  CON_TENZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione  Progressivo Codice Ragrruppamento da GSSO_MCO
    this.w_CFUNC = this.oParentObject.cFunction
    do case
      case this.Pparam="AGG"
        * --- Aggiornamento parametri contenzioso nell' Anagrafica Clienti 
        this.w_TIPCON = "C"
        do case
          case this.oParentObject.w_GESCON="B"
            * --- Inserisci Blocco contenzioso
            this.w_BLOCON = "S"
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ANFLGCON ="+cp_NullLink(cp_ToStrODBC(this.w_BLOCON),'CONTI','ANFLGCON');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODCON);
                     )
            else
              update (i_cTable) set;
                  ANFLGCON = this.w_BLOCON;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.oParentObject.w_COCODCON;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_GESCON="M"
            * --- Aggiorno Data Moratoria
            this.w_DATMOR = this.oParentObject.w_CODATREG+this.oParentObject.w_DURMOR
            this.w_LPAGMOR = IIF(EMPTY(this.oParentObject.w_PAGPAR), this.oParentObject.w_PAGMOR, this.oParentObject.w_PAGPAR)
            * --- Write into CONTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ANPAGPAR ="+cp_NullLink(cp_ToStrODBC(this.w_LPAGMOR),'CONTI','ANPAGPAR');
              +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(this.w_DATMOR),'CONTI','ANDATMOR');
                  +i_ccchkf ;
              +" where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODCON);
                     )
            else
              update (i_cTable) set;
                  ANPAGPAR = this.w_LPAGMOR;
                  ,ANDATMOR = this.w_DATMOR;
                  &i_ccchkf. ;
               where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.oParentObject.w_COCODCON;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      case this.Pparam="CON" 
        * --- Controlli Sulla Contabilizzazione Dell'insouto
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNDATREG"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_CORIFCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNDATREG;
            from (i_cTable) where;
                PNSERIAL = this.oParentObject.w_CORIFCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(NVL(this.oParentObject.w_CORIFCON," ")) and (this.w_cfunc <>"Load")
          if i_rows<> 0
            do case
              case this.w_CFUNC<>"Edit"
                this.w_MESS = ah_Msgformat("Contenzioso contabilizzato; impossibile cancellare")
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=this.w_mess
                i_retcode = 'stop'
                return
            endcase
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,Pparam)
    this.Pparam=Pparam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CON_TENZ'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Pparam"
endproc
