* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_boo                                                        *
*              Aggiorna residuo                                                *
*                                                                              *
*      Author: Nicola gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-13                                                      *
* Last revis.: 2000-10-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_boo",oParentObject)
return(i_retval)

define class tgsso_boo as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_PADRE = .NULL.
  w_PROG = .NULL.
  w_ORDRIF = 0
  w_OBJCT = .NULL.
  * --- WorkFile variables
  CONDTENZ_idx=0
  INC_AVVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Residuo A seguito di  incassi
    this.w_PROG = this.oparentobject.oparentobject
    this.w_PADRE = This.oparentobject
    this.w_SERIAL = this.w_PROG.w_COSERIAL
    this.w_PADRE.Markpos()     
    this.w_PADRE.Exec_Select("_Tmp_Sq_","Sum(t_IAIMPINC) As TOTRIG,MAX(t_IAORDRIF) AS ORDRIF"," Not Deleted()  ","","","")     
    this.oParentObject.w_TOTINC = _Tmp_Sq_.TOTRIG
    this.w_ORDRIF = _Tmp_Sq_.ORDRIF
    this.w_PADRE.Repos()     
    vq_exec("..\SOLL\EXE\QUERY\GSSO_RES.VQR",this,"INSO")
    SELECT INSO
    GO TOP
    this.oParentObject.w_IARESINC = INSO.PTTOTIMP-this.oParentObject.w_TOTINC
    if this.w_PADRE.w_IARESINC<>0 AND this.w_PROG.w_COSTATUS="CI" AND (this.w_ORDRIF<>0 OR this.oParentObject.w_TOTINC=0)
      * --- Visualizzo messaggio se non ho nessun incasso non abbinato tra le righe rimanenti
      *     e nel caso in cui il totale incassato = 0
      if ah_YesNo("Attenzione, insoluto non incassato totalmente: aggiorno status?")
        this.w_OBJCT = this.w_PROG.GetCtrl( "w_COSTATUS" )
        this.w_OBJCT.value = 1
        this.w_PROG.w_COSTATUS = "PE"
        this.w_PROG.bHeaderUpdated = .T.
      endif
    endif
    if USED("INSO")
      SELECT INSO
      USE
    endif
    if USED("_Tmp_Sq_")
      SELECT _Tmp_Sq_
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONDTENZ'
    this.cWorkTables[2]='INC_AVVE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
