* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_si1                                                        *
*              Selezione partite contenzioso                                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_67]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-08                                                      *
* Last revis.: 2007-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_si1",oParentObject))

* --- Class definition
define class tgsso_si1 as StdForm
  Top    = 50
  Left   = 62

  * --- Standard Properties
  Width  = 577
  Height = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-18"
  HelpContextID=258577769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsso_si1"
  cComment = "Selezione partite contenzioso"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PTDATSCA = ctod('  /  /  ')
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod('  /  /  ')
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_PTTOTIMP = 0
  w_RNUMDIS = space(10)
  w_PT_SEGNO = space(1)
  w_PTCODCON = space(15)
  w_CONPAR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_si1Pag1","gsso_si1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CONPAR = this.oPgFrm.Pages(1).oPag.CONPAR
    DoDefault()
    proc Destroy()
      this.w_CONPAR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PTDATSCA=ctod("  /  /  ")
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_CPROWNUM=0
      .w_PTTOTIMP=0
      .w_RNUMDIS=space(10)
      .w_PT_SEGNO=space(1)
      .w_PTCODCON=space(15)
      .w_PTDATSCA=oParentObject.w_PTDATSCA
      .w_PTNUMDOC=oParentObject.w_PTNUMDOC
      .w_PTALFDOC=oParentObject.w_PTALFDOC
      .w_PTDATDOC=oParentObject.w_PTDATDOC
      .w_PTSERIAL=oParentObject.w_PTSERIAL
      .w_PTROWORD=oParentObject.w_PTROWORD
      .w_CPROWNUM=oParentObject.w_CPROWNUM
      .w_PTTOTIMP=oParentObject.w_PTTOTIMP
      .w_RNUMDIS=oParentObject.w_RNUMDIS
      .w_PT_SEGNO=oParentObject.w_PT_SEGNO
      .w_PTCODCON=oParentObject.w_PTCODCON
      .oPgFrm.Page1.oPag.CONPAR.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .w_PTDATSCA = CP_TODATE(.w_CONPAR.getVar('PTDATSCA'))
        .w_PTNUMDOC = Nvl( .w_CONPAR.getVar('PTNUMDOC') , 0 )
        .w_PTALFDOC = Nvl(.w_CONPAR.getVar('PTALFDOC'),Space(10))
        .w_PTDATDOC = CP_TODATE(.w_CONPAR.getVar('PTDATDOC'))
        .w_PTSERIAL = Nvl(.w_CONPAR.getVar('PTSERIAL'),Space(10))
        .w_PTROWORD = Nvl( .w_CONPAR.getVar('PTROWORD') , 0 )
        .w_CPROWNUM = Nvl( .w_CONPAR.getVar('CPROWNUM') , 0 )
        .w_PTTOTIMP = Nvl( .w_CONPAR.getVar('PTTOTIMP') , 0)
        .w_RNUMDIS = Nvl(.w_CONPAR.getVar('PTNUMDIS'),0)
        .w_PT_SEGNO = Nvl(.w_CONPAR.getVar('PT_SEGNO'),Space(1))
        .w_PTCODCON = Nvl(.w_CONPAR.getVar('PTCODCON'),Space(15))
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PTDATSCA=.w_PTDATSCA
      .oParentObject.w_PTNUMDOC=.w_PTNUMDOC
      .oParentObject.w_PTALFDOC=.w_PTALFDOC
      .oParentObject.w_PTDATDOC=.w_PTDATDOC
      .oParentObject.w_PTSERIAL=.w_PTSERIAL
      .oParentObject.w_PTROWORD=.w_PTROWORD
      .oParentObject.w_CPROWNUM=.w_CPROWNUM
      .oParentObject.w_PTTOTIMP=.w_PTTOTIMP
      .oParentObject.w_RNUMDIS=.w_RNUMDIS
      .oParentObject.w_PT_SEGNO=.w_PT_SEGNO
      .oParentObject.w_PTCODCON=.w_PTCODCON
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CONPAR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
            .w_PTDATSCA = CP_TODATE(.w_CONPAR.getVar('PTDATSCA'))
            .w_PTNUMDOC = Nvl( .w_CONPAR.getVar('PTNUMDOC') , 0 )
            .w_PTALFDOC = Nvl(.w_CONPAR.getVar('PTALFDOC'),Space(10))
            .w_PTDATDOC = CP_TODATE(.w_CONPAR.getVar('PTDATDOC'))
            .w_PTSERIAL = Nvl(.w_CONPAR.getVar('PTSERIAL'),Space(10))
            .w_PTROWORD = Nvl( .w_CONPAR.getVar('PTROWORD') , 0 )
            .w_CPROWNUM = Nvl( .w_CONPAR.getVar('CPROWNUM') , 0 )
            .w_PTTOTIMP = Nvl( .w_CONPAR.getVar('PTTOTIMP') , 0)
            .w_RNUMDIS = Nvl(.w_CONPAR.getVar('PTNUMDIS'),0)
            .w_PT_SEGNO = Nvl(.w_CONPAR.getVar('PT_SEGNO'),Space(1))
            .w_PTCODCON = Nvl(.w_CONPAR.getVar('PTCODCON'),Space(15))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CONPAR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CONPAR.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_si1Pag1 as StdContainer
  Width  = 573
  height = 334
  stdWidth  = 573
  stdheight = 334
  resizeXpos=453
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CONPAR as cp_szoombox with uid="TUGLPHAHRT",left=4, top=3, width=566,height=277,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,cZoomFile="GSSO_QIN",cTable="PAR_TITE",bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 80580122


  add object oObj_1_2 as cp_runprogram with uid="QOAPMJWMJH",left=2, top=336, width=142,height=21,;
    caption='GSSO_BI3(INIT)',;
   bGlobalFont=.t.,;
    prg="GSSO_BI3('INIT')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 144105959


  add object oBtn_1_13 as StdButton with uid="NUBHVPSUDL",left=514, top=285, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'operazione";
    , HelpContextID = 251260346;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSSO_BI3(this.Parent.oContained,"EXIT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="SHRFOKIYMQ",left=461, top=285, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inserire le partite nel contenzioso";
    , HelpContextID = 258549018;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_runprogram with uid="XECJPNFMFJ",left=306, top=336, width=201,height=21,;
    caption='GSSO_BI3(CONT)',;
   bGlobalFont=.t.,;
    prg="GSSO_BI3('CONT')",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 143775719


  add object oObj_1_17 as cp_runprogram with uid="EGIKTNSZDO",left=147, top=336, width=156,height=21,;
    caption='GSSO_BI3(FIN)',;
   bGlobalFont=.t.,;
    prg="GSSO_BI3('FINI')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 196987673
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_si1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
