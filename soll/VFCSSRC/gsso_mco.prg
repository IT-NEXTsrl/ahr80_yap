* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_mco                                                        *
*              Gestione contenzioso                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_485]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-19                                                      *
* Last revis.: 2015-01-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_mco"))

* --- Class definition
define class tgsso_mco as StdTrsForm
  Top    = -1
  Left   = 12

  * --- Standard Properties
  Width  = 831
  Height = 429+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-07"
  HelpContextID=171338391
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=86

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CON_TENZ_IDX = 0
  CONDTENZ_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  PAR_TITE_IDX = 0
  BAN_CHE_IDX = 0
  AZIENDA_IDX = 0
  PAR_CONT_IDX = 0
  DIS_TINT_IDX = 0
  COC_MAST_IDX = 0
  cFile = "CON_TENZ"
  cFileDetail = "CONDTENZ"
  cKeySelect = "COSERIAL"
  cKeyWhere  = "COSERIAL=this.w_COSERIAL"
  cKeyDetail  = "COSERIAL=this.w_COSERIAL"
  cKeyWhereODBC = '"COSERIAL="+cp_ToStrODBC(this.w_COSERIAL)';

  cKeyDetailWhereODBC = '"COSERIAL="+cp_ToStrODBC(this.w_COSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CONDTENZ.COSERIAL="+cp_ToStrODBC(this.w_COSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONDTENZ.CPROWORD '
  cPrg = "gsso_mco"
  cComment = "Gestione contenzioso"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPAR = space(5)
  w_PAGMOR = space(5)
  w_DURMOR = 0
  w_GIOPAR = 0
  w_COSERIAL = space(10)
  o_COSERIAL = space(10)
  w_CONUMREG = 0
  w_CODATREG = ctod('  /  /  ')
  o_CODATREG = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_COCOMPET = space(4)
  w_CO__TIPO = space(1)
  o_CO__TIPO = space(1)
  w_CODATVAL = ctod('  /  /  ')
  w_COTIPEFF = space(1)
  o_COTIPEFF = space(1)
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  o_COCODCON = space(15)
  w_CONUMDOC = 0
  w_COSTATUS = space(2)
  w_COSERRIF = space(10)
  w_COORDRIF = 0
  w_CONUMRIF = 0
  o_CONUMRIF = 0
  w_CPROWORD = 0
  w_CODESCRI = space(50)
  w_CO__NOTE = space(0)
  w_DESCLI = space(40)
  w_IMPSPE = 0
  w_CONUMLIV = 0
  w_CODATULT = ctod('  /  /  ')
  w_VALPAR1 = space(3)
  w_VALPAR = space(3)
  w_DEC = 0
  w_CALCPIP = space(1)
  w_COADDSPE = space(1)
  o_COADDSPE = space(1)
  w_COIMPSPE = 0
  w_COADDINT = space(1)
  o_COADDINT = space(1)
  w_COPERINT = 0
  w_COCATPAG = space(2)
  w_CODATDOC = ctod('  /  /  ')
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_TIPCON = space(1)
  w_COVALNAZ = space(3)
  w_COCODVAL = space(3)
  o_COCODVAL = space(3)
  w_TESVAL1 = 0
  w_DECTOT = 0
  w_COCAOVAL = 0
  w_COALFDOC = space(10)
  w_NUMPAR = space(31)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_COTOTIMP = 0
  w_DATDOC = ctod('  /  /  ')
  w_TOTIMP = 0
  w_COCODBAN = space(15)
  w_COCONTRO = space(15)
  w_COBANPRE = space(15)
  w_DESAPP = space(35)
  w_SIMVAL = space(5)
  w_CALCPICT = space(1)
  w_TOTABB = 0
  w_TOTPAR = 0
  w_SEGNO = space(1)
  w_CODBAN = space(15)
  w_INISCA = ctod('  /  /  ')
  w_FINSCA = ctod('  /  /  ')
  w_DATVAL = ctod('  /  /  ')
  w_TOTALE = 0
  w_FLAGSCA = space(1)
  w_COADDSOL = space(1)
  w_COCODRAG = space(10)
  w_CORIFCON = space(10)
  o_CORIFCON = space(10)
  w_GESCON = space(1)
  w_DATMOR = ctod('  /  /  ')
  w_CODESSPE = space(1)
  w_DATINT = ctod('  /  /  ')
  w_CODATINT = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_PAGPAR = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_CONTRO = space(15)
  w_SAGINT = 0
  w_OLDSTATUS = space(10)
  w_CONOTEIN = space(0)
  w_EDITTIPO = .F.
  w_ANSPRINT = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_COSERIAL = this.W_COSERIAL
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_COCOMPET = this.W_COCOMPET
  op_CONUMREG = this.W_CONUMREG
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_COCOMPET = this.W_COCOMPET
  op_COCODRAG = this.W_COCODRAG

  * --- Children pointers
  GSSO_MDS = .NULL.
  GSSO_MIA = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsso_mco
  Proc F6()
      return
      DoDefault()
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CON_TENZ','gsso_mco')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_mcoPag1","gsso_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gestione")
      .Pages(1).HelpContextID = 264108853
      .Pages(2).addobject("oPag","tgsso_mcoPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Status")
      .Pages(2).HelpContextID = 184486874
      .Pages(3).addobject("oPag","tgsso_mcoPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Solleciti")
      .Pages(3).HelpContextID = 66692650
      .Pages(4).addobject("oPag","tgsso_mcoPag4")
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note")
      .Pages(4).HelpContextID = 178462422
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PAR_TITE'
    this.cWorkTables[4]='BAN_CHE'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='PAR_CONT'
    this.cWorkTables[7]='DIS_TINT'
    this.cWorkTables[8]='COC_MAST'
    this.cWorkTables[9]='CON_TENZ'
    this.cWorkTables[10]='CONDTENZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_TENZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_TENZ_IDX,3]
  return

  function CreateChildren()
    this.GSSO_MDS = CREATEOBJECT('stdDynamicChild',this,'GSSO_MDS',this.oPgFrm.Page3.oPag.oLinkPC_5_1)
    this.GSSO_MIA = CREATEOBJECT('stdDynamicChild',this,'GSSO_MIA',this.oPgFrm.Page2.oPag.oLinkPC_4_23)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSSO_MDS)
      this.GSSO_MDS.DestroyChildrenChain()
      this.GSSO_MDS=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_5_1')
    if !ISNULL(this.GSSO_MIA)
      this.GSSO_MIA.DestroyChildrenChain()
      this.GSSO_MIA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_23')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSSO_MDS.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSSO_MIA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSSO_MDS.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSSO_MIA.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSSO_MDS.NewDocument()
    this.GSSO_MIA.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSSO_MDS.ChangeRow(this.cRowID+'      1',1;
             ,.w_COSERIAL,"DSSERIAL";
             )
      .GSSO_MIA.ChangeRow(this.cRowID+'      1',1;
             ,.w_COSERIAL,"IASERIAL";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_COSERIAL = NVL(COSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CON_TENZ where COSERIAL=KeySet.COSERIAL
    *
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2],this.bLoadRecFilter,this.CON_TENZ_IDX,"gsso_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_TENZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_TENZ.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CONDTENZ.","CON_TENZ.")
      i_cTable = i_cTable+' CON_TENZ '
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_PAGMOR = space(5)
        .w_DURMOR = 0
        .w_GIOPAR = 0
        .w_DESCLI = space(40)
        .w_IMPSPE = 0
        .w_VALPAR1 = space(3)
        .w_DEC = 0
        .w_VALUTA = space(3)
        .w_TESVAL1 = 0
        .w_DECTOT = 0
        .w_DESAPP = space(35)
        .w_SIMVAL = space(5)
        .w_DATVAL = i_datsys
        .w_TOTALE = 0
        .w_FLAGSCA = space(1)
        .w_GESCON = space(1)
        .w_DATMOR = ctod("  /  /  ")
        .w_PAGPAR = space(5)
        .w_DATOBSO = ctod("  /  /  ")
        .w_CONTRO = space(15)
        .w_SAGINT = 0
        .w_EDITTIPO = .T.
        .w_ANSPRINT = space(1)
        .w_READPAR = i_CODAZI
          .link_1_1('Load')
        .w_COSERIAL = NVL(COSERIAL,space(10))
        .op_COSERIAL = .w_COSERIAL
        .w_CONUMREG = NVL(CONUMREG,0)
        .op_CONUMREG = .w_CONUMREG
        .w_CODATREG = NVL(cp_ToDate(CODATREG),ctod("  /  /  "))
        .w_OBTEST = .w_CODATREG
        .w_COCOMPET = NVL(COCOMPET,space(4))
        .op_COCOMPET = .w_COCOMPET
        .op_COCOMPET = .w_COCOMPET
        .w_CO__TIPO = NVL(CO__TIPO,space(1))
        .w_CODATVAL = NVL(cp_ToDate(CODATVAL),ctod("  /  /  "))
        .w_COTIPEFF = NVL(COTIPEFF,space(1))
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_COCODCON = NVL(COCODCON,space(15))
          .link_1_15('Load')
        .w_CONUMDOC = NVL(CONUMDOC,0)
        .w_COSTATUS = NVL(COSTATUS,space(2))
        .w_CODESCRI = NVL(CODESCRI,space(50))
        .w_CO__NOTE = NVL(CO__NOTE,space(0))
        .w_CONUMLIV = NVL(CONUMLIV,0)
        .w_CODATULT = NVL(cp_ToDate(CODATULT),ctod("  /  /  "))
        .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1), .w_VALPAR1,g_PERVAL)
          .link_4_9('Load')
        .w_CALCPIP = DEFPIP(.w_DEC)
        .w_COADDSPE = NVL(COADDSPE,space(1))
        .w_COIMPSPE = NVL(COIMPSPE,0)
        .w_COADDINT = NVL(COADDINT,space(1))
        .w_COPERINT = NVL(COPERINT,0)
        .w_COCATPAG = NVL(COCATPAG,space(2))
        .w_CODATDOC = NVL(cp_ToDate(CODATDOC),ctod("  /  /  "))
        .w_TIPCON = 'G'
        .w_COVALNAZ = NVL(COVALNAZ,space(3))
        .w_COCODVAL = NVL(COCODVAL,space(3))
          if link_1_32_joined
            this.w_COCODVAL = NVL(VACODVAL132,NVL(this.w_COCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL132,space(35))
            this.w_DECTOT = NVL(VADECTOT132,0)
            this.w_TESVAL1 = NVL(VACAOVAL132,0)
            this.w_SIMVAL = NVL(VASIMVAL132,space(5))
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO132),ctod("  /  /  "))
          else
          .link_1_32('Load')
          endif
        .w_COCAOVAL = NVL(COCAOVAL,0)
        .w_COALFDOC = NVL(COALFDOC,space(10))
        .w_COCONTRO = NVL(COCONTRO,space(15))
          * evitabile
          *.link_1_37('Load')
        .w_COBANPRE = NVL(COBANPRE,space(15))
          * evitabile
          *.link_1_38('Load')
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_INISCA = .w_CODATREG-60
        .w_FINSCA = .w_CODATREG
        .w_COADDSOL = NVL(COADDSOL,space(1))
        .w_COCODRAG = NVL(COCODRAG,space(10))
        .op_COCODRAG = .w_COCODRAG
        .w_CORIFCON = NVL(CORIFCON,space(10))
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .w_CODESSPE = NVL(CODESSPE,space(1))
        .w_DATINT = .w_CODATINT
        .w_CODATINT = NVL(cp_ToDate(CODATINT),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(Empty(.w_NUMPAR) and Not empty(.w_COSERRIF), AH_Msgformat('Contenzioso storicizzato'),''))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate()
        .w_OLDSTATUS = .w_COSTATUS
        .w_CONOTEIN = NVL(CONOTEIN,space(0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CON_TENZ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CONDTENZ where COSERIAL=KeySet.COSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CONDTENZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONDTENZ_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CONDTENZ')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CONDTENZ.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CONDTENZ"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
        select * from (i_cTable) CONDTENZ where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_NUMPAR = space(31)
          .w_NUMDOC = 0
          .w_ALFDOC = space(10)
          .w_DATDOC = ctod("  /  /  ")
          .w_TOTABB = 0
          .w_TOTPAR = 0
          .w_SEGNO = space(1)
          .w_DATSCA = ctod("  /  /  ")
          .w_CPROWNUM = CPROWNUM
          .w_COSERRIF = NVL(COSERRIF,space(10))
          .w_COORDRIF = NVL(COORDRIF,0)
          .w_CONUMRIF = NVL(CONUMRIF,0)
          if link_2_3_joined
            this.w_CONUMRIF = NVL(CPROWNUM203,NVL(this.w_CONUMRIF,0))
            this.w_NUMPAR = NVL(PTNUMPAR203,space(31))
            this.w_DATSCA = NVL(cp_ToDate(PTDATSCA203),ctod("  /  /  "))
            this.w_TOTPAR = NVL(PTTOTIMP203,0)
            this.w_NUMDOC = NVL(PTNUMDOC203,0)
            this.w_ALFDOC = NVL(PTALFDOC203,space(10))
            this.w_CODBAN = NVL(PTBANNOS203,space(15))
            this.w_DATDOC = NVL(cp_ToDate(PTDATDOC203),ctod("  /  /  "))
            this.w_TOTABB = NVL(PTTOTABB203,0)
            this.w_SEGNO = NVL(PT_SEGNO203,space(1))
          else
          .link_2_3('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_COTOTIMP = NVL(COTOTIMP,0)
        .w_TOTIMP = IIF(.w_CO__TIPO='M',.w_COTOTIMP,(IIF(.w_SEGNO='A',.w_TOTPAR +.w_TOTABB,-(.w_TOTPAR +.w_TOTABB))))
          .w_COCODBAN = NVL(COCODBAN,space(15))
        .w_CODBAN = .w_CODBAN
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_TOTIMP
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_READPAR = i_CODAZI
        .w_OBTEST = .w_CODATREG
        .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1), .w_VALPAR1,g_PERVAL)
        .w_CALCPIP = DEFPIP(.w_DEC)
        .w_TIPCON = 'G'
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_INISCA = .w_CODATREG-60
        .w_FINSCA = .w_CODATREG
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .w_DATINT = .w_CODATINT
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(Empty(.w_NUMPAR) and Not empty(.w_COSERRIF), AH_Msgformat('Contenzioso storicizzato'),''))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate()
        .w_OLDSTATUS = .w_COSTATUS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_57.enabled = .oPgFrm.Page1.oPag.oBtn_1_57.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_READPAR=space(5)
      .w_PAGMOR=space(5)
      .w_DURMOR=0
      .w_GIOPAR=0
      .w_COSERIAL=space(10)
      .w_CONUMREG=0
      .w_CODATREG=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_COCOMPET=space(4)
      .w_CO__TIPO=space(1)
      .w_CODATVAL=ctod("  /  /  ")
      .w_COTIPEFF=space(1)
      .w_COTIPCON=space(1)
      .w_COCODCON=space(15)
      .w_CONUMDOC=0
      .w_COSTATUS=space(2)
      .w_COSERRIF=space(10)
      .w_COORDRIF=0
      .w_CONUMRIF=0
      .w_CPROWORD=10
      .w_CODESCRI=space(50)
      .w_CO__NOTE=space(0)
      .w_DESCLI=space(40)
      .w_IMPSPE=0
      .w_CONUMLIV=0
      .w_CODATULT=ctod("  /  /  ")
      .w_VALPAR1=space(3)
      .w_VALPAR=space(3)
      .w_DEC=0
      .w_CALCPIP=space(1)
      .w_COADDSPE=space(1)
      .w_COIMPSPE=0
      .w_COADDINT=space(1)
      .w_COPERINT=0
      .w_COCATPAG=space(2)
      .w_CODATDOC=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_TIPCON=space(1)
      .w_COVALNAZ=space(3)
      .w_COCODVAL=space(3)
      .w_TESVAL1=0
      .w_DECTOT=0
      .w_COCAOVAL=0
      .w_COALFDOC=space(10)
      .w_NUMPAR=space(31)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_COTOTIMP=0
      .w_DATDOC=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_COCODBAN=space(15)
      .w_COCONTRO=space(15)
      .w_COBANPRE=space(15)
      .w_DESAPP=space(35)
      .w_SIMVAL=space(5)
      .w_CALCPICT=space(1)
      .w_TOTABB=0
      .w_TOTPAR=0
      .w_SEGNO=space(1)
      .w_CODBAN=space(15)
      .w_INISCA=ctod("  /  /  ")
      .w_FINSCA=ctod("  /  /  ")
      .w_DATVAL=ctod("  /  /  ")
      .w_TOTALE=0
      .w_FLAGSCA=space(1)
      .w_COADDSOL=space(1)
      .w_COCODRAG=space(10)
      .w_CORIFCON=space(10)
      .w_GESCON=space(1)
      .w_DATMOR=ctod("  /  /  ")
      .w_CODESSPE=space(1)
      .w_DATINT=ctod("  /  /  ")
      .w_CODATINT=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_PAGPAR=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CONTRO=space(15)
      .w_SAGINT=0
      .w_OLDSTATUS=space(10)
      .w_CONOTEIN=space(0)
      .w_EDITTIPO=.f.
      .w_ANSPRINT=space(1)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_READPAR = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,6,.f.)
        .w_CODATREG = i_DATSYS
        .w_OBTEST = .w_CODATREG
        .w_COCOMPET = CALCESER(.w_CODATREG)
        .w_CO__TIPO = 'I'
        .DoRTCalc(11,11,.f.)
        .w_COTIPEFF = 'E'
        .w_COTIPCON = 'C'
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_COCODCON))
         .link_1_15('Full')
        endif
        .DoRTCalc(15,15,.f.)
        .w_COSTATUS = 'PE'
        .DoRTCalc(17,19,.f.)
        if not(empty(.w_CONUMRIF))
         .link_2_3('Full')
        endif
        .DoRTCalc(20,27,.f.)
        .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1), .w_VALPAR1,g_PERVAL)
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_VALPAR))
         .link_4_9('Full')
        endif
        .DoRTCalc(29,29,.f.)
        .w_CALCPIP = DEFPIP(.w_DEC)
        .DoRTCalc(31,31,.f.)
        .w_COIMPSPE = iif(.w_COADDSPE='S',.w_IMPSPE,0)
        .w_COADDINT = 'N'
        .w_COPERINT = IIF(.w_COADDINT='S' AND .w_ANSPRINT='N', .w_SAGINT, 0)
        .w_COCATPAG = 'RB'
        .DoRTCalc(36,37,.f.)
        .w_TIPCON = 'G'
        .w_COVALNAZ = g_perval
        .w_COCODVAL = IIF(EMPTY(.w_VALUTA),g_PERVAL,.w_VALUTA)
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_COCODVAL))
         .link_1_32('Full')
        endif
        .DoRTCalc(41,42,.f.)
        .w_COCAOVAL = GETCAM(.w_COCODVAL, .w_DATVAL, 7)
        .DoRTCalc(44,49,.f.)
        .w_TOTIMP = IIF(.w_CO__TIPO='M',.w_COTOTIMP,(IIF(.w_SEGNO='A',.w_TOTPAR +.w_TOTABB,-(.w_TOTPAR +.w_TOTABB))))
        .w_COCODBAN = IIF(.w_CO__TIPO='M',.w_CODBAN,.w_COCODBAN)
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_COCONTRO))
         .link_1_37('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_COBANPRE))
         .link_1_38('Full')
        endif
        .DoRTCalc(54,55,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(57,59,.f.)
        .w_CODBAN = .w_CODBAN
        .w_INISCA = .w_CODATREG-60
        .w_FINSCA = .w_CODATREG
        .w_DATVAL = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .DoRTCalc(64,70,.f.)
        .w_CODESSPE = 'C'
        .w_DATINT = .w_CODATINT
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(Empty(.w_NUMPAR) and Not empty(.w_COSERRIF), AH_Msgformat('Contenzioso storicizzato'),''))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate()
        .DoRTCalc(73,78,.f.)
        .w_OLDSTATUS = .w_COSTATUS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(80,80,.f.)
        .w_EDITTIPO = .T.
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_TENZ')
    this.DoRTCalc(82,86,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsso_mco
     Local L_Object
     L_Object=This.GetCtrl('w_COADDINT')
     L_Object.Value=1
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCONUMREG_1_6.enabled = i_bVal
      .Page1.oPag.oCODATREG_1_7.enabled = i_bVal
      .Page1.oPag.oCOCOMPET_1_9.enabled = i_bVal
      .Page1.oPag.oCO__TIPO_1_10.enabled = i_bVal
      .Page1.oPag.oCODATVAL_1_11.enabled = i_bVal
      .Page1.oPag.oCOTIPEFF_1_12.enabled = i_bVal
      .Page1.oPag.oCOCODCON_1_15.enabled = i_bVal
      .Page2.oPag.oCOSTATUS_4_1.enabled = i_bVal
      .Page1.oPag.oCODESCRI_1_20.enabled = i_bVal
      .Page1.oPag.oCO__NOTE_1_22.enabled = i_bVal
      .Page2.oPag.oCONUMLIV_4_4.enabled = i_bVal
      .Page2.oPag.oCODATULT_4_6.enabled = i_bVal
      .Page2.oPag.oCOADDSPE_4_12.enabled = i_bVal
      .Page2.oPag.oCOIMPSPE_4_13.enabled = i_bVal
      .Page2.oPag.oCOADDINT_4_15.enabled = i_bVal
      .Page2.oPag.oCOPERINT_4_16.enabled = i_bVal
      .Page1.oPag.oCOCATPAG_1_26.enabled = i_bVal
      .Page1.oPag.oCOCODVAL_1_32.enabled = i_bVal
      .Page1.oPag.oCOCAOVAL_1_35.enabled = i_bVal
      .Page1.oPag.oCOCONTRO_1_37.enabled = i_bVal
      .Page1.oPag.oCOBANPRE_1_38.enabled = i_bVal
      .Page1.oPag.oINISCA_1_48.enabled = i_bVal
      .Page1.oPag.oFINSCA_1_49.enabled = i_bVal
      .Page2.oPag.oCOADDSOL_4_18.enabled = i_bVal
      .Page1.oPag.oCOCODRAG_1_54.enabled = i_bVal
      .Page2.oPag.oCODESSPE_4_19.enabled = i_bVal
      .Page2.oPag.oCODATINT_4_21.enabled = i_bVal
      .Page4.oPag.oCONOTEIN_6_1.enabled = i_bVal
      .Page1.oPag.oBtn_1_42.enabled = i_bVal
      .Page1.oPag.oBtn_1_57.enabled = .Page1.oPag.oBtn_1_57.mCond()
      .Page1.oPag.oObj_1_59.enabled = i_bVal
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_71.enabled = i_bVal
      .Page2.oPag.oObj_4_28.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCONUMREG_1_6.enabled = .t.
        .Page1.oPag.oCODATREG_1_7.enabled = .t.
        .Page1.oPag.oCOCODCON_1_15.enabled = .t.
      endif
    endwith
    this.GSSO_MDS.SetStatus(i_cOp)
    this.GSSO_MIA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CON_TENZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEINS","i_CODAZI,w_COSERIAL")
      cp_AskTableProg(this,i_nConn,"PRINS","i_CODAZI,w_COCOMPET,w_CONUMREG")
      cp_AskTableProg(this,i_nConn,"GRINS","i_CODAZI,w_COCOMPET,w_COCODRAG")
      .op_CODAZI = .w_CODAZI
      .op_COSERIAL = .w_COSERIAL
      .op_CODAZI = .w_CODAZI
      .op_COCOMPET = .w_COCOMPET
      .op_CONUMREG = .w_CONUMREG
      .op_CODAZI = .w_CODAZI
      .op_COCOMPET = .w_COCOMPET
      .op_COCODRAG = .w_COCODRAG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSSO_MDS.SetChildrenStatus(i_cOp)
  *  this.GSSO_MIA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSERIAL,"COSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMREG,"CONUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATREG,"CODATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOMPET,"COCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__TIPO,"CO__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATVAL,"CODATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPEFF,"COTIPEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCON,"COCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMDOC,"CONUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSTATUS,"COSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCRI,"CODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__NOTE,"CO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMLIV,"CONUMLIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATULT,"CODATULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COADDSPE,"COADDSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIMPSPE,"COIMPSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COADDINT,"COADDINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPERINT,"COPERINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCATPAG,"COCATPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATDOC,"CODATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALNAZ,"COVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODVAL,"COCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAOVAL,"COCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COALFDOC,"COALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCONTRO,"COCONTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COBANPRE,"COBANPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COADDSOL,"COADDSOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODRAG,"COCODRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORIFCON,"CORIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESSPE,"CODESSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATINT,"CODATINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONOTEIN,"CONOTEIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
    i_lTable = "CON_TENZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CON_TENZ_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSSO_SSO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_NUMPAR C(31);
      ,t_NUMDOC N(15);
      ,t_ALFDOC C(10);
      ,t_COTOTIMP N(18,4);
      ,t_DATDOC D(8);
      ,t_TOTIMP N(18,4);
      ,t_COCODBAN C(15);
      ,t_DATSCA D(8);
      ,CPROWNUM N(10);
      ,t_COSERRIF C(10);
      ,t_COORDRIF N(4);
      ,t_CONUMRIF N(3);
      ,t_TOTABB N(18,4);
      ,t_TOTPAR N(18,4);
      ,t_SEGNO C(1);
      ,t_CODBAN C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsso_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_5.controlsource=this.cTrsName+'.t_NUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_6.controlsource=this.cTrsName+'.t_NUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_7.controlsource=this.cTrsName+'.t_ALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.controlsource=this.cTrsName+'.t_COTOTIMP'
    this.oPgFRm.Page1.oPag.oDATDOC_2_9.controlsource=this.cTrsName+'.t_DATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.controlsource=this.cTrsName+'.t_TOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODBAN_2_11.controlsource=this.cTrsName+'.t_COCODBAN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_16.controlsource=this.cTrsName+'.t_DATSCA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(79)
    this.AddVLine(312)
    this.AddVLine(434)
    this.AddVLine(522)
    this.AddVLine(675)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEINS","i_CODAZI,w_COSERIAL")
          cp_NextTableProg(this,i_nConn,"PRINS","i_CODAZI,w_COCOMPET,w_CONUMREG")
          cp_NextTableProg(this,i_nConn,"GRINS","i_CODAZI,w_COCOMPET,w_COCODRAG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_TENZ
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_TENZ')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_TENZ')
        local i_cFld
        i_cFld=" "+;
                  "(COSERIAL,CONUMREG,CODATREG,COCOMPET,CO__TIPO"+;
                  ",CODATVAL,COTIPEFF,COTIPCON,COCODCON,CONUMDOC"+;
                  ",COSTATUS,CODESCRI,CO__NOTE,CONUMLIV,CODATULT"+;
                  ",COADDSPE,COIMPSPE,COADDINT,COPERINT,COCATPAG"+;
                  ",CODATDOC,COVALNAZ,COCODVAL,COCAOVAL,COALFDOC"+;
                  ",COCONTRO,COBANPRE,COADDSOL,COCODRAG,CORIFCON"+;
                  ",CODESSPE,CODATINT,CONOTEIN,UTCC,UTDC"+;
                  ",UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_COSERIAL)+;
                    ","+cp_ToStrODBC(this.w_CONUMREG)+;
                    ","+cp_ToStrODBC(this.w_CODATREG)+;
                    ","+cp_ToStrODBC(this.w_COCOMPET)+;
                    ","+cp_ToStrODBC(this.w_CO__TIPO)+;
                    ","+cp_ToStrODBC(this.w_CODATVAL)+;
                    ","+cp_ToStrODBC(this.w_COTIPEFF)+;
                    ","+cp_ToStrODBC(this.w_COTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_COCODCON)+;
                    ","+cp_ToStrODBC(this.w_CONUMDOC)+;
                    ","+cp_ToStrODBC(this.w_COSTATUS)+;
                    ","+cp_ToStrODBC(this.w_CODESCRI)+;
                    ","+cp_ToStrODBC(this.w_CO__NOTE)+;
                    ","+cp_ToStrODBC(this.w_CONUMLIV)+;
                    ","+cp_ToStrODBC(this.w_CODATULT)+;
                    ","+cp_ToStrODBC(this.w_COADDSPE)+;
                    ","+cp_ToStrODBC(this.w_COIMPSPE)+;
                    ","+cp_ToStrODBC(this.w_COADDINT)+;
                    ","+cp_ToStrODBC(this.w_COPERINT)+;
                    ","+cp_ToStrODBC(this.w_COCATPAG)+;
                    ","+cp_ToStrODBC(this.w_CODATDOC)+;
                    ","+cp_ToStrODBC(this.w_COVALNAZ)+;
                    ","+cp_ToStrODBCNull(this.w_COCODVAL)+;
                    ","+cp_ToStrODBC(this.w_COCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_COALFDOC)+;
                    ","+cp_ToStrODBCNull(this.w_COCONTRO)+;
                    ","+cp_ToStrODBCNull(this.w_COBANPRE)+;
                    ","+cp_ToStrODBC(this.w_COADDSOL)+;
                    ","+cp_ToStrODBC(this.w_COCODRAG)+;
                    ","+cp_ToStrODBC(this.w_CORIFCON)+;
                    ","+cp_ToStrODBC(this.w_CODESSPE)+;
                    ","+cp_ToStrODBC(this.w_CODATINT)+;
                    ","+cp_ToStrODBC(this.w_CONOTEIN)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_TENZ')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_TENZ')
        cp_CheckDeletedKey(i_cTable,0,'COSERIAL',this.w_COSERIAL)
        INSERT INTO (i_cTable);
              (COSERIAL,CONUMREG,CODATREG,COCOMPET,CO__TIPO,CODATVAL,COTIPEFF,COTIPCON,COCODCON,CONUMDOC,COSTATUS,CODESCRI,CO__NOTE,CONUMLIV,CODATULT,COADDSPE,COIMPSPE,COADDINT,COPERINT,COCATPAG,CODATDOC,COVALNAZ,COCODVAL,COCAOVAL,COALFDOC,COCONTRO,COBANPRE,COADDSOL,COCODRAG,CORIFCON,CODESSPE,CODATINT,CONOTEIN,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_COSERIAL;
                  ,this.w_CONUMREG;
                  ,this.w_CODATREG;
                  ,this.w_COCOMPET;
                  ,this.w_CO__TIPO;
                  ,this.w_CODATVAL;
                  ,this.w_COTIPEFF;
                  ,this.w_COTIPCON;
                  ,this.w_COCODCON;
                  ,this.w_CONUMDOC;
                  ,this.w_COSTATUS;
                  ,this.w_CODESCRI;
                  ,this.w_CO__NOTE;
                  ,this.w_CONUMLIV;
                  ,this.w_CODATULT;
                  ,this.w_COADDSPE;
                  ,this.w_COIMPSPE;
                  ,this.w_COADDINT;
                  ,this.w_COPERINT;
                  ,this.w_COCATPAG;
                  ,this.w_CODATDOC;
                  ,this.w_COVALNAZ;
                  ,this.w_COCODVAL;
                  ,this.w_COCAOVAL;
                  ,this.w_COALFDOC;
                  ,this.w_COCONTRO;
                  ,this.w_COBANPRE;
                  ,this.w_COADDSOL;
                  ,this.w_COCODRAG;
                  ,this.w_CORIFCON;
                  ,this.w_CODESSPE;
                  ,this.w_CODATINT;
                  ,this.w_CONOTEIN;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONDTENZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONDTENZ_IDX,2])
      *
      * insert into CONDTENZ
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(COSERIAL,COSERRIF,COORDRIF,CONUMRIF,CPROWORD"+;
                  ",COTOTIMP,COCODBAN,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_COSERIAL)+","+cp_ToStrODBC(this.w_COSERRIF)+","+cp_ToStrODBC(this.w_COORDRIF)+","+cp_ToStrODBCNull(this.w_CONUMRIF)+","+cp_ToStrODBC(this.w_CPROWORD)+;
             ","+cp_ToStrODBC(this.w_COTOTIMP)+","+cp_ToStrODBC(this.w_COCODBAN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'COSERIAL',this.w_COSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_COSERIAL,this.w_COSERRIF,this.w_COORDRIF,this.w_CONUMRIF,this.w_CPROWORD"+;
                ",this.w_COTOTIMP,this.w_COCODBAN,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CON_TENZ
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CON_TENZ')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CONUMREG="+cp_ToStrODBC(this.w_CONUMREG)+;
             ",CODATREG="+cp_ToStrODBC(this.w_CODATREG)+;
             ",COCOMPET="+cp_ToStrODBC(this.w_COCOMPET)+;
             ",CO__TIPO="+cp_ToStrODBC(this.w_CO__TIPO)+;
             ",CODATVAL="+cp_ToStrODBC(this.w_CODATVAL)+;
             ",COTIPEFF="+cp_ToStrODBC(this.w_COTIPEFF)+;
             ",COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",COCODCON="+cp_ToStrODBCNull(this.w_COCODCON)+;
             ",CONUMDOC="+cp_ToStrODBC(this.w_CONUMDOC)+;
             ",COSTATUS="+cp_ToStrODBC(this.w_COSTATUS)+;
             ",CODESCRI="+cp_ToStrODBC(this.w_CODESCRI)+;
             ",CO__NOTE="+cp_ToStrODBC(this.w_CO__NOTE)+;
             ",CONUMLIV="+cp_ToStrODBC(this.w_CONUMLIV)+;
             ",CODATULT="+cp_ToStrODBC(this.w_CODATULT)+;
             ",COADDSPE="+cp_ToStrODBC(this.w_COADDSPE)+;
             ",COIMPSPE="+cp_ToStrODBC(this.w_COIMPSPE)+;
             ",COADDINT="+cp_ToStrODBC(this.w_COADDINT)+;
             ",COPERINT="+cp_ToStrODBC(this.w_COPERINT)+;
             ",COCATPAG="+cp_ToStrODBC(this.w_COCATPAG)+;
             ",CODATDOC="+cp_ToStrODBC(this.w_CODATDOC)+;
             ",COVALNAZ="+cp_ToStrODBC(this.w_COVALNAZ)+;
             ",COCODVAL="+cp_ToStrODBCNull(this.w_COCODVAL)+;
             ",COCAOVAL="+cp_ToStrODBC(this.w_COCAOVAL)+;
             ",COALFDOC="+cp_ToStrODBC(this.w_COALFDOC)+;
             ",COCONTRO="+cp_ToStrODBCNull(this.w_COCONTRO)+;
             ",COBANPRE="+cp_ToStrODBCNull(this.w_COBANPRE)+;
             ",COADDSOL="+cp_ToStrODBC(this.w_COADDSOL)+;
             ",COCODRAG="+cp_ToStrODBC(this.w_COCODRAG)+;
             ",CORIFCON="+cp_ToStrODBC(this.w_CORIFCON)+;
             ",CODESSPE="+cp_ToStrODBC(this.w_CODESSPE)+;
             ",CODATINT="+cp_ToStrODBC(this.w_CODATINT)+;
             ",CONOTEIN="+cp_ToStrODBC(this.w_CONOTEIN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CON_TENZ')
          i_cWhere = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
          UPDATE (i_cTable) SET;
              CONUMREG=this.w_CONUMREG;
             ,CODATREG=this.w_CODATREG;
             ,COCOMPET=this.w_COCOMPET;
             ,CO__TIPO=this.w_CO__TIPO;
             ,CODATVAL=this.w_CODATVAL;
             ,COTIPEFF=this.w_COTIPEFF;
             ,COTIPCON=this.w_COTIPCON;
             ,COCODCON=this.w_COCODCON;
             ,CONUMDOC=this.w_CONUMDOC;
             ,COSTATUS=this.w_COSTATUS;
             ,CODESCRI=this.w_CODESCRI;
             ,CO__NOTE=this.w_CO__NOTE;
             ,CONUMLIV=this.w_CONUMLIV;
             ,CODATULT=this.w_CODATULT;
             ,COADDSPE=this.w_COADDSPE;
             ,COIMPSPE=this.w_COIMPSPE;
             ,COADDINT=this.w_COADDINT;
             ,COPERINT=this.w_COPERINT;
             ,COCATPAG=this.w_COCATPAG;
             ,CODATDOC=this.w_CODATDOC;
             ,COVALNAZ=this.w_COVALNAZ;
             ,COCODVAL=this.w_COCODVAL;
             ,COCAOVAL=this.w_COCAOVAL;
             ,COALFDOC=this.w_COALFDOC;
             ,COCONTRO=this.w_COCONTRO;
             ,COBANPRE=this.w_COBANPRE;
             ,COADDSOL=this.w_COADDSOL;
             ,COCODRAG=this.w_COCODRAG;
             ,CORIFCON=this.w_CORIFCON;
             ,CODESSPE=this.w_CODESSPE;
             ,CODATINT=this.w_CODATINT;
             ,CONOTEIN=this.w_CONOTEIN;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CONUMRIF)and Empty(t_COSERRIF) and Empty(t_COORDRIF)  )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CONDTENZ_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CONDTENZ_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CONDTENZ
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONDTENZ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COSERRIF="+cp_ToStrODBC(this.w_COSERRIF)+;
                     ",COORDRIF="+cp_ToStrODBC(this.w_COORDRIF)+;
                     ",CONUMRIF="+cp_ToStrODBCNull(this.w_CONUMRIF)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",COTOTIMP="+cp_ToStrODBC(this.w_COTOTIMP)+;
                     ",COCODBAN="+cp_ToStrODBC(this.w_COCODBAN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COSERRIF=this.w_COSERRIF;
                     ,COORDRIF=this.w_COORDRIF;
                     ,CONUMRIF=this.w_CONUMRIF;
                     ,CPROWORD=this.w_CPROWORD;
                     ,COTOTIMP=this.w_COTOTIMP;
                     ,COCODBAN=this.w_COCODBAN;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSSO_MDS : Saving
      this.GSSO_MDS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_COSERIAL,"DSSERIAL";
             )
      this.GSSO_MDS.mReplace()
      * --- GSSO_MIA : Saving
      this.GSSO_MIA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_COSERIAL,"IASERIAL";
             )
      this.GSSO_MIA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsso_mco
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSSO_MDS : Deleting
    this.GSSO_MDS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_COSERIAL,"DSSERIAL";
           )
    this.GSSO_MDS.mDelete()
    * --- GSSO_MIA : Deleting
    this.GSSO_MIA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_COSERIAL,"IASERIAL";
           )
    this.GSSO_MIA.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CONUMRIF)and Empty(t_COSERRIF) and Empty(t_COORDRIF)  )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CONDTENZ_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CONDTENZ_IDX,2])
        *
        * delete CONDTENZ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
        *
        * delete CON_TENZ
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CONUMRIF)and Empty(t_COSERRIF) and Empty(t_COORDRIF)  )) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TENZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TENZ_IDX,2])
    if i_bUpd
      with this
          .w_READPAR = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
          .w_OBTEST = .w_CODATREG
        if .o_CODATREG<>.w_CODATREG
          .w_COCOMPET = CALCESER(.w_CODATREG)
        endif
        .DoRTCalc(10,18,.t.)
          .link_2_3('Full')
        .DoRTCalc(20,27,.t.)
          .w_VALPAR = IIF(NOT EMPTY(.w_VALPAR1), .w_VALPAR1,g_PERVAL)
          .link_4_9('Full')
        .DoRTCalc(29,29,.t.)
          .w_CALCPIP = DEFPIP(.w_DEC)
        .DoRTCalc(31,31,.t.)
        if .o_COADDSPE<>.w_COADDSPE.or. .o_CO__TIPO<>.w_CO__TIPO
          .w_COIMPSPE = iif(.w_COADDSPE='S',.w_IMPSPE,0)
        endif
        .DoRTCalc(33,33,.t.)
        if .o_COADDINT<>.w_COADDINT.or. .o_COCODCON<>.w_COCODCON
          .w_COPERINT = IIF(.w_COADDINT='S' AND .w_ANSPRINT='N', .w_SAGINT, 0)
        endif
        .DoRTCalc(35,37,.t.)
          .w_TIPCON = 'G'
        .DoRTCalc(39,39,.t.)
        if .o_VALUTA<>.w_VALUTA
          .w_COCODVAL = IIF(EMPTY(.w_VALUTA),g_PERVAL,.w_VALUTA)
          .link_1_32('Full')
        endif
        .DoRTCalc(41,42,.t.)
        if .o_COCODVAL<>.w_COCODVAL
          .w_COCAOVAL = GETCAM(.w_COCODVAL, .w_DATVAL, 7)
        endif
        .DoRTCalc(44,49,.t.)
        if .o_CONUMRIF<>.w_CONUMRIF
          .w_TOTALE = .w_TOTALE-.w_totimp
          .w_TOTIMP = IIF(.w_CO__TIPO='M',.w_COTOTIMP,(IIF(.w_SEGNO='A',.w_TOTPAR +.w_TOTABB,-(.w_TOTPAR +.w_TOTABB))))
          .w_TOTALE = .w_TOTALE+.w_totimp
        endif
          .w_COCODBAN = IIF(.w_CO__TIPO='M',.w_CODBAN,.w_COCODBAN)
        if .o_CORIFCON<>.w_CORIFCON
          .link_1_37('Full')
        endif
        if .o_CO__TIPO<>.w_CO__TIPO.or. .o_COTIPEFF<>.w_COTIPEFF
          .link_1_38('Full')
        endif
        .DoRTCalc(54,55,.t.)
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(57,59,.t.)
          .w_CODBAN = .w_CODBAN
        if .o_CODATREG<>.w_CODATREG
          .w_INISCA = .w_CODATREG-60
        endif
        if .o_CODATREG<>.w_CODATREG
          .w_FINSCA = .w_CODATREG
        endif
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .DoRTCalc(63,71,.t.)
        if .o_COSERIAL<>.w_COSERIAL
          .w_DATINT = .w_CODATINT
        endif
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(Empty(.w_NUMPAR) and Not empty(.w_COSERRIF), AH_Msgformat('Contenzioso storicizzato'),''))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate()
        .DoRTCalc(73,78,.t.)
        if .o_COSERIAL<>.w_COSERIAL
          .w_OLDSTATUS = .w_COSTATUS
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEINS","i_CODAZI,w_COSERIAL")
          .op_COSERIAL = .w_COSERIAL
        endif
        if .op_CODAZI<>.w_CODAZI .or. .op_COCOMPET<>.w_COCOMPET
           cp_AskTableProg(this,i_nConn,"PRINS","i_CODAZI,w_COCOMPET,w_CONUMREG")
          .op_CONUMREG = .w_CONUMREG
        endif
        if .op_CODAZI<>.w_CODAZI .or. .op_COCOMPET<>.w_COCOMPET
           cp_AskTableProg(this,i_nConn,"GRINS","i_CODAZI,w_COCOMPET,w_COCODRAG")
          .op_COCODRAG = .w_COCODRAG
        endif
        .op_CODAZI = .w_CODAZI
        .op_CODAZI = .w_CODAZI
        .op_COCOMPET = .w_COCOMPET
        .op_CODAZI = .w_CODAZI
        .op_COCOMPET = .w_COCOMPET
      endwith
      this.DoRTCalc(80,86,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_COSERRIF with this.w_COSERRIF
      replace t_COORDRIF with this.w_COORDRIF
      replace t_CONUMRIF with this.w_CONUMRIF
      replace t_TOTABB with this.w_TOTABB
      replace t_TOTPAR with this.w_TOTPAR
      replace t_SEGNO with this.w_SEGNO
      replace t_CODBAN with this.w_CODBAN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(Empty(.w_NUMPAR) and Not empty(.w_COSERRIF), AH_Msgformat('Contenzioso storicizzato'),''))
        .oPgFrm.Page2.oPag.oObj_4_28.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCONUMREG_1_6.enabled = this.oPgFrm.Page1.oPag.oCONUMREG_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCOCOMPET_1_9.enabled = this.oPgFrm.Page1.oPag.oCOCOMPET_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCO__TIPO_1_10.enabled = this.oPgFrm.Page1.oPag.oCO__TIPO_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.enabled = this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCOCODCON_1_15.enabled = this.oPgFrm.Page1.oPag.oCOCODCON_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCODESCRI_1_20.enabled = this.oPgFrm.Page1.oPag.oCODESCRI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCO__NOTE_1_22.enabled = this.oPgFrm.Page1.oPag.oCO__NOTE_1_22.mCond()
    this.oPgFrm.Page2.oPag.oCOADDSPE_4_12.enabled = this.oPgFrm.Page2.oPag.oCOADDSPE_4_12.mCond()
    this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.enabled = this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.mCond()
    this.oPgFrm.Page2.oPag.oCOPERINT_4_16.enabled = this.oPgFrm.Page2.oPag.oCOPERINT_4_16.mCond()
    this.oPgFrm.Page1.oPag.oCOCATPAG_1_26.enabled = this.oPgFrm.Page1.oPag.oCOCATPAG_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCOCODVAL_1_32.enabled = this.oPgFrm.Page1.oPag.oCOCODVAL_1_32.mCond()
    this.oPgFrm.Page1.oPag.oCOCAOVAL_1_35.enabled = this.oPgFrm.Page1.oPag.oCOCAOVAL_1_35.mCond()
    this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.enabled = this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.enabled = this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.mCond()
    this.oPgFrm.Page1.oPag.oINISCA_1_48.enabled = this.oPgFrm.Page1.oPag.oINISCA_1_48.mCond()
    this.oPgFrm.Page1.oPag.oFINSCA_1_49.enabled = this.oPgFrm.Page1.oPag.oFINSCA_1_49.mCond()
    this.oPgFrm.Page1.oPag.oCOCODRAG_1_54.enabled = this.oPgFrm.Page1.oPag.oCOCODRAG_1_54.mCond()
    this.oPgFrm.Page2.oPag.oCODESSPE_4_19.enabled = this.oPgFrm.Page2.oPag.oCODESSPE_4_19.mCond()
    this.oPgFrm.Page2.oPag.oCODATINT_4_21.enabled = this.oPgFrm.Page2.oPag.oCODATINT_4_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODATVAL_1_11.visible=!this.oPgFrm.Page1.oPag.oCODATVAL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.visible=!this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.mHide()
    this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.visible=!this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_14.visible=!this.oPgFrm.Page2.oPag.oStr_4_14.mHide()
    this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.visible=!this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.visible=!this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_57.visible=!this.oPgFrm.Page1.oPag.oBtn_1_57.mHide()
    this.oPgFrm.Page2.oPag.oCODESSPE_4_19.visible=!this.oPgFrm.Page2.oPag.oCODESSPE_4_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_lTable = "PAR_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2], .t., this.PAR_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PCCODAZI,PCPAGMOR,PCDURMOR,PCSPEINS,PCVALIMP,PCINTSOLL,PCCONTRO";
                   +" from "+i_cTable+" "+i_lTable+" where PCCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PCCODAZI',this.w_READPAR)
            select PCCODAZI,PCPAGMOR,PCDURMOR,PCSPEINS,PCVALIMP,PCINTSOLL,PCCONTRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PCCODAZI,space(5))
      this.w_PAGMOR = NVL(_Link_.PCPAGMOR,space(5))
      this.w_DURMOR = NVL(_Link_.PCDURMOR,0)
      this.w_IMPSPE = NVL(_Link_.PCSPEINS,0)
      this.w_VALPAR1 = NVL(_Link_.PCVALIMP,space(3))
      this.w_GIOPAR = NVL(_Link_.PCINTSOLL,0)
      this.w_CONTRO = NVL(_Link_.PCCONTRO,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(5)
      endif
      this.w_PAGMOR = space(5)
      this.w_DURMOR = 0
      this.w_IMPSPE = 0
      this.w_VALPAR1 = space(3)
      this.w_GIOPAR = 0
      this.w_CONTRO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])+'\'+cp_ToStr(_Link_.PCCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODCON
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODCON_1_15'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANFLRAGG,ANGESCON,ANDATMOR,ANPAGPAR,ANSAGINT,ANSPRINT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_VALUTA = NVL(_Link_.ANCODVAL,space(3))
      this.w_FLAGSCA = NVL(_Link_.ANFLRAGG,space(1))
      this.w_GESCON = NVL(_Link_.ANGESCON,space(1))
      this.w_DATMOR = NVL(cp_ToDate(_Link_.ANDATMOR),ctod("  /  /  "))
      this.w_PAGPAR = NVL(_Link_.ANPAGPAR,space(5))
      this.w_SAGINT = NVL(_Link_.ANSAGINT,0)
      this.w_ANSPRINT = NVL(_Link_.ANSPRINT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCON = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_VALUTA = space(3)
      this.w_FLAGSCA = space(1)
      this.w_GESCON = space(1)
      this.w_DATMOR = ctod("  /  /  ")
      this.w_PAGPAR = space(5)
      this.w_SAGINT = 0
      this.w_ANSPRINT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONUMRIF
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONUMRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONUMRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTOTIMP,PTNUMDOC,PTALFDOC,PTBANNOS,PTDATDOC,PTTOTABB,PT_SEGNO";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_CONUMRIF);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_COSERRIF);
                   +" and PTROWORD="+cp_ToStrODBC(this.w_COORDRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_COSERRIF;
                       ,'PTROWORD',this.w_COORDRIF;
                       ,'CPROWNUM',this.w_CONUMRIF)
            select PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTOTIMP,PTNUMDOC,PTALFDOC,PTBANNOS,PTDATDOC,PTTOTABB,PT_SEGNO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONUMRIF = NVL(_Link_.CPROWNUM,0)
      this.w_NUMPAR = NVL(_Link_.PTNUMPAR,space(31))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.PTDATSCA),ctod("  /  /  "))
      this.w_TOTPAR = NVL(_Link_.PTTOTIMP,0)
      this.w_NUMDOC = NVL(_Link_.PTNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.PTALFDOC,space(10))
      this.w_CODBAN = NVL(_Link_.PTBANNOS,space(15))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.PTDATDOC),ctod("  /  /  "))
      this.w_TOTABB = NVL(_Link_.PTTOTABB,0)
      this.w_SEGNO = NVL(_Link_.PT_SEGNO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONUMRIF = 0
      endif
      this.w_NUMPAR = space(31)
      this.w_DATSCA = ctod("  /  /  ")
      this.w_TOTPAR = 0
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_CODBAN = space(15)
      this.w_DATDOC = ctod("  /  /  ")
      this.w_TOTABB = 0
      this.w_SEGNO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.PTROWORD,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONUMRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAR_TITE_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CPROWNUM as CPROWNUM203"+ ",link_2_3.PTNUMPAR as PTNUMPAR203"+ ",link_2_3.PTDATSCA as PTDATSCA203"+ ",link_2_3.PTTOTIMP as PTTOTIMP203"+ ",link_2_3.PTNUMDOC as PTNUMDOC203"+ ",link_2_3.PTALFDOC as PTALFDOC203"+ ",link_2_3.PTBANNOS as PTBANNOS203"+ ",link_2_3.PTDATDOC as PTDATDOC203"+ ",link_2_3.PTTOTABB as PTTOTABB203"+ ",link_2_3.PT_SEGNO as PT_SEGNO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CONDTENZ.CONUMRIF=link_2_3.CPROWNUM"+" and CONDTENZ.COSERRIF=link_2_3.PTSERIAL"+" and CONDTENZ.COORDRIF=link_2_3.PTROWORD"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CONDTENZ.CONUMRIF=link_2_3.CPROWNUM(+)"'+'+" and CONDTENZ.COSERRIF=link_2_3.PTSERIAL(+)"'+'+" and CONDTENZ.COORDRIF=link_2_3.PTROWORD(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VALPAR
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPAR)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPAR = NVL(_Link_.VACODVAL,space(3))
      this.w_DEC = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALPAR = space(3)
      endif
      this.w_DEC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COCODVAL))
          select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOCODVAL_1_32'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VADESVAL,VADECTOT,VACAOVAL,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_TESVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_DECTOT = 0
      this.w_TESVAL1 = 0
      this.w_SIMVAL = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COCODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_DECTOT = 0
        this.w_TESVAL1 = 0
        this.w_SIMVAL = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.VACODVAL as VACODVAL132"+ ",link_1_32.VADESVAL as VADESVAL132"+ ",link_1_32.VADECTOT as VADECTOT132"+ ",link_1_32.VACAOVAL as VACAOVAL132"+ ",link_1_32.VASIMVAL as VASIMVAL132"+ ",link_1_32.VADTOBSO as VADTOBSO132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on CON_TENZ.COCODVAL=link_1_32.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and CON_TENZ.COCODVAL=link_1_32.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCONTRO
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_COCONTRO))
          select ANTIPCON,ANCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCONTRO_1_37'),i_cWhere,'GSAR_BZC',"Conti contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_COCONTRO)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCONTRO = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COCONTRO = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COBANPRE
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COBANPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gste_acb',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_COBANPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_COBANPRE))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COBANPRE)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COBANPRE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCOBANPRE_1_38'),i_cWhere,'gste_acb',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COBANPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_COBANPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_COBANPRE)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COBANPRE = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COBANPRE = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COBANPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCONUMREG_1_6.value==this.w_CONUMREG)
      this.oPgFrm.Page1.oPag.oCONUMREG_1_6.value=this.w_CONUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATREG_1_7.value==this.w_CODATREG)
      this.oPgFrm.Page1.oPag.oCODATREG_1_7.value=this.w_CODATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOMPET_1_9.value==this.w_COCOMPET)
      this.oPgFrm.Page1.oPag.oCOCOMPET_1_9.value=this.w_COCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oCO__TIPO_1_10.RadioValue()==this.w_CO__TIPO)
      this.oPgFrm.Page1.oPag.oCO__TIPO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATVAL_1_11.value==this.w_CODATVAL)
      this.oPgFrm.Page1.oPag.oCODATVAL_1_11.value=this.w_CODATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.RadioValue()==this.w_COTIPEFF)
      this.oPgFrm.Page1.oPag.oCOTIPEFF_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCON_1_15.value==this.w_COCODCON)
      this.oPgFrm.Page1.oPag.oCOCODCON_1_15.value=this.w_COCODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSTATUS_4_1.RadioValue()==this.w_COSTATUS)
      this.oPgFrm.Page2.oPag.oCOSTATUS_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCRI_1_20.value==this.w_CODESCRI)
      this.oPgFrm.Page1.oPag.oCODESCRI_1_20.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCO__NOTE_1_22.value==this.w_CO__NOTE)
      this.oPgFrm.Page1.oPag.oCO__NOTE_1_22.value=this.w_CO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_25.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_25.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCONUMLIV_4_4.value==this.w_CONUMLIV)
      this.oPgFrm.Page2.oPag.oCONUMLIV_4_4.value=this.w_CONUMLIV
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATULT_4_6.value==this.w_CODATULT)
      this.oPgFrm.Page2.oPag.oCODATULT_4_6.value=this.w_CODATULT
    endif
    if not(this.oPgFrm.Page2.oPag.oVALPAR_4_9.RadioValue()==this.w_VALPAR)
      this.oPgFrm.Page2.oPag.oVALPAR_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOADDSPE_4_12.RadioValue()==this.w_COADDSPE)
      this.oPgFrm.Page2.oPag.oCOADDSPE_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.value==this.w_COIMPSPE)
      this.oPgFrm.Page2.oPag.oCOIMPSPE_4_13.value=this.w_COIMPSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOADDINT_4_15.RadioValue()==this.w_COADDINT)
      this.oPgFrm.Page2.oPag.oCOADDINT_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOPERINT_4_16.value==this.w_COPERINT)
      this.oPgFrm.Page2.oPag.oCOPERINT_4_16.value=this.w_COPERINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCATPAG_1_26.RadioValue()==this.w_COCATPAG)
      this.oPgFrm.Page1.oPag.oCOCATPAG_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODVAL_1_32.value==this.w_COCODVAL)
      this.oPgFrm.Page1.oPag.oCOCODVAL_1_32.value=this.w_COCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAOVAL_1_35.value==this.w_COCAOVAL)
      this.oPgFrm.Page1.oPag.oCOCAOVAL_1_35.value=this.w_COCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDOC_2_9.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oDATDOC_2_9.value=this.w_DATDOC
      replace t_DATDOC with this.oPgFrm.Page1.oPag.oDATDOC_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.value==this.w_COCONTRO)
      this.oPgFrm.Page1.oPag.oCOCONTRO_1_37.value=this.w_COCONTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.value==this.w_COBANPRE)
      this.oPgFrm.Page1.oPag.oCOBANPRE_1_38.value=this.w_COBANPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oINISCA_1_48.value==this.w_INISCA)
      this.oPgFrm.Page1.oPag.oINISCA_1_48.value=this.w_INISCA
    endif
    if not(this.oPgFrm.Page1.oPag.oFINSCA_1_49.value==this.w_FINSCA)
      this.oPgFrm.Page1.oPag.oFINSCA_1_49.value=this.w_FINSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page2.oPag.oCOADDSOL_4_18.RadioValue()==this.w_COADDSOL)
      this.oPgFrm.Page2.oPag.oCOADDSOL_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODRAG_1_54.value==this.w_COCODRAG)
      this.oPgFrm.Page1.oPag.oCOCODRAG_1_54.value=this.w_COCODRAG
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESSPE_4_19.RadioValue()==this.w_CODESSPE)
      this.oPgFrm.Page2.oPag.oCODESSPE_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATINT_4_21.value==this.w_CODATINT)
      this.oPgFrm.Page2.oPag.oCODATINT_4_21.value=this.w_CODATINT
    endif
    if not(this.oPgFrm.Page4.oPag.oCONOTEIN_6_1.value==this.w_CONOTEIN)
      this.oPgFrm.Page4.oPag.oCONOTEIN_6_1.value=this.w_CONOTEIN
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_5.value==this.w_NUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_5.value=this.w_NUMPAR
      replace t_NUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMPAR_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_6.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_6.value=this.w_NUMDOC
      replace t_NUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_7.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_7.value=this.w_ALFDOC
      replace t_ALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.value==this.w_COTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.value=this.w_COTOTIMP
      replace t_COTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOTOTIMP_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.value=this.w_TOTIMP
      replace t_TOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTIMP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODBAN_2_11.value==this.w_COCODBAN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODBAN_2_11.value=this.w_COCODBAN
      replace t_COCODBAN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODBAN_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_16.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_16.value=this.w_DATSCA
      replace t_DATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATSCA_2_16.value
    endif
    cp_SetControlsValueExtFlds(this,'CON_TENZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((NOT EMPTY(.w_COBANPRE) AND .w_COTIPEFF<>'A') OR .w_CO__TIPO='M' OR  (NOT EMPTY(.w_COCONTRO) AND .w_COTIPEFF='A') )
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Attenzione banca\contropartita non specificata"))
          case   (empty(.w_CONUMREG))  and ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCONUMREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CONUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Contenzioso contabilizzato impossibile variare")
          case   (empty(.w_CODATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATREG_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CODATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COCODCON))  and ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODCON_1_15.SetFocus()
            i_bnoObbl = !empty(.w_COCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COPERINT) or not(.w_COADDINT<>'S' OR .w_COPERINT<>0))  and (.w_COADDINT='S' )
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOPERINT_4_16.SetFocus()
            i_bnoObbl = !empty(.w_COPERINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COCODVAL) or not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODVAL_1_32.SetFocus()
            i_bnoObbl = !empty(.w_COCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_inisca<=.w_finsca)  and (.cFunction='Load')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oFINSCA_1_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale inferiore alla data iniziale")
          case   (empty(.w_COCODRAG))  and ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODRAG_1_54.SetFocus()
            i_bnoObbl = !empty(.w_COCODRAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATINT))  and (.w_COADDINT $ 'M-S')
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCODATINT_4_21.SetFocus()
            i_bnoObbl = !empty(.w_CODATINT)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSSO_MDS.CheckForm()
      if i_bres
        i_bres=  .GSSO_MDS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSSO_MIA.CheckForm()
      if i_bres
        i_bres=  .GSSO_MIA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsso_mco
      if i_bRes
        i_bRes =CHKMEMO(.w_CONOTEIN)
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CONUMRIF)and Empty(.w_COSERRIF) and Empty(.w_COORDRIF)  )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COSERIAL = this.w_COSERIAL
    this.o_CODATREG = this.w_CODATREG
    this.o_CO__TIPO = this.w_CO__TIPO
    this.o_COTIPEFF = this.w_COTIPEFF
    this.o_COCODCON = this.w_COCODCON
    this.o_CONUMRIF = this.w_CONUMRIF
    this.o_COADDSPE = this.w_COADDSPE
    this.o_COADDINT = this.w_COADDINT
    this.o_VALUTA = this.w_VALUTA
    this.o_COCODVAL = this.w_COCODVAL
    this.o_CORIFCON = this.w_CORIFCON
    * --- GSSO_MDS : Depends On
    this.GSSO_MDS.SaveDependsOn()
    * --- GSSO_MIA : Depends On
    this.GSSO_MIA.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CONUMRIF)and Empty(t_COSERRIF) and Empty(t_COORDRIF)  ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COSERRIF=space(10)
      .w_COORDRIF=0
      .w_CONUMRIF=0
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_NUMPAR=space(31)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_COTOTIMP=0
      .w_DATDOC=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_COCODBAN=space(15)
      .w_TOTABB=0
      .w_TOTPAR=0
      .w_SEGNO=space(1)
      .w_CODBAN=space(15)
      .w_DATSCA=ctod("  /  /  ")
      .DoRTCalc(1,19,.f.)
      if not(empty(.w_CONUMRIF))
        .link_2_3('Full')
      endif
      .DoRTCalc(20,49,.f.)
        .w_TOTIMP = IIF(.w_CO__TIPO='M',.w_COTOTIMP,(IIF(.w_SEGNO='A',.w_TOTPAR +.w_TOTABB,-(.w_TOTPAR +.w_TOTABB))))
        .w_COCODBAN = IIF(.w_CO__TIPO='M',.w_CODBAN,.w_COCODBAN)
      .DoRTCalc(52,59,.f.)
        .w_CODBAN = .w_CODBAN
    endwith
    this.DoRTCalc(61,86,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_COSERRIF = t_COSERRIF
    this.w_COORDRIF = t_COORDRIF
    this.w_CONUMRIF = t_CONUMRIF
    this.w_CPROWORD = t_CPROWORD
    this.w_NUMPAR = t_NUMPAR
    this.w_NUMDOC = t_NUMDOC
    this.w_ALFDOC = t_ALFDOC
    this.w_COTOTIMP = t_COTOTIMP
    this.w_DATDOC = t_DATDOC
    this.w_TOTIMP = t_TOTIMP
    this.w_COCODBAN = t_COCODBAN
    this.w_TOTABB = t_TOTABB
    this.w_TOTPAR = t_TOTPAR
    this.w_SEGNO = t_SEGNO
    this.w_CODBAN = t_CODBAN
    this.w_DATSCA = t_DATSCA
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_COSERRIF with this.w_COSERRIF
    replace t_COORDRIF with this.w_COORDRIF
    replace t_CONUMRIF with this.w_CONUMRIF
    replace t_CPROWORD with this.w_CPROWORD
    replace t_NUMPAR with this.w_NUMPAR
    replace t_NUMDOC with this.w_NUMDOC
    replace t_ALFDOC with this.w_ALFDOC
    replace t_COTOTIMP with this.w_COTOTIMP
    replace t_DATDOC with this.w_DATDOC
    replace t_TOTIMP with this.w_TOTIMP
    replace t_COCODBAN with this.w_COCODBAN
    replace t_TOTABB with this.w_TOTABB
    replace t_TOTPAR with this.w_TOTPAR
    replace t_SEGNO with this.w_SEGNO
    replace t_CODBAN with this.w_CODBAN
    replace t_DATSCA with this.w_DATSCA
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_totimp
        .SetControlsValue()
      endwith
  EndProc
  func CanAdd()
    local i_res
    i_res=NOT EMPTY(this.w_VALPAR1) AND NOT EMPTY(this.w_PAGMOR) AND not empty(this.w_CONTRO)
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione compilare parametri contenzioso"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsso_mcoPag1 as StdContainer
  Width  = 827
  height = 429
  stdWidth  = 827
  stdheight = 429
  resizeXpos=695
  resizeYpos=365
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONUMREG_1_6 as StdField with uid="PEVDCXUGFI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CONUMREG", cQueryName = "CONUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Contenzioso contabilizzato impossibile variare",;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 23109229,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=102, Top=16, cSayPict='"999999"', cGetPict='"999999"'

  func oCONUMREG_1_6.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  proc oCONUMREG_1_6.mBefore
    with this.Parent.oContained
      this.cQueryName = "COCOMPET,CONUMREG"
    endwith
  endproc

  add object oCODATREG_1_7 as StdField with uid="VGRNOAPVLN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODATREG", cQueryName = "CODATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 29097581,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=331, Top=17

  add object oCOCOMPET_1_9 as StdField with uid="WYQCGUWJBC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COCOMPET", cQueryName = "COCOMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 10883462,;
   bGlobalFont=.t.,;
    Height=21, Width=54, Left=168, Top=16, InputMask=replicate('X',4), tabstop=.f.

  func oCOCOMPET_1_9.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc


  add object oCO__TIPO_1_10 as StdCombo with uid="XLDJCOZKEF",rtseq=10,rtrep=.f.,left=495,top=17,width=133,height=21;
    , ToolTipText = "Tipo: I=insoluto, M=mancato pagamento";
    , HelpContextID = 119820683;
    , cFormVar="w_CO__TIPO",RowSource=""+"Insoluto,"+"Mancato pagamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCO__TIPO_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CO__TIPO,&i_cF..t_CO__TIPO),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'M',;
    space(1))))
  endfunc
  func oCO__TIPO_1_10.GetRadio()
    this.Parent.oContained.w_CO__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oCO__TIPO_1_10.ToRadio()
    this.Parent.oContained.w_CO__TIPO=trim(this.Parent.oContained.w_CO__TIPO)
    return(;
      iif(this.Parent.oContained.w_CO__TIPO=='I',1,;
      iif(this.Parent.oContained.w_CO__TIPO=='M',2,;
      0)))
  endfunc

  func oCO__TIPO_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCO__TIPO_1_10.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))) AND .cFunction='Load' AND .w_EDITTIPO)
    endwith
  endfunc

  add object oCODATVAL_1_11 as StdField with uid="XBKFDRQHCW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODATVAL", cQueryName = "CODATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data valuta di riferimento per la generazione di movimenti di C\C",;
    HelpContextID = 172229006,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=331, Top=39

  func oCODATVAL_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC<>'S' OR (.w_CO__TIPO<>'I' AND g_BANC='S'))
    endwith
    endif
  endfunc


  add object oCOTIPEFF_1_12 as StdCombo with uid="PBOSVYIVND",rtseq=12,rtrep=.f.,left=495,top=42,width=133,height=21;
    , ToolTipText = "Tipologia effetti E=solo effetti A= nessun effetto";
    , HelpContextID = 75824748;
    , cFormVar="w_COTIPEFF",RowSource=""+"Solo effetti,"+"Nessun effetto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPEFF_1_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COTIPEFF,&i_cF..t_COTIPEFF),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'A',;
    space(1))))
  endfunc
  func oCOTIPEFF_1_12.GetRadio()
    this.Parent.oContained.w_COTIPEFF = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPEFF_1_12.ToRadio()
    this.Parent.oContained.w_COTIPEFF=trim(this.Parent.oContained.w_COTIPEFF)
    return(;
      iif(this.Parent.oContained.w_COTIPEFF=='E',1,;
      iif(this.Parent.oContained.w_COTIPEFF=='A',2,;
      0)))
  endfunc

  func oCOTIPEFF_1_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOTIPEFF_1_12.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))) AND .cFunction='Load')
    endwith
  endfunc

  func oCOTIPEFF_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CO__TIPO='M')
    endwith
    endif
  endfunc

  add object oCOCODCON_1_15 as StdField with uid="ODSEAGZCNP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COCODCON", cQueryName = "COCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 238424460,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=102, Top=67, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCON"

  func oCOCODCON_1_15.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  func oCOCODCON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODCON_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCON_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODCON_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCOCODCON_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODCON
    i_obj.ecpSave()
  endproc

  add object oCODESCRI_1_20 as StdField with uid="UCSZSZPBRO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Il contenuto di tale campo � riportato nei dati di riga e di testata nella registrazione di contabilizzazione insoluto.Se non valorizzato la procedura considera eventuali Formule di riga e di testata indicate sulla causale di contabilizzazione insoluti.",;
    HelpContextID = 223347089,;
   bGlobalFont=.t.,;
    Height=21, Width=526, Left=102, Top=94, InputMask=replicate('X',50)

  func oCODESCRI_1_20.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  add object oCO__NOTE_1_22 as StdMemo with uid="MIMELPWCNU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CO__NOTE", cQueryName = "CO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note contenzioso",;
    HelpContextID = 25448853,;
   bGlobalFont=.t.,;
    Height=58, Width=712, Left=102, Top=121

  func oCO__NOTE_1_22.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  add object oDESCLI_1_25 as StdField with uid="BBRJYHTJBG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente",;
    HelpContextID = 130096074,;
   bGlobalFont=.t.,;
    Height=21, Width=402, Left=226, Top=67, InputMask=replicate('X',40)


  add object oCOCATPAG_1_26 as StdCombo with uid="DTAYFPPKJS",rtseq=35,rtrep=.f.,left=136,top=209,width=136,height=21;
    , ToolTipText = "Categoria pagamento";
    , HelpContextID = 4460947;
    , cFormVar="w_COCATPAG",RowSource=""+"Ric. bancaria/RiBa,"+"Rimessa diretta,"+"Bonifico,"+"Anticipazioni/M.AV.,"+"R.I.D.,"+"Cambiale/tratta,"+"Compensazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOCATPAG_1_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COCATPAG,&i_cF..t_COCATPAG),this.value)
    return(iif(xVal =1,'RB',;
    iif(xVal =2,'RD',;
    iif(xVal =3,'BO',;
    iif(xVal =4,'MA',;
    iif(xVal =5,'RI',;
    iif(xVal =6,'CA',;
    iif(xVal =7,'CC',;
    space(2)))))))))
  endfunc
  func oCOCATPAG_1_26.GetRadio()
    this.Parent.oContained.w_COCATPAG = this.RadioValue()
    return .t.
  endfunc

  func oCOCATPAG_1_26.ToRadio()
    this.Parent.oContained.w_COCATPAG=trim(this.Parent.oContained.w_COCATPAG)
    return(;
      iif(this.Parent.oContained.w_COCATPAG=='RB',1,;
      iif(this.Parent.oContained.w_COCATPAG=='RD',2,;
      iif(this.Parent.oContained.w_COCATPAG=='BO',3,;
      iif(this.Parent.oContained.w_COCATPAG=='MA',4,;
      iif(this.Parent.oContained.w_COCATPAG=='RI',5,;
      iif(this.Parent.oContained.w_COCATPAG=='CA',6,;
      iif(this.Parent.oContained.w_COCATPAG=='CC',7,;
      0))))))))
  endfunc

  func oCOCATPAG_1_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOCATPAG_1_26.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oCOCODVAL_1_32 as StdField with uid="WFBYFNOSCI",rtseq=40,rtrep=.f.,;
    cFormVar = "w_COCODVAL", cQueryName = "COCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta scadenze",;
    HelpContextID = 188092814,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=237, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COCODVAL"

  func oCOCODVAL_1_32.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oCOCODVAL_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODVAL_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODVAL_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOCODVAL_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCOCODVAL_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COCODVAL
    i_obj.ecpSave()
  endproc

  add object oCOCAOVAL_1_35 as StdField with uid="VYINVUYAUD",rtseq=43,rtrep=.f.,;
    cFormVar = "w_COCAOVAL", cQueryName = "COCAOVAL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio di riapertura scadenze",;
    HelpContextID = 177475982,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=382, Top=238, cSayPict='"99999.9999999"', cGetPict='"99999.9999999"', tabstop=.f.

  func oCOCAOVAL_1_35.mCond()
    with this.Parent.oContained
      return (.w_TESVAL1=0 and  EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  add object oCOCONTRO_1_37 as StdField with uid="MKNOSSQFNY",rtseq=52,rtrep=.f.,;
    cFormVar = "w_COCONTRO", cQueryName = "COCONTRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita riapertura scadenze",;
    HelpContextID = 211161483,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=550, Top=239, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCONTRO"

  func oCOCONTRO_1_37.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  func oCOCONTRO_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPEFF<>'A' OR .w_CO__TIPO='M')
    endwith
    endif
  endfunc

  func oCOCONTRO_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCONTRO_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCONTRO_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCONTRO_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contabili",'',this.parent.oContained
  endproc
  proc oCOCONTRO_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCONTRO
    i_obj.ecpSave()
  endproc

  add object oCOBANPRE_1_38 as StdField with uid="TODWVZSXJK",rtseq=53,rtrep=.f.,;
    cFormVar = "w_COBANPRE", cQueryName = "COBANPRE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Banca di presentazione per eseguire editing nelle partite.",;
    HelpContextID = 10756501,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=550, Top=239, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="gste_acb", oKey_1_1="BACODBAN", oKey_1_2="this.w_COBANPRE"

  func oCOBANPRE_1_38.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  func oCOBANPRE_1_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( .w_CO__TIPO='M' OR .w_COTIPEFF='A')
    endwith
    endif
  endfunc

  func oCOBANPRE_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOBANPRE_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOBANPRE_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCOBANPRE_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'gste_acb',"Conti banche",'',this.parent.oContained
  endproc
  proc oCOBANPRE_1_38.mZoomOnZoom
    local i_obj
    i_obj=gste_acb()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_COBANPRE
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_40 as StdField with uid="HNZPGGBQRQ",rtseq=55,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90076890,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=202, Top=237, InputMask=replicate('X',5)


  add object oBtn_1_42 as StdButton with uid="MLEQPFNYXG",left=766, top=212, width=48,height=45,;
    CpPicture="BMP\SOLDIT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare partite da inserire.";
    , HelpContextID = 88207606;
    , tabstop=.f., caption='\<Partite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        do GSSO_BI1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_COCODCON) AND .cFunction='Load')
    endwith
  endfunc

  func oBtn_1_42.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( .cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oINISCA_1_48 as StdField with uid="AAPVYXAHRQ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_INISCA", cQueryName = "INISCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale",;
    HelpContextID = 4305530,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=382, Top=211

  func oINISCA_1_48.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oFINSCA_1_49 as StdField with uid="ZSGDCBMMJB",rtseq=62,rtrep=.f.,;
    cFormVar = "w_FINSCA", cQueryName = "FINSCA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale inferiore alla data iniziale",;
    ToolTipText = "Data finale",;
    HelpContextID = 4286378,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=550, Top=211

  func oFINSCA_1_49.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oFINSCA_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_inisca<=.w_finsca)
    endwith
    return bRes
  endfunc

  add object oCOCODRAG_1_54 as StdField with uid="TMYQETFUTP",rtseq=67,rtrep=.f.,;
    cFormVar = "w_COCODRAG", cQueryName = "COCODRAG",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice raggruppamento",;
    HelpContextID = 255201683,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=102, Top=41, InputMask=replicate('X',10)

  func oCOCODRAG_1_54.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc


  add object oBtn_1_57 as StdButton with uid="BETIBTUGVT",left=766, top=17, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare registrazione dell'insoluto.";
    , HelpContextID = 21333894;
    , tabstop=.f., caption='\<Reg.cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        gsar_bzp(this.Parent.oContained,.w_corifcon)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mCond()
    with this.Parent.oContained
      return (.w_CO__TIPO='I' and NOT EMPTY(NVL(.w_CORIFCON,space(10))))
    endwith
  endfunc

  func oBtn_1_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,space(10))))
    endwith
   endif
  endfunc


  add object oObj_1_59 as cp_runprogram with uid="QCWPBCUDAY",left=1, top=442, width=182,height=21,;
    caption='GSSO_BPR(AGG)',;
   bGlobalFont=.t.,;
    prg="GSSO_BPR('AGG')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 178870728


  add object oObj_1_61 as cp_runprogram with uid="BCFYKECQOR",left=446, top=442, width=228,height=21,;
    caption='GSSO_BPR(CON)',;
   bGlobalFont=.t.,;
    prg="GSSO_BPR('CON')",;
    cEvent = "Edit Started,Delete start",;
    nPag=1;
    , HelpContextID = 178378696


  add object oObj_1_65 as cp_runprogram with uid="LYKSJFYTPZ",left=1, top=462, width=183,height=21,;
    caption='GSSO_BAN(banc)',;
   bGlobalFont=.t.,;
    prg="GSSO_BAN('BANC')",;
    cEvent = "w_COBANPRE Changed,w_COCONTRO Changed",;
    nPag=1;
    , HelpContextID = 232822988


  add object oObj_1_66 as cp_runprogram with uid="IOBSSHPSAT",left=188, top=462, width=207,height=21,;
    caption='GSSO_BCK(C)',;
   bGlobalFont=.t.,;
    prg="GSSO_BCK('C')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 226637775


  add object oObj_1_71 as cp_runprogram with uid="YXDJBBLWNQ",left=188, top=442, width=257,height=21,;
    caption='GSSO_BAN(DELE)',;
   bGlobalFont=.t.,;
    prg="GSSO_BAN('DELE')",;
    cEvent = "w_COTIPEFF Changed",;
    nPag=1;
    , HelpContextID = 266630860


  add object oObj_1_73 as cp_calclbl with uid="UHYPVCCROL",left=2, top=410, width=183,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=1,;
    nPag=1;
    , HelpContextID = 187534874


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=268, width=814,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DATSCA",Label1="Scad. al",Field2="NUMPAR",Label2="N. partita",Field3="NUMDOC",Label3="Doc. n.",Field4="ALFDOC",Label4="Serie",Field5="TOTIMP",Label5="Importo",Field6="COCODBAN",Label6="Banca presentaz.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49441670

  add object oStr_1_14 as StdString with uid="TYROERANDQ",Visible=.t., Left=2, Top=17,;
    Alignment=1, Width=96, Height=19,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="UZFRVVHUYM",Visible=.t., Left=159, Top=19,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ZTVPCGDDDT",Visible=.t., Left=298, Top=19,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="ACDSEDGIYZ",Visible=.t., Left=2, Top=71,;
    Alignment=1, Width=96, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="GBKDNELUXD",Visible=.t., Left=2, Top=123,;
    Alignment=1, Width=96, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="KNTYWKBAKG",Visible=.t., Left=2, Top=94,;
    Alignment=1, Width=96, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JCHPNZYLXR",Visible=.t., Left=430, Top=18,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KMNELTTQKQ",Visible=.t., Left=39, Top=210,;
    Alignment=1, Width=94, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="CBHNVAHKOU",Visible=.t., Left=201, Top=407,;
    Alignment=1, Width=110, Height=18,;
    Caption="Documento del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="WMNOZUHYTK",Visible=.t., Left=7, Top=188,;
    Alignment=0, Width=268, Height=18,;
    Caption="Partite di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="EGBEMGOOWY",Visible=.t., Left=4, Top=238,;
    Alignment=1, Width=128, Height=18,;
    Caption="Valuta contenzioso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="YDCEAZCOVM",Visible=.t., Left=283, Top=240,;
    Alignment=1, Width=94, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="JMOEYDOZST",Visible=.t., Left=283, Top=214,;
    Alignment=1, Width=94, Height=18,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="SGBNMBLPMQ",Visible=.t., Left=530, Top=214,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="WJYIKVNCFN",Visible=.t., Left=2, Top=43,;
    Alignment=1, Width=96, Height=18,;
    Caption="Codice raggr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="GPXHMUSPEE",Visible=.t., Left=479, Top=240,;
    Alignment=1, Width=69, Height=18,;
    Caption="Banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return ( .w_CO__TIPO='M' OR .w_COTIPEFF='A')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="KSJZKFAHUK",Visible=.t., Left=232, Top=43,;
    Alignment=1, Width=97, Height=18,;
    Caption="Data valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S' OR (.w_CO__TIPO<>'I' AND g_BANC='S'))
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="FCTKMQNEDH",Visible=.t., Left=430, Top=43,;
    Alignment=1, Width=63, Height=18,;
    Caption="Effetti:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_CO__TIPO='M')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="WPUWGHXCBX",Visible=.t., Left=479, Top=240,;
    Alignment=1, Width=69, Height=18,;
    Caption="Controp.:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return ( .w_CO__TIPO='M' OR .w_COTIPEFF<>'A')
    endwith
  endfunc

  add object oBox_1_44 as StdBox with uid="UZRNLAPWPJ",left=-1, top=203, width=810,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=287,;
    width=810+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=288,width=809+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDATDOC_2_9.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oDATDOC_2_9 as StdTrsField with uid="OWWMDMUTPT",rtseq=49,rtrep=.t.,;
    cFormVar="w_DATDOC",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 227545034,;
    cTotal="", bFixedPos=.t., cQueryName = "DATDOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=314, Top=405

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="XUTPUZPSCQ",rtseq=64,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    HelpContextID = 197329098,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=525, Top=405, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  add object oStr_3_2 as StdString with uid="FDZBAAGMUH",Visible=.t., Left=431, Top=406,;
    Alignment=1, Width=91, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsso_mcoPag2 as StdContainer
    Width  = 827
    height = 429
    stdWidth  = 827
    stdheight = 429
  resizeXpos=235
  resizeYpos=305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCOSTATUS_4_1 as StdCombo with uid="BDKIUXEHOP",rtseq=16,rtrep=.f.,left=89,top=20,width=161,height=21;
    , ToolTipText = "Status pratica";
    , HelpContextID = 44035705;
    , cFormVar="w_COSTATUS",RowSource=""+"Pendente,"+"Chiuso con incasso,"+"Chiuso senza incasso,"+"Pratica legale", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCOSTATUS_4_1.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSTATUS,&i_cF..t_COSTATUS),this.value)
    return(iif(xVal =1,'PE',;
    iif(xVal =2,'CI',;
    iif(xVal =3,'CS',;
    iif(xVal =4,'PL',;
    space(2))))))
  endfunc
  func oCOSTATUS_4_1.GetRadio()
    this.Parent.oContained.w_COSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oCOSTATUS_4_1.ToRadio()
    this.Parent.oContained.w_COSTATUS=trim(this.Parent.oContained.w_COSTATUS)
    return(;
      iif(this.Parent.oContained.w_COSTATUS=='PE',1,;
      iif(this.Parent.oContained.w_COSTATUS=='CI',2,;
      iif(this.Parent.oContained.w_COSTATUS=='CS',3,;
      iif(this.Parent.oContained.w_COSTATUS=='PL',4,;
      0)))))
  endfunc

  func oCOSTATUS_4_1.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCONUMLIV_4_4 as StdField with uid="LEZMXKLRII",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CONUMLIV", cQueryName = "CONUMLIV",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello sollecito",;
    HelpContextID = 190881404,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=344, Top=20, cSayPict='"99"', cGetPict='"99"'

  add object oCODATULT_4_6 as StdField with uid="ZDRWUJXAFP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODATULT", cQueryName = "CODATULT",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo sollecito",;
    HelpContextID = 79429242,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=735, Top=20


  add object oVALPAR_4_9 as StdCombo with uid="FBVZDTXFNE",rtseq=28,rtrep=.f.,left=735,top=52,width=81,height=21, enabled=.f.;
    , ToolTipText = "Valuta importi dei parametri";
    , HelpContextID = 258248362;
    , cFormVar="w_VALPAR",RowSource=""+"Lire,"+"Euro", bObbl = .f. , nPag = 4;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oVALPAR_4_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..VALPAR,&i_cF..t_VALPAR),this.value)
    return(iif(xVal =1,ALLTR(g_CODLIR),;
    iif(xVal =2,ALLTR(g_CODEUR),;
    space(3))))
  endfunc
  func oVALPAR_4_9.GetRadio()
    this.Parent.oContained.w_VALPAR = this.RadioValue()
    return .t.
  endfunc

  func oVALPAR_4_9.ToRadio()
    this.Parent.oContained.w_VALPAR=trim(this.Parent.oContained.w_VALPAR)
    return(;
      iif(this.Parent.oContained.w_VALPAR==ALLTR(g_CODLIR),1,;
      iif(this.Parent.oContained.w_VALPAR==ALLTR(g_CODEUR),2,;
      0)))
  endfunc

  func oVALPAR_4_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oVALPAR_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCOADDSPE_4_12 as StdCheck with uid="BMVXZOLISN",rtseq=31,rtrep=.f.,left=89, top=89, caption="Addebito spese bancarie",;
    ToolTipText = "Se attivo abilita l'addebito delle spese bancarie",;
    HelpContextID = 239153557,;
    cFormVar="w_COADDSPE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOADDSPE_4_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COADDSPE,&i_cF..t_COADDSPE),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCOADDSPE_4_12.GetRadio()
    this.Parent.oContained.w_COADDSPE = this.RadioValue()
    return .t.
  endfunc

  func oCOADDSPE_4_12.ToRadio()
    this.Parent.oContained.w_COADDSPE=trim(this.Parent.oContained.w_COADDSPE)
    return(;
      iif(this.Parent.oContained.w_COADDSPE=='S',1,;
      0))
  endfunc

  func oCOADDSPE_4_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOADDSPE_4_12.mCond()
    with this.Parent.oContained
      return (.w_CO__TIPO='I')
    endwith
  endfunc

  add object oCOIMPSPE_4_13 as StdField with uid="TDXOEDJMDD",rtseq=32,rtrep=.f.,;
    cFormVar = "w_COIMPSPE", cQueryName = "COIMPSPE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese bancarie espresso nella valuta dei parametri",;
    HelpContextID = 225948053,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=89, Top=115, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oCOIMPSPE_4_13.mCond()
    with this.Parent.oContained
      return (.w_COADDSPE='S' AND .w_CO__TIPO='I')
    endwith
  endfunc

  func oCOIMPSPE_4_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COADDSPE<>'S')
    endwith
    endif
  endfunc


  add object oCOADDINT_4_15 as StdCombo with uid="TSGWQRAKQZ",rtseq=33,rtrep=.f.,left=89,top=52,width=161,height=21;
    , ToolTipText = "Determina l'origine del saggio di interesse da applicare: tabella saggio interessi di mora / saggio forzato";
    , HelpContextID = 138490246;
    , cFormVar="w_COADDINT",RowSource=""+"Non applicati,"+"Saggio di mora,"+"Saggio forzato", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCOADDINT_4_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COADDINT,&i_cF..t_COADDINT),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'M',;
    iif(xVal =3,'S',;
    space(1)))))
  endfunc
  func oCOADDINT_4_15.GetRadio()
    this.Parent.oContained.w_COADDINT = this.RadioValue()
    return .t.
  endfunc

  func oCOADDINT_4_15.ToRadio()
    this.Parent.oContained.w_COADDINT=trim(this.Parent.oContained.w_COADDINT)
    return(;
      iif(this.Parent.oContained.w_COADDINT=='N',1,;
      iif(this.Parent.oContained.w_COADDINT=='M',2,;
      iif(this.Parent.oContained.w_COADDINT=='S',3,;
      0))))
  endfunc

  func oCOADDINT_4_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOPERINT_4_16 as StdField with uid="POOBTCMOOO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_COPERINT", cQueryName = "COPERINT",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale per il calcolo degli interessi: se il saggio � forzato il campo pu� venire valorizzato in automatico se la percentuale interessi di mora � definita sul cliente",;
    HelpContextID = 123683206,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=344, Top=52, cSayPict='"999.99"', cGetPict='"999.99"'

  func oCOPERINT_4_16.mCond()
    with this.Parent.oContained
      return (.w_COADDINT='S' )
    endwith
  endfunc

  func oCOPERINT_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COADDINT<>'S' OR .w_COPERINT<>0)
    endwith
    return bRes
  endfunc

  add object oCOADDSOL_4_18 as StdCheck with uid="SYRVXEMOME",rtseq=66,rtrep=.f.,left=344, top=89, caption="Addebito spese solleciti",;
    ToolTipText = "Se attivo consente l'addebito delle spese solleciti",;
    HelpContextID = 239153550,;
    cFormVar="w_COADDSOL", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOADDSOL_4_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COADDSOL,&i_cF..t_COADDSOL),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCOADDSOL_4_18.GetRadio()
    this.Parent.oContained.w_COADDSOL = this.RadioValue()
    return .t.
  endfunc

  func oCOADDSOL_4_18.ToRadio()
    this.Parent.oContained.w_COADDSOL=trim(this.Parent.oContained.w_COADDSOL)
    return(;
      iif(this.Parent.oContained.w_COADDSOL=='S',1,;
      0))
  endfunc

  func oCOADDSOL_4_18.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCODESSPE_4_19 as StdCombo with uid="ZMNWYLWQFH",rtseq=71,rtrep=.f.,left=345,top=115,width=161,height=21;
    , ToolTipText = "Destinazione spese bancarie S= addebito al cliente C= addebito al costo";
    , HelpContextID = 223347093;
    , cFormVar="w_CODESSPE",RowSource=""+"Assegna spese al costo,"+"Assegna spese al cliente", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCODESSPE_4_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CODESSPE,&i_cF..t_CODESSPE),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oCODESSPE_4_19.GetRadio()
    this.Parent.oContained.w_CODESSPE = this.RadioValue()
    return .t.
  endfunc

  func oCODESSPE_4_19.ToRadio()
    this.Parent.oContained.w_CODESSPE=trim(this.Parent.oContained.w_CODESSPE)
    return(;
      iif(this.Parent.oContained.w_CODESSPE=='C',1,;
      iif(this.Parent.oContained.w_CODESSPE=='S',2,;
      0)))
  endfunc

  func oCODESSPE_4_19.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCODESSPE_4_19.mCond()
    with this.Parent.oContained
      return ( EMPTY(NVL(.w_CORIFCON,SPACE(10))))
    endwith
  endfunc

  func oCODESSPE_4_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COADDSPE<>'S' OR .w_CO__TIPO='M')
    endwith
    endif
  endfunc

  add object oCODATINT_4_21 as StdField with uid="RRSRURBJWK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_CODATINT", cQueryName = "CODATINT",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Dal giorno successivo alla data impostata verranno calcolati gli interessi di mora",;
    HelpContextID = 121897350,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=735, Top=89

  func oCODATINT_4_21.mCond()
    with this.Parent.oContained
      return (.w_COADDINT $ 'M-S')
    endwith
  endfunc


  add object oLinkPC_4_23 as stdDynamicChildContainer with uid="ZNDQKHMZNI",left=9, top=167, width=367, height=225, bOnScreen=.t.;



  add object oObj_4_28 as cp_runprogram with uid="FCQBIRDGXK",left=18, top=449, width=207,height=21,;
    caption='GSSO_BCK(S)',;
   bGlobalFont=.t.,;
    prg="GSSO_BCK('S')",;
    cEvent = "w_COSTATUS Changed",;
    nPag=4;
    , HelpContextID = 226633679

  add object oStr_4_2 as StdString with uid="JHAPVYGCKB",Visible=.t., Left=3, Top=20,;
    Alignment=1, Width=83, Height=18,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="FJUMWJUROS",Visible=.t., Left=283, Top=20,;
    Alignment=1, Width=60, Height=18,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="PYDHBVUGLG",Visible=.t., Left=614, Top=20,;
    Alignment=1, Width=118, Height=18,;
    Caption="Ultimo sollecito:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="UPKRGGRZPW",Visible=.t., Left=3, Top=116,;
    Alignment=1, Width=83, Height=18,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  func oStr_4_14.mHide()
    with this.Parent.oContained
      return (.w_COADDSPE<>'S')
    endwith
  endfunc

  add object oStr_4_17 as StdString with uid="OIQAHBMXZB",Visible=.t., Left=325, Top=52,;
    Alignment=1, Width=18, Height=18,;
    Caption="%:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_22 as StdString with uid="ROPMHEXAJP",Visible=.t., Left=614, Top=52,;
    Alignment=1, Width=118, Height=18,;
    Caption="Valuta parametri:"  ;
  , bGlobalFont=.t.

  add object oStr_4_24 as StdString with uid="PWXPAOJRNN",Visible=.t., Left=9, Top=148,;
    Alignment=0, Width=367, Height=18,;
    Caption="Incassi avvenuti"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="NPLZBWEFLO",Visible=.t., Left=3, Top=52,;
    Alignment=1, Width=83, Height=18,;
    Caption="Interessi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="NPVVLTEIJO",Visible=.t., Left=614, Top=89,;
    Alignment=1, Width=118, Height=18,;
    Caption="Data mat. int.:"  ;
  , bGlobalFont=.t.

  add object oBox_4_25 as StdBox with uid="UZSPLCCJLL",left=7, top=163, width=405,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsso_mia",lower(this.oContained.GSSO_MIA.class))=0
        this.oContained.GSSO_MIA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsso_mcoPag3 as StdContainer
    Width  = 827
    height = 429
    stdWidth  = 827
    stdheight = 429
  resizeXpos=341
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="JVTKYBRIZW",left=3, top=13, width=700, height=406, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsso_mds",lower(this.oContained.GSSO_MDS.class))=0
        this.oContained.GSSO_MDS.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

  define class tgsso_mcoPag4 as StdContainer
    Width  = 827
    height = 429
    stdWidth  = 827
    stdheight = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONOTEIN_6_1 as StdMemo with uid="YLARCODASJ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_CONOTEIN", cQueryName = "CONOTEIN",;
    bObbl = .f. , nPag = 6, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 80387700,;
   bGlobalFont=.t.,;
    Height=414, Width=813, Left=7, Top=10
enddefine

* --- Defining Body row
define class tgsso_mcoBodyRow as CPBodyRowCnt
  Width=800
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_4 as StdTrsField with uid="MIEPHUVMHT",rtseq=20,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo insoluto",;
    HelpContextID = 17113238,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=3, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], BackStyle=IIF(type('i_udisabledbackcolor')$'UL' And type('i_udisabledforecolor')$'UL',0,1)

  add object oNUMPAR_2_5 as StdTrsField with uid="SOIFMQFKHT",rtseq=45,rtrep=.t.,;
    cFormVar="w_NUMPAR",value=space(31),enabled=.f.,;
    ToolTipText = "Numero della partita",;
    HelpContextID = 258239274,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=230, Left=75, Top=0, InputMask=replicate('X',31)

  add object oNUMDOC_2_6 as StdTrsField with uid="WEYMLCIEJI",rtseq=46,rtrep=.t.,;
    cFormVar="w_NUMDOC",value=0,enabled=.f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 227568426,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=308, Top=0

  add object oALFDOC_2_7 as StdTrsField with uid="KWFISWCWTU",rtseq=47,rtrep=.t.,;
    cFormVar="w_ALFDOC",value=space(10),enabled=.f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 227599610,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=430, Top=0, InputMask=replicate('X',10)

  add object oCOTOTIMP_2_8 as StdTrsField with uid="SYRPUKHDNZ",rtseq=48,rtrep=.t.,;
    cFormVar="w_COTOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Importo del mancato pagamento",;
    HelpContextID = 147521142,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=148, Left=519, Top=0, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  func oCOTOTIMP_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CO__TIPO='I')
    endwith
    endif
  endfunc

  add object oTOTIMP_2_10 as StdTrsField with uid="ENUTWHVYXD",rtseq=50,rtrep=.t.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Importo della partita",;
    HelpContextID = 11206858,;
    cTotal = "this.Parent.oContained.w_totale", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=148, Left=519, Top=0, cSayPict=[v_PU(40+VVL)], cGetPict=[v_GU(40+VVL)]

  func oTOTIMP_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CO__TIPO='M')
    endwith
    endif
  endfunc

  add object oCOCODBAN_2_11 as StdTrsField with uid="KEXUDYGPCI",rtseq=51,rtrep=.t.,;
    cFormVar="w_COCODBAN",value=space(15),enabled=.f.,;
    ToolTipText = "Codice banca di presentazione",;
    HelpContextID = 255201676,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=124, Left=671, Top=0, InputMask=replicate('X',15)

  add object oDATSCA_2_16 as StdTrsField with uid="SSCWXIAHEO",rtseq=74,rtrep=.t.,;
    cFormVar="w_DATSCA",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data scadenza della partita",;
    HelpContextID = 4263882,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=74, Left=-2, Top=0
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_4.When()
    return(.t.)
  proc oCPROWORD_2_4.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_4.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_mco','CON_TENZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COSERIAL=CON_TENZ.COSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
