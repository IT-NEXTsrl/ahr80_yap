* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_kvi                                                        *
*              Visualizza contenziosi                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-09                                                      *
* Last revis.: 2010-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_kvi",oParentObject))

* --- Class definition
define class tgsso_kvi as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 741
  Height = 428
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-02-16"
  HelpContextID=219572887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AGENTI_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsso_kvi"
  cComment = "Visualizza contenziosi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(5)
  w_GPCODRAG = space(10)
  w_GPCOMPET = space(4)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_CODCON = space(15)
  w_STATUS = space(2)
  w_DESCON = space(40)
  w_TIPOCON = space(1)
  w_CODVAL = space(3)
  w_TIPCON = space(1)
  w_DATVAL = ctod('  /  /  ')
  w_SERIAL = space(10)
  w_TIPO = space(1)
  w_RIFDIS = space(15)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPICT = 0
  w_OBTEST = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_BOOLEAN = .F.
  w_CATPAG = space(2)
  w_TIPPAG = space(10)
  w_TOTCLI = 0
  w_Zoomcont = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_kviPag1","gsso_kvi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGPCODRAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomcont = this.oPgFrm.Pages(1).oPag.Zoomcont
    DoDefault()
    proc Destroy()
      this.w_Zoomcont = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(5)
      .w_GPCODRAG=space(10)
      .w_GPCOMPET=space(4)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_CODCON=space(15)
      .w_STATUS=space(2)
      .w_DESCON=space(40)
      .w_TIPOCON=space(1)
      .w_CODVAL=space(3)
      .w_TIPCON=space(1)
      .w_DATVAL=ctod("  /  /  ")
      .w_SERIAL=space(10)
      .w_TIPO=space(1)
      .w_RIFDIS=space(15)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_OBTEST=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_BOOLEAN=.f.
      .w_CATPAG=space(2)
      .w_TIPPAG=space(10)
      .w_TOTCLI=0
        .w_CODICE = i_CODAZI
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_GPCOMPET))
          .link_1_3('Full')
        endif
        .w_SCAINI = i_DATSYS-60
        .w_SCAFIN = i_DATSYS
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODCON))
          .link_1_6('Full')
        endif
        .w_STATUS = 'PE'
          .DoRTCalc(8,8,.f.)
        .w_TIPOCON = 'T'
        .w_CODVAL = g_perval
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODVAL))
          .link_1_10('Full')
        endif
        .w_TIPCON = 'C'
          .DoRTCalc(12,12,.f.)
        .w_SERIAL = .w_Zoomcont.getVar('COSERIAL')
        .w_TIPO = IIF(.w_TIPOCON='T',' ',.w_TIPOCON)
          .DoRTCalc(15,16,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OBTEST = I_DATSYS
      .oPgFrm.Page1.oPag.Zoomcont.Calculate()
          .DoRTCalc(19,20,.f.)
        .w_CATPAG = 'TT'
        .w_TIPPAG = IIF(.w_CATPAG='TT','  ',.w_CATPAG)
    endwith
    this.DoRTCalc(23,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
            .w_SERIAL = .w_Zoomcont.getVar('COSERIAL')
            .w_TIPO = IIF(.w_TIPOCON='T',' ',.w_TIPOCON)
        .DoRTCalc(15,16,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
            .w_OBTEST = I_DATSYS
        .oPgFrm.Page1.oPag.Zoomcont.Calculate()
        .DoRTCalc(19,21,.t.)
            .w_TIPPAG = IIF(.w_CATPAG='TT','  ',.w_CATPAG)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoomcont.Calculate()
    endwith
  return

  proc Calculate_BKQLNWJEZV()
    with this
          * --- Azzero totcli
          .w_TOTCLI = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTOTCLI_1_36.visible=!this.oPgFrm.Page1.oPag.oTOTCLI_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoomcont.Event(cEvent)
        if lower(cEvent)==lower("w_CODCON Changed")
          .Calculate_BKQLNWJEZV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsso_kvi
    if CEVENT='Ricerca'
       SELECT (THIS.w_Zoomcont.cCursor )
       SUM PTTOTIMP TO this.w_TOTCLI
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GPCOMPET
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_GPCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODICE);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODICE;
                     ,'ESCODESE',trim(this.w_GPCOMPET))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oGPCOMPET_1_3'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODICE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_GPCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODICE;
                       ,'ESCODESE',this.w_GPCOMPET)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCOMPET = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_GPCOMPET = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_6'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_10'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGPCODRAG_1_2.value==this.w_GPCODRAG)
      this.oPgFrm.Page1.oPag.oGPCODRAG_1_2.value=this.w_GPCODRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCOMPET_1_3.value==this.w_GPCOMPET)
      this.oPgFrm.Page1.oPag.oGPCOMPET_1_3.value=this.w_GPCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_4.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_4.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_5.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_5.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_6.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_6.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATUS_1_7.RadioValue()==this.w_STATUS)
      this.oPgFrm.Page1.oPag.oSTATUS_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_8.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_8.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCON_1_9.RadioValue()==this.w_TIPOCON)
      this.oPgFrm.Page1.oPag.oTIPOCON_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_10.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_10.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_29.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_29.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCATPAG_1_33.RadioValue()==this.w_CATPAG)
      this.oPgFrm.Page1.oPag.oCATPAG_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTCLI_1_36.value==this.w_TOTCLI)
      this.oPgFrm.Page1.oPag.oTOTCLI_1_36.value=this.w_TOTCLI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_SCAINI<=.w_SCAFIN OR EMPTY(.w_SCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_SCAINI<=.w_SCAFIN OR EMPTY(.w_SCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   (empty(.w_CODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVAL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DECTOT = this.w_DECTOT
    return

enddefine

* --- Define pages as container
define class tgsso_kviPag1 as StdContainer
  Width  = 737
  height = 428
  stdWidth  = 737
  stdheight = 428
  resizeXpos=434
  resizeYpos=242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPCODRAG_1_2 as StdField with uid="HRIZXFDDOP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GPCODRAG", cQueryName = "GPCODRAG",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice raggruppamento",;
    HelpContextID = 61468589,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=87, Top=15, InputMask=replicate('X',10)

  add object oGPCOMPET_1_3 as StdField with uid="WJOHFMJYAB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GPCOMPET", cQueryName = "GPCOMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 231084102,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=271, Top=15, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODICE", oKey_2_1="ESCODESE", oKey_2_2="this.w_GPCOMPET"

  func oGPCOMPET_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCOMPET_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCOMPET_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODICE)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oGPCOMPET_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oSCAINI_1_4 as StdField with uid="MHNCBUQKCT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio selezione (vuota=no selezione)",;
    HelpContextID = 188990246,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=86, Top=41

  func oSCAINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SCAINI<=.w_SCAFIN OR EMPTY(.w_SCAFIN))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_5 as StdField with uid="ZRPUEJPAFK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di fine selezione (vuota=no selezione)",;
    HelpContextID = 267436838,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=86, Top=67

  func oSCAFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SCAINI<=.w_SCAFIN OR EMPTY(.w_SCAFIN))
    endwith
    return bRes
  endfunc

  add object oCODCON_1_6 as StdField with uid="LTHKJTPXJU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 5111334,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=271, Top=41, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODCON_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc


  add object oSTATUS_1_7 as StdCombo with uid="LMGPZJZERK",rtseq=7,rtrep=.f.,left=271,top=68,width=142,height=21;
    , ToolTipText = "Status contenzioso";
    , HelpContextID = 96392230;
    , cFormVar="w_STATUS",RowSource=""+"Pendenti,"+"Chiuso con incasso,"+"Chiuso senza incasso,"+"Pratica legale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATUS_1_7.RadioValue()
    return(iif(this.value =1,'PE',;
    iif(this.value =2,'CI',;
    iif(this.value =3,'CS',;
    iif(this.value =4,'PL',;
    space(2))))))
  endfunc
  func oSTATUS_1_7.GetRadio()
    this.Parent.oContained.w_STATUS = this.RadioValue()
    return .t.
  endfunc

  func oSTATUS_1_7.SetRadio()
    this.Parent.oContained.w_STATUS=trim(this.Parent.oContained.w_STATUS)
    this.value = ;
      iif(this.Parent.oContained.w_STATUS=='PE',1,;
      iif(this.Parent.oContained.w_STATUS=='CI',2,;
      iif(this.Parent.oContained.w_STATUS=='CS',3,;
      iif(this.Parent.oContained.w_STATUS=='PL',4,;
      0))))
  endfunc

  add object oDESCON_1_8 as StdField with uid="EMOULPIWDS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 5170230,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=408, Top=41, InputMask=replicate('X',40)


  add object oTIPOCON_1_9 as StdCombo with uid="XOGTMIUKYZ",rtseq=9,rtrep=.f.,left=496,top=68,width=142,height=21;
    , ToolTipText = "Tipologia contenzioso";
    , HelpContextID = 10139958;
    , cFormVar="w_TIPOCON",RowSource=""+"Insoluto,"+"Mancato pagamento,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCON_1_9.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPOCON_1_9.GetRadio()
    this.Parent.oContained.w_TIPOCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCON_1_9.SetRadio()
    this.Parent.oContained.w_TIPOCON=trim(this.Parent.oContained.w_TIPOCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCON=='I',1,;
      iif(this.Parent.oContained.w_TIPOCON=='M',2,;
      iif(this.Parent.oContained.w_TIPOCON=='T',3,;
      0)))
  endfunc

  add object oCODVAL_1_10 as StdField with uid="HBEXBZAGAI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codoce valuta selezionata",;
    HelpContextID = 226557478,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=86, Top=93, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc


  add object Zoomcont as cp_zoombox with uid="NCWSDYHXKN",left=-1, top=118, width=741,height=258,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.f.,cZoomFile="GSSO_KVI",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cTable="CON_TENZ",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 139300378


  add object oBtn_1_27 as StdButton with uid="OSKJUCDDJL",left=683, top=68, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la query";
    , HelpContextID = 140375786;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_28 as StdButton with uid="VRONCUQWAW",left=683, top=380, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226890310;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMVAL_1_29 as StdField with uid="DHXLXJNLQH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 226593062,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=138, Top=93, InputMask=replicate('X',5)


  add object oBtn_1_31 as StdButton with uid="XFXJCJDEUZ",left=5, top=380, width=48,height=45,;
    CpPicture="bmp\origine.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il contenzioso";
    , HelpContextID = 233541146;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        .w_BOOLEAN=OpenGest("A",'GSSO_MCO',"COSERIAL",.w_SERIAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(nvl(.w_SERIAL,'')))
      endwith
    endif
  endfunc


  add object oCATPAG_1_33 as StdCombo with uid="CWOBMSTBNT",rtseq=21,rtrep=.f.,left=271,top=94,width=142,height=21;
    , ToolTipText = "Selezione categoria di pagamento";
    , HelpContextID = 142340134;
    , cFormVar="w_CATPAG",RowSource=""+"Cambiale/tratta,"+"Rimessa diretta,"+"Bonifico,"+"M.AV.,"+"R.I.D.,"+"Ricevuta bancaria,"+"Compensazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATPAG_1_33.RadioValue()
    return(iif(this.value =1,'CA',;
    iif(this.value =2,'RD',;
    iif(this.value =3,'BO',;
    iif(this.value =4,'MA',;
    iif(this.value =5,'RI',;
    iif(this.value =6,'RB',;
    iif(this.value =7,'CC',;
    iif(this.value =8,'TT',;
    space(2))))))))))
  endfunc
  func oCATPAG_1_33.GetRadio()
    this.Parent.oContained.w_CATPAG = this.RadioValue()
    return .t.
  endfunc

  func oCATPAG_1_33.SetRadio()
    this.Parent.oContained.w_CATPAG=trim(this.Parent.oContained.w_CATPAG)
    this.value = ;
      iif(this.Parent.oContained.w_CATPAG=='CA',1,;
      iif(this.Parent.oContained.w_CATPAG=='RD',2,;
      iif(this.Parent.oContained.w_CATPAG=='BO',3,;
      iif(this.Parent.oContained.w_CATPAG=='MA',4,;
      iif(this.Parent.oContained.w_CATPAG=='RI',5,;
      iif(this.Parent.oContained.w_CATPAG=='RB',6,;
      iif(this.Parent.oContained.w_CATPAG=='CC',7,;
      iif(this.Parent.oContained.w_CATPAG=='TT',8,;
      0))))))))
  endfunc

  add object oTOTCLI_1_36 as StdField with uid="LGDBRAQXYZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_TOTCLI", cQueryName = "TOTCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 186580790,;
   bGlobalFont=.t.,;
    Height=21, Width=123, Left=500, Top=383, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTCLI_1_36.mHide()
    with this.Parent.oContained
      return (empty(.w_CODCON))
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="ASIYOSPVBW",Visible=.t., Left=197, Top=41,;
    Alignment=1, Width=70, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FEETTHZIDR",Visible=.t., Left=13, Top=41,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YQNSDUHICE",Visible=.t., Left=12, Top=68,;
    Alignment=1, Width=71, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DPJMVNFTAV",Visible=.t., Left=423, Top=68,;
    Alignment=1, Width=70, Height=18,;
    Caption="Contenz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GKNTSIBIQB",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=78, Height=18,;
    Caption="Raggrupp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="YKZHBRLFFM",Visible=.t., Left=219, Top=68,;
    Alignment=1, Width=48, Height=18,;
    Caption="Status.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SLOAZGHCSR",Visible=.t., Left=214, Top=15,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="LSRMOKTEVM",Visible=.t., Left=13, Top=93,;
    Alignment=1, Width=70, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="SBYNESUCPG",Visible=.t., Left=202, Top=94,;
    Alignment=1, Width=65, Height=18,;
    Caption="Tipo pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="DOSCQFPWOI",Visible=.t., Left=422, Top=383,;
    Alignment=1, Width=76, Height=18,;
    Caption="Totale cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (empty(.w_CODCON))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_kvi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
