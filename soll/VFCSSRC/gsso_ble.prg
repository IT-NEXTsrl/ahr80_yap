* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_ble                                                        *
*              Visualizza archivi                                              *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-25                                                      *
* Last revis.: 2012-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_ble",oParentObject,m.pNOMSUP,m.pDTARIC,m.pNOMFIL,m.pERTIPIMP)
return(i_retval)

define class tgsso_ble as StdBatch
  * --- Local variables
  pNOMSUP = space(20)
  pDTARIC = ctod("  /  /  ")
  pNOMFIL = space(100)
  pERTIPIMP = space(1)
  w_CLASSE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza il flusso di ritorno inerente al log di errori selezionato (da gsrb_MLE) 
    * --- Oggetto che sar� definito come Flussi di ritorno
    do case
      case this.pERTIPIMP="F"
        this.w_CLASSE = gsso_MFR()
        if !(this.w_CLASSE.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_CLASSE.w_FLNOMSUP = this.pNOMSUP
        this.w_CLASSE.w_FLDTARIC = this.pDTARIC
        this.w_CLASSE.w_FLNOMFIL = this.pNOMFIL
        this.w_CLASSE.QueryKeySet("FLNOMSUP="+cp_ToStrODBC(this.pNOMSUP)+ " AND FLDTARIC="+cp_ToStrODBC(this.pDTARIC)+" AND FLNOMFIL="+cp_ToStrODBC(this.pNOMFIL)+"","")     
    endcase
    this.w_CLASSE.LoadRecWarn()     
  endproc


  proc Init(oParentObject,pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP)
    this.pNOMSUP=pNOMSUP
    this.pDTARIC=pDTARIC
    this.pNOMFIL=pNOMFIL
    this.pERTIPIMP=pERTIPIMP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNOMSUP,pDTARIC,pNOMFIL,pERTIPIMP"
endproc
