* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_ser                                                        *
*              Stampa esiti ricevuti                                           *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-11                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_ser",oParentObject))

* --- Class definition
define class tgsso_ser as StdForm
  Top    = 93
  Left   = 36

  * --- Standard Properties
  Width  = 618
  Height = 290
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-23"
  HelpContextID=57251177
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  FLU_RITO_IDX = 0
  COC_MAST_IDX = 0
  CONTI_IDX = 0
  OFF_NOMI_IDX = 0
  FLU_ESRI_IDX = 0
  cPrg = "gsso_ser"
  cComment = "Stampa esiti ricevuti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_TIPOCONF = space(1)
  w_TIPFLU = space(2)
  w_NOMSUPIN = space(20)
  w_NOMSUPFI = space(20)
  w_DATCREIN = ctod('  /  /  ')
  w_DATCREFI = ctod('  /  /  ')
  w_DATSCAIN = ctod('  /  /  ')
  w_DATSCAFI = ctod('  /  /  ')
  w_NUMCOR = space(15)
  w_FLGFILCF = space(1)
  o_FLGFILCF = space(1)
  w_TIPCLFO = space(1)
  w_CODCLFO = space(15)
  w_DATOBSO = ctod('  /  /  ')
  w_BADESCRI = space(35)
  w_DESCLFO = space(40)
  w_DATOBSN = ctod('  /  /  ')
  w_NOFLGBEN = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_serPag1","gsso_ser",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOCONF_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='FLU_RITO'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='FLU_ESRI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOCONF=space(1)
      .w_TIPFLU=space(2)
      .w_NOMSUPIN=space(20)
      .w_NOMSUPFI=space(20)
      .w_DATCREIN=ctod("  /  /  ")
      .w_DATCREFI=ctod("  /  /  ")
      .w_DATSCAIN=ctod("  /  /  ")
      .w_DATSCAFI=ctod("  /  /  ")
      .w_NUMCOR=space(15)
      .w_FLGFILCF=space(1)
      .w_TIPCLFO=space(1)
      .w_CODCLFO=space(15)
      .w_DATOBSO=ctod("  /  /  ")
      .w_BADESCRI=space(35)
      .w_DESCLFO=space(40)
      .w_DATOBSN=ctod("  /  /  ")
      .w_NOFLGBEN=space(1)
        .w_OBTEST = i_datsys
        .w_TIPOCONF = 'T'
        .w_TIPFLU = ''
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_NOMSUPIN))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_NOMSUPFI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,10,.f.)
        if not(empty(.w_NUMCOR))
          .link_1_10('Full')
        endif
        .w_FLGFILCF = 'N'
        .w_TIPCLFO = 'C'
        .w_CODCLFO = ''
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODCLFO))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
    this.DoRTCalc(14,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_FLGFILCF<>.w_FLGFILCF
            .w_TIPCLFO = 'C'
        endif
        if .o_FLGFILCF<>.w_FLGFILCF
            .w_CODCLFO = ''
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(14,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPCLFO_1_12.enabled = this.oPgFrm.Page1.oPag.oTIPCLFO_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCODCLFO_1_13.enabled = this.oPgFrm.Page1.oPag.oCODCLFO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NOMSUPIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_lTable = "FLU_ESRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2], .t., this.FLU_ESRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMSUPIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSRB_MER',True,'FLU_ESRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRNOMSUP like "+cp_ToStrODBC(trim(this.w_NOMSUPIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRNOMSUP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRNOMSUP',trim(this.w_NOMSUPIN))
          select FRNOMSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRNOMSUP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMSUPIN)==trim(_Link_.FRNOMSUP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMSUPIN) and !this.bDontReportError
            deferred_cp_zoom('FLU_ESRI','*','FRNOMSUP',cp_AbsName(oSource.parent,'oNOMSUPIN_1_4'),i_cWhere,'GSRB_MER',"ESITI RICEVUTI",'GSRB3SER.FLU_ESRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                     +" from "+i_cTable+" "+i_lTable+" where FRNOMSUP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRNOMSUP',oSource.xKey(1))
            select FRNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMSUPIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                   +" from "+i_cTable+" "+i_lTable+" where FRNOMSUP="+cp_ToStrODBC(this.w_NOMSUPIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRNOMSUP',this.w_NOMSUPIN)
            select FRNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMSUPIN = NVL(_Link_.FRNOMSUP,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_NOMSUPIN = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_NOMSUPIN <= .w_NOMSUPFI) OR EMPTY(.w_NOMSUPFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il nome del supporto iniziale � maggiore di quello finale")
        endif
        this.w_NOMSUPIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])+'\'+cp_ToStr(_Link_.FRNOMSUP,1)
      cp_ShowWarn(i_cKey,this.FLU_ESRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMSUPIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMSUPFI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FLU_ESRI_IDX,3]
    i_lTable = "FLU_ESRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2], .t., this.FLU_ESRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMSUPFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsrb_mer',True,'FLU_ESRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRNOMSUP like "+cp_ToStrODBC(trim(this.w_NOMSUPFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRNOMSUP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRNOMSUP',trim(this.w_NOMSUPFI))
          select FRNOMSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRNOMSUP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMSUPFI)==trim(_Link_.FRNOMSUP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMSUPFI) and !this.bDontReportError
            deferred_cp_zoom('FLU_ESRI','*','FRNOMSUP',cp_AbsName(oSource.parent,'oNOMSUPFI_1_5'),i_cWhere,'gsrb_mer',"ESITI RICEVUTI",'GSRB3SER.FLU_ESRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                     +" from "+i_cTable+" "+i_lTable+" where FRNOMSUP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRNOMSUP',oSource.xKey(1))
            select FRNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMSUPFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRNOMSUP";
                   +" from "+i_cTable+" "+i_lTable+" where FRNOMSUP="+cp_ToStrODBC(this.w_NOMSUPFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRNOMSUP',this.w_NOMSUPFI)
            select FRNOMSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMSUPFI = NVL(_Link_.FRNOMSUP,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_NOMSUPFI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_NOMSUPIN<= .w_NOMSUPFI) OR EMPTY(.w_NOMSUPIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il nome del supporto finale � minore di quello iniziale")
        endif
        this.w_NOMSUPFI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FLU_ESRI_IDX,2])+'\'+cp_ToStr(_Link_.FRNOMSUP,1)
      cp_ShowWarn(i_cKey,this.FLU_ESRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMSUPFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMCOR
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NUMCOR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NUMCOR))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMCOR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_NUMCOR)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_NUMCOR)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NUMCOR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oNUMCOR_1_10'),i_cWhere,'GSTE_ACB',"CONTI BANCHE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NUMCOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NUMCOR)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOR = NVL(_Link_.BACODBAN,space(15))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOR = space(15)
      endif
      this.w_BADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLFO
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLFO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLFO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLFO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLFO;
                     ,'ANCODICE',trim(this.w_CODCLFO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLFO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLFO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLFO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLFO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLFO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLFO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLFO_1_13'),i_cWhere,'GSAR_BZC',"Elenco conti",'CONTISALD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLFO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice intestatario � obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLFO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLFO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLFO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLFO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLFO;
                       ,'ANCODICE',this.w_CODCLFO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLFO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLFO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLFO = space(15)
      endif
      this.w_DESCLFO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice intestatario � obsoleto")
        endif
        this.w_CODCLFO = space(15)
        this.w_DESCLFO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLFO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOCONF_1_2.RadioValue()==this.w_TIPOCONF)
      this.oPgFrm.Page1.oPag.oTIPOCONF_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPFLU_1_3.RadioValue()==this.w_TIPFLU)
      this.oPgFrm.Page1.oPag.oTIPFLU_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMSUPIN_1_4.value==this.w_NOMSUPIN)
      this.oPgFrm.Page1.oPag.oNOMSUPIN_1_4.value=this.w_NOMSUPIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMSUPFI_1_5.value==this.w_NOMSUPFI)
      this.oPgFrm.Page1.oPag.oNOMSUPFI_1_5.value=this.w_NOMSUPFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREIN_1_6.value==this.w_DATCREIN)
      this.oPgFrm.Page1.oPag.oDATCREIN_1_6.value=this.w_DATCREIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCREFI_1_7.value==this.w_DATCREFI)
      this.oPgFrm.Page1.oPag.oDATCREFI_1_7.value=this.w_DATCREFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCAIN_1_8.value==this.w_DATSCAIN)
      this.oPgFrm.Page1.oPag.oDATSCAIN_1_8.value=this.w_DATSCAIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCAFI_1_9.value==this.w_DATSCAFI)
      this.oPgFrm.Page1.oPag.oDATSCAFI_1_9.value=this.w_DATSCAFI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOR_1_10.value==this.w_NUMCOR)
      this.oPgFrm.Page1.oPag.oNUMCOR_1_10.value=this.w_NUMCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGFILCF_1_11.RadioValue()==this.w_FLGFILCF)
      this.oPgFrm.Page1.oPag.oFLGFILCF_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLFO_1_12.RadioValue()==this.w_TIPCLFO)
      this.oPgFrm.Page1.oPag.oTIPCLFO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLFO_1_13.value==this.w_CODCLFO)
      this.oPgFrm.Page1.oPag.oCODCLFO_1_13.value=this.w_CODCLFO
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_1_25.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_1_25.value=this.w_BADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLFO_1_27.value==this.w_DESCLFO)
      this.oPgFrm.Page1.oPag.oDESCLFO_1_27.value=this.w_DESCLFO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_NOMSUPIN <= .w_NOMSUPFI) OR EMPTY(.w_NOMSUPFI))  and not(empty(.w_NOMSUPIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMSUPIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome del supporto iniziale � maggiore di quello finale")
          case   not((.w_NOMSUPIN<= .w_NOMSUPFI) OR EMPTY(.w_NOMSUPIN))  and not(empty(.w_NOMSUPFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMSUPFI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome del supporto finale � minore di quello iniziale")
          case   not((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCREFI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore di quella iniziale")
          case   not((.w_DATSCAIN<=.w_DATSCAFI) OR EMPTY(.w_DATSCAFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSCAIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not((.w_DATSCAIN<=.w_DATSCAFI) OR EMPTY(.w_DATSCAIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSCAFI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale � minore di quella iniziale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_FLGFILCF='S')  and not(empty(.w_CODCLFO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLFO_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice intestatario � obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLGFILCF = this.w_FLGFILCF
    return

enddefine

* --- Define pages as container
define class tgsso_serPag1 as StdContainer
  Width  = 614
  height = 290
  stdWidth  = 614
  stdheight = 290
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOCONF_1_2 as StdCombo with uid="UVJRMFGIJF",rtseq=2,rtrep=.f.,left=141,top=6,width=101,height=21;
    , ToolTipText = "Stato della ricezione (errati\elaborati\da elaborare\forzati\tutti)";
    , HelpContextID = 266684036;
    , cFormVar="w_TIPOCONF",RowSource=""+"Errati,"+"Elaborati,"+"Da elaborare,"+"Forzati,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCONF_1_2.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    iif(this.value =4,'F',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oTIPOCONF_1_2.GetRadio()
    this.Parent.oContained.w_TIPOCONF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCONF_1_2.SetRadio()
    this.Parent.oContained.w_TIPOCONF=trim(this.Parent.oContained.w_TIPOCONF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCONF=='E',1,;
      iif(this.Parent.oContained.w_TIPOCONF=='C',2,;
      iif(this.Parent.oContained.w_TIPOCONF=='D',3,;
      iif(this.Parent.oContained.w_TIPOCONF=='F',4,;
      iif(this.Parent.oContained.w_TIPOCONF=='T',5,;
      0)))))
  endfunc


  add object oTIPFLU_1_3 as StdCombo with uid="JUQLTEHCPG",value=5,rtseq=3,rtrep=.f.,left=439,top=7,width=164,height=21;
    , ToolTipText = "Tipologia flusso";
    , HelpContextID = 157173450;
    , cFormVar="w_TIPFLU",RowSource=""+"Ri.Ba.,"+"R.I.D.,"+"M.AV.,"+"Sepa Direct Debit,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPFLU_1_3.RadioValue()
    return(iif(this.value =1,'IB',;
    iif(this.value =2,'IR',;
    iif(this.value =3,'IM',;
    iif(this.value =4,'SD',;
    iif(this.value =5,'',;
    space(2)))))))
  endfunc
  func oTIPFLU_1_3.GetRadio()
    this.Parent.oContained.w_TIPFLU = this.RadioValue()
    return .t.
  endfunc

  func oTIPFLU_1_3.SetRadio()
    this.Parent.oContained.w_TIPFLU=trim(this.Parent.oContained.w_TIPFLU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFLU=='IB',1,;
      iif(this.Parent.oContained.w_TIPFLU=='IR',2,;
      iif(this.Parent.oContained.w_TIPFLU=='IM',3,;
      iif(this.Parent.oContained.w_TIPFLU=='SD',4,;
      iif(this.Parent.oContained.w_TIPFLU=='',5,;
      0)))))
  endfunc

  add object oNOMSUPIN_1_4 as StdField with uid="KAPITWNLDO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NOMSUPIN", cQueryName = "NOMSUPIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome del supporto iniziale � maggiore di quello finale",;
    ToolTipText = "Nome del supporto di inizio selezione",;
    HelpContextID = 37654308,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=141, Top=34, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FLU_ESRI", cZoomOnZoom="GSRB_MER", oKey_1_1="FRNOMSUP", oKey_1_2="this.w_NOMSUPIN"

  func oNOMSUPIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMSUPIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMSUPIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLU_ESRI','*','FRNOMSUP',cp_AbsName(this.parent,'oNOMSUPIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSRB_MER',"ESITI RICEVUTI",'GSRB3SER.FLU_ESRI_VZM',this.parent.oContained
  endproc
  proc oNOMSUPIN_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSRB_MER()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRNOMSUP=this.parent.oContained.w_NOMSUPIN
     i_obj.ecpSave()
  endproc

  add object oNOMSUPFI_1_5 as StdField with uid="RJUAURRDNH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NOMSUPFI", cQueryName = "NOMSUPFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome del supporto finale � minore di quello iniziale",;
    ToolTipText = "Nome del supporto di fine selezione",;
    HelpContextID = 37654303,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=439, Top=34, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="FLU_ESRI", cZoomOnZoom="gsrb_mer", oKey_1_1="FRNOMSUP", oKey_1_2="this.w_NOMSUPFI"

  func oNOMSUPFI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMSUPFI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMSUPFI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FLU_ESRI','*','FRNOMSUP',cp_AbsName(this.parent,'oNOMSUPFI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsrb_mer',"ESITI RICEVUTI",'GSRB3SER.FLU_ESRI_VZM',this.parent.oContained
  endproc
  proc oNOMSUPFI_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsrb_mer()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRNOMSUP=this.parent.oContained.w_NOMSUPFI
     i_obj.ecpSave()
  endproc

  add object oDATCREIN_1_6 as StdField with uid="ROYQUKZJZX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATCREIN", cQueryName = "DATCREIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data creazione di inizio selezione",;
    HelpContextID = 117371012,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=141, Top=63

  func oDATCREIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREFI))
    endwith
    return bRes
  endfunc

  add object oDATCREFI_1_7 as StdField with uid="VPRQRKDJWB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATCREFI", cQueryName = "DATCREFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore di quella iniziale",;
    ToolTipText = "Data creazione di fine selezione",;
    HelpContextID = 117371007,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=63

  func oDATCREFI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCREIN<=.w_DATCREFI) OR EMPTY(.w_DATCREIN))
    endwith
    return bRes
  endfunc

  add object oDATSCAIN_1_8 as StdField with uid="LGNWYCRGNR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATSCAIN", cQueryName = "DATSCAIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data scadenza di inizio selezione",;
    HelpContextID = 35582084,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=141, Top=92

  func oDATSCAIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATSCAIN<=.w_DATSCAFI) OR EMPTY(.w_DATSCAFI))
    endwith
    return bRes
  endfunc

  add object oDATSCAFI_1_9 as StdField with uid="PTGPTLRHYL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATSCAFI", cQueryName = "DATSCAFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale � minore di quella iniziale",;
    ToolTipText = "Data scadenza di fine selezione",;
    HelpContextID = 35582079,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=92

  func oDATSCAFI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATSCAIN<=.w_DATSCAFI) OR EMPTY(.w_DATSCAIN))
    endwith
    return bRes
  endfunc

  add object oNUMCOR_1_10 as StdField with uid="BIOIJKYJZM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMCOR", cQueryName = "NUMCOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Banca assuntrice da selezionare",;
    HelpContextID = 204565290,;
   bGlobalFont=.t.,;
    Height=21, Width=147, Left=141, Top=121, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_NUMCOR"

  func oNUMCOR_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMCOR_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMCOR_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oNUMCOR_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"CONTI BANCHE",'',this.parent.oContained
  endproc
  proc oNUMCOR_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NUMCOR
     i_obj.ecpSave()
  endproc

  add object oFLGFILCF_1_11 as StdCheck with uid="LOGBKPBAOF",rtseq=11,rtrep=.f.,left=141, top=150, caption="Filtra su intestatario",;
    ToolTipText = "Se attivo, abilita i filtri sull'intestatario",;
    HelpContextID = 225520540,;
    cFormVar="w_FLGFILCF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGFILCF_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGFILCF_1_11.GetRadio()
    this.Parent.oContained.w_FLGFILCF = this.RadioValue()
    return .t.
  endfunc

  func oFLGFILCF_1_11.SetRadio()
    this.Parent.oContained.w_FLGFILCF=trim(this.Parent.oContained.w_FLGFILCF)
    this.value = ;
      iif(this.Parent.oContained.w_FLGFILCF=='S',1,;
      0)
  endfunc


  add object oTIPCLFO_1_12 as StdCombo with uid="WQSFDQVBUO",rtseq=12,rtrep=.f.,left=439,top=150,width=146,height=21;
    , ToolTipText = "Tipologia di intestatario da selezionare";
    , HelpContextID = 140592842;
    , cFormVar="w_TIPCLFO",RowSource=""+"Cliente,"+"Fornitore,"+"Generico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLFO_1_12.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPCLFO_1_12.GetRadio()
    this.Parent.oContained.w_TIPCLFO = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLFO_1_12.SetRadio()
    this.Parent.oContained.w_TIPCLFO=trim(this.Parent.oContained.w_TIPCLFO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLFO=='C',1,;
      iif(this.Parent.oContained.w_TIPCLFO=='F',2,;
      iif(this.Parent.oContained.w_TIPCLFO=='G',3,;
      0)))
  endfunc

  func oTIPCLFO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILCF='S')
    endwith
   endif
  endfunc

  func oTIPCLFO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLFO)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLFO_1_13 as StdField with uid="ECDYTXRISL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCLFO", cQueryName = "CODCLFO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice intestatario � obsoleto",;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 140640730,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=141, Top=177, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLFO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLFO"

  func oCODCLFO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGFILCF='S')
    endwith
   endif
  endfunc

  func oCODCLFO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLFO_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLFO_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLFO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLFO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLFO_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'CONTISALD.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCLFO_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLFO
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLFO
     i_obj.ecpSave()
  endproc


  add object oObj_1_20 as cp_outputCombo with uid="ZLERXIVPWT",left=141, top=207, width=458,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 120746470


  add object oBtn_1_21 as StdButton with uid="KWFWCRWNHO",left=507, top=233, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 33694742;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="KUWCMEGMLG",left=559, top=233, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 33694742;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBADESCRI_1_25 as StdField with uid="SQMLWNDUTM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 183504801,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=292, Top=120, InputMask=replicate('X',35)

  add object oDESCLFO_1_27 as StdField with uid="FTNCLPUOXT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCLFO", cQueryName = "DESCLFO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 140581834,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=177, InputMask=replicate('X',40)

  add object oStr_1_15 as StdString with uid="PCACAIFDMJ",Visible=.t., Left=9, Top=38,;
    Alignment=1, Width=128, Height=18,;
    Caption="Da nome supporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="OZHAPFJMZN",Visible=.t., Left=9, Top=67,;
    Alignment=1, Width=128, Height=18,;
    Caption="Da data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="RWOUOWBKBH",Visible=.t., Left=326, Top=67,;
    Alignment=1, Width=111, Height=18,;
    Caption="A data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="JLZEGUAYPD",Visible=.t., Left=314, Top=38,;
    Alignment=1, Width=123, Height=18,;
    Caption="A nome supporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PRFZRUDLEF",Visible=.t., Left=9, Top=9,;
    Alignment=1, Width=128, Height=17,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VDNVYCBIMJ",Visible=.t., Left=36, Top=207,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RLCYDXVNRF",Visible=.t., Left=326, Top=9,;
    Alignment=1, Width=111, Height=18,;
    Caption="Tipo flusso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ZWMTFUQYYE",Visible=.t., Left=7, Top=124,;
    Alignment=1, Width=130, Height=18,;
    Caption="Banca assuntrice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XOQDIOAWWU",Visible=.t., Left=34, Top=179,;
    Alignment=1, Width=103, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="PFJDHMYGHH",Visible=.t., Left=337, Top=152,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YDRLPPIEPZ",Visible=.t., Left=9, Top=96,;
    Alignment=1, Width=128, Height=18,;
    Caption="Da data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="VUWGYJKZWP",Visible=.t., Left=326, Top=96,;
    Alignment=1, Width=111, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_ser','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
