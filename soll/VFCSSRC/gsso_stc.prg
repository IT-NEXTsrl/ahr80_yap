* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_stc                                                        *
*              Stampa testi contenzioso                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_3]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-08                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_stc",oParentObject))

* --- Class definition
define class tgsso_stc as StdForm
  Top    = 50
  Left   = 90

  * --- Standard Properties
  Width  = 508
  Height = 191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=74028393
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  TES_CONT_IDX = 0
  cPrg = "gsso_stc"
  cComment = "Stampa testi contenzioso"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_Numero1 = space(6)
  w_Numero2 = space(6)
  w_Livello = 0
  w_Descri1 = space(50)
  w_Descri2 = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_stcPag1","gsso_stc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNumero1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TES_CONT'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsso_stc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_Numero1=space(6)
      .w_Numero2=space(6)
      .w_Livello=0
      .w_Descri1=space(50)
      .w_Descri2=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_Numero1))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_Numero2))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
    this.DoRTCalc(3,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=Numero1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_lTable = "TES_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2], .t., this.TES_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Numero1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSSO_ATC',True,'TES_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCNUMERO like "+cp_ToStrODBC(trim(this.w_Numero1)+"%");

          i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCNUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCNUMERO',trim(this.w_Numero1))
          select TCNUMERO,TCNOMTES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCNUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Numero1)==trim(_Link_.TCNUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Numero1) and !this.bDontReportError
            deferred_cp_zoom('TES_CONT','*','TCNUMERO',cp_AbsName(oSource.parent,'oNumero1_1_2'),i_cWhere,'GSSO_ATC',"Testi contenzioso",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                     +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',oSource.xKey(1))
            select TCNUMERO,TCNOMTES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Numero1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                   +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(this.w_Numero1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',this.w_Numero1)
            select TCNUMERO,TCNOMTES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Numero1 = NVL(_Link_.TCNUMERO,space(6))
      this.w_Descri1 = NVL(_Link_.TCNOMTES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_Numero1 = space(6)
      endif
      this.w_Descri1 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_numero1<=.w_numero2 or (empty(.w_numero2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande del numero finale")
        endif
        this.w_Numero1 = space(6)
        this.w_Descri1 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])+'\'+cp_ToStr(_Link_.TCNUMERO,1)
      cp_ShowWarn(i_cKey,this.TES_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Numero1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Numero2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_lTable = "TES_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2], .t., this.TES_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Numero2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSSO_ATC',True,'TES_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCNUMERO like "+cp_ToStrODBC(trim(this.w_Numero2)+"%");

          i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCNUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCNUMERO',trim(this.w_Numero2))
          select TCNUMERO,TCNOMTES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCNUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_Numero2)==trim(_Link_.TCNUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_Numero2) and !this.bDontReportError
            deferred_cp_zoom('TES_CONT','*','TCNUMERO',cp_AbsName(oSource.parent,'oNumero2_1_3'),i_cWhere,'GSSO_ATC',"Testi contenzioso",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                     +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',oSource.xKey(1))
            select TCNUMERO,TCNOMTES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Numero2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCNUMERO,TCNOMTES";
                   +" from "+i_cTable+" "+i_lTable+" where TCNUMERO="+cp_ToStrODBC(this.w_Numero2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCNUMERO',this.w_Numero2)
            select TCNUMERO,TCNOMTES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Numero2 = NVL(_Link_.TCNUMERO,space(6))
      this.w_Descri2 = NVL(_Link_.TCNOMTES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_Numero2 = space(6)
      endif
      this.w_Descri2 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_numero1<=.w_numero2 or (empty(.w_numero1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande del numero finale")
        endif
        this.w_Numero2 = space(6)
        this.w_Descri2 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])+'\'+cp_ToStr(_Link_.TCNUMERO,1)
      cp_ShowWarn(i_cKey,this.TES_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Numero2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNumero1_1_2.value==this.w_Numero1)
      this.oPgFrm.Page1.oPag.oNumero1_1_2.value=this.w_Numero1
    endif
    if not(this.oPgFrm.Page1.oPag.oNumero2_1_3.value==this.w_Numero2)
      this.oPgFrm.Page1.oPag.oNumero2_1_3.value=this.w_Numero2
    endif
    if not(this.oPgFrm.Page1.oPag.oLivello_1_4.value==this.w_Livello)
      this.oPgFrm.Page1.oPag.oLivello_1_4.value=this.w_Livello
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri1_1_9.value==this.w_Descri1)
      this.oPgFrm.Page1.oPag.oDescri1_1_9.value=this.w_Descri1
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri2_1_10.value==this.w_Descri2)
      this.oPgFrm.Page1.oPag.oDescri2_1_10.value=this.w_Descri2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_numero1<=.w_numero2 or (empty(.w_numero2)))  and not(empty(.w_Numero1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNumero1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande del numero finale")
          case   not(.w_numero1<=.w_numero2 or (empty(.w_numero1)))  and not(empty(.w_Numero2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNumero2_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande del numero finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_stcPag1 as StdContainer
  Width  = 504
  height = 191
  stdWidth  = 504
  stdheight = 191
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNumero1_1_2 as StdField with uid="ODFJLZQIKD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_Numero1", cQueryName = "Numero1",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande del numero finale",;
    ToolTipText = "Testo contenzioso di inizio selezione",;
    HelpContextID = 35828950,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=106, Top=21, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="TES_CONT", cZoomOnZoom="GSSO_ATC", oKey_1_1="TCNUMERO", oKey_1_2="this.w_Numero1"

  func oNumero1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oNumero1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNumero1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TES_CONT','*','TCNUMERO',cp_AbsName(this.parent,'oNumero1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSSO_ATC',"Testi contenzioso",'',this.parent.oContained
  endproc
  proc oNumero1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSSO_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCNUMERO=this.parent.oContained.w_Numero1
     i_obj.ecpSave()
  endproc

  add object oNumero2_1_3 as StdField with uid="VOFGZVECCX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_Numero2", cQueryName = "Numero2",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande del numero finale",;
    ToolTipText = "Testo contenzioso di fine selezione",;
    HelpContextID = 35828950,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=106, Top=49, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="TES_CONT", cZoomOnZoom="GSSO_ATC", oKey_1_1="TCNUMERO", oKey_1_2="this.w_Numero2"

  func oNumero2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oNumero2_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNumero2_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TES_CONT','*','TCNUMERO',cp_AbsName(this.parent,'oNumero2_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSSO_ATC',"Testi contenzioso",'',this.parent.oContained
  endproc
  proc oNumero2_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSSO_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCNUMERO=this.parent.oContained.w_Numero2
     i_obj.ecpSave()
  endproc

  add object oLivello_1_4 as StdField with uid="BJJSJYUYEJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_Livello", cQueryName = "Livello",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello insoluto selezionato",;
    HelpContextID = 247675062,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=106, Top=77, cSayPict='"99"', cGetPict='"99"'


  add object oObj_1_5 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=109, width=389,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 103969254


  add object oBtn_1_6 as StdButton with uid="CSYTPQJIIR",left=396, top=139, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i testi";
    , HelpContextID = 67761190;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="HXMTLKQQNY",left=447, top=139, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 66710970;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDescri1_1_9 as StdField with uid="CMXVWXFSXY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_Descri1", cQueryName = "Descri1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 203490358,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=188, Top=21, InputMask=replicate('X',50)

  add object oDescri2_1_10 as StdField with uid="KTACYTMWNZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_Descri2", cQueryName = "Descri2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 203490358,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=188, Top=49, InputMask=replicate('X',50)

  add object oStr_1_1 as StdString with uid="MKCVIQLXUF",Visible=.t., Left=7, Top=78,;
    Alignment=1, Width=95, Height=18,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XQAYGSRVZL",Visible=.t., Left=7, Top=109,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FPVKUQRUNI",Visible=.t., Left=7, Top=21,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="THNTZMUPOW",Visible=.t., Left=7, Top=49,;
    Alignment=1, Width=95, Height=15,;
    Caption="A testo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_stc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
