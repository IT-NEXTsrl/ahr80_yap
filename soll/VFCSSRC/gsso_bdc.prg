* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bdc                                                        *
*              Aggiorna movimenti insoluto da P.N.                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-22                                                      *
* Last revis.: 2001-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bdc",oParentObject)
return(i_retval)

define class tgsso_bdc as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_APPO = space(10)
  * --- WorkFile variables
  CON_TENZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo Batch viene eseguito dalla procedura GSCG_BCK del Modulo COGE
    * --- E' necessario che avvwenga dentro tale Modulo poiche' non verrebbe gestito dalla sua Analisi
    this.w_SERIAL = this.oParentObject.oParentObject.w_PNSERIAL
    if NOT EMPTY(this.w_SERIAL)
      ah_Msg("Elimino riferimento contenziosi...",.T.)
      * --- Select from CON_TENZ
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COSERIAL  from "+i_cTable+" CON_TENZ ";
            +" where CORIFCON="+cp_ToStrODBC(this.w_SERIAL)+"";
             ,"_Curs_CON_TENZ")
      else
        select COSERIAL from (i_cTable);
         where CORIFCON=this.w_SERIAL;
          into cursor _Curs_CON_TENZ
      endif
      if used('_Curs_CON_TENZ')
        select _Curs_CON_TENZ
        locate for 1=1
        do while not(eof())
        this.w_APPO = _Curs_CON_TENZ.COSERIAL
        * --- Write into CON_TENZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CON_TENZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CORIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'CON_TENZ','CORIFCON');
              +i_ccchkf ;
          +" where ";
              +"COSERIAL = "+cp_ToStrODBC(this.w_APPO);
                 )
        else
          update (i_cTable) set;
              CORIFCON = SPACE(10);
              &i_ccchkf. ;
           where;
              COSERIAL = this.w_APPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_CON_TENZ
          continue
        enddo
        use
      endif
      WAIT CLEAR
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CON_TENZ'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CON_TENZ')
      use in _Curs_CON_TENZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
