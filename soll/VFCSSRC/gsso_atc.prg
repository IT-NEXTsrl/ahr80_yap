* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_atc                                                        *
*              Testi contenzioso                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_13]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-19                                                      *
* Last revis.: 2009-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_atc"))

* --- Class definition
define class tgsso_atc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 537
  Height = 325+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-18"
  HelpContextID=92902761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  TES_CONT_IDX = 0
  LINGUE_IDX = 0
  cFile = "TES_CONT"
  cKeySelect = "TCNUMERO"
  cKeyWhere  = "TCNUMERO=this.w_TCNUMERO"
  cKeyWhereODBC = '"TCNUMERO="+cp_ToStrODBC(this.w_TCNUMERO)';

  cKeyWhereODBCqualified = '"TES_CONT.TCNUMERO="+cp_ToStrODBC(this.w_TCNUMERO)';

  cPrg = "gsso_atc"
  cComment = "Testi contenzioso"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TCNUMERO = space(6)
  w_TCTIPTES = space(1)
  w_TCNUMLIV = 0
  w_TCNOMTES = space(50)
  w_TCOGGTES = space(50)
  w_TCCODLIN = space(3)
  w_TCTESDOC = space(0)
  w_TCPIEDOC = space(0)
  w_DESLIN = space(30)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_TCNUMERO = this.W_TCNUMERO
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TES_CONT','gsso_atc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_atcPag1","gsso_atc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Testi contenzioso")
      .Pages(1).HelpContextID = 43440104
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTCNUMERO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='LINGUE'
    this.cWorkTables[2]='TES_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TES_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TES_CONT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TCNUMERO = NVL(TCNUMERO,space(6))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TES_CONT where TCNUMERO=KeySet.TCNUMERO
    *
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TES_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TES_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TES_CONT '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TCNUMERO',this.w_TCNUMERO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESLIN = space(30)
        .w_TCNUMERO = NVL(TCNUMERO,space(6))
        .op_TCNUMERO = .w_TCNUMERO
        .w_TCTIPTES = NVL(TCTIPTES,space(1))
        .w_TCNUMLIV = NVL(TCNUMLIV,0)
        .w_TCNOMTES = NVL(TCNOMTES,space(50))
        .w_TCOGGTES = NVL(TCOGGTES,space(50))
        .w_TCCODLIN = NVL(TCCODLIN,space(3))
          if link_1_9_joined
            this.w_TCCODLIN = NVL(LUCODICE109,NVL(this.w_TCCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI109,space(30))
          else
          .link_1_9('Load')
          endif
        .w_TCTESDOC = NVL(TCTESDOC,space(0))
        .w_TCPIEDOC = NVL(TCPIEDOC,space(0))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'TES_CONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TCNUMERO = space(6)
      .w_TCTIPTES = space(1)
      .w_TCNUMLIV = 0
      .w_TCNOMTES = space(50)
      .w_TCOGGTES = space(50)
      .w_TCCODLIN = space(3)
      .w_TCTESDOC = space(0)
      .w_TCPIEDOC = space(0)
      .w_DESLIN = space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
          if not(empty(.w_TCCODLIN))
          .link_1_9('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TES_CONT')
    this.DoRTCalc(7,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRTEC","i_codazi,w_TCNUMERO")
      .op_codazi = .w_codazi
      .op_TCNUMERO = .w_TCNUMERO
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTCNUMERO_1_1.enabled = i_bVal
      .Page1.oPag.oTCTIPTES_1_3.enabled_(i_bVal)
      .Page1.oPag.oTCNUMLIV_1_4.enabled = i_bVal
      .Page1.oPag.oTCNOMTES_1_6.enabled = i_bVal
      .Page1.oPag.oTCOGGTES_1_7.enabled = i_bVal
      .Page1.oPag.oTCCODLIN_1_9.enabled = i_bVal
      .Page1.oPag.oTCTESDOC_1_12.enabled = i_bVal
      .Page1.oPag.oTCPIEDOC_1_14.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTCNUMERO_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTCNUMERO_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TES_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCNUMERO,"TCNUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCTIPTES,"TCTIPTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCNUMLIV,"TCNUMLIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCNOMTES,"TCNOMTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCOGGTES,"TCOGGTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCCODLIN,"TCCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCTESDOC,"TCTESDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCPIEDOC,"TCPIEDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    i_lTable = "TES_CONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TES_CONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TES_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TES_CONT_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PRTEC","i_codazi,w_TCNUMERO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TES_CONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TES_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'TES_CONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TCNUMERO,TCTIPTES,TCNUMLIV,TCNOMTES,TCOGGTES"+;
                  ",TCCODLIN,TCTESDOC,TCPIEDOC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TCNUMERO)+;
                  ","+cp_ToStrODBC(this.w_TCTIPTES)+;
                  ","+cp_ToStrODBC(this.w_TCNUMLIV)+;
                  ","+cp_ToStrODBC(this.w_TCNOMTES)+;
                  ","+cp_ToStrODBC(this.w_TCOGGTES)+;
                  ","+cp_ToStrODBCNull(this.w_TCCODLIN)+;
                  ","+cp_ToStrODBC(this.w_TCTESDOC)+;
                  ","+cp_ToStrODBC(this.w_TCPIEDOC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TES_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'TES_CONT')
        cp_CheckDeletedKey(i_cTable,0,'TCNUMERO',this.w_TCNUMERO)
        INSERT INTO (i_cTable);
              (TCNUMERO,TCTIPTES,TCNUMLIV,TCNOMTES,TCOGGTES,TCCODLIN,TCTESDOC,TCPIEDOC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TCNUMERO;
                  ,this.w_TCTIPTES;
                  ,this.w_TCNUMLIV;
                  ,this.w_TCNOMTES;
                  ,this.w_TCOGGTES;
                  ,this.w_TCCODLIN;
                  ,this.w_TCTESDOC;
                  ,this.w_TCPIEDOC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TES_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TES_CONT_IDX,i_nConn)
      *
      * update TES_CONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TES_CONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TCTIPTES="+cp_ToStrODBC(this.w_TCTIPTES)+;
             ",TCNUMLIV="+cp_ToStrODBC(this.w_TCNUMLIV)+;
             ",TCNOMTES="+cp_ToStrODBC(this.w_TCNOMTES)+;
             ",TCOGGTES="+cp_ToStrODBC(this.w_TCOGGTES)+;
             ",TCCODLIN="+cp_ToStrODBCNull(this.w_TCCODLIN)+;
             ",TCTESDOC="+cp_ToStrODBC(this.w_TCTESDOC)+;
             ",TCPIEDOC="+cp_ToStrODBC(this.w_TCPIEDOC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TES_CONT')
        i_cWhere = cp_PKFox(i_cTable  ,'TCNUMERO',this.w_TCNUMERO  )
        UPDATE (i_cTable) SET;
              TCTIPTES=this.w_TCTIPTES;
             ,TCNUMLIV=this.w_TCNUMLIV;
             ,TCNOMTES=this.w_TCNOMTES;
             ,TCOGGTES=this.w_TCOGGTES;
             ,TCCODLIN=this.w_TCCODLIN;
             ,TCTESDOC=this.w_TCTESDOC;
             ,TCPIEDOC=this.w_TCPIEDOC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TES_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TES_CONT_IDX,i_nConn)
      *
      * delete TES_CONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TCNUMERO',this.w_TCNUMERO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TES_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TES_CONT_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"PRTEC","i_codazi,w_TCNUMERO")
          .op_TCNUMERO = .w_TCNUMERO
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TCCODLIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TCCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_TCCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_TCCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TCCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TCCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oTCCODLIN_1_9'),i_cWhere,'GSAR_ALG',"Lingua",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TCCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_TCCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_TCCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TCCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TCCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TCCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.LUCODICE as LUCODICE109"+ ",link_1_9.LUDESCRI as LUDESCRI109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on TES_CONT.TCCODLIN=link_1_9.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and TES_CONT.TCCODLIN=link_1_9.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTCNUMERO_1_1.value==this.w_TCNUMERO)
      this.oPgFrm.Page1.oPag.oTCNUMERO_1_1.value=this.w_TCNUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oTCTIPTES_1_3.RadioValue()==this.w_TCTIPTES)
      this.oPgFrm.Page1.oPag.oTCTIPTES_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTCNUMLIV_1_4.value==this.w_TCNUMLIV)
      this.oPgFrm.Page1.oPag.oTCNUMLIV_1_4.value=this.w_TCNUMLIV
    endif
    if not(this.oPgFrm.Page1.oPag.oTCNOMTES_1_6.value==this.w_TCNOMTES)
      this.oPgFrm.Page1.oPag.oTCNOMTES_1_6.value=this.w_TCNOMTES
    endif
    if not(this.oPgFrm.Page1.oPag.oTCOGGTES_1_7.value==this.w_TCOGGTES)
      this.oPgFrm.Page1.oPag.oTCOGGTES_1_7.value=this.w_TCOGGTES
    endif
    if not(this.oPgFrm.Page1.oPag.oTCCODLIN_1_9.value==this.w_TCCODLIN)
      this.oPgFrm.Page1.oPag.oTCCODLIN_1_9.value=this.w_TCCODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTCTESDOC_1_12.value==this.w_TCTESDOC)
      this.oPgFrm.Page1.oPag.oTCTESDOC_1_12.value=this.w_TCTESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTCPIEDOC_1_14.value==this.w_TCPIEDOC)
      this.oPgFrm.Page1.oPag.oTCPIEDOC_1_14.value=this.w_TCPIEDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIN_1_16.value==this.w_DESLIN)
      this.oPgFrm.Page1.oPag.oDESLIN_1_16.value=this.w_DESLIN
    endif
    cp_SetControlsValueExtFlds(this,'TES_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TCNUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTCNUMERO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TCNUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_atcPag1 as StdContainer
  Width  = 533
  height = 325
  stdWidth  = 533
  stdheight = 325
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTCNUMERO_1_1 as StdField with uid="WXGAAAXSAX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TCNUMERO", cQueryName = "TCNUMERO",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero del testo",;
    HelpContextID = 77632389,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=13, InputMask=replicate('X',6)

  add object oTCTIPTES_1_3 as StdRadio with uid="YNVWYKFANJ",rtseq=2,rtrep=.f.,left=223, top=13, width=173,height=33;
    , cFormVar="w_TCTIPTES", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oTCTIPTES_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Insoluto"
      this.Buttons(1).HelpContextID = 63239049
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Mancato pagamento"
      this.Buttons(2).HelpContextID = 63239049
      this.Buttons(2).Top=15
      this.SetAll("Width",171)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oTCTIPTES_1_3.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oTCTIPTES_1_3.GetRadio()
    this.Parent.oContained.w_TCTIPTES = this.RadioValue()
    return .t.
  endfunc

  func oTCTIPTES_1_3.SetRadio()
    this.Parent.oContained.w_TCTIPTES=trim(this.Parent.oContained.w_TCTIPTES)
    this.value = ;
      iif(this.Parent.oContained.w_TCTIPTES=='I',1,;
      iif(this.Parent.oContained.w_TCTIPTES=='M',2,;
      0))
  endfunc

  add object oTCNUMLIV_1_4 as StdField with uid="WPNFTXNCRU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TCNUMLIV", cQueryName = "TCNUMLIV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero livello del testo",;
    HelpContextID = 73362548,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=490, Top=13, cSayPict='"99"', cGetPict='"99"'

  add object oTCNOMTES_1_6 as StdField with uid="RBFJZPTBNY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TCNOMTES", cQueryName = "TCNOMTES",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Titolo del testo",;
    HelpContextID = 60461961,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=135, Top=52, InputMask=replicate('X',50)

  add object oTCOGGTES_1_7 as StdField with uid="IYPHZELWEQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TCOGGTES", cQueryName = "TCOGGTES",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto del testo",;
    HelpContextID = 53650313,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=135, Top=76, InputMask=replicate('X',50)

  add object oTCCODLIN_1_9 as StdField with uid="DSHITMGRYX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TCCODLIN", cQueryName = "TCCODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua",;
    HelpContextID = 83238012,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=99, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_TCCODLIN"

  func oTCCODLIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTCCODLIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTCCODLIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oTCCODLIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingua",'',this.parent.oContained
  endproc
  proc oTCCODLIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_TCCODLIN
     i_obj.ecpSave()
  endproc

  add object oTCTESDOC_1_12 as StdMemo with uid="PFZWYBLLAM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_TCTESDOC", cQueryName = "TCTESDOC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testata del documento",;
    HelpContextID = 202312839,;
   bGlobalFont=.t.,;
    Height=83, Width=364, Left=135, Top=131

  add object oTCPIEDOC_1_14 as StdMemo with uid="WJIFDXYRDP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TCPIEDOC", cQueryName = "TCPIEDOC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Piede del documento",;
    HelpContextID = 216747143,;
   bGlobalFont=.t.,;
    Height=83, Width=364, Left=135, Top=224

  add object oDESLIN_1_16 as StdField with uid="WVHBUICXLF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 44571594,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=184, Top=99, InputMask=replicate('X',30)


  add object oObj_1_17 as cp_runprogram with uid="KHDQSVKJDR",left=-1, top=337, width=135,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsso_btc",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 85094886

  add object oStr_1_2 as StdString with uid="FRFIUZWASH",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=124, Height=18,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="MIGQQFDWDT",Visible=.t., Left=403, Top=15,;
    Alignment=1, Width=86, Height=18,;
    Caption="Livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="OZLKXMWZLG",Visible=.t., Left=5, Top=56,;
    Alignment=1, Width=124, Height=18,;
    Caption="Nome del testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="DYBLYQXRAC",Visible=.t., Left=5, Top=102,;
    Alignment=1, Width=124, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OWKNXESDVF",Visible=.t., Left=5, Top=80,;
    Alignment=1, Width=124, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YUXPCDFKEI",Visible=.t., Left=5, Top=132,;
    Alignment=1, Width=124, Height=18,;
    Caption="Testata documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="ZJKPGXGYXZ",Visible=.t., Left=5, Top=225,;
    Alignment=1, Width=124, Height=18,;
    Caption="Piede documento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_atc','TES_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TCNUMERO=TES_CONT.TCNUMERO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
