* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_apc                                                        *
*              Parametri contenzioso                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_79]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-28                                                      *
* Last revis.: 2015-11-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_apc"))

* --- Class definition
define class tgsso_apc as StdForm
  Top    = 4
  Left   = 13

  * --- Standard Properties
  Width  = 597
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-23"
  HelpContextID=160011625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  PAR_CONT_IDX = 0
  PAR_CONT_IDX = 0
  AZIENDA_IDX = 0
  MOD_PAGA_IDX = 0
  PAG_AMEN_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cFile = "PAR_CONT"
  cKeySelect = "PCCODAZI"
  cKeyWhere  = "PCCODAZI=this.w_PCCODAZI"
  cKeyWhereODBC = '"PCCODAZI="+cp_ToStrODBC(this.w_PCCODAZI)';

  cKeyWhereODBCqualified = '"PAR_CONT.PCCODAZI="+cp_ToStrODBC(this.w_PCCODAZI)';

  cPrg = "gsso_apc"
  cComment = "Parametri contenzioso"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PCCODAZI = space(5)
  w_COAZI = space(5)
  w_RAGAZI = space(40)
  w_TIPCON = space(1)
  w_PCCAUINS = space(5)
  w_PCCONBAN = space(15)
  w_PCCONTRO = space(15)
  w_PCSPEINS = 0
  w_PCPAGMOR = space(5)
  w_PCVALIMP = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_TIPCON = space(1)
  w_PCDURMOR = 0
  w_PCDESSUP = space(0)
  w_PCINTSOLL = 0
  w_PCMODINV = space(2)
  w_PCSPELET = 0
  w_PCSPEFAX = 0
  w_PCSPERAC = 0
  w_PCSPEMAI = 0
  w_PCSPERAR = 0
  w_PCSPEPEC = 0
  w_PCSPETEL = 0
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DESC1 = space(35)
  w_DESC2 = space(35)
  w_DESC3 = space(30)
  w_CODESC = space(40)
  w_PCADDINT = space(1)
  o_PCADDINT = space(1)
  w_PCPERINT = 0
  w_PCSAINTC = space(1)
  w_PCDESSPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsso_apc
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  proc SetCPToolBar()
          doDefault()
          oCpToolBar.b3.enabled=.f.
          oCpToolBar.b5.enabled=.f.
          oCpToolBar.b9.enabled=.f.
  endproc
  * ---- Disattiva i metodi Load e Delete (posso solo variare)
  proc ecpLoad()
      * ----
  endproc
  proc ecpDelete()
      * ----
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_CONT','gsso_apc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_apcPag1","gsso_apc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Parametri contenzioso")
      .Pages(1).HelpContextID = 91138310
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsso_apc
      * Nascondo le tabs in vecchia configurazione interfaccia
     *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab="T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='PAR_CONT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='MOD_PAGA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='PAR_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_CONT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PCCODAZI = NVL(PCCODAZI,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_CONT where PCCODAZI=KeySet.PCCODAZI
    *
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_CONT '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_TIPCON = 'G'
        .w_DECTOT = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_DESC1 = space(35)
        .w_DESC2 = space(35)
        .w_DESC3 = space(30)
        .w_CODESC = space(40)
        .w_PCCODAZI = NVL(PCCODAZI,space(5))
          if link_1_1_joined
            this.w_PCCODAZI = NVL(AZCODAZI101,NVL(this.w_PCCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COAZI = .w_PCCODAZI
        .w_PCCAUINS = NVL(PCCAUINS,space(5))
          if link_1_5_joined
            this.w_PCCAUINS = NVL(CCCODICE105,NVL(this.w_PCCAUINS,space(5)))
            this.w_DESC1 = NVL(CCDESCRI105,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO105),ctod("  /  /  "))
          else
          .link_1_5('Load')
          endif
        .w_PCCONBAN = NVL(PCCONBAN,space(15))
          .link_1_6('Load')
        .w_PCCONTRO = NVL(PCCONTRO,space(15))
          .link_1_7('Load')
        .w_PCSPEINS = NVL(PCSPEINS,0)
        .w_PCPAGMOR = NVL(PCPAGMOR,space(5))
          if link_1_9_joined
            this.w_PCPAGMOR = NVL(PACODICE109,NVL(this.w_PCPAGMOR,space(5)))
            this.w_DESC3 = NVL(PADESCRI109,space(30))
          else
          .link_1_9('Load')
          endif
        .w_PCVALIMP = NVL(PCVALIMP,space(3))
          if link_1_10_joined
            this.w_PCVALIMP = NVL(VACODVAL110,NVL(this.w_PCVALIMP,space(3)))
            this.w_DECTOT = NVL(VADECTOT110,0)
          else
          .link_1_10('Load')
          endif
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_TIPCON = 'G'
        .w_PCDURMOR = NVL(PCDURMOR,0)
        .w_PCDESSUP = NVL(PCDESSUP,space(0))
        .w_PCINTSOLL = NVL(PCINTSOLL,0)
        .w_PCMODINV = NVL(PCMODINV,space(2))
        .w_PCSPELET = NVL(PCSPELET,0)
        .w_PCSPEFAX = NVL(PCSPEFAX,0)
        .w_PCSPERAC = NVL(PCSPERAC,0)
        .w_PCSPEMAI = NVL(PCSPEMAI,0)
        .w_PCSPERAR = NVL(PCSPERAR,0)
        .w_PCSPEPEC = NVL(PCSPEPEC,0)
        .w_PCSPETEL = NVL(PCSPETEL,0)
        .w_PCADDINT = NVL(PCADDINT,space(1))
        .w_PCPERINT = NVL(PCPERINT,0)
        .w_PCSAINTC = NVL(PCSAINTC,space(1))
        .w_PCDESSPE = NVL(PCDESSPE,space(1))
        cp_LoadRecExtFlds(this,'PAR_CONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsso_apc
    if this.w_PCCODAZI<>i_codazi and not empty(this.w_PCCODAZI)
     this.blankrec()
     this.w_PCCODAZI=""
     ah_ErrorMsg("Manutenzione consentita alla sola azienda corrente (%1)",,'',i_CODAZI)
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PCCODAZI = space(5)
      .w_COAZI = space(5)
      .w_RAGAZI = space(40)
      .w_TIPCON = space(1)
      .w_PCCAUINS = space(5)
      .w_PCCONBAN = space(15)
      .w_PCCONTRO = space(15)
      .w_PCSPEINS = 0
      .w_PCPAGMOR = space(5)
      .w_PCVALIMP = space(3)
      .w_DECTOT = 0
      .w_CALCPIC = 0
      .w_TIPCON = space(1)
      .w_PCDURMOR = 0
      .w_PCDESSUP = space(0)
      .w_PCINTSOLL = 0
      .w_PCMODINV = space(2)
      .w_PCSPELET = 0
      .w_PCSPEFAX = 0
      .w_PCSPERAC = 0
      .w_PCSPEMAI = 0
      .w_PCSPERAR = 0
      .w_PCSPEPEC = 0
      .w_PCSPETEL = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DESC1 = space(35)
      .w_DESC2 = space(35)
      .w_DESC3 = space(30)
      .w_CODESC = space(40)
      .w_PCADDINT = space(1)
      .w_PCPERINT = 0
      .w_PCSAINTC = space(1)
      .w_PCDESSPE = space(1)
      if .cFunction<>"Filter"
        .w_PCCODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PCCODAZI))
          .link_1_1('Full')
          endif
        .w_COAZI = .w_PCCODAZI
          .DoRTCalc(3,3,.f.)
        .w_TIPCON = 'G'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_PCCAUINS))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_PCCONBAN))
          .link_1_6('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_PCCONTRO))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_PCPAGMOR))
          .link_1_9('Full')
          endif
        .w_PCVALIMP = g_PERVAL
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_PCVALIMP))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_TIPCON = 'G'
          .DoRTCalc(14,16,.f.)
        .w_PCMODINV = 'RA'
          .DoRTCalc(18,25,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(27,30,.f.)
        .w_PCADDINT = 'N'
        .w_PCPERINT = IIF(.w_PCADDINT='S', .w_PCPERINT, 0)
        .w_PCSAINTC = 'N'
        .w_PCDESSPE = 'C'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_CONT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsso_apc
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPCCAUINS_1_5.enabled = i_bVal
      .Page1.oPag.oPCCONBAN_1_6.enabled = i_bVal
      .Page1.oPag.oPCCONTRO_1_7.enabled = i_bVal
      .Page1.oPag.oPCSPEINS_1_8.enabled = i_bVal
      .Page1.oPag.oPCPAGMOR_1_9.enabled = i_bVal
      .Page1.oPag.oPCVALIMP_1_10.enabled = i_bVal
      .Page1.oPag.oPCDURMOR_1_14.enabled = i_bVal
      .Page1.oPag.oPCDESSUP_1_15.enabled = i_bVal
      .Page1.oPag.oPCINTSOLL_1_16.enabled = i_bVal
      .Page1.oPag.oPCMODINV_1_17.enabled = i_bVal
      .Page1.oPag.oPCSPELET_1_18.enabled = i_bVal
      .Page1.oPag.oPCSPEFAX_1_19.enabled = i_bVal
      .Page1.oPag.oPCSPERAC_1_20.enabled = i_bVal
      .Page1.oPag.oPCSPEMAI_1_21.enabled = i_bVal
      .Page1.oPag.oPCSPERAR_1_22.enabled = i_bVal
      .Page1.oPag.oPCSPEPEC_1_23.enabled = i_bVal
      .Page1.oPag.oPCSPETEL_1_24.enabled = i_bVal
      .Page1.oPag.oPCADDINT_1_52.enabled = i_bVal
      .Page1.oPag.oPCPERINT_1_57.enabled = i_bVal
      .Page1.oPag.oPCSAINTC_1_58.enabled = i_bVal
      .Page1.oPag.oPCDESSPE_1_60.enabled = i_bVal
      .Page1.oPag.oBtn_1_25.enabled = .Page1.oPag.oBtn_1_25.mCond()
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'PAR_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCODAZI,"PCCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCAUINS,"PCCAUINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONBAN,"PCCONBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCONTRO,"PCCONTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPEINS,"PCSPEINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPAGMOR,"PCPAGMOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCVALIMP,"PCVALIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCDURMOR,"PCDURMOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCDESSUP,"PCDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCINTSOLL,"PCINTSOLL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCMODINV,"PCMODINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPELET,"PCSPELET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPEFAX,"PCSPEFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPERAC,"PCSPERAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPEMAI,"PCSPEMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPERAR,"PCSPERAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPEPEC,"PCSPEPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSPETEL,"PCSPETEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCADDINT,"PCADDINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERINT,"PCPERINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCSAINTC,"PCSAINTC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCDESSPE,"PCDESSPE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    i_lTable = "PAR_CONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_CONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_CONT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_CONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_CONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PCCODAZI,PCCAUINS,PCCONBAN,PCCONTRO,PCSPEINS"+;
                  ",PCPAGMOR,PCVALIMP,PCDURMOR,PCDESSUP,PCINTSOLL"+;
                  ",PCMODINV,PCSPELET,PCSPEFAX,PCSPERAC,PCSPEMAI"+;
                  ",PCSPERAR,PCSPEPEC,PCSPETEL,PCADDINT,PCPERINT"+;
                  ",PCSAINTC,PCDESSPE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PCCODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_PCCAUINS)+;
                  ","+cp_ToStrODBCNull(this.w_PCCONBAN)+;
                  ","+cp_ToStrODBCNull(this.w_PCCONTRO)+;
                  ","+cp_ToStrODBC(this.w_PCSPEINS)+;
                  ","+cp_ToStrODBCNull(this.w_PCPAGMOR)+;
                  ","+cp_ToStrODBCNull(this.w_PCVALIMP)+;
                  ","+cp_ToStrODBC(this.w_PCDURMOR)+;
                  ","+cp_ToStrODBC(this.w_PCDESSUP)+;
                  ","+cp_ToStrODBC(this.w_PCINTSOLL)+;
                  ","+cp_ToStrODBC(this.w_PCMODINV)+;
                  ","+cp_ToStrODBC(this.w_PCSPELET)+;
                  ","+cp_ToStrODBC(this.w_PCSPEFAX)+;
                  ","+cp_ToStrODBC(this.w_PCSPERAC)+;
                  ","+cp_ToStrODBC(this.w_PCSPEMAI)+;
                  ","+cp_ToStrODBC(this.w_PCSPERAR)+;
                  ","+cp_ToStrODBC(this.w_PCSPEPEC)+;
                  ","+cp_ToStrODBC(this.w_PCSPETEL)+;
                  ","+cp_ToStrODBC(this.w_PCADDINT)+;
                  ","+cp_ToStrODBC(this.w_PCPERINT)+;
                  ","+cp_ToStrODBC(this.w_PCSAINTC)+;
                  ","+cp_ToStrODBC(this.w_PCDESSPE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_CONT')
        cp_CheckDeletedKey(i_cTable,0,'PCCODAZI',this.w_PCCODAZI)
        INSERT INTO (i_cTable);
              (PCCODAZI,PCCAUINS,PCCONBAN,PCCONTRO,PCSPEINS,PCPAGMOR,PCVALIMP,PCDURMOR,PCDESSUP,PCINTSOLL,PCMODINV,PCSPELET,PCSPEFAX,PCSPERAC,PCSPEMAI,PCSPERAR,PCSPEPEC,PCSPETEL,PCADDINT,PCPERINT,PCSAINTC,PCDESSPE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PCCODAZI;
                  ,this.w_PCCAUINS;
                  ,this.w_PCCONBAN;
                  ,this.w_PCCONTRO;
                  ,this.w_PCSPEINS;
                  ,this.w_PCPAGMOR;
                  ,this.w_PCVALIMP;
                  ,this.w_PCDURMOR;
                  ,this.w_PCDESSUP;
                  ,this.w_PCINTSOLL;
                  ,this.w_PCMODINV;
                  ,this.w_PCSPELET;
                  ,this.w_PCSPEFAX;
                  ,this.w_PCSPERAC;
                  ,this.w_PCSPEMAI;
                  ,this.w_PCSPERAR;
                  ,this.w_PCSPEPEC;
                  ,this.w_PCSPETEL;
                  ,this.w_PCADDINT;
                  ,this.w_PCPERINT;
                  ,this.w_PCSAINTC;
                  ,this.w_PCDESSPE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_CONT_IDX,i_nConn)
      *
      * update PAR_CONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_CONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PCCAUINS="+cp_ToStrODBCNull(this.w_PCCAUINS)+;
             ",PCCONBAN="+cp_ToStrODBCNull(this.w_PCCONBAN)+;
             ",PCCONTRO="+cp_ToStrODBCNull(this.w_PCCONTRO)+;
             ",PCSPEINS="+cp_ToStrODBC(this.w_PCSPEINS)+;
             ",PCPAGMOR="+cp_ToStrODBCNull(this.w_PCPAGMOR)+;
             ",PCVALIMP="+cp_ToStrODBCNull(this.w_PCVALIMP)+;
             ",PCDURMOR="+cp_ToStrODBC(this.w_PCDURMOR)+;
             ",PCDESSUP="+cp_ToStrODBC(this.w_PCDESSUP)+;
             ",PCINTSOLL="+cp_ToStrODBC(this.w_PCINTSOLL)+;
             ",PCMODINV="+cp_ToStrODBC(this.w_PCMODINV)+;
             ",PCSPELET="+cp_ToStrODBC(this.w_PCSPELET)+;
             ",PCSPEFAX="+cp_ToStrODBC(this.w_PCSPEFAX)+;
             ",PCSPERAC="+cp_ToStrODBC(this.w_PCSPERAC)+;
             ",PCSPEMAI="+cp_ToStrODBC(this.w_PCSPEMAI)+;
             ",PCSPERAR="+cp_ToStrODBC(this.w_PCSPERAR)+;
             ",PCSPEPEC="+cp_ToStrODBC(this.w_PCSPEPEC)+;
             ",PCSPETEL="+cp_ToStrODBC(this.w_PCSPETEL)+;
             ",PCADDINT="+cp_ToStrODBC(this.w_PCADDINT)+;
             ",PCPERINT="+cp_ToStrODBC(this.w_PCPERINT)+;
             ",PCSAINTC="+cp_ToStrODBC(this.w_PCSAINTC)+;
             ",PCDESSPE="+cp_ToStrODBC(this.w_PCDESSPE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_CONT')
        i_cWhere = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
        UPDATE (i_cTable) SET;
              PCCAUINS=this.w_PCCAUINS;
             ,PCCONBAN=this.w_PCCONBAN;
             ,PCCONTRO=this.w_PCCONTRO;
             ,PCSPEINS=this.w_PCSPEINS;
             ,PCPAGMOR=this.w_PCPAGMOR;
             ,PCVALIMP=this.w_PCVALIMP;
             ,PCDURMOR=this.w_PCDURMOR;
             ,PCDESSUP=this.w_PCDESSUP;
             ,PCINTSOLL=this.w_PCINTSOLL;
             ,PCMODINV=this.w_PCMODINV;
             ,PCSPELET=this.w_PCSPELET;
             ,PCSPEFAX=this.w_PCSPEFAX;
             ,PCSPERAC=this.w_PCSPERAC;
             ,PCSPEMAI=this.w_PCSPEMAI;
             ,PCSPERAR=this.w_PCSPERAR;
             ,PCSPEPEC=this.w_PCSPEPEC;
             ,PCSPETEL=this.w_PCSPETEL;
             ,PCADDINT=this.w_PCADDINT;
             ,PCPERINT=this.w_PCPERINT;
             ,PCSAINTC=this.w_PCSAINTC;
             ,PCDESSPE=this.w_PCDESSPE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_CONT_IDX,i_nConn)
      *
      * delete PAR_CONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PCCODAZI',this.w_PCCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_CONT_IDX,2])
    if i_bUpd
      with this
            .w_PCCODAZI = I_CODAZI
          .link_1_1('Full')
            .w_COAZI = .w_PCCODAZI
        .DoRTCalc(3,11,.t.)
            .w_CALCPIC = DEFPIC(.w_DECTOT)
            .w_TIPCON = 'G'
        .DoRTCalc(14,31,.t.)
        if .o_PCADDINT<>.w_PCADDINT
            .w_PCPERINT = IIF(.w_PCADDINT='S', .w_PCPERINT, 0)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPCPERINT_1_57.enabled = this.oPgFrm.Page1.oPag.oPCPERINT_1_57.mCond()
    this.oPgFrm.Page1.oPag.oPCSAINTC_1_58.enabled = this.oPgFrm.Page1.oPag.oPCSAINTC_1_58.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PCCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PCCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on PAR_CONT.PCCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and PAR_CONT.PCCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCCAUINS
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCAUINS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PCCAUINS)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PCCAUINS))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCAUINS)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCAUINS) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPCCAUINS_1_5'),i_cWhere,'',"",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCAUINS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PCCAUINS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PCCAUINS)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCAUINS = NVL(_Link_.CCCODICE,space(5))
      this.w_DESC1 = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PCCAUINS = space(5)
      endif
      this.w_DESC1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
        endif
        this.w_PCCAUINS = space(5)
        this.w_DESC1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCAUINS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.CCCODICE as CCCODICE105"+ ",link_1_5.CCDESCRI as CCDESCRI105"+ ",link_1_5.CCDTOBSO as CCDTOBSO105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PAR_CONT.PCCAUINS=link_1_5.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PAR_CONT.PCCAUINS=link_1_5.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCCONBAN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PCCONBAN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PCCONBAN))
          select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCONBAN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCONBAN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPCCONBAN_1_6'),i_cWhere,'GSAR_BZC',"Conti contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente od obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PCCONBAN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PCCONBAN)
            select ANTIPCON,ANCODICE,ANDTOBSO,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCONBAN = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_DESC2 = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PCCONBAN = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESC2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente od obsoleto")
        endif
        this.w_PCCONBAN = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESC2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCCONTRO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PCCONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PCCONTRO))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPCCONTRO_1_7'),i_cWhere,'GSAR_BZC',"Conti contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PCCONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PCCONTRO)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESC = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PCCONTRO = space(15)
      endif
      this.w_CODESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCPAGMOR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCPAGMOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PCPAGMOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PCPAGMOR))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCPAGMOR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCPAGMOR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPCPAGMOR_1_9'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCPAGMOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PCPAGMOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PCPAGMOR)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCPAGMOR = NVL(_Link_.PACODICE,space(5))
      this.w_DESC3 = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PCPAGMOR = space(5)
      endif
      this.w_DESC3 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCPAGMOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.PACODICE as PACODICE109"+ ","+cp_TransLinkFldName('link_1_9.PADESCRI')+" as PADESCRI109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on PAR_CONT.PCPAGMOR=link_1_9.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and PAR_CONT.PCPAGMOR=link_1_9.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCVALIMP
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCVALIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PCVALIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PCVALIMP))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCVALIMP)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCVALIMP) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPCVALIMP_1_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCVALIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PCVALIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PCVALIMP)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCVALIMP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PCVALIMP = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCVALIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.VACODVAL as VACODVAL110"+ ",link_1_10.VADECTOT as VADECTOT110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PAR_CONT.PCVALIMP=link_1_10.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PAR_CONT.PCVALIMP=link_1_10.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOAZI_1_2.value==this.w_COAZI)
      this.oPgFrm.Page1.oPag.oCOAZI_1_2.value=this.w_COAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_3.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_3.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oPCCAUINS_1_5.value==this.w_PCCAUINS)
      this.oPgFrm.Page1.oPag.oPCCAUINS_1_5.value=this.w_PCCAUINS
    endif
    if not(this.oPgFrm.Page1.oPag.oPCCONBAN_1_6.value==this.w_PCCONBAN)
      this.oPgFrm.Page1.oPag.oPCCONBAN_1_6.value=this.w_PCCONBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oPCCONTRO_1_7.value==this.w_PCCONTRO)
      this.oPgFrm.Page1.oPag.oPCCONTRO_1_7.value=this.w_PCCONTRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPEINS_1_8.value==this.w_PCSPEINS)
      this.oPgFrm.Page1.oPag.oPCSPEINS_1_8.value=this.w_PCSPEINS
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPAGMOR_1_9.value==this.w_PCPAGMOR)
      this.oPgFrm.Page1.oPag.oPCPAGMOR_1_9.value=this.w_PCPAGMOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPCVALIMP_1_10.RadioValue()==this.w_PCVALIMP)
      this.oPgFrm.Page1.oPag.oPCVALIMP_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDURMOR_1_14.value==this.w_PCDURMOR)
      this.oPgFrm.Page1.oPag.oPCDURMOR_1_14.value=this.w_PCDURMOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDESSUP_1_15.value==this.w_PCDESSUP)
      this.oPgFrm.Page1.oPag.oPCDESSUP_1_15.value=this.w_PCDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oPCINTSOLL_1_16.value==this.w_PCINTSOLL)
      this.oPgFrm.Page1.oPag.oPCINTSOLL_1_16.value=this.w_PCINTSOLL
    endif
    if not(this.oPgFrm.Page1.oPag.oPCMODINV_1_17.RadioValue()==this.w_PCMODINV)
      this.oPgFrm.Page1.oPag.oPCMODINV_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPELET_1_18.value==this.w_PCSPELET)
      this.oPgFrm.Page1.oPag.oPCSPELET_1_18.value=this.w_PCSPELET
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPEFAX_1_19.value==this.w_PCSPEFAX)
      this.oPgFrm.Page1.oPag.oPCSPEFAX_1_19.value=this.w_PCSPEFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPERAC_1_20.value==this.w_PCSPERAC)
      this.oPgFrm.Page1.oPag.oPCSPERAC_1_20.value=this.w_PCSPERAC
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPEMAI_1_21.value==this.w_PCSPEMAI)
      this.oPgFrm.Page1.oPag.oPCSPEMAI_1_21.value=this.w_PCSPEMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPERAR_1_22.value==this.w_PCSPERAR)
      this.oPgFrm.Page1.oPag.oPCSPERAR_1_22.value=this.w_PCSPERAR
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPEPEC_1_23.value==this.w_PCSPEPEC)
      this.oPgFrm.Page1.oPag.oPCSPEPEC_1_23.value=this.w_PCSPEPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSPETEL_1_24.value==this.w_PCSPETEL)
      this.oPgFrm.Page1.oPag.oPCSPETEL_1_24.value=this.w_PCSPETEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC1_1_35.value==this.w_DESC1)
      this.oPgFrm.Page1.oPag.oDESC1_1_35.value=this.w_DESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC2_1_36.value==this.w_DESC2)
      this.oPgFrm.Page1.oPag.oDESC2_1_36.value=this.w_DESC2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC3_1_37.value==this.w_DESC3)
      this.oPgFrm.Page1.oPag.oDESC3_1_37.value=this.w_DESC3
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESC_1_47.value==this.w_CODESC)
      this.oPgFrm.Page1.oPag.oCODESC_1_47.value=this.w_CODESC
    endif
    if not(this.oPgFrm.Page1.oPag.oPCADDINT_1_52.RadioValue()==this.w_PCADDINT)
      this.oPgFrm.Page1.oPag.oPCADDINT_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPERINT_1_57.value==this.w_PCPERINT)
      this.oPgFrm.Page1.oPag.oPCPERINT_1_57.value=this.w_PCPERINT
    endif
    if not(this.oPgFrm.Page1.oPag.oPCSAINTC_1_58.RadioValue()==this.w_PCSAINTC)
      this.oPgFrm.Page1.oPag.oPCSAINTC_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCDESSPE_1_60.RadioValue()==this.w_PCDESSPE)
      this.oPgFrm.Page1.oPag.oPCDESSPE_1_60.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_PCCAUINS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCCAUINS_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente, obsoleta o di tipo IVA")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_PCCONBAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCCONBAN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente od obsoleto")
          case   (empty(.w_PCCONTRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCCONTRO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PCCONTRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PCADDINT<>'S' OR .w_PCPERINT<>0)  and (.w_PCADDINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCPERINT_1_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PCADDINT = this.w_PCADDINT
    return

enddefine

* --- Define pages as container
define class tgsso_apcPag1 as StdContainer
  Width  = 593
  height = 532
  stdWidth  = 593
  stdheight = 532
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOAZI_1_2 as StdField with uid="DWXKLHFQDX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COAZI", cQueryName = "COAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda",;
    HelpContextID = 77279706,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=203, Top=13, InputMask=replicate('X',5)

  add object oRAGAZI_1_3 as StdField with uid="PNXYUNMGRK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 178511594,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=270, Top=13, InputMask=replicate('X',40)

  add object oPCCAUINS_1_5 as StdField with uid="MCQNADAIFC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PCCAUINS", cQueryName = "PCCAUINS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente, obsoleta o di tipo IVA",;
    ToolTipText = "Causale insoluto",;
    HelpContextID = 183770295,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=203, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_PCCAUINS"

  func oPCCAUINS_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCAUINS_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCCAUINS_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPCCAUINS_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc

  add object oPCCONBAN_1_6 as StdField with uid="VUQFGLIBFD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PCCONBAN", cQueryName = "PCCONBAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente od obsoleto",;
    ToolTipText = "Conto spese bancarie",;
    HelpContextID = 39197884,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=203, Top=63, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PCCONBAN"

  func oPCCONBAN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCONBAN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCCONBAN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPCCONBAN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contabili",'',this.parent.oContained
  endproc
  proc oPCCONBAN_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PCCONBAN
     i_obj.ecpSave()
  endproc

  add object oPCCONTRO_1_7 as StdField with uid="PFGJHRGOUS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PCCONTRO", cQueryName = "PCCONTRO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita riapertura scadenze",;
    HelpContextID = 262792005,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=203, Top=88, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PCCONTRO"

  func oPCCONTRO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCONTRO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCCONTRO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPCCONTRO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contabili",'',this.parent.oContained
  endproc
  proc oPCCONTRO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PCCONTRO
     i_obj.ecpSave()
  endproc

  add object oPCSPEINS_1_8 as StdField with uid="UEESGRDVZS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PCSPEINS", cQueryName = "PCSPEINS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese insoluto",;
    HelpContextID = 199498935,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=203, Top=140, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCPAGMOR_1_9 as StdField with uid="IWCLVFDKSA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PCPAGMOR", cQueryName = "PCPAGMOR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo pagamento moratoria",;
    HelpContextID = 131288248,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=203, Top=165, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PCPAGMOR"

  func oPCPAGMOR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCPAGMOR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCPAGMOR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPCPAGMOR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPCPAGMOR_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PCPAGMOR
     i_obj.ecpSave()
  endproc


  add object oPCVALIMP_1_10 as StdCombo with uid="YEGBFYUDOC",rtseq=10,rtrep=.f.,left=203,top=115,width=113,height=21;
    , ToolTipText = "Valuta di riferimento per gli importi dei parametri contenzioso";
    , HelpContextID = 193129658;
    , cFormVar="w_PCVALIMP",RowSource=""+"Valuta nazionale,"+"Valuta di conto", bObbl = .f. , nPag = 1;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oPCVALIMP_1_10.RadioValue()
    return(iif(this.value =1,ALLTR(g_CODLIR),;
    iif(this.value =2,ALLTR(g_PERVAL),;
    space(3))))
  endfunc
  func oPCVALIMP_1_10.GetRadio()
    this.Parent.oContained.w_PCVALIMP = this.RadioValue()
    return .t.
  endfunc

  func oPCVALIMP_1_10.SetRadio()
    this.Parent.oContained.w_PCVALIMP=trim(this.Parent.oContained.w_PCVALIMP)
    this.value = ;
      iif(this.Parent.oContained.w_PCVALIMP==ALLTR(g_CODLIR),1,;
      iif(this.Parent.oContained.w_PCVALIMP==ALLTR(g_PERVAL),2,;
      0))
  endfunc

  func oPCVALIMP_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCVALIMP_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oPCDURMOR_1_14 as StdField with uid="IWMCQNNPUL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PCDURMOR", cQueryName = "PCDURMOR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata moratoria",;
    HelpContextID = 118492344,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=203, Top=190, cSayPict="'999'", cGetPict="'999'"

  add object oPCDESSUP_1_15 as StdMemo with uid="DATWISTPWA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PCDESSUP", cQueryName = "PCDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 250606406,;
   bGlobalFont=.t.,;
    Height=57, Width=387, Left=203, Top=216

  add object oPCINTSOLL_1_16 as StdField with uid="KIDMQATEUW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PCINTSOLL", cQueryName = "PCINTSOLL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo solleciti da inserire",;
    HelpContextID = 16168958,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=454, Top=190, cSayPict="'999'", cGetPict="'999'"


  add object oPCMODINV_1_17 as StdCombo with uid="VSYPWEOABB",rtseq=17,rtrep=.f.,left=203,top=299,width=162,height=21;
    , ToolTipText = "Modalit� di inoltro solleciti";
    , HelpContextID = 200637620;
    , cFormVar="w_PCMODINV",RowSource=""+"Lettera,"+"Raccomandata,"+"Raccomandata A.R.,"+"FAX,"+"E-mail,"+"Telefono,"+"PEC", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCMODINV_1_17.RadioValue()
    return(iif(this.value =1,'LE',;
    iif(this.value =2,'RA',;
    iif(this.value =3,'AR',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'EM',;
    iif(this.value =6,'TE',;
    iif(this.value =7,'PE',;
    space(2)))))))))
  endfunc
  func oPCMODINV_1_17.GetRadio()
    this.Parent.oContained.w_PCMODINV = this.RadioValue()
    return .t.
  endfunc

  func oPCMODINV_1_17.SetRadio()
    this.Parent.oContained.w_PCMODINV=trim(this.Parent.oContained.w_PCMODINV)
    this.value = ;
      iif(this.Parent.oContained.w_PCMODINV=='LE',1,;
      iif(this.Parent.oContained.w_PCMODINV=='RA',2,;
      iif(this.Parent.oContained.w_PCMODINV=='AR',3,;
      iif(this.Parent.oContained.w_PCMODINV=='FA',4,;
      iif(this.Parent.oContained.w_PCMODINV=='EM',5,;
      iif(this.Parent.oContained.w_PCMODINV=='TE',6,;
      iif(this.Parent.oContained.w_PCMODINV=='PE',7,;
      0)))))))
  endfunc

  add object oPCSPELET_1_18 as StdField with uid="JTMSNMEBUB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PCSPELET", cQueryName = "PCSPELET",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro lettera",;
    HelpContextID = 119268170,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=203, Top=325, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPEFAX_1_19 as StdField with uid="TJKJUUZEMK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PCSPEFAX", cQueryName = "PCSPEFAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro FAX",;
    HelpContextID = 18604878,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=448, Top=325, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPERAC_1_20 as StdField with uid="CPVZNIUDMQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PCSPERAC", cQueryName = "PCSPERAC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro raccomandata",;
    HelpContextID = 219931449,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=203, Top=352, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPEMAI_1_21 as StdField with uid="MIGCKWXVPM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PCSPEMAI", cQueryName = "PCSPEMAI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro E-mail",;
    HelpContextID = 136045375,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=448, Top=352, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPERAR_1_22 as StdField with uid="OSTFGXSVKE",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PCSPERAR", cQueryName = "PCSPERAR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro raccomandata A.R.",;
    HelpContextID = 219931464,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=203, Top=379, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPEPEC_1_23 as StdField with uid="KZPFIRXQOD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PCSPEPEC", cQueryName = "PCSPEPEC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro PEC",;
    HelpContextID = 186377017,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=448, Top=379, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPCSPETEL_1_24 as StdField with uid="VFZIFBDWMN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PCSPETEL", cQueryName = "PCSPETEL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Spese di inoltro telefono",;
    HelpContextID = 253485890,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=203, Top=406, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oBtn_1_25 as StdButton with uid="JONTDSFYDU",left=487, top=483, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 159982874;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="MBDDJNOSCT",left=539, top=483, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 152694202;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESC1_1_35 as StdField with uid="IHJASYASFK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESC1", cQueryName = "DESC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103881674,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=270, Top=38, InputMask=replicate('X',35)

  add object oDESC2_1_36 as StdField with uid="DOSKLSEMDQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESC2", cQueryName = "DESC2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 102833098,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=321, Top=63, InputMask=replicate('X',35)

  add object oDESC3_1_37 as StdField with uid="KZHZDGUWOL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESC3", cQueryName = "DESC3",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione pagamento",;
    HelpContextID = 101784522,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=270, Top=165, InputMask=replicate('X',30)

  add object oCODESC_1_47 as StdField with uid="UOGXCXUGAB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODESC", cQueryName = "CODESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contropartita",;
    HelpContextID = 17826266,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=321, Top=88, InputMask=replicate('X',40)


  add object oPCADDINT_1_52 as StdCombo with uid="FAISUWSCPQ",rtseq=31,rtrep=.f.,left=203,top=455,width=98,height=22;
    , ToolTipText = "Determina l'origine del saggio di interesse da applicare negli esiti : tabella saggio interessi di mora / saggio forzato";
    , HelpContextID = 201407670;
    , cFormVar="w_PCADDINT",RowSource=""+"Non applicati,"+"Saggio di mora,"+"Saggio forzato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCADDINT_1_52.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oPCADDINT_1_52.GetRadio()
    this.Parent.oContained.w_PCADDINT = this.RadioValue()
    return .t.
  endfunc

  func oPCADDINT_1_52.SetRadio()
    this.Parent.oContained.w_PCADDINT=trim(this.Parent.oContained.w_PCADDINT)
    this.value = ;
      iif(this.Parent.oContained.w_PCADDINT=='N',1,;
      iif(this.Parent.oContained.w_PCADDINT=='M',2,;
      iif(this.Parent.oContained.w_PCADDINT=='S',3,;
      0)))
  endfunc

  add object oPCPERINT_1_57 as StdField with uid="BHUDLKHRFV",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PCPERINT", cQueryName = "PCPERINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale interessi",;
    HelpContextID = 186600630,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=323, Top=455, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPCPERINT_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCADDINT='S')
    endwith
   endif
  endfunc

  func oPCPERINT_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PCADDINT<>'S' OR .w_PCPERINT<>0)
    endwith
    return bRes
  endfunc

  add object oPCSAINTC_1_58 as StdCheck with uid="MNMZBKVMMO",rtseq=33,rtrep=.f.,left=203, top=481, caption="Saggio da anagrafica cliente",;
    ToolTipText = "Check editabile se il tasso da applicare � forzato. Se attivo nella stampa solleciti viene applicato il tasso di mora definito in anagrafica cliente in sostituzione del saggio forzato",;
    HelpContextID = 156033849,;
    cFormVar="w_PCSAINTC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPCSAINTC_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCSAINTC_1_58.GetRadio()
    this.Parent.oContained.w_PCSAINTC = this.RadioValue()
    return .t.
  endfunc

  func oPCSAINTC_1_58.SetRadio()
    this.Parent.oContained.w_PCSAINTC=trim(this.Parent.oContained.w_PCSAINTC)
    this.value = ;
      iif(this.Parent.oContained.w_PCSAINTC=='S',1,;
      0)
  endfunc

  func oPCSAINTC_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCADDINT='S')
    endwith
   endif
  endfunc


  add object oPCDESSPE_1_60 as StdCombo with uid="ISAQCPZDKX",rtseq=34,rtrep=.f.,left=203,top=508,width=163,height=21;
    , ToolTipText = "Addebito spese direttamente sul cliente o sul relativo costo";
    , HelpContextID = 17829061;
    , cFormVar="w_PCDESSPE",RowSource=""+"Assegna spese al cliente,"+"Assegna spese al costo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPCDESSPE_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oPCDESSPE_1_60.GetRadio()
    this.Parent.oContained.w_PCDESSPE = this.RadioValue()
    return .t.
  endfunc

  func oPCDESSPE_1_60.SetRadio()
    this.Parent.oContained.w_PCDESSPE=trim(this.Parent.oContained.w_PCDESSPE)
    this.value = ;
      iif(this.Parent.oContained.w_PCDESSPE=='S',1,;
      iif(this.Parent.oContained.w_PCDESSPE=='C',2,;
      0))
  endfunc

  add object oStr_1_27 as StdString with uid="GQWIDZBQUE",Visible=.t., Left=30, Top=13,;
    Alignment=1, Width=171, Height=15,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="UWMKOEQRMI",Visible=.t., Left=10, Top=38,;
    Alignment=1, Width=191, Height=18,;
    Caption="Causale contabile insoluto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="DGEYKBIZZG",Visible=.t., Left=10, Top=140,;
    Alignment=1, Width=191, Height=18,;
    Caption="Spese bancarie insoluto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HSDDJQBJTS",Visible=.t., Left=10, Top=63,;
    Alignment=1, Width=191, Height=18,;
    Caption="Contropartita spese bancarie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YZQJIRGIGR",Visible=.t., Left=10, Top=165,;
    Alignment=1, Width=191, Height=18,;
    Caption="Pag.clienti in moratoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="ABVZLCHICN",Visible=.t., Left=13, Top=325,;
    Alignment=1, Width=188, Height=18,;
    Caption="Spese lettera:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="CCSIVRXQER",Visible=.t., Left=10, Top=271,;
    Alignment=1, Width=85, Height=18,;
    Caption="Inoltro solleciti"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="WJZHFZOTLH",Visible=.t., Left=10, Top=190,;
    Alignment=1, Width=191, Height=18,;
    Caption="Durata moratoria (gg.):"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RAAGNJQLAJ",Visible=.t., Left=273, Top=190,;
    Alignment=1, Width=179, Height=18,;
    Caption="Intervallo tra solleciti (gg.):"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="TVJQTMYHDW",Visible=.t., Left=13, Top=299,;
    Alignment=1, Width=188, Height=18,;
    Caption="Modalit� di inoltro predefinita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="TMQWXUSKHF",Visible=.t., Left=13, Top=352,;
    Alignment=1, Width=188, Height=18,;
    Caption="Spese raccomandata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="DVOAXKVYGJ",Visible=.t., Left=13, Top=379,;
    Alignment=1, Width=188, Height=18,;
    Caption="Spese raccomandata A.R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="WYFRUHJYYP",Visible=.t., Left=349, Top=325,;
    Alignment=1, Width=97, Height=18,;
    Caption="Spese FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="CUXTXXOMRN",Visible=.t., Left=349, Top=352,;
    Alignment=1, Width=97, Height=18,;
    Caption="Spese E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="JNKTNMOLIG",Visible=.t., Left=10, Top=114,;
    Alignment=1, Width=191, Height=18,;
    Caption="Valuta importi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="DMNVGQFIWV",Visible=.t., Left=10, Top=89,;
    Alignment=1, Width=191, Height=18,;
    Caption="Contropartita riapertura scad.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="WITAFLXEDV",Visible=.t., Left=10, Top=218,;
    Alignment=1, Width=191, Height=18,;
    Caption="Descrizione parametrica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="JICTTCBVTB",Visible=.t., Left=48, Top=406,;
    Alignment=1, Width=153, Height=18,;
    Caption="Spese telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="PDPTPSLFNJ",Visible=.t., Left=349, Top=379,;
    Alignment=1, Width=97, Height=18,;
    Caption="Spese PEC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="PSXWTDPEYD",Visible=.t., Left=145, Top=455,;
    Alignment=1, Width=56, Height=18,;
    Caption="Interessi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="GBJZJJGIRF",Visible=.t., Left=4, Top=432,;
    Alignment=1, Width=91, Height=18,;
    Caption="Flussi di ritorno"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="SWDARLGSNI",Visible=.t., Left=303, Top=455,;
    Alignment=1, Width=19, Height=18,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="OKJNUCOANV",Visible=.t., Left=110, Top=509,;
    Alignment=1, Width=91, Height=18,;
    Caption="Destin. spese:"  ;
  , bGlobalFont=.t.

  add object oBox_1_55 as StdBox with uid="SEPOUOOKJS",left=4, top=288, width=587,height=2

  add object oBox_1_56 as StdBox with uid="LOKUFJSYZM",left=4, top=448, width=587,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_apc','PAR_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PCCODAZI=PAR_CONT.PCCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
