* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_ber                                                        *
*              Elimina flusso di ritorno                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-04                                                      *
* Last revis.: 2012-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NOMSUP,w_DTARIC,w_NOMFIL,w_TABLE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_ber",oParentObject,m.w_NOMSUP,m.w_DTARIC,m.w_NOMFIL,m.w_TABLE)
return(i_retval)

define class tgsso_ber as StdBatch
  * --- Local variables
  w_NOMSUP = space(20)
  w_DTARIC = ctod("  /  /  ")
  w_NOMFIL = space(100)
  w_TABLE = space(1)
  * --- WorkFile variables
  FLU_RITO_idx=0
  FLU_ESRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_TABLE="F"
      * --- Try
      local bErr_0347B2F0
      bErr_0347B2F0=bTrsErr
      this.Try_0347B2F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0347B2F0
      * --- End
    else
      * --- Try
      local bErr_034719B0
      bErr_034719B0=bTrsErr
      this.Try_034719B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_034719B0
      * --- End
    endif
  endproc
  proc Try_0347B2F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from FLU_RITO
    i_nConn=i_TableProp[this.FLU_RITO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FLNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
            +" and FLDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
            +" and FLNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
             )
    else
      delete from (i_cTable) where;
            FLNOMSUP = this.w_NOMSUP;
            and FLDTARIC = this.w_DTARIC;
            and FLNOMFIL = this.w_NOMFIL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_034719B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from FLU_ESRI
    i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FRNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
            +" and FRDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
            +" and FRNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
             )
    else
      delete from (i_cTable) where;
            FRNOMSUP = this.w_NOMSUP;
            and FRDTARIC = this.w_DTARIC;
            and FRNOMFIL = this.w_NOMFIL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_NOMSUP,w_DTARIC,w_NOMFIL,w_TABLE)
    this.w_NOMSUP=w_NOMSUP
    this.w_DTARIC=w_DTARIC
    this.w_NOMFIL=w_NOMFIL
    this.w_TABLE=w_TABLE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='FLU_RITO'
    this.cWorkTables[2]='FLU_ESRI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NOMSUP,w_DTARIC,w_NOMFIL,w_TABLE"
endproc
