* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_bfo                                                        *
*              Gestione flussi operativi                                       *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-17                                                      *
* Last revis.: 2015-12-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Param
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsso_bfo",oParentObject,m.w_Param)
return(i_retval)

define class tgsso_bfo as StdBatch
  * --- Local variables
  w_NUMRAG = 0
  w_Param = space(3)
  w_ZOOM = space(10)
  w_NOMFIL = space(100)
  w_DATIEL = space(200)
  w_NOMSUP = space(20)
  w_DTARIC = ctod("  /  /  ")
  w_TIPREC = space(2)
  w_NUMREC = 0
  w_CAUABI = space(5)
  w_DESABI = space(40)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_NUMEFF = 0
  w_TOTIMP = 0
  w_TOTSPE = 0
  w_TOTADD = 0
  w_NOSELECT = .f.
  w_ERRORMSG = space(150)
  w_ERRORE = space(100)
  w_ERRATO = .f.
  w_DATVAL = ctod("  /  /  ")
  w_STARIC = space(1)
  w_NUMMOV = 0
  w_NUMMOV1 = 0
  w_NUMMOV2 = 0
  w_ERROR = .f.
  w_TRANMSG = space(100)
  w_TIPMOV = space(1)
  w_FLGRIC = space(1)
  w_FLPART = space(1)
  w_STARIC = space(1)
  w_RSNUMDIS = space(10)
  w_RSDATDIS = ctod("  /  /  ")
  w_RSDISEFF = space(10)
  w_RSFLGRIC = space(1)
  w_RSFLDEFI = space(1)
  w_RSRICRIF = space(5)
  w_RSBANRIF = space(15)
  w_RSBANCRE = space(15)
  w_RSDESCRI = space(35)
  w_RSNUMDOC = 0
  w_RSALFDOC = space(2)
  w_RSDATDOC = ctod("  /  /  ")
  w_RSMOVTES = space(10)
  w_CAUDIS = space(5)
  w_DATCOM = ctod("  /  /  ")
  w_NUMDIS = space(10)
  w_FLFLGACC = space(1)
  w_NUMRIGA = 0
  w_CPROWNUM = 0
  w_FLGFRZ = space(1)
  w_LIBSCA = space(1)
  w_BANUMCOR = space(15)
  w_TIPOOPER = space(1)
  w_ANDESCRI = space(40)
  w_CAUTES = space(5)
  w_DESCRI = space(50)
  w_NUMDIS = space(10)
  w_FLANEV = space(1)
  w_COSERIAL = space(10)
  w_CODATREG = ctod("  /  /  ")
  w_COCOMPET = space(4)
  w_CONUMREG = 0
  w_CODATULT = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_NOTE = space(0)
  w_TOTIMP = 0
  w_PTNUMEFF = 0
  w_COMMISS = 0
  w_MESS = space(10)
  w_VAL1 = 0
  w_CALFIR = space(3)
  w_TEDATREG = ctod("  /  /  ")
  w_TERIFCON = space(10)
  w_SELEZI = space(1)
  w_PISERIAL = space(10)
  w_PIMOVTES = space(10)
  w_PINUMDOC = 0
  w_PIALFDOC = space(2)
  w_PIDATDOC = ctod("  /  /  ")
  w_PICATPAG = space(2)
  w_PIDESCRI = space(50)
  w_PICODVAL = space(3)
  w_PIVALNAZ = space(3)
  w_VALSPE = space(3)
  w_PICODCAU = space(5)
  w_PICODBAN = space(15)
  w_PIDATREG = ctod("  /  /  ")
  w_PIDATVAL = ctod("  /  /  ")
  w_PICAOVAL = 0
  w_PICAOVAT = 0
  w_PICOSCOM = 0
  w_PIADDSPE = space(1)
  w_PIADDINT = space(1)
  w_PIPERINT = 0
  w_STATUS = space(1)
  w_SALINI = space(1)
  w_CODVAC = space(3)
  w_DECVAC = 0
  w_INSAS = space(1)
  w_PICOMPET = space(4)
  w_PINUMREG = 0
  w_COCODRAG = space(10)
  w_PICOSCOR = 0
  w_PIDATVAR = ctod("  /  /  ")
  w_PIDTVLCO = ctod("  /  /  ")
  w_CAFLPART = space(1)
  w_CACONIND = space(1)
  w_INTMORA = space(1)
  w_EXISTRIC = .f.
  w_INSREC = 0
  w_INSREC1 = 0
  w_INSREC2 = 0
  w_DATCOM = ctod("  /  /  ")
  w_DTARIC = ctod("  /  /  ")
  w_RIGAESITO = 0
  w_TIPOSCA = 0
  w_NUMDISRIC = space(10)
  w_UPMOVTES = space(10)
  w_INSOLUTO = space(10)
  w_EFFETTO = 0
  w_MANCATOINC = space(10)
  w_DATINS = ctod("  /  /  ")
  w_NUMINS = 0
  w_EFSERIAL = space(10)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_RIGA = 0
  w_PTFLIMPE = space(2)
  w_TIPOEFFETTO = space(1)
  w_ORDINA = space(1)
  w_NEWORDINA = space(1)
  w_SPESE = 0
  w_CODAZI = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_SAGINT = 0
  w_SPRINT = space(1)
  w_PERINT = 0
  w_SAINTC = space(1)
  w_ADDINT = space(1)
  w_SPEINS = 0
  w_DESSPE = space(1)
  w_CODVAL = space(3)
  w_ADDSPE = space(1)
  w_COIMPSPE = 0
  w_INTERESSI = 0
  w_COCODBAN = space(15)
  w_COCODBAN = space(15)
  w_DATAVUOTA = ctod("  /  /  ")
  w_GESCON = space(1)
  w_DATINS = ctod("  /  /  ")
  w_NUMINS = 0
  w_SERIAL = space(10)
  w_PCDESSUP = space(0)
  w_ALFRAG = space(10)
  w_DATRAG = ctod("  /  /  ")
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  * --- WorkFile variables
  DIS_TINT_idx=0
  CON_TENZ_idx=0
  CONDTENZ_idx=0
  FLU_RITO_idx=0
  PAR_TITE_idx=0
  CAU_REBA_idx=0
  FLU_ESRI_idx=0
  CONTI_idx=0
  GES_PIAN_idx=0
  PAR_CONT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione maschera GSRB_KFO "GESTIONE FLUSSI OPERATIVI"
    * --- Variabili da passare al batch di generazione mov.tesoreria x richiamo scadenze
    * --- --
    this.w_NUMMOV = 0
    this.w_NUMMOV1 = 0
    this.w_NUMMOV2 = 0
    this.w_ERRATO = .F.
    * --- Creo cursore degli errori
    if this.w_PARAM="SAL"
      CREATE CURSOR CURSERR (MSG C(254), ERRORE C(254),NOMESUP C(20),NOMEFIL C(100),DATARIC D)
    endif
    this.w_ZOOM = this.oParentObject.w_GSSO_KFO
    if this.w_Param="CAR"
      this.oParentObject.NotifyEvent("CaricaZoom")
      * --- Inserisco il risultato del cursore nello zoom
      this.oParentObject.w_TOTSPESE = 0
      this.oParentObject.w_TOTIMPO = 0
      this.oParentObject.w_NUMRECZ = 0
      SELECT ( this.w_ZOOM.cCursor )
      SUM TOTIMP, TOTSPE TO this.oParentObject.w_TOTIMPO, this.oParentObject.w_TOTSPESE
      COUNT TO this.oParentObject.w_NUMRECZ
      * --- Se il cursore � vuoto non crea il vettore
      SELECT ( this.w_ZOOM.cCursor )
      GO TOP
      this.w_zoom.grd.refresh
      * --- Setta Proprieta' Campi del Cursore
      FOR I=1 TO This.w_Zoom.grd.ColumnCount
      NC = ALLTRIM(STR(I))
      if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
        This.w_Zoom.grd.Column&NC..Enabled=.f.
      endif
      ENDFOR
      * --- Creo temporaneo con il contenuto dello zoom
    else
      * --- Creo la tabella temporanea con il contenuto dello zoom
      ah_msg( "Creazione temporaneo elaborazione" ) 
 SELECT * from ( this.w_ZOOM.cCursor ) ; 
 WHERE NUMELA=1 AND CAUABI<>"16000" AND CAUABI<>"37000" AND CAUABI<>"57000" AND NOT(CAUABI="07000" AND EMPTY(NVL(TIPCON," "))) ; 
 INTO CURSOR Dati_Elaborazione
      if used("Dati_Elaborazione")
        CURTOTAB("Dati_Elaborazione","TMPCURSSS")
        SELECT Dati_Elaborazione 
 USE
      else
        Ah_ErrorMsg("Non ci sono dati da generare",48)
        i_retcode = 'stop'
        return
      endif
      do case
        case this.w_Param="SAL" AND this.oParentObject.w_TIPELA="INS"
          ah_msg( "Creazione insoluti e mancati incassi" )
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
      * --- Aggiorno i promemoria contabili (se le righe con riferimento a questi sono state tutte elaborate)
      * --- Write into FLU_RITO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FLU_RITO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="FLNOMSUP,FLNOMFIL,FLDTARIC,CPROWNUM"
        do vq_exec with '..\soll\exe\query\gssobbfo',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_RITO_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" from "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 set ";
        +"FLU_RITO.FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +Iif(Empty(i_ccchkf),"",",FLU_RITO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="FLU_RITO.FLNOMSUP = t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set (";
            +"FLFLGACC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".FLNOMSUP = "+i_cQueryTable+".FLNOMSUP";
                +" and "+i_cTable+".FLNOMFIL = "+i_cQueryTable+".FLNOMFIL";
                +" and "+i_cTable+".FLDTARIC = "+i_cQueryTable+".FLDTARIC";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into FLU_ESRI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="FRNOMSUP,FRNOMFIL,FRDTARIC,CPROWNUM"
        do vq_exec with '..\soll\exe\query\gssoabfo',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_ESRI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" from "+i_cTable+" FLU_ESRI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI, "+i_cQueryTable+" _t2 set ";
        +"FLU_ESRI.FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +Iif(Empty(i_ccchkf),"",",FLU_ESRI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="FLU_ESRI.FRNOMSUP = t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI set (";
            +"FRFLGELA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".FRNOMSUP = "+i_cQueryTable+".FRNOMSUP";
                +" and "+i_cTable+".FRNOMFIL = "+i_cQueryTable+".FRNOMFIL";
                +" and "+i_cTable+".FRDTARIC = "+i_cQueryTable+".FRDTARIC";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- aggiorno flag elaborato per riferimenti promemoria contabili dove tutte le righe del flusso sono elaborate
      * --- Write into FLU_RITO
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FLU_RITO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="FLNOMSUP,FLNOMFIL,FLDTARIC,CPROWNUM"
        do vq_exec with '..\soll\exe\query\gssobbfo1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_RITO_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" from "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 set ";
        +"FLU_RITO.FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +Iif(Empty(i_ccchkf),"",",FLU_RITO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="FLU_RITO.FLNOMSUP = t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set (";
            +"FLFLGACC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
                +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
                +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
                +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".FLNOMSUP = "+i_cQueryTable+".FLNOMSUP";
                +" and "+i_cTable+".FLNOMFIL = "+i_cQueryTable+".FLNOMFIL";
                +" and "+i_cTable+".FLDTARIC = "+i_cQueryTable+".FLDTARIC";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into FLU_ESRI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="FRNOMSUP,FRNOMFIL,FRDTARIC,CPROWNUM"
        do vq_exec with '..\soll\exe\query\gssoabfo1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_ESRI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" from "+i_cTable+" FLU_ESRI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI, "+i_cQueryTable+" _t2 set ";
        +"FLU_ESRI.FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +Iif(Empty(i_ccchkf),"",",FLU_ESRI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="FLU_ESRI.FRNOMSUP = t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI set (";
            +"FRFLGELA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="FLU_ESRI.FRNOMSUP = _t2.FRNOMSUP";
                +" and "+"FLU_ESRI.FRNOMFIL = _t2.FRNOMFIL";
                +" and "+"FLU_ESRI.FRDTARIC = _t2.FRDTARIC";
                +" and "+"FLU_ESRI.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_ESRI set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".FRNOMSUP = "+i_cQueryTable+".FRNOMSUP";
                +" and "+i_cTable+".FRNOMFIL = "+i_cQueryTable+".FRNOMFIL";
                +" and "+i_cTable+".FRDTARIC = "+i_cQueryTable+".FRDTARIC";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Chiudo i cursori
      if USED("_Curs_PARTITE")
        Select _Curs_PARTITE 
 Use
      endif
      if USED("CursCausaliRem")
        Select CursCausaliRem 
 Use
      endif
      if USED("Dati_Elaborazione")
        Select Dati_Elaborazione 
 Use
      endif
      * --- --
      * --- Drop temporary table TMPCURSSS
      i_nIdx=cp_GetTableDefIdx('TMPCURSSS')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPCURSSS')
      endif
      if this.w_ERRATO OR RECCOUNT("CURSERR")>0
        if AH_YESNO("Si sono verificati degli errori, vuoi eseguire la stampa?")
          Select distinct * from CURSERR into cursor __TMP__
          cp_chprn("..\soll\exe\query\GSSO_BFO.FRX","",this.oParentObject)
        endif
      endif
      this.oParentObject.NotifyEvent("Riesegui")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Movimento di Insoluto
    * --- definizione variaibli per gestione generazione da insoluto
    this.w_INTMORA = "I"
    this.w_NUMDISRIC = "zzzzzzzzzz"
    this.w_CODAZI = I_CODAZI
    * --- Read from PAR_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_CONT_idx,2],.t.,this.PAR_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCDESSUP,PCADDINT,PCPERINT,PCSAINTC,PCSPEINS,PCDESSPE"+;
        " from "+i_cTable+" PAR_CONT where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCDESSUP,PCADDINT,PCPERINT,PCSAINTC,PCSPEINS,PCDESSPE;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PCDESSUP = NVL(cp_ToDate(_read_.PCDESSUP),cp_NullValue(_read_.PCDESSUP))
      this.w_ADDINT = NVL(cp_ToDate(_read_.PCADDINT),cp_NullValue(_read_.PCADDINT))
      this.w_PERINT = NVL(cp_ToDate(_read_.PCPERINT),cp_NullValue(_read_.PCPERINT))
      this.w_SAINTC = NVL(cp_ToDate(_read_.PCSAINTC),cp_NullValue(_read_.PCSAINTC))
      this.w_SPEINS = NVL(cp_ToDate(_read_.PCSPEINS),cp_NullValue(_read_.PCSPEINS))
      this.w_DESSPE = NVL(cp_ToDate(_read_.PCDESSPE),cp_NullValue(_read_.PCDESSPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    VQ_EXEC("..\soll\exe\query\gsso1bfo",this,"_Curs_PARTITE")
    if USED("_Curs_PARTITE")
      ah_msg( "Generazione insoluti/mancati incassi" )
      do GSSO_BRC with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NUMDIS = SPACE(10)
      this.w_NUMRIGA = 0
      this.w_NOMSUP = SPACE(20)
      this.w_DATCOM = cp_CharToDate("  -  -    ")
      this.w_DTARIC = cp_CharToDate("  -  -    ")
      this.w_PISERIAL = space(10)
      this.w_COSERIAL = space(10)
      this.w_TIPOSCA = 0
      * --- Try
      local bErr_04245618
      bErr_04245618=bTrsErr
      this.Try_04245618()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if USED("_Curs_PARTITE")
          Select _Curs_PARTITE 
 Use
        endif
        this.w_NUMMOV = this.w_NUMMOV - this.w_INSREC
        this.w_ERRATO = .T.
        this.w_ERRORMSG = AH_MSGFORMAT ("Errore nella creazione degli insoluti")
        this.w_ERRORE = NVL(i_ErrMsg,"")+IIF("OP_"$message(),"",IIF("ODBC"$message(),CHR(13)+message(),""))
        INSERT INTO CURSERR (MSG,ERRORE,NOMESUP,NOMEFIL,DATARIC) VALUES (this.w_ERRORMSG,this.w_ERRORE, ; 
 this.w_NOMSUP,this.w_NOMFIL,this.w_DTARIC)
      endif
      bTrsErr=bTrsErr or bErr_04245618
      * --- End
    else
      Ah_ErrorMsg("Non ci sono dati da generare",48)
    endif
  endproc
  proc Try_04245618()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Controllo se � stato variato il nome supporto oppure il numero distinta
    *     oppure il nome file oppure la data ricezione
    * --- begin transaction
    cp_BeginTrs()
    this.w_PIDATREG = cp_ToDate(this.oParentObject.w_DATELA)
    this.w_PICOMPET = CALCESER(this.w_PIDATREG)
    this.w_ORDINA = "X"
    i_Conn=i_TableProp[this.CON_TENZ_IDX, 3]
    SELECT _Curs_PARTITE 
 GO TOP 
 SCAN
    this.w_ERROR = .F.
    this.w_INSREC = 0
    this.w_INSREC1 = 0
    this.w_INSREC2 = 0
    this.w_TIPCON = " "
    this.w_CODCON = space(15)
    this.w_NUMDIS = NVL(NUMDIS, SPACE(10))
    this.w_CODVAL = g_PERVAL
    this.w_EFSERIAL = EFSERIAL 
    this.w_EFFETTO = PTNUMEFF 
    this.w_INSOLUTO = ""
    this.w_PTSERIAL = PTSERIAL
    this.w_PTROWORD = PTROWORD
    this.w_RIGA = CPROWNUM
    this.w_NEWORDINA = ORDINA
    this.w_PTFLIMPE = NVL(PTFLIMPE,"")
    this.w_TIPOEFFETTO = TIPO
    this.w_ADDSPE = IIF(this.w_TIPOEFFETTO="I", "S","")
    this.w_SPESE = NVL(TOTSPE,0)
    this.w_COIMPSPE = IIF(this.w_SPESE<>0,this.w_SPESE,this.w_SPEINS)
    if this.w_ORDINA<>this.w_NEWORDINA
      this.w_COCODRAG = SPACE(10)
      cp_NextTableProg(this, i_Conn, "GRINS", "i_CODAZI,w_PICOMPET,w_COCODRAG")
      * --- Insert into GES_PIAN
      i_nConn=i_TableProp[this.GES_PIAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GES_PIAN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GES_PIAN_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"GPCODRAG"+",GPCOMPET"+",GPADDSOL"+",GPADDINT"+",GP__TIPO"+",GPTIPEFF"+",GPDESSPE"+",GPDATREG"+",GPCODVAL"+",GPIMPSPE"+",GPADDSPE"+",GPCATPAG"+",GPPERINT"+",GPSAINTC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_COCODRAG),'GES_PIAN','GPCODRAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PICOMPET),'GES_PIAN','GPCOMPET');
        +","+cp_NullLink(cp_ToStrODBC("S"),'GES_PIAN','GPADDSOL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ADDINT),'GES_PIAN','GPADDINT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOEFFETTO),'GES_PIAN','GP__TIPO');
        +","+cp_NullLink(cp_ToStrODBC("E"),'GES_PIAN','GPTIPEFF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESSPE),'GES_PIAN','GPDESSPE');
        +","+cp_NullLink(cp_ToStrODBC(I_DATSYS),'GES_PIAN','GPDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'GES_PIAN','GPCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(0),'GES_PIAN','GPIMPSPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ADDSPE),'GES_PIAN','GPADDSPE');
        +","+cp_NullLink(cp_ToStrODBC("TT"),'GES_PIAN','GPCATPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PERINT),'GES_PIAN','GPPERINT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SAINTC),'GES_PIAN','GPSAINTC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'GPCODRAG',this.w_COCODRAG,'GPCOMPET',this.w_PICOMPET,'GPADDSOL',"S",'GPADDINT',this.w_ADDINT,'GP__TIPO',this.w_TIPOEFFETTO,'GPTIPEFF',"E",'GPDESSPE',this.w_DESSPE,'GPDATREG',I_DATSYS,'GPCODVAL',this.w_CODVAL,'GPIMPSPE',0,'GPADDSPE',this.w_ADDSPE,'GPCATPAG',"TT")
        insert into (i_cTable) (GPCODRAG,GPCOMPET,GPADDSOL,GPADDINT,GP__TIPO,GPTIPEFF,GPDESSPE,GPDATREG,GPCODVAL,GPIMPSPE,GPADDSPE,GPCATPAG,GPPERINT,GPSAINTC &i_ccchkf. );
           values (;
             this.w_COCODRAG;
             ,this.w_PICOMPET;
             ,"S";
             ,this.w_ADDINT;
             ,this.w_TIPOEFFETTO;
             ,"E";
             ,this.w_DESSPE;
             ,I_DATSYS;
             ,this.w_CODVAL;
             ,0;
             ,this.w_ADDSPE;
             ,"TT";
             ,this.w_PERINT;
             ,this.w_SAINTC;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Verifica che la partita non sia gi� stata inserita in contenzioso
    * --- Select from gsso3bfo
    do vq_exec with 'gsso3bfo',this,'_Curs_gsso3bfo','',.f.,.t.
    if used('_Curs_gsso3bfo')
      select _Curs_gsso3bfo
      locate for 1=1
      do while not(eof())
      this.w_INSOLUTO = _Curs_GSSO3BFO.COSERIAL
        select _Curs_gsso3bfo
        continue
      enddo
      use
    endif
    if EMPTY (this.w_INSOLUTO)
      SELECT _Curs_PARTITE
      if this.w_NOMSUP<>NOMSUP OR this.w_NOMFIL<>NOMFIL OR this.w_DTARIC<>NVL(DTARIC,cp_CharToDate("  -  -    ")) OR NVL(TIPOSCA, 0) <> this.w_TIPOSCA OR _Curs_PARTITE.BANUMCOR<>this.w_BANUMCOR
        this.w_NOMSUP = NVL(NOMSUP, SPACE(20))
        this.w_NOMFIL = NOMFIL
        this.w_DTARIC = NVL(DTARIC,cp_CharToDate("  -  -    "))
        this.w_BANUMCOR = _Curs_PARTITE.BANUMCOR
      endif
      if NVL(TIPOSCA, 0) <> this.w_TIPOSCA
        this.w_TIPOSCA = NVL(TIPOSCA, 0) 
        this.w_PISERIAL = space(10)
      endif
      this.w_DESCRI = SUBSTR(DESABI,1,50)
      this.w_DATCOM = NVL(DATCOM,cp_CharToDate("  -  -    "))
      * --- Se CAFLPART= "S" oppure CAFLPART <> 'S'  e CACONIND='S' , genero insoluti
      *     altrimenti genero mancati incassi
      this.w_CAFLPART = NVL(CAFLPART," ")
      * --- Inserisce Piano insoluti
      this.w_NUMDIS = NVL(DINUMDIS,space(10))
      this.w_EXISTRIC = .F.
      this.w_COMMISS = NVL(TOTSPE,0)
      this.w_RSMOVTES = space(10)
      this.w_UPMOVTES = space(10)
      if this.w_CAFLPART = "S" OR (this.w_CAFLPART<>"S" AND this.w_PTFLIMPE="DI")
        if this.w_PTNUMEFF<>_Curs_PARTITE.PTNUMEFF
          this.w_PTNUMEFF = _Curs_PARTITE.PTNUMEFF
          this.w_INTERESSI = IIF(this.w_SAINTC="S" AND NOT EMPTY(NVL(_Curs_PARTITE.ANSAGINT,0)) AND _Curs_PARTITE.ANSPRINT<>"S",_Curs_PARTITE.ANSAGINT,this.w_PERINT)
          this.w_COCODBAN = IIF(_Curs_PARTITE.BACONSBF="S",_Curs_PARTITE.BACONASS,_Curs_PARTITE.DIBANRIF)
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Determina Seriale e Numero Registrazione INSOLUTO
          this.w_PISERIAL = space(10)
          cp_NextTableProg(this, i_Conn, "SEINS", "i_CODAZI,w_PISERIAL")
          this.w_PIDATDOC = cp_ToDate(this.w_DTARIC)
          this.w_PINUMREG = 0
          cp_NextTableProg(this, i_Conn, "PRINS", "i_CODAZI,w_PICOMPET,w_PINUMREG")
          * --- Inserisce Testata INSOLUTO
          this.w_PIDESCRI = this.w_DESCRI
          this.w_PICODVAL = _Curs_PARTITE.DICODVAL
          this.w_PIVALNAZ = this.oParentObject.w_VALNAZ
          this.w_VALSPE = _Curs_PARTITE.BACODVAL
          this.w_PICODBAN = IIF(EMPTY(NVL(_Curs_PARTITE.BACONCOL,"")),_Curs_PARTITE.BANUMCOR,_Curs_PARTITE.BACONCOL)
          this.w_PICAOVAL = _Curs_PARTITE.DICAOVAC
          this.w_PICAOVAT = _Curs_PARTITE.DICAOVAT
          this.w_CODVAC = _Curs_PARTITE.BACODVAL
          this.w_DECVAC = _Curs_PARTITE.DECVAC
          this.w_PICATPAG = _Curs_PARTITE.CATPAG
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          this.w_CALFIR = _Curs_PARTITE.BACALFES
          this.w_CAUABI = _Curs_PARTITE.CAUABI
          if this.w_TIPOEFFETTO="M"
            this.w_COCODBAN = _Curs_PARTITE.PTBANNOS
          else
            this.w_COCODBAN = IIF(_Curs_PARTITE.BACONSBF="S",_Curs_PARTITE.BACONASS,_Curs_PARTITE.DIBANRIF)
          endif
          Select _Curs_PARTITE
          this.w_INSREC = this.w_INSREC + 1
          this.w_NUMMOV = this.w_NUMMOV + 1
          this.w_DATAVUOTA = cp_CharToDate("  -  -    ")
          * --- Insert into CON_TENZ
          i_nConn=i_TableProp[this.CON_TENZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TENZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"COSERIAL"+",CONUMREG"+",COCOMPET"+",CODATREG"+",COCODRAG"+",COCAOVAL"+",COCATPAG"+",COBANPRE"+",COCONTRO"+",COCODVAL"+",COIMPSPE"+",CODESCRI"+",COVALNAZ"+",COADDSPE"+",COADDINT"+",CODATDOC"+",COTIPCON"+",COCODCON"+",CO__TIPO"+",COTIPEFF"+",CO__NOTE"+",COSTATUS"+",COADDSOL"+",CODESSPE"+",CODATVAL"+",COPERINT"+",CODATINT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PISERIAL),'CON_TENZ','COSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PINUMREG),'CON_TENZ','CONUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PICOMPET),'CON_TENZ','COCOMPET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDATREG),'CON_TENZ','CODATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCODRAG),'CON_TENZ','COCODRAG');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.DICAOVAC),'CON_TENZ','COCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PICATPAG),'CON_TENZ','COCATPAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COCODBAN),'CON_TENZ','COBANPRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAUTES),'CON_TENZ','COCONTRO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.DICODVAL),'CON_TENZ','COCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COIMPSPE),'CON_TENZ','COIMPSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDESCRI),'CON_TENZ','CODESCRI');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VALNAZ),'CON_TENZ','COVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ADDSPE),'CON_TENZ','COADDSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ADDINT),'CON_TENZ','COADDINT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDATDOC),'CON_TENZ','CODATDOC');
            +","+cp_NullLink(cp_ToStrODBC("C"),'CON_TENZ','COTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.PTCODCON),'CON_TENZ','COCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOEFFETTO),'CON_TENZ','CO__TIPO');
            +","+cp_NullLink(cp_ToStrODBC("E"),'CON_TENZ','COTIPEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CON_TENZ','CO__NOTE');
            +","+cp_NullLink(cp_ToStrODBC("PE"),'CON_TENZ','COSTATUS');
            +","+cp_NullLink(cp_ToStrODBC("S"),'CON_TENZ','COADDSOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSPE),'CON_TENZ','CODESSPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CON_TENZ','CODATVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_INTERESSI),'CON_TENZ','COPERINT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.PTDATSCA),'CON_TENZ','CODATINT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'COSERIAL',this.w_PISERIAL,'CONUMREG',this.w_PINUMREG,'COCOMPET',this.w_PICOMPET,'CODATREG',this.w_PIDATREG,'COCODRAG',this.w_COCODRAG,'COCAOVAL',_Curs_PARTITE.DICAOVAC,'COCATPAG',this.w_PICATPAG,'COBANPRE',this.w_COCODBAN,'COCONTRO',this.w_CAUTES,'COCODVAL',_Curs_PARTITE.DICODVAL,'COIMPSPE',this.w_COIMPSPE,'CODESCRI',this.w_PIDESCRI)
            insert into (i_cTable) (COSERIAL,CONUMREG,COCOMPET,CODATREG,COCODRAG,COCAOVAL,COCATPAG,COBANPRE,COCONTRO,COCODVAL,COIMPSPE,CODESCRI,COVALNAZ,COADDSPE,COADDINT,CODATDOC,COTIPCON,COCODCON,CO__TIPO,COTIPEFF,CO__NOTE,COSTATUS,COADDSOL,CODESSPE,CODATVAL,COPERINT,CODATINT &i_ccchkf. );
               values (;
                 this.w_PISERIAL;
                 ,this.w_PINUMREG;
                 ,this.w_PICOMPET;
                 ,this.w_PIDATREG;
                 ,this.w_COCODRAG;
                 ,_Curs_PARTITE.DICAOVAC;
                 ,this.w_PICATPAG;
                 ,this.w_COCODBAN;
                 ,this.w_CAUTES;
                 ,_Curs_PARTITE.DICODVAL;
                 ,this.w_COIMPSPE;
                 ,this.w_PIDESCRI;
                 ,this.oParentObject.w_VALNAZ;
                 ,this.w_ADDSPE;
                 ,this.w_ADDINT;
                 ,this.w_PIDATDOC;
                 ,"C";
                 ,_Curs_PARTITE.PTCODCON;
                 ,this.w_TIPOEFFETTO;
                 ,"E";
                 ,this.w_NOTE;
                 ,"PE";
                 ,"S";
                 ,this.w_DESSPE;
                 ,this.w_DATAVUOTA;
                 ,this.w_INTERESSI;
                 ,_Curs_PARTITE.PTDATSCA;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.w_NUMDOC = _Curs_PARTITE.NUMDOC
        this.w_ALFDOC = _Curs_PARTITE.ALFDOC
        this.w_DATDOC = _Curs_PARTITE.DATDOC
        if _Curs_PARTITE.PTFLRAGG<>"S"
          this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(this.w_NUMDOC,0),15)) + IIF(NOT EMPTY(NVL(this.w_ALFDOC,Space(10))), " / "+Alltrim(this.w_ALFDOC), ""), DTOC(this.w_DATDOC) )
        else
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Inserisce Dettaglio INSOLUTO
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWNUM * 10
        Select _Curs_PARTITE
        this.w_PICOSCOR = NVL(TOTSPE,0)
        this.w_VAL1 = RECNO()
        CALCULATE SUM(_Curs_PARTITE.TOTIMP) FOR _Curs_PARTITE.PTNUMEFF=this.w_PTNUMEFF ; 
 AND (_Curs_PARTITE.NOMSUP=this.w_NOMSUP OR _Curs_PARTITE.DINUMDIS = this.w_NUMDIS) TO w_PTTOTIMP
        Select _Curs_PARTITE 
 Go this.w_VAL1
        * --- Insert into CONDTENZ
        i_nConn=i_TableProp[this.CONDTENZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONDTENZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONDTENZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CPROWNUM"+",CPROWORD"+",COSERIAL"+",COSERRIF"+",COORDRIF"+",CONUMRIF"+",COCODBAN"+",COTOTIMP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CONDTENZ','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CONDTENZ','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PISERIAL),'CONDTENZ','COSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.PTSERIAL),'CONDTENZ','COSERRIF');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.PTROWORD),'CONDTENZ','COORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.CPROWNUM),'CONDTENZ','CONUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COCODBAN),'CONDTENZ','COCODBAN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_PARTITE.PTTOTIMP),'CONDTENZ','COTOTIMP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'COSERIAL',this.w_PISERIAL,'COSERRIF',_Curs_PARTITE.PTSERIAL,'COORDRIF',_Curs_PARTITE.PTROWORD,'CONUMRIF',_Curs_PARTITE.CPROWNUM,'COCODBAN',this.w_COCODBAN,'COTOTIMP',_Curs_PARTITE.PTTOTIMP)
          insert into (i_cTable) (CPROWNUM,CPROWORD,COSERIAL,COSERRIF,COORDRIF,CONUMRIF,COCODBAN,COTOTIMP &i_ccchkf. );
             values (;
               this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_PISERIAL;
               ,_Curs_PARTITE.PTSERIAL;
               ,_Curs_PARTITE.PTROWORD;
               ,_Curs_PARTITE.CPROWNUM;
               ,this.w_COCODBAN;
               ,_Curs_PARTITE.PTTOTIMP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Aggiorno la tabella flussi operativi di ritorno
      Select _Curs_PARTITE
      this.w_NUMRIGA = NVL(NUMRIGA,0)
      this.w_RIGAESITO = NVL(RIGAESITO,0)
      * --- Write into FLU_ESRI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_ESRI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FRFLGELA ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGELA');
        +",FRRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PISERIAL),'FLU_ESRI','FRRIFCON');
        +",FRRIFINS ="+cp_NullLink(cp_ToStrODBC(this.w_COCODRAG),'FLU_ESRI','FRRIFINS');
        +",FRRIFTES ="+cp_NullLink(cp_ToStrODBC(this.w_UPMOVTES),'FLU_ESRI','FRRIFTES');
            +i_ccchkf ;
        +" where ";
            +"FRNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
            +" and FRNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
            +" and FRDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGAESITO);
               )
      else
        update (i_cTable) set;
            FRFLGELA = "S";
            ,FRRIFCON = this.w_PISERIAL;
            ,FRRIFINS = this.w_COCODRAG;
            ,FRRIFTES = this.w_UPMOVTES;
            &i_ccchkf. ;
         where;
            FRNOMSUP = this.w_NOMSUP;
            and FRNOMFIL = this.w_NOMFIL;
            and FRDTARIC = this.w_DTARIC;
            and CPROWNUM = this.w_RIGAESITO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_GESCON = _Curs_PARTITE.ANGESCON
      if this.w_GESCON="B"
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANFLGCON ="+cp_NullLink(cp_ToStrODBC("S"),'CONTI','ANFLGCON');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC("C");
              +" and ANCODICE = "+cp_ToStrODBC(_Curs_PARTITE.PTCODCON);
                 )
        else
          update (i_cTable) set;
              ANFLGCON = "S";
              &i_ccchkf. ;
           where;
              ANTIPCON = "C";
              and ANCODICE = _Curs_PARTITE.PTCODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      Select _Curs_PARTITE
      this.w_RIGAESITO = NVL(RIGAESITO,0)
      this.w_NOMSUP = NVL(NOMSUP, SPACE(20))
      this.w_NOMFIL = NOMFIL
      this.w_DTARIC = NVL(DTARIC,cp_CharToDate("  -  -    "))
      if NOT EMPTY (this.w_INSOLUTO) 
        * --- Visualizza messaggio per l insoluto
        * --- Read from CON_TENZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_TENZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2],.t.,this.CON_TENZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CONUMREG,CODATREG"+;
            " from "+i_cTable+" CON_TENZ where ";
                +"COSERIAL = "+cp_ToStrODBC(this.w_INSOLUTO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CONUMREG,CODATREG;
            from (i_cTable) where;
                COSERIAL = this.w_INSOLUTO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMINS = NVL(cp_ToDate(_read_.CONUMREG),cp_NullValue(_read_.CONUMREG))
          this.w_DATINS = NVL(cp_ToDate(_read_.CODATREG),cp_NullValue(_read_.CODATREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into FLU_ESRI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FLU_ESRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FLU_ESRI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_ESRI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FRFLGERR ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_ESRI','FRFLGERR');
              +i_ccchkf ;
          +" where ";
              +"FRNOMSUP = "+cp_ToStrODBC(this.w_NOMSUP);
              +" and FRNOMFIL = "+cp_ToStrODBC(this.w_NOMFIL);
              +" and FRDTARIC = "+cp_ToStrODBC(this.w_DTARIC);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGAESITO);
                 )
        else
          update (i_cTable) set;
              FRFLGERR = "S";
              &i_ccchkf. ;
           where;
              FRNOMSUP = this.w_NOMSUP;
              and FRNOMFIL = this.w_NOMFIL;
              and FRDTARIC = this.w_DTARIC;
              and CPROWNUM = this.w_RIGAESITO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_ERRORMSG = AH_MSGFORMAT ("Errore nella creazione degli insoluti")
        this.w_ERRORE = AH_MSGFORMAT ("L' effetto n. %1 � gi� presente nell' insoluto n. %2 del %3",ALLTRIM (str(this.w_EFFETTO)),this.w_NUMINS, DTOC(this.w_DATINS))
        INSERT INTO CURSERR (MSG,ERRORE,NOMESUP,NOMEFIL,DATARIC) VALUES (this.w_ERRORMSG,this.w_ERRORE, ; 
 this.w_NOMSUP , this.w_NOMFIL , this.w_DTARIC)
      endif
    endif
    * --- Write into FLU_RITO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.FLU_RITO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FLU_RITO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="FLNOMSUP,FLNOMFIL,FLDTARIC,CPROWNUM"
      do vq_exec with 'GSSO8BFO',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.FLU_RITO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
              +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
              +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
              +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
          +i_ccchkf;
          +" from "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
              +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
              +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
              +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO, "+i_cQueryTable+" _t2 set ";
      +"FLU_RITO.FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
          +Iif(Empty(i_ccchkf),"",",FLU_RITO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="FLU_RITO.FLNOMSUP = t2.FLNOMSUP";
              +" and "+"FLU_RITO.FLNOMFIL = t2.FLNOMFIL";
              +" and "+"FLU_RITO.FLDTARIC = t2.FLDTARIC";
              +" and "+"FLU_RITO.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set (";
          +"FLFLGACC";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="FLU_RITO.FLNOMSUP = _t2.FLNOMSUP";
              +" and "+"FLU_RITO.FLNOMFIL = _t2.FLNOMFIL";
              +" and "+"FLU_RITO.FLDTARIC = _t2.FLDTARIC";
              +" and "+"FLU_RITO.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" FLU_RITO set ";
      +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".FLNOMSUP = "+i_cQueryTable+".FLNOMSUP";
              +" and "+i_cTable+".FLNOMFIL = "+i_cQueryTable+".FLNOMFIL";
              +" and "+i_cTable+".FLDTARIC = "+i_cQueryTable+".FLDTARIC";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLFLGACC ="+cp_NullLink(cp_ToStrODBC("S"),'FLU_RITO','FLFLGACC');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Verifico se esiste almeno un contenzioso con questo codice raggruppamento
    if this.w_ERROR=.T.
      * --- Raise
      i_Error=""
      return
    endif
    this.w_ORDINA = this.w_NEWORDINA
    ENDSCAN
    * --- Test ultima riga
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_ERROR=.F. AND NOT EMPTY(this.w_PISERIAL)
      if bTrsErr
        this.w_ERROR = .T.
        this.w_ERRATO = .T.
        this.w_ERRORMSG = AH_MSGFORMAT ("Errore nella creazione degli insoluti%0%1",message())
        this.w_ERRORE = MESSAGE()
        INSERT INTO CURSERR (MSG,ERRORE,NOMESUP,NOMEFIL,DATARIC) VALUES (this.w_ERRORMSG,this.w_ERRORE, this.w_NOMSUP,this.w_NOMFIL,this.w_DTARIC)
      endif
    endif
    if this.w_ERROR=.F. AND NOT EMPTY(this.w_COSERIAL)
      * --- Write into CON_TENZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CON_TENZ','CO__NOTE');
            +i_ccchkf ;
        +" where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_PISERIAL);
               )
      else
        update (i_cTable) set;
            CO__NOTE = this.w_NOTE;
            &i_ccchkf. ;
         where;
            COSERIAL = this.w_PISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore: inserimento del campo note contenzioso  ('+message()+')'
        return
      endif
      if bTrsErr
        this.w_ERROR = .T.
        this.w_ERRATO = .T.
        this.w_ERRORMSG = AH_MSGFORMAT ("Errore nella creazione degli insoluti")
        this.w_ERRORE = MESSAGE()
        INSERT INTO CURSERR (MSG,ERRORE,NOMESUP,NOMEFIL,DATARIC) VALUES (this.w_ERRORMSG,this.w_ERRORE, this.w_NOMSUP,this.w_NOMFIL,this.w_DTARIC)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NUMMOV>0
      this.w_MESS = ""
      do case
        case this.w_NUMMOV=1
          this.w_MESS = "Generato %1 movimento di contenzioso%0"
        case this.w_NUMMOV>1
          this.w_MESS = "Generati %1 movimenti di contenziosi%0"
      endcase
      Ah_ErrorMsg(this.w_MESS,48,"", ALLTRIM(STR(this.w_NUMMOV,6,0)), ALLTRIM(STR(this.w_NUMMOV1,6,0)), ALLTRIM(STR(this.w_NUMMOV2,6,0)))
    else
      ah_ErrorMsg("Non sono stati generati movimenti di mancato incasso o contenziosi",48)
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if NOT EMPTY(this.w_PISERIAL)
      if Not Empty(this.w_PCDESSUP)
        * --- Array elenco parametri per descrizioni di riga e testata
         
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PIDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(_Curs_PARTITE.PTCODCON) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]=alltrim(this.w_NOTE) 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(_Curs_PARTITE.PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(_Curs_PARTITE.PTDATSCA) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]="C"
        this.w_NOTE = CALDESPA(this.w_PCDESSUP,@ARPARAM,.t.)
      endif
      * --- Write into CON_TENZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TENZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TENZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TENZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CON_TENZ','CO__NOTE');
            +i_ccchkf ;
        +" where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_PISERIAL);
               )
      else
        update (i_cTable) set;
            CO__NOTE = this.w_NOTE;
            &i_ccchkf. ;
         where;
            COSERIAL = this.w_PISERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore: inserimento del campo note contenzioso ('+message()+')'
        return
      endif
      this.w_NOTE = ""
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PTSERRIF = _Curs_PARTITE.PTSERRIF
    this.w_PTORDRIF = _Curs_PARTITE.PTORDRIF
    this.w_PTNUMRIF = _Curs_PARTITE.PTNUMRIF
    * --- Select from GSSO_BFO
    do vq_exec with 'GSSO_BFO',this,'_Curs_GSSO_BFO','',.f.,.t.
    if used('_Curs_GSSO_BFO')
      select _Curs_GSSO_BFO
      locate for 1=1
      do while not(eof())
      this.w_NUMRAG = _Curs_GSSO_BFO.PTNUMDOC
      this.w_ALFRAG = _Curs_GSSO_BFO.PTALFDOC
      this.w_DATRAG = _Curs_GSSO_BFO.PTDATDOC
      this.w_NOTE = this.w_NOTE + ah_Msgformat("Doc. num. %1 del %2%0", ALLTRIM(STR(NVL(this.w_NUMRAG,0),15)) + IIF(NOT EMPTY(NVL(this.w_ALFRAG,Space(10))), " / "+Alltrim(this.w_ALFRAG), ""), DTOC(this.w_DATRAG) )
        select _Curs_GSSO_BFO
        continue
      enddo
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Param)
    this.w_Param=w_Param
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='CON_TENZ'
    this.cWorkTables[3]='CONDTENZ'
    this.cWorkTables[4]='FLU_RITO'
    this.cWorkTables[5]='PAR_TITE'
    this.cWorkTables[6]='CAU_REBA'
    this.cWorkTables[7]='FLU_ESRI'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='GES_PIAN'
    this.cWorkTables[10]='PAR_CONT'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_gsso3bfo')
      use in _Curs_gsso3bfo
    endif
    if used('_Curs_GSSO_BFO')
      use in _Curs_GSSO_BFO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Param"
endproc
