* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_kpi                                                        *
*              Zoom piano contenziosi                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_104]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-08                                                      *
* Last revis.: 2014-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_kpi",oParentObject))

* --- Class definition
define class tgsso_kpi as StdForm
  Top    = 24
  Left   = 26

  * --- Standard Properties
  Width  = 685
  Height = 363
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-23"
  HelpContextID=118909591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsso_kpi"
  cComment = "Zoom piano contenziosi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODVAL = space(3)
  w_TIPPAG = space(2)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_GPCODRAG = space(10)
  w_RIFDIS = space(10)
  w_SELEZI = space(1)
  w_COCOMPET = space(4)
  w_DATREG = ctod('  /  /  ')
  w_TIPEFF = space(1)
  w_CODCON = space(15)
  w_BANSEL = space(15)
  w_TIPSBF = space(15)
  w_CONASS = space(15)
  w_CONPAR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_kpiPag1","gsso_kpi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CONPAR = this.oPgFrm.Pages(1).oPag.CONPAR
    DoDefault()
    proc Destroy()
      this.w_CONPAR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODVAL=space(3)
      .w_TIPPAG=space(2)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_GPCODRAG=space(10)
      .w_RIFDIS=space(10)
      .w_SELEZI=space(1)
      .w_COCOMPET=space(4)
      .w_DATREG=ctod("  /  /  ")
      .w_TIPEFF=space(1)
      .w_CODCON=space(15)
      .w_BANSEL=space(15)
      .w_TIPSBF=space(15)
      .w_CONASS=space(15)
      .w_DATREG=oParentObject.w_DATREG
        .w_CODVAL = This.oparentobject.w_CODVAL
        .w_TIPPAG = This.oparentobject.w_TIPPAG
        .w_SCAINI = This.oparentobject.w_SCAINI
        .w_SCAFIN = This.oparentobject.w_SCAFIN
          .DoRTCalc(5,5,.f.)
        .w_RIFDIS = This.oparentobject.w_RIFDIS
      .oPgFrm.Page1.oPag.CONPAR.Calculate()
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_COCOMPET = This.oparentobject.w_GPCOMPET
          .DoRTCalc(9,9,.f.)
        .w_TIPEFF = This.oparentobject.w_GPTIPEFF
        .w_CODCON = This.oparentobject.w_CODCON
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_BANSEL = this.oparentobject .w_BANSEL
        .w_TIPSBF = this.oparentobject .w_TIPSBF
        .w_CONASS = this.oparentobject .w_CONASS
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATREG=.w_DATREG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CONPAR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(1,9,.t.)
            .w_TIPEFF = This.oparentobject.w_GPTIPEFF
            .w_CODCON = This.oparentobject.w_CODCON
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
            .w_BANSEL = this.oparentobject .w_BANSEL
            .w_TIPSBF = this.oparentobject .w_TIPSBF
            .w_CONASS = this.oparentobject .w_CONASS
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CONPAR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CONPAR.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_10.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsso_kpiPag1 as StdContainer
  Width  = 681
  height = 363
  stdWidth  = 681
  stdheight = 363
  resizeXpos=480
  resizeYpos=232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object CONPAR as cp_szoombox with uid="TUGLPHAHRT",left=8, top=8, width=670,height=303,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,cZoomFile="GSSO_GPI",cTable="PAR_TITE",bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",bQueryOnDblClick=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 239963674


  add object oBtn_1_8 as StdButton with uid="NUBHVPSUDL",left=620, top=315, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126227014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="SHRFOKIYMQ",left=568, top=315, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 118938342;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSSO_BPI(this.Parent.oContained,"PIAN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_10 as StdRadio with uid="MTSNNKRKYU",rtseq=7,rtrep=.f.,left=16, top=321, width=129,height=35;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.f., FntUnderline=.f., FntStrikeThru=.f.

    proc oSELEZI_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 100693286
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 100693286
      this.Buttons(2).Top=16
      this.SetAll("Width",127)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_10.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_10.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="LRULLCGZHV",left=1, top=365, width=238,height=23,;
    caption='GSSO_BPI(SELE)',;
   bGlobalFont=.t.,;
    prg="GSSO_BPI('SELE')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 217815087


  add object oObj_1_16 as cp_runprogram with uid="OMQBIWXEZL",left=243, top=366, width=172,height=23,;
    caption='GSSO_BPI(INIT)',;
   bGlobalFont=.t.,;
    prg="GSSO_BPI('INIT')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 233381423


  add object oObj_1_20 as cp_runprogram with uid="TFXPMXIIIE",left=431, top=363, width=172,height=23,;
    caption='GSSO_BPI(DONE)',;
   bGlobalFont=.t.,;
    prg="GSSO_BPI('DONE')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 217983279

  add object oStr_1_21 as StdString with uid="EDAIOSXGFJ",Visible=.t., Left=156, Top=321,;
    Alignment=0, Width=259, Height=17,;
    Caption="Spread su saggio di mora"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_kpi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
