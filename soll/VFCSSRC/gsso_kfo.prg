* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsso_kfo                                                        *
*              Gestione flussi operativi                                       *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2015-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsso_kfo",oParentObject))

* --- Class definition
define class tgsso_kfo as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 797
  Height = 454
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-16"
  HelpContextID=219572887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  CONTROPA_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  LOG_REBA_IDX = 0
  cPrg = "gsso_kfo"
  cComment = "Gestione flussi operativi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CAUINS = space(5)
  w_DATELA = ctod('  /  /  ')
  o_DATELA = ctod('  /  /  ')
  w_TIPELA = space(3)
  o_TIPELA = space(3)
  w_ERRATO = space(1)
  o_ERRATO = space(1)
  w_ESER = space(5)
  w_VALNAZ = space(3)
  w_CAONAZ = 0
  w_DECTOT = 0
  w_CONDCA = space(15)
  w_CONDCP = space(15)
  w_DIFCON = space(15)
  w_TIPELA1 = space(3)
  w_NUMRECZ = 0
  w_TOTIMPO = 0
  w_TOTSPESE = 0
  w_NOMSUP = space(20)
  w_NOMFIL = space(100)
  w_DTARIC = ctod('  /  /  ')
  w_RERNOMFIL = space(100)
  w_RERROWNUM = 0
  w_CPROWNUM = 0
  w_FRRIFPRO = space(12)
  w_FRNOMSUP = space(20)
  w_FRNOMFIL = space(100)
  w_FRDTARIC = ctod('  /  /  ')
  w_FRFIRRIG = 0
  w_FRLASRIG = 0
  w_GSSO_KFO = .NULL.
  w_GSSO_BFO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsso_kfoPag1","gsso_kfo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATELA_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_GSSO_KFO = this.oPgFrm.Pages(1).oPag.GSSO_KFO
    this.w_GSSO_BFO = this.oPgFrm.Pages(1).oPag.GSSO_BFO
    DoDefault()
    proc Destroy()
      this.w_GSSO_KFO = .NULL.
      this.w_GSSO_BFO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='LOG_REBA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CAUINS=space(5)
      .w_DATELA=ctod("  /  /  ")
      .w_TIPELA=space(3)
      .w_ERRATO=space(1)
      .w_ESER=space(5)
      .w_VALNAZ=space(3)
      .w_CAONAZ=0
      .w_DECTOT=0
      .w_CONDCA=space(15)
      .w_CONDCP=space(15)
      .w_DIFCON=space(15)
      .w_TIPELA1=space(3)
      .w_NUMRECZ=0
      .w_TOTIMPO=0
      .w_TOTSPESE=0
      .w_NOMSUP=space(20)
      .w_NOMFIL=space(100)
      .w_DTARIC=ctod("  /  /  ")
      .w_RERNOMFIL=space(100)
      .w_RERROWNUM=0
      .w_CPROWNUM=0
      .w_FRRIFPRO=space(12)
      .w_FRNOMSUP=space(20)
      .w_FRNOMFIL=space(100)
      .w_FRDTARIC=ctod("  /  /  ")
      .w_FRFIRRIG=0
      .w_FRLASRIG=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_DATELA = i_datsys
        .w_TIPELA = 'INS'
        .w_ERRATO = ' '
      .oPgFrm.Page1.oPag.GSSO_KFO.Calculate()
        .w_ESER = CALCESER(.w_DATELA)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ESER))
          .link_1_11('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_VALNAZ))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.GSSO_BFO.Calculate()
          .DoRTCalc(8,12,.f.)
        .w_TIPELA1 = 'INS'
          .DoRTCalc(14,16,.f.)
        .w_NOMSUP = .w_GSSO_KFO.getVar('NOMSUP')
        .w_NOMFIL = .w_GSSO_KFO.getVar('NOMFIL')
        .w_DTARIC = .w_GSSO_KFO.getVar('DTARIC')
        .w_RERNOMFIL = .w_FRNOMFIL
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_RERNOMFIL))
          .link_1_29('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_CPROWNUM = .w_GSSO_KFO.getVar('CPROWNUM')
        .w_FRRIFPRO = .w_GSSO_KFO.getVar('FRRIFPRO')
        .w_FRNOMSUP = .w_GSSO_KFO.getVar('NOMSUP')
        .w_FRNOMFIL = .w_GSSO_KFO.getVar('NOMFIL')
        .w_FRDTARIC = .w_GSSO_KFO.getVar('DTARIC')
        .w_FRFIRRIG = .w_GSSO_KFO.getVar('FRFIRRIG')
        .w_FRLASRIG = .w_GSSO_KFO.getVar('FRLASRIG')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.GSSO_KFO.Calculate()
        .DoRTCalc(2,5,.t.)
        if .o_DATELA<>.w_DATELA
            .w_ESER = CALCESER(.w_DATELA)
          .link_1_11('Full')
        endif
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.GSSO_BFO.Calculate()
        .DoRTCalc(8,16,.t.)
            .w_NOMSUP = .w_GSSO_KFO.getVar('NOMSUP')
            .w_NOMFIL = .w_GSSO_KFO.getVar('NOMFIL')
            .w_DTARIC = .w_GSSO_KFO.getVar('DTARIC')
            .w_RERNOMFIL = .w_FRNOMFIL
          .link_1_29('Full')
        .DoRTCalc(21,21,.t.)
            .w_CPROWNUM = .w_GSSO_KFO.getVar('CPROWNUM')
            .w_FRRIFPRO = .w_GSSO_KFO.getVar('FRRIFPRO')
        if .o_TIPELA<>.w_TIPELA.or. .o_ERRATO<>.w_ERRATO
          .Calculate_FORSVRGBKZ()
        endif
            .w_FRNOMSUP = .w_GSSO_KFO.getVar('NOMSUP')
            .w_FRNOMFIL = .w_GSSO_KFO.getVar('NOMFIL')
            .w_FRDTARIC = .w_GSSO_KFO.getVar('DTARIC')
            .w_FRFIRRIG = .w_GSSO_KFO.getVar('FRFIRRIG')
            .w_FRLASRIG = .w_GSSO_KFO.getVar('FRLASRIG')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.GSSO_KFO.Calculate()
        .oPgFrm.Page1.oPag.GSSO_BFO.Calculate()
    endwith
  return

  proc Calculate_FORSVRGBKZ()
    with this
          * --- Ricalcola zoom
          GSSO_BFO(this;
              ,'CAR';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_32.visible=!this.oPgFrm.Page1.oPag.oBtn_1_32.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.GSSO_KFO.Event(cEvent)
      .oPgFrm.Page1.oPag.GSSO_BFO.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCAUINS,COSADIFC,COSADIFN,CODIFCON";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCAUINS,COSADIFC,COSADIFN,CODIFCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CAUINS = NVL(_Link_.COCAUINS,space(5))
      this.w_CONDCA = NVL(_Link_.COSADIFC,space(15))
      this.w_CONDCP = NVL(_Link_.COSADIFN,space(15))
      this.w_DIFCON = NVL(_Link_.CODIFCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CAUINS = space(5)
      this.w_CONDCA = space(15)
      this.w_CONDCP = space(15)
      this.w_DIFCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(5))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(5)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_CAONAZ = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RERNOMFIL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOG_REBA_IDX,3]
    i_lTable = "LOG_REBA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2], .t., this.LOG_REBA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RERNOMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RERNOMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where ERNOMFIL="+cp_ToStrODBC(this.w_RERNOMFIL);
                   +" and ERNOMSUP="+cp_ToStrODBC(this.w_FRNOMSUP);
                   +" and ERDTARIC="+cp_ToStrODBC(this.w_FRDTARIC);
                   +" and ERNOMFIL="+cp_ToStrODBC(this.w_FRNOMFIL);
                   +" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ERNOMSUP',this.w_FRNOMSUP;
                       ,'ERDTARIC',this.w_FRDTARIC;
                       ,'ERNOMFIL',this.w_FRNOMFIL;
                       ,'CPROWNUM',this.w_CPROWNUM;
                       ,'ERNOMFIL',this.w_RERNOMFIL)
            select ERNOMSUP,ERDTARIC,ERNOMFIL,CPROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RERNOMFIL = NVL(_Link_.ERNOMFIL,space(100))
      this.w_RERROWNUM = NVL(_Link_.CPROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_RERNOMFIL = space(100)
      endif
      this.w_RERROWNUM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOG_REBA_IDX,2])+'\'+cp_ToStr(_Link_.ERNOMSUP,1)+'\'+cp_ToStr(_Link_.ERDTARIC,1)+'\'+cp_ToStr(_Link_.ERNOMFIL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)+'\'+cp_ToStr(_Link_.ERNOMFIL,1)
      cp_ShowWarn(i_cKey,this.LOG_REBA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RERNOMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATELA_1_3.value==this.w_DATELA)
      this.oPgFrm.Page1.oPag.oDATELA_1_3.value=this.w_DATELA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPELA_1_5.RadioValue()==this.w_TIPELA)
      this.oPgFrm.Page1.oPag.oTIPELA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oERRATO_1_6.RadioValue()==this.w_ERRATO)
      this.oPgFrm.Page1.oPag.oERRATO_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRECZ_1_20.value==this.w_NUMRECZ)
      this.oPgFrm.Page1.oPag.oNUMRECZ_1_20.value=this.w_NUMRECZ
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMPO_1_21.value==this.w_TOTIMPO)
      this.oPgFrm.Page1.oPag.oTOTIMPO_1_21.value=this.w_TOTIMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSPESE_1_22.value==this.w_TOTSPESE)
      this.oPgFrm.Page1.oPag.oTOTSPESE_1_22.value=this.w_TOTSPESE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATELA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATELA_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATELA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATELA = this.w_DATELA
    this.o_TIPELA = this.w_TIPELA
    this.o_ERRATO = this.w_ERRATO
    return

enddefine

* --- Define pages as container
define class tgsso_kfoPag1 as StdContainer
  Width  = 793
  height = 454
  stdWidth  = 793
  stdheight = 454
  resizeXpos=688
  resizeYpos=236
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATELA_1_3 as StdField with uid="KQTDFZHCDV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATELA", cQueryName = "DATELA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data elaborazione",;
    HelpContextID = 215945162,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=129, Top=6


  add object oTIPELA_1_5 as StdCombo with uid="HBWPOQBYSD",rtseq=4,rtrep=.f.,left=250,top=6,width=237,height=21, enabled=.f.;
    , HelpContextID = 215959242;
    , cFormVar="w_TIPELA",RowSource=""+"Insoluti o mancati pagamenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPELA_1_5.RadioValue()
    return(iif(this.value =1,'INS',;
    space(3)))
  endfunc
  func oTIPELA_1_5.GetRadio()
    this.Parent.oContained.w_TIPELA = this.RadioValue()
    return .t.
  endfunc

  func oTIPELA_1_5.SetRadio()
    this.Parent.oContained.w_TIPELA=trim(this.Parent.oContained.w_TIPELA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPELA=='INS',1,;
      0)
  endfunc

  add object oERRATO_1_6 as StdCheck with uid="KBKMZHJXKU",rtseq=5,rtrep=.f.,left=509, top=6, caption="Visualizza esiti con errori",;
    ToolTipText = "Visualizza esiti con errori",;
    HelpContextID = 241376954,;
    cFormVar="w_ERRATO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oERRATO_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oERRATO_1_6.GetRadio()
    this.Parent.oContained.w_ERRATO = this.RadioValue()
    return .t.
  endfunc

  func oERRATO_1_6.SetRadio()
    this.Parent.oContained.w_ERRATO=trim(this.Parent.oContained.w_ERRATO)
    this.value = ;
      iif(this.Parent.oContained.w_ERRATO=='S',1,;
      0)
  endfunc


  add object oBtn_1_7 as StdButton with uid="PSTUCDPWKC",left=735, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la query";
    , HelpContextID = 140375786;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSSO_BFO(this.Parent.oContained,"CAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="NENFZDFCQL",left=741, top=404, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226352106;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object GSSO_KFO as cp_zoombox with uid="QGFJSLJYIQ",left=6, top=56, width=785,height=343,;
    caption='GSSO_KFO',;
   bGlobalFont=.t.,;
    cTable="FLU_RITO",cZoomFile="GSSO_KFO",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.t.,bNoZoomGridShape=.f.,;
    cEvent = "CaricaZoom",;
    nPag=1;
    , HelpContextID = 240841397


  add object GSSO_BFO as cp_runprogram with uid="FZJIUDMZZM",left=2, top=462, width=230,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSO_BFO('CAR')",;
    cEvent = "Init, Riesegui",;
    nPag=1;
    , HelpContextID = 139300378

  add object oNUMRECZ_1_20 as StdField with uid="MJZTGUOOQH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NUMRECZ", cQueryName = "NUMRECZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero record",;
    HelpContextID = 79533270,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=404

  add object oTOTIMPO_1_21 as StdField with uid="SYBTTIZONM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TOTIMPO", cQueryName = "TOTIMPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo",;
    HelpContextID = 231407818,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=453, Top=404, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oTOTSPESE_1_22 as StdField with uid="OYTJYHWWAS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_TOTSPESE", cQueryName = "TOTSPESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale spese",;
    HelpContextID = 143720581,;
   bGlobalFont=.t.,;
    Height=21, Width=91, Left=595, Top=404, cSayPict="v_PV(16)", cGetPict="v_GV(16)"


  add object oBtn_1_25 as StdButton with uid="ATWJDENOIZ",left=195, top=404, width=48,height=45,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare gli esiti ricevuti";
    , HelpContextID = 157595370;
    , Caption='\<Rig.es.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSSO_MER', 'FRNOMSUP', .w_NOMSUP, 'FRDTARIC', .w_DTARIC, 'FRNOMFIL', .w_NOMFIL,'CPROWNUM',.w_CPROWNUM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOMSUP))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="JKDXEKYIKS",left=245, top=404, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log";
    , HelpContextID = 192853874;
    , Caption='Riga \<log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        do GSSO_BE1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (nvl(.w_RERROWNUM,0)<>0)
      endwith
    endif
  endfunc

  func oBtn_1_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (nvl(.w_RERROWNUM,0)=0)
     endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="YYGMCZXSWL",left=295, top=404, width=48,height=45,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il promemoria contabile";
    , HelpContextID = 226766300;
    , Caption='\<Pro.con.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        do GSSO_BE2 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOMSUP) and NOT EMPTY(.w_FRRIFPRO))
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="OSXGXXPXXC",left=345, top=404, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Premere eseguire la stampa";
    , HelpContextID = 226352106;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      vx_exec("..\SOLL\EXE\QUERY\GSSO1KFO.VQR, ..\SOLL\EXE\QUERY\GSSO_KFO.FRX",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NUMRECZ<>0)
      endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="VRLQWYRDWM",left=690, top=404, width=48,height=45,;
    CpPicture="bmp\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare i movimenti";
    , HelpContextID = 61051174;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSSO_BFO(this.Parent.oContained,"SAL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ERRATO<>'S')
      endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="WKISHMVLPJ",Visible=.t., Left=10, Top=6,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ZVFWRENINY",Visible=.t., Left=217, Top=8,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="OBBUZXRNPY",Visible=.t., Left=6, Top=408,;
    Alignment=1, Width=107, Height=18,;
    Caption="Numero record:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MOOQHLLSJZ",Visible=.t., Left=397, Top=408,;
    Alignment=1, Width=55, Height=18,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsso_kfo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
