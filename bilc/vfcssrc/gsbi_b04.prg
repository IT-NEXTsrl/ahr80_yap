* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b04                                                        *
*              Elabora coge cespiti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_61]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2000-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b04",oParentObject)
return(i_retval)

define class tgsbi_b04 as StdBatch
  * --- Local variables
  w_CAUCES = space(5)
  w_APPO1 = 0
  w_ERRORE = 0
  w_IMPRIG = 0
  w_MESS = space(10)
  w_COMPET = space(4)
  w_APPVAL = 0
  w_CODVAL = space(3)
  w_APPSEZ = space(1)
  w_CAOVAL = 0
  w_CODICE = space(15)
  w_INIESE = ctod("  /  /  ")
  w_IMPDAR = 0
  w_FINESE = ctod("  /  /  ")
  w_IMPAVE = 0
  w_ACOD = space(3)
  w_DATREG = ctod("  /  /  ")
  w_CDAR = space(3)
  w_CODGRU = space(15)
  w_CAVE = space(3)
  w_CODCAT = space(15)
  w_CODPERI = space(0)
  w_PETIPPER = space(1)
  w_PEDATINI = ctod("  /  /  ")
  w_PEDATFIN = ctod("  /  /  ")
  w_NUMGIO = 0
  w_NUMFRA = 0
  w_PERIODO = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  VALUTE_idx=0
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relative ai Cespiti COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTCE001
    this.w_CODPERI = this.oParentObject.oParentObject.w_CODPER
    * --- Read from PER_ELAB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PETIPPER,PEDATINI,PEDATFIN"+;
        " from "+i_cTable+" PER_ELAB where ";
            +"PECODICE = "+cp_ToStrODBC(this.w_CODPERI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PETIPPER,PEDATINI,PEDATFIN;
        from (i_cTable) where;
            PECODICE = this.w_CODPERI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PETIPPER = NVL(cp_ToDate(_read_.PETIPPER),cp_NullValue(_read_.PETIPPER))
      this.w_PEDATINI = NVL(cp_ToDate(_read_.PEDATINI),cp_NullValue(_read_.PEDATINI))
      this.w_PEDATFIN = NVL(cp_ToDate(_read_.PEDATFIN),cp_NullValue(_read_.PEDATFIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NUMGIO = (this.w_PEDATFIN+1) - this.w_PEDATINI
    this.w_INIESE = g_INIESE
    this.w_FINESE = g_FINESE
    this.w_ERRORE = 0
    this.w_MESS = " "
    DIMENSION IMP[24], GPC[16]
    FOR L_i = 1 TO 24
    IMP[L_i] = 0
    ENDFOR
    if USED("EXTCE001")
      * --- Crea il Temporaneo di Elaborazione Primanota
      CREATE CURSOR EXTCG001 ; 
 (TIPCON C(1), CODICE C(15), INICOM D(8), FINCOM D(8), ; 
 VALNAZ C(3), DATREG D(8), IMPDAR N(18,4), IMPAVE N(18,4))
      * --- Inizio Aggiornamento vero e proprio
      ah_Msg("Elaborazione movimenti cespite da generare...",.T.)
      SELECT EXTCE001
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MCSERIAL," ")) AND NOT EMPTY(NVL(MCCODCAU," "))
      this.w_CAUCES = MCCODCAU
      this.w_DATREG = NVL(MCDATREG, i_datsys)
      this.w_COMPET = NVL(MCCOMPET, SPACE(4))
      this.w_CODVAL = NVL(MCCODVAL, g_PERVAL)
      this.w_CAOVAL = NVL(MCCAOVAL, g_CAOVAL)
      this.w_CODGRU = NVL(GPCODICE, SPACE(15))
      this.w_CODCAT = NVL(CCCODICE, SPACE(15))
      this.w_NUMFRA = NVL(PANUMFRA,0)
      if NOT EMPTY(this.w_CODGRU)
        GPC[1] = NVL(GPCONC01, SPACE(15))
        GPC[2] = NVL(GPCONC02, SPACE(15))
        GPC[3] = NVL(GPCONC03, SPACE(15))
        GPC[4] = NVL(GPCONC04, SPACE(15))
        GPC[5] = NVL(GPCONC05, SPACE(15))
        GPC[6] = NVL(GPCONC06, SPACE(15))
        GPC[7] = NVL(GPCONC07, SPACE(15))
        GPC[8] = NVL(GPCONC08, SPACE(15))
        GPC[9] = NVL(GPCONC09, SPACE(15))
        GPC[10] = NVL(GPCONC10, SPACE(15))
        GPC[11] = NVL(GPCONC11, SPACE(15))
        GPC[12] = NVL(GPCONC12, SPACE(15))
        GPC[13] = NVL(GPCONC13, SPACE(15))
        GPC[14] = NVL(GPCONC14, SPACE(15))
        GPC[15] = NVL(GPCONC15, SPACE(15))
        GPC[16] = NVL(GPCONC16, SPACE(15))
        IMP[1] = NVL(MCIMPA01, 0)
        IMP[2] = NVL(MCIMPA02, 0)
        IMP[3] = NVL(MCIMPA03, 0)
        IMP[4] = NVL(MCIMPA04, 0)
        IMP[5] = NVL(MCIMPA05, 0)
        IMP[6] = NVL(MCIMPA06, 0)
        IMP[7] = NVL(MCIMPA07, 0)
        IMP[8] = NVL(MCIMPA08, 0)
        IMP[9] = NVL(MCIMPA09, 0)
        IMP[10] = NVL(MCIMPA10, 0)
        IMP[11] = NVL(MCIMPA11, 0)
        IMP[12] = NVL(MCIMPA12, 0)
        IMP[13] = NVL(MCIMPA13, 0)
        IMP[14] = NVL(MCIMPA14, 0)
        IMP[15] = NVL(MCIMPA15, 0)
        IMP[16] = NVL(MCIMPA16, 0)
        IMP[18] = NVL(MCIMPA18, 0)
        IMP[19] = NVL(MCIMPA19, 0)
        IMP[20] = NVL(MCIMPA20, 0)
        IMP[21] = NVL(MCIMPA21, 0)
        IMP[22] = NVL(MCIMPA22, 0)
        IMP[23] = NVL(MCIMPA23, 0)
        IMP[24] = NVL(MCIMPA24, 0)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_ERRORE = 1
      endif
      SELECT EXTCE001
      ENDSCAN
      * --- Elimina il Cursore
      SELECT EXTCE001
      USE
    endif
    do case
      case this.w_ERRORE=1
        this.w_MESS = "Elaborazione cespiti%0ATTENZIONE:%0Alcuni articoli non sono associati a categorie omogenee"
        ah_ErrorMsg(this.w_MESS,,"")
      case this.w_ERRORE=2
        this.w_oMess.Ah_ErrorMsg()     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla Sui Modelli Contabili
    * --- Select from GSBI_B04
    do vq_exec with 'GSBI_B04',this,'_Curs_GSBI_B04','',.f.,.t.
    if used('_Curs_GSBI_B04')
      select _Curs_GSBI_B04
      locate for 1=1
      do while not(eof())
      this.w_ACOD = NVL(_Curs_GSBI_B04.MCCODICE,"   ")
      this.w_CDAR = NVL(_Curs_GSBI_B04.MCCONDAR, "   ")
      this.w_CAVE = NVL(_Curs_GSBI_B04.MCCONAVE, "   ")
      if NOT EMPTY(this.w_ACOD) AND LEN(this.w_ACOD)=3
        this.w_APPO1 = VAL(RIGHT(this.w_ACOD,2))
        if this.w_APPO1>0 AND this.w_APPO1<25
          if IMP[this.w_APPO1]<>0
            * --- Importo da Scrivere
            this.w_IMPRIG = IMP[this.w_APPO1]
            IMP[this.w_APPO1] = 0
            * --- Cerca Conto DARE
            if NOT EMPTY(this.w_CDAR) AND LEN(this.w_CDAR)=3
              this.w_APPO1 = VAL(RIGHT(this.w_CDAR,2))
              if this.w_APPO1>0 AND this.w_APPO1<17
                if NOT EMPTY(GPC[this.w_APPO1])
                  this.w_APPVAL = this.w_IMPRIG
                  this.w_APPSEZ = "D"
                  this.w_CODICE = GPC[this.w_APPO1]
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
            * --- Cerca Conto AVERE
            if NOT EMPTY(this.w_CAVE) AND LEN(this.w_CAVE)=3
              this.w_APPO1 = VAL(RIGHT(this.w_CAVE,2))
              if this.w_APPO1>0 AND this.w_APPO1<17
                if NOT EMPTY(GPC[this.w_APPO1])
                  this.w_APPVAL = this.w_IMPRIG
                  this.w_APPSEZ = "A"
                  this.w_CODICE = GPC[this.w_APPO1]
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
        select _Curs_GSBI_B04
        continue
      enddo
      use
    endif
    * --- Verifica 'Orfani'
    if this.w_ERRORE=0
      this.w_oMess=createobject("Ah_Message")
      this.w_oMess.AddMsgPartNL("Elaborazione cespiti%0")     
      FOR L_i = 1 TO 24
      if IMP[L_i]<>0
        this.w_ERRORE = 2
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare modelli contabili causale cespite: %1")
        this.w_oPart.AddParam(this.w_CAUCES)     
        this.w_oPart.AddMsgPartNL("Riferimento a: %1 non definito!")     
        this.w_oPart.AddParam(RIGHT("00"+ALLTRIM(STR(L_i)), 2))     
      endif
      ENDFOR
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Temporaneo di Elaborazione Primanota
    if this.w_CODVAL<>g_PERVAL
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = cp_ROUND(VAL2MON(this.w_APPVAL,this.w_CAOVAL,g_CAOVAL,this.w_DATREG,g_PERVAL), g_PERPVL)
    endif
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    if this.w_APPSEZ="D"
      * --- calcola importo in base al periodo d'ammortamento
      this.w_IMPDAR = ABS(this.w_APPVAL)
      this.w_IMPAVE = 0
      do case
        case this.w_PETIPPER="M"
          this.w_PERIODO = 1
        case this.w_PETIPPER="T"
          this.w_PERIODO = 3
        case this.w_PETIPPER="Q"
          this.w_PERIODO = 4
        case this.w_PETIPPER="S"
          this.w_PERIODO = 6
        case this.w_PETIPPER="A"
          this.w_PERIODO = 12
        case this.w_PETIPPER="G"
          this.w_PERIODO = cp_ROUND((this.w_NUMGIO/30),0)
      endcase
      this.w_IMPDAR = (this.w_IMPDAR * this.w_PERIODO)/this.w_NUMFRA
    else
      this.w_IMPDAR = 0
      this.w_IMPAVE = ABS(this.w_APPVAL)
      do case
        case this.w_PETIPPER="M"
          this.w_PERIODO = 1
        case this.w_PETIPPER="T"
          this.w_PERIODO = 3
        case this.w_PETIPPER="Q"
          this.w_PERIODO = 4
        case this.w_PETIPPER="S"
          this.w_PERIODO = 6
        case this.w_PETIPPER="A"
          this.w_PERIODO = 12
        case this.w_PETIPPER="G"
          this.w_PERIODO = cp_ROUND((this.w_NUMGIO/30),0)
      endcase
      this.w_IMPAVE = (this.w_IMPAVE * this.w_PERIODO)/this.w_NUMFRA
    endif
    INSERT INTO EXTCG001 (TIPCON, CODICE, INICOM, FINCOM, ;
    VALNAZ, DATREG, IMPDAR, IMPAVE) ;
    VALUES ("G", this.w_CODICE, this.w_INIESE, this.w_FINESE, ;
    g_PERVAL, this.w_DATREG, this.w_IMPDAR, this.w_IMPAVE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PER_ELAB'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSBI_B04')
      use in _Curs_GSBI_B04
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
