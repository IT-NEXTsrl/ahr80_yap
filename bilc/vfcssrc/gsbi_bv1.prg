* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bv1                                                        *
*              Assegna valori a gsbi_mda                                       *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_36]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-23                                                      *
* Last revis.: 2000-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bv1",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsbi_bv1 as StdBatch
  * --- Local variables
  w_PARAM = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegno valore a variabile del figlio per tipo voce (GSBI_AVA)
    do case
      case this.w_PARAM="CHG"
        SELECT (this.oParentObject.GSBI_MDA.cTrsName)
        ZAP
        this.oParentObject.GSBI_MDA.BlankRec()
        this.oParentObject.GSBI_MDA.w_TIPVOC=this.oParentObject.w_VRTIPVOC
        UPDATE (this.oParentObject.GSBI_MDA.cTrsName) SET ; 
 t_DVTIPRIG=this.oParentObject.w_VRTIPVOC
        this.oParentObject.GSBI_MDA.SetControlsValue()
        this.oParentObject.GSBI_MDA.mEnableControls()
        this.oParentObject.GSBI_MDA.Refresh()
      case this.w_PARAM="LOAD"
        this.oParentObject.GSBI_MDA.w_TIPVOC=this.oParentObject.w_VRTIPVOC
    endcase
  endproc


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
