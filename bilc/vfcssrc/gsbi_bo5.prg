* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bo5                                                        *
*              Zoom on zoom strut.indici                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_37]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bo5",oParentObject)
return(i_retval)

define class tgsbi_bo5 as StdBatch
  * --- Local variables
  w_IBTIPIND = space(1)
  w_PROG = .NULL.
  w_INDICI = space(1)
  w_TIPO = space(1)
  w_DXBTN = .f.
  w_CODICE = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH PER LANCIARE LO ZOOM ON ZOOM DALLE STRUTTURE INDICI
    * --- Chiamato da GSBI_SIC
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_IBTIPIND = Nvl(g_oMenu.oKey(1,3),"")
      this.w_CODICE = Nvl(g_oMenu.oKey(2,3),"")
    else
      * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
      if i_curform=NULL
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_IBTIPIND = &cCurs..TITIPIND
      if EMPTY(this.w_IBTIPIND) OR NOT this.w_IBTIPIND $ "AG"
        * --- Se non � stato riconosciuto in precedenza
        * --- legge il tipo di conto partendo dalla espressione di Where della select..
        cInit=i_curform.z1.o_initsel
        if TYPE ("cInit") ="C"
          cInit=UPPER(cInit)
          this.w_INDICI = AT("TITIPIND=", UPPER(cInit))
          if this.w_INDICI<>0
            this.w_INDICI = this.w_INDICI+10
            if LEN(ALLTRIM(cInit))>=this.w_INDICI
              this.w_TIPO = SUBSTR(cInit, this.w_INDICI,1)
              this.w_IBTIPIND = IIF(this.w_TIPO $ "AG", this.w_TIPO, this.w_IBTIPIND)
            endif
          endif
        endif
      endif
    endif
    this.w_PROG = GSBI_MSI(this.w_IBTIPIND)
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_TICODICE = this.w_CODICE
        this.w_PROG.w_TITIPIND = this.w_IBTIPIND
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
