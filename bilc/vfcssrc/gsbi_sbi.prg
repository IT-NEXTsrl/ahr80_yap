* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sbi                                                        *
*              Stampa bilancio contabile                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_55]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2013-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sbi",oParentObject))

* --- Class definition
define class tgsbi_sbi as StdForm
  Top    = 38
  Left   = 29

  * --- Standard Properties
  Width  = 646
  Height = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-09"
  HelpContextID=107611753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  STR_ANAC_IDX = 0
  PER_ELAB_IDX = 0
  VALUTE_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsbi_sbi"
  cComment = "Stampa bilancio contabile"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ESER = space(4)
  o_ESER = space(4)
  w_INESER = ctod('  /  /  ')
  w_FINESER = ctod('  /  /  ')
  w_STATO = space(1)
  w_STRUTTUR = space(15)
  w_MOVIMENT = space(1)
  w_PERIODO = space(15)
  w_TIPBILAN = space(1)
  w_COMMESSA = space(15)
  w_DESCRI = space(40)
  w_DESCPER = space(35)
  w_VALUTA = space(3)
  w_NUMERO = 0
  w_ESERCI = space(4)
  w_DATABIL = ctod('  /  /  ')
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_DESCRIBI = space(35)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_MONETE = space(1)
  o_MONETE = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_SIMVAL = space(5)
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_CAMBGIOR = 0
  w_CAMVAL = 0
  o_CAMVAL = 0
  w_DECIMI = 0
  w_TIPOSTAM = space(1)
  w_RIGHESI = space(1)
  w_DTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sbiPag1","gsbi_sbi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESER_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='STR_ANAC'
    this.cWorkTables[3]='PER_ELAB'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='AZIENDA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ESER=space(4)
      .w_INESER=ctod("  /  /  ")
      .w_FINESER=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_STRUTTUR=space(15)
      .w_MOVIMENT=space(1)
      .w_PERIODO=space(15)
      .w_TIPBILAN=space(1)
      .w_COMMESSA=space(15)
      .w_DESCRI=space(40)
      .w_DESCPER=space(35)
      .w_VALUTA=space(3)
      .w_NUMERO=0
      .w_ESERCI=space(4)
      .w_DATABIL=ctod("  /  /  ")
      .w_VALAPP=space(3)
      .w_DESCRIBI=space(35)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_MONETE=space(1)
      .w_DIVISA=space(3)
      .w_SIMVAL=space(5)
      .w_EXTRAEUR=0
      .w_CAMBGIOR=0
      .w_CAMVAL=0
      .w_DECIMI=0
      .w_TIPOSTAM=space(1)
      .w_RIGHESI=space(1)
      .w_DTOBSO=ctod("  /  /  ")
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_ESER = g_codese
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ESER))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_STATO = 'S'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_6('Full')
        endif
        .w_MOVIMENT = 'E'
        .w_PERIODO = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PERIODO))
          .link_1_8('Full')
        endif
        .w_TIPBILAN = 'G'
        .w_COMMESSA = space(15)
          .DoRTCalc(11,12,.f.)
        .w_VALUTA = g_perval
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_ESERCI))
          .link_1_25('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(16,20,.f.)
        .w_MONETE = 'c'
        .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_DIVISA)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_DIVISA))
          .link_1_39('Full')
        endif
          .DoRTCalc(23,24,.f.)
        .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        .w_CAMVAL = GETCAM( .w_DIVISA , i_DATSYS )
          .DoRTCalc(27,27,.f.)
        .w_TIPOSTAM = 'S'
          .DoRTCalc(29,29,.f.)
        .w_DTOBSO = i_datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_ESER<>.w_ESER
            .w_PERIODO = SPACE(15)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,14,.t.)
          .link_1_25('Full')
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(16,21,.t.)
        if .o_MONETE<>.w_MONETE.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_DIVISA)
          .link_1_39('Full')
        endif
        .DoRTCalc(23,24,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        endif
        if .o_EXTRAEUR<>.w_EXTRAEUR
            .w_CAMVAL = GETCAM( .w_DIVISA , i_DATSYS )
        endif
        if .o_CAMVAL<>.w_CAMVAL
          .Calculate_TORYRCHNSJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return

  proc Calculate_TORYRCHNSJ()
    with this
          * --- Inserisci cambio giornaliero
     if not empty(.w_DIVISA) and .w_DIVISA<>g_perval and .w_CAMBGIOR<>.w_CAMVAL
          GSAR_BIC(this;
              ,.w_DIVISA;
              ,.w_CAMVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDIVISA_1_39.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_44.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDIVISA_1_39.visible=!this.oPgFrm.Page1.oPag.oDIVISA_1_39.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_40.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_44.visible=!this.oPgFrm.Page1.oPag.oCAMVAL_1_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_2'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_INESER = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESER = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_INESER = ctod("  /  /  ")
      this.w_FINESER = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSC',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_6'),i_cWhere,'GSBI_MSC',"Strutture di bilancio cont.",'GSBI_AGB.STR_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE,PEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_8'),i_cWhere,'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
      this.w_DESCPER = NVL(_Link_.PEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
      this.w_DESCPER = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_39'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_DECIMI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_EXTRAEUR = 0
      this.w_DECIMI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESER_1_2.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_2.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oINESER_1_3.value==this.w_INESER)
      this.oPgFrm.Page1.oPag.oINESER_1_3.value=this.w_INESER
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESER_1_4.value==this.w_FINESER)
      this.oPgFrm.Page1.oPag.oFINESER_1_4.value=this.w_FINESER
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_5.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_6.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_6.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_7.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_8.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_8.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPER_1_15.value==this.w_DESCPER)
      this.oPgFrm.Page1.oPag.oDESCPER_1_15.value=this.w_DESCPER
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_21.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_21.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_25.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_25.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATABIL_1_26.value==this.w_DATABIL)
      this.oPgFrm.Page1.oPag.oDATABIL_1_26.value=this.w_DATABIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIBI_1_29.value==this.w_DESCRIBI)
      this.oPgFrm.Page1.oPag.oDESCRIBI_1_29.value=this.w_DESCRIBI
    endif
    if not(this.oPgFrm.Page1.oPag.oMONETE_1_37.RadioValue()==this.w_MONETE)
      this.oPgFrm.Page1.oPag.oMONETE_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_39.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_39.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_44.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_44.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAM_1_46.RadioValue()==this.w_TIPOSTAM)
      this.oPgFrm.Page1.oPag.oTIPOSTAM_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGHESI_1_50.RadioValue()==this.w_RIGHESI)
      this.oPgFrm.Page1.oPag.oRIGHESI_1_50.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DIVISA))  and not(.w_monete='c')  and (.w_monete<>'c')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_39.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and not(EMPTY(.w_DIVISA) OR .w_monete='c')  and (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 or ( FASETRAN(i_DATSYS)=1 and .w_EXTRAEUR=0 )))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_44.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    this.o_VALAPP = this.w_VALAPP
    this.o_MONETE = this.w_MONETE
    this.o_DIVISA = this.w_DIVISA
    this.o_EXTRAEUR = this.w_EXTRAEUR
    this.o_CAMVAL = this.w_CAMVAL
    return

enddefine

* --- Define pages as container
define class tgsbi_sbiPag1 as StdContainer
  Width  = 642
  height = 300
  stdWidth  = 642
  stdheight = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESER_1_2 as StdField with uid="DVJDDVKKMU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 101932730,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=100, Top=30, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oINESER_1_3 as StdField with uid="FQJWSUKLTZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_INESER", cQueryName = "INESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 4037766,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=312, Top=30

  add object oFINESER_1_4 as StdField with uid="ZITKAOGQAY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FINESER", cQueryName = "FINESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 68167510,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=528, Top=30


  add object oSTATO_1_5 as StdCombo with uid="TQERIMFGOM",rtseq=5,rtrep=.f.,left=100,top=66,width=114,height=21;
    , ToolTipText = "Stato bilancio selezionato";
    , HelpContextID = 18980058;
    , cFormVar="w_STATO",RowSource=""+"Confermato,"+"Da confermare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSTATO_1_5.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_5.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      0))
  endfunc

  add object oSTRUTTUR_1_6 as StdField with uid="YIAHHLODVT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 214928520,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=279, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", cZoomOnZoom="GSBI_MSC", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSC',"Strutture di bilancio cont.",'GSBI_AGB.STR_ANAC_VZM',this.parent.oContained
  endproc
  proc oSTRUTTUR_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR
     i_obj.ecpSave()
  endproc


  add object oMOVIMENT_1_7 as StdCombo with uid="NKZSCOKZYP",rtseq=7,rtrep=.f.,left=100,top=100,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 62172698;
    , cFormVar="w_MOVIMENT",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_7.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oMOVIMENT_1_7.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_7.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='E',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      0))
  endfunc

  add object oPERIODO_1_8 as StdField with uid="VNMGWURUOP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 47473654,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=279, Top=100, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_14 as StdField with uid="DXKUNAXXPP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134116150,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=398, Top=66, InputMask=replicate('X',40)

  add object oDESCPER_1_15 as StdField with uid="HHNJQZQMOD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCPER", cQueryName = "DESCPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 64910134,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=398, Top=100, InputMask=replicate('X',35)


  add object oBtn_1_18 as StdButton with uid="XEGAPOXFFB",left=86, top=145, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 107410730;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      do GSBI_KBI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_21 as StdField with uid="ODAWZBRCSI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 234890198,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=62, Top=180

  add object oESERCI_1_25 as StdField with uid="AOSBXIYGEN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 119316806,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=186, Top=180, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDATABIL_1_26 as StdField with uid="ESUJTWTXTK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATABIL", cQueryName = "DATABIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 117210934,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=271, Top=180

  add object oDESCRIBI_1_29 as StdField with uid="ZWXAEQGGCL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCRIBI", cQueryName = "DESCRIBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134116223,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=398, Top=180, InputMask=replicate('X',35)


  add object oObj_1_31 as cp_runprogram with uid="VZEFFXGINO",left=669, top=206, width=158,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BBI",;
    cEvent = "w_STATO Changed,w_STRUTTUR Changed,w_MOVIMENT Changed,w_PERIODO Changed,w_ESER Changed",;
    nPag=1;
    , HelpContextID = 70385894


  add object oBtn_1_32 as StdButton with uid="OXPTYCNQOO",left=535, top=249, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 107583002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        do GSBI_BB3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO))
      endwith
    endif
  endfunc


  add object oMONETE_1_37 as StdCombo with uid="KKOSWNFWHY",rtseq=21,rtrep=.f.,left=143,top=216,width=122,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 69217734;
    , cFormVar="w_MONETE",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMONETE_1_37.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func oMONETE_1_37.GetRadio()
    this.Parent.oContained.w_MONETE = this.RadioValue()
    return .t.
  endfunc

  func oMONETE_1_37.SetRadio()
    this.Parent.oContained.w_MONETE=trim(this.Parent.oContained.w_MONETE)
    this.value = ;
      iif(this.Parent.oContained.w_MONETE=='c',1,;
      iif(this.Parent.oContained.w_MONETE=='a',2,;
      0))
  endfunc

  add object oDIVISA_1_39 as StdField with uid="QBFXGCOQQD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 1353526,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=325, Top=216, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete<>'c')
    endwith
   endif
  endfunc

  func oDIVISA_1_39.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  func oDIVISA_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc

  add object oSIMVAL_1_40 as StdField with uid="KYTLMTBRDO",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 167843878,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=375, Top=216, InputMask=replicate('X',5)

  func oSIMVAL_1_40.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  add object oCAMVAL_1_44 as StdField with uid="DYRJOEWDBP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 167841574,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=506, Top=216, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 or ( FASETRAN(i_DATSYS)=1 and .w_EXTRAEUR=0 )))
    endwith
   endif
  endfunc

  func oCAMVAL_1_44.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_monete='c')
    endwith
  endfunc


  add object oTIPOSTAM_1_46 as StdCombo with uid="YVEYHBGRQZ",rtseq=28,rtrep=.f.,left=143,top=250,width=142,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 52054147;
    , cFormVar="w_TIPOSTAM",RowSource=""+"Stampa su Excel,"+"Stampa standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTAM_1_46.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPOSTAM_1_46.GetRadio()
    this.Parent.oContained.w_TIPOSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAM_1_46.SetRadio()
    this.Parent.oContained.w_TIPOSTAM=trim(this.Parent.oContained.w_TIPOSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAM=='E',1,;
      iif(this.Parent.oContained.w_TIPOSTAM=='S',2,;
      0))
  endfunc

  add object oRIGHESI_1_50 as StdCheck with uid="HDYGHPYOYO",rtseq=29,rtrep=.f.,left=326, top=248, caption="Righe significative",;
    ToolTipText = "Se attivo: stampa solo righe valorizzate",;
    HelpContextID = 248334314,;
    cFormVar="w_RIGHESI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRIGHESI_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRIGHESI_1_50.GetRadio()
    this.Parent.oContained.w_RIGHESI = this.RadioValue()
    return .t.
  endfunc

  func oRIGHESI_1_50.SetRadio()
    this.Parent.oContained.w_RIGHESI=trim(this.Parent.oContained.w_RIGHESI)
    this.value = ;
      iif(this.Parent.oContained.w_RIGHESI=='S',1,;
      0)
  endfunc


  add object oBtn_1_52 as StdButton with uid="EFZTAPUEYA",left=587, top=249, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 100294330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_11 as StdString with uid="VRJULVDIHD",Visible=.t., Left=32, Top=66,;
    Alignment=1, Width=65, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="WSYPDMMVGA",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YFWSKGCNHJ",Visible=.t., Left=216, Top=66,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="HVOHSEGPIE",Visible=.t., Left=220, Top=100,;
    Alignment=1, Width=57, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IUTVHQUYGP",Visible=.t., Left=11, Top=147,;
    Alignment=0, Width=72, Height=15,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="JHSIKHWAGX",Visible=.t., Left=11, Top=4,;
    Alignment=0, Width=109, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="MDTPGMTJPW",Visible=.t., Left=6, Top=180,;
    Alignment=1, Width=55, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="WMQKCDGBVE",Visible=.t., Left=117, Top=180,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FXAFNORJPW",Visible=.t., Left=239, Top=180,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="CRUZITHWIH",Visible=.t., Left=346, Top=180,;
    Alignment=1, Width=51, Height=15,;
    Caption="Descr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="DECFLNEFPR",Visible=.t., Left=3, Top=250,;
    Alignment=1, Width=136, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="YUCTBWKUQT",Visible=.t., Left=5, Top=216,;
    Alignment=1, Width=134, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="WRYVZYYYUI",Visible=.t., Left=269, Top=216,;
    Alignment=1, Width=53, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="ERLSGOYGQJ",Visible=.t., Left=437, Top=216,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_monete='c')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="HGHWIGNHUM",Visible=.t., Left=30, Top=30,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="PPWCTXPCHH",Visible=.t., Left=159, Top=30,;
    Alignment=1, Width=151, Height=15,;
    Caption="Inizio esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="RBMKIWJTNX",Visible=.t., Left=419, Top=30,;
    Alignment=1, Width=107, Height=15,;
    Caption="Fine esercizio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_23 as StdBox with uid="NJCTTVUKEX",left=1, top=136, width=617,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sbi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
