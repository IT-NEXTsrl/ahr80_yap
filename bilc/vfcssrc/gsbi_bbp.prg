* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bbp                                                        *
*              Importa x bilancio previsionale                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_126]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bbp",oParentObject)
return(i_retval)

define class tgsbi_bbp as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa dati per generazione bilancio previsionale
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    ah_Msg("Elaborazione dati bilancio...",.T.)
    if this.oParentObject.w_TIPBILAN ="G"
      VQ_EXEC("..\BILC\EXE\QUERY\GESTDETT.VQR",this,"CursElab")
    else
      VQ_EXEC("..\BILC\EXE\QUERY\GESTDET1.VQR",this,"CursElab")
    endif
    * --- Cancello il contenuto dello zoom
    DELETE FROM ( this.w_ZOOM.cCursor )
    * --- Passo i valori allo ZOOM
    * --- Passo per un Array (Fox non accetta istruzioni del tipo Insert into from select ...)
    SELECT * FROM CursElab ;
    INTO ARRAY VETTORE ORDER BY CursElab.CPROWORD
    SELECT CursElab
    USE
    * --- Se il cursore � vuoto non crea il vettore
    if TYPE("VETTORE")<>"U"
      * --- Inizializzo la variabile OK a true (Posso effettuare le nuove elaborazioni sulla maschera)
      this.oParentObject.w_OK = .T.
      insert into ( this.w_ZOOM.cCursor ) from array vettore
    endif
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom.grd.refresh
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
      This.w_Zoom.grd.Column&NC..Enabled=.t.
    endif
    ENDFOR
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
