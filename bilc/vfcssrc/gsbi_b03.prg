* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b03                                                        *
*              Elabora coge documenti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_123]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-22                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b03",oParentObject)
return(i_retval)

define class tgsbi_b03 as StdBatch
  * --- Local variables
  AI = space(10)
  w_CODVAL = space(3)
  w_TOTVAL = 0
  w_XINICOM = ctod("  /  /  ")
  w_APPO = space(10)
  w_CAOVAL = 0
  w_PERIVA = 0
  w_XFINCOM = ctod("  /  /  ")
  w_OSERIAL = space(10)
  w_DECTOT = 0
  w_PERIND = 0
  w_APPVAL = 0
  w_FATEME = space(15)
  w_TESVAL = 0
  w_APPO1 = 0
  w_APPSEZ = space(1)
  w_FATRIC = space(15)
  w_TOTDOC = 0
  w_FLVAL = space(1)
  w_IVADEB = space(15)
  w_CODIVE = space(5)
  w_SEZCLF = space(1)
  w_APPO2 = 0
  w_IVACRE = space(15)
  w_FLRINC = space(1)
  w_ARCATCON = space(3)
  w_SCOMER = 0
  w_CONINC = space(15)
  w_FLRIMB = space(1)
  w_CODIVA = space(5)
  w_TOTDAR = 0
  w_CONIMB = space(15)
  w_FLRTRA = space(1)
  w_APPOK = .f.
  w_TOTAVE = 0
  w_CONTRA = space(15)
  w_SPEINC = 0
  w_CONTRO = space(15)
  w_IMPDAR = 0
  w_CONBOL = space(15)
  w_SPEIMB = 0
  w_CONIND = space(15)
  w_IMPAVE = 0
  w_CONDIC = space(15)
  w_SPETRA = 0
  w_FLOMAG = space(1)
  w_NOAGG = .f.
  w_CONARR = space(15)
  w_SPEBOL = 0
  w_VALMAG = 0
  w_FLVEAC = space(1)
  w_ARROTA = 0
  w_CONVEA = space(15)
  w_CODCON = space(15)
  w_CFCATCON = space(3)
  w_CONOMA = space(15)
  w_CONIVA = space(15)
  w_IVAGLO = 0
  w_CONOMI = space(15)
  w_INICOM = ctod("  /  /  ")
  w_IVADET = 0
  w_CONNAC = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_IVAOMA = 0
  w_XCODICE = space(15)
  w_DATDOC = ctod("  /  /  ")
  w_IVAIND = 0
  w_TESINICOM = ctod("  /  /  ")
  w_COMP = .f.
  w_TESFINCOM = ctod("  /  /  ")
  * --- WorkFile variables
  VOCIIVA_idx=0
  CONTVEAC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi i Documenti COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTFA001
    * --- Pagina 5 Inizializza le variabili
    DIMENSION AI[6, 6]
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    * --- Parametri dalla Maschera
    this.w_FATEME = this.oParentObject.oParentObject.w_FATEME
    this.w_FATRIC = this.oParentObject.oParentObject.w_FATRIC
    this.w_IVADEB = this.oParentObject.oParentObject.w_IVADEB
    this.w_IVACRE = this.oParentObject.oParentObject.w_IVACRE
    this.w_CONINC = this.oParentObject.oParentObject.w_CONINC
    this.w_CONIMB = this.oParentObject.oParentObject.w_CONIMB
    this.w_CONTRA = this.oParentObject.oParentObject.w_CONTRA
    this.w_CONBOL = this.oParentObject.oParentObject.w_CONBOL
    this.w_CONDIC = this.oParentObject.oParentObject.w_CONDIC
    this.w_CONARR = this.oParentObject.oParentObject.w_CONARR
    if USED("EXTFA001")
      * --- Crea il Temporaneo di Elaborazione Primanota
      CREATE CURSOR EXTCG001 ;
      (TIPCON C(1), CODICE C(15), INICOM D(8), FINCOM D(8), ;
      VALNAZ C(3), DATREG D(8), IMPDAR N(18,4), IMPAVE N(18,4))
      * --- Inizio Aggiornamento vero e proprio
      this.w_NOAGG = .F.
      Ah_Msg("Elaborazione Documenti da Generare...",.T.)
      SELECT EXTFA001
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVSERIAL," "))
      * --- Testa Cambio Documento
      if this.w_OSERIAL<>MVSERIAL
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT EXTFA001
        * --- Inizializza i dati di Testata della Nuovo Documento
        this.w_NOAGG = .F.
        this.w_FLVEAC = NVL(MVFLVEAC, " ")
        this.w_CODCON = IIF(this.w_FLVEAC="V", this.w_FATEME, this.w_FATRIC)
        this.w_CONIVA = IIF(this.w_FLVEAC="V", this.w_IVADEB, this.w_IVACRE)
        this.w_TESINICOM = NVL(TINCOM, cp_CharToDate("  -  -  "))
        this.w_TESFINCOM = NVL(TFICOM, cp_CharToDate("  -  -  "))
        this.w_COMP = (this.w_TESINICOM<=this.oParentObject.oParentObject.w_DATFIN AND this.w_TESFINCOM>=this.oParentObject.oParentObject.w_DATINI)
        this.w_COMP = this.w_COMP OR EMPTY(this.w_TESINICOM)
        this.w_DATDOC = NVL(MVDATDOC, cp_CharToDate("  -  -  "))
        this.w_CODVAL = NVL(MVCODVAL, g_PERVAL)
        this.w_CAOVAL = NVL(MVCAOVAL, 1)
        this.w_DECTOT = NVL(VADECTOT, 0)
        this.w_TESVAL = NVL(VACAOVAL, 0)
        this.w_CODIVE = NVL(MVCODIVE, SPACE(5))
        this.w_FLRINC = NVL(MVFLRINC, " ")
        this.w_FLRIMB = NVL(MVFLRIMB, " ")
        this.w_FLRTRA = NVL(MVFLRTRA, " ")
        this.w_SPEINC = NVL(MVSPEINC, 0)
        this.w_SPEIMB = NVL(MVSPEIMB, 0)
        this.w_SPETRA = NVL(MVSPETRA, 0)
        this.w_SPEBOL = NVL(MVSPEBOL, 0)
        this.w_ARROTA = NVL(MVIMPARR, 0)
        this.w_CFCATCON = NVL(ANCATCON, SPACE(3))
        this.w_IVAGLO = 0
        this.w_IVADET = 0
        this.w_IVAOMA = 0
        this.w_IVAIND = 0
        this.w_TOTVAL = this.w_ARROTA
        FOR L_i = 1 TO 6
        y = ALLTRIM(STR(L_i))
        AI[L_i, 1] = NVL(MVAFLOM&y, " ")
        AI[L_i, 2] = NVL(MVACIVA&y, SPACE(5))
        AI[L_i, 3] = NVL(MVAIMPN&y, 0)
        AI[L_i, 4] = NVL(MVAIMPS&y, 0)
        * --- Legge %Iva e %Indetraibile
        this.w_PERIVA = 0
        this.w_PERIND = 0
        if EMPTY(this.w_CODIVE) AND NOT EMPTY(AI[L_i, 2])
          this.w_APPO = AI[L_i, 2]
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_APPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT EXTFA001
        endif
        AI[L_i, 5] = this.w_PERIVA
        AI[L_i, 6] = this.w_PERIND
        * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
        this.w_TOTVAL = this.w_TOTVAL + (IIF(AI[L_i, 1] $ "IE", 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, AI[L_i, 4]))
        if this.w_CODVAL<>g_PERVAL
          * --- Converte in Moneta di Conto
          AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
          AI[L_i, 4] = VAL2MON(AI[L_i, 4],this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
        endif
        * --- Totale IVA Globale
        this.w_IVAGLO = this.w_IVAGLO + AI[L_i, 4]
        * --- Calcola la Parte Indetraibile sull'Ammontare dell'Aliquota
        this.w_APPO1 = IIF(AI[L_i, 4]=0 OR AI[L_i, 6]=0, 0, cp_ROUND((AI[L_i, 4] * AI[L_i, 6]) / 100, g_PERPVL))
        * --- Totale IVA Indetraibile
        this.w_IVAIND = this.w_IVAIND + this.w_APPO1
        * --- Totale IVA Detraibile
        this.w_IVADET = this.w_IVADET + (AI[L_i, 4] - this.w_APPO1)
        * --- Totale IVA su Omaggi
        this.w_IVAOMA = this.w_IVAOMA + IIF(AI[L_i, 1]="E", AI[L_i, 4], 0)
        ENDFOR
        * --- Totale Documento in Moneta di Conto
        this.w_TOTDOC = this.w_TOTVAL
        if this.w_CODVAL<>g_PERVAL
          this.w_TOTDOC = VAL2MON(this.w_TOTVAL,this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
        endif
        * --- Sezione Cli/For (Dare/Avere)
        this.w_SEZCLF = IIF(this.w_FLVEAC="V", "D", "A")
      endif
      SELECT EXTFA001
      this.w_OSERIAL = MVSERIAL
      this.w_INICOM = NVL(MVTINCOM, cp_CharToDate("  -  -  "))
      this.w_FINCOM = NVL(MVTFICOM, cp_CharToDate("  -  -  "))
      * --- Se c'e' almeno una riga evasa non scrive il documento
      this.w_NOAGG = IIF(NVL(MVFLEVAS," ")="S" OR NVL(MVQTAEVA,0)<>0 OR NVL(MVIMPEVA,0)<>0, .T. , this.w_NOAGG)
      * --- Scrive nuova Riga sul Temporaneo di Appoggio
      this.w_ARCATCON = NVL(MVCATCON, SPACE(3))
      this.w_CODIVA = NVL(MVCODIVA, SPACE(5))
      if NOT EMPTY(this.w_CODIVE)
        * --- Se Doc. Esente verifica che il Codice IVA della Riga sia Coerente altrimenti prende quello esente
        this.w_APPOK = .F.
        FOR L_i = 1 TO 6
        if this.w_CODIVA = AI[L_i, 2] AND NOT EMPTY(this.w_CODIVA)
          this.w_APPOK = .T.
          EXIT
        endif
        ENDFOR
        this.w_CODIVA = IIF(this.w_APPOK=.F., this.w_CODIVE, this.w_CODIVA)
      endif
      this.w_CONTRO = NVL(MVCONTRO, SPACE(15))
      this.w_CONIND = NVL(MVCONIND, SPACE(15))
      this.w_FLOMAG = NVL(MVFLOMAG, " ")
      this.w_VALMAG = NVL(VALMAG, 0)
      * --- Legge Contropartite Vendite/Acquisti
      this.w_CONVEA = SPACE(15)
      this.w_CONOMA = SPACE(15)
      this.w_CONOMI = SPACE(15)
      this.w_CONNAC = SPACE(15)
      if NOT EMPTY(this.w_ARCATCON) AND NOT EMPTY(this.w_CFCATCON)
        if this.w_FLVEAC="V"
          * --- Contropartite Vendite
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONRIC),cp_NullValue(_read_.CVCONRIC))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMV),cp_NullValue(_read_.CVCONOMV))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMA),cp_NullValue(_read_.CVIVAOMA))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCC),cp_NullValue(_read_.CVCONNCC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Contropartite Acquisti
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONACQ),cp_NullValue(_read_.CVCONACQ))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMA),cp_NullValue(_read_.CVCONOMA))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMC),cp_NullValue(_read_.CVIVAOMC))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCF),cp_NullValue(_read_.CVCONNCF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        SELECT EXTFA001
      endif
      this.w_CONTRO = IIF(EMPTY(this.w_CONTRO), this.w_CONVEA, this.w_CONTRO)
      this.w_CONIND = IIF(EMPTY(this.w_CONIND), this.w_CONTRO, this.w_CONIND)
      INSERT INTO DettDocu (FLOMAG, CONTRO, CONOMA, CONOMI, ;
      CONIND, VALRIG, CODIVA, CATCLF, CATART,INICOM,FINCOM) ;
      VALUES (this.w_FLOMAG, this.w_CONTRO, this.w_CONOMA, this.w_CONOMI, ;
      this.w_CONIND, this.w_VALMAG, this.w_CODIVA, this.w_CFCATCON, this.w_ARCATCON,this.w_INICOM,this.w_FINCOM)
      SELECT EXTFA001
      ENDSCAN
      * --- Testa l'Ultima Uscita
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Elimina il Cursore
      SELECT EXTFA001
      USE
      if USED("DettDocu")
        SELECT DettDocu
        USE
      endif
      if USED("AppCur")
        SELECT AppCur
        USE
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Temporaneo di Elaborazione
    if this.w_OSERIAL <> REPL("#", 10) AND RECCOUNT("DettDocu") > 0 AND this.w_NOAGG=.F.
      * --- Scrive il Dettaglio: Riga Cliente/Fornitore
      this.w_XCODICE = this.w_CODCON
      this.w_APPVAL = this.w_TOTDOC
      this.w_APPSEZ = this.w_SEZCLF
      this.w_XINICOM = cp_CharToDate("  -  -  ")
      this.w_XFINCOM = cp_CharToDate("  -  -  ")
      this.w_FLVAL = "N"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrive il Dettaglio: Conto IVA Detraibile
      if this.w_IVADET<>0 AND NOT EMPTY(this.w_CONIVA)
        this.w_XCODICE = this.w_CONIVA
        this.w_APPVAL = this.w_IVADET
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_XINICOM = cp_CharToDate("  -  -  ")
        this.w_XFINCOM = cp_CharToDate("  -  -  ")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Scrive il Dettaglio: Conti IVA Indetraibile
      if this.w_IVAIND<>0
        SELECT CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART FROM DettDocu ;
        WHERE FLOMAG $ "XIE" INTO ARRAY AppArr GROUP BY CONIND, CODIVA
        this.w_APPO1 = this.w_IVAIND
        FOR L_i = 1 TO ALEN(AppArr, 1)
        if this.w_CODVAL<>g_PERVAL
          * --- Converte in Moneta di Conto
          AppArr[L_i, 3] = VAL2MON(AppArr[L_i, 3],this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
        endif
        * --- Prende la Parte Indetraibile
        this.w_APPO2 = 0
        FOR L_y = 1 TO 6
        if AI[L_y, 2] = AppArr[L_i, 2]
          if AppArr[L_i, 3]=0 OR AI[L_y, 5]=0 OR AI[L_y, 6]=0
            EXIT
          else
            this.w_APPO2 = cp_ROUND(((AppArr[L_i, 3] * AI[L_y, 5]) / 100) + IIF(g_PERPVL=0, .499, .00499), g_PERPVL)
            this.w_APPO2 = cp_ROUND((this.w_APPO2 * AI[L_y, 6]) / 100, g_PERPVL)
            EXIT
          endif
        endif
        ENDFOR
        * --- Scrive Importo Indetraibile sul Vettore
        AppArr[L_i, 3] = this.w_APPO2
        * --- Storna dal Totale IVA Indetraibile
        this.w_APPO1 = this.w_APPO1 - this.w_APPO2
        ENDFOR
        * --- Alla Fine Scrive le Righe P.N.
        this.w_APPVAL = 0
        FOR L_i = 1 TO ALEN(AppArr, 1)
        this.w_XCODICE = AppArr[L_i, 1]
        * --- Somma gli Importi delle Aliquote IVA e Mette l'eventuale Resto sulla Prima Riga
        this.w_APPVAL = this.w_APPVAL + AppArr[L_i, 3] + IIF(L_i = 1, this.w_APPO1, 0)
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_XINICOM = cp_CharToDate("  -  -  ")
        this.w_XFINCOM = cp_CharToDate("  -  -  ")
        if L_i = ALEN(AppArr, 1)
          * --- Se siamo in Fondo Scrive
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if AppArr[L_i+1, 1] <> this.w_XCODICE
            * --- ...Oppure Scrive se il Successivo Conto e' diverso
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_APPVAL = 0
          endif
        endif
        ENDFOR
      endif
      * --- Scrive il Dettaglio: Riga Contropartite
      SELECT CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ;
      WHERE FLOMAG $ "XIE" INTO CURSOR AppCur GROUP BY CONTRO, INICOM, FINCOM
      SELECT AppCur
      GO TOP
      SCAN FOR NVL(VALAPP,0)<>0 OR NOT EMPTY(NVL(CONTRO," "))
      this.w_SCOMER = 0
      if NVL(VALAPP, 0)<>0 AND NOT EMPTY(CONTRO)
        this.w_XCODICE = CONTRO
        this.w_APPVAL = VALAPP
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_XINICOM = INICOM
        this.w_XFINCOM = FINCOM
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT AppCur
      endif
      ENDSCAN
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR DettDocu ;
    (FLOMAG C(1), CONTRO C(15), CONOMA C(15), CONOMI C(15), CONIND C(15), ;
    VALRIG N(18,4), CODIVA C(5), CATCLF C(3), CATART C(3), INICOM D(8), FINCOM D(8))
    this.w_TOTVAL = 0
    this.w_TOTDOC = 0
    this.w_TOTDAR = 0
    this.w_TOTAVE = 0
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Temporaneo di Elaborazione Primanota
    if this.w_FLVAL="S" AND this.w_CODVAL<>g_PERVAL
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
    endif
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    if this.w_APPSEZ="D"
      this.w_IMPDAR = ABS(this.w_APPVAL)
      this.w_IMPAVE = 0
    else
      this.w_IMPDAR = 0
      this.w_IMPAVE = ABS(this.w_APPVAL)
    endif
    this.w_TOTDAR = this.w_TOTDAR + this.w_IMPDAR
    this.w_TOTAVE = this.w_TOTAVE + this.w_IMPAVE
    INSERT INTO EXTCG001 (TIPCON, CODICE, INICOM, FINCOM, ;
    VALNAZ, DATREG, IMPDAR, IMPAVE) ;
    VALUES ("G", this.w_XCODICE, this.w_XINICOM, this.w_XFINCOM, ;
    g_PERVAL, this.w_DATDOC, this.w_IMPDAR, this.w_IMPAVE)
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non ci Sono Importi da Registrare Verifica se si Tratta di Documento con Importo a 0 (Solo Sconto Merce)
    if this.w_TOTDOC=0 AND this.w_IVAGLO=0 AND this.w_SCOMER=1
      * --- Scrive il Dettaglio Sconto Merce: Riga Contropartite
      SELECT CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ;
      WHERE ((INICOM <= this.oParentObject.oParentObject.w_DATFIN AND FINCOM >= this.oParentObject.oParentObject.w_DATINI) ;
      OR EMPTY(INICOM)) AND FLOMAG $ "S" ;
      INTO CURSOR AppCur GROUP BY CONTRO, INICOM, FINCOM
      SELECT AppCur
      GO TOP
      SCAN FOR NVL(VALAPP,0)<>0 AND NOT EMPTY(NVL(CONTRO," "))
      this.w_SCOMER = 10
      this.w_XCODICE = CONTRO
      this.w_APPVAL = 0
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "N"
      this.w_XINICOM = INICOM
      this.w_XFINCOM = FINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT AppCur
      ENDSCAN
      * --- Scrive Riga IVA Sc.Merce
      if this.w_SCOMER=10 AND NOT EMPTY(this.w_CONIVA)
        this.w_XCODICE = this.w_CONIVA
        this.w_APPVAL = 0
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_XINICOM = cp_CharToDate("  -  -  ")
        this.w_XFINCOM = cp_CharToDate("  -  -  ")
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive le Spese
    * --- Scrive il Dettaglio: Spese Incasso
    if this.w_SPEINC<>0 AND this.w_FLRINC<>"S" AND NOT EMPTY(this.w_CONINC) AND this.w_COMP
      this.w_XCODICE = this.w_CONINC
      this.w_APPVAL = this.w_SPEINC
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive il Dettaglio: Spese Imballo
    if this.w_SPEIMB<>0 AND this.w_FLRIMB<>"S" AND NOT EMPTY(this.w_CONIMB) AND this.w_COMP
      this.w_XCODICE = this.w_CONIMB
      this.w_APPVAL = this.w_SPEIMB
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive il Dettaglio: Spese Trasporto
    if this.w_SPETRA<>0 AND this.w_FLRTRA<>"S" AND NOT EMPTY(this.w_CONTRA) AND this.w_COMP
      this.w_XCODICE = this.w_CONTRA
      this.w_APPVAL = this.w_SPETRA
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive il Dettaglio: Spese Bolli
    if this.w_SPEBOL<>0 AND NOT EMPTY(this.w_CONBOL) AND this.w_COMP
      this.w_XCODICE = this.w_CONBOL
      this.w_APPVAL = this.w_SPEBOL
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive il Dettaglio: Omaggi di Imponibile
    SELECT CONOMA, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ;
    WHERE ((INICOM <= this.oParentObject.oParentObject.w_DATFIN AND FINCOM >= this.oParentObject.oParentObject.w_DATINI) ;
    OR EMPTY(INICOM)) AND FLOMAG $ "IE" ;
    INTO CURSOR AppCur GROUP BY CONOMA, INICOM, FINCOM
    SELECT AppCur
    GO TOP
    SCAN FOR VALAPP<>0 AND NOT EMPTY(this.w_CONOMA)
    this.w_XCODICE = CONOMA
    this.w_APPVAL = VALAPP
    this.w_APPSEZ = this.w_SEZCLF
    this.w_FLVAL = "S"
    this.w_XINICOM = INICOM
    this.w_XFINCOM = FINCOM
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT AppCur
    ENDSCAN
    * --- Scrive il Dettaglio: Conti IVA su Omaggi
    if this.w_IVAOMA<>0
      SELECT CONOMI, CODIVA, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ;
      WHERE ((INICOM <= this.oParentObject.oParentObject.w_DATFIN AND FINCOM >= this.oParentObject.oParentObject.w_DATINI) ;
      OR EMPTY(INICOM)) AND FLOMAG = "E" ;
      INTO ARRAY AppArr GROUP BY CONOMI, CODIVA, INICOM, FINCOM
      this.w_APPO1 = this.w_IVAOMA
      FOR L_i = 1 TO ALEN(AppArr, 1)
      if this.w_CODVAL<>g_PERVAL
        * --- Converte in Moneta di Conto
        AppArr[L_i, 3] = VAL2MON(AppArr[L_i, 3],this.w_CAOVAL,1,this.w_DATDOC,g_PERVAL, g_PERPVL)
      endif
      this.w_APPO2 = 0
      FOR L_y = 1 TO 6
      if AI[L_y, 2] = AppArr[L_i, 2]
        if AppArr[L_i, 3]=0 OR AI[L_y, 5]=0
          EXIT
        else
          this.w_APPO2 = cp_ROUND(((AppArr[L_i, 3] * AI[L_y, 5]) / 100) + IIF(g_PERPVL=0, .499, .00499), g_PERPVL)
          EXIT
        endif
      endif
      ENDFOR
      * --- Scrive Imposta sul Vettore
      AppArr[L_i, 3] = this.w_APPO2
      * --- Storna dal Totale IVA Omaggi
      this.w_APPO1 = this.w_APPO1 - this.w_APPO2
      ENDFOR
      * --- Alla Fine Scrive le Righe P.N.
      this.w_APPVAL = 0
      FOR L_i = 1 TO ALEN(AppArr, 1)
      this.w_XCODICE = AppArr[L_i, 1]
      * --- Somma gli Importi delle Aliquote IVA e Mette l'eventuale Resto sulla Prima Riga
      this.w_APPVAL = this.w_APPVAL + AppArr[L_i, 3] + IIF(L_i = 1, this.w_APPO1, 0)
      this.w_APPSEZ = this.w_SEZCLF
      this.w_FLVAL = "N"
      this.w_XINICOM = cp_CharToDate("  -  -  ")
      this.w_XFINCOM = cp_CharToDate("  -  -  ")
      if L_i = ALEN(AppArr, 1)
        * --- Se siamo in Fondo Scrive
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if AppArr[L_i+1, 1] <> this.w_XCODICE
          * --- ...Oppure Scrive se il Successivo Conto e' diverso
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_APPVAL = 0
        endif
      endif
      ENDFOR
    endif
    * --- Scrive il Dettaglio: Arrotondamenti
    if this.w_ARROTA<>0 AND NOT EMPTY(this.w_CONARR) AND this.w_COMP
      this.w_XCODICE = this.w_CONARR
      this.w_APPVAL = this.w_ARROTA
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive il Dettaglio: Eventuali Differenze di Conversione (Solo alla Fine)
    if this.w_TOTDAR<>this.w_TOTAVE AND this.w_CODVAL<>g_PERVAL AND NOT EMPTY(this.w_CONDIC) AND this.w_COMP
      this.w_XCODICE = this.w_CONDIC
      this.w_APPSEZ = IIF(this.w_TOTDAR-this.w_TOTAVE>0, "A", "D")
      this.w_APPVAL = ABS(this.w_TOTDAR-this.w_TOTAVE)
      this.w_FLVAL = "N"
      this.w_XINICOM = this.w_TESINICOM
      this.w_XFINCOM = this.w_TESFINCOM
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione Variabili
    * --- Variabili Locali
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CONTVEAC'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
