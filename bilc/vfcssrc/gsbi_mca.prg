* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mca                                                        *
*              Movimenti extra contabili                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_mca"))

* --- Class definition
define class tgsbi_mca as StdTrsForm
  Top    = 4
  Left   = 10

  * --- Standard Properties
  Width  = 693
  Height = 428+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=171309463
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MOX_COAM_IDX = 0
  MOX_COAN_IDX = 0
  PER_ELAB_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  REG_EXTM_IDX = 0
  cFile = "MOX_COAM"
  cFileDetail = "MOX_COAN"
  cKeySelect = "CASERIAL"
  cKeyWhere  = "CASERIAL=this.w_CASERIAL"
  cKeyDetail  = "CASERIAL=this.w_CASERIAL"
  cKeyWhereODBC = '"CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';

  cKeyDetailWhereODBC = '"CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MOX_COAN.CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOX_COAN.CPROWNUM '
  cPrg = "gsbi_mca"
  cComment = "Movimenti extra contabili"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_CASERIAL = space(10)
  w_CANUMREG = 0
  w_CACODESE = space(4)
  o_CACODESE = space(4)
  w_CADATREG = ctod('  /  /  ')
  o_CADATREG = ctod('  /  /  ')
  w_CAFLGCON = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_CATIPMOV = space(1)
  w_CADESCRI = space(45)
  w_CGCODESE = space(4)
  w_CAVALNAZ = space(3)
  o_CAVALNAZ = space(3)
  w_CACODPER = space(15)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_DESPER = space(35)
  w_PERINI = ctod('  /  /  ')
  o_PERINI = ctod('  /  /  ')
  w_PERFIN = ctod('  /  /  ')
  w_CACODREG = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DESREG = space(45)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPREG = space(1)
  w_CAVOCCEN = space(15)
  w_CACODCEN = space(15)
  w_CACODCOM = space(15)
  w_CAIMPDAR = 0
  o_CAIMPDAR = 0
  w_CAIMPAVE = 0
  o_CAIMPAVE = 0
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SIMVAL = space(5)
  w_DATAOBSO = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CASERIAL = this.W_CASERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CACODESE = this.W_CACODESE
  op_CANUMREG = this.W_CANUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOX_COAM','gsbi_mca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mcaPag1","gsbi_mca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti extra contabili")
      .Pages(1).HelpContextID = 146841878
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='PER_ELAB'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='VOC_COST'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='VALUTE'
    this.cWorkTables[7]='REG_EXTM'
    this.cWorkTables[8]='MOX_COAM'
    this.cWorkTables[9]='MOX_COAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOX_COAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOX_COAM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CASERIAL = NVL(CASERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MOX_COAM where CASERIAL=KeySet.CASERIAL
    *
    i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2],this.bLoadRecFilter,this.MOX_COAM_IDX,"gsbi_mca")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOX_COAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOX_COAM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MOX_COAN.","MOX_COAM.")
      i_cTable = i_cTable+' MOX_COAM '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CASERIAL',this.w_CASERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_DECTOT = 0
        .w_DESPER = space(35)
        .w_PERINI = ctod("  /  /  ")
        .w_PERFIN = ctod("  /  /  ")
        .w_DESREG = space(45)
        .w_TIPREG = space(1)
        .w_TOTDAR = 0
        .w_TOTAVE = 0
        .w_SIMVAL = space(5)
        .w_DATAOBSO = ctod("  /  /  ")
        .w_CASERIAL = NVL(CASERIAL,space(10))
        .op_CASERIAL = .w_CASERIAL
        .w_CANUMREG = NVL(CANUMREG,0)
        .op_CANUMREG = .w_CANUMREG
        .w_CACODESE = NVL(CACODESE,space(4))
        .op_CACODESE = .w_CACODESE
          * evitabile
          *.link_1_4('Load')
        .w_CADATREG = NVL(cp_ToDate(CADATREG),ctod("  /  /  "))
        .w_CAFLGCON = NVL(CAFLGCON,space(1))
        .w_DTOBSO = .w_CADATREG
        .w_CATIPMOV = NVL(CATIPMOV,space(1))
        .w_CADESCRI = NVL(CADESCRI,space(45))
        .w_CGCODESE = .w_CACODESE
        .w_CAVALNAZ = NVL(CAVALNAZ,space(3))
          if link_1_11_joined
            this.w_CAVALNAZ = NVL(VACODVAL111,NVL(this.w_CAVALNAZ,space(3)))
            this.w_DECTOT = NVL(VADECTOT111,0)
            this.w_SIMVAL = NVL(VASIMVAL111,space(5))
          else
          .link_1_11('Load')
          endif
        .w_CACODPER = NVL(CACODPER,space(15))
          if link_1_12_joined
            this.w_CACODPER = NVL(PECODICE112,NVL(this.w_CACODPER,space(15)))
            this.w_DESPER = NVL(PEDESCRI112,space(35))
            this.w_PERINI = NVL(cp_ToDate(PEDATINI112),ctod("  /  /  "))
            this.w_PERFIN = NVL(cp_ToDate(PEDATFIN112),ctod("  /  /  "))
            this.w_DATAOBSO = NVL(cp_ToDate(PEDTOBSO112),ctod("  /  /  "))
          else
          .link_1_12('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CACODREG = NVL(CACODREG,space(15))
          if link_1_24_joined
            this.w_CACODREG = NVL(RECODICE124,NVL(this.w_CACODREG,space(15)))
            this.w_DESREG = NVL(REDESCRI124,space(45))
            this.w_TIPREG = NVL(RE__TIPO124,space(1))
          else
          .link_1_24('Load')
          endif
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOX_COAM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MOX_COAN where CASERIAL=KeySet.CASERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MOX_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAN_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MOX_COAN')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MOX_COAN.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MOX_COAN"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CASERIAL',this.w_CASERIAL  )
        select * from (i_cTable) MOX_COAN where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      scan
        with this
          .w_DESVOC = space(40)
          .w_DESPIA = space(40)
          .w_DESCAN = space(30)
          .w_CPROWNUM = CPROWNUM
          .w_CAVOCCEN = NVL(CAVOCCEN,space(15))
          if link_2_1_joined
            this.w_CAVOCCEN = NVL(VCCODICE201,NVL(this.w_CAVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI201,space(40))
          else
          .link_2_1('Load')
          endif
          .w_CACODCEN = NVL(CACODCEN,space(15))
          if link_2_2_joined
            this.w_CACODCEN = NVL(CC_CONTO202,NVL(this.w_CACODCEN,space(15)))
            this.w_DESPIA = NVL(CCDESPIA202,space(40))
          else
          .link_2_2('Load')
          endif
          .w_CACODCOM = NVL(CACODCOM,space(15))
          if link_2_3_joined
            this.w_CACODCOM = NVL(CNCODCAN203,NVL(this.w_CACODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN203,space(30))
          else
          .link_2_3('Load')
          endif
          .w_CAIMPDAR = NVL(CAIMPDAR,0)
          .w_CAIMPAVE = NVL(CAIMPAVE,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTDAR = .w_TOTDAR+.w_CAIMPDAR
          .w_TOTAVE = .w_TOTAVE+.w_CAIMPAVE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_DTOBSO = .w_CADATREG
        .w_CGCODESE = .w_CACODESE
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODAZI=space(5)
      .w_CASERIAL=space(10)
      .w_CANUMREG=0
      .w_CACODESE=space(4)
      .w_CADATREG=ctod("  /  /  ")
      .w_CAFLGCON=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_CATIPMOV=space(1)
      .w_CADESCRI=space(45)
      .w_CGCODESE=space(4)
      .w_CAVALNAZ=space(3)
      .w_CACODPER=space(15)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_DESPER=space(35)
      .w_PERINI=ctod("  /  /  ")
      .w_PERFIN=ctod("  /  /  ")
      .w_CACODREG=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESREG=space(45)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_TIPREG=space(1)
      .w_CAVOCCEN=space(15)
      .w_CACODCEN=space(15)
      .w_CACODCOM=space(15)
      .w_CAIMPDAR=0
      .w_CAIMPAVE=0
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SIMVAL=space(5)
      .w_DATAOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,3,.f.)
        .w_CACODESE = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CACODESE))
         .link_1_4('Full')
        endif
        .w_CADATREG = i_datsys
        .w_CAFLGCON = 'S'
        .w_DTOBSO = .w_CADATREG
        .w_CATIPMOV = 'E'
        .DoRTCalc(9,9,.f.)
        .w_CGCODESE = .w_CACODESE
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CAVALNAZ))
         .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CACODPER))
         .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(15,18,.f.)
        if not(empty(.w_CACODREG))
         .link_1_24('Full')
        endif
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .DoRTCalc(20,26,.f.)
        if not(empty(.w_CAVOCCEN))
         .link_2_1('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CACODCEN))
         .link_2_2('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CACODCOM))
         .link_2_3('Full')
        endif
        .w_CAIMPDAR = 0
        .w_CAIMPAVE = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOX_COAM')
    this.DoRTCalc(31,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCANUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oCACODESE_1_4.enabled = i_bVal
      .Page1.oPag.oCADATREG_1_5.enabled = i_bVal
      .Page1.oPag.oCATIPMOV_1_8.enabled = i_bVal
      .Page1.oPag.oCADESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oCACODPER_1_12.enabled = i_bVal
      .Page1.oPag.oCACODREG_1_24.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCANUMREG_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOX_COAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMOXAN","i_codazi,w_CASERIAL")
      cp_AskTableProg(this,i_nConn,"PRMOXAN","i_codazi,w_CACODESE,w_CANUMREG")
      .op_codazi = .w_codazi
      .op_CASERIAL = .w_CASERIAL
      .op_codazi = .w_codazi
      .op_CACODESE = .w_CACODESE
      .op_CANUMREG = .w_CANUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASERIAL,"CASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CANUMREG,"CANUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODESE,"CACODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADATREG,"CADATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLGCON,"CAFLGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPMOV,"CATIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCRI,"CADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAVALNAZ,"CAVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODPER,"CACODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODREG,"CACODREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
    i_lTable = "MOX_COAM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOX_COAM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSBI_SCA with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CAVOCCEN C(15);
      ,t_CACODCEN C(15);
      ,t_CACODCOM C(15);
      ,t_CAIMPDAR N(18,4);
      ,t_CAIMPAVE N(18,4);
      ,t_DESVOC C(40);
      ,t_DESPIA C(40);
      ,t_DESCAN C(30);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mcabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAVOCCEN_2_1.controlsource=this.cTrsName+'.t_CAVOCCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCEN_2_2.controlsource=this.cTrsName+'.t_CACODCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCOM_2_3.controlsource=this.cTrsName+'.t_CACODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPDAR_2_4.controlsource=this.cTrsName+'.t_CAIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPAVE_2_5.controlsource=this.cTrsName+'.t_CAIMPAVE'
    this.oPgFRm.Page1.oPag.oDESVOC_2_6.controlsource=this.cTrsName+'.t_DESVOC'
    this.oPgFRm.Page1.oPag.oDESPIA_2_7.controlsource=this.cTrsName+'.t_DESPIA'
    this.oPgFRm.Page1.oPag.oDESCAN_2_8.controlsource=this.cTrsName+'.t_DESCAN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(126)
    this.AddVLine(251)
    this.AddVLine(376)
    this.AddVLine(522)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVOCCEN_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEMOXAN","i_codazi,w_CASERIAL")
          cp_NextTableProg(this,i_nConn,"PRMOXAN","i_codazi,w_CACODESE,w_CANUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOX_COAM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOX_COAM')
        i_extval=cp_InsertValODBCExtFlds(this,'MOX_COAM')
        local i_cFld
        i_cFld=" "+;
                  "(CASERIAL,CANUMREG,CACODESE,CADATREG,CAFLGCON"+;
                  ",CATIPMOV,CADESCRI,CAVALNAZ,CACODPER,CACODREG"+;
                  ",UTCC,UTCV,UTDC,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CASERIAL)+;
                    ","+cp_ToStrODBC(this.w_CANUMREG)+;
                    ","+cp_ToStrODBCNull(this.w_CACODESE)+;
                    ","+cp_ToStrODBC(this.w_CADATREG)+;
                    ","+cp_ToStrODBC(this.w_CAFLGCON)+;
                    ","+cp_ToStrODBC(this.w_CATIPMOV)+;
                    ","+cp_ToStrODBC(this.w_CADESCRI)+;
                    ","+cp_ToStrODBCNull(this.w_CAVALNAZ)+;
                    ","+cp_ToStrODBCNull(this.w_CACODPER)+;
                    ","+cp_ToStrODBCNull(this.w_CACODREG)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOX_COAM')
        i_extval=cp_InsertValVFPExtFlds(this,'MOX_COAM')
        cp_CheckDeletedKey(i_cTable,0,'CASERIAL',this.w_CASERIAL)
        INSERT INTO (i_cTable);
              (CASERIAL,CANUMREG,CACODESE,CADATREG,CAFLGCON,CATIPMOV,CADESCRI,CAVALNAZ,CACODPER,CACODREG,UTCC,UTCV,UTDC,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CASERIAL;
                  ,this.w_CANUMREG;
                  ,this.w_CACODESE;
                  ,this.w_CADATREG;
                  ,this.w_CAFLGCON;
                  ,this.w_CATIPMOV;
                  ,this.w_CADESCRI;
                  ,this.w_CAVALNAZ;
                  ,this.w_CACODPER;
                  ,this.w_CACODREG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOX_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAN_IDX,2])
      *
      * insert into MOX_COAN
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CASERIAL,CAVOCCEN,CACODCEN,CACODCOM,CAIMPDAR"+;
                  ",CAIMPAVE,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CASERIAL)+","+cp_ToStrODBCNull(this.w_CAVOCCEN)+","+cp_ToStrODBCNull(this.w_CACODCEN)+","+cp_ToStrODBCNull(this.w_CACODCOM)+","+cp_ToStrODBC(this.w_CAIMPDAR)+;
             ","+cp_ToStrODBC(this.w_CAIMPAVE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CASERIAL',this.w_CASERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CASERIAL,this.w_CAVOCCEN,this.w_CACODCEN,this.w_CACODCOM,this.w_CAIMPDAR"+;
                ",this.w_CAIMPAVE,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MOX_COAM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MOX_COAM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CANUMREG="+cp_ToStrODBC(this.w_CANUMREG)+;
             ",CACODESE="+cp_ToStrODBCNull(this.w_CACODESE)+;
             ",CADATREG="+cp_ToStrODBC(this.w_CADATREG)+;
             ",CAFLGCON="+cp_ToStrODBC(this.w_CAFLGCON)+;
             ",CATIPMOV="+cp_ToStrODBC(this.w_CATIPMOV)+;
             ",CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
             ",CAVALNAZ="+cp_ToStrODBCNull(this.w_CAVALNAZ)+;
             ",CACODPER="+cp_ToStrODBCNull(this.w_CACODPER)+;
             ",CACODREG="+cp_ToStrODBCNull(this.w_CACODREG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MOX_COAM')
          i_cWhere = cp_PKFox(i_cTable  ,'CASERIAL',this.w_CASERIAL  )
          UPDATE (i_cTable) SET;
              CANUMREG=this.w_CANUMREG;
             ,CACODESE=this.w_CACODESE;
             ,CADATREG=this.w_CADATREG;
             ,CAFLGCON=this.w_CAFLGCON;
             ,CATIPMOV=this.w_CATIPMOV;
             ,CADESCRI=this.w_CADESCRI;
             ,CAVALNAZ=this.w_CAVALNAZ;
             ,CACODPER=this.w_CACODPER;
             ,CACODREG=this.w_CACODREG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_CAVOCCEN) AND NOT EMPTY(t_CACODCEN)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MOX_COAN_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAN_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MOX_COAN
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOX_COAN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CAVOCCEN="+cp_ToStrODBCNull(this.w_CAVOCCEN)+;
                     ",CACODCEN="+cp_ToStrODBCNull(this.w_CACODCEN)+;
                     ",CACODCOM="+cp_ToStrODBCNull(this.w_CACODCOM)+;
                     ",CAIMPDAR="+cp_ToStrODBC(this.w_CAIMPDAR)+;
                     ",CAIMPAVE="+cp_ToStrODBC(this.w_CAIMPAVE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CAVOCCEN=this.w_CAVOCCEN;
                     ,CACODCEN=this.w_CACODCEN;
                     ,CACODCOM=this.w_CACODCOM;
                     ,CAIMPDAR=this.w_CAIMPDAR;
                     ,CAIMPAVE=this.w_CAIMPAVE;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_CAVOCCEN) AND NOT EMPTY(t_CACODCEN)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MOX_COAN_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAN_IDX,2])
        *
        * delete MOX_COAN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
        *
        * delete MOX_COAM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_CAVOCCEN) AND NOT EMPTY(t_CACODCEN)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOX_COAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COAM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CADATREG<>.w_CADATREG
          .w_DTOBSO = .w_CADATREG
        endif
        .DoRTCalc(8,9,.t.)
        if .o_CACODESE<>.w_CACODESE
          .w_CGCODESE = .w_CACODESE
        endif
        if .o_CACODESE<>.w_CACODESE
          .link_1_11('Full')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_CAVALNAZ<>.w_CAVALNAZ
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(15,18,.t.)
        if .o_PERINI<>.w_PERINI
          .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        endif
        .DoRTCalc(20,28,.t.)
        if .o_CAIMPAVE<>.w_CAIMPAVE
          .w_TOTDAR = .w_TOTDAR-.w_caimpdar
          .w_CAIMPDAR = 0
          .w_TOTDAR = .w_TOTDAR+.w_caimpdar
        endif
        if .o_CAIMPDAR<>.w_CAIMPDAR
          .w_TOTAVE = .w_TOTAVE-.w_caimpave
          .w_CAIMPAVE = 0
          .w_TOTAVE = .w_TOTAVE+.w_caimpave
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMOXAN","i_codazi,w_CASERIAL")
          .op_CASERIAL = .w_CASERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_CACODESE<>.w_CACODESE
           cp_AskTableProg(this,i_nConn,"PRMOXAN","i_codazi,w_CACODESE,w_CANUMREG")
          .op_CANUMREG = .w_CANUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_CACODESE = .w_CACODESE
      endwith
      this.DoRTCalc(31,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODCOM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODCOM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAIMPDAR_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAIMPDAR_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAIMPAVE_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAIMPAVE_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CACODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CACODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCACODESE_1_4'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CACODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CACODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_CAVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CACODESE = space(4)
      endif
      this.w_CAVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAVALNAZ
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CAVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CAVALNAZ)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAVALNAZ = space(3)
      endif
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.VACODVAL as VACODVAL111"+ ",link_1_11.VADECTOT as VADECTOT111"+ ",link_1_11.VASIMVAL as VASIMVAL111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MOX_COAM.CAVALNAZ=link_1_11.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MOX_COAM.CAVALNAZ=link_1_11.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODPER
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_CACODPER)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_CACODPER))
          select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODPER)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStrODBC(trim(this.w_CACODPER)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStr(trim(this.w_CACODPER)+"%");

            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODPER) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oCACODPER_1_12'),i_cWhere,'GSBI_APE',"Periodi di elaborazione",'GSBI_MCG.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_CACODPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_CACODPER)
            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODPER = NVL(_Link_.PECODICE,space(15))
      this.w_DESPER = NVL(_Link_.PEDESCRI,space(35))
      this.w_PERINI = NVL(cp_ToDate(_Link_.PEDATINI),ctod("  /  /  "))
      this.w_PERFIN = NVL(cp_ToDate(_Link_.PEDATFIN),ctod("  /  /  "))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.PEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODPER = space(15)
      endif
      this.w_DESPER = space(35)
      this.w_PERINI = ctod("  /  /  ")
      this.w_PERFIN = ctod("  /  /  ")
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PER_ELAB_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.PECODICE as PECODICE112"+ ",link_1_12.PEDESCRI as PEDESCRI112"+ ",link_1_12.PEDATINI as PEDATINI112"+ ",link_1_12.PEDATFIN as PEDATFIN112"+ ",link_1_12.PEDTOBSO as PEDTOBSO112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on MOX_COAM.CACODPER=link_1_12.PECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and MOX_COAM.CACODPER=link_1_12.PECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODREG
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_EXTM_IDX,3]
    i_lTable = "REG_EXTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2], .t., this.REG_EXTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MRE',True,'REG_EXTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_CACODREG)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_CACODREG))
          select RECODICE,REDESCRI,RE__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODREG)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_CACODREG)+"%");

            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_CACODREG)+"%");

            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODREG) and !this.bDontReportError
            deferred_cp_zoom('REG_EXTM','*','RECODICE',cp_AbsName(oSource.parent,'oCACODREG_1_24'),i_cWhere,'GSBI_MRE',"Regole di elaborazione",'GSBI_MCA.REG_EXTM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_CACODREG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_CACODREG)
            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODREG = NVL(_Link_.RECODICE,space(15))
      this.w_DESREG = NVL(_Link_.REDESCRI,space(45))
      this.w_TIPREG = NVL(_Link_.RE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODREG = space(15)
      endif
      this.w_DESREG = space(45)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREG $ ' A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Regola inesistente o di tipo contabile")
        endif
        this.w_CACODREG = space(15)
        this.w_DESREG = space(45)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REG_EXTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REG_EXTM_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.RECODICE as RECODICE124"+ ",link_1_24.REDESCRI as REDESCRI124"+ ",link_1_24.RE__TIPO as RE__TIPO124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on MOX_COAM.CACODREG=link_1_24.RECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and MOX_COAM.CACODREG=link_1_24.RECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAVOCCEN
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_CAVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_CAVOCCEN))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oCAVOCCEN_2_1'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_CAVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_CAVOCCEN)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CAVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VCCODICE as VCCODICE201"+ ",link_2_1.VCDESCRI as VCDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MOX_COAN.CAVOCCEN=link_2_1.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MOX_COAN.CAVOCCEN=link_2_1.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODCEN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CACODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CACODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_CACODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_CACODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCACODCEN_2_2'),i_cWhere,'GSCA_ACC',"Centri di costo o ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CACODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CACODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CACODCEN = space(15)
      endif
      this.w_DESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CC_CONTO as CC_CONTO202"+ ",link_2_2.CCDESPIA as CCDESPIA202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MOX_COAN.CACODCEN=link_2_2.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MOX_COAN.CACODCEN=link_2_2.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CACODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CACODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CACODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CACODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCACODCOM_2_3'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CACODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CACODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CACODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CNCODCAN as CNCODCAN203"+ ",link_2_3.CNDESCAN as CNDESCAN203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOX_COAN.CACODCOM=link_2_3.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOX_COAN.CACODCOM=link_2_3.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCANUMREG_1_3.value==this.w_CANUMREG)
      this.oPgFrm.Page1.oPag.oCANUMREG_1_3.value=this.w_CANUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODESE_1_4.value==this.w_CACODESE)
      this.oPgFrm.Page1.oPag.oCACODESE_1_4.value=this.w_CACODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADATREG_1_5.value==this.w_CADATREG)
      this.oPgFrm.Page1.oPag.oCADATREG_1_5.value=this.w_CADATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPMOV_1_8.RadioValue()==this.w_CATIPMOV)
      this.oPgFrm.Page1.oPag.oCATIPMOV_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_9.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_9.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAVALNAZ_1_11.value==this.w_CAVALNAZ)
      this.oPgFrm.Page1.oPag.oCAVALNAZ_1_11.value=this.w_CAVALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODPER_1_12.value==this.w_CACODPER)
      this.oPgFrm.Page1.oPag.oCACODPER_1_12.value=this.w_CACODPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_21.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_21.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oPERINI_1_22.value==this.w_PERINI)
      this.oPgFrm.Page1.oPag.oPERINI_1_22.value=this.w_PERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERFIN_1_23.value==this.w_PERFIN)
      this.oPgFrm.Page1.oPag.oPERFIN_1_23.value=this.w_PERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODREG_1_24.value==this.w_CACODREG)
      this.oPgFrm.Page1.oPag.oCACODREG_1_24.value=this.w_CACODREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREG_1_26.value==this.w_DESREG)
      this.oPgFrm.Page1.oPag.oDESREG_1_26.value=this.w_DESREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_2_6.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_2_6.value=this.w_DESVOC
      replace t_DESVOC with this.oPgFrm.Page1.oPag.oDESVOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_2_7.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_2_7.value=this.w_DESPIA
      replace t_DESPIA with this.oPgFrm.Page1.oPag.oDESPIA_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_2_8.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_2_8.value=this.w_DESCAN
      replace t_DESCAN with this.oPgFrm.Page1.oPag.oDESCAN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_39.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_39.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVOCCEN_2_1.value==this.w_CAVOCCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVOCCEN_2_1.value=this.w_CAVOCCEN
      replace t_CAVOCCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVOCCEN_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCEN_2_2.value==this.w_CACODCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCEN_2_2.value=this.w_CACODCEN
      replace t_CACODCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCEN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCOM_2_3.value==this.w_CACODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCOM_2_3.value=this.w_CACODCOM
      replace t_CACODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODCOM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPDAR_2_4.value==this.w_CAIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPDAR_2_4.value=this.w_CAIMPDAR
      replace t_CAIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPDAR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPAVE_2_5.value==this.w_CAIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPAVE_2_5.value=this.w_CAIMPAVE
      replace t_CAIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAIMPAVE_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'MOX_COAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CANUMREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCANUMREG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CANUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CACODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACODESE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CACODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CADATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCADATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CADATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CATIPMOV))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCATIPMOV_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CATIPMOV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CACODPER))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACODPER_1_12.SetFocus()
            i_bnoObbl = !empty(.w_CACODPER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPREG $ ' A')  and not(empty(.w_CACODREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACODREG_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Regola inesistente o di tipo contabile")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_CAVOCCEN) AND NOT EMPTY(.w_CACODCEN)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODESE = this.w_CACODESE
    this.o_CADATREG = this.w_CADATREG
    this.o_CAVALNAZ = this.w_CAVALNAZ
    this.o_PERINI = this.w_PERINI
    this.o_CAIMPDAR = this.w_CAIMPDAR
    this.o_CAIMPAVE = this.w_CAIMPAVE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_CAVOCCEN) AND NOT EMPTY(t_CACODCEN))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CAVOCCEN=space(15)
      .w_CACODCEN=space(15)
      .w_CACODCOM=space(15)
      .w_CAIMPDAR=0
      .w_CAIMPAVE=0
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .DoRTCalc(1,26,.f.)
      if not(empty(.w_CAVOCCEN))
        .link_2_1('Full')
      endif
      .DoRTCalc(27,27,.f.)
      if not(empty(.w_CACODCEN))
        .link_2_2('Full')
      endif
      .DoRTCalc(28,28,.f.)
      if not(empty(.w_CACODCOM))
        .link_2_3('Full')
      endif
        .w_CAIMPDAR = 0
        .w_CAIMPAVE = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_11.Calculate()
    endwith
    this.DoRTCalc(31,37,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CAVOCCEN = t_CAVOCCEN
    this.w_CACODCEN = t_CACODCEN
    this.w_CACODCOM = t_CACODCOM
    this.w_CAIMPDAR = t_CAIMPDAR
    this.w_CAIMPAVE = t_CAIMPAVE
    this.w_DESVOC = t_DESVOC
    this.w_DESPIA = t_DESPIA
    this.w_DESCAN = t_DESCAN
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CAVOCCEN with this.w_CAVOCCEN
    replace t_CACODCEN with this.w_CACODCEN
    replace t_CACODCOM with this.w_CACODCOM
    replace t_CAIMPDAR with this.w_CAIMPDAR
    replace t_CAIMPAVE with this.w_CAIMPAVE
    replace t_DESVOC with this.w_DESVOC
    replace t_DESPIA with this.w_DESPIA
    replace t_DESCAN with this.w_DESCAN
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTDAR = .w_TOTDAR-.w_caimpdar
        .w_TOTAVE = .w_TOTAVE-.w_caimpave
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mcaPag1 as StdContainer
  Width  = 689
  height = 428
  stdWidth  = 689
  stdheight = 428
  resizeXpos=349
  resizeYpos=327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCANUMREG_1_3 as StdField with uid="TLZHOOPLTE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CANUMREG", cQueryName = "CANUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo numero di registrazione",;
    HelpContextID = 23076717,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=108, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  add object oCACODESE_1_4 as StdField with uid="SIFKLTGWUK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CACODESE", cQueryName = "CACODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento della registrazione",;
    HelpContextID = 204902549,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=183, Top=12, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CACODESE"

  func oCACODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCACODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCADATREG_1_5 as StdField with uid="NURISORPKJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CADATREG", cQueryName = "CADATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 29065069,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=292, Top=12


  add object oCATIPMOV_1_8 as StdCombo with uid="NVVIVVWZFS",rtseq=8,rtrep=.f.,left=553,top=12,width=124,height=21;
    , ToolTipText = "Tipologia dei movimenti: effettivi/previsionali";
    , HelpContextID = 58425476;
    , cFormVar="w_CATIPMOV",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPMOV_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CATIPMOV,&i_cF..t_CATIPMOV),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'P',;
    space(1))))
  endfunc
  func oCATIPMOV_1_8.GetRadio()
    this.Parent.oContained.w_CATIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oCATIPMOV_1_8.ToRadio()
    this.Parent.oContained.w_CATIPMOV=trim(this.Parent.oContained.w_CATIPMOV)
    return(;
      iif(this.Parent.oContained.w_CATIPMOV=='E',1,;
      iif(this.Parent.oContained.w_CATIPMOV=='P',2,;
      0)))
  endfunc

  func oCATIPMOV_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCADESCRI_1_9 as StdField with uid="CZXUMXDPFG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 223379601,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=108, Top=42, InputMask=replicate('X',45)

  add object oCAVALNAZ_1_11 as StdField with uid="NVEJCOCUQO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CAVALNAZ", cQueryName = "CAVALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 46358656,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=553, Top=42, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CAVALNAZ"

  func oCAVALNAZ_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCACODPER_1_12 as StdField with uid="BFSRDAAKPQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CACODPER", cQueryName = "CACODPER",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di riferimento dei movimenti",;
    HelpContextID = 248082296,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=108, Top=72, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_CACODPER"

  func oCACODPER_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODPER_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODPER_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oCACODPER_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi di elaborazione",'GSBI_MCG.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oCACODPER_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_CACODPER
    i_obj.ecpSave()
  endproc

  add object oDESPER_1_21 as StdField with uid="IIUKCSXOML",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 254053578,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=246, Top=72, InputMask=replicate('X',35)

  add object oPERINI_1_22 as StdField with uid="AFZVXWDZBZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PERINI", cQueryName = "PERINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 127638538,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=598, Top=72

  add object oPERFIN_1_23 as StdField with uid="GSCKKQQCPY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PERFIN", cQueryName = "PERFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 49191946,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=598, Top=102

  add object oCACODREG_1_24 as StdField with uid="KEPJNFBXNV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CACODREG", cQueryName = "CACODREG",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Regola inesistente o di tipo contabile",;
    ToolTipText = "Regola di elaborazione utilizzata per la generazione (vuoto=manuale)",;
    HelpContextID = 13201261,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=108, Top=102, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="REG_EXTM", cZoomOnZoom="GSBI_MRE", oKey_1_1="RECODICE", oKey_1_2="this.w_CACODREG"

  func oCACODREG_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODREG_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODREG_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_EXTM','*','RECODICE',cp_AbsName(this.parent,'oCACODREG_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MRE',"Regole di elaborazione",'GSBI_MCA.REG_EXTM_VZM',this.parent.oContained
  endproc
  proc oCACODREG_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODICE=this.parent.oContained.w_CACODREG
    i_obj.ecpSave()
  endproc

  add object oDESREG_1_26 as StdField with uid="YOXAETGBCW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESREG", cQueryName = "DESREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 170036426,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=246, Top=102, InputMask=replicate('X',45)

  add object oSIMVAL_1_39 as StdField with uid="ZZZXGWQPZL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90105818,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=594, Top=42, InputMask=replicate('X',5)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=135, width=680,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CAVOCCEN",Label1="Voce di costo/ric.",Field2="CACODCEN",Label2="Centro di costo/ric.",Field3="CACODCOM",Label3="Commessa",Field4="CAIMPDAR",Label4="DARE",Field5="CAIMPAVE",Label5="AVERE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218964858

  add object oStr_1_15 as StdString with uid="NXGMXPICJH",Visible=.t., Left=1, Top=12,;
    Alignment=1, Width=105, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="PRSIDLAOKO",Visible=.t., Left=177, Top=12,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="QWXPLYBQJG",Visible=.t., Left=251, Top=12,;
    Alignment=1, Width=37, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="YWJXXHXMLW",Visible=.t., Left=1, Top=42,;
    Alignment=1, Width=105, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LWQILZTEQF",Visible=.t., Left=444, Top=12,;
    Alignment=1, Width=106, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="EEUGKXDPGE",Visible=.t., Left=1, Top=72,;
    Alignment=1, Width=105, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="BHJDSCDOZR",Visible=.t., Left=535, Top=72,;
    Alignment=1, Width=60, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="OUJUFMMIJO",Visible=.t., Left=534, Top=102,;
    Alignment=1, Width=61, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="YKTUBXRSPF",Visible=.t., Left=452, Top=42,;
    Alignment=1, Width=98, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SUCAMELOPM",Visible=.t., Left=4, Top=378,;
    Alignment=1, Width=93, Height=15,;
    Caption="Voce di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ZEYSTIPQRS",Visible=.t., Left=4, Top=405,;
    Alignment=1, Width=93, Height=15,;
    Caption="Centro di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HXHSJWCLSZ",Visible=.t., Left=379, Top=405,;
    Alignment=1, Width=86, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AXTBHYIMGR",Visible=.t., Left=1, Top=102,;
    Alignment=1, Width=105, Height=15,;
    Caption="Regola:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=156,;
    width=676+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=157,width=675+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|CENCOST|CAN_TIER|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESVOC_2_6.Refresh()
      this.Parent.oDESPIA_2_7.Refresh()
      this.Parent.oDESCAN_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oCAVOCCEN_2_1
      case cFile='CENCOST'
        oDropInto=this.oBodyCol.oRow.oCACODCEN_2_2
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oCACODCOM_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESVOC_2_6 as StdTrsField with uid="DHJGGRYSQU",rtseq=31,rtrep=.t.,;
    cFormVar="w_DESVOC",value=space(40),enabled=.f.,;
    HelpContextID = 226397386,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=101, Top=377, InputMask=replicate('X',40)

  add object oDESPIA_2_7 as StdTrsField with uid="BMPNBBXQLG",rtseq=32,rtrep=.t.,;
    cFormVar="w_DESPIA",value=space(40),enabled=.f.,;
    HelpContextID = 266636490,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=101, Top=405, InputMask=replicate('X',40)

  add object oDESCAN_2_8 as StdTrsField with uid="MWXPNACIRM",rtseq=33,rtrep=.t.,;
    cFormVar="w_DESCAN",value=space(30),enabled=.f.,;
    HelpContextID = 57773258,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=468, Top=405, InputMask=replicate('X',30)

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTDAR_3_1 as StdField with uid="IZULXFDXLY",rtseq=34,rtrep=.f.,;
    cFormVar="w_TOTDAR",value=0,enabled=.f.,;
    HelpContextID = 259027402,;
    cQueryName = "TOTDAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=379, Top=377, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTAVE_3_2 as StdField with uid="SUNOJXXZGB",rtseq=35,rtrep=.f.,;
    cFormVar="w_TOTAVE",value=0,enabled=.f.,;
    HelpContextID = 186872266,;
    cQueryName = "TOTAVE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=524, Top=377, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
enddefine

* --- Defining Body row
define class tgsbi_mcaBodyRow as CPBodyRowCnt
  Width=666
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCAVOCCEN_2_1 as StdTrsField with uid="ZELNUOJDHX",rtseq=26,rtrep=.t.,;
    cFormVar="w_CAVOCCEN",value=space(15),;
    ToolTipText = "Voce costo/ricavo:",;
    HelpContextID = 29007732,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=-2, Top=0, cSayPict=[p_MCE], cGetPict=[p_MCE], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_CAVOCCEN"

  func oCAVOCCEN_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAVOCCEN_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCAVOCCEN_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oCAVOCCEN_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'',this.parent.oContained
  endproc
  proc oCAVOCCEN_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_CAVOCCEN
    i_obj.ecpSave()
  endproc

  add object oCACODCEN_2_2 as StdTrsField with uid="CZVVHRHUSU",rtseq=27,rtrep=.t.,;
    cFormVar="w_CACODCEN",value=space(15),;
    ToolTipText = "Centro di costo o ricavo",;
    HelpContextID = 29978484,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=123, Top=0, cSayPict=[p_CEN], cGetPict=[p_CEN], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CACODCEN"

  func oCACODCEN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCEN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODCEN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCACODCEN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo o ricavo",'',this.parent.oContained
  endproc
  proc oCACODCEN_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CACODCEN
    i_obj.ecpSave()
  endproc

  add object oCACODCOM_2_3 as StdTrsField with uid="VMKROHJCPO",rtseq=28,rtrep=.t.,;
    cFormVar="w_CACODCOM",value=space(15),;
    ToolTipText = "Codice commessa",;
    HelpContextID = 238456973,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=248, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CACODCOM"

  func oCACODCOM_2_3.mCond()
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
  endfunc

  func oCACODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCACODCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCACODCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CACODCOM
    i_obj.ecpSave()
  endproc

  add object oCAIMPDAR_2_4 as StdTrsField with uid="QPIOSGVGHH",rtseq=29,rtrep=.t.,;
    cFormVar="w_CAIMPDAR",value=0,;
    ToolTipText = "Importo dare",;
    HelpContextID = 209203336,;
    cTotal = "this.Parent.oContained.w_totdar", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=373, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oCAIMPDAR_2_4.mCond()
    with this.Parent.oContained
      return (.w_CAIMPAVE=0)
    endwith
  endfunc

  add object oCAIMPAVE_2_5 as StdTrsField with uid="LYWUHQGVOR",rtseq=30,rtrep=.t.,;
    cFormVar="w_CAIMPAVE",value=0,;
    ToolTipText = "Importo avere",;
    HelpContextID = 8900459,;
    cTotal = "this.Parent.oContained.w_totave", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=519, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oCAIMPAVE_2_5.mCond()
    with this.Parent.oContained
      return (.w_CAIMPDAR=0)
    endwith
  endfunc

  add object oObj_2_9 as cp_runprogram with uid="BTLNCUUWPD",width=113,height=23,;
   left=12, top=283,;
    caption='GSBI_BCA(V)',;
   bGlobalFont=.t.,;
    prg="GSBI_BCA('V')",;
    cEvent = "w_CAVOCCEN Changed",;
    nPag=2;
    , HelpContextID = 227124697

  add object oObj_2_10 as cp_runprogram with uid="QJMMWXTVQM",width=113,height=23,;
   left=12, top=305,;
    caption='GSBI_BCA(R)',;
   bGlobalFont=.t.,;
    prg="GSBI_BCA('R')",;
    cEvent = "w_CACODCEN Changed",;
    nPag=2;
    , HelpContextID = 227125721

  add object oObj_2_11 as cp_runprogram with uid="ICJZFOLLFX",width=113,height=23,;
   left=12, top=327,;
    caption='GSBI_BCA(M)',;
   bGlobalFont=.t.,;
    prg="GSBI_BCA('M')",;
    cEvent = "w_CACODCOM Changed",;
    nPag=2;
    , HelpContextID = 227127001
  add object oLast as LastKeyMover
  * ---
  func oCAVOCCEN_2_1.When()
    return(.t.)
  proc oCAVOCCEN_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCAVOCCEN_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mca','MOX_COAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CASERIAL=MOX_COAM.CASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
