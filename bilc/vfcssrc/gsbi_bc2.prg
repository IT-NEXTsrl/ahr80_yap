* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bc2                                                        *
*              Calcola confronto bilancio contabile                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_29]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2013-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bc2",oParentObject)
return(i_retval)

define class tgsbi_bc2 as StdBatch
  * --- Local variables
  w_CPROWORD = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciata dalla stampa Confronto tra Bilanci (GSBI_SBC)
    * --- Lancio la query che legge i dati dal primo bilancio.
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_SCB",this,"BILAN1")
    * --- Lancio la query che legge i dati dal secondo bilancio.
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI1SCB",this,"BILAN2")
    * --- Controllo se voglio stampare solo le righe valorizzate
    if this.oParentObject.w_RIGHESI="S"
      * --- Metto in join i due cursori che sono stati creati.Effettuo una right join sia con il campo di riga sia con il codice della voce.
      * --- Controllo che almeno uno dei due importi dei cursori sia maggiore di zero.
      SELECT BILAN2.ROWORD AS ROWORD , BILAN2.DESCRI AS DESCRI,BILAN2.CODVOC AS CODVOC ,BILAN1.CPROWORD AS CPROWORD,;
      BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1,BILAN2.VRTIPDRI AS TIPDRI2;
      FROM BILAN1 RIGHT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC ;
       WHERE BILAN1.MRIMPORT<>0 OR BILAN2.IMPORT<>0 ;
      OR BILAN1.VRFLDESC="D" ;
      GROUP BY ROWORD,DESCRI,CODVOC,IMPORT,MRIMPORT,BILAN2.VRTIPDRI ;
      INTO CURSOR BILANCIO1
    else
      * --- Metto in join i due cursori che sono stati creati.Effettuo una right join sia con il campo di riga sia con il codice della voce
      SELECT BILAN2.ROWORD AS ROWORD , BILAN2.DESCRI AS DESCRI,BILAN2.CODVOC AS CODVOC,BILAN1.CPROWORD AS CPROWORD,;
      BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1,BILAN2.VRTIPDRI AS TIPDRI2;
      FROM BILAN1 RIGHT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC ;
      GROUP BY ROWORD,DESCRI,CODVOC,IMPORT,MRIMPORT,BILAN2.VRTIPDRI ;
       INTO CURSOR BILANCIO1
    endif
    * --- Se il bilancio 1 ha pi� voci del bilancio 2 allora nel cursore risultante dalla 'Select .. Outer Join'  mancheranno alcune voci del bilancio 1
    *     Bisogna scandire il cursore risultante dalla unione dei 2 bilanci e riposizionare le voci mancanti nella posizione corretta (corretta rispetto alla strutura di bilancio a cui si riferisce)
    * --- Estrae tutte le voci di Bilancio che si sarebbero perse a causa della 'right outer join' fatta al passo precedente
    SELECT BILAN2.ROWORD AS ROWORD , BILAN1.MRDESCRI AS DESCRI,BILAN1.MRCODVOC AS CODVOC ,BILAN1.CPROWORD AS CPROWORD,; 
 BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1, BILAN1.VRTIPDRI AS TIPDRI2; 
 FROM BILAN1 LEFT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC ; 
 where BILAN1.MRCODVOC not in (Select CODVOC FROM BILANCIO1) AND( BILAN1.MRIMPORT<>0 OR BILAN2.IMPORT<>0 OR BILAN1.VRFLDESC="D") INTO CURSOR BILANCIO2 
    * --- Crea il cursore Bilancio  che conterr� tutte le voci (inizialmente vuoto)
    SELECT BILAN2.ROWORD AS ROWORD , BILAN2.DESCRI AS DESCRI,BILAN2.CODVOC AS CODVOC,BILAN1.CPROWORD AS CPROWORD,;
    BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1,BILAN2.VRTIPDRI AS TIPDRI2;
    FROM BILAN1 RIGHT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC AND;
    BILAN1.CPROWORD=BILAN2.ROWORD where 1=0 INTO CURSOR BILANCIO
    WRCURSOR ("BILANCIO")
    * --- Carica il cursore Bilancio  che contine tutte le voci e mantiene la struttura dei bilanci
    SELECT BILANCIO1 
 Go Top 
 SELECT BILANCIO2 
 Go Top 
 SCAN 
    * --- Scandisce il cursore contenente le voci del bilancio1 cercando il punto in cui inserire le voci mancanti (rispettando la struttura di bilancio)
    SELECT BILANCIO1
    do while NOT EOF ()
      this.w_CPROWORD = BILANCIO1.ROWORD
      if BILANCIO2.CPROWORD < BILANCIO1.CPROWORD
        exit
      else
        SCATTER MEMVAR MEMO
        INSERT INTO BILANCIO FROM MEMVAR
      endif
      SKIP 1
    enddo
    SELECT BILANCIO2 
 SCATTER MEMVAR MEMO 
 INSERT INTO BILANCIO FROM MEMVAR 
 SELECT BILANCIO 
 REPLACE ROWORD with this.w_CPROWORD
    ENDSCAN
    * --- Inserisce le voci non inserite al passo precedente
    SELECT BILANCIO1
    do while NOT EOF ()
      SCATTER MEMVAR MEMO
      INSERT INTO BILANCIO FROM MEMVAR
      SKIP 1
    enddo
    * --- Passo il cursore BILANCIO al cursore __TMP__ destinato alla stampa
    SELECT * FROM BILANCIO INTO CURSOR __TMP__ ORDER BY ROWORD,CPROWORD
    * --- Controllo se le valute dei due bilanci sono identiche.
    if this.oParentObject.w_VALAPP=this.oParentObject.w_VALAPP2
      * --- Valute dei bilanci identiche.Controllo se sono identiche alla valuta di stampa.
      if this.oParentObject.w_VALAPP=this.oParentObject.w_DIVISA
        * --- Valute identiche non applico nessun cambio.
        L_MOLTI=1
        L_MOLTI2=1
        L_CAMVAL=1
        L_CAMVAL2=1
      else
        * --- Valute bilancio differenti alla valuta di stampa.Effettuo i dovuti cambi.
        L_MOLTI=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_MOLTI2=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL=GETCAM(this.oParentObject.w_VALAPP,i_DATSYS,0)
        L_CAMVAL2=GETCAM(this.oParentObject.w_VALAPP2,i_DATSYS,0)
      endif
    else
      * --- Valute dei bilanci differenti.
      * --- Controllo che la valuta del primo bilancio sia uguale a quella di stampa.
      if this.oParentObject.w_VALAPP=this.oParentObject.w_DIVISA
        * --- Non applico nessun cambio.
        L_MOLTI=1
        L_CAMVAL=1
      else
        * --- La valuta del primo bilancio � differente da quella di stampa.
        * --- Effettuo i dovuti cambi.
        L_MOLTI=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL=GETCAM(this.oParentObject.w_VALAPP,i_DATSYS,0)
      endif
      * --- Controllo che la valuta del primo bilancio sia uguale a quella di stampa.
      if this.oParentObject.w_VALAPP2=this.oParentObject.w_DIVISA
        * --- Non applico nessun cambio.
        L_MOLTI2=1
        L_CAMVAL2=1
      else
        * --- La valuta del secondo bilancio � differente da quella di stampa.
        * --- Effettuo i dovuti cambi.
        L_MOLTI2=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL2=GETCAM(this.oParentObject.w_VALAPP2,i_DATSYS,0)
      endif
    endif
    * --- Formato importi
    L_DECIMI=this.oParentObject.w_DECIMI
    L_DIVISA=this.oParentObject.w_DIVISA
    L_SIMVAL=this.oParentObject.w_SIMVAL
    s=20*(l_decimi+2)
    L_NUMERO=this.oParentObject.w_NUMERO
    L_ESERCI=this.oParentObject.w_ESERCI
    L_DESCRIBI=this.oParentObject.w_DESCRIBI
    L_NUMERO2=this.oParentObject.w_NUMERO2
    L_ESERCI2=this.oParentObject.w_ESERCI2
    L_DESCRIB2=this.oParentObject.w_DESCRIB2
    L_PERIODO=this.oParentObject.w_PERIODO
    L_PERIODO2=this.oParentObject.w_PERIODO2
    L_CAMBIO=this.oParentObject.w_CAMVAL
    if this.oParentObject.w_TIPOSTAM="S"
      * --- Stampa Standard
      * --- Lancio il report assocciato
      CP_CHPRN("..\BILC\EXE\QUERY\GSBI_SCB.FRX", " ", this)
    else
      * --- Su EXCEL - Creo il cursore da quello di stampa
      select ROWORD AS ROWORD , DESCRI as DESCRI, CODVOC AS CODVOC, TIPDRI2 AS TIPDRI2,;
      IIF(TIPDRI2="M", (cp_Round((IMPORTO1/l_camval)*l_molti,this.oParentObject.w_decimi)), cp_round(IMPORTO1, this.oParentObject.w_decimi)) AS IMPORTO1,;
      IIF(TIPDRI2="M", (cp_Round((IMPORTO2/l_camval2)*l_molti2,this.oParentObject.w_decimi)), cp_round(IMPORTO2, this.oParentObject.w_decimi)) AS IMPORTO2,"CONTABILE" as TIPO ;
      FROM __TMP__ INTO CURSOR __TMP__ ORDER BY 1,2
      * --- Lancio la stampa su Excell
      CP_CHPRN("..\BILC\EXE\QUERY\GSBIESCB.XLT", " ", this)
    endif
    if this.oParentObject.w_divisa<>g_perval
      this.oParentObject.w_DIVISA = space(3)
      this.oParentObject.w_SIMVAL = space(5)
      this.oParentObject.w_CAMVAL = 0
    endif
    if USED("BILAN1")
      SELECT BILAN1
      USE
    endif
    if USED("BILAN2")
      SELECT BILAN2
      USE
    endif
    if USED("BILANCIO")
      SELECT BILANCIO
      USE
    endif
    if USED("BILANCIO1")
      SELECT BILANCIO1
      USE
    endif
    if USED("BILANCIO2")
      SELECT BILANCIO2
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
