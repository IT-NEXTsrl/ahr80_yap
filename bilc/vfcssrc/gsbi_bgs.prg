* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bgs                                                        *
*              Treev. strutture bilancio                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_170]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2014-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bgs",oParentObject,m.pTipOp)
return(i_retval)

define class tgsbi_bgs as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_TRCODICE = space(15)
  ORDINA = space(10)
  w_CPROWORD = 0
  w_SEQELA = 0
  w_CODVOC = space(15)
  w_DESCRI = space(50)
  w_NWROWORD = 0
  w_NWSEQELA = 0
  w_NWCODVOC = space(15)
  w_NWDESCRI = space(50)
  w_VOCE = space(15)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  STRDANAC_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView
    * --- Se treeview aperta e gestione sotto stante chiusa
    if Type("this.oParentObject.oParentObject.w_TRCODICE")="U"
      i_retcode = 'stop'
      return
    endif
    this.w_TRCODICE = this.oParentObject.oParentObject.w_TRCODICE
    if this.pTipOp = "Ord"
      * --- Elimina i cursore
      if used(this.oParentObject.w_CURSORNA)
        select ( this.oParentObject.w_CURSORNA ) 
        use
      endif
      if this.oParentObject.w_TIPORD = "S"
        this.ORDINA = "SEQELA"
      else
        this.ORDINA = "CPROWORD"
      endif
      this.pTipOp = "Reload"
    else
      this.ORDINA = "SEQELA"
    endif
    if this.pTipOp = "Click" AND (this.oParentObject.oParentObject.cFunction="Load" OR this.oParentObject.oParentObject.cFunction="Edit")
      * --- Premuto tasto destro per modifica numero riga o modifica numero sequenza
      this.w_CPROWORD = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("CPROWORD") , 0 )
      this.w_SEQELA = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("SEQELA") , 0 )
      this.w_CODVOC = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("VRCODVOC") , Space(15) )
      this.w_DESCRI = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("DESCRI") , Space(50) )
      this.w_NWROWORD = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("CPROWORD") , 0 )
      this.w_NWSEQELA = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("SEQELA") , 0 )
      this.w_NWCODVOC = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("VRCODVOC") ,Space(15) )
      this.w_NWDESCRI = Nvl ( this.oParentObject.w_TREEVIEW.GETVAR("DESCRI") , Space(50) )
      do GSBI_KMR with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_NWSEQELA<>0 AND this.w_NWROWORD<>0 AND this.w_NWCODVOC<>" "
        UPDATE (this.oParentObject.oParentObject.cTrsName) ;
        SET t_CPROWORD=this.w_NWROWORD, t_TRSEQELA=this.w_NWSEQELA, ;
        t_TRCODVOC=this.w_NWCODVOC, t_TRDESDET=this.w_NWDESCRI, I_SRV="U" ;
        WHERE t_CPROWORD=this.w_CPROWORD AND t_TRSEQELA=this.w_SEQELA
      endif
      this.pTipOp = "Reload"
      * --- Elimina i cursore
      if used(this.oParentObject.w_CURSORNA)
        select ( this.oParentObject.w_CURSORNA ) 
        use
      endif
    endif
    do case
      case this.pTipop="DClick"
        this.w_VOCE = Nvl( this.oParentObject.w_TREEVIEW.GETVAR("VRCODVOC") , Space(15) )
        this.w_OBJECT = GSBI_AVC()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_VRCODVOC = this.w_VOCE
        this.w_OBJECT.QueryKeySet("VRCODVOC='"+this.w_VOCE+"'","")     
        this.w_OBJECT.LoadRecWarn()     
      case this.pTipop="Reload"
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        L_NomeCursore = this.oParentObject.w_CURSORNA
         
 CREATE CURSOR &L_NomeCursore (CPROWORD N(6), CODICE C(15), VRCODVOC C(15), ; 
 FLMACO C(6), SEQELA N(6), DESCRI C(40), CAMPO C(30))
        * --- Reperimento dati su Voci di bilancio
        VQ_EXEC("..\BILC\EXE\QUERY\GSBI_BGS.VQR",this,"CursVoci")
        * --- Reperisco i dati dalla maschera padre
         
 SELECT * FROM (this.oParentObject.oParentObject.cTrsName) ; 
 INTO CURSOR CursStrut WHERE NOT EMPTY(NVL(t_TRCODVOC,"")) ; 
 ORDER BY t_CPROWORD NOFILTER
        * --- Seleziono solo i valori utili
         
 SELECT DISTINCT CursStrut.t_CPROWORD, CursStrut.t_TRSEQELA, CursStrut.t_TRDESDET,CursVoci.* ; 
 FROM (CursStrut Left Outer Join CursVoci on CursVoci.VRCODVOC=CursStrut.t_TRCODVOC) ; 
 INTO CURSOR CursSelez NOFILTER
        SELECT CursSelez
        GO TOP
        SCAN
         
 INSERT INTO &L_NomeCursore (CPROWORD,CODICE,VRCODVOC,FLMACO,SEQELA,DESCRI,CAMPO) ; 
 VALUES (CursSelez.t_CPROWORD, ; 
 IIF(CursSelez.DVFLMACO="V",NVL(CursSelez.DVCODVOC,""), ; 
 IIF(CursSelez.DVFLMACO="M", ; 
 NVL(CursSelez.DVCODMAS," "),NVL(CursSelez.DVCODCON," "))), ; 
 CursSelez.VRCODVOC, ; 
 IIF(CursSelez.DVFLMACO="V",Ah_MsgFormat("Voce"),IIF(CursSelez.DVFLMACO="M", ; 
 Ah_MsgFormat("Mastro"),IIF(CursSelez.DVFLMACO="G",Ah_MsgFormat("Conto"), " "))), ; 
 CursSelez.t_TRSEQELA,CursSelez.t_TRDESDET,CursSelez.VRCODVOC) 
 
        ENDSCAN
        * --- Costruisco con una query contentente la base per l'esplosione (Il nodo radice)
        SELECT ( L_NomeCursore )
        GO TOP
        cp_Level1( this.oParentObject.w_CURSORNA ,ALLTRIM(this.ORDINA),"")
        DELETE FROM &L_NomeCursore WHERE &L_NomeCursore..CODICE=" " AND SUBSTR(&L_NomeCursore..LVLKEY,14,1)="1"
        UPDATE &L_NomeCursore SET &L_NomeCursore..CAMPO=ALLTRIM(&L_NomeCursore..FLMACO)+": "+ALLTRIM(&L_NomeCursore..CODICE) ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)="."
        if used("CursStrut")
          select CursStrut
          use
        endif
        if used("CursVoci")
          select CursVoci
          use
        endif
        if used("CursSelez")
          select CursSelez
          use
        endif
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
        this.oParentObject.w_TREEVIEW.Filltree()     
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used( this.oParentObject.w_CURSORNA )
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRDANAC'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
