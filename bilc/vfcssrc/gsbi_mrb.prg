* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mrb                                                        *
*              Righe di bilanci contabili                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsbi_mrb")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsbi_mrb")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsbi_mrb")
  return

* --- Class definition
define class tgsbi_mrb as StdPCForm
  Width  = 737
  Height = 306
  Top    = 24
  Left   = 47
  cComment = "Righe di bilanci contabili"
  cPrg = "gsbi_mrb"
  HelpContextID=113903209
  add object cnt as tcgsbi_mrb
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsbi_mrb as PCContext
  w_MRNUMBIL = 0
  w_MRCODESE = space(4)
  w_CPROWORD = 0
  w_MRCODVOC = space(15)
  w_VRFLDESC = space(1)
  w_MRDESCRI = space(90)
  w_MR__NOTE = space(10)
  w_MRIMPORT = 0
  w_MRPERCEN = 0
  w_MR__FONT = space(1)
  w_MR__FTST = space(1)
  w_MR__COLO = 0
  w_MR__NUCO = 0
  w_MR__INDE = 0
  w_MRRAGCDC = space(15)
  w_MRCODVOA = space(15)
  w_DTOBSO = space(8)
  w_DATAOBSO = space(8)
  proc Save(i_oFrom)
    this.w_MRNUMBIL = i_oFrom.w_MRNUMBIL
    this.w_MRCODESE = i_oFrom.w_MRCODESE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MRCODVOC = i_oFrom.w_MRCODVOC
    this.w_VRFLDESC = i_oFrom.w_VRFLDESC
    this.w_MRDESCRI = i_oFrom.w_MRDESCRI
    this.w_MR__NOTE = i_oFrom.w_MR__NOTE
    this.w_MRIMPORT = i_oFrom.w_MRIMPORT
    this.w_MRPERCEN = i_oFrom.w_MRPERCEN
    this.w_MR__FONT = i_oFrom.w_MR__FONT
    this.w_MR__FTST = i_oFrom.w_MR__FTST
    this.w_MR__COLO = i_oFrom.w_MR__COLO
    this.w_MR__NUCO = i_oFrom.w_MR__NUCO
    this.w_MR__INDE = i_oFrom.w_MR__INDE
    this.w_MRRAGCDC = i_oFrom.w_MRRAGCDC
    this.w_MRCODVOA = i_oFrom.w_MRCODVOA
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_DATAOBSO = i_oFrom.w_DATAOBSO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MRNUMBIL = this.w_MRNUMBIL
    i_oTo.w_MRCODESE = this.w_MRCODESE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MRCODVOC = this.w_MRCODVOC
    i_oTo.w_VRFLDESC = this.w_VRFLDESC
    i_oTo.w_MRDESCRI = this.w_MRDESCRI
    i_oTo.w_MR__NOTE = this.w_MR__NOTE
    i_oTo.w_MRIMPORT = this.w_MRIMPORT
    i_oTo.w_MRPERCEN = this.w_MRPERCEN
    i_oTo.w_MR__FONT = this.w_MR__FONT
    i_oTo.w_MR__FTST = this.w_MR__FTST
    i_oTo.w_MR__COLO = this.w_MR__COLO
    i_oTo.w_MR__NUCO = this.w_MR__NUCO
    i_oTo.w_MR__INDE = this.w_MR__INDE
    i_oTo.w_MRRAGCDC = this.w_MRRAGCDC
    i_oTo.w_MRCODVOA = this.w_MRCODVOA
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_DATAOBSO = this.w_DATAOBSO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsbi_mrb as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 737
  Height = 306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=113903209
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DETTBILA_IDX = 0
  VOC_ANAC_IDX = 0
  VOC_BIAN_IDX = 0
  RAGMCENC_IDX = 0
  cFile = "DETTBILA"
  cKeySelect = "MRNUMBIL,MRCODESE"
  cKeyWhere  = "MRNUMBIL=this.w_MRNUMBIL and MRCODESE=this.w_MRCODESE"
  cKeyDetail  = "MRNUMBIL=this.w_MRNUMBIL and MRCODESE=this.w_MRCODESE"
  cKeyWhereODBC = '"MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';

  cKeyDetailWhereODBC = '"MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DETTBILA.MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)';
      +'+" and DETTBILA.MRCODESE="+cp_ToStrODBC(this.w_MRCODESE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DETTBILA.CPROWORD '
  cPrg = "gsbi_mrb"
  cComment = "Righe di bilanci contabili"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MRNUMBIL = 0
  w_MRCODESE = space(4)
  w_CPROWORD = 0
  w_MRCODVOC = space(15)
  w_VRFLDESC = space(1)
  w_MRDESCRI = space(90)
  w_MR__NOTE = space(0)
  w_MRIMPORT = 0
  w_MRPERCEN = 0
  w_MR__FONT = space(1)
  w_MR__FTST = space(1)
  w_MR__COLO = 0
  w_MR__NUCO = 0
  w_MR__INDE = 0
  w_MRRAGCDC = space(15)
  w_MRCODVOA = space(15)
  w_DTOBSO = ctod('  /  /  ')
  w_DATAOBSO = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mrbPag1","gsbi_mrb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VOC_ANAC'
    this.cWorkTables[2]='VOC_BIAN'
    this.cWorkTables[3]='RAGMCENC'
    this.cWorkTables[4]='DETTBILA'
    * --- Area Manuale = Open Work Table
    * --- gsbi_mrb
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.DynamicBackColor= ;
         "IIF(t_MR__COLO=1,RGB(255,255,255), ;
              IIF(t_MR__COLO=2,RGB(255,128,128),RGB(128,128,255)))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DETTBILA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DETTBILA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsbi_mrb'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DETTBILA where MRNUMBIL=KeySet.MRNUMBIL
    *                            and MRCODESE=KeySet.MRCODESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DETTBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2],this.bLoadRecFilter,this.DETTBILA_IDX,"gsbi_mrb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DETTBILA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DETTBILA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DETTBILA '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRNUMBIL',this.w_MRNUMBIL  ,'MRCODESE',this.w_MRCODESE  )
      select * from (i_cTable) DETTBILA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTOBSO = i_datsys
        .w_MRNUMBIL = NVL(MRNUMBIL,0)
        .w_MRCODESE = NVL(MRCODESE,space(4))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DETTBILA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_VRFLDESC = space(1)
          .w_DATAOBSO = ctod("  /  /  ")
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MRCODVOC = NVL(MRCODVOC,space(15))
          if link_2_2_joined
            this.w_MRCODVOC = NVL(VRCODVOC202,NVL(this.w_MRCODVOC,space(15)))
            this.w_MRDESCRI = NVL(VRDESVOC202,space(90))
            this.w_MR__NOTE = NVL(VR__NOTE202,space(0))
            this.w_VRFLDESC = NVL(VRFLDESC202,space(1))
            this.w_DATAOBSO = NVL(cp_ToDate(VRDTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          .w_MRDESCRI = NVL(MRDESCRI,space(90))
          .w_MR__NOTE = NVL(MR__NOTE,space(0))
          .w_MRIMPORT = NVL(MRIMPORT,0)
          .w_MRPERCEN = NVL(MRPERCEN,0)
          .w_MR__FONT = NVL(MR__FONT,space(1))
          .w_MR__FTST = NVL(MR__FTST,space(1))
          .w_MR__COLO = NVL(MR__COLO,0)
          .w_MR__NUCO = NVL(MR__NUCO,0)
          .w_MR__INDE = NVL(MR__INDE,0)
          .w_MRRAGCDC = NVL(MRRAGCDC,space(15))
          * evitabile
          *.link_2_15('Load')
          .w_MRCODVOA = NVL(MRCODVOA,space(15))
          * evitabile
          *.link_2_16('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MRNUMBIL=0
      .w_MRCODESE=space(4)
      .w_CPROWORD=10
      .w_MRCODVOC=space(15)
      .w_VRFLDESC=space(1)
      .w_MRDESCRI=space(90)
      .w_MR__NOTE=space(0)
      .w_MRIMPORT=0
      .w_MRPERCEN=0
      .w_MR__FONT=space(1)
      .w_MR__FTST=space(1)
      .w_MR__COLO=0
      .w_MR__NUCO=0
      .w_MR__INDE=0
      .w_MRRAGCDC=space(15)
      .w_MRCODVOA=space(15)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DATAOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_MRCODVOC))
         .link_2_2('Full')
        endif
        .DoRTCalc(5,9,.f.)
        .w_MR__FONT = 'A'
        .w_MR__FTST = 'N'
        .DoRTCalc(12,14,.f.)
        .w_MRRAGCDC = space(15)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MRRAGCDC))
         .link_2_15('Full')
        endif
        .w_MRCODVOA = space(15)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MRCODVOA))
         .link_2_16('Full')
        endif
        .w_DTOBSO = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DETTBILA')
    this.DoRTCalc(18,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMR__FONT_2_8.enabled = i_bVal
      .Page1.oPag.oMR__FTST_2_9.enabled = i_bVal
      .Page1.oPag.oMR__COLO_2_10.enabled = i_bVal
      .Page1.oPag.oMR__NUCO_2_11.enabled = i_bVal
      .Page1.oPag.oMR__INDE_2_13.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DETTBILA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DETTBILA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRNUMBIL,"MRNUMBIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODESE,"MRCODESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MRCODVOC C(15);
      ,t_MRDESCRI C(90);
      ,t_MRIMPORT N(18,4);
      ,t_MRPERCEN N(6,2);
      ,t_MR__FONT N(3);
      ,t_MR__FTST N(3);
      ,t_MR__COLO N(3);
      ,t_MR__NUCO N(3);
      ,t_MR__INDE N(3);
      ,CPROWNUM N(10);
      ,t_VRFLDESC C(1);
      ,t_MR__NOTE M(10);
      ,t_MRRAGCDC C(15);
      ,t_MRCODVOA C(15);
      ,t_DATAOBSO D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mrbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_2.controlsource=this.cTrsName+'.t_MRCODVOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRDESCRI_2_4.controlsource=this.cTrsName+'.t_MRDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_6.controlsource=this.cTrsName+'.t_MRIMPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRPERCEN_2_7.controlsource=this.cTrsName+'.t_MRPERCEN'
    this.oPgFRm.Page1.oPag.oMR__FONT_2_8.controlsource=this.cTrsName+'.t_MR__FONT'
    this.oPgFRm.Page1.oPag.oMR__FTST_2_9.controlsource=this.cTrsName+'.t_MR__FTST'
    this.oPgFRm.Page1.oPag.oMR__COLO_2_10.controlsource=this.cTrsName+'.t_MR__COLO'
    this.oPgFRm.Page1.oPag.oMR__NUCO_2_11.controlsource=this.cTrsName+'.t_MR__NUCO'
    this.oPgFRm.Page1.oPag.oMR__INDE_2_13.controlsource=this.cTrsName+'.t_MR__INDE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(76)
    this.AddVLine(199)
    this.AddVLine(489)
    this.AddVLine(648)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DETTBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DETTBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2])
      *
      * insert into DETTBILA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DETTBILA')
        i_extval=cp_InsertValODBCExtFlds(this,'DETTBILA')
        i_cFldBody=" "+;
                  "(MRNUMBIL,MRCODESE,CPROWORD,MRCODVOC,MRDESCRI"+;
                  ",MR__NOTE,MRIMPORT,MRPERCEN,MR__FONT,MR__FTST"+;
                  ",MR__COLO,MR__NUCO,MR__INDE,MRRAGCDC,MRCODVOA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MRNUMBIL)+","+cp_ToStrODBC(this.w_MRCODESE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MRCODVOC)+","+cp_ToStrODBC(this.w_MRDESCRI)+;
             ","+cp_ToStrODBC(this.w_MR__NOTE)+","+cp_ToStrODBC(this.w_MRIMPORT)+","+cp_ToStrODBC(this.w_MRPERCEN)+","+cp_ToStrODBC(this.w_MR__FONT)+","+cp_ToStrODBC(this.w_MR__FTST)+;
             ","+cp_ToStrODBC(this.w_MR__COLO)+","+cp_ToStrODBC(this.w_MR__NUCO)+","+cp_ToStrODBC(this.w_MR__INDE)+","+cp_ToStrODBCNull(this.w_MRRAGCDC)+","+cp_ToStrODBCNull(this.w_MRCODVOA)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DETTBILA')
        i_extval=cp_InsertValVFPExtFlds(this,'DETTBILA')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MRNUMBIL',this.w_MRNUMBIL,'MRCODESE',this.w_MRCODESE)
        INSERT INTO (i_cTable) (;
                   MRNUMBIL;
                  ,MRCODESE;
                  ,CPROWORD;
                  ,MRCODVOC;
                  ,MRDESCRI;
                  ,MR__NOTE;
                  ,MRIMPORT;
                  ,MRPERCEN;
                  ,MR__FONT;
                  ,MR__FTST;
                  ,MR__COLO;
                  ,MR__NUCO;
                  ,MR__INDE;
                  ,MRRAGCDC;
                  ,MRCODVOA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MRNUMBIL;
                  ,this.w_MRCODESE;
                  ,this.w_CPROWORD;
                  ,this.w_MRCODVOC;
                  ,this.w_MRDESCRI;
                  ,this.w_MR__NOTE;
                  ,this.w_MRIMPORT;
                  ,this.w_MRPERCEN;
                  ,this.w_MR__FONT;
                  ,this.w_MR__FTST;
                  ,this.w_MR__COLO;
                  ,this.w_MR__NUCO;
                  ,this.w_MR__INDE;
                  ,this.w_MRRAGCDC;
                  ,this.w_MRCODVOA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DETTBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 AND (t_MRDESCRI<>space(90) OR t_MRCODVOC<>space(15))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DETTBILA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DETTBILA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND (t_MRDESCRI<>space(90) OR t_MRCODVOC<>space(15))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DETTBILA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DETTBILA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MRCODVOC="+cp_ToStrODBCNull(this.w_MRCODVOC)+;
                     ",MRDESCRI="+cp_ToStrODBC(this.w_MRDESCRI)+;
                     ",MR__NOTE="+cp_ToStrODBC(this.w_MR__NOTE)+;
                     ",MRIMPORT="+cp_ToStrODBC(this.w_MRIMPORT)+;
                     ",MRPERCEN="+cp_ToStrODBC(this.w_MRPERCEN)+;
                     ",MR__FONT="+cp_ToStrODBC(this.w_MR__FONT)+;
                     ",MR__FTST="+cp_ToStrODBC(this.w_MR__FTST)+;
                     ",MR__COLO="+cp_ToStrODBC(this.w_MR__COLO)+;
                     ",MR__NUCO="+cp_ToStrODBC(this.w_MR__NUCO)+;
                     ",MR__INDE="+cp_ToStrODBC(this.w_MR__INDE)+;
                     ",MRRAGCDC="+cp_ToStrODBCNull(this.w_MRRAGCDC)+;
                     ",MRCODVOA="+cp_ToStrODBCNull(this.w_MRCODVOA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DETTBILA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MRCODVOC=this.w_MRCODVOC;
                     ,MRDESCRI=this.w_MRDESCRI;
                     ,MR__NOTE=this.w_MR__NOTE;
                     ,MRIMPORT=this.w_MRIMPORT;
                     ,MRPERCEN=this.w_MRPERCEN;
                     ,MR__FONT=this.w_MR__FONT;
                     ,MR__FTST=this.w_MR__FTST;
                     ,MR__COLO=this.w_MR__COLO;
                     ,MR__NUCO=this.w_MR__NUCO;
                     ,MR__INDE=this.w_MR__INDE;
                     ,MRRAGCDC=this.w_MRRAGCDC;
                     ,MRCODVOA=this.w_MRCODVOA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DETTBILA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND (t_MRDESCRI<>space(90) OR t_MRCODVOC<>space(15))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DETTBILA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND (t_MRDESCRI<>space(90) OR t_MRCODVOC<>space(15))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DETTBILA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DETTBILA_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,14,.t.)
          .link_2_15('Full')
          .link_2_16('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_VRFLDESC with this.w_VRFLDESC
      replace t_MR__NOTE with this.w_MR__NOTE
      replace t_MRRAGCDC with this.w_MRRAGCDC
      replace t_MRCODVOA with this.w_MRCODVOA
      replace t_DATAOBSO with this.w_DATAOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRIMPORT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRIMPORT_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRPERCEN_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRPERCEN_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODVOC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_ANAC_IDX,3]
    i_lTable = "VOC_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2], .t., this.VOC_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVC',True,'VOC_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_MRCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_MRCODVOC))
          select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVOC)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_ANAC','*','VRCODVOC',cp_AbsName(oSource.parent,'oMRCODVOC_2_2'),i_cWhere,'GSBI_AVC',"Voci di bilancio",'GSBI_AVC.VOC_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_MRCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_MRCODVOC)
            select VRCODVOC,VRDESVOC,VR__NOTE,VRFLDESC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOC = NVL(_Link_.VRCODVOC,space(15))
      this.w_MRDESCRI = NVL(_Link_.VRDESVOC,space(90))
      this.w_MR__NOTE = NVL(_Link_.VR__NOTE,space(0))
      this.w_VRFLDESC = NVL(_Link_.VRFLDESC,space(1))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.VRDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOC = space(15)
      endif
      this.w_MRDESCRI = space(90)
      this.w_MR__NOTE = space(0)
      this.w_VRFLDESC = space(1)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_DTOBSO OR .cFunction='Query'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di ricavo obsoleta")
        endif
        this.w_MRCODVOC = space(15)
        this.w_MRDESCRI = space(90)
        this.w_MR__NOTE = space(0)
        this.w_VRFLDESC = space(1)
        this.w_DATAOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_ANAC_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.VRCODVOC as VRCODVOC202"+ ",link_2_2.VRDESVOC as VRDESVOC202"+ ",link_2_2.VR__NOTE as VR__NOTE202"+ ",link_2_2.VRFLDESC as VRFLDESC202"+ ",link_2_2.VRDTOBSO as VRDTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DETTBILA.MRCODVOC=link_2_2.VRCODVOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DETTBILA.MRCODVOC=link_2_2.VRCODVOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MRRAGCDC
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RAGMCENC_IDX,3]
    i_lTable = "RAGMCENC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2], .t., this.RAGMCENC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRRAGCDC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRRAGCDC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_MRRAGCDC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_MRRAGCDC)
            select RCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRRAGCDC = NVL(_Link_.RCCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MRRAGCDC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RAGMCENC_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RAGMCENC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRRAGCDC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODVOA
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_BIAN_IDX,3]
    i_lTable = "VOC_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2], .t., this.VOC_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_MRCODVOA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_MRCODVOA)
            select VRCODVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOA = NVL(_Link_.VRCODVOC,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMR__FONT_2_8.RadioValue()==this.w_MR__FONT)
      this.oPgFrm.Page1.oPag.oMR__FONT_2_8.SetRadio()
      replace t_MR__FONT with this.oPgFrm.Page1.oPag.oMR__FONT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMR__FTST_2_9.RadioValue()==this.w_MR__FTST)
      this.oPgFrm.Page1.oPag.oMR__FTST_2_9.SetRadio()
      replace t_MR__FTST with this.oPgFrm.Page1.oPag.oMR__FTST_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMR__COLO_2_10.RadioValue()==this.w_MR__COLO)
      this.oPgFrm.Page1.oPag.oMR__COLO_2_10.SetRadio()
      replace t_MR__COLO with this.oPgFrm.Page1.oPag.oMR__COLO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMR__NUCO_2_11.RadioValue()==this.w_MR__NUCO)
      this.oPgFrm.Page1.oPag.oMR__NUCO_2_11.SetRadio()
      replace t_MR__NUCO with this.oPgFrm.Page1.oPag.oMR__NUCO_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMR__INDE_2_13.value==this.w_MR__INDE)
      this.oPgFrm.Page1.oPag.oMR__INDE_2_13.value=this.w_MR__INDE
      replace t_MR__INDE with this.oPgFrm.Page1.oPag.oMR__INDE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_2.value==this.w_MRCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_2.value=this.w_MRCODVOC
      replace t_MRCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDESCRI_2_4.value==this.w_MRDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDESCRI_2_4.value=this.w_MRDESCRI
      replace t_MRDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDESCRI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_6.value==this.w_MRIMPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_6.value=this.w_MRIMPORT
      replace t_MRIMPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRIMPORT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPERCEN_2_7.value==this.w_MRPERCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPERCEN_2_7.value=this.w_MRPERCEN
      replace t_MRPERCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRPERCEN_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DETTBILA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_DTOBSO OR .cFunction='Query') and not(empty(.w_MRCODVOC)) and (.w_CPROWORD<>0 AND (.w_MRDESCRI<>space(90) OR .w_MRCODVOC<>space(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRCODVOC_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Voce di ricavo obsoleta")
      endcase
      if .w_CPROWORD<>0 AND (.w_MRDESCRI<>space(90) OR .w_MRCODVOC<>space(15))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND (t_MRDESCRI<>space(90) OR t_MRCODVOC<>space(15)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MRCODVOC=space(15)
      .w_VRFLDESC=space(1)
      .w_MRDESCRI=space(90)
      .w_MR__NOTE=space(0)
      .w_MRIMPORT=0
      .w_MRPERCEN=0
      .w_MR__FONT=space(1)
      .w_MR__FTST=space(1)
      .w_MR__COLO=0
      .w_MR__NUCO=0
      .w_MR__INDE=0
      .w_MRRAGCDC=space(15)
      .w_MRCODVOA=space(15)
      .w_DATAOBSO=ctod("  /  /  ")
      .DoRTCalc(1,4,.f.)
      if not(empty(.w_MRCODVOC))
        .link_2_2('Full')
      endif
      .DoRTCalc(5,9,.f.)
        .w_MR__FONT = 'A'
        .w_MR__FTST = 'N'
      .DoRTCalc(12,14,.f.)
        .w_MRRAGCDC = space(15)
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_MRRAGCDC))
        .link_2_15('Full')
      endif
        .w_MRCODVOA = space(15)
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_MRCODVOA))
        .link_2_16('Full')
      endif
    endwith
    this.DoRTCalc(17,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MRCODVOC = t_MRCODVOC
    this.w_VRFLDESC = t_VRFLDESC
    this.w_MRDESCRI = t_MRDESCRI
    this.w_MR__NOTE = t_MR__NOTE
    this.w_MRIMPORT = t_MRIMPORT
    this.w_MRPERCEN = t_MRPERCEN
    this.w_MR__FONT = this.oPgFrm.Page1.oPag.oMR__FONT_2_8.RadioValue(.t.)
    this.w_MR__FTST = this.oPgFrm.Page1.oPag.oMR__FTST_2_9.RadioValue(.t.)
    this.w_MR__COLO = this.oPgFrm.Page1.oPag.oMR__COLO_2_10.RadioValue(.t.)
    this.w_MR__NUCO = this.oPgFrm.Page1.oPag.oMR__NUCO_2_11.RadioValue(.t.)
    this.w_MR__INDE = t_MR__INDE
    this.w_MRRAGCDC = t_MRRAGCDC
    this.w_MRCODVOA = t_MRCODVOA
    this.w_DATAOBSO = t_DATAOBSO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MRCODVOC with this.w_MRCODVOC
    replace t_VRFLDESC with this.w_VRFLDESC
    replace t_MRDESCRI with this.w_MRDESCRI
    replace t_MR__NOTE with this.w_MR__NOTE
    replace t_MRIMPORT with this.w_MRIMPORT
    replace t_MRPERCEN with this.w_MRPERCEN
    replace t_MR__FONT with this.oPgFrm.Page1.oPag.oMR__FONT_2_8.ToRadio()
    replace t_MR__FTST with this.oPgFrm.Page1.oPag.oMR__FTST_2_9.ToRadio()
    replace t_MR__COLO with this.oPgFrm.Page1.oPag.oMR__COLO_2_10.ToRadio()
    replace t_MR__NUCO with this.oPgFrm.Page1.oPag.oMR__NUCO_2_11.ToRadio()
    replace t_MR__INDE with this.w_MR__INDE
    replace t_MRRAGCDC with this.w_MRRAGCDC
    replace t_MRCODVOA with this.w_MRCODVOA
    replace t_DATAOBSO with this.w_DATAOBSO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mrbPag1 as StdContainer
  Width  = 733
  height = 306
  stdWidth  = 733
  stdheight = 306
  resizeXpos=480
  resizeYpos=166
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_5 as cp_runprogram with uid="OWMTYONTWP",left=734, top=31, width=93,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BGB",;
    cEvent = "Salva",;
    nPag=1;
    , HelpContextID = 64094438


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=5, width=723,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="MRCODVOC",Label2="Codice voce",Field3="MRDESCRI",Label3="Descrizione",Field4="MRIMPORT",Label4="Importo",Field5="MRPERCEN",Label5="Percen.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202187642

  add object oStr_1_1 as StdString with uid="FXMVVEUXPX",Visible=.t., Left=-149, Top=-73,;
    Alignment=0, Width=85, Height=15,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oBox_1_2 as StdBox with uid="WBYPWEAZIJ",left=2, top=7, width=727,height=20

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=25,;
    width=719+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=26,width=718+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_ANAC|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oMR__FONT_2_8.Refresh()
      this.Parent.oMR__FTST_2_9.Refresh()
      this.Parent.oMR__COLO_2_10.Refresh()
      this.Parent.oMR__NUCO_2_11.Refresh()
      this.Parent.oMR__INDE_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_ANAC'
        oDropInto=this.oBodyCol.oRow.oMRCODVOC_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oMR__FONT_2_8 as StdTrsCombo with uid="XVCWQMIWJN",rtrep=.t.,;
    cFormVar="w_MR__FONT", RowSource=""+"Arial,"+"Times New Roman" , ;
    ToolTipText = "Font del rigo",;
    HelpContextID = 50642662,;
    Height=25, Width=105, Left=93, Top=277,;
    cTotal="", cQueryName = "MR__FONT",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMR__FONT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MR__FONT,&i_cF..t_MR__FONT),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'T',;
    space(1))))
  endfunc
  func oMR__FONT_2_8.GetRadio()
    this.Parent.oContained.w_MR__FONT = this.RadioValue()
    return .t.
  endfunc

  func oMR__FONT_2_8.ToRadio()
    this.Parent.oContained.w_MR__FONT=trim(this.Parent.oContained.w_MR__FONT)
    return(;
      iif(this.Parent.oContained.w_MR__FONT=='A',1,;
      iif(this.Parent.oContained.w_MR__FONT=='T',2,;
      0)))
  endfunc

  func oMR__FONT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMR__FTST_2_9 as StdTrsCombo with uid="VUVHLWVUOZ",rtrep=.t.,;
    cFormVar="w_MR__FTST", RowSource=""+"Normale,"+"Grassetto,"+"Corsivo" , ;
    ToolTipText = "Stile font",;
    HelpContextID = 33243418,;
    Height=25, Width=97, Left=210, Top=277,;
    cTotal="", cQueryName = "MR__FTST",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMR__FTST_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MR__FTST,&i_cF..t_MR__FTST),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'B',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oMR__FTST_2_9.GetRadio()
    this.Parent.oContained.w_MR__FTST = this.RadioValue()
    return .t.
  endfunc

  func oMR__FTST_2_9.ToRadio()
    this.Parent.oContained.w_MR__FTST=trim(this.Parent.oContained.w_MR__FTST)
    return(;
      iif(this.Parent.oContained.w_MR__FTST=='N',1,;
      iif(this.Parent.oContained.w_MR__FTST=='B',2,;
      iif(this.Parent.oContained.w_MR__FTST=='I',3,;
      0))))
  endfunc

  func oMR__FTST_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMR__COLO_2_10 as StdTrsCombo with uid="EZWUONYKFX",rtrep=.t.,;
    cFormVar="w_MR__COLO", RowSource=""+"Nero,"+"Rosso,"+"Blue" , ;
    ToolTipText = "Colore della voce",;
    HelpContextID = 53788395,;
    Height=25, Width=79, Left=319, Top=277,;
    cTotal="", cQueryName = "MR__COLO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMR__COLO_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MR__COLO,&i_cF..t_MR__COLO),this.value)
    return(iif(xVal =1,0,;
    iif(xVal =2,255,;
    iif(xVal =3,16711680,;
    0))))
  endfunc
  func oMR__COLO_2_10.GetRadio()
    this.Parent.oContained.w_MR__COLO = this.RadioValue()
    return .t.
  endfunc

  func oMR__COLO_2_10.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_MR__COLO==0,1,;
      iif(this.Parent.oContained.w_MR__COLO==255,2,;
      iif(this.Parent.oContained.w_MR__COLO==16711680,3,;
      0))))
  endfunc

  func oMR__COLO_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMR__NUCO_2_11 as StdTrsCombo with uid="WJOHDOMREP",rtrep=.t.,;
    cFormVar="w_MR__NUCO", RowSource=""+"Non stampato,"+"Colonna 1,"+"Colonna 2,"+"Colonna 3" , ;
    ToolTipText = "Numero colonna",;
    HelpContextID = 58409237,;
    Height=25, Width=88, Left=410, Top=277,;
    cTotal="", cQueryName = "MR__NUCO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMR__NUCO_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MR__NUCO,&i_cF..t_MR__NUCO),this.value)
    return(iif(xVal =1,0,;
    iif(xVal =2,1,;
    iif(xVal =3,2,;
    iif(xVal =4,3,;
    0)))))
  endfunc
  func oMR__NUCO_2_11.GetRadio()
    this.Parent.oContained.w_MR__NUCO = this.RadioValue()
    return .t.
  endfunc

  func oMR__NUCO_2_11.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_MR__NUCO==0,1,;
      iif(this.Parent.oContained.w_MR__NUCO==1,2,;
      iif(this.Parent.oContained.w_MR__NUCO==2,3,;
      iif(this.Parent.oContained.w_MR__NUCO==3,4,;
      0)))))
  endfunc

  func oMR__NUCO_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMR__INDE_2_13 as StdTrsField with uid="BJADSXPWOC",rtseq=14,rtrep=.t.,;
    cFormVar="w_MR__INDE",value=0,;
    ToolTipText = "Caratteri di indentatura",;
    HelpContextID = 204161291,;
    cTotal="", bFixedPos=.t., cQueryName = "MR__INDE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=603, Top=277, cSayPict=["999"], cGetPict=["999"]

  add object oStr_2_12 as StdString with uid="DGCEEPJUJE",Visible=.t., Left=515, Top=277,;
    Alignment=1, Width=84, Height=15,;
    Caption="Indentatura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="HINOTCQBYR",Visible=.t., Left=10, Top=277,;
    Alignment=1, Width=81, Height=15,;
    Caption="Carattere:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsbi_mrbBodyRow as CPBodyRowCnt
  Width=709
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="JQSCTJADIJ",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo riga",;
    HelpContextID = 234516074,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=68, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], BackStyle = 0

  add object oMRCODVOC_2_2 as StdTrsField with uid="SETQDJUWAP",rtseq=4,rtrep=.t.,;
    cFormVar="w_MRCODVOC",value=space(15),;
    ToolTipText = "Codice voce di bilancio",;
    HelpContextID = 204898039,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Voce di ricavo obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=75, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , BackStyle = 0, cLinkFile="VOC_ANAC", cZoomOnZoom="GSBI_AVC", oKey_1_1="VRCODVOC", oKey_1_2="this.w_MRCODVOC"

  func oMRCODVOC_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVOC_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMRCODVOC_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_ANAC','*','VRCODVOC',cp_AbsName(this.parent,'oMRCODVOC_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVC',"Voci di bilancio",'GSBI_AVC.VOC_ANAC_VZM',this.parent.oContained
  endproc
  proc oMRCODVOC_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_MRCODVOC
    i_obj.ecpSave()
  endproc

  add object oMRDESCRI_2_4 as StdTrsField with uid="JBESHRWNLE",rtseq=6,rtrep=.t.,;
    cFormVar="w_MRDESCRI",value=space(90),;
    ToolTipText = "Descrizione (F9 = accede alle note aggiuntive)",;
    HelpContextID = 28283151,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=282, Left=197, Top=0, InputMask=replicate('X',90), bHasZoom = .t. , BackStyle = 0

  proc oMRDESCRI_2_4.mZoom
    do GSBI_KDS with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMRIMPORT_2_6 as StdTrsField with uid="SEUHZEKWJY",rtseq=8,rtrep=.t.,;
    cFormVar="w_MRIMPORT",value=0,;
    HelpContextID = 227008794,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=152, Left=487, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)], BackStyle = 0

  func oMRIMPORT_2_6.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC = 'N' OR .w_VRFLDESC = 'T')
    endwith
  endfunc

  add object oMRPERCEN_2_7 as StdTrsField with uid="IVJYBXQOXD",rtseq=9,rtrep=.t.,;
    cFormVar="w_MRPERCEN",value=0,;
    HelpContextID = 27283732,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=646, Top=0, cSayPict=["999.99"], cGetPict=["999.99"], BackStyle = 0

  func oMRPERCEN_2_7.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC = 'N' OR .w_VRFLDESC = 'T')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mrb','DETTBILA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRNUMBIL=DETTBILA.MRNUMBIL";
  +" and "+i_cAliasName2+".MRCODESE=DETTBILA.MRCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
