* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_baf                                                        *
*              Stampa voci di bilancio analitica                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_145]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_baf",oParentObject)
return(i_retval)

define class tgsbi_baf as StdBatch
  * --- Local variables
  w_CODICE = space(15)
  Primo = .f.
  w_OLDVOC = space(15)
  Func = space(0)
  Msg = space(20)
  w_OPER = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione Variabili Globali lette dalla Maschera
    * --- Dichiarazione Variabili Locali
    this.Primo = .T.
    * --- Chiamata alla query che ritorna il dettaglio delle Voci
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_SAF", this, "TOTAVOCI")
    SELECT TOTAVOCI
    if RECNO() = 0
      i_retcode = 'stop'
      return
    endif
    * --- Creo un cursore con un unico campo Memo per aggiungerlo al cursore della query
    CREATE CURSOR SOLOMEMO (FORMULA M(10))
    APPEND BLANK
    * --- Aggiungo il Memo al cursore
    SELECT * FROM TOTAVOCI INNER JOIN SOLOMEMO ;
    ON 1 = 1 INTO CURSOR TOTAVOCI NOFILTER
    * --- Chiudo il cursore di appoggio che non serve pi�
    select SOLOMEMO
    use
    SELECT TOTAVOCI
    =WRCURSOR("TOTAVOCI")
    GO TOP
    * --- Inizializzazione del codice Voce per rottura di codice
    this.w_OLDVOC = NVL(VRCODVOC,"")
    SCAN
    if NVL(VRTIPVOC,"") = "N" OR NVL(VRTIPVOC,"") = "T"
      this.Func = "("
      * --- Calcolo la Formula
      do while NOT EOF() AND VRCODVOC = this.w_OLDVOC
        * --- Valorizzazione del Codice
        this.w_CODICE = SPACE(15)
        do case
          case VRTIPVOC = "T"
            this.w_CODICE = NVL(DVCODVOB,"")
          case VRTIPVOC = "N"
            this.w_CODICE = NVL(DVCODVOC,"")
        endcase
        * --- Valorizzazione dell'operatore di piede formula
        this.w_OPER = NVL(DVFLOPER,"")
        this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_OPER)
        * --- Inserisco parentesi tonde aperte all'inizio se ci sono '(' '(('
        if NVL(DVFLPARE,"") = "(" OR NVL(DVFLPARE,"") = "(("
          this.Func = ALLTRIM(this.Func) + NVL(DVFLPARE,"")
        endif
        * --- Se esiste la condizione la inserisco sotto forma di IIF
        if NOT (NVL(ALLTRIM(DVCONDIZ), "") == "")
          this.Func = ALLTRIM(this.Func) + "IIF(" + ALLTRIM(this.w_CODICE) + ALLTRIM(NVL(DVCONDIZ,"")) + ","
          this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_CODICE) + ALLTRIM(NVL(DVFORMUL,""))
          this.Func = ALLTRIM(this.Func) + "," + ALLTRIM(this.w_CODICE) + ")"
        else
          this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_CODICE) + ALLTRIM(NVL(DVFORMUL,""))
        endif
        * --- Chiudo le parentesi tonde
        if NVL(DVFLPARE,"") = ")" OR NVL(DVFLPARE,"") = "))"
          this.Func = ALLTRIM(this.Func) + NVL(DVFLPARE,"")
        endif
        * --- Vado avanti
        SKIP
      enddo
      * --- Scrivo nell'ultimo record della vecchia chiave la formula
      SKIP - 1
      this.Func = ALLTRIM(this.Func) + ")"
      if this.Func = "()"
        this.Func = ""
      endif
      this.Func = ALLTRIM(this.Func) + IIF(EMPTY(NVL(VRFORMUL,"")),"",ALLTRIM(NVL(VRFLOPER,"")))
      this.Func = ALLTRIM(this.Func) + ALLTRIM(NVL(VRFORMUL,""))
      REPLACE FORMULA WITH this.FUNC
    endif
    SKIP
    * --- Se non sono in fondo al cursore, cambio la chiave di rottura
    if NOT EOF()
      this.w_OLDVOC = NVL(VRCODVOC,"")
      this.Func = ""
    endif
    SKIP - 1
    ENDSCAN
    * --- Scrittura della formula su tutte le righe per la stampa
    SELECT VRCODVOC AS CODICE, FORMULA FROM TOTAVOCI WHERE NOT EMPTY(FORMULA) ;
    INTO CURSOR APPOGGIO NOFILTER NOCONSOLE
    * --- Con la Inner porto le informazioni sul cursore per la stampa
    SELECT TOTAVOCI.VRCODVOC, TOTAVOCI.VRDESVOC, TOTAVOCI.VR__NOTE, TOTAVOCI.VRTIPVOC, ;
    TOTAVOCI.DVCODVOC, TOTAVOCI.DVCODVOB, APPOGGIO.FORMULA ;
    FROM TOTAVOCI LEFT JOIN APPOGGIO ON TOTAVOCI.VRCODVOC = APPOGGIO.CODICE ;
    INTO CURSOR __TMP__ NOFILTER
    * --- Lancio la stampa
    CP_CHPRN("..\BILC\EXE\QUERY\GSBI1SAF","",this.oParentObject)
    * --- Chiusura Cursori
    select APPOGGIO
    use
    select TOTAVOCI
    use
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
