* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bi2                                                        *
*              Verifica indice di bilancio                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-11                                                      *
* Last revis.: 2005-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Evento
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bi2",oParentObject,m.Evento)
return(i_retval)

define class tgsbi_bi2 as StdBatch
  * --- Local variables
  Evento = space(5)
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_DIFFE = 0
  w_VALORE = 0
  Func = space(0)
  CODVOC = space(15)
  w_MESS = space(50)
  INDICE = 0
  * --- WorkFile variables
  VOC_ANAC_idx=0
  VOC_BIAN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della formula integrata nell'indice di bilancio
    Messaggio = "ZCorretta"
    this.TmpSave = ALLTRIM(this.oParentObject.w_IBFORMUL)
    * --- Ricerco il segno '<' il quale indica che ho inserito una costante
    this.w_OCCUR = RATC("<",this.TmpSave)
    this.w_OCCUR1 = RATC(">",this.TmpSave)
    this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    do while this.w_OCCUR <> 0
      * --- Ciclo sulla stringa per verificarne la correttezza
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      this.TmpSave1 = this.TmpSave1 + "10"
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      * --- Verifico la correttezza della voce o della costante
      this.CODVOC = ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+1,this.w_DIFFE-1))
      if this.oParentObject.w_IBTIPIND = "G"
        * --- Read from VOC_ANAC
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOC_ANAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_ANAC_idx,2],.t.,this.VOC_ANAC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" VOC_ANAC where ";
                +"VRCODVOC = "+cp_ToStrODBC(this.CODVOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                VRCODVOC = this.CODVOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from VOC_BIAN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOC_BIAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_BIAN_idx,2],.t.,this.VOC_BIAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" VOC_BIAN where ";
                +"VRCODVOC = "+cp_ToStrODBC(this.CODVOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                VRCODVOC = this.CODVOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if i_rows = 0
        Messaggio = "ZErrata"
        if INLIST(this.CODVOC,"C01","C02","C03","C04","C05","C06","C07","C08","C09","C10")
          Messaggio = "ZCorretta"
        endif
      endif
      if Messaggio <> "ZCorretta"
        * --- Sostituisco la voce di bilancio errata con mettendola tra ##
        this.TmpSave1 = SUBSTR(this.oParentObject.w_IBFORMUL,1,this.w_OCCUR)
        this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODVOC) + "#"
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_IBFORMUL,this.w_OCCUR1))
        this.oParentObject.w_IBFORMUL = ALLTRIM(this.TmpSave1)
        this.oParentObject.w_ERRMSG = "T"
        EXIT
      endif
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("<",this.TmpSave)
      this.w_OCCUR1 = RATC(">",this.TmpSave)
      this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    enddo
    if Messaggio = "ZCorretta"
      this.Func = this.oParentObject.w_IBFORMUL
      this.FuncCalc =  ALLTRIM(this.TmpSave)
      ON ERROR Messaggio = "ZErrata"
      TmpVal = this.FuncCalc
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      ON ERROR
      if Messaggio="ZErrata"
        this.oParentObject.w_ERRMSG = "N"
      else
        this.oParentObject.w_ERRMSG = "S"
      endif
    endif
    WAIT CLEAR
    if Messaggio = "ZErrata" AND this.Evento = "Save"
      this.w_MESS = AH_MsgFormat("Impossibile salvare, sintassi errata")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,Evento)
    this.Evento=Evento
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VOC_ANAC'
    this.cWorkTables[2]='VOC_BIAN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Evento"
endproc
