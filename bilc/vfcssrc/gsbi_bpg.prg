* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bpg                                                        *
*              Stampa proiezione bilancio                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_129]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2000-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bpg",oParentObject,m.pOper)
return(i_retval)

define class tgsbi_bpg as StdBatch
  * --- Local variables
  pOper = space(4)
  w_ZOOM1 = space(10)
  w_EXCEL = space(1)
  w_ZOOM2 = space(10)
  w_NUMERO = 0
  w_DATFIN = ctod("  /  /  ")
  w_ESERCI = space(4)
  w_DECIMI = 0
  * --- WorkFile variables
  PER_ELAB_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Machera Stampa Confronto Indici
    this.w_ZOOM1 = this.oParentObject.w_ZoomBilP
    this.w_ZOOM2 = this.oParentObject.w_ZoomBilE
    this.w_EXCEL = "N"
    do case
      case this.pOper="SELE1" OR this.pOper="SELE2"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "CARICA"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="ELAB"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Chiudo la Maschera
        This.bUpdateParentObject=.f.
        This.oParentObject.bUpdated=.f.
        This.oParentObject.ecpQuit()
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/Deseleziona Tutto
    if this.pOper = "SELE1"
      NC = this.w_ZOOM1.cCursor
      if this.oParentObject.w_SELEZI1="S"
        UPDATE &NC SET XCHK = 1
        this.oParentObject.w_FLSELE1 = 1
      else
        UPDATE &NC SET XCHK = 0
        this.oParentObject.w_FLSELE1 = 0
      endif
    else
      NC = this.w_ZOOM2.cCursor
      if this.oParentObject.w_SELEZI2="S"
        UPDATE &NC SET XCHK = 1
        this.oParentObject.w_FLSELE2 = 1
      else
        UPDATE &NC SET XCHK = 0
        this.oParentObject.w_FLSELE2 = 0
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Zoom
    ah_Msg("Elaborazione dati previsionali...",.T.)
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI1BPG.VQR",this,"CursElab")
    ah_Msg("Elaborazione dati effettivi...",.T.)
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_BPG.VQR",this,"CursElab2")
    * --- Cancello il contenuto dello zoom
    DELETE FROM ( this.w_ZOOM1.cCursor )
    DELETE FROM ( this.w_ZOOM2.cCursor )
    * --- Passo i valori allo ZOOM
    * --- Passo per un Array (Fox non accetta istruzioni del tipo Insert into from select ...)
    SELECT * FROM CursElab ;
    INTO ARRAY VETTORE ORDER BY GBBILESE,GBNUMBIL
    SELECT CursElab
    USE
    SELECT * FROM CursElab2 ;
    INTO ARRAY VETTORE2 ORDER BY GBBILESE,GBNUMBIL
    SELECT CursElab2
    USE
    * --- Se il cursore � vuoto non crea il vettore
    if TYPE("VETTORE")<>"U"
      insert into ( this.w_ZOOM1.cCursor ) from array vettore
    endif
    if TYPE("VETTORE2")<>"U"
      insert into ( this.w_ZOOM2.cCursor ) from array vettore2
    endif
    SELECT ( this.w_ZOOM1.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom1.grd.refresh
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom1.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom1.grd.Column&NC..ControlSource)
      This.w_Zoom1.grd.Column&NC..Enabled=.f.
    endif
    ENDFOR
    SELECT ( this.w_ZOOM2.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom2.grd.refresh
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom2.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom2.grd.Column&NC..ControlSource)
      This.w_Zoom2.grd.Column&NC..Enabled=.f.
    endif
    ENDFOR
    if used("CursElab")
      SELECT CursElab
      USE
    endif
    if used("CursElab2")
      SELECT CursElab2
      USE
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Stampa
    * --- Molte parti del codice riprendono GSBI_BCI
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi carico su un cursore la struttura del bilancio per i calcoli
    ah_Msg("Fase 1: reperimento dati struttura bilancio",.T.)
    if this.oParentObject.w_TIPOBIL = "G"
      vq_exec("..\bilc\exe\query\gsbi2bpg",this,"CursStrut1")
    else
      vq_exec("..\bilc\exe\query\gsbi3bpg",this,"CursStrut1")
    endif
    CREATE CURSOR CursVal (IMPORT1 N(18,6),IMPORT2 N(18,6),IMPORT3 N(18,6),IMPORT4 N(18,6), ;
    FORECAST N(18,6),SCOSASSO N(18,6),SCOSTAMP N(7,2),SCOSTAMT N(7,2))
    b = WRCURSOR("CursVal")
    * --- IMPORT1 = Budget (somma degli importi dei previsionali prima della fine periodo di rottura)
    * --- IMPORT2 = Consuntivo (somma degli importi dei consuntivi prima della fine periodo di rottura)
    * --- IMPORT3 = Budget Anno (somma degli importi dei previsionali selezionati)
    * --- IMPORT4 = Proiezione (Budget Anno + Differenza tra Consuntivo e Budget)
    * --- FORECAST = Forecast (Budget Anno - Differenza tra Consuntivo e Budget)
    INSERT INTO CursVal (IMPORT1,IMPORT2,IMPORT3,IMPORT4, ;
    FORECAST,SCOSASSO,SCOSTAMP,SCOSTAMT) ;
    VALUES(0,0,0,0,0,0,0,0)
    SELECT CursStrut1.*, CursVal.* FROM CursStrut1,CursVal INTO CURSOR CursStrut
    a = WRCURSOR("CursStrut")
    SELECT CursVal
    USE
    if used("CursStrut1")
      SELECT CursStrut1
      USE
    endif
    * --- Calcolo Valori previsionali
    NC = this.w_ZOOM1.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_NUMERO = GBNUMBIL
    this.w_ESERCI = GBBILESE
    * --- Mi carico su un cursore il bilancio
    ah_Msg("Fase 2: reperimento dati bilancio previsionale",.T.)
    vq_exec("..\bilc\exe\query\gsbi_bci",this,"CursBilc")
    SELECT CursBilc
    GO TOP
    SCAN
    if CursBilc.PEDATFIN <= this.oParentObject.w_DATROT
      UPDATE CursStrut SET IMPORT1 = IMPORT1 + CursBilc.MRIMPORT, ;
      IMPORT3 = IMPORT3 + CursBilc.MRIMPORT ;
      WHERE CursBilc.CPROWORD=CursStrut.CPROWORD
    else
      UPDATE CursStrut SET IMPORT3 = IMPORT3 + CursBilc.MRIMPORT ;
      WHERE CursBilc.CPROWORD=CursStrut.CPROWORD
    endif
    if this.w_DATFIN < CursBilc.PEDATFIN
      this.w_DATFIN = CursBilc.PEDATFIN
    endif
    ENDSCAN
    if used("CursBilc")
      select CursBilc
      use
    endif
    ENDSCAN
    * --- Calcolo Valori effettivi
    NC = this.w_ZOOM2.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_NUMERO = GBNUMBIL
    this.w_ESERCI = GBBILESE
    * --- Mi carico su un cursore il bilancio
    ah_Msg("Fase 3: reperimento dati bilancio effettivo",.T.)
    vq_exec("..\bilc\exe\query\gsbi_bci",this,"CursBilc")
    SELECT CursBilc
    GO TOP
    SCAN
    UPDATE CursStrut SET IMPORT2 = IMPORT2 + CursBilc.MRIMPORT ;
    WHERE CursBilc.CPROWORD=CursStrut.CPROWORD
    ENDSCAN
    if used("CursBilc")
      select CursBilc
      use
    endif
    ENDSCAN
    * --- Calcolo Proiezione
    SELECT CursStrut
    GO TOP
    SCAN
    UPDATE CursStrut SET ;
    SCOSASSO = IMPORT1 - IMPORT2, ;
    SCOSTAMP = IIF(IMPORT1=0, 0, ((IMPORT1 - IMPORT2) / IMPORT1) * 100), ;
    SCOSTAMT = IIF(IMPORT2=0, 0, ((IMPORT1 - IMPORT2) / IMPORT2) * 100), ;
    IMPORT4 = IMPORT3 + (( IMPORT3 * SCOSTAMP)/100), ;
    FORECAST = (IMPORT3 - IMPORT1) - (IMPORT4 - IMPORT3) ;
    WHERE IMPORT1 <> 0 OR IMPORT2 <> 0 OR IMPORT3 <> 0
    ENDSCAN
    * --- Assegno variabili da riportare sulla stampa
    L_TIPOPER = this.oParentObject.w_TIPOPER
    L_ESER = this.oParentObject.w_ESER
    L_DATROT = this.oParentObject.w_DATROT
    L_STRUTTUR = this.oParentObject.w_STRUTTUR
    L_DATFIN = this.w_DATFIN
    L_DECIMI = this.w_DECIMI
    s=20*(l_decimi+2)
    SELECT * FROM CursStrut INTO CURSOR __TMP__
    if this.oParentObject.w_STAEXCEL = "S"
      * --- Excel
      CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BPG.XLT", " ", this)
    else
      * --- Stampa
      if this.oParentObject.w_TIPOBIL = "G"
        CP_CHPRN("..\BILC\EXE\QUERY\GSBI_SPG.FRX", " ", this)
      else
        CP_CHPRN("..\BILC\EXE\QUERY\GSBI_SPA.FRX", " ", this)
      endif
    endif
    if used("CursStrut")
      select CursStrut
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PER_ELAB'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
