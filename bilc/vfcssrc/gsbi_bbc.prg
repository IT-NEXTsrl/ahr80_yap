* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bbc                                                        *
*              Lancio stampa controllo conti                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_18]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bbc",oParentObject)
return(i_retval)

define class tgsbi_bbc as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Stampa di Controllo (da GSBI_SBC)
    L_BILANCIO = this.oParentObject.w_BILANCIO
    L_costi = this.oParentObject.w_costi
    L_ricavi = this.oParentObject.w_ricavi
    L_ordine = this.oParentObject.w_ordine
    L_transi = this.oParentObject.w_transi
    L_attivita = this.oParentObject.w_attivita
    L_passivi = this.oParentObject.w_passivi
    ah_Msg("Elaborazione in corso",.T.)
    * --- query su tutti i conti che fanno parte dei mastri del bilancio
    vq_exec("..\BILC\EXE\QUERY\GSBI1BBC",this,"struttura")
    vq_exec("..\BILC\EXE\QUERY\GSBI2BBC",this,"struttura2")
    ah_Msg("Selezione dati",.T.)
    SELECT * FROM struttura INTO CURSOR struttura ;
    WHERE calcsez(ANCONSUP2)=this.oParentObject.w_ATTIVITA OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_PASSIVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_COSTI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_RICAVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_ORDINE OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_TRANSI
    * --- query su tutti i conti del bilancio
    ah_Msg("Selezione dati",.T.)
    SELECT * FROM struttura2 INTO CURSOR struttura2 ;
    WHERE calcsez(ANCONSUP2)=this.oParentObject.w_ATTIVITA OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_PASSIVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_COSTI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_RICAVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_ORDINE OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_TRANSI
    * --- query su tutti i conti del Piano dei Conti
    ah_Msg("Elaborazione in corso",.T.)
    vq_exec("..\BILC\EXE\QUERY\GSBI_BBC",this,"allconti")
    ah_Msg("Selezione dati",.T.)
    SELECT * FROM allconti INTO CURSOR allconti ;
    WHERE calcsez(ANCONSUP2)=this.oParentObject.w_ATTIVITA OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_PASSIVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_COSTI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_RICAVI OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_ORDINE OR ;
    calcsez(ANCONSUP2)=this.oParentObject.w_TRANSI
    * --- determino i conti mancanti
    * --- trovo tutti i conti utilizzati nel cursore struttura
    SELECT * FROM struttura INTO CURSOR struttura UNION ;
    ALL SELECT * FROM struttura2
    * --- determino i conti non utilizzati
    ah_Msg("Elaborazione in corso (determino conti non utilizzati)",.T.)
    SELECT * FROM allconti INTO CURSOR allconti WHERE allconti.CONMASTRO ;
    NOT IN (SELECT struttura.CONMASTRO FROM struttura)
    * --- controllo il numero di elementi doppi
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- unisco i due risultati
    ah_Msg("Elaborazione in corso (segnalazione errore)",.T.)
    SELECT allconti.*, "Mancante" AS ERRORE ;
    FROM allconti INTO CURSOR stampa ;
    UNION SELECT numero.CONMASTRO,numero.ANDESCRI, ;
    numero.ANCONSUP2,numero.ANTIPCON, ;
    "Rip: "+ALLTRIM(STR(numero.QUANTI)) AS ERRORE FROM numero
    * --- Lancio il report
    SELECT * FROM stampa INTO CURSOR __TMP__
    CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BBC.FRX", " ", this)
    * --- Chiudo i cursori
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("struttura")
      select struttura
      use
    endif
    if used("allconti")
      select allconti
      use
    endif
    if used("ripetuti")
      select ripetuti
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("struttura2")
      select struttura2
      use
    endif
    if used("numero")
      select numero
      use
    endif
    if used("stampa")
      select stampa
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_Msg("Elaborazione in corso (controllo conti doppi)",.T.)
    * --- controllo il numero di elementi doppi
    SELECT CONMASTRO, ANTIPCON, COUNT(CONMASTRO) AS QUANTI FROM struttura ;
    INTO CURSOR ripetuti GROUP BY CONMASTRO,ANTIPCON ;
    HAVING QUANTI > 1
    SELECT DISTINCT struttura.*, ripetuti.quanti FROM struttura, ripetuti ;
    INTO CURSOR numero WHERE struttura.CONMASTRO=ripetuti.CONMASTRO AND struttura.ANTIPCON =ripetuti.ANTIPCON
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
