* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bel                                                        *
*              Elaborazione dati                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_156]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2007-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bel",oParentObject)
return(i_retval)

define class tgsbi_bel as StdBatch
  * --- Local variables
  w_APPO = space(10)
  w_CONCES = ctod("  /  /  ")
  w_MESS = space(10)
  w_RECODICE = space(15)
  w_CASERIAL = space(10)
  w_TIPCON = space(1)
  w_REDESCRI = space(45)
  w_CANUMREG = 0
  w_COD001 = space(15)
  w_REDESOPE = space(10)
  w_CACODESE = space(4)
  w_COD002 = space(15)
  w_RETIPSTE = space(2)
  w_CPROWNUM = 0
  w_COD003 = space(15)
  w_RENOMCUR = space(8)
  w_CONTA1 = 0
  w_IMPDAR = 0
  w_CGSERIAL = space(10)
  w_CONTA2 = 0
  w_IMPAVE = 0
  w_CGCODESE = space(4)
  w_FLGCON = space(1)
  w_VALNAZ = space(3)
  w_CGNUMREG = 0
  w_OSERIAL = space(21)
  w_CCMAGA = space(1)
  w_ERROR = .f.
  w_CESPITE = .f.
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  MOX_COAM_idx=0
  MOX_COAN_idx=0
  MOX_COGE_idx=0
  MOX_COGM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Estrazione Dati e generazione Movimenti Extracontabili (da GSBI_KEL)
    this.w_ERROR = .f.
    this.w_CESPITE = .f.
    * --- Controlli Preliminari
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CCMAGA = iif(this.oParentObject.w_CCMAGA="S","S","N")
    this.w_VALNAZ = g_PERVAL
    * --- Elaborazione Movimenti di Contabilita' Generale/Analitica
    this.w_MESS = IIF(this.oParentObject.w_FASE="C","Generale","Analitica")
    * --- Cursore per la stampa degli errori durante l'elaborazione
    CREATE CURSOR CursError (CODICE C(15), CPROWNUM N(6), DESCRI C(90), TIPCON C(1), ;
    COD001 C(15), COD002 C(15), COD003 C(15))
    b = WRCURSOR("CursError")
    * --- Crea il Temporaneo di Appoggio
    CREATE CURSOR TmpAgg ;
    (CODICE C(15), DESCRI C(45), ;
    TIPCON C(1), COD001 C(15), COD002 C(15), COD003 C(15), IMPDAR N(18,4), IMPAVE N(18,4))
    ah_Msg("Contabilit� %1; elabora regole...",.T.,.F.,.F.,this.w_MESS)
    vq_exec("..\BILC\EXE\QUERY\GSBI_BEL.VQR",this,"ELABORA")
    if USED("ELABORA")
      * --- Cicla sul risultato delle Regole, 1^Volta Contabilita' Generale; 2^Volta Contab.Analitica
      * --- Scrive il Temporaneo di Aggiornamento (TMPAGG)
      SELECT ELABORA
      GO TOP
      SCAN FOR NOT EMPTY(NVL(REDESOPE," ")) AND NOT EMPTY(NVL(RETIPSTE," "))
      * --- Per ciascuna Riga Legge: Tipo, Operazione, Cursore
      this.w_RECODICE = RECODICE
      this.w_REDESCRI = NVL(REDESCRI," ")
      this.w_RETIPSTE = RETIPSTE
      this.w_REDESOPE = ALLTRIM(REDESOPE)
      this.w_RENOMCUR = IIF(EMPTY(NVL(RENOMCUR," ")),"TMPANBIL", RENOMCUR)
      do case
        case this.w_RETIPSTE="BA"
          * --- Esegue batch di Elaborazione
          FILTMP = this.w_REDESOPE
          do &FILTMP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_RETIPSTE="MA"
          * --- Esegue Maschera di Elaborazione
          FILTMP = this.w_REDESOPE
          do &FILTMP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_NUMREG = this.oParentObject.w_PANUMREG
        case this.w_RETIPSTE $ "QU-QA" 
          * --- Esegue Query
          if g_CESP<>"S" AND ALLTRIM(this.w_RENOMCUR)="EXTCE001"
            ah_ErrorMsg("Modulo cespiti non attivato, impossibile importare",,"")
            i_retcode = 'stop'
            return
          endif
          this.oParentObject.w_NUMREG = this.oParentObject.w_PANUMREG
          vq_exec(ALLTRIM(this.w_REDESOPE), this, ALLTRIM(this.w_RENOMCUR))
          if ALLTRIM(this.w_RENOMCUR)="EXTCE001"
            this.w_CESPITE = .t.
          else
            this.w_CESPITE = .f.
          endif
          if this.w_RETIPSTE="QA" AND USED(this.w_RENOMCUR)
            * --- Se query con Aggiornamento aggiorna Cursore
            FILTMP = this.w_RENOMCUR
            SELECT &FILTMP
            GO TOP
            SCAN
            this.w_TIPCON = NVL(TIPCON," ")
            this.w_COD001 = NVL(COD001, " ")
            this.w_COD002 = NVL(COD002, " ")
            this.w_COD003 = NVL(COD003, " ")
            this.w_IMPDAR = NVL(IMPDAR, 0)
            this.w_IMPAVE = NVL(IMPAVE, 0)
            INSERT INTO TmpAgg (CODICE, DESCRI, ;
            TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
            VALUES (this.w_RECODICE, this.w_REDESCRI, ;
            this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
            SELECT &FILTMP
            ENDSCAN
          endif
      endcase
      SELECT ELABORA
      ENDSCAN
      * --- Azzera temporaneo
      SELECT ELABORA
      USE
    endif
    * --- Al Termine Aggiorna i Movimenti Extracontabili
    if USED("TmpAgg")
      * --- Prima Ordina il Cursore per i dati di Testata
      SELECT * FROM TmpAgg INTO CURSOR TmpAgg ;
      ORDER BY CODICE
      * --- Try
      local bErr_036ED390
      bErr_036ED390=bTrsErr
      this.Try_036ED390()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_CONTA2 = 0
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_ERROR = .T.
        INSERT INTO CursError (CODICE,CPROWNUM,DESCRI,TIPCON,COD001,COD002,COD003) ;
        VALUES (this.w_RECODICE,this.w_CPROWNUM,Ah_MsgFormat("Errore nell'Insert"), ;
        this.w_TIPCON,NVL(this.w_COD001," "),NVL(this.w_COD002," "),NVL(this.w_COD003," "))
      endif
      bTrsErr=bTrsErr or bErr_036ED390
      * --- End
      * --- Azzera temporaneo
      SELECT TmpAgg
      USE
    endif
    if NOT this.w_ERROR
      this.w_oMess=createobject("Ah_Message")
      if this.w_CONTA1>0
        this.w_oMess.AddMsgPartNL("Operazione completata")     
        this.w_oPart = this.w_oMess.AddMsgPartNL("N.%1 registrazioni generate")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_CONTA2)))     
        this.w_oPart = this.w_oMess.AddMsgPartNL("Su %1 registrazioni elaborate")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_CONTA1)))     
      else
        this.w_oMess.AddMsgPart("Non ci sono movimenti da generare")     
      endif
      this.w_oMess.Ah_ErrorMsg()     
    else
      this.w_MESS = "Operazione completata%0N. %1 registrazioni generate su %2 registrazioni elaborate%0Si sono verificati degli errori! Si vuole procedere con la stampa?"
      if ah_YesNo(this.w_MESS,,ALLTRIM(STR(this.w_CONTA2)),ALLTRIM(STR(this.w_CONTA1)))
        SELECT * FROM CursError INTO CURSOR __TMP__ ORDER BY CPROWNUM
        CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BEL.FRX", " ", this)
      endif
    endif
    if used("CursError")
      select CursError
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc
  proc Try_036ED390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Movimenti Extracontabili
    this.w_CPROWNUM = 0
    this.w_OSERIAL = "zzzzzz"
    ah_Msg("Aggiorna registrazioni extracontabili...",.T.)
    SELECT TmpAgg
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODICE," "))
    this.w_RECODICE = CODICE
    this.w_REDESCRI = NVL(DESCRI, " ")
    this.w_TIPCON = NVL(TIPCON," ")
    this.w_COD001 = NVL(COD001," ")
    this.w_COD002 = NVL(COD002," ")
    this.w_COD003 = NVL(COD003," ")
    this.w_IMPDAR = NVL(IMPDAR, 0)
    this.w_IMPAVE = NVL(IMPAVE, 0)
    if this.w_OSERIAL<>this.w_RECODICE OR this.w_CPROWNUM>100
      * --- Se Cambiano i Dati di testata o piu' di 100 Righe scrive nuova Registrazione
      this.w_CPROWNUM = 0
    endif
    this.w_OSERIAL = this.w_RECODICE
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    if this.w_CPROWNUM=1
      * --- Prima Riga: Scrive la Testata
      this.w_CONTA1 = this.w_CONTA1 + 1
      this.w_CONTA2 = this.w_CONTA2 + 1
      if this.oParentObject.w_FASE="C"
        * --- Scrive Movimenti Contabilita' Generale
        this.w_CGSERIAL = SPACE(10)
        this.w_CGNUMREG = 0
        this.w_CGCODESE = this.oParentObject.w_CODESE
        this.w_FLGCON = "S"
        i_Conn=i_TableProp[this.MOX_COGM_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEMOXCG", "i_codazi,w_CGSERIAL")
        cp_NextTableProg(this, i_Conn, "PRMOXCG", "i_codazi,w_CGCODESE,w_CGNUMREG")
        * --- Insert into MOX_COGM
        i_nConn=i_TableProp[this.MOX_COGM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOX_COGM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOX_COGM_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CGSERIAL"+",CGNUMREG"+",CGCODESE"+",CGDATREG"+",CGDESCRI"+",CGTIPMOV"+",CGCODPER"+",CGCODREG"+",CGVALNAZ"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",CGFLGCON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CGSERIAL),'MOX_COGM','CGSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CGNUMREG),'MOX_COGM','CGNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CGCODESE),'MOX_COGM','CGCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATREG),'MOX_COGM','CGDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_REDESCRI),'MOX_COGM','CGDESCRI');
          +","+cp_NullLink(cp_ToStrODBC("E"),'MOX_COGM','CGTIPMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODPER),'MOX_COGM','CGCODPER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RECODICE),'MOX_COGM','CGCODREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VALNAZ),'MOX_COGM','CGVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MOX_COGM','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MOX_COGM','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'MOX_COGM','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MOX_COGM','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FLGCON),'MOX_COGM','CGFLGCON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CGSERIAL',this.w_CGSERIAL,'CGNUMREG',this.w_CGNUMREG,'CGCODESE',this.w_CGCODESE,'CGDATREG',this.oParentObject.w_DATREG,'CGDESCRI',this.w_REDESCRI,'CGTIPMOV',"E",'CGCODPER',this.oParentObject.w_CODPER,'CGCODREG',this.w_RECODICE,'CGVALNAZ',this.w_VALNAZ,'UTCC',i_CODUTE,'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0)
          insert into (i_cTable) (CGSERIAL,CGNUMREG,CGCODESE,CGDATREG,CGDESCRI,CGTIPMOV,CGCODPER,CGCODREG,CGVALNAZ,UTCC,UTDC,UTCV,UTDV,CGFLGCON &i_ccchkf. );
             values (;
               this.w_CGSERIAL;
               ,this.w_CGNUMREG;
               ,this.w_CGCODESE;
               ,this.oParentObject.w_DATREG;
               ,this.w_REDESCRI;
               ,"E";
               ,this.oParentObject.w_CODPER;
               ,this.w_RECODICE;
               ,this.w_VALNAZ;
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,this.w_FLGCON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Scrive Movimenti Contabilita' Analitica
        this.w_CASERIAL = SPACE(10)
        this.w_CANUMREG = 0
        this.w_CACODESE = this.oParentObject.w_CODESE
        this.w_FLGCON = "S"
        i_Conn=i_TableProp[this.MOX_COAM_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEMOXAN", "i_codazi,w_CASERIAL")
        cp_NextTableProg(this, i_Conn, "PRMOXAN", "i_codazi,w_CACODESE,w_CANUMREG")
        * --- Insert into MOX_COAM
        i_nConn=i_TableProp[this.MOX_COAM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOX_COAM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOX_COAM_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CASERIAL"+",CANUMREG"+",CACODESE"+",CADATREG"+",CADESCRI"+",CATIPMOV"+",CACODPER"+",CACODREG"+",CAVALNAZ"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",CAFLGCON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CASERIAL),'MOX_COAM','CASERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CANUMREG),'MOX_COAM','CANUMREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CACODESE),'MOX_COAM','CACODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATREG),'MOX_COAM','CADATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_REDESCRI),'MOX_COAM','CADESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'MOX_COAM','CATIPMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODPER),'MOX_COAM','CACODPER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RECODICE),'MOX_COAM','CACODREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VALNAZ),'MOX_COAM','CAVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MOX_COAM','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'MOX_COAM','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'MOX_COAM','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'MOX_COAM','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FLGCON),'MOX_COAM','CAFLGCON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CASERIAL',this.w_CASERIAL,'CANUMREG',this.w_CANUMREG,'CACODESE',this.w_CACODESE,'CADATREG',this.oParentObject.w_DATREG,'CADESCRI',this.w_REDESCRI,'CATIPMOV',this.w_TIPCON,'CACODPER',this.oParentObject.w_CODPER,'CACODREG',this.w_RECODICE,'CAVALNAZ',this.w_VALNAZ,'UTCC',i_CODUTE,'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0)
          insert into (i_cTable) (CASERIAL,CANUMREG,CACODESE,CADATREG,CADESCRI,CATIPMOV,CACODPER,CACODREG,CAVALNAZ,UTCC,UTDC,UTCV,UTDV,CAFLGCON &i_ccchkf. );
             values (;
               this.w_CASERIAL;
               ,this.w_CANUMREG;
               ,this.w_CACODESE;
               ,this.oParentObject.w_DATREG;
               ,this.w_REDESCRI;
               ,this.w_TIPCON;
               ,this.oParentObject.w_CODPER;
               ,this.w_RECODICE;
               ,this.w_VALNAZ;
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,this.w_FLGCON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Uso TIPCON per il passaggio del FLAG effettivo o previsionale
      endif
    endif
    * --- Scrive il Detail
    if this.oParentObject.w_FASE="C"
      if this.w_TIPCON="G"
        * --- Se Conto Generico, Legge anche il Mastro
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_COD002);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCON;
                and ANCODICE = this.w_COD002;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COD001 = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Insert into MOX_COGE
      i_nConn=i_TableProp[this.MOX_COGE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOX_COGE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOX_COGE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CGSERIAL"+",CPROWNUM"+",CGTIPCON"+",CGCODMAS"+",CGCODCON"+",CGIMPDAR"+",CGIMPAVE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CGSERIAL),'MOX_COGE','CGSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOX_COGE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'MOX_COGE','CGTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COD001),'MOX_COGE','CGCODMAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COD002),'MOX_COGE','CGCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'MOX_COGE','CGIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'MOX_COGE','CGIMPAVE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CGSERIAL',this.w_CGSERIAL,'CPROWNUM',this.w_CPROWNUM,'CGTIPCON',this.w_TIPCON,'CGCODMAS',this.w_COD001,'CGCODCON',this.w_COD002,'CGIMPDAR',this.w_IMPDAR,'CGIMPAVE',this.w_IMPAVE)
        insert into (i_cTable) (CGSERIAL,CPROWNUM,CGTIPCON,CGCODMAS,CGCODCON,CGIMPDAR,CGIMPAVE &i_ccchkf. );
           values (;
             this.w_CGSERIAL;
             ,this.w_CPROWNUM;
             ,this.w_TIPCON;
             ,this.w_COD001;
             ,this.w_COD002;
             ,this.w_IMPDAR;
             ,this.w_IMPAVE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into MOX_COAN
      i_nConn=i_TableProp[this.MOX_COAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOX_COAN_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOX_COAN_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CASERIAL"+",CPROWNUM"+",CAVOCCEN"+",CACODCEN"+",CACODCOM"+",CAIMPDAR"+",CAIMPAVE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CASERIAL),'MOX_COAN','CASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOX_COAN','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COD001),'MOX_COAN','CAVOCCEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COD002),'MOX_COAN','CACODCEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_COD003),'MOX_COAN','CACODCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'MOX_COAN','CAIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'MOX_COAN','CAIMPAVE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CASERIAL',this.w_CASERIAL,'CPROWNUM',this.w_CPROWNUM,'CAVOCCEN',this.w_COD001,'CACODCEN',this.w_COD002,'CACODCOM',this.w_COD003,'CAIMPDAR',this.w_IMPDAR,'CAIMPAVE',this.w_IMPAVE)
        insert into (i_cTable) (CASERIAL,CPROWNUM,CAVOCCEN,CACODCEN,CACODCOM,CAIMPDAR,CAIMPAVE &i_ccchkf. );
           values (;
             this.w_CASERIAL;
             ,this.w_CPROWNUM;
             ,this.w_COD001;
             ,this.w_COD002;
             ,this.w_COD003;
             ,this.w_IMPDAR;
             ,this.w_IMPAVE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    SELECT TmpAgg
    ENDSCAN
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Preliminari
    if this.oParentObject.w_FASE="C"
      if EMPTY(this.oParentObject.w_RISATT)
        ah_ErrorMsg("Contropartita risconti attivi non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_RISPAS)
        ah_ErrorMsg("Contropartita risconti passivi non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_RATATT)
        ah_ErrorMsg("Contropartita ratei attivi non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_RATPAS)
        ah_ErrorMsg("Contropartita ratei passivi non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_FATEME)
        ah_ErrorMsg("Contropartita fatture emesse non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_FATRIC)
        ah_ErrorMsg("Contropartita fatture ricevute non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_IVADEB)
        ah_ErrorMsg("Contropartita IVA debito non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_IVACRE)
        ah_ErrorMsg("Contropartita IVA credito non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONIMB)
        ah_ErrorMsg("Contropartita spese imballo non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONINC)
        ah_ErrorMsg("Contropartita spese incasso non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONTRA)
        ah_ErrorMsg("Contropartita spese trasporto non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONBOL)
        ah_ErrorMsg("Contropartita spese bolli non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONDIC)
        ah_ErrorMsg("Contropartita diff.conversione non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CONARR)
        ah_ErrorMsg("Contropartita arrotondamenti non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CREBRE)
        ah_ErrorMsg("Contropartita crediti a breve non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_CREMED)
        ah_ErrorMsg("Contropartita crediti a medio/lungo non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_DEBBRE)
        ah_ErrorMsg("Contropartita debiti a breve non definita",,"")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.oParentObject.w_DEBMED)
        ah_ErrorMsg("Contropartita debiti a medio/lungo non definita",,"")
        i_retcode = 'stop'
        return
      endif
      vq_exec("..\BILC\EXE\QUERY\GSBI1BEL.VQR",this,"RICERCA")
    else
      vq_exec("..\BILC\EXE\QUERY\GSBI2BEL.VQR",this,"RICERCA")
    endif
    if USED("RICERCA")
      * --- ****
      SELECT RICERCA
      if RECCOUNT()>0
        if ah_YesNo("Per il periodo e regola selezionati, sono gi� presenti dei movimenti extracontabili%0Si desidera proseguire comunque?")
          ah_Msg("Azzero precedenti elaborazioni...",.T.)
          SELECT RICERCA
          GO TOP
          SCAN FOR NOT EMPTY(NVL(SERIAL,""))
          this.w_APPO = SERIAL
          if this.oParentObject.w_FASE="C"
            * --- Delete from MOX_COGE
            i_nConn=i_TableProp[this.MOX_COGE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOX_COGE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CGSERIAL = "+cp_ToStrODBC(this.w_APPO);
                     )
            else
              delete from (i_cTable) where;
                    CGSERIAL = this.w_APPO;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from MOX_COGM
            i_nConn=i_TableProp[this.MOX_COGM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOX_COGM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CGSERIAL = "+cp_ToStrODBC(this.w_APPO);
                     )
            else
              delete from (i_cTable) where;
                    CGSERIAL = this.w_APPO;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            * --- Delete from MOX_COAN
            i_nConn=i_TableProp[this.MOX_COAN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOX_COAN_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CASERIAL = "+cp_ToStrODBC(this.w_APPO);
                     )
            else
              delete from (i_cTable) where;
                    CASERIAL = this.w_APPO;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Delete from MOX_COAM
            i_nConn=i_TableProp[this.MOX_COAM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MOX_COAM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CASERIAL = "+cp_ToStrODBC(this.w_APPO);
                     )
            else
              delete from (i_cTable) where;
                    CASERIAL = this.w_APPO;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          SELECT RICERCA
          ENDSCAN
        else
          USE
          i_retcode = 'stop'
          return
        endif
      endif
      USE
    endif
    if EMPTY(this.oParentObject.w_CODPER)
      ah_ErrorMsg("Specificare codice periodo",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_CODGRU)
      ah_ErrorMsg("Specificare il gruppo di regole",,"")
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MOX_COAM'
    this.cWorkTables[3]='MOX_COAN'
    this.cWorkTables[4]='MOX_COGE'
    this.cWorkTables[5]='MOX_COGM'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
