* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mda                                                        *
*              Dett.voci di bil. analitica                                     *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_101]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-15                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsbi_mda")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsbi_mda")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsbi_mda")
  return

* --- Class definition
define class tgsbi_mda as StdPCForm
  Width  = 757
  Height = 260
  Top    = 2
  Left   = 4
  cComment = "Dett.voci di bil. analitica"
  cPrg = "gsbi_mda"
  HelpContextID=188086679
  add object cnt as tcgsbi_mda
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsbi_mda as PCContext
  w_TIPVOC = space(1)
  w_DVCODICE = space(15)
  w_OK = space(1)
  w_MESS = space(50)
  w_DVTIPRIG = space(1)
  w_CPROWORD = 0
  w_DVCODVOC = space(15)
  w_DVCODVOB = space(15)
  w_DATAOBSO = space(8)
  w_DVFORMUL = space(30)
  w_DVFLOPER = space(1)
  w_DVFLPARE = space(2)
  w_DVCONDIZ = space(20)
  w_DESCRI = space(40)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DTOBSO = space(8)
  w_DATOBSO = space(8)
  w_OBTEST = space(8)
  proc Save(i_oFrom)
    this.w_TIPVOC = i_oFrom.w_TIPVOC
    this.w_DVCODICE = i_oFrom.w_DVCODICE
    this.w_OK = i_oFrom.w_OK
    this.w_MESS = i_oFrom.w_MESS
    this.w_DVTIPRIG = i_oFrom.w_DVTIPRIG
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DVCODVOC = i_oFrom.w_DVCODVOC
    this.w_DVCODVOB = i_oFrom.w_DVCODVOB
    this.w_DATAOBSO = i_oFrom.w_DATAOBSO
    this.w_DVFORMUL = i_oFrom.w_DVFORMUL
    this.w_DVFLOPER = i_oFrom.w_DVFLOPER
    this.w_DVFLPARE = i_oFrom.w_DVFLPARE
    this.w_DVCONDIZ = i_oFrom.w_DVCONDIZ
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_DESMAS = i_oFrom.w_DESMAS
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_OBTEST = i_oFrom.w_OBTEST
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TIPVOC = this.w_TIPVOC
    i_oTo.w_DVCODICE = this.w_DVCODICE
    i_oTo.w_OK = this.w_OK
    i_oTo.w_MESS = this.w_MESS
    i_oTo.w_DVTIPRIG = this.w_DVTIPRIG
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DVCODVOC = this.w_DVCODVOC
    i_oTo.w_DVCODVOB = this.w_DVCODVOB
    i_oTo.w_DATAOBSO = this.w_DATAOBSO
    i_oTo.w_DVFORMUL = this.w_DVFORMUL
    i_oTo.w_DVFLOPER = this.w_DVFLOPER
    i_oTo.w_DVFLPARE = this.w_DVFLPARE
    i_oTo.w_DVCONDIZ = this.w_DVCONDIZ
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_DESMAS = this.w_DESMAS
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsbi_mda as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 757
  Height = 260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=188086679
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VOCDBIAN_IDX = 0
  VOC_COST_IDX = 0
  VOC_BIAN_IDX = 0
  cFile = "VOCDBIAN"
  cKeySelect = "DVCODICE"
  cKeyWhere  = "DVCODICE=this.w_DVCODICE"
  cKeyDetail  = "DVCODICE=this.w_DVCODICE"
  cKeyWhereODBC = '"DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';

  cKeyDetailWhereODBC = '"DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"VOCDBIAN.DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VOCDBIAN.CPROWORD '
  cPrg = "gsbi_mda"
  cComment = "Dett.voci di bil. analitica"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPVOC = space(1)
  w_DVCODICE = space(15)
  w_OK = .F.
  w_MESS = space(50)
  w_DVTIPRIG = space(1)
  w_CPROWORD = 0
  w_DVCODVOC = space(15)
  w_DVCODVOB = space(15)
  w_DATAOBSO = ctod('  /  /  ')
  w_DVFORMUL = space(30)
  w_DVFLOPER = space(1)
  w_DVFLPARE = space(2)
  w_DVCONDIZ = space(20)
  w_DESCRI = space(40)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DTOBSO = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mdaPag1","gsbi_mda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOC_COST'
    this.cWorkTables[2]='VOC_BIAN'
    this.cWorkTables[3]='VOCDBIAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VOCDBIAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VOCDBIAN_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsbi_mda'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VOCDBIAN where DVCODICE=KeySet.DVCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2],this.bLoadRecFilter,this.VOCDBIAN_IDX,"gsbi_mda")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VOCDBIAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VOCDBIAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VOCDBIAN '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DVCODICE',this.w_DVCODICE  )
      select * from (i_cTable) VOCDBIAN where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPVOC = this.oParentObject .w_VRTIPVOC
        .w_OK = .T.
        .w_MESS = space(50)
        .w_DATOBSO = i_datsys
        .w_OBTEST = i_datsys
        .w_DVCODICE = NVL(DVCODICE,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'VOCDBIAN')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DATAOBSO = ctod("  /  /  ")
          .w_DESMAS = space(40)
          .w_DESCON = space(40)
          .w_DTOBSO = ctod("  /  /  ")
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DVTIPRIG = NVL(DVTIPRIG,space(1))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DVCODVOC = NVL(DVCODVOC,space(15))
          if link_2_3_joined
            this.w_DVCODVOC = NVL(VCCODICE203,NVL(this.w_DVCODVOC,space(15)))
            this.w_DESCON = NVL(VCDESCRI203,space(40))
            this.w_DATAOBSO = NVL(cp_ToDate(VCDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_DVCODVOB = NVL(DVCODVOB,space(15))
          if link_2_4_joined
            this.w_DVCODVOB = NVL(VRCODVOC204,NVL(this.w_DVCODVOB,space(15)))
            this.w_DESMAS = NVL(VRDESVOC204,space(40))
            this.w_DTOBSO = NVL(cp_ToDate(VRDTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_DVFORMUL = NVL(DVFORMUL,space(30))
          .w_DVFLOPER = NVL(DVFLOPER,space(1))
          .w_DVFLPARE = NVL(DVFLPARE,space(2))
          .w_DVCONDIZ = NVL(DVCONDIZ,space(20))
        .w_DESCRI = IIF(.w_DVTIPRIG='N', .w_DESCON, IIF(.w_DVTIPRIG='T', .w_DESMAS, SPACE(40)))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TIPVOC=space(1)
      .w_DVCODICE=space(15)
      .w_OK=.f.
      .w_MESS=space(50)
      .w_DVTIPRIG=space(1)
      .w_CPROWORD=10
      .w_DVCODVOC=space(15)
      .w_DVCODVOB=space(15)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DVFORMUL=space(30)
      .w_DVFLOPER=space(1)
      .w_DVFLPARE=space(2)
      .w_DVCONDIZ=space(20)
      .w_DESCRI=space(40)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_TIPVOC = this.oParentObject .w_VRTIPVOC
        .DoRTCalc(2,2,.f.)
        .w_OK = .T.
        .DoRTCalc(4,4,.f.)
        .w_DVTIPRIG = this.oParentObject .w_VRTIPVOC
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_DVCODVOC))
         .link_2_3('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DVCODVOB))
         .link_2_4('Full')
        endif
        .DoRTCalc(9,10,.f.)
        .w_DVFLOPER = '+'
        .w_DVFLPARE = '  '
        .DoRTCalc(13,13,.f.)
        .w_DESCRI = IIF(.w_DVTIPRIG='N', .w_DESCON, IIF(.w_DVTIPRIG='T', .w_DESMAS, SPACE(40)))
        .DoRTCalc(15,17,.f.)
        .w_DATOBSO = i_datsys
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VOCDBIAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'VOCDBIAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DVCODICE,"DVCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_DVCODVOC C(15);
      ,t_DVCODVOB C(15);
      ,t_DVFORMUL C(30);
      ,t_DVFLOPER N(3);
      ,t_DVFLPARE N(3);
      ,t_DVCONDIZ C(20);
      ,t_DESCRI C(40);
      ,CPROWNUM N(10);
      ,t_DVTIPRIG C(1);
      ,t_DATAOBSO D(8);
      ,t_DESMAS C(40);
      ,t_DESCON C(40);
      ,t_DTOBSO D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mdabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_3.controlsource=this.cTrsName+'.t_DVCODVOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOB_2_4.controlsource=this.cTrsName+'.t_DVCODVOB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_6.controlsource=this.cTrsName+'.t_DVFORMUL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.controlsource=this.cTrsName+'.t_DVFLOPER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.controlsource=this.cTrsName+'.t_DVFLPARE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_9.controlsource=this.cTrsName+'.t_DVCONDIZ'
    this.oPgFRm.Page1.oPag.oDESCRI_2_10.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(43)
    this.AddVLine(163)
    this.AddVLine(284)
    this.AddVLine(506)
    this.AddVLine(544)
    this.AddVLine(579)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2])
      *
      * insert into VOCDBIAN
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VOCDBIAN')
        i_extval=cp_InsertValODBCExtFlds(this,'VOCDBIAN')
        i_cFldBody=" "+;
                  "(DVCODICE,DVTIPRIG,CPROWORD,DVCODVOC,DVCODVOB"+;
                  ",DVFORMUL,DVFLOPER,DVFLPARE,DVCONDIZ,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DVCODICE)+","+cp_ToStrODBC(this.w_DVTIPRIG)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DVCODVOC)+","+cp_ToStrODBCNull(this.w_DVCODVOB)+;
             ","+cp_ToStrODBC(this.w_DVFORMUL)+","+cp_ToStrODBC(this.w_DVFLOPER)+","+cp_ToStrODBC(this.w_DVFLPARE)+","+cp_ToStrODBC(this.w_DVCONDIZ)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VOCDBIAN')
        i_extval=cp_InsertValVFPExtFlds(this,'VOCDBIAN')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DVCODICE',this.w_DVCODICE)
        INSERT INTO (i_cTable) (;
                   DVCODICE;
                  ,DVTIPRIG;
                  ,CPROWORD;
                  ,DVCODVOC;
                  ,DVCODVOB;
                  ,DVFORMUL;
                  ,DVFLOPER;
                  ,DVFLPARE;
                  ,DVCONDIZ;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DVCODICE;
                  ,this.w_DVTIPRIG;
                  ,this.w_CPROWORD;
                  ,this.w_DVCODVOC;
                  ,this.w_DVCODVOB;
                  ,this.w_DVFORMUL;
                  ,this.w_DVFLOPER;
                  ,this.w_DVFLPARE;
                  ,this.w_DVCONDIZ;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 AND NOT EMPTY(t_DVCODVOC+t_DVCODVOB)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VOCDBIAN')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VOCDBIAN')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_DVCODVOC+t_DVCODVOB)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VOCDBIAN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VOCDBIAN')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DVTIPRIG="+cp_ToStrODBC(this.w_DVTIPRIG)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DVCODVOC="+cp_ToStrODBCNull(this.w_DVCODVOC)+;
                     ",DVCODVOB="+cp_ToStrODBCNull(this.w_DVCODVOB)+;
                     ",DVFORMUL="+cp_ToStrODBC(this.w_DVFORMUL)+;
                     ",DVFLOPER="+cp_ToStrODBC(this.w_DVFLOPER)+;
                     ",DVFLPARE="+cp_ToStrODBC(this.w_DVFLPARE)+;
                     ",DVCONDIZ="+cp_ToStrODBC(this.w_DVCONDIZ)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VOCDBIAN')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DVTIPRIG=this.w_DVTIPRIG;
                     ,CPROWORD=this.w_CPROWORD;
                     ,DVCODVOC=this.w_DVCODVOC;
                     ,DVCODVOB=this.w_DVCODVOB;
                     ,DVFORMUL=this.w_DVFORMUL;
                     ,DVFLOPER=this.w_DVFLOPER;
                     ,DVFLPARE=this.w_DVFLPARE;
                     ,DVCONDIZ=this.w_DVCONDIZ;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_DVCODVOC+t_DVCODVOB)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VOCDBIAN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_DVCODVOC+t_DVCODVOB)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VOCDBIAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDBIAN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,13,.t.)
          .w_DESCRI = IIF(.w_DVTIPRIG='N', .w_DESCON, IIF(.w_DVTIPRIG='T', .w_DESMAS, SPACE(40)))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DVTIPRIG with this.w_DVTIPRIG
      replace t_DATAOBSO with this.w_DATAOBSO
      replace t_DESMAS with this.w_DESMAS
      replace t_DESCON with this.w_DESCON
      replace t_DTOBSO with this.w_DTOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOC_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOC_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOB_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOB_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DVCODVOC
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DVCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DVCODVOC))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_DVCODVOC)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_DVCODVOC)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDVCODVOC_2_3'),i_cWhere,'',"Voci di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DVCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DVCODVOC)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESCON = NVL(_Link_.VCDESCRI,space(40))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DVCODVOC = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST OR .cFunction='Query'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di analitica inesistente oppure obsoleta")
        endif
        this.w_DVCODVOC = space(15)
        this.w_DESCON = space(40)
        this.w_DATAOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.VCCODICE as VCCODICE203"+ ",link_2_3.VCDESCRI as VCDESCRI203"+ ",link_2_3.VCDTOBSO as VCDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on VOCDBIAN.DVCODVOC=link_2_3.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and VOCDBIAN.DVCODVOC=link_2_3.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DVCODVOB
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_BIAN_IDX,3]
    i_lTable = "VOC_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2], .t., this.VOC_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODVOB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVA',True,'VOC_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_DVCODVOB)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_DVCODVOB))
          select VRCODVOC,VRDESVOC,VRDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODVOB)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VRDESVOC like "+cp_ToStrODBC(trim(this.w_DVCODVOB)+"%");

            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VRDESVOC like "+cp_ToStr(trim(this.w_DVCODVOB)+"%");

            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODVOB) and !this.bDontReportError
            deferred_cp_zoom('VOC_BIAN','*','VRCODVOC',cp_AbsName(oSource.parent,'oDVCODVOB_2_4'),i_cWhere,'GSBI_AVA',"Voci di bilancio analitica",'GSBI_MDA.VOC_BIAN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODVOB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_DVCODVOB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_DVCODVOB)
            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODVOB = NVL(_Link_.VRCODVOC,space(15))
      this.w_DESMAS = NVL(_Link_.VRDESVOC,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VRDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DVCODVOB = space(15)
      endif
      this.w_DESMAS = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) OR .cFunction='Query')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce errato o obsoleto")
        endif
        this.w_DVCODVOB = space(15)
        this.w_DESMAS = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODVOB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_BIAN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.VRCODVOC as VRCODVOC204"+ ",link_2_4.VRDESVOC as VRDESVOC204"+ ",link_2_4.VRDTOBSO as VRDTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on VOCDBIAN.DVCODVOB=link_2_4.VRCODVOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and VOCDBIAN.DVCODVOB=link_2_4.VRCODVOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCRI_2_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_2_10.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oDESCRI_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_3.value==this.w_DVCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_3.value=this.w_DVCODVOC
      replace t_DVCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOB_2_4.value==this.w_DVCODVOB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOB_2_4.value=this.w_DVCODVOB
      replace t_DVCODVOB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOB_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_6.value==this.w_DVFORMUL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_6.value=this.w_DVFORMUL
      replace t_DVFORMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.RadioValue()==this.w_DVFLOPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.SetRadio()
      replace t_DVFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.RadioValue()==this.w_DVFLPARE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.SetRadio()
      replace t_DVFLPARE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_9.value==this.w_DVCONDIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_9.value=this.w_DVCONDIZ
      replace t_DVCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'VOCDBIAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST OR .cFunction='Query') and (.w_DVTIPRIG='N') and not(empty(.w_DVCODVOC)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_DVCODVOC+.w_DVCODVOB))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Voce di analitica inesistente oppure obsoleta")
        case   not(((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) OR .cFunction='Query')) and (.w_DVTIPRIG='T') and not(empty(.w_DVCODVOB)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_DVCODVOC+.w_DVCODVOB))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOB_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice voce errato o obsoleto")
      endcase
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_DVCODVOC+.w_DVCODVOB)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_DVCODVOC+t_DVCODVOB))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DVTIPRIG=space(1)
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_DVCODVOC=space(15)
      .w_DVCODVOB=space(15)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DVFORMUL=space(30)
      .w_DVFLOPER=space(1)
      .w_DVFLPARE=space(2)
      .w_DVCONDIZ=space(20)
      .w_DESCRI=space(40)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DTOBSO=ctod("  /  /  ")
      .DoRTCalc(1,4,.f.)
        .w_DVTIPRIG = this.oParentObject .w_VRTIPVOC
      .DoRTCalc(6,7,.f.)
      if not(empty(.w_DVCODVOC))
        .link_2_3('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_DVCODVOB))
        .link_2_4('Full')
      endif
      .DoRTCalc(9,10,.f.)
        .w_DVFLOPER = '+'
        .w_DVFLPARE = '  '
      .DoRTCalc(13,13,.f.)
        .w_DESCRI = IIF(.w_DVTIPRIG='N', .w_DESCON, IIF(.w_DVTIPRIG='T', .w_DESMAS, SPACE(40)))
    endwith
    this.DoRTCalc(15,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DVTIPRIG = t_DVTIPRIG
    this.w_CPROWORD = t_CPROWORD
    this.w_DVCODVOC = t_DVCODVOC
    this.w_DVCODVOB = t_DVCODVOB
    this.w_DATAOBSO = t_DATAOBSO
    this.w_DVFORMUL = t_DVFORMUL
    this.w_DVFLOPER = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.RadioValue(.t.)
    this.w_DVFLPARE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.RadioValue(.t.)
    this.w_DVCONDIZ = t_DVCONDIZ
    this.w_DESCRI = t_DESCRI
    this.w_DESMAS = t_DESMAS
    this.w_DESCON = t_DESCON
    this.w_DTOBSO = t_DTOBSO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DVTIPRIG with this.w_DVTIPRIG
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DVCODVOC with this.w_DVCODVOC
    replace t_DVCODVOB with this.w_DVCODVOB
    replace t_DATAOBSO with this.w_DATAOBSO
    replace t_DVFORMUL with this.w_DVFORMUL
    replace t_DVFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_7.ToRadio()
    replace t_DVFLPARE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_8.ToRadio()
    replace t_DVCONDIZ with this.w_DVCONDIZ
    replace t_DESCRI with this.w_DESCRI
    replace t_DESMAS with this.w_DESMAS
    replace t_DESCON with this.w_DESCON
    replace t_DTOBSO with this.w_DTOBSO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mdaPag1 as StdContainer
  Width  = 753
  height = 260
  stdWidth  = 753
  stdheight = 260
  resizeXpos=437
  resizeYpos=151
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_4 as StdButton with uid="WCBAVCIVHU",left=696, top=212, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere il bottone per avviare la verifica della formula";
    , HelpContextID = 184976567;
    , tabstop=.f., caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSBI_BVA (this.Parent.oContained,"Cont")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    with this.Parent.oContained
      return (.w_TIPVOC='N' OR .w_TIPVOC='T')
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=1, top=1, width=745,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Seq.",Field2="DVCODVOC",Label2="Voce di analitica",Field3="DVCODVOB",Label3="Voce di bilancio",Field4="DVFORMUL",Label4="Formula",Field5="DVFLOPER",Label5="Oper.",Field6="DVFLPARE",Label6="Par.",Field7="DVCONDIZ",Label7="Condizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235742074

  add object oStr_1_3 as StdString with uid="HJGATKEBIN",Visible=.t., Left=5, Top=222,;
    Alignment=1, Width=104, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=21,;
    width=741+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=22,width=740+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|VOC_BIAN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCRI_2_10.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oDVCODVOC_2_3
      case cFile='VOC_BIAN'
        oDropInto=this.oBodyCol.oRow.oDVCODVOB_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCRI_2_10 as StdTrsField with uid="TIIWZKSIVF",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 107056330,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=112, Top=222, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsbi_mdaBodyRow as CPBodyRowCnt
  Width=731
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="XXJOYGRDKE",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di elaborazione",;
    HelpContextID = 364950,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDVCODVOC_2_3 as StdTrsField with uid="DLXAGRRECX",rtseq=7,rtrep=.t.,;
    cFormVar="w_DVCODVOC",value=space(15),;
    ToolTipText = "Codice voce analitica",;
    HelpContextID = 171342727,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Voce di analitica inesistente oppure obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=41, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", oKey_1_1="VCCODICE", oKey_1_2="this.w_DVCODVOC"

  func oDVCODVOC_2_3.mCond()
    with this.Parent.oContained
      return (.w_DVTIPRIG='N')
    endwith
  endfunc

  func oDVCODVOC_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODVOC_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODVOC_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDVCODVOC_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Voci di costo/ricavo",'',this.parent.oContained
  endproc

  add object oDVCODVOB_2_4 as StdTrsField with uid="ROEJQTOETT",rtseq=8,rtrep=.t.,;
    cFormVar="w_DVCODVOB",value=space(15),;
    ToolTipText = "Codice voce di bilancio di analitica",;
    HelpContextID = 171342728,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce errato o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=162, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_BIAN", cZoomOnZoom="GSBI_AVA", oKey_1_1="VRCODVOC", oKey_1_2="this.w_DVCODVOB"

  func oDVCODVOB_2_4.mCond()
    with this.Parent.oContained
      return (.w_DVTIPRIG='T')
    endwith
  endfunc

  func oDVCODVOB_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODVOB_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODVOB_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_BIAN','*','VRCODVOC',cp_AbsName(this.parent,'oDVCODVOB_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVA',"Voci di bilancio analitica",'GSBI_MDA.VOC_BIAN_VZM',this.parent.oContained
  endproc
  proc oDVCODVOB_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_DVCODVOB
    i_obj.ecpSave()
  endproc

  add object oDVFORMUL_2_6 as StdTrsField with uid="USQIRPRGYD",rtseq=10,rtrep=.t.,;
    cFormVar="w_DVFORMUL",value=space(30),;
    ToolTipText = "Eventuale ulteriore formula di elaborazione della riga",;
    HelpContextID = 229225602,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=219, Left=282, Top=0, InputMask=replicate('X',30)

  add object oDVFLOPER_2_7 as StdTrsCombo with uid="JSVQRAOXDT",rtrep=.t.,;
    cFormVar="w_DVFLOPER", RowSource=""+"+ = Somma,"+"- = Sottrae,"+"/ = Divide,"+"* = Moltiplica" , ;
    ToolTipText = "Tipo di operazione tra le righe",;
    HelpContextID = 7779464,;
    Height=21, Width=35, Left=502, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDVFLOPER_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DVFLOPER,&i_cF..t_DVFLOPER),this.value)
    return(iif(xVal =1,'+',;
    iif(xVal =2,'-',;
    iif(xVal =3,'/',;
    iif(xVal =4,'*',;
    space(1))))))
  endfunc
  func oDVFLOPER_2_7.GetRadio()
    this.Parent.oContained.w_DVFLOPER = this.RadioValue()
    return .t.
  endfunc

  func oDVFLOPER_2_7.ToRadio()
    this.Parent.oContained.w_DVFLOPER=trim(this.Parent.oContained.w_DVFLOPER)
    return(;
      iif(this.Parent.oContained.w_DVFLOPER=='+',1,;
      iif(this.Parent.oContained.w_DVFLOPER=='-',2,;
      iif(this.Parent.oContained.w_DVFLOPER=='/',3,;
      iif(this.Parent.oContained.w_DVFLOPER=='*',4,;
      0)))))
  endfunc

  func oDVFLOPER_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDVFLPARE_2_8 as StdTrsCombo with uid="PYCFFNDUPE",rtrep=.t.,;
    cFormVar="w_DVFLPARE", RowSource=""+","+"( = Apre parentesi,"+") = Chiude parent.,"+"(( = Apre doppia,"+")) = Chiude doppia" , ;
    ToolTipText = "Eventuale parentesi",;
    HelpContextID = 242830213,;
    Height=21, Width=30, Left=542, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDVFLPARE_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DVFLPARE,&i_cF..t_DVFLPARE),this.value)
    return(iif(xVal =1,'  ',;
    iif(xVal =2,'(',;
    iif(xVal =3,')',;
    iif(xVal =4,'((',;
    iif(xVal =5,'))',;
    space(2)))))))
  endfunc
  func oDVFLPARE_2_8.GetRadio()
    this.Parent.oContained.w_DVFLPARE = this.RadioValue()
    return .t.
  endfunc

  func oDVFLPARE_2_8.ToRadio()
    this.Parent.oContained.w_DVFLPARE=trim(this.Parent.oContained.w_DVFLPARE)
    return(;
      iif(this.Parent.oContained.w_DVFLPARE=='',1,;
      iif(this.Parent.oContained.w_DVFLPARE=='(',2,;
      iif(this.Parent.oContained.w_DVFLPARE==')',3,;
      iif(this.Parent.oContained.w_DVFLPARE=='((',4,;
      iif(this.Parent.oContained.w_DVFLPARE=='))',5,;
      0))))))
  endfunc

  func oDVFLPARE_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDVCONDIZ_2_9 as StdTrsField with uid="GJKNIHKTAK",rtseq=13,rtrep=.t.,;
    cFormVar="w_DVCONDIZ",value=space(20),;
    ToolTipText = "Condizione di elaborazione della riga",;
    HelpContextID = 74024080,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=149, Left=577, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mda','VOCDBIAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DVCODICE=VOCDBIAN.DVCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
