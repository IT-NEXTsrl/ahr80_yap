* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sba                                                        *
*              Stampa bilancio analitica                                       *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_54]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2013-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sba",oParentObject))

* --- Class definition
define class tgsbi_sba as StdForm
  Top    = 50
  Left   = 56

  * --- Standard Properties
  Width  = 657
  Height = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-09"
  HelpContextID=160823703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  STR_BIAN_IDX = 0
  PER_ELAB_IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsbi_sba"
  cComment = "Stampa bilancio analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_ESER = space(4)
  o_ESER = space(4)
  w_INESER = ctod('  /  /  ')
  w_FINESER = ctod('  /  /  ')
  w_STATO = space(1)
  w_STRUTTUR = space(15)
  w_MOVIMENT = space(1)
  w_PERIODO = space(15)
  w_TIPBILAN = space(1)
  w_DESCRI = space(40)
  w_DESCPER = space(35)
  w_VALUTA = space(3)
  w_NUMERO = 0
  w_ESERCI = space(4)
  w_DATABIL = ctod('  /  /  ')
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_DESCRIBI = space(35)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_MONETE = space(1)
  o_MONETE = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_SIMVAL = space(5)
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_CAMBGIOR = 0
  w_CAMVAL = 0
  o_CAMVAL = 0
  w_DECIMI = 0
  w_TIPOSTAM = space(1)
  w_RIGHESI = space(1)
  w_COMMESSA = space(15)
  w_DESCCOM = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sbaPag1","gsbi_sba",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESER_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='STR_BIAN'
    this.cWorkTables[3]='PER_ELAB'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='AZIENDA'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_DTOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_ESER=space(4)
      .w_INESER=ctod("  /  /  ")
      .w_FINESER=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_STRUTTUR=space(15)
      .w_MOVIMENT=space(1)
      .w_PERIODO=space(15)
      .w_TIPBILAN=space(1)
      .w_DESCRI=space(40)
      .w_DESCPER=space(35)
      .w_VALUTA=space(3)
      .w_NUMERO=0
      .w_ESERCI=space(4)
      .w_DATABIL=ctod("  /  /  ")
      .w_VALAPP=space(3)
      .w_DESCRIBI=space(35)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_MONETE=space(1)
      .w_DIVISA=space(3)
      .w_SIMVAL=space(5)
      .w_EXTRAEUR=0
      .w_CAMBGIOR=0
      .w_CAMVAL=0
      .w_DECIMI=0
      .w_TIPOSTAM=space(1)
      .w_RIGHESI=space(1)
      .w_COMMESSA=space(15)
      .w_DESCCOM=space(30)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_DTOBSO = i_datsys
        .w_OBTEST = i_datsys
        .w_ESER = g_codese
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ESER))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_STATO = 'S'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_8('Full')
        endif
        .w_MOVIMENT = 'E'
        .w_PERIODO = SPACE(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PERIODO))
          .link_1_10('Full')
        endif
        .w_TIPBILAN = 'A'
          .DoRTCalc(12,13,.f.)
        .w_VALUTA = g_perval
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_ESERCI))
          .link_1_26('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(17,21,.f.)
        .w_MONETE = 'c'
        .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_DIVISA)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_DIVISA))
          .link_1_39('Full')
        endif
          .DoRTCalc(24,25,.f.)
        .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        .w_CAMVAL = GETCAM( .w_DIVISA , i_DATSYS )
          .DoRTCalc(28,28,.f.)
        .w_TIPOSTAM = 'S'
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_COMMESSA))
          .link_1_51('Full')
        endif
    endwith
    this.DoRTCalc(32,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,9,.t.)
        if .o_ESER<>.w_ESER
            .w_PERIODO = SPACE(15)
          .link_1_10('Full')
        endif
        .DoRTCalc(11,15,.t.)
          .link_1_26('Full')
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(17,22,.t.)
        if .o_MONETE<>.w_MONETE.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_DIVISA)
          .link_1_39('Full')
        endif
        .DoRTCalc(24,25,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        endif
        if .o_EXTRAEUR<>.w_EXTRAEUR
            .w_CAMVAL = GETCAM( .w_DIVISA , i_DATSYS )
        endif
        if .o_CAMVAL<>.w_CAMVAL
          .Calculate_SWSDHBLJFS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
  return

  proc Calculate_SWSDHBLJFS()
    with this
          * --- Inserisci cambio giornaliero
     if not empty(.w_DIVISA) and .w_DIVISA<>g_perval and .w_CAMBGIOR<>.w_CAMVAL
          GSAR_BIC(this;
              ,.w_DIVISA;
              ,.w_CAMVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDIVISA_1_39.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_44.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDIVISA_1_39.visible=!this.oPgFrm.Page1.oPag.oDIVISA_1_39.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_40.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_44.visible=!this.oPgFrm.Page1.oPag.oCAMVAL_1_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_4'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_INESER = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESER = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_INESER = ctod("  /  /  ")
      this.w_FINESER = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSA',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_8'),i_cWhere,'GSBI_MSA',"Strutture di bilancio analitica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE,PEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_10'),i_cWhere,'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
      this.w_DESCPER = NVL(_Link_.PEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
      this.w_DESCPER = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_39'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_DECIMI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_EXTRAEUR = 0
      this.w_DECIMI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMESSA
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMESSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMESSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMESSA))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMESSA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMMESSA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMESSA_1_51'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMESSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMESSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMESSA)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMESSA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMMESSA = space(15)
      endif
      this.w_DESCCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMESSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESER_1_4.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_4.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oINESER_1_5.value==this.w_INESER)
      this.oPgFrm.Page1.oPag.oINESER_1_5.value=this.w_INESER
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESER_1_6.value==this.w_FINESER)
      this.oPgFrm.Page1.oPag.oFINESER_1_6.value=this.w_FINESER
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_7.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_8.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_8.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_9.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_10.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_10.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_15.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_15.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPER_1_16.value==this.w_DESCPER)
      this.oPgFrm.Page1.oPag.oDESCPER_1_16.value=this.w_DESCPER
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_22.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_22.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_26.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_26.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATABIL_1_27.value==this.w_DATABIL)
      this.oPgFrm.Page1.oPag.oDATABIL_1_27.value=this.w_DATABIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIBI_1_30.value==this.w_DESCRIBI)
      this.oPgFrm.Page1.oPag.oDESCRIBI_1_30.value=this.w_DESCRIBI
    endif
    if not(this.oPgFrm.Page1.oPag.oMONETE_1_37.RadioValue()==this.w_MONETE)
      this.oPgFrm.Page1.oPag.oMONETE_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_39.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_39.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_40.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_44.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_44.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAM_1_46.RadioValue()==this.w_TIPOSTAM)
      this.oPgFrm.Page1.oPag.oTIPOSTAM_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGHESI_1_50.RadioValue()==this.w_RIGHESI)
      this.oPgFrm.Page1.oPag.oRIGHESI_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMESSA_1_51.value==this.w_COMMESSA)
      this.oPgFrm.Page1.oPag.oCOMMESSA_1_51.value=this.w_COMMESSA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCOM_1_53.value==this.w_DESCCOM)
      this.oPgFrm.Page1.oPag.oDESCCOM_1_53.value=this.w_DESCCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DIVISA))  and not(.w_monete='c')  and (.w_monete<>'c')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_39.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and not(EMPTY(.w_DIVISA) OR .w_monete='c')  and (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 or ( FASETRAN(i_DATSYS)=1 and .w_EXTRAEUR=0 )))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_44.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    this.o_VALAPP = this.w_VALAPP
    this.o_MONETE = this.w_MONETE
    this.o_DIVISA = this.w_DIVISA
    this.o_EXTRAEUR = this.w_EXTRAEUR
    this.o_CAMVAL = this.w_CAMVAL
    return

enddefine

* --- Define pages as container
define class tgsbi_sbaPag1 as StdContainer
  Width  = 653
  height = 346
  stdWidth  = 653
  stdheight = 346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESER_1_4 as StdField with uid="ACXPUSYCDS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 166502726,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=103, Top=27, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oINESER_1_5 as StdField with uid="MUUGPWEVVX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INESER", cQueryName = "INESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 264397690,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=315, Top=27

  add object oFINESER_1_6 as StdField with uid="DSPABECZEL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FINESER", cQueryName = "FINESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 200267946,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=498, Top=27


  add object oSTATO_1_7 as StdCombo with uid="CYWFGOCANO",rtseq=7,rtrep=.f.,left=103,top=63,width=114,height=21;
    , ToolTipText = "Stato bilancio selezionato";
    , HelpContextID = 249455398;
    , cFormVar="w_STATO",RowSource=""+"Confermato,"+"Da confermare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSTATO_1_7.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_7.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      0))
  endfunc

  add object oSTRUTTUR_1_8 as StdField with uid="VGRHFNBAME",rtseq=8,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 53506936,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=280, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", cZoomOnZoom="GSBI_MSA", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSA',"Strutture di bilancio analitica",'',this.parent.oContained
  endproc
  proc oSTRUTTUR_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR
     i_obj.ecpSave()
  endproc


  add object oMOVIMENT_1_9 as StdCombo with uid="YDJHABBOCP",rtseq=9,rtrep=.f.,left=103,top=97,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 206262758;
    , cFormVar="w_MOVIMENT",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_9.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oMOVIMENT_1_9.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_9.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='E',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      0))
  endfunc

  add object oPERIODO_1_10 as StdField with uid="SLITBMOTKC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 220961802,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=280, Top=98, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_15 as StdField with uid="RCZMVIYQZH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134319306,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=398, Top=63, InputMask=replicate('X',40)

  add object oDESCPER_1_16 as StdField with uid="JJCKGGGWSZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCPER", cQueryName = "DESCPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203525322,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=398, Top=98, InputMask=replicate('X',35)


  add object oBtn_1_19 as StdButton with uid="FZSZWUJISJ",left=80, top=188, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 161024726;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      do GSBI_KBI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_22 as StdField with uid="XVMVNIDKRQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 33545258,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=63, Top=221

  add object oESERCI_1_26 as StdField with uid="WWCJWDOJWQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 149118650,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=190, Top=221, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDATABIL_1_27 as StdField with uid="OWQQARLKZY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATABIL", cQueryName = "DATABIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 151224522,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=280, Top=221

  add object oDESCRIBI_1_30 as StdField with uid="LXATZDCGYG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRIBI", cQueryName = "DESCRIBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134319233,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=412, Top=221, InputMask=replicate('X',35)


  add object oObj_1_32 as cp_runprogram with uid="LCIVXRYOEO",left=684, top=206, width=159,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BBI",;
    cEvent = "w_STATO Changed,w_STRUTTUR Changed,w_MOVIMENT Changed,w_PERIODO Changed,w_ESER Changed,w_COMMESSA Changed",;
    nPag=1;
    , HelpContextID = 198049562


  add object oMONETE_1_37 as StdCombo with uid="BIWCCEJSBA",rtseq=22,rtrep=.f.,left=132,top=257,width=122,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 199217722;
    , cFormVar="w_MONETE",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMONETE_1_37.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func oMONETE_1_37.GetRadio()
    this.Parent.oContained.w_MONETE = this.RadioValue()
    return .t.
  endfunc

  func oMONETE_1_37.SetRadio()
    this.Parent.oContained.w_MONETE=trim(this.Parent.oContained.w_MONETE)
    this.value = ;
      iif(this.Parent.oContained.w_MONETE=='c',1,;
      iif(this.Parent.oContained.w_MONETE=='a',2,;
      0))
  endfunc

  add object oDIVISA_1_39 as StdField with uid="ZHQKKEBMSY",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 267081930,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=347, Top=257, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete<>'c')
    endwith
   endif
  endfunc

  func oDIVISA_1_39.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  func oDIVISA_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc

  add object oSIMVAL_1_40 as StdField with uid="TWYDJBLHWJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 100591578,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=399, Top=257, InputMask=replicate('X',5)

  func oSIMVAL_1_40.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  add object oCAMVAL_1_44 as StdField with uid="QLEYAVSPUF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 100593882,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=517, Top=257, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 or ( FASETRAN(i_DATSYS)=1 and .w_EXTRAEUR=0 )))
    endwith
   endif
  endfunc

  func oCAMVAL_1_44.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_monete='c')
    endwith
  endfunc


  add object oTIPOSTAM_1_46 as StdCombo with uid="DULHMCOZZG",rtseq=29,rtrep=.f.,left=132,top=291,width=142,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 216381309;
    , cFormVar="w_TIPOSTAM",RowSource=""+"Stampa su Excel,"+"Stampa standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTAM_1_46.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPOSTAM_1_46.GetRadio()
    this.Parent.oContained.w_TIPOSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAM_1_46.SetRadio()
    this.Parent.oContained.w_TIPOSTAM=trim(this.Parent.oContained.w_TIPOSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAM=='E',1,;
      iif(this.Parent.oContained.w_TIPOSTAM=='S',2,;
      0))
  endfunc

  add object oRIGHESI_1_50 as StdCheck with uid="ORMNVLVRNK",rtseq=30,rtrep=.f.,left=347, top=288, caption="Righe significative",;
    ToolTipText = "Stampa solo righe valorizzate",;
    HelpContextID = 20101142,;
    cFormVar="w_RIGHESI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRIGHESI_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRIGHESI_1_50.GetRadio()
    this.Parent.oContained.w_RIGHESI = this.RadioValue()
    return .t.
  endfunc

  func oRIGHESI_1_50.SetRadio()
    this.Parent.oContained.w_RIGHESI=trim(this.Parent.oContained.w_RIGHESI)
    this.value = ;
      iif(this.Parent.oContained.w_RIGHESI=='S',1,;
      0)
  endfunc

  add object oCOMMESSA_1_51 as StdField with uid="DLQCYXLWLH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_COMMESSA", cQueryName = "COMMESSA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 20454759,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=103, Top=131, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMESSA"

  func oCOMMESSA_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMESSA_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMESSA_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMESSA_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCCOM_1_53 as StdField with uid="TDAEBMOWBO",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCCOM", cQueryName = "DESCCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 49384650,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=225, Top=131, InputMask=replicate('X',30)


  add object oBtn_1_54 as StdButton with uid="OXPTYCNQOO",left=543, top=295, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 160852454;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        do GSBI_BB4 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO))
      endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="EFZTAPUEYA",left=597, top=295, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 168141126;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="SWHSGVJBTB",Visible=.t., Left=30, Top=63,;
    Alignment=1, Width=70, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="XWSQVOXEMT",Visible=.t., Left=8, Top=99,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JXMCSYQQAP",Visible=.t., Left=217, Top=63,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EDXVWJHFFL",Visible=.t., Left=220, Top=99,;
    Alignment=1, Width=58, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="HVSBLRGZIL",Visible=.t., Left=4, Top=190,;
    Alignment=0, Width=69, Height=15,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="LRMCWLXFAM",Visible=.t., Left=7, Top=4,;
    Alignment=0, Width=114, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="BAULRZMFJE",Visible=.t., Left=8, Top=221,;
    Alignment=1, Width=53, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WKVWHPTDIZ",Visible=.t., Left=121, Top=221,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QZOGOWFOLT",Visible=.t., Left=247, Top=221,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="LFNPLMLDMV",Visible=.t., Left=354, Top=221,;
    Alignment=1, Width=55, Height=15,;
    Caption="Descr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="IYCMIBNVMO",Visible=.t., Left=4, Top=291,;
    Alignment=1, Width=125, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KNPBQVIVRY",Visible=.t., Left=5, Top=257,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="SLIHWWPHBW",Visible=.t., Left=282, Top=257,;
    Alignment=1, Width=62, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_monete='c')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="OBBCYTHUBD",Visible=.t., Left=448, Top=257,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_monete='c')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="WWWALTACBE",Visible=.t., Left=30, Top=27,;
    Alignment=1, Width=70, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="YDTZLOMGXL",Visible=.t., Left=159, Top=27,;
    Alignment=1, Width=151, Height=15,;
    Caption="Inizio esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="CBWUNHHMPJ",Visible=.t., Left=403, Top=27,;
    Alignment=1, Width=93, Height=15,;
    Caption="Fine esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="SGZGUDGPMY",Visible=.t., Left=30, Top=131,;
    Alignment=1, Width=70, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="RKFJECBCXN",left=1, top=176, width=617,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sba','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
