* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_scg                                                        *
*              Stampa movimenti extra contabili                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_scg",oParentObject))

* --- Class definition
define class tgsbi_scg as StdForm
  Top    = 20
  Left   = 67

  * --- Standard Properties
  Width  = 553
  Height = 240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=90834537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  PER_ELAB_IDX = 0
  CONTBILC_IDX = 0
  REG_EXTM_IDX = 0
  GRU_REGO_IDX = 0
  MOX_COGM_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsbi_scg"
  cComment = "Stampa movimenti extra contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPO = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_CGNUMREG1 = 0
  w_CGNUMREG2 = 0
  w_CGDATREG1 = ctod('  /  /  ')
  w_CGDATREG2 = ctod('  /  /  ')
  w_CGTIPMOV = space(1)
  w_CGCODESE = space(4)
  w_CGVALNAZ = space(3)
  w_CGCODPER = space(15)
  w_DESPER = space(35)
  w_CGCODREG = space(15)
  w_DESREG = space(35)
  w_SIMVAL = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_scgPag1","gsbi_scg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCGNUMREG1_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='PER_ELAB'
    this.cWorkTables[2]='CONTBILC'
    this.cWorkTables[3]='REG_EXTM'
    this.cWorkTables[4]='GRU_REGO'
    this.cWorkTables[5]='MOX_COGM'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='VALUTE'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TIPO=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_CGNUMREG1=0
      .w_CGNUMREG2=0
      .w_CGDATREG1=ctod("  /  /  ")
      .w_CGDATREG2=ctod("  /  /  ")
      .w_CGTIPMOV=space(1)
      .w_CGCODESE=space(4)
      .w_CGVALNAZ=space(3)
      .w_CGCODPER=space(15)
      .w_DESPER=space(35)
      .w_CGCODREG=space(15)
      .w_DESREG=space(35)
      .w_SIMVAL=space(5)
        .w_CODAZI = i_CODAZI
        .w_TIPO = 'C'
        .w_DTOBSO = i_datsys
          .DoRTCalc(4,7,.f.)
        .w_CGTIPMOV = 'T'
        .w_CGCODESE = g_CODESE
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CGCODESE))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CGVALNAZ))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CGCODPER))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CGCODREG))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
    this.DoRTCalc(14,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CGCODESE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CGCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CGCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCGCODESE_1_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CGCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CGCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGVALNAZ
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CGVALNAZ)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CGVALNAZ))
          select VACODVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGVALNAZ)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGVALNAZ) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCGVALNAZ_1_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CGVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CGVALNAZ)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CGVALNAZ = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGCODPER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_CGCODPER)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_CGCODPER))
          select PECODICE,PEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODPER)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODPER) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oCGCODPER_1_11'),i_cWhere,'GSBI_APE',"Periodi di elaborazione",'gsbi_mcg.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_CGCODPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_CGCODPER)
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODPER = NVL(_Link_.PECODICE,space(15))
      this.w_DESPER = NVL(_Link_.PEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODPER = space(15)
      endif
      this.w_DESPER = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGCODREG
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_EXTM_IDX,3]
    i_lTable = "REG_EXTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2], .t., this.REG_EXTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MRE',True,'REG_EXTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_CGCODREG)+"%");
                   +" and RE__TIPO="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select RE__TIPO,RECODICE,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RE__TIPO,RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RE__TIPO',this.w_TIPO;
                     ,'RECODICE',trim(this.w_CGCODREG))
          select RE__TIPO,RECODICE,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RE__TIPO,RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODREG)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODREG) and !this.bDontReportError
            deferred_cp_zoom('REG_EXTM','*','RE__TIPO,RECODICE',cp_AbsName(oSource.parent,'oCGCODREG_1_13'),i_cWhere,'GSBI_MRE',"Regole di elaborazione",'gsbi_mcg.REG_EXTM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RE__TIPO,RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RE__TIPO,RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RE__TIPO,RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RE__TIPO="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RE__TIPO',oSource.xKey(1);
                       ,'RECODICE',oSource.xKey(2))
            select RE__TIPO,RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RE__TIPO,RECODICE,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_CGCODREG);
                   +" and RE__TIPO="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RE__TIPO',this.w_TIPO;
                       ,'RECODICE',this.w_CGCODREG)
            select RE__TIPO,RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODREG = NVL(_Link_.RECODICE,space(15))
      this.w_DESREG = NVL(_Link_.REDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODREG = space(15)
      endif
      this.w_DESREG = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])+'\'+cp_ToStr(_Link_.RE__TIPO,1)+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REG_EXTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCGNUMREG1_1_4.value==this.w_CGNUMREG1)
      this.oPgFrm.Page1.oPag.oCGNUMREG1_1_4.value=this.w_CGNUMREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oCGNUMREG2_1_5.value==this.w_CGNUMREG2)
      this.oPgFrm.Page1.oPag.oCGNUMREG2_1_5.value=this.w_CGNUMREG2
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDATREG1_1_6.value==this.w_CGDATREG1)
      this.oPgFrm.Page1.oPag.oCGDATREG1_1_6.value=this.w_CGDATREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDATREG2_1_7.value==this.w_CGDATREG2)
      this.oPgFrm.Page1.oPag.oCGDATREG2_1_7.value=this.w_CGDATREG2
    endif
    if not(this.oPgFrm.Page1.oPag.oCGTIPMOV_1_8.RadioValue()==this.w_CGTIPMOV)
      this.oPgFrm.Page1.oPag.oCGTIPMOV_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODESE_1_9.value==this.w_CGCODESE)
      this.oPgFrm.Page1.oPag.oCGCODESE_1_9.value=this.w_CGCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCGVALNAZ_1_10.value==this.w_CGVALNAZ)
      this.oPgFrm.Page1.oPag.oCGVALNAZ_1_10.value=this.w_CGVALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODPER_1_11.value==this.w_CGCODPER)
      this.oPgFrm.Page1.oPag.oCGCODPER_1_11.value=this.w_CGCODPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_12.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_12.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODREG_1_13.value==this.w_CGCODREG)
      this.oPgFrm.Page1.oPag.oCGCODREG_1_13.value=this.w_CGCODREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREG_1_14.value==this.w_DESREG)
      this.oPgFrm.Page1.oPag.oDESREG_1_14.value=this.w_DESREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_28.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_28.value=this.w_SIMVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_CGNUMREG2) or (.w_CGNUMREG2>=.w_CGNUMREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCGNUMREG1_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero registrazione iniziale � pi� grande del finale o errato")
          case   not(empty(.w_CGNUMREG1) or (.w_CGNUMREG2>=.w_CGNUMREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCGNUMREG2_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero registrazione iniziale � pi� grande del finale o errato")
          case   not(empty(.w_CGDATREG2) or (.w_CGDATREG2>=.w_CGDATREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCGDATREG1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di quella finale o errata")
          case   not(empty(.w_CGDATREG1) or (.w_CGDATREG2>=.w_CGDATREG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCGDATREG2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data finale minore di quella iniziale o errata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_scgPag1 as StdContainer
  Width  = 549
  height = 240
  stdWidth  = 549
  stdheight = 240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCGNUMREG1_1_4 as StdField with uid="GXZVPBWPZA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CGNUMREG1", cQueryName = "CGNUMREG1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero registrazione iniziale � pi� grande del finale o errato",;
    ToolTipText = "Progressivo numero di registrazione di partenza",;
    HelpContextID = 239064963,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=15

  func oCGNUMREG1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_CGNUMREG2) or (.w_CGNUMREG2>=.w_CGNUMREG1))
    endwith
    return bRes
  endfunc

  add object oCGNUMREG2_1_5 as StdField with uid="SJOVMNETMV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CGNUMREG2", cQueryName = "CGNUMREG2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero registrazione iniziale � pi� grande del finale o errato",;
    ToolTipText = "Progressivo numero di registrazione di arrivo",;
    HelpContextID = 239064947,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=44

  func oCGNUMREG2_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_CGNUMREG1) or (.w_CGNUMREG2>=.w_CGNUMREG1))
    endwith
    return bRes
  endfunc

  add object oCGDATREG1_1_6 as StdField with uid="PGYXRKDELR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CGDATREG1", cQueryName = "CGDATREG1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di quella finale o errata",;
    ToolTipText = "Data di registrazione di partenza",;
    HelpContextID = 233076611,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=269, Top=15

  func oCGDATREG1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_CGDATREG2) or (.w_CGDATREG2>=.w_CGDATREG1))
    endwith
    return bRes
  endfunc

  add object oCGDATREG2_1_7 as StdField with uid="RWFMAMXUYV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CGDATREG2", cQueryName = "CGDATREG2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data finale minore di quella iniziale o errata",;
    ToolTipText = "Data di registrazione di arrivo",;
    HelpContextID = 233076595,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=269, Top=44

  func oCGDATREG2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_CGDATREG1) or (.w_CGDATREG2>=.w_CGDATREG1))
    endwith
    return bRes
  endfunc


  add object oCGTIPMOV_1_8 as StdCombo with uid="EFHHHOXLWT",rtseq=8,rtrep=.f.,left=444,top=15,width=100,height=21;
    , ToolTipText = "Tipologia dei movimenti: effettivi/previsionali";
    , HelpContextID = 216302972;
    , cFormVar="w_CGTIPMOV",RowSource=""+"Effettivi,"+"Previsionali,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCGTIPMOV_1_8.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCGTIPMOV_1_8.GetRadio()
    this.Parent.oContained.w_CGTIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oCGTIPMOV_1_8.SetRadio()
    this.Parent.oContained.w_CGTIPMOV=trim(this.Parent.oContained.w_CGTIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_CGTIPMOV=='E',1,;
      iif(this.Parent.oContained.w_CGTIPMOV=='P',2,;
      iif(this.Parent.oContained.w_CGTIPMOV=='T',3,;
      0)))
  endfunc

  add object oCGCODESE_1_9 as StdField with uid="BQOCMMNYZX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CGCODESE", cQueryName = "CGCODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento della registrazione di partenza",;
    HelpContextID = 69825899,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=135, Top=73, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CGCODESE"

  func oCGCODESE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODESE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODESE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCGCODESE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCGVALNAZ_1_10 as StdField with uid="CXYKWILBNW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CGVALNAZ", cQueryName = "CGVALNAZ",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 228369792,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=269, Top=73, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CGVALNAZ"

  func oCGVALNAZ_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGVALNAZ_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGVALNAZ_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCGVALNAZ_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCGCODPER_1_11 as StdField with uid="ISWTYYVSWE",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CGCODPER", cQueryName = "CGCODPER",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice periodo elaborazione",;
    HelpContextID = 254375288,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=102, cSayPict="'!!!!!!!!!!!!!!!'", cGetPict="'!!!!!!!!!!!!!!!'", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_CGCODPER"

  func oCGCODPER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODPER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODPER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oCGCODPER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi di elaborazione",'gsbi_mcg.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oCGCODPER_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_CGCODPER
     i_obj.ecpSave()
  endproc

  add object oDESPER_1_12 as StdField with uid="WHKDOGSJIF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del periodo",;
    HelpContextID = 20673334,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=255, Top=102, InputMask=replicate('X',35)

  add object oCGCODREG_1_13 as StdField with uid="DXTJHAUQZX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CGCODREG", cQueryName = "CGCODREG",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice regola di elaborazione",;
    HelpContextID = 248941203,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=135, Top=131, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="REG_EXTM", cZoomOnZoom="GSBI_MRE", oKey_1_1="RE__TIPO", oKey_1_2="this.w_TIPO", oKey_2_1="RECODICE", oKey_2_2="this.w_CGCODREG"

  func oCGCODREG_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODREG_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODREG_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.REG_EXTM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RE__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RE__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'REG_EXTM','*','RE__TIPO,RECODICE',cp_AbsName(this.parent,'oCGCODREG_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MRE',"Regole di elaborazione",'gsbi_mcg.REG_EXTM_VZM',this.parent.oContained
  endproc
  proc oCGCODREG_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.RE__TIPO=w_TIPO
     i_obj.w_RECODICE=this.parent.oContained.w_CGCODREG
     i_obj.ecpSave()
  endproc

  add object oDESREG_1_14 as StdField with uid="SGYINKZMKY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESREG", cQueryName = "DESREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del codice",;
    HelpContextID = 104690486,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=255, Top=131, InputMask=replicate('X',35)


  add object oObj_1_25 as cp_outputCombo with uid="ZLERXIVPWT",left=135, top=160, width=381,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87163110


  add object oBtn_1_26 as StdButton with uid="WZFWVQXIDZ",left=437, top=188, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50955046;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="EFZTAPUEYA",left=493, top=188, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83517114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMVAL_1_28 as StdField with uid="KHJNGQJAOT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 184621094,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=317, Top=73, InputMask=replicate('X',5)

  add object oStr_1_15 as StdString with uid="IAJLHLKMTB",Visible=.t., Left=6, Top=16,;
    Alignment=1, Width=127, Height=18,;
    Caption="Da registrazione n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TYPZPHFKAY",Visible=.t., Left=6, Top=164,;
    Alignment=1, Width=127, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="UGOLCTHMPP",Visible=.t., Left=348, Top=16,;
    Alignment=1, Width=92, Height=18,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XCKFZEFJUI",Visible=.t., Left=193, Top=15,;
    Alignment=1, Width=74, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="OSUAZDZYTV",Visible=.t., Left=193, Top=44,;
    Alignment=1, Width=74, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="GXQTVDFUTD",Visible=.t., Left=6, Top=103,;
    Alignment=1, Width=127, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="OZRPYLCOZS",Visible=.t., Left=6, Top=132,;
    Alignment=1, Width=127, Height=18,;
    Caption="Regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ILNRNCILKZ",Visible=.t., Left=193, Top=74,;
    Alignment=1, Width=74, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ALKMEVJURB",Visible=.t., Left=6, Top=75,;
    Alignment=1, Width=127, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="LZHULHCBGJ",Visible=.t., Left=6, Top=48,;
    Alignment=1, Width=127, Height=18,;
    Caption="A registrazione n.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_scg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
