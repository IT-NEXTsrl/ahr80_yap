* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sic                                                        *
*              Stampa confronto indici                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_109]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sic",oParentObject))

* --- Class definition
define class tgsbi_sic as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 749
  Height = 469
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=258606697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  STR_INDI_IDX = 0
  STR_ANAC_IDX = 0
  PER_ELAB_IDX = 0
  ESERCIZI_IDX = 0
  STR_BIAN_IDX = 0
  cPrg = "gsbi_sic"
  cComment = "Stampa confronto indici"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPBILAN = space(1)
  w_STATO = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_STRUINDI = space(15)
  w_STRUDESC = space(40)
  w_TIPOBIL = space(1)
  w_MOVIMENT = space(1)
  w_TIPOPER = space(1)
  w_STATO = space(1)
  w_STRUG = space(15)
  w_STRUA = space(15)
  w_STRUTTUR = space(15)
  w_PERIODO = space(15)
  w_ESER = space(4)
  o_ESER = space(4)
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_STADESAG = space(1)
  w_STAFORM = space(1)
  w_TIPIND = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_ZoomGepr = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsbi_sic
  pTIPOCONT=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSBI_SIC'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sicPag1","gsbi_sic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTRUINDI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGepr = this.oPgFrm.Pages(1).oPag.ZoomGepr
    DoDefault()
    proc Destroy()
      this.w_ZoomGepr = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='STR_INDI'
    this.cWorkTables[3]='STR_ANAC'
    this.cWorkTables[4]='PER_ELAB'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='STR_BIAN'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsbi_sic
    this.pTIPOCONT=this.oParentObject
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TIPBILAN=space(1)
      .w_STATO=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_STRUINDI=space(15)
      .w_STRUDESC=space(40)
      .w_TIPOBIL=space(1)
      .w_MOVIMENT=space(1)
      .w_TIPOPER=space(1)
      .w_STATO=space(1)
      .w_STRUG=space(15)
      .w_STRUA=space(15)
      .w_STRUTTUR=space(15)
      .w_PERIODO=space(15)
      .w_ESER=space(4)
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_STADESAG=space(1)
      .w_STAFORM=space(1)
      .w_TIPIND=space(1)
      .w_DATOBSO=ctod("  /  /  ")
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_TIPBILAN = .pTIPOCONT
        .w_STATO = 'S'
        .w_DTOBSO = i_datsys
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_STRUINDI))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_TIPOBIL = .pTIPOCONT
        .w_MOVIMENT = 'T'
        .w_TIPOPER = 'L'
        .w_STATO = 'S'
        .w_STRUG = space(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_STRUG))
          .link_1_11('Full')
        endif
        .w_STRUA = space(15)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_STRUA))
          .link_1_12('Full')
        endif
        .w_STRUTTUR = IIF(.w_TIPBILAN='G', .w_STRUG, .w_STRUA)
        .w_PERIODO = space(15)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_PERIODO))
          .link_1_14('Full')
        endif
        .w_ESER = g_codese
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_ESER))
          .link_1_15('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .w_SELEZI = 'D'
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_STADESAG = ' '
        .w_STAFORM = ' '
    endwith
    this.DoRTCalc(20,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,12,.t.)
            .w_STRUTTUR = IIF(.w_TIPBILAN='G', .w_STRUG, .w_STRUA)
        if .o_ESER<>.w_ESER
            .w_PERIODO = space(15)
          .link_1_14('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTRUG_1_11.enabled = this.oPgFrm.Page1.oPag.oSTRUG_1_11.mCond()
    this.oPgFrm.Page1.oPag.oSTRUA_1_12.enabled = this.oPgFrm.Page1.oPag.oSTRUA_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTRUG_1_11.visible=!this.oPgFrm.Page1.oPag.oSTRUG_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSTRUA_1_12.visible=!this.oPgFrm.Page1.oPag.oSTRUA_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGepr.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUINDI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_INDI_IDX,3]
    i_lTable = "STR_INDI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2], .t., this.STR_INDI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_BO5',True,'STR_INDI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_STRUINDI)+"%");
                   +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);

          i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TITIPIND,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TITIPIND',this.w_TIPBILAN;
                     ,'TICODICE',trim(this.w_STRUINDI))
          select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TITIPIND,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUINDI)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUINDI) and !this.bDontReportError
            deferred_cp_zoom('STR_INDI','*','TITIPIND,TICODICE',cp_AbsName(oSource.parent,'oSTRUINDI_1_5'),i_cWhere,'GSBI_BO5',"Strutture indici",'GSBI_SCI.STR_INDI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPBILAN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo struttura indici errato od obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPIND',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_STRUINDI);
                   +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPIND',this.w_TIPBILAN;
                       ,'TICODICE',this.w_STRUINDI)
            select TITIPIND,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUINDI = NVL(_Link_.TICODICE,space(15))
      this.w_STRUDESC = NVL(_Link_.TIDESCRI,space(40))
      this.w_TIPIND = NVL(_Link_.TITIPIND,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_STRUINDI = space(15)
      endif
      this.w_STRUDESC = space(40)
      this.w_TIPIND = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPBILAN=.w_TIPIND AND (EMPTY(.w_DATOBSO) or .w_DATOBSO > i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo struttura indici errato od obsoleto")
        endif
        this.w_STRUINDI = space(15)
        this.w_STRUDESC = space(40)
        this.w_TIPIND = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2])+'\'+cp_ToStr(_Link_.TITIPIND,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.STR_INDI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUG)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUG))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUG)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUG) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUG_1_11'),i_cWhere,'',"Strutture di bilancio generale",'gsbi_agb.STR_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUG)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUG = NVL(_Link_.TRCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STRUG = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUA
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUA))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUA)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUA) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUA_1_12'),i_cWhere,'',"Strutture di bilancio analitica",'GSBI1AGB.STR_BIAN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUA)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUA = NVL(_Link_.TRCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STRUA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_14'),i_cWhere,'GSBI_APE',"Periodi di elaborazione",'PERIODO1.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_15'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTRUINDI_1_5.value==this.w_STRUINDI)
      this.oPgFrm.Page1.oPag.oSTRUINDI_1_5.value=this.w_STRUINDI
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUDESC_1_6.value==this.w_STRUDESC)
      this.oPgFrm.Page1.oPag.oSTRUDESC_1_6.value=this.w_STRUDESC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOBIL_1_7.RadioValue()==this.w_TIPOBIL)
      this.oPgFrm.Page1.oPag.oTIPOBIL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_8.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPER_1_9.RadioValue()==this.w_TIPOPER)
      this.oPgFrm.Page1.oPag.oTIPOPER_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_10.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUG_1_11.value==this.w_STRUG)
      this.oPgFrm.Page1.oPag.oSTRUG_1_11.value=this.w_STRUG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUA_1_12.value==this.w_STRUA)
      this.oPgFrm.Page1.oPag.oSTRUA_1_12.value=this.w_STRUA
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_14.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_14.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_15.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_15.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_18.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADESAG_1_21.RadioValue()==this.w_STADESAG)
      this.oPgFrm.Page1.oPag.oSTADESAG_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAFORM_1_22.RadioValue()==this.w_STAFORM)
      this.oPgFrm.Page1.oPag.oSTAFORM_1_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPBILAN=.w_TIPIND AND (EMPTY(.w_DATOBSO) or .w_DATOBSO > i_datsys))  and not(empty(.w_STRUINDI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTRUINDI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo struttura indici errato od obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    return

enddefine

* --- Define pages as container
define class tgsbi_sicPag1 as StdContainer
  Width  = 745
  height = 469
  stdWidth  = 745
  stdheight = 469
  resizeXpos=579
  resizeYpos=269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTRUINDI_1_5 as StdField with uid="CCPWRPQMNR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STRUINDI", cQueryName = "STRUINDI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo struttura indici errato od obsoleto",;
    ToolTipText = "Struttura indici da stampare",;
    HelpContextID = 58749807,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=120, Top=6, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_INDI", cZoomOnZoom="GSBI_BO5", oKey_1_1="TITIPIND", oKey_1_2="this.w_TIPBILAN", oKey_2_1="TICODICE", oKey_2_2="this.w_STRUINDI"

  func oSTRUINDI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUINDI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUINDI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.STR_INDI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPIND="+cp_ToStrODBC(this.Parent.oContained.w_TIPBILAN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPIND="+cp_ToStr(this.Parent.oContained.w_TIPBILAN)
    endif
    do cp_zoom with 'STR_INDI','*','TITIPIND,TICODICE',cp_AbsName(this.parent,'oSTRUINDI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_BO5',"Strutture indici",'GSBI_SCI.STR_INDI_VZM',this.parent.oContained
  endproc
  proc oSTRUINDI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSBI_BO5()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TITIPIND=w_TIPBILAN
     i_obj.w_TICODICE=this.parent.oContained.w_STRUINDI
     i_obj.ecpSave()
  endproc

  add object oSTRUDESC_1_6 as StdField with uid="BEBRKNJNZL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STRUDESC", cQueryName = "STRUDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura indici",;
    HelpContextID = 170947433,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=240, Top=6, InputMask=replicate('X',40)


  add object oTIPOBIL_1_7 as StdCombo with uid="WNGUCEIVPQ",rtseq=7,rtrep=.f.,left=97,top=62,width=115,height=21, enabled=.f.;
    , ToolTipText = "Tipo bilancio (cont. generale, cont. analitica)";
    , HelpContextID = 32880586;
    , cFormVar="w_TIPOBIL",RowSource=""+"Cont. generale,"+"Cont. analitica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOBIL_1_7.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'A',;
    ' ')))
  endfunc
  func oTIPOBIL_1_7.GetRadio()
    this.Parent.oContained.w_TIPOBIL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOBIL_1_7.SetRadio()
    this.Parent.oContained.w_TIPOBIL=trim(this.Parent.oContained.w_TIPOBIL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOBIL=='G',1,;
      iif(this.Parent.oContained.w_TIPOBIL=='A',2,;
      0))
  endfunc


  add object oMOVIMENT_1_8 as StdCombo with uid="BVSTUAHNNZ",rtseq=8,rtrep=.f.,left=267,top=62,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 88822246;
    , cFormVar="w_MOVIMENT",RowSource=""+"Tutti,"+"Previsionali,"+"Effettivi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_8.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    ' '))))
  endfunc
  func oMOVIMENT_1_8.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_8.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='T',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      iif(this.Parent.oContained.w_MOVIMENT=='E',3,;
      0)))
  endfunc


  add object oTIPOPER_1_9 as StdCombo with uid="KNEMRZYLQF",rtseq=9,rtrep=.f.,left=465,top=62,width=113,height=21;
    , ToolTipText = "Tipo periodo (tutti, gen., ann., sem., quad., trim., mens.)";
    , HelpContextID = 183126070;
    , cFormVar="w_TIPOPER",RowSource=""+"Tutti,"+"Solo generico,"+"Annuale,"+"Semestrale,"+"Quadrimestrale,"+"Trimestrale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPER_1_9.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'G',;
    iif(this.value =3,'A',;
    iif(this.value =4,'S',;
    iif(this.value =5,'Q',;
    iif(this.value =6,'T',;
    iif(this.value =7,'M',;
    ' '))))))))
  endfunc
  func oTIPOPER_1_9.GetRadio()
    this.Parent.oContained.w_TIPOPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPER_1_9.SetRadio()
    this.Parent.oContained.w_TIPOPER=trim(this.Parent.oContained.w_TIPOPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPER=='L',1,;
      iif(this.Parent.oContained.w_TIPOPER=='G',2,;
      iif(this.Parent.oContained.w_TIPOPER=='A',3,;
      iif(this.Parent.oContained.w_TIPOPER=='S',4,;
      iif(this.Parent.oContained.w_TIPOPER=='Q',5,;
      iif(this.Parent.oContained.w_TIPOPER=='T',6,;
      iif(this.Parent.oContained.w_TIPOPER=='M',7,;
      0)))))))
  endfunc


  add object oSTATO_1_10 as StdCombo with uid="GYUOYUQGKB",rtseq=10,rtrep=.f.,left=621,top=62,width=114,height=21;
    , ToolTipText = "Stato bilancio selezionato";
    , HelpContextID = 169975002;
    , cFormVar="w_STATO",RowSource=""+"Confermato,"+"Da confermare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oSTATO_1_10.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_10.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='T',3,;
      0)))
  endfunc

  add object oSTRUG_1_11 as StdField with uid="KXAYCWKBUD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_STRUG", cQueryName = "STRUG",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio generale",;
    HelpContextID = 178228442,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=97, Top=88, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUG"

  func oSTRUG_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPBILAN='G')
    endwith
   endif
  endfunc

  func oSTRUG_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPBILAN<>'G')
    endwith
  endfunc

  func oSTRUG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oSTRUG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio generale",'gsbi_agb.STR_ANAC_VZM',this.parent.oContained
  endproc

  add object oSTRUA_1_12 as StdField with uid="LZZPWPSIIL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_STRUA", cQueryName = "STRUA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio analitica",;
    HelpContextID = 184519898,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=97, Top=88, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUA"

  func oSTRUA_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPBILAN<>'G')
    endwith
   endif
  endfunc

  func oSTRUA_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPBILAN='G')
    endwith
  endfunc

  func oSTRUA_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUA_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUA_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUA_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio analitica",'GSBI1AGB.STR_BIAN_VZM',this.parent.oContained
  endproc

  add object oPERIODO_1_14 as StdField with uid="IVFRAVRZAU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 103521290,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=267, Top=88, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi di elaborazione",'PERIODO1.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc

  add object oESER_1_15 as StdField with uid="UPXVWCICUO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione",;
    HelpContextID = 252927674,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=465, Top=88, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oBtn_1_16 as StdButton with uid="VFPEDPHBUB",left=687, top=88, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Carica bilanci da scegliere";
    , HelpContextID = 186750998;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSBI_BIC(this.Parent.oContained,"CARICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TIPOBIL))
      endwith
    endif
  endfunc


  add object ZoomGepr as cp_szoombox with uid="UYCRAYMAIF",left=-5, top=137, width=754,height=277,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="GESTBILA",cZoomFile="GSBI_BIC",bOptions=.F.,bAdvOptions=.F.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 187826406

  add object oSELEZI_1_18 as StdRadio with uid="KBQGKGSVCH",rtseq=16,rtrep=.f.,left=5, top=421, width=134,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_18.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 8387546
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 8387546
      this.Buttons(2).Top=15
      this.SetAll("Width",132)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_18.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_18.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_20 as cp_runprogram with uid="ELHJQUBLLR",left=755, top=118, width=129,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BIC("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 187826406

  add object oSTADESAG_1_21 as StdCheck with uid="RZIAEWXWLZ",rtseq=18,rtrep=.f.,left=140, top=421, caption="Stampa descrizione agg.va",;
    HelpContextID = 137257837,;
    cFormVar="w_STADESAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADESAG_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADESAG_1_21.GetRadio()
    this.Parent.oContained.w_STADESAG = this.RadioValue()
    return .t.
  endfunc

  func oSTADESAG_1_21.SetRadio()
    this.Parent.oContained.w_STADESAG=trim(this.Parent.oContained.w_STADESAG)
    this.value = ;
      iif(this.Parent.oContained.w_STADESAG=='S',1,;
      0)
  endfunc

  add object oSTAFORM_1_22 as StdCheck with uid="BWXJWIBEGZ",rtseq=19,rtrep=.f.,left=358, top=421, caption="Stampa formula",;
    HelpContextID = 137338074,;
    cFormVar="w_STAFORM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAFORM_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTAFORM_1_22.GetRadio()
    this.Parent.oContained.w_STAFORM = this.RadioValue()
    return .t.
  endfunc

  func oSTAFORM_1_22.SetRadio()
    this.Parent.oContained.w_STAFORM=trim(this.Parent.oContained.w_STAFORM)
    this.value = ;
      iif(this.Parent.oContained.w_STAFORM=='S',1,;
      0)
  endfunc


  add object oBtn_1_34 as StdButton with uid="HMLRBFRNYB",left=687, top=419, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 251289274;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="CBFNKRVYBR",left=631, top=419, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa confronto indici dei bilanci selezionati";
    , HelpContextID = 111981177;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSBI_BIC(this.Parent.oContained,"ELAB")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_STRUINDI))
      endwith
    endif
  endfunc

  add object oStr_1_23 as StdString with uid="GFLIPTMVFG",Visible=.t., Left=213, Top=62,;
    Alignment=1, Width=52, Height=15,;
    Caption="Movim.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RYGQECFCYH",Visible=.t., Left=34, Top=88,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NTCLCXSVZW",Visible=.t., Left=212, Top=88,;
    Alignment=1, Width=53, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VFMIJMNHUF",Visible=.t., Left=9, Top=40,;
    Alignment=0, Width=37, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="UOGTUZVEDX",Visible=.t., Left=384, Top=88,;
    Alignment=1, Width=79, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="JLOKNTEVMB",Visible=.t., Left=12, Top=6,;
    Alignment=1, Width=106, Height=15,;
    Caption="Struttura indici:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="OHWBAIPIEV",Visible=.t., Left=381, Top=62,;
    Alignment=1, Width=81, Height=15,;
    Caption="Tipo periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YJRVXKRLKZ",Visible=.t., Left=10, Top=62,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo bil.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="WCATTSWWOI",Visible=.t., Left=574, Top=62,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oBox_1_27 as StdBox with uid="IFBFXQOZGY",left=0, top=34, width=743,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
