* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bbi                                                        *
*              Batch controlli bilancio                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_4]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bbi",oParentObject)
return(i_retval)

define class tgsbi_bbi as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciata dalla maschera di Stampa Bilancio Contabile (GSBI_SBI)
    * --- Questa procedura effettua lo sbiancamento dei dati del bilancio selezionato
    * --- al variare di uno dei filtri impostati sulla maschera.
    * --- Azzero i seguenti campi al verificarsi dell'evento della maschera.
    this.oParentObject.w_NUMERO = 0
    this.oParentObject.w_ESERCI = SPACE(4)
    this.oParentObject.w_DATABIL = cp_CharToDate("  -  -  ")
    this.oParentObject.w_DESCRIBI = SPACE(35)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
