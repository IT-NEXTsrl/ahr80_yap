* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sci                                                        *
*              Stampa indici                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_86]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2008-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sci",oParentObject))

* --- Class definition
define class tgsbi_sci as StdForm
  Top    = 17
  Left   = 68

  * --- Standard Properties
  Width  = 652
  Height = 344
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-09"
  HelpContextID=90834537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  STR_INDI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  STR_ANAC_IDX = 0
  PER_ELAB_IDX = 0
  GESTBILA_IDX = 0
  STR_BIAN_IDX = 0
  cPrg = "gsbi_sci"
  cComment = "Stampa indici"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPBILAN = space(10)
  w_COMMESSA = space(15)
  w_CODAZI = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_STRUINDI = space(15)
  w_DESCINDI = space(40)
  w_STADESAG = space(1)
  w_STAFORM = space(1)
  w_STAEXCEL = space(1)
  w_ESER = space(4)
  o_ESER = space(4)
  w_INESER = ctod('  /  /  ')
  w_FINESER = ctod('  /  /  ')
  w_STATO = space(1)
  w_STRUTTUR_B = space(15)
  o_STRUTTUR_B = space(15)
  w_MOVIMENT = space(1)
  w_PERIODO = space(15)
  w_STRUTTUR_A = space(15)
  o_STRUTTUR_A = space(15)
  w_STRUTTUR = space(15)
  w_DESCRI = space(40)
  w_DESCPER = space(35)
  w_NUMERO = 0
  w_PERBILC = space(15)
  w_ESERCI = space(4)
  w_DATABIL = ctod('  /  /  ')
  w_DESCRIBI = space(35)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_TIPIND = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsbi_sci
  pTIPOCONT=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSBI_SCI'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sciPag1","gsbi_sci",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTRUINDI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='STR_INDI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='STR_ANAC'
    this.cWorkTables[5]='PER_ELAB'
    this.cWorkTables[6]='GESTBILA'
    this.cWorkTables[7]='STR_BIAN'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsbi_sci
    this.pTIPOCONT=this.oParentObject
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPBILAN=space(10)
      .w_COMMESSA=space(15)
      .w_CODAZI=space(5)
      .w_DTOBSO=ctod("  /  /  ")
      .w_STRUINDI=space(15)
      .w_DESCINDI=space(40)
      .w_STADESAG=space(1)
      .w_STAFORM=space(1)
      .w_STAEXCEL=space(1)
      .w_ESER=space(4)
      .w_INESER=ctod("  /  /  ")
      .w_FINESER=ctod("  /  /  ")
      .w_STATO=space(1)
      .w_STRUTTUR_B=space(15)
      .w_MOVIMENT=space(1)
      .w_PERIODO=space(15)
      .w_STRUTTUR_A=space(15)
      .w_STRUTTUR=space(15)
      .w_DESCRI=space(40)
      .w_DESCPER=space(35)
      .w_NUMERO=0
      .w_PERBILC=space(15)
      .w_ESERCI=space(4)
      .w_DATABIL=ctod("  /  /  ")
      .w_DESCRIBI=space(35)
      .w_OQRY=space(50)
      .w_OREP=space(50)
      .w_TIPIND=space(1)
        .w_TIPBILAN = .pTIPOCONT
        .w_COMMESSA = space(15)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_3('Full')
        endif
        .w_DTOBSO = i_datsys
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_STRUINDI))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_STADESAG = 'S'
        .w_STAFORM = 'S'
        .w_STAEXCEL = ' '
        .w_ESER = g_codese
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ESER))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,12,.f.)
        .w_STATO = 'S'
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_STRUTTUR_B))
          .link_1_14('Full')
        endif
        .w_MOVIMENT = 'E'
        .w_PERIODO = SPACE(15)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_PERIODO))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_STRUTTUR_A))
          .link_1_20('Full')
        endif
        .w_STRUTTUR = IIF(.w_TIPBILAN='A',.w_STRUTTUR_A,.w_STRUTTUR_B)
        .DoRTCalc(19,21,.f.)
        if not(empty(.w_NUMERO))
          .link_1_28('Full')
        endif
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_ESERCI))
          .link_1_33('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
    this.DoRTCalc(24,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,15,.t.)
        if .o_ESER<>.w_ESER
            .w_PERIODO = SPACE(15)
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_STRUTTUR_A<>.w_STRUTTUR_A.or. .o_STRUTTUR_B<>.w_STRUTTUR_B
            .w_STRUTTUR = IIF(.w_TIPBILAN='A',.w_STRUTTUR_A,.w_STRUTTUR_B)
        endif
        .DoRTCalc(19,20,.t.)
          .link_1_28('Full')
        .DoRTCalc(22,22,.t.)
          .link_1_33('Full')
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTRUTTUR_B_1_14.visible=!this.oPgFrm.Page1.oPag.oSTRUTTUR_B_1_14.mHide()
    this.oPgFrm.Page1.oPag.oSTRUTTUR_A_1_20.visible=!this.oPgFrm.Page1.oPag.oSTRUTTUR_A_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUINDI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_INDI_IDX,3]
    i_lTable = "STR_INDI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2], .t., this.STR_INDI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsbi_bo5',True,'STR_INDI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_STRUINDI)+"%");
                   +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);

          i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TITIPIND,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TITIPIND',this.w_TIPBILAN;
                     ,'TICODICE',trim(this.w_STRUINDI))
          select TITIPIND,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TITIPIND,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUINDI)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUINDI) and !this.bDontReportError
            deferred_cp_zoom('STR_INDI','*','TITIPIND,TICODICE',cp_AbsName(oSource.parent,'oSTRUINDI_1_5'),i_cWhere,'gsbi_bo5',"Strutture indici",'GSBI_SCI.STR_INDI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPBILAN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TITIPIND,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Tipo struttura indici errato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPIND',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TITIPIND,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPIND,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_STRUINDI);
                   +" and TITIPIND="+cp_ToStrODBC(this.w_TIPBILAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPIND',this.w_TIPBILAN;
                       ,'TICODICE',this.w_STRUINDI)
            select TITIPIND,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUINDI = NVL(_Link_.TICODICE,space(15))
      this.w_DESCINDI = NVL(_Link_.TIDESCRI,space(40))
      this.w_TIPIND = NVL(_Link_.TITIPIND,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_STRUINDI = space(15)
      endif
      this.w_DESCINDI = space(40)
      this.w_TIPIND = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPBILAN=.w_TIPIND
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo struttura indici errato")
        endif
        this.w_STRUINDI = space(15)
        this.w_DESCINDI = space(40)
        this.w_TIPIND = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_INDI_IDX,2])+'\'+cp_ToStr(_Link_.TITIPIND,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.STR_INDI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_10'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_INESER = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESER = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_INESER = ctod("  /  /  ")
      this.w_FINESER = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR_B
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR_B) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSC',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR_B)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR_B))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR_B)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR_B) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_B_1_14'),i_cWhere,'GSBI_MSC',"Strutture di bilancio cont.",'GSBI_AGB.STR_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR_B)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR_B);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR_B)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR_B = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR_B = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR_B Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE,PEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_16'),i_cWhere,'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE,PEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
      this.w_DESCPER = NVL(_Link_.PEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
      this.w_DESCPER = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR_A
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSA',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR_A)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR_A))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR_A)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR_A) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_A_1_20'),i_cWhere,'GSBI_MSA',"Strutture di bilancio analitico",'GSBI_AGA.STR_BIAN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR_A)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR_A = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR_A = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMERO
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GESTBILA_IDX,3]
    i_lTable = "GESTBILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GESTBILA_IDX,2], .t., this.GESTBILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GESTBILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GBNUMBIL,GBPERIOD";
                   +" from "+i_cTable+" "+i_lTable+" where GBNUMBIL="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GBNUMBIL',this.w_NUMERO)
            select GBNUMBIL,GBPERIOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.GBNUMBIL,0)
      this.w_PERBILC = NVL(_Link_.GBPERIOD,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = 0
      endif
      this.w_PERBILC = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GESTBILA_IDX,2])+'\'+cp_ToStr(_Link_.GBNUMBIL,1)
      cp_ShowWarn(i_cKey,this.GESTBILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTRUINDI_1_5.value==this.w_STRUINDI)
      this.oPgFrm.Page1.oPag.oSTRUINDI_1_5.value=this.w_STRUINDI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCINDI_1_6.value==this.w_DESCINDI)
      this.oPgFrm.Page1.oPag.oDESCINDI_1_6.value=this.w_DESCINDI
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADESAG_1_7.RadioValue()==this.w_STADESAG)
      this.oPgFrm.Page1.oPag.oSTADESAG_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAFORM_1_8.RadioValue()==this.w_STAFORM)
      this.oPgFrm.Page1.oPag.oSTAFORM_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAEXCEL_1_9.RadioValue()==this.w_STAEXCEL)
      this.oPgFrm.Page1.oPag.oSTAEXCEL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_10.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_10.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oINESER_1_11.value==this.w_INESER)
      this.oPgFrm.Page1.oPag.oINESER_1_11.value=this.w_INESER
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESER_1_12.value==this.w_FINESER)
      this.oPgFrm.Page1.oPag.oFINESER_1_12.value=this.w_FINESER
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_13.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_B_1_14.value==this.w_STRUTTUR_B)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_B_1_14.value=this.w_STRUTTUR_B
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_15.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_16.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_16.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_A_1_20.value==this.w_STRUTTUR_A)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_A_1_20.value=this.w_STRUTTUR_A
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_22.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_22.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPER_1_23.value==this.w_DESCPER)
      this.oPgFrm.Page1.oPag.oDESCPER_1_23.value=this.w_DESCPER
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_28.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_28.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_33.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_33.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATABIL_1_34.value==this.w_DATABIL)
      this.oPgFrm.Page1.oPag.oDATABIL_1_34.value=this.w_DATABIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIBI_1_36.value==this.w_DESCRIBI)
      this.oPgFrm.Page1.oPag.oDESCRIBI_1_36.value=this.w_DESCRIBI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPBILAN=.w_TIPIND)  and not(empty(.w_STRUINDI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTRUINDI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo struttura indici errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    this.o_STRUTTUR_B = this.w_STRUTTUR_B
    this.o_STRUTTUR_A = this.w_STRUTTUR_A
    return

enddefine

* --- Define pages as container
define class tgsbi_sciPag1 as StdContainer
  Width  = 648
  height = 344
  stdWidth  = 648
  stdheight = 344
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTRUINDI_1_5 as StdField with uid="FDGRYQOKDO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STRUINDI", cQueryName = "STRUINDI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo struttura indici errato",;
    ToolTipText = "Struttura indici",;
    HelpContextID = 41913489,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=115, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_INDI", cZoomOnZoom="gsbi_bo5", oKey_1_1="TITIPIND", oKey_1_2="this.w_TIPBILAN", oKey_2_1="TICODICE", oKey_2_2="this.w_STRUINDI"

  func oSTRUINDI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUINDI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUINDI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.STR_INDI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPIND="+cp_ToStrODBC(this.Parent.oContained.w_TIPBILAN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPIND="+cp_ToStr(this.Parent.oContained.w_TIPBILAN)
    endif
    do cp_zoom with 'STR_INDI','*','TITIPIND,TICODICE',cp_AbsName(this.parent,'oSTRUINDI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsbi_bo5',"Strutture indici",'GSBI_SCI.STR_INDI_VZM',this.parent.oContained
  endproc
  proc oSTRUINDI_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsbi_bo5()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TITIPIND=w_TIPBILAN
     i_obj.w_TICODICE=this.parent.oContained.w_STRUINDI
     i_obj.ecpSave()
  endproc

  add object oDESCINDI_1_6 as StdField with uid="SYBVXTWJBH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCINDI", cQueryName = "DESCINDI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura indici",;
    HelpContextID = 43093121,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=236, Top=9, InputMask=replicate('X',40)

  add object oSTADESAG_1_7 as StdCheck with uid="CLWNLNGLAQ",rtseq=7,rtrep=.f.,left=115, top=34, caption="Stampa descrizione agg.va",;
    HelpContextID = 36594541,;
    cFormVar="w_STADESAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADESAG_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADESAG_1_7.GetRadio()
    this.Parent.oContained.w_STADESAG = this.RadioValue()
    return .t.
  endfunc

  func oSTADESAG_1_7.SetRadio()
    this.Parent.oContained.w_STADESAG=trim(this.Parent.oContained.w_STADESAG)
    this.value = ;
      iif(this.Parent.oContained.w_STADESAG=='S',1,;
      0)
  endfunc

  add object oSTAFORM_1_8 as StdCheck with uid="FGPSQLNWQS",rtseq=8,rtrep=.f.,left=325, top=34, caption="Stampa formula",;
    HelpContextID = 30434086,;
    cFormVar="w_STAFORM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAFORM_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTAFORM_1_8.GetRadio()
    this.Parent.oContained.w_STAFORM = this.RadioValue()
    return .t.
  endfunc

  func oSTAFORM_1_8.SetRadio()
    this.Parent.oContained.w_STAFORM=trim(this.Parent.oContained.w_STAFORM)
    this.value = ;
      iif(this.Parent.oContained.w_STAFORM=='S',1,;
      0)
  endfunc

  add object oSTAEXCEL_1_9 as StdCheck with uid="EBHWUNFYFE",rtseq=9,rtrep=.f.,left=456, top=34, caption="Stampa su Excel",;
    HelpContextID = 211852430,;
    cFormVar="w_STAEXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAEXCEL_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTAEXCEL_1_9.GetRadio()
    this.Parent.oContained.w_STAEXCEL = this.RadioValue()
    return .t.
  endfunc

  func oSTAEXCEL_1_9.SetRadio()
    this.Parent.oContained.w_STAEXCEL=trim(this.Parent.oContained.w_STAEXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_STAEXCEL=='S',1,;
      0)
  endfunc

  add object oESER_1_10 as StdField with uid="PHAVRDKVMF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 85155514,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=103, Top=103, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oINESER_1_11 as StdField with uid="KLERCYMGZY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_INESER", cQueryName = "INESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 20814982,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=353, Top=103

  add object oFINESER_1_12 as StdField with uid="SUZSTINWRJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FINESER", cQueryName = "FINESER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 84944726,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=549, Top=103


  add object oSTATO_1_13 as StdCombo with uid="XBQTNGGROK",rtseq=13,rtrep=.f.,left=103,top=139,width=114,height=21;
    , ToolTipText = "Stato bilancio selezionato";
    , HelpContextID = 2202842;
    , cFormVar="w_STATO",RowSource=""+"Confermato,"+"Da confermare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSTATO_1_13.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_13.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      0))
  endfunc

  add object oSTRUTTUR_B_1_14 as StdField with uid="PCAHLHFDJO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_STRUTTUR_B", cQueryName = "STRUTTUR_B",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 198132888,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=282, Top=139, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", cZoomOnZoom="GSBI_MSC", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR_B"

  func oSTRUTTUR_B_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPBILAN='A')
    endwith
  endfunc

  func oSTRUTTUR_B_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_B_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_B_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_B_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSC',"Strutture di bilancio cont.",'GSBI_AGB.STR_ANAC_VZM',this.parent.oContained
  endproc
  proc oSTRUTTUR_B_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR_B
     i_obj.ecpSave()
  endproc


  add object oMOVIMENT_1_15 as StdCombo with uid="DJNEVFIIED",rtseq=15,rtrep=.f.,left=103,top=173,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 78949914;
    , cFormVar="w_MOVIMENT",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_15.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oMOVIMENT_1_15.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_15.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='E',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      0))
  endfunc

  add object oPERIODO_1_16 as StdField with uid="FUHHERHQFZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 64250870,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=282, Top=173, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc

  add object oSTRUTTUR_A_1_20 as StdField with uid="DYRPQGOVLC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_STRUTTUR_A", cQueryName = "STRUTTUR_A",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 198133144,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=283, Top=139, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", cZoomOnZoom="GSBI_MSA", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR_A"

  func oSTRUTTUR_A_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPBILAN='G')
    endwith
  endfunc

  func oSTRUTTUR_A_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_A_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_A_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_A_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSA',"Strutture di bilancio analitico",'GSBI_AGA.STR_BIAN_VZM',this.parent.oContained
  endproc
  proc oSTRUTTUR_A_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR_A
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_22 as StdField with uid="TCYAXZPKPD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 150893366,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=400, Top=139, InputMask=replicate('X',40)

  add object oDESCPER_1_23 as StdField with uid="DRPMLXSRNX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCPER", cQueryName = "DESCPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 81687350,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=400, Top=173, InputMask=replicate('X',35)


  add object oBtn_1_26 as StdButton with uid="ZQVZPYVGON",left=164, top=219, width=19,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 90633514;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      do GSBI_KBI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_28 as StdField with uid="KTHULPJCEC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 251667414,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=84, Top=250, cLinkFile="GESTBILA", cZoomOnZoom="GSBI_AGB", oKey_1_1="GBNUMBIL", oKey_1_2="this.w_NUMERO"

  func oNUMERO_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oESERCI_1_33 as StdField with uid="YPGXLHRADQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 136094022,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=84, Top=284, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDATABIL_1_34 as StdField with uid="NHVGFDIJDN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATABIL", cQueryName = "DATABIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 133988150,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=258, Top=284

  add object oDESCRIBI_1_36 as StdField with uid="RTYVYPMHKD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRIBI", cQueryName = "DESCRIBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 150893439,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=258, Top=250, InputMask=replicate('X',35)


  add object oObj_1_38 as cp_runprogram with uid="LHXNXPDZZK",left=670, top=164, width=150,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BBI",;
    cEvent = "w_STATO Changed,w_STRUTTUR Changed,w_MOVIMENT Changed,w_PERIODO Changed,w_ESER Changed",;
    nPag=1;
    , HelpContextID = 87163110


  add object oBtn_1_47 as StdButton with uid="CVTQQSGBNQ",left=536, top=292, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 90805786;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        do GSBI_BCI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO) AND NOT EMPTY(.w_STRUINDI))
      endwith
    endif
  endfunc


  add object oBtn_1_48 as StdButton with uid="HMLRBFRNYB",left=591, top=292, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83517114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_17 as StdString with uid="NKBMPHKVWK",Visible=.t., Left=43, Top=139,;
    Alignment=1, Width=58, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XZBLPLLUKP",Visible=.t., Left=9, Top=173,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZNCSQDJYWI",Visible=.t., Left=218, Top=139,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="WVOUUNVJFU",Visible=.t., Left=219, Top=173,;
    Alignment=1, Width=60, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="INDZJUSCJW",Visible=.t., Left=20, Top=219,;
    Alignment=0, Width=138, Height=15,;
    Caption="Selezione bilancio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="FAFXZHELVT",Visible=.t., Left=24, Top=71,;
    Alignment=0, Width=601, Height=18,;
    Caption="Filtri bilancio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="WEKAOAKVPC",Visible=.t., Left=8, Top=252,;
    Alignment=1, Width=74, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="LRYBXHROZU",Visible=.t., Left=8, Top=286,;
    Alignment=1, Width=74, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="XYGIAPRYIB",Visible=.t., Left=141, Top=286,;
    Alignment=1, Width=114, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QKNRAXMSUW",Visible=.t., Left=143, Top=249,;
    Alignment=1, Width=112, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BAVNQNGAPK",Visible=.t., Left=34, Top=103,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="PBZMVRRMJJ",Visible=.t., Left=199, Top=103,;
    Alignment=1, Width=151, Height=15,;
    Caption="Inizio esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JYXTBAZAJE",Visible=.t., Left=452, Top=103,;
    Alignment=1, Width=94, Height=15,;
    Caption="Fine esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="IMJKDAQQGQ",Visible=.t., Left=8, Top=9,;
    Alignment=1, Width=106, Height=15,;
    Caption="Struttura indici:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_31 as StdBox with uid="YFYMDQKDDE",left=18, top=211, width=617,height=1

  add object oBox_1_45 as StdBox with uid="PCHLRFORXL",left=18, top=90, width=617,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sci','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
