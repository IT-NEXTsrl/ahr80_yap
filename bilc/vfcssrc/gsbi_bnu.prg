* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bnu                                                        *
*              Notifica riga variata                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_2]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bnu",oParentObject)
return(i_retval)

define class tgsbi_bnu as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo Batch non Assolutamente fa Nulla tranne il Notificare che la Riga e' Stata Variata (da GSBI_KDS)
    SELECT (this.oParentObject.oParentObject.cTrsName)
    if EMPTY(i_SRV) AND NOT DELETED()
      REPLACE i_SRV WITH "U"
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
