* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bci                                                        *
*              Stampa indici                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_72]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2000-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bci",oParentObject)
return(i_retval)

define class tgsbi_bci as StdBatch
  * --- Local variables
  w_IBFORMUL = space(100)
  w_VALORE = 0
  Msg = space(0)
  w_TIPINDI = space(1)
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_DIFFE = 0
  Func = space(0)
  CODVOC = space(15)
  INDICE = 0
  w_VALORES = space(19)
  * --- WorkFile variables
  ESERCIZI_idx=0
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa indici da bilancio
    * --- Prendo valori predefiniti in periodo e li memorizzo in un array
    DIMENSION COST(10)
    * --- Select from PER_ELAB
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PER_ELAB ";
          +" where PECODICE = "+cp_ToStrODBC(this.oParentObject.w_PERBILC)+"";
           ,"_Curs_PER_ELAB")
    else
      select * from (i_cTable);
       where PECODICE = this.oParentObject.w_PERBILC;
        into cursor _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      select _Curs_PER_ELAB
      locate for 1=1
      do while not(eof())
      COST(1)=NVL(_Curs_PER_ELAB.PEVALC01,0)
      COST(2)=NVL(_Curs_PER_ELAB.PEVALC02,0)
      COST(3)=NVL(_Curs_PER_ELAB.PEVALC03,0)
      COST(4)=NVL(_Curs_PER_ELAB.PEVALC04,0)
      COST(5)=NVL(_Curs_PER_ELAB.PEVALC05,0)
      COST(6)=NVL(_Curs_PER_ELAB.PEVALC06,0)
      COST(7)=NVL(_Curs_PER_ELAB.PEVALC07,0)
      COST(8)=NVL(_Curs_PER_ELAB.PEVALC08,0)
      COST(9)=NVL(_Curs_PER_ELAB.PEVALC09,0)
      COST(10)=NVL(_Curs_PER_ELAB.PEVALC10,0)
        select _Curs_PER_ELAB
        continue
      enddo
      use
    endif
    * --- Mi carico su un cursore il bilancio
    ah_Msg("Fase 1: reperimento dati bilancio",.T.)
    vq_exec("..\bilc\exe\query\gsbi_bci",this,"CursBilc")
    vq_exec("..\bilc\exe\query\gsbi3bci",this,"CursDriver")
    SELECT CursBilc
    GO TOP
    L_GBNUMBIL = CursBilc.GBNUMBIL
    L_GBBILESE = CursBilc.GBBILESE
    L_GBDESCRI = CursBilc.GBDESCRI
    * --- Mi carico su un cursore la struttura dell'indice per i calcoli
    ah_Msg("Fase 2: reperimento dati struttura indici",.T.)
    vq_exec("..\bilc\exe\query\gsbi1bci",this,"CursIndi")
    * --- Cursore per Stampa
    CREATE CURSOR CursStampa (CODICE C(15), CPROWORD1 N(6), IMPORT N(18,4), MSGERROR M(10))
    a = WRCURSOR("CursStampa")
    ah_Msg("Fase 3: calcolo valori indici",.T.)
    SELECT CursIndi
    GO TOP
    SCAN
    this.w_IBFORMUL = CursIndi.IBFORMUL
    this.w_TIPINDI = CursIndi.IBTIPIND
    this.Msg = ""
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    INSERT INTO CursStampa (CODICE,CPROWORD1,IMPORT,MSGERROR) ;
    VALUES (CursIndi.TICODIND,CursIndi.CPROWORD,this.w_VALORE,this.Msg)
    ENDSCAN
    L_STAFORM = this.oParentObject.w_STAFORM
    L_STADESA = this.oParentObject.w_STADESAG
    SELECT CursIndi.*, CursStampa.* FROM ;
    (CursIndi Inner Join CursStampa on CursIndi.TICODIND=CursStampa.CODICE ;
    AND CursIndi.CPROWORD=CursStampa.CPROWORD1) ;
    INTO CURSOR __TMP__ ORDER BY CursIndi.CPROWORD
    * --- Stampa
    CP_CHPRN("..\BILC\EXE\QUERY\GSBI1BCI.FRX", " ", this)
    * --- Chiusura Cursori
    if used("CursBilc")
      select CursBilc
      use
    endif
    if used("CursDriver")
      select CursDriver
      use
    endif
    if used("CursIndi")
      select CursIndi
      use
    endif
    if used("CursStampa")
      select CursStampa
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valore Indice
    Messaggio = "Corretta"
    this.TmpSave = ALLTRIM(this.w_IBFORMUL)
    * --- Ricerco il segno '<' il quale indica che ho inserito una costante
    this.w_OCCUR = RATC("<",this.TmpSave)
    this.w_OCCUR1 = RATC(">",this.TmpSave)
    this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    do while this.w_OCCUR <> 0
      * --- Ciclo sulla stringa per sostituire con i valori del bilancio
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      * --- Trovo il valore della Voce
      this.CODVOC = ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+1,this.w_DIFFE-1))
      if this.w_TIPINDI = "G"
        SELECT * FROM CursBilc INTO CURSOR PSG WHERE CursBilc.MRCODVOC=this.CODVOC
      else
        SELECT * FROM CursBilc INTO CURSOR PSG WHERE CursBilc.MRCODVOA=this.CODVOC
      endif
      if RECCOUNT("PSG")>0
        this.w_VALORE = PSG.MRIMPORT
      else
        if INLIST(this.CODVOC,"C01","C02","C03","C04","C05","C06","C07","C08","C09","C10")
          this.w_VALORE = COST(VAL(SUBSTR(this.CODVOC,2,2)))
        else
          if this.w_TIPINDI = "G"
            SELECT * FROM CursDriver INTO CURSOR PSG WHERE MRCODVOC=this.CODVOC
          else
            SELECT * FROM CursDriver INTO CURSOR PSG WHERE MRCODVOA=this.CODVOC
          endif
          if RECCOUNT("PSG")>0
            this.w_VALORE = PSG.MRIMPORT
          else
            this.w_VALORE = 0
            this.Msg = this.Msg + Ah_MsgFormat("Codice voce < %1 > non presente nel bilancio selezionato.",ALLTRIM(this.CODVOC))
          endif
        endif
      endif
      if used("PSG")
        SELECT PSG
        USE
      endif
      this.w_VALORES = STR(this.w_VALORE,18,4)
      * --- Passaggio per non perdere i valori dopo la virgola
      this.w_VALORES = LEFT(this.w_VALORES,13) + "." + RIGHT(this.w_VALORES,4)
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(this.w_VALORES)
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("<",this.TmpSave)
      this.w_OCCUR1 = RATC(">",this.TmpSave)
      this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    enddo
    this.FuncCalc =  ALLTRIM(this.TmpSave)
    ON ERROR Messaggio = "Errata"
    TmpVal = this.FuncCalc
    * --- Valutazione dell'errore
    this.w_VALORE = &TmpVal
    ON ERROR
    if Messaggio = "Errata"
      this.w_VALORE = 0
      this.Msg = this.Msg + Ah_MsgFormat("Errore nella funzione calcolata.")
    endif
    * --- Filtra eventuali divisioni per 0 (*********)
    this.w_VALORE = IIF(AT("*", ALLTR(STR(this.w_VALORE)))<>0, 0, this.w_VALORE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='PER_ELAB'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PER_ELAB')
      use in _Curs_PER_ELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
