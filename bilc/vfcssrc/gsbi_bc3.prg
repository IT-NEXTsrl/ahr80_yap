* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bc3                                                        *
*              Calcola confronto bilancio analitica                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_42]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2010-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bc3",oParentObject)
return(i_retval)

define class tgsbi_bc3 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciata dalla stampa Confronto tra Bilanci (GSBI_SBC)
    * --- Lancio la query che legge i dati dal primo bilancio.
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_SAB",this,"BILAN1")
    * --- Lancio la query che legge i dati dal secondo bilancio.
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI1SAB",this,"BILAN2")
    * --- Controllo se voglio stampare solo le righe valorizzate
    if this.oParentObject.w_RIGHESI="S"
      * --- Metto in join i due cursori che sono stati creati.Effettuo una right join sia con il campo di riga sia con il codice della voce.
      * --- Controllo che almeno uno dei due importi dei cursori sia maggiore di zero.
      SELECT BILAN2.ROWORD AS ROWORD,BILAN2.MRRAGCDC AS RAGCDC,BILAN2.DESCRI AS DESCRI,;
      BILAN2.CODVOC AS CODVOC,BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1, BILAN2.VRTIPDRI AS TIPDRI2;
      FROM BILAN1 RIGHT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC AND;
      BILAN1.CPROWORD=BILAN2.ROWORD WHERE BILAN1.MRIMPORT<>0 OR BILAN2.IMPORT<>0 ;
      OR BILAN1.VRTIPVOC="D" INTO CURSOR BILANCIO
    else
      * --- Metto in join i due cursori che sono stati creati.Effettuo una right join sia con il campo di riga sia con il codice della voce
      SELECT BILAN2.ROWORD AS ROWORD,BILAN2.MRRAGCDC AS RAGCDC,BILAN2.DESCRI AS DESCRI,;
      BILAN2.CODVOC AS CODVOC,BILAN2.IMPORT AS IMPORTO2,BILAN1.MRIMPORT AS IMPORTO1, BILAN2.VRTIPDRI AS TIPDRI2;
      FROM BILAN1 RIGHT OUTER JOIN BILAN2 ON BILAN1.MRCODVOC=BILAN2.CODVOC AND;
      BILAN1.CPROWORD=BILAN2.ROWORD INTO CURSOR BILANCIO
    endif
    * --- Passo il cursore BILANCIO al cursore __TMP__ destinato alla stampa
    SELECT * FROM BILANCIO INTO CURSOR __TMP__ ORDER BY 1
    * --- Controllo se le valute dei due bilanci sono identiche.
    if this.oParentObject.w_VALAPP=this.oParentObject.w_VALAPP2
      * --- Valute dei bilanci identiche.Controllo se sono identiche alla valuta di stampa.
      if this.oParentObject.w_VALAPP=this.oParentObject.w_DIVISA
        * --- Valute identiche non applico nessun cambio.
        L_MOLTI=1
        L_MOLTI2=1
        L_CAMVAL=1
        L_CAMVAL2=1
      else
        * --- Valute bilancio differenti alla valuta di stampa.Effettuo i dovuti cambi.
        L_MOLTI=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_MOLTI2=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL=GETCAM(this.oParentObject.w_VALAPP,i_DATSYS,0)
        L_CAMVAL2=GETCAM(this.oParentObject.w_VALAPP2,i_DATSYS,0)
      endif
    else
      * --- Valute dei bilanci differenti.
      * --- Controllo che la valuta del primo bilancio sia uguale a quella di stampa.
      if this.oParentObject.w_VALAPP=this.oParentObject.w_DIVISA
        * --- Non applico nessun cambio.
        L_MOLTI=1
        L_CAMVAL=1
      else
        * --- La valuta del primo bilancio � differente da quella di stampa.
        * --- Effettuo i dovuti cambi.
        L_MOLTI=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL=GETCAM(this.oParentObject.w_VALAPP,i_DATSYS,0)
      endif
      * --- Controllo che la valuta del primo bilancio sia uguale a quella di stampa.
      if this.oParentObject.w_VALAPP2=this.oParentObject.w_DIVISA
        * --- Non applico nessun cambio.
        L_MOLTI2=1
        L_CAMVAL2=1
      else
        * --- La valuta del secondo bilancio � differente da quella di stampa.
        * --- Effettuo i dovuti cambi.
        L_MOLTI2=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
        L_CAMVAL2=GETCAM(this.oParentObject.w_VALAPP2,i_DATSYS,0)
      endif
    endif
    * --- Formato importi
    L_DECIMI=this.oParentObject.w_DECIMI
    L_DIVISA=this.oParentObject.w_DIVISA
    L_SIMVAL=this.oParentObject.w_SIMVAL
    s=20*(l_decimi+2)
    L_NUMERO=this.oParentObject.w_NUMERO
    L_ESERCI=this.oParentObject.w_ESERCI
    L_DESCRIBI=this.oParentObject.w_DESCRIBI
    L_NUMERO2=this.oParentObject.w_NUMERO2
    L_ESERCI2=this.oParentObject.w_ESERCI2
    L_DESCRIB2=this.oParentObject.w_DESCRIB2
    L_PERIODO=this.oParentObject.w_PERIODO
    L_PERIODO2=this.oParentObject.w_PERIODO2
    L_CAMBIO=this.oParentObject.w_CAMVAL
    if this.oParentObject.w_TIPOSTAM="S"
      * --- Stampa Standard
      * --- Lancio il report assocciato
      CP_CHPRN("..\BILC\EXE\QUERY\GSBI_SAB.FRX", " ", this)
    else
      * --- Su EXCEL - Creo il cursore da quello di stampa
      select ROWORD AS ROWORD, RAGCDC as RAGCDC, DESCRI as DESCRI, CODVOC AS CODVOC, TIPDRI2 AS TIPDRI2,;
      IIF(TIPDRI2="M", (cp_Round((IMPORTO1/l_camval)*l_molti,this.oParentObject.w_decimi)), cp_round(IMPORTO1, this.oParentObject.w_decimi)) AS IMPORTO1,;
      IIF(TIPDRI2="M", (cp_Round((IMPORTO2/l_camval2)*l_molti2,this.oParentObject.w_decimi)), cp_round(IMPORTO2, this.oParentObject.w_decimi)) AS IMPORTO2,"ANALITICA" as TIPO ;
      FROM __TMP__ INTO CURSOR __TMP__ ORDER BY 1,2
      * --- Lancio la stampa su Excell
      CP_CHPRN("..\BILC\EXE\QUERY\GSBIESCB.XLT", " ", this)
    endif
    if this.oParentObject.w_divisa<>g_perval
      this.oParentObject.w_DIVISA = space(3)
      this.oParentObject.w_SIMVAL = space(5)
      this.oParentObject.w_CAMVAL = 0
    endif
    if USED("BILAN1")
      SELECT BILAN1
      USE
    endif
    if USED("BILAN2")
      SELECT BILAN2
      USE
    endif
    if USED("BILANCIO")
      SELECT BILANCIO
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
