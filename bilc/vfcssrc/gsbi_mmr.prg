* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mmr                                                        *
*              Manutenzione ratei                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_31]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2011-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_mmr"))

* --- Class definition
define class tgsbi_mmr as StdTrsForm
  Top    = 3
  Left   = 11

  * --- Standard Properties
  Width  = 507
  Height = 344+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-08"
  HelpContextID=197789289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MAN_RATE_IDX = 0
  MANDRATE_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cFile = "MAN_RATE"
  cFileDetail = "MANDRATE"
  cKeySelect = "MRCODICE"
  cKeyWhere  = "MRCODICE=this.w_MRCODICE"
  cKeyDetail  = "MRCODICE=this.w_MRCODICE"
  cKeyWhereODBC = '"MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';

  cKeyDetailWhereODBC = '"MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MANDRATE.MRCODICE="+cp_ToStrODBC(this.w_MRCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MANDRATE.CPROWNUM '
  cPrg = "gsbi_mmr"
  cComment = "Manutenzione ratei"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_MRCODICE = space(15)
  w_MRDESCRI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_MRTIPCON = space(1)
  w_MRCODCON = space(15)
  w_DESCON = space(40)
  w_DTOBSO = ctod('  /  /  ')
  w_MRCODVAL = space(3)
  o_MRCODVAL = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_MRTOTIMP = 0
  w_MRFLDAVE = space(1)
  w_MRDATREG = ctod('  /  /  ')
  w_MRINICOM = ctod('  /  /  ')
  w_MRFINCOM = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MAN_RATE','gsbi_mmr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mmrPag1","gsbi_mmr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione ratei")
      .Pages(1).HelpContextID = 90113428
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMRCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='MAN_RATE'
    this.cWorkTables[4]='MANDRATE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MAN_RATE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MAN_RATE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MRCODICE = NVL(MRCODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MAN_RATE where MRCODICE=KeySet.MRCODICE
    *
    i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2],this.bLoadRecFilter,this.MAN_RATE_IDX,"gsbi_mmr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MAN_RATE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MAN_RATE.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MANDRATE.","MAN_RATE.")
      i_cTable = i_cTable+' MAN_RATE '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MRCODICE',this.w_MRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_DESCON = space(40)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DECTOT = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_MRCODICE = NVL(MRCODICE,space(15))
        .w_MRDESCRI = NVL(MRDESCRI,space(40))
        .w_OBTEST = i_datsys
        .w_MRTIPCON = NVL(MRTIPCON,space(1))
        .w_MRCODCON = NVL(MRCODCON,space(15))
          .link_1_6('Load')
        .w_MRCODVAL = NVL(MRCODVAL,space(3))
          if link_1_11_joined
            this.w_MRCODVAL = NVL(VACODVAL111,NVL(this.w_MRCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT111,0)
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'MAN_RATE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MANDRATE where MRCODICE=KeySet.MRCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MANDRATE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANDRATE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MANDRATE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MANDRATE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MANDRATE"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MRCODICE',this.w_MRCODICE  )
        select * from (i_cTable) MANDRATE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_MRTOTIMP = NVL(MRTOTIMP,0)
          .w_MRFLDAVE = NVL(MRFLDAVE,space(1))
          .w_MRDATREG = NVL(cp_ToDate(MRDATREG),ctod("  /  /  "))
          .w_MRINICOM = NVL(cp_ToDate(MRINICOM),ctod("  /  /  "))
          .w_MRFINCOM = NVL(cp_ToDate(MRFINCOM),ctod("  /  /  "))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = i_datsys
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODAZI=space(5)
      .w_MRCODICE=space(15)
      .w_MRDESCRI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_MRTIPCON=space(1)
      .w_MRCODCON=space(15)
      .w_DESCON=space(40)
      .w_DTOBSO=ctod("  /  /  ")
      .w_MRCODVAL=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_MRTOTIMP=0
      .w_MRFLDAVE=space(1)
      .w_MRDATREG=ctod("  /  /  ")
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,3,.f.)
        .w_OBTEST = i_datsys
        .w_MRTIPCON = 'G'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MRCODCON))
         .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        .w_MRCODVAL = g_PERVAL
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_MRCODVAL))
         .link_1_11('Full')
        endif
        .DoRTCalc(10,10,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(12,16,.f.)
        .w_MRFLDAVE = 'D'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MAN_RATE')
    this.DoRTCalc(18,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMRCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oMRDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oMRCODCON_1_6.enabled = i_bVal
      .Page1.oPag.oMRCODVAL_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMRCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMRCODICE_1_2.enabled = .t.
        .Page1.oPag.oMRDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MAN_RATE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODICE,"MRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRDESCRI,"MRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRTIPCON,"MRTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODCON,"MRCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MRCODVAL,"MRCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2])
    i_lTable = "MAN_RATE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MAN_RATE_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSBI_SMR with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MRTOTIMP N(18,4);
      ,t_MRFLDAVE N(3);
      ,t_MRDATREG D(8);
      ,t_MRINICOM D(8);
      ,t_MRFINCOM D(8);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mmrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_1.controlsource=this.cTrsName+'.t_MRTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.controlsource=this.cTrsName+'.t_MRFLDAVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRDATREG_2_3.controlsource=this.cTrsName+'.t_MRDATREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_4.controlsource=this.cTrsName+'.t_MRINICOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_5.controlsource=this.cTrsName+'.t_MRFINCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(148)
    this.AddVLine(223)
    this.AddVLine(307)
    this.AddVLine(391)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MAN_RATE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MAN_RATE')
        i_extval=cp_InsertValODBCExtFlds(this,'MAN_RATE')
        local i_cFld
        i_cFld=" "+;
                  "(MRCODICE,MRDESCRI,MRTIPCON,MRCODCON,MRCODVAL"+;
                  ",UTCC,UTCV,UTDC,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MRCODICE)+;
                    ","+cp_ToStrODBC(this.w_MRDESCRI)+;
                    ","+cp_ToStrODBC(this.w_MRTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_MRCODCON)+;
                    ","+cp_ToStrODBCNull(this.w_MRCODVAL)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MAN_RATE')
        i_extval=cp_InsertValVFPExtFlds(this,'MAN_RATE')
        cp_CheckDeletedKey(i_cTable,0,'MRCODICE',this.w_MRCODICE)
        INSERT INTO (i_cTable);
              (MRCODICE,MRDESCRI,MRTIPCON,MRCODCON,MRCODVAL,UTCC,UTCV,UTDC,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MRCODICE;
                  ,this.w_MRDESCRI;
                  ,this.w_MRTIPCON;
                  ,this.w_MRCODCON;
                  ,this.w_MRCODVAL;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MANDRATE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANDRATE_IDX,2])
      *
      * insert into MANDRATE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MRCODICE,MRTOTIMP,MRFLDAVE,MRDATREG,MRINICOM"+;
                  ",MRFINCOM,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MRCODICE)+","+cp_ToStrODBC(this.w_MRTOTIMP)+","+cp_ToStrODBC(this.w_MRFLDAVE)+","+cp_ToStrODBC(this.w_MRDATREG)+","+cp_ToStrODBC(this.w_MRINICOM)+;
             ","+cp_ToStrODBC(this.w_MRFINCOM)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MRCODICE',this.w_MRCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MRCODICE,this.w_MRTOTIMP,this.w_MRFLDAVE,this.w_MRDATREG,this.w_MRINICOM"+;
                ",this.w_MRFINCOM,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MAN_RATE
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MAN_RATE')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MRDESCRI="+cp_ToStrODBC(this.w_MRDESCRI)+;
             ",MRTIPCON="+cp_ToStrODBC(this.w_MRTIPCON)+;
             ",MRCODCON="+cp_ToStrODBCNull(this.w_MRCODCON)+;
             ",MRCODVAL="+cp_ToStrODBCNull(this.w_MRCODVAL)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MAN_RATE')
          i_cWhere = cp_PKFox(i_cTable  ,'MRCODICE',this.w_MRCODICE  )
          UPDATE (i_cTable) SET;
              MRDESCRI=this.w_MRDESCRI;
             ,MRTIPCON=this.w_MRTIPCON;
             ,MRCODCON=this.w_MRCODCON;
             ,MRCODVAL=this.w_MRCODVAL;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_MRTOTIMP<>0 AND NOT EMPTY(t_MRDATREG)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MANDRATE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MANDRATE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MANDRATE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MANDRATE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MRTOTIMP="+cp_ToStrODBC(this.w_MRTOTIMP)+;
                     ",MRFLDAVE="+cp_ToStrODBC(this.w_MRFLDAVE)+;
                     ",MRDATREG="+cp_ToStrODBC(this.w_MRDATREG)+;
                     ",MRINICOM="+cp_ToStrODBC(this.w_MRINICOM)+;
                     ",MRFINCOM="+cp_ToStrODBC(this.w_MRFINCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MRTOTIMP=this.w_MRTOTIMP;
                     ,MRFLDAVE=this.w_MRFLDAVE;
                     ,MRDATREG=this.w_MRDATREG;
                     ,MRINICOM=this.w_MRINICOM;
                     ,MRFINCOM=this.w_MRFINCOM;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_MRTOTIMP<>0 AND NOT EMPTY(t_MRDATREG)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MANDRATE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MANDRATE_IDX,2])
        *
        * delete MANDRATE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2])
        *
        * delete MAN_RATE
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_MRTOTIMP<>0 AND NOT EMPTY(t_MRDATREG)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MAN_RATE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MAN_RATE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_OBTEST = i_datsys
        .DoRTCalc(5,10,.t.)
        if .o_MRCODVAL<>.w_MRCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFLDAVE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFLDAVE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRDATREG_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRDATREG_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRINICOM_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRINICOM_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFINCOM_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMRFINCOM_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MRCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MRTIPCON;
                     ,'ANCODICE',trim(this.w_MRCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MRCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MRCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MRTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMRCODCON_1_6'),i_cWhere,'GSAR_BZC',"Elenco conti",'GSAR0MDV.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MRTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MRCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MRTIPCON;
                       ,'ANCODICE',this.w_MRCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_OBTEST<.w_DTOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_MRCODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODVAL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MRCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MRCODVAL))
          select VACODVAL,VADECTOT,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMRCODVAL_1_11'),i_cWhere,'',"Elenco valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MRCODVAL)
            select VACODVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MRCODVAL = space(3)
        this.w_DECTOT = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.VACODVAL as VACODVAL111"+ ",link_1_11.VADECTOT as VADECTOT111"+ ",link_1_11.VADTOBSO as VADTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MAN_RATE.MRCODVAL=link_1_11.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MAN_RATE.MRCODVAL=link_1_11.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMRCODICE_1_2.value==this.w_MRCODICE)
      this.oPgFrm.Page1.oPag.oMRCODICE_1_2.value=this.w_MRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMRDESCRI_1_3.value==this.w_MRDESCRI)
      this.oPgFrm.Page1.oPag.oMRDESCRI_1_3.value=this.w_MRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODCON_1_6.value==this.w_MRCODCON)
      this.oPgFrm.Page1.oPag.oMRCODCON_1_6.value=this.w_MRCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODVAL_1_11.value==this.w_MRCODVAL)
      this.oPgFrm.Page1.oPag.oMRCODVAL_1_11.value=this.w_MRCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_1.value==this.w_MRTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_1.value=this.w_MRTOTIMP
      replace t_MRTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRTOTIMP_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.RadioValue()==this.w_MRFLDAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.SetRadio()
      replace t_MRFLDAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDATREG_2_3.value==this.w_MRDATREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDATREG_2_3.value=this.w_MRDATREG
      replace t_MRDATREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRDATREG_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_4.value==this.w_MRINICOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_4.value=this.w_MRINICOM
      replace t_MRINICOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_5.value==this.w_MRFINCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_5.value=this.w_MRFINCOM
      replace t_MRFINCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'MAN_RATE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MRCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMRCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_MRCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MRCODCON) or not(EMPTY(.w_DTOBSO) OR .w_OBTEST<.w_DTOBSO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMRCODCON_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MRCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   (empty(.w_MRCODVAL) or not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMRCODVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MRCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_MRFLDAVE $ 'DA') and (.w_MRTOTIMP<>0) and (.w_MRTOTIMP<>0 AND NOT EMPTY(.w_MRDATREG))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: 'D' (dare) o 'A' (avere)")
        case   (empty(.w_MRINICOM) or not(.w_MRFINCOM>=.w_MRINICOM OR EMPTY(.w_MRFINCOM))) and (.w_MRTOTIMP<>0) and (.w_MRTOTIMP<>0 AND NOT EMPTY(.w_MRDATREG))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRINICOM_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_MRFINCOM) or not(.w_MRFINCOM>=.w_MRINICOM)) and (.w_MRTOTIMP<>0) and (.w_MRTOTIMP<>0 AND NOT EMPTY(.w_MRDATREG))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFINCOM_2_5
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if .w_MRTOTIMP<>0 AND NOT EMPTY(.w_MRDATREG)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MRCODVAL = this.w_MRCODVAL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_MRTOTIMP<>0 AND NOT EMPTY(t_MRDATREG))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MRTOTIMP=0
      .w_MRFLDAVE=space(1)
      .w_MRDATREG=ctod("  /  /  ")
      .w_MRINICOM=ctod("  /  /  ")
      .w_MRFINCOM=ctod("  /  /  ")
      .DoRTCalc(1,16,.f.)
        .w_MRFLDAVE = 'D'
    endwith
    this.DoRTCalc(18,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MRTOTIMP = t_MRTOTIMP
    this.w_MRFLDAVE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.RadioValue(.t.)
    this.w_MRDATREG = t_MRDATREG
    this.w_MRINICOM = t_MRINICOM
    this.w_MRFINCOM = t_MRFINCOM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MRTOTIMP with this.w_MRTOTIMP
    replace t_MRFLDAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMRFLDAVE_2_2.ToRadio()
    replace t_MRDATREG with this.w_MRDATREG
    replace t_MRINICOM with this.w_MRINICOM
    replace t_MRFINCOM with this.w_MRFINCOM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mmrPag1 as StdContainer
  Width  = 503
  height = 344
  stdWidth  = 503
  stdheight = 344
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMRCODICE_1_2 as StdField with uid="YTMVJMHIIT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MRCODICE", cQueryName = "MRCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del rateo",;
    HelpContextID = 29982987,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=57, Top=10, cSayPict="'!!!!!!!!!!!!!!!'", cGetPict="'!!!!!!!!!!!!!!!'", InputMask=replicate('X',15)

  add object oMRDESCRI_1_3 as StdField with uid="PNGTBJXCKJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MRDESCRI", cQueryName = "MRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del rateo",;
    HelpContextID = 55602929,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=194, Top=10, InputMask=replicate('X',40)

  add object oMRCODCON_1_6 as StdField with uid="NUDGGKLEDG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MRCODCON", cQueryName = "MRCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Conto contabile associato",;
    HelpContextID = 70680300,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=57, Top=41, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MRCODCON"

  func oMRCODCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MRTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MRTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMRCODCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'GSAR0MDV.CONTI_VZM',this.parent.oContained
  endproc
  proc oMRCODCON_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MRTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MRCODCON
    i_obj.ecpSave()
  endproc

  add object oDESCON_1_9 as StdField with uid="KXPYVLDQCP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143756490,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=194, Top=41, InputMask=replicate('X',40)

  add object oMRCODVAL_1_11 as StdField with uid="GOQWMRTTEJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MRCODVAL", cQueryName = "MRCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 20348654,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=57, Top=72, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_MRCODVAL"

  func oMRCODVAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVAL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODVAL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMRCODVAL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=101, width=485,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="MRTOTIMP",Label1="Importo",Field2="MRFLDAVE",Label2="Sezione",Field3="MRDATREG",Label3="Data",Field4="MRINICOM",Label4="Dal",Field5="MRFINCOM",Label5="Al",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118301562

  add object oStr_1_7 as StdString with uid="GEUJDLRCGS",Visible=.t., Left=0, Top=10,;
    Alignment=1, Width=55, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="PRTGCHDWAV",Visible=.t., Left=2, Top=41,;
    Alignment=1, Width=52, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PLCSJTIPCM",Visible=.t., Left=313, Top=84,;
    Alignment=2, Width=151, Height=15,;
    Caption="Competenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="UUWGIOTLHZ",Visible=.t., Left=225, Top=84,;
    Alignment=2, Width=82, Height=15,;
    Caption="Manifestazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="UFVWLRDRGV",Visible=.t., Left=1, Top=72,;
    Alignment=1, Width=53, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="SWYPWCYCGN",left=391, top=102, width=1,height=235

  add object oBox_1_15 as StdBox with uid="KAUXJSOMWT",left=223, top=80, width=269,height=23

  add object oBox_1_25 as StdBox with uid="MEPBRSHZEM",left=307, top=80, width=1,height=22

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=122,;
    width=481+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=123,width=480+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsbi_mmrBodyRow as CPBodyRowCnt
  Width=471
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMRTOTIMP_2_1 as StdTrsField with uid="PAJWLADCAD",rtseq=16,rtrep=.t.,;
    cFormVar="w_MRTOTIMP",value=0,;
    ToolTipText = "Importo rateo",;
    HelpContextID = 221605610,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=-2, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oMRFLDAVE_2_2 as StdTrsCombo with uid="BWBHFAWUSO",rtrep=.t.,;
    cFormVar="w_MRFLDAVE", RowSource=""+"Dare,"+"Avere" , ;
    ToolTipText = "Sezione: dare/avere",;
    HelpContextID = 164016395,;
    Height=21, Width=71, Left=144, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Inserire: 'D' (dare) o 'A' (avere)";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMRFLDAVE_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MRFLDAVE,&i_cF..t_MRFLDAVE),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'A',;
    space(1))))
  endfunc
  func oMRFLDAVE_2_2.GetRadio()
    this.Parent.oContained.w_MRFLDAVE = this.RadioValue()
    return .t.
  endfunc

  func oMRFLDAVE_2_2.ToRadio()
    this.Parent.oContained.w_MRFLDAVE=trim(this.Parent.oContained.w_MRFLDAVE)
    return(;
      iif(this.Parent.oContained.w_MRFLDAVE=='D',1,;
      iif(this.Parent.oContained.w_MRFLDAVE=='A',2,;
      0)))
  endfunc

  func oMRFLDAVE_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMRFLDAVE_2_2.mCond()
    with this.Parent.oContained
      return (.w_MRTOTIMP<>0)
    endwith
  endfunc

  func oMRFLDAVE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MRFLDAVE $ 'DA')
    endwith
    return bRes
  endfunc

  add object oMRDATREG_2_3 as StdTrsField with uid="YTCLMGASKP",rtseq=18,rtrep=.t.,;
    cFormVar="w_MRDATREG",value=ctod("  /  /  "),;
    ToolTipText = "Data di previsto incasso/pagamento",;
    HelpContextID = 196841741,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=220, Top=0

  func oMRDATREG_2_3.mCond()
    with this.Parent.oContained
      return (.w_MRTOTIMP<>0)
    endwith
  endfunc

  add object oMRINICOM_2_4 as StdTrsField with uid="ZKWKNQDOQG",rtseq=19,rtrep=.t.,;
    cFormVar="w_MRINICOM",value=ctod("  /  /  "),;
    ToolTipText = "Inizio competenza",;
    HelpContextID = 65478381,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=304, Top=0

  func oMRINICOM_2_4.mCond()
    with this.Parent.oContained
      return (.w_MRTOTIMP<>0)
    endwith
  endfunc

  func oMRINICOM_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MRFINCOM>=.w_MRINICOM OR EMPTY(.w_MRFINCOM))
    endwith
    return bRes
  endfunc

  add object oMRFINCOM_2_5 as StdTrsField with uid="NUKUXEMRXU",rtseq=20,rtrep=.t.,;
    cFormVar="w_MRFINCOM",value=ctod("  /  /  "),;
    ToolTipText = "Fine competenza",;
    HelpContextID = 60575469,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=387, Top=0

  func oMRFINCOM_2_5.mCond()
    with this.Parent.oContained
      return (.w_MRTOTIMP<>0)
    endwith
  endfunc

  func oMRFINCOM_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MRFINCOM>=.w_MRINICOM)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oMRTOTIMP_2_1.When()
    return(.t.)
  proc oMRTOTIMP_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMRTOTIMP_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mmr','MAN_RATE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MRCODICE=MAN_RATE.MRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
