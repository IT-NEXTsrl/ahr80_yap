* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kba                                                        *
*              Generazione bilancio previsionale analitica                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_95]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_kba",oParentObject))

* --- Class definition
define class tgsbi_kba as StdForm
  Top    = 18
  Left   = 16

  * --- Standard Properties
  Width  = 754
  Height = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=152435095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  STR_BIAN_IDX = 0
  ESERCIZI_IDX = 0
  PER_ELAB_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsbi_kba"
  cComment = "Generazione bilancio previsionale analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OK = .F.
  w_TIPBILAN = space(1)
  w_MOVIMENT = space(1)
  w_STRUTTUR = space(15)
  w_ESER = space(4)
  o_ESER = space(4)
  w_PERIODO = space(15)
  w_NUMERO = 0
  w_ESERCI = space(4)
  w_VALNAZ = space(3)
  w_DECIMA = 0
  w_DATABIL = ctod('  /  /  ')
  w_DESCRIBI = space(35)
  w_COSTANTE = space(100)
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_NEWPER = space(15)
  w_NEWESE = space(4)
  w_NEWVAL = space(3)
  w_DECIMI = 0
  w_DTOBSO = ctod('  /  /  ')
  w_GBNUMBIL = 0
  w_GBBILESE = space(4)
  w_ZoomGepr = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kbaPag1","gsbi_kba",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOVIMENT_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGepr = this.oPgFrm.Pages(1).oPag.ZoomGepr
    DoDefault()
    proc Destroy()
      this.w_ZoomGepr = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='STR_BIAN'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='PER_ELAB'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OK=.f.
      .w_TIPBILAN=space(1)
      .w_MOVIMENT=space(1)
      .w_STRUTTUR=space(15)
      .w_ESER=space(4)
      .w_PERIODO=space(15)
      .w_NUMERO=0
      .w_ESERCI=space(4)
      .w_VALNAZ=space(3)
      .w_DECIMA=0
      .w_DATABIL=ctod("  /  /  ")
      .w_DESCRIBI=space(35)
      .w_COSTANTE=space(100)
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_NEWPER=space(15)
      .w_NEWESE=space(4)
      .w_NEWVAL=space(3)
      .w_DECIMI=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_GBNUMBIL=0
      .w_GBBILESE=space(4)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_OK = .F.
        .w_TIPBILAN = 'A'
        .w_MOVIMENT = 'E'
        .w_STRUTTUR = space(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_5('Full')
        endif
        .w_ESER = g_codese
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ESER))
          .link_1_6('Full')
        endif
        .w_PERIODO = space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PERIODO))
          .link_1_7('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_ESERCI))
          .link_1_11('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_VALNAZ))
          .link_1_12('Full')
        endif
          .DoRTCalc(11,14,.f.)
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_FLSELE = 0
        .w_NEWPER = SPACE(15)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_NEWPER))
          .link_1_33('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_NEWESE))
          .link_1_35('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_NEWVAL))
          .link_1_36('Full')
        endif
          .DoRTCalc(20,20,.f.)
        .w_DTOBSO = i_datsys
        .w_GBNUMBIL = NVL(.w_ZoomGepr.GetVar( 'GBNUMBIL' ) , 0)
        .w_GBBILESE = NVL(.w_ZoomGepr.GetVar( 'GBNUMBIL' ) , SPACE(4))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_ESER<>.w_ESER
            .w_PERIODO = space(15)
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(8,8,.t.)
          .link_1_11('Full')
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(11,17,.t.)
          .link_1_35('Full')
          .link_1_36('Full')
        .DoRTCalc(20,21,.t.)
            .w_GBNUMBIL = NVL(.w_ZoomGepr.GetVar( 'GBNUMBIL' ) , 0)
            .w_GBBILESE = NVL(.w_ZoomGepr.GetVar( 'GBNUMBIL' ) , SPACE(4))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOSTANTE_1_16.visible=!this.oPgFrm.Page1.oPag.oCOSTANTE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomGepr.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSA',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_5'),i_cWhere,'GSBI_MSA',"Strutture di bilancio analitica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_6'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_7'),i_cWhere,'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMA = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECIMA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWPER
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_NEWPER)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PECODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_NEWPER))
          select PECODICE,PECODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWPER)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWPER) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oNEWPER_1_33'),i_cWhere,'GSBI_APE',"Periodo di elaborazione",'gsbi_kel.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PECODESE";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PECODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PECODESE";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_NEWPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_NEWPER)
            select PECODICE,PECODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWPER = NVL(_Link_.PECODICE,space(15))
      this.w_NEWESE = NVL(_Link_.PECODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_NEWPER = space(15)
      endif
      this.w_NEWESE = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWESE
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_NEWESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_NEWESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWESE = NVL(_Link_.ESCODESE,space(4))
      this.w_NEWVAL = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NEWESE = space(4)
      endif
      this.w_NEWVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWVAL
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_NEWVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_NEWVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIMI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_NEWVAL = space(3)
      endif
      this.w_DECIMI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_4.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_5.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_5.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_6.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_6.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_7.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_7.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_10.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_10.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_11.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_11.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATABIL_1_14.value==this.w_DATABIL)
      this.oPgFrm.Page1.oPag.oDATABIL_1_14.value=this.w_DATABIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIBI_1_15.value==this.w_DESCRIBI)
      this.oPgFrm.Page1.oPag.oDESCRIBI_1_15.value=this.w_DESCRIBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSTANTE_1_16.value==this.w_COSTANTE)
      this.oPgFrm.Page1.oPag.oCOSTANTE_1_16.value=this.w_COSTANTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_17.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPER_1_33.value==this.w_NEWPER)
      this.oPgFrm.Page1.oPag.oNEWPER_1_33.value=this.w_NEWPER
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    return

enddefine

* --- Define pages as container
define class tgsbi_kbaPag1 as StdContainer
  Width  = 750
  height = 453
  stdWidth  = 750
  stdheight = 453
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oMOVIMENT_1_4 as StdCombo with uid="UCVMTKEVIN",rtseq=4,rtrep=.f.,left=136,top=21,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 214651366;
    , cFormVar="w_MOVIMENT",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_4.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oMOVIMENT_1_4.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_4.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='E',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      0))
  endfunc

  add object oSTRUTTUR_1_5 as StdField with uid="EZNGHHZFSR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 45118328,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=323, Top=21, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", cZoomOnZoom="GSBI_MSA", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSA',"Strutture di bilancio analitica",'',this.parent.oContained
  endproc
  proc oSTRUTTUR_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR
     i_obj.ecpSave()
  endproc

  add object oESER_1_6 as StdField with uid="ZMZCLPSILB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione",;
    HelpContextID = 158114118,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=513, Top=21, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPERIODO_1_7 as StdField with uid="SHEYBYBNVV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 229350410,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=625, Top=21, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodo di elaborazione",'PERIODO.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc


  add object oObj_1_8 as cp_runprogram with uid="LQRPASKAUV",left=760, top=206, width=129,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BBI",;
    cEvent = "w_STRUTTUR Changed,w_MOVIMENT Changed,w_PERIODO Changed,w_ESERCI Changed,w_ESER Changed",;
    nPag=1;
    , HelpContextID = 206438170


  add object oBtn_1_9 as StdButton with uid="KJUJPJFGHN",left=88, top=53, width=18,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 152636118;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      do GSBI_KBZ with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMERO_1_10 as StdField with uid="NFCWVXAEKD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 41933866,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=136, Top=75

  add object oESERCI_1_11 as StdField with uid="FYAUUHOXMB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 157507258,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=261, Top=75, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDATABIL_1_14 as StdField with uid="CIYXKOQVND",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATABIL", cQueryName = "DATABIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 159613130,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=338, Top=75

  add object oDESCRIBI_1_15 as StdField with uid="VPDDIXGYNX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCRIBI", cQueryName = "DESCRIBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142707841,;
   bGlobalFont=.t.,;
    Height=21, Width=221, Left=461, Top=75, InputMask=replicate('X',35)

  add object oCOSTANTE_1_16 as StdField with uid="MIDBUBWNHO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COSTANTE", cQueryName = "COSTANTE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Formula per ricalcolo importi (utilizzare costanti, o parametri costanti)",;
    HelpContextID = 192904555,;
   bGlobalFont=.t.,;
    Height=21, Width=511, Left=136, Top=101, cSayPict="REPL('!', 100)", cGetPict="REPL('!', 100)", InputMask=replicate('X',100)

  func oCOSTANTE_1_16.mHide()
    with this.Parent.oContained
      return (.w_OK=.F.)
    endwith
  endfunc

  add object oSELEZI_1_17 as StdRadio with uid="RUIRTAXSVL",rtseq=15,rtrep=.f.,left=3, top=408, width=130,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 134216666
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 134216666
      this.Buttons(2).Top=15
      this.SetAll("Width",128)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_17.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_17.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object ZoomGepr as cp_szoombox with uid="SMOVLERHQH",left=3, top=126, width=747,height=274,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="DETTBIL1",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSBI_AGA",cTable="GESTBILA",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 206438170


  add object oObj_1_31 as cp_runprogram with uid="ZFMNTDJWHA",left=760, top=227, width=129,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BEP("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 206438170

  add object oNEWPER_1_33 as StdField with uid="SAMLCULTVN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NEWPER", cQueryName = "NEWPER",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione del bilancio previsionale da generare",;
    HelpContextID = 4475946,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=464, Top=408, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_NEWPER"

  func oNEWPER_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWPER_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWPER_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oNEWPER_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodo di elaborazione",'gsbi_kel.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oNEWPER_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_NEWPER
     i_obj.ecpSave()
  endproc


  add object oBtn_1_39 as StdButton with uid="TAPDIOARMF",left=696, top=75, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca dei bilanci da generare";
    , HelpContextID = 207513578;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        do GSBI_BBP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO) AND .w_OK=.F.)
      endwith
    endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="TMIYEOPTVN",left=136, top=405, width=48,height=45,;
    CpPicture="BMP\Elabora.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 40794694;
    , caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSBI_BEP(this.Parent.oContained,"ELAB")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OK=.T. AND NOT EMPTY(.w_COSTANTE))
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="CVTQQSGBNQ",left=641, top=405, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Salva il bilancio previsionale";
    , HelpContextID = 237810297;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        do GSBI_BBS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO) AND NOT EMPTY(.w_NEWPER))
      endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="HMLRBFRNYB",left=696, top=405, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 159752518;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_19 as StdString with uid="WFZESZXDQF",Visible=.t., Left=42, Top=21,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="CZDFLEUEPH",Visible=.t., Left=260, Top=21,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EOJORDYBRH",Visible=.t., Left=568, Top=21,;
    Alignment=1, Width=55, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SALNLRUYAV",Visible=.t., Left=3, Top=53,;
    Alignment=0, Width=78, Height=15,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="DEOUWSNUGN",Visible=.t., Left=3, Top=5,;
    Alignment=0, Width=79, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="TYPRINSHJX",Visible=.t., Left=193, Top=75,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="POORSPKLKM",Visible=.t., Left=309, Top=75,;
    Alignment=1, Width=28, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="VCUNQFEFXD",Visible=.t., Left=409, Top=75,;
    Alignment=1, Width=50, Height=15,;
    Caption="Descr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="YVWLFBZQBI",Visible=.t., Left=73, Top=75,;
    Alignment=1, Width=61, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="LMFVMVVHTD",Visible=.t., Left=444, Top=21,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="HDDGEXMYIJ",Visible=.t., Left=5, Top=101,;
    Alignment=1, Width=129, Height=15,;
    Caption="Variazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_OK=.F.)
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="ETCSIUGHLL",Visible=.t., Left=214, Top=408,;
    Alignment=1, Width=246, Height=15,;
    Caption="Periodo bilancio previsionale da generare:"  ;
  , bGlobalFont=.t.

  add object oBox_1_24 as StdBox with uid="VICYFSWXQB",left=1, top=49, width=743,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kba','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
