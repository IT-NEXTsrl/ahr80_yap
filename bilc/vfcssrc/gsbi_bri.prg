* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bri                                                        *
*              Inserimento regole analisi di bilancio                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_52]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-28                                                      *
* Last revis.: 2002-11-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPINSERT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bri",oParentObject,m.w_TIPINSERT)
return(i_retval)

define class tgsbi_bri as StdBatch
  * --- Local variables
  w_TIPINSERT = space(1)
  w_MESS = space(50)
  * --- WorkFile variables
  REG_EXTM_idx=0
  REG_EXTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'inserimento delle regole di Analisi di Bilancio, con  Carica/Salva dati esterni, invocato da 'GSUT_BCO('Carica')'
    * --- Try
    local bErr_038856E8
    bErr_038856E8=bTrsErr
    this.Try_038856E8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORE = .T.
    endif
    bTrsErr=bTrsErr or bErr_038856E8
    * --- End
  endproc
  proc Try_038856E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_TIPINSERT="D"
        * --- Try
        local bErr_0387D768
        bErr_0387D768=bTrsErr
        this.Try_0387D768()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_038A1948
          bErr_038A1948=bTrsErr
          this.Try_038A1948()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESS = "La regola il cui codice �: %1 � gi� presente, inserimento non effettuato.%0Continuo?"
            if not ah_YesNo(this.w_MESS,,this.oParentObject.w_RECODICE)
              * --- Raise
              i_Error="Import fallito"
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_038A1948
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_0387D768
        * --- End
      case this.w_TIPINSERT="M"
        * --- Try
        local bErr_03880BE8
        bErr_03880BE8=bTrsErr
        this.Try_03880BE8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_03679FB0
          bErr_03679FB0=bTrsErr
          this.Try_03679FB0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESS = "La regola il cui codice �: %1 � gi� presente, inserimento non effettuato.%0Continuo?"
            if not ah_YesNo(this.w_MESS,,this.oParentObject.w_RECODICE)
              * --- Raise
              i_Error="Import fallito"
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_03679FB0
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_03880BE8
        * --- End
        * --- Inserisco la prima riga di dettaglio
        * --- Try
        local bErr_036796E0
        bErr_036796E0=bTrsErr
        this.Try_036796E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_03679770
          bErr_03679770=bTrsErr
          this.Try_03679770()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESS = "La regola il cui codice �: %1 � gi� presente, inserimento non effettuato.%0Continuo?"
            if not ah_YesNo(this.w_MESS,,this.oParentObject.w_RECODICE)
              * --- Raise
              i_Error="Import fallito"
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_03679770
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_036796E0
        * --- End
    endcase
    return
  proc Try_0387D768()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into REG_EXTR
    i_nConn=i_TableProp[this.REG_EXTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_EXTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",CPROWORD"+",RETIPSTE"+",REDESOPE"+",RENOMCUR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'REG_EXTR','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'REG_EXTR','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'REG_EXTR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'REG_EXTR','RETIPSTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'REG_EXTR','REDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'REG_EXTR','RENOMCUR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'RETIPSTE',this.oParentObject.w_RETIPSTE,'REDESOPE',this.oParentObject.w_REDESOPE,'RENOMCUR',this.oParentObject.w_RENOMCUR)
      insert into (i_cTable) (RECODICE,CPROWNUM,CPROWORD,RETIPSTE,REDESOPE,RENOMCUR &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_RETIPSTE;
           ,this.oParentObject.w_REDESOPE;
           ,this.oParentObject.w_RENOMCUR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_038A1948()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into REG_EXTR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.REG_EXTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_EXTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'REG_EXTR','CPROWORD');
      +",RETIPSTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'REG_EXTR','RETIPSTE');
      +",REDESOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'REG_EXTR','REDESOPE');
      +",RENOMCUR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'REG_EXTR','RENOMCUR');
          +i_ccchkf ;
      +" where ";
          +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.oParentObject.w_CPROWORD;
          ,RETIPSTE = this.oParentObject.w_RETIPSTE;
          ,REDESOPE = this.oParentObject.w_REDESOPE;
          ,RENOMCUR = this.oParentObject.w_RENOMCUR;
          &i_ccchkf. ;
       where;
          RECODICE = this.oParentObject.w_RECODICE;
          and CPROWNUM = this.oParentObject.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03880BE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserisco la testata
    * --- Insert into REG_EXTM
    i_nConn=i_TableProp[this.REG_EXTM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_EXTM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",REDESCRI"+",REDESSUP"+",RE__TIPO"+",REDTINVA"+",REDTOBSO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'REG_EXTM','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESCRI),'REG_EXTM','REDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESSUP),'REG_EXTM','REDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RE__TIPO),'REG_EXTM','RE__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTINVA),'REG_EXTM','REDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTOBSO),'REG_EXTM','REDTOBSO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'REDESCRI',this.oParentObject.w_REDESCRI,'REDESSUP',this.oParentObject.w_REDESSUP,'RE__TIPO',this.oParentObject.w_RE__TIPO,'REDTINVA',this.oParentObject.w_REDTINVA,'REDTOBSO',this.oParentObject.w_REDTOBSO)
      insert into (i_cTable) (RECODICE,REDESCRI,REDESSUP,RE__TIPO,REDTINVA,REDTOBSO &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_REDESCRI;
           ,this.oParentObject.w_REDESSUP;
           ,this.oParentObject.w_RE__TIPO;
           ,this.oParentObject.w_REDTINVA;
           ,this.oParentObject.w_REDTOBSO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03679FB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into REG_EXTM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.REG_EXTM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTM_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_EXTM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"REDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESCRI),'REG_EXTM','REDESCRI');
      +",REDESSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESSUP),'REG_EXTM','REDESSUP');
      +",RE__TIPO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RE__TIPO),'REG_EXTM','RE__TIPO');
      +",REDTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTINVA),'REG_EXTM','REDTINVA');
      +",REDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDTOBSO),'REG_EXTM','REDTOBSO');
          +i_ccchkf ;
      +" where ";
          +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
             )
    else
      update (i_cTable) set;
          REDESCRI = this.oParentObject.w_REDESCRI;
          ,REDESSUP = this.oParentObject.w_REDESSUP;
          ,RE__TIPO = this.oParentObject.w_RE__TIPO;
          ,REDTINVA = this.oParentObject.w_REDTINVA;
          ,REDTOBSO = this.oParentObject.w_REDTOBSO;
          &i_ccchkf. ;
       where;
          RECODICE = this.oParentObject.w_RECODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_036796E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into REG_EXTR
    i_nConn=i_TableProp[this.REG_EXTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_EXTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RECODICE"+",CPROWNUM"+",CPROWORD"+",RETIPSTE"+",REDESOPE"+",RENOMCUR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RECODICE),'REG_EXTR','RECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'REG_EXTR','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'REG_EXTR','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'REG_EXTR','RETIPSTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'REG_EXTR','REDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'REG_EXTR','RENOMCUR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RECODICE',this.oParentObject.w_RECODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'RETIPSTE',this.oParentObject.w_RETIPSTE,'REDESOPE',this.oParentObject.w_REDESOPE,'RENOMCUR',this.oParentObject.w_RENOMCUR)
      insert into (i_cTable) (RECODICE,CPROWNUM,CPROWORD,RETIPSTE,REDESOPE,RENOMCUR &i_ccchkf. );
         values (;
           this.oParentObject.w_RECODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_RETIPSTE;
           ,this.oParentObject.w_REDESOPE;
           ,this.oParentObject.w_RENOMCUR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03679770()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into REG_EXTR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.REG_EXTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REG_EXTR_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_EXTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'REG_EXTR','CPROWORD');
      +",RETIPSTE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RETIPSTE),'REG_EXTR','RETIPSTE');
      +",REDESOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_REDESOPE),'REG_EXTR','REDESOPE');
      +",RENOMCUR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RENOMCUR),'REG_EXTR','RENOMCUR');
          +i_ccchkf ;
      +" where ";
          +"RECODICE = "+cp_ToStrODBC(this.oParentObject.w_RECODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.oParentObject.w_CPROWORD;
          ,RETIPSTE = this.oParentObject.w_RETIPSTE;
          ,REDESOPE = this.oParentObject.w_REDESOPE;
          ,RENOMCUR = this.oParentObject.w_RENOMCUR;
          &i_ccchkf. ;
       where;
          RECODICE = this.oParentObject.w_RECODICE;
          and CPROWNUM = this.oParentObject.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_TIPINSERT)
    this.w_TIPINSERT=w_TIPINSERT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='REG_EXTM'
    this.cWorkTables[2]='REG_EXTR'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPINSERT"
endproc
