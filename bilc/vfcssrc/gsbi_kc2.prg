* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kc2                                                        *
*              Selezione confronto secondo bilancio                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_38]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2005-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_kc2",oParentObject))

* --- Class definition
define class tgsbi_kc2 as StdForm
  Top    = 220
  Left   = 98

  * --- Standard Properties
  Width  = 598
  Height = 178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2005-07-04"
  HelpContextID=99223145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsbi_kc2"
  cComment = "Selezione confronto secondo bilancio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_STATO = space(1)
  w_STRUTTUR = space(15)
  w_NUMERO2 = 0
  w_ESERCI2 = space(4)
  w_DESCRIB2 = space(35)
  w_PERIODO2 = space(15)
  w_TIPOMOV2 = space(20)
  w_NUMBIL1 = 0
  w_TIPBILAN = space(1)
  w_ESERCI = space(4)
  w_ragru = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kc2Pag1","gsbi_kc2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ragru = this.oPgFrm.Pages(1).oPag.ragru
    DoDefault()
    proc Destroy()
      this.w_ragru = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STATO=space(1)
      .w_STRUTTUR=space(15)
      .w_NUMERO2=0
      .w_ESERCI2=space(4)
      .w_DESCRIB2=space(35)
      .w_PERIODO2=space(15)
      .w_TIPOMOV2=space(20)
      .w_NUMBIL1=0
      .w_TIPBILAN=space(1)
      .w_ESERCI=space(4)
      .w_STATO=oParentObject.w_STATO
      .w_STRUTTUR=oParentObject.w_STRUTTUR
      .w_NUMERO2=oParentObject.w_NUMERO2
      .w_ESERCI2=oParentObject.w_ESERCI2
      .w_DESCRIB2=oParentObject.w_DESCRIB2
      .w_PERIODO2=oParentObject.w_PERIODO2
      .w_TIPOMOV2=oParentObject.w_TIPOMOV2
      .w_NUMBIL1=oParentObject.w_NUMBIL1
      .w_TIPBILAN=oParentObject.w_TIPBILAN
      .w_ESERCI=oParentObject.w_ESERCI
      .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_NUMERO2 = Nvl( .w_ragru.getVar('GBNUMBIL') , 0 )
        .w_ESERCI2 = Nvl(.w_ragru.getVar('GBBILESE'), Space(4))
        .w_DESCRIB2 = Nvl ( .w_ragru.getVar('GBDESCRI') , Space(35) )
        .w_PERIODO2 = Nvl( .w_ragru.getVar('GBPERIOD') , Space(15) )
        .w_TIPOMOV2 = Nvl ( .w_ragru.getVar('TIPOMOV') , Space(20) )
    endwith
    this.DoRTCalc(8,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_STATO=.w_STATO
      .oParentObject.w_STRUTTUR=.w_STRUTTUR
      .oParentObject.w_NUMERO2=.w_NUMERO2
      .oParentObject.w_ESERCI2=.w_ESERCI2
      .oParentObject.w_DESCRIB2=.w_DESCRIB2
      .oParentObject.w_PERIODO2=.w_PERIODO2
      .oParentObject.w_TIPOMOV2=.w_TIPOMOV2
      .oParentObject.w_NUMBIL1=.w_NUMBIL1
      .oParentObject.w_TIPBILAN=.w_TIPBILAN
      .oParentObject.w_ESERCI=.w_ESERCI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_NUMERO2 = Nvl( .w_ragru.getVar('GBNUMBIL') , 0 )
            .w_ESERCI2 = Nvl(.w_ragru.getVar('GBBILESE'), Space(4))
            .w_DESCRIB2 = Nvl ( .w_ragru.getVar('GBDESCRI') , Space(35) )
            .w_PERIODO2 = Nvl( .w_ragru.getVar('GBPERIOD') , Space(15) )
            .w_TIPOMOV2 = Nvl ( .w_ragru.getVar('TIPOMOV') , Space(20) )
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ragru.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ragru.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_kc2Pag1 as StdContainer
  Width  = 594
  height = 178
  stdWidth  = 594
  stdheight = 178
  resizeXpos=406
  resizeYpos=106
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ragru as cp_zoombox with uid="AYPIELFPLG",left=2, top=5, width=589,height=171,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="GESTBILA",cZoomFile="CONF_BIL",bOptions=.F.,bAdvOptions=.F.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 78774502


  add object oObj_1_2 as cp_runprogram with uid="RQARPKXQHZ",left=596, top=178, width=140,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BB1",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 78774502


  add object oObj_1_3 as cp_runprogram with uid="QTCHDRNTIQ",left=596, top=200, width=140,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSBI_BB2",;
    cEvent = "w_ragru selected",;
    nPag=1;
    , HelpContextID = 78774502
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kc2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
