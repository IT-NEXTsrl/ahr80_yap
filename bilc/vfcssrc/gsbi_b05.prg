* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b05                                                        *
*              Elabora coge ratei                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_31]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b05",oParentObject)
return(i_retval)

define class tgsbi_b05 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_SCARTO = 0
  w_COD003 = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_APPO = space(15)
  w_IMPDAR = 0
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_IMPAVE = 0
  w_RATATT = space(15)
  w_RECODICE = space(15)
  w_RATPAS = space(15)
  w_REDESCRI = space(45)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Manutenzione Ratei COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTRA001
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_FILINI = this.oParentObject.oParentObject.w_DATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_DATFIN
    this.w_RATATT = this.oParentObject.oParentObject.w_RATATT
    this.w_RATPAS = this.oParentObject.oParentObject.w_RATPAS
    if USED("EXTRA001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), TOTIMP N(18,4))
      SELECT EXTRA001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "MG" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_CODICE = CODICE
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_SCARTO = 0
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, i_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, 1, i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      * --- 2^OPERAZIONE: Se presente una Competenza, calcola l'esatto Importo
      if NOT EMPTY(this.w_INICOM) AND NOT EMPTY(this.w_FINCOM)
        if this.w_FINCOM<this.w_FILINI OR this.w_INICOM>this.w_FILFIN
          * --- Registrazione Totalmente fuori dal Periodo, Ignora e passa al record successivo
          LOOP
        else
          this.w_GIOCOM = this.w_FINCOM - (this.w_INICOM-1)
          * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
          if this.w_INICOM<this.w_FILINI AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FILINI - this.w_INICOM
            this.w_SCARTO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Registrazione con Competenza che Prosegue oltre il Periodo, Scarta anche la parte successiva
          if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FINCOM - this.w_FILFIN
            this.w_SCARTO = this.w_SCARTO + cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Ricalcola l'Importo di Competenza
          this.w_TOTIMP = this.w_TOTIMP - this.w_SCARTO
        endif
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      if this.w_TOTIMP<>0
        INSERT INTO TmpAgg1 (TIPCON, CODICE, TOTIMP) ;
        VALUES (this.w_TIPCON, this.w_CODICE, this.w_TOTIMP)
        this.w_APPO = IIF(this.w_TOTIMP>0, this.w_RATPAS, this.w_RATATT)
        * --- Riga Risconto di Storno
        INSERT INTO TmpAgg1 (TIPCON, CODICE, TOTIMP) ;
        VALUES ("G", this.w_APPO, -this.w_TOTIMP)
      endif
      SELECT EXTRA001
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTRA001
      USE
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("TmpAgg1")
        SELECT tmpAgg1
        if RECCOUNT()>0
          SELECT TIPCON, CODICE, SUM(TOTIMP) AS TOTALE FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY TIPCON, CODICE
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_TIPCON = TIPCON
          this.w_COD001 = IIF(this.w_TIPCON="M", CODICE, SPACE(15))
          this.w_COD002 = IIF(this.w_TIPCON="G", CODICE, SPACE(15))
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
