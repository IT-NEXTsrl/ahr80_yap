* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bo3                                                        *
*              Zoom on zoom strut.indici                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_31]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bo3",oParentObject)
return(i_retval)

define class tgsbi_bo3 as StdBatch
  * --- Local variables
  w_IBTIPIND = space(1)
  w_PROG = .NULL.
  w_DXBTN = .f.
  w_CODICE = space(20)
  * --- WorkFile variables
  STR_INDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH PER LANCIARE LO ZOOM ON ZOOM DALLE STRUTTURE INDICI (GSBI_SSI)
    * --- Chiamato da GSBI_SSI
    * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      * --- Read from STR_INDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STR_INDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STR_INDI_idx,2],.t.,this.STR_INDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TITIPIND"+;
          " from "+i_cTable+" STR_INDI where ";
              +"TICODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TITIPIND;
          from (i_cTable) where;
              TICODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IBTIPIND = NVL(cp_ToDate(_read_.TITIPIND),cp_NullValue(_read_.TITIPIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      if i_curform=NULL
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_IBTIPIND = &cCurs..TITIPIND
    endif
    this.w_PROG = GSBI_MSI(this.w_IBTIPIND)
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_TICODICE = this.w_CODICE
        this.w_PROG.w_TITIPIND = this.w_IBTIPIND
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STR_INDI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
