* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bpe                                                        *
*              Calc. date periodi per tipo                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_43]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2000-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,CHGPAR,pORI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bpe",oParentObject,m.CHGPAR,m.pORI)
return(i_retval)

define class tgsbi_bpe as StdBatch
  * --- Local variables
  CHGPAR = space(10)
  pORI = space(1)
  w_DATA = ctod("  /  /  ")
  w_PERIODO = 0
  w_ANNO = 0
  w_LUNPER = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola date inizio fine periodo di elaborazione in funzione del tipo e del numero
    this.w_LUNPER = iif(this.oParentObject.w_PETIPPER="A",12,iif(this.oParentObject.w_PETIPPER="M",1,iif(this.oParentObject.w_PETIPPER="T",3,iif(this.oParentObject.w_PETIPPER="Q",4,iif(this.oParentObject.w_PETIPPER="S",6,0)))))
    do case
      case this.CHGPAR = "CHG1"
        this.oParentObject.w_PENUMPER = 0
      case this.CHGPAR = "CHG2"
        if (this.oParentObject.w_PENUMPER<>0 AND this.pORI="G") OR this.pORI="C"
          this.w_ANNO = Year(this.oParentObject.w_DATINI)+INT(((MONTH(this.oParentObject.w_DATINI)+(this.oParentObject.w_PENUMPER-1)*this.w_LUNPER)-1)/12)
          this.w_PERIODO = Mod(MONTH(this.oParentObject.w_DATINI)+(this.oParentObject.w_PENUMPER-1)*this.w_LUNPER,12)
          if this.w_PERIODO=0
            this.w_PERIODO = 12
            this.oParentObject.w_PEDATINI = cp_CharToDate("01/"+ALLTRIM(STR(this.w_PERIODO))+"/"+ALLTRIM(STR(this.w_ANNO)))
            this.w_ANNO = this.w_ANNO+1
            this.oParentObject.w_PEDATFIN = cp_CharToDate("01/"+ALLTRIM(STR(MOD(MONTH(this.oParentObject.w_PEDATINI)+this.w_LUNPER,12)))+"/"+ALLTRIM(STR(this.w_ANNO)))-1
          else
            this.oParentObject.w_PEDATINI = cp_CharToDate("01/"+ALLTRIM(STR(this.w_PERIODO))+"/"+ALLTRIM(STR(this.w_ANNO)))
            if MONTH(this.oParentObject.w_PEDATINI)+this.w_LUNPER>12
              this.oParentObject.w_PEDATFIN = cp_CharToDate("01/"+ALLTRIM(STR(MOD(MONTH(this.oParentObject.w_PEDATINI)+this.w_LUNPER,12)))+"/"+ALLTRIM(STR(this.w_ANNO+1)))-1
            else
              this.oParentObject.w_PEDATFIN = cp_CharToDate("01/"+ALLTRIM(STR(MONTH(this.oParentObject.w_PEDATINI)+this.w_LUNPER))+"/"+ALLTRIM(STR(this.w_ANNO)))-1
            endif
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,CHGPAR,pORI)
    this.CHGPAR=CHGPAR
    this.pORI=pORI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="CHGPAR,pORI"
endproc
