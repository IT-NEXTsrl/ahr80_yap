* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b06                                                        *
*              Elabora coge crediti/debiti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_36]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b06",oParentObject)
return(i_retval)

define class tgsbi_b06 as StdBatch
  * --- Local variables
  w_TIPCON = space(10)
  w_CODVAL = space(3)
  w_CODICE = space(15)
  w_CAOVAL = 0
  w_TOTIMP = 0
  w_RECODICE = space(15)
  w_DATCRD = ctod("  /  /  ")
  w_REDESCRI = space(45)
  w_CREBRE = space(15)
  w_APPO = space(15)
  w_CREMED = space(15)
  w_COD001 = space(15)
  w_DEBBRE = space(15)
  w_COD002 = space(15)
  w_DEBMED = space(15)
  w_COD003 = space(15)
  w_DATSCA = ctod("  /  /  ")
  w_IMPDAR = 0
  w_IMPAVE = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Manutenzione Crediti/Debiti COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTDC001
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_DATCRD = this.oParentObject.oParentObject.w_DATCRD
    this.w_CREBRE = this.oParentObject.oParentObject.w_CREBRE
    this.w_CREMED = this.oParentObject.oParentObject.w_CREMED
    this.w_DEBBRE = this.oParentObject.oParentObject.w_DEBBRE
    this.w_DEBMED = this.oParentObject.oParentObject.w_DEBMED
    if USED("EXTDC001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), TOTIMP N(18,4))
      SELECT EXTDC001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "CF" AND NVL(TOTIMP,0)<>0
      this.w_TIPCON = TIPCON
      this.w_TOTIMP = TOTIMP
      this.w_CODVAL = NVL(CODVAL, g_PERVAL)
      this.w_DATSCA = CP_TODATE(DATSCA)
      this.w_CAOVAL = NVL(CAOAPE, 1)
      * --- Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_CODVAL<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_CODVAL, i_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, 1, i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      if this.w_TIPCON="C"
        this.w_CODICE = IIF(this.w_DATSCA>this.w_DATCRD, this.w_CREMED, this.w_CREBRE)
      else
        this.w_CODICE = IIF(this.w_DATSCA>this.w_DATCRD, this.w_DEBMED, this.w_DEBBRE)
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      INSERT INTO TmpAgg1 (TIPCON, CODICE, TOTIMP) ;
      VALUES ("G", this.w_CODICE, this.w_TOTIMP)
      SELECT EXTDC001
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTDC001
      USE
      * --- Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("TmpAgg1")
        SELECT tmpAgg1
        if RECCOUNT()>0
          SELECT TIPCON, CODICE, SUM(TOTIMP) AS TOTALE FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY TIPCON, CODICE
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_TIPCON = TIPCON
          this.w_COD001 = IIF(this.w_TIPCON="M", CODICE, SPACE(15))
          this.w_COD002 = IIF(this.w_TIPCON="G", CODICE, SPACE(15))
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
