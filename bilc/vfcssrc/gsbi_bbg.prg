* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bbg                                                        *
*              Crea bilancio struttura contabile                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_303]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bbg",oParentObject)
return(i_retval)

define class tgsbi_bbg as StdBatch
  * --- Local variables
  w_OLDESE = space(4)
  w_OLDDIVISA = space(3)
  w_IMPORT41 = 0
  w_INIESE = ctod("  /  /  ")
  w_VALESE = space(3)
  w_IMPORT31 = 0
  w_GBNUMBIL = 0
  w_MESS = space(10)
  w_RIGA = 0
  w_ESE = space(4)
  Msg = space(90)
  w_INDIC = 0
  CodiceVoce = space(15)
  w_ERROR = .f.
  w_VALPER = 0
  w_OREC = 0
  w_CODICE = space(15)
  Func = space(0)
  TmpSave = space(0)
  TmpSave1 = space(0)
  w_VALORE = 0
  w_OPER = space(1)
  w_OCCUR = 0
  w_VALORES = space(19)
  w_VALPSGS = space(19)
  * --- WorkFile variables
  VALUTE_idx=0
  PER_ELAB_idx=0
  ESERCIZI_idx=0
  GESTBILA_idx=0
  DETTBILA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH STAMPA BILANCIO STRUTTURATO
    this.w_ESE = this.oParentObject.w_ESE
    this.w_ERROR = .f.
    * --- Mi calcolo il Bilancio in memoria e solo alla fine lo scrivo
     
 CREATE CURSOR CurBilancio (CODICE C(15), CPROWORD1 N(6), MRDESCRI C(90), RIGPERC N(6), ; 
 MRPERCEN N(6,2), MRIMPORT N(18,4), MR__INDE N(10), ; 
 MR__FTST C(1), MR__FONT C(1), MR__COLO N(10), MR__NUCO N(1), ; 
 MRCODVOC C(15), MR__NOTE M(10), VRFLDESC C(1)) 
 a = WRCURSOR("CurBilancio")
    * --- Cursore per la stampa degli errori durante l'elaborazione
     
 CREATE CURSOR CursError (CODICE C(15), CPROWORD1 N(6), DESCRI C(90)) 
 b = WRCURSOR("CursError")
    * --- Prendo valori predefiniti in periodo e li memorizzo in un array
     
 DIMENSION COST(10)
    * --- Select from PER_ELAB
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PER_ELAB ";
          +" where PECODICE = "+cp_ToStrODBC(this.oParentObject.w_PECODICE)+"";
           ,"_Curs_PER_ELAB")
    else
      select * from (i_cTable);
       where PECODICE = this.oParentObject.w_PECODICE;
        into cursor _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      select _Curs_PER_ELAB
      locate for 1=1
      do while not(eof())
      COST(1)=NVL(_Curs_PER_ELAB.PEVALC01,0)
      COST(2)=NVL(_Curs_PER_ELAB.PEVALC02,0)
      COST(3)=NVL(_Curs_PER_ELAB.PEVALC03,0)
      COST(4)=NVL(_Curs_PER_ELAB.PEVALC04,0)
      COST(5)=NVL(_Curs_PER_ELAB.PEVALC05,0)
      COST(6)=NVL(_Curs_PER_ELAB.PEVALC06,0)
      COST(7)=NVL(_Curs_PER_ELAB.PEVALC07,0)
      COST(8)=NVL(_Curs_PER_ELAB.PEVALC08,0)
      COST(9)=NVL(_Curs_PER_ELAB.PEVALC09,0)
      COST(10)=NVL(_Curs_PER_ELAB.PEVALC10,0)
        select _Curs_PER_ELAB
        continue
      enddo
      use
    endif
    * --- Query per trovare gli importi dei conti dei filtri della stuttura di bilancio selezionata
    ah_Msg("Fase 1: reperimento dati conti",.T.)
    vq_exec("..\bilc\exe\query\gsbi1bbg",this,"CursConti1")
    * --- Query per trovare gli importi dei mastri dei filtri della stuttura di bilancio selezionata
    ah_Msg("Fase 2: reperimento dati mastri",.T.)
    vq_exec("..\bilc\exe\query\gsbi2bbg",this,"CursMastri11")
    * --- Query per trovare il valore dei conti appartenenti al mastro movimentati
    vq_exec("..\bilc\exe\query\gsbi4bbg",this,"CursMastri21")
    * --- per il valore dei mastri faccio la union tra le due query precedenti
    * --- Per evitare errori di duplicazione conti/mastri in struttura
     
 SELECT * FROM CursConti1 INTO CURSOR CursConti GROUP BY CODICE 
 SELECT * FROM CursMastri11 INTO CURSOR CursMastri1 GROUP BY CODICE 
 SELECT * FROM CursMastri21 INTO CURSOR CursMastri2 GROUP BY CODICE
    if USED("CursConti1")
      SELECT CursConti1
      USE
    endif
    if USED("CursMastri11")
      SELECT CursMastri11
      USE
    endif
    if USED("CursMastri21")
      SELECT CursMastri21
      USE
    endif
    * --- Per errori sul cursore bisogna fare prima la count
     
 SELECT CursConti 
 COUNT TO INDICE 
 SELECT CursMastri1 
 COUNT TO INDICE 
 SELECT CursMastri2 
 COUNT TO INDICE 
 SELECT DISTINCT * FROM CursMastri1 INTO CURSOR CursMastri3 ; 
 UNION SELECT * FROM CursMastri2 NOFILTER
    * --- Per errori sul cursore bisogna fare prima la count
     
 SELECT CursMastri3 
 COUNT TO INDICE 
 SELECT CODVOC, CPROWN, CODICE, SUM(CursMastri3.IMPDAR) AS IMPDAR, SUM(CursMastri3.IMPAVE) AS IMPAVE ; 
 FROM CursMastri3 INTO CURSOR CursMastri ORDER BY CODICE ; 
 GROUP BY CODVOC, CPROWN, CODICE NOFILTER 
 WRCursMastri=WRCURSOR("CursMastri")
    if used("CursMastri1")
      select CursMastri1
      use
    endif
    if used("CursMastri2")
      select CursMastri2
      use
    endif
    if used("CursMastri3")
      select CursMastri3
      use
    endif
    SELECT * FROM (this.oParentObject.oParentObject.oParentObject.GSBI_MEG.cTrsName) ;
    INTO CURSOR CursDrivers
    * --- Query per reperire tutte le voci di bilancio nella struttura di bilancio da calcolare (con i dati per il calcolo)
    * --- l'ordine � in base alla sequenza di calcolo
    ah_Msg("Fase 3: reperimento dati voci",.T.)
    vq_exec("..\bilc\exe\query\gsbi3bbg",this,"CursVociT")
    * --- Elaboro le voci
    ah_Msg("Fase 4: calcolo valore voci",.T.)
    SELECT * FROM CursVociT INTO CURSOR CursSelez GROUP BY CPROWORD ORDER BY TRSEQELA
    SELECT CursSelez
    GO TOP
    SCAN
    this.CodiceVoce = CursSelez.CODVOC
    do case
      case CursSelez.VRFLDESC = "D"
        * --- Voce descrittiva
        this.w_VALORE = 0
      case CursSelez.VRFLDESC = "R"
        * --- Voce DRIVER
        SELECT * FROM CursDrivers INTO CURSOR PSG ;
        WHERE this.CodiceVoce = CursDrivers.t_MRCODVOC
        if used("PSG")
          this.w_VALORE = PSG.t_MRIMPORT
          SELECT PSG
          USE
        else
          this.w_VALORE = 0
        endif
      case .T.
        * --- Voce di Totalizzazione o Normale (va calcolato il valore)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if this.oParentObject.w_ARROTOND = "S"
      if this.oParentObject.DECIMI > 0
        this.w_VALORE = cp_ROUND(this.w_VALORE, 0)
      else
        this.w_VALORE = cp_ROUND(this.w_VALORE, -3)
      endif
    endif
    if this.w_VALORE>999999999999999999999999999999
      this.w_VALORE = 0
      this.Msg = Ah_MsgFormat("Numeric overflow")
      INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
      VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
      this.w_ERROR = .T.
    endif
    INSERT INTO CurBilancio (CODICE,CPROWORD1,MRDESCRI,RIGPERC,MRPERCEN, ;
    MRIMPORT,MR__INDE,MR__FTST,MR__FONT,MR__COLO,MR__NUCO, ;
    MRCODVOC, MR__NOTE,VRFLDESC) ;
    VALUES (nvl(CursSelez.CODVOC, space(15)),nvl(CursSelez.CPROWORD, 0),nvl(CursSelez.TRDESDET, space(90)), ;
    IIF(CursSelez.TRFLPERC="S",CursSelez.TRRIGPER,0),0,this.w_VALORE, ;
    nvl(CursSelez.TC__INDE, 0),nvl(CursSelez.TC__FTST, " "),nvl(CursSelez.TC__FONT, " "), ;
    nvl(CursSelez.TC__COLO, 0),IIF(CursSelez.TR__COL1="X",1, ;
    IIF(CursSelez.TR__COL2="X",2,IIF(CursSelez.TR__COL3="X",3,0))), ;
    nvl(CursSelez.CODVOC, space(15)),NVL(CursSelez.VR__NOTE," "),nvl(CursSelez.VRFLDESC, " "))
    SELECT CursSelez
    ENDSCAN
    ah_Msg("Fase 5: calcolo percentuali",.T.)
    if used("CursMastri")
      select CursMastri
      use
    endif
    if used("CursConti")
      select CursConti
      use
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Cursori
    if used("CursVociT")
      select CursVociT
      use
    endif
    if used("CursCosto")
      select CursCosto
      use
    endif
    if used("CursVoci")
      select CursVoci
      use
    endif
    if used("CursSelez")
      select CursSelez
      use
    endif
    if used("CursDrivers")
      select CursDrivers
      use
    endif
    * --- Calcolo Percentuali
    SELECT * FROM CurBilancio INTO CURSOR CurPercen WHERE RIGPERC <> 0
    * --- Ciclo per valorizzare le percentuali
    SELECT CurPercen
    GO TOP
    SCAN
    SELECT * FROM CurBilancio INTO CURSOR PSG ;
    WHERE CurBilancio.CPROWORD1 = CurPercen.RIGPERC
    if USED("PSG")
      if PSG.MRIMPORT = 0
        this.Msg = Ah_MsgFormat("Divisore su percentuale = 0 sulla riga %1",ALLTRIM(STR(CurBilancio.CPROWORD1)))
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (CurPercen.CODICE,CurPercen.CPROWORD1,this.Msg)
        this.w_ERROR = .T.
        this.w_VALPER = 999.99
      else
        this.w_VALPER = CurPercen.MRIMPORT / PSG.MRIMPORT * 100
      endif
      this.w_VALPER = cp_ROUND(this.w_VALPER,2)
      if ABS(this.w_VALPER) > 999.99
        this.Msg = Ah_MsgFormat("Valore percentuale troppo grande rispetto alla riga %1",ALLTRIM(STR(CurBilancio.CPROWORD1)))
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (CurPercen.CODICE,CurPercen.CPROWORD1,this.Msg)
        this.w_ERROR = .T.
        this.w_VALPER = 999.99
      endif
      UPDATE CurBilancio SET CurBilancio.MRPERCEN = this.w_VALPER ;
      WHERE CurPercen.CODICE = CurBilancio.CODICE AND ;
      CurPercen.CPROWORD1 = CurBilancio.CPROWORD1
      SELECT PSG
      USE
    endif
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ordino il cursore del bilancio per riga
    select * from CurBilancio into cursor TMP ORDER BY CPROWORD1
    if used("CurBilancio")
      select CurBilancio
      use
    endif
    select * from TMP into cursor CurBilancio ORDER BY CPROWORD1
    * --- Chiusura Cursori
    if used("CurPercen")
      select CurPercen
      use
    endif
    if used("TMP")
      select TMP
      use
    endif
    * --- Richiedo se stampare errori e produco la stampa
    if this.w_ERROR
      * --- Avviso - Si sono verificati degli errori (lacia la stampa di verifica)
      if ah_YesNo("Si vuole stampare la lista delle segnalazioni?%0")
        L_GBNUMBIL = this.oParentObject.w_GBNUMBIL
        L_ESE = this.oParentObject.w_ESE
        SELECT * FROM CursError INTO CURSOR __TMP__ ORDER BY CPROWORD1
        CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BBG", " ", this)
      endif
    endif
    if used("CursError")
      select CursError
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valore Voce
    this.Func = "("
    SELECT * FROM CursVociT INTO CURSOR CursVoci ;
    WHERE CursVociT.CODVOC=this.CodiceVoce ;
    GROUP BY CPROWNUM
    * --- Continuo a Ciclare su CursVoci per Codice identico
    SELECT CursVoci
    SCAN FOR (CursVoci.CODVOC = this.CodiceVoce AND CursVoci.CPROWORD<>0 AND ;
    (NOT EMPTY(NVL(ALLTRIM(DVCODCON),"")) OR NOT EMPTY(NVL(ALLTRIM(DVCODVOC),"")) ;
    OR NOT EMPTY(NVL(ALLTRIM(DVCODMAS),"")))) OR Empty(Nvl(DVFLMACO," "))
    this.w_CODICE = SPACE(15)
    * --- Valorizzo il codice
    do case
      case DVFLMACO = "G"
        this.w_CODICE = DVCODCON
        SELECT CursConti.IMPDAR, CursConti.IMPAVE FROM CursConti ;
        INTO CURSOR PSG WHERE CursConti.CODICE = this.w_CODICE
        SELECT PSG
        if RECCOUNT("PSG")>0
          this.w_VALORE = PSG.IMPDAR - PSG.IMPAVE
        else
          this.Msg = Ah_MsgFormat("Conto %1 non valorizzato", ALLTRIM(this.w_CODICE))
          INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
          VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
          this.w_ERROR = .T.
          this.w_VALORE = 0
        endif
      case DVFLMACO = "M"
        this.w_CODICE = DVCODMAS
        SELECT CursMastri.IMPDAR, CursMastri.IMPAVE FROM CursMastri ;
        INTO CURSOR PSG WHERE CursMastri.CODICE = this.w_CODICE
        SELECT PSG
        if RECCOUNT("PSG")>0
          this.w_VALORE = PSG.IMPDAR - PSG.IMPAVE
        else
          this.Msg = Ah_MsgFormat("Mastro %1 non valorizzato",ALLTRIM(this.w_CODICE))
          INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
          VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
          this.w_ERROR = .T.
          this.w_VALORE = 0
        endif
      case DVFLMACO = "V"
        this.w_CODICE = DVCODVOC
        SELECT * FROM CursDrivers INTO CURSOR PSG ;
        WHERE this.w_CODICE = CursDrivers.t_MRCODVOC
        SELECT PSG
        if RECCOUNT("PSG")>0
          this.w_VALORE = PSG.t_MRIMPORT
        else
          SELECT CurBilancio.MRIMPORT FROM CurBilancio ;
          INTO CURSOR PSG WHERE CurBilancio.CODICE = this.w_CODICE
          SELECT PSG
          if RECCOUNT("PSG")>0
            this.w_VALORE = PSG.MRIMPORT
          else
            this.Msg = Ah_MsgFormat("Voce %1 non ancora calcolata",ALLTRIM(this.w_CODICE))
            INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
            VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
            this.w_ERROR = .T.
            this.w_VALORE = 0
          endif
        endif
      otherwise
        this.w_VALORE = 0
    endcase
    if USED("PSG")
      SELECT PSG
      USE
    endif
    SELECT CursVoci
    this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLOPER,""))
    if ALLTRIM(DVFLOPER)="-" OR ALLTRIM(DVFLOPER)="+"
      this.Func = ALLTRIM(this.Func) + "1*"
    endif
    if DVFLPARE = "(" OR DVFLPARE = "(("
      * --- Inserisco parentesi tonde aperte all'inizio se ci sono '(' '(('
      this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLPARE,""))
    endif
    this.w_VALPSGS = STR(this.w_VALORE,18,4)
    this.w_VALORES = LEFT(this.w_VALPSGS,13) + "." + RIGHT(this.w_VALPSGS,4)
    if NOT EMPTY(Nvl(DVCONDIZ,""))
      * --- Se esiste la condizione la inserisco
      this.Func = ALLTRIM(this.Func) + "IIF(" + ALLTRIM(this.w_VALORES) + ALLTRIM(Nvl(DVCONDIZ,"")) + ","
    endif
    * --- nel then metto la formula
    this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_VALORES) + ALLTRIM(Nvl(DVFORMUL,""))
    if NOT EMPTY(Nvl(DVCONDIZ," "))
      * --- chiudo else se sono sotto condizione
      this.Func = ALLTRIM(this.Func) + "," + "0" + ")"
    endif
    if DVFLPARE = ")" OR DVFLPARE = "))"
      * --- Chiudo le parentesi tonde
      this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLPARE,""))
    endif
    ENDSCAN
    if NOT EMPTY(ALLTRIM(Nvl(CursSelez.VRFORMUL,"")))
      this.Func = ALLTRIM(this.Func) + ")"
      if this.Func = "()"
        this.Func = ""
      endif
      this.Func = ALLTRIM(this.Func) + ALLTRIM(CursSelez.VRFLOPER)
      if ALLTRIM(DVFLOPER)="-" OR ALLTRIM(DVFLOPER)="+"
        this.Func = ALLTRIM(this.Func) + "1*"
      endif
      this.TmpSave = this.Func
      this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(CursSelez.VRFORMUL,""))
    else
      this.Func = ALLTRIM(this.Func) + ")"
    endif
    Messaggio = "Corretta"
    ON ERROR Messaggio = "Errata"
    TmpVal = this.Func
    * --- Valutazione dell'errore
    this.w_VALORE = &TmpVal
    ON ERROR
    if Messaggio = "Errata"
      * --- Controllo se non ho delle variabili di periodo su VRFORMUL
      this.Func = ALLTRIM(this.TmpSave)
      this.TmpSave = CursSelez.VRFORMUL
      this.w_OCCUR = RATC("C",this.TmpSave)
      do while this.w_OCCUR <> 0
        this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
        this.w_INDIC = VAL(SUBSTR(this.TmpSave,this.w_OCCUR+1,2))
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(STR(COST(this.w_INDIC),18,4))
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+3))
        this.TmpSave = ALLTRIM(this.TmpSave1)
        this.w_OCCUR = RATC("C",this.TmpSave)
      enddo
      this.Func = ALLTRIM(this.Func) + ALLTRIM(this.TmpSave)
      Messaggio = "Corretta"
      ON ERROR Messaggio = "Errata"
      TmpVal = this.Func
      * --- Valutazione dell'errore
      APPO=STRTRAN(TMPVAL, ",", ".")
      this.w_VALORE = &APPO
      ON ERROR
      if Messaggio = "Errata"
        this.Msg = Ah_MsgFormat("Funzione calcolata Errata")
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (nvl(CursSelez.CODVOC, space(15)),nvl(CursSelez.CPROWORD, 0),this.Msg)
        this.w_ERROR = .T.
      endif
    endif
    if Messaggio = "Errata"
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PER_ELAB'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='GESTBILA'
    this.cWorkTables[5]='DETTBILA'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_PER_ELAB')
      use in _Curs_PER_ELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
