* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bgp                                                        *
*              Gen. periodi elaborazione                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_26]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2000-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bgp",oParentObject)
return(i_retval)

define class tgsbi_bgp as StdBatch
  * --- Local variables
  w_PECODNEW = space(15)
  w_PEDATINI = ctod("  /  /  ")
  w_PEDESNEW = space(35)
  w_PENUMPER = 0
  w_PEDATFIN = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PETIPPER = space(1)
  w_NUMPER = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera periodi per tipo (da GSBI_KGP)
    this.w_DATINI = this.oParentObject.w_INIESE
    this.w_DATFIN = this.oParentObject.w_FINESE
    this.w_PETIPPER = this.oParentObject.w_PETIPPER
    this.w_NUMPER = iif(this.w_PETIPPER="G",0,iif(this.w_PETIPPER="M",INT((this.w_DATFIN-this.w_DATINI)/30),iif(this.w_PETIPPER="T",INT((this.w_DATFIN-this.w_DATINI)/90),iif(this.w_PETIPPER="Q",INT((this.w_DATFIN-this.w_DATINI)/120),iif(this.w_PETIPPER="S",INT((this.w_DATFIN-this.w_DATINI)/180),INT((this.w_DATFIN-this.w_DATINI)/360))))))
    this.w_PENUMPER = 1
    * --- Try
    local bErr_036ECE50
    bErr_036ECE50=bTrsErr
    this.Try_036ECE50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Impossibile generare: codice gi� esistente","","")
    endif
    bTrsErr=bTrsErr or bErr_036ECE50
    * --- End
  endproc
  proc Try_036ECE50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do while this.w_PENUMPER <= this.w_NUMPER
      GSBI_BPE(this,"CHG2","C")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PECODNEW = ALLTRIM(this.oParentObject.w_PECODICE) + " " +ALLTRIM(STR(this.w_PENUMPER))
      this.w_PEDESNEW = ALLTRIM(this.oParentObject.w_PEDESCRI)+" ("+ALLTRIM(STR(this.w_PENUMPER))+")"
      * --- Insert into PER_ELAB
      i_nConn=i_TableProp[this.PER_ELAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PER_ELAB_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PECODICE"+",PEDESCRI"+",PEDATINI"+",PEDATFIN"+",PECODESE"+",PEVALC01"+",PEVALC02"+",PEVALC03"+",PEVALC04"+",PEVALC05"+",PEVALC06"+",PEVALC07"+",PEVALC08"+",PEVALC09"+",PEVALC10"+",PEDTINVA"+",PETIPPER"+",PENUMPER"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PECODNEW),'PER_ELAB','PECODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PEDESNEW),'PER_ELAB','PEDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PEDATINI),'PER_ELAB','PEDATINI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PEDATFIN),'PER_ELAB','PEDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PECODESE),'PER_ELAB','PECODESE');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC01');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC02');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC03');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC04');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC05');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC06');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC07');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC08');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC09');
        +","+cp_NullLink(cp_ToStrODBC(0),'PER_ELAB','PEVALC10');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DTINVA),'PER_ELAB','PEDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PETIPPER),'PER_ELAB','PETIPPER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PENUMPER),'PER_ELAB','PENUMPER');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PECODICE',this.w_PECODNEW,'PEDESCRI',this.w_PEDESNEW,'PEDATINI',this.w_PEDATINI,'PEDATFIN',this.w_PEDATFIN,'PECODESE',this.oParentObject.w_PECODESE,'PEVALC01',0,'PEVALC02',0,'PEVALC03',0,'PEVALC04',0,'PEVALC05',0,'PEVALC06',0,'PEVALC07',0)
        insert into (i_cTable) (PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEVALC01,PEVALC02,PEVALC03,PEVALC04,PEVALC05,PEVALC06,PEVALC07,PEVALC08,PEVALC09,PEVALC10,PEDTINVA,PETIPPER,PENUMPER &i_ccchkf. );
           values (;
             this.w_PECODNEW;
             ,this.w_PEDESNEW;
             ,this.w_PEDATINI;
             ,this.w_PEDATFIN;
             ,this.oParentObject.w_PECODESE;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,this.oParentObject.w_DTINVA;
             ,this.w_PETIPPER;
             ,this.w_PENUMPER;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_PENUMPER = this.w_PENUMPER+1
    enddo
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione periodi terminata","","")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='PER_ELAB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
