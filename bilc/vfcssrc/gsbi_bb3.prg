* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bb3                                                        *
*              Calcolo bilancio contabile                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_40]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2009-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bb3",oParentObject)
return(i_retval)

define class tgsbi_bb3 as StdBatch
  * --- Local variables
  w_DESCRI = space(40)
  w_PEDATINI = ctod("  /  /  ")
  w_PEDATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  STR_ANAC_idx=0
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciata dalla stampa Bilancio Contabile (GSBI_SBI)
    * --- In questo batch vengono effettuate tutte le operazioni di cambio.
    * --- Eseguo la query dove leggo i dati del bilancio che ho deciso di stampare.
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_SBI",this,"BILANCIO")
    * --- Variabili da passare al Report, Cambio, Divisa, Formato ,Descrizioni
    * --- il cambio viene calcolato in modo diverso a seconda se i_DATSYS e minore di GETVALUT(g_PERVAL, 'VADATEUR')
    * --- se maggiore ho l'euro e due cambi (in caso di valuta intra euroepa)
    * --- q cambio che divide, L_molti cambio che moltiplica (1 se prima di GETVALUT(g_PERVAL, 'VADATEUR'))
    if this.oParentObject.w_VALAPP=this.oParentObject.w_DIVISA
      L_MOLTI=1
      L_CAMVAL=1
    else
      L_MOLTI=iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)
      L_CAMVAL=GETCAM(this.oParentObject.w_VALAPP,i_DATSYS,0)
    endif
    * --- Formato importi
    L_DECIMI=this.oParentObject.w_DECIMI
    s=20*(l_decimi+2)
    L_NUMERO=this.oParentObject.w_NUMERO
    L_ESERCI=this.oParentObject.w_ESERCI
    L_DATABIL=this.oParentObject.w_DATABIL
    L_DESCRIBI=this.oParentObject.w_DESCRIBI
    * --- Read from STR_ANAC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STR_ANAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STR_ANAC_idx,2],.t.,this.STR_ANAC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TRDESCRI"+;
        " from "+i_cTable+" STR_ANAC where ";
            +"TRCODICE = "+cp_ToStrODBC(this.oParentObject.w_STRUTTUR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TRDESCRI;
        from (i_cTable) where;
            TRCODICE = this.oParentObject.w_STRUTTUR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESCRI = NVL(cp_ToDate(_read_.TRDESCRI),cp_NullValue(_read_.TRDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PER_ELAB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PEDATINI,PEDATFIN"+;
        " from "+i_cTable+" PER_ELAB where ";
            +"PECODICE = "+cp_ToStrODBC(this.oParentObject.w_PERIODO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PEDATINI,PEDATFIN;
        from (i_cTable) where;
            PECODICE = this.oParentObject.w_PERIODO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PEDATINI = NVL(cp_ToDate(_read_.PEDATINI),cp_NullValue(_read_.PEDATINI))
      this.w_PEDATFIN = NVL(cp_ToDate(_read_.PEDATFIN),cp_NullValue(_read_.PEDATFIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_DESCRI=this.w_DESCRI
    L_STRUTTUR=this.oParentObject.w_STRUTTUR
    L_PERIODO=this.oParentObject.w_PERIODO
    L_DESCPER=this.oParentObject.w_DESCPER
    L_PEDATINI=this.w_PEDATINI
    L_PEDATFIN=this.w_PEDATFIN
    if this.oParentObject.w_RIGHESI="S"
      SELECT * FROM BILANCIO WHERE MRIMPORT<>0 OR VRFLDESC="D";
       INTO CURSOR __TMP__ ORDER BY CPROWORD
    else
      SELECT * FROM BILANCIO INTO CURSOR __TMP__ ORDER BY CPROWORD
    endif
    * --- Controllo il tipo di stampa selezionata
    if this.oParentObject.w_TIPOSTAM="S"
      * --- Stampa Standard
      * --- Lancio il report associato
      CP_CHPRN("..\BILC\EXE\QUERY\GSBI_SBI.FRX", " ", this)
    else
      * --- Su EXCEL - Creo il cursore da quello di stampa
      select CPROWORD AS CPROWORD ,IIF(MR__INDE>0,"_"+SPACE(MR__INDE-1),"")+MRDESCRI as MRDESCRI,VRTIPDRI as VRTIPDRI,;
      IIF(VRTIPDRI="M", MON2VAL(MRIMPORT,L_MOLTI,1,i_DATSYS,g_PERVAL,this.oParentObject.w_Decimi), cp_round(mrimport, this.oParentObject.w_decimi)) AS MRIMPORT,;
      MRPERCEN AS MRPERCEN,MR__FTST AS MR__FTST, MR__FONT AS MR__FONT,;
      MR__COLO AS MR__COLO, MR__NUCO AS MR__NUCO, "CONTABILE" as TIPO FROM __TMP__ INTO CURSOR __TMP__ ORDER BY 1
      * --- Lancio la stampa su Excel
      CP_CHPRN("..\BILC\EXE\QUERY\GSBIESBI.XLT", " ", this)
    endif
    if USED("BILANCIO")
      SELECT BILANCIO
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STR_ANAC'
    this.cWorkTables[2]='PER_ELAB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
