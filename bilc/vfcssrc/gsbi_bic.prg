* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bic                                                        *
*              Stampa/vis. confronto indici                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_89]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-17                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bic",oParentObject,m.pOper)
return(i_retval)

define class tgsbi_bic as StdBatch
  * --- Local variables
  pOper = space(4)
  w_ZOOM = space(10)
  w_EXCEL = space(1)
  w_TIPINDI = space(1)
  w_NUMERO = 0
  w_ESERCI = space(4)
  NoName = space(10)
  w_DESCRI = space(40)
  w_IBFORMUL = space(100)
  w_VALORE = 0
  Msg = space(0)
  PERIODO = space(15)
  w_TIPINDI = space(1)
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_DIFFE = 0
  Func = space(0)
  CODVOC = space(15)
  INDICE = 0
  w_VALORES = space(19)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_NUMERO = 0
  w_ESERCI = space(4)
  NoName = space(10)
  w_DESCRI = space(40)
  w_IBFORMUL = space(100)
  w_VALORE = 0
  Msg = space(0)
  PERIODO = space(15)
  w_VALOTT = 0
  w_TIPINDI = space(1)
  * --- WorkFile variables
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Maschera Stampa Confronto Indici
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    this.w_EXCEL = "N"
    Messaggio = "Corretta"
    do case
      case this.pOper="SELE"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "CARICA"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="ELAB"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "GRAF"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper = "QUIT"
        if used("GSBI_KIC")
          select GSBI_KIC
          use
        endif
      case this.pOper = "GRXL"
        this.w_EXCEL = "S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/Deseleziona Tutto
    NC = this.w_ZOOM.cCursor
    if this.oParentObject.w_SELEZI="S"
      UPDATE &NC SET XCHK = 1
      this.oParentObject.w_FLSELE = 1
    else
      UPDATE &NC SET XCHK = 0
      this.oParentObject.w_FLSELE = 0
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Zoom
    ah_Msg("Elaborazione dati...",.T.)
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_BIC.VQR",this,"CursElab")
    * --- Cancello il contenuto dello zoom
    DELETE FROM ( this.w_ZOOM.cCursor )
    * --- Passo i valori allo ZOOM
    * --- Passo per un Array (Fox non accetta istruzioni del tipo Insert into from select ...)
    SELECT * FROM CursElab ;
    INTO ARRAY VETTORE ORDER BY GBBILESE,GBNUMBIL
    SELECT CursElab
    USE
    * --- Se il cursore � vuoto non crea il vettore
    if TYPE("VETTORE")<>"U"
      insert into ( this.w_ZOOM.cCursor ) from array vettore
    endif
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom.grd.refresh
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
      This.w_Zoom.grd.Column&NC..Enabled=.f.
    endif
    ENDFOR
    if used("CursElab")
      SELECT CursElab
      USE
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Stampa
    * --- Molte parti del codice riprendono GSBI_BCI
    this.w_TIPINDI = this.oParentObject.w_TIPOBIL
    DIMENSION COST(10)
    * --- Mi carico su un cursore la struttura dell'indice per i calcoli
    ah_Msg("Fase 2: reperimento dati struttura indici",.T.)
    vq_exec("..\bilc\exe\query\gsbi1bci",this,"CursIndi")
    * --- Cursore per Stampa
    CREATE CURSOR CursStampa (CODICE C(15), CPROWORD1 N(6), IMPORT N(18,4), MSGERROR M(10), ;
    NUMBIL N(6), CODESE C(4), DESCRI C(40))
    a = WRCURSOR("CursStampa")
    NC = this.w_ZOOM.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_NUMERO = GBNUMBIL
    this.w_ESERCI = GBBILESE
    * --- Mi carico su un cursore il bilancio
    ah_Msg("Fase 2: reperimento dati bilancio",.T.)
    vq_exec("..\bilc\exe\query\gsbi_bci",this,"CursBilc")
    vq_exec("..\bilc\exe\query\gsbi3bci",this,"CursDriver")
    SELECT CursBilc
    GO TOP
    this.PERIODO = GBPERIOD
    this.w_DESCRI = GBDESCRI
    * --- Prendo valori predefiniti in periodo e li memorizzo in un array
    * --- Select from PER_ELAB
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PER_ELAB ";
          +" where PECODICE = "+cp_ToStrODBC(this.PERIODO)+"";
           ,"_Curs_PER_ELAB")
    else
      select * from (i_cTable);
       where PECODICE = this.PERIODO;
        into cursor _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      select _Curs_PER_ELAB
      locate for 1=1
      do while not(eof())
      COST(1)=NVL(_Curs_PER_ELAB.PEVALC01,0)
      COST(2)=NVL(_Curs_PER_ELAB.PEVALC02,0)
      COST(3)=NVL(_Curs_PER_ELAB.PEVALC03,0)
      COST(4)=NVL(_Curs_PER_ELAB.PEVALC04,0)
      COST(5)=NVL(_Curs_PER_ELAB.PEVALC05,0)
      COST(6)=NVL(_Curs_PER_ELAB.PEVALC06,0)
      COST(7)=NVL(_Curs_PER_ELAB.PEVALC07,0)
      COST(8)=NVL(_Curs_PER_ELAB.PEVALC08,0)
      COST(9)=NVL(_Curs_PER_ELAB.PEVALC09,0)
      COST(10)=NVL(_Curs_PER_ELAB.PEVALC10,0)
        select _Curs_PER_ELAB
        continue
      enddo
      use
    endif
    ah_Msg("Fase 3: calcolo valori indici",.T.)
    SELECT CursIndi
    GO TOP
    SCAN
    this.w_TIPINDI = CursIndi.IBTIPIND
    this.w_IBFORMUL = CursIndi.IBFORMUL
    this.Msg = ""
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Msg = this.w_oMess.ComposeMessage()
    INSERT INTO CursStampa (CODICE,CPROWORD1,IMPORT,MSGERROR, ;
    NUMBIL, CODESE, DESCRI) ;
    VALUES (CursIndi.TICODIND,CursIndi.CPROWORD,this.w_VALORE,this.Msg, ;
    this.w_NUMERO,this.w_ESERCI,this.w_DESCRI)
    ENDSCAN
    if used("CursBilc")
      select CursBilc
      use
    endif
    if used("CursDriver")
      select CursDriver
      use
    endif
    ENDSCAN
    SELECT CursIndi.*, CursStampa.* FROM ;
    (CursIndi Left Outer Join CursStampa on CursIndi.TICODIND=CursStampa.CODICE ;
    AND CursIndi.CPROWORD=CursStampa.CPROWORD1) ;
    INTO CURSOR __TMP__ ORDER BY CursIndi.CPROWORD,CursStampa.CODESE,CursStampa.NUMBIL
    L_STAFORM = this.oParentObject.w_STAFORM
    L_STADESA = this.oParentObject.w_STADESAG
    * --- Stampa
    CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BIC.FRX", " ", this)
    if used("CursIndi")
      select CursIndi
      use
    endif
    if used("CursStampa")
      select CursStampa
      use
    endif
    if used("CursIndi")
      select CursIndi
      use
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valore Indice
    * --- Molte parti del codice riprendono GSBI_BCI
    this.w_oMess=createobject("Ah_Message")
    Messaggio = "Corretta"
    this.TmpSave = ALLTRIM(this.w_IBFORMUL)
    * --- Ricerco il segno '<' il quale indica che ho inserito una costante
    this.w_OCCUR = RATC("<",this.TmpSave)
    this.w_OCCUR1 = RATC(">",this.TmpSave)
    this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    do while this.w_OCCUR <> 0
      * --- Ciclo sulla stringa per sostituire con i valori del bilancio
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      * --- Trovo il valore della Voce
      this.CODVOC = ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+1,this.w_DIFFE-1))
      SELECT * FROM CursBilc INTO CURSOR PSG WHERE ;
      (NOT EMPTY(CursBilc.MRCODVOC) AND CursBilc.MRCODVOC=this.CODVOC) ;
      OR (NOT EMPTY(CursBilc.MRCODVOA) AND CursBilc.MRCODVOA=this.CODVOC)
      if RECCOUNT("PSG")>0
        this.w_VALORE = PSG.MRIMPORT
      else
        if INLIST(this.CODVOC,"C01","C02","C03","C04","C05","C06","C07","C08","C09","C10")
          this.w_VALORE = COST(VAL(SUBSTR(this.CODVOC,2,2)))
        else
          if this.w_TIPINDI = "G"
            SELECT * FROM CursDriver INTO CURSOR PSG WHERE MRCODVOC=this.CODVOC
          else
            SELECT * FROM CursDriver INTO CURSOR PSG WHERE MRCODVOA=this.CODVOC
          endif
          if RECCOUNT("PSG")>0
            this.w_VALORE = PSG.MRIMPORT
          else
            this.w_VALORE = 0
            this.w_oPart = this.w_oMess.AddMsgPart("Codice voce < %1 > non presente nel bilancio selezionato.")
            this.w_oPart.AddParam(ALLTRIM(this.CODVOC))     
          endif
        endif
      endif
      if used("PSG")
        SELECT PSG
        USE
      endif
      this.w_VALORES = STR(this.w_VALORE,18,4)
      * --- Passaggio per non perdere i valori dopo la virgola
      this.w_VALORES = LEFT(this.w_VALORES,13) + "." + RIGHT(this.w_VALORES,4)
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(this.w_VALORES)
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("<",this.TmpSave)
      this.w_OCCUR1 = RATC(">",this.TmpSave)
      this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    enddo
    this.FuncCalc =  ALLTRIM(this.TmpSave)
    ON ERROR Messaggio = "Errata"
    TmpVal = this.FuncCalc
    * --- Valutazione dell'errore
    this.w_VALORE = &TmpVal
    ON ERROR
    if Messaggio = "Errata"
      this.w_VALORE = 0
      this.w_oMess.AddMsgPart("Errore nella funzione calcolata.")     
    endif
    * --- Filtra eventuali divisioni per 0 (********)
    this.w_VALORE = IIF(AT("*", ALLTR(STR(this.w_VALORE)))<>0, 0, this.w_VALORE)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Grafico
    * --- Molte parti del codice riprendono GSBI_BCI
    DIMENSION COST(10)
    this.w_TIPINDI = this.oParentObject.w_TIPOBIL
    if used("GSBI_KIC")
      select GSBI_KIC
      use
    endif
    * --- Mi carico su un cursore la struttura dell'indice per i calcoli
    ah_Msg("Fase 2: reperimento dati struttura indici",.T.)
    vq_exec("..\bilc\exe\query\gsbi2bci",this,"CursIndi")
    * --- Cursore per Grafico
    CREATE CURSOR CursStampa (NUMERO C(11), IMPORT N(18,4),VALOTT N(18,4))
    Z = WRCURSOR("CursStampa")
    NC = this.w_ZOOM.cCursor
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK<>0
    this.w_TIPINDI = CursIndi.IBTIPIND
    this.w_NUMERO = GBNUMBIL
    this.w_ESERCI = GBBILESE
    * --- Mi carico su un cursore il bilancio
    ah_Msg("Fase 2: reperimento dati bilancio",.T.)
    vq_exec("..\bilc\exe\query\gsbi_bci",this,"CursBilc")
    vq_exec("..\bilc\exe\query\gsbi3bci",this,"CursDriver")
    SELECT CursBilc
    GO TOP
    this.PERIODO = GBPERIOD
    * --- Prendo valori predefiniti in periodo e li memorizzo in un array
    * --- Select from PER_ELAB
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PER_ELAB ";
          +" where PECODICE = "+cp_ToStrODBC(this.PERIODO)+"";
           ,"_Curs_PER_ELAB")
    else
      select * from (i_cTable);
       where PECODICE = this.PERIODO;
        into cursor _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      select _Curs_PER_ELAB
      locate for 1=1
      do while not(eof())
      COST(1)=NVL(_Curs_PER_ELAB.PEVALC01,0)
      COST(2)=NVL(_Curs_PER_ELAB.PEVALC02,0)
      COST(3)=NVL(_Curs_PER_ELAB.PEVALC03,0)
      COST(4)=NVL(_Curs_PER_ELAB.PEVALC04,0)
      COST(5)=NVL(_Curs_PER_ELAB.PEVALC05,0)
      COST(6)=NVL(_Curs_PER_ELAB.PEVALC06,0)
      COST(7)=NVL(_Curs_PER_ELAB.PEVALC07,0)
      COST(8)=NVL(_Curs_PER_ELAB.PEVALC08,0)
      COST(9)=NVL(_Curs_PER_ELAB.PEVALC09,0)
      COST(10)=NVL(_Curs_PER_ELAB.PEVALC10,0)
        select _Curs_PER_ELAB
        continue
      enddo
      use
    endif
    ah_Msg("Fase 3: calcolo valori indice",.T.)
    SELECT CursIndi
    GO TOP
    SCAN
    this.w_IBFORMUL = CursIndi.IBFORMUL
    this.w_VALOTT = CursIndi.IBVALOTT
    this.Msg = ""
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    INSERT INTO CursStampa (NUMERO, IMPORT, VALOTT) ;
    VALUES (ALLTRIM(this.PERIODO)+"/"+ALLTRIM(STR(this.w_NUMERO)), ;
    this.w_VALORE,this.w_VALOTT)
    ENDSCAN
    if used("CursBilc")
      select CursBilc
      use
    endif
    if used("CursDriver")
      select CursDriver
      use
    endif
    ENDSCAN
    SELECT * FROM CursStampa ;
    INTO CURSOR GSBI_KIC ORDER BY CursStampa.NUMERO
    * --- Stampa su excel
    if this.w_EXCEL = "S"
      select * from CursStampa into cursor __TMP__ NOFILTER
      select __TMP__
      CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BCI.XLT", " ", this)
    endif
    if used("CursIndi")
      select CursIndi
      use
    endif
    if used("CursStampa")
      select CursStampa
      use
    endif
    this.oParentObject.w_QUERY = NOT this.oParentObject.w_QUERY
    this.oParentobject.oPgFrm.ActivePage=2
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PER_ELAB'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PER_ELAB')
      use in _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      use in _Curs_PER_ELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
