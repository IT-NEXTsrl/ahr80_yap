* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sib                                                        *
*              Stampa definizione indici                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sib",oParentObject))

* --- Class definition
define class tgsbi_sib as StdForm
  Top    = 50
  Left   = 99

  * --- Standard Properties
  Width  = 497
  Height = 159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=258606697
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  IND_BILA_IDX = 0
  cPrg = "gsbi_sib"
  cComment = "Stampa definizione indici"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE1 = space(15)
  w_DESCRI1 = space(50)
  w_CODICE2 = space(15)
  w_DESCRI2 = space(35)
  w_TITIPIND = space(1)
  w_TIPIND1 = space(1)
  w_TIPIND2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsbi_sib
  pTIPOCONT=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSBI_SIB'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sibPag1","gsbi_sib",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='IND_BILA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsbi_sib
    this.pTIPOCONT=this.oParentObject
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE1=space(15)
      .w_DESCRI1=space(50)
      .w_CODICE2=space(15)
      .w_DESCRI2=space(35)
      .w_TITIPIND=space(1)
      .w_TIPIND1=space(1)
      .w_TIPIND2=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODICE1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_CODICE2))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_TITIPIND = .pTIPOCONT
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'IND_BILA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IBCODICE like "+cp_ToStrODBC(trim(this.w_CODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IBCODICE',trim(this.w_CODICE1))
          select IBCODICE,IBDESCRI,IBTIPIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE1)==trim(_Link_.IBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE1) and !this.bDontReportError
            deferred_cp_zoom('IND_BILA','*','IBCODICE',cp_AbsName(oSource.parent,'oCODICE1_1_1'),i_cWhere,'',"Indici di bilancio",'GSBI2AIB.IND_BILA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                     +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',oSource.xKey(1))
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODICE1)
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE1 = NVL(_Link_.IBCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.IBDESCRI,space(50))
      this.w_TIPIND1 = NVL(_Link_.IBTIPIND,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE1 = space(15)
      endif
      this.w_DESCRI1 = space(50)
      this.w_TIPIND1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODICE2) OR (.w_CODICE1<=.w_CODICE2)) AND .w_TITIPIND=.w_TIPIND1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endif
        this.w_CODICE1 = space(15)
        this.w_DESCRI1 = space(50)
        this.w_TIPIND1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE2
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'IND_BILA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IBCODICE like "+cp_ToStrODBC(trim(this.w_CODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IBCODICE',trim(this.w_CODICE2))
          select IBCODICE,IBDESCRI,IBTIPIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE2)==trim(_Link_.IBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE2) and !this.bDontReportError
            deferred_cp_zoom('IND_BILA','*','IBCODICE',cp_AbsName(oSource.parent,'oCODICE2_1_3'),i_cWhere,'',"Indici di bilancio",'GSBI2AIB.IND_BILA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                     +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',oSource.xKey(1))
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODICE2)
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE2 = NVL(_Link_.IBCODICE,space(15))
      this.w_DESCRI2 = NVL(_Link_.IBDESCRI,space(35))
      this.w_TIPIND2 = NVL(_Link_.IBTIPIND,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE2 = space(15)
      endif
      this.w_DESCRI2 = space(35)
      this.w_TIPIND2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_CODICE1) or (.w_CODICE2>=.w_CODICE1)) AND .w_TITIPIND=.w_TIPIND2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endif
        this.w_CODICE2 = space(15)
        this.w_DESCRI2 = space(35)
        this.w_TIPIND2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE1_1_1.value==this.w_CODICE1)
      this.oPgFrm.Page1.oPag.oCODICE1_1_1.value=this.w_CODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_2.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_2.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE2_1_3.value==this.w_CODICE2)
      this.oPgFrm.Page1.oPag.oCODICE2_1_3.value=this.w_CODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_4.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_4.value=this.w_DESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_CODICE2) OR (.w_CODICE1<=.w_CODICE2)) AND .w_TITIPIND=.w_TIPIND1)  and not(empty(.w_CODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE1_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
          case   not((empty(.w_CODICE1) or (.w_CODICE2>=.w_CODICE1)) AND .w_TITIPIND=.w_TIPIND2)  and not(empty(.w_CODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE2_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_sibPag1 as StdContainer
  Width  = 493
  height = 159
  stdWidth  = 493
  stdheight = 159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE1_1_1 as StdField with uid="KMZWVCGRZH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODICE1", cQueryName = "CODICE1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato",;
    ToolTipText = "Codice indice di bilancio di partenza",;
    HelpContextID = 99381978,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=104, Top=13, cSayPict="'!!!!!!!!!!!!!!!'", cGetPict="'!!!!!!!!!!!!!!!'", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="IND_BILA", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODICE1"

  func oCODICE1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IND_BILA','*','IBCODICE',cp_AbsName(this.parent,'oCODICE1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Indici di bilancio",'GSBI2AIB.IND_BILA_VZM',this.parent.oContained
  endproc

  add object oDESCRI1_1_2 as StdField with uid="CZWPXJTYZX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione indice di bilancio di partenza",;
    HelpContextID = 16878794,;
   bGlobalFont=.t.,;
    Height=21, Width=262, Left=223, Top=13, InputMask=replicate('X',50)

  add object oCODICE2_1_3 as StdField with uid="TDDUXMMTSR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODICE2", cQueryName = "CODICE2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato",;
    ToolTipText = "Codice indice di bilancio di arrivo",;
    HelpContextID = 99381978,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=103, Top=45, cSayPict="'!!!!!!!!!!!!!!!'", cGetPict="'!!!!!!!!!!!!!!!'", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="IND_BILA", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODICE2"

  func oCODICE2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE2_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE2_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IND_BILA','*','IBCODICE',cp_AbsName(this.parent,'oCODICE2_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Indici di bilancio",'GSBI2AIB.IND_BILA_VZM',this.parent.oContained
  endproc

  add object oDESCRI2_1_4 as StdField with uid="TFLFWMMKNB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione indice di bilancio di arrivo",;
    HelpContextID = 16878794,;
   bGlobalFont=.t.,;
    Height=21, Width=262, Left=223, Top=45, InputMask=replicate('X',35)


  add object oObj_1_11 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=82, width=380,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 187826406


  add object oBtn_1_12 as StdButton with uid="GRNRUUCUDL",left=383, top=110, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 116817114;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_OQRY)))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="EFZTAPUEYA",left=438, top=110, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 251289274;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="SMFGDBWUKT",Visible=.t., Left=1, Top=15,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da indice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PRYBVOLMZW",Visible=.t., Left=0, Top=47,;
    Alignment=1, Width=103, Height=18,;
    Caption="a indice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QISNZNEWVE",Visible=.t., Left=2, Top=82,;
    Alignment=1, Width=102, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sib','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
