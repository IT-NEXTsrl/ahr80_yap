* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mcg                                                        *
*              Movimenti extra contabili                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_58]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_mcg"))

* --- Class definition
define class tgsbi_mcg as StdTrsForm
  Top    = 3
  Left   = 7

  * --- Standard Properties
  Width  = 641
  Height = 417+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=97125993
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MOX_COGM_IDX = 0
  MOX_COGE_IDX = 0
  MASTRI_IDX = 0
  CONTI_IDX = 0
  PER_ELAB_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  REG_EXTM_IDX = 0
  cFile = "MOX_COGM"
  cFileDetail = "MOX_COGE"
  cKeySelect = "CGSERIAL"
  cKeyWhere  = "CGSERIAL=this.w_CGSERIAL"
  cKeyDetail  = "CGSERIAL=this.w_CGSERIAL"
  cKeyWhereODBC = '"CGSERIAL="+cp_ToStrODBC(this.w_CGSERIAL)';

  cKeyDetailWhereODBC = '"CGSERIAL="+cp_ToStrODBC(this.w_CGSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MOX_COGE.CGSERIAL="+cp_ToStrODBC(this.w_CGSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOX_COGE.CPROWNUM '
  cPrg = "gsbi_mcg"
  cComment = "Movimenti extra contabili"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_CGSERIAL = space(10)
  w_CGNUMREG = 0
  w_CGCODESE = space(4)
  o_CGCODESE = space(4)
  w_CGDATREG = ctod('  /  /  ')
  w_CGFLGCON = space(1)
  w_CGTIPMOV = space(1)
  w_CGDESCRI = space(45)
  w_CGVALNAZ = space(3)
  o_CGVALNAZ = space(3)
  w_DECTOT = 0
  w_CGCODPER = space(15)
  w_DESPER = space(35)
  w_PERINI = ctod('  /  /  ')
  o_PERINI = ctod('  /  /  ')
  w_PERFIN = ctod('  /  /  ')
  w_CGCODREG = space(15)
  w_DESREG = space(45)
  w_DTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CALCPICT = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPREG = space(1)
  w_PECODESE = space(4)
  w_CGTIPCON = space(1)
  o_CGTIPCON = space(1)
  w_CGCODCON = space(15)
  w_CGCODMAS = space(15)
  w_DESMAS = space(40)
  w_NUMLIV = 0
  w_DESCON = space(40)
  w_DESCRI = space(40)
  w_CGIMPDAR = 0
  o_CGIMPDAR = 0
  w_CGIMPAVE = 0
  o_CGIMPAVE = 0
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SIMVAL = space(5)
  w_DATAOBSO = ctod('  /  /  ')
  w_DTINVA = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CGSERIAL = this.W_CGSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CGCODESE = this.W_CGCODESE
  op_CGNUMREG = this.W_CGNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOX_COGM','gsbi_mcg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mcgPag1","gsbi_mcg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti extra contabili")
      .Pages(1).HelpContextID = 121593578
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PER_ELAB'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='REG_EXTM'
    this.cWorkTables[7]='MOX_COGM'
    this.cWorkTables[8]='MOX_COGE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOX_COGM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOX_COGM_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CGSERIAL = NVL(CGSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MOX_COGM where CGSERIAL=KeySet.CGSERIAL
    *
    i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2],this.bLoadRecFilter,this.MOX_COGM_IDX,"gsbi_mcg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOX_COGM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOX_COGM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MOX_COGE.","MOX_COGM.")
      i_cTable = i_cTable+' MOX_COGM '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CGSERIAL',this.w_CGSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_DECTOT = 0
        .w_DESPER = space(35)
        .w_PERINI = ctod("  /  /  ")
        .w_PERFIN = ctod("  /  /  ")
        .w_DESREG = space(45)
        .w_TIPREG = space(1)
        .w_PECODESE = space(4)
        .w_TOTDAR = 0
        .w_TOTAVE = 0
        .w_SIMVAL = space(5)
        .w_DATAOBSO = ctod("  /  /  ")
        .w_DTINVA = ctod("  /  /  ")
        .w_CGSERIAL = NVL(CGSERIAL,space(10))
        .op_CGSERIAL = .w_CGSERIAL
        .w_CGNUMREG = NVL(CGNUMREG,0)
        .op_CGNUMREG = .w_CGNUMREG
        .w_CGCODESE = NVL(CGCODESE,space(4))
        .op_CGCODESE = .w_CGCODESE
          * evitabile
          *.link_1_4('Load')
        .w_CGDATREG = NVL(cp_ToDate(CGDATREG),ctod("  /  /  "))
        .w_CGFLGCON = NVL(CGFLGCON,space(1))
        .w_CGTIPMOV = NVL(CGTIPMOV,space(1))
        .w_CGDESCRI = NVL(CGDESCRI,space(45))
        .w_CGVALNAZ = NVL(CGVALNAZ,space(3))
          if link_1_9_joined
            this.w_CGVALNAZ = NVL(VACODVAL109,NVL(this.w_CGVALNAZ,space(3)))
            this.w_DECTOT = NVL(VADECTOT109,0)
            this.w_SIMVAL = NVL(VASIMVAL109,space(5))
          else
          .link_1_9('Load')
          endif
        .w_CGCODPER = NVL(CGCODPER,space(15))
          if link_1_11_joined
            this.w_CGCODPER = NVL(PECODICE111,NVL(this.w_CGCODPER,space(15)))
            this.w_DESPER = NVL(PEDESCRI111,space(35))
            this.w_PERINI = NVL(cp_ToDate(PEDATINI111),ctod("  /  /  "))
            this.w_PERFIN = NVL(cp_ToDate(PEDATFIN111),ctod("  /  /  "))
            this.w_PECODESE = NVL(PECODESE111,space(4))
            this.w_DATAOBSO = NVL(cp_ToDate(PEDTOBSO111),ctod("  /  /  "))
          else
          .link_1_11('Load')
          endif
        .w_CGCODREG = NVL(CGCODREG,space(15))
          if link_1_21_joined
            this.w_CGCODREG = NVL(RECODICE121,NVL(this.w_CGCODREG,space(15)))
            this.w_DESREG = NVL(REDESCRI121,space(45))
            this.w_TIPREG = NVL(RE__TIPO121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_DTOBSO = i_DATSYS
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOX_COGM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MOX_COGE where CGSERIAL=KeySet.CGSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MOX_COGE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGE_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MOX_COGE')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MOX_COGE.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MOX_COGE"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CGSERIAL',this.w_CGSERIAL  )
        select * from (i_cTable) MOX_COGE where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      scan
        with this
          .w_DESMAS = space(40)
          .w_NUMLIV = 0
          .w_DESCON = space(40)
          .w_CPROWNUM = CPROWNUM
          .w_CGTIPCON = NVL(CGTIPCON,space(1))
          .w_CGCODCON = NVL(CGCODCON,space(15))
          .link_2_2('Load')
          .w_CGCODMAS = NVL(CGCODMAS,space(15))
          if link_2_3_joined
            this.w_CGCODMAS = NVL(MCCODICE203,NVL(this.w_CGCODMAS,space(15)))
            this.w_DESMAS = NVL(MCDESCRI203,space(40))
            this.w_NUMLIV = NVL(MCNUMLIV203,0)
          else
          .link_2_3('Load')
          endif
        .w_DESCRI = IIF(.w_CGTIPCON='M', .w_DESMAS, IIF(.w_CGTIPCON='G', .w_DESCON, SPACE(40)))
          .w_CGIMPDAR = NVL(CGIMPDAR,0)
          .w_CGIMPAVE = NVL(CGIMPAVE,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTDAR = .w_TOTDAR+.w_CGIMPDAR
          .w_TOTAVE = .w_TOTAVE+.w_CGIMPAVE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_DTOBSO = i_DATSYS
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODAZI=space(5)
      .w_CGSERIAL=space(10)
      .w_CGNUMREG=0
      .w_CGCODESE=space(4)
      .w_CGDATREG=ctod("  /  /  ")
      .w_CGFLGCON=space(1)
      .w_CGTIPMOV=space(1)
      .w_CGDESCRI=space(45)
      .w_CGVALNAZ=space(3)
      .w_DECTOT=0
      .w_CGCODPER=space(15)
      .w_DESPER=space(35)
      .w_PERINI=ctod("  /  /  ")
      .w_PERFIN=ctod("  /  /  ")
      .w_CGCODREG=space(15)
      .w_DESREG=space(45)
      .w_DTOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CALCPICT=0
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_TIPREG=space(1)
      .w_PECODESE=space(4)
      .w_CGTIPCON=space(1)
      .w_CGCODCON=space(15)
      .w_CGCODMAS=space(15)
      .w_DESMAS=space(40)
      .w_NUMLIV=0
      .w_DESCON=space(40)
      .w_DESCRI=space(40)
      .w_CGIMPDAR=0
      .w_CGIMPAVE=0
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SIMVAL=space(5)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DTINVA=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,3,.f.)
        .w_CGCODESE = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CGCODESE))
         .link_1_4('Full')
        endif
        .w_CGDATREG = i_datsys
        .w_CGFLGCON = 'S'
        .w_CGTIPMOV = 'E'
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CGVALNAZ))
         .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        .w_CGCODPER = SPACE(15)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CGCODPER))
         .link_1_11('Full')
        endif
        .DoRTCalc(12,15,.f.)
        if not(empty(.w_CGCODREG))
         .link_1_21('Full')
        endif
        .DoRTCalc(16,16,.f.)
        .w_DTOBSO = i_DATSYS
        .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(20,25,.f.)
        .w_CGTIPCON = 'G'
        .w_CGCODCON = SPACE(15)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CGCODCON))
         .link_2_2('Full')
        endif
        .w_CGCODMAS = SPACE(15)
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CGCODMAS))
         .link_2_3('Full')
        endif
        .DoRTCalc(29,31,.f.)
        .w_DESCRI = IIF(.w_CGTIPCON='M', .w_DESMAS, IIF(.w_CGTIPCON='G', .w_DESCON, SPACE(40)))
        .w_CGIMPDAR = 0
        .w_CGIMPAVE = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOX_COGM')
    this.DoRTCalc(35,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCGNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oCGCODESE_1_4.enabled = i_bVal
      .Page1.oPag.oCGDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oCGTIPMOV_1_7.enabled = i_bVal
      .Page1.oPag.oCGDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oCGCODPER_1_11.enabled = i_bVal
      .Page1.oPag.oCGCODREG_1_21.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCGNUMREG_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOX_COGM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMOXCG","i_codazi,w_CGSERIAL")
      cp_AskTableProg(this,i_nConn,"PRMOXCG","i_codazi,w_CGCODESE,w_CGNUMREG")
      .op_codazi = .w_codazi
      .op_CGSERIAL = .w_CGSERIAL
      .op_codazi = .w_codazi
      .op_CGCODESE = .w_CGCODESE
      .op_CGNUMREG = .w_CGNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGSERIAL,"CGSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGNUMREG,"CGNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGCODESE,"CGCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGDATREG,"CGDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGFLGCON,"CGFLGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGTIPMOV,"CGTIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGDESCRI,"CGDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGVALNAZ,"CGVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGCODPER,"CGCODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CGCODREG,"CGCODREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
    i_lTable = "MOX_COGM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOX_COGM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSBI_SCG with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CGTIPCON C(1);
      ,t_CGCODCON C(15);
      ,t_CGCODMAS C(15);
      ,t_DESCRI C(40);
      ,t_CGIMPDAR N(18,4);
      ,t_CGIMPAVE N(18,4);
      ,CPROWNUM N(10);
      ,t_DESMAS C(40);
      ,t_NUMLIV N(1);
      ,t_DESCON C(40);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mcgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1.controlsource=this.cTrsName+'.t_CGTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODCON_2_2.controlsource=this.cTrsName+'.t_CGCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODMAS_2_3.controlsource=this.cTrsName+'.t_CGCODMAS'
    this.oPgFRm.Page1.oPag.oDESCRI_2_7.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPDAR_2_8.controlsource=this.cTrsName+'.t_CGIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPAVE_2_9.controlsource=this.cTrsName+'.t_CGIMPAVE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(28)
    this.AddVLine(160)
    this.AddVLine(292)
    this.AddVLine(452)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEMOXCG","i_codazi,w_CGSERIAL")
          cp_NextTableProg(this,i_nConn,"PRMOXCG","i_codazi,w_CGCODESE,w_CGNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOX_COGM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOX_COGM')
        i_extval=cp_InsertValODBCExtFlds(this,'MOX_COGM')
        local i_cFld
        i_cFld=" "+;
                  "(CGSERIAL,CGNUMREG,CGCODESE,CGDATREG,CGFLGCON"+;
                  ",CGTIPMOV,CGDESCRI,CGVALNAZ,CGCODPER,CGCODREG"+;
                  ",UTCC,UTCV,UTDC,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CGSERIAL)+;
                    ","+cp_ToStrODBC(this.w_CGNUMREG)+;
                    ","+cp_ToStrODBCNull(this.w_CGCODESE)+;
                    ","+cp_ToStrODBC(this.w_CGDATREG)+;
                    ","+cp_ToStrODBC(this.w_CGFLGCON)+;
                    ","+cp_ToStrODBC(this.w_CGTIPMOV)+;
                    ","+cp_ToStrODBC(this.w_CGDESCRI)+;
                    ","+cp_ToStrODBCNull(this.w_CGVALNAZ)+;
                    ","+cp_ToStrODBCNull(this.w_CGCODPER)+;
                    ","+cp_ToStrODBCNull(this.w_CGCODREG)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOX_COGM')
        i_extval=cp_InsertValVFPExtFlds(this,'MOX_COGM')
        cp_CheckDeletedKey(i_cTable,0,'CGSERIAL',this.w_CGSERIAL)
        INSERT INTO (i_cTable);
              (CGSERIAL,CGNUMREG,CGCODESE,CGDATREG,CGFLGCON,CGTIPMOV,CGDESCRI,CGVALNAZ,CGCODPER,CGCODREG,UTCC,UTCV,UTDC,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CGSERIAL;
                  ,this.w_CGNUMREG;
                  ,this.w_CGCODESE;
                  ,this.w_CGDATREG;
                  ,this.w_CGFLGCON;
                  ,this.w_CGTIPMOV;
                  ,this.w_CGDESCRI;
                  ,this.w_CGVALNAZ;
                  ,this.w_CGCODPER;
                  ,this.w_CGCODREG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOX_COGE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGE_IDX,2])
      *
      * insert into MOX_COGE
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CGSERIAL,CGTIPCON,CGCODCON,CGCODMAS,CGIMPDAR"+;
                  ",CGIMPAVE,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CGSERIAL)+","+cp_ToStrODBC(this.w_CGTIPCON)+","+cp_ToStrODBCNull(this.w_CGCODCON)+","+cp_ToStrODBCNull(this.w_CGCODMAS)+","+cp_ToStrODBC(this.w_CGIMPDAR)+;
             ","+cp_ToStrODBC(this.w_CGIMPAVE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CGSERIAL',this.w_CGSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CGSERIAL,this.w_CGTIPCON,this.w_CGCODCON,this.w_CGCODMAS,this.w_CGIMPDAR"+;
                ",this.w_CGIMPAVE,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MOX_COGM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MOX_COGM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CGNUMREG="+cp_ToStrODBC(this.w_CGNUMREG)+;
             ",CGCODESE="+cp_ToStrODBCNull(this.w_CGCODESE)+;
             ",CGDATREG="+cp_ToStrODBC(this.w_CGDATREG)+;
             ",CGFLGCON="+cp_ToStrODBC(this.w_CGFLGCON)+;
             ",CGTIPMOV="+cp_ToStrODBC(this.w_CGTIPMOV)+;
             ",CGDESCRI="+cp_ToStrODBC(this.w_CGDESCRI)+;
             ",CGVALNAZ="+cp_ToStrODBCNull(this.w_CGVALNAZ)+;
             ",CGCODPER="+cp_ToStrODBCNull(this.w_CGCODPER)+;
             ",CGCODREG="+cp_ToStrODBCNull(this.w_CGCODREG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MOX_COGM')
          i_cWhere = cp_PKFox(i_cTable  ,'CGSERIAL',this.w_CGSERIAL  )
          UPDATE (i_cTable) SET;
              CGNUMREG=this.w_CGNUMREG;
             ,CGCODESE=this.w_CGCODESE;
             ,CGDATREG=this.w_CGDATREG;
             ,CGFLGCON=this.w_CGFLGCON;
             ,CGTIPMOV=this.w_CGTIPMOV;
             ,CGDESCRI=this.w_CGDESCRI;
             ,CGVALNAZ=this.w_CGVALNAZ;
             ,CGCODPER=this.w_CGCODPER;
             ,CGCODREG=this.w_CGCODREG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CGTIPCON<>' ' AND NOT EMPTY(t_CGCODMAS+t_CGCODCON)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MOX_COGE_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGE_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MOX_COGE
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOX_COGE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CGTIPCON="+cp_ToStrODBC(this.w_CGTIPCON)+;
                     ",CGCODCON="+cp_ToStrODBCNull(this.w_CGCODCON)+;
                     ",CGCODMAS="+cp_ToStrODBCNull(this.w_CGCODMAS)+;
                     ",CGIMPDAR="+cp_ToStrODBC(this.w_CGIMPDAR)+;
                     ",CGIMPAVE="+cp_ToStrODBC(this.w_CGIMPAVE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CGTIPCON=this.w_CGTIPCON;
                     ,CGCODCON=this.w_CGCODCON;
                     ,CGCODMAS=this.w_CGCODMAS;
                     ,CGIMPDAR=this.w_CGIMPDAR;
                     ,CGIMPAVE=this.w_CGIMPAVE;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CGTIPCON<>' ' AND NOT EMPTY(t_CGCODMAS+t_CGCODCON)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MOX_COGE_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGE_IDX,2])
        *
        * delete MOX_COGE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
        *
        * delete MOX_COGM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CGTIPCON<>' ' AND NOT EMPTY(t_CGCODMAS+t_CGCODCON)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOX_COGM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOX_COGM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_CGCODESE<>.w_CGCODESE
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_CGCODESE<>.w_CGCODESE
          .w_CGCODPER = SPACE(15)
          .link_1_11('Full')
        endif
        .DoRTCalc(12,16,.t.)
          .w_DTOBSO = i_DATSYS
        if .o_PERINI<>.w_PERINI
          .w_OBTEST = IIF(NOT EMPTY(.w_PERINI), .w_PERINI, i_datsys)
        endif
        if .o_CGVALNAZ<>.w_CGVALNAZ
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(20,26,.t.)
        if .o_CGTIPCON<>.w_CGTIPCON
          .w_CGCODCON = SPACE(15)
          .link_2_2('Full')
        endif
        if .o_CGTIPCON<>.w_CGTIPCON
          .w_CGCODMAS = SPACE(15)
          .link_2_3('Full')
        endif
        .DoRTCalc(29,31,.t.)
          .w_DESCRI = IIF(.w_CGTIPCON='M', .w_DESMAS, IIF(.w_CGTIPCON='G', .w_DESCON, SPACE(40)))
        if .o_CGIMPAVE<>.w_CGIMPAVE
          .w_TOTDAR = .w_TOTDAR-.w_cgimpdar
          .w_CGIMPDAR = 0
          .w_TOTDAR = .w_TOTDAR+.w_cgimpdar
        endif
        if .o_CGIMPDAR<>.w_CGIMPDAR
          .w_TOTAVE = .w_TOTAVE-.w_cgimpave
          .w_CGIMPAVE = 0
          .w_TOTAVE = .w_TOTAVE+.w_cgimpave
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMOXCG","i_codazi,w_CGSERIAL")
          .op_CGSERIAL = .w_CGSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_CGCODESE<>.w_CGCODESE
           cp_AskTableProg(this,i_nConn,"PRMOXCG","i_codazi,w_CGCODESE,w_CGNUMREG")
          .op_CGNUMREG = .w_CGNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_CGCODESE = .w_CGCODESE
      endwith
      this.DoRTCalc(35,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESMAS with this.w_DESMAS
      replace t_NUMLIV with this.w_NUMLIV
      replace t_DESCON with this.w_DESCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGCODCON_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGCODCON_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPDAR_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPDAR_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPAVE_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCGIMPAVE_2_9.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CGCODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CGCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CGCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCGCODESE_1_4'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CGCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CGCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_CGVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODESE = space(4)
      endif
      this.w_CGVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGVALNAZ
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CGVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CGVALNAZ)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CGVALNAZ = space(3)
      endif
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.VACODVAL as VACODVAL109"+ ",link_1_9.VADECTOT as VADECTOT109"+ ",link_1_9.VASIMVAL as VASIMVAL109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on MOX_COGM.CGVALNAZ=link_1_9.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and MOX_COGM.CGVALNAZ=link_1_9.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGCODPER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_CGCODPER)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_CGCODPER))
          select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODPER)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStrODBC(trim(this.w_CGCODPER)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PEDESCRI like "+cp_ToStr(trim(this.w_CGCODPER)+"%");

            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGCODPER) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oCGCODPER_1_11'),i_cWhere,'GSBI_APE',"Periodi di elaborazione",'GSBI_MCG.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_CGCODPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_CGCODPER)
            select PECODICE,PEDESCRI,PEDATINI,PEDATFIN,PECODESE,PEDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODPER = NVL(_Link_.PECODICE,space(15))
      this.w_DESPER = NVL(_Link_.PEDESCRI,space(35))
      this.w_PERINI = NVL(cp_ToDate(_Link_.PEDATINI),ctod("  /  /  "))
      this.w_PERFIN = NVL(cp_ToDate(_Link_.PEDATFIN),ctod("  /  /  "))
      this.w_PECODESE = NVL(_Link_.PECODESE,space(4))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.PEDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODPER = space(15)
      endif
      this.w_DESPER = space(35)
      this.w_PERINI = ctod("  /  /  ")
      this.w_PERFIN = ctod("  /  /  ")
      this.w_PECODESE = space(4)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CGCODESE = .w_PECODESE AND (.w_DATAOBSO>=.w_DTOBSO OR EMPTY(.w_DATAOBSO)) AND (.w_DTINVA<=.w_DTOBSO OR EMPTY(.w_DTINVA))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Periodo incongruente od obsoleto")
        endif
        this.w_CGCODPER = space(15)
        this.w_DESPER = space(35)
        this.w_PERINI = ctod("  /  /  ")
        this.w_PERFIN = ctod("  /  /  ")
        this.w_PECODESE = space(4)
        this.w_DATAOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PER_ELAB_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.PECODICE as PECODICE111"+ ",link_1_11.PEDESCRI as PEDESCRI111"+ ",link_1_11.PEDATINI as PEDATINI111"+ ",link_1_11.PEDATFIN as PEDATFIN111"+ ",link_1_11.PECODESE as PECODESE111"+ ",link_1_11.PEDTOBSO as PEDTOBSO111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MOX_COGM.CGCODPER=link_1_11.PECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MOX_COGM.CGCODPER=link_1_11.PECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGCODREG
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_EXTM_IDX,3]
    i_lTable = "REG_EXTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2], .t., this.REG_EXTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODREG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MRE',True,'REG_EXTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_CGCODREG)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_CGCODREG))
          select RECODICE,REDESCRI,RE__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODREG)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_CGCODREG)+"%");

            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_CGCODREG)+"%");

            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGCODREG) and !this.bDontReportError
            deferred_cp_zoom('REG_EXTM','*','RECODICE',cp_AbsName(oSource.parent,'oCGCODREG_1_21'),i_cWhere,'GSBI_MRE',"Regole di elaborazione",'GSBI_MCG.REG_EXTM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODREG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI,RE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_CGCODREG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_CGCODREG)
            select RECODICE,REDESCRI,RE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODREG = NVL(_Link_.RECODICE,space(15))
      this.w_DESREG = NVL(_Link_.REDESCRI,space(45))
      this.w_TIPREG = NVL(_Link_.RE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODREG = space(15)
      endif
      this.w_DESREG = space(45)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREG $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Regola inesistente o di tipo analitica")
        endif
        this.w_CGCODREG = space(15)
        this.w_DESREG = space(45)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REG_EXTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODREG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REG_EXTM_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.RECODICE as RECODICE121"+ ",link_1_21.REDESCRI as REDESCRI121"+ ",link_1_21.RE__TIPO as RE__TIPO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on MOX_COGM.CGCODREG=link_1_21.RECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and MOX_COGM.CGCODREG=link_1_21.RECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CGCODCON
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CGCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CGTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CGTIPCON;
                     ,'ANCODICE',trim(this.w_CGCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CGCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CGTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CGCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CGTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CGCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCGCODCON_2_2'),i_cWhere,'GSAR_BZC',"Conti",'GSBI_MCG.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CGTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CGTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CGCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CGTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CGTIPCON;
                       ,'ANCODICE',this.w_CGCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CGCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CGCODMAS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CGCODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CGCODMAS)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CGCODMAS))
          select MCCODICE,MCDESCRI,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CGCODMAS)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CGCODMAS) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCGCODMAS_2_3'),i_cWhere,'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CGCODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CGCODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CGCODMAS)
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CGCODMAS = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_NUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_CGCODMAS = space(15)
      endif
      this.w_DESMAS = space(40)
      this.w_NUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NUMLIV=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il mastro deve essere di livello 1")
        endif
        this.w_CGCODMAS = space(15)
        this.w_DESMAS = space(40)
        this.w_NUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CGCODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.MCCODICE as MCCODICE203"+ ",link_2_3.MCDESCRI as MCDESCRI203"+ ",link_2_3.MCNUMLIV as MCNUMLIV203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOX_COGE.CGCODMAS=link_2_3.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOX_COGE.CGCODMAS=link_2_3.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCGNUMREG_1_3.value==this.w_CGNUMREG)
      this.oPgFrm.Page1.oPag.oCGNUMREG_1_3.value=this.w_CGNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODESE_1_4.value==this.w_CGCODESE)
      this.oPgFrm.Page1.oPag.oCGCODESE_1_4.value=this.w_CGCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDATREG_1_5.value==this.w_CGDATREG)
      this.oPgFrm.Page1.oPag.oCGDATREG_1_5.value=this.w_CGDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCGTIPMOV_1_7.RadioValue()==this.w_CGTIPMOV)
      this.oPgFrm.Page1.oPag.oCGTIPMOV_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCGDESCRI_1_8.value==this.w_CGDESCRI)
      this.oPgFrm.Page1.oPag.oCGDESCRI_1_8.value=this.w_CGDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCGVALNAZ_1_9.value==this.w_CGVALNAZ)
      this.oPgFrm.Page1.oPag.oCGVALNAZ_1_9.value=this.w_CGVALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODPER_1_11.value==this.w_CGCODPER)
      this.oPgFrm.Page1.oPag.oCGCODPER_1_11.value=this.w_CGCODPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_18.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_18.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oPERINI_1_19.value==this.w_PERINI)
      this.oPgFrm.Page1.oPag.oPERINI_1_19.value=this.w_PERINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERFIN_1_20.value==this.w_PERFIN)
      this.oPgFrm.Page1.oPag.oPERFIN_1_20.value=this.w_PERFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCGCODREG_1_21.value==this.w_CGCODREG)
      this.oPgFrm.Page1.oPag.oCGCODREG_1_21.value=this.w_CGCODREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREG_1_22.value==this.w_DESREG)
      this.oPgFrm.Page1.oPag.oDESREG_1_22.value=this.w_DESREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_2_7.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_2_7.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oDESCRI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_38.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_38.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1.value==this.w_CGTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1.value=this.w_CGTIPCON
      replace t_CGTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODCON_2_2.value==this.w_CGCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODCON_2_2.value=this.w_CGCODCON
      replace t_CGCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODMAS_2_3.value==this.w_CGCODMAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODMAS_2_3.value=this.w_CGCODMAS
      replace t_CGCODMAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODMAS_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPDAR_2_8.value==this.w_CGIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPDAR_2_8.value=this.w_CGIMPDAR
      replace t_CGIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPDAR_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPAVE_2_9.value==this.w_CGIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPAVE_2_9.value=this.w_CGIMPAVE
      replace t_CGIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGIMPAVE_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'MOX_COGM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CGNUMREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGNUMREG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CGNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGCODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGCODESE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CGCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGDATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CGDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGTIPMOV))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGTIPMOV_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CGTIPMOV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CGCODPER) or not(.w_CGCODESE = .w_PECODESE AND (.w_DATAOBSO>=.w_DTOBSO OR EMPTY(.w_DATAOBSO)) AND (.w_DTINVA<=.w_DTOBSO OR EMPTY(.w_DTINVA))))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGCODPER_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CGCODPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Periodo incongruente od obsoleto")
          case   not(.w_TIPREG $ ' C')  and not(empty(.w_CGCODREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCGCODREG_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Regola inesistente o di tipo analitica")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CGTIPCON $ 'GM') and (.w_CGTIPCON<>' ' AND NOT EMPTY(.w_CGCODMAS+.w_CGCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGTIPCON_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: G (conto generico) o M (mastro)")
        case   not(.w_NUMLIV=1) and not(empty(.w_CGCODMAS)) and (.w_CGTIPCON<>' ' AND NOT EMPTY(.w_CGCODMAS+.w_CGCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCGCODMAS_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il mastro deve essere di livello 1")
      endcase
      if .w_CGTIPCON<>' ' AND NOT EMPTY(.w_CGCODMAS+.w_CGCODCON)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CGCODESE = this.w_CGCODESE
    this.o_CGVALNAZ = this.w_CGVALNAZ
    this.o_PERINI = this.w_PERINI
    this.o_CGTIPCON = this.w_CGTIPCON
    this.o_CGIMPDAR = this.w_CGIMPDAR
    this.o_CGIMPAVE = this.w_CGIMPAVE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CGTIPCON<>' ' AND NOT EMPTY(t_CGCODMAS+t_CGCODCON))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CGTIPCON=space(1)
      .w_CGCODCON=space(15)
      .w_CGCODMAS=space(15)
      .w_DESMAS=space(40)
      .w_NUMLIV=0
      .w_DESCON=space(40)
      .w_DESCRI=space(40)
      .w_CGIMPDAR=0
      .w_CGIMPAVE=0
      .DoRTCalc(1,25,.f.)
        .w_CGTIPCON = 'G'
        .w_CGCODCON = SPACE(15)
      .DoRTCalc(27,27,.f.)
      if not(empty(.w_CGCODCON))
        .link_2_2('Full')
      endif
        .w_CGCODMAS = SPACE(15)
      .DoRTCalc(28,28,.f.)
      if not(empty(.w_CGCODMAS))
        .link_2_3('Full')
      endif
      .DoRTCalc(29,31,.f.)
        .w_DESCRI = IIF(.w_CGTIPCON='M', .w_DESMAS, IIF(.w_CGTIPCON='G', .w_DESCON, SPACE(40)))
        .w_CGIMPDAR = 0
        .w_CGIMPAVE = 0
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_10.Calculate()
    endwith
    this.DoRTCalc(35,39,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CGTIPCON = t_CGTIPCON
    this.w_CGCODCON = t_CGCODCON
    this.w_CGCODMAS = t_CGCODMAS
    this.w_DESMAS = t_DESMAS
    this.w_NUMLIV = t_NUMLIV
    this.w_DESCON = t_DESCON
    this.w_DESCRI = t_DESCRI
    this.w_CGIMPDAR = t_CGIMPDAR
    this.w_CGIMPAVE = t_CGIMPAVE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CGTIPCON with this.w_CGTIPCON
    replace t_CGCODCON with this.w_CGCODCON
    replace t_CGCODMAS with this.w_CGCODMAS
    replace t_DESMAS with this.w_DESMAS
    replace t_NUMLIV with this.w_NUMLIV
    replace t_DESCON with this.w_DESCON
    replace t_DESCRI with this.w_DESCRI
    replace t_CGIMPDAR with this.w_CGIMPDAR
    replace t_CGIMPAVE with this.w_CGIMPAVE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTDAR = .w_TOTDAR-.w_cgimpdar
        .w_TOTAVE = .w_TOTAVE-.w_cgimpave
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mcgPag1 as StdContainer
  Width  = 637
  height = 417
  stdWidth  = 637
  stdheight = 417
  resizeXpos=231
  resizeYpos=330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCGNUMREG_1_3 as StdField with uid="TSRBJXRCHR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CGNUMREG", cQueryName = "CGNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo numero di registrazione",;
    HelpContextID = 245357203,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=86, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oCGCODESE_1_4 as StdField with uid="CQBKMTVECH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CGCODESE", cQueryName = "CGCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di riferimento della registrazione",;
    HelpContextID = 63534443,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CGCODESE"

  func oCGCODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCGCODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCGDATREG_1_5 as StdField with uid="QONIQODGGW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CGDATREG", cQueryName = "CGDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 239368851,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=246, Top=11


  add object oCGTIPMOV_1_7 as StdCombo with uid="MQWDVGWFRT",rtseq=7,rtrep=.f.,left=519,top=11,width=116,height=21;
    , ToolTipText = "Tipologia dei movimenti: effettivi/previsionali";
    , HelpContextID = 210011516;
    , cFormVar="w_CGTIPMOV",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oCGTIPMOV_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CGTIPMOV,&i_cF..t_CGTIPMOV),this.value)
    return(iif(xVal =1,'E',;
    iif(xVal =2,'P',;
    space(1))))
  endfunc
  func oCGTIPMOV_1_7.GetRadio()
    this.Parent.oContained.w_CGTIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oCGTIPMOV_1_7.ToRadio()
    this.Parent.oContained.w_CGTIPMOV=trim(this.Parent.oContained.w_CGTIPMOV)
    return(;
      iif(this.Parent.oContained.w_CGTIPMOV=='E',1,;
      iif(this.Parent.oContained.w_CGTIPMOV=='P',2,;
      0)))
  endfunc

  func oCGTIPMOV_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCGDESCRI_1_8 as StdField with uid="UAPJSRXIEY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CGDESCRI", cQueryName = "CGDESCRI",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 45057391,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=86, Top=41, InputMask=replicate('X',45)

  add object oCGVALNAZ_1_9 as StdField with uid="TKTUALMRAC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CGVALNAZ", cQueryName = "CGVALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 222078336,;
   bGlobalFont=.t.,;
    Height=21, Width=39, Left=519, Top=41, InputMask=replicate('X',3), cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_CGVALNAZ"

  func oCGVALNAZ_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCGCODPER_1_11 as StdField with uid="ZYRJUNXNLW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CGCODPER", cQueryName = "CGCODPER",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Periodo incongruente od obsoleto",;
    ToolTipText = "Periodo di riferimento dei movimenti",;
    HelpContextID = 248083832,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=71, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_CGCODPER"

  func oCGCODPER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODPER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODPER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oCGCODPER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi di elaborazione",'GSBI_MCG.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oCGCODPER_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_CGCODPER
    i_obj.ecpSave()
  endproc

  add object oDESPER_1_18 as StdField with uid="XTEBYVQSZL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 14381878,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=224, Top=71, InputMask=replicate('X',35)

  add object oPERINI_1_19 as StdField with uid="JPQFYNCQAB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PERINI", cQueryName = "PERINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 140796918,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=519, Top=71

  add object oPERFIN_1_20 as StdField with uid="QERYIQRPBS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PERFIN", cQueryName = "PERFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 219243510,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=519, Top=101

  add object oCGCODREG_1_21 as StdField with uid="KCMMDDCNEQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CGCODREG", cQueryName = "CGCODREG",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Regola inesistente o di tipo analitica",;
    ToolTipText = "Regola di elaborazione utilizzata per la generazione (vuoto=manuale)",;
    HelpContextID = 255232659,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=86, Top=101, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="REG_EXTM", cZoomOnZoom="GSBI_MRE", oKey_1_1="RECODICE", oKey_1_2="this.w_CGCODREG"

  func oCGCODREG_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODREG_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCGCODREG_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_EXTM','*','RECODICE',cp_AbsName(this.parent,'oCGCODREG_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MRE',"Regole di elaborazione",'GSBI_MCG.REG_EXTM_VZM',this.parent.oContained
  endproc
  proc oCGCODREG_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODICE=this.parent.oContained.w_CGCODREG
    i_obj.ecpSave()
  endproc

  add object oDESREG_1_22 as StdField with uid="BDTITAUNCK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESREG", cQueryName = "DESREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    HelpContextID = 98399030,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=224, Top=101, InputMask=replicate('X',45)

  add object oSIMVAL_1_38 as StdField with uid="ZQDXLEDWFA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 178329638,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=562, Top=41, InputMask=replicate('X',5)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=133, width=624,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CGTIPCON",Label1="T",Field2="CGCODCON",Label2="Conto",Field3="CGCODMAS",Label3="Mastro",Field4="CGIMPDAR",Label4="DARE",Field5="CGIMPAVE",Label5="AVERE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218964858

  add object oStr_1_12 as StdString with uid="SSIIIPNVCE",Visible=.t., Left=11, Top=11,;
    Alignment=1, Width=73, Height=15,;
    Caption="Registr. n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="CJZUQPYMMW",Visible=.t., Left=155, Top=11,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="UWWGIIUZVV",Visible=.t., Left=210, Top=11,;
    Alignment=1, Width=33, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="DVBFFEBXXC",Visible=.t., Left=2, Top=41,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PZMAGGIBQA",Visible=.t., Left=422, Top=11,;
    Alignment=1, Width=94, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="JTRRBXNYPC",Visible=.t., Left=11, Top=71,;
    Alignment=1, Width=73, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NFUXDFMEDC",Visible=.t., Left=482, Top=71,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HCAJXBXYLZ",Visible=.t., Left=489, Top=101,;
    Alignment=1, Width=27, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="IVBNDZLZWB",Visible=.t., Left=161, Top=397,;
    Alignment=1, Width=131, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KNQPWCYHCE",Visible=.t., Left=427, Top=41,;
    Alignment=1, Width=89, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="NGPIXEFYHL",Visible=.t., Left=11, Top=101,;
    Alignment=1, Width=73, Height=15,;
    Caption="Regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="QTEWQYRUWD",Visible=.t., Left=165, Top=374,;
    Alignment=1, Width=127, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=155,;
    width=620+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=156,width=619+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|MASTRI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCRI_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oCGCODCON_2_2
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oCGCODMAS_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCRI_2_7 as StdTrsField with uid="OBLRUVXJKU",rtseq=32,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 144601910,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=314, Left=296, Top=397, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTDAR_3_1 as StdField with uid="KWGJXOSKEH",rtseq=35,rtrep=.f.,;
    cFormVar="w_TOTDAR",value=0,enabled=.f.,;
    HelpContextID = 9408054,;
    cQueryName = "TOTDAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=295, Top=372, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTOTAVE_3_2 as StdField with uid="LXWLABSYYF",rtseq=36,rtrep=.f.,;
    cFormVar="w_TOTAVE",value=0,enabled=.f.,;
    HelpContextID = 81563190,;
    cQueryName = "TOTAVE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=454, Top=372, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
enddefine

* --- Defining Body row
define class tgsbi_mcgBodyRow as CPBodyRowCnt
  Width=610
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCGTIPCON_2_1 as StdTrsField with uid="DLHACDOYHM",rtseq=26,rtrep=.t.,;
    cFormVar="w_CGTIPCON",value=space(1),;
    ToolTipText = "Tipologia di riga: M=mastro; G=conto generico",;
    HelpContextID = 42239348,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire: G (conto generico) o M (mastro)",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=-2, Top=0, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oCGTIPCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CGTIPCON $ 'GM')
      if .not. empty(.w_CGCODCON)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCGCODCON_2_2 as StdTrsField with uid="AJYMAWLNHS",rtseq=27,rtrep=.t.,;
    cFormVar="w_CGCODCON",value=space(15),;
    ToolTipText = "Conto generico",;
    HelpContextID = 29980020,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=25, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CGTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CGCODCON"

  func oCGCODCON_2_2.mCond()
    with this.Parent.oContained
      return (.w_CGTIPCON='G')
    endwith
  endfunc

  func oCGCODCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODCON_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCGCODCON_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CGTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CGTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCGCODCON_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSBI_MCG.CONTI_VZM',this.parent.oContained
  endproc
  proc oCGCODCON_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CGTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CGCODCON
    i_obj.ecpSave()
  endproc

  add object oCGCODMAS_2_3 as StdTrsField with uid="NMZTNIVDFL",rtseq=28,rtrep=.t.,;
    cFormVar="w_CGCODMAS",value=space(15),;
    ToolTipText = "Mastro contabile",;
    HelpContextID = 197752185,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il mastro deve essere di livello 1",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=157, Top=0, cSayPict=[p_MAS], cGetPict=[p_MAS], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CGCODMAS"

  func oCGCODMAS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCGCODMAS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCGCODMAS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCGCODMAS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCGCODMAS_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CGCODMAS
    i_obj.ecpSave()
  endproc

  add object oCGIMPDAR_2_8 as StdTrsField with uid="KBSYMYAFME",rtseq=33,rtrep=.t.,;
    cFormVar="w_CGIMPDAR",value=0,;
    ToolTipText = "Importo dare",;
    HelpContextID = 59233656,;
    cTotal = "this.Parent.oContained.w_totdar", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=156, Left=289, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oCGIMPDAR_2_8.mCond()
    with this.Parent.oContained
      return (.w_CGIMPAVE=0)
    endwith
  endfunc

  add object oCGIMPAVE_2_9 as StdTrsField with uid="RQSODKNHRS",rtseq=34,rtrep=.t.,;
    cFormVar="w_CGIMPAVE",value=0,;
    ToolTipText = "Importo avere",;
    HelpContextID = 259533461,;
    cTotal = "this.Parent.oContained.w_totave", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=156, Left=449, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oCGIMPAVE_2_9.mCond()
    with this.Parent.oContained
      return (.w_CGIMPDAR=0)
    endwith
  endfunc

  add object oObj_2_10 as cp_runprogram with uid="BTLNCUUWPD",width=113,height=23,;
   left=-1, top=277,;
    caption='GSBI_BCA(C)',;
   bGlobalFont=.t.,;
    prg="GSBI_BCA('C')",;
    cEvent = "w_CGCODCON Changed",;
    nPag=2;
    , HelpContextID = 41305895
  add object oLast as LastKeyMover
  * ---
  func oCGTIPCON_2_1.When()
    return(.t.)
  proc oCGTIPCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCGTIPCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mcg','MOX_COGM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CGSERIAL=MOX_COGM.CGSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
