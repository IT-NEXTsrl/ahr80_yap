* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sre                                                        *
*              Stampa regole di elaborazione                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-09-03                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sre",oParentObject))

* --- Class definition
define class tgsbi_sre as StdForm
  Top    = 20
  Left   = 67

  * --- Standard Properties
  Width  = 496
  Height = 194
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-10"
  HelpContextID=107611753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  PER_ELAB_IDX = 0
  CONTBILC_IDX = 0
  REG_EXTM_IDX = 0
  cPrg = "gsbi_sre"
  cComment = "Stampa regole di elaborazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RE__TIPO = space(10)
  w_RECODICE1 = space(15)
  w_REDESCRI1 = space(35)
  w_RECODICE2 = space(15)
  w_REDESCRI2 = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_srePag1","gsbi_sre",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRE__TIPO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='PER_ELAB'
    this.cWorkTables[2]='CONTBILC'
    this.cWorkTables[3]='REG_EXTM'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RE__TIPO=space(10)
      .w_RECODICE1=space(15)
      .w_REDESCRI1=space(35)
      .w_RECODICE2=space(15)
      .w_REDESCRI2=space(35)
        .w_RE__TIPO = 'T'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_RECODICE1))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_RECODICE2))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RECODICE1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_EXTM_IDX,3]
    i_lTable = "REG_EXTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2], .t., this.REG_EXTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RECODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'REG_EXTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_RECODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_RECODICE1))
          select RECODICE,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RECODICE1)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RECODICE1) and !this.bDontReportError
            deferred_cp_zoom('REG_EXTM','*','RECODICE',cp_AbsName(oSource.parent,'oRECODICE1_1_2'),i_cWhere,'',"Regole di elaborazione",'regela.REG_EXTM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RECODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_RECODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_RECODICE1)
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RECODICE1 = NVL(_Link_.RECODICE,space(15))
      this.w_REDESCRI1 = NVL(_Link_.REDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_RECODICE1 = space(15)
      endif
      this.w_REDESCRI1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_RECODICE2) or (.w_RECODICE2>=.w_RECODICE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endif
        this.w_RECODICE1 = space(15)
        this.w_REDESCRI1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REG_EXTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RECODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RECODICE2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_EXTM_IDX,3]
    i_lTable = "REG_EXTM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2], .t., this.REG_EXTM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RECODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'REG_EXTM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_RECODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_RECODICE2))
          select RECODICE,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RECODICE2)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RECODICE2) and !this.bDontReportError
            deferred_cp_zoom('REG_EXTM','*','RECODICE',cp_AbsName(oSource.parent,'oRECODICE2_1_4'),i_cWhere,'',"Regole di elaborazione",'regela.REG_EXTM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RECODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_RECODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_RECODICE2)
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RECODICE2 = NVL(_Link_.RECODICE,space(15))
      this.w_REDESCRI2 = NVL(_Link_.REDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_RECODICE2 = space(15)
      endif
      this.w_REDESCRI2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_RECODICE1) or (.w_RECODICE2>=.w_RECODICE1)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endif
        this.w_RECODICE2 = space(15)
        this.w_REDESCRI2 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_EXTM_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REG_EXTM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RECODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRE__TIPO_1_1.RadioValue()==this.w_RE__TIPO)
      this.oPgFrm.Page1.oPag.oRE__TIPO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRECODICE1_1_2.value==this.w_RECODICE1)
      this.oPgFrm.Page1.oPag.oRECODICE1_1_2.value=this.w_RECODICE1
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI1_1_3.value==this.w_REDESCRI1)
      this.oPgFrm.Page1.oPag.oREDESCRI1_1_3.value=this.w_REDESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oRECODICE2_1_4.value==this.w_RECODICE2)
      this.oPgFrm.Page1.oPag.oRECODICE2_1_4.value=this.w_RECODICE2
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI2_1_5.value==this.w_REDESCRI2)
      this.oPgFrm.Page1.oPag.oREDESCRI2_1_5.value=this.w_REDESCRI2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_RECODICE2) or (.w_RECODICE2>=.w_RECODICE1))  and not(empty(.w_RECODICE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECODICE1_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
          case   not(empty(.w_RECODICE1) or (.w_RECODICE2>=.w_RECODICE1))  and not(empty(.w_RECODICE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRECODICE2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del finale o errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_srePag1 as StdContainer
  Width  = 492
  height = 194
  stdWidth  = 492
  stdheight = 194
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oRE__TIPO_1_1 as StdCombo with uid="ZKKUZFCHIJ",rtseq=1,rtrep=.f.,left=101,top=13,width=147,height=21;
    , ToolTipText = "Tipo di movim. extracontabili generati (contabilit� generale o analitica)";
    , HelpContextID = 138097765;
    , cFormVar="w_RE__TIPO",RowSource=""+"Contab. generale,"+"Contab. analitica,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRE__TIPO_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    space(10)))))
  endfunc
  func oRE__TIPO_1_1.GetRadio()
    this.Parent.oContained.w_RE__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oRE__TIPO_1_1.SetRadio()
    this.Parent.oContained.w_RE__TIPO=trim(this.Parent.oContained.w_RE__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_RE__TIPO=='C',1,;
      iif(this.Parent.oContained.w_RE__TIPO=='A',2,;
      iif(this.Parent.oContained.w_RE__TIPO=='T',3,;
      0)))
  endfunc

  add object oRECODICE1_1_2 as StdField with uid="EFJOOMXOMD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RECODICE1", cQueryName = "RECODICE1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato",;
    ToolTipText = "Codice regola di elaborazione di partenza",;
    HelpContextID = 120158059,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=101, Top=50, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="REG_EXTM", oKey_1_1="RECODICE", oKey_1_2="this.w_RECODICE1"

  func oRECODICE1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRECODICE1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRECODICE1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_EXTM','*','RECODICE',cp_AbsName(this.parent,'oRECODICE1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Regole di elaborazione",'regela.REG_EXTM_VZM',this.parent.oContained
  endproc

  add object oREDESCRI1_1_3 as StdField with uid="RAEGUTGFCK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_REDESCRI1", cQueryName = "REDESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del codice",;
    HelpContextID = 34572143,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=220, Top=49, InputMask=replicate('X',35)

  add object oRECODICE2_1_4 as StdField with uid="LJFLWSOSPD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RECODICE2", cQueryName = "RECODICE2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del finale o errato",;
    ToolTipText = "Codice regola di elaborazione di arrivo",;
    HelpContextID = 120158075,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=101, Top=83, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="REG_EXTM", oKey_1_1="RECODICE", oKey_1_2="this.w_RECODICE2"

  func oRECODICE2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oRECODICE2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRECODICE2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_EXTM','*','RECODICE',cp_AbsName(this.parent,'oRECODICE2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Regole di elaborazione",'regela.REG_EXTM_VZM',this.parent.oContained
  endproc

  add object oREDESCRI2_1_5 as StdField with uid="WNMMDJBKKP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_REDESCRI2", cQueryName = "REDESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione del codice",;
    HelpContextID = 34572159,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=220, Top=83, InputMask=replicate('X',35)


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=101, top=115, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 70385894


  add object oBtn_1_11 as StdButton with uid="WZFWVQXIDZ",left=379, top=144, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 34177830;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="EFZTAPUEYA",left=437, top=144, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100294330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_6 as StdString with uid="HHMAASSBJL",Visible=.t., Left=11, Top=51,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="DKGWAKMIVF",Visible=.t., Left=11, Top=85,;
    Alignment=1, Width=86, Height=18,;
    Caption="a codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="EKPKQLPLTJ",Visible=.t., Left=11, Top=117,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="GRELTTSIBE",Visible=.t., Left=11, Top=14,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sre','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
