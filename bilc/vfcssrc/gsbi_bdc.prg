* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bdc                                                        *
*              Eventi al cambio voce descrittiva                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_123]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2000-09-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bdc",oParentObject,m.pParam)
return(i_retval)

define class tgsbi_bdc as StdBatch
  * --- Local variables
  pParam = space(10)
  * --- WorkFile variables
  VOCDANAC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se cambio la voce in descrittiva devo cancellare il figlio integrato e le righe di dettaglio dall'archivio
    if this.oParentObject.w_VRFLDESC = "D" or this.oParentObject.w_VRFLDESC = "R"
      if this.pParam = "CHG"
        * --- Cancello le righe nel figlio integrato
        DELETE FROM ( this.oParentObject.GSBI_MDC.cTrsName )
        this.oParentObject.GSBI_MDC.BlankRec()
        this.oParentObject.GSBI_MDC.refresh
      else
        * --- Cancello dalla tabella le righe di dettaglio
        * --- Delete from VOCDANAC
        i_nConn=i_TableProp[this.VOCDANAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCDANAC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DVCODICE = "+cp_ToStrODBC(this.oParentObject.w_VRCODVOC);
                 )
        else
          delete from (i_cTable) where;
                DVCODICE = this.oParentObject.w_VRCODVOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    * --- Modifico la variabile del figlio per rendere non editabile il bottone
    this.oParentObject.GSBI_MDC.w_VRFLDESC=this.oParentObject.w_VRFLDESC
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VOCDANAC'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
