* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_scb                                                        *
*              Confronto tra bilanci contabili                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_55]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2013-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_scb",oParentObject))

* --- Class definition
define class tgsbi_scb as StdForm
  Top    = 12
  Left   = 54

  * --- Standard Properties
  Width  = 654
  Height = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-21"
  HelpContextID=90834537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  STR_ANAC_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsbi_scb"
  cComment = "Confronto tra bilanci contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPBILAN = space(1)
  w_CODAZI = space(10)
  w_STATO = space(1)
  w_STRUTTUR = space(15)
  w_NUMERO = 0
  o_NUMERO = 0
  w_ESERCI = space(4)
  o_ESERCI = space(4)
  w_DESCRIBI = space(35)
  w_PERIODO = space(15)
  w_DESCRI = space(40)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_TIPOMOV = space(20)
  w_VALUTA = space(3)
  w_NUMBIL1 = 0
  w_NUMERO2 = 0
  w_ESERCI2 = space(4)
  w_DESCRIB2 = space(35)
  w_PERIODO2 = space(15)
  w_VALAPP2 = space(3)
  w_TIPOMOV2 = space(20)
  w_MONETE = space(1)
  o_MONETE = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_SIMVAL = space(5)
  w_CAMVAL = 0
  w_DECIMI = 0
  w_TIPOSTAM = space(1)
  w_RIGHESI = space(1)
  w_DTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_scbPag1","gsbi_scb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTATO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='STR_ANAC'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='VALUTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSBI_BC2 with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPBILAN=space(1)
      .w_CODAZI=space(10)
      .w_STATO=space(1)
      .w_STRUTTUR=space(15)
      .w_NUMERO=0
      .w_ESERCI=space(4)
      .w_DESCRIBI=space(35)
      .w_PERIODO=space(15)
      .w_DESCRI=space(40)
      .w_VALAPP=space(3)
      .w_TIPOMOV=space(20)
      .w_VALUTA=space(3)
      .w_NUMBIL1=0
      .w_NUMERO2=0
      .w_ESERCI2=space(4)
      .w_DESCRIB2=space(35)
      .w_PERIODO2=space(15)
      .w_VALAPP2=space(3)
      .w_TIPOMOV2=space(20)
      .w_MONETE=space(1)
      .w_DIVISA=space(3)
      .w_SIMVAL=space(5)
      .w_CAMVAL=0
      .w_DECIMI=0
      .w_TIPOSTAM=space(1)
      .w_RIGHESI=space(1)
      .w_DTOBSO=ctod("  /  /  ")
        .w_TIPBILAN = 'G'
        .w_CODAZI = I_CODAZI
        .w_STATO = 'S'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_ESERCI))
          .link_1_7('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
          .DoRTCalc(7,11,.f.)
        .w_VALUTA = g_perval
        .w_NUMBIL1 = .w_NUMERO
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_ESERCI2))
          .link_1_32('Full')
        endif
          .DoRTCalc(16,19,.f.)
        .w_MONETE = 'c'
        .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_divisa)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_DIVISA))
          .link_1_47('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_CAMVAL = GETCAM(.w_DIVISA, i_DATSYS, 7)
          .DoRTCalc(24,24,.f.)
        .w_TIPOSTAM = 'S'
          .DoRTCalc(26,26,.f.)
        .w_DTOBSO = i_datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_1_7('Full')
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .DoRTCalc(7,12,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_NUMBIL1 = .w_NUMERO
        endif
        .DoRTCalc(14,14,.t.)
        if .o_ESERCI<>.w_ESERCI
          .link_1_32('Full')
        endif
        .DoRTCalc(16,20,.t.)
        if .o_MONETE<>.w_MONETE.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALUTA,.w_divisa)
          .link_1_47('Full')
        endif
        .DoRTCalc(22,22,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMVAL = GETCAM(.w_DIVISA, i_DATSYS, 7)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(24,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDIVISA_1_47.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_47.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_50.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oDIVISA_1_47.visible=!this.oPgFrm.Page1.oPag.oDIVISA_1_47.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_48.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_50.visible=!this.oPgFrm.Page1.oPag.oCAMVAL_1_50.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSC',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_4'),i_cWhere,'GSBI_MSC',"Strutture di bilancio contabile",'gsbi_agb.STR_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI2
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI2);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCI2)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI2 = NVL(_Link_.ESCODESE,space(4))
      this.w_VALAPP2 = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI2 = space(4)
      endif
      this.w_VALAPP2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_47'),i_cWhere,'',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECIMI = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECIMI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_3.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_4.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_4.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_6.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_6.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI_1_7.value==this.w_ESERCI)
      this.oPgFrm.Page1.oPag.oESERCI_1_7.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIBI_1_8.value==this.w_DESCRIBI)
      this.oPgFrm.Page1.oPag.oDESCRIBI_1_8.value=this.w_DESCRIBI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_9.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_9.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_13.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_13.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oVALAPP_1_21.value==this.w_VALAPP)
      this.oPgFrm.Page1.oPag.oVALAPP_1_21.value=this.w_VALAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOMOV_1_23.value==this.w_TIPOMOV)
      this.oPgFrm.Page1.oPag.oTIPOMOV_1_23.value=this.w_TIPOMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO2_1_31.value==this.w_NUMERO2)
      this.oPgFrm.Page1.oPag.oNUMERO2_1_31.value=this.w_NUMERO2
    endif
    if not(this.oPgFrm.Page1.oPag.oESERCI2_1_32.value==this.w_ESERCI2)
      this.oPgFrm.Page1.oPag.oESERCI2_1_32.value=this.w_ESERCI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIB2_1_33.value==this.w_DESCRIB2)
      this.oPgFrm.Page1.oPag.oDESCRIB2_1_33.value=this.w_DESCRIB2
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO2_1_34.value==this.w_PERIODO2)
      this.oPgFrm.Page1.oPag.oPERIODO2_1_34.value=this.w_PERIODO2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALAPP2_1_39.value==this.w_VALAPP2)
      this.oPgFrm.Page1.oPag.oVALAPP2_1_39.value=this.w_VALAPP2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOMOV2_1_41.value==this.w_TIPOMOV2)
      this.oPgFrm.Page1.oPag.oTIPOMOV2_1_41.value=this.w_TIPOMOV2
    endif
    if not(this.oPgFrm.Page1.oPag.oMONETE_1_45.RadioValue()==this.w_MONETE)
      this.oPgFrm.Page1.oPag.oMONETE_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_47.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_47.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_48.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_48.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_50.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_50.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSTAM_1_54.RadioValue()==this.w_TIPOSTAM)
      this.oPgFrm.Page1.oPag.oTIPOSTAM_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRIGHESI_1_55.RadioValue()==this.w_RIGHESI)
      this.oPgFrm.Page1.oPag.oRIGHESI_1_55.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_STRUTTUR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSTRUTTUR_1_4.SetFocus()
            i_bnoObbl = !empty(.w_STRUTTUR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIVISA))  and not(.w_MONETE='c')  and (.w_monete<>'c')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_47.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and not(EMPTY(.w_DIVISA) OR .w_MONETE='c')  and (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_50.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsbi_scb
      IF .w_NUMERO=.w_NUMERO2 AND .w_ESERCI=.w_ESERCI2
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=ah_MsgFormat("Inserire bilanci diversi di confronto")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NUMERO = this.w_NUMERO
    this.o_ESERCI = this.w_ESERCI
    this.o_VALAPP = this.w_VALAPP
    this.o_MONETE = this.w_MONETE
    this.o_DIVISA = this.w_DIVISA
    return

enddefine

* --- Define pages as container
define class tgsbi_scbPag1 as StdContainer
  Width  = 650
  height = 411
  stdWidth  = 650
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTATO_1_3 as StdCombo with uid="DLKTHEMZYT",value=3,rtseq=3,rtrep=.f.,left=65,top=34,width=114,height=21;
    , ToolTipText = "Stato bilancio selezionato";
    , HelpContextID = 2202842;
    , cFormVar="w_STATO",RowSource=""+"Confermato,"+"Da confermare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oSTATO_1_3.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_3.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='S',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='',3,;
      0)))
  endfunc

  add object oSTRUTTUR_1_4 as StdField with uid="GNCWNDZXJN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 70284152,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=247, Top=34, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", cZoomOnZoom="GSBI_MSC", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSC',"Strutture di bilancio contabile",'gsbi_agb.STR_ANAC_VZM',this.parent.oContained
  endproc
  proc oSTRUTTUR_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR
     i_obj.ecpSave()
  endproc


  add object oBtn_1_5 as StdButton with uid="XRNYXTQWIN",left=154, top=81, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 90633514;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      do GSBI_KC1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_STRUTTUR))
     endwith
    endif
  endfunc

  add object oNUMERO_1_6 as StdField with uid="HMUYMFNDRN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16768042,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=65, Top=115

  add object oESERCI_1_7 as StdField with uid="YPKXEWOTGV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 132341434,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=192, Top=115, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCRIBI_1_8 as StdField with uid="OEJLFXYVVE",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRIBI", cQueryName = "DESCRIBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117542017,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=334, Top=115, InputMask=replicate('X',35)

  add object oPERIODO_1_9 as StdField with uid="LGXQXJNMVJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 204184586,;
   bGlobalFont=.t.,;
    Height=21, Width=162, Left=65, Top=150, InputMask=replicate('X',15)

  add object oDESCRI_1_13 as StdField with uid="HNCRARAVAU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 117542090,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=365, Top=34, InputMask=replicate('X',40)

  add object oVALAPP_1_21 as StdField with uid="KSSLHHLCZW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_VALAPP", cQueryName = "VALAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 2359210,;
   bGlobalFont=.t.,;
    Height=21, Width=35, Left=608, Top=115, InputMask=replicate('X',3)

  add object oTIPOMOV_1_23 as StdField with uid="LAZUIPLVML",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TIPOMOV", cQueryName = "TIPOMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 247089206,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=334, Top=150, InputMask=replicate('X',20)


  add object oObj_1_25 as cp_runprogram with uid="PJRUCEPFIT",left=732, top=155, width=158,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BC1',;
    cEvent = "w_STATO Changed,w_STRUTTUR Changed",;
    nPag=1;
    , HelpContextID = 87163110


  add object oBtn_1_26 as StdButton with uid="NCRARATDAM",left=154, top=189, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 90633514;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      do GSBI_KC2 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_NUMERO))
     endwith
    endif
  endfunc

  add object oNUMERO2_1_31 as StdField with uid="XXNKLEVPGB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NUMERO2", cQueryName = "NUMERO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16768042,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=65, Top=225

  add object oESERCI2_1_32 as StdField with uid="PUDLWZWTLK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ESERCI2", cQueryName = "ESERCI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 132341434,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=192, Top=225, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI2"

  func oESERCI2_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCRIB2_1_33 as StdField with uid="XTPNACEQHB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRIB2", cQueryName = "DESCRIB2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117542040,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=334, Top=225, InputMask=replicate('X',35)

  add object oPERIODO2_1_34 as StdField with uid="LTODMPSKQZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PERIODO2", cQueryName = "PERIODO2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 204184536,;
   bGlobalFont=.t.,;
    Height=21, Width=162, Left=65, Top=260, InputMask=replicate('X',15)

  add object oVALAPP2_1_39 as StdField with uid="FELKTMNHIF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VALAPP2", cQueryName = "VALAPP2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 2359210,;
   bGlobalFont=.t.,;
    Height=21, Width=35, Left=608, Top=225, InputMask=replicate('X',3)

  add object oTIPOMOV2_1_41 as StdField with uid="HVDGJKJDOY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TIPOMOV2", cQueryName = "TIPOMOV2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 247089256,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=334, Top=260, InputMask=replicate('X',20)


  add object oMONETE_1_45 as StdCombo with uid="LYFFHPNXXI",rtseq=20,rtrep=.f.,left=113,top=311,width=122,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 182440506;
    , cFormVar="w_MONETE",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMONETE_1_45.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func oMONETE_1_45.GetRadio()
    this.Parent.oContained.w_MONETE = this.RadioValue()
    return .t.
  endfunc

  func oMONETE_1_45.SetRadio()
    this.Parent.oContained.w_MONETE=trim(this.Parent.oContained.w_MONETE)
    this.value = ;
      iif(this.Parent.oContained.w_MONETE=='c',1,;
      iif(this.Parent.oContained.w_MONETE=='a',2,;
      0))
  endfunc

  add object oDIVISA_1_47 as StdField with uid="ODUKOHLZRR",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 250304714,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=307, Top=311, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete<>'c')
    endwith
   endif
  endfunc

  func oDIVISA_1_47.mHide()
    with this.Parent.oContained
      return (.w_MONETE='c')
    endwith
  endfunc

  func oDIVISA_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Valute",'',this.parent.oContained
  endproc

  add object oSIMVAL_1_48 as StdField with uid="KDDISATZQE",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 83814362,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=370, Top=311, InputMask=replicate('X',5)

  func oSIMVAL_1_48.mHide()
    with this.Parent.oContained
      return (.w_MONETE='c')
    endwith
  endfunc

  add object oCAMVAL_1_50 as StdField with uid="YNLUTGLKRB",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 83816666,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=495, Top=311, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MONETE='a' and (FASETRAN(i_DATSYS)=0 ))
    endwith
   endif
  endfunc

  func oCAMVAL_1_50.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_MONETE='c')
    endwith
  endfunc


  add object oBtn_1_51 as StdButton with uid="HUTTWUEDUL",left=540, top=360, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 90805786;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NUMERO) AND NOT EMPTY(.w_NUMERO2))
      endwith
    endif
  endfunc


  add object oTIPOSTAM_1_54 as StdCombo with uid="IHHNQGQJIX",rtseq=25,rtrep=.f.,left=113,top=349,width=142,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 199604093;
    , cFormVar="w_TIPOSTAM",RowSource=""+"Stampa su Excel,"+"Stampa standard", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOSTAM_1_54.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPOSTAM_1_54.GetRadio()
    this.Parent.oContained.w_TIPOSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSTAM_1_54.SetRadio()
    this.Parent.oContained.w_TIPOSTAM=trim(this.Parent.oContained.w_TIPOSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSTAM=='E',1,;
      iif(this.Parent.oContained.w_TIPOSTAM=='S',2,;
      0))
  endfunc

  add object oRIGHESI_1_55 as StdCheck with uid="JXOWBSNIFQ",rtseq=26,rtrep=.f.,left=277, top=349, caption="Righe significative",;
    ToolTipText = "Stampa solo righe valorizzate",;
    HelpContextID = 36878358,;
    cFormVar="w_RIGHESI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRIGHESI_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oRIGHESI_1_55.GetRadio()
    this.Parent.oContained.w_RIGHESI = this.RadioValue()
    return .t.
  endfunc

  func oRIGHESI_1_55.SetRadio()
    this.Parent.oContained.w_RIGHESI=trim(this.Parent.oContained.w_RIGHESI)
    this.value = ;
      iif(this.Parent.oContained.w_RIGHESI=='S',1,;
      0)
  endfunc


  add object oBtn_1_57 as StdButton with uid="HMLRBFRNYB",left=594, top=360, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83517114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="JJBMFWBNCV",Visible=.t., Left=6, Top=81,;
    Alignment=0, Width=72, Height=15,;
    Caption="1� Bilancio"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="LLORTBNCJF",Visible=.t., Left=6, Top=34,;
    Alignment=1, Width=57, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YQWGGANFPP",Visible=.t., Left=184, Top=34,;
    Alignment=1, Width=61, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="LTFDBGUWGM",Visible=.t., Left=4, Top=9,;
    Alignment=0, Width=62, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="LMUGRSAUSK",Visible=.t., Left=86, Top=81,;
    Alignment=0, Width=64, Height=15,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="VDRNNFWZTO",Visible=.t., Left=4, Top=115,;
    Alignment=1, Width=59, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="UIVEDOPEEN",Visible=.t., Left=121, Top=115,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WBHERUUVZI",Visible=.t., Left=251, Top=115,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="NRXDKQSUKD",Visible=.t., Left=4, Top=150,;
    Alignment=1, Width=59, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YJQWMYQYZU",Visible=.t., Left=553, Top=115,;
    Alignment=1, Width=55, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AYZHIRAZKM",Visible=.t., Left=241, Top=150,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VJIRXAFDJA",Visible=.t., Left=6, Top=191,;
    Alignment=0, Width=72, Height=15,;
    Caption="2� Bilancio"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QKHRMYSVFL",Visible=.t., Left=86, Top=191,;
    Alignment=0, Width=65, Height=15,;
    Caption="Selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="FPFMCSLITZ",Visible=.t., Left=3, Top=225,;
    Alignment=1, Width=60, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ACNROYPSIG",Visible=.t., Left=121, Top=225,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ZLEEXMJQKE",Visible=.t., Left=251, Top=225,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FUNZPMXIQV",Visible=.t., Left=5, Top=260,;
    Alignment=1, Width=58, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="WOJACLTHRC",Visible=.t., Left=555, Top=225,;
    Alignment=1, Width=53, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="IYXWVNIHSC",Visible=.t., Left=241, Top=260,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VFEQDKLIOU",Visible=.t., Left=4, Top=311,;
    Alignment=1, Width=104, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VJIRLJPYRQ",Visible=.t., Left=240, Top=311,;
    Alignment=1, Width=66, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_MONETE='c')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="YXAXCEKJYW",Visible=.t., Left=427, Top=311,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_DIVISA) OR .w_MONETE='c')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="NNBYUDJRBN",Visible=.t., Left=6, Top=349,;
    Alignment=1, Width=102, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="PDMLZTAUMJ",left=1, top=72, width=596,height=1

  add object oBox_1_43 as StdBox with uid="HIGETBNXTP",left=2, top=295, width=596,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_scb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
