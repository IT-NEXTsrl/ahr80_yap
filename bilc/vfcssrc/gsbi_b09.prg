* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b09                                                        *
*              Elabora coge primanota                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_63]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-20                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b09",oParentObject)
return(i_retval)

define class tgsbi_b09 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_SCARTO = 0
  w_COD003 = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_RISATT = space(15)
  w_RECODICE = space(15)
  w_DATREG = ctod("  /  /  ")
  w_RATATT = space(15)
  w_RISPAS = space(15)
  w_REDESCRI = space(45)
  w_RATPAS = space(15)
  w_FATTEMET = space(15)
  w_FATTRICE = space(15)
  w_TIPMAS = space(1)
  * --- WorkFile variables
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Primanota COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTCG001
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_FILINI = this.oParentObject.oParentObject.w_DATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_DATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_RISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_RISPAS
    this.w_RATATT = this.oParentObject.oParentObject.w_RATATT
    this.w_RATPAS = this.oParentObject.oParentObject.w_RATPAS
    if USED("EXTCG001")
      * --- Leggo le contropartite per Fatture da Emettere o Ricevere
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COFATEME,COFATRIC"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COFATEME,COFATRIC;
          from (i_cTable) where;
              COCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FATTEMET = NVL(cp_ToDate(_read_.COFATEME),cp_NullValue(_read_.COFATEME))
        this.w_FATTRICE = NVL(cp_ToDate(_read_.COFATRIC),cp_NullValue(_read_.COFATRIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), TOTIMP N(18,4), TIPMAS C(1))
      SELECT EXTCG001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "MG" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_TIPMAS = TIPMAS
      this.w_CODICE = CODICE
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_SCARTO = 0
      this.w_RISCON = 0
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, i_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, 1, i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      * --- 2^OPERAZIONE: Se presente una Competenza, calcola l'esatto Importo e eventuali Risconti
      if NOT EMPTY(this.w_INICOM) AND NOT EMPTY(this.w_FINCOM)
        if this.w_FINCOM<this.w_FILINI OR this.w_INICOM>this.w_FILFIN
          * --- Registrazione Totalmente fuori dal Periodo, Ignora e passa al record successivo
          LOOP
        else
          this.w_GIOCOM = (this.w_FINCOM+1) - this.w_INICOM
          * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
          if this.w_INICOM<this.w_FILINI AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FILINI - this.w_INICOM
            this.w_SCARTO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Registrazione con Competenza che Prosegue oltre il Periodo, elimina la parte non di competenza
          if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FINCOM - this.w_FILFIN
            this.w_SCARTO = this.w_SCARTO + cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Ricalcola l'Importo di Competenza
          this.w_TOTIMP = this.w_TOTIMP - this.w_SCARTO
        endif
      else
        * --- Non deve fare registrazione per le date registrazioni successive che non hanno data competenza
        LOOP
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      if this.w_TOTIMP<>0
        INSERT INTO TmpAgg1 (TIPCON, CODICE, TOTIMP, TIPMAS) ;
        VALUES (this.w_TIPCON, this.w_CODICE, this.w_TOTIMP, NVL(this.w_TIPMAS, "N"))
      endif
      SELECT EXTCG001
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTCG001
      USE
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("TmpAgg1")
        SELECT tmpAgg1
        if RECCOUNT()>0
          SELECT TIPCON, CODICE, SUM(TOTIMP) AS TOTALE, MIN(TIPMAS) AS TIPMAS FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY TIPCON, CODICE
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_COD001 = SPACE(15)
          * --- TIPMAS del cursore contiene C o F per essere utilizzato nella scelta del conto
          * --- fatture da emettere o ricevere
          this.w_TIPMAS = "G"
          this.w_TIPCON = "G"
          this.w_COD002 = CODICE
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
          * --- Assegno il conto Fatture da Emettere se l'importo � avere, da Ricevere Viceversa
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TOTALE,0)<>0
          this.w_TIPCON = "G"
          this.w_COD001 = SPACE(15)
          this.w_COD003 = SPACE(15)
          * --- Metto Fatture d emettere se il tipo mastro � clienti, Fattura da ricevere se fornitore
          this.w_COD002 = IIF(TIPMAS = "C", this.w_FATTEMET, this.w_FATTRICE)
          this.w_IMPDAR = IIF(TOTALE<0, ABS(TOTALE), 0)
          this.w_IMPAVE = IIF(TOTALE>0, TOTALE, 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTROPA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
