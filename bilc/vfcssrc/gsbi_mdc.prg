* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_mdc                                                        *
*              Dett.voci di ricl.mastri-conti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_66]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2013-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsbi_mdc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsbi_mdc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsbi_mdc")
  return

* --- Class definition
define class tgsbi_mdc as StdPCForm
  Width  = 768
  Height = 257
  Top    = 2
  Left   = 4
  cComment = "Dett.voci di ricl.mastri-conti"
  cPrg = "gsbi_mdc"
  HelpContextID=80348777
  add object cnt as tcgsbi_mdc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsbi_mdc as PCContext
  w_DTOBSO = space(8)
  w_DVCODICE = space(15)
  w_VRFLDESC = space(1)
  w_OK = space(1)
  w_MESS = space(50)
  w_CPROWORD = 0
  w_DVFLMACO = space(1)
  w_DVCODCON = space(15)
  w_DVCODMAS = space(15)
  w_DVCODVOC = space(15)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DESVOC = space(50)
  w_NUMLIV = 0
  w_OBTEST = space(8)
  w_DESCRI = space(40)
  w_DVFORMUL = space(30)
  w_DVFLOPER = space(1)
  w_DVFLPARE = space(2)
  w_DVCONDIZ = space(20)
  w_DATAOBSO = space(8)
  w_DATAOBS1 = space(8)
  proc Save(i_oFrom)
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_DVCODICE = i_oFrom.w_DVCODICE
    this.w_VRFLDESC = i_oFrom.w_VRFLDESC
    this.w_OK = i_oFrom.w_OK
    this.w_MESS = i_oFrom.w_MESS
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DVFLMACO = i_oFrom.w_DVFLMACO
    this.w_DVCODCON = i_oFrom.w_DVCODCON
    this.w_DVCODMAS = i_oFrom.w_DVCODMAS
    this.w_DVCODVOC = i_oFrom.w_DVCODVOC
    this.w_DESMAS = i_oFrom.w_DESMAS
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_DESVOC = i_oFrom.w_DESVOC
    this.w_NUMLIV = i_oFrom.w_NUMLIV
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_DVFORMUL = i_oFrom.w_DVFORMUL
    this.w_DVFLOPER = i_oFrom.w_DVFLOPER
    this.w_DVFLPARE = i_oFrom.w_DVFLPARE
    this.w_DVCONDIZ = i_oFrom.w_DVCONDIZ
    this.w_DATAOBSO = i_oFrom.w_DATAOBSO
    this.w_DATAOBS1 = i_oFrom.w_DATAOBS1
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_DVCODICE = this.w_DVCODICE
    i_oTo.w_VRFLDESC = this.w_VRFLDESC
    i_oTo.w_OK = this.w_OK
    i_oTo.w_MESS = this.w_MESS
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DVFLMACO = this.w_DVFLMACO
    i_oTo.w_DVCODCON = this.w_DVCODCON
    i_oTo.w_DVCODMAS = this.w_DVCODMAS
    i_oTo.w_DVCODVOC = this.w_DVCODVOC
    i_oTo.w_DESMAS = this.w_DESMAS
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_DESVOC = this.w_DESVOC
    i_oTo.w_NUMLIV = this.w_NUMLIV
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_DVFORMUL = this.w_DVFORMUL
    i_oTo.w_DVFLOPER = this.w_DVFLOPER
    i_oTo.w_DVFLPARE = this.w_DVFLPARE
    i_oTo.w_DVCONDIZ = this.w_DVCONDIZ
    i_oTo.w_DATAOBSO = this.w_DATAOBSO
    i_oTo.w_DATAOBS1 = this.w_DATAOBS1
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsbi_mdc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 768
  Height = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-05"
  HelpContextID=80348777
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VOCDANAC_IDX = 0
  MASTRI_IDX = 0
  CONTI_IDX = 0
  VOC_ANAC_IDX = 0
  cFile = "VOCDANAC"
  cKeySelect = "DVCODICE"
  cKeyWhere  = "DVCODICE=this.w_DVCODICE"
  cKeyDetail  = "DVCODICE=this.w_DVCODICE"
  cKeyWhereODBC = '"DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';

  cKeyDetailWhereODBC = '"DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"VOCDANAC.DVCODICE="+cp_ToStrODBC(this.w_DVCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VOCDANAC.CPROWORD '
  cPrg = "gsbi_mdc"
  cComment = "Dett.voci di ricl.mastri-conti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DTOBSO = ctod('  /  /  ')
  w_DVCODICE = space(15)
  w_VRFLDESC = space(1)
  w_OK = .F.
  w_MESS = space(50)
  w_CPROWORD = 0
  w_DVFLMACO = space(1)
  o_DVFLMACO = space(1)
  w_DVCODCON = space(15)
  w_DVCODMAS = space(15)
  w_DVCODVOC = space(15)
  w_DESMAS = space(40)
  w_DESCON = space(40)
  w_DESVOC = space(50)
  w_NUMLIV = 0
  w_OBTEST = ctod('  /  /  ')
  w_DESCRI = space(40)
  w_DVFORMUL = space(30)
  w_DVFLOPER = space(1)
  w_DVFLPARE = space(2)
  w_DVCONDIZ = space(20)
  w_DATAOBSO = ctod('  /  /  ')
  w_DATAOBS1 = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_mdcPag1","gsbi_mdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='VOC_ANAC'
    this.cWorkTables[4]='VOCDANAC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VOCDANAC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VOCDANAC_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsbi_mdc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VOCDANAC where DVCODICE=KeySet.DVCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2],this.bLoadRecFilter,this.VOCDANAC_IDX,"gsbi_mdc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VOCDANAC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VOCDANAC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VOCDANAC '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DVCODICE',this.w_DVCODICE  )
      select * from (i_cTable) VOCDANAC where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTOBSO = i_datsys
        .w_VRFLDESC = this.oParentObject .w_VRFLDESC
        .w_OK = .T.
        .w_MESS = space(50)
        .w_DVCODICE = NVL(DVCODICE,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'VOCDANAC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESMAS = space(40)
          .w_DESCON = space(40)
          .w_DESVOC = space(50)
          .w_NUMLIV = 0
        .w_OBTEST = I_DATSYS
          .w_DATAOBSO = ctod("  /  /  ")
          .w_DATAOBS1 = ctod("  /  /  ")
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DVFLMACO = NVL(DVFLMACO,space(1))
          .w_DVCODCON = NVL(DVCODCON,space(15))
          .link_2_3('Load')
          .w_DVCODMAS = NVL(DVCODMAS,space(15))
          if link_2_4_joined
            this.w_DVCODMAS = NVL(MCCODICE204,NVL(this.w_DVCODMAS,space(15)))
            this.w_DESMAS = NVL(MCDESCRI204,space(40))
            this.w_NUMLIV = NVL(MCNUMLIV204,0)
          else
          .link_2_4('Load')
          endif
          .w_DVCODVOC = NVL(DVCODVOC,space(15))
          if link_2_5_joined
            this.w_DVCODVOC = NVL(VRCODVOC205,NVL(this.w_DVCODVOC,space(15)))
            this.w_DESVOC = NVL(VRDESVOC205,space(50))
            this.w_DATAOBS1 = NVL(cp_ToDate(VRDTOBSO205),ctod("  /  /  "))
          else
          .link_2_5('Load')
          endif
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, IIF(.w_DVFLMACO='V',.w_DESVOC,SPACE(40))))
          .w_DVFORMUL = NVL(DVFORMUL,space(30))
          .w_DVFLOPER = NVL(DVFLOPER,space(1))
          .w_DVFLPARE = NVL(DVFLPARE,space(2))
          .w_DVCONDIZ = NVL(DVCONDIZ,space(20))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DTOBSO=ctod("  /  /  ")
      .w_DVCODICE=space(15)
      .w_VRFLDESC=space(1)
      .w_OK=.f.
      .w_MESS=space(50)
      .w_CPROWORD=10
      .w_DVFLMACO=space(1)
      .w_DVCODCON=space(15)
      .w_DVCODMAS=space(15)
      .w_DVCODVOC=space(15)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DESVOC=space(50)
      .w_NUMLIV=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_DVFORMUL=space(30)
      .w_DVFLOPER=space(1)
      .w_DVFLPARE=space(2)
      .w_DVCONDIZ=space(20)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DATAOBS1=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_DTOBSO = i_datsys
        .DoRTCalc(2,2,.f.)
        .w_VRFLDESC = this.oParentObject .w_VRFLDESC
        .w_OK = .T.
        .DoRTCalc(5,7,.f.)
        .w_DVCODCON = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DVCODCON))
         .link_2_3('Full')
        endif
        .w_DVCODMAS = SPACE(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DVCODMAS))
         .link_2_4('Full')
        endif
        .w_DVCODVOC = SPACE(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_DVCODVOC))
         .link_2_5('Full')
        endif
        .DoRTCalc(11,14,.f.)
        .w_OBTEST = I_DATSYS
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, IIF(.w_DVFLMACO='V',.w_DESVOC,SPACE(40))))
        .DoRTCalc(17,17,.f.)
        .w_DVFLOPER = '+'
        .w_DVFLPARE = '  '
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VOCDANAC')
    this.DoRTCalc(20,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'VOCDANAC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DVCODICE,"DVCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_DVFLMACO N(3);
      ,t_DVCODCON C(15);
      ,t_DVCODMAS C(15);
      ,t_DVCODVOC C(15);
      ,t_DESCRI C(40);
      ,t_DVFORMUL C(30);
      ,t_DVFLOPER N(3);
      ,t_DVFLPARE N(3);
      ,t_DVCONDIZ C(20);
      ,CPROWNUM N(10);
      ,t_DESMAS C(40);
      ,t_DESCON C(40);
      ,t_DESVOC C(50);
      ,t_NUMLIV N(1);
      ,t_OBTEST D(8);
      ,t_DATAOBSO D(8);
      ,t_DATAOBS1 D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsbi_mdcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.controlsource=this.cTrsName+'.t_DVFLMACO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.controlsource=this.cTrsName+'.t_DVCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_4.controlsource=this.cTrsName+'.t_DVCODMAS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_5.controlsource=this.cTrsName+'.t_DVCODVOC'
    this.oPgFRm.Page1.oPag.oDESCRI_2_11.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_12.controlsource=this.cTrsName+'.t_DVFORMUL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.controlsource=this.cTrsName+'.t_DVFLOPER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.controlsource=this.cTrsName+'.t_DVFLPARE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_15.controlsource=this.cTrsName+'.t_DVCONDIZ'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(42)
    this.AddVLine(78)
    this.AddVLine(203)
    this.AddVLine(328)
    this.AddVLine(453)
    this.AddVLine(597)
    this.AddVLine(634)
    this.AddVLine(669)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2])
      *
      * insert into VOCDANAC
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VOCDANAC')
        i_extval=cp_InsertValODBCExtFlds(this,'VOCDANAC')
        i_cFldBody=" "+;
                  "(DVCODICE,CPROWORD,DVFLMACO,DVCODCON,DVCODMAS"+;
                  ",DVCODVOC,DVFORMUL,DVFLOPER,DVFLPARE,DVCONDIZ,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DVCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DVFLMACO)+","+cp_ToStrODBCNull(this.w_DVCODCON)+","+cp_ToStrODBCNull(this.w_DVCODMAS)+;
             ","+cp_ToStrODBCNull(this.w_DVCODVOC)+","+cp_ToStrODBC(this.w_DVFORMUL)+","+cp_ToStrODBC(this.w_DVFLOPER)+","+cp_ToStrODBC(this.w_DVFLPARE)+","+cp_ToStrODBC(this.w_DVCONDIZ)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VOCDANAC')
        i_extval=cp_InsertValVFPExtFlds(this,'VOCDANAC')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DVCODICE',this.w_DVCODICE)
        INSERT INTO (i_cTable) (;
                   DVCODICE;
                  ,CPROWORD;
                  ,DVFLMACO;
                  ,DVCODCON;
                  ,DVCODMAS;
                  ,DVCODVOC;
                  ,DVFORMUL;
                  ,DVFLOPER;
                  ,DVFLPARE;
                  ,DVCONDIZ;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DVCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_DVFLMACO;
                  ,this.w_DVCODCON;
                  ,this.w_DVCODMAS;
                  ,this.w_DVCODVOC;
                  ,this.w_DVFORMUL;
                  ,this.w_DVFLOPER;
                  ,this.w_DVFLPARE;
                  ,this.w_DVCONDIZ;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DVCODMAS+t_DVCODCON+t_DVCODVOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VOCDANAC')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VOCDANAC')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DVCODMAS+t_DVCODCON+t_DVCODVOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VOCDANAC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VOCDANAC')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DVFLMACO="+cp_ToStrODBC(this.w_DVFLMACO)+;
                     ",DVCODCON="+cp_ToStrODBCNull(this.w_DVCODCON)+;
                     ",DVCODMAS="+cp_ToStrODBCNull(this.w_DVCODMAS)+;
                     ",DVCODVOC="+cp_ToStrODBCNull(this.w_DVCODVOC)+;
                     ",DVFORMUL="+cp_ToStrODBC(this.w_DVFORMUL)+;
                     ",DVFLOPER="+cp_ToStrODBC(this.w_DVFLOPER)+;
                     ",DVFLPARE="+cp_ToStrODBC(this.w_DVFLPARE)+;
                     ",DVCONDIZ="+cp_ToStrODBC(this.w_DVCONDIZ)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VOCDANAC')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DVFLMACO=this.w_DVFLMACO;
                     ,DVCODCON=this.w_DVCODCON;
                     ,DVCODMAS=this.w_DVCODMAS;
                     ,DVCODVOC=this.w_DVCODVOC;
                     ,DVFORMUL=this.w_DVFORMUL;
                     ,DVFLOPER=this.w_DVFLOPER;
                     ,DVFLPARE=this.w_DVFLPARE;
                     ,DVCONDIZ=this.w_DVCONDIZ;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DVCODMAS+t_DVCODCON+t_DVCODVOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VOCDANAC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DVCODMAS+t_DVCODCON+t_DVCODVOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VOCDANAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VOCDANAC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_DVFLMACO<>.w_DVFLMACO
          .w_DVCODCON = SPACE(15)
          .link_2_3('Full')
        endif
        if .o_DVFLMACO<>.w_DVFLMACO
          .w_DVCODMAS = SPACE(15)
          .link_2_4('Full')
        endif
        if .o_DVFLMACO<>.w_DVFLMACO
          .w_DVCODVOC = SPACE(15)
          .link_2_5('Full')
        endif
        .DoRTCalc(11,15,.t.)
          .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, IIF(.w_DVFLMACO='V',.w_DESVOC,SPACE(40))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESMAS with this.w_DESMAS
      replace t_DESCON with this.w_DESCON
      replace t_DESVOC with this.w_DESVOC
      replace t_NUMLIV with this.w_NUMLIV
      replace t_OBTEST with this.w_OBTEST
      replace t_DATAOBSO with this.w_DATAOBSO
      replace t_DATAOBS1 with this.w_DATAOBS1
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODCON_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODCON_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODMAS_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODMAS_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOC_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDVCODVOC_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DVCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_DVFLMACO;
                     ,'ANCODICE',trim(this.w_DVCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_DVCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_DVFLMACO);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDVCODCON_2_3'),i_cWhere,'GSAR_API',"Conti",'GSAR0MDV.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DVFLMACO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DVCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_DVFLMACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_DVFLMACO;
                       ,'ANCODICE',this.w_DVCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DVCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST) OR .cFunction='Query'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_DVCODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATAOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DVCODMAS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODMAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_DVCODMAS)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_DVCODMAS))
          select MCCODICE,MCDESCRI,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODMAS)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_DVCODMAS)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_DVCODMAS)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODMAS) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oDVCODMAS_2_4'),i_cWhere,'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODMAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_DVCODMAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_DVCODMAS)
            select MCCODICE,MCDESCRI,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODMAS = NVL(_Link_.MCCODICE,space(15))
      this.w_DESMAS = NVL(_Link_.MCDESCRI,space(40))
      this.w_NUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_DVCODMAS = space(15)
      endif
      this.w_DESMAS = space(40)
      this.w_NUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NUMLIV=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il mastro deve essere di livello 1")
        endif
        this.w_DVCODMAS = space(15)
        this.w_DESMAS = space(40)
        this.w_NUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODMAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.MCCODICE as MCCODICE204"+ ",link_2_4.MCDESCRI as MCDESCRI204"+ ",link_2_4.MCNUMLIV as MCNUMLIV204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on VOCDANAC.DVCODMAS=link_2_4.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and VOCDANAC.DVCODMAS=link_2_4.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DVCODVOC
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_ANAC_IDX,3]
    i_lTable = "VOC_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2], .t., this.VOC_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DVCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVC',True,'VOC_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_DVCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_DVCODVOC))
          select VRCODVOC,VRDESVOC,VRDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DVCODVOC)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VRDESVOC like "+cp_ToStrODBC(trim(this.w_DVCODVOC)+"%");

            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VRDESVOC like "+cp_ToStr(trim(this.w_DVCODVOC)+"%");

            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DVCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_ANAC','*','VRCODVOC',cp_AbsName(oSource.parent,'oDVCODVOC_2_5'),i_cWhere,'GSBI_AVC',"Voci di bilancio",'GSBI_MDC.VOC_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DVCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC,VRDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_DVCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_DVCODVOC)
            select VRCODVOC,VRDESVOC,VRDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DVCODVOC = NVL(_Link_.VRCODVOC,space(15))
      this.w_DESVOC = NVL(_Link_.VRDESVOC,space(50))
      this.w_DATAOBS1 = NVL(cp_ToDate(_Link_.VRDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DVCODVOC = space(15)
      endif
      this.w_DESVOC = space(50)
      this.w_DATAOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DVCODICE<>.w_DVCODVOC AND ((EMPTY(.w_DATAOBS1) OR .w_DATAOBS1>.w_OBTEST) OR .cFunction='Query')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La voce deve essere diversa dalla voce di riferimento o non obsoleta")
        endif
        this.w_DVCODVOC = space(15)
        this.w_DESVOC = space(50)
        this.w_DATAOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DVCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_ANAC_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.VRCODVOC as VRCODVOC205"+ ",link_2_5.VRDESVOC as VRDESVOC205"+ ",link_2_5.VRDTOBSO as VRDTOBSO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on VOCDANAC.DVCODVOC=link_2_5.VRCODVOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and VOCDANAC.DVCODVOC=link_2_5.VRCODVOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCRI_2_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_2_11.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oDESCRI_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.RadioValue()==this.w_DVFLMACO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.SetRadio()
      replace t_DVFLMACO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value==this.w_DVCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value=this.w_DVCODCON
      replace t_DVCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_4.value==this.w_DVCODMAS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_4.value=this.w_DVCODMAS
      replace t_DVCODMAS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_5.value==this.w_DVCODVOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_5.value=this.w_DVCODVOC
      replace t_DVCODVOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_12.value==this.w_DVFORMUL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_12.value=this.w_DVFORMUL
      replace t_DVFORMUL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFORMUL_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.RadioValue()==this.w_DVFLOPER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.SetRadio()
      replace t_DVFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.RadioValue()==this.w_DVFLPARE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.SetRadio()
      replace t_DVFLPARE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_15.value==this.w_DVCONDIZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_15.value=this.w_DVCONDIZ
      replace t_DVCONDIZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCONDIZ_2_15.value
    endif
    cp_SetControlsValueExtFlds(this,'VOCDANAC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_DVFLMACO $ 'MG' AND .w_VRFLDESC='N') OR (.w_DVFLMACO='V' AND .w_VRFLDESC='T')) and (NOT EMPTY(.w_DVCODMAS+.w_DVCODCON+.w_DVCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: M o C se voce riepilogativa e V per voce di totalizzazione")
        case   not((EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST) OR .cFunction='Query') and (.w_VRFLDESC='N' and .w_DVFLMACO='G') and not(empty(.w_DVCODCON)) and (NOT EMPTY(.w_DVCODMAS+.w_DVCODCON+.w_DVCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODCON_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
        case   not(.w_NUMLIV=1) and (.w_VRFLDESC='N' and .w_DVFLMACO='M') and not(empty(.w_DVCODMAS)) and (NOT EMPTY(.w_DVCODMAS+.w_DVCODCON+.w_DVCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODMAS_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il mastro deve essere di livello 1")
        case   not(.w_DVCODICE<>.w_DVCODVOC AND ((EMPTY(.w_DATAOBS1) OR .w_DATAOBS1>.w_OBTEST) OR .cFunction='Query')) and (.w_VRFLDESC='T' and .w_DVFLMACO='V') and not(empty(.w_DVCODVOC)) and (NOT EMPTY(.w_DVCODMAS+.w_DVCODCON+.w_DVCODVOC))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVCODVOC_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("La voce deve essere diversa dalla voce di riferimento o non obsoleta")
      endcase
      if NOT EMPTY(.w_DVCODMAS+.w_DVCODCON+.w_DVCODVOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DVFLMACO = this.w_DVFLMACO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DVCODMAS+t_DVCODCON+t_DVCODVOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_DVFLMACO=space(1)
      .w_DVCODCON=space(15)
      .w_DVCODMAS=space(15)
      .w_DVCODVOC=space(15)
      .w_DESMAS=space(40)
      .w_DESCON=space(40)
      .w_DESVOC=space(50)
      .w_NUMLIV=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_DVFORMUL=space(30)
      .w_DVFLOPER=space(1)
      .w_DVFLPARE=space(2)
      .w_DVCONDIZ=space(20)
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DATAOBS1=ctod("  /  /  ")
      .DoRTCalc(1,7,.f.)
        .w_DVCODCON = SPACE(15)
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_DVCODCON))
        .link_2_3('Full')
      endif
        .w_DVCODMAS = SPACE(15)
      .DoRTCalc(9,9,.f.)
      if not(empty(.w_DVCODMAS))
        .link_2_4('Full')
      endif
        .w_DVCODVOC = SPACE(15)
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_DVCODVOC))
        .link_2_5('Full')
      endif
      .DoRTCalc(11,14,.f.)
        .w_OBTEST = I_DATSYS
        .w_DESCRI = IIF(.w_DVFLMACO='M', .w_DESMAS, IIF(.w_DVFLMACO='G', .w_DESCON, IIF(.w_DVFLMACO='V',.w_DESVOC,SPACE(40))))
      .DoRTCalc(17,17,.f.)
        .w_DVFLOPER = '+'
        .w_DVFLPARE = '  '
    endwith
    this.DoRTCalc(20,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DVFLMACO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.RadioValue(.t.)
    this.w_DVCODCON = t_DVCODCON
    this.w_DVCODMAS = t_DVCODMAS
    this.w_DVCODVOC = t_DVCODVOC
    this.w_DESMAS = t_DESMAS
    this.w_DESCON = t_DESCON
    this.w_DESVOC = t_DESVOC
    this.w_NUMLIV = t_NUMLIV
    this.w_OBTEST = t_OBTEST
    this.w_DESCRI = t_DESCRI
    this.w_DVFORMUL = t_DVFORMUL
    this.w_DVFLOPER = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.RadioValue(.t.)
    this.w_DVFLPARE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.RadioValue(.t.)
    this.w_DVCONDIZ = t_DVCONDIZ
    this.w_DATAOBSO = t_DATAOBSO
    this.w_DATAOBS1 = t_DATAOBS1
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DVFLMACO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLMACO_2_2.ToRadio()
    replace t_DVCODCON with this.w_DVCODCON
    replace t_DVCODMAS with this.w_DVCODMAS
    replace t_DVCODVOC with this.w_DVCODVOC
    replace t_DESMAS with this.w_DESMAS
    replace t_DESCON with this.w_DESCON
    replace t_DESVOC with this.w_DESVOC
    replace t_NUMLIV with this.w_NUMLIV
    replace t_OBTEST with this.w_OBTEST
    replace t_DESCRI with this.w_DESCRI
    replace t_DVFORMUL with this.w_DVFORMUL
    replace t_DVFLOPER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLOPER_2_13.ToRadio()
    replace t_DVFLPARE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDVFLPARE_2_14.ToRadio()
    replace t_DVCONDIZ with this.w_DVCONDIZ
    replace t_DATAOBSO with this.w_DATAOBSO
    replace t_DATAOBS1 with this.w_DATAOBS1
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsbi_mdcPag1 as StdContainer
  Width  = 764
  height = 257
  stdWidth  = 764
  stdheight = 257
  resizeXpos=505
  resizeYpos=159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_7 as StdButton with uid="BTGHEPOPZV",left=709, top=211, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere il bottone per avviare la verifica della formula";
    , HelpContextID = 184976567;
    , tabstop=.f., caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSBI_BVC(this.Parent.oContained,"Cont")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC = 'N' OR .w_VRFLDESC = 'T')
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=3, width=757,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Seq.",Field2="DVFLMACO",Label2="Tipo",Field3="DVCODCON",Label3="Conto",Field4="DVCODMAS",Label4="Mastro",Field5="DVCODVOC",Label5="Voce",Field6="DVFORMUL",Label6="Formula",Field7="DVFLOPER",Label7="Oper.",Field8="DVFLPARE",Label8="Par.",Field9="DVCONDIZ",Label9="Condizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235742074

  add object oStr_1_3 as StdString with uid="BDZILNXUVJ",Visible=.t., Left=18, Top=221,;
    Alignment=1, Width=101, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=20,;
    width=753+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=21,width=752+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|MASTRI|VOC_ANAC|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCRI_2_11.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oDVCODCON_2_3
      case cFile='MASTRI'
        oDropInto=this.oBodyCol.oRow.oDVCODMAS_2_4
      case cFile='VOC_ANAC'
        oDropInto=this.oBodyCol.oRow.oDVCODVOC_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCRI_2_11 as StdTrsField with uid="IBEFCFAYGW",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 107056330,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=122, Top=221, InputMask=replicate('X',40)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsbi_mdcBodyRow as CPBodyRowCnt
  Width=743
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="ZUANFSPTII",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Sequenza di elaborazione",;
    HelpContextID = 268070506,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oDVFLMACO_2_2 as StdTrsCombo with uid="GACOMIDOVP",rtrep=.t.,;
    cFormVar="w_DVFLMACO", RowSource=""+"C = conto,"+"M = mastro,"+"V = altra voce" , ;
    ToolTipText = "Digitare: M =mastro o G =conto generico o V =voce",;
    HelpContextID = 22459525,;
    Height=21, Width=32, Left=39, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , sErrorMsg = "Inserire: M o C se voce riepilogativa e V per voce di totalizzazione";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDVFLMACO_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DVFLMACO,&i_cF..t_DVFLMACO),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'M',;
    iif(xVal =3,'V',;
    'G'))))
  endfunc
  func oDVFLMACO_2_2.GetRadio()
    this.Parent.oContained.w_DVFLMACO = this.RadioValue()
    return .t.
  endfunc

  func oDVFLMACO_2_2.ToRadio()
    this.Parent.oContained.w_DVFLMACO=trim(this.Parent.oContained.w_DVFLMACO)
    return(;
      iif(this.Parent.oContained.w_DVFLMACO=='G',1,;
      iif(this.Parent.oContained.w_DVFLMACO=='M',2,;
      iif(this.Parent.oContained.w_DVFLMACO=='V',3,;
      0))))
  endfunc

  func oDVFLMACO_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  proc oDVFLMACO_2_2.mDefault
    with this.Parent.oContained
      if empty(.w_DVFLMACO)
        .w_DVFLMACO = ''
      endif
    endwith
  endproc

  func oDVFLMACO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DVFLMACO $ 'MG' AND .w_VRFLDESC='N') OR (.w_DVFLMACO='V' AND .w_VRFLDESC='T'))
      if .not. empty(.w_DVCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDVCODCON_2_3 as StdTrsField with uid="FRRVVSBFYE",rtseq=8,rtrep=.t.,;
    cFormVar="w_DVCODCON",value=space(15),;
    ToolTipText = "Codice del conto generico",;
    HelpContextID = 221674364,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=75, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_DVFLMACO", oKey_2_1="ANCODICE", oKey_2_2="this.w_DVCODCON"

  func oDVCODCON_2_3.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC='N' and .w_DVFLMACO='G')
    endwith
  endfunc

  func oDVCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_DVFLMACO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_DVFLMACO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDVCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti",'GSAR0MDV.CONTI_VZM',this.parent.oContained
  endproc
  proc oDVCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_DVFLMACO
     i_obj.w_ANCODICE=this.parent.oContained.w_DVCODCON
    i_obj.ecpSave()
  endproc

  add object oDVCODMAS_2_4 as StdTrsField with uid="VTMSMECQUW",rtseq=9,rtrep=.t.,;
    cFormVar="w_DVCODMAS",value=space(15),;
    ToolTipText = "Codice del mastro contabile",;
    HelpContextID = 53902199,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il mastro deve essere di livello 1",;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=200, Top=0, cSayPict=[p_MAS], cGetPict=[p_MAS], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_DVCODMAS"

  func oDVCODMAS_2_4.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC='N' and .w_DVFLMACO='M')
    endwith
  endfunc

  func oDVCODMAS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODMAS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODMAS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oDVCODMAS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili",'GSAR0MDV.MASTRI_VZM',this.parent.oContained
  endproc
  proc oDVCODMAS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_DVCODMAS
    i_obj.ecpSave()
  endproc

  add object oDVCODVOC_2_5 as StdTrsField with uid="NIORJXNHWN",rtseq=10,rtrep=.t.,;
    cFormVar="w_DVCODVOC",value=space(15),;
    ToolTipText = "Codice della voce di bilancio",;
    HelpContextID = 171342727,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "La voce deve essere diversa dalla voce di riferimento o non obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=325, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_ANAC", cZoomOnZoom="GSBI_AVC", oKey_1_1="VRCODVOC", oKey_1_2="this.w_DVCODVOC"

  func oDVCODVOC_2_5.mCond()
    with this.Parent.oContained
      return (.w_VRFLDESC='T' and .w_DVFLMACO='V')
    endwith
  endfunc

  func oDVCODVOC_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDVCODVOC_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDVCODVOC_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_ANAC','*','VRCODVOC',cp_AbsName(this.parent,'oDVCODVOC_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVC',"Voci di bilancio",'GSBI_MDC.VOC_ANAC_VZM',this.parent.oContained
  endproc
  proc oDVCODVOC_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_DVCODVOC
    i_obj.ecpSave()
  endproc

  add object oDVFORMUL_2_12 as StdTrsField with uid="LCIDVZZFQA",rtseq=17,rtrep=.t.,;
    cFormVar="w_DVFORMUL",value=space(30),;
    ToolTipText = "Eventuale ulteriore formula di elaborazione della riga",;
    HelpContextID = 229225602,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=450, Top=0, InputMask=replicate('X',30)

  add object oDVFLOPER_2_13 as StdTrsCombo with uid="WSETVFYCGK",rtrep=.t.,;
    cFormVar="w_DVFLOPER", RowSource=""+"+ = Somma,"+"- = Sottrae,"+"/ = Divide,"+"* = Moltiplica" , ;
    ToolTipText = "Tipo di operazione tra le righe",;
    HelpContextID = 7779464,;
    Height=21, Width=35, Left=592, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDVFLOPER_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DVFLOPER,&i_cF..t_DVFLOPER),this.value)
    return(iif(xVal =1,'+',;
    iif(xVal =2,'-',;
    iif(xVal =3,'/',;
    iif(xVal =4,'*',;
    space(1))))))
  endfunc
  func oDVFLOPER_2_13.GetRadio()
    this.Parent.oContained.w_DVFLOPER = this.RadioValue()
    return .t.
  endfunc

  func oDVFLOPER_2_13.ToRadio()
    this.Parent.oContained.w_DVFLOPER=trim(this.Parent.oContained.w_DVFLOPER)
    return(;
      iif(this.Parent.oContained.w_DVFLOPER=='+',1,;
      iif(this.Parent.oContained.w_DVFLOPER=='-',2,;
      iif(this.Parent.oContained.w_DVFLOPER=='/',3,;
      iif(this.Parent.oContained.w_DVFLOPER=='*',4,;
      0)))))
  endfunc

  func oDVFLOPER_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDVFLPARE_2_14 as StdTrsCombo with uid="AYSVCOEQYT",rtrep=.t.,;
    cFormVar="w_DVFLPARE", RowSource=""+","+"( = Apre parentesi,"+") = Chiude parent.,"+"(( = Apre doppia,"+")) = Chiude doppia" , ;
    ToolTipText = "Eventuale parentesi",;
    HelpContextID = 25605243,;
    Height=21, Width=30, Left=632, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDVFLPARE_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DVFLPARE,&i_cF..t_DVFLPARE),this.value)
    return(iif(xVal =1,'  ',;
    iif(xVal =2,'(',;
    iif(xVal =3,')',;
    iif(xVal =4,'((',;
    iif(xVal =5,'))',;
    space(2)))))))
  endfunc
  func oDVFLPARE_2_14.GetRadio()
    this.Parent.oContained.w_DVFLPARE = this.RadioValue()
    return .t.
  endfunc

  func oDVFLPARE_2_14.ToRadio()
    this.Parent.oContained.w_DVFLPARE=trim(this.Parent.oContained.w_DVFLPARE)
    return(;
      iif(this.Parent.oContained.w_DVFLPARE=='',1,;
      iif(this.Parent.oContained.w_DVFLPARE=='(',2,;
      iif(this.Parent.oContained.w_DVFLPARE==')',3,;
      iif(this.Parent.oContained.w_DVFLPARE=='((',4,;
      iif(this.Parent.oContained.w_DVFLPARE=='))',5,;
      0))))))
  endfunc

  func oDVFLPARE_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDVCONDIZ_2_15 as StdTrsField with uid="QBUMHHFHFS",rtseq=20,rtrep=.t.,;
    cFormVar="w_DVCONDIZ",value=space(20),;
    ToolTipText = "Condizione di elaborazione della riga",;
    HelpContextID = 74024080,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=666, Top=0, InputMask=replicate('X',20)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_mdc','VOCDANAC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DVCODICE=VOCDANAC.DVCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
