* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_ba1                                                        *
*              Elabora analitica primanota                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_39]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_ba1",oParentObject)
return(i_retval)

define class tgsbi_ba1 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_SCARTO = 0
  w_COD003 = space(15)
  w_TOTIMP = 0
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_RISATT = space(15)
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_RISPAS = space(15)
  w_RECODICE = space(15)
  w_REDESCRI = space(45)
  w_CODICE = space(15)
  w_CODVOC = space(15)
  w_CODCOM = space(15)
  w_FLGMOV = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Primanota ANAL
    * --- Opera su un Precedente Cursore generato dalla Query EXTAN001
    * --- Molte porzioni di codice sono riprese da GSBI_B01
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_FILINI = this.oParentObject.oParentObject.w_DATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_DATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_RISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_RISPAS
    if USED("EXTAN001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (CODVOC C(15),CODICE C(15),CODCOM C(15),TOTIMP N(18,4),FLGMOV C(1))
      SELECT EXTAN001
      GO TOP
      SCAN FOR (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND (EMPTY(NVL(CODVOC," ")) ;
      OR NOT EMPTY(NVL(CODICE," ")) OR NOT EMPTY(NVL(CODCOM," ")))
      this.w_CODVOC = NVL(CODVOC,SPACE(15))
      this.w_CODICE = NVL(CODICE,SPACE(15))
      this.w_CODCOM = NVL(CODCOM,SPACE(15))
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_FLGMOV = NVL(FLGMOV,"E")
      this.w_SCARTO = 0
      this.w_RISCON = 0
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, i_DATSYS)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, 1, i_DATSYS, g_PERVAL, g_PERPVL)
      endif
      * --- 2^OPERAZIONE: Se presente una Competenza, calcola l'esatto Importo
      if NOT EMPTY(this.w_INICOM) AND NOT EMPTY(this.w_FINCOM)
        if this.w_FINCOM<this.w_FILINI OR this.w_INICOM>this.w_FILFIN
          * --- Registrazione Totalmente fuori dal Periodo, Ignora e passa al record successivo
          LOOP
        else
          this.w_GIOCOM = (this.w_FINCOM+1) - this.w_INICOM
          * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
          if this.w_INICOM<this.w_FILINI AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FILINI - this.w_INICOM
            this.w_SCARTO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Registrazione con Competenza che Prosegue oltre il Periodo
          if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FINCOM - this.w_FILFIN
            this.w_RISCON = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Ricalcola l'Importo di Competenza
          this.w_TOTIMP = this.w_TOTIMP - (this.w_SCARTO + this.w_RISCON)
        endif
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      if this.w_TOTIMP<>0
        INSERT INTO TmpAgg1 (CODVOC, CODICE, CODCOM, TOTIMP, FLGMOV) ;
        VALUES (this.w_CODVOC, this.w_CODICE, this.w_CODCOM, this.w_TOTIMP, this.w_FLGMOV)
      endif
      SELECT EXTAN001
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTAN001
      USE
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Voce di Costo, Centro di Costo, Commessa, Business Unit, FLGMOV
      if USED("TmpAgg1")
        SELECT tmpAgg1
        if RECCOUNT()>0
          SELECT CODVOC,CODICE,CODCOM,SUM(TOTIMP) AS TOTALE,FLGMOV FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY CODVOC, CODICE, CODCOM, FLGMOV
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TOTALE,0)<>0 AND (NOT EMPTY(NVL(CODICE," ")) OR ;
          NOT EMPTY(NVL(CODVOC," ")) OR NOT EMPTY(NVL(CODCOM," ")))
          this.w_TIPCON = NVL(FLGMOV,"E")
          this.w_COD001 = NVL(CODVOC,SPACE(15))
          this.w_COD002 = NVL(CODICE,SPACE(15))
          this.w_COD003 = NVL(CODCOM,SPACE(15))
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
