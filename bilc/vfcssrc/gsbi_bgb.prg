* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bgb                                                        *
*              Generazione bilanci contabili                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_17]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bgb",oParentObject)
return(i_retval)

define class tgsbi_bgb as StdBatch
  * --- Local variables
  w_MESS = space(200)
  w_INIZIO = .f.
  w_ENTRATO = .f.
  DECIMI = 0
  w_BILANCIO = space(15)
  w_ESE = space(4)
  w_PROVVI = space(1)
  w_GBDATBIL = ctod("  /  /  ")
  w_PECODICE = space(15)
  w_DETCON = space(1)
  w_GBNUMBIL = 0
  w_ARROTOND = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione automatica Bilancio - da GSBI_MRB
    * --- Riempie le righe del dettaglio
    this.DECIMI = this.oParentObject.oParentObject.w_DECIMI
    * --- Controllo che sia stata inserita una struttura
    if EMPTY(this.oParentObject.oParentObject.w_GBSTRUCT)
      ah_ErrorMsg("Selezionare una struttura di bilancio",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Svuoto le righe
    * --- Azzera il Transitorio
    SELECT (this.oParentObject.cTrsName)
    if reccount()>1
      * --- Almeno una riga c'� sempre
      * --- Avviso - La generazione svuota le righe presenti
      if NOT ah_YesNo("Le righe presenti verranno cancellate.%0Proseguire con l'elaborazione?")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Comunque elimino tutto il cursore
    SELECT (this.oParentObject.cTrsName)
    zap
    * --- Disabilito i campi del'anagrafica
    this.oParentObject.oParentObject.w_ABILITA=.F.
    * --- Calcolo il Bilancio e lo metto in un cursore chiamato -curbilancio-
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_INIZIO = .F.
    this.w_ENTRATO = .f.
    * --- Carica il Transitorio
    select curbilancio
    GO TOP
    this.oParentObject.w_CPROWNUM = 0
    SCAN
    if this.w_INIZIO=.F.
      * --- Append gia' eseguito la prima volta nella BlankRec
      this.oParentObject.InitRow()
    else
      this.w_INIZIO = .F.
    endif
    * --- Carico le variabili
    select curbilancio
    this.oParentObject.w_MRDESCRI = NVL(CURBILANCIO.MRDESCRI,"")
    this.oParentObject.w_MR__NOTE = NVL(CURBILANCIO.MR__NOTE,"")
    this.oParentObject.w_MRCODVOC = NVL(CURBILANCIO.MRCODVOC,"")
    this.oParentObject.w_CPROWORD = NVL(CURBILANCIO.CPROWORD1,0)
    this.oParentObject.w_CPROWNUM = this.oParentObject.w_CPROWNUM+1
    this.oParentObject.w_MRIMPORT = cp_ROUND(NVL(CURBILANCIO.MRIMPORT,0),this.DECIMI)
    this.oParentObject.w_MRPERCEN = NVL(CURBILANCIO.MRPERCEN,0)
    * --- Carattere e Identatura
    this.oParentObject.w_MR__FONT = NVL(CURBILANCIO.MR__FONT,"")
    this.oParentObject.w_MR__FTST = NVL(CURBILANCIO.MR__FTST,"")
    this.oParentObject.w_MR__INDE = NVL(CURBILANCIO.MR__INDE,0)
    this.oParentObject.w_MR__COLO = NVL(CURBILANCIO.MR__COLO,0)
    this.oParentObject.w_MR__NUCO = NVL(CURBILANCIO.MR__NUCO,0)
    this.oParentObject.w_VRFLDESC = NVL(CURBILANCIO.VRFLDESC,"N")
    this.w_ENTRATO = .T.
    this.oParentObject.TrsFromWork()
    select curbilancio
    ENDSCAN
    * --- Se non trovo neanche una riga
    if NOT this.w_ENTRATO
      ah_ErrorMsg("Nessuna riga generata",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
    * --- Rilascio il cursore
    if USED("curbilancio")
      select curbilancio
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancio la generazione - Richiamo la Procedura di Stampa del Bilancio
    * --- Creo un insieme di variabili da passare al Batch, quest'ultimo mi ritorner� un cursore
    * --- Contente i dati da mettere nelle righe.
    * --- Inizializzo le variabili da passare al Batch
    this.w_ESE = this.oParentObject.oParentObject.w_GBBILESE
    this.w_BILANCIO = this.oParentObject.oParentObject.w_GBSTRUCT
    this.w_PROVVI = this.oParentObject.oParentObject.w_GBTIPMOV
    * --- // tutti i movimenti
    this.w_GBDATBIL = this.oParentObject.oParentObject.w_GBDATBIL
    this.w_PECODICE = this.oParentObject.oParentObject.w_GBPERIOD
    this.w_GBNUMBIL = this.oParentObject.oParentObject.w_GBNUMBIL
    this.w_ARROTOND = this.oParentObject.oParentObject.w_ARROTOND
    * --- Chiamo il batch
    do GSBI_BBG with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
