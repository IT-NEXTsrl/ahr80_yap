* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi1bib                                                        *
*              Cancellazione indici                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-26                                                      *
* Last revis.: 2015-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi1bib",oParentObject)
return(i_retval)

define class tgsbi1bib as StdBatch
  * --- Local variables
  w_ERRORE = .f.
  w_MESS = space(100)
  * --- WorkFile variables
  IND_BILA_idx=0
  STRDINDI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito dalla gsbi_aib in fase di cancellazione
    * --- Controlli per la cancellazione
    this.w_ERRORE = .F.
    * --- Select from STRDINDI
    i_nConn=i_TableProp[this.STRDINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STRDINDI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COUNT(TICODIND) AS COUNT  from "+i_cTable+" STRDINDI ";
          +" where TICODIND="+cp_ToStrODBC(this.oParentObject.w_IBCODICE)+"";
           ,"_Curs_STRDINDI")
    else
      select COUNT(TICODIND) AS COUNT from (i_cTable);
       where TICODIND=this.oParentObject.w_IBCODICE;
        into cursor _Curs_STRDINDI
    endif
    if used('_Curs_STRDINDI')
      select _Curs_STRDINDI
      locate for 1=1
      do while not(eof())
      this.w_ERRORE = NVL(_Curs_STRDINDI.COUNT, 0) <> 0
        select _Curs_STRDINDI
        continue
      enddo
      use
    endif
    if this.w_ERRORE
      this.w_MESS = ah_MsgFormat("Impossibile cancellare, indice presente nella struttura indici")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='IND_BILA'
    this.cWorkTables[2]='STRDINDI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_STRDINDI')
      use in _Curs_STRDINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
