* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kga                                                        *
*              Treeview struttura bilancio analitica                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_99]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2008-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_kga",oParentObject))

* --- Class definition
define class tgsbi_kga as StdForm
  Top    = 4
  Left   = 20

  * --- Standard Properties
  Width  = 464
  Height = 500
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-09"
  HelpContextID=236321175
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsbi_kga"
  cComment = "Treeview struttura bilancio analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TRCODICE = space(15)
  w_TRDESCRI = space(40)
  w_CPROWORD = 0
  w_SEQELA = 0
  w_DESCRI = space(40)
  w_TIPORD = space(1)
  w_CODRCC = space(15)
  w_CURSORNA = space(10)
  w_TREEVIEW = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kgaPag1","gsbi_kga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPORD_1_15
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    DoDefault()
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODICE=space(15)
      .w_TRDESCRI=space(40)
      .w_CPROWORD=0
      .w_SEQELA=0
      .w_DESCRI=space(40)
      .w_TIPORD=space(1)
      .w_CODRCC=space(15)
      .w_CURSORNA=space(10)
      .w_TRCODICE=oParentObject.w_TRCODICE
      .w_TRDESCRI=oParentObject.w_TRDESCRI
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_CPROWORD = Nvl( .w_TREEVIEW.GETVAR('CPROWORD') , 0 )
        .w_SEQELA = Nvl( .w_TREEVIEW.GETVAR('SEQELA') , 0 )
        .w_DESCRI = Nvl( .w_TREEVIEW.GETVAR('DESCRI') ,Space(40))
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_TIPORD = 'S'
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_CODRCC = Nvl( .w_TREEVIEW.GETVAR('CODRCC') , Space(15) )
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TRCODICE=.w_TRCODICE
      .oParentObject.w_TRDESCRI=.w_TRDESCRI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_CPROWORD = Nvl( .w_TREEVIEW.GETVAR('CPROWORD') , 0 )
            .w_SEQELA = Nvl( .w_TREEVIEW.GETVAR('SEQELA') , 0 )
            .w_DESCRI = Nvl( .w_TREEVIEW.GETVAR('DESCRI') ,Space(40))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(6,6,.t.)
            .w_CODRCC = Nvl( .w_TREEVIEW.GETVAR('CODRCC') , Space(15) )
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value==this.w_TRCODICE)
      this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value=this.w_TRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value==this.w_TRDESCRI)
      this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value=this.w_TRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_7.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_7.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oSEQELA_1_10.value==this.w_SEQELA)
      this.oPgFrm.Page1.oPag.oSEQELA_1_10.value=this.w_SEQELA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_11.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_15.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRCC_1_18.value==this.w_CODRCC)
      this.oPgFrm.Page1.oPag.oCODRCC_1_18.value=this.w_CODRCC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_kgaPag1 as StdContainer
  Width  = 460
  height = 500
  stdWidth  = 460
  stdheight = 500
  resizeXpos=324
  resizeYpos=155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODICE_1_1 as StdField with uid="ABHYHWWMLO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODICE", cQueryName = "TRCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura di bilancio",;
    HelpContextID = 72777349,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=66, Top=8, InputMask=replicate('X',15)

  add object oTRDESCRI_1_2 as StdField with uid="PFEASYQIYQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRDESCRI", cQueryName = "TRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura di bilancio",;
    HelpContextID = 158363265,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=198, Top=8, InputMask=replicate('X',40)


  add object TREEVIEW as cp_Treeview with uid="BXKKGRCUIR",left=3, top=34, width=366,height=357,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor='Vista',cShowFields='+" "+CAMPO',cNodeShowField='',cLeafShowField='',cNodeBmp='TMASTRO.BMP',cLeafBmp='TCONTO.BMP',;
    cEvent = "Esegui",;
    nPag=1;
    , ToolTipText = "Visualizza struttura (prem. tasto destro del mouse per modif. riga)";
    , HelpContextID = 122552090


  add object oObj_1_5 as cp_runprogram with uid="CDZEHNFBDG",left=466, top=84, width=277,height=23,;
    caption='GSBI_BGA(Reload)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGA("Reload")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 21621168


  add object oObj_1_6 as cp_runprogram with uid="LBHVXIGCQM",left=466, top=106, width=277,height=23,;
    caption='GSBI_BGA(Close)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGA("Close")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 49903399

  add object oCPROWORD_1_7 as StdField with uid="IJHWAFAHEO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 220565910,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=92, Top=421, cSayPict='"999999"', cGetPict='"999999"'

  add object oSEQELA_1_10 as StdField with uid="DHYACPWCTA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SEQELA", cQueryName = "SEQELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di aequenza elaborazione",;
    HelpContextID = 199207898,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=241, Top=421, cSayPict='"999999"', cGetPict='"999999"'

  add object oDESCRI_1_11 as StdField with uid="EVTSHHPRIM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione voce",;
    HelpContextID = 58821834,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=92, Top=471, InputMask=replicate('X',40)


  add object oObj_1_13 as cp_runprogram with uid="YWEWUMVAWX",left=466, top=128, width=277,height=23,;
    caption='GSBA_BGI(Click)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGA("Click")',;
    cEvent = "w_treeview NodeRightClick",;
    nPag=1;
    , HelpContextID = 132871983


  add object oTIPORD_1_15 as StdCombo with uid="WXQJKNHFCJ",rtseq=6,rtrep=.f.,left=341,top=396,width=109,height=21;
    , ToolTipText = "Tipo ordinamento";
    , HelpContextID = 141932490;
    , cFormVar="w_TIPORD",RowSource=""+"Seq.elabor.,"+"Riga stampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPORD_1_15.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_15.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='S',1,;
      iif(this.Parent.oContained.w_TIPORD=='R',2,;
      0))
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="QDNJRCLOIB",left=466, top=150, width=277,height=23,;
    caption='GSVI_BGA(Ord)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGA("Ord")',;
    cEvent = "w_TIPORD Changed",;
    nPag=1;
    , HelpContextID = 156246823

  add object oCODRCC_1_18 as StdField with uid="RHXMBTPFBM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODRCC", cQueryName = "CODRCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 174289626,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=92, Top=446, InputMask=replicate('X',15)


  add object oObj_1_19 as cp_runprogram with uid="NNRCWNWTJS",left=466, top=172, width=277,height=23,;
    caption='GSBI_BGA(DClick)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGA("DClick")',;
    cEvent = "w_treeview NodeClick",;
    nPag=1;
    , HelpContextID = 5498960


  add object oBtn_1_20 as StdButton with uid="WZFWVQXIDZ",left=393, top=35, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Espande la struttura";
    , HelpContextID = 157184326;
    , Caption='\<Espandi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="OLKINCLNTO",left=393, top=87, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la struttura";
    , HelpContextID = 36571610;
    , Caption='\<Chiudi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="EFZTAPUEYA",left=393, top=448, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 243638598;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="VQWUVNCFVX",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=57, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="EBEHTOECGX",Visible=.t., Left=22, Top=421,;
    Alignment=1, Width=69, Height=15,;
    Caption="Num. riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="BZJDMISACC",Visible=.t., Left=170, Top=421,;
    Alignment=1, Width=69, Height=15,;
    Caption="Num. seq.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="RQVDMCBEGU",Visible=.t., Left=6, Top=473,;
    Alignment=1, Width=85, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BRXCRNJJJM",Visible=.t., Left=226, Top=394,;
    Alignment=1, Width=114, Height=18,;
    Caption="Ordinata per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IBKEWNBYNP",Visible=.t., Left=17, Top=449,;
    Alignment=1, Width=74, Height=15,;
    Caption="Rag. C/C:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
