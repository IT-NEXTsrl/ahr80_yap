* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bva                                                        *
*              Verifica formula voce anal                                      *
*                                                                              *
*      Author: ZUCCHETTI TAM S.r.l.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-11-15                                                      *
* Last revis.: 2010-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOperazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bva",oParentObject,m.pOperazione)
return(i_retval)

define class tgsbi_bva as StdBatch
  * --- Local variables
  pOperazione = space(4)
  w_OREC = 0
  w_CODICE = space(15)
  COSTANTE = space(3)
  Func = space(0)
  Msg = space(20)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  TmpSave = space(0)
  w_VALORE = 0
  w_OPER = space(1)
  w_OCCUR = 0
  * --- WorkFile variables
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della formula integrata nella voce di bilancio
    this.Func = "("
    this.FuncCalc = "("
    SELECT * FROM (this.oParentObject.cTrsName) INTO CURSOR CursFunc ORDER BY t_CPROWORD
    SELECT CursFunc
    this.w_OREC = RECNO()
    GO TOP
    SCAN FOR t_CPROWORD<>0 AND (NOT EMPTY(t_DVCODVOC) OR NOT EMPTY(t_DVCODVOB))
    this.w_CODICE = SPACE(15)
    * --- Valorizzo il codice
    do case
      case t_DVTIPRIG = "N"
        this.w_CODICE = t_DVCODVOC
      case t_DVTIPRIG = "T"
        this.w_CODICE = t_DVCODVOB
    endcase
    this.w_OPER = SPACE(1)
    * --- inserisco l'operatore
    do case
      case t_DVFLOPER = 1
        this.w_OPER = "+"
      case t_DVFLOPER = 2
        this.w_OPER = "-"
      case t_DVFLOPER = 3
        this.w_OPER = "/"
      case t_DVFLOPER = 4
        this.w_OPER = "*"
    endcase
    this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_OPER)
    this.FuncCalc = ALLTRIM(this.FuncCalc) + ALLTRIM(this.w_OPER)
    if t_DVFLPARE = 2 OR t_DVFLPARE = 4
      * --- Inserisco parentesi tonde aperte all'inizio se ci sono '(' '(('
      this.Func = ALLTRIM(this.Func) + IIF(t_DVFLPARE = 2, "(", "((")
      this.FuncCalc = ALLTRIM(this.FuncCalc) + IIF(t_DVFLPARE = 2, "(", "((")
    endif
    if NOT EMPTY(t_DVCONDIZ)
      * --- Se esiste la condizione la inserisco
      this.Func = ALLTRIM(this.Func) + "IIF(" + ALLTRIM(this.w_CODICE) + ALLTRIM(t_DVCONDIZ) + ","
      this.FuncCalc = ALLTRIM(this.FuncCalc) + "IIF(" + "10" + ALLTRIM(t_DVCONDIZ) + ","
    endif
    * --- nel then metto la formula
    this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_CODICE) + ALLTRIM(t_DVFORMUL)
    this.FuncCalc = ALLTRIM(this.FuncCalc) + "10" + ALLTRIM(t_DVFORMUL)
    if NOT EMPTY(t_DVCONDIZ)
      * --- chiudo else se sono sotto condizione
      this.Func = ALLTRIM(this.Func) + "," + "0" + ")"
      this.FuncCalc = ALLTRIM(this.FuncCalc) + "," + "10" + ")"
    endif
    if t_DVFLPARE = 3 OR t_DVFLPARE = 5
      * --- Chiudo le parentesi tonde
      this.Func = ALLTRIM(this.Func) + IIF(t_DVFLPARE = 3, ")", "))")
      this.FuncCalc = ALLTRIM(this.FuncCalc) + IIF(t_DVFLPARE = 3, ")", "))")
    endif
    ENDSCAN
    if NOT EMPTY(ALLTRIM(this.oParentObject.oParentObject.w_VRFORMUL))
      this.w_OPER = SPACE(1)
      do case
        case this.oParentObject.oParentObject.w_VRFLOPER = "+"
          this.w_OPER = "+"
        case this.oParentObject.oParentObject.w_VRFLOPER = "-"
          this.w_OPER = "-"
        case this.oParentObject.oParentObject.w_VRFLOPER = "/"
          this.w_OPER = "/"
        case this.oParentObject.oParentObject.w_VRFLOPER = "*"
          this.w_OPER = "*"
      endcase
      this.Func = ALLTRIM(this.Func) + ")"
      this.FuncCalc = ALLTRIM(this.FuncCalc) + ")"
      if this.FuncCalc = "()"
        this.Func = ""
        this.FuncCalc = ""
      endif
      this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_OPER)
      this.FuncCalc = ALLTRIM(this.FuncCalc) + ALLTRIM(this.w_OPER)
      this.TmpSave = this.FuncCalc
      this.Func = ALLTRIM(this.Func) + ALLTRIM(this.oParentObject.oParentObject.w_VRFORMUL)
      this.FuncCalc = ALLTRIM(this.FuncCalc) + ALLTRIM(this.oParentObject.oParentObject.w_VRFORMUL)
    else
      this.Func = ALLTRIM(this.Func) + ")"
      this.FuncCalc = ALLTRIM(this.FuncCalc) + ")"
    endif
    Messaggio = "Corretta"
    ON ERROR Messaggio = "Errata"
    TmpVal = this.FuncCalc
    * --- Valutazione dell'errore
    this.w_VALORE = &TmpVal
    ON ERROR
    if this.w_VALORE>99999999999999.9999
      Messaggio="Errata"
    endif
    if Messaggio = "Errata"
      * --- Controllo se non ho delle variabili di periodo su VRFORMUL
      this.FuncCalc = ALLTRIM(this.TmpSave)
      this.TmpSave = ALLTRIM(this.oParentObject.oParentObject.w_VRFORMUL)
      this.w_OCCUR = RATC("C",this.TmpSave)
      do while this.w_OCCUR <> 0
        this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
        this.COSTANTE = SUBSTR(this.TmpSave,this.w_OCCUR,3)
        if INLIST(this.COSTANTE,"C01","C02","C03","C04","C05","C06","C07","C08","C09","C10")
          this.TmpSave1 = this.TmpSave1 + "10"
        else
          this.TmpSave1 = this.TmpSave1 + "#0"
          * --- Nome costante errato
        endif
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+3))
        this.TmpSave = ALLTRIM(this.TmpSave1)
        this.w_OCCUR = RATC("C",this.TmpSave)
      enddo
      this.FuncCalc = ALLTRIM(this.FuncCalc) + ALLTRIM(this.TmpSave)
      Messaggio = "Corretta"
      ON ERROR Messaggio = "Errata"
      TmpVal = this.FuncCalc
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      ON ERROR
    endif
    this.Msg = Messaggio
    do GSBI_KVC with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT (this.oParentObject.cTrsName)
    if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
      GOTO this.w_OREC
    else
      GO TOP
    endif
    if used("CursFunc")
      select CursFunc
      use
    endif
  endproc


  proc Init(oParentObject,pOperazione)
    this.pOperazione=pOperazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PER_ELAB'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOperazione"
endproc
