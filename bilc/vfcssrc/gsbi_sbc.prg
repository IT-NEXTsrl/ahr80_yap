* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_sbc                                                        *
*              Stampa controllo struttura di bilancio                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_sbc",oParentObject))

* --- Class definition
define class tgsbi_sbc as StdForm
  Top    = 30
  Left   = 39

  * --- Standard Properties
  Width  = 579
  Height = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=107611753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  STR_ANAC_IDX = 0
  cPrg = "gsbi_sbc"
  cComment = "Stampa controllo struttura di bilancio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_BILANCIO = space(15)
  w_BILDES = space(0)
  w_attivita = space(1)
  w_passivi = space(1)
  w_costi = space(1)
  w_ricavi = space(1)
  w_ordine = space(1)
  w_transi = space(1)
  w_BILDES1 = space(40)
  w_DTOBSO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_sbcPag1","gsbi_sbc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oBILANCIO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STR_ANAC'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BILANCIO=space(15)
      .w_BILDES=space(0)
      .w_attivita=space(1)
      .w_passivi=space(1)
      .w_costi=space(1)
      .w_ricavi=space(1)
      .w_ordine=space(1)
      .w_transi=space(1)
      .w_BILDES1=space(40)
      .w_DTOBSO=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_BILANCIO))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_attivita = 'A'
        .w_passivi = 'P'
        .w_costi = 'C'
        .w_ricavi = 'R'
          .DoRTCalc(7,9,.f.)
        .w_DTOBSO = i_datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BILANCIO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BILANCIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_BILANCIO)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR__NOTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_BILANCIO))
          select TRCODICE,TRDESCRI,TR__NOTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BILANCIO)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BILANCIO) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oBILANCIO_1_1'),i_cWhere,'',"Strutture di bilancio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR__NOTE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI,TR__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BILANCIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI,TR__NOTE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_BILANCIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_BILANCIO)
            select TRCODICE,TRDESCRI,TR__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BILANCIO = NVL(_Link_.TRCODICE,space(15))
      this.w_BILDES1 = NVL(_Link_.TRDESCRI,space(40))
      this.w_BILDES = NVL(_Link_.TR__NOTE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_BILANCIO = space(15)
      endif
      this.w_BILDES1 = space(40)
      this.w_BILDES = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BILANCIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oBILANCIO_1_1.value==this.w_BILANCIO)
      this.oPgFrm.Page1.oPag.oBILANCIO_1_1.value=this.w_BILANCIO
    endif
    if not(this.oPgFrm.Page1.oPag.oBILDES_1_2.value==this.w_BILDES)
      this.oPgFrm.Page1.oPag.oBILDES_1_2.value=this.w_BILDES
    endif
    if not(this.oPgFrm.Page1.oPag.oattivita_1_4.RadioValue()==this.w_attivita)
      this.oPgFrm.Page1.oPag.oattivita_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.opassivi_1_5.RadioValue()==this.w_passivi)
      this.oPgFrm.Page1.oPag.opassivi_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocosti_1_6.RadioValue()==this.w_costi)
      this.oPgFrm.Page1.oPag.ocosti_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oricavi_1_7.RadioValue()==this.w_ricavi)
      this.oPgFrm.Page1.oPag.oricavi_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oordine_1_8.RadioValue()==this.w_ordine)
      this.oPgFrm.Page1.oPag.oordine_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.otransi_1_9.RadioValue()==this.w_transi)
      this.oPgFrm.Page1.oPag.otransi_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBILDES1_1_11.value==this.w_BILDES1)
      this.oPgFrm.Page1.oPag.oBILDES1_1_11.value=this.w_BILDES1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_BILANCIO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBILANCIO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_BILANCIO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_sbcPag1 as StdContainer
  Width  = 575
  height = 225
  stdWidth  = 575
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBILANCIO_1_1 as StdField with uid="WWTWBXNDZO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_BILANCIO", cQueryName = "BILANCIO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio selezionata",;
    HelpContextID = 29099877,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=141, Top=17, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", oKey_1_1="TRCODICE", oKey_1_2="this.w_BILANCIO"

  func oBILANCIO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oBILANCIO_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBILANCIO_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oBILANCIO_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio",'',this.parent.oContained
  endproc

  add object oBILDES_1_2 as StdMemo with uid="HEIKXJFKOK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_BILDES", cQueryName = "BILDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del bilancio",;
    HelpContextID = 248576234,;
   bGlobalFont=.t.,;
    Height=62, Width=424, Left=141, Top=43

  add object oattivita_1_4 as StdCheck with uid="PYAQXEVZZN",rtseq=3,rtrep=.f.,left=141, top=113, caption="Attivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = attivit�",;
    HelpContextID = 174503015,;
    cFormVar="w_attivita", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oattivita_1_4.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oattivita_1_4.GetRadio()
    this.Parent.oContained.w_attivita = this.RadioValue()
    return .t.
  endfunc

  func oattivita_1_4.SetRadio()
    this.Parent.oContained.w_attivita=trim(this.Parent.oContained.w_attivita)
    this.value = ;
      iif(this.Parent.oContained.w_attivita=='A',1,;
      0)
  endfunc

  add object opassivi_1_5 as StdCheck with uid="OXUBZJZCRT",rtseq=4,rtrep=.f.,left=141, top=131, caption="Passivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = passivit�",;
    HelpContextID = 157249034,;
    cFormVar="w_passivi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func opassivi_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func opassivi_1_5.GetRadio()
    this.Parent.oContained.w_passivi = this.RadioValue()
    return .t.
  endfunc

  func opassivi_1_5.SetRadio()
    this.Parent.oContained.w_passivi=trim(this.Parent.oContained.w_passivi)
    this.value = ;
      iif(this.Parent.oContained.w_passivi=='P',1,;
      0)
  endfunc

  add object ocosti_1_6 as StdCheck with uid="ZDHASCFSUG",rtseq=5,rtrep=.f.,left=141, top=149, caption="Costi",;
    ToolTipText = "Stampa i conti con sezione bilancio = costi",;
    HelpContextID = 257843418,;
    cFormVar="w_costi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocosti_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    ' '))
  endfunc
  func ocosti_1_6.GetRadio()
    this.Parent.oContained.w_costi = this.RadioValue()
    return .t.
  endfunc

  func ocosti_1_6.SetRadio()
    this.Parent.oContained.w_costi=trim(this.Parent.oContained.w_costi)
    this.value = ;
      iif(this.Parent.oContained.w_costi=='C',1,;
      0)
  endfunc

  add object oricavi_1_7 as StdCheck with uid="PLBXXWHAOU",rtseq=6,rtrep=.f.,left=141, top=167, caption="Ricavi",;
    ToolTipText = "Stampa i conti con sezione bilancio = ricavi",;
    HelpContextID = 173906454,;
    cFormVar="w_ricavi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oricavi_1_7.RadioValue()
    return(iif(this.value =1,'R',;
    ' '))
  endfunc
  func oricavi_1_7.GetRadio()
    this.Parent.oContained.w_ricavi = this.RadioValue()
    return .t.
  endfunc

  func oricavi_1_7.SetRadio()
    this.Parent.oContained.w_ricavi=trim(this.Parent.oContained.w_ricavi)
    this.value = ;
      iif(this.Parent.oContained.w_ricavi=='R',1,;
      0)
  endfunc

  add object oordine_1_8 as StdCheck with uid="QVHAPFAGCB",rtseq=7,rtrep=.f.,left=141, top=185, caption="Ordine",;
    ToolTipText = "Stampa i conti con sezione bilancio = ordine",;
    HelpContextID = 98939622,;
    cFormVar="w_ordine", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oordine_1_8.RadioValue()
    return(iif(this.value =1,'O',;
    ' '))
  endfunc
  func oordine_1_8.GetRadio()
    this.Parent.oContained.w_ordine = this.RadioValue()
    return .t.
  endfunc

  func oordine_1_8.SetRadio()
    this.Parent.oContained.w_ordine=trim(this.Parent.oContained.w_ordine)
    this.value = ;
      iif(this.Parent.oContained.w_ordine=='O',1,;
      0)
  endfunc

  add object otransi_1_9 as StdCheck with uid="LMFVPMDMMX",rtseq=8,rtrep=.f.,left=141, top=203, caption="Transitori",;
    ToolTipText = "Stampa i conti con sezione bilancio = transitori",;
    HelpContextID = 171606838,;
    cFormVar="w_transi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func otransi_1_9.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func otransi_1_9.GetRadio()
    this.Parent.oContained.w_transi = this.RadioValue()
    return .t.
  endfunc

  func otransi_1_9.SetRadio()
    this.Parent.oContained.w_transi=trim(this.Parent.oContained.w_transi)
    this.value = ;
      iif(this.Parent.oContained.w_transi=='T',1,;
      0)
  endfunc

  add object oBILDES1_1_11 as StdField with uid="XDXCQEBLUM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_BILDES1", cQueryName = "BILDES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 19859222,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=276, Top=17, InputMask=replicate('X',40)


  add object oBtn_1_13 as StdButton with uid="WZFWVQXIDZ",left=463, top=174, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa di controllo";
    , HelpContextID = 107583002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSBI_BBC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_BILANCIO) and (.w_COSTI='C' or .w_ATTIVITA='A' or .w_PASSIVI='P' or .w_RICAVI='R' or .w_ORDINE='O' or .w_TRANSI='T'))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="EFZTAPUEYA",left=518, top=174, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 100294330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="BUAJGPTTVH",Visible=.t., Left=4, Top=17,;
    Alignment=1, Width=135, Height=15,;
    Caption="Struttura di bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="LWUIDDMALX",Visible=.t., Left=10, Top=113,;
    Alignment=1, Width=127, Height=15,;
    Caption="Sezione bilancio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_sbc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
