* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kic                                                        *
*              Visualizza confronto indici                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_111]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2012-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsbi_kic
       CREATE CURSOR GSBI_KIC (NUMERO C(11), IMPORT N(18,4),VALOTT N(18,4))
       D = WRCURSOR('GSBI_KIC')
* --- Fine Area Manuale
return(createobject("tgsbi_kic",oParentObject))

* --- Class definition
define class tgsbi_kic as StdForm
  Top    = 6
  Left   = 12

  * --- Standard Properties
  Width  = 749
  Height = 435+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-25"
  HelpContextID=266995305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  IND_BILA_IDX = 0
  STR_BIAN_IDX = 0
  STR_ANAC_IDX = 0
  PER_ELAB_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsbi_kic"
  cComment = "Visualizza confronto indici"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_STATO = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_CODINDI = space(15)
  w_DESC = space(40)
  w_TIPOBIL = space(1)
  w_MOVIMENT = space(1)
  w_TIPOPER = space(1)
  w_STRUTTUR = space(15)
  w_PERIODO = space(15)
  w_ESER = space(4)
  o_ESER = space(4)
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_STRUTTUR = space(15)
  w_TITIPIND = space(1)
  w_CODINDI = space(15)
  w_DESC = space(40)
  w_QUERY = .F.
  w_CODINDI = space(15)
  w_DESC = space(40)
  w_CODINDI = space(15)
  w_DESC = space(40)
  w_ZoomGepr = .NULL.
  w_ISTO = .NULL.
  w_LINE = .NULL.
  w_LINE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsbi_kic
  pTIPOCONT=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSBI_KIC'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kicPag1","gsbi_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezione")
      .Pages(2).addobject("oPag","tgsbi_kicPag2","gsbi_kic",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Istogramma")
      .Pages(3).addobject("oPag","tgsbi_kicPag3","gsbi_kic",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Linee")
      .Pages(4).addobject("oPag","tgsbi_kicPag4","gsbi_kic",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Torta")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINDI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGepr = this.oPgFrm.Pages(1).oPag.ZoomGepr
    this.w_ISTO = this.oPgFrm.Pages(2).oPag.ISTO
    this.w_LINE = this.oPgFrm.Pages(3).oPag.LINE
    this.w_LINE = this.oPgFrm.Pages(4).oPag.LINE
    DoDefault()
    proc Destroy()
      this.w_ZoomGepr = .NULL.
      this.w_ISTO = .NULL.
      this.w_LINE = .NULL.
      this.w_LINE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='IND_BILA'
    this.cWorkTables[3]='STR_BIAN'
    this.cWorkTables[4]='STR_ANAC'
    this.cWorkTables[5]='PER_ELAB'
    this.cWorkTables[6]='ESERCIZI'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsbi_kic
    this.pTIPOCONT=This.oParentObject
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_STATO=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_CODINDI=space(15)
      .w_DESC=space(40)
      .w_TIPOBIL=space(1)
      .w_MOVIMENT=space(1)
      .w_TIPOPER=space(1)
      .w_STRUTTUR=space(15)
      .w_PERIODO=space(15)
      .w_ESER=space(4)
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_STRUTTUR=space(15)
      .w_TITIPIND=space(1)
      .w_CODINDI=space(15)
      .w_DESC=space(40)
      .w_QUERY=.f.
      .w_CODINDI=space(15)
      .w_DESC=space(40)
      .w_CODINDI=space(15)
      .w_DESC=space(40)
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_STATO = 'S'
        .w_DTOBSO = i_datsys
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODINDI))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_TIPOBIL = .pTIPOCONT
        .w_MOVIMENT = 'T'
        .w_TIPOPER = 'L'
        .w_STRUTTUR = space(15)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_9('Full')
        endif
        .w_PERIODO = space(15)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PERIODO))
          .link_1_10('Full')
        endif
        .w_ESER = g_codese
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_ESER))
          .link_1_11('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .w_SELEZI = 'D'
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .w_STRUTTUR = space(15)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_30('Full')
        endif
        .w_TITIPIND = .pTIPOCONT
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODINDI))
          .link_2_1('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_QUERY = .F.
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODINDI))
          .link_3_1('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_CODINDI))
          .link_4_1('Full')
        endif
      .oPgFrm.Page2.oPag.ISTO.Calculate(.w_QUERY)
      .oPgFrm.Page3.oPag.LINE.Calculate(.w_QUERY)
      .oPgFrm.Page4.oPag.LINE.Calculate(.w_QUERY)
    endwith
    this.DoRTCalc(22,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_5.enabled = this.oPgFrm.Page4.oPag.oBtn_4_5.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,9,.t.)
        if .o_ESER<>.w_ESER
            .w_PERIODO = space(15)
          .link_1_10('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .DoRTCalc(11,15,.t.)
          .link_2_1('Full')
        .DoRTCalc(17,18,.t.)
          .link_3_1('Full')
        .DoRTCalc(20,20,.t.)
          .link_4_1('Full')
        .oPgFrm.Page2.oPag.ISTO.Calculate(.w_QUERY)
        .oPgFrm.Page3.oPag.LINE.Calculate(.w_QUERY)
        .oPgFrm.Page4.oPag.LINE.Calculate(.w_QUERY)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGepr.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page2.oPag.ISTO.Calculate(.w_QUERY)
        .oPgFrm.Page3.oPag.LINE.Calculate(.w_QUERY)
        .oPgFrm.Page4.oPag.LINE.Calculate(.w_QUERY)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTRUTTUR_1_9.visible=!this.oPgFrm.Page1.oPag.oSTRUTTUR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSTRUTTUR_1_30.visible=!this.oPgFrm.Page1.oPag.oSTRUTTUR_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGepr.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page2.oPag.ISTO.Event(cEvent)
      .oPgFrm.Page3.oPag.LINE.Event(cEvent)
      .oPgFrm.Page4.oPag.LINE.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINDI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_BO1',True,'IND_BILA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IBCODICE like "+cp_ToStrODBC(trim(this.w_CODINDI)+"%");

          i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IBCODICE',trim(this.w_CODINDI))
          select IBCODICE,IBDESCRI,IBTIPIND;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINDI)==trim(_Link_.IBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINDI) and !this.bDontReportError
            deferred_cp_zoom('IND_BILA','*','IBCODICE',cp_AbsName(oSource.parent,'oCODINDI_1_4'),i_cWhere,'GSBI_BO1',"Indici di bilancio",'GSBI3AIB.IND_BILA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                     +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',oSource.xKey(1))
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI,IBTIPIND";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODINDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODINDI)
            select IBCODICE,IBDESCRI,IBTIPIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINDI = NVL(_Link_.IBCODICE,space(15))
      this.w_DESC = NVL(_Link_.IBDESCRI,space(40))
      this.w_TITIPIND = NVL(_Link_.IBTIPIND,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODINDI = space(15)
      endif
      this.w_DESC = space(40)
      this.w_TITIPIND = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TITIPIND=.w_TIPOBIL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINDI = space(15)
        this.w_DESC = space(40)
        this.w_TITIPIND = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_ANAC_IDX,3]
    i_lTable = "STR_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2], .t., this.STR_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STR_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_ANAC','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_9'),i_cWhere,'',"Strutture di bilancio contabile",'gsbi_agb.STR_ANAC_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_PERIODO)+"%");

            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_10'),i_cWhere,'GSBI_APE',"Periodi di elaborazione",'PERIODO1.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_11'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_30'),i_cWhere,'',"Strutture di bilancio analitica",'gsbi_agb.STR_BIAN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINDI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODINDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODINDI)
            select IBCODICE,IBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINDI = NVL(_Link_.IBCODICE,space(15))
      this.w_DESC = NVL(_Link_.IBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINDI = space(15)
      endif
      this.w_DESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINDI
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODINDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODINDI)
            select IBCODICE,IBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINDI = NVL(_Link_.IBCODICE,space(15))
      this.w_DESC = NVL(_Link_.IBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINDI = space(15)
      endif
      this.w_DESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINDI
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IND_BILA_IDX,3]
    i_lTable = "IND_BILA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2], .t., this.IND_BILA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_CODINDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_CODINDI)
            select IBCODICE,IBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINDI = NVL(_Link_.IBCODICE,space(15))
      this.w_DESC = NVL(_Link_.IBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINDI = space(15)
      endif
      this.w_DESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IND_BILA_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.IND_BILA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINDI_1_4.value==this.w_CODINDI)
      this.oPgFrm.Page1.oPag.oCODINDI_1_4.value=this.w_CODINDI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_1_5.value==this.w_DESC)
      this.oPgFrm.Page1.oPag.oDESC_1_5.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOBIL_1_6.RadioValue()==this.w_TIPOBIL)
      this.oPgFrm.Page1.oPag.oTIPOBIL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVIMENT_1_7.RadioValue()==this.w_MOVIMENT)
      this.oPgFrm.Page1.oPag.oMOVIMENT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPER_1_8.RadioValue()==this.w_TIPOPER)
      this.oPgFrm.Page1.oPag.oTIPOPER_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_9.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_9.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_10.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_10.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_11.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_11.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_14.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_30.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_30.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page2.oPag.oCODINDI_2_1.value==this.w_CODINDI)
      this.oPgFrm.Page2.oPag.oCODINDI_2_1.value=this.w_CODINDI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page3.oPag.oCODINDI_3_1.value==this.w_CODINDI)
      this.oPgFrm.Page3.oPag.oCODINDI_3_1.value=this.w_CODINDI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESC_3_2.value==this.w_DESC)
      this.oPgFrm.Page3.oPag.oDESC_3_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page4.oPag.oCODINDI_4_1.value==this.w_CODINDI)
      this.oPgFrm.Page4.oPag.oCODINDI_4_1.value=this.w_CODINDI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESC_4_2.value==this.w_DESC)
      this.oPgFrm.Page4.oPag.oDESC_4_2.value=this.w_DESC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TITIPIND=.w_TIPOBIL)  and not(empty(.w_CODINDI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINDI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    return

enddefine

* --- Define pages as container
define class tgsbi_kicPag1 as StdContainer
  Width  = 745
  height = 435
  stdWidth  = 745
  stdheight = 435
  resizeXpos=406
  resizeYpos=301
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINDI_1_4 as StdField with uid="ZQVUPOVCYL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODINDI", cQueryName = "CODINDI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Indice da confrontare",;
    HelpContextID = 155421990,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=139, Top=13, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="IND_BILA", cZoomOnZoom="GSBI_BO1", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODINDI"

  func oCODINDI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINDI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINDI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IND_BILA','*','IBCODICE',cp_AbsName(this.parent,'oCODINDI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_BO1',"Indici di bilancio",'GSBI3AIB.IND_BILA_VZM',this.parent.oContained
  endproc
  proc oCODINDI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSBI_BO1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IBCODICE=this.parent.oContained.w_CODINDI
     i_obj.ecpSave()
  endproc

  add object oDESC_1_5 as StdField with uid="BYLERGASOQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indice",;
    HelpContextID = 262245578,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=259, Top=13, InputMask=replicate('X',40)


  add object oTIPOBIL_1_6 as StdCombo with uid="TZVZDDCTJK",rtseq=6,rtrep=.f.,left=139,top=64,width=115,height=21, enabled=.f.;
    , ToolTipText = "Tipo bilancio (cont. generale, cont. analitica)";
    , HelpContextID = 41269194;
    , cFormVar="w_TIPOBIL",RowSource=""+"Cont. generale,"+"Cont. analitica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOBIL_1_6.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'A',;
    ' ')))
  endfunc
  func oTIPOBIL_1_6.GetRadio()
    this.Parent.oContained.w_TIPOBIL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOBIL_1_6.SetRadio()
    this.Parent.oContained.w_TIPOBIL=trim(this.Parent.oContained.w_TIPOBIL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOBIL=='G',1,;
      iif(this.Parent.oContained.w_TIPOBIL=='A',2,;
      0))
  endfunc


  add object oMOVIMENT_1_7 as StdCombo with uid="RCSCXACRIY",rtseq=7,rtrep=.f.,left=353,top=64,width=114,height=21;
    , ToolTipText = "Movimenti effettivi o previsionali";
    , HelpContextID = 97210854;
    , cFormVar="w_MOVIMENT",RowSource=""+"Tutti,"+"Previsionali,"+"Effettivi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOVIMENT_1_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    ' '))))
  endfunc
  func oMOVIMENT_1_7.GetRadio()
    this.Parent.oContained.w_MOVIMENT = this.RadioValue()
    return .t.
  endfunc

  func oMOVIMENT_1_7.SetRadio()
    this.Parent.oContained.w_MOVIMENT=trim(this.Parent.oContained.w_MOVIMENT)
    this.value = ;
      iif(this.Parent.oContained.w_MOVIMENT=='T',1,;
      iif(this.Parent.oContained.w_MOVIMENT=='P',2,;
      iif(this.Parent.oContained.w_MOVIMENT=='E',3,;
      0)))
  endfunc


  add object oTIPOPER_1_8 as StdCombo with uid="YYDGQNFRRD",rtseq=8,rtrep=.f.,left=567,top=64,width=113,height=21;
    , ToolTipText = "Tipo periodo (tutti, gen., ann., sem., quad., trim., mens.)";
    , HelpContextID = 174737462;
    , cFormVar="w_TIPOPER",RowSource=""+"Tutti,"+"Solo generico,"+"Annuale,"+"Semestrale,"+"Quadrimestrale,"+"Trimestrale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPER_1_8.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'G',;
    iif(this.value =3,'A',;
    iif(this.value =4,'S',;
    iif(this.value =5,'Q',;
    iif(this.value =6,'T',;
    iif(this.value =7,'M',;
    ' '))))))))
  endfunc
  func oTIPOPER_1_8.GetRadio()
    this.Parent.oContained.w_TIPOPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPER_1_8.SetRadio()
    this.Parent.oContained.w_TIPOPER=trim(this.Parent.oContained.w_TIPOPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPER=='L',1,;
      iif(this.Parent.oContained.w_TIPOPER=='G',2,;
      iif(this.Parent.oContained.w_TIPOPER=='A',3,;
      iif(this.Parent.oContained.w_TIPOPER=='S',4,;
      iif(this.Parent.oContained.w_TIPOPER=='Q',5,;
      iif(this.Parent.oContained.w_TIPOPER=='T',6,;
      iif(this.Parent.oContained.w_TIPOPER=='M',7,;
      0)))))))
  endfunc

  add object oSTRUTTUR_1_9 as StdField with uid="QGJMBCCDZU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 162558840,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=139, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_ANAC", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOBIL='A')
    endwith
  endfunc

  func oSTRUTTUR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_ANAC','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio contabile",'gsbi_agb.STR_ANAC_VZM',this.parent.oContained
  endproc

  add object oPERIODO_1_10 as StdField with uid="INHDKMVVNI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 111909898,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=353, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi di elaborazione",'PERIODO1.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc

  add object oESER_1_11 as StdField with uid="LOUVGFVHHP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione",;
    HelpContextID = 261316282,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=567, Top=94, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oBtn_1_12 as StdButton with uid="POVIVRMKXG",left=691, top=66, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Carica bilanci da scegliere";
    , HelpContextID = 178362390;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSBI_BIC(this.Parent.oContained,"CARICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TIPOBIL))
      endwith
    endif
  endfunc


  add object ZoomGepr as cp_szoombox with uid="XJOKVNLIHL",left=3, top=120, width=738,height=265,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="GESTBILA",cZoomFile="GSBI_BIC",bOptions=.F.,bAdvOptions=.F.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 179437798

  add object oSELEZI_1_14 as StdRadio with uid="SXHHPPCZCO",rtseq=12,rtrep=.f.,left=5, top=389, width=138,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 16776154
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 16776154
      this.Buttons(2).Top=15
      this.SetAll("Width",136)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_14.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_14.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="YOTPXVYLIA",left=755, top=95, width=129,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BIC("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 179437798


  add object oBtn_1_23 as StdButton with uid="ISGWKPYFSI",left=691, top=389, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 259677882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="EEJGNTVQTV",left=637, top=389, width=48,height=45,;
    CpPicture="BMP\GRAFICO.BMP", caption="", nPag=1;
    , ToolTipText = "Elabora grafico";
    , HelpContextID = 99450778;
    , caption='\<Grafico';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSBI_BIC(this.Parent.oContained,"GRAF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODINDI))
      endwith
    endif
  endfunc


  add object oObj_1_28 as cp_runprogram with uid="YWHCOXWBLT",left=755, top=116, width=129,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BIC("QUIT")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 179437798


  add object oBtn_1_29 as StdButton with uid="JEABYREPDG",left=582, top=389, width=48,height=45,;
    CpPicture="bmp\Excel.bmp", caption="", nPag=1;
    , ToolTipText = "Invia su foglio Excel";
    , HelpContextID = 146692538;
    , caption='\<Excel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSBI_BIC(this.Parent.oContained,"GRXL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODINDI))
      endwith
    endif
  endfunc

  add object oSTRUTTUR_1_30 as StdField with uid="FTCJRAZSQY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 162558840,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=139, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPOBIL='G')
    endwith
  endfunc

  func oSTRUTTUR_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture di bilancio analitica",'gsbi_agb.STR_BIAN_VZM',this.parent.oContained
  endproc

  add object oStr_1_17 as StdString with uid="THQRQZWCCU",Visible=.t., Left=258, Top=64,;
    Alignment=1, Width=92, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CQUGQJYVCI",Visible=.t., Left=41, Top=94,;
    Alignment=1, Width=97, Height=15,;
    Caption="Struttura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="SEKBEVEHPR",Visible=.t., Left=279, Top=94,;
    Alignment=1, Width=71, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="GLXDCOGJFH",Visible=.t., Left=4, Top=49,;
    Alignment=0, Width=64, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="BRHTMLGOIT",Visible=.t., Left=482, Top=94,;
    Alignment=1, Width=82, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="OYOXYXSAMQ",Visible=.t., Left=43, Top=13,;
    Alignment=1, Width=95, Height=15,;
    Caption="Codice indice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="AXOYVYARLD",Visible=.t., Left=479, Top=64,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IORNPHBDOC",Visible=.t., Left=53, Top=64,;
    Alignment=1, Width=85, Height=15,;
    Caption="Tipo bil.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_21 as StdBox with uid="EENHTVLNXO",left=0, top=45, width=743,height=0
enddefine
define class tgsbi_kicPag2 as StdContainer
  Width  = 745
  height = 435
  stdWidth  = 745
  stdheight = 435
  resizeXpos=582
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINDI_2_1 as StdField with uid="CLZHAFERTP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODINDI", cQueryName = "CODINDI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Indice da confrontare",;
    HelpContextID = 155421990,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=110, Top=13, InputMask=replicate('X',15), cLinkFile="IND_BILA", cZoomOnZoom="GSBI_AIB", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODINDI"

  func oCODINDI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESC_2_2 as StdField with uid="POONGQMQOT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indice",;
    HelpContextID = 262245578,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=230, Top=13, InputMask=replicate('X',40)


  add object oBtn_2_6 as StdButton with uid="LLYIATINXH",left=687, top=387, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 259677882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ISTO as cp_FoxCharts with uid="LEIAKNXMPT",left=7, top=53, width=729,height=327,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="gsbi_kic",cFileCfg="..\bilc\exe\query\gsbi_kic_i",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "Esegui",;
    nPag=2;
    , HelpContextID = 179437798

  add object oStr_2_3 as StdString with uid="EHLIDBWBZR",Visible=.t., Left=17, Top=13,;
    Alignment=1, Width=92, Height=15,;
    Caption="Codice indice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_4 as StdBox with uid="MVLOLFPSPY",left=1, top=45, width=743,height=0
enddefine
define class tgsbi_kicPag3 as StdContainer
  Width  = 745
  height = 435
  stdWidth  = 745
  stdheight = 435
  resizeXpos=594
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINDI_3_1 as StdField with uid="RKTRYRFDSN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODINDI", cQueryName = "CODINDI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Indice da confrontare",;
    HelpContextID = 155421990,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=115, Top=13, InputMask=replicate('X',15), cLinkFile="IND_BILA", cZoomOnZoom="GSBI_AIB", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODINDI"

  func oCODINDI_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESC_3_2 as StdField with uid="OEPWNAMUJM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indice",;
    HelpContextID = 262245578,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=234, Top=13, InputMask=replicate('X',40)


  add object oBtn_3_5 as StdButton with uid="RFRDDKMLPT",left=686, top=387, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 259677882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object LINE as cp_FoxCharts with uid="TQYPMFOMNH",left=7, top=53, width=729,height=327,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="gsbi_kic",cFileCfg="..\bilc\exe\query\gsbi_kic_l",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "Esegui",;
    nPag=3;
    , HelpContextID = 179437798

  add object oStr_3_3 as StdString with uid="BNOCUYSSKX",Visible=.t., Left=16, Top=13,;
    Alignment=1, Width=95, Height=15,;
    Caption="Codice indice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_4 as StdBox with uid="JSCUMQVNVF",left=1, top=45, width=743,height=0
enddefine
define class tgsbi_kicPag4 as StdContainer
  Width  = 745
  height = 435
  stdWidth  = 745
  stdheight = 435
  resizeXpos=601
  resizeYpos=226
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINDI_4_1 as StdField with uid="AXNTWMVREZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODINDI", cQueryName = "CODINDI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Indice da confrontare",;
    HelpContextID = 155421990,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=111, Top=13, InputMask=replicate('X',15), cLinkFile="IND_BILA", cZoomOnZoom="GSBI_AIB", oKey_1_1="IBCODICE", oKey_1_2="this.w_CODINDI"

  func oCODINDI_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESC_4_2 as StdField with uid="ILJXAVEVEB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indice",;
    HelpContextID = 262245578,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=230, Top=13, InputMask=replicate('X',40)


  add object oBtn_4_5 as StdButton with uid="BCICHJGJCK",left=687, top=387, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Esci dalla visualizzazione";
    , HelpContextID = 259677882;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object LINE as cp_FoxCharts with uid="PUOYOSNHXT",left=7, top=53, width=729,height=327,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="gsbi_kic",cFileCfg="..\bilc\exe\query\gsbi_kic_t",bDisplayLogo=.f.,cLogoFileName="",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",;
    cEvent = "Esegui",;
    nPag=4;
    , HelpContextID = 179437798

  add object oStr_4_3 as StdString with uid="LZRMCJDMAM",Visible=.t., Left=18, Top=13,;
    Alignment=1, Width=92, Height=15,;
    Caption="Codice indice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_4_4 as StdBox with uid="TIEZMYPZKI",left=1, top=45, width=743,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
