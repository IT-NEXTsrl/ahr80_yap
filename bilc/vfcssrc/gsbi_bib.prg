* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bib                                                        *
*              Calcolo indici di bilancio                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_25]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bib",oParentObject,m.pOper)
return(i_retval)

define class tgsbi_bib as StdBatch
  * --- Local variables
  pOper = space(5)
  w_OCCUR = 0
  w_LUNGVAR = 0
  w_INDOCC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato per il calcolo degli indici.(GSBI_AIB Definizione indici)
    do case
      case alltrim(this.pOper)="<C01>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C02>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C03>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C04>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C05>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C06>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C07>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C08>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C09>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="<C10>"
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case INLIST(alltrim(this.pOper),"0","1","2","3","4","5","6","7","8","9")
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case INLIST(alltrim(this.pOper),"+","-","*","/","(",".",")")
        this.oParentObject.w_IBFORMUL = alltrim(this.oParentObject.w_IBFORMUL)+alltrim(this.pOper)
    endcase
    do case
      case alltrim(this.pOper)="CE"
        this.w_OCCUR = 0
        DIMENSION OCCURV(6)
        OCCURV(1) = RATC("+",this.oParentObject.w_IBFORMUL)
        OCCURV(2) = RATC("-",this.oParentObject.w_IBFORMUL)
        OCCURV(3) = RATC("*",this.oParentObject.w_IBFORMUL)
        OCCURV(4) = RATC("/",this.oParentObject.w_IBFORMUL)
        OCCURV(5) = RATC("(",this.oParentObject.w_IBFORMUL)
        OCCURV(6) = RATC(")",this.oParentObject.w_IBFORMUL)
        this.w_LUNGVAR = LEN(ALLTRIM(this.oParentObject.w_IBFORMUL))
        FOR this.w_INDOCC=1 TO 6
        this.w_OCCUR = IIF(this.w_OCCUR < OCCURV(this.w_INDOCC), IIF(this.w_LUNGVAR=OCCURV(this.w_INDOCC),OCCURV(this.w_INDOCC)-1,OCCURV(this.w_INDOCC)), this.w_OCCUR)
        ENDFOR
        this.oParentObject.w_IBFORMUL = SUBSTR(ALLTRIM(this.oParentObject.w_IBFORMUL),1,this.w_OCCUR)
    endcase
    do case
      case alltrim(this.pOper)="CZ"
        this.oParentObject.w_IBFORMUL = ""
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
