* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_spa                                                        *
*              Proiezione bilancio di analitica                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_122]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_spa",oParentObject))

* --- Class definition
define class tgsbi_spa as StdForm
  Top    = 7
  Left   = 16

  * --- Standard Properties
  Width  = 749
  Height = 456
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=141166185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  STR_BIAN_IDX = 0
  ESERCIZI_IDX = 0
  PER_ELAB_IDX = 0
  cPrg = "gsbi_spa"
  cComment = "Proiezione bilancio di analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_STATO = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_TIPOBIL = space(1)
  w_STRUTTUR = space(15)
  w_DESC = space(40)
  w_VALNAZ = space(3)
  w_TIPOPER = space(1)
  w_ESER = space(4)
  o_ESER = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_PERIODO = space(15)
  w_SELEZI1 = space(1)
  w_FLSELE1 = 0
  w_SELEZI2 = space(1)
  w_FLSELE2 = 0
  w_MOVIMENT = space(1)
  w_STAEXCEL = space(1)
  w_DATROT = ctod('  /  /  ')
  w_ZoomBilP = .NULL.
  w_ZoomBilE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_spaPag1","gsbi_spa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezione")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTRUTTUR_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomBilP = this.oPgFrm.Pages(1).oPag.ZoomBilP
    this.w_ZoomBilE = this.oPgFrm.Pages(1).oPag.ZoomBilE
    DoDefault()
    proc Destroy()
      this.w_ZoomBilP = .NULL.
      this.w_ZoomBilE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='STR_BIAN'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='PER_ELAB'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_STATO=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TIPOBIL=space(1)
      .w_STRUTTUR=space(15)
      .w_DESC=space(40)
      .w_VALNAZ=space(3)
      .w_TIPOPER=space(1)
      .w_ESER=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PERIODO=space(15)
      .w_SELEZI1=space(1)
      .w_FLSELE1=0
      .w_SELEZI2=space(1)
      .w_FLSELE2=0
      .w_MOVIMENT=space(1)
      .w_STAEXCEL=space(1)
      .w_DATROT=ctod("  /  /  ")
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_STATO = 'S'
        .w_DTOBSO = i_datsys
        .w_TIPOBIL = 'A'
        .w_STRUTTUR = space(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_STRUTTUR))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_TIPOPER = 'M'
        .w_ESER = g_codese
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ESER))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_PERIODO))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomBilP.Calculate(.F.)
        .w_SELEZI1 = 'D'
        .w_FLSELE1 = 0
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.ZoomBilE.Calculate(.F.)
        .w_SELEZI2 = 'D'
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_FLSELE2 = 0
        .w_MOVIMENT = 'T'
        .w_STAEXCEL = 'N'
    endwith
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
        if .o_ESER<>.w_ESER
          .link_1_12('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomBilP.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomBilE.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomBilP.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.ZoomBilE.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomBilP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomBilE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STRUTTUR
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STR_BIAN_IDX,3]
    i_lTable = "STR_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2], .t., this.STR_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STRUTTUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_MSA',True,'STR_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_STRUTTUR)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_STRUTTUR))
          select TRCODICE,TRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STRUTTUR)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STRUTTUR) and !this.bDontReportError
            deferred_cp_zoom('STR_BIAN','*','TRCODICE',cp_AbsName(oSource.parent,'oSTRUTTUR_1_5'),i_cWhere,'GSBI_MSA',"Strutture di bilancio analitica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STRUTTUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_STRUTTUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_STRUTTUR)
            select TRCODICE,TRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STRUTTUR = NVL(_Link_.TRCODICE,space(15))
      this.w_DESC = NVL(_Link_.TRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_STRUTTUR = space(15)
      endif
      this.w_DESC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STR_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.STR_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STRUTTUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESER
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESER)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESER))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESER)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESER) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESER_1_9'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESER);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESER)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESER = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESER = space(4)
      endif
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIODO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PER_ELAB_IDX,3]
    i_lTable = "PER_ELAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2], .t., this.PER_ELAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_APE',True,'PER_ELAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PECODICE like "+cp_ToStrODBC(trim(this.w_PERIODO)+"%");

          i_ret=cp_SQL(i_nConn,"select PECODICE,PEDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PECODICE',trim(this.w_PERIODO))
          select PECODICE,PEDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIODO)==trim(_Link_.PECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PERIODO) and !this.bDontReportError
            deferred_cp_zoom('PER_ELAB','*','PECODICE',cp_AbsName(oSource.parent,'oPERIODO_1_12'),i_cWhere,'GSBI_APE',"Periodi elaborazione",'PERIODO1.PER_ELAB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',oSource.xKey(1))
            select PECODICE,PEDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PECODICE,PEDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where PECODICE="+cp_ToStrODBC(this.w_PERIODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PECODICE',this.w_PERIODO)
            select PECODICE,PEDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIODO = NVL(_Link_.PECODICE,space(15))
      this.w_DATROT = NVL(cp_ToDate(_Link_.PEDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PERIODO = space(15)
      endif
      this.w_DATROT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DATINI<=.w_DATROT AND .w_DATROT<=.w_DATFIN) OR (EMPTY(.w_DATINI) AND EMPTY(.w_DATFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PERIODO = space(15)
        this.w_DATROT = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PER_ELAB_IDX,2])+'\'+cp_ToStr(_Link_.PECODICE,1)
      cp_ShowWarn(i_cKey,this.PER_ELAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTRUTTUR_1_5.value==this.w_STRUTTUR)
      this.oPgFrm.Page1.oPag.oSTRUTTUR_1_5.value=this.w_STRUTTUR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_1_6.value==this.w_DESC)
      this.oPgFrm.Page1.oPag.oDESC_1_6.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPER_1_8.RadioValue()==this.w_TIPOPER)
      this.oPgFrm.Page1.oPag.oTIPOPER_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESER_1_9.value==this.w_ESER)
      this.oPgFrm.Page1.oPag.oESER_1_9.value=this.w_ESER
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_12.value==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_12.value=this.w_PERIODO
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI1_1_14.RadioValue()==this.w_SELEZI1)
      this.oPgFrm.Page1.oPag.oSELEZI1_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI2_1_23.RadioValue()==this.w_SELEZI2)
      this.oPgFrm.Page1.oPag.oSELEZI2_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAEXCEL_1_27.RadioValue()==this.w_STAEXCEL)
      this.oPgFrm.Page1.oPag.oSTAEXCEL_1_27.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PERIODO)) or not((.w_DATINI<=.w_DATROT AND .w_DATROT<=.w_DATFIN) OR (EMPTY(.w_DATINI) AND EMPTY(.w_DATFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIODO_1_12.SetFocus()
            i_bnoObbl = !empty(.w_PERIODO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESER = this.w_ESER
    return

enddefine

* --- Define pages as container
define class tgsbi_spaPag1 as StdContainer
  Width  = 745
  height = 456
  stdWidth  = 745
  stdheight = 456
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTRUTTUR_1_5 as StdField with uid="FCCHYAUMAD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STRUTTUR", cQueryName = "STRUTTUR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Struttura di bilancio",;
    HelpContextID = 19952504,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=149, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="STR_BIAN", cZoomOnZoom="GSBI_MSA", oKey_1_1="TRCODICE", oKey_1_2="this.w_STRUTTUR"

  func oSTRUTTUR_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTRUTTUR_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTRUTTUR_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STR_BIAN','*','TRCODICE',cp_AbsName(this.parent,'oSTRUTTUR_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_MSA',"Strutture di bilancio analitica",'',this.parent.oContained
  endproc
  proc oSTRUTTUR_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSBI_MSA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_STRUTTUR
     i_obj.ecpSave()
  endproc

  add object oDESC_1_6 as StdField with uid="JKOIDXQXGQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura di bilancio",;
    HelpContextID = 136416458,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=271, Top=9, InputMask=replicate('X',40)


  add object oTIPOPER_1_8 as StdCombo with uid="JBZAJELJOW",rtseq=8,rtrep=.f.,left=126,top=69,width=113,height=21;
    , ToolTipText = "Tipo periodo (sem., quad., trim., mens.)";
    , HelpContextID = 236304330;
    , cFormVar="w_TIPOPER",RowSource=""+"Mensile,"+"Trimestrale,"+"Quadrimestrale,"+"Semestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPER_1_8.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Q',;
    iif(this.value =4,'S',;
    ' ')))))
  endfunc
  func oTIPOPER_1_8.GetRadio()
    this.Parent.oContained.w_TIPOPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPER_1_8.SetRadio()
    this.Parent.oContained.w_TIPOPER=trim(this.Parent.oContained.w_TIPOPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPER=='M',1,;
      iif(this.Parent.oContained.w_TIPOPER=='T',2,;
      iif(this.Parent.oContained.w_TIPOPER=='Q',3,;
      iif(this.Parent.oContained.w_TIPOPER=='S',4,;
      0))))
  endfunc

  add object oESER_1_9 as StdField with uid="MPWNXBODZF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ESER", cQueryName = "ESER",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di selezione",;
    HelpContextID = 135487162,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=321, Top=69, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESER"

  func oESER_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oESER_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESER_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESER_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPERIODO_1_12 as StdField with uid="BQKWEDLAFT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PERIODO", cQueryName = "PERIODO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di elaborazione",;
    HelpContextID = 254516234,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=532, Top=69, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PER_ELAB", cZoomOnZoom="GSBI_APE", oKey_1_1="PECODICE", oKey_1_2="this.w_PERIODO"

  func oPERIODO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIODO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIODO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PER_ELAB','*','PECODICE',cp_AbsName(this.parent,'oPERIODO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_APE',"Periodi elaborazione",'PERIODO1.PER_ELAB_VZM',this.parent.oContained
  endproc
  proc oPERIODO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSBI_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PECODICE=this.parent.oContained.w_PERIODO
     i_obj.ecpSave()
  endproc


  add object ZoomBilP as cp_szoombox with uid="ITCVGAWXXB",left=3, top=109, width=366,height=280,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='GESTBILA',cZoomFile='GSBI1BIC',bOptions=.F.,bAdvOptions=.F.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 36831462

  add object oSELEZI1_1_14 as StdRadio with uid="KHYDRQRHXY",rtseq=13,rtrep=.f.,left=11, top=396, width=311,height=32;
    , cFormVar="w_SELEZI1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI1_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 159382490
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 159382490
      this.Buttons(2).Top=15
      this.SetAll("Width",309)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI1_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI1_1_14.GetRadio()
    this.Parent.oContained.w_SELEZI1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI1_1_14.SetRadio()
    this.Parent.oContained.w_SELEZI1=trim(this.Parent.oContained.w_SELEZI1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI1=='S',1,;
      iif(this.Parent.oContained.w_SELEZI1=='D',2,;
      0))
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="JFCKZYMZBS",left=747, top=333, width=124,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BPG("SELE1")',;
    cEvent = "w_SELEZI1 Changed",;
    nPag=1;
    , HelpContextID = 36831462


  add object ZoomBilE as cp_szoombox with uid="GIPGMMPLOO",left=375, top=109, width=366,height=280,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='GESTBILA',cZoomFile='GSBI1BIC',bOptions=.F.,bAdvOptions=.F.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 36831462

  add object oSELEZI2_1_23 as StdRadio with uid="IYXRZBSPUX",rtseq=15,rtrep=.f.,left=383, top=396, width=130,height=32;
    , cFormVar="w_SELEZI2", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI2_1_23.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 159382490
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 159382490
      this.Buttons(2).Top=15
      this.SetAll("Width",128)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI2_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI2_1_23.GetRadio()
    this.Parent.oContained.w_SELEZI2 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI2_1_23.SetRadio()
    this.Parent.oContained.w_SELEZI2=trim(this.Parent.oContained.w_SELEZI2)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI2=='S',1,;
      iif(this.Parent.oContained.w_SELEZI2=='D',2,;
      0))
  endfunc


  add object oObj_1_24 as cp_runprogram with uid="WQIYRISQUV",left=747, top=354, width=124,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSBI_BPG("SELE2")',;
    cEvent = "w_SELEZI2 Changed",;
    nPag=1;
    , HelpContextID = 36831462

  add object oSTAEXCEL_1_27 as StdCheck with uid="HCKKVTSXVE",rtseq=18,rtrep=.f.,left=519, top=394, caption="Stampa Excel",;
    HelpContextID = 6251378,;
    cFormVar="w_STAEXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAEXCEL_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSTAEXCEL_1_27.GetRadio()
    this.Parent.oContained.w_STAEXCEL = this.RadioValue()
    return .t.
  endfunc

  func oSTAEXCEL_1_27.SetRadio()
    this.Parent.oContained.w_STAEXCEL=trim(this.Parent.oContained.w_STAEXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_STAEXCEL=='S',1,;
      0)
  endfunc


  add object oBtn_1_32 as StdButton with uid="FMLOYWYRDO",left=631, top=405, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa proiezione";
    , HelpContextID = 141137434;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSBI_BPG(this.Parent.oContained,"ELAB")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_STRUTTUR) and not empty(.w_DATROT))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="EFZTAPUEYA",left=688, top=405, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133848762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="NNXUKWGFIV",left=688, top=60, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Carica bilanci da scegliere";
    , HelpContextID = 35756054;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSBI_BPG(this.Parent.oContained,"CARICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_STRUTTUR) AND NOT EMPTY(.w_DATROT))
      endwith
    endif
  endfunc

  add object oStr_1_17 as StdString with uid="FNVQXJFVMY",Visible=.t., Left=13, Top=9,;
    Alignment=1, Width=135, Height=15,;
    Caption="Struttura di bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="OBRWRQYYDQ",Visible=.t., Left=5, Top=48,;
    Alignment=0, Width=52, Height=15,;
    Caption="Filtri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="WWZRGDKSIN",Visible=.t., Left=6, Top=92,;
    Alignment=0, Width=291, Height=15,;
    Caption="Bilanci previsionali"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BBRSUSHDHC",Visible=.t., Left=381, Top=92,;
    Alignment=0, Width=262, Height=15,;
    Caption="Bilanci effettivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="JEEMMLIMQM",Visible=.t., Left=245, Top=69,;
    Alignment=1, Width=75, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="BZKGBIOKKS",Visible=.t., Left=42, Top=69,;
    Alignment=1, Width=81, Height=15,;
    Caption="Tipo periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ISGGVBLCKB",Visible=.t., Left=380, Top=69,;
    Alignment=1, Width=149, Height=15,;
    Caption="Periodo rottura:"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="GXRAGOKPZP",left=0, top=40, width=743,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_spa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
