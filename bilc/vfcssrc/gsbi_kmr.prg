* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kmr                                                        *
*              Modifica riga contabile                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_kmr",oParentObject))

* --- Class definition
define class tgsbi_kmr as StdForm
  Top    = 20
  Left   = 19

  * --- Standard Properties
  Width  = 590
  Height = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=199886441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  VOC_ANAC_IDX = 0
  cPrg = "gsbi_kmr"
  cComment = "Modifica riga contabile"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CPROWORD = 0
  w_SEQELA = 0
  w_CODVOC = space(15)
  w_DESCRI = space(50)
  w_NWROWORD = 0
  w_NWSEQELA = 0
  w_NWCODVOC = space(15)
  w_NWDESCRI = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kmrPag1","gsbi_kmr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNWROWORD_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VOC_ANAC'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CPROWORD=0
      .w_SEQELA=0
      .w_CODVOC=space(15)
      .w_DESCRI=space(50)
      .w_NWROWORD=0
      .w_NWSEQELA=0
      .w_NWCODVOC=space(15)
      .w_NWDESCRI=space(50)
      .w_CPROWORD=oParentObject.w_CPROWORD
      .w_SEQELA=oParentObject.w_SEQELA
      .w_CODVOC=oParentObject.w_CODVOC
      .w_DESCRI=oParentObject.w_DESCRI
      .w_NWROWORD=oParentObject.w_NWROWORD
      .w_NWSEQELA=oParentObject.w_NWSEQELA
      .w_NWCODVOC=oParentObject.w_NWCODVOC
      .w_NWDESCRI=oParentObject.w_NWDESCRI
        .DoRTCalc(1,7,.f.)
        if not(empty(.w_NWCODVOC))
          .link_1_14('Full')
        endif
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CPROWORD=.w_CPROWORD
      .oParentObject.w_SEQELA=.w_SEQELA
      .oParentObject.w_CODVOC=.w_CODVOC
      .oParentObject.w_DESCRI=.w_DESCRI
      .oParentObject.w_NWROWORD=.w_NWROWORD
      .oParentObject.w_NWSEQELA=.w_NWSEQELA
      .oParentObject.w_NWCODVOC=.w_NWCODVOC
      .oParentObject.w_NWDESCRI=.w_NWDESCRI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NWCODVOC
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_ANAC_IDX,3]
    i_lTable = "VOC_ANAC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2], .t., this.VOC_ANAC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NWCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVC',True,'VOC_ANAC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_NWCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_NWCODVOC))
          select VRCODVOC,VRDESVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NWCODVOC)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NWCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_ANAC','*','VRCODVOC',cp_AbsName(oSource.parent,'oNWCODVOC_1_14'),i_cWhere,'GSBI_AVC',"Voci di bilancio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NWCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_NWCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_NWCODVOC)
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NWCODVOC = NVL(_Link_.VRCODVOC,space(15))
      this.w_NWDESCRI = NVL(_Link_.VRDESVOC,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_NWCODVOC = space(15)
      endif
      this.w_NWDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_ANAC_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_ANAC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NWCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_1.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oSEQELA_1_2.value==this.w_SEQELA)
      this.oPgFrm.Page1.oPag.oSEQELA_1_2.value=this.w_SEQELA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVOC_1_3.value==this.w_CODVOC)
      this.oPgFrm.Page1.oPag.oCODVOC_1_3.value=this.w_CODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_4.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_4.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNWROWORD_1_8.value==this.w_NWROWORD)
      this.oPgFrm.Page1.oPag.oNWROWORD_1_8.value=this.w_NWROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oNWSEQELA_1_9.value==this.w_NWSEQELA)
      this.oPgFrm.Page1.oPag.oNWSEQELA_1_9.value=this.w_NWSEQELA
    endif
    if not(this.oPgFrm.Page1.oPag.oNWCODVOC_1_14.value==this.w_NWCODVOC)
      this.oPgFrm.Page1.oPag.oNWCODVOC_1_14.value=this.w_NWCODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNWDESCRI_1_15.value==this.w_NWDESCRI)
      this.oPgFrm.Page1.oPag.oNWDESCRI_1_15.value=this.w_NWDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_kmrPag1 as StdContainer
  Width  = 586
  height = 202
  stdWidth  = 586
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCPROWORD_1_1 as StdField with uid="NYBMVLFVME",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 148532842,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=98, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oSEQELA_1_2 as StdField with uid="ZFMZPCDYLA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SEQELA", cQueryName = "SEQELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero sequenza di elaborazione",;
    HelpContextID = 98544602,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=355, Top=11, cSayPict='"999999"', cGetPict='"999999"'

  add object oCODVOC_1_3 as StdField with uid="ISJIVOJUPW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODVOC", cQueryName = "CODVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice voce",;
    HelpContextID = 60781274,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=98, Top=37, InputMask=replicate('X',15)

  add object oDESCRI_1_4 as StdField with uid="EIKQTMAHHB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 226593994,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=37, InputMask=replicate('X',50)

  add object oNWROWORD_1_8 as StdField with uid="XNSNXCLSGB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NWROWORD", cQueryName = "NWROWORD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nuovo numero riga",;
    HelpContextID = 148534810,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=98, Top=100, cSayPict='"999999"', cGetPict='"999999"'

  add object oNWSEQELA_1_9 as StdField with uid="YUXYFLRNVD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NWSEQELA", cQueryName = "NWSEQELA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nuovo numero sequenza di elaborazione",;
    HelpContextID = 26180073,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=355, Top=100, cSayPict='"999999"', cGetPict='"999999"'

  add object oNWCODVOC_1_14 as StdField with uid="MTXIMDJOTU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NWCODVOC", cQueryName = "NWCODVOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nuovo codice voce",;
    HelpContextID = 22444519,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=98, Top=127, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_ANAC", cZoomOnZoom="GSBI_AVC", oKey_1_1="VRCODVOC", oKey_1_2="this.w_NWCODVOC"

  func oNWCODVOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oNWCODVOC_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNWCODVOC_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_ANAC','*','VRCODVOC',cp_AbsName(this.parent,'oNWCODVOC_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVC',"Voci di bilancio",'',this.parent.oContained
  endproc
  proc oNWCODVOC_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_NWCODVOC
     i_obj.ecpSave()
  endproc

  add object oNWDESCRI_1_15 as StdField with uid="BKMPHEHRCZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NWDESCRI", cQueryName = "NWDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nuova descrizione",;
    HelpContextID = 57698785,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=216, Top=127, InputMask=replicate('X',50)


  add object oBtn_1_17 as StdButton with uid="WZFWVQXIDZ",left=472, top=152, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare le modifiche";
    , HelpContextID = 199857690;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NWROWORD) AND NOT EMPTY(.w_NWSEQELA) AND NOT EMPTY(.w_NWCODVOC))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="EFZTAPUEYA",left=527, top=152, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere uscire senza effettuare le modifiche";
    , HelpContextID = 192569018;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="UQKBAEWSSC",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=93, Height=15,;
    Caption="Numero riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XJJRJQOYDR",Visible=.t., Left=152, Top=13,;
    Alignment=1, Width=200, Height=15,;
    Caption="Num. sequenza di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FPAGPLGWGR",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=92, Height=15,;
    Caption="Codice voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="HHIDBRGQHN",Visible=.t., Left=4, Top=102,;
    Alignment=1, Width=92, Height=15,;
    Caption="Numero riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="GBCBSAVADG",Visible=.t., Left=151, Top=102,;
    Alignment=1, Width=201, Height=15,;
    Caption="Num. sequenza di elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="APBHVYVSNL",Visible=.t., Left=17, Top=72,;
    Alignment=0, Width=555, Height=15,;
    Caption="Nuovo elemento"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TSCVSUOLZO",Visible=.t., Left=3, Top=129,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice voce:"  ;
  , bGlobalFont=.t.

  add object oBox_1_12 as StdBox with uid="RXJJALQACZ",left=17, top=90, width=558,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kmr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
