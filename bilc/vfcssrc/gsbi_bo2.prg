* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bo2                                                        *
*              Zoom on zoom indici                                             *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_23]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bo2",oParentObject)
return(i_retval)

define class tgsbi_bo2 as StdBatch
  * --- Local variables
  w_IBTIPIND = space(1)
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH PER LANCIARE LO ZOOM ON ZOOM DALLE STRUTTURE INDICI
    * --- Chiamato da GSBI_MSI
    * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
    if i_curform=NULL
      i_retcode = 'stop'
      return
    endif
    * --- Puntatore al nome del Cursore da cui e' stato lanciato
    this.w_IBTIPIND = frmtgsbi_sib1.w_TITIPIND
    this.w_PROG = GSBI_AIB(this.w_IBTIPIND)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
