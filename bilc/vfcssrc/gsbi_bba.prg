* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bba                                                        *
*              Crea bilancio struttura analitica                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_307]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bba",oParentObject)
return(i_retval)

define class tgsbi_bba as StdBatch
  * --- Local variables
  w_OLDESE = space(4)
  w_OLDDIVISA = space(3)
  w_RIGA = 0
  w_INIESE = ctod("  /  /  ")
  w_VALESE = space(3)
  w_RAGCDC = space(15)
  w_GBNUMBIL = 0
  w_MESS = space(10)
  w_ESE = space(4)
  w_ROWVOC = 0
  w_ERROR = .f.
  Msg = space(90)
  w_INDIC = 0
  CodiceVoce = space(15)
  w_RIGTOT = 0
  w_VALPER = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  Func = space(0)
  w_VALORE = 0
  w_OPER = space(1)
  w_OCCUR = 0
  w_VALORES = space(19)
  w_VALPSGS = space(19)
  w_OREC = 0
  w_CODICE = space(15)
  w_TIPO = space(1)
  * --- WorkFile variables
  VALUTE_idx=0
  PER_ELAB_idx=0
  ESERCIZI_idx=0
  GESTBILA_idx=0
  DETTBILA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CREA BILANCIO STRUTTURATO x ANALITICA
    this.w_ESE = this.oParentObject.w_ESE
    this.w_ERROR = .f.
    this.w_RIGTOT = 0
    * --- Mi calcolo il Bilancio in memoria e solo alla fine lo scrivo
    CREATE CURSOR CurBilancio (CODICE C(15), CPROWORD1 N(6), MRDESCRI C(90), RIGPERC N(6), ;
    MRPERCEN N(6,2), MRIMPORT N(18,4), MR__INDE N(10), ;
    MR__FTST C(1), MR__FONT C(1), MR__COLO N(10), MR__NUCO N(1), ;
    MRCODVOA C(15), MRRAGCDC C(15), MR__NOTE M(10), VRFLDESC C(1))
    a = WRCURSOR("CurBilancio")
    * --- Prendo valori predefiniti in periodo e li memorizzo in un array
    DIMENSION COST(10)
    * --- Select from PER_ELAB
    i_nConn=i_TableProp[this.PER_ELAB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PER_ELAB ";
          +" where PECODICE = "+cp_ToStrODBC(this.oParentObject.w_PECODICE)+"";
           ,"_Curs_PER_ELAB")
    else
      select * from (i_cTable);
       where PECODICE = this.oParentObject.w_PECODICE;
        into cursor _Curs_PER_ELAB
    endif
    if used('_Curs_PER_ELAB')
      select _Curs_PER_ELAB
      locate for 1=1
      do while not(eof())
      COST(1)=NVL(_Curs_PER_ELAB.PEVALC01,0)
      COST(2)=NVL(_Curs_PER_ELAB.PEVALC02,0)
      COST(3)=NVL(_Curs_PER_ELAB.PEVALC03,0)
      COST(4)=NVL(_Curs_PER_ELAB.PEVALC04,0)
      COST(5)=NVL(_Curs_PER_ELAB.PEVALC05,0)
      COST(6)=NVL(_Curs_PER_ELAB.PEVALC06,0)
      COST(7)=NVL(_Curs_PER_ELAB.PEVALC07,0)
      COST(8)=NVL(_Curs_PER_ELAB.PEVALC08,0)
      COST(9)=NVL(_Curs_PER_ELAB.PEVALC09,0)
      COST(10)=NVL(_Curs_PER_ELAB.PEVALC10,0)
        select _Curs_PER_ELAB
        continue
      enddo
      use
    endif
    * --- Cursore per la stampa degli errori durante l'elaborazione
    CREATE CURSOR CursError (CODICE C(15), CPROWORD1 N(6), DESCRI C(90))
    b = WRCURSOR("CursError")
    * --- Query per trovare le Voci di Costo della struttura
    ah_Msg("Fase 1: reperimento dati struttura",.T.)
    vq_exec("..\bilc\exe\query\gsbi2bba",this,"CursCosto1")
    e = WRCURSOR("CursCosto1")
    * --- Query per trovare gli importi delle Voci di Costro per Centro di costo selezionate
    ah_Msg("Fase 2: reperimento dati voci di costo",.T.)
    vq_exec("..\bilc\exe\query\gsbi5bba",this,"CursCosto2")
    d = WRCURSOR("CursCosto2")
    SELECT * FROM CursCosto2 INTO CURSOR CursCosto WHERE ;
    CursCosto2.VOCCEN IN (SELECT * FROM CursCosto1) NOFILTER
    if used("CursCosto1")
      SELECT CursCosto1
      USE
    endif
    if used("CursCosto2")
      SELECT CursCosto2
      USE
    endif
    SELECT * FROM (this.oParentObject.oParentObject.oParentObject.GSBI_MED.cTrsName) ;
    INTO CURSOR CursDrivers
    * --- Query per reperire tutte le voci di bilancio nella struttura di bilancio da calcolare (con i dati per il calcolo)
    * --- l'ordine � in base alla sequenza di calcolo
    ah_Msg("Fase 3: reperimento dati voci",.T.)
    vq_exec("..\bilc\exe\query\gsbi1bba",this,"CursVociT")
    SELECT * FROM CursVociT INTO CURSOR CursSelez GROUP BY CPROWORD ORDER BY TRSEQELA
    * --- Elaboro le voci
    ah_Msg("Fase 4: calcolo valore voci",.T.)
    SELECT CursSelez
    GO TOP
    SCAN
    this.CodiceVoce = CursSelez.CODVOC
    this.w_RAGCDC = NVL(CursSelez.TRCODRCC,space(15))
    do case
      case CursSelez.VRTIPVOC = "D"
        * --- Voce descrittiva
        this.w_VALORE = 0
      case CursSelez.VRTIPVOC = "R"
        * --- Voce DRIVER
        SELECT * FROM CursDrivers INTO CURSOR PSG ;
        WHERE this.CodiceVoce = CursDrivers.t_MRCODVOA
        if used("PSG")
          this.w_VALORE = PSG.t_MRIMPORT
          SELECT PSG
          USE
        else
          this.w_VALORE = 0
        endif
      case .T.
        * --- Voce di Totalizzazione o Normale (va calcolato il valore)
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if this.oParentObject.w_ARROTOND = "S"
      if this.oParentObject.DECIMI > 0
        this.w_VALORE = cp_ROUND(this.w_VALORE, 0)
      else
        this.w_VALORE = cp_ROUND(this.w_VALORE, -3)
      endif
    endif
    if ABS(this.w_VALORE)>999999999999999999999999999999
      this.w_VALORE = 0
      this.Msg = Ah_MsgFormat("Numeric overflow")
      INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
      VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
      this.w_ERROR = .T.
    endif
    if this.w_TIPO="T"
      this.w_RIGTOT = CursSelez.CPROWORD
    endif
    INSERT INTO CurBilancio (CODICE,CPROWORD1,MRDESCRI,RIGPERC,MRPERCEN, ;
    MRIMPORT,MR__INDE,MR__FTST,MR__FONT,MR__COLO,MR__NUCO, ;
    MRCODVOA, MRRAGCDC, MR__NOTE,VRFLDESC) ;
    VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,CursSelez.TRDESDET, ;
    IIF(CursSelez.TRFLPERC="S",CursSelez.TRRIGPER,0),0,this.w_VALORE, ;
    CursSelez.TC__INDE,CursSelez.TC__FTST,CursSelez.TC__FONT, ;
    CursSelez.TC__COLO,IIF(CursSelez.TR__COL1="X",1, ;
    IIF(CursSelez.TR__COL2="X",2,IIF(CursSelez.TR__COL3="X",3,0))), ;
    CursSelez.CODVOC,NVL(this.w_RAGCDC," "), ;
    NVL(CursSelez.VR__NOTE," "),CursSelez.VRTIPVOC)
    SELECT CursSelez
    ENDSCAN
    ah_Msg("Fase 5: calcolo percentuali",.T.)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Cursori
    if used("CursVociT")
      select CursVociT
      use
    endif
    if used("CursCosto")
      select CursCosto
      use
    endif
    if used("CursVoci")
      select CursVoci
      use
    endif
    if used("CursSelez")
      select CursSelez
      use
    endif
    if used("CursDrivers")
      select CursDrivers
      use
    endif
    * --- Calcolo Percentuali
    SELECT * FROM CurBilancio INTO CURSOR CurPercen WHERE RIGPERC <> 0
    * --- Ciclo per valorizzare le percentuali
    SELECT CurPercen
    GO TOP
    SCAN
    SELECT * FROM CurBilancio INTO CURSOR PSG ;
    WHERE CurBilancio.CPROWORD1 = CurPercen.RIGPERC
    if USED("PSG")
      if PSG.MRIMPORT = 0
        this.Msg = Ah_MsgFormat("Divisore su percentuale = 0 sulla riga %1",ALLTRIM(STR(CurBilancio.CPROWORD1)))
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (CurPercen.CODICE,CurPercen.CPROWORD1,this.Msg)
        this.w_ERROR = .T.
        this.w_VALPER = 999.99
      else
        this.w_VALPER = CurPercen.MRIMPORT / PSG.MRIMPORT * 100
      endif
      this.w_VALPER = cp_ROUND(this.w_VALPER,2)
      if ABS (this.w_VALPER) > 999.99
        this.Msg = Ah_MsgFormat("Valore percentuale troppo grande rispetto alla riga %1",ALLTRIM(STR(CurBilancio.CPROWORD1)))
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (CurPercen.CODICE,CurPercen.CPROWORD1,this.Msg)
        this.w_ERROR = .T.
        this.w_VALPER = 999.99
      endif
      UPDATE CurBilancio SET CurBilancio.MRPERCEN = this.w_VALPER ;
      WHERE CurPercen.CODICE = CurBilancio.CODICE AND ;
      CurPercen.CPROWORD1 = CurBilancio.CPROWORD1
      SELECT PSG
      USE
    endif
    ENDSCAN
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ordino il cursore del bilancio per riga
    select * from CurBilancio into cursor TMP ORDER BY CPROWORD1
    if used("CurBilancio")
      select CurBilancio
      use
    endif
    select * from TMP into cursor CurBilancio ORDER BY CPROWORD1
    * --- Chiusura Cursori
    if used("CurPercen")
      select CurPercen
      use
    endif
    if used("TMP")
      select TMP
      use
    endif
    * --- Richiedo se stampare errori e produco la stampa
    if this.w_ERROR
      * --- Avviso - Si sono verificati degli errori (lacia la stampa di verifica)
      if ah_YesNo("Si vuole stampare la lista delle segnalazioni?%0")
        L_GBNUMBIL = this.oParentObject.w_GBNUMBIL
        L_ESE = this.oParentObject.w_ESE
        SELECT * FROM CursError INTO CURSOR __TMP__ ORDER BY CPROWORD1
        CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BBG"," ", this)
      endif
    endif
    if used("CursError")
      select CursError
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
    if used("__VOC__")
      select __VOC__
      use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Valore Voce
    this.Func = "("
    SELECT * FROM CursVociT INTO CURSOR CursVoci ;
    WHERE CursVociT.CODVOC=this.CodiceVoce ;
    GROUP BY CursVociT.CPROWNUM1
    * --- Continuo a Ciclare su CursVoci per Codice identico
    SELECT CursVoci
    SCAN FOR (CursVoci.CODVOC = this.CodiceVoce AND CursVoci.CPROWORD<>0 AND ;
    (NOT EMPTY(NVL(ALLTRIM(DVCODVOB),"")) OR NOT EMPTY(NVL(ALLTRIM(DVCODVOC),"")))) OR Not Empty(Nvl(VRTIPVOC," "))
    this.w_CODICE = SPACE(15)
    * --- Valorizzo il codice
    this.w_TIPO = VRTIPVOC
    do case
      case VRTIPVOC = "N"
        * --- calcolo solo Voci di Costo
        this.w_CODICE = DVCODVOC
        if RECCOUNT("CursCosto")>0
          if this.w_RAGCDC <> space(15)
            * --- Devo controllare il raggruppamento per Centro di Costo
            vq_exec("..\bilc\exe\query\gsbi4bba",this,"CursRag")
            b = WRCURSOR("CursRag")
            SELECT SUM(CursCosto.IMPDAR) AS IMPDAR, SUM(CursCosto.IMPAVE) AS IMPAVE ;
            FROM (CursCosto INNER JOIN CursRag ON CODCEN=RCCODCEN) ;
            INTO CURSOR PSG WHERE CursCosto.VOCCEN = this.w_CODICE ;
            GROUP BY CursCosto.VOCCEN
            if used("CursRag")
              select CursRag
              use
            endif
          else
            SELECT SUM(CursCosto.IMPDAR) AS IMPDAR, SUM(CursCosto.IMPAVE) AS IMPAVE ;
            FROM CursCosto INTO CURSOR PSG WHERE CursCosto.VOCCEN = this.w_CODICE ;
            GROUP BY CursCosto.VOCCEN
          endif
          SELECT PSG
          if RECCOUNT("PSG")>0
            this.w_VALORE = PSG.IMPDAR - PSG.IMPAVE
          else
            this.Msg = Ah_MsgFormat("Conto %1 non valorizzato", ALLTRIM(Nvl(this.w_CODICE," ")))
            INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
            VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
            this.w_ERROR = .T.
            this.w_VALORE = 0
          endif
        else
          this.w_VALORE = 0
        endif
      case VRTIPVOC = "T"
        * --- calcolo solo Voci di Bilancio
        this.w_CODICE = DVCODVOB
        this.w_ROWVOC = CPROWORD1
         
 SELECT SUM(CursDrivers.t_MRIMPORT) AS t_MRIMPORT FROM CursDrivers ; 
 WHERE CursDrivers.t_MRCODVOA = this.w_CODICE group by CursDrivers.t_MRCODVOA into Cursor PSG
        SELECT PSG
        if RECCOUNT("PSG")>0
          this.w_VALORE = PSG.t_MRIMPORT
        else
          * --- Nel caso di Voce Totalizzante a parita di Voce  confronto la sequenza indicata nella struttura 
          *     con la sequenaza delle voci sommate indicate nella Voce di Bilancio
           
 SELECT COUNT(CurBilancio.CODICE) AS NUMVOC FROM CurBilancio ; 
 WHERE CurBilancio.CODICE = this.w_CODICE group by CurBilancio.CODICE into Cursor __VOC__
          if __VOC__.NUMVOC>1
             
 SELECT SUM(CurBilancio.MRIMPORT) AS MRIMPORT FROM CurBilancio ; 
 WHERE CurBilancio.CODICE = this.w_CODICE AND CurBilancio.CPROWORD1>this.w_RIGTOT group by CurBilancio.CODICE into Cursor PSG
          else
             
 SELECT SUM(CurBilancio.MRIMPORT) AS MRIMPORT FROM CurBilancio ; 
 WHERE CurBilancio.CODICE = this.w_CODICE group by CurBilancio.CODICE into Cursor PSG
          endif
          SELECT PSG
          if RECCOUNT("PSG")>0
            this.w_VALORE = PSG.MRIMPORT
          else
            this.Msg = Ah_MsgFormat("Voce %1 non ancora calcolata", ALLTRIM(Nvl(this.w_CODICE," ")))
            INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
            VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
            this.w_ERROR = .T.
            this.w_VALORE = 0
          endif
        endif
      otherwise
        this.w_VALORE = 0
    endcase
    if USED("PSG")
      SELECT PSG
      USE
    endif
    SELECT CursVoci
    this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLOPER,""))
    if ALLTRIM(Nvl(DVFLOPER," "))="-" OR ALLTRIM(Nvl(DVFLOPER," "))="+"
      this.Func = ALLTRIM(this.Func) + "1*"
    endif
    if DVFLPARE = "(" OR DVFLPARE = "(("
      * --- Inserisco parentesi tonde aperte all'inizio se ci sono '(' '(('
      this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLPARE,""))
    endif
    this.w_VALPSGS = STR(this.w_VALORE,18,4)
    this.w_VALORES = LEFT(this.w_VALPSGS,13) + "." + RIGHT(this.w_VALPSGS,4)
    if NOT EMPTY(Nvl(DVCONDIZ,""))
      * --- Se esiste la condizione la inserisco
      this.Func = ALLTRIM(this.Func) + "IIF(" + ALLTRIM(this.w_VALORES) + ALLTRIM(Nvl(DVCONDIZ,"")) + ","
    endif
    * --- nel then metto la formula
    this.Func = ALLTRIM(this.Func) + ALLTRIM(this.w_VALORES) + ALLTRIM(Nvl(DVFORMUL,""))
    if NOT EMPTY(Nvl(DVCONDIZ," "))
      * --- chiudo else se sono sotto condizione
      this.Func = ALLTRIM(this.Func) + "," + "0" + ")"
    endif
    if DVFLPARE = ")" OR DVFLPARE = "))"
      * --- Chiudo le parentesi tonde
      this.Func = ALLTRIM(this.Func) + ALLTRIM(Nvl(DVFLPARE,""))
    endif
    ENDSCAN
    if NOT EMPTY(ALLTRIM(CursSelez.VRFORMUL))
      this.Func = ALLTRIM(this.Func) + ")"
      if this.Func = "()"
        this.Func = ""
      endif
      this.Func = ALLTRIM(this.Func) + ALLTRIM(CursSelez.VRFLOPER)
      this.TmpSave = this.Func
      this.Func = ALLTRIM(this.Func) + ALLTRIM(CursSelez.VRFORMUL)
    else
      this.Func = ALLTRIM(this.Func) + ")"
    endif
    Messaggio = "Corretta"
    ON ERROR Messaggio = "Errata"
    TmpVal = this.Func
    * --- Valutazione dell'errore
    this.w_VALORE = &TmpVal
    ON ERROR
    if Messaggio = "Errata"
      this.Func = ALLTRIM(this.TmpSave)
      this.TmpSave = CursSelez.VRFORMUL
      this.w_OCCUR = RATC("C",this.TmpSave)
      do while this.w_OCCUR <> 0
        this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
        this.w_INDIC = VAL(SUBSTR(this.TmpSave,this.w_OCCUR+1,2))
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(STR(COST(this.w_INDIC)))
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+3))
        this.TmpSave = ALLTRIM(this.TmpSave1)
        this.w_OCCUR = RATC("C",this.TmpSave)
      enddo
      this.Func = ALLTRIM(this.Func) + ALLTRIM(this.TmpSave)
      Messaggio = "Corretta"
      ON ERROR Messaggio = "Errata"
      TmpVal = this.Func
      this.w_VALORE = &TmpVal
      ON ERROR
      if Messaggio = "Errata"
        this.Msg = Ah_MsgFormat("Funzione calcolata Errata")
        INSERT INTO CursError (CODICE,CPROWORD1,DESCRI) ;
        VALUES (CursSelez.CODVOC,CursSelez.CPROWORD,this.Msg)
        this.w_ERROR = .T.
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PER_ELAB'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='GESTBILA'
    this.cWorkTables[5]='DETTBILA'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_PER_ELAB')
      use in _Curs_PER_ELAB
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
