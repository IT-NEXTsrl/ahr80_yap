* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_brb                                                        *
*              Attiva caricamento righe                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_127]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Par
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_brb",oParentObject,m.Par)
return(i_retval)

define class tgsbi_brb as StdBatch
  * --- Local variables
  Par = 0
  w_MRNUMBIL = space(6)
  w_MRCODESE = space(4)
  w_ARROPSG = space(1)
  w_NUMERO = 0
  w_ESERCI = space(4)
  w_DATABIL = ctod("  /  /  ")
  w_DESCRIBI = space(35)
  w_MONETE = space(1)
  w_DIVISA = space(3)
  w_SIMVAL = space(5)
  w_CAMVAL = 0
  w_DECIMI = 0
  w_TIPOSTAM = space(1)
  w_RIGHESI = space(1)
  w_VALAPP = space(3)
  w_VALUTA = space(3)
  w_STRUTTUR = space(15)
  w_PERIODO = space(15)
  w_DESCPER = space(35)
  w_STRUTT = space(15)
  w_INIZIO = .f.
  w_CONTA = 0
  w_BILANCIO = space(15)
  w_PECODICE = space(15)
  w_PROVVI = space(1)
  w_ESE = space(4)
  w_ARROPSG = space(1)
  * --- WorkFile variables
  VALUTE_idx=0
  PER_ELAB_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.Par = 1 AND this.oParentObject.w_SALVA
        this.oParentObject.GSBI_MRB.NotifyEvent("Salva")
      case this.Par = 3
        * --- Avvio Stampa
        this.w_NUMERO = this.oParentObject.w_GBNUMBIL
        this.w_STRUTTUR = this.oParentObject.w_GBSTRUCT
        this.w_ESERCI = this.oParentObject.w_GBBILESE
        this.w_DATABIL = this.oParentObject.w_GBDATBIL
        this.w_DESCRIBI = this.oParentObject.w_GBDESCRI
        this.w_MONETE = "c"
        this.w_VALUTA = this.oParentObject.w_VALUTA
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACODVAL,VADECTOT,VACAOVAL"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACODVAL,VADECTOT,VACAOVAL;
            from (i_cTable) where;
                VACODVAL = this.w_VALUTA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DIVISA = NVL(cp_ToDate(_read_.VACODVAL),cp_NullValue(_read_.VACODVAL))
          this.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          this.w_CAMVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_SIMVAL = this.oParentObject.w_SIMVAL
        this.w_TIPOSTAM = "S"
        this.w_RIGHESI = " "
        this.w_VALAPP = this.oParentObject.w_VALNAZ
        this.w_PERIODO = this.oParentObject.w_GBPERIOD
        * --- Read from PER_ELAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PER_ELAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PER_ELAB_idx,2],.t.,this.PER_ELAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PEDESCRI"+;
            " from "+i_cTable+" PER_ELAB where ";
                +"PECODICE = "+cp_ToStrODBC(this.w_PERIODO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PEDESCRI;
            from (i_cTable) where;
                PECODICE = this.w_PERIODO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCPER = NVL(cp_ToDate(_read_.PEDESCRI),cp_NullValue(_read_.PEDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do GSBI_BB3 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Par = 2
        * --- Avvio caricamento driver
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Par = 4
        * --- Avvio caricamento dati
        this.w_MRNUMBIL = this.oParentObject.w_GBNUMBIL
        this.w_MRCODESE = this.oParentObject.w_GBBILESE
        this.oParentObject.w_SALVA = .F.
        this.w_ARROPSG = this.oParentObject.w_ARROTOND
        * --- Memorizzazione Dati testata Bilancio
        this.oParentobject.GotFocus()
        this.oParentobject.ecpSave()
        this.oParentobject.cFunction="Query"
        this.oParentObject.w_GBNUMBIL = this.w_MRNUMBIL
        this.oParentObject.w_GBBILESE = this.w_MRCODESE
        * --- Creazione cursore delle chiavi
        this.oParentobject.GSBI_MRB.QueryKeySet( "MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)+ ;
        " and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE) , "" )
        this.oParentobject.QueryKeySet( "GBNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)+ ;
        " and GBBILESE="+cp_ToStrODBC(this.w_MRCODESE) , "" )
        * --- Caricamento record ed accesso in modifica
        this.oParentobject.LoadRec()
        this.oParentobject.NotifyEvent("Edit")
        this.oParentobject.ecpEdit()
        * --- Posizionamento sulla seconda pagina
        this.oParentobject.oPgFrm.ActivePage=3
        this.oParentObject.w_ARROTOND = this.w_ARROPSG
        this.oParentObject.GSBI_MRB.NotifyEvent("Salva")
      case this.Par = 5
        * --- Caricamento cursore drivers
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_STRUTT =  this.oParentObject.oParentObject.w_GBSTRUCT
    this.w_ARROPSG = this.oParentObject.oParentObject.w_ARROTON
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_BRD",this,"DRIVER")
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI1BRD",this,"DRIVER1")
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI2BRD",this,"DRIVER2")
    SELECT DRIVER.* ;
    FROM DRIVER INTO CURSOR CursDriver ;
    UNION SELECT DRIVER1.* FROM DRIVER1 ;
    UNION SELECT DRIVER2.* FROM DRIVER2
    SELECT (this.oParentObject.cTrsName)
    ZAP
    this.w_INIZIO = .T.
    this.w_CONTA = 1
    * --- Carica il Transitorio
    SELECT CursDriver
    GO TOP
    SCAN
    if this.w_INIZIO
      * --- Append gia' eseguito la prima volta nella BlankRec
      this.oParentObject.i_nRowNum=0
      this.w_INIZIO = .F.
    endif
    this.oParentObject.InitRow()
    * --- Carico le variabili
    select CursDriver
    this.oParentObject.w_MRCODVOC = NVL(CursDriver.VRCODVOC," ")
    this.oParentObject.w_DESCRI = NVL(CursDriver.VRDESVOC," ")
    this.oParentObject.w_CPROWNUM = this.w_CONTA
    this.w_CONTA = this.w_CONTA + 1
    this.oParentObject.w_MRIMPORT = 0
    this.oParentObject.TrsFromWork()
    select CursDriver
    ENDSCAN
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
    if RECCOUNT("CursDriver")=0
      this.oParentObject.oParentObject.NotifyEvent("Vuoto")
    endif
    if used("CursDriver")
      SELECT CursDriver
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_BILANCIO = this.oParentObject.w_GBSTRUCT
    this.w_PECODICE = this.oParentObject.w_GBPERIOD
    this.w_PROVVI = this.oParentObject.w_GBTIPMOV
    this.w_ESE = this.oParentObject.w_GBBILESE
    this.w_ARROPSG = this.oParentObject.w_ARROTON
    this.w_MRNUMBIL = this.oParentObject.w_GBNUMBIL
    this.w_MRCODESE = this.oParentObject.w_GBBILESE
    this.oParentObject.w_SALVA = .F.
    * --- Memorizzazione Dati testata Bilancio
    this.oParentobject.GotFocus()
    this.oParentobject.ecpSave()
    this.oParentobject.cFunction="Query"
    * --- w_ONUMBIL definita nell'area manuale declare variable dell'anagrafiva GSBI_AGB (Gestione bilancio)
    *     Memorizza il numero bilancio.
    *     Serve per la gestione in multiutenza altrimenti teneva in memoria il numero sbagliato.
    this.oParentObject.w_GBNUMBIL = this.oParentObject.w_ONUMBIL
    this.w_MRNUMBIL = this.oParentObject.w_ONUMBIL
    this.oParentObject.w_GBBILESE = this.w_MRCODESE
    * --- Creazione cursore delle chiavi
    this.oParentobject.GSBI_MEG.QueryKeySet( "MRNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)+ ;
    " and MRCODESE="+cp_ToStrODBC(this.w_MRCODESE) , "" )
    this.oParentobject.QueryKeySet( "GBNUMBIL="+cp_ToStrODBC(this.w_MRNUMBIL)+ ;
    " and GBBILESE="+cp_ToStrODBC(this.w_MRCODESE) , "" )
    * --- Caricamento record ed accesso in modifica
    this.oParentobject.LoadRec()
    this.oParentobject.NotifyEvent("Edit")
    this.oParentobject.ecpEdit()
    this.oParentObject.w_CARICA = .T.
    * --- Posizionamento sulla seconda pagina
    this.oParentobject.oPgFrm.ActivePage=2
    this.oParentObject.w_ARROTOND = this.w_ARROPSG
    this.oParentObject.GSBI_MEG.NotifyEvent("Salva")
  endproc


  proc Init(oParentObject,Par)
    this.Par=Par
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PER_ELAB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Par"
endproc
