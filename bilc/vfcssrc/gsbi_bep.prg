* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bep                                                        *
*              Elabora generazione bilancio                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_25]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2000-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bep",oParentObject,m.pOper)
return(i_retval)

define class tgsbi_bep as StdBatch
  * --- Local variables
  pOper = space(4)
  w_ZOOM = space(10)
  w_IMPO = space(100)
  w_IMPORTO = space(100)
  w_IMPORT = 0
  w_MESS = space(10)
  w_FLERR = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    Messaggio = "Corretta"
    do case
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="ELAB"
        * --- Crea il File delle Messaggistiche di Errore
        this.w_FLERR = .F.
        CREATE CURSOR MessErr (MSG C(120))
        NC = this.w_ZOOM.cCursor
        SELECT &NC
        GO TOP
        * --- Cicla sui record Selezionati
        SCAN FOR XCHK<>0
        if (VRFLDESC = "N" AND this.oParentObject.w_TIPBILAN="G") OR (this.oParentObject.w_TIPBILAN="A" AND VRFLDESC <> "D")
          * --- Calcolo i valori
          this.w_IMPO = STR(MRIMPORT,18,4)
          this.w_IMPORTO = LEFT(this.w_IMPO,13)+"."+RIGHT(this.w_IMPO,4)
          this.w_IMPORTO = LEFT(this.w_IMPORTO,13+IIF(this.oParentObject.w_DECIMA=0,0,this.oParentObject.w_DECIMA+1))
          * --- Trasformo l'importo in stringa per poi applicare la costante
          this.w_IMPORTO = ALLTRIM(this.w_IMPORTO)+ALLTRIM(this.oParentObject.w_COSTANTE)
          * --- Importo a cui ho applicato la costante
        else
          this.w_IMPORTO = "0"
        endif
        ON ERROR Messaggio="Errata"
        this.w_IMPORT = EVAL(alltrim(this.w_IMPORTO))
        ON ERROR
        if Messaggio = "Errata"
          this.w_FLERR = .T.
          this.w_MESS = Ah_MsgFormat("Verificare riga: %1 funzione di ricalcolo errata",ALLTRIM(STR(NVL(CPROWORD,0))))
          INSERT INTO MessErr (MSG) VALUES (left(this.w_MESS,120))
          SELECT &NC
        else
          REPLACE MRIMPORT WITH this.w_IMPORT, XCHK WITH 0
        endif
        ENDSCAN
        if Messaggio <> "Errata"
          this.oParentObject.w_COSTANTE = space(100)
        endif
        if this.w_FLERR=.T. AND USED("MessErr")
          if RECCOUNT("MessErr") > 0
            this.w_MESS = "Durante l'elaborazione alcune funzioni di ricalcolo associate alle righe hanno segnalato errori.%0Stampo situazione messaggi di errore?"
            if ah_YesNo(this.w_MESS)
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("QUERY\GSVE_BCV.FRX", " ", this)
            endif
          endif
        endif
        if USED("MessErr")
          SELECT MessErr
          USE
        endif
        if USED("__TMP__")
          SELECT __TMP__
          USE
        endif
        SELECT &NC
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
