* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b11                                                        *
*              Elabora coge primanota                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_60]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-07-21                                                      *
* Last revis.: 2007-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b11",oParentObject)
return(i_retval)

define class tgsbi_b11 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_CODBUN = space(3)
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_SCARTO = 0
  w_COD003 = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_RISATT = space(15)
  w_RECODICE = space(15)
  w_DATREG = ctod("  /  /  ")
  w_RATATT = space(15)
  w_RISPAS = space(15)
  w_REDESCRI = space(45)
  w_RATPAS = space(15)
  w_FATTEMET = space(15)
  w_FATTRICE = space(15)
  w_TIPMAS = space(1)
  * --- WorkFile variables
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Storno Risconti rilevati in periodi precedenti
    * --- Non utilizzare per estrarre i dati di inizio esercizio dopo avere effettuato l'apertura di Bilancio
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alla Primanota COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTCG001
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_FILINI = this.oParentObject.oParentObject.w_DATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_DATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_RISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_RISPAS
    if USED("EXTCG001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), CODBUN C(3), TOTIMP N(18,4), TIPMAS C(1))
      SELECT EXTCG001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "MG" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_TIPMAS = "G"
      this.w_CODICE = CODICE
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_CODBUN = NVL(CODBUN, g_CODBUN)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_SCARTO = 0
      this.w_RISCON = 0
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, this.w_DATREG)
        this.w_TOTIMP = cp_ROUND(VAL2MON(this.w_TOTIMP, this.w_CAOVAL, g_CAOVAL, i_DATSYS, g_PERVAL), g_PERPVL)
      endif
      * --- Si presume che la competenza sia presente e successiva alla data di registrazioen, ma interna al periodo
      this.w_GIOCOM = (this.w_FINCOM+1) - this.w_INICOM
      * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
      * --- Lo scarto viene eliminato senza generare registrazioni di storno
      if this.w_INICOM<this.w_FILINI AND this.w_GIOCOM<>0
        this.w_GIOPER = this.w_FILINI - this.w_INICOM
        this.w_SCARTO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
      endif
      * --- Registrazione con Competenza che Prosegue oltre il Periodo, elimina la parte non di competenza
      * --- Non generiamo risconti perch� le registrazioni consitono nello storno del risconto
      if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0
        this.w_GIOPER = this.w_FINCOM - this.w_FILFIN
        this.w_SCARTO = this.w_SCARTO + cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
      endif
      * --- Ricalcola l'Importo di Competenza
      this.w_TOTIMP = this.w_TOTIMP - this.w_SCARTO
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      * --- Devo stornare il risconto per la quota parte: Es: Acquisti a Risconti Attivi
      * --- Quindi scrivo la prima riga con il conto acquisti e la seconda per il risconto
      if this.w_TOTIMP<>0
        INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, TIPMAS) ;
        VALUES (this.w_TIPCON, this.w_CODICE, this.w_CODBUN, this.w_TOTIMP, NVL(this.w_TIPMAS, "N"))
        this.w_APPO = IIF(this.w_TOTIMP>0, this.w_RISATT, this.w_RISPAS)
        INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, TIPMAS) ;
        VALUES ("G", this.w_APPO, this.w_CODBUN, -1*this.w_TOTIMP, NVL(this.w_TIPMAS, "N"))
      endif
      SELECT EXTCG001
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTCG001
      USE
      * --- 3^OPERAZIONE: Raggruppa tutto quanto per Tipo, Codice, Business Unit
      if USED("TmpAgg1")
        SELECT tmpAgg1
        if RECCOUNT()>0
          SELECT TIPCON, CODICE, CODBUN, SUM(TOTIMP) AS TOTALE, TIPMAS FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY TIPCON, TIPMAS, CODICE, CODBUN
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_CODBUN = NVL(CODBUN, g_CODBUN)
          this.w_COD001 = SPACE(15)
          this.w_TIPMAS = "G"
          this.w_TIPCON = "G"
          this.w_COD002 = CODICE
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTROPA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
