* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bca                                                        *
*              Controlla obsolescenza                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_12]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-28                                                      *
* Last revis.: 2004-01-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,POPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bca",oParentObject,m.POPER)
return(i_retval)

define class tgsbi_bca as StdBatch
  * --- Local variables
  POPER = space(1)
  w_DTOBSO = ctod("  /  /  ")
  * --- WorkFile variables
  CONTI_idx=0
  VOC_COST_idx=0
  CENCOST_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- VERIFICA LA DATA D'OBSOLESCENZA NELLE MASCHERE GSBI_MCG E GSBI_MCA
    do case
      case this.POPER="C"
        * --- Conti
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDTOBSO"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CGCODCON);
                +" and ANTIPCON = "+cp_ToStrODBC("G");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDTOBSO;
            from (i_cTable) where;
                ANCODICE = this.oParentObject.w_CGCODCON;
                and ANTIPCON = "G";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<this.oParentObject.w_PERINI AND NOT EMPTY(this.oParentObject.w_CGCODCON)
          this.oParentObject.w_CGCODCON = SPACE(15)
          this.oParentObject.w_DESCRI = SPACE(40)
          ah_ErrorMsg("Codice conto obsoleto dal %1",,"",dtoc(this.w_dtobso))
          i_retcode = 'stop'
          return
        endif
      case this.POPER="V"
        * --- Voci di costo\ricavo
        * --- Read from VOC_COST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VCDTOBSO"+;
            " from "+i_cTable+" VOC_COST where ";
                +"VCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CAVOCCEN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VCDTOBSO;
            from (i_cTable) where;
                VCCODICE = this.oParentObject.w_CAVOCCEN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBSO = NVL(cp_ToDate(_read_.VCDTOBSO),cp_NullValue(_read_.VCDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<this.oParentObject.w_PERINI AND NOT EMPTY(this.oParentObject.w_CAVOCCEN)
          this.oParentObject.w_CAVOCCEN = SPACE(15)
          this.oParentObject.w_DESVOC = SPACE(40)
          ah_ErrorMsg("Codice obsoleto dal %1",,"",dtoc(this.w_dtobso))
          i_retcode = 'stop'
          return
        endif
      case this.POPER="R"
        * --- centro di costo\ricavo
        * --- Read from CENCOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDTOBSO"+;
            " from "+i_cTable+" CENCOST where ";
                +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_CACODCEN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDTOBSO;
            from (i_cTable) where;
                CC_CONTO = this.oParentObject.w_CACODCEN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBSO = NVL(cp_ToDate(_read_.CCDTOBSO),cp_NullValue(_read_.CCDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<this.oParentObject.w_PERINI AND NOT EMPTY(this.oParentObject.w_CACODCEN)
          this.oParentObject.w_CACODCEN = SPACE(15)
          this.oParentObject.w_DESPIA = SPACE(40)
          ah_ErrorMsg("Codice obsoleto dal %1",,"",dtoc(this.w_dtobso))
          i_retcode = 'stop'
          return
        endif
      case this.POPER="M"
        * --- commessa
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDTOBSO"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.oParentObject.w_CACODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDTOBSO;
            from (i_cTable) where;
                CNCODCAN = this.oParentObject.w_CACODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DTOBSO = NVL(cp_ToDate(_read_.CNDTOBSO),cp_NullValue(_read_.CNDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<this.oParentObject.w_PERINI AND NOT EMPTY(this.oParentObject.w_CACODCOM)
          this.oParentObject.w_CACODCOM = SPACE(15)
          this.oParentObject.w_DESCAN = SPACE(40)
          ah_ErrorMsg("Codice obsoleto dal %1",,"",dtoc(this.w_dtobso))
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,POPER)
    this.POPER=POPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='CAN_TIER'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="POPER"
endproc
