* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_b02                                                        *
*              Elabora coge rimanenze                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_32]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2007-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_b02",oParentObject)
return(i_retval)

define class tgsbi_b02 as StdBatch
  * --- Local variables
  w_TOTIMP = 0
  w_QTAESI = 0
  w_ERRORE = .f.
  w_VALNAZ = space(3)
  w_COSTO = 0
  w_MESS = space(10)
  w_CAOVAL = 0
  w_COD001 = space(15)
  w_RECODICE = space(15)
  w_COD002 = space(15)
  w_TIPCON = space(10)
  w_REDESCRI = space(45)
  w_COD003 = space(15)
  w_TIPVAL = 0
  w_IMPDAR = 0
  w_CONRIM = space(15)
  w_IMPAVE = 0
  w_CONRIC = space(15)
  w_APPO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Mov.Extracontabili (da GSBI_BEL)
    * --- Viene impostato nelle regole di Elaborazione relativi alle Rimanenze di Magazzino COGE
    * --- Opera su un Precedente Cursore generato dalla Query EXTRI001
    this.w_RECODICE = this.oParentObject.w_RECODICE
    this.w_REDESCRI = this.oParentObject.w_REDESCRI
    this.w_ERRORE = .F.
    if USED("EXTRI001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (CODCON C(15), TOTIMP N(18,4))
      SELECT EXTRI001
      GO TOP
      SCAN FOR NVL(QTAESI,0)<>0 AND NOT EMPTY(NVL(TIPVAL," "))
      this.w_TIPVAL = VAL(TIPVAL)
      this.w_CONRIM = NVL(CONRIM," ")
      this.w_CONRIC = NVL(CONRIC," ")
      this.w_QTAESI = QTAESI
      this.w_COSTO = 0
      * --- Calcola il Costo in Funzione del Criterio
      this.w_COSTO = IIF(this.w_TIPVAL=1, NVL(COSMPA,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=2, NVL(COSMPP,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=3, NVL(COSULT,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=4, NVL(COSSTA,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=5, NVL(COSFCO,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=6, NVL(COSLCO,0), this.w_COSTO)
      this.w_COSTO = IIF(this.w_TIPVAL=7, NVL(COSLSC,0), this.w_COSTO)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_TOTIMP = this.w_QTAESI * this.w_COSTO
      if this.w_TOTIMP<>0
        * --- Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
        if this.w_VALNAZ<>g_PERVAL
          this.w_CAOVAL = GETCAM(this.w_VALNAZ, i_DATSYS)
          this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, 1, i_DATSYS, g_PERVAL, g_PERPVL)
        endif
        if NOT EMPTY(this.w_CONRIM) AND NOT EMPTY(this.w_CONRIC)
          * --- Scrive il Temporaneo di Appoggio interno al Batch CONRIM (Def.DARE)
          INSERT INTO TmpAgg1 (CODCON, TOTIMP) ;
          VALUES (this.w_CONRIM, this.w_TOTIMP)
          * --- Scrive il Temporaneo di Appoggio interno al Batch CONRIC (Def.AVERE)
          INSERT INTO TmpAgg1 (CODCON, TOTIMP) ;
          VALUES (this.w_CONRIC, -this.w_TOTIMP)
        else
          * --- Test se ci sono registrazioni senza Contropartite
          this.w_ERRORE = .T.
        endif
        SELECT EXTRI001
      endif
      ENDSCAN
      * --- Elimina il Cursore della Query
      SELECT EXTRI001
      USE
      * --- Raggruppa tutto quanto per Codice Conto
      if USED("TmpAgg1")
        if this.w_ERRORE=.T.
          this.w_MESS = "ATTENZIONE:%0Alcuni articoli non sono associati a categorie omogenee%0Proseguo ugualmente?"
          this.w_ERRORE = NOT ah_YesNo(this.w_MESS)
        endif
        SELECT tmpAgg1
        if this.w_ERRORE=.F. AND RECCOUNT()>0
          SELECT CODCON, SUM(TOTIMP) AS TOTALE FROM TmpAgg1 ;
          INTO CURSOR TmpAgg1 GROUP BY CODCON
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT tmpAgg1
          GO TOP
          SCAN FOR NVL(TOTALE,0)<>0 AND NOT EMPTY(NVL(CODCON," "))
          this.w_TIPCON = "G"
          this.w_COD001 = SPACE(15)
          this.w_COD002 = CODCON
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTALE>0, TOTALE, 0)
          this.w_IMPAVE = IIF(TOTALE<0, ABS(TOTALE), 0)
          INSERT INTO TmpAgg (CODICE, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE) ;
          VALUES (this.w_RECODICE, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE)
          SELECT tmpAgg1
          ENDSCAN
        endif
        SELECT tmpAgg1
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
