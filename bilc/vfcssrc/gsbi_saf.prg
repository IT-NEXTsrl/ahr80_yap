* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_saf                                                        *
*              Stampa voci di bilancio analitica                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_20]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-29                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_saf",oParentObject))

* --- Class definition
define class tgsbi_saf as StdForm
  Top    = 24
  Left   = 19

  * --- Standard Properties
  Width  = 580
  Height = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=124388969
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  VOC_BIAN_IDX = 0
  cPrg = "gsbi_saf"
  cComment = "Stampa voci di bilancio analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VOCEINIZ = space(15)
  w_DESINI = space(50)
  w_VOCEFINA = space(15)
  w_DESFIN = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_safPag1","gsbi_saf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVOCEINIZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VOC_BIAN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VOCEINIZ=space(15)
      .w_DESINI=space(50)
      .w_VOCEFINA=space(15)
      .w_DESFIN=space(50)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_VOCEINIZ))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_VOCEFINA))
          .link_1_3('Full')
        endif
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VOCEINIZ
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_BIAN_IDX,3]
    i_lTable = "VOC_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2], .t., this.VOC_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCEINIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVA',True,'VOC_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_VOCEINIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_VOCEINIZ))
          select VRCODVOC,VRDESVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCEINIZ)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCEINIZ) and !this.bDontReportError
            deferred_cp_zoom('VOC_BIAN','*','VRCODVOC',cp_AbsName(oSource.parent,'oVOCEINIZ_1_1'),i_cWhere,'GSBI_AVA',"Voci di bilancio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCEINIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_VOCEINIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_VOCEINIZ)
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCEINIZ = NVL(_Link_.VRCODVOC,space(15))
      this.w_DESINI = NVL(_Link_.VRDESVOC,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_VOCEINIZ = space(15)
      endif
      this.w_DESINI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_VOCEFINA) OR (.w_VOCEFINA >= .w_VOCEINIZ)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore o uguale a quello finale")
        endif
        this.w_VOCEINIZ = space(15)
        this.w_DESINI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCEINIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCEFINA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_BIAN_IDX,3]
    i_lTable = "VOC_BIAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2], .t., this.VOC_BIAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCEFINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBI_AVA',True,'VOC_BIAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VRCODVOC like "+cp_ToStrODBC(trim(this.w_VOCEFINA)+"%");

          i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VRCODVOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VRCODVOC',trim(this.w_VOCEFINA))
          select VRCODVOC,VRDESVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VRCODVOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCEFINA)==trim(_Link_.VRCODVOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCEFINA) and !this.bDontReportError
            deferred_cp_zoom('VOC_BIAN','*','VRCODVOC',cp_AbsName(oSource.parent,'oVOCEFINA_1_3'),i_cWhere,'GSBI_AVA',"Voci di bilancio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',oSource.xKey(1))
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCEFINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VRCODVOC,VRDESVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VRCODVOC="+cp_ToStrODBC(this.w_VOCEFINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VRCODVOC',this.w_VOCEFINA)
            select VRCODVOC,VRDESVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCEFINA = NVL(_Link_.VRCODVOC,space(15))
      this.w_DESFIN = NVL(_Link_.VRDESVOC,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_VOCEFINA = space(15)
      endif
      this.w_DESFIN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_VOCEINIZ) OR (.w_VOCEFINA >= .w_VOCEINIZ)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore o uguale a quello finale")
        endif
        this.w_VOCEFINA = space(15)
        this.w_DESFIN = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_BIAN_IDX,2])+'\'+cp_ToStr(_Link_.VRCODVOC,1)
      cp_ShowWarn(i_cKey,this.VOC_BIAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCEFINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVOCEINIZ_1_1.value==this.w_VOCEINIZ)
      this.oPgFrm.Page1.oPag.oVOCEINIZ_1_1.value=this.w_VOCEINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_2.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_2.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCEFINA_1_3.value==this.w_VOCEFINA)
      this.oPgFrm.Page1.oPag.oVOCEFINA_1_3.value=this.w_VOCEFINA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_4.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_4.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_VOCEFINA) OR (.w_VOCEFINA >= .w_VOCEINIZ))  and not(empty(.w_VOCEINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVOCEINIZ_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale deve essere minore o uguale a quello finale")
          case   not(EMPTY(.w_VOCEINIZ) OR (.w_VOCEFINA >= .w_VOCEINIZ))  and not(empty(.w_VOCEFINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVOCEFINA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale deve essere minore o uguale a quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_safPag1 as StdContainer
  Width  = 576
  height = 112
  stdWidth  = 576
  stdheight = 112
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVOCEINIZ_1_1 as StdField with uid="UAAWLFLTJA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_VOCEINIZ", cQueryName = "VOCEINIZ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale deve essere minore o uguale a quello finale",;
    ToolTipText = "Voce di bilancio di inizio selezione",;
    HelpContextID = 76579152,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=88, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_BIAN", cZoomOnZoom="GSBI_AVA", oKey_1_1="VRCODVOC", oKey_1_2="this.w_VOCEINIZ"

  func oVOCEINIZ_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCEINIZ_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCEINIZ_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_BIAN','*','VRCODVOC',cp_AbsName(this.parent,'oVOCEINIZ_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVA',"Voci di bilancio",'',this.parent.oContained
  endproc
  proc oVOCEINIZ_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_VOCEINIZ
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_2 as StdField with uid="SXFPITXTXM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 113537846,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=207, Top=9, InputMask=replicate('X',50)

  add object oVOCEFINA_1_3 as StdField with uid="QEZXMHDSSU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VOCEFINA", cQueryName = "VOCEFINA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale deve essere minore o uguale a quello finale",;
    ToolTipText = "Voce di bilancio di fine selezione",;
    HelpContextID = 163610985,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=88, Top=37, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_BIAN", cZoomOnZoom="GSBI_AVA", oKey_1_1="VRCODVOC", oKey_1_2="this.w_VOCEFINA"

  func oVOCEFINA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCEFINA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCEFINA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_BIAN','*','VRCODVOC',cp_AbsName(this.parent,'oVOCEFINA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBI_AVA',"Voci di bilancio",'',this.parent.oContained
  endproc
  proc oVOCEFINA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSBI_AVA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VRCODVOC=this.parent.oContained.w_VOCEFINA
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_4 as StdField with uid="FPSRXOHPNJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 191984438,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=207, Top=37, InputMask=replicate('X',50)


  add object oBtn_1_7 as StdButton with uid="WZFWVQXIDZ",left=462, top=62, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 124360218;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSBI_BAF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="EFZTAPUEYA",left=518, top=62, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117071546;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="JZEWLBUUEQ",Visible=.t., Left=11, Top=9,;
    Alignment=1, Width=75, Height=15,;
    Caption="Da voce:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YCQZTRNBCB",Visible=.t., Left=20, Top=37,;
    Alignment=1, Width=66, Height=15,;
    Caption="A voce:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_saf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
