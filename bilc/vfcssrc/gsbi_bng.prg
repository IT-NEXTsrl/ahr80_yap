* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_bng                                                        *
*              Stampa confronto N bilanci                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_58]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-02                                                      *
* Last revis.: 2014-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsbi_bng",oParentObject,m.pParam)
return(i_retval)

define class tgsbi_bng as StdBatch
  * --- Local variables
  pParam = space(6)
  w_STRUT1 = space(15)
  w_DIVISA1 = space(3)
  w_ZOOM = space(10)
  w_MESS = space(200)
  w_DESCRI = space(90)
  w_NUMPER = 0
  w_OK = .f.
  w_PRIMO = .f.
  w_NUMBIL = 0
  w_BILESE = space(4)
  w_NUMERO = 0
  w_TIPPER = space(5)
  w_PERIODO = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa N Confronto Bilanci (GSBI_SNG)
    this.w_ZOOM = this.oParentObject.w_ZoomGepr
    if this.pParam = "CARICA"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    SELECT * FROM (this.w_ZOOM.cCursor) INTO CURSOR CursSelez WHERE XCHK=1
    * --- Controllo i bilanci selezionati hanno :
    * --- 1 - Valuta
    * --- 2 - Struttura
    this.w_OK = .T.
    this.w_PRIMO = .T.
    if RECCOUNT("CursSelez")<2
      this.w_MESS = "Selezionare almeno due bilanci"
      this.w_OK = .F.
    endif
    if RECCOUNT("CursSelez")>12
      this.w_MESS = "Selezionare al massimo 12 bilanci"
      this.w_OK = .F.
    endif
    SELECT CursSelez
    GO TOP
    SCAN WHILE this.w_OK
    if this.w_PRIMO
      this.w_DIVISA1 = CursSelez.ESVALNAZ
      this.w_STRUT1 = CursSelez.GBSTRUCT
      this.w_PRIMO = .F.
    else
      if this.w_DIVISA1<>CursSelez.ESVALNAZ AND this.w_OK
        this.w_MESS = "Bilanci con valute differenti"
        this.w_OK = .F.
      endif
      if this.w_STRUT1<>CursSelez.GBSTRUCT AND this.w_OK
        this.w_MESS = "Bilanci con strutture differenti"
        this.w_OK = .F.
      endif
    endif
    ENDSCAN
    * --- Segnalazione ERRORE
    if not this.w_OK
      if used("CursSelez")
        SELECT CursSelez
        USE
      endif
      ah_ErrorMsg(this.w_MESS,,"")
      i_retcode = 'stop'
      return
    endif
    * --- Carico il primo bilancio per definire la struttura
    this.w_PRIMO = .T.
    SELECT CursSelez
    GO TOP
    SCAN
    this.w_NUMBIL = CursSelez.GBNUMBIL
    this.w_BILESE = CursSelez.GBBILESE
    this.w_TIPPER = CursSelez.PETIPPER
    this.w_PERIODO = CursSelez.GBPERIOD
    this.w_NUMPER = CursSelez.PENUMPER
    this.w_DESCRI = this.w_PERIODO
    this.w_NUMERO = this.w_NUMERO + 1
    vq_exec("..\bilc\exe\query\gsbi1bng",this,"CursBila1")
    if this.w_PRIMO
      SELECT CursBila1.* ,CursBila1.MRDESCRI AS MRDESCRI0, CursBila1.MRIMPORT AS MRIMPORT1, CursBila1.MRDESCRI AS MRDESCRI1, ;
      CursBila1.MRIMPORT AS MRIMPORT2, CursBila1.MRDESCRI AS MRDESCRI2, CursBila1.MRIMPORT AS MRIMPORT3, ;
      CursBila1.MRDESCRI AS MRDESCRI3, CursBila1.MRIMPORT AS MRIMPORT4, CursBila1.MRDESCRI AS MRDESCRI4, ;
      CursBila1.MRIMPORT AS MRIMPORT5, CursBila1.MRDESCRI AS MRDESCRI5, CursBila1.MRIMPORT AS MRIMPORT6, ;
      CursBila1.MRDESCRI AS MRDESCRI6, CursBila1.MRIMPORT AS MRIMPORT7, CursBila1.MRDESCRI AS MRDESCRI7, ;
      CursBila1.MRIMPORT AS MRIMPORT8, CursBila1.MRDESCRI AS MRDESCRI8, CursBila1.MRIMPORT AS MRIMPORT9 ,;
      CursBila1.MRDESCRI AS MRDESCRI9, CursBila1.MRIMPORT AS MRIMPORT10, CursBila1.MRDESCRI AS MRDESCRI10, ;
      CursBila1.MRIMPORT AS MRIMPORT11, CursBila1.MRDESCRI AS MRDESCRI11 FROM CursBila1 INTO CURSOR CursBila WHERE 1=1
      b = WRCURSOR("CursBila")
      UPDATE CursBila SET MRIMPORT1=0,MRIMPORT2=0,MRIMPORT3=0, ;
      MRIMPORT4=0,MRIMPORT5=0,MRIMPORT6=0,MRIMPORT7=0, ;
      MRIMPORT8=0,MRIMPORT9=0,MRIMPORT10=0,MRIMPORT11=0,;
      MRDESCRI0=this.w_DESCRI,MRDESCRI1=SPACE(90),MRDESCRI2=SPACE(90),MRDESCRI3=SPACE(90),;
      MRDESCRI4=SPACE(90),MRDESCRI5=SPACE(90),MRDESCRI6=SPACE(90),MRDESCRI7=SPACE(90),;
      MRDESCRI8=SPACE(90),MRDESCRI9=SPACE(90),MRDESCRI10=SPACE(90),MRDESCRI11=SPACE(90)
      this.w_PRIMO = .F.
    else
      NAMEVAR = "MRIMPORT" + ALLTRIM(STR(this.w_NUMERO-1))
      NAMEDES = "MRDESCRI" + ALLTRIM(STR(this.w_NUMERO-1))
      SELECT CursBila1
      GO TOP
      SCAN
      UPDATE CursBila SET &NAMEVAR=CursBila1.MRIMPORT ;
      WHERE CursBila1.CPROWORD=CursBila.CPROWORD
      UPDATE CursBila SET &NAMEDES=this.w_DESCRI 
      ENDSCAN
    endif
    if used("CursBila1")
      SELECT CursBila1
      USE
    endif
    ENDSCAN
    * --- Lancio la Stampa
    SELECT *, IIF(UPPER(THIS.OPARENTOBJECT.CLASS)="TGSBI_SNA","ANALITICA","CONTABILE") AS TIPO FROM CursBila ;
    INTO CURSOR __TMP__ ORDER BY CPROWORD
    select __TMP__
    * --- Stampa solo su excel
    CP_CHPRN("..\BILC\EXE\QUERY\GSBI_BNG.XLT", " ", this)
    if used("CursBila")
      select CursBila
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Zoom
    ah_Msg("Elaborazione dati...",.T.)
    VQ_EXEC("..\BILC\EXE\QUERY\GSBI_BNG.VQR",this,"CursElab")
    * --- Cancello il contenuto dello zoom
    DELETE FROM ( this.w_ZOOM.cCursor )
    * --- Passo i valori allo ZOOM
    * --- Passo per un Array (Fox non accetta istruzioni del tipo Insert into from select ...)
    SELECT * FROM CursElab ;
    INTO ARRAY VETTORE ORDER BY GBBILESE,GBNUMBIL
    SELECT CursElab
    USE
    * --- Se il cursore � vuoto non crea il vettore
    if TYPE("VETTORE")<>"U"
      insert into ( this.w_ZOOM.cCursor ) from array vettore
    endif
    SELECT ( this.w_ZOOM.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom.grd.refresh
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
      This.w_Zoom.grd.Column&NC..Enabled=.f.
    endif
    ENDFOR
    if used("CursElab")
      SELECT CursElab
      USE
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
