* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsbi_kgs                                                        *
*              Treeview struttura bilancio contabile                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_107]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-28                                                      *
* Last revis.: 2008-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsbi_kgs",oParentObject))

* --- Class definition
define class tgsbi_kgs as StdForm
  Top    = 4
  Left   = 20

  * --- Standard Properties
  Width  = 469
  Height = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-09"
  HelpContextID=32114281
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsbi_kgs"
  cComment = "Treeview struttura bilancio contabile"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TRCODICE = space(15)
  w_TRDESCRI = space(40)
  w_CPROWORD = 0
  w_SEQELA = 0
  w_DESCRI = space(40)
  w_TIPORD = space(1)
  w_CURSORNA = space(10)
  w_TREEVIEW = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsbi_kgsPag1","gsbi_kgs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPORD_1_15
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    DoDefault()
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODICE=space(15)
      .w_TRDESCRI=space(40)
      .w_CPROWORD=0
      .w_SEQELA=0
      .w_DESCRI=space(40)
      .w_TIPORD=space(1)
      .w_CURSORNA=space(10)
      .w_TRCODICE=oParentObject.w_TRCODICE
      .w_TRDESCRI=oParentObject.w_TRDESCRI
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
      .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
          .DoRTCalc(1,2,.f.)
        .w_CPROWORD = Nvl( .w_TREEVIEW.GETVAR('CPROWORD') ,  0 )
        .w_SEQELA = Nvl ( .w_TREEVIEW.GETVAR('SEQELA') ,  0 )
        .w_DESCRI = Nvl( .w_TREEVIEW.GETVAR('DESCRI') , Space(40))
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_TIPORD = 'S'
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TRCODICE=.w_TRCODICE
      .oParentObject.w_TRDESCRI=.w_TRDESCRI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_CPROWORD = Nvl( .w_TREEVIEW.GETVAR('CPROWORD') ,  0 )
            .w_SEQELA = Nvl ( .w_TREEVIEW.GETVAR('SEQELA') ,  0 )
            .w_DESCRI = Nvl( .w_TREEVIEW.GETVAR('DESCRI') , Space(40))
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate(.f.)
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value==this.w_TRCODICE)
      this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value=this.w_TRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value==this.w_TRDESCRI)
      this.oPgFrm.Page1.oPag.oTRDESCRI_1_2.value=this.w_TRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_7.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_7.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oSEQELA_1_10.value==this.w_SEQELA)
      this.oPgFrm.Page1.oPag.oSEQELA_1_10.value=this.w_SEQELA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_11.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_15.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsbi_kgsPag1 as StdContainer
  Width  = 465
  height = 472
  stdWidth  = 465
  stdheight = 472
  resizeXpos=288
  resizeYpos=201
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODICE_1_1 as StdField with uid="YBFRDYVZIV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODICE", cQueryName = "TRCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura di bilancio",;
    HelpContextID = 195658107,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=66, Top=8, InputMask=replicate('X',15)

  add object oTRDESCRI_1_2 as StdField with uid="QYFGHLXPYS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRDESCRI", cQueryName = "TRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione struttura di bilancio",;
    HelpContextID = 110072191,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=198, Top=8, InputMask=replicate('X',40)


  add object TREEVIEW as cp_Treeview with uid="VXALGIWFHY",left=3, top=34, width=373,height=354,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="Vista",cShowFields="CAMPO",cNodeShowField="",cLeafShowField="",cNodeBmp="TMASTRO.BMP",cLeafBmp="TCONTO.BMP",nIndent=20,;
    cEvent = "Esegui",;
    nPag=1;
    , ToolTipText = "Visualizza struttura (prem. tasto destro del mouse per modif. riga)";
    , HelpContextID = 145883366


  add object oObj_1_5 as cp_runprogram with uid="EGQMNSEZES",left=470, top=29, width=276,height=23,;
    caption='GSBI_BGS(Reload)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGS("Reload")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 21621150


  add object oObj_1_6 as cp_runprogram with uid="FCFDTQCPRF",left=470, top=51, width=276,height=23,;
    caption='GSBI_BGS(Close)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGS("Close")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 49903417

  add object oCPROWORD_1_7 as StdField with uid="JKGLXGYDRW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 47869546,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=90, Top=417, cSayPict='"999999"', cGetPict='"999999"'

  add object oSEQELA_1_10 as StdField with uid="LZGQEOOFJZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SEQELA", cQueryName = "SEQELA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di aequenza elaborazione",;
    HelpContextID = 199207898,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=240, Top=417, cSayPict='"999999"', cGetPict='"999999"'

  add object oDESCRI_1_11 as StdField with uid="DELKQVVYNY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione voce",;
    HelpContextID = 58821834,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=90, Top=444, InputMask=replicate('X',40)


  add object oObj_1_13 as cp_runprogram with uid="GRDDXFFPOJ",left=470, top=73, width=276,height=23,;
    caption='GSBI_BGS(Click)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGS("Click")',;
    cEvent = "w_treeview NodeRightClick",;
    nPag=1;
    , HelpContextID = 133396281


  add object oTIPORD_1_15 as StdCombo with uid="NRYSXRDTSO",rtseq=6,rtrep=.f.,left=348,top=395,width=104,height=21;
    , ToolTipText = "Tipo ordinamento";
    , HelpContextID = 141932490;
    , cFormVar="w_TIPORD",RowSource=""+"Seq.elabor.,"+"Riga stampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPORD_1_15.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_15.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='S',1,;
      iif(this.Parent.oContained.w_TIPORD=='R',2,;
      0))
  endfunc


  add object oObj_1_16 as cp_runprogram with uid="BQLNDFBFSV",left=470, top=95, width=276,height=23,;
    caption='GSBI_BGS(Ord)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGS("Ord")',;
    cEvent = "w_TIPORD Changed",;
    nPag=1;
    , HelpContextID = 156164921


  add object oObj_1_17 as cp_runprogram with uid="RZKEDYIZPF",left=470, top=117, width=276,height=23,;
    caption='GSBI_BGS(DClick)',;
   bGlobalFont=.t.,;
    prg='GSBI_BGS("DClick")',;
    cEvent = "w_treeview NodeClick",;
    nPag=1;
    , HelpContextID = 5498978


  add object oBtn_1_18 as StdButton with uid="WZFWVQXIDZ",left=404, top=34, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Espande la struttura";
    , HelpContextID = 111251130;
    , Caption='\<Espandi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="EFZTAPUEYA",left=404, top=422, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 24796858;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="OLKINCLNTO",left=404, top=85, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la struttura";
    , HelpContextID = 231863846;
    , Caption='\<Chiudi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="WKAVXKDPRI",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=61, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="KITGGWYIXC",Visible=.t., Left=18, Top=417,;
    Alignment=1, Width=70, Height=15,;
    Caption="Num. riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FRUUCPKTPN",Visible=.t., Left=168, Top=417,;
    Alignment=1, Width=70, Height=15,;
    Caption="Num. seq.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZOEFRHDCCT",Visible=.t., Left=6, Top=444,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FSBYUNVJZZ",Visible=.t., Left=259, Top=395,;
    Alignment=1, Width=85, Height=18,;
    Caption="Ordinata per:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsbi_kgs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
