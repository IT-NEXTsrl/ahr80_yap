*!*	 runtime_filters=0 : Block routine if user has run-time filters
*!*	 runtime_filters=1 : Apply run-time filters
*!*	 runtime_filters=2 : Don't apply run-time filters

*!*	 Il valore da impostare deve essere 1 su tutti batch 
*!*	 ad esclusione di quelli contenuti un questa lista


DIMENSION arrproc(56)
arrproc[1]= 'GSAL_BAP.BTCDEF'  && Aggiornamento progressivi
arrproc[2]= 'GSAG_BIK.BTCDEF'  && Controllo impianto
arrproc[3]= 'GSCG_BLG.BTCDEF'  && Elabora stampa libro giornale
arrproc[4]= 'GSCG_BRS.BTCDEF'  && Ricostruzione saldi contabili
arrproc[5]= 'GSCG_BCK.BTCDEF'  && Primanota controlli finali
arrproc[6]= 'GSAR_BCC.BTCDEF'  && Check cancellazione commesse (AHE)
arrproc[7]= 'GSAR_BEA.BTCDEF'  && Evasione componenti da documenti (AHR)
arrproc[8]= 'GSAR_BFA.BTCDEF'  && Calcoli fattura
arrproc[9]= 'GSVE_BAF.BTCDEF'  && Aggiorna flag saldi in documenti (AHR)
arrproc[10]='GSVE_BCA.BTCDEF'  && Ricerca articoli alternativi (AHR)
arrproc[11]='GSVE_BCD.BTCDEF'  && Calcola prezzo listino da doc.
arrproc[12]='GSVE_BCK.BTCDEF'  && Controlli documento
arrproc[13]='GSVE_BCO.BTCDEF'  && Controlla dati spedizione (AHR)
arrproc[14]='GSVE_BCP.BTCDEF'  && Calcola percentuale provvigione da tabelle provv.
arrproc[15]='GSVE_BER.BTCDEF'  && Elabora rischio cliente
arrproc[16]='GSVE_BI1.BTCDEF'  && Eventi da import documenti
arrproc[17]='GSVE_BI2.BTCDEF'  && Import da documenti collegati
arrproc[18]='GSVE_BI3.BTCDEF'  && Import da documenti collegati
arrproc[19]='GSVE_BIA.BTCDEF'  && Carica righe da kit/varianti
arrproc[20]='GSVE_BIP.BTCDEF'  && Controlli quantita/prezzo
arrproc[21]='GSVE_BM2.BTCDEF'  && Documenti, cambia dati testata
arrproc[22]='GSVE_BM3.BTCDEF'  && Documenti, cambia causale documento (AHR)
arrproc[23]='GSVE_BM4.BTCDEF'  && Rival.uca e upv (AHE)
arrproc[24]='GSVE_BMC.BTCDEF'  && Aggiorna modifica cambio (ahr) / GESTIONE GSVE_KMC (ahe) ????
arrproc[25]='GSVE_BMK.BTCDEF'  && Check multi ut. import (AHR) / Controlli finali doc. (transazione) (AHE)
arrproc[26]='GSVE_BMU.BTCDEF'  && Check multi ut. import
arrproc[27]='GSVE_BNR.BTCDEF'  && Documenti nuovo record
arrproc[28]='GSPR_BNV.BTCDEF'  && Stampa numeri vuoti pratiche (AHR)
arrproc[29]='GSMA_BRS.BTCDEF'  && RICOSTRUZIONE SALDI DI MAGAZZINO
arrproc[30]='UNIVOC.BTCDEF'    && Controllo univocit� documenti (AHR)
arrproc[31]='GSDB_BGS.BTCDEF' && Pianificazione materiali a scorta
arrproc[32]='GSDB_BRI.BTCDEF' && Generazione MPS
arrproc[33]='GSDB_BGF.BTCDEF' && Generazione MPS
arrproc[34]='GSDB_BRP.BTCDEF' && Verifica congruit� giacenze pegging di commessa
arrproc[35]='GSDB_BMS.BTCDEF' && Pianificazione a scorta Intervallo costante
arrproc[36]='GSDB_BTB.BTCDEF' && Riassegnazione Time-Buckets
arrproc[37]='GSDB_BCB.BTCDEF' && Costruisce i Time-Buckets
arrproc[38]='GSCO_BGL.BTCDEF' && Pianificazione conto lavoro
arrproc[39]='GSDB_BDO.BTCDEF' && Crea dettaglio ODL
arrproc[40]='GSCI_BCL.BTCDEF' && Controllo cicli di lavorazione
arrproc[41]='GSCI_BGC.BTCDEF' && Genera codici conto lavoro WIP  di fase
arrproc[42]='GSDB_BAD.BTCDEF' && Aggiornamento differito
arrproc[43]='GSDB1BAD.BTCDEF' && Aggiornamento differito
arrproc[44]='GSDB_BAM.BTCDEF' && Aggiornamento senza ODL
arrproc[45]='GSRA_BCM.BTCDEF' && Materiali a standard
arrproc[46]='GSMR_BGP.BTCDEF' && Elaborazione MRP
arrproc[47]='GSMR_BPG.BTCDEF' && Pegging 2 livello
arrproc[48]='GSDB_BTV.BTCDEF' && Esplosione distinta base
arrproc[49]='GSDB_BEX.BTCDEF' && Esplosione distinta base lato server
arrproc[50]='GSMR_BLC.BTCDEF' && Calcolo LLC
arrproc[51]='GSMR_BEX.BTCDEF' && Messaggi di ripianificazione
arrproc[52]='GSVE_BPC.BTCDEF' && Pianificazione materiali a scorta intervallo costante
arrproc[53]='GSDB_BAC.BTCDEF' && Validazione controlli CQAC
arrproc[54]='GSVE_BFB.BTCDEF' && Elaborazione fabbisogno
arrproc[55]='GSVE_BGF.BTCDEF' && Elaborazione PDA
arrproc[56]='GSVE_BCT.BTCDEF' && Controllo cancellazione categoria contabile

DO appfilterproc && IN applyfiltervalue

PROCEDURE appfilterproc()
    LOCAL filenumber,i,j,namearray,dirnumber
    * 
    PRGR=0
    oldPRGR=-1
    filenumber = ADIR(alistfile , "*.btcdef")
    mydir=SYS(2003)
    mypos=UPPER(SUBSTR(mydir,RAT("\",mydir)+1))
    for i=1 to filenumber
	    namefile = alistfile[i,1]
	    gcString = filetostr(namefile)
	    possearch=ATC("runtime_filters=", gcString)
	    filtvalue=SUBSTRC(gcString, possearch+16,1)
		PRGR=INT(i*100/filenumber)
		IF PRGR<>oldPRGR
			WAIT WINDOW mydir+" -> "+STR(PRGR,3,0)+"%" nowait	
			oldPRGR=PRGR
		ENDIF
	    if (possearch=0 OR (mypos<>"PCON" AND filtvalue="2") OR (mypos="PCON" AND filtvalue="1")) AND AScAN(arrproc, namefile, -1, -1, 1, 1)=0
	      w_Handle = fopen(namefile, 2)
	      IF mypos="PCON" 
		  	IF filtvalue="1"
		  	* procedura di conversione, forzo don't apply run-time filters
		  	gcString = leftc(gcString, possearch+15) + "2" + SUBSTRC(gcString, possearch+17)
		  	ENDIF
	      ELSE
		    IF possearch=0 
			    * nessun valore trovato
				possearch=ATC("tpl_name=", gcString)
				gcString = leftc(gcString, possearch-1)+"runtime_filters=1"+CHR(13)+CHR(10)+rightc(gcString, len(gcString)-possearch+1)
			ELSE
			  	* Trovato don't apply run-time filters
			  	gcString = leftc(gcString, possearch+15) + "1" + SUBSTRC(gcString, possearch+17)
			ENDIF
		  ENDIF
	      =Fwrite(w_Handle, gcstring)
	      FCLOSE(w_Handle)
	    ENDIF
    endfor
    namearray=SYS(2015)
    dirnumber = ADIR(&namearray , "", "D")
    for j=1 to dirnumber
        IF LEFT(&namearray[j,1],1)<>'.' AND LOWER(&namearray[j,1])<>"blackbox"
		        CD &namearray[j,1]
		        appfilterproc()
		        CD ..\
		endif
    endfor
endproc

