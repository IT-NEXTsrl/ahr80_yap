* --- Template Stampe
FoxPro report (Ver 2.0)
� FoxPro report (Ver 2.0)
\note *** il controllo per MAKE\
\declare string f_date outdir\
\set outdir = par_val('-o')\
\if par_opt('-make')\
\  set f_date=file_date(outdir+file_name+'.prg')\
\  if f_date>file_date(file_name+'.def') and f_date>file_date(tpl_name+'.tpl')\
  File \file_name\: Up to date.
\    stop\
\  endif\
\endif\
\note ********************************************\
\note *                                          *\
\note *  Template standard per stampe FoxPro!    *\
\note *                                          *\
\note *  Autore    : Nestor Jose' Juncos         *\
\note *  Versione  : 2.0.0 int 0                 *\
\note *  Data crea.: 04/06/93                    *\
\note *  Data rev. : 28/10/96                    *\
\note ********************************************\
\declare integer i j h ph k mx my src_key\
\declare string i_v i_f i_kv i_kf item_n _style _font\
\declare boolean no_screen\
\set src_key=1\
\declare string n_print n_say brk_cond\
\set n_print = copy(file_name,1,4)+'4'+copy(file_name,6,)\
\set n_say = copy(file_name,1,4)+'5'+copy(file_name,6,)\
\declare integer access_lev_load access_lev_edit access_lev_del\
\set access_lev_load=-1\\set access_lev_edit=-1\\set access_lev_del=-1\
\declare string access_grp_edit access_grp_load access_grp_del)\
\set access_grp_edit=''\\set access_grp_load=''\\set access_grp_del=''\
\declare integer form_height start_row fc max_brk_cond frm_brk_cond\
\declare integer pform_height pstart_row\
\declare integer first_row pfirst_row\
\declare integer last_row plast_row\
\declare boolean form_detail grpformfeed log_sys\
\declare boolean fixed_printed swappfph swaptmp redefpfph\
\declare integer phf phh phpg\
\set redefpfph=FALSE\
\set phf=0\\set phh=0\\set phpg=0\
\declare integer a_memo a_memo_y a_memo_py\
\set log_sys = FALSE\
\mess  Report procedure    = [file_name]: [comment]\
\output outdir+file_name+'.prg'\
* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: \left_just(file_name,64)\*
*              \left_just(comment,64)\*
*                                                                              *
*      Author: \left_just(author,64)\*
*      Client: \left_just(client,64)\*
*                                                                              *
*    Language: \left_just(language,64)\*
*          OS: \left_just(os,64)\*
*                                                                              *
*     Version: \left_just(vers,64)\*
* Date creat.: \left_just(date_creat,64)\*
* Last revis.: \left_just(date_rev,64)\*
*                                                                              *
* \left_just(note,77)\*
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
\note *** Dichiarazione variabili di work ***\
\set i=0\
\repeat all for obj='variabile' and copy(name,1,2)<>'i_'\
\  set i=i+1\
\  if i=1\
private \name\\+
\  else\\if i<>5\
,\name\\+
\  else\
,\name\\set i=0\
\  endif\\endif\
\next\

\repeat all for obj='variabile' and copy(name,1,2)<>'i_'\
\name\ = \.blank_value\
\next\

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
\note ****** Calcolo ingombri ******\
\set mx=0\\set my=0\
\repeat pag\
\  set h=0\\set ph=0\
\  repeat item for obj<>'gruppo' and obj<>'e_gruppo' and obj<>'cond'\
\    if obj='stringa'\
\      if x+length(txt_help)>mx\\set mx=x+length(txt_help)\\endif\
\    else\
\      if x+len>mx\\set mx=x+len\\endif\
\    endif\
\    if y>my\\set my=y\\endif\
\    if h<y\\set h=y\\endif\
\    if ph<pyy\\set ph=pyy\\endif\
\  next\
\  if pag>=13\
  i_formh\pag\ = \h+1\
\  endif\
\next\
\set no_screen=FALSE\
\note if mx>79\
\note  set no_screen=TRUE\
\note endif\
\if my>23\\set no_screen=TRUE\\endif\
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do \n_print\ with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do \n_print\ with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


\mess  Print procedure     = [n_print]\
procedure \n_print\
* === Procedure \n_print\
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break\+
\set max_brk_cond = 0\
\repeat pag\
\  set frm_brk_cond = 0\
\  repeat item for obj='gruppo'\
\    set frm_brk_cond = frm_brk_cond+1\
\  next\
\  if frm_brk_cond>max_brk_cond\
\    set max_brk_cond = frm_brk_cond\
\  endif\
\next\
\if max_brk_cond>0\
\  for frm_brk_cond = 1 to max_brk_cond\
, i_cond\frm_brk_cond\\+
\  next\
\endif\


do case
\repeat pag\
\  set max_brk_cond = 0\
\  repeat item\
\    if obj='gruppo'\
\      set max_brk_cond = max_brk_cond+1\
\    endif\
\  next\
  case i_form_id=\pag\
\  if no_void(form_file)\
\note ****** Selezione del file ******\
\if no_void(form_file)\
    select __tmp__
    i_warea = '__tmp__'
\endif\
\note ****** Seek iniziale ******\
\if no_void(frm_seek)\
    seek \frm_seek\
\else\
\  if form_order<>0\
    go top
\  endif\
\endif\
\if no_void(frm_filter)\
    do while .not. eof() .and. .not. (\frm_filter\)\if no_void(frm_while)\ .and. (\frm_while\)\endif\
      skip
    enddo
\endif\
\    if no_void(frm_break)\
    i_break = \frm_break\
\    endif\
    * --- inizializza le condizioni dei break interform
\    set frm_brk_cond = 1\
\    repeat item for obj='gruppo'\
\if void(frm_calc)\
\  if no_void(frm_while)\
\    set brk_cond = '('+frm_while+')'\
\  else\
\    set brk_cond = 'eof()'\
\  endif\
\else\
\  set brk_cond = frm_calc\
\endif\
    i_cond\frm_brk_cond\ = (\brk_cond\)
\      set frm_brk_cond = frm_brk_cond+1\
\    next\
    i_frm_brk = .T.
    do while .not. eof() \if no_void(frm_while)\.and. (\frm_while\)\endif\
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
\    if pag<11\
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do \n_print\ with 11, 0
        i_frm_rpr = .T.
      endif
\    endif\
\    set form_detail = FALSE\
\    set form_height = 0\\set pform_height = 0\
\    set start_row = 0\\set pstart_row = 0\
\    set fc = 0\
\    set first_row = 0\\set pfirst_row = 0\
\    set frm_brk_cond = 0\
\    repeat item for obj='gruppo' or obj='e_gruppo'\
\      set form_height  = y-start_row\
\      set pform_height = py-pstart_row\
\      set start_row    = y+1\
\      set pstart_row   = pyy\
\      set last_row     = y\
\      set plast_row    = py\
\      if obj='gruppo'\
\        set frm_brk_cond = frm_brk_cond+1\
\note ****** gestione della testata in un form (inizio) ******\
\if void(frm_calc)\
\  if no_void(frm_while)\
\    set brk_cond = '('+frm_while+')'\
\  else\
\    set brk_cond = 'eof()'\
\  endif\
\else\
\  set brk_cond = frm_calc\
\endif\
      if i_cond\frm_brk_cond\<>(\brk_cond\) .or. i_frm_brk
        i_cond\frm_brk_cond\ = (\brk_cond\)
\if chainphf\
        do \n_print\ with 98
\  set redefpfph=TRUE\
\else\
        i_frm_brk = .T.
\endif\
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
        if \brk_cond\
\      endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
        endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
        if \brk_cond\
\    endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
        endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
\if chainphf\
        i_form_ph = \pag\.\if fc-1<10\0\endif\\fc-1\
        i_form_phh = \form_height\
        i_form_phg = \pform_height\
\  set redefpfph=FALSE\
\endif\
      endif
\note ****** gestione della testata in un form (fine) ******\
\      else\
\        if not(form_detail)\
\          set form_detail = TRUE\
\          set swappfph = chainphf\
\note ****** gestione del dettaglio di un form (inizio) ******\
      i_frm_brk = .F.
      * stampa del dettaglio
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
      if \brk_cond\
\      endif\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
      endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
      if \brk_cond\
\    endif\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
      endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
      if i_usr_brk
\  if no_void(form_file)\
        exit
\  else\
        return
\  endif\
      endif
\if sub_form<>0\
      * lancia la stampa del subform
      do \n_print\ with \sub_form\, 0
\  if no_void(form_file)\
      select __tmp__
      i_warea = '__tmp__'
\  endif\
\endif\
\if void(frm_break) and max_brk_cond=0\
\if adv_rec\
      if .not. eof()
        skip
      endif
\  if no_void(frm_filter)\
      do while .not. eof() .and. .not. (\frm_filter\)\if no_void(frm_while)\ .and. (\frm_while\)\endif\
        skip
      enddo
\  endif\
\endif\
\else\
      * --- passa al record successivo
      i_prevrec = recno()
\if adv_rec\
      if .not. eof()
        skip
      endif
\  if no_void(frm_filter)\
      do while .not. eof() .and. .not. (\frm_filter\)\if no_void(frm_while)\ .and. (\frm_while\)\endif\
        skip
      enddo
\  endif\
\endif\
\  if no_void(frm_break)\
      if i_break<>\frm_break\
        * --- lancia la stampa del subform di rottura
        i_currec = iif(eof(), -1, recno())
        go i_prevrec
        do \n_print\ with \brk_form\, 0
        if i_usr_brk
\    if no_void(form_file)\
          exit
\    else\
          return
\    endif\
        endif
\    if no_void(form_file)\
        select __tmp__
        i_warea = '__tmp__'
\    endif\
        do cplu_go with i_currec
        i_break = \frm_break\
      endif
\  endif\
\endif\
\note ****** gestione del dettaglio di un form (fine) ******\
\        else\
\note ****** gestione del piedino in un form (inizio) ******\
\if frm_brk_cond=max_brk_cond\
      i_currec = iif(eof(), -1, recno())
\endif\
\set swaptmp = chainphf\
      if eof() \if no_void(frm_while)\.or..not.(\frm_while\)\endif\ .or. i_cond\frm_brk_cond\<>(\brk_cond\)\+
\set i = frm_brk_cond\
\repeat item for obj='e_gruppo' and (y>=start_row-1 or py>pstart_row+1)\
\if void(frm_calc)\
\  if no_void(frm_while)\
\    set brk_cond = '('+frm_while+')'\
\  else\
\    set brk_cond = 'eof()'\
\  endif\
\else\
\  set brk_cond = frm_calc\
\endif\
\  set i = i-1\ .or. i_cond\i\<>(\brk_cond\)\+
\next\
      
\if swappfph\
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
\  set redefpfph=TRUE\
\else\
        go i_prevrec
\endif\
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
        if \brk_cond\
\      endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
        endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
        if \brk_cond\
\    endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
        endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
\if swappfph\
\  set phpg=pag\
\  set phf=fc-1\
\  set phh=form_height\
\  set redefpfph=FALSE\
\else\
        do cplu_go with i_currec
\endif\
      endif
\set swappfph = swaptmp\
\note ****** gestione del piedino in un form (fine) ******\
\          set frm_brk_cond = frm_brk_cond-1\
\        endif\
\      endif\
\if void(frm_calc)\
\  if no_void(frm_while)\
\    set brk_cond = '('+frm_while+')'\
\  else\
\    set brk_cond = 'eof()'\
\  endif\
\else\
\  set brk_cond = frm_calc\
\endif\
\      set first_row = y+1\\set pfirst_row = pyy+1\
\    next\
\    set h=0\\set ph=0\
\    repeat item\
\      if h<y\\set h = y\\endif\
\      if ph<pyy\\set ph = pyy\\endif\
\    next\
\    set form_height  = h-start_row+1\
\    set pform_height = ph-pstart_row+1\
\    set start_row    = start_row+1\
\    set pstart_row   = pstart_row+1\
\    set last_row     = h+1\
\    set plast_row    = ph+1\
\    if not(form_detail)\
\note ****** gestione del dettaglio di un form (inizio) ******\
      i_frm_brk = .F.
      * stampa del dettaglio
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
      if \brk_cond\
\      endif\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
      endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
      if \brk_cond\
\    endif\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
      endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
      if i_usr_brk
\  if no_void(form_file)\
        exit
\  else\
        return
\  endif\
      endif
\if sub_form<>0\
      * lancia la stampa del subform
      do \n_print\ with \sub_form\, 0
\  if no_void(form_file)\
      select __tmp__
      i_warea = '__tmp__'
\  endif\
\endif\
\if void(frm_break) and max_brk_cond=0\
\if adv_rec\
      if .not. eof()
        skip
      endif
\  if no_void(frm_filter)\
      do while .not. eof() .and. .not. (\frm_filter\)\if no_void(frm_while)\ .and. (\frm_while\)\endif\
        skip
      enddo
\  endif\
\endif\
\else\
      * --- passa al record successivo
      i_prevrec = recno()
\if adv_rec\
      if .not. eof()
        skip
      endif
\  if no_void(frm_filter)\
      do while .not. eof() .and. .not. (\frm_filter\)\if no_void(frm_while)\ .and. (\frm_while\)\endif\
        skip
      enddo
\  endif\
\endif\
\  if no_void(frm_break)\
      if i_break<>\frm_break\
        * --- lancia la stampa del subform di rottura
        i_currec = iif(eof(), -1, recno())
        go i_prevrec
        do \n_print\ with \brk_form\, 0
        if i_usr_brk
\    if no_void(form_file)\
          exit
\    else\
          return
\    endif\
        endif
\    if no_void(form_file)\
        select __tmp__
        i_warea = '__tmp__'
\    endif\
        do cplu_go with i_currec
        i_break = \frm_break\
      endif
\  endif\
\endif\
\note ****** gestione del dettaglio di un form (fine) ******\
\    else\
\note ****** gestione del piedino in un form (inizio) ******\
\if frm_brk_cond=max_brk_cond\
      i_currec = iif(eof(), -1, recno())
\endif\
\set swaptmp = chainphf\
      if eof() \if no_void(frm_while)\.or..not.(\frm_while\)\endif\ .or. i_cond\frm_brk_cond\<>(\brk_cond\)\+
\set i = frm_brk_cond\
\repeat item for obj='e_gruppo' and (y>=start_row-1 or py>pstart_row+1)\
\if void(frm_calc)\
\  if no_void(frm_while)\
\    set brk_cond = '('+frm_while+')'\
\  else\
\    set brk_cond = 'eof()'\
\  endif\
\else\
\  set brk_cond = frm_calc\
\endif\
\  set i = i-1\ .or. i_cond\i\<>(\brk_cond\)\+
\next\
      
\if swappfph\
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
\  set redefpfph=TRUE\
\else\
        go i_prevrec
\endif\
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
        if \brk_cond\
\      endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
        endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
        do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
        if \brk_cond\
\    endif\
          do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
        endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
\if swappfph\
\  set phpg=pag\
\  set phf=fc-1\
\  set phh=form_height\
\  set redefpfph=FALSE\
\else\
        do cplu_go with i_currec
\endif\
      endif
\set swappfph = swaptmp\
\note ****** gestione del piedino in un form (fine) ******\
\    endif\
\    if not(adv_rec)\
      exit
\    endif\
    enddo
\  else\
\    set h = 0\\set ph = 0\
\    repeat item\
\      if h<y\\set h = y\\endif\
\      if ph<pyy\\set ph = pyy\\endif\
\    next\
\    set form_height  = h+1\
\    set pform_height = ph+1\
\    set last_row     = h+1\
\    set plast_row    = ph+1\
\    set first_row = 0\\set pfirst_row = 0\
\    set fc = 0\
\    if pag = 14\
    if i_row+\form_height\>w_t_stnrig
      * --- stampa il piede di pagina
      do \n_print\ with 99, \form_height\
    endif
\    endif\
\set brk_cond = ''\
\set h = first_row\\set ph = pfirst_row\
\set fixed_printed = FALSE\
\repeat item for obj='cond' and (y>=first_row or pyy>=pfirst_row) and (y<=last_row or pyy<=plast_row)\
\  set form_height = y-h\\set pform_height = pyy-ph\
\  set h = y+1\\set ph = pyy+1\
\  if (form_height=0 or pform_height=0) and not(fixed_printed)\
\    note **** non � prevista una parte fissa iniziale ****\
\    set fixed_printed = TRUE\
\  endif\
\  if form_height>0 or pform_height>0\
\    if not(fixed_printed)\
    do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      set fixed_printed = TRUE\
\    else\
\      if no_void(brk_cond)\
    if \brk_cond\
\      endif\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\      if no_void(brk_cond)\
    endif
\      endif\
\    endif\
\  endif\
\  set fc = fc+1\
\  set brk_cond = frm_calc\
\next\
\set form_height = last_row-h\\set pform_height = plast_row-ph\
\if not(redefpfph)\
\  if not(fixed_printed)\
    do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\  else\
\    if no_void(brk_cond)\
    if \brk_cond\
\    endif\
      do \n_say\ with \pag\.\if fc<10\0\endif\\fc\, \form_height\
\    if no_void(brk_cond)\
    endif
\    endif\
\  endif\
\endif\
\set fc = fc+1\
\  endif\
\note ******* Controllo del salto pagina finale *******\
\  if new_page\
    * --- imposta i_row in modo da provocare un salto pagina
    i_row = w_t_stnrig
\  endif\
\note ****** Lancio dell'eventuale form successivo ******\
\  if end_form<>0\
    * --- lancio del form successivo
    if .not. i_usr_brk
      do \n_print\ with \end_form\, 0
    endif
\  endif\
\next\
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do \n_print\ with 13, 0
        else
          do \n_say\ with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do \n_print\ with 12, 0
        else
          do \n_say\ with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = \phpg\.\if phf<10\0\endif\\phf\
    i_saveh13 = i_formh13
    i_formh13 = \phh\
endcase
return

\mess  Output procedure    = [n_say]\
procedure \n_say\
* === Procedure \n_say\
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do \n_print\ with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
\set form_height = 0\\set pform_height = 0\
\repeat pag\
  * --- \pag\� form
  case i_form_id=\pag\.0
    do frm\pag\_0
\  set fc = 0\
\  set grpformfeed = FALSE\
\  set start_row = 0\\set pstart_row = 0\
\  repeat item\
\    if obj='gruppo' or obj='e_gruppo' or obj='cond'\
\      set a_memo_y=y\\set a_memo_py=py\
\      set start_row = y+1\\set pstart_row = pyy\
\      set fc = fc+1\
\      if grpformfeed\
\        set grpformfeed = FALSE\
\      endif\
  case i_form_id=\pag\.\if fc<10\0\endif\\fc\
    do frm\pag\_\if fc<10\0\endif\\fc\
\      if obj='e_gruppo' and clear\
\        set grpformfeed = TRUE\
\      endif\
\    endif\
\  next\
\  set a_memo_y=30000\\set a_memo_py=30000\
\next\
endcase
i_row = i_row+i_form_h
return

\set form_height = 0\\set pform_height = 0\
\repeat pag\

* --- \pag\� form
procedure frm\pag\_0
\  if pag=13\
  i_row = w_t_stnrig-i_formh13
\  endif\
\  if pag=14\
  if i_row+i_formh14>w_t_stnrig
    * stampa il piede di pagina
    do \n_print\ with 13, 0
  endif
\  endif\
\  set fc = 0\
\  set grpformfeed = FALSE\
\  set start_row = 0\\set pstart_row = 0\
\  repeat item\
\    if obj='gruppo' or obj='e_gruppo' or obj='cond'\
\     set a_memo_y=y\\set a_memo_py=py\
\set a_memo=0\
\repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
\  set a_memo=1\
\endif\
\next\
\if a_memo<>0\
  i_ml = 0
\  repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\  if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
  w_s = \xx-x+1\
  set memowidth to w_s
  i_ml = max(i_ml,memlines(\name\))
\  endif\
\  next\
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do \n_print\ with 99,1
    if i_usr_brk
      exit
    endif
\  repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\  if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
    w_s = \xx-x+1\
    set memowidth to w_s
\      if no_void(attrib_before)\
      if len(trim('\attrib_before\'))<>0
        do F_Say with -1,-1,-1,-1,\attrib_before\,""
      endif
\      endif\
\    set _style=''\
\    if no_void(font)\
\      if font_bold\\set _style=_style+'B'\\endif\
\      if font_italic\\set _style=_style+'I'\\endif\
\      if font_under\\set _style=_style+'U'\\endif\
\      if font_strike\\set _style=_style+'-'\\endif\
      i_fn = ''
\    else\
\      if no_void(gfont)\
\        if gfont_bold\\set _style=_style+'B'\\endif\
\        if gfont_italic\\set _style=_style+'I'\\endif\
\        if gfont_under\\set _style=_style+'U'\\endif\
\        if gfont_strike\\set _style=_style+'-'\\endif\
      i_fn = ''
\      else\
      i_fn = ""
\      endif\
\    endif\
    Sm = mline(\name\,i_i)+space(w_s-len(mline(\name\,i_i)))
    do F_Say with i_row,i_row,\x\,at_x(\px\),alltrim(Sm),i_fn
\      if no_void(attrib_after)\
      if len(trim('\attrib_after\'))<>0
        do F_Say with -1,-1,-1,-1,\attrib_after\,""
      endif
\      endif\
\  endif\
\  next\
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
\endif\
\      set start_row = y+1\\set pstart_row = pyy\
\      set fc = fc+1\
\    if grpformfeed\
  i_row = w_t_stnrig-i_formh13
\      set grpformfeed = FALSE\
\    endif\
return

* --- frm\pag\_\if fc<10\0\endif\\fc\
procedure frm\pag\_\if fc<10\0\endif\\fc\
\      if obj='e_gruppo' and bottom\
  i_row = w_t_stnrig-i_form_h-i_formh13
\      endif\
\      if obj='e_gruppo' and clear\
\        set grpformfeed = TRUE\
\      endif\
\    else\
\      if no_void(frm_calc)\
\        if copy(frm_calc,1,3)='do '\
  \frm_calc\
\        else\
  \name\ = \frm_calc\
\        endif\
\      endif\
\      if no_void(before_print)\
  \before_print\
\      endif\
\      if cond\
  if \frm_cond\
\      endif\
\      if no_void(attrib_before)\
  if len(trim('\attrib_before\'))<>0
    do F_Say with -1,-1,-1,-1,\attrib_before\,""
  endif
\      endif\
\      set _style=''\
\      if no_void(font)\
\        if font_bold\\set _style=_style+'B'\\endif\
\        if font_italic\\set _style=_style+'I'\\endif\
\        if font_under\\set _style=_style+'U'\\endif\
\        if font_strike\\set _style=_style+'-'\\endif\
  i_fn = ''
\      else\
\        if no_void(gfont)\
\          if gfont_bold\\set _style=_style+'B'\\endif\
\          if gfont_italic\\set _style=_style+'I'\\endif\
\          if gfont_under\\set _style=_style+'U'\\endif\
\          if gfont_strike\\set _style=_style+'-'\\endif\
  i_fn = ''
\        else\
  i_fn = ""
\        endif\
\      endif\
\   if obj='stringa'\
  do F_Say with i_row+\y-start_row\,i_row+at_y(\py\-\pstart_row\),\x\,at_x(\px\),"\txt_help\",i_fn
\   else\
\     if obj_type<>'M'\
  do F_Say with i_row+\y-start_row\,i_row+at_y(\py\-\pstart_row\),\x\,at_x(\px\),transform(\name\,\+
\if no_void(say_pict)\\say_pict\\else\""\endif\),i_fn
\     else\
  w_s = \xx-x+1\
  set memowidth to w_s
  Sm = mline(\name\,1)+space(w_s-len(mline(\name\,1)))
  do F_Say with i_row+\y-start_row\,i_row+at_y(\py\-\pstart_row\),\x\,at_x(\px\),alltrim(Sm),i_fn
\     endif\
\   endif\
\      if no_void(attrib_after)\
  if len(trim('\attrib_after\'))<>0
    do F_Say with -1,-1,-1,-1,\attrib_after\,""
  endif
\      endif\
\      if no_void(after_print)\
   \after_print\
\      endif\
\      if no_void(total)\
   \total\ = \total\+\name\
\      endif\
\      if cond\
  endif
\      endif\
\      if clear\
   \name\ = \.blank_value\
\      endif\
\      if check='file'\
  w_\name\ = \name\
\if no_void(rd_var2)\
\  if arch=form_file\
  w_s = iif(eof(),-1,recno())
\  endif\
  select \arch\
  seek \if no_void(rd_key)\\rd_key\\else\w_\name\\endif\
\  for k=2 to 20\
\    if no_void(rd_var[k])\
    \rd_var[k]\ = \rd_field[k]\
\    endif\
\  next\
  select &i_warea
\  if arch=form_file\
  do cplu_go with w_s
\  endif\
\endif\
\      endif\
\      if check='table'\
  w_\name\ = \name\
\    set i=0\
\    for k=2 to 20\
\      if no_void(rd_var[k])\\set i=i+1\
  i_rdvars[\k\,1] = "\rd_var[k]\"
  i_rdvars[\k\,2] = "\rd_field[k]\"
\      endif\
\    next\
  i_rdvars[1,1] = \i\
\if obj_type='N'\
  do cplu_tab with "\arch\",str(w_\name\,\len\,0),w_s
\else\
  do cplu_tab with "\arch\",w_\name\,w_s
\endif\
\      endif\
\    endif\
\  next\
\  set a_memo_y=30000\\set a_memo_py=30000\
\set a_memo=0\
\repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
\  set a_memo=1\
\endif\
\next\
\if a_memo<>0\
  i_ml = 0
\  repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\  if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
  w_s = \xx-x+1\
  set memowidth to w_s
  i_ml = max(i_ml,memlines(\name\))
\  endif\
\  next\
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do \n_print\ with 99,1
    if i_usr_brk
      exit
    endif
\  repeat item for (obj='campo' or obj='variabile') and (obj_type='M')\
\  if (start_row<=y) and (y<=a_memo_y) and (pstart_row<=py) and (py<=a_memo_py)\
    w_s = \xx-x+1\
    set memowidth to w_s
\      if no_void(attrib_before)\
      if len(trim('\attrib_before\'))<>0
        do F_Say with -1,-1,-1,-1,\attrib_before\,""
      endif
\      endif\
\    set _style=''\
\    if no_void(font)\
\      if font_bold\\set _style=_style+'B'\\endif\
\      if font_italic\\set _style=_style+'I'\\endif\
\      if font_under\\set _style=_style+'U'\\endif\
\      if font_strike\\set _style=_style+'-'\\endif\
      i_fn = ''
\    else\
\      if no_void(gfont)\
\        if gfont_bold\\set _style=_style+'B'\\endif\
\        if gfont_italic\\set _style=_style+'I'\\endif\
\        if gfont_under\\set _style=_style+'U'\\endif\
\        if gfont_strike\\set _style=_style+'-'\\endif\
      i_fn = ''
\      else\
      i_fn = ""
\      endif\
\    endif\
    Sm = mline(\name\,i_i)+space(w_s-len(mline(\name\,i_i)))
    do F_Say with i_row,i_row,\x\,at_x(\px\),alltrim(Sm),i_fn
\      if no_void(attrib_after)\
      if len(trim('\attrib_after\'))<>0
        do F_Say with -1,-1,-1,-1,\attrib_after\,""
      endif
\      endif\
\  endif\
\  next\
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
\endif\
\  if grpformfeed\
  i_row = w_t_stnrig-i_formh13
\  endif\
return
\next\

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

\man 'Functions & Procedures'\	
