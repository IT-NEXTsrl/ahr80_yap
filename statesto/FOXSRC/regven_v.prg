* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: REGVEN_V                                                        *
*              Registro vendite Vert.                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 10/4/01                                                         *
* Last revis.: 11/9/14                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_TIPREC,w_NUMALFDOC,w_NOTE,w_TIPO,w_ALIQUO
private w_IMPTOT,w_COMPET,w_RIEIMP,w_RIEIVA,w_RIEIMP1
private w_RIEIMP2,w_RIEIVA1,w_RIEIVA2,w_ARIMP1,w_ARIMP2
private w_ARIVA1,w_ARIVA2,w_SPAZIO,w_TOTDOC,w_APIMP
private w_APIVA,w_ARIMP,w_ARIVA,w_EPIMP,w_EPIVA
private w_ERIMP,w_ERIVA,w_APIMPT,w_APIVAT,w_ARIMPT
private w_ARIVAT,w_SETTA,w_SETTA,PREFIS,NUMPAG
private p_RAGAZI,p_PARTO,w_DATIAZ,p_CODO,w_Set
private w_INTESTA,P_DATINI,P_DATFIN,P_VALSIM,P_DESVAL
private w_SETTA,w_SETTA,PREFIS,NUMPAG,p_RAGAZI
private p_PARTO,w_DATIAZ,p_CODO,w_INTESTA,P_DATINI
private P_DATFIN,P_VALSIM,P_DESVAL
w_TIPREC = space(8)
w_NUMALFDOC = space(26)
w_NOTE = space(5)
w_TIPO = space(2)
w_ALIQUO = space(5)
w_IMPTOT = 0
w_COMPET = space(6)
w_RIEIMP = 0
w_RIEIVA = 0
w_RIEIMP1 = 0
w_RIEIMP2 = 0
w_RIEIVA1 = 0
w_RIEIVA2 = 0
w_ARIMP1 = 0
w_ARIMP2 = 0
w_ARIVA1 = 0
w_ARIVA2 = 0
w_SPAZIO = space(10)
w_TOTDOC = 0
w_APIMP = 0
w_APIVA = 0
w_ARIMP = 0
w_ARIVA = 0
w_EPIMP = 0
w_EPIVA = 0
w_ERIMP = 0
w_ERIVA = 0
w_APIMPT = 0
w_APIVAT = 0
w_ARIMPT = 0
w_ARIVAT = 0
w_SETTA = space(1)
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_Set = space(10)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)
w_SETTA = space(1)
w_SETTA = space(1)
PREFIS = space(20)
NUMPAG = 0
p_RAGAZI = space(99)
p_PARTO = space(16)
w_DATIAZ = space(99)
p_CODO = space(16)
w_INTESTA = space(35)
P_DATINI = ctod("  /  /  ")
P_DATFIN = ctod("  /  /  ")
P_VALSIM = space(3)
P_DESVAL = space(35)

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do REGV4N_V with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do REGV4N_V with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure REGV4N_V
* === Procedure REGV4N_V
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (TIPREC)
    i_cond2 = (alfdoc+str(numdoc))
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do REGV4N_V with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(TIPREC) .or. i_frm_brk
        i_cond1 = (TIPREC)
        i_frm_brk = .T.
        do REGV5N_V with 1.00, 0
      endif
      if i_cond2<>(alfdoc+str(numdoc)) .or. i_frm_brk
        i_cond2 = (alfdoc+str(numdoc))
        i_frm_brk = .T.
        do REGV5N_V with 1.01, 0
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
        do REGV5N_V with 1.02, 0
      if TIPREC$'FI'
        do REGV5N_V with 1.03, 1
      endif
      if TIPREC='R'
        do REGV5N_V with 1.04, 1
      endif
      if TIPREC='R'
        do REGV5N_V with 1.05, 1
      endif
      if IMPPRE+IVAPRE<>0
        do REGV5N_V with 1.06, 1
      endif
      if IMPSEG+IVASEG<>0
        do REGV5N_V with 1.07, 1
      endif
      if IMPFAD+IVAFAD<>0
        do REGV5N_V with 1.08, 1
      endif
      if IMPIND+IVAIND<>0
        do REGV5N_V with 1.09, 1
      endif
      if TIPREC='R'
        do REGV5N_V with 1.10, 2
      endif
      if TIPREC$ 'FI' AND VALNAZ<>CODVAL AND TOTDOC<>0 AND POSIZ$"XU"
        do REGV5N_V with 1.11, 1
      endif
      if EndOfGroup() .and. TIPREC<>'R'
        do REGV5N_V with 1.12, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(alfdoc+str(numdoc)) .or. i_cond1<>(TIPREC)      
        go i_prevrec
          do REGV5N_V with 1.13, 0
        if TIPREC="R"
          do REGV5N_V with 1.14, 1
        endif
        if TIPREC="R"
          do REGV5N_V with 1.15, 1
        endif
        if TIPREC="R"
          do REGV5N_V with 1.16, 1
        endif
        if TIPREC="R"
          do REGV5N_V with 1.17, 1
        endif
        if TIPREC="R" .and.  i_Row<>ts_RowOk
          do REGV5N_V with 1.18, 2
        endif
        do cplu_go with i_currec
      endif
      if eof()  .or. i_cond1<>(TIPREC)      
        go i_prevrec
        do REGV5N_V with 1.19, 0
        do cplu_go with i_currec
      endif
    enddo
    * --- lancio del form successivo
    if .not. i_usr_brk
      do REGV4N_V with 2, 0
    endif
  case i_form_id=11
    do REGV5N_V with 11.00, 12
  case i_form_id=12
    do REGV5N_V with 12.00, 12
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do REGV4N_V with 13, 0
        else
          do REGV5N_V with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do REGV4N_V with 12, 0
        else
          do REGV5N_V with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 0.00
    i_saveh13 = i_formh13
    i_formh13 = 0
endcase
return

procedure REGV5N_V
* === Procedure REGV5N_V
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do REGV4N_V with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
  case i_form_id=1.09
    do frm1_09
  case i_form_id=1.10
    do frm1_10
  case i_form_id=1.11
    do frm1_11
  case i_form_id=1.12
    do frm1_12
  case i_form_id=1.13
    do frm1_13
  case i_form_id=1.14
    do frm1_14
  case i_form_id=1.15
    do frm1_15
  case i_form_id=1.16
    do frm1_16
  case i_form_id=1.17
    do frm1_17
  case i_form_id=1.18
    do frm1_18
  case i_form_id=1.19
    do frm1_19
  * --- 11� form
  case i_form_id=11.0
    do frm11_0
  * --- 12� form
  case i_form_id=12.0
    do frm12_0
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
return

* --- frm1_01
procedure frm1_01
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_TIPREC = iif(TIPREC$'FI',CP_TODATE(datdoc),' ')
  if POSIZ$'XP'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),0,at_x(5),transform(w_TIPREC,""),i_fn
  endif
  w_NUMALFDOC = iif(TIPREC$'FI',alltrim(str(numdoc,15)+alltrim(iif(not empty(nvl(alfdoc,' ')),'/', ' '))+ ALLTRIM(ALFDOC)),' ')
  if POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),9,at_x(73),transform(w_NUMALFDOC,""),i_fn
  endif
  w_NOTE = '*'+iif(NVL(FLPROV,'N')='S' AND TIPREC$ 'FI','R*','')+IIF(NOT EMPTY(FLPNUM),'N*',IIF(NOT EMPTY(FLPDAT),'D*',''))
  if (NVL(FLPROV,'N')='S' AND TIPREC$ 'FI') or (L_FLDEFI<>'S' AND (NOT EMPTY(FLPNUM) OR NOT EMPTY(FLPDAT))) AND POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),30,at_x(240),transform(w_NOTE,""),i_fn
  endif
  if POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),35,at_x(284),transform(DESCLF,Repl('X',26)),i_fn
  endif
  w_TIPO = iif(tipdoc="NO",'',tipdoc)
  if POSIZ$"XP"
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),62,at_x(503),transform(w_TIPO,""),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),65,at_x(524),transform(IMPONI,V_PV[14]),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),79,at_x(638),transform(IMPIVA,V_PV[14]),i_fn
  endif
  w_ALIQUO = ALLTR(TRAN(PERIVA, '999.9'))
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),94,at_x(754),transform(w_ALIQUO,""),i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),99,at_x(797),transform(DESIVA,Repl('X',20)),i_fn
  endif
  w_IMPTOT = TRAN(IMPONI+IMPIVA, V_PV[14])
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),121,at_x(968),transform(w_IMPTOT,""),i_fn
  endif
  w_COMPET = IIF(VAL(COMPET)>9.or.VAL(COMPET)=0, "", " ")+COMPET
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(58-57),136,at_x(1088),transform(w_COMPET,""),i_fn
return

* --- frm1_04
procedure frm1_04
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(96-95),3,at_x(26),transform(CODIVA,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(96-95),10,at_x(82),transform(PERIVA, '999.9'),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(96-95),18,at_x(150),transform(DESIVA,""),i_fn
return

* --- frm1_05
procedure frm1_05
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(134-133),8,at_x(70),"Documenti Registrati nel Periodo:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(134-133),42,at_x(336),transform(IMPSTA,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(134-133),56,at_x(453),transform(IVASTA,V_PV[14]),i_fn
return

* --- frm1_06
procedure frm1_06
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(172-171),0,at_x(6),"- Documenti Competenza del Periodo Prec.:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(172-171),42,at_x(336),transform(IMPPRE,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(172-171),56,at_x(453),transform(IVAPRE,V_PV[14]),i_fn
return

* --- frm1_07
procedure frm1_07
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(210-209),1,at_x(14),"+ Documenti Registrati nel Periodo Seg.:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(210-209),42,at_x(336),transform(IMPSEG,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(210-209),56,at_x(453),transform(IVASEG,V_PV[14]),i_fn
return

* --- frm1_08
procedure frm1_08
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(248-247),5,at_x(46),"- Fatture ad Esigibilita' Differita:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(248-247),42,at_x(336),transform(IMPFAD,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(248-247),56,at_x(453),transform(IVAFAD,V_PV[14]),i_fn
return

* --- frm1_09
procedure frm1_09
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(286-285),5,at_x(46),"+ Incassi Ad Esigibilita' Differita:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(286-285),42,at_x(336),transform(IMPIND,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(286-285),56,at_x(453),transform(IVAIND,V_PV[14]),i_fn
return

* --- frm1_10
procedure frm1_10
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),11,at_x(94),"= Doc. Competenza del Periodo:",i_fn
  w_RIEIMP = (IMPSTA+IMPSEG+IMPIND)-(IMPPRE+IMPFAD)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),42,at_x(336),transform(w_RIEIMP,V_PV[14]),i_fn
  w_RIEIVA = (IVASTA+IVASEG+IVAIND)-(IVAPRE+IVAFAD)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),56,at_x(453),transform(w_RIEIVA,V_PV[14]),i_fn
  w_RIEIMP1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),72,at_x(582),transform(w_RIEIMP1,V_PV[20]),i_fn
   endif
   w_APIMP = w_APIMP+w_RIEIMP1
  w_RIEIMP2 = IIF(NVL(PERIVA,0)=0,w_RIEIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),76,at_x(610),transform(w_RIEIMP2,V_PV[20]),i_fn
   endif
   w_EPIMP = w_EPIMP+w_RIEIMP2
  w_RIEIVA1 = IIF(NVL(PERIVA,0)=0,0,w_RIEIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),79,at_x(638),transform(w_RIEIVA1,V_PV[20]),i_fn
   endif
   w_APIVA = w_APIVA+w_RIEIVA1
  w_RIEIVA2 = IIF(NVL(PERIVA,0)=0,w_RIEIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),83,at_x(667),transform(w_RIEIVA2,V_PV[20]),i_fn
   endif
   w_EPIVA = w_EPIVA+w_RIEIVA2
  w_ARIMP1 = IIF(NVL(PERIVA,0)=0,0,PROIMP)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),87,at_x(698),transform(w_ARIMP1,V_PV[20]),i_fn
   endif
  w_ARIMP2 = IIF(NVL(PERIVA,0)=0,PROIMP,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),90,at_x(726),transform(w_ARIMP2,V_PV[20]),i_fn
   endif
  w_ARIVA1 = IIF(NVL(PERIVA,0)=0,0,PROIVA)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),94,at_x(755),transform(w_ARIVA1,V_PV[20]),i_fn
   endif
  w_ARIVA2 = IIF(NVL(PERIVA,0)=0,PROIVA,0)
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(324-323),97,at_x(782),transform(w_ARIVA2,V_PV[20]),i_fn
   endif
  w_SPAZIO = Space(10)
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(342-323),41,at_x(331),transform(w_SPAZIO,""),i_fn
return

* --- frm1_11
procedure frm1_11
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(380-379),41,at_x(332),"Totale Documento:",i_fn
  w_TOTDOC = Right(Space(18)+ALLTRIM(TRAN(TOTDOC, V_PV[40+(18*NVL(DECTOT,0))])),18)
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(380-379),59,at_x(476),transform(w_TOTDOC,""),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(380-379),78,at_x(624),transform(SIMVAL,""),i_fn
return

* --- frm1_12
procedure frm1_12
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(418-417),18,at_x(147),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(418-417),90,at_x(725),"considerarsi annullato",i_fn
return

* --- frm1_13
procedure frm1_13
return

* --- frm1_14
procedure frm1_14
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(480-475),69,at_x(555),"Imponibile Progr.IVA Esigibile Progr.",i_fn
return

* --- frm1_15
procedure frm1_15
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(518-517),26,at_x(208),"Totale a Debito:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(518-517),42,at_x(338),transform(w_APIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(518-517),56,at_x(455),transform(w_APIVA,V_PV[14]),i_fn
  w_ARIMP = L_ARIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(518-517),71,at_x(572),transform(w_ARIMP,V_PV[14]),i_fn
  w_ARIVA = L_ARIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(518-517),86,at_x(688),transform(w_ARIVA,V_PV[14]),i_fn
return

* --- frm1_16
procedure frm1_16
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(556-555),20,at_x(160),"Totale altre Aliquote:",i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(556-555),42,at_x(338),transform(w_EPIMP,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(556-555),56,at_x(455),transform(w_EPIVA,V_PV[14]),i_fn
  w_ERIMP = L_ERIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(556-555),71,at_x(572),transform(w_ERIMP,V_PV[14]),i_fn
  w_ERIVA = L_ERIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(556-555),86,at_x(688),transform(w_ERIVA,V_PV[14]),i_fn
return

* --- frm1_17
procedure frm1_17
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(594-593),23,at_x(184),"Totale Complessivo:",i_fn
  w_APIMPT = w_APIMP+w_EPIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(594-593),42,at_x(338),transform(w_APIMPT,V_PV[14]),i_fn
  w_APIVAT = w_APIVA+w_EPIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(594-593),56,at_x(455),transform(w_APIVAT,V_PV[14]),i_fn
  w_ARIMPT = L_ARIMP+L_ERIMP
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(594-593),71,at_x(572),transform(w_ARIMPT,V_PV[14]),i_fn
  w_ARIVAT = L_ARIVA+L_ERIVA
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(594-593),86,at_x(688),transform(w_ARIVAT,V_PV[14]),i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(609-593),20,at_x(162),"��������������������������������������������������������������������������������",i_fn
return

* --- frm1_18
procedure frm1_18
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(647-640),18,at_x(149),"Lo spazio sottostante di questa pagina non e' stato utilizzato ed e' da",i_fn
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(647-640),90,at_x(727),"considerarsi annullato",i_fn
return

* --- frm1_19
procedure frm1_19
  i_row = w_t_stnrig-i_formh13
return

* --- 11� form
procedure frm11_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(2),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),11,at_x(90),"// Setta carattere di stampa e compresso",i_fn
   endif
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),60,at_x(482),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),0,at_x(2),"REGISTRO IVA VENDITE",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),87,at_x(700),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(892),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(2),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(828),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(2),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(804),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  if L_FLDEFI<>'S'  AND TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(117-0),41,at_x(335),"*N*=Numero fuori sequenza *D*=Data fuori sequenza *R*=Registraz. non confermata",i_fn
  endif
  if .f.
  if len(trim('&w_t_st10'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_st10,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(141-0),0,at_x(2),transform(w_Set,""),i_fn
   endif
  w_INTESTA = CompString()
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(141-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),65,at_x(520),transform(P_DESVAL,""),i_fn
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),0,at_x(4),"Data Doc. Num.Doc.               Ditta Denom. o Rag.Soc.     Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),71,at_x(570),"Imponibile          Imposta Aliquota IVA",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),115,at_x(926),"      Importo Totale  Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(220-0),1,at_x(8)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(220-0),39,at_x(315),"Imponibile Periodo IVA Esigibile Periodo",i_fn
  endif
return

* --- 12� form
procedure frm12_0
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),0,at_x(2),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  if .f.
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),11,at_x(90),"// Setta carattere di stampa e compresso",i_fn
   endif
  w_SETTA = " "
  i_fn = ""
  do F_Say with i_row+0,i_row+at_y(0-0),60,at_x(482),transform(w_SETTA,"X"),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if L_INTLIG='S'
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ""
  do F_Say with i_row+1,i_row+at_y(19-0),0,at_x(2),"REGISTRO IVA VENDITE",i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  endif
  PREFIS = RIGHT(SPACE(20)+ALLTRIM(L_PREFIS),20)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),87,at_x(700),transform(PREFIS,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),111,at_x(892),"Pag.",i_fn
  endif
  NUMPAG = L_PRPARI+I_PAG
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+2,i_row+at_y(38-0),125,at_x(1000),transform(NUMPAG,"9999999"),i_fn
   STPAG=NUMPAG
  endif
  p_RAGAZI = g_RAGAZI
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),0,at_x(2),transform(p_RAGAZI,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),103,at_x(828),"Partita IVA:",i_fn
  endif
  p_PARTO = RIGHT(SPACE(16) + ALLTRIM(L_PIVAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+3,i_row+at_y(57-0),116,at_x(928),transform(p_PARTO,""),i_fn
  endif
  w_DATIAZ = trim(L_INDAZI)+' - '+L_CAPAZI+' - '+TRIM(L_LOCAZI)+IIF(EMPTY(L_PROAZI),'',' ( '+L_PROAZI+' )')
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),0,at_x(2),transform(w_DATIAZ,""),i_fn
  endif
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),100,at_x(804),"Codice fiscale:",i_fn
  endif
  p_CODO = RIGHT(SPACE(16) + ALLTRIM(L_COFAZI),16)
  if L_INTLIG='S'
  i_fn = ""
  do F_Say with i_row+4,i_row+at_y(76-0),116,at_x(928),transform(p_CODO,""),i_fn
  endif
  if L_FLDEFI<>'S'  AND TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+6,i_row+at_y(121-0),42,at_x(336),"*N*=Numero fuori sequenza *D*=Data fuori sequenza *R*=Registraz. non confermata",i_fn
  endif
  w_INTESTA = CompString()
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  i_fn = ""
  do F_Say with i_row+7,i_row+at_y(141-0),2,at_x(18),transform(w_INTESTA,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),1,at_x(8),"Dal:",i_fn
  P_DATINI = L_DATINI
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),5,at_x(42),transform(P_DATINI,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),17,at_x(142),"Al:",i_fn
  P_DATFIN = L_DATFIN
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),20,at_x(167),transform(P_DATFIN,""),i_fn
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),40,at_x(320),"Importi espressi in:",i_fn
  P_VALSIM = G_VALSIM
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),60,at_x(487),transform(P_VALSIM,""),i_fn
  P_DESVAL = L_DESVAL
  i_fn = ""
  do F_Say with i_row+8,i_row+at_y(168-0),65,at_x(520),transform(P_DESVAL,""),i_fn
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),0,at_x(4),"Data Doc. Num.Doc.               Ditta Denom. o Rag.Soc.     Tipo",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),71,at_x(570),"Imponibile          Imposta Aliquota IVA",i_fn
  endif
  if TIPREC$ 'FI'
  i_fn = ""
  do F_Say with i_row+10,i_row+at_y(196-0),115,at_x(926),"      Importo Totale  Comp.",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(220-0),1,at_x(8)," C.I.     %      Descrizione",i_fn
  endif
  if TIPREC='R'
  i_fn = ""
  do F_Say with i_row+11,i_row+at_y(220-0),39,at_x(315),"Imponibile Periodo IVA Esigibile Periodo",i_fn
  endif
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- REGVEN_V

FUNCTION CompString
* Composizione stringa per intestazione pagina
private w_r_Stringa
w_r_Stringa=Space(35)

if TIPREC$'F'
   w_r_Stringa='Registro IVA vendite num. '+Alltrim(str(L_NUMREG,6,0)) +'  anno: ' + alltrim(str(year(l_datini)))
else
     if TIPREC$'R' 
        w_r_Stringa='Riepilogo registro IVA ven. num. '+Alltrim(str(L_NUMREG,6,0)) +'  anno: ' + alltrim(str(year(l_datini)))
     else
         if tiprec='I'
           w_r_Stringa='Incassi ad esigibilit� diff.'
         endif
     endif
endif

return (w_r_Stringa)

FUNCTION EndOfGroup()
*Verifica fine gruppo e fine pagina per stampa
*dicitura di annullamento spazio sottostante

Private w_Ret, w_CtrlREC
w_Ret=.f.
w_CtrlREC=TIPREC
skip 
If w_CtrlREC<>TIPREC .and. i_Row<>ts_RowOk .and. (!empty(TIPREC).or.eof()) .and. empty(TOTDOC)
      w_Ret=.t.
endif
skip -1

Return (w_Ret)
* --- Fine Area Manuale 
