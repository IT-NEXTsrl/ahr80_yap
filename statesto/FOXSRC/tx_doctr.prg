* ---------------------------------------------------------------------------- *
* #%&%#Build:0000
*                                                                              *
*   Procedure: TX_DOCTR                                                        *
*              Documento Di Trasporto                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 31/5/01                                                         *
* Last revis.: 28/4/10                                                         *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
private i_formh13,i_formh14
private i_brk,i_quit,i_row,i_pag,i_oldarea,i_oldrows
private i_rdvars,i_rdkvars,i_rdkey
private w_s,i_rec,i_wait,i_modal
private i_usr_brk          && .T. se l'utente ha interrotto la stampa
private i_frm_rpr          && .T. se � stata stampata la testata del report
private Sm                 && Variabile di appoggio per formattazione memo
private i_form_ph,i_form_phh,i_form_phg,i_form_pf,i_saveh13,i_exec

* --- Variabili per configurazione stampante
private w_t_stdevi,w_t_stmsin,w_t_stnrig
private w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
private w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
private w_t_stpica,w_t_stelit
private w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
private w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
private w_stdesc
store " " to w_t_stdevi
store " " to w_t_strese,w_t_stlung,w_t_st10,w_t_st12,w_t_st15
store " " to w_t_stcomp,w_t_stnorm,w_t_stnlq,w_t_stdraf
store " " to w_t_stpica,w_t_stelit, i_rdkey
store " " to w_t_stbold,w_t_stwide,w_t_stital,w_t_stunde
store " " to w_t_stbol_,w_t_stwid_,w_t_stita_,w_t_stund_
store ""  to Sm
store 0 to i_formh13,i_formh14,i_form_phh,i_form_phg,i_saveh13
store space(20) to w_stdesc
i_form_ph = 12
i_form_pf = 13
i_wait = 1
i_modal = .t.
i_oldrows = 0
store .F. to i_usr_brk, i_frm_rpr, w_s
i_exec = ""

dimension i_rdvars[41,2],i_rdkvars[6,2],i_zoompars[3]
store 0 to i_rdvars[1,1],i_rdkvars[1,1]
private w_StComp,w_NOMDES,w_DATITEST,w_DATITEST1,w_CODNAZ
private w_CODNAZ1,w_StComp,w_RATPAG,w_TIPPAG,w_DATO
private w_CNAZ,w_IVAFIS,w_CODPAG,w_NUMDOC,w_DATDOC
private w_DESBAN,w_DESBAN1,w_CODMAG,w_SERIAL,w_DESVAL
private w_StComp,w_NOMDES,w_DATITEST,w_DATITEST1,w_CODNAZ
private w_CODNAZ1,w_StComp,w_RATPAG,w_TIPPAG,w_DATO
private w_CNAZ,w_IVAFIS,w_CODPAG,w_NUMDOC,w_DATDOC
private w_DESBAN,w_DESBAN1,w_CODMAG,w_SERIAL,w_DESVAL
private w_CODICE,w_MVDESART,pMVVALRIG,w_PREZZO,SCONTO1
private SCONT2,w_VALRIG,w_IVA,SCONT3,SCONT4
private w_DESAR2,w_TXT2,IMPOSTA,w_IMPIVA,TOTALEDOC
private IMPIV1,CORRIS,L_NOTE,w_VETDAT2,w_PROVET2
private w_VETDAT1,w_PROVE1,NONCORRIS,SETRIG,w_DESSPE
private w_CAUTRA,w_MVQTACOL,w_QTALOR,w_DESPOR,w_DATORA
private w_DESVET,w_VETDAT,w_DATTRA,w_DESVE2,w_VETDA2
private TOTALE
w_StComp = space(2)
w_NOMDES = space(40)
w_DATITEST = space(35)
w_DATITEST1 = space(35)
w_CODNAZ = space(35)
w_CODNAZ1 = space(35)
w_StComp = space(2)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_DATO = space(24)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESBAN = space(10)
w_DESBAN1 = space(10)
w_CODMAG = space(10)
w_SERIAL = space(10)
w_DESVAL = space(23)
w_StComp = space(2)
w_NOMDES = space(40)
w_DATITEST = space(35)
w_DATITEST1 = space(35)
w_CODNAZ = space(35)
w_CODNAZ1 = space(35)
w_StComp = space(2)
w_RATPAG = space(10)
w_TIPPAG = space(2)
w_DATO = space(24)
w_CNAZ = space(4)
w_IVAFIS = space(20)
w_CODPAG = space(30)
w_NUMDOC = space(10)
w_DATDOC = ctod("  /  /  ")
w_DESBAN = space(10)
w_DESBAN1 = space(10)
w_CODMAG = space(10)
w_SERIAL = space(10)
w_DESVAL = space(23)
w_CODICE = space(18)
w_MVDESART = space(40)
pMVVALRIG = 0
w_PREZZO = 0
SCONTO1 = 0
SCONT2 = 0
w_VALRIG = 0
w_IVA = space(5)
SCONT3 = 0
SCONT4 = 0
w_DESAR2 = space(0)
w_TXT2 = space(0)
IMPOSTA = 0
w_IMPIVA = 0
TOTALEDOC = 0
IMPIV1 = 0
CORRIS = 0
L_NOTE = space(40)
w_VETDAT2 = space(30)
w_PROVET2 = space(30)
w_VETDAT1 = space(30)
w_PROVE1 = space(2)
NONCORRIS = 0
SETRIG = 0
w_DESSPE = space(20)
w_CAUTRA = space(25)
w_MVQTACOL = space(4)
w_QTALOR = space(8)
w_DESPOR = space(20)
w_DATORA = space(20)
w_DESVET = space(40)
w_VETDAT = space(35)
w_DATTRA = space(20)
w_DESVE2 = space(40)
w_VETDA2 = space(35)
TOTALE = 0

i_oldarea = select()
select __tmp__
go top

w_t_stnrig = 65
w_t_stmsin = 0
  
  
* --- Inizializza Variabili per configurazione stampante da CP_CHPRN
w_t_stdevi = cFileStampa+'.prn'
w_t_stlung = ts_ForPag
w_t_stnrig = ts_RowOk 
w_t_strese = ts_Reset+ts_Inizia
w_t_st10 = ts_10Cpi
w_t_st12 = ts_12Cpi
w_t_st15 = ts_15Cpi
w_t_stcomp = ts_Comp
w_t_stnorm = ts_RtComp
w_t_stbold = ts_StBold
w_t_stwide = ts_StDoub
w_t_stital = ts_StItal
w_t_stunde = ts_StUnde
w_t_stbol_ = ts_FiBold
w_t_stwid_ = ts_FiDoub
w_t_stita_ = ts_FiItal
w_t_stund_ = ts_FiUnde
* --- non definiti
*w_t_stmsin
*w_t_stnlq
*w_t_stdraf
*w_t_stpica
*w_t_stelit

i_row = 0
i_pag = 1
*---------------------------------------
wait wind "Generazione file appoggio..." nowait
*----------------------------------------
activate screen
* --- Settaggio stampante
set printer to &w_t_stdevi
set device to printer
set margin to w_t_stmsin
if len(trim(w_t_strese))>0
  @ 0,0 say &w_t_strese
endif
if len(trim(w_t_stlung))>0
  @ 0,0 say &w_t_stlung
endif
* --- Inizio stampa
do TX_D4CTR with 1, 0
if i_frm_rpr .and. .not. i_usr_brk
  * stampa il piede del report
  do TX_D4CTR with 14, 0
endif
if i_row<>0 
  @ prow(),pcol() say chr(12)
endif
set device to screen
set printer off
set printer to
if .not. i_frm_rpr
  do cplu_erm with "Non ci sono dati da stampare"
endif
* --- Fine
if .not.(empty(wontop()))
  activate  window (wontop())
endif
i_warea = alltrim(str(i_oldarea))
select (i_oldarea)
return


procedure TX_D4CTR
* === Procedure TX_D4CTR
parameters i_form_id, i_height

private i_currec, i_prevrec, i_formh
private i_frm_brk    && flag che indica il verificarsi di un break interform
                     && anche se la specifica condizione non � soddisfatta
private i_break, i_cond1, i_cond2

do case
  case i_form_id=1
    select __tmp__
    i_warea = '__tmp__'
    * --- inizializza le condizioni dei break interform
    i_cond1 = (MVSERIAL)
    i_cond2 = (MVSERIAL)
    i_frm_brk = .T.
    do while .not. eof() 
     wait wind "Elabora riga:"+str(recno()) +"/"+str(reccount()) nowait
      if .not. i_frm_rpr
        * --- stampa l'intestazione del report
        do TX_D4CTR with 11, 0
        i_frm_rpr = .T.
      endif
      if i_cond1<>(MVSERIAL) .or. i_frm_brk
        i_cond1 = (MVSERIAL)
        i_frm_brk = .T.
        do TX_D5CTR with 1.00, 24
      endif
      if i_cond2<>(MVSERIAL) .or. i_frm_brk
        i_cond2 = (MVSERIAL)
        do TX_D4CTR with 98
        i_form_ph = 1.01
        i_form_phh = 24
        i_form_phg = 464
      endif
      i_frm_brk = .F.
      * stampa del dettaglio
      do TX_D5CTR with 1.02, 1
      if (CTRL_A() or CTRL_D()) and not empty(nvl(mvcodice,' ')) AND CTRL_COAC()
        do TX_D5CTR with 1.03, 1
      endif
      if Ctrl2() AND CTRL_COAC()
        do TX_D5CTR with 1.04, 1
      endif
      if Ctrl1() AND Fscrdessup() AND NOT EMPTY(ALLTRIM(NVL(mvdessup,' '))) AND g_PERSDA='S' AND CTRL_COAC()
        do TX_D5CTR with 1.05, 1
      endif
      if not empty(nvl(L_NOTE,' ')) AND ULTRIG='S'
        do TX_D5CTR with 1.06, 1
      endif
      if i_usr_brk
        exit
      endif
      * --- passa al record successivo
      i_prevrec = recno()
      if .not. eof()
        skip
      endif
      i_currec = iif(eof(), -1, recno())
      if eof()  .or. i_cond2<>(MVSERIAL) .or. i_cond1<>(MVSERIAL)      
        i_form_ph = 12
        i_form_pf = 13
        i_formh13 = i_saveh13
      endif
      if eof()  .or. i_cond1<>(MVSERIAL)      
        go i_prevrec
        do TX_D5CTR with 1.08, 10
        do cplu_go with i_currec
      endif
    enddo
  case i_form_id=99
    * --- controllo per il salto pagina
    if inkey()=27
      i_usr_brk = .T.
    else
      if i_row+i_height+i_formh13>w_t_stnrig
        * --- stampa il piede di pagina
        i_row = w_t_stnrig-i_formh13
        if i_form_pf=13
          do TX_D4CTR with 13, 0
        else
          do TX_D5CTR with -i_form_pf,i_formh13
        endif
        i_row = 0
        i_pag = i_pag+1
        @ prow(),pcol() say chr(12)+chr(13)
        w_s = 0
        * --- stampa l'intestazione di pagina
        if i_form_ph=12
          do TX_D4CTR with 12, 0
        else
          do TX_D5CTR with -i_form_ph,i_form_phh
        endif
      endif
    endif
  case i_form_id=98
    i_form_pf = 1.07
    i_saveh13 = i_formh13
    i_formh13 = 13
endcase
return

procedure TX_D5CTR
* === Procedure TX_D5CTR
parameters i_form_id, i_form_h

* --- controllo per il salto pagina
if i_form_id<11 .and. i_form_id>0
  do TX_D4CTR with 99, i_form_h
  if i_usr_brk
    return
  endif
endif
if i_form_id<0
  i_form_id = -i_form_id
endif
do case
  * --- 1� form
  case i_form_id=1.0
    do frm1_0
  case i_form_id=1.01
    do frm1_01
  case i_form_id=1.02
    do frm1_02
  case i_form_id=1.03
    do frm1_03
  case i_form_id=1.04
    do frm1_04
  case i_form_id=1.05
    do frm1_05
  case i_form_id=1.06
    do frm1_06
  case i_form_id=1.07
    do frm1_07
  case i_form_id=1.08
    do frm1_08
endcase
i_row = i_row+i_form_h
return


* --- 1� form
procedure frm1_0
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(133-0),0,at_x(5),transform(w_StComp,""),i_fn
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(171-0),0,at_x(5),"Indirizzo di Consegna",i_fn
  if NOT EMPTY (NVL(ANDESCRI,' '))
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(171-0),40,at_x(323),"Spett.le",i_fn
  endif
  if EMPTY (NVL(DDNOMDES,' '))
  i_fn = ''
  do F_Say with i_row+10,i_row+at_y(190-0),0,at_x(5),"IDEM",i_fn
  endif
  w_NOMDES = ALLTRIM(LEFT(DDNOMDES,40))
  i_fn = ''
  do F_Say with i_row+11,i_row+at_y(209-0),0,at_x(5),transform(w_NOMDES,""),i_fn
  i_fn = ''
  do F_Say with i_row+11,i_row+at_y(209-0),40,at_x(323),transform(ANDESCRI,""),i_fn
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+12,i_row+at_y(228-0),0,at_x(5),transform(DDINDIRI,""),i_fn
  endif
  i_fn = ''
  do F_Say with i_row+12,i_row+at_y(228-0),40,at_x(323),transform(ANINDIRI,""),i_fn
  w_DATITEST = left(Fdatitest(),35)
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(247-0),0,at_x(5),transform(w_DATITEST,""),i_fn
  endif
  w_DATITEST1 = left(Fdatitest1(),35)
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(247-0),40,at_x(323),transform(w_DATITEST1,""),i_fn
  w_CODNAZ = LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(DDCODNAZ))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(266-0),0,at_x(5),transform(w_CODNAZ,""),i_fn
  w_CODNAZ1 = LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(ANNAZION))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(266-0),40,at_x(323),transform(w_CODNAZ1,""),i_fn
  w_StComp = ' '
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(266-0),77,at_x(616),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  if TDCATDOC='RF'
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(273-0),-1,at_x(-9),"X",i_fn
  endif
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(321-0),58,at_x(468),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(321-0),65,at_x(523),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_DATO = 'DOCUMENTO DI TRASPORTO'
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(321-0),92,at_x(738),transform(w_DATO,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(321-0),130,at_x(1047),transform(i_pag,"9999"),i_fn
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),0,at_x(5),transform(MVCODCON,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),17,at_x(142),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),22,at_x(177),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),41,at_x(332),transform(w_CODPAG,""),i_fn
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),106,at_x(850),transform(w_NUMDOC,""),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(359-0),126,at_x(1015),transform(w_DATDOC,""),i_fn
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(395-0),0,at_x(5),transform(MVCODAGE,""),i_fn
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(395-0),7,at_x(61),transform(AGDESAGE,""),i_fn
  w_DESBAN = nvl(looktab('ban_che','badesban','bacodban',mvcodban),'')
  if w_TIPPAG<>'MA' AND w_TIPPAG<>'BO'
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(395-0),67,at_x(543),transform(w_DESBAN,""),i_fn
  endif
  w_DESBAN1 = nvl(looktab('coc_mast','badescri','bacodban',mvcodba2),'')
  if w_TIPPAG='MA' OR w_TIPPAG='BO'
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(395-0),67,at_x(543),transform(w_DESBAN1,""),i_fn
  endif
  w_CODMAG = nvl(mvtcomag,'')
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(395-0),128,at_x(1029),transform(w_CODMAG,""),i_fn
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(431-0),0,at_x(5),transform(MVRIFORD,""),i_fn
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(431-0),107,at_x(857),transform(w_SERIAL,""),i_fn
   ENDIF
  if .f.
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(431-0),109,at_x(878),transform(VACODVAL,""),i_fn
   endif
  w_DESVAL = left(nvl(looktab('VALUTE','VADESVAL','VACODVAL',VACODVAL),''),23)
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(431-0),111,at_x(895),transform(w_DESVAL,""),i_fn
   Ambdoc()
return

* --- frm1_01
procedure frm1_01
  w_StComp = ' '
  if len(trim('&w_t_stnorm'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stnorm,""
  endif
  i_fn = ''
  do F_Say with i_row+8,i_row+at_y(627-481),0,at_x(5),transform(w_StComp,""),i_fn
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(646-481),0,at_x(5),"Indirizzo di Consegna",i_fn
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(646-481),40,at_x(325),"Spett.le",i_fn
  if EMPTY (NVL(DDNOMDES,' '))
  i_fn = ''
  do F_Say with i_row+10,i_row+at_y(665-481),0,at_x(5),"IDEM",i_fn
  endif
  w_NOMDES = ALLTRIM(LEFT(DDNOMDES,40))
  i_fn = ''
  do F_Say with i_row+11,i_row+at_y(684-481),0,at_x(5),transform(w_NOMDES,""),i_fn
  i_fn = ''
  do F_Say with i_row+11,i_row+at_y(684-481),40,at_x(325),transform(ANDESCRI,""),i_fn
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+12,i_row+at_y(703-481),0,at_x(5),transform(DDINDIRI,""),i_fn
  endif
  i_fn = ''
  do F_Say with i_row+12,i_row+at_y(703-481),40,at_x(325),transform(ANINDIRI,""),i_fn
  w_DATITEST = left(Fdatitest(),35)
  if not empty(nvl(mvcoddes,''))
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(722-481),0,at_x(5),transform(w_DATITEST,""),i_fn
  endif
  w_DATITEST1 = left(Fdatitest1(),35)
  i_fn = ''
  do F_Say with i_row+13,i_row+at_y(722-481),40,at_x(325),transform(w_DATITEST1,""),i_fn
  w_CODNAZ = LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(DDCODNAZ))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(741-481),0,at_x(5),transform(w_CODNAZ,""),i_fn
  w_CODNAZ1 = LOOKTAB('NAZIONI','NADESNAZ','NACODNAZ',ALLTRIM(ANNAZION))
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(741-481),40,at_x(325),transform(w_CODNAZ1,""),i_fn
  w_StComp = ' '
  i_fn = ''
  do F_Say with i_row+14,i_row+at_y(741-481),74,at_x(597),transform(w_StComp,""),i_fn
  if len(trim('&w_t_stcomp'))<>0
    do F_Say with -1,-1,-1,-1,&w_t_stcomp,""
  endif
  w_RATPAG = NVL(LOOKTAB('DOC_RATE','RSMODPAG','RSSERIAL',MVSERIAL,'RSNUMRAT',1),' ')
  IF .F.
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(786-481),54,at_x(434),transform(w_RATPAG,""),i_fn
   ENDIF
  w_TIPPAG = NVL(LOOKTAB('MOD_PAGA','MPTIPPAG','MPCODICE',w_RATPAG),' ')
  IF .F.
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(786-481),61,at_x(489),transform(w_TIPPAG,""),i_fn
   ENDIF
  w_DATO = 'DOCUMENTO DI TRASPORTO'
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(786-481),92,at_x(738),transform(w_DATO,""),i_fn
  i_PAG=IIF(w_SERIAL<>MVSERIAL,1,i_PAG)
  i_fn = ''
  do F_Say with i_row+16,i_row+at_y(786-481),130,at_x(1047),transform(i_pag,"9999"),i_fn
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),0,at_x(5),transform(MVCODCON,""),i_fn
  w_CNAZ = LTRIM(nvl(looktab('nazioni','nacodiso','nacodnaz',annazion),''))
  if !empty(nvl(anpariva,''))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),17,at_x(141),transform(w_CNAZ,""),i_fn
  endif
  w_IVAFIS = ltrim(iif(empty(anpariva),ancodfis,anpariva))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),22,at_x(177),transform(w_IVAFIS,""),i_fn
  w_CODPAG = Caldespag()
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),41,at_x(332),transform(w_CODPAG,""),i_fn
  w_NUMDOC = Alltrim(str(mvnumdoc,15))+iif(empty(mvalfdoc),'','/'+Alltrim(mvalfdoc))
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),106,at_x(850),transform(w_NUMDOC,""),i_fn
  w_DATDOC = CP_TODATE(MVDATDOC)
  i_fn = ''
  do F_Say with i_row+18,i_row+at_y(824-481),126,at_x(1015),transform(w_DATDOC,""),i_fn
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(862-481),0,at_x(5),transform(MVCODAGE,""),i_fn
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(862-481),7,at_x(61),transform(AGDESAGE,""),i_fn
  w_DESBAN = nvl(looktab('ban_che','badesban','bacodban',mvcodban),'')
  if w_TIPPAG<>'MA' AND w_TIPPAG<>'BO'
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(862-481),67,at_x(543),transform(w_DESBAN,""),i_fn
  endif
  w_DESBAN1 = nvl(looktab('coc_mast','badescri','bacodban',mvcodba2),'')
  if w_TIPPAG='MA' OR w_TIPPAG='BO'
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(862-481),67,at_x(543),transform(w_DESBAN1,""),i_fn
  endif
  w_CODMAG = nvl(mvtcomag,'')
  i_fn = ''
  do F_Say with i_row+20,i_row+at_y(862-481),128,at_x(1029),transform(w_CODMAG,""),i_fn
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(900-481),0,at_x(5),transform(MVRIFORD,""),i_fn
  w_SERIAL = MVSERIAL
  IF .F.
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(900-481),106,at_x(855),transform(w_SERIAL,""),i_fn
   ENDIF
  if .f.
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(900-481),109,at_x(877),transform(VACODVAL,""),i_fn
   endif
  w_DESVAL = left(nvl(looktab('VALUTE','VADESVAL','VACODVAL',VACODVAL),''),23)
  i_fn = ''
  do F_Say with i_row+22,i_row+at_y(900-481),111,at_x(895),transform(w_DESVAL,""),i_fn
   Ambdoc()
return

* --- frm1_02
procedure frm1_02
return

* --- frm1_03
procedure frm1_03
  w_CODICE = iif(nvl(anflcodi,' ')='S',left(mvcodice,18),left(mvcodart,18))
  if nvl(arstacod,' ')<>'S'
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),1,at_x(8),transform(w_CODICE,""),i_fn
  endif
  w_MVDESART = left(alltrim(MVDESART),40)
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),21,at_x(168),transform(w_MVDESART,""),i_fn
  pMVVALRIG = IIF(MVFLOMAG='X',MVVALRIG,0)
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),64,at_x(512),transform(pMVVALRIG,""),i_fn
   ENDIF
   TOTALE = TOTALE+pMVVALRIG
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),70,at_x(560),transform(MVUNIMIS,Repl('X',3)),i_fn
  if mvtiprig<>'D' and fcodart()
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),73,at_x(584),transform(MVQTAMOV,'@Z '+v_PQ(11)),i_fn
  endif
  w_PREZZO = iif(mvrifkit<>0,'',TRAN(mvprezzo,'@Z '+V_PV[34+(20*VADECUNI)]))
  if (NVL(TDCATDOC,'')<>'DT') OR anprebol='S'
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),87,at_x(696),transform(w_PREZZO,""),i_fn
  endif
  SCONTO1 = iif(mvrifkit<>0,0,mvscont1)
  if Ctrl3() .and. sconto1<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),102,at_x(816),transform(SCONTO1,'999.99'),i_fn
  endif
  SCONT2 = iif(mvrifkit<>0,0,mvscont2)
  if Ctrl4() .and. scont2<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),109,at_x(872),transform(SCONT2,'999.99'),i_fn
  endif
  w_VALRIG = right(space(16)+alltrim(FValrig()),16)
  if (NVL(TDCATDOC,'')<>'DT') OR anprebol='S'
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),114,at_x(912),transform(w_VALRIG,""),i_fn
  endif
  w_IVA = Right(alltrim(Space(5)+FIva()),5)
  if ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S') and (!empty(mvprezzo))
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(988-987),132,at_x(1056),transform(w_IVA,Repl('X',5)),i_fn
  endif
return

* --- frm1_04
procedure frm1_04
  SCONT3 = iif(mvrifkit<>0,0,mvscont3)
  if scont3<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1026-1025),102,at_x(816),transform(SCONT3,'999.99'),i_fn
  endif
  SCONT4 = iif(mvrifkit<>0,0,mvscont4)
  if scont4<>0 .AND. ((NVL(TDCATDOC,'')<>'DT') OR anprebol='S')
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1026-1025),109,at_x(872),transform(SCONT4,'999.99'),i_fn
  endif
return

* --- frm1_05
procedure frm1_05
  w_DESAR2 = mvdessup
  i_fn = ''
  w_s = 41
  set memowidth to w_s
  Sm = mline(w_DESAR2,1)+space(w_s-len(mline(w_DESAR2,1)))
  do F_Say with i_row+0,i_row+at_y(1064-1063),21,at_x(168),alltrim(Sm),i_fn
  i_ml = 0
  w_s = 41
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_DESAR2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_D4CTR with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 41
    set memowidth to w_s
      i_fn = ''
    Sm = mline(w_DESAR2,i_i)+space(w_s-len(mline(w_DESAR2,i_i)))
    do F_Say with i_row,i_row,21,at_x(168),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_06
procedure frm1_06
  w_TXT2 = L_NOTE
  if ultrig='S'
  i_fn = ''
  w_s = 40
  set memowidth to w_s
  Sm = mline(w_TXT2,1)+space(w_s-len(mline(w_TXT2,1)))
  do F_Say with i_row+0,i_row+at_y(1102-1101),21,at_x(168),alltrim(Sm),i_fn
  endif
  i_ml = 0
  w_s = 40
  set memowidth to w_s
  i_ml = max(i_ml,memlines(w_TXT2))
  i_row = i_row+i_form_h
  for i_i=2 to i_ml
    do TX_D4CTR with 99,1
    if i_usr_brk
      exit
    endif
    w_s = 40
    set memowidth to w_s
      i_fn = ''
    Sm = mline(w_TXT2,i_i)+space(w_s-len(mline(w_TXT2,i_i)))
    do F_Say with i_row,i_row,21,at_x(168),alltrim(Sm),i_fn
    i_row = i_row+1
  next
  i_row = i_row-i_form_h
return

* --- frm1_07
procedure frm1_07
  i_fn = ''
  do F_Say with i_row+4,i_row+at_y(1234-1139),117,at_x(936),"SEGUE =======>",i_fn
return

* --- frm1_08
procedure frm1_08
  IMPOSTA = FImposta()
  if .f.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),1,at_x(11),transform(IMPOSTA,""),i_fn
   endif
  w_IMPIVA = FImpiva()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),4,at_x(33),transform(w_IMPIVA,""),i_fn
   ENDIF
  TOTALEDOC = w_impiva+imposta+mvimparr+mvspebol
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),6,at_x(53),transform(TOTALEDOC,""),i_fn
   ENDIF
  IMPIV1 = nvl(mvaimpn1,0)+nvl(mvaimpn2,0)+nvl(mvaimpn3,0)+nvl(mvaimpn4,0)+nvl(mvaimpn5,0)+nvl(mvaimpn6,0)
  if .f.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),9,at_x(75),transform(IMPIV1,""),i_fn
   endif
  CORRIS = iif(MVACCONT=0,totaledoc,MVACCONT)
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),11,at_x(95),transform(CORRIS,""),i_fn
   ENDIF
  L_NOTE = ' '
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),16,at_x(135),transform(L_NOTE,""),i_fn
   ENDIF
  w_VETDAT2 = FVetdat2()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),19,at_x(156),transform(w_VETDAT2,""),i_fn
   ENDIF
  w_PROVET2 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodve2),'')
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),22,at_x(177),transform(w_PROVET2,""),i_fn
   ENDIF
  w_VETDAT1 = FVetdat1()
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),24,at_x(197),transform(w_VETDAT1,""),i_fn
   ENDIF
  w_PROVE1 = NVL(looktab('vettori','vtprovet','vtcodvet',mvcodvet),'')
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),27,at_x(216),transform(w_PROVE1,""),i_fn
   ENDIF
  NONCORRIS = iif(MVACCONT=0,0,totaledoc-corris)
  IF .F.
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),30,at_x(240),transform(NONCORRIS,""),i_fn
   ENDIF
  SETRIG = ' '
  I_ROW=52
  i_fn = ''
  do F_Say with i_row+0,i_row+at_y(1417-1416),33,at_x(264),transform(SETRIG,""),i_fn
  w_DESSPE = NVL(translat(MVTIPCON,MVCODCON,'TRADSPED','lgdescri','LGCODSPE','lgcodlin','MODASPED', 'SPDESSPE','SPCODSPE',mvcodspe),'')
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1439-1416),1,at_x(10),transform(w_DESSPE,""),i_fn
  if NVL(TDFLACCO,' ')='S'
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1439-1416),59,at_x(478),transform(MVASPEST,""),i_fn
  endif
  w_CAUTRA = looktab('cam_agaz','cmdescri','cmcodice',mvtcamag)
  i_fn = ''
  do F_Say with i_row+1,i_row+at_y(1439-1416),98,at_x(789),transform(w_CAUTRA,""),i_fn
  w_MVQTACOL = TRAN(MVQTACOL)
  if NOT EMPTY(MVQTACOL) AND nvl(tdflpack,'')='S'
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1474-1416),1,at_x(10),transform(w_MVQTACOL,""),i_fn
  endif
  w_QTALOR = TRAN(mvqtalor,'@KZ '+v_PV(91))
  if NOT EMPTY(NVL(MVQTALOR,'')) and nvl(tdflpack,'')='S'
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1474-1416),8,at_x(71),transform(w_QTALOR,""),i_fn
  endif
  w_DESPOR = NVL(translat(MVTIPCON,MVCODCON,'TRADPORT','lgdescri','LGCODPOR','lgcodlin','PORTI', 'PODESPOR','POCODPOR',mvcodpor),'')
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1474-1416),27,at_x(216),transform(w_DESPOR,""),i_fn
  w_DATORA = dtoc(mvdattra)+'  '+iif(not empty(nvl(mvoratra,0)) and not empty(nvl(mvmintra,0)),mvoratra+':'+mvmintra,space(5))
  if empty(NVL(mvcodvet,'')) and not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ''
  do F_Say with i_row+3,i_row+at_y(1474-1416),60,at_x(484),transform(w_DATORA,""),i_fn
  endif
  i_fn = ''
  do F_Say with i_row+5,i_row+at_y(1515-1416),1,at_x(10),transform(MVNOTAGG,""),i_fn
  w_DESVET = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodvet),'')
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(1554-1416),3,at_x(29),transform(w_DESVET,""),i_fn
  w_VETDAT = LEFT(w_VETDAT1,38)+space(1)+alltrim(w_PROVE1)
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(1554-1416),44,at_x(354),transform(w_VETDAT,""),i_fn
  w_DATTRA = dtoc(mvdattra)+'  '+iif(not empty(nvl(mvoratra,0)) and not empty(nvl(mvmintra,0)),mvoratra+':'+mvmintra,space(5))
  if NOT EMPTY(NVL(MVCODVET,'')) and not empty(nvl(dtoc(mvdattra),' '))
  i_fn = ''
  do F_Say with i_row+7,i_row+at_y(1554-1416),87,at_x(700),transform(w_DATTRA,""),i_fn
  endif
  w_DESVE2 = nvl(looktab('vettori','vtdesvet','vtcodvet',mvcodve2),'')
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(1592-1416),3,at_x(29),transform(w_DESVE2,""),i_fn
  w_VETDA2 = w_VETDAT2+alltrim(w_PROVET2)
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(1592-1416),44,at_x(354),transform(w_VETDA2,""),i_fn
  TOTALE = 0
  IF .F.
  i_fn = ''
  do F_Say with i_row+9,i_row+at_y(1592-1416),106,at_x(850),transform(TOTALE,""),i_fn
   ENDIF
return

function at_x
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/6
  return i_pos

function at_y
parameters i_pos, i_fnt, i_h, i_s
  i_pos = i_pos/13
  return i_pos

procedure F_Say
  parameter y,py,x,px,s,f
  * --- Questa funzione corregge un errore del driver "Generica solo testo"
  *     di Windows che aggiunge spazi oltre la 89 colonna
  *     Inoltre in Windows sostituisce il carattere 196 con un '-'
  if y=-1
    y = prow()
    x = pcol()
  endif
  @ y,x say s
  return

PROCEDURE CPLU_GO
parameter i_recpos

if i_recpos<=0 .or. i_recpos>reccount()
  if reccount()<>0
    goto bottom
    if .not. eof()
      skip
    endif
  endif  
else
  goto i_recpos
endif
return

* --- Area Manuale = Functions & Procedures 
* --- TX_DOCTR

FUNCTION CTRL_A
Private w_RET
w_RET=.f.

if empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))
        w_RET=.T.
endif




Return (w_RET)
endfunc

FUNCTION CTRL_D
Private w_RET
w_RET=.f.

if !empty(mvdesart) and ((empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ') and ;
mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ') ;
and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
   w_RET=.T.
endif
Return (w_RET)
endfunc

FUNCTION CTRL1
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and empty(mvcodcla) or ;
   (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ') and ; 
   mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ') and ;
   mvcodcla<>nvl(tdstacl5,' '))) 
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL2
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont3) or not empty(mvscont4);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc


FUNCTION CTRL4
Private w_RET
w_RET=.f.

if (mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont2);
   and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
   and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
   and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' ')))
       w_RET=.t.
endif
Return (w_RET)
endfunc

FUNCTION CTRL3
Private w_RET
w_RET=.f.

if ((mvrifkit=0 or (mvrifkit<>0 and arflstco='S')) and not empty(mvscont1);
    and (empty(mvcodcla) or (mvcodcla<>nvl(tdstacl1,' ');
    and mvcodcla<>nvl(tdstacl2,' ') and mvcodcla<>nvl(tdstacl3,' ');
    and mvcodcla<>nvl(tdstacl4,' ') and mvcodcla<>nvl(tdstacl5,' '))))
       w_RET=.t.
endif
Return (w_RET)
endfunc


Function FValrig
private w_VALRIG 
w_VALRIG=iif(mvrifkit<>0,'',iif(mvflomag='X',TRAN(mvvalrig,'@Z '+V_PV[18*(VADECTOT+2)]),;
                  iif(mvflomag='S','Sconto Merce',iif(mvflomag='I','Omaggio Imponibile',;
                  iif(mvflomag='E','Omaggio Imp.+IVA','')))))
Return(w_VALRIG)
endfunc

Function FIva
private w_IVA
w_IVA=iif(mvrifkit<>0,'',iif(empty(nvl(mvcodive,' ')) or nvl(ivperiva,0)=0,mvcodiva,mvcodive))
Return (w_IVA)
endfunc

Function Fscrdessup
Private w_RET
w_RET=.f.
if 'S'=nvl(looktab('ART_ICOL','ARSTASUP','ARCODART',MVCODART),'')
    w_RET=.t.
endif
Return (w_RET)
endfunc

Function Fdatitest1
private w_DATITEST1
w_DATITEST1=alltrim(an___cap)+iif(empty(anlocali),'',' '+alltrim(anlocali ))+;
                      iif(empty(alltrim(anprovin)),'',+' ( ' +anprovin+' ) ')
Return (w_DATITEST1)
endfunc

Function Fdatitest
private w_DATITEST
w_DATITEST=alltrim(dd___cap)+iif(empty(ddlocali),'',' '+alltrim(ddlocali ));
                      +iif(empty(alltrim(ddprovin)),'',+' ( ' +ddprovin+' ) ')
return(w_DATITEST)
endfunc


Function FTxt1
Private w_TXT1
w_TXT1='Dichiarazione di intento n� ' + alltrim(str(L_NDIC));
                + ' del ' +dtoc(L_DDIC)+' Art. Es. '+alltrim(L_ARTESE);
                +' del D.P.R. 633/72'+iif(L_TIPOPER='D','  Valida dal '+DTOC(L_DATINI);
                + ' al '+DTOC(L_DATFIN),IIF(L_TIPOPER='I',' Limite es: '+alltrim(IMP),''))
Return (w_TXT1)
Endfunc

Function FImposta
Private imposta
imposta=iif(mvaflom1$'XI',mvaimps1,0)+iif(mvaflom2$'XI',mvaimps2,0);
               +iif(mvaflom3$'XI',mvaimps3,0)+iif(mvaflom4$'XI',mvaimps4,0);
                +iif(mvaflom5$'XI',mvaimps5,0)+iif(mvaflom6$'XI',mvaimps6,0)
Return(imposta)
Endfunc

FUNCTIO FVETDAT1
private w_VETDAT1
w_VETDAT1=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodvet),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodvet),''))+'   '
RETURN (w_VETDAT1)
ENDFUNC

FUNCTION FVETDAT2
private w_VETDAT2
w_VETDAT2=alltrim(NVL(looktab('vettori','vtindvet','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtvetcap','vtcodvet',mvcodve2),''));
                     +'   '+alltrim(NVL(looktab('vettori','vtlocvet','vtcodvet',mvcodve3),''))+'   '
RETURN(w_VETDAT2)
ENDFUNC

FUNCTION FIMPIVA
PRIVATE w_IMPIVA
w_IMPIVA=iif(mvaflom1='X',mvaimpn1,0)+iif(mvaflom2='X',mvaimpn2,0);
                 +iif(mvaflom3='X',mvaimpn3,0)+iif(mvaflom4='X',mvaimpn4,0);
                 +iif(mvaflom5='X',mvaimpn5,0)+iif(mvaflom6='X',mvaimpn6,0)
RETURN(w_IMPIVA)
ENDFUNC

FUNCTION FCODART
Private w_RET
w_RET=.f.

if (not empty(mvqtamov) or not empty(mvprezzo) or empty(mvcodcla)) or (mvcodcla<>nvl(tdstacl1,' ') and mvcodcla<>nvl(tdstacl2,' ');
   and mvcodcla<>nvl(tdstacl3,' ') and mvcodcla<>nvl(tdstacl4,' ');
   and mvcodcla<>nvl(tdstacl5,' '))
       w_RET=.t.
endif
Return (w_RET)
endfunc
* ----------------------------------------------------------------------
* Funzione : Pari_A
* ----------------------------------------------------------------------
*  Esegue traduzione importo documento in EURO o LIRE
* ----------------------------------------------------------------------
Function Pari_a
parameter Valuta
private w_ImpRet,w_TotPar
do case
   case MVCODVAL=g_CODEUR
        w_TotPar =  ROUND(( TOTALEA*p_Cambio),0)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(40)])
   case MVCODVAL=g_CODLIR
         w_TotPar =  ROUND((TOTALEA/p_Cambio),2)
         w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   otherwise
   if mvdatdoc>=g_dateur and vacodval<>g_codeur
        w_TotPar = ROUND((TOTALEA/mvcaoval),2)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   else
        w_TotPar = ROUND((TOTALEA*p_cambio),0)
        w_ImpRet = TRAN(w_TotPar,'@Z '+V_PV[(80)])
   endif
endcase

return (w_ImpRet)

Func Caldespag()
    Private w_Ret
    w_Ret = mvcodpag+' '+NVL(translat(mvtipcon,mvcodcon,'tradpaga','lgdescod','lgcodice','lgcodlin','pag_amen','padescri','pacodice',mvcodpag),'')
    Return (w_Ret)
Endfunc

FUNCTION CTRL_COAC
Private w_RET
w_RET=.t.
* INDICA SE STAMPARE LA RIGA DI CONTRIBUTO ACCESSORIO
if G_COAC = "S"
  if VARTYPE(MVCACONT) = "C"
    if NVL( LOOKTAB( "TIP_DOCU" , "TDNOSTCO"  , "TDTIPDOC" , TDTIPDOC ) , "N" ) = "S" and !EMPTY(MVCACONT)
      * se il flag sulla causale � attivo e siamo su una riga di contributo accessorio,
      * la riga non viene stampata
      w_RET=.f.
    else
      w_RET=.t.
    endif
  else
    w_RET=.t.
  endif
else
  w_RET=.t.
endif

Return (w_RET)
endfunc

* --- Fine Area Manuale 
