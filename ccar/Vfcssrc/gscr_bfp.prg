* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bfp                                                        *
*              Calcolo formule modelli U.M. parametriche                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-24                                                      *
* Last revis.: 2014-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pVAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bfp",oParentObject,m.pOper,m.pVAR)
return(i_retval)

define class tgscr_bfp as StdBatch
  * --- Local variables
  pOper = space(5)
  pVAR = space(5)
  w_OCCUR = 0
  w_LUNGVAR = 0
  w_INDOCC = 0
  w_CODICE = space(5)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato per l' inserimento delle Formule associate alle Configurazioni/U.M. Parametriche (da GSAR_ATM)
    do case
      case INLIST(alltrim(this.pOper),"0","1","2","3","4","5","6","7","8","9")
        this.oParentObject.w_CCFORMUL = alltrim(this.oParentObject.w_CCFORMUL)+alltrim(this.pOper)
      case INLIST(alltrim(this.pOper),"+","-","*","/","(",".",")")
        this.oParentObject.w_CCFORMUL = alltrim(this.oParentObject.w_CCFORMUL)+alltrim(this.pOper)
      case alltrim(this.pOper)="CE"
        this.w_OCCUR = 0
        DIMENSION OCCURV(6)
        OCCURV(1) = RATC("+",this.oParentObject.w_CCFORMUL)
        OCCURV(2) = RATC("-",this.oParentObject.w_CCFORMUL)
        OCCURV(3) = RATC("*",this.oParentObject.w_CCFORMUL)
        OCCURV(4) = RATC("/",this.oParentObject.w_CCFORMUL)
        OCCURV(5) = RATC("(",this.oParentObject.w_CCFORMUL)
        OCCURV(6) = RATC(")",this.oParentObject.w_CCFORMUL)
        this.w_LUNGVAR = LEN(ALLTRIM(this.oParentObject.w_CCFORMUL))
        FOR this.w_INDOCC=1 TO 6
        this.w_OCCUR = IIF(this.w_OCCUR < OCCURV(this.w_INDOCC), IIF(this.w_LUNGVAR=OCCURV(this.w_INDOCC),OCCURV(this.w_INDOCC)-1,OCCURV(this.w_INDOCC)), this.w_OCCUR)
        ENDFOR
        this.oParentObject.w_CCFORMUL = SUBSTR(ALLTRIM(this.oParentObject.w_CCFORMUL),1,this.w_OCCUR)
      case alltrim(this.pOper)="CZ"
        this.oParentObject.w_CCFORMUL = ""
      case alltrim(this.pOper)="VARIABILI"
        this.w_CODICE = ALLTRIM(this.pVAR)
        if NOT EMPTY(NVL(this.w_CODICE, " "))
          this.oParentObject.w_CCFORMUL = alltrim(this.oParentObject.w_CCFORMUL) + "<" + alltrim(this.w_CODICE) + ">"
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper,pVAR)
    this.pOper=pOper
    this.pVAR=pVAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pVAR"
endproc
