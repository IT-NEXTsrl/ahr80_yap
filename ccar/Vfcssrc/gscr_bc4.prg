* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bc4                                                        *
*              Configuratore caratteristiche                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-16                                                      *
* Last revis.: 2016-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bc4",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_bc4 as StdBatch
  * --- Local variables
  w_OPER = space(10)
  GSCR_KCC = .NULL.
  w_nTMP = 0
  w_TIPPAD = space(2)
  w_cTMP = space(1)
  w_cTMP1 = space(1)
  w_CORIFDIS = space(41)
  w_COCODCOM = space(41)
  w_CCCODICE = space(5)
  w_CPROWORD = 0
  w_CC__DEFA = space(1)
  w_CCOPERAT = space(1)
  w_CC_COEFF = 0
  w_LVLKEY = space(200)
  w_EXPLDB = space(10)
  w_OK = .f.
  w_CCROWORD = 0
  w_i = 0
  w_MESS = space(10)
  w_VAR_i = space(5)
  w_UPDSTR = .f.
  w_CCCODIDX = space(100)
  w_CC__DIBA = space(41)
  w_CCROWORD = 0
  w_CCPROWORD = 0
  w_CCCOMPON = space(41)
  w_CCDESCRI = space(40)
  w_DITRADUZ = space(40)
  LenSez = 0
  Undscor = space(10)
  w_cCursor = space(20)
  Albero = .NULL.
  w_FirstTime = .f.
  w_TOTEXP = space(1)
  w_CODCAA = space(5)
  w_CCOS = space(15)
  w_SINGOLO = space(1)
  w_TTRISP = 0
  w_MSG = space(254)
  w_CCDESSUP = space(80)
  w_OBJCTRL = .NULL.
  w_TEST = space(41)
  w_MAXLVL = 0
  w_VALORE = 0
  w_MYKEY = space(10)
  w_LenLvl = 0
  w_TIPART = space(2)
  Colonne = 0
  w_TIPPAD = space(2)
  w_STADIST = space(1)
  w_EXPLODED = space(1)
  w_EXPANDED = space(1)
  Dettaglio = space(10)
  w_NODITV = .NULL.
  w_nNODO = 0
  w_lvlk = space(200)
  NFigli = 0
  w_KEY = 0
  Prefisso = space(200)
  w_trovato = .f.
  w_car = space(150)
  w_bmp = space(150)
  w_cardet = space(150)
  w_carmod = space(150)
  w_cardes = space(150)
  w_carmod = space(150)
  w_descrizione = space(150)
  w_modello = space(150)
  w_modmod = space(150)
  w_descrizione = space(150)
  w_descrimod = space(150)
  w_CODICE = space(20)
  TmpC = space(200)
  w_righe = 0
  w_FINECIC = .f.
  w_CC = 0
  w_LIVELLO = 0
  w_LASTLIV = 0
  w_FIRST = .f.
  w_TOTRISP = 0
  w_SCEDEFA = space(200)
  w_PROG = .NULL.
  w_chiave = space(200)
  w_CLAPRE = space(5)
  w_CLARIF = space(5)
  w_TIPART = space(2)
  w_CONFIG = space(0)
  w_DDESSUP = space(0)
  w_MSGDESC = space(200)
  w_TREEVIEW = .NULL.
  GSCR_KCC = .NULL.
  w_CINITCURD = space(10)
  w_nRec = 0
  w_LVLKEYP = space(200)
  w_AGGIORNATE = 0
  w_TMPNC = 0
  w_NEWCURS = space(10)
  w_CANCLIV = .f.
  w_CARUPDATED = 0
  w_ROWS = 0
  w_GENERADIBA = .f.
  w_CFGINI = 0
  w_CFGFIN = 0
  w_PDBLCFGC = space(1)
  w_TENTATIVI = 0
  w_TTENTATIVI = 0
  GSOR_MDV = .NULL.
  w_oTMP = .NULL.
  w_LVLKEYP = space(200)
  w_CC_NOTED = space(40)
  w_CADESART = space(40)
  w_Progressivo = space(15)
  w_CADESSUP = space(0)
  w_COCODCOM = space(41)
  w_CODESSUP = space(40)
  w_CACODART = space(20)
  w_COCODART = space(20)
  w_CATIPCON = space(1)
  w_CA__TIPO = space(1)
  w_PRKEYVAR = space(20)
  SUPREMO = space(20)
  w_SUPREMO = space(20)
  PadreSupremo = space(20)
  w_CATIPBAR = space(1)
  w_DesDis = space(40)
  w_CAFLSTAM = space(1)
  w_DesSup = space(10)
  w_CAUNIMIS = space(3)
  w_COXCHKGE = space(1)
  w_CAOPERAT = space(2)
  w_ARCENCOS = space(15)
  TIPVAR = space(5)
  w_CAMOLTIP = 0
  w_CADTINVA = ctod("  /  /  ")
  w_CADTOBSO = ctod("  /  /  ")
  w_CACODICE = space(41)
  w_nPOS = 0
  w_CodArt = space(20)
  w_DBDATDIS = ctod("  /  /  ")
  w_LenSez = 0
  w_ELSCELTE = space(10)
  w_CAUDIS = space(5)
  w_PDRICONF = space(1)
  w_n = 0
  w_RECZERO = 0
  w_lvlkey = space(200)
  w_RIGA = 0
  NFigli = 0
  w_KEY = 0
  w_NODI = .NULL.
  Prefisso = space(200)
  w_car = space(150)
  w_CPROWNUM = 0
  w_NUMDIST = 0
  w_max = 0
  w_codcar = space(5)
  w_LVLKEY2 = space(200)
  w_LVLKEY1 = space(200)
  w_LVLKEY3 = space(200)
  w_SCRIPTSQL = space(0)
  w_RESULT = 0
  w_LCONN = 0
  w_LTABLE = space(20)
  w_maxlvl = 0
  w_cIntestazione = space(10)
  w_LVLKEYO = space(200)
  w_CARAT = space(20)
  w_CARATO = space(20)
  w_AggModelli = .f.
  w_VAL_i = 0
  w_i_Tot = 0
  w_CONTINUECHECK = space(1)
  w_INTERACTIVECHECK = .f.
  w_NUMCARMAN = 0
  w_NUMCADES = 0
  w_NUMCNDES = 0
  m_CCCODICE = space(5)
  m_CCTIPOCA = space(1)
  w_ROWSELE = 0
  GSCRIKCC = .NULL.
  Albero1 = .NULL.
  w_cExpldb2 = space(10)
  w_TRADUZ = space(10)
  w_cTMP2 = space(10)
  w_cTMP2 = space(10)
  w_POS = 0
  w_POS1 = 0
  w_CONT = 0
  w_STR = space(6)
  w_STR1 = space(6)
  w_OLDVAL = space(10)
  w_NEWVAL = space(0)
  w_INALBE = .f.
  w_LIV = 0
  w_LIVMAX = 0
  w_lvlkeycur = space(200)
  w_lvlkeymax = space(200)
  w_expchiave = space(200)
  w_INALBE = .f.
  w_ESPLODI = .f.
  w_LIVCUR = 0
  w_OKLIV = .f.
  w_DAINSERIRE = .f.
  w_FINDDAINS = .f.
  w_LVLKEYVER = space(200)
  w_DCARA = 0
  w_TIPCA = space(1)
  w_CODCA = space(5)
  w_RSEL = 0
  w_NOC = 0
  w_VARIAR = space(20)
  w_CODIAR = space(20)
  w_RIFDIS = space(41)
  w_NUMLIV = 0
  w_CHIAVE = space(254)
  w_DBFOUND = space(20)
  w_CURNAME = space(10)
  w_FOUNDSUP = .f.
  w_DISFOUND = space(41)
  w_TIPART = space(2)
  w_CCRIFE = space(20)
  w_CCCHAR = space(1)
  w_CCLUNG = 0
  w_CCPROG = space(18)
  w_NEWCODE = space(20)
  w_KEYSAL = space(40)
  w_SUPFOUND = space(41)
  w_KEYPROG = space(18)
  w_LenSez = 0
  w_DBGEN = .f.
  w_CODESSUP = space(80)
  w_VIR_DESCRI = space(40)
  w_VIR_DESSUP = space(0)
  w_RIF = space(20)
  w_TOFIND = space(41)
  w_RIFFOUND = space(20)
  w_ARTICOLO = space(20)
  w_ARRIFE = space(20)
  w_DBPRIORI = space(4)
  w_DBTIPDIS = space(4)
  w_DBCODRIS = space(20)
  w_cExpldb2 = space(10)
  w_ARFOUND = space(20)
  w_DIBAFOUND = .f.
  w_RISPOSTA = .f.
  w_CCNOTEMO = space(0)
  w_UPDMODEL = .f.
  w_EXISTMODEL = .f.
  w_MODAGEN = .f.
  w_CONFKME = .f.
  w_ANNULLA = .f.
  w_CCFORMUL = space(0)
  w_MVCODICE = .NULL.
  w_OBJDOC = .NULL.
  L_CCDETTAG = space(0)
  L_CCTIPOCA = space(1)
  L_CCCODICE = space(5)
  w_ROWNUM = 0
  w_CAUNMISU = space(3)
  w_CAQTAMOV = 0
  w_CAQTAMO1 = 0
  L_CCFORMUL = space(0)
  w_ERRORE = space(1)
  w_MSGERRORE = space(0)
  L_CCFORMUL = space(0)
  w_bFormulaNonValida = .f.
  w_bValoreTroppoGrande = .f.
  w_bValoreTroppoPiccolo = .f.
  w_CCTIPOLD = space(1)
  w_CCCODOLD = space(5)
  w_OKCONFIG = .f.
  m_CPROWNUM = 0
  w_L_CCTIPOCA = space(1)
  w_CHIAVE1 = space(10)
  w_bFormulaNonValida = .f.
  w_bValoreTroppoGrande = .f.
  w_bValoreTroppoPiccolo = .f.
  w_TMPLVL = 0
  w_SOTTOALBERO = .f.
  w_DBCONGES = space(41)
  w_CLCODART = space(20)
  w_CLCODDIS = space(20)
  w_CLCODCIC = space(20)
  w_CLINDCIC = space(2)
  w_CLDESCIC = space(40)
  w_CLDESAGG = space(0)
  w_CLSERIAL = space(10)
  w_CCOPIA = space(1)
  w_DATRIF = ctod("  /  /  ")
  w_ARTFIN = space(20)
  w_DISTFIN = space(20)
  w_CODFIN = space(41)
  w_PREFIN = space(2)
  w_DESFIN = space(40)
  w_DAGFIN = space(0)
  w_MADATCIC = space(1)
  w_DATINI = ctod("  /  /  ")
  w_CLPRCOST = space(1)
  w_CLFLCMOD = space(1)
  w_CLNOTFAS = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONF_DIS_idx=0
  CONFDDIS_idx=0
  DISMBASE_idx=0
  DISTBASE_idx=0
  KEY_ARTI_idx=0
  TRADARTI_idx=0
  PAR_DISB_idx=0
  CONFDCAR_idx=0
  CONF_CAR_idx=0
  MODE_DIS_idx=0
  CONFSTAT_idx=0
  CONFMEMO_idx=0
  CONFTRAD_idx=0
  CONFTRDE_idx=0
  ART_TEMP_idx=0
  PAR_RIOR_idx=0
  TMPCONFDDIS_idx=0
  TMPCONFORA_idx=0
  TABMCICL_idx=0
  TMPEXPDB_idx=0
  ANA_CONF_idx=0
  ART_DIST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Configuratore Caratteristiche (da GSCR_KCC)
    * --- Dal padre
    * --- -- Variabili contenenti il nome dei cursore per consentire di aprire pi� istanze del configuratore --
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    * --- Locali...
    * --- Assegnamenti
    *     -----------------------
    this.w_car = padr(".\bmp\TreeCd_VDT.bmp",150)
    this.w_cardet = padr(".\bmp\deftree.bmp",150)
    this.w_carmod = padr(".\bmp\wf_conferma.bmp",150)
    this.w_cardes = padr(".\bmp\Fattura.bmp",150)
    this.w_modello = padr(".\bmp\calcola.bmp",150)
    this.w_modmod = padr(".\bmp\calcola_mod.bmp",150)
    this.w_descrizione = padr(".\bmp\CONTRATT.BMP",150)
    this.w_descrimod = padr(".\bmp\CONTRATT_MOD.BMP",150)
    * --- Variabili locali
    * --- Puntatore a Maschera Padre
    this.GSCR_KCC = this.oParentObject
    this.w_cCursor = this.GSCR_KCC.w_cCursor
    * --- Tree-view
    this.Albero = this.GSCR_KCC.w_Albero
    this.w_NODI = .NULL. && this.Albero.oTree
    this.w_TREEVIEW = this.GSCR_KCC.w_TREEVIEW
    * --- Tipo Esplosione
    this.w_SINGOLO = "N"
    this.w_FirstTime = .T.
    * --- Variabili VFox
    * --- Contiene il nome del cursore dell'esplosione distinta base
    EXPLDB = this.oParentObject.w_cExpldb
    * --- Caratteristiche descrittive
    cCursD = space(10)
    * --- Elenco codici caratteristiche
    cCursM = this.oParentObject.w_cCursM
    * --- Caratteristiche parametriche
    Modelli = this.GSCR_KCC.w_Modelli
    cInitCur = this.GSCR_KCC.w_cInitCur
    cRispDoc = this.oParentObject.w_cRispDoc
    cCheckCur = this.oParentObject.w_cCheckCur
    cTestInit = this.oParentObject.w_cTestInit
    * --- Nomi dei cursori definiti in maschera
    this.w_EXPLDB = EXPLDB
    cCursD = this.Albero.cCursor
    cCarParam = this.GSCR_KCC.w_cCarParam
    Fisse = this.GSCR_KCC.w_Fisse
    cFisse = this.GSCR_KCC.w_cFisse
    cModelli = this.GSCR_KCC.w_cModelli
    Descrit = this.GSCR_KCC.w_Descrit
    cDescrit = this.GSCR_KCC.w_cDescrit
    cCursZ = this.GSCR_KCC.w_cCursZ
    cCarParam = this.GSCR_KCC.w_cCarParam
    cCursTree = this.oParentObject.w_cCursTree
    cCursBmp = this.oParentObject.w_cCursBmp
    cCursCONF_DIS = this.oParentObject.w_cCursCONF_DIS
    cCursCONFDDIS = this.oParentObject.w_cCursCONFDDIS
    if this.w_OPER="VISUALIZZA" 
      use in select(EXPLDB)
      use in select(this.w_cCursor)
      use in select(cCursM)
      use in select(cInitcur)
      use in select(cTestInit)
      use in select(Modelli)
      Use in Select(Fisse)
      Use in Select(cFisse)
      Use in Select(Modelli)
      Use in Select(cModelli)
      Use in Select(Descrit)
      Use in Select(cDescrit)
      Use in Select(cCursZ)
      Use in Select(cCarParam)
      use in select(cCursTree)
      use in select(cCursBmp)
      if used(cRispDoc) AND this.oParentObject.w_RIC<>"S"
        use in select(cRispDoc)
      endif
      this.w_TREEVIEW.cCursor = cCursTree
      this.w_SCEDEFA = SPACE(200)
      this.oParentObject.w_NUMCAR = 0
      this.oParentObject.w_Fine = .F.
    endif
    do case
      case this.w_OPER = "SottoAlbero"
        Select (cCursD)
        this.w_nTMP = RecNo(cCursD)
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        * --- --Riempire il cursore relativo al sottoalbero contenente i componenti da configurare
        SELECT (this.w_cCursor)
        GO TOP
        COUNT FOR CCTIPOCA="D" AND LIVELLO = this.oParentObject.w_LEVEL TO this.w_NUMCADES
        SELECT (this.w_cCursor)
        GO TOP
        COUNT FOR CCTIPOCA<>"D" AND LIVELLO = this.oParentObject.w_LEVEL TO this.w_NUMCNDES
        if this.w_NUMCADES > 0 AND this.w_NUMCNDES = 0
          Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello = this.oParentObject.w_LEVEL and livcursm > 0 having CARSCE=CARTOT into Cursor Scelte
        else
          Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello = this.oParentObject.w_LEVEL and CCTIPOCA<>"D" and livcursm > 0 having CARSCE=CARTOT into Cursor Scelte
        endif
        if reccount("Scelte")<>0
          this.w_TOTEXP = "S"
          if USED(cInitCur)
            this.Page_17()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          select (this.w_cCursor)
          GO TOP 
 SCAN for rsel=0 and NVL(CCTIPOCA,"D")<>"D" and !EMPTY(CCTIPOCA)
          if !(alltrim(NVL(DESCRITV,"")) $ this.w_MSG)
            this.w_MSG = this.w_MSG+chr(13)+alltrim(NVL(DESCRITV,""))
          endif
          ENDSCAN
        endif
      case this.w_OPER = "VISUALIZZA"
        this.oParentObject.w_CCCONFIG = ""
        this.GSCR_KCC.NotifyEvent("Interroga")     
        this.GSCR_KCC.mCalc(True)     
        cCursD = this.Albero.cCursor
        SELECT(cCursD)
        INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
        INDEX ON LVLKEYTV TAG LVLKEYTV COLLATE "MACHINE"
        SET ORDER TO LVLKEYTV
        if !Empty(this.oParentObject.w_ARCODART) AND !EMPTY(this.oParentObject.w_DBCODICE)
          * --- --calcola  le caratteristiche associate alla distinta e le memorizza in un cursore
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_FLINIT="L" or (this.oParentObject.w_FLINIT $ "C-M" and !Empty(this.oParentObject.w_CCSERIAL1)) or (this.oParentObject.w_RIC="S" and !Empty(this.oParentObject.w_CCSERIAL))
            if this.oParentObject.w_RIC="S" and !Empty(this.oParentObject.w_CCSERIAL)
              vq_exec("..\CCAR\exe\query\GSCR14bc41", this, cInitCur)
            else
              do case
                case this.oParentObject.w_FLINIT="L"
                  * --- --calcola  le caratteristiche associate alla distinta e le memorizza in un cursore
                  * --- --Inserisce primo livello
                  vq_exec("..\CCAR\exe\query\GSCR14bc5", this, cCheckCur)
                  select cctipoca,cccodice,count(cccodice) as counter where ccflinit="S" from (cCheckCur) ; 
 group by 1,2 into cursor (cTestInit)
                  if USED(cTestInit)
                    Select (cTestInit) 
 GO TOP 
 SCAN FOR counter>1
                    this.w_MSG = iif(EMPTY(this.w_MSG),"",", ")+ALLTRIM(CCCODICE)
                    ENDSCAN
                    if !EMPTY(this.w_MSG)
                      ah_errormsg("Per le seguenti caratteristiche sono presenti pi� di un codice di inzializzazione.%0Verificare l'intera catena della distinta.%0Caratteristiche: %1%0%0(Verr� disabilitato il flag inizializzazione rendendo possibile la configurazione)", 48,"",alltrim(this.w_MSG))
                      this.oParentObject.w_FLINIT = "N"
                      this.GSCR_KCC.NotifyEvent("Visualizza")     
                      i_retcode = 'stop'
                      return
                    endif
                  endif
                  use in select(cCheckCur)
                  vq_exec("..\CCAR\exe\query\GSCR14bc4", this, cInitCur)
                case this.oParentObject.w_FLINIT $ "C-M"
                  vq_exec("..\CCAR\exe\query\GSCR14bc41", this, cInitCur)
                  * --- Cursore di init delle caratteristiche parametriche
                  CINITCURM = SYS(2015)
                  USE IN SELECT(CINITCURM)
                  VQ_EXEC("..\CCAR\EXE\QUERY\GSCR14BC43", This, CINITCURM)
                  SELECT(CINITCURM)
                  GO TOP
                  SCAN
                  SCATTER MEMVAR MEMO
                  if m.cctipoca = "M"
                    w_s = "01"
                    this.w_i = 1
                    this.w_NEWVAL = ""
                    do while this.w_i <= 15
                      this.w_VAR_i = Alltrim(nvl(&CINITCURM..CCVAR1&w_s,""))
                      this.w_VAL_i = NVL(&CINITCURM..CCQTA1&w_s,0)
                      if not empty(this.w_VAR_i)
                        this.w_NEWVAL = this.w_NEWVAL + this.w_VAR_i + "=" + Alltrim(str(this.w_VAL_i,10,3))+":"
                        SELECT(cInitCur)
                        APPEND BLANK
                        CPROWORD = this.w_VAL_i 
 CCDETTAG = this.w_VAR_i 
 CCFLINIT="S" 
 CC__DEFA="S" 
 CC_COEFF=0
                        GATHER MEMVAR MEMO
                      endif
                      this.w_i = this.w_i + 1
                      w_s = padl(alltrim(str(this.w_i)), 2, "0")
                    enddo
                  else
                  endif
                  SELECT(CINITCURM)
                  ENDSCAN
                  USE IN SELECT(CINITCURM)
                  * --- Cursore di init delle caratteristiche descrittive
                  this.w_CINITCURD = SYS(2015)
                  USE IN SELECT(this.w_CINITCURD)
                otherwise
                  * --- Non faccio niente
              endcase
            endif
            this.Page_11()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if used(Modelli) and this.oParentObject.w_RIC<>"S"
              Select(Modelli) 
 zap
            endif
            SELECT(this.w_cCursor)
            ZAP
            APPEND BLANK
            REPLACE CC__DIBA WITH this.oParentObject.w_DBCODICE, LVLKEY WITH "AAA", LIVELLO WITH 0, LIVCURSM WITH 0
            if this.oParentObject.w_RIC="S"
              Select (cCursM)
              scan for rsel=1
              cod=NVL(CCCODICE,"")
              tipo=NVL(CCTIPOCA,"")
              Update (this.w_cCursor) set rsel=1 where cccodice=cod and cctipoca=tipo
              endscan
            endif
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_SOTTOALBERO = .T.
            this.Page_16()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Delete from ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                   )
          else
            delete from (i_cTable) where;
                  CAKEYRIF = this.oParentObject.w_KEYRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          if !this.oParentObject.w_DURINGINIT
            this.GSCR_KCC.LockScreen = .T.
            * --- Controllo che caratteristiche devo caricare
            SELECT(cCursD)
            GO TOP
            this.Albero.Grd.Refresh()     
            this.GSCR_KCC.__dummy__.Enabled = .t.
            this.GSCR_KCC.__dummy__.SetFocus()     
            this.GSCR_KCC.__dummy__.Enabled = .f.
            this.GSCR_KCC.LockScreen = .F.
            this.Albero.Grd.SetFocus()     
            this.Albero.Grd.Refresh()     
          else
            this.oParentObject.w_DURINGINIT = !this.oParentObject.w_DURINGINIT
          endif
        endif
        if !EMPTY(this.w_MSG)
          ah_errormsg("Per confermare il livello occorre specificare le seguenti caratteristiche: %1", 48,"",alltrim(this.w_MSG))
        endif
      case this.w_OPER = "EXPNODE"
        * --- --Esplosione Singolo Nodo
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER = "ROWSEL"
        this.GSCR_KCC.LockScreen = .T.
        this.w_nRec = 0
        * --- Aggiorna la testata con la selezione impostata
        * --- --vedo se la risposta data in precedenza � diversa 
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        this.w_lvlkey = this.oParentObject.w_LVLKEYTV
        this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        Select (cCursD)
        this.w_nTMP = RecNo(cCursD)
        this.w_LVLKEYP = LVLKEYP
        this.w_lvlkey = LVLKEYP
        SET FILTER TO
        Select (cCursD)
        GO TOP
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.L_CCDETTAG = NVL(CCDETTAG,"")
        do case
          case this.oParentObject.w_CCTIPOCA="C"
            * --- --Le scelte vengono riportate nel dettaglio
            * --- --controlla se sto cambiando una scelta di quel livello
            update (cCursM) set scelta = 1 where ("."+alltrim(str(this.oParentObject.w_rowsel))+"." $ alltrim(ccdettag)) and this.w_cccodice=cccodice and this.oParentObject.w_cctipoca=cctipoca
            update (cCursM) set canc=1 where !("."+alltrim(str(this.oParentObject.w_rowsel))+"." $ alltrim(ccdettag)) and this.w_cccodice=cccodice and this.oParentObject.w_cctipoca=cctipoca and livello=this.w_livello
            * --- --Riapro le scelte precedentemente fatte
            select distinct lvlkey1, ccdettag, cccodice, cctipoca from (cCursM) into cursor TMPCursM where LIVELLO=this.w_livello and ; 
 ("."+alltrim(str(this.oParentObject.w_rowsel))+"." $ alltrim(ccdettag) and this.w_cccodice=cccodice and this.oParentObject.w_cctipoca=cctipoca)
            * --- Riapre i nodi padri del nodo corrente scelto
            Update (cCursM) set CANC=0 from TMPCursM where alltrim(TMPCursM.lvlkey1) $ alltrim(&cCursM..lvlkey1)
            * --- Riapro tutti i figli
            SELECT DISTINCT ALLTRIM(A.LVLKEY1) as lvlkey1 FROM (cCursM) A into cursor TMPCursM1 WHERE CANC=0
            Update (cCursM) set CANC=0 from TMPCursM1 where alltrim(TMPCursM1.lvlkey1) $ alltrim(&cCursM..lvlkey1)
            use in Select("TMPCursM1")
            use in Select("TMPCursM")
            * --- --Le scelte vengono riportate nel dettaglio
            Select (cCursD)
            Update (cCursD) set xchk=0 where CPROWORD<>this.oParentObject.w_ROWSEL AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            Update (cCursD) set xchk=1 where CPROWORD=this.oParentObject.w_ROWSEL AND ccheader<>"S" AND lvlkey = this.w_lvlkeycur
            Select (cCursD)
            =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            Select (this.w_cCursor)
            if SEEK(this.w_LVLKEYP)
              Replace CPROWORD with this.oParentObject.w_ROWSEL, CCDETTAG with this.L_CCDETTAG, RSEL with 1
            endif
            Select (cCursD)
            =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            this.w_NEWVAL = Alltrim(Str(this.oParentObject.w_ROWSEL))
            * --- --Controllo inserimento-disinserimento foglie albero caratteristiche
            Select SUM(SCELTA) as CARSCE, COUNT(SCELTA) as CARTOT,SUM(CANC) as CAN, LVLKEY1 AS CHIAVE from (cCursM) group by LVLKEY1; 
 where livello=this.w_livello having CARSCE<=CARTOT AND CARSCE>0 AND CAN=0 into Cursor Scelte
            * --- --Controllo a quali nodi appartiene la scelta che io sto effettuando e che posso percorrere
            SELECT Alltrim(lvlkey1) as nodo from (cCursM) where CCCODICE=this.w_CCCODICE and CCTIPOCA=this.oParentObject.w_CCTIPOCA; 
 AND CANC<>1 and LIVELLO=this.w_livello into cursor Nodi
            Select Scelte.* from Scelte Inner Join Nodi on Chiave=Nodo into cursor Scelte
            if reccount("Scelte")>0
              * --- --Occorre tenere valide anche le scelte associate ai nodi fratelli che potrebbero ancora essere scelte
              * --- --alltrim(lvlkey1)=alltrim(chiave) AND
              Select distinct &cCursM..cccodice, &cCursM..cctipoca,&cCursM..livello from (cCursM) where livello=this.w_livello and canc=0 into cursor SceOk
              * --- --Si selezionano le caratteristiche da cancellare
              Select Distinct &cCursM..cccodice,&cCursM..cctipoca,&cCursM..livello,0 as Cancello from (cCursM) where LIVELLO=this.w_livello AND; 
 ALLTRIM(&cCursM..cccodice)+ALLTRIM(&cCursM..cctipoca)+ALLTRIM(str(&cCursM..livello)) NOT IN; 
 (select ALLTRIM(SceOk.cccodice)+ALLTRIM(SceOk.cctipoca)+ALLTRIM(str(SceOK.livello)) as codice FROM SceOk) INTO CURSOR CarCanc
              wrcursor("CarCanc")
              this.w_AGGIORNATE = 0
              if reccount("CarCanc")>0
                this.w_trovato = .F.
                do while !(this.w_trovato)
                  Select CarCanc 
 go top
                  scan for cancello=0
                  scatter memvar
                  Select (this.w_cCursor) 
 go bottom
                  if livcursm = m.livello and cccodice=m.cccodice and cctipoca=m.cctipoca
                    scatter memvar
                    this.w_LVLKEYVER = LVLKEY
                    this.w_TMPNC = Recno (this.w_cCursor) 
                    DELETE FOR LVLKEYP = this.w_LVLKEYVER AND LIVELLO > 0
                    this.w_AGGIORNATE = this.w_AGGIORNATE + _TALLY
                    SELECT(cCursD)
                    DELETE FOR LVLKEYP = this.w_LVLKEYVER AND CCHDRLVL<>"S"
                    =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
                    Select (this.w_cCursor) 
                    GO this.w_TMPNC
                  endif
                  endscan
                  update CarCanc set cancello=1 where cccodice=m.cccodice and cctipoca=m.cctipoca
                  if Not Eof()
                    Skip
                  endif
                  Select count(cancello) as Tot, sum(cancello) as Visitati from CarCanc having Tot=Visitati into curso Canc
                  if reccount("canc")>0
                    this.w_trovato = .T.
                  endif
                enddo
                * --- --Posso cancellare la caratteristica
                * --- --vengono anche cancellate dal cursore delle scelte
                Select CarCanc 
 go top
                scan 
                scatter memvar
                Select (cCursM)
                replace canc with 1, scelta with 0 for m.cccodice=cccodice and m.cctipoca=cctipoca and m.livello=livello
                endscan
              endif
              SELECT(cCursD)
              =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            endif
            * --- --Ho effettuato tutte le scelte per quel livello posso cancellare le caratteristiche 
            *     non associate a quel componete che sto configurando
            use in select("CarCanc")
            use in select("SceOk")
            use in select("Scelte")
            use in select("Nodi")
            use in select("Canc")
            Select (cCursD)
            =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            this.oParentObject.w_CCAMBIO = .T.
          case this.oParentObject.w_CCTIPOCA="D"
            Select (cCursD)
            this.w_CCROWORD = this.GSCR_KCC.w_CCROWORD
            this.w_CCCOMPON = this.GSCR_KCC.w_CCCOMPON
            this.w_CC__DIBA = this.GSCR_KCC.w_CC__DIBA
            this.oParentObject.w_CC__NOTE = this.L_CCDETTAG
            this.Page_14("VERIFICA",this.oParentObject.w_CCTIPOCA,this.w_CCCODICE,this.w_CC__DIBA,this.w_CCCOMPON,this.w_CCROWORD,this.w_LVLKEYP)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            UPDATE (Modelli) SET CCFORMUL="OK", CCDETTAG=this.oParentObject.w_CC__NOTE WHERE CCCODICE=this.w_CCCODICE and CCTIPOCA=this.oParentObject.w_CCTIPOCA
            Select (this.w_cCursor)
            if SEEK(this.w_LVLKEYP)
              this.oParentObject.w_CCDETTAG = alltrim(nvl(this.oParentObject.w_CC__NOTE, ""))
              Replace CCFORMUL with "OK", CCDETTAG with this.oParentObject.w_CCDETTAG, RSEL with 1 for cccodice=this.w_CCCODICE and CCTIPOCA="D"
            endif
        endcase
        if !this.oParentObject.w_CCAMBIO
          SELECT(cCursD)
          SET FILTER TO SHOWNODE="S" AND EXPANDED="S"
        endif
        this.GSCR_KCC.LockScreen = .T.
        SELECT(cCursD)
        GO TOP
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        if !this.oParentObject.w_DURINGINIT
          if !this.oParentObject.w_CCAMBIO
            if this.w_nRec > 0
              this.Albero.Grd.Refresh()     
            endif
            this.GSCR_KCC.LockScreen = .F.
            if this.w_nRec > 0
              this.GSCR_KCC.__dummy__.Enabled = .t.
              this.GSCR_KCC.__dummy__.SetFocus()     
              this.GSCR_KCC.__dummy__.Enabled = .f.
              SELECT(cCursD)
            endif
          endif
        else
          this.GSCR_KCC.SetControlsValue()     
          This.bUpdateParentObject = False
        endif
      case this.w_OPER = "UNROWSEL"
        * --- --occorre cancellare la scelta anche dall'albero LEGATA a quella caratteristica
        this.GSCR_KCC.LockScreen = .T.
        Select (cCursD)
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_nTMP = RecNo(cCursD)
        this.w_LVLKEYP = LVLKEYP
        this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        do case
          case this.oParentObject.w_CCTIPOCA="C"
            Select (this.w_cCursor)
            if SEEK(this.w_LVLKEYP)
              Replace CPROWORD with 99999-99999, CCDETTAG with SPACE(40), RSEL with 0
            endif
            UPDATE (cCursM) set scelta=0 where cccodice=this.w_cccodice and cctipoca=this.oParentObject.w_cctipoca 
            if USED(cInitCur)
              UPDATE (cInitCur) SET CCFLINIT="N" WHERE CCCODICE=this.w_CCCODICE and CCTIPOCA=this.oParentObject.w_CCTIPOCA AND CPROWORD=this.oParentObject.w_ROWSEL
            endif
            Select (cCursM) 
 GO TOP
            SCAN FOR LIVELLO=this.w_livello
            this.w_RIGA = recno()
            if this.w_cccodice=cccodice and this.oParentObject.w_cctipoca=cctipoca 
              * --- --alltrim(str(w_rowsel)) $ alltrim(ccdettag)  and 
              this.w_chiave = ALLTRIM(LVLKEY1)
              replace CANC with 0 for this.w_chiave $ alltrim(lvlkey1) 
              GO this.w_riga
            endif
            Select (cCursM)
            ENDSCAN
            Update (cCursD) set xchk=0 where CPROWORD<>this.oParentObject.w_ROWSEL AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            UPDATE (cCursM) set canc=1 where LIVELLO>this.w_livello
            Select (cCursD)
            =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            this.oParentObject.w_CCAMBIO = .T.
          case this.oParentObject.w_CCTIPOCA="D"
            Delete from (Modelli) where cccodice=this.w_CCCODICE and cctipoca=this.oParentObject.w_CCTIPOCA
            Select (this.w_cCursor)
            if SEEK(this.w_LVLKEYP)
              Replace CPROWORD with 99999-99999, CCDETTAG with SPACE(40), RSEL with 0, ccformul with ""
            endif
            UPDATE (cCursM) set scelta=0 where cccodice=this.w_cccodice and cctipoca=this.oParentObject.w_cctipoca 
        endcase
        this.GSCR_KCC.LockScreen = .T.
        SELECT(cCursD)
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        if !this.oParentObject.w_DURINGINIT
          this.Albero.Grd.Refresh()     
          this.GSCR_KCC.__dummy__.Enabled = .t.
          this.GSCR_KCC.__dummy__.SetFocus()     
          this.GSCR_KCC.__dummy__.Enabled = .f.
          this.GSCR_KCC.LockScreen = .F.
          this.Albero.Grd.SetFocus()     
        else
          this.GSCR_KCC.SetControlsValue()     
          This.bUpdateParentObject = False
        endif
      case this.w_OPER = "CAMBIO"
        this.GSCR_KCC.LockScreen = .T.
        Select (cCursD)
        this.w_nTMP = RecNo(cCursD)
        * --- --Cambio della scelta associata alla caratteristica
        *     occorre rinconfigurare l'albero delle scelte:
        *     1-eliminare le scelte precedetemente fatte da quel livello in poi
        *     2-impoldere l'albero fino a quel livello
        * --- --Mi posiziono sul livello superiore 
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_CANCLIV = .F.
        this.w_lvlk = ALLTRIM(SUBSTR(this.oParentObject.w_LVLKEYTV,1, LEN(ALLTRIM(this.oParentObject.w_LVLKEYTV))-4))
        this.w_nNODO = "k"+alltrim(this.w_lvlk)
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        * --- Sono state effettuate scelte tutte le risposte per quel livello.
        if this.oParentObject.w_CCAMBIO
          this.w_CARUPDATED = 0
          Select (this.w_cCursor) 
 go bottom
          do while livcursm>this.w_livello and !BOF()
            * --- --occorre cancellare tutto il sottoalbero e ricalcolare le scelte
            scatter memvar
            this.w_LVLKEYVER = m.lvlkey
            this.w_TMPNC = Recno (this.w_cCursor) 
            DELETE
            this.w_CARUPDATED = this.w_CARUPDATED + _TALLY
            if m.cctipoca<>"C" and Used(Modelli)
              * --- Parametriche e descrittive
              SELECT(Modelli)
              if m.cctipoca="M"
                DELETE FOR CCTIPOCA = m.CCTIPOCA AND CCCODICE = m.CCCODICE AND CC__DIBA = m.CC__DIBA AND CCROWORD = m.CCROWORD AND CCCOMPON = m.CCCOMPON
              else
                DELETE FOR LVLKEYP = m.lvlkeytv and cccodice=m.cccodice and cctipoca=m.cctipoca
              endif
            endif
            SELECT(cCursD)
            DELETE FOR LVLKEYP = this.w_LVLKEYVER AND CCHDRLVL<>"S"
            =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
            Select (this.w_cCursor) 
            GO this.w_TMPNC
            skip-1
          enddo
          Select (cCursD)
          =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
          this.oParentObject.w_CCAMBIO = .F.
        endif
        this.GSCR_KCC.LockScreen = .T.
        SELECT(cCursD)
        SET FILTER TO SHOWNODE="S" && AND EXPANDED="S"
        SELECT(cCursD)
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        if !this.oParentObject.w_DURINGINIT
          this.w_SOTTOALBERO = .T.
          this.Page_16()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT(cCursD)
          =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
          this.Albero.Grd.Refresh()     
          this.GSCR_KCC.LockScreen = .F.
          this.Albero.Grd.SetFocus()     
          this.GSCR_KCC.__dummy__.Enabled = .t.
          this.GSCR_KCC.__dummy__.SetFocus()     
          this.GSCR_KCC.__dummy__.Enabled = .f.
          this.GSCR_KCC.NotifyEvent("AggiornaTV")     
          this.w_TREEVIEW.ExpandAll(.T.)
        else
          this.GSCR_KCC.SetControlsValue()     
          This.bUpdateParentObject = False
        endif
      case this.w_OPER = "UPDSEL"
        * --- Aggiorna il dettaglio sulle impostazioni fatte
        do case
          case this.oParentObject.w_CCTIPOCA="C"
            Select (cCursM)
            Scatter memvar memo
            if M.CPROWORD>0
              Select (cCursD)
              Update (cCursD) set xchk=0 where ccheader<>"S" AND lvlkeyp = m.lvlkey1
              Update (cCursD) set xchk=1 where CPROWORD=m.CPROWORD AND ccheader<>"S" AND lvlkeyp = m.lvlkey1
              Select (cCursD)
              locate for xchk=1 AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
              if found()
                this.oParentObject.w_CCDETTAG = NVL(CCDETTAG,"")
              endif
            endif
          case this.oParentObject.w_CCTIPOCA="M"
            this.Page_13()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_UPDSTR = False
            this.Page_14("CALCOLA",this.oParentObject.w_CCTIPOCA,this.w_CCCODICE,this.w_CC__DIBA,this.w_CCCOMPON,this.w_CCROWORD,this.w_LVLKEYP)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if !this.w_UPDSTR
              if this.oParentObject.w_FLINIT<>"N"
                Select (cCursM)
                Replace RSEL with 1 FOR cctipoca=this.oParentObject.w_CCTIPOCA and cccodice= this.w_CCCODICE
              endif
            endif
          case this.oParentObject.w_CCTIPOCA="D"
            this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        endcase
        if USED(cInitCur) and used(cCursM)
          Select (cInitCur) 
 SCAN FOR CCFLINIT="S" 
 l_TIPOCA=CCTIPOCA 
 l_CARAT=CCCODICE 
 l_ROW=CPROWORD 
 l_DETTAG=CCDETTAG 
 Select (cCursM) 
 LOCATE FOR CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and RSEL=1
          if not Found()
            UPDATE (cCursM) set RSEL=1, cproword=l_ROW, ccdettag=l_DETTAG where CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and RSEL<>1
            if this.oParentObject.w_FLINIT<>"N"
              Select (cCursD) 
 LOCATE FOR XCHK=1
              if not Found()
                Update (cCursD) set xchk=1 where CCFLINIT="S"
              endif
            endif
          endif
          ENDSCAN
        endif
        * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
        * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
        this.oParentObject.setControlsValue()
        this.bUpdateParentObject = False
      case this.w_OPER = "CREADIS" or this.w_OPER = "STAMPA"
        * --- Nel caso di creazione devo verificare se un altro utente sta generando e aspetto che abbia finito
        if !Empty(this.oParentObject.w_ARCODART) AND !EMPTY(this.oParentObject.w_DBCODICE)
          if !Used (this.w_cCursor)
            * --- Verifico che l'utente abbia esploso la distinta base
            this.GSCR_KCC.NotifyEvent("Visualizza")     
          endif
          if this.w_OPER = "CREADIS"
            this.w_TTENTATIVI = 10
            this.w_TENTATIVI = 0
            this.w_PDBLCFGC = "S"
            this.w_GENERADIBA = .F.
            this.w_ROWS = 0
            this.w_CFGINI = SECONDS()
            * --- Write into PAR_DISB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_DISB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC(this.w_PDBLCFGC),'PAR_DISB','PDBLCFGC');
                  +i_ccchkf ;
              +" where ";
                  +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                  +" and PDBLCFGC = "+cp_ToStrODBC("N");
                     )
            else
              update (i_cTable) set;
                  PDBLCFGC = this.w_PDBLCFGC;
                  &i_ccchkf. ;
               where;
                  PDCODAZI = i_CODAZI;
                  and PDBLCFGC = "N";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_ROWS = i_ROWS
            if this.w_ROWS > 0
              this.w_GENERADIBA = .T.
              this.w_TENTATIVI = this.w_TTENTATIVI
            endif
            do while this.w_ROWS = 0 Or this.w_TENTATIVI < this.w_TTENTATIVI
              this.w_CFGFIN = SECONDS() - this.w_CFGINI
              if this.w_CFGFIN >= 3
                ah_msg("Altra configurazione in corso, attendere ...")
                * --- Write into PAR_DISB
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_DISB_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC(this.w_PDBLCFGC),'PAR_DISB','PDBLCFGC');
                      +i_ccchkf ;
                  +" where ";
                      +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                      +" and PDBLCFGC = "+cp_ToStrODBC("N");
                         )
                else
                  update (i_cTable) set;
                      PDBLCFGC = this.w_PDBLCFGC;
                      &i_ccchkf. ;
                   where;
                      PDCODAZI = i_CODAZI;
                      and PDBLCFGC = "N";

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.w_ROWS = i_ROWS
                this.w_CFGINI = SECONDS()
                this.w_TENTATIVI = this.w_TENTATIVI + 1
              endif
              if this.w_ROWS > 0
                this.w_GENERADIBA = .T.
                exit
              else
                if this.w_TENTATIVI >= this.w_TTENTATIVI
                  this.w_GENERADIBA = .F.
                  exit
                endif
              endif
            enddo
            if this.w_GENERADIBA
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Write into PAR_DISB
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_DISB_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
                    +i_ccchkf ;
                +" where ";
                    +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                       )
              else
                update (i_cTable) set;
                    PDBLCFGC = "N";
                    &i_ccchkf. ;
                 where;
                    PDCODAZI = i_CODAZI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              ah_errormsg("Verificare blocco configuratore e riprovare",48)
            endif
          else
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          ah_errormsg("Inserire un codcie articolo",48)
        endif
      case left(this.w_OPER,7) = "w_CCQTA"
        * --- Aggiorna Cursore Modelli Parametrici
        this.GSCR_KCC.LockScreen = .T.
        Select (cCursD)
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_nTMP = RecNo(cCursD)
        this.w_LVLKEY = this.oParentObject.w_LVLKEYTV
        this.w_LVLKEYP = LVLKEYP
        this.w_CPROWORD = NVL(CPROWORD, 0)
        this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        this.w_trovato = SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.w_CCROWORD = this.GSCR_KCC.w_CCROWORD
        this.w_CCCOMPON = this.GSCR_KCC.w_CCCOMPON
        this.w_CC__DIBA = this.GSCR_KCC.w_CC__DIBA
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        this.Page_14("VERIFICA",this.oParentObject.w_CCTIPOCA,this.w_CCCODICE,this.w_CC__DIBA,this.w_CCCOMPON,this.w_CCROWORD,this.w_LVLKEYP)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT(this.w_cCursor)
        GO TOP
        if SEEK(this.w_LVLKEYP)
          Select (Modelli)
          LOCATE FOR CCTIPOCA = this.oParentObject.w_CCTIPOCA AND CCCODICE = this.w_CCCODICE AND ALLTRIM(CCDETTAG) = ALLTRIM(this.oParentObject.w_CCDETTAG)
          if FOUND()
            REPLACE CPROWORD WITH this.w_CPROWORD FOR CCTIPOCA = this.oParentObject.w_CCTIPOCA AND CCCODICE = this.w_CCCODICE AND ALLTRIM(CCDETTAG) = ALLTRIM(this.oParentObject.w_CCDETTAG)
          endif
          this.Page_14("SOLOCALCOLO",this.oParentObject.w_CCTIPOCA,this.w_CCCODICE,this.w_CC__DIBA,this.w_CCCOMPON,this.w_CCROWORD,this.w_LVLKEYP)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_UPDSTR
            this.w_NEWVAL = Left(this.w_NEWVAL, len(this.w_NEWVAL)-1)
            Select (this.w_cCursor)
            REPLACE ccformul with this.L_CCFORMUL, ccdettag WITH padr(this.w_NEWVAL,40), rsel with 1, cproword with this.w_CPROWORD, ERRORE with this.w_ERRORE, msgerr with this.w_msgerrore
            Select (cCursM)
            if Eof(cCursM)
              this.w_nTMP = RecNo(cCursM) - 1
            else
              this.w_nTMP = RecNo(cCursM)
            endif
            GO TOP
            REPLACE SCELTA with 1, RSEL WITH 1, CPROWORD with Round(this.w_CPROWORD, 3), CCDETTAG WITH "."+ALLTRIM(STR(Round(this.w_CPROWORD,3)))+"." FOR CCTIPOCA="M" AND CCCODICE= this.w_CCCODICE
            Select (cCursM)
            GO this.w_nTMP
          endif
        endif
        Select (cCursD)
        this.w_trovato = SEEK(this.w_LVLKEYP , (cCursD) , "LVLKEY")
        if this.w_trovato
          REPLACE ccformul with this.L_CCFORMUL, ccdettag WITH padr(this.w_NEWVAL,40)
        endif
        this.w_trovato = SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.GSCR_KCC.LockScreen = .F.
        if !this.oParentObject.w_DURINGINIT
          this.GSCR_KCC.NotifyEvent("AggiornaTV")     
          this.w_TREEVIEW.ExpandAll(.T.)
        endif
      case this.w_OPER = "BLANK"
        * --- Se chiamato dai Doc. inizializza variabili
        if type("this.GSCR_KCC.oParentObject.class")="C" and "GSOR_MDV" $ upper(this.GSCR_KCC.oParentObject.class)
          this.GSOR_MDV = this.GSCR_KCC.oParentObject
          this.oParentObject.w_ARCODART = this.GSOR_MDV.w_MVCODART
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODDIS,ARTIPART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODDIS,ARTIPART;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_ARCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DBCODICE = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
            this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_cCursor = sys(2015)
          this.Albero.cCursor = this.w_cCursor
          this.w_SCEDEFA = SPACE(200)
          this.oParentObject.w_NUMCAR = 0
          this.oParentObject.w_CCCONFIG = ""
          this.GSCR_KCC.NotifyEvent("Interroga")     
          this.GSCR_KCC.mCalc(True)     
          this.GSCR_KCC.NotifyEvent("Visualizza")     
          this.w_oTMP = this.GSCR_KCC.GetCtrl("w_DBCODICE")
          this.w_oTMP.Enabled = False
          this.oParentObject.w_DADOC = True
          this.oParentObject.w_TIPGEN = "C"
        endif
        USE IN SELECT(cCursBmp)
        CREATE CURSOR (cCursBmp) (ARTIPART C(2), BMPNAME C(200))
        if used(cCursBmp)
          Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("PF" , "BMP\Tree1.ico")
          Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("SE" , "BMP\Tree4.ico")
          Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("MP" , "BMP\Tree2.ico")
          Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("PH" , "BMP\Tree5.ico")
          Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("CC" , "BMP\applica.ico")
        endif
      case this.w_OPER="CLOSE"
        this.bUpdateParentObject = False
        this.w_TREEVIEW.OTREE.Nodes.Clear()     
        if used(this.w_cCursor)
          * --- Cancello elementi dell'albero
          Select (this.w_cCursor) 
 delete all
          this.GSCR_KCC.w_CCDESCRI = ""
        endif
        if not (this.oParentObject.w_DADOC)
          use in select(cRispDoc)
        endif
        * --- Chiusura Cursori
        use in select("SupDB")
        use in select("sup")
        use in select("__bcp__")
        use in select(Modelli)
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.oParentObject.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if !EMPTY(this.oParentObject.w_KEYRIF1)
          * --- Delete from ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF1);
                   )
          else
            delete from (i_cTable) where;
                  CAKEYRIF = this.oParentObject.w_KEYRIF1;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        Use in Select(Fisse)
        Use in Select(cFisse)
        Use in Select(Modelli)
        Use in Select(cModelli)
        Use in Select(Descrit)
        Use in Select(cDescrit)
        Use in Select(cCursZ)
        Use in Select(this.w_cCursor)
        Use in Select(cCarParam)
        Use in Select("cCarValide")
        Use in Select(cCursCONF_DIS)
        Use in Select(cCursCONFDDIS)
        * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        use in select("Scelte")
        use in select(EXPLDB)
        use in select("EXPLDB1")
        use in select("EXPLALL")
        use in select(cCursTree)
        use in select("cursor")
        use in select(cCursBmp)
        use in select(cCursM)
        use in select("__tmp__")
        use in select(EXPLDB)
        use in select(this.w_cCursor)
        use in select(cInitcur)
        use in select(cTestInit)
        use in select(Modelli)
        * --- Drop temporary table TMPEXPDB
        i_nIdx=cp_GetTableDefIdx('TMPEXPDB')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPEXPDB')
        endif
        * --- Drop temporary table TMPCONFORA
        i_nIdx=cp_GetTableDefIdx('TMPCONFORA')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPCONFORA')
        endif
        * --- Drop temporary table TMPCONFDDIS
        i_nIdx=cp_GetTableDefIdx('TMPCONFDDIS')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPCONFDDIS')
        endif
        this.w_TREEVIEW = .NULL.
        this.Albero = .NULL.
        this.w_NODI = .NULL.
        this.GSCR_KCC = .NULL.
      case this.w_OPER="AFTDET"
        * --- Devo simulare il flag sulle caratteristiche per le quali ho un valore di init
        Select (cCursD) 
 replace XCHK with 1 for CCFLINIT="S" 
 Select (cCursd) 
 locate for XCHK=1
        if found()
          this.oParentObject.NotifyEvent("w_ZOOM_DET row checked")
        endif
      case this.w_OPER="TROVANODO" 
        Select (cCursd)
        this.w_LVLKEY = LVLKEYTV
        LOCATE FOR CCTIPOCA = this.oParentObject.w_TIPONODO and CCCODICE = this.oParentObject.w_NODO and CCHEADER = "S"
        if Found()
          this.Albero.Grd.SetFocus()     
        else
          ah_ErrorMsg("Nessun elemento trovato",,"")
          SEEK(this.w_LVLKEY)
          this.Albero.Grd.SetFocus()     
        endif
      case this.w_OPER="TVDISTINTA"
        this.w_OK = .T.
        this.w_EXPLDB = cCursTree
        Select * from (EXPLDB) into cursor (this.w_EXPLDB) Where ARCONCAR<>"X" nofilter order by lvlkey readwrite
        SELECT(this.w_EXPLDB)
        INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
        this.Page_15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        UPDATE (cCursTree) SET CPBMPNAME = &cCursBmp..bmpname from (cCursBmp) where &cCursTree..artipart=&cCursBmp..artipart
      case this.w_OPER="OPENCARDESC"
        Select (cCursD)
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        this.w_lvlkey = this.oParentObject.w_LVLKEYTV
        this.w_CCCODICE = this.GSCR_KCC.w_CCCODICE
        Select (cCursD)
        this.w_nTMP = RecNo(cCursD)
        this.w_LVLKEYP = LVLKEYP
        this.w_lvlkey = LVLKEYP
        SET FILTER TO
        Select (cCursD)
        GO TOP
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.L_CCDETTAG = NVL(CCDETTAG,"")
        this.w_CC_NOTED = this.oParentObject.w_CC__NOTE
        do GSCRDKCC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONFKME
          Select (cCursD)
          replace CCDETTAG with this.w_CC_NOTED
          if XCHK=1
            Select (cCursD)
            this.w_CCROWORD = this.GSCR_KCC.w_CCROWORD
            this.w_CCCOMPON = this.GSCR_KCC.w_CCCOMPON
            this.w_CC__DIBA = this.GSCR_KCC.w_CC__DIBA
            this.oParentObject.w_CC__NOTE = this.w_CC_NOTED
            this.Page_14("VERIFICA",this.oParentObject.w_CCTIPOCA,this.w_CCCODICE,this.w_CC__DIBA,this.w_CCCOMPON,this.w_CCROWORD,this.w_LVLKEYP)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            UPDATE (Modelli) SET CCFORMUL="OK", CCDETTAG=this.oParentObject.w_CC__NOTE WHERE CCCODICE=this.w_CCCODICE and CCTIPOCA=this.oParentObject.w_CCTIPOCA
            Select (this.w_cCursor)
            if SEEK(this.w_LVLKEYP)
              this.oParentObject.w_CCDETTAG = alltrim(nvl(this.oParentObject.w_CC__NOTE, ""))
              Replace CCFORMUL with "OK", CCDETTAG with this.oParentObject.w_CCDETTAG, RSEL with 1 for cccodice=this.w_CCCODICE and CCTIPOCA="D"
            endif
          endif
        endif
    endcase
    this.w_TREEVIEW = .NULL.
    this.Albero = .NULL.
    this.w_NODI = .NULL.
    this.GSCR_KCC = .NULL.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CREAZIONE DISTINTA CONFIGURATA
    * --- Generazione Distinte e codici di ricerca
    Select (EXPLDB) 
 locate for ARTIPART=="CC" and CACONCAR="S"
    if found()
      ah_errormsg("Impossibile creare la distinta configurata,%0Alcuni componenti configurabili non sono del tipo atteso.",16)
      i_retcode = 'stop'
      return
    endif
    this.w_car = padr(".\bmp\TVcela.bmp",150)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempie l'elenco dei codici caratteristiche
    Use in Select(Fisse) 
 Use in Select(cFisse) 
 Use in Select(Modelli) 
 Use in Select(cModelli) 
 Use in Select(Descrit) 
 Use in Select(cDescrit) 
 Use in Select(cCursZ) 
 Use in Select(this.w_cCursor) 
 Use in Select(cCarParam)
    Use in Select(cCursCONF_DIS)
    Use in Select(cCursCONFDDIS)
    this.w_TOTRISP = 0
    if .F.
      * --- Create temporary table TMPEXPDB
      i_nIdx=cp_AddTableDef('TMPEXPDB') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPEXPDB_proto';
            )
      this.TMPEXPDB_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    GSDS_BEX(this,"R", SPACE(1), "N", this.oParentObject.w_ARCODART, this.oParentObject.w_ARCODART, SPACE(2), Expldb, "N", "N", i_DATSYS, 1, 999, "N", "S", "N", "N", "P")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_NUMDIST = 1
    this.TMPEXPDB_idx = cp_OpenTable("TMPEXPDB")
    * --- Select from TMPEXPDB
    i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2],.t.,this.TMPEXPDB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(NUMDIST) AS NUMDIST  from "+i_cTable+" TMPEXPDB ";
           ,"_Curs_TMPEXPDB")
    else
      select MAX(NUMDIST) AS NUMDIST from (i_cTable);
        into cursor _Curs_TMPEXPDB
    endif
    if used('_Curs_TMPEXPDB')
      select _Curs_TMPEXPDB
      locate for 1=1
      do while not(eof())
      this.w_NUMDIST = NVL(_Curs_TMPEXPDB.NUMDIST, 0)
        select _Curs_TMPEXPDB
        continue
      enddo
      use
    endif
    if this.w_NUMDIST=0
      ah_ErrorMsg("Attenzione: selezione vuota", 48)
      i_retcode = 'stop'
      return
    endif
    Select *, repl(" ",80) as dessup from (EXPLDB) into cursor (EXPLDB) readwrite
    this.oParentObject.w_TREEVIEW.cNodeBmp = "tree1.bmp"
    this.oParentObject.w_TREEVIEW.cLeafBmp = "Tree2.bmp"
    this.w_TREEVIEW.nIndent = 20
    if Used(cCursBmp)
      USE IN SELECT(cCursBmp)
    endif
    CREATE CURSOR (cCursBmp) (ARTIPART C(2), BMPNAME C(200))
    if used(cCursBmp)
      Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("PF" , "BMP\Tree1.ico")
      Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("SE" , "BMP\Tree4.ico")
      Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("MP" , "BMP\Tree2.ico")
      Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("PH" , "BMP\Tree5.ico")
      Insert into (cCursBmp) (ARTIPART, BMPNAME) VALUES ("CC" , "BMP\compone.ico")
      UPDATE (EXPLDB) SET CPBMPNAME = NVL(&cCursBmp..BMPNAME, "BMP\treedefa.ico") FROM (cCursBmp) WHERE &EXPLDB..ARTIPART = &cCursBmp..ARTIPART
    endif
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_KCC", this, cCursM)
    this.oParentObject.w_KEYRIF = SYS(2015)
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.oParentObject.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\CCAR\EXE\QUERY\GSCR21BC4",this.ART_TEMP_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    do case
      case UPPER(CP_DBTYPE)="SQLSERVER"
        * --- QUESTA PROCEDURA FUNZIONA SU TUTTE LE VERSIONI DI SQL SERVER
        *     - 2000
        *     - 2005
        *     - 2008
        *     - 2008R2
        *     - 2012
        * --- Creo la tabella temporanea di appoggio
        * --- Create temporary table TMPCONFDDIS
        i_nIdx=cp_AddTableDef('TMPCONFDDIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCR22BC4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPCONFDDIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Preparo la query per concatenare le stringhe
        * --- Try
        local bErr_0532B178
        bErr_0532B178=bTrsErr
        this.Try_0532B178()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0532B178
        * --- End
      case UPPER(CP_DBTYPE)="ORACLE"
        * --- QUESTA PROCEDURA FUNZIONA SU TUTTE LE VERSIONI DI ORACLE
        *     - 9
        *     - 10
        *     - 11
        * --- Creo una tabella temporanea di appoggio in modo da poter concatenare le scelte
        * --- Create temporary table TMPCONFORA
        i_nIdx=cp_AddTableDef('TMPCONFORA') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSCR24BC4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPCONFORA_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Concateno le scelte
        * --- Create temporary table TMPCONFDDIS
        i_nIdx=cp_AddTableDef('TMPCONFDDIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.TMPCONFORA_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCONFORA_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"CC__DIBA as CC__DIBA, CCCOMPON as CCCOMPON, CCCODICE as CCCODICE, CAST( RTRIM(LTRIM(MAX(SYS_CONNECT_BY_PATH(CPROWORD,'.')) KEEP (DENSE_RANK LAST ORDER BY curr),'.')) AS VARCHAR(4000)) as CPROWORD "," from "+i_cTable;
              +" group by CC__DIBA,CCCOMPON,CCCODICE CONNECT BY prev = PRIOR curr AND CC__DIBA = PRIOR CC__DIBA AND CCCOMPON = PRIOR CCCOMPON START WITH curr = 1";
              )
        this.TMPCONFDDIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      case UPPER(CP_DBTYPE)="POSTGRES"
        * --- QUESTA PROCEDURA FUNZIONA SULLA VERSIONE DI POSTGRES
        *     - 9.3
        * --- Concateno le scelte
        * --- Create temporary table TMPCONFDDIS
        i_nIdx=cp_AddTableDef('TMPCONFDDIS') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\CCAR\EXE\QUERY\GSCR27BC4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPCONFDDIS_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      otherwise
        ah_ErrorMsg("Tipo database non gestito")
        i_retcode = 'stop'
        return
    endcase
    * --- Restutuisce i dati nel formato corretto per essere gestiti con il configuratore di caratteristiche
    vq_exec("GSCR25BC4", this, cCursM)
    * --- Cursore che contiene solo le caratteristiche da inserire nello zoom
    *     Ogni caratteristica deve essere presente una sola volta
    USE IN SELECT("cCarValide")
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCRVBC4", this, "cCarValide")
    SELECT distinct livello AS livello FROM "cCarValide" INTO CURSOR maxlevel2
    SELECT RECNO() AS massimo, LIVELLO AS LIVELLO FROM maxlevel2 INTO CURSOR maxlevel order by 1,2
    use in select("maxlevel2")
    * --- Cursore vuoto con la struttura dello Zoom
    cCursZ = this.oParentObject.w_cCursZ
    USE IN SELECT(cCursZ)
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_KCC", this, cCursZ)
    SELECT(cCursZ)
    INDEX ON CCTIPOCA+CCCODICE+ALLTRIM(STR(CPROWORD)) TAG CHIAVE COLLATE "MACHINE"
    select maxlevel
    go top
    scan
    this.w_maxlvl = maxlevel.massimo
    this.w_LIVELLO = livello
    SELECT(cCursZ)
    APPEND BLANK
    REPLACE CCHDRLVL WITH "S", EXPANDED WITH "S", CCHEADER WITH "N", CCENABLE WITH "S", ; 
 DESCRITV WITH ah_msgFormat("Gruppo caratteristiche di %1� livello", alltrim(str(this.w_maxlvl,3,0))), ; 
 lvlkeytv with "AAA." + padl(alltrim(str(this.w_maxlvl)), 3, "0"), livello with this.w_maxlvl, shownode with "S", livtreev with 1, livcursm with this.w_LIVELLO
    select maxlevel
    endscan
    use in select("maxlevel")
    SELECT(cCursZ) 
 GO BOTTOM
    myRefresh = reccount()
    Select(cCursD) 
 Append from dbf(cCursZ)
    this.w_cIntestazione = SYS(2015)
    USE IN SELECT(this.w_cIntestazione)
     Insert into (cCursZ) ; 
 Select distinct alltrim(b.cccodice) + " ( " + alltrim(b.ccdescri) + " ) " as DESCRITV, 0 as ccvalmin, 0 as ccvalmax, "N" as cchdrlvl, "S" AS CCHEADER, "S" AS EXPANDED, "S" AS CCENABLE,; 
 b.cctipoca, b.cccodice, b.ccdescri, b.cc__diba, space(20), b.ccroword*0, SPACE(20), 9*0 as xchk, 9*0 as scelta, 99999*0 as cproword ,; 
 space(0) as ccdettag, b.livello, (999*0+2) as livtreev, b.lvlkeyn as lvlkey, 9*0 as rsel, 9*0 as canc, 9*0 as carisp, space(80) as ctooltip, ; 
 space(0) as ccformul, space(1) as ccflinit, b.livello as livcursm ,; 
 b.lvlkey1 as lvlkeym, b.lvlkeyn as lvlkeyp, "S" AS shownode, b.lvlkey1 as lvlkeytv from (cCursM) a inner join "cCarValide" b ; 
 on a.cctipoca=b.cctipoca and a.cccodice=b.cccodice
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCRFBC4", this, cFisse)
    insert into (cCursZ) select a.DESCRITV, 0 as ccvalmin, 0 as ccvalmax, a.cchdrlvl, a.ccheader, a.expanded, a.ccenable,; 
 a.cctipoca, a.cccodice, a.ccdescri, a.cc__diba, a.cccompon, a.ccroword, a.cccodart, a.xchk, a.scelta, a.cproword ,; 
 a.ccdettag, b.livello as livello, 999*0+3 as livtreev, alltrim(b.lvlkeyn)+ "." + str(a.cproword,3,0) as lvlkey, a.rsel, a.canc, a.carisp, space(80) as ctooltip, ; 
 cast(space(0) as memo) as ccformul, a.ccflinit, a.livcursm ,; 
 b.lvlkey1 as lvlkeym, b.lvlkeyn as lvlkeyp, "S" AS shownode, b.lvlkey1 as lvlkeytv from (cFisse) a inner join "cCarValide" b ; 
 on a.cctipoca=b.cctipoca and a.cccodice=b.cccodice
    cModelli = this.oParentObject.w_cModelli
    Use in Select(cModelli)
    Use in Select(Modelli)
    * --- Carico in memoria i dati relativi alle caratteristiche parametriche
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCRMBC4", this, cModelli)
    select a.*, 999*0+3 as livtreev, b.lvlkeyn as lvlkey, b.lvlkey1 as lvlkeym, b.lvlkeyn as lvlkeyp, "S" AS shownode, b.lvlkey1 as lvlkeytv from (cModelli) a inner join "cCarValide" b ; 
 on a.cctipoca=b.cctipoca and a.cccodice=b.cccodice into cursor (cModelli) readwrite
    * --- Cambio il formato del dato in modo che sia compatibile con il formato zoom
    SELECT(cModelli)
    GO TOP
    SCAN
    SCATTER MEMVAR MEMO
    this.w_AggModelli = .T.
    w_s = "01"
    this.w_i = 1
    this.w_LVLKEY = m.lvlkey
    this.w_LIVELLO = m.livello
    this.w_NEWVAL = ""
    do while this.w_i <= 15
      this.w_VAR_i = Alltrim(nvl(&cModelli..CCVAR1&w_s,""))
      this.w_VAL_i = NVL(&cModelli..CCQTA1&w_s,0)
      if not empty(this.w_VAR_i)
        this.w_NEWVAL = this.w_NEWVAL + this.w_VAR_i + "=" + Alltrim(str(this.w_VAL_i,10,3))+":"
        SELECT(cCursZ)
        APPEND BLANK
        CCVALMIN = m.CCMIN1&w_s 
 CCVALMAX = m.CCMAX1&w_s 
 CPROWORD = this.w_VAL_i 
 CCDETTAG = this.w_VAR_i 
 CCFLINIT="N" 
 XCHK=1
        m.lvlkey = rtrim(this.w_LVLKEY) + "." + padl(alltrim(str(this.w_i)), 3, "0")
        GATHER MEMVAR MEMO
      endif
      SELECT(cCursZ)
      this.w_i = this.w_i + 1
      w_s = padl(alltrim(str(this.w_i)), 2, "0")
      SELECT(cModelli)
    enddo
    this.w_LVLKEYP = m.lvlkeyp
    SELECT(cCursZ)
    if SEEK(this.w_LVLKEYP)
      Replace CCDETTAG with padr(this.w_NEWVAL,40), ccformul with m.ccformul
    endif
    SELECT(cModelli)
    ENDSCAN
    Use in Select(cModelli)
    SELECT(cCursZ)
    myRefresh = reccount()
    GO TOP
    USE IN SELECT(this.w_cCursor)
    SELECT A.*, A.CCFORMUL AS FORMULAO, "N" AS ERRORE, CAST(SPACE(0) AS MEMO) AS MSGERR FROM (cCursZ) A INTO CURSOR (this.w_cCursor) WHERE 1=0 READWRITE
    SELECT(this.w_cCursor)
    INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
    * --- Cursore parallelo che mi serve per valorizzare le formule
    USE IN SELECT(cCarParam)
    SELECT A.*, A.CPROWORD AS CPROWORDS, A.CCFORMUL AS FORMULAO, "N" AS ERRORE, CAST(SPACE(0) AS MEMO) AS MSGERR ; 
 FROM (cCursZ) A into Cursor (cCarParam) where a.cctipoca<>"C" and a.cchdrlvl<>"S" and a.ccheader<>"S" readwrite
    INDEX ON CCTIPOCA+CCCODICE+CC__DIBA+CCCOMPON+ALLTRIM(STR(CCROWORD)) tag lvlkey COLLATE "MACHINE"
    * --- --Sono sui documenti e voglio ricordarmi le risposte fatte in precedenza
    if this.oParentObject.w_RIC="S" and type("this.GSCR_KCC.oParentObject.class")="C" and "GSOR_MDV" $ upper(this.GSCR_KCC.oParentObject.class) 
      this.oParentObject.w_RICDOC = True
    else
      this.oParentObject.w_RICDOC = False
    endif
    * --- Cursori che contengono tutte le risposte per le distinte
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCRFBC42", this, cCursCONF_DIS)
    INDEX ON CC__DIBA+STR(CCROWORD)+CCCOMPON+CCCODICE TAG CHIAVE COLLATE "MACHINE"
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCRFBC43", this, cCursCONFDDIS)
    INDEX ON CC__DIBA+STR(CCROWORD)+CCCOMPON+CCCODICE+STR(CPROWORD) TAG CHIAVE COLLATE "MACHINE"
    if this.oParentObject.w_FLINIT="N"
      this.GSCR_KCC.NotifyEvent("AggiornaTV")     
      this.w_TREEVIEW.ExpandAll(.T.)
    endif
    * --- Delete from TMPEXPDB
    i_nConn=i_TableProp[this.TMPEXPDB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPEXPDB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where 1=1")
    else
      delete from (i_cTable) where 1=1
      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Drop temporary table TMPCONFORA
    i_nIdx=cp_GetTableDefIdx('TMPCONFORA')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCONFORA')
    endif
    * --- Drop temporary table TMPCONFDDIS
    i_nIdx=cp_GetTableDefIdx('TMPCONFDDIS')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPCONFDDIS')
    endif
  endproc
  proc Try_0532B178()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Concateno le stringhe CPROWORD di confddis
    * --- begin transaction
    cp_BeginTrs()
    this.w_LCONN = i_TableProp[this.TMPCONFDDIS_idx,3]
    this.w_LTABLE = cp_SetAzi(i_TableProp[this.TMPCONFDDIS_idx,2])
    this.w_SCRIPTSQL = ""
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "DECLARE @Authors VARCHAR(8000);"+CHR(13)
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "SET @Authors = '';"+CHR(13)
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "DECLARE @CC__DIBA CHAR(41);"+CHR(13)
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "DECLARE @CCCOMPON CHAR(41);"+CHR(13)
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "DECLARE @CCCODICE CHAR(5);"+CHR(13)
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "Update " + this.w_LTABLE + " SET @Authors = "
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + " CPROWORD = CASE WHEN @CC__DIBA=CC__DIBA AND @CCCOMPON=CCCOMPON "
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "AND @CCCODICE=CCCODICE THEN @Authors + '.' + CPROWORD ELSE '.' + CPROWORD END,"
    this.w_SCRIPTSQL = this.w_SCRIPTSQL + "@CC__DIBA = CC__DIBA, @CCCOMPON=CCCOMPON,  @CCCODICE=CCCODICE"
    this.w_RESULT = cp_sqlexec( this.w_LCONN , this.w_SCRIPTSQL)
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera/Stampa DiBa Configurata
    this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
    this.w_OK = .T.
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2],.t.,this.PAR_DISB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDRICONF"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDRICONF;
        from (i_cTable) where;
            PDCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PDRICONF = NVL(cp_ToDate(_read_.PDRICONF),cp_NullValue(_read_.PDRICONF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if USED(cInitCur)
      this.Page_17()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Verifica la correttezza dei valori inseriti
    Select (this.w_cCursor) 
 go top 
 scan for cproword<>0 and cctipoca="C"
    this.w_OK = True
    this.w_CCCODICE = cccodice
    this.w_CPROWORD = cproword
    this.w_nTMP = 0
    if this.w_CPROWORD<>0
      * --- Read from CONFDCAR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONFDCAR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDCAR_idx,2],.t.,this.CONFDCAR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" CONFDCAR where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWORD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              CCCODICE = this.w_CCCODICE;
              and CPROWORD = this.w_CPROWORD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_nTMP = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_nTMP=0
        this.w_OK = False
        ah_errormsg("Per il codice %1 � stato impostato un valore non valido", 48,"",this.w_CCCODICE) 
 go bottom
      endif
    endif
    Select (this.w_cCursor) 
 endscan
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- Controllo errori caratteristiche parametriche
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    if this.w_OK
      Select (this.w_cCursor) 
 go top 
 scan for cctipoca="M"
      this.w_CCCODICE = cccodice
      this.w_CPROWORD = cproword
      this.w_ERRORE = nvl(errore, "N")
      if this.w_ERRORE="S"
        this.w_OK = False
        this.w_MSG = this.w_MSG+chr(13)+alltrim(NVL(MSGERR,""))
      endif
      Select (this.w_cCursor) 
 endscan
      if !this.w_OK
        ah_errormsg("In fase di selezione si sono verificati i seguenti errori: %0%1", 48,"",alltrim(this.w_MSG))
      endif
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    this.w_INTERACTIVECHECK = this.GSCR_KCC.w_CONTINUECHECK = "S"
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    * --- --Occorre vedere se � stata inserita almeno una scelta
    *      nel primo livello per configurare
    if this.w_OK
      if this.oParentObject.w_GENMOD<>"M"
        this.w_CONTINUECHECK = "S"
        do while this.w_CONTINUECHECK="S"
          SELECT (this.w_cCursor)
          GO TOP
          COUNT FOR CCTIPOCA="D" AND LIVELLO>0 TO this.w_NUMCADES
          SELECT (this.w_cCursor)
          GO TOP
          COUNT FOR CCTIPOCA<>"D" AND LIVELLO>0 TO this.w_NUMCNDES
          if this.w_NUMCADES > 0 AND this.w_NUMCNDES = 0
            Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello>0 having CARSCE=CARTOT into Cursor Scelte
          else
            Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello>0 and CCTIPOCA<>"D" having CARSCE=CARTOT into Cursor Scelte
          endif
          if Reccount("Scelte")=0
            if this.w_INTERACTIVECHECK
              this.w_CONTINUECHECK = "S"
              this.w_OK = False
              Select (this.w_cCursor)
              GO TOP
              LOCATE FOR RSEL=0 and NVL(CCTIPOCA,"D")<>"D" and !EMPTY(CCTIPOCA)
              if FOUND()
                this.m_CCCODICE = CCCODICE
                this.m_CCTIPOCA = CCTIPOCA
                * --- Macro
                Local L_GSCRIKCC
                L_GSCRIKCC = "GSCRIKCC(This)"
                this.GSCRIKCC = &L_GSCRIKCC && this.GSCRIKCC(This)
                this.GSCRIKCC.NotiFyEvent("Interroga")     
                this.Albero1 = this.GSCRIKCC.w_Albero
                Local cCursD1
                cCursD1 = this.Albero1.cCursor
                SELECT(cCursD1)
                INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
                INDEX ON LVLKEYTV TAG LVLKEYTV COLLATE "MACHINE"
                SET ORDER TO LVLKEYTV
                SELECT(cCursD1)
                APPEND FROM DBF(cCursD) FOR CCTIPOCA = this.m_CCTIPOCA and CCCODICE = this.m_CCCODICE
                SELECT(cCursD1)
                GO TOP
                this.GSCRIKCC.mCalc(True)     
                this.Albero1 = .NULL.
                this.GSCRIKCC.Visible = .F.
                this.GSCRIKCC.Show(1)     
                this.GSCRIKCC = .NULL.
                cCursD = this.Albero.cCursor
                if this.w_CONFKME AND this.w_ROWSELE > 0
                  Select (cCursD)
                  if SEEK(this.w_LVLKEYCUR , (cCursD) , "LVLKEY")
                    this.GSCR_KCC.__dummy__.Enabled = .t.
                    this.GSCR_KCC.__dummy__.SetFocus()     
                    this.GSCR_KCC.__dummy__.Enabled = .f.
                    this.Albero.Grd.SetFocus()     
                    this.GSCR_KCC.NotifyEvent("w_Albero row checked")     
                    this.GSCR_KCC.NotifyEvent("SottoAlbero")     
                  endif
                else
                  this.w_INTERACTIVECHECK = .F.
                  Select (cCursD)
                  GO TOP
                  this.GSCR_KCC.__dummy__.Enabled = .t.
                  this.GSCR_KCC.__dummy__.SetFocus()     
                  this.GSCR_KCC.__dummy__.Enabled = .f.
                  this.Albero.Grd.SetFocus()     
                endif
                * --- A volte chiamando una maschera non modale come modale, alla chiusura non si riattiva il menu
                if Vartype(i_ThemesManager)="O"
                  i_ThemesManager.LockVfp(.F., .T.)
                endif
              endif
            else
              this.w_CONTINUECHECK = "N"
              this.w_OK = False
              select (this.w_cCursor)
              GO TOP 
 SCAN for rsel=0 and NVL(CCTIPOCA,"D")<>"D" and !EMPTY(CCTIPOCA)
              this.w_MSG = this.w_MSG+chr(13)+alltrim(NVL(DESCRITV,""))
              ENDSCAN
              ah_errormsg("Per effettuare la generazione occorre specificare le seguenti caratteristiche: %1", 48,"",alltrim(this.w_MSG))
              go bottom
            endif
          else
            this.w_OK = .T.
            this.w_CONTINUECHECK = "N"
            exit
          endif
        enddo
      endif
      use in select("Scelte")
    endif
    if this.w_OK AND used(EXPLDB)
      this.oParentObject.w_CCCONFIG = ""
      Select (this.w_cCursor) 
 go top 
 scan for livello >0 and !Empty(nvl(cctipoca, ""))
      this.w_CCCODICE = CCCODICE
      this.w_L_CCTIPOCA = CCTIPOCA
      this.w_STR = this.w_L_CCTIPOCA+alltrim(this.w_CCCODICE)+"("
      this.w_NEWVAL = ""
      do case
        case this.w_L_CCTIPOCA = "C"
          this.w_CPROWORD = cproword
          this.w_NEWVAL = Alltrim(Str(this.w_CPROWORD))
        case this.w_L_CCTIPOCA $ "D-M"
          this.w_NEWVAL = Alltrim(NVL(CCDETTAG,""))
      endcase
      if !Empty(this.w_NEWVAL)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Select (this.w_cCursor) 
 endscan
    endif
    =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
    if this.w_OK AND used(EXPLDB)
      Select CACODICE from (EXPLDB) where empty(corifdis) into cursor sup
      Select("sup")
      go top
      this.w_SUPREMO = ALLTRIM(SUP.cacodice)+"%"
      this.w_CCCODIDX = Padr(this.oParentObject.w_CCCONFIG,100)
      this.w_MESS = ""
      * --- --IL CONTROLLO SULLA CONFIGURAZIONE DEVE AVVENIRE PER DISTINTA NEUTRA
      this.w_CONFIG = left(this.oParentObject.w_CCCONFIG,4000)
      * --- Select from CONFMEMO
      i_nConn=i_TableProp[this.CONFMEMO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFMEMO_idx,2],.t.,this.CONFMEMO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CCCODRIC  from "+i_cTable+" CONFMEMO ";
            +" where CCCODRIC LIKE "+cp_ToStrODBC(this.w_SUPREMO)+" AND CCCODIDX="+cp_ToStrODBC(this.w_CCCODIDX)+" AND CCKEYSAL="+cp_ToStrODBC(this.oParentObject.w_CCKEYSAL)+" AND CCCONFIG like "+cp_ToStrODBC(this.w_CONFIG)+"";
            +" order by CCSERIAL DESC";
             ,"_Curs_CONFMEMO")
      else
        select CCCODRIC from (i_cTable);
         where CCCODRIC LIKE this.w_SUPREMO AND CCCODIDX=this.w_CCCODIDX AND CCKEYSAL=this.oParentObject.w_CCKEYSAL AND CCCONFIG like this.w_CONFIG;
         order by CCSERIAL DESC;
          into cursor _Curs_CONFMEMO
      endif
      if used('_Curs_CONFMEMO')
        select _Curs_CONFMEMO
        locate for 1=1
        do while not(eof())
        this.SUPREMO = _Curs_CONFMEMO.CCCODRIC
        this.w_MESS = "Configurazione gi� presente: %1"
        go bottom
          select _Curs_CONFMEMO
          continue
        enddo
        use
      endif
      use in select("sup")
      if this.w_OPER = "STAMPA"
        if not empty(this.w_MESS)
          ah_errormsg(this.w_MESS,48,"", this.SUPREMO)
        endif
      endif
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      * --- Per la generazione della distinta e i controlli utilizzo un altro cursore
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      this.w_cExpldb2 = SYS(2015)
      Use in Select(this.w_cExpldb2)
      this.w_EXPLDB = this.w_cExpldb2
      Select * from (EXPLDB) into cursor (this.w_EXPLDB) nofilter order by lvlkey readwrite
      * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      this.Page_15()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if not this.w_OK
        use in select(this.w_EXPLDB)
        if this.w_OPER = "CREADIS"
          * --- Rinfresca il cursore della distinta neutra
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Write into PAR_DISB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_DISB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
              +i_ccchkf ;
          +" where ";
              +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              PDBLCFGC = "N";
              &i_ccchkf. ;
           where;
              PDCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        i_retcode = 'stop'
        return
      else
        EXPLDB = this.w_EXPLDB
      endif
      this.w_ELSCELTE = "Configurazione effettuata:"
      if not empty(nvl(this.oParentObject.w_CODLIN,""))
        this.w_TRADUZ = this.w_ELSCELTE
      endif
      * --- CONTROLLO
      Select (this.w_cCursor)
      Scan for CCTIPOCA = "C"
      this.w_CCDESCRI = CCDESCRI
      Select (this.w_cCursor)
      this.w_ELSCELTE = this.w_ELSCELTE + chr(13) +alltrim(CCCODICE) + " (" + alltrim(this.w_CCDESCRI) + ") = "
      if not empty(nvl(this.oParentObject.w_CODLIN,""))
        * --- Read from CONFTRAD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONFTRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFTRAD_idx,2],.t.,this.CONFTRAD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITRADUZ"+;
            " from "+i_cTable+" CONFTRAD where ";
                +"DIDESCRI = "+cp_ToStrODBC(this.w_CCDESCRI);
                +" and DICODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITRADUZ;
            from (i_cTable) where;
                DIDESCRI = this.w_CCDESCRI;
                and DICODLIN = this.oParentObject.w_CODLIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DITRADUZ = NVL(cp_ToDate(_read_.DITRADUZ),cp_NullValue(_read_.DITRADUZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not empty(this.w_DITRADUZ)
          this.w_CCDESCRI = this.w_DITRADUZ
        endif
        Select (this.w_cCursor)
        this.w_TRADUZ = this.w_TRADUZ + chr(13) + CCCODICE + " (" + alltrim(this.w_CCDESCRI) + ") = "
      endif
      this.w_CCDESCRI = CCDETTAG
      Select (this.w_cCursor)
      this.w_ELSCELTE = this.w_ELSCELTE + alltrim(str(CPROWORD)) + ", " + alltrim(this.w_CCDESCRI)
      if not empty(nvl(this.oParentObject.w_CODLIN,""))
        * --- Read from CONFTRAD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONFTRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFTRAD_idx,2],.t.,this.CONFTRAD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITRADUZ"+;
            " from "+i_cTable+" CONFTRAD where ";
                +"DIDESCRI = "+cp_ToStrODBC(this.w_CCDESCRI);
                +" and DICODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITRADUZ;
            from (i_cTable) where;
                DIDESCRI = this.w_CCDESCRI;
                and DICODLIN = this.oParentObject.w_CODLIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DITRADUZ = NVL(cp_ToDate(_read_.DITRADUZ),cp_NullValue(_read_.DITRADUZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not empty(this.w_DITRADUZ)
          this.w_CCDESCRI = this.w_DITRADUZ
        endif
        Select (this.w_cCursor)
        this.w_TRADUZ = this.w_TRADUZ + str(CPROWORD) + ", " + this.w_CCDESCRI
      endif
      EndScan
      Select (this.w_cCursor)
      Scan for left(ccformul,6) = "Valori" and cctipoca="M"
      this.w_ELSCELTE = this.w_ELSCELTE + chr(13) + alltrim(CCFORMUL)
      if not empty(nvl(this.oParentObject.w_CODLIN,""))
        this.w_cTMP2 = left(CCFORMUL, at(")",CCFORMUL)-1)
        this.w_cTMP2 = substr(this.w_cTMP2, at("(",this.w_cTMP2)+1)
        * --- Read from CONFTRAD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONFTRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFTRAD_idx,2],.t.,this.CONFTRAD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITRADUZ"+;
            " from "+i_cTable+" CONFTRAD where ";
                +"DICODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                +" and DIDESCRI = "+cp_ToStrODBC(this.w_cTMP2);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITRADUZ;
            from (i_cTable) where;
                DICODLIN = this.oParentObject.w_CODLIN;
                and DIDESCRI = this.w_cTMP2;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DITRADUZ = NVL(cp_ToDate(_read_.DITRADUZ),cp_NullValue(_read_.DITRADUZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CCDESCRI = CCFORMUL
        if not empty(this.w_DITRADUZ)
          this.w_CCDESCRI = strtran(this.w_CCDESCRI, "("+this.w_cTMP2+")", "("+alltrim(this.w_DITRADUZ)+")")
        endif
        Select (this.w_cCursor)
        this.w_TRADUZ = this.w_TRADUZ + chr(13) + this.w_CCDESCRI
      endif
      EndScan
      if used(Modelli)
        * --- Formule parametriche
        Select (Modelli)
        Scan for left(ccformul,6) = "Valori" and cctipoca="M"
        this.w_ELSCELTE = this.w_ELSCELTE + chr(13) + alltrim(CCFORMUL)
        if not empty(nvl(this.oParentObject.w_CODLIN,""))
          this.w_cTMP2 = left(CCFORMUL, at(")",CCFORMUL)-1)
          this.w_cTMP2 = substr(this.w_cTMP2, at("(",this.w_cTMP2)+1)
          * --- Read from CONFTRAD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONFTRAD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONFTRAD_idx,2],.t.,this.CONFTRAD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DITRADUZ"+;
              " from "+i_cTable+" CONFTRAD where ";
                  +"DICODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                  +" and DIDESCRI = "+cp_ToStrODBC(this.w_cTMP2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DITRADUZ;
              from (i_cTable) where;
                  DICODLIN = this.oParentObject.w_CODLIN;
                  and DIDESCRI = this.w_cTMP2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DITRADUZ = NVL(cp_ToDate(_read_.DITRADUZ),cp_NullValue(_read_.DITRADUZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CCDESCRI = CCFORMUL
          if not empty(this.w_DITRADUZ)
            this.w_CCDESCRI = strtran(this.w_CCDESCRI, "("+this.w_cTMP2+")", "("+alltrim(this.w_DITRADUZ)+")")
          endif
          Select (this.w_cCursor)
          this.w_TRADUZ = this.w_TRADUZ + chr(13) + this.w_CCDESCRI
        endif
        EndScan
        * --- Descrizioni
        Select distinct * from (Modelli) into cursor Mod_1
        if USED(Modelli)
          use in select(Modelli)
        endif
        Select * from Mod_1 into cursor (Modelli) readwrite
        use in select("Mod_1")
        Select (Modelli)
        Scan for cctipoca="D" and not empty(nvl(CCDETTAG,"")) and CCFORMUL="OK"
        this.w_ELSCELTE = this.w_ELSCELTE + chr(13) + Replicate("_",30) + chr(13) + "Descrizione "
        this.w_CCDESCRI = CCDETTAG
        Select (Modelli)
        this.w_ELSCELTE = this.w_ELSCELTE + alltrim(CCCODICE) + " (" + alltrim(CCDESCRI) + ")  " + CHR(13) + alltrim(this.w_CCDESCRI)
        if not empty(nvl(this.oParentObject.w_CODLIN,""))
          this.w_TRADUZ = this.w_TRADUZ + chr(13) + Replicate("_",30) + chr(13) + "Descrizione "
          * --- Read from CONFTRDE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONFTRDE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONFTRDE_idx,2],.t.,this.CONFTRDE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DITRADUZ"+;
              " from "+i_cTable+" CONFTRDE where ";
                  +"DICODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                  +" and DICODICE = "+cp_ToStrODBC(CCCODICE);
                  +" and DITIPOCA = "+cp_ToStrODBC("D");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DITRADUZ;
              from (i_cTable) where;
                  DICODLIN = this.oParentObject.w_CODLIN;
                  and DICODICE = CCCODICE;
                  and DITIPOCA = "D";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DITRADUZ = NVL(cp_ToDate(_read_.DITRADUZ),cp_NullValue(_read_.DITRADUZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not empty(this.w_DITRADUZ)
            this.w_CCDESCRI = this.w_DITRADUZ
          endif
          Select (this.w_cCursor)
          this.w_TRADUZ = this.w_TRADUZ + CCCODICE + " (" + alltrim(CCDESCRI) + ")  " + CHR(13) + this.w_CCDESCRI
        endif
        EndScan
      endif
      if this.w_OPER = "STAMPA"
        l_ELSCELTE = this.w_ELSCELTE 
 Select (this.w_EXPLDB) 
 Replace all livello with occur(".", lvlkey)
      endif
      do case
        case this.w_OPER == "STAMPAP"
          l_ELSCELTE = this.w_ELSCELTE 
 Select (this.w_EXPLDB) 
 Replace all livello with occur(".", lvlkey)
          this.Page_12()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_OPER == "STAMPA"
          Select * from (this.w_EXPLDB) into cursor __tmp__ nofilter order by lvlkey 
 cp_chprn("..\CCAR\exe\query\GSCR_kcc") 
 Use in (this.w_EXPLDB)
        otherwise
          Select (EXPLDB) 
 go top
          COUNT FOR NOT DELETED() TO this.w_n
          if this.w_n >1
            this.oParentObject.w_TESTCODE = "S"
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_RISPOSTA
              if TYPE("this.oParentObject.class")<>"U"
                this.GSCR_KCC.NotifyEvent("Visualizza")     
                this.oParentObject.w_TESTCODE = "N"
              endif
            else
              this.oParentObject.w_TESTCODE = "N"
            endif
          else
            * --- Write into PAR_DISB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PAR_DISB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
                  +i_ccchkf ;
              +" where ";
                  +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                     )
            else
              update (i_cTable) set;
                  PDBLCFGC = "N";
                  &i_ccchkf. ;
               where;
                  PDCODAZI = i_CODAZI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_cTMP = "Le scelte selezionate non consentono la creazione della distinta%0Rieffettuare la configurazione"
            ah_ErrorMsg(this.w_cTMP,48," ")
            this.GSCR_KCC.NotifyEvent("Visualizza")     
          endif
      endcase
    else
      ah_errormsg("Configurazione non completa",48)
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna stringa di configurazione
    this.w_POS = At(this.w_STR, this.oParentObject.w_CCCONFIG)
    if this.w_POS>0
      * --- La caratteristica � gi� presente
      this.w_OLDVAL = Substr(this.oParentObject.w_CCCONFIG, this.w_POS+Len(this.w_STR))
      this.w_OLDVAL = Left(this.w_OLDVAL, At(")", this.w_OLDVAL) -1)
      if this.w_OLDVAL<>this.w_NEWVAL
        this.oParentObject.w_CCCONFIG = Strtran(this.oParentObject.w_CCCONFIG, this.w_STR+this.w_OLDVAL, this.w_STR+this.w_NEWVAL, 1, 1)
      endif
    else
      if Len(this.oParentObject.w_CCCONFIG)=0
        this.oParentObject.w_CCCONFIG = this.w_STR+this.w_NEWVAL+");"
      else
        this.w_POS = At(";", this.oParentObject.w_CCCONFIG)
        this.w_POS1 = 0
        this.w_STR1 = Left(this.oParentObject.w_CCCONFIG, this.w_POS)
        this.w_CONT = 1
        do while this.w_STR1<this.w_STR and not empty(this.w_STR1)
          this.w_CONT = this.w_CONT + 1
          this.w_POS1 = this.w_POS+1
          this.w_POS = At(";", this.oParentObject.w_CCCONFIG, this.w_CONT)
          this.w_STR1 = Substr(this.oParentObject.w_CCCONFIG, this.w_POS1, this.w_POS-this.w_POS1)
        enddo
        do case
          case this.w_POS1=0
            this.oParentObject.w_CCCONFIG = this.w_STR + this.w_NEWVAL + ");" + this.oParentObject.w_CCCONFIG
          case Empty(this.w_STR1)
            this.oParentObject.w_CCCONFIG = this.oParentObject.w_CCCONFIG + this.w_STR + this.w_NEWVAL + ");"
          otherwise
            this.oParentObject.w_CCCONFIG = left(this.oParentObject.w_CCCONFIG,this.w_POS1-1) + this.w_STR + this.w_NEWVAL + ");" + substr(this.oParentObject.w_CCCONFIG,this.w_POS1)
        endcase
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Solo Prima Riga della Distinta
    this.w_LIVELLO = 1
    this.w_FIRST = .T.
    do vq_exec with "..\CCAR\exe\query\GSCR_bgt", this, "cursor"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    select CACODICE,CADESART,CACODART,CACODART AS CCCODART,TIPPRO,CPBMPNAME,LVLKEY,LIVELLO,CODICEDIBA,SPACE(41) as CC__DIBA; 
 ,DESCRITV,COLLAPSED,EXPLODED,DBCODICE, ARTIPART,CACONATO,CACONFIG,SPACE(41) AS CCCOMPON, "DIST:" as CCCODICE,STADIS,99999*0 AS CPROWORD,; 
 999*0 AS LIVCURSM,space(1) as CCTIPOCA, PADR(CACODICE,40) as CCDESCRI,; 
 99999*0 as CCPROWORD, SPACE(40) AS CCDETTAG, 99999*0 AS RSEL,99999*0 AS CCROWORD ; 
 from cursor into cursor (this.w_cCursor) readwrite
    index on LvlKey tag LvlKey COLLATE "MACHINE" 
 replace descritv with AH_Msgformat("Distinta da configurare") 
 cTmpTipPro=ARTIPART 
 cTmpConato=CACONATO 
 cTmpConfig=CACONFIG
    * --- Cursore BMP
    if Used(cCursBmp)
      USE IN SELECT(cCursBmp)
    endif
    CREATE CURSOR (cCursBmp) (TIPART C(2), XOC C(1), BITMAP C(200), SERIAL N(3,0))
    if used(cCursBmp)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("PF" , "C", "BMP\Tree1.ico", 1)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("PF" , "X", "BMP\Tree1.ico", 1)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("SE" , "C", "BMP\Tree4.ico", 2)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("SE" , "X", "BMP\Tree4.ico", 2)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("MP" , "C", "BMP\Tree2.ico", 3)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("MP" , "X", "BMP\Tree2.ico", 3)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("PH" , "C", "BMP\Tree5.ico", 4)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("PH" , "X", "BMP\Tree5.ico", 4)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("CC" , "C", "BMP\compone.ico", 5)
      Insert into (cCursBmp) (TIPART, XOC, BITMAP, SERIAL) VALUES ("CC" , "X", "BMP\compone.ico", 5)
    endif
    if not used(cCursBmp)
      do vq_exec with "..\DIBA\exe\query\BitMapT1", this, cCursBmp
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if used(cCursBmp) and reccount(cCursBmp)>0
      select tipart+xoc,bitmap,serial from (cCursBmp) into array aBmpPar 
 cIdxBmp=AScan(aBmpPar, cTmpTipPro+iif(cTmpTipPro="DC",iif(cTmpConato="S","C","X"),iif(cTmpConfig="S","C","X"))) 
 cTmpBmpName=aBmpPar(cIdxBmp+1)
    endif
    * --- Calcolo della BMP della riga e simulazione di una riga vuota per abilitare il pulsante '+' della tree-view
    select (this.w_cCursor) 
 replace CPBmpName with iif(type("cTmpBmpName")="C" and file(cTmpBmpName),cTmpBmpName,"bmp\treedefa.bmp") 
 append blank 
 replace lvlkey with "AAB.  0" 
    oldlocks=this.GSCR_KCC.lockscreen
    this.GSCR_KCC.lockscreen = .t.
    this.GSCR_KCC.NotifyEvent("Interroga")     
    this.GSCR_KCC.lockscreen = oldlocks
    if this.oParentObject.w_RIC="S"
      Select (cCursM)
      scan for rsel=1
      cod=NVL(CCCODICE,"")
      tipo=NVL(CCTIPOCA,"")
      Update (this.w_cCursor) set rsel=1 where cccodice=cod and cctipoca=tipo
      endscan
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplode il nodo della distinta (se non � gi� esploso)
    * --- --Si controlla se esistono delle caratteristiche che non appartengono al livello
    *     in questo caso occorre inserire un altro nodo
    this.w_trovato = .F.
    if USED(cCursM)
      if RECCOUNT(cCursM)>0
        Select (cCursM) 
 go top
        Select (this.w_cCursor) 
 go Top
        SCATTER MEMVAR MEMO
        this.w_lvlkey = rtrim(m.lvlkey)
        this.w_lvlkeymax = this.w_lvlkey
        this.w_LIVELLO = OCCURS(".",this.oParentObject.w_LVLKEYTV)+1
        this.w_POS = 0
        this.w_CC = 0
        this.w_INALBE = .F.
        this.w_LIV = this.w_LIVELLO
        * --- --Cerco il primo livello dell'albero da caricare
        if USED(cCursM)
          Select max(livello) as maxl from (cCursM) into cursor mcu
          Select mcu
          this.w_LIVMAX = NVL(maxl,1)
          Select mcu 
 USE
        endif
        if this.w_LIVMAX >= this.w_LIVELLO
          do while !(this.w_trovato)
            Select (cCursM) 
 Go top
            Locate for livello=this.w_livello
            if found()
              this.w_trovato = .t.
              this.w_LIVELLO = livello
            else
              this.w_LIVELLO = this.w_LIVELLO+1
            endif
          enddo
        endif
        Select (cCursM) 
 go top
        * --- --and ! (w_RIC='S' and USED(cRispDoc))
        if this.w_LIV=1 
          Locate for (livello=this.w_livello and RSEL<>1) or (livello=this.w_livello and CARISP=1)
          do while not eof() 
            scatter memvar memo
            if (livello=this.w_livello and RSEL<>1 and CANC<>1) or (livello=this.w_livello and CARISP=1)
              replace RSEL with 1 in (cCursM)
              * --- --occorre inserire il nuovo nodo caratteristiche al livello n+1
              *     e marcare come gi� letta la caratteristica
              Select (this.w_cCursor)
              Locate for CCCODICE = m.cccodice and CCTIPOCA = m.cctipoca
              if not found()
                this.w_DCARA = 0
                this.oParentObject.w_NUMCAR = this.oParentObject.w_NUMCAR+1
                this.w_CC = this.w_CC+1
                this.w_LVLKEY1 = Left(alltrim(this.w_LVLKEY)+"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200)
                this.w_LVLKEY1 = Left(alltrim(this.w_lvlkeymax)+"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200)
                this.w_LVLKEY3 = Left(Left(alltrim(this.w_LVLKEY1), 3) + "." +padl(alltrim(str(this.w_livello)),3, "0") +"."+padl(alltrim(str(this.w_cc)),3)+space(200), 200)
                m.TIPPRO="CC" 
 m.rsel=iif(m.cctipoca="D",1,0) 
 m.LIVCURSM=m.livello 
 m.LvlKey = this.w_LVLKEY1 
 m.LvlKeyp = this.w_LVLKEY1 
 m.lvlkeym = m.lvlkey1 
 m.SHOWNODE="S" 
 m.ccvalmin=0 
 m.ccvalmax=0 
 m.livtreev=2
                lvlkeytv = this.w_LVLKEY3
                m.errore = "N"
                m.msgerr = ""
                if m.cctipoca="M"
                  this.Page_14("CALCOLA",m.cctipoca,m.cccodice,m.cc__diba,m.cccompon,m.ccroword,m.lvlkeyp)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                insert into (this.w_cCursor) from memvar
                insert into (cCursD) from memvar
                SELECT(cCursZ)
                * --- cctipoca=m.cctipoca and cccodice=m.cccodice and ccheader<>'S'  && and livello=m.livello and cc__diba=m.cc__diba
                local l_ExprSearch, l_SearchCurs
                if m.cctipoca="M"
                  l_ExprSearch = "cctipoca=m.cctipoca and cccodice=m.cccodice and cc__diba=m.cc__diba and cccompon=m.cccompon and ccroword=m.ccroword and ccheader<>'S' "
                  l_SearchCurs = Modelli
                else
                  l_ExprSearch = "cctipoca=m.cctipoca and cccodice=m.cccodice and ccheader<>'S' "
                  l_SearchCurs = cCursZ
                endif
                SELECT(l_SearchCurs)
                SCAN FOR &l_ExprSearch
                SCATTER MEMVAR MEMO
                this.w_DCARA = this.w_DCARA + 1
                m.LIVCURSM=this.w_livello 
 m.LvlKey = (alltrim(this.w_LVLKEY1)+"."+padl(alltrim(str(this.w_DCARA)),3)) 
 m.lvlkeyp = this.w_LVLKEY1 
 m.lvlkeym = m.lvlkey1 
 m.SHOWNODE="S" 
 m.livtreev=3
                m.lvlkeytv = Left(alltrim(this.w_LVLKEY3)+"."+padl(alltrim(str(this.w_DCARA)),3)+ space(200), 200)
                insert into (cCursD) from memvar
                SELECT(l_SearchCurs)
                ENDSCAN
              endif
              Select (cCursM)
            endif
            if Not Eof()
              Skip
            endif
          enddo
        endif
      else
        ah_errormsg("Non ci sono caratteristiche valide associate alla distinta",48)
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Si controlla se esistono delle caratteristiche che non appartengono al livello
    *     in questo caso occorre inserire un altro nodo
    SELECT(cCursD)
    go bottom
    do while CCHDRLVL = "S" and !BOF()
      SELECT(cCursD)
      this.w_TMPNC = Recno(cCursD)
      GO this.w_TMPNC
      skip-1
    enddo
    this.w_lvlkeymax = rtrim(lvlkey)
    Select (cCursD)
    go top
    =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
    * --- --Mi posiziono sull'ultima scelta effettuata
    Select (this.w_cCursor)
    go bottom
    this.w_lvlkey = rtrim(lvlkey)
    if Empty(this.w_lvlkeymax)
      this.w_lvlkeymax = this.w_lvlkey
    endif
    this.w_LIVELLO = LIVELLO && OCCURS(".",this.w_lvlkey)
    this.w_LIVCUR = this.oParentObject.w_LIVCURS
    this.w_ESPLODI = .F.
    * --- --Si cicla sulle scelte effettuate per quel livello
    COND=" B.cctipoca=C.cctipoca and B.cccodice=C.cccodice and c.cctipoca='C' and c.cctipoca='C' "
    select distinct c.* from (this.w_cCursor) C where c.livello=this.w_livello and C.Rsel=1 Into Cursor SoloChiave READWRITE
    select distinct B.lvlkey1 from SoloChiave C inner join (cCursM) B on ( !("."+alltrim(str(C.cproword,3,0))+"." $ alltrim(B.ccdettag)) and &COND AND val(str(C.cproword,3,0))>0 AND B.livello=C.livcursm ) ; 
 into cursor TMPCursM readwrite
    Select("TMPCursM")
    INDEX ON LVLKEY1 TAG LVLKEY1 COLLATE "MACHINE"
    Select("TMPCursM")
    GO TOP
    Update (cCursM) set CARISP=0 , CANC=1 from TMPCursM where alltrim(TMPCursM.lvlkey1) $ alltrim(&cCursM..lvlkey1)
    use in Select("SoloChiave")
    use in Select("TMPCursM")
    * --- --Inserimento sottoalbero caratteristiche in base alle scelte effettuate
    this.w_POS = 0
    this.w_CC = 0
    this.w_oKliv = .f.
    * --- --Occorre contare se ho effettuato tutte le scelte per quel componente a quel livello
    Select SUM(SCELTA) as CARSCE, COUNT(SCELTA) as CARTOT from (cCursM) group by LVLKEY1; 
 having CARSCE=CARTOT where livello=this.w_livcur and canc<>1 into Cursor Sottoalbero
    Select (this.w_cCursor)
    this.w_AGGIORNATE = 0
    this.w_DAINSERIRE = .F.
    this.w_FINDDAINS = .T.
    use in select("TMPCursM")
    select MIN(B.livello) as livello from (cCursM) B into cursor TMPCursM where b.livello > this.w_livcur and b.cctipoca+b.cccodice not in (select CCTIPOCA+CCCODICE from (this.w_cCursor) where CANC<>1)
    Select (cCursM) 
 go top
    this.w_FINDDAINS = NVL(TMPCursM.livello, 0) > this.w_livcur
    if this.w_FINDDAINS
      this.w_livcur = NVL(TMPCursM.livello, 0)
      select distinct B.* from (cCursM) B into cursor TMPCursM where ((B.livello=this.w_livcur and B.CANC<>1) OR (B.livello=this.w_livcur and B.CARISP<>0)) ; 
 and b.cctipoca+b.cccodice not in (select CCTIPOCA+CCCODICE from (this.w_cCursor) where CANC<>1)
      this.w_DAINSERIRE = RECCOUNT("TMPCursM")>0
      update (cCursM) set RSEL=1 where livello=this.w_livcur and CANC<>1 OR (livello=this.w_livcur and CARISP<>0)
      if this.w_DAINSERIRE
        Select TMPCursM 
 go top
        do while not eof("TMPCursM")
          Select TMPCursM
          SCATTER MEMVAR MEMO
          if livello=this.w_livcur and CANC<>1 OR (livello=this.w_livcur and CARISP<>0)
            * --- --Se devo inserire la caratteristica controllo se questa appartiene gi�
            *     all'albero
            Select (this.w_cCursor)
            Locate for CCCODICE = m.cccodice and CCTIPOCA = m.cctipoca and CANC<>1
            if not found()
              this.w_DCARA = 0
              this.oParentObject.w_NUMCAR = this.oParentObject.w_NUMCAR+1
              this.w_CC = this.w_CC+1
              this.w_LVLKEY1 = Left(alltrim(this.w_LVLKEY)+"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200)
              this.w_LVLKEY1 = Left(alltrim(this.w_lvlkeymax)+"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200)
              this.w_LVLKEY3 = Left(Left(alltrim(this.w_LVLKEY1), 3) + "." +padl(alltrim(str(this.w_livcur)),3, "0") +"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200)
              m.TIPPRO="CC" 
 m.rsel=iif(m.cctipoca="D",1,0) 
 m.LIVCURSM=m.livello 
 m.LvlKey = this.w_LVLKEY1 
 m.LvlKeyp = this.w_LVLKEY1 
 m.lvlkeym = m.lvlkey1 
 m.SHOWNODE="S" 
 m.ccvalmin=0 
 m.ccvalmax=0 
 m.livtreev=2
              m.lvlkeytv = this.w_LVLKEY3
              m.errore = "N"
              m.msgerr = ""
              if m.cctipoca="M"
                this.Page_14("CALCOLA",m.cctipoca,m.cccodice,m.cc__diba,m.cccompon,m.ccroword,m.lvlkeyp)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              Select (this.w_cCursor)
              insert into (this.w_cCursor) from memvar
              Select (cCursD)
              insert into (cCursD) from memvar
              SELECT(cCursZ)
              local l_ExprSearch, l_SearchCurs
              * --- cctipoca=m.cctipoca and cccodice=m.cccodice and ccheader<>'S'  && and livello=m.livello and cc__diba=m.cc__diba
              if m.cctipoca="M"
                l_ExprSearch = "cctipoca=m.cctipoca and cccodice=m.cccodice and cc__diba=m.cc__diba and cccompon=m.cccompon and ccroword=m.ccroword and ccheader<>'S' "
                l_SearchCurs = Modelli
              else
                l_ExprSearch = "cctipoca=m.cctipoca and cccodice=m.cccodice and ccheader<>'S' "
                l_SearchCurs = cCursZ
              endif
              SELECT(l_SearchCurs)
              GO TOP
              SCAN FOR &l_ExprSearch
              SCATTER MEMVAR MEMO
              this.w_DCARA = this.w_DCARA + 1
              m.LIVCURSM=m.livcursm 
 m.LvlKey = Left(alltrim(this.w_LVLKEY1)+"."+padl(alltrim(str(this.w_DCARA)),3)+space(200), 200) 
 m.lvlkeyp = this.w_LVLKEY1 
 m.lvlkeym = m.lvlkey1 
 m.SHOWNODE="S" 
 m.livtreev=3
              m.lvlkeytv = Left(alltrim(this.w_LVLKEY3)+"."+padl(alltrim(str(this.w_DCARA)),3, "0")+space(200), 200)
              Select (cCursD)
              insert into (cCursD) from memvar
              SELECT(l_SearchCurs)
              ENDSCAN
              this.w_oKliv = .t.
              this.w_ESPLODI = .T.
              this.w_LASTLIV = M.LIVELLO
            endif
          endif
          Select TMPCursM
          if Not Eof("TMPCursM")
            Skip
          endif
          * --- --Cerco se esistono altre caratteristiche al livello n+1
          *     ho effettuato gi� le scelte per quel livello posso passare a quello successivo
        enddo
      endif
    endif
    use in select("TMPCursM")
    if reccount("Sottoalbero")=0
      if this.oParentObject.w_RIC="S" and USED(cRispDoc) and this.oParentObject.w_TESTCODE="S"
      else
        UPDATE (cCursM) set scelta=0 where livello=this.w_livcur
        Select (this.w_cCursor) 
 GO TOP 
 SCAN
        this.w_TIPCA = NVL(CCTIPOCA,"")
        this.w_CODCA = NVL(CCCODICE,"")
        this.w_RSEL = NVL(RSEL,0)
        UPDATE (cCursM) set scelta=this.w_RSEL where CCTIPOCA=this.w_TIPCA and CCCODICE=this.w_CODCA and LIVELLO=this.w_livcur
        ENDSCAN
        * --- Ripristino le caratteristiche sul livello quando cambio una scelta
        Select SUM(SCELTA) as CARSCE, COUNT(SCELTA) as CARTOT from (cCursM) group by LVLKEY1; 
 having CARSCE<CARTOT where livello=this.w_livcur and CANC=1 into Cursor Scelte
        if reccount("Scelte")=0
          * --- --Potrei rinserire le caratteristiche per quel livello ho bisogno di altre scelte per 
          *     effettuare la configurazione
          * --- --Mi posiziono su l'ultimo nodo dell'albero 
          Select (this.w_cCursor) 
 go bottom
          scatter memvar memo
          this.w_lvlkey = rtrim(m.lvlkey)
          this.w_NOC = occurs(".",this.w_lvlkey)
          this.w_lvlk = ALLTRIM(SUBSTR(this.w_LVLKEY,1, LEN(ALLTRIM(this.w_LVLKEY))-4))
          this.w_POS = LEN(ALLTRIM(this.w_LVLKEY))-(LEN(ALLTRIM(this.w_LVLKEY))-AT(".",this.w_LVLKEY)+1)
          this.w_CC = VAL(SUBSTR(this.w_LVLKEY, AT(".",this.w_LVLKEY,this.w_NOC)+1, this.w_POS))
          Select (cCursM) 
 go top
          * --- --Non ho pi� scelte da effettuare si controlla che tutte le caratteristiche
          *     associate a quel nodo siano caricate
          SCAN FOR livello=this.w_livcur 
          this.w_RIGA = recno()
          this.w_oKliv = .F.
          scatter memvar memo
          Select (this.w_cCursor) 
 go top
          Locate for CCCODICE = m.cccodice and CCTIPOCA = m.cctipoca 
          if not found()
            * --- --Si devono inserire solo le caratteristiche di quel livello che non sono gi� presenti 
            *     nei livelli successivi.
            this.w_DCARA = 0
            this.oParentObject.w_NUMCAR = this.oParentObject.w_NUMCAR+1
            this.w_CC = this.w_CC+1
            this.w_LVLKEY1 = Left(alltrim(this.w_lvlk)+"."+padl(alltrim(str(this.w_cc)),3, "0")+space(200), 200) && (alltrim(this.w_LVLKEY)+"."+padl(alltrim(str(this.w_cc)),3))
            this.w_LVLKEY3 = Left(Left(alltrim(this.w_LVLKEY1), 3) + "." +padl(alltrim(str(this.w_livello)),3, "0") +"."+padl(alltrim(str(this.w_cc)),3, "0")+ space(200), 200)
            TIPPRO="CC" 
 rsel=iif(m.cctipoca="D",1,0) 
 LIVCURSM=m.livello 
 LvlKey = this.w_LVLKEY1 
 LvlKeyp = this.w_LVLKEY1 
 SHOWNODE="S"
            lvlkeytv = this.w_LVLKEY3
            m.errore = "N"
            m.msgerr = ""
            insert into (this.w_cCursor) from memvar
            insert into (cCursD) from memvar
            SELECT(cCursZ)
            SCAN FOR cctipoca=m.cctipoca and cccodice=m.cccodice and cc__diba=m.cc__diba and livello=m.livello and ccheader<>"S"
            SCATTER MEMVAR MEMO
            this.w_DCARA = this.w_DCARA + 1
            LIVCURSM=m.livcursm 
 LvlKey = (alltrim(this.w_LVLKEY1)+"."+padl(alltrim(str(this.w_DCARA)),3, "0")) 
 lvlkeyp = this.w_LVLKEY1 
 SHOWNODE="S"
            lvlkeytv = Left(alltrim(this.w_LVLKEY3)+"."+padl(alltrim(str(this.w_DCARA)),3, "0")+ space(200), 200)
            insert into (cCursD) from memvar
            SELECT(cCursZ)
            ENDSCAN
            this.w_oKliv = .t.
          endif
          if this.w_oKliv
            Select (cCursM)
            UPDATE (cCursM) set CANC=0, rsel=0 where CCCODICE = m.cccodice and CCTIPOCA = m.cctipoca and alltrim(m.lvlkey1) $ alltrim(lvlkey1) 
            Select (cCursM)
            replace scelta with 1 for CCCODICE = m.cccodice and CCTIPOCA = m.cctipoca and CCTIPOCA<>"C"
            Go this.w_RIGA
          endif
          ENDSCAN
        endif
      endif
    endif
    use in select("Sottoalbero")
    use in select("Sottoalbero1")
    use in select("Scelte")
    use in select("SceOK")
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare il cursore ArtDist per passare i codici
    * --- Cancella gli articoli inseriti
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.oParentObject.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_VARIAR = SPACE(21)
    this.oParentObject.w_KEYRIF = SYS(2015)
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    if Type("ArtDist.cakeysal")="C"
      SCAN
      this.w_CODIAR = padr(ArtDist.Cacodart,41)
      this.w_KEYSAL = ArtDist.Cakeysal
      this.w_RIFDIS = NVL(corifdis,"")
      * --- Try
      local bErr_05508D00
      bErr_05508D00=bTrsErr
      this.Try_05508D00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into ART_TEMP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_TEMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CAKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
              +i_ccchkf ;
          +" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF);
              +" and CACODRIC = "+cp_ToStrODBC(this.w_CODIAR);
                 )
        else
          update (i_cTable) set;
              CAKEYSAL = this.w_KEYSAL;
              &i_ccchkf. ;
           where;
              CAKEYRIF = this.oParentObject.w_KEYRIF;
              and CACODRIC = this.w_CODIAR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_05508D00
      * --- End
      ENDSCAN
    else
      SCAN
      this.w_CODIAR = padr(ArtDist.Cacodart,41)
      this.w_RIFDIS = NVL(ArtDist.corifdis,"")
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIFDIS),'ART_TEMP','CACODDIS');
        +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CACODDIS',this.w_RIFDIS,'CPROWNUM',0)
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CACODDIS,CPROWNUM &i_ccchkf. );
           values (;
             this.oParentObject.w_KEYRIF;
             ,this.w_CODIAR;
             ,this.w_RIFDIS;
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    use in select("ArtDist")
  endproc
  proc Try_05508D00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CACODDIS"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFDIS),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_KEYSAL,'CACODDIS',this.w_RIFDIS,'CPROWNUM',0)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CACODDIS,CPROWNUM &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIF;
           ,this.w_CODIAR;
           ,this.w_KEYSAL;
           ,this.w_RIFDIS;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione distinta configurata con verifica e creazione codici
    Select (EXPLDB) 
 locate for ARTIPART<>"CC" and ARCONCAR="S"
    if found()
      ah_errormsg("Impossibile creare la distinta configurata,%0Alcuni componenti configurabili non sono del tipo atteso.",16)
      i_retcode = 'stop'
      return
    endif
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_cExpldb2 = SYS(2015)
    Use in Select(this.w_cExpldb2)
    * --- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    this.w_RISPOSTA = .T.
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARCCRIFE"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARCCRIFE;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ARRIFE = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_DIBAFOUND = .F.
    this.w_FOUNDSUP = .f.
    this.w_DBGEN = .f.
    if this.oParentObject.w_GENMOD<>"M"
      if TYPE("&EXPLDB..FOUND") == "U"
        Select *,space(41) as FOUND, " " as PROCESSED,len(alltrim(lvlkey)) as lenlevel from (EXPLDB) into cursor EXPLDB1 order by lenlevel desc, lvlkey desc readwrite
      else
        Select * from (EXPLDB) into cursor EXPLDB1 order by lenlevel desc, lvlkey desc readwrite
      endif
      * --- Invece di eliminare il cursore cambio il nome e lo tengo di backup
      EXPLDB = this.w_cExpldb2
      Select EXPLDB1 
 go top
      this.w_NUMLIV = OCCURS(".",lvlkey)
      do while this.w_NUMLIV > 0
        select distinct SUBSTR(lvlkey,1,ATC(".",lvlkey,this.w_NUMLIV)-1) as padre, dbartpad as tofind; 
 from EXPLDB1 where OCCURS(".",lvlkey) = this.w_NUMLIV into cursor curpadri
        Select curpadri 
 go top 
 scan
        this.w_CHIAVE = NVL(padre, "")
        this.w_TOFIND = NVL(tofind,"")
        this.w_CURNAME = sys(2015)
        Select * from EXPLDB1 where lvlkey like this.w_CHIAVE+".%" and occurs(".",lvlkey)=occurs(".",this.w_CHIAVE)+1 and processed<>"S" into cursor (this.w_CURNAME) ReadWrite
        Update EXPLDB1 set processed="S" where lvlkey like this.w_CHIAVE+".%"
        this.w_DBFOUND = ""
        this.w_ARFOUND = ""
        this.w_DIBAFOUND = .F.
        LOCAL lcDBFOUND, lcARFOUND
        STORE "" TO lcDBFOUND, lcARFOUND
        if this.oParentObject.w_VERMEM<>"T"
          * --- Non faccio nessuna verifica
          this.w_DIBAFOUND = chkdisb(this.w_CURNAME,left(this.oParentObject.w_CCCONFIG,4000),this.w_TOFIND,this.oParentObject.w_VERMEM, @lcDBFOUND, @lcARFOUND)
          this.w_DBFOUND = lcDBFOUND
          this.w_ARFOUND = lcARFOUND
        endif
        USE IN SELECT ( this.w_CURNAME )
        if this.w_DIBAFOUND AND !EMPTY(this.w_DBFOUND)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCCRIFE"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_ARFOUND);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCCRIFE;
              from (i_cTable) where;
                  ARCODART = this.w_ARFOUND;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RIFFOUND = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if alltrim(this.w_RIFFOUND)=alltrim(this.w_TOFIND)
            if this.w_NUMLIV = 1 and this.w_DBFOUND<>this.oParentObject.w_DBCODICE
              * --- Ho trovato una distinta configurata equivalente a quella che sto cercando di generare
              this.SUPREMO = this.w_ARFOUND
              this.w_FOUNDSUP = .t.
              if this.oParentObject.w_VERMEM="N"
                this.w_MESS = "E' gi� presente una distinta con i soliti componenti: %1%0Crearne una nuova?"
              else
                this.w_MESS = "E' gi� presente una distinta con i soliti componenti e scelte: %1%0Crearne una nuova?"
              endif
              this.w_RISPOSTA = ah_YesNo(this.w_MESS,"",alltrim(this.SUPREMO))
              if !this.w_RISPOSTA
                * --- Restituiisce il codice generato
                if this.oParentObject.w_DADOC 
                  * --- Se chiamato dai Doc. inizializza variabili
                  this.GSCR_KCC.oParentObject.w_MVCODICE = this.SUPREMO
                  this.GSCR_KCC.oParentObject.w_MVCODART = this.SUPREMO
                  this.GSCR_KCC.oParentObject.w_MVKEYSAL = LEFT(this.SUPREMO+REPLICATE(" ",20),20)
                  this.GSCR_KCC.oParentObject.w_MVDESSUP = this.w_CADESSUP
                  this.w_MVCODICE = this.GSCR_KCC.oParentObject.GetBodyCtrl("w_MVCODICE")
                  this.w_MVCODICE.Value = this.SUPREMO
                  this.w_MVCODICE.Check()     
                  this.GSCR_KCC.oParentObject.NotifyEvent("w_MVCODICE Changed")     
                  this.GSCR_KCC.oParentObject.w_MVTIPRIG = "R"
                  * --- Write into PAR_DISB
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PAR_DISB_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
                        +i_ccchkf ;
                    +" where ";
                        +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                           )
                  else
                    update (i_cTable) set;
                        PDBLCFGC = "N";
                        &i_ccchkf. ;
                     where;
                        PDCODAZI = i_CODAZI;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Chiude i Configuratore
                  this.Albero = .NULL.
                  this.w_NODI = .NULL.
                  this.GSCR_KCC.Hide()     
                  this.GSCR_KCC.NotifyEvent("Done")     
                  this.GSCR_KCC.Release()     
                  this.GSCR_KCC = .NULL.
                  i_retcode = 'stop'
                  return
                else
                  * --- Copia il codice Generato in memoria (appunti)
                  _CLIPTEXT = this.SUPREMO
                endif
                Select * from EXPLDB1 into cursor (EXPLDB) readwrite
              else
                * --- Try
                local bErr_05660BE0
                bErr_05660BE0=bTrsErr
                this.Try_05660BE0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                  ah_errormsg("Impossibile creare la distinta configurata",16)
                endif
                bTrsErr=bTrsErr or bErr_05660BE0
                * --- End
              endif
            endif
            Update EXPLDB1 set processed="F" , found=this.w_ARFOUND, cocodart=this.w_ARFOUND, cocodcom=this.w_ARFOUND where lvlkey == this.w_CHIAVE
            Update EXPLDB1 set processed="X", corifdis=this.w_ARFOUND where corifdis=this.w_CODICE 
          endif
        endif
        endscan
        this.w_NUMLIV = this.w_NUMLIV-1
      enddo
      * --- Se non esiste una distinta equivalente alla radice esamino tutto l'albero...
      if !this.w_FOUNDSUP
        select * from EXPLDB1 into cursor (EXPLDB) order by lvlkey readwrite
        Select (EXPLDB)
        INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
        go top
        * --- Try
        local bErr_05594F68
        bErr_05594F68=bTrsErr
        this.Try_05594F68()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_05594F68
        * --- End
      endif
    endif
    this.w_UPDMODEL = .T.
    this.w_EXISTMODEL = .T.
    if this.oParentObject.w_GENMOD<>"N"
      if !this.w_DBGEN
        * --- Aggiorna Campo Memo Distinta
        this.w_DesSup = this.w_ELSCELTE
      endif
      if this.oParentObject.w_GENMOD="M" and Empty(this.SUPREMO)
        this.SUPREMO = Alltrim(this.oParentObject.w_ARCODART)
      endif
      * --- memorizza le risposte
      this.w_CCDESSUP = "Modello " + Alltrim(this.SUPREMO)
      this.w_ANNULLA = False
      this.w_CONFKME = False
      do GSCR_KMD with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CONFKME = this.w_ANNULLA
      if this.w_CONFKME
        if !Empty(this.w_Progressivo)
          * --- Verifico se esiste modello con lo stesso nome
          * --- Read from ANA_CONF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ANA_CONF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANA_CONF_idx,2],.t.,this.ANA_CONF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" ANA_CONF where ";
                  +"CCSERIAL = "+cp_ToStrODBC(this.w_Progressivo);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CCSERIAL = this.w_Progressivo;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              this.w_EXISTMODEL = .F.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_EXISTMODEL
            this.TmpC = "Esiste gi� un modello con il codice %1%0Si desidera sovrascriverlo?"
            this.w_UPDMODEL = ah_YesNo(this.TmpC, " ", Alltrim(this.w_Progressivo))
          endif
        endif
      endif
    endif
    if this.w_CONFKME and this.oParentObject.w_GENMOD<>"N" and this.w_UPDMODEL
      * --- begin transaction
      cp_BeginTrs()
      if !Empty(this.w_Progressivo) and this.w_UPDMODEL
        * --- Delete from CONFSTAT
        i_nConn=i_TableProp[this.CONFSTAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCSERIAL = "+cp_ToStrODBC(this.w_Progressivo);
                 )
        else
          delete from (i_cTable) where;
                CCSERIAL = this.w_Progressivo;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from ANA_CONF
        i_nConn=i_TableProp[this.ANA_CONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANA_CONF_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCSERIAL = "+cp_ToStrODBC(this.w_Progressivo);
                 )
        else
          delete from (i_cTable) where;
                CCSERIAL = this.w_Progressivo;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      if Empty(this.w_Progressivo)
        this.w_Progressivo = cp_GetProg("DISMBASE","PRCCA", this.w_Progressivo, i_CODAZI)
      endif
      * --- Insert into ANA_CONF
      i_nConn=i_TableProp[this.ANA_CONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANA_CONF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANA_CONF_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCDESCRI"+",CCCODART"+",CCCODDIS"+",CCCODRIC"+",CCDESSUP"+",CC__NOTE"+",CCTICONF"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'ANA_CONF','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'ANA_CONF','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'ANA_CONF','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODICE),'ANA_CONF','CCCODDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'ANA_CONF','CCCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DesSup),'ANA_CONF','CCDESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCNOTEMO),'ANA_CONF','CC__NOTE');
        +","+cp_NullLink(cp_ToStrODBC("M"),'ANA_CONF','CCTICONF');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCDESCRI',this.w_CCDESSUP,'CCCODART',this.oParentObject.w_ARCODART,'CCCODDIS',this.oParentObject.w_DBCODICE,'CCCODRIC',this.SUPREMO,'CCDESSUP',this.w_DesSup,'CC__NOTE',this.w_CCNOTEMO,'CCTICONF',"M")
        insert into (i_cTable) (CCSERIAL,CCDESCRI,CCCODART,CCCODDIS,CCCODRIC,CCDESSUP,CC__NOTE,CCTICONF &i_ccchkf. );
           values (;
             this.w_Progressivo;
             ,this.w_CCDESSUP;
             ,this.oParentObject.w_ARCODART;
             ,this.oParentObject.w_DBCODICE;
             ,this.SUPREMO;
             ,this.w_DesSup;
             ,this.w_CCNOTEMO;
             ,"M";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      Select (this.w_cCursor)
      Scan for CCTIPOCA = "C" and rsel=1 and cproword > 0
      * --- Insert into CONFSTAT
      i_nConn=i_TableProp[this.CONFSTAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCCODRIC"+",CPROWORD"+",CCTIPOCA"+",CCDETTAG"+",CCCODICE"+",CCTICONF"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFSTAT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCTIPOCA),'CONFSTAT','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFSTAT','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(CCCODICE),'CONFSTAT','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC("M"),'CONFSTAT','CCTICONF');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODRIC',this.SUPREMO,'CPROWORD',CPROWORD,'CCTIPOCA',CCTIPOCA,'CCDETTAG',CCDETTAG,'CCCODICE',CCCODICE,'CCTICONF',"M")
        insert into (i_cTable) (CCSERIAL,CCCODRIC,CPROWORD,CCTIPOCA,CCDETTAG,CCCODICE,CCTICONF &i_ccchkf. );
           values (;
             this.w_Progressivo;
             ,this.SUPREMO;
             ,CPROWORD;
             ,CCTIPOCA;
             ,CCDETTAG;
             ,CCCODICE;
             ,"M";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      EndScan
      if used(Modelli)
        * --- Formule parametriche
        Select (this.w_cCursor)
        Scan for CCTIPOCA = "M" and rsel=1 and cproword > 0
        SCATTER MEMVAR
        this.w_i = 1
        Dimension ARRCCVAR[15] 
 Dimension ARRCCQTA[15]
        STORE "" TO ARRCCVAR 
 STORE 0 TO ARRCCQTA
        Select (Modelli)
        Scan for cccodice = m.cccodice and cctipoca = m.cctipoca and cc__diba = m.cc__diba and cccompon = m.cccompon and ccroword = m.ccroword
        ARRCCVAR[this.w_i] = Alltrim(nvl(ccdettag, ""))
        ARRCCQTA[this.w_i] = NVL(CPROWORD, 0)
        this.w_CCFORMUL = NVL(CCFORMUL, SPACE(0))
        this.w_i = this.w_i + 1
        EndScan
        * --- Insert into CONFSTAT
        i_nConn=i_TableProp[this.CONFSTAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCSERIAL"+",CCCODICE"+",CCTIPOCA"+",CCVAR101"+",CCQTA101"+",CCVAR102"+",CCQTA102"+",CCVAR103"+",CCQTA103"+",CCVAR104"+",CCQTA104"+",CCVAR105"+",CCQTA105"+",CCVAR106"+",CCQTA106"+",CCVAR107"+",CCQTA107"+",CCVAR108"+",CCQTA108"+",CCVAR109"+",CCQTA109"+",CCVAR110"+",CCQTA110"+",CCVAR111"+",CCQTA111"+",CCVAR112"+",CCQTA112"+",CCVAR113"+",CCQTA113"+",CCVAR114"+",CCQTA114"+",CCVAR115"+",CCQTA115"+",CCFORMUL"+",CCCODRIC"+",CCTICONF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(m.CCCODICE),'CONFSTAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(m.CCTIPOCA),'CONFSTAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[1]),'CONFSTAT','CCVAR101');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[1]),'CONFSTAT','CCQTA101');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[2]),'CONFSTAT','CCVAR102');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[2]),'CONFSTAT','CCQTA102');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[3]),'CONFSTAT','CCVAR103');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[3]),'CONFSTAT','CCQTA103');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[4]),'CONFSTAT','CCVAR104');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[4]),'CONFSTAT','CCQTA104');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[5]),'CONFSTAT','CCVAR105');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[5]),'CONFSTAT','CCQTA105');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[6]),'CONFSTAT','CCVAR106');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[6]),'CONFSTAT','CCQTA106');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[7]),'CONFSTAT','CCVAR107');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[7]),'CONFSTAT','CCQTA107');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[8]),'CONFSTAT','CCVAR108');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[8]),'CONFSTAT','CCQTA108');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[9]),'CONFSTAT','CCVAR109');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[9]),'CONFSTAT','CCQTA109');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[10]),'CONFSTAT','CCVAR110');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[10]),'CONFSTAT','CCQTA110');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[11]),'CONFSTAT','CCVAR111');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[11]),'CONFSTAT','CCQTA111');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[12]),'CONFSTAT','CCVAR112');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[12]),'CONFSTAT','CCQTA112');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[13]),'CONFSTAT','CCVAR113');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[13]),'CONFSTAT','CCQTA113');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[14]),'CONFSTAT','CCVAR114');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[14]),'CONFSTAT','CCQTA114');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[15]),'CONFSTAT','CCVAR115');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[15]),'CONFSTAT','CCQTA115');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCFORMUL),'CONFSTAT','CCFORMUL');
          +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
          +","+cp_NullLink(cp_ToStrODBC("M"),'CONFSTAT','CCTICONF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODICE',m.CCCODICE,'CCTIPOCA',m.CCTIPOCA,'CCVAR101',ARRCCVAR[1],'CCQTA101',ARRCCQTA[1],'CCVAR102',ARRCCVAR[2],'CCQTA102',ARRCCQTA[2],'CCVAR103',ARRCCVAR[3],'CCQTA103',ARRCCQTA[3],'CCVAR104',ARRCCVAR[4],'CCQTA104',ARRCCQTA[4],'CCVAR105',ARRCCVAR[5])
          insert into (i_cTable) (CCSERIAL,CCCODICE,CCTIPOCA,CCVAR101,CCQTA101,CCVAR102,CCQTA102,CCVAR103,CCQTA103,CCVAR104,CCQTA104,CCVAR105,CCQTA105,CCVAR106,CCQTA106,CCVAR107,CCQTA107,CCVAR108,CCQTA108,CCVAR109,CCQTA109,CCVAR110,CCQTA110,CCVAR111,CCQTA111,CCVAR112,CCQTA112,CCVAR113,CCQTA113,CCVAR114,CCQTA114,CCVAR115,CCQTA115,CCFORMUL,CCCODRIC,CCTICONF &i_ccchkf. );
             values (;
               this.w_Progressivo;
               ,m.CCCODICE;
               ,m.CCTIPOCA;
               ,ARRCCVAR[1];
               ,ARRCCQTA[1];
               ,ARRCCVAR[2];
               ,ARRCCQTA[2];
               ,ARRCCVAR[3];
               ,ARRCCQTA[3];
               ,ARRCCVAR[4];
               ,ARRCCQTA[4];
               ,ARRCCVAR[5];
               ,ARRCCQTA[5];
               ,ARRCCVAR[6];
               ,ARRCCQTA[6];
               ,ARRCCVAR[7];
               ,ARRCCQTA[7];
               ,ARRCCVAR[8];
               ,ARRCCQTA[8];
               ,ARRCCVAR[9];
               ,ARRCCQTA[9];
               ,ARRCCVAR[10];
               ,ARRCCQTA[10];
               ,ARRCCVAR[11];
               ,ARRCCQTA[11];
               ,ARRCCVAR[12];
               ,ARRCCQTA[12];
               ,ARRCCVAR[13];
               ,ARRCCQTA[13];
               ,ARRCCVAR[14];
               ,ARRCCQTA[14];
               ,ARRCCVAR[15];
               ,ARRCCQTA[15];
               ,this.w_CCFORMUL;
               ,this.SUPREMO;
               ,"M";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        Select (this.w_cCursor)
        EndScan
      endif
      if used(Modelli)
        * --- caratteristiche descrittive
        Select (this.w_cCursor)
        Scan for CCTIPOCA = "D" AND CCFORMUL="OK"
        * --- Insert into CONFSTAT
        i_nConn=i_TableProp[this.CONFSTAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCSERIAL"+",CCCODICE"+",CCTIPOCA"+",CC__NOTE"+",CCCODRIC"+",CCTICONF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(CCCODICE),'CONFSTAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(CCTIPOCA),'CONFSTAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFSTAT','CC__NOTE');
          +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
          +","+cp_NullLink(cp_ToStrODBC("M"),'CONFSTAT','CCTICONF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODICE',CCCODICE,'CCTIPOCA',CCTIPOCA,'CC__NOTE',CCDETTAG,'CCCODRIC',this.SUPREMO,'CCTICONF',"M")
          insert into (i_cTable) (CCSERIAL,CCCODICE,CCTIPOCA,CC__NOTE,CCCODRIC,CCTICONF &i_ccchkf. );
             values (;
               this.w_Progressivo;
               ,CCCODICE;
               ,CCTIPOCA;
               ,CCDETTAG;
               ,this.SUPREMO;
               ,"M";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        Select (this.w_cCursor)
        EndScan
      endif
      * --- commit
      cp_EndTrs(.t.)
    endif
    if this.w_DBGEN
      * --- begin transaction
      cp_BeginTrs()
      * --- memorizza le risposte
      this.w_Progressivo = cp_GetProg("DISMBASE","PRCCA", this.w_Progressivo, i_CODAZI)
      this.w_CCDESSUP = "Configurazione " + Alltrim(this.SUPREMO)
      * --- Insert into ANA_CONF
      i_nConn=i_TableProp[this.ANA_CONF_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANA_CONF_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANA_CONF_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCDESCRI"+",CCCODART"+",CCCODDIS"+",CCCODRIC"+",CCDESSUP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'ANA_CONF','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'ANA_CONF','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'ANA_CONF','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODICE),'ANA_CONF','CCCODDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'ANA_CONF','CCCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DesSup),'ANA_CONF','CCDESSUP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCDESCRI',this.w_CCDESSUP,'CCCODART',this.oParentObject.w_ARCODART,'CCCODDIS',this.oParentObject.w_DBCODICE,'CCCODRIC',this.SUPREMO,'CCDESSUP',this.w_DesSup)
        insert into (i_cTable) (CCSERIAL,CCDESCRI,CCCODART,CCCODDIS,CCCODRIC,CCDESSUP &i_ccchkf. );
           values (;
             this.w_Progressivo;
             ,this.w_CCDESSUP;
             ,this.oParentObject.w_ARCODART;
             ,this.oParentObject.w_DBCODICE;
             ,this.SUPREMO;
             ,this.w_DesSup;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      Select (this.w_cCursor)
      Scan for CCTIPOCA = "C"
      * --- Insert into CONFSTAT
      i_nConn=i_TableProp[this.CONFSTAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCCODRIC"+",CPROWORD"+",CCTIPOCA"+",CCDETTAG"+",CCCODICE"+",CCTICONF"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFSTAT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCTIPOCA),'CONFSTAT','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFSTAT','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(CCCODICE),'CONFSTAT','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC("C"),'CONFSTAT','CCTICONF');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODRIC',this.SUPREMO,'CPROWORD',CPROWORD,'CCTIPOCA',CCTIPOCA,'CCDETTAG',CCDETTAG,'CCCODICE',CCCODICE,'CCTICONF',"C")
        insert into (i_cTable) (CCSERIAL,CCCODRIC,CPROWORD,CCTIPOCA,CCDETTAG,CCCODICE,CCTICONF &i_ccchkf. );
           values (;
             this.w_Progressivo;
             ,this.SUPREMO;
             ,CPROWORD;
             ,CCTIPOCA;
             ,CCDETTAG;
             ,CCCODICE;
             ,"C";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      EndScan
      if used(Modelli)
        * --- Formule parametriche
        Select (this.w_cCursor)
        Scan for CCTIPOCA = "M"
        SCATTER MEMVAR
        this.w_i = 1
        Dimension ARRCCVAR[15] 
 Dimension ARRCCQTA[15]
        STORE "" TO ARRCCVAR 
 STORE 0 TO ARRCCQTA
        Select (Modelli)
        Scan for cccodice = m.cccodice and cctipoca = m.cctipoca and cc__diba = m.cc__diba and cccompon = m.cccompon and ccroword = m.ccroword
        ARRCCVAR[this.w_i] = Alltrim(nvl(ccdettag, ""))
        ARRCCQTA[this.w_i] = NVL(CPROWORD, 0)
        this.w_CCFORMUL = ccformul
        this.w_i = this.w_i + 1
        EndScan
        * --- Insert into CONFSTAT
        i_nConn=i_TableProp[this.CONFSTAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCSERIAL"+",CCCODICE"+",CCTIPOCA"+",CCVAR101"+",CCQTA101"+",CCVAR102"+",CCQTA102"+",CCVAR103"+",CCQTA103"+",CCVAR104"+",CCQTA104"+",CCVAR105"+",CCQTA105"+",CCVAR106"+",CCQTA106"+",CCVAR107"+",CCQTA107"+",CCVAR108"+",CCQTA108"+",CCVAR109"+",CCQTA109"+",CCVAR110"+",CCQTA110"+",CCVAR111"+",CCQTA111"+",CCVAR112"+",CCQTA112"+",CCVAR113"+",CCQTA113"+",CCVAR114"+",CCQTA114"+",CCVAR115"+",CCQTA115"+",CCFORMUL"+",CCCODRIC"+",CCTICONF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(m.CCCODICE),'CONFSTAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(m.CCTIPOCA),'CONFSTAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[1]),'CONFSTAT','CCVAR101');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[1]),'CONFSTAT','CCQTA101');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[2]),'CONFSTAT','CCVAR102');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[2]),'CONFSTAT','CCQTA102');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[3]),'CONFSTAT','CCVAR103');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[3]),'CONFSTAT','CCQTA103');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[4]),'CONFSTAT','CCVAR104');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[4]),'CONFSTAT','CCQTA104');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[5]),'CONFSTAT','CCVAR105');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[5]),'CONFSTAT','CCQTA105');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[6]),'CONFSTAT','CCVAR106');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[6]),'CONFSTAT','CCQTA106');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[7]),'CONFSTAT','CCVAR107');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[7]),'CONFSTAT','CCQTA107');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[8]),'CONFSTAT','CCVAR108');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[8]),'CONFSTAT','CCQTA108');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[9]),'CONFSTAT','CCVAR109');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[9]),'CONFSTAT','CCQTA109');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[10]),'CONFSTAT','CCVAR110');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[10]),'CONFSTAT','CCQTA110');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[11]),'CONFSTAT','CCVAR111');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[11]),'CONFSTAT','CCQTA111');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[12]),'CONFSTAT','CCVAR112');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[12]),'CONFSTAT','CCQTA112');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[13]),'CONFSTAT','CCVAR113');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[13]),'CONFSTAT','CCQTA113');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[14]),'CONFSTAT','CCVAR114');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[14]),'CONFSTAT','CCQTA114');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCVAR[15]),'CONFSTAT','CCVAR115');
          +","+cp_NullLink(cp_ToStrODBC(ARRCCQTA[15]),'CONFSTAT','CCQTA115');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCFORMUL),'CONFSTAT','CCFORMUL');
          +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
          +","+cp_NullLink(cp_ToStrODBC("C"),'CONFSTAT','CCTICONF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODICE',m.CCCODICE,'CCTIPOCA',m.CCTIPOCA,'CCVAR101',ARRCCVAR[1],'CCQTA101',ARRCCQTA[1],'CCVAR102',ARRCCVAR[2],'CCQTA102',ARRCCQTA[2],'CCVAR103',ARRCCVAR[3],'CCQTA103',ARRCCQTA[3],'CCVAR104',ARRCCVAR[4],'CCQTA104',ARRCCQTA[4],'CCVAR105',ARRCCVAR[5])
          insert into (i_cTable) (CCSERIAL,CCCODICE,CCTIPOCA,CCVAR101,CCQTA101,CCVAR102,CCQTA102,CCVAR103,CCQTA103,CCVAR104,CCQTA104,CCVAR105,CCQTA105,CCVAR106,CCQTA106,CCVAR107,CCQTA107,CCVAR108,CCQTA108,CCVAR109,CCQTA109,CCVAR110,CCQTA110,CCVAR111,CCQTA111,CCVAR112,CCQTA112,CCVAR113,CCQTA113,CCVAR114,CCQTA114,CCVAR115,CCQTA115,CCFORMUL,CCCODRIC,CCTICONF &i_ccchkf. );
             values (;
               this.w_Progressivo;
               ,m.CCCODICE;
               ,m.CCTIPOCA;
               ,ARRCCVAR[1];
               ,ARRCCQTA[1];
               ,ARRCCVAR[2];
               ,ARRCCQTA[2];
               ,ARRCCVAR[3];
               ,ARRCCQTA[3];
               ,ARRCCVAR[4];
               ,ARRCCQTA[4];
               ,ARRCCVAR[5];
               ,ARRCCQTA[5];
               ,ARRCCVAR[6];
               ,ARRCCQTA[6];
               ,ARRCCVAR[7];
               ,ARRCCQTA[7];
               ,ARRCCVAR[8];
               ,ARRCCQTA[8];
               ,ARRCCVAR[9];
               ,ARRCCQTA[9];
               ,ARRCCVAR[10];
               ,ARRCCQTA[10];
               ,ARRCCVAR[11];
               ,ARRCCQTA[11];
               ,ARRCCVAR[12];
               ,ARRCCQTA[12];
               ,ARRCCVAR[13];
               ,ARRCCQTA[13];
               ,ARRCCVAR[14];
               ,ARRCCQTA[14];
               ,ARRCCVAR[15];
               ,ARRCCQTA[15];
               ,this.w_CCFORMUL;
               ,this.SUPREMO;
               ,"C";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        Select (this.w_cCursor)
        EndScan
      endif
      if used(Modelli)
        * --- caratteristiche descrittive
        Select (this.w_cCursor)
        Scan for CCTIPOCA = "D" AND CCFORMUL="OK"
        * --- Insert into CONFSTAT
        i_nConn=i_TableProp[this.CONFSTAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFSTAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFSTAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCSERIAL"+",CCCODICE"+",CCTIPOCA"+",CC__NOTE"+",CCCODRIC"+",CCTICONF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFSTAT','CCSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(CCCODICE),'CONFSTAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(CCTIPOCA),'CONFSTAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFSTAT','CC__NOTE');
          +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFSTAT','CCCODRIC');
          +","+cp_NullLink(cp_ToStrODBC("C"),'CONFSTAT','CCTICONF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODICE',CCCODICE,'CCTIPOCA',CCTIPOCA,'CC__NOTE',CCDETTAG,'CCCODRIC',this.SUPREMO,'CCTICONF',"C")
          insert into (i_cTable) (CCSERIAL,CCCODICE,CCTIPOCA,CC__NOTE,CCCODRIC,CCTICONF &i_ccchkf. );
             values (;
               this.w_Progressivo;
               ,CCCODICE;
               ,CCTIPOCA;
               ,CCDETTAG;
               ,this.SUPREMO;
               ,"C";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        Select (this.w_cCursor)
        EndScan
      endif
      this.w_KEYSAL = LEFT(alltrim(this.SUPREMO)+REPL(" ",20),20)
      this.oParentObject.w_CCCONFIG = left(this.oParentObject.w_CCCONFIG,4000)
      * --- Insert into CONFMEMO
      i_nConn=i_TableProp[this.CONFMEMO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFMEMO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFMEMO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCSERIAL"+",CCCODIDX"+",CCCONFIG"+",CCKEYSAL"+",CCCODRIC"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFMEMO','CCSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODIDX),'CONFMEMO','CCCODIDX');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCCONFIG),'CONFMEMO','CCCONFIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'CONFMEMO','CCKEYSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'CONFMEMO','CCCODRIC');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODIDX',this.w_CCCODIDX,'CCCONFIG',this.oParentObject.w_CCCONFIG,'CCKEYSAL',this.w_KEYSAL,'CCCODRIC',this.SUPREMO)
        insert into (i_cTable) (CCSERIAL,CCCODIDX,CCCONFIG,CCKEYSAL,CCCODRIC &i_ccchkf. );
           values (;
             this.w_Progressivo;
             ,this.w_CCCODIDX;
             ,this.oParentObject.w_CCCONFIG;
             ,this.w_KEYSAL;
             ,this.SUPREMO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- commit
      cp_EndTrs(.t.)
      if g_PRFA="S"
        * --- Duplica i cicli
        this.Page_18()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.oParentObject.w_CCSERIAL = this.w_Progressivo
      if this.oParentObject.w_DADOC 
        this.w_OBJDOC = this.GSCR_KCC.oParentObject
        * --- Se chiamato dai Doc. inizializza variabili
        this.GSCR_KCC.oParentObject.w_MVCODICE = this.SUPREMO
        this.GSCR_KCC.oParentObject.w_MVCODART = this.SUPREMO
        this.GSCR_KCC.oParentObject.w_MVKEYSAL = LEFT(this.SUPREMO+REPLICATE(" ",20),20)
        this.w_OBJDOC.w_MVDESSUP = this.w_CADESSUP
        this.w_MVCODICE = this.GSCR_KCC.oParentObject.GetBodyCtrl("w_MVCODICE")
        this.w_MVCODICE.Value = this.SUPREMO
        this.w_MVCODICE.Check()     
        this.GSCR_KCC.oParentObject.NotifyEvent("w_MVCODICE Changed")     
        this.GSCR_KCC.oParentObject.w_MVTIPRIG = "R"
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARRIFE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ARCODART = this.w_ARRIFE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.oParentObject.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.oParentObject.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.oParentObject.oParentObject.w_MVCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
          this.oParentObject.oParentObject.w_MVNOMENC = NVL(cp_ToDate(_read_.ARNOMENC),cp_NullValue(_read_.ARNOMENC))
          this.oParentObject.oParentObject.w_MVUMSUPP = NVL(cp_ToDate(_read_.ARUMSUPP),cp_NullValue(_read_.ARUMSUPP))
          this.oParentObject.oParentObject.w_MVMOLSUP = NVL(cp_ToDate(_read_.ARMOLSUP),cp_NullValue(_read_.ARMOLSUP))
          this.oParentObject.oParentObject.w_MVPESNET = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
          this.oParentObject.oParentObject.w_MVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
          this.oParentObject.oParentObject.w_ARTDIS = NVL(cp_ToDate(_read_.ARFLDISP),cp_NullValue(_read_.ARFLDISP))
          this.oParentObject.oParentObject.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
          this.oParentObject.oParentObject.w_VOCCEN = NVL(cp_ToDate(_read_.ARVOCCEN),cp_NullValue(_read_.ARVOCCEN))
          this.oParentObject.oParentObject.w_VOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
          this.oParentObject.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
          this.oParentObject.oParentObject.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          this.oParentObject.oParentObject.w_FLSERA = NVL(cp_ToDate(_read_.ARTIPSER),cp_NullValue(_read_.ARTIPSER))
          this.oParentObject.oParentObject.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
          this.oParentObject.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
          this.oParentObject.oParentObject.w_FLDISC = NVL(cp_ToDate(_read_.ARFLDISC),cp_NullValue(_read_.ARFLDISC))
          this.oParentObject.oParentObject.w_GESMAT = NVL(cp_ToDate(_read_.ARGESMAT),cp_NullValue(_read_.ARGESMAT))
          this.oParentObject.oParentObject.w_TPCON1 = NVL(cp_ToDate(_read_.ARTPCONF),cp_NullValue(_read_.ARTPCONF))
          this.oParentObject.oParentObject.w_TPCON2 = NVL(cp_ToDate(_read_.ARTPCON2),cp_NullValue(_read_.ARTPCON2))
          this.oParentObject.oParentObject.w_ARTPRO = NVL(cp_ToDate(_read_.ARGRUPRO),cp_NullValue(_read_.ARGRUPRO))
          this.oParentObject.oParentObject.w_DISLOT = NVL(cp_ToDate(_read_.ARDISLOT),cp_NullValue(_read_.ARDISLOT))
          this.oParentObject.oParentObject.w_FLCESP = NVL(cp_ToDate(_read_.ARFLCESP),cp_NullValue(_read_.ARFLCESP))
          this.oParentObject.oParentObject.w_CODCEN = NVL(cp_ToDate(_read_.ARCODCEN),cp_NullValue(_read_.ARCODCEN))
          this.oParentObject.oParentObject.w_CODDIS = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
          this.oParentObject.oParentObject.w_FLESIM = NVL(cp_ToDate(_read_.ARFLESIM),cp_NullValue(_read_.ARFLESIM))
          this.oParentObject.oParentObject.w_PRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
          this.oParentObject.oParentObject.w_ARFLAPCA = NVL(cp_ToDate(_read_.ARFLAPCA),cp_NullValue(_read_.ARFLAPCA))
          this.oParentObject.oParentObject.w_ARRIPCON = NVL(cp_ToDate(_read_.ARRIPCON),cp_NullValue(_read_.ARRIPCON))
          this.oParentObject.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          this.oParentObject.oParentObject.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
          this.oParentObject.oParentObject.w_DATINTR = NVL(cp_ToDate(_read_.ARDATINT),cp_NullValue(_read_.ARDATINT))
          this.oParentObject.oParentObject.w_ARCHKUCA = NVL(cp_ToDate(_read_.ARCHKUCA),cp_NullValue(_read_.ARCHKUCA))
          this.oParentObject.oParentObject.w_ARTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
          this.oParentObject.oParentObject.w_ARMINVEN = NVL(cp_ToDate(_read_.ARMINVEN),cp_NullValue(_read_.ARMINVEN))
          this.oParentObject.oParentObject.w_FLCOM1 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          this.oParentObject.oParentObject.w_ARCLAMAT = NVL(cp_ToDate(_read_.ARCLAMAT),cp_NullValue(_read_.ARCLAMAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if not used (cRispDoc)
          Select * ,space(41) as Supdb from (this.w_cCursor) into cursor (cRispDoc) readwrite
        endif
        update (cRispDoc) set SupDB=this.w_Supremo
        * --- Chiude i Configuratore
        * --- Write into PAR_DISB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_DISB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
              +i_ccchkf ;
          +" where ";
              +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              PDBLCFGC = "N";
              &i_ccchkf. ;
           where;
              PDCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.Albero = .NULL.
        this.w_NODI = .NULL.
        this.GSCR_KCC.Hide()     
        this.GSCR_KCC.NotifyEvent("Done")     
        this.GSCR_KCC.Release()     
        this.GSCR_KCC = .NULL.
        i_retcode = 'stop'
        return
      else
        * --- Se chiamato dal Conf. inizializza variabili
        use in Select(cRispDoc)
        Select * ,space(20) as Supdb from (this.w_cCursor) into cursor (cRispDoc) readwrite
        update (cRispDoc) set SupDB=this.w_Supremo
        * --- Copia il codice Generato in memoria (appunti)
        * --- Write into PAR_DISB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_DISB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_DISB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDBLCFGC ="+cp_NullLink(cp_ToStrODBC("N"),'PAR_DISB','PDBLCFGC');
              +i_ccchkf ;
          +" where ";
              +"PDCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              PDBLCFGC = "N";
              &i_ccchkf. ;
           where;
              PDCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_cTMP = "Creazione distinta configurata completata con successo.%0 Attribuito codice: %1"
        ah_errormsg(this.w_cTMP,48,"",this.SUPREMO)
        _CLIPTEXT = this.SUPREMO
      endif
      * --- Restituiisce il codice generato
      * --- Chiude Maschera configuratore
      if this.w_OPER = "CREADIS_CLOSE"
        * --- Ripristino il vecchio cursore ho detto di non generare la distinta
        USE IN SELECT(this.w_EXPLDB)
        EXPLDB = this.oParentObject.w_cExpldb
        if this.oParentObject.w_RIC="S"
          Select (this.w_cCursor) 
 GO TOP
          this.GSCR_KCC.w_CCCODICE = CCCODICE
          this.GSCR_KCC.Refresh()     
        else
          this.oParentObject.w_CCCONFIG = ""
          this.oParentObject.w_TESTCODE = "N"
        endif
        if this.oParentObject.w_RICDOC
          this.GSCR_KCC.NotifyEvent("SALVADIS")     
        endif
      endif
    else
      * --- Ripristino il vecchio cursore ho detto di non generare la distinta
      USE IN SELECT(EXPLDB)
      USE IN SELECT(this.w_EXPLDB)
      EXPLDB = this.oParentObject.w_cExpldb
    endif
    use in Select("Curpadri")
    use in Select("EXPLDB1")
  endproc
  proc Try_05660BE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Genero il codice della distinta padre
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_ARCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      this.w_CCRIFE = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
      this.w_CCCHAR = NVL(cp_ToDate(_read_.ARCCCHAR),cp_NullValue(_read_.ARCCCHAR))
      this.w_CCLUNG = NVL(cp_ToDate(_read_.ARCCLUNG),cp_NullValue(_read_.ARCCLUNG))
      this.w_CCPROG = NVL(cp_ToDate(_read_.ARCCPROG),cp_NullValue(_read_.ARCCPROG))
      this.w_VIR_DESCRI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
      this.w_VIR_DESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_RIF = LEFT(this.oParentObject.w_ARCODART,20)
    this.w_TEST = ""
    this.w_KEYPROG = calccprod(this.w_CCPROG,this.w_CCLUNG)
    this.w_NEWCODE = alltrim(this.oParentObject.w_ARCODART)+this.w_CCCHAR+this.w_KEYPROG
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODICE"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODICE;
        from (i_cTable) where;
            CACODICE = this.w_NEWCODE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do while !EMPTY(this.w_TEST)
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
               )
      else
        update (i_cTable) set;
            ARCCPROG = this.w_KEYPROG;
            &i_ccchkf. ;
         where;
            ARCODART = this.oParentObject.w_ARCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_KEYPROG = calccprod(this.w_KEYPROG,this.w_CCLUNG)
      this.w_NEWCODE = alltrim(this.oParentObject.w_ARCODART)+this.w_CCCHAR+this.w_KEYPROG
      this.w_TEST = ""
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE;
          from (i_cTable) where;
              CACODICE = this.w_NEWCODE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    enddo
    this.SUPREMO = this.w_NEWCODE
    this.w_KEYSAL = this.w_RIF
    * --- Se ho modificato in maschera la descrizione dell'articolo allora la utilizzo
    if Alltrim(this.w_VIR_DESCRI) # Alltrim(this.oParentObject.w_ARDESART)
      this.w_VIR_DESCRI = this.oParentObject.w_ARDESART
    endif
    Update EXPLDB1 set corifdis=this.w_NEWCODE where corifdis=this.w_DBFOUND
    * --- Insert into ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR7BC4",this.ART_ICOL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_VIR_DESSUP = iif(!EMPTY(this.w_VIR_DESSUP),this.w_VIR_DESSUP+chr(13),"")+this.w_ELSCELTE
    this.w_VIR_DESSUP = left(this.w_VIR_DESSUP,iif(CP_DBTYPE="Oracle",2000,4000))
    this.w_ELSCELTE = left(this.w_ELSCELTE,iif(CP_DBTYPE="Oracle",2000,4000))
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'ART_ICOL','ARDESART');
      +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESSUP),'ART_ICOL','ARDESSUP');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_NEWCODE);
             )
    else
      update (i_cTable) set;
          ARDESART = this.w_VIR_DESCRI;
          ,ARDESSUP = this.w_VIR_DESSUP;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_NEWCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR6bc4",this.KEY_ARTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ELSCELTE),'KEY_ARTI','CADESSUP');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
             )
    else
      update (i_cTable) set;
          CADESSUP = this.w_ELSCELTE;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_NEWCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TRADARTI
    i_nConn=i_TableProp[this.TRADARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2],.t.,this.TRADARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select LGCODLIN,LGDESCOD,LGDESSUP  from "+i_cTable+" TRADARTI ";
          +" where LGCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE)+"";
           ,"_Curs_TRADARTI")
    else
      select LGCODLIN,LGDESCOD,LGDESSUP from (i_cTable);
       where LGCODICE = this.oParentObject.w_DBCODICE;
        into cursor _Curs_TRADARTI
    endif
    if used('_Curs_TRADARTI')
      select _Curs_TRADARTI
      locate for 1=1
      do while not(eof())
      * --- Insert into TRADARTI
      i_nConn=i_TableProp[this.TRADARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGCODLIN),'TRADARTI','LGCODLIN');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESCOD),'TRADARTI','LGDESCOD');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESSUP),'TRADARTI','LGDESSUP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',_Curs_TRADARTI.LGCODLIN,'LGDESCOD',_Curs_TRADARTI.LGDESCOD,'LGDESSUP',_Curs_TRADARTI.LGDESSUP)
        insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
           values (;
             this.w_NEWCODE;
             ,_Curs_TRADARTI.LGCODLIN;
             ,_Curs_TRADARTI.LGDESCOD;
             ,_Curs_TRADARTI.LGDESSUP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_TRADARTI
        continue
      enddo
      use
    endif
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR8BC4",this.PAR_RIOR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if not empty(nvl(this.oParentObject.w_CODLIN,""))
      * --- Write into TRADARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TRADARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LGDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
            +i_ccchkf ;
        +" where ";
            +"LGCODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
            +" and LGCODICE = "+cp_ToStrODBC(this.w_NEWCODE);
               )
      else
        update (i_cTable) set;
            LGDESSUP = this.w_TRADUZ;
            &i_ccchkf. ;
         where;
            LGCODLIN = this.oParentObject.w_CODLIN;
            and LGCODICE = this.w_NEWCODE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_ROWS=0
        * --- Insert into TRADARTI
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLIN),'TRADARTI','LGCODLIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'TRADARTI','LGDESCOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',this.oParentObject.w_CODLIN,'LGDESCOD',this.w_VIR_DESCRI,'LGDESSUP',this.w_TRADUZ)
          insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
             values (;
               this.w_NEWCODE;
               ,this.oParentObject.w_CODLIN;
               ,this.w_VIR_DESCRI;
               ,this.w_TRADUZ;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Aggiorno il progressivo...
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
             )
    else
      update (i_cTable) set;
          ARCCPROG = this.w_KEYPROG;
          &i_ccchkf. ;
       where;
          ARCODART = this.oParentObject.w_ARCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Creo la nuova distinta con i componenti gi� presenti sul gestionale
    Select * from EXPLDB1 into cursor (EXPLDB) order by lvlkey asc 
 go top
    if &EXPLDB..ARCONCAR="S" and (&EXPLDB..TIPPAD<>"DC" or recno()=1)
      * --- Crea Nuova Distinta Base
      this.w_DesDis = this.oParentObject.w_DBDESCRI
      this.w_DBDATDIS = cp_CharToDate("31/12/2099")
      this.w_DBPRIORI = &EXPLDB..DBPRIORI
      this.w_DBTIPDIS = &EXPLDB..DBTIPDIS
      this.w_DBCODRIS = &EXPLDB..DBCODRIS
      * --- Insert into DISMBASE
      i_nConn=i_TableProp[this.DISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISMBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DBCODICE"+",DBDESCRI"+",DBFLSTAT"+",UTDC"+",DBDTINVA"+",DBDTOBSO"+",DBNOTAGG"+",DBCONGES"+",DBTIPGEN"+",DBPRIORI"+",DBTIPDIS"+",DBCODRIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'DISMBASE','DBCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DesDis),'DISMBASE','DBDESCRI');
        +","+cp_NullLink(cp_ToStrODBC("S"),'DISMBASE','DBFLSTAT');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISMBASE','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISMBASE','DBDTINVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DBDATDIS),'DISMBASE','DBDTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESSUP),'DISMBASE','DBNOTAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'DISMBASE','DBCONGES');
        +","+cp_NullLink(cp_ToStrODBC("C"),'DISMBASE','DBTIPGEN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DBPRIORI),'DISMBASE','DBPRIORI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DBTIPDIS),'DISMBASE','DBTIPDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DBCODRIS),'DISMBASE','DBCODRIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_NEWCODE,'DBDESCRI',this.w_DesDis,'DBFLSTAT',"S",'UTDC',i_DATSYS,'DBDTINVA',i_DATSYS,'DBDTOBSO',this.w_DBDATDIS,'DBNOTAGG',this.w_VIR_DESSUP,'DBCONGES',this.SUPREMO,'DBTIPGEN',"C",'DBPRIORI',this.w_DBPRIORI,'DBTIPDIS',this.w_DBTIPDIS,'DBCODRIS',this.w_DBCODRIS)
        insert into (i_cTable) (DBCODICE,DBDESCRI,DBFLSTAT,UTDC,DBDTINVA,DBDTOBSO,DBNOTAGG,DBCONGES,DBTIPGEN,DBPRIORI,DBTIPDIS,DBCODRIS &i_ccchkf. );
           values (;
             this.w_NEWCODE;
             ,this.w_DesDis;
             ,"S";
             ,i_DATSYS;
             ,i_DATSYS;
             ,this.w_DBDATDIS;
             ,this.w_VIR_DESSUP;
             ,this.SUPREMO;
             ,"C";
             ,this.w_DBPRIORI;
             ,this.w_DBTIPDIS;
             ,this.w_DBCODRIS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'ART_ICOL','ARCODDIS');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_NEWCODE);
               )
      else
        update (i_cTable) set;
            ARCODDIS = this.w_NEWCODE;
            &i_ccchkf. ;
         where;
            ARCODART = this.w_NEWCODE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if g_PRFA="S"
        * --- Insert into ART_DIST
        i_nConn=i_TableProp[this.ART_DIST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_DIST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DICODART"+",DICODDIS"+",DIFLPREF"+",DITIPDIS"+",CPROWORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'ART_DIST','DICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'ART_DIST','DICODDIS');
          +","+cp_NullLink(cp_ToStrODBC("S"),'ART_DIST','DIFLPREF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DBTIPDIS),'ART_DIST','DITIPDIS');
          +","+cp_NullLink(cp_ToStrODBC(10),'ART_DIST','CPROWORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DICODART',this.w_NEWCODE,'DICODDIS',this.w_NEWCODE,'DIFLPREF',"S",'DITIPDIS',this.w_DBTIPDIS,'CPROWORD',10)
          insert into (i_cTable) (DICODART,DICODDIS,DIFLPREF,DITIPDIS,CPROWORD &i_ccchkf. );
             values (;
               this.w_NEWCODE;
               ,this.w_NEWCODE;
               ,"S";
               ,this.w_DBTIPDIS;
               ,10;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Legge i figli diretti
    this.w_LenSez = 4
    this.w_cTMP = &EXPLDB..LVLKEY
    this.w_nTMP = len(rTrim(this.w_cTMP))
    this.w_cTMP1 = NVL(&EXPLDB..CODICEDIBA, SPACE(41))
    select * from (EXPLDB) ;
    where left(LVLKEY,this.w_nTMP)=rtrim(this.w_cTMP) and len(rTrim(LvlKey))=this.w_nTMP+this.w_LenSez ;
    order by LVLKEY into cursor __BCP__ nofilter
    this.w_nTMP = 0
    select __BCP__
    SCAN
    this.w_COCODCOM = iif(EMPTY(__BCP__.FOUND), __BCP__.COCODCOM,__BCP__.FOUND)
    this.w_COCODART = __BCP__.COCODART
    this.w_CODESSUP = IIF(empty(nvl(__bcp__.CODESSUP,"")),nvl(__bcp__.DESSUP,""),__BCP__.CODESSUP)
    * --- Progressivo di riga ...
    this.w_nTMP = this.w_nTMP+1
    * --- Crea riga distinta configurata
    this.w_COXCHKGE = 1
    * --- Try
    local bErr_05681308
    bErr_05681308=bTrsErr
    this.Try_05681308()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Riga configurata gi� inserita
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_05681308
    * --- End
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    this.w_DBGEN = .t.
    return
  proc Try_05594F68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Genero il codice della distinta padre
    this.w_CodArt = &EXPLDB..CACODART
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
      this.w_CCRIFE = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
      this.w_CCCHAR = NVL(cp_ToDate(_read_.ARCCCHAR),cp_NullValue(_read_.ARCCCHAR))
      this.w_CCLUNG = NVL(cp_ToDate(_read_.ARCCLUNG),cp_NullValue(_read_.ARCCLUNG))
      this.w_CCPROG = NVL(cp_ToDate(_read_.ARCCPROG),cp_NullValue(_read_.ARCCPROG))
      this.w_VIR_DESCRI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
      this.w_VIR_DESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_RIF = this.w_CODART
    this.w_TEST = ""
    this.w_KEYPROG = calccprod(this.w_CCPROG,this.w_CCLUNG)
    this.w_NEWCODE = alltrim(this.w_CODART)+this.w_CCCHAR+this.w_KEYPROG
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODICE"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODICE;
        from (i_cTable) where;
            CACODICE = this.w_NEWCODE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do while !EMPTY(this.w_TEST)
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               )
      else
        update (i_cTable) set;
            ARCCPROG = this.w_KEYPROG;
            &i_ccchkf. ;
         where;
            ARCODART = this.w_CODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_KEYPROG = calccprod(this.w_KEYPROG,this.w_CCLUNG)
      this.w_NEWCODE = alltrim(this.w_CODART)+this.w_CCCHAR+this.w_KEYPROG
      this.w_TEST = ""
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODICE"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODICE;
          from (i_cTable) where;
              CACODICE = this.w_NEWCODE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    enddo
    this.w_KEYSAL = this.w_RIF
    this.SUPREMO = this.w_NEWCODE
    * --- Se ho modificato in maschera la descrizione dell'articolo allora la utilizzo
    if Alltrim(this.w_VIR_DESCRI) # Alltrim(this.oParentObject.w_ARDESART)
      this.w_VIR_DESCRI = this.oParentObject.w_ARDESART
    endif
    * --- Aggiorno il cursore
    replace CACODICE with this.w_NEWCODE, CACODART with this.w_NEWCODE, CADESART With this.w_VIR_DESCRI in (EXPLDB)
    * --- Insert into ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR7BC4",this.ART_ICOL_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Creo nuovo articolo
    this.w_VIR_DESSUP = iif(!EMPTY(this.w_VIR_DESSUP),this.w_VIR_DESSUP+chr(13),"")+this.w_ELSCELTE
    this.w_VIR_DESSUP = left(this.w_VIR_DESSUP,iif(CP_DBTYPE="Oracle",2000,4000))
    this.w_ELSCELTE = left(this.w_ELSCELTE,iif(CP_DBTYPE="Oracle",2000,4000))
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'ART_ICOL','ARDESART');
      +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESSUP),'ART_ICOL','ARDESSUP');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_NEWCODE);
             )
    else
      update (i_cTable) set;
          ARDESART = this.w_VIR_DESCRI;
          ,ARDESSUP = this.w_VIR_DESSUP;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_NEWCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR6bc4",this.KEY_ARTI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ELSCELTE),'KEY_ARTI','CADESSUP');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
             )
    else
      update (i_cTable) set;
          CADESSUP = this.w_ELSCELTE;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_NEWCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from TRADARTI
    i_nConn=i_TableProp[this.TRADARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2],.t.,this.TRADARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select LGCODLIN,LGDESCOD,LGDESSUP  from "+i_cTable+" TRADARTI ";
          +" where LGCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE)+"";
           ,"_Curs_TRADARTI")
    else
      select LGCODLIN,LGDESCOD,LGDESSUP from (i_cTable);
       where LGCODICE = this.oParentObject.w_DBCODICE;
        into cursor _Curs_TRADARTI
    endif
    if used('_Curs_TRADARTI')
      select _Curs_TRADARTI
      locate for 1=1
      do while not(eof())
      * --- Insert into TRADARTI
      i_nConn=i_TableProp[this.TRADARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGCODLIN),'TRADARTI','LGCODLIN');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESCOD),'TRADARTI','LGDESCOD');
        +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESSUP),'TRADARTI','LGDESSUP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',_Curs_TRADARTI.LGCODLIN,'LGDESCOD',_Curs_TRADARTI.LGDESCOD,'LGDESSUP',_Curs_TRADARTI.LGDESSUP)
        insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
           values (;
             this.w_NEWCODE;
             ,_Curs_TRADARTI.LGCODLIN;
             ,_Curs_TRADARTI.LGDESCOD;
             ,_Curs_TRADARTI.LGDESSUP;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_TRADARTI
        continue
      enddo
      use
    endif
    * --- Insert into PAR_RIOR
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR8BC4",this.PAR_RIOR_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if not empty(nvl(this.oParentObject.w_CODLIN,""))
      * --- Write into TRADARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TRADARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LGDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
            +i_ccchkf ;
        +" where ";
            +"LGCODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
            +" and LGCODICE = "+cp_ToStrODBC(this.w_NEWCODE);
               )
      else
        update (i_cTable) set;
            LGDESSUP = this.w_TRADUZ;
            &i_ccchkf. ;
         where;
            LGCODLIN = this.oParentObject.w_CODLIN;
            and LGCODICE = this.w_NEWCODE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_ROWS=0
        * --- Insert into TRADARTI
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLIN),'TRADARTI','LGCODLIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'TRADARTI','LGDESCOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',this.oParentObject.w_CODLIN,'LGDESCOD',this.w_VIR_DESCRI,'LGDESSUP',this.w_TRADUZ)
          insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
             values (;
               this.w_NEWCODE;
               ,this.oParentObject.w_CODLIN;
               ,this.w_VIR_DESCRI;
               ,this.w_TRADUZ;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Aggiorno il progressivo...
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             )
    else
      update (i_cTable) set;
          ARCCPROG = this.w_KEYPROG;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_CODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_LenSez = 4
    Update (EXPLDB) set corifdis=this.w_NEWCODE where corifdis=this.w_CODART
    Select (EXPLDB) 
 go top
    do while not eof()
      * --- Memorizza posizione
      this.w_nPOS = recno()
      this.w_Codice = iif(!EMPTY(&EXPLDB..FOUND),&EXPLDB..FOUND,&EXPLDB..CACODICE)
      this.w_CodArt = &EXPLDB..CACODART
      this.w_cTMP1 = NVL(&EXPLDB..CODICEDIBA, SPACE(41))
      if &EXPLDB..ARCONCAR $ "S-Z" and EMPTY(&EXPLDB..FOUND)
        * --- Legge i figli diretti
        this.w_cTMP = &EXPLDB..LVLKEY
        this.w_nTMP = len(rTrim(this.w_cTMP))
        select * from (EXPLDB) ;
        where left(LVLKEY,this.w_nTMP)=rtrim(this.w_cTMP) and len(rTrim(LvlKey))=this.w_nTMP+this.w_LenSez ;
        order by LVLKEY into cursor __BCP__ nofilter
        if _TALLY>0
          * --- Se il prodotto in esame e' configurabile, allora crea la nuova distinta
          if &EXPLDB..ARCONCAR="S" and (&EXPLDB..TIPPAD<>"DC" or recno()=1)
            * --- Crea Nuova Distinta Base
            this.w_DesDis = this.oParentObject.w_DBDESCRI
            this.w_DBDATDIS = cp_CharToDate("31/12/2099")
            this.w_ARTICOLO = SUBSTR(this.w_CODICE,1,20)
            this.w_DBPRIORI = &EXPLDB..DBPRIORI
            this.w_DBTIPDIS = &EXPLDB..DBTIPDIS
            this.w_DBCODRIS = &EXPLDB..DBCODRIS
            * --- Insert into DISMBASE
            i_nConn=i_TableProp[this.DISMBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISMBASE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DBCODICE"+",DBDESCRI"+",DBFLSTAT"+",DBDTINVA"+",DBDTOBSO"+",DBCONGES"+",DBTIPGEN"+",DBPRIORI"+",DBTIPDIS"+",DBCODRIS"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_codice),'DISMBASE','DBCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DesDis),'DISMBASE','DBDESCRI');
              +","+cp_NullLink(cp_ToStrODBC("S"),'DISMBASE','DBFLSTAT');
              +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISMBASE','DBDTINVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DBDATDIS),'DISMBASE','DBDTOBSO');
              +","+cp_NullLink(cp_ToStrODBC(this.SUPREMO),'DISMBASE','DBCONGES');
              +","+cp_NullLink(cp_ToStrODBC("C"),'DISMBASE','DBTIPGEN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DBPRIORI),'DISMBASE','DBPRIORI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DBTIPDIS),'DISMBASE','DBTIPDIS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DBCODRIS),'DISMBASE','DBCODRIS');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_codice,'DBDESCRI',this.w_DesDis,'DBFLSTAT',"S",'DBDTINVA',i_DATSYS,'DBDTOBSO',this.w_DBDATDIS,'DBCONGES',this.SUPREMO,'DBTIPGEN',"C",'DBPRIORI',this.w_DBPRIORI,'DBTIPDIS',this.w_DBTIPDIS,'DBCODRIS',this.w_DBCODRIS)
              insert into (i_cTable) (DBCODICE,DBDESCRI,DBFLSTAT,DBDTINVA,DBDTOBSO,DBCONGES,DBTIPGEN,DBPRIORI,DBTIPDIS,DBCODRIS &i_ccchkf. );
                 values (;
                   this.w_codice;
                   ,this.w_DesDis;
                   ,"S";
                   ,i_DATSYS;
                   ,this.w_DBDATDIS;
                   ,this.SUPREMO;
                   ,"C";
                   ,this.w_DBPRIORI;
                   ,this.w_DBTIPDIS;
                   ,this.w_DBCODRIS;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Write into ART_ICOL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ARCODDIS ="+cp_NullLink(cp_ToStrODBC(this.w_codice),'ART_ICOL','ARCODDIS');
                  +i_ccchkf ;
              +" where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_codice);
                     )
            else
              update (i_cTable) set;
                  ARCODDIS = this.w_codice;
                  &i_ccchkf. ;
               where;
                  ARCODART = this.w_codice;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if g_PRFA="S"
              * --- Insert into ART_DIST
              i_nConn=i_TableProp[this.ART_DIST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_DIST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_DIST_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"DICODART"+",DICODDIS"+",DIFLPREF"+",DITIPDIS"+",CPROWORD"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_codice),'ART_DIST','DICODART');
                +","+cp_NullLink(cp_ToStrODBC(this.w_codice),'ART_DIST','DICODDIS');
                +","+cp_NullLink(cp_ToStrODBC("S"),'ART_DIST','DIFLPREF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DBTIPDIS),'ART_DIST','DITIPDIS');
                +","+cp_NullLink(cp_ToStrODBC(10),'ART_DIST','CPROWORD');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'DICODART',this.w_codice,'DICODDIS',this.w_codice,'DIFLPREF',"S",'DITIPDIS',this.w_DBTIPDIS,'CPROWORD',10)
                insert into (i_cTable) (DICODART,DICODDIS,DIFLPREF,DITIPDIS,CPROWORD &i_ccchkf. );
                   values (;
                     this.w_codice;
                     ,this.w_codice;
                     ,"S";
                     ,this.w_DBTIPDIS;
                     ,10;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
          * --- Aggiorna Campo Memo Distinta
          this.w_DesSup = this.w_ELSCELTE
          * --- Write into DISMBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISMBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DBNOTAGG ="+cp_NullLink(cp_ToStrODBC(this.w_DesSup),'DISMBASE','DBNOTAGG');
                +i_ccchkf ;
            +" where ";
                +"DBCODICE = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                DBNOTAGG = this.w_DesSup;
                &i_ccchkf. ;
             where;
                DBCODICE = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if (&EXPLDB..ARCONCAR $ "S-Z" and (&EXPLDB..TIPPAD<>"DC" or recno()=1))
            * --- Inserisce Componenti
            this.w_nTMP = 0
            select __BCP__
            SCAN
            * --- Inserisce righe - Se figlio <CONFIGURABILE e non ancora CONFIGURATO, assegna nuovo codice
            this.w_COCODCOM = iif(!EMPTY(__BCP__.FOUND),__BCP__.FOUND,__BCP__.COCODCOM)
            this.w_COCODART = __BCP__.COCODART
            this.w_CODESSUP = IIF(empty(nvl(__bcp__.CODESSUP,"")),nvl(__bcp__.DESSUP,""),__BCP__.CODESSUP)
            if __BCP__.ARCONCAR="S"
              this.w_cTMP = this.w_COCODCOM
              if EMPTY(__BCP__.FOUND)
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(__BCP__.COCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARTIPART,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,ARDESART,ARDESSUP;
                    from (i_cTable) where;
                        ARCODART = __BCP__.COCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
                  this.w_CCRIFE = NVL(cp_ToDate(_read_.ARCCRIFE),cp_NullValue(_read_.ARCCRIFE))
                  this.w_CCCHAR = NVL(cp_ToDate(_read_.ARCCCHAR),cp_NullValue(_read_.ARCCCHAR))
                  this.w_CCLUNG = NVL(cp_ToDate(_read_.ARCCLUNG),cp_NullValue(_read_.ARCCLUNG))
                  this.w_CCPROG = NVL(cp_ToDate(_read_.ARCCPROG),cp_NullValue(_read_.ARCCPROG))
                  this.w_VIR_DESCRI = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                  this.w_VIR_DESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_RIF = __BCP__.COCODART
                this.w_TEST = ""
                this.w_KEYPROG = calccprod(this.w_CCPROG,this.w_CCLUNG)
                this.w_NEWCODE = alltrim(this.w_COCODART)+this.w_CCCHAR+this.w_KEYPROG
                * --- Read from KEY_ARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CACODICE"+;
                    " from "+i_cTable+" KEY_ARTI where ";
                        +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CACODICE;
                    from (i_cTable) where;
                        CACODICE = this.w_NEWCODE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                do while !EMPTY(this.w_TEST)
                  * --- Write into ART_ICOL
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
                        +i_ccchkf ;
                    +" where ";
                        +"ARCODART = "+cp_ToStrODBC(__BCP__.COCODCOM);
                           )
                  else
                    update (i_cTable) set;
                        ARCCPROG = this.w_KEYPROG;
                        &i_ccchkf. ;
                     where;
                        ARCODART = __BCP__.COCODCOM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  this.w_KEYPROG = calccprod(this.w_KEYPROG,this.w_CCLUNG)
                  this.w_NEWCODE = alltrim(this.w_COCODART)+this.w_CCCHAR+this.w_KEYPROG
                  this.w_TEST = ""
                  * --- Read from KEY_ARTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CACODICE"+;
                      " from "+i_cTable+" KEY_ARTI where ";
                          +"CACODICE = "+cp_ToStrODBC(this.w_NEWCODE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CACODICE;
                      from (i_cTable) where;
                          CACODICE = this.w_NEWCODE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_TEST = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                enddo
                this.w_COCODCOM = this.w_NEWCODE
                this.w_COCODART = this.w_NEWCODE
                this.w_KEYSAL = this.w_RIF
                * --- Insert into ART_ICOL
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR7BC4",this.ART_ICOL_idx)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                this.w_VIR_DESSUP = iif(!EMPTY(this.w_VIR_DESSUP),this.w_VIR_DESSUP+chr(13),"")+this.w_ELSCELTE
                this.w_VIR_DESSUP = left(this.w_VIR_DESSUP,4000)
                this.w_Progressivo = cp_GetProg("DISMBASE","PRCCA", this.w_Progressivo, i_CODAZI)
                this.oParentObject.w_CCCONFIG = left(this.oParentObject.w_CCCONFIG,4000)
                * --- Insert into CONFMEMO
                i_nConn=i_TableProp[this.CONFMEMO_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONFMEMO_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFMEMO_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"CCSERIAL"+",CCCODIDX"+",CCCONFIG"+",CCKEYSAL"+",CCCODRIC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_Progressivo),'CONFMEMO','CCSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODIDX),'CONFMEMO','CCCODIDX');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCCONFIG),'CONFMEMO','CCCONFIG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'CONFMEMO','CCKEYSAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'CONFMEMO','CCCODRIC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_Progressivo,'CCCODIDX',this.w_CCCODIDX,'CCCONFIG',this.oParentObject.w_CCCONFIG,'CCKEYSAL',this.w_KEYSAL,'CCCODRIC',this.w_NEWCODE)
                  insert into (i_cTable) (CCSERIAL,CCCODIDX,CCCONFIG,CCKEYSAL,CCCODRIC &i_ccchkf. );
                     values (;
                       this.w_Progressivo;
                       ,this.w_CCCODIDX;
                       ,this.oParentObject.w_CCCONFIG;
                       ,this.w_KEYSAL;
                       ,this.w_NEWCODE;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                * --- Write into ART_ICOL
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'ART_ICOL','ARDESART');
                  +",ARDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESSUP),'ART_ICOL','ARDESSUP');
                      +i_ccchkf ;
                  +" where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_NEWCODE);
                         )
                else
                  update (i_cTable) set;
                      ARDESART = this.w_VIR_DESCRI;
                      ,ARDESSUP = this.w_VIR_DESSUP;
                      &i_ccchkf. ;
                   where;
                      ARCODART = this.w_NEWCODE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Insert into KEY_ARTI
                i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR6bc4",this.KEY_ARTI_idx)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                * --- Select from TRADARTI
                i_nConn=i_TableProp[this.TRADARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2],.t.,this.TRADARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select LGCODLIN,LGDESCOD,LGDESSUP  from "+i_cTable+" TRADARTI ";
                      +" where LGCODICE = "+cp_ToStrODBC(this.w_COCODART)+"";
                       ,"_Curs_TRADARTI")
                else
                  select LGCODLIN,LGDESCOD,LGDESSUP from (i_cTable);
                   where LGCODICE = this.w_COCODART;
                    into cursor _Curs_TRADARTI
                endif
                if used('_Curs_TRADARTI')
                  select _Curs_TRADARTI
                  locate for 1=1
                  do while not(eof())
                  * --- Insert into TRADARTI
                  i_nConn=i_TableProp[this.TRADARTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
                    +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGCODLIN),'TRADARTI','LGCODLIN');
                    +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESCOD),'TRADARTI','LGDESCOD');
                    +","+cp_NullLink(cp_ToStrODBC(_Curs_TRADARTI.LGDESSUP),'TRADARTI','LGDESSUP');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',_Curs_TRADARTI.LGCODLIN,'LGDESCOD',_Curs_TRADARTI.LGDESCOD,'LGDESSUP',_Curs_TRADARTI.LGDESSUP)
                    insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
                       values (;
                         this.w_NEWCODE;
                         ,_Curs_TRADARTI.LGCODLIN;
                         ,_Curs_TRADARTI.LGDESCOD;
                         ,_Curs_TRADARTI.LGDESSUP;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                    select _Curs_TRADARTI
                    continue
                  enddo
                  use
                endif
                * --- Insert into PAR_RIOR
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR8BC4",this.PAR_RIOR_idx)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                if not empty(nvl(this.oParentObject.w_CODLIN,""))
                  * --- Write into TRADARTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.TRADARTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.TRADARTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"LGDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
                        +i_ccchkf ;
                    +" where ";
                        +"LGCODLIN = "+cp_ToStrODBC(this.oParentObject.w_CODLIN);
                        +" and LGCODICE = "+cp_ToStrODBC(this.w_NEWCODE);
                           )
                  else
                    update (i_cTable) set;
                        LGDESSUP = this.w_TRADUZ;
                        &i_ccchkf. ;
                     where;
                        LGCODLIN = this.oParentObject.w_CODLIN;
                        and LGCODICE = this.w_NEWCODE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  if i_ROWS=0
                    * --- Insert into TRADARTI
                    i_nConn=i_TableProp[this.TRADARTI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRADARTI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"LGCODICE"+",LGCODLIN"+",LGDESCOD"+",LGDESSUP"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'TRADARTI','LGCODICE');
                      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLIN),'TRADARTI','LGCODLIN');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_VIR_DESCRI),'TRADARTI','LGDESCOD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_TRADUZ),'TRADARTI','LGDESSUP');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'LGCODICE',this.w_NEWCODE,'LGCODLIN',this.oParentObject.w_CODLIN,'LGDESCOD',this.w_VIR_DESCRI,'LGDESSUP',this.w_TRADUZ)
                      insert into (i_cTable) (LGCODICE,LGCODLIN,LGDESCOD,LGDESSUP &i_ccchkf. );
                         values (;
                           this.w_NEWCODE;
                           ,this.oParentObject.w_CODLIN;
                           ,this.w_VIR_DESCRI;
                           ,this.w_TRADUZ;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                  endif
                endif
                * --- Aggiorno il progressivo...
                * --- Write into ART_ICOL
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ARCCPROG ="+cp_NullLink(cp_ToStrODBC(this.w_KEYPROG),'ART_ICOL','ARCCPROG');
                      +i_ccchkf ;
                  +" where ";
                      +"ARCODART = "+cp_ToStrODBC(__BCP__.COCODCOM);
                         )
                else
                  update (i_cTable) set;
                      ARCCPROG = this.w_KEYPROG;
                      &i_ccchkf. ;
                   where;
                      ARCODART = __BCP__.COCODCOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Aggiorna Cursore EXPLDB
                Select (EXPLDB)
                if seek(__BCP__.LVLKEY)
                  replace CACODICE with this.w_COCODCOM, CACODART with this.w_COCODCOM, processed with "S" in (EXPLDB)
                endif
              endif
            endif
            * --- Progressivo di riga ...
            this.w_nTMP = this.w_nTMP+1
            * --- Crea riga distinta configurata
            * --- Try
            local bErr_055C50F0
            bErr_055C50F0=bTrsErr
            this.Try_055C50F0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Riga configurata gi� inserita
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_055C50F0
            * --- End
            ENDSCAN
            * --- Chiude Cursore
            if used("__BCP__")
              select __BCP__
              use
            endif
          endif
        endif
      endif
      * --- Ripristina e passa al record successivo
      Select (EXPLDB)
      goto this.w_nPOS
      if not eof()
        skip
      endif
    enddo
    * --- commit
    cp_EndTrs(.t.)
    this.w_DBGEN = .t.
    return
  proc Try_05681308()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBCODCOM"+",DBDESCOM"+",DBARTCOM"+",DBUNIMIS"+",DBQTADIS"+",DBCOEUM1"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBINIVAL"+",DBFINVAL"+",DB__NOTE"+",DBFLESPL"+",DBFLVARI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NEWCODE),'DISTBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_nTMP),'DISTBASE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_nTMP*10),'DISTBASE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COCODCOM),'DISTBASE','DBCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CADESART),'DISTBASE','DBDESCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COCODART),'DISTBASE','DBARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COUNIMIS),'DISTBASE','DBUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COCOEIMP),'DISTBASE','DBQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COCOEUM1),'DISTBASE','DBCOEUM1');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COSCAPRO),'DISTBASE','DBPERSCA');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CORECSCA),'DISTBASE','DBRECSCA');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COSFRIDO),'DISTBASE','DBPERSFR');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CORECSFR),'DISTBASE','DBRECSFR');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COPERRIC),'DISTBASE','DBPERRIC');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CODISGES),'DISTBASE','DBFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODESSUP),'DISTBASE','DB__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.FLESPL),'DISTBASE','DBFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.FLVARI),'DISTBASE','DBFLVARI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_NEWCODE,'CPROWNUM',this.w_nTMP,'CPROWORD',this.w_nTMP*10,'DBCODCOM',this.w_COCODCOM,'DBDESCOM',__BCP__.CADESART,'DBARTCOM',this.w_COCODART,'DBUNIMIS',__BCP__.COUNIMIS,'DBQTADIS',__BCP__.COCOEIMP,'DBCOEUM1',__BCP__.COCOEUM1,'DBPERSCA',__BCP__.COSCAPRO,'DBRECSCA',__BCP__.CORECSCA,'DBPERSFR',__BCP__.COSFRIDO)
      insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBCODCOM,DBDESCOM,DBARTCOM,DBUNIMIS,DBQTADIS,DBCOEUM1,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBINIVAL,DBFINVAL,DB__NOTE,DBFLESPL,DBFLVARI &i_ccchkf. );
         values (;
           this.w_NEWCODE;
           ,this.w_nTMP;
           ,this.w_nTMP*10;
           ,this.w_COCODCOM;
           ,__BCP__.CADESART;
           ,this.w_COCODART;
           ,__BCP__.COUNIMIS;
           ,__BCP__.COCOEIMP;
           ,__BCP__.COCOEUM1;
           ,__BCP__.COSCAPRO;
           ,__BCP__.CORECSCA;
           ,__BCP__.COSFRIDO;
           ,__BCP__.CORECSFR;
           ,__BCP__.COPERRIC;
           ,i_DATSYS;
           ,__BCP__.CODISGES;
           ,this.w_CODESSUP;
           ,__BCP__.FLESPL;
           ,__BCP__.FLVARI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_055C50F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+",CPROWNUM"+",CPROWORD"+",DBCODCOM"+",DBDESCOM"+",DBARTCOM"+",DBUNIMIS"+",DBQTADIS"+",DBCOEUM1"+",DBPERSCA"+",DBRECSCA"+",DBPERSFR"+",DBRECSFR"+",DBPERRIC"+",DBINIVAL"+",DBFINVAL"+",DB__NOTE"+",DBFLESPL"+",DBFLVARI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'DISTBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_nTMP),'DISTBASE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_nTMP*10),'DISTBASE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COCODCOM),'DISTBASE','DBCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CADESART),'DISTBASE','DBDESCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COCODART),'DISTBASE','DBARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COUNIMIS),'DISTBASE','DBUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COCOEIMP),'DISTBASE','DBQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COCOEUM1),'DISTBASE','DBCOEUM1');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COSCAPRO),'DISTBASE','DBPERSCA');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CORECSCA),'DISTBASE','DBRECSCA');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COSFRIDO),'DISTBASE','DBPERSFR');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CORECSFR),'DISTBASE','DBRECSFR');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.COPERRIC),'DISTBASE','DBPERRIC');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DISTBASE','DBINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.CODISGES),'DISTBASE','DBFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODESSUP),'DISTBASE','DB__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.FLESPL),'DISTBASE','DBFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(__BCP__.FLVARI),'DISTBASE','DBFLVARI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.w_CODICE,'CPROWNUM',this.w_nTMP,'CPROWORD',this.w_nTMP*10,'DBCODCOM',this.w_COCODCOM,'DBDESCOM',__BCP__.CADESART,'DBARTCOM',this.w_COCODART,'DBUNIMIS',__BCP__.COUNIMIS,'DBQTADIS',__BCP__.COCOEIMP,'DBCOEUM1',__BCP__.COCOEUM1,'DBPERSCA',__BCP__.COSCAPRO,'DBRECSCA',__BCP__.CORECSCA,'DBPERSFR',__BCP__.COSFRIDO)
      insert into (i_cTable) (DBCODICE,CPROWNUM,CPROWORD,DBCODCOM,DBDESCOM,DBARTCOM,DBUNIMIS,DBQTADIS,DBCOEUM1,DBPERSCA,DBRECSCA,DBPERSFR,DBRECSFR,DBPERRIC,DBINIVAL,DBFINVAL,DB__NOTE,DBFLESPL,DBFLVARI &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_nTMP;
           ,this.w_nTMP*10;
           ,this.w_COCODCOM;
           ,__BCP__.CADESART;
           ,this.w_COCODART;
           ,__BCP__.COUNIMIS;
           ,__BCP__.COCOEIMP;
           ,__BCP__.COCOEUM1;
           ,__BCP__.COSCAPRO;
           ,__BCP__.CORECSCA;
           ,__BCP__.COSFRIDO;
           ,__BCP__.CORECSFR;
           ,__BCP__.COPERRIC;
           ,i_DATSYS;
           ,__BCP__.CODISGES;
           ,this.w_CODESSUP;
           ,__BCP__.FLESPL;
           ,__BCP__.FLVARI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !EMPTY(this.oParentObject.w_ARCODART) and !EMPTY(this.oParentObject.w_DBCODICE)
      this.oParentObject.w_DURINGINIT = .T.
      this.w_LIVELLO = 1
      this.w_FIRST = .T.
      SELECT(this.w_cCursor)
      ZAP
      APPEND BLANK
      REPLACE CC__DIBA WITH this.oParentObject.w_DBCODICE, LVLKEY WITH "AAA", LIVELLO WITH 0, LIVCURSM WITH 0
      if this.oParentObject.w_RIC="S"
        Select (cCursM)
        scan for rsel=1
        cod=NVL(CCCODICE,"")
        tipo=NVL(CCTIPOCA,"")
        Update (this.w_cCursor) set rsel=1 where cccodice=cod and cctipoca=tipo
        endscan
      endif
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if USED(cInitCur)
        this.oParentObject.w_DURINGINIT = .T.
        this.Page_17()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Select (this.w_cCursor)
      GO TOP
      SCAN FOR RSEL=1 AND NOT EMPTY(NVL(CCTIPOCA,""))
      this.L_CCTIPOCA = NVL(CCTIPOCA,"")
      this.L_CCCODICE = NVL(CCCODICE,"")
      this.w_LVLKEY = LVLKEY
      pos=recno()
      this.w_LVLKEYP = LVLKEYP
      Select (cCursD)
      if SEEK(this.w_LVLKEY , (cCursD) , "LVLKEY")
        * --- Aggiorna il dettaglio sulle impostazioni fatte
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        if this.L_CCTIPOCA = "C"
          Select (this.w_cCursor)
          Scatter memvar memo
          if m.CPROWORD>0
            Select (cCursD)
            Update (cCursD) set xchk=0 where ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            Update (cCursD) set xchk=1 where CPROWORD=m.CPROWORD AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            Select (cCursD)
            locate for xchk=1 AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            if found()
              this.L_CCDETTAG = NVL(CCDETTAG,"")
            endif
          endif
          this.w_NEWVAL = Alltrim(Str(m.CPROWORD,3,0))
          Select (this.w_cCursor)
          if SEEK(this.w_LVLKEY)
            Replace CCDETTAG with this.L_CCDETTAG, RSEL with 1 FOR cctipoca=this.L_CCTIPOCA and cccodice= this.L_CCCODICE
            Select (cCursD)
            locate for xchk=1 AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
            if found()
              this.GSCR_KCC.NotifyEvent("w_Albero row checked")     
              this.GSCR_KCC.NotifyEvent("SottoAlbero")     
            endif
          endif
        endif
        * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
        * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
        this.GSCR_KCC.SetControlsValue()     
        this.bUpdateParentObject = False
        this.w_SOTTOALBERO = .T.
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Select (this.w_cCursor)
      goto pos
      ENDSCAN
      Select (cCursD)
      Go top
      this.Albero.Grd.Refresh()     
      this.Albero.Grd.SetFocus()     
      this.GSCR_KCC.__dummy__.Enabled = .t.
      this.GSCR_KCC.__dummy__.SetFocus()     
      this.GSCR_KCC.__dummy__.Enabled = .f.
      this.GSCR_KCC.LockScreen = .F.
      this.Albero.Grd.SetFocus()     
      this.oParentObject.w_DURINGINIT = .F.
      this.bUpdateParentObject = False
      this.GSCR_KCC.NotifyEvent("AggiornaTV")     
      this.w_TREEVIEW.ExpandAll(.T.)
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione distinta con listino statistico
    if !EMPTY(this.oParentObject.w_CODLIS)
      this.oParentObject.w_KEYRIF1 = SYS(2015)
      if this.oParentObject.w_EXPSL<>"S"
        Select * from (this.w_EXPLDB) into cursor Expconf Where ARCONCAR<>"X"
      endif
      Select distinct * from (this.w_EXPLDB) into cursor ExplAll readwrite
      Select distinct * from ExplAll into cursor Stampa1
      Select Stampa1 
 GO TOP 
 SCAN
      this.w_CODIAR = Cacodart
      this.w_RIFDIS = NVL(corifdis,"")
      this.w_KEYSAL = this.w_Codiar + repl("#",20)
      this.w_ROWNUM = CPROWNUM
      this.w_CAUNMISU = NVL(COUNIMIS, SPACE(3))
      this.w_CAQTAMOV = NVL(QTALOR, 0)
      this.w_CAQTAMO1 = NVL(QTALO1, 0)
      * --- Try
      local bErr_056FF860
      bErr_056FF860=bTrsErr
      this.Try_056FF860()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into ART_TEMP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_TEMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CACODDIS ="+cp_NullLink(cp_ToStrODBC(this.w_RIFDIS),'ART_TEMP','CACODDIS');
          +",CAKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
          +",CACODART ="+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODART');
              +i_ccchkf ;
          +" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.oParentObject.w_KEYRIF1);
              +" and CACODRIC = "+cp_ToStrODBC(this.w_CODIAR);
                 )
        else
          update (i_cTable) set;
              CACODDIS = this.w_RIFDIS;
              ,CAKEYSAL = this.w_KEYSAL;
              ,CACODART = this.w_CODIAR;
              &i_ccchkf. ;
           where;
              CAKEYRIF = this.oParentObject.w_KEYRIF1;
              and CACODRIC = this.w_CODIAR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_056FF860
      * --- End
      Select Stampa1
      ENDSCAN
      GSDSCBDC (this,this.oParentObject.w_KEYRIF1, i_DATSYS, "L", "N", g_CODESE, SPACE(5), this.oParentObject.w_CODLIS , "N" )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      select distinct Stampa1.*, Round(9999999999999.999999*0+nvl(przart.laprezzo,0)*(Stampa1.cocoeum1*Stampa1.qtacompl/Stampa1.cocoeimp),5) as prezzo, ; 
 Round(9999999999999.999999*0+iif(!empty(nvl(codicediba,"")),nvl(przart.laprezzo,0)*(Stampa1.cocoeum1*Stampa1.qtacompl/Stampa1.cocoeimp),0),5) as totliv from Stampa1 ; 
 LEFT OUTER JOIN przart on ; 
 Stampa1.cacodart=przart.cacodric and Stampa1.counimis=nvl(przart.unimis, space(3)) and Round(Stampa1.qtalor, 10)=round(nvl(przart.caqtamov, 0), 10) ; 
 into cursor __tmp1__ nofilter order by lvlkey desc readwrite
      INDEX ON lvlkey TAG lvlkey COLLATE "MACHINE"
      this.w_LenLvl = 4
      Select max(livello) as maxliv from __tmp1__ into cursor massimo
      this.w_MAXLVL = massimo.maxliv
      do while this.w_MAXLVL>=0
        SELECT __tmp1__
        SCAN FOR livello=this.w_MAXLVL
        this.w_MYKEY = lvlkey
        this.w_VALORE = iif(empty(nvl(codicediba,"")),nvl(prezzo,0),nvl(totliv,0))
        if this.w_maxlvl>0 and this.w_VALORE>0
          * --- Aggiorna il padre
          SEEK left(this.w_mykey, this.w_maxlvl*this.w_LenLvl-1) ORDER lvlkey
          REPLACE totliv WITH totliv+this.w_VALORE
          SEEK this.w_mykey ORDER lvlkey
        endif
        SELECT __tmp1__
        ENDSCAN
        this.w_MAXLVL = this.w_MAXLVL - 1
      enddo
      Select distinct cacodart,prezzo from __tmp1__ into cursor __tmp2__ 
 select __tmp2__ 
 COUNT FOR nvl(prezzo,0)=0 to this.w_RECZERO
      * --- Stampa la lista degli articoli per cui non � definito il prezzo
      if this.w_RECZERO>0
        this.TmpC = "Ci sono %1 articoli per i quali non � definito il prezzo%0Si desidera stamparli?"
        if ah_YesNo(this.TmpC,"",alltrim(str(this.w_RECZERO,5,0)))
          Select distinct cacodart as licodart from __tmp2__ where nvl(prezzo,0)=0 into cursor __TMP__
          Listino = this.oParentObject.w_CODLIS
          Distinta = this.oParentObject.w_DBCODICE
          cp_chprn("..\CCAR\exe\query\GSCR_sam","",this.oParentObject)
        endif
      endif
      l_CODLIS = this.oParentObject.w_CODLIS 
 l_DESLIS = this.oParentObject.w_DESLIS 
 l_PERCEN = this.oParentObject.w_PERCEN 
 l_EXPSL = this.oParentObject.w_EXPSL
      if this.oParentObject.w_EXPSL<>"S"
        Update __tmp1__ set dbtipgen="N" where lvlkey not in (select lvlkey from Expconf)
      endif
      Select * from __tmp1__ into cursor __tmp__ order by lvlkey
      CP_CHPRN("..\CCAR\EXE\QUERY\GSCR1KCC") 
 Use in (this.w_EXPLDB)
      use in select("_max_")
      use in select("Expconf")
      use in select("_level_")
      use in select("Stampa1")
      use in select("__tmp1__")
      use in select("__tmp2__")
      use in select("massimo")
      use in select("PrzArt")
      use in select("ToDel")
      use in select("okcomp")
      use in select("cliv")
      use in select("deleting")
    endif
  endproc
  proc Try_056FF860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CACODDIS"+",CAKEYSAL"+",CACODART"+",CPROWNUM"+",CAUNMISU"+",CAQTAMOV"+",CAQTAMO1"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_KEYRIF1),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFDIS),'ART_TEMP','CACODDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ART_TEMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUNMISU),'ART_TEMP','CAUNMISU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMOV),'ART_TEMP','CAQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAQTAMO1),'ART_TEMP','CAQTAMO1');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.oParentObject.w_KEYRIF1,'CACODRIC',this.w_CODIAR,'CACODDIS',this.w_RIFDIS,'CAKEYSAL',this.w_KEYSAL,'CACODART',this.w_CODIAR,'CPROWNUM',this.w_ROWNUM,'CAUNMISU',this.w_CAUNMISU,'CAQTAMOV',this.w_CAQTAMOV,'CAQTAMO1',this.w_CAQTAMO1)
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CACODDIS,CAKEYSAL,CACODART,CPROWNUM,CAUNMISU,CAQTAMOV,CAQTAMO1 &i_ccchkf. );
         values (;
           this.oParentObject.w_KEYRIF1;
           ,this.w_CODIAR;
           ,this.w_RIFDIS;
           ,this.w_KEYSAL;
           ,this.w_CODIAR;
           ,this.w_ROWNUM;
           ,this.w_CAUNMISU;
           ,this.w_CAQTAMOV;
           ,this.w_CAQTAMO1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sostituisco la Calculate che contiene le getvar della maschera evito refresh
    Select (cCursD)
    this.oParentObject.w_CCTIPOCA = NVL( &cCursD..CCTIPOCA, SPACE(1))
    this.GSCR_KCC.w_CCCODICE = NVL( &cCursD..CCCODICE, SPACE(5))
    this.GSCR_KCC.w_CCDESCRI = NVL( &cCursD..CCDESCRI, SPACE(40))
    this.oParentObject.w_LEVEL = NVL( &cCursD..LIVELLO, 0)
    this.oParentObject.w_ROWSEL = INT(NVL( &cCursD..CPROWORD, 0))
    this.oParentObject.w_CCDETTAG = NVL( &cCursD..CCDETTAG, SPACE(40))
    this.oParentObject.w_LIVCURS = NVL( &cCursD..LIVCURSM, 0)
    this.oParentObject.w_LVLKEYTV = NVL( &cCursD..LVLKEY, SPACE(200))
    this.GSCR_KCC.w_CCROWORD = NVL( &cCursD..CCROWORD, 0)
    this.GSCR_KCC.w_CCCOMPON = NVL( &cCursD..CCCOMPON, SPACE(20))
    this.GSCR_KCC.w_CC__DIBA = NVL( &cCursD..CC__DIBA, SPACE(20))
    this.GSCR_KCC.w_LVLKEY1 = NVL( &cCursD..LVLKEY, SPACE(200))
  endproc


  procedure Page_14
    param pTIPOPE,pCCTIPOCA,pCCCODICE,pCC__DIBA,pCCCOMPON,pCCROWORD,pLVLKEYP
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private L_CCDESCRI
    m.L_CCDESCRI = space(40)
    this.L_CCFORMUL = ""
    this.w_UPDSTR = False
    if INLIST(m.pTIPOPE , "VERIFICA", "CALCOLA")
      if not used(Modelli)
        * --- Crea il Cursore
        Select A.*, A.CCFORMUL AS FORMULAO from (cCursZ) a into cursor (Modelli) Where 1=0 ReadWrite
        INDEX ON CCTIPOCA+CCCODICE TAG CHIAVE COLLATE "MACHINE"
      endif
      Select (Modelli) 
 Locate for CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
      if not Found()
        Select(cCarParam) 
 Locate for CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
        if not Found()
          SELECT A.*, A.CCFORMUL AS FORMULAO FROM (cCursZ) A into Cursor (cCarParam) ; 
 where a.cctipoca<>"C" and a.cchdrlvl<>"S" and a.ccheader<>"S" readwrite
          INDEX ON CCTIPOCA+CCCODICE+CC__DIBA+CCCOMPON+ALLTRIM(STR(CCROWORD)) tag lvlkey COLLATE "MACHINE"
        endif
        Select(cCarParam) 
 Locate for CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
        if Found()
          Select (Modelli)
          APPEND FROM DBF(cCarParam) FOR CCTIPOCA = m.pCCTIPOCA AND CCCODICE = m.pCCCODICE AND CC__DIBA = m.pCC__DIBA AND CCROWORD = m.pCCROWORD AND CCCOMPON = m.pCCCOMPON
        endif
      endif
      if this.oParentObject.w_FLINIT $ "C-M" and !Empty(this.oParentObject.w_CCSERIAL1) and Used(cInitCur)
        Select (Modelli) 
 Locate for CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
        if Found()
          Select (Modelli)
          SCAN FOR CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
          l_DETTAG=ALLTRIM(CCDETTAG)
          Select (cInitCur)
          LOCATE FOR CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA AND ALLTRIM(CCDETTAG)=L_DETTAG
          if Found()
            L_ROWORD = nvl(CPROWORD, 0)
            Select (Modelli)
            REPLACE CPROWORD WITH L_ROWORD
          endif
          Select (Modelli)
          ENDSCAN
        endif
      endif
    endif
    if INLIST(m.pTIPOPE , "SOLOCALCOLO", "CALCOLA")
      Select (Modelli)
      SCAN FOR CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA
      l_DETTAG=ALLTRIM(CCDETTAG)
      L_ROWORD = nvl(CPROWORD, 0)
      Select(cCarParam)
      REPLACE CPROWORDS WITH L_ROWORD FOR CCCODICE = m.pCCCODICE and CCTIPOCA = m.pCCTIPOCA and ALLTRIM(CCDETTAG)==l_DETTAG
      Select (Modelli)
      ENDSCAN
    endif
    this.w_UPDSTR = False
    if INLIST(m.pTIPOPE , "SOLOCALCOLO", "CALCOLA")
      this.w_NEWVAL = ""
      local i_olderr, bOkEval, myformula, cMyformula, lccFormul
      lccFormul = ""
      myformula = ""
      this.w_bValoreTroppoGrande = .F.
      this.w_bFormulaNonValida = .F.
      this.w_bValoreTroppoPiccolo = .F.
      Dimension myvar[1,1]
      SELECT ALLTRIM(cast(ccdettag as varchar(5))), cproword, alltrim(cast(nvl(formulao, "") as varchar(254))), alltrim(ccdescri) from (Modelli) INTO ARRAY myvar ; 
 where cctipoca = m.pCCTIPOCA and cccodice = m.pCCCODICE AND CC__DIBA = m.pCC__DIBA AND CCROWORD = m.pCCROWORD AND CCCOMPON = m.pCCCOMPON order by lvlkey
      * --- AND CCROWORD = pCCROWORD
      this.w_i_Tot = Alen(myvar, 1)
      this.w_i = 1
      do while this.w_i<=this.w_i_Tot and not empty(nvl(myvar(this.w_i, 1),""))
        if Empty(myformula)
          myformula = myvar(this.w_i, 3)
          m.L_CCDESCRI = myvar(this.w_i, 4)
          this.L_CCFORMUL = "Valori del modello parametrico " + alltrim(m.pCCCODICE) + " (" + alltrim(m.L_CCDESCRI) + ") : " + chr(13)
        endif
        myformula = strtran(myformula, "<"+alltrim(myvar(this.w_i, 1))+">", str(myvar(this.w_i, 2),10,3))
        this.w_NEWVAL = this.w_NEWVAL + alltrim(myvar(this.w_i, 1)) + " = " + alltrim(str(myvar(this.w_i, 2),10,3)) + ":"
        this.w_i = this.w_i + 1
      enddo
      this.L_CCFORMUL = this.L_CCFORMUL + this.w_NEWVAL
      i_olderr = on("ERROR") 
 bOkEval = True 
 on error bOkEval = False 
 myformula = strtran(myformula, "," , ".")
      this.w_CPROWORD = Eval(myformula)
      this.w_CPROWORD = Round(this.w_CPROWORD, 3)
      if "*"$str(this.w_CPROWORD)
        * --- Trappo sia numeric overflow che divisione per zero
        this.w_CPROWORD = 0
        this.w_bFormulaNonValida = .T.
      else
        * --- Valore troppo grande
        if this.w_CPROWORD > 9999999999.999
          this.w_CPROWORD = 0
          this.w_bValoreTroppoGrande = .T.
        endif
        if this.w_CPROWORD < -999999999.999
          this.w_CPROWORD = 0
          this.w_bValoreTroppoPiccolo = .T.
        endif
      endif
      on error &i_olderr
      this.w_UPDSTR = bOkEval
      this.w_ERRORE = "N"
      this.w_MSGERRORE = ""
      if this.w_bFormulaNonValida
        this.w_ERRORE = "S"
        this.w_MSGERRORE = ah_msgformat("Formula non valida %1" , m.pCCCODICE)
        if m.pTIPOPE<>"CALCOLA"
          ah_errormsg("Formula non valida %1" ,48,"", m.pCCCODICE)
        endif
      endif
      if this.w_bValoreTroppoGrande
        if this.w_ERRORE="S"
          this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("%0")
        endif
        this.w_ERRORE = "S"
        this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("Valore troppo grande per la formula %1" , m.pCCCODICE)
        if m.pTIPOPE<>"CALCOLA"
          ah_errormsg("Valore troppo grande per la formula %1" ,48,"", m.pCCCODICE)
        endif
      endif
      if this.w_bValoreTroppoPiccolo
        if this.w_ERRORE="S"
          this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("%0")
        endif
        this.w_ERRORE = "S"
        this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("Valore troppo piccolo per la formula %1" , m.pCCCODICE)
        if m.pTIPOPE<>"CALCOLA"
          ah_errormsg("Valore troppo piccolo per la formula %1" ,48,"", m.pCCCODICE)
        endif
      endif
      m.errore = this.w_ERRORE
      m.msgerr = this.w_msgerrore
    endif
    if m.pTIPOPE <> "VERIFICA" AND m.pTIPOPE <> "SOLOCALCOLO"
      if this.w_UPDSTR
        this.w_CCCODOLD = this.GSCR_KCC.w_CCCODICE
        this.w_CCTIPOLD = this.oParentObject.w_CCTIPOCA
        this.GSCR_KCC.w_CCCODICE = m.pCCCODICE
        this.oParentObject.w_CCTIPOCA = m.pCCTIPOCA
        this.w_NEWVAL = Left(this.w_NEWVAL, len(this.w_NEWVAL)-1)
        m.ccformul = this.L_CCFORMUL 
 m.ccdettag = padr(this.w_NEWVAL,40) 
 m.rsel=1 
 m.cproword = this.w_CPROWORD
        Select (this.w_cCursor)
        Replace CCDETTAG with padr(this.w_NEWVAL,40), ERRORE with this.w_ERRORE, msgerr with this.w_msgerrore FOR CCTIPOCA=m.pCCTIPOCA and cccodice=m.pCCCODICE
        Select (cCursM)
        this.w_nTMP = RecNo(cCursM)
        GO TOP
        REPLACE SCELTA with 1, RSEL WITH 1, CPROWORD with Round(this.w_CPROWORD, 3), CCDETTAG WITH "."+ALLTRIM(STR(ROUND(this.w_CPROWORD,3)))+"." FOR CCTIPOCA="M" AND CCCODICE= m.pCCCODICE
        Select (cCursM)
        GO this.w_nTMP
        this.GSCR_KCC.w_CCCODICE = this.w_CCCODOLD
        this.oParentObject.w_CCTIPOCA = this.w_CCTIPOLD
      endif
    endif
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Configurazione Distinta
    this.w_OKCONFIG = this.w_OK
    Select (this.w_EXPLDB) 
 Scan for this.w_OK
    this.w_CORIFDIS = corifdis
    this.w_CCROWORD = CPROWNUM
    this.w_COCODCOM = cocodcom
    this.w_LVLKEY = rtrim(lvlkey)
    SELECT(cCursCONF_DIS)
    SCAN FOR &cCursCONF_DIS..CC__DIBA=this.w_CORIFDIS AND &cCursCONF_DIS..CCROWORD=this.w_CCROWORD AND &cCursCONF_DIS..CCCOMPON=this.w_COCODCOM
    * --- Verifica le impostazioni dei codici caratteristiche
    if this.w_OK
      this.w_CCCODICE = &cCursCONF_DIS..CCCODICE
      this.w_L_CCTIPOCA = &cCursCONF_DIS..CCTIPOCA
      do case
        case &cCursCONF_DIS..CCTIPOCA = "C"
          Select (this.w_cCursor)
          Locate for cccodice = this.w_CCCODICE and cctipoca = "C"
          if Found()
            this.w_CPROWORD = cproword
          else
            this.w_CPROWORD = 0
          endif
          if this.w_CPROWORD<>0
            this.w_CHIAVE1 = this.w_CORIFDIS + STR(this.w_CCROWORD) + this.w_COCODCOM + this.w_CCCODICE + STR(this.w_CPROWORD)
            this.w_CC__DEFA = "N"
            this.w_CCOPERAT = SPACE(1)
            this.w_CC_COEFF = 0
            this.w_CCDESSUP = SPACE(80)
            SELECT(cCursCONFDDIS)
            if SEEK(this.w_CHIAVE1)
              this.w_CC__DEFA = NVL(&cCursCONFDDIS..CC__DEFA, SPACE(1))
              this.w_CCOPERAT = &cCursCONFDDIS..CCOPERAT
              this.w_CC_COEFF = &cCursCONFDDIS..CC_COEFF
              this.w_CCDESSUP = &cCursCONFDDIS..CCDESSUP
            endif
            Select (this.w_EXPLDB)
            this.w_nTMP = RecNo(this.w_EXPLDB)
            Replace codessup with this.w_CCDESSUP for lvlkey=this.w_LVLKEY
            GO this.w_nTMP
            Select(cCursCONF_DIS)
            if this.w_CC__DEFA="S"
              if this.w_CCOPERAT $ "*/" and this.w_CC_COEFF<>0 and this.w_CC_COEFF<>1
                * --- Aggiorna il coefficiente d'impiego del legame
                Select (this.w_EXPLDB)
                this.w_nTMP = RecNo(this.w_EXPLDB)
                if this.w_CCOPERAT = "*" 
                  Replace cocoeimp with Round(cocoeimp*this.w_CC_COEFF, 5) 
 Replace cocoeum1 with Round(cocoeum1*this.w_CC_COEFF, 5) 
 Replace qtacomp with Round(qtacomp*this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY 
 Replace qtacompn with Round(qtacompn*this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY 
 Replace qtacompl with Round(qtacompl*this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY
                else
                  Replace cocoeimp with Round(cocoeimp/this.w_CC_COEFF, 5) 
 Replace cocoeum1 with Round(cocoeum1/this.w_CC_COEFF, 5) 
 Replace qtacomp with Round(qtacomp/this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY 
 Replace qtacompn with Round(qtacompn/this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY 
 Replace qtacompl with Round(qtacompl/this.w_CC_COEFF, 5) for lvlkey=this.w_LVLKEY
                endif
                GO this.w_nTMP
                Select(cCursCONF_DIS)
              endif
            else
              * --- Elimina tutto il ramo della distinta
              Select (this.w_EXPLDB)
              this.w_nTMP = RecNo(this.w_EXPLDB)
              Delete for lvlkey=this.w_LVLKEY
              Select (this.w_EXPLDB)
              GO TOP
              GO this.w_nTMP
              skip -1
              Select(cCursCONF_DIS)
              GO bottom
            endif
          else
            * --- Elimina tutto il ramo della distinta
            Select (this.w_EXPLDB)
            this.w_nTMP = RecNo(this.w_EXPLDB)
            Delete for lvlkey=this.w_LVLKEY
            Select (this.w_EXPLDB)
            GO TOP
            GO this.w_nTMP
            skip -1
            Select(cCursCONF_DIS)
            GO bottom
          endif
        case &cCursCONF_DIS..CCTIPOCA = "M"
          * --- Modelli Parametrici: risolve la formula e controlla gli intervalli di validit�
          Select (this.w_cCursor)
          Locate for cccodice = this.w_CCCODICE and cctipoca = "M"
          if found()
            this.w_CPROWORD = cproword
            this.w_CCDESSUP = ccformul
          else
            this.w_CPROWORD = 0
          endif
          this.w_NEWVAL = ""
          local i_olderr, bOkEval, myformula, cMyformula, lccFormul
          lccFormul = ""
          myformula = ""
          this.w_bValoreTroppoGrande = .F.
          this.w_bFormulaNonValida = .F.
          this.w_bValoreTroppoPiccolo = .F.
          Dimension myvar[1,1]
          SELECT ALLTRIM(cast(ccdettag as varchar(5))), cprowords, alltrim(cast(nvl(formulao, "") as varchar(254))), alltrim(ccdescri) from (cCarParam) INTO ARRAY myvar ; 
 where cctipoca = "M" and cccodice = this.w_CCCODICE AND CC__DIBA = this.w_CORIFDIS AND CCROWORD = this.w_CCROWORD AND CCCOMPON = this.w_COCODCOM order by lvlkey
          * --- AND CCROWORD = pCCROWORD
          this.w_i_Tot = Alen(myvar, 1)
          this.w_i = 1
          do while this.w_i<=this.w_i_Tot and not empty(nvl(myvar(this.w_i, 1),""))
            if Empty(myformula)
              myformula = myvar(this.w_i, 3)
              m.L_CCDESCRI = myvar(this.w_i, 4)
              this.L_CCFORMUL = "Valori del modello parametrico " + alltrim(this.w_CCCODICE) + " (" + alltrim(m.L_CCDESCRI) + ") : " + chr(13)
            endif
            myformula = strtran(myformula, "<"+alltrim(myvar(this.w_i, 1))+">", str(myvar(this.w_i, 2),10,3))
            this.w_NEWVAL = this.w_NEWVAL + alltrim(myvar(this.w_i, 1)) + " = " + alltrim(str(myvar(this.w_i, 2),10,3)) + ":"
            this.w_i = this.w_i + 1
          enddo
          this.L_CCFORMUL = this.L_CCFORMUL + this.w_NEWVAL
          i_olderr = on("ERROR") 
 bOkEval = True 
 on error bOkEval = False 
 myformula = strtran(myformula, "," , ".")
          this.w_CPROWORD = Eval(myformula)
          this.w_CPROWORD = Round(this.w_CPROWORD, 3)
          if "*"$str(this.w_CPROWORD)
            * --- Trappo sia numeric overflow che divisione per zero
            this.w_CPROWORD = 0
            this.w_OKCONFIG = this.w_OKCONFIG and .F.
            this.w_bFormulaNonValida = .T.
          else
            * --- Valore troppo grande
            if this.w_CPROWORD > 9999999999.999
              this.w_CPROWORD = 0
              this.w_OKCONFIG = this.w_OKCONFIG and .F.
              this.w_bValoreTroppoGrande = .T.
            endif
            if this.w_CPROWORD < -999999999.999
              this.w_CPROWORD = 0
              this.w_OKCONFIG = this.w_OKCONFIG and .F.
              this.w_bValoreTroppoPiccolo = .T.
            endif
          endif
          on error &i_olderr
          this.w_ERRORE = "N"
          this.w_MSGERRORE = ""
          if this.w_bFormulaNonValida
            this.w_ERRORE = "S"
            this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("Formula non valida %1" , this.w_CCCODICE)
          endif
          if this.w_bValoreTroppoGrande
            if this.w_ERRORE="S"
              this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("%0")
            endif
            this.w_ERRORE = "S"
            this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("Valore troppo grande per la formula %1" , this.w_CCCODICE)
          endif
          if this.w_bValoreTroppoPiccolo
            if this.w_ERRORE="S"
              this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("%0")
            endif
            this.w_ERRORE = "S"
            this.w_MSGERRORE = alltrim(this.w_MSGERRORE) + ah_msgformat("Valore troppo piccolo per la formula %1" , this.w_CCCODICE)
          endif
          if this.w_ERRORE="S"
            this.w_MSG = this.w_MSG+chr(13)+alltrim(this.w_MSGERRORE)
          endif
          if this.w_CPROWORD<=0
            * --- Elimina tutto il ramo della distinta
            Select (this.w_EXPLDB)
            this.w_nTMP = RecNo(this.w_EXPLDB)
            Delete for lvlkey=this.w_LVLKEY
            Select (this.w_EXPLDB)
            GO TOP
            GO this.w_nTMP
            skip -1
            Select(cCursCONF_DIS)
            GO bottom
          else
            if this.w_CPROWORD<>1
              Select(cCursCONF_DIS)
              this.w_CC_COEFF = this.w_CPROWORD
              * --- Aggiorna il coefficiente d'impiego del legame
              Select (this.w_EXPLDB)
              this.w_nTMP = RecNo(this.w_EXPLDB)
              Replace cocoeimp with Round(cocoeimp*this.w_CC_COEFF,5) 
 Replace cocoeum1 with Round(cocoeum1*this.w_CC_COEFF,5) 
 Replace qtacomp with Round(qtacomp*this.w_CC_COEFF,5) for lvlkey=this.w_LVLKEY and !deleted() 
 Replace qtacompn with Round(qtacompn*this.w_CC_COEFF,5) for lvlkey=this.w_LVLKEY and !deleted() 
 Replace qtacompl with Round(qtacompl*this.w_CC_COEFF,5) for lvlkey=this.w_LVLKEY and !deleted()
              Replace codessup with this.w_CCDESSUP for lvlkey=this.w_LVLKEY
              Select (this.w_EXPLDB)
              GO this.w_nTMP
              Select(cCursCONF_DIS)
            endif
          endif
        case &cCursCONF_DIS..CCTIPOCA = "D"
          Select (this.w_cCursor)
          Locate for cccodice = this.w_CCCODICE and cctipoca = "D"
          if Found()
            if CCFORMUL="OK" and not empty(nvl(CCDETTAG,""))
              this.w_CPROWORD = cproword
              this.w_NEWVAL = alltrim(NVL(CCDETTAG, ""))
              Select (this.w_cCursor)
            endif
          else
            this.w_CPROWORD = 0
            this.w_NEWVAL = ""
          endif
      endcase
    endif
    Select(cCursCONF_DIS)
    ENDSCAN
    Select (this.w_EXPLDB)
    Endscan
    if this.w_OPER<>"TVDISTINTA"
      this.w_OK = this.w_OKCONFIG
      if !this.w_OK
        ah_errormsg("In fase di selezione si sono verificati i seguenti errori: %0%1", 48,"",alltrim(this.w_MSG))
      endif
    endif
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_CARADD = 1
    Select (this.w_cCursor)
    go bottom
    this.w_TMPLVL = LIVELLO
    do while this.oParentObject.w_CARADD<>0
      SELECT(cCursD)
      locate for livello = this.w_TMPLVL AND CCHDRLVL<>"S" AND CCHEADER="S"
      if found()
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.GSCR_KCC.SetControlsValue()     
        SELECT (this.w_cCursor)
        GO TOP
        COUNT FOR CCTIPOCA="D" AND LIVELLO = this.w_TMPLVL TO this.w_NUMCADES
        SELECT (this.w_cCursor)
        GO TOP
        COUNT FOR CCTIPOCA<>"D" AND LIVELLO = this.w_TMPLVL TO this.w_NUMCNDES
        if this.w_NUMCADES > 0 AND this.w_NUMCNDES = 0
          Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello = this.w_TMPLVL and livcursm > 0 having CARSCE=CARTOT into Cursor Scelte
        else
          Select SUM(rsel) as CARSCE, COUNT(rsel) as CARTOT FROM (this.w_cCursor); 
 where livello = this.w_TMPLVL and CCTIPOCA<>"D" and livcursm > 0 having CARSCE=CARTOT into Cursor Scelte
        endif
        if reccount("Scelte")>0
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if USED(cInitCur)
            this.Page_17()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !this.w_SOTTOALBERO
            this.GSCR_KCC.NotifyEvent("SottoAlbero")     
          endif
        else
          this.oParentObject.w_CARADD = 0
        endif
      else
        this.oParentObject.w_CARADD = 0
      endif
      this.w_TMPLVL = this.w_TMPLVL + 1
    enddo
    SELECT(cCursD)
    GO TOP
    SET ORDER TO LVLKEYTV
    =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
    this.Page_13()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.GSCR_KCC.SetControlsValue()     
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED(cInitCur)
      Select (cInitCur)
      SCAN FOR CCTIPOCA="C" AND CCFLINIT="S"
      l_TIPOCA=CCTIPOCA 
 l_CARAT=CCCODICE 
 l_ROW=CPROWORD
      Select (this.w_cCursor)
      LOCATE FOR CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and RSEL=1
      if not Found()
        UPDATE (this.w_cCursor) set RSEL=1, cproword=l_ROW where CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and RSEL<>1
        Select (cCursD)
        LOCATE FOR XCHK=1 AND CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and CCHEADER<>"S"
        if not Found()
          Update (cCursD) set xchk=1 where CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and CCHEADER<>"S" AND CCFLINIT="S"
        endif
        LOCATE FOR XCHK=1 AND CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and CCHEADER<>"S"
        if Found()
          this.L_CCDETTAG = NVL(CCDETTAG,"")
          Select (this.w_cCursor)
          REPLACE CCDETTAG WITH this.L_CCDETTAG
        endif
      else
        Select (cCursD)
        LOCATE FOR XCHK=1 AND CCTIPOCA=l_TIPOCA and CCCODICE=l_CARAT and CCHEADER<>"S"
        if Found()
          this.L_CCDETTAG = NVL(CCDETTAG,"")
          Select (this.w_cCursor)
          Replace CCDETTAG with this.L_CCDETTAG
        endif
      endif
      Select (cInitCur)
      ENDSCAN
    endif
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplica i Cicli di Lavorazione / Cicli semplicati
    this.w_DBCONGES = this.SUPREMO
    if g_PRFA="S"
      * --- Duplica i Cicli di Lavorazione
      * --- Codice articolo di Origine
      * --- Codice Distinta Base di Origine
      * --- Codice Ciclo di Lavorazione di Origine
      * --- Indice di preferenza ciclo
      * --- Descrizione ciclo di origine
      * --- Note ciclo di origine
      * --- Seriale ciclo
      * --- Copia materiali di input
      * --- data riferimento
      * --- Codice articolo di destinazione
      * --- Codice Distinta Base di Destinazione
      * --- Codice Ciclo di Lavorazione di Destinazione
      * --- Indice di preferenza ciclo di destinazione
      * --- Descrizione ciclo di destinazione
      * --- Descrizione aggiuntiva ciclo di destinazione
      * --- Mantienie la data inizio validit� delle fasi
      * --- Data inizio validit� forzata
      * --- Considera per costificazione
      * --- Consente la modifica del ciclo di lavoro legato all'ordine
      * --- Copia note fase
      * --- --CIclo preferenziale
      this.w_MADATCIC = "N"
      this.w_DATINI = i_DATSYS
      * --- Select from GSCR11BC4
      do vq_exec with 'GSCR11BC4',this,'_Curs_GSCR11BC4','',.f.,.t.
      if used('_Curs_GSCR11BC4')
        select _Curs_GSCR11BC4
        locate for 1=1
        do while not(eof())
        * --- Inizializzazione variabili caller di GSCI_BSR
        this.w_CLCODART = _Curs_GSCR11BC4.CLCODART
        this.w_CLCODDIS = _Curs_GSCR11BC4.CLCODDIS
        this.w_CLCODCIC = _Curs_GSCR11BC4.CLCODCIC
        this.w_DATRIF = i_DATSYS
        this.w_ARTFIN = _Curs_GSCR11BC4.DBCODART
        this.w_DISTFIN = _Curs_GSCR11BC4.DBCODICE
        this.w_CODFIN = this.w_CLCODCIC
        this.w_DESFIN = _Curs_GSCR11BC4.CLDESCIC
        this.w_DAGFIN = _Curs_GSCR11BC4.CLDESAGG
        this.w_PREFIN = "00"
        this.w_CLSERIAL = _Curs_GSCR11BC4.CLSERIAL
        this.w_CLPRCOST = _Curs_GSCR11BC4.CLPRCOST
        this.w_CLFLCMOD = _Curs_GSCR11BC4.CLFLCMOD
        * --- Chiama batch di duplicazione cicli
        GSCI_BSR(this,"Duplica_silent")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_GSCR11BC4
          continue
        enddo
        use
      endif
      * --- --Copia ciclo alternativo
      * --- Select from GSCR12BC4
      do vq_exec with 'GSCR12BC4',this,'_Curs_GSCR12BC4','',.f.,.t.
      if used('_Curs_GSCR12BC4')
        select _Curs_GSCR12BC4
        locate for 1=1
        do while not(eof())
        * --- Inizializzazione variabili caller di GSCI_BSR
        this.w_CLCODART = _Curs_GSCR12BC4.CLCODART
        this.w_CLCODDIS = _Curs_GSCR12BC4.CLCODDIS
        this.w_CLCODCIC = _Curs_GSCR12BC4.CLCODCIC
        this.w_DATRIF = i_DATSYS
        this.w_ARTFIN = _Curs_GSCR12BC4.DBCODART
        this.w_DISTFIN = _Curs_GSCR12BC4.DBCODICE
        this.w_CODFIN = this.w_CLCODCIC
        this.w_DESFIN = _Curs_GSCR12BC4.CLDESCIC
        this.w_DAGFIN = _Curs_GSCR12BC4.CLDESAGG
        this.w_PREFIN = _Curs_GSCR12BC4.CLINDCIC
        this.w_CLSERIAL = _Curs_GSCR12BC4.CLSERIAL
        this.w_CLPRCOST = _Curs_GSCR12BC4.CLPRCOST
        this.w_CLFLCMOD = _Curs_GSCR12BC4.CLFLCMOD
        * --- Chiama batch di duplicazione cicli
        GSCI_BSR(this,"Duplica_silent")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_GSCR12BC4
          continue
        enddo
        use
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,23)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONF_DIS'
    this.cWorkTables[3]='CONFDDIS'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DISTBASE'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='TRADARTI'
    this.cWorkTables[8]='PAR_DISB'
    this.cWorkTables[9]='CONFDCAR'
    this.cWorkTables[10]='CONF_CAR'
    this.cWorkTables[11]='MODE_DIS'
    this.cWorkTables[12]='CONFSTAT'
    this.cWorkTables[13]='CONFMEMO'
    this.cWorkTables[14]='CONFTRAD'
    this.cWorkTables[15]='CONFTRDE'
    this.cWorkTables[16]='ART_TEMP'
    this.cWorkTables[17]='PAR_RIOR'
    this.cWorkTables[18]='*TMPCONFDDIS'
    this.cWorkTables[19]='*TMPCONFORA'
    this.cWorkTables[20]='TABMCICL'
    this.cWorkTables[21]='*TMPEXPDB'
    this.cWorkTables[22]='ANA_CONF'
    this.cWorkTables[23]='ART_DIST'
    return(this.OpenAllTables(23))

  proc CloseCursors()
    if used('_Curs_TMPEXPDB')
      use in _Curs_TMPEXPDB
    endif
    if used('_Curs_CONFMEMO')
      use in _Curs_CONFMEMO
    endif
    if used('_Curs_TRADARTI')
      use in _Curs_TRADARTI
    endif
    if used('_Curs_TRADARTI')
      use in _Curs_TRADARTI
    endif
    if used('_Curs_TRADARTI')
      use in _Curs_TRADARTI
    endif
    if used('_Curs_GSCR11BC4')
      use in _Curs_GSCR11BC4
    endif
    if used('_Curs_GSCR12BC4')
      use in _Curs_GSCR12BC4
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
