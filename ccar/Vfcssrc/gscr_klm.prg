* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_klm                                                        *
*              Manutenzione caratteristiche legami/articoli                    *
*                                                                              *
*      Author: Zucchetto Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-16                                                      *
* Last revis.: 2015-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_klm",oParentObject))

* --- Class definition
define class tgscr_klm as StdForm
  Top    = 3
  Left   = 8

  * --- Standard Properties
  Width  = 789
  Height = 519+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-25"
  HelpContextID=51808919
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  PAR_PROD_IDX = 0
  UNIMIS_IDX = 0
  DISTBASE_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  CONF_CAR_IDX = 0
  cPrg = "gscr_klm"
  cComment = "Manutenzione caratteristiche legami/articoli"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CARINS = space(1)
  o_CARINS = space(1)
  w_OPERAZ = space(1)
  o_OPERAZ = space(1)
  w_AGGART = space(1)
  w_AGGDIS = space(1)
  w_TIPOCA = space(1)
  w_PARAM = space(1)
  w_OLDCAR = space(5)
  o_OLDCAR = space(5)
  w_OLDDES = space(40)
  w_CODGES = space(2)
  w_FLDIGE = space(1)
  w_TIPGES = space(1)
  w_TIPGE = space(1)
  w_TIPARTINI = space(2)
  w_TIPARTFIN = space(2)
  w_SELEZCAR1 = space(1)
  w_SELEZI = space(1)
  w_STADIT = space(1)
  w_TIPOARTI = space(2)
  w_NEWCAR = space(5)
  w_NEWDES = space(40)
  w_SELEZCAR = space(1)
  w_DEFAULT = space(1)
  w_TIPGEN = space(1)
  w_ARTINI = space(20)
  o_ARTINI = space(20)
  w_DESINI = space(40)
  w_ARTFIN = space(20)
  w_DESFIN = space(40)
  w_DISINI = space(20)
  w_DISFIN = space(20)
  w_GRUMER = space(5)
  w_GRUDES = space(35)
  w_GRUMERF = space(5)
  w_GRUDESF = space(35)
  w_CODFAMA = space(5)
  w_DESFAMAI = space(35)
  w_CODFAMAF = space(5)
  w_DESFAMAIF = space(35)
  w_CODCAT = space(5)
  w_DESCATI = space(35)
  w_CODCATF = space(5)
  w_DESCATIF = space(35)
  w_CODMAG = space(5)
  w_DESMAGI = space(30)
  w_CODMAGF = space(5)
  w_DESMAGIF = space(30)
  w_DBDESINI = space(40)
  w_DBDESFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_TIPRIG1 = space(1)
  w_TIPRIG2 = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_DTOBS2 = ctod('  /  /  ')
  w_DATSTA = ctod('  /  /  ')
  w_ZOOMCAR1 = .NULL.
  w_ZoomSel = .NULL.
  w_ZoomSel1 = .NULL.
  w_ZOOMCAR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscr_klm
  *Abilita le scrollbar
  ScrollBars=3
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_klmPag1","gscr_klm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgscr_klmPag2","gscr_klm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(3).addobject("oPag","tgscr_klmPag3","gscr_klm",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Lista articoli/legami distinte")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCARINS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMCAR1 = this.oPgFrm.Pages(1).oPag.ZOOMCAR1
    this.w_ZoomSel = this.oPgFrm.Pages(3).oPag.ZoomSel
    this.w_ZoomSel1 = this.oPgFrm.Pages(3).oPag.ZoomSel1
    this.w_ZOOMCAR = this.oPgFrm.Pages(1).oPag.ZOOMCAR
    DoDefault()
    proc Destroy()
      this.w_ZOOMCAR1 = .NULL.
      this.w_ZoomSel = .NULL.
      this.w_ZoomSel1 = .NULL.
      this.w_ZOOMCAR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='DISTBASE'
    this.cWorkTables[5]='MAGAZZIN'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='CONF_CAR'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gscr_blm(this,"AG")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CARINS=space(1)
      .w_OPERAZ=space(1)
      .w_AGGART=space(1)
      .w_AGGDIS=space(1)
      .w_TIPOCA=space(1)
      .w_PARAM=space(1)
      .w_OLDCAR=space(5)
      .w_OLDDES=space(40)
      .w_CODGES=space(2)
      .w_FLDIGE=space(1)
      .w_TIPGES=space(1)
      .w_TIPGE=space(1)
      .w_TIPARTINI=space(2)
      .w_TIPARTFIN=space(2)
      .w_SELEZCAR1=space(1)
      .w_SELEZI=space(1)
      .w_STADIT=space(1)
      .w_TIPOARTI=space(2)
      .w_NEWCAR=space(5)
      .w_NEWDES=space(40)
      .w_SELEZCAR=space(1)
      .w_DEFAULT=space(1)
      .w_TIPGEN=space(1)
      .w_ARTINI=space(20)
      .w_DESINI=space(40)
      .w_ARTFIN=space(20)
      .w_DESFIN=space(40)
      .w_DISINI=space(20)
      .w_DISFIN=space(20)
      .w_GRUMER=space(5)
      .w_GRUDES=space(35)
      .w_GRUMERF=space(5)
      .w_GRUDESF=space(35)
      .w_CODFAMA=space(5)
      .w_DESFAMAI=space(35)
      .w_CODFAMAF=space(5)
      .w_DESFAMAIF=space(35)
      .w_CODCAT=space(5)
      .w_DESCATI=space(35)
      .w_CODCATF=space(5)
      .w_DESCATIF=space(35)
      .w_CODMAG=space(5)
      .w_DESMAGI=space(30)
      .w_CODMAGF=space(5)
      .w_DESMAGIF=space(30)
      .w_DBDESINI=space(40)
      .w_DBDESFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPRIG1=space(1)
      .w_TIPRIG2=space(1)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DTOBS2=ctod("  /  /  ")
      .w_DATSTA=ctod("  /  /  ")
        .w_CARINS = 'C'
        .w_OPERAZ = 'S'
        .w_AGGART = 'S'
        .w_AGGDIS = 'S'
        .w_TIPOCA = 'C'
        .w_PARAM = 'T'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_OLDCAR))
          .link_1_7('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMCAR1.Calculate(.w_OLDCAR)
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .DoRTCalc(8,8,.f.)
        .w_CODGES = 'PP'
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODGES))
          .link_1_14('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_TIPGES = iif(.w_PARAM="P" or .w_FLDIGE="S", .w_PARAM, "G")
        .w_TIPGE = iif(.w_PARAM="P" or .w_FLDIGE="S", .w_PARAM, "G")
          .DoRTCalc(13,14,.f.)
        .w_SELEZCAR1 = "D"
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      .oPgFrm.Page3.oPag.ZoomSel.Calculate()
        .w_SELEZI = "D"
      .oPgFrm.Page3.oPag.ZoomSel1.Calculate()
        .w_STADIT = SPACE(1)
        .w_TIPOARTI = ""
        .w_NEWCAR = iif(.w_OPERAZ='S' and .w_CARINS='D',.w_OLDCAR,.w_NEWCAR)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_NEWCAR))
          .link_1_23('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMCAR.Calculate(.w_NEWCAR)
          .DoRTCalc(20,20,.f.)
        .w_SELEZCAR = "D"
      .oPgFrm.Page3.oPag.oObj_3_6.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_7.Calculate()
      .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
        .w_DEFAULT = 'S'
        .w_TIPGEN = ' '
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_ARTINI))
          .link_2_7('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_ARTFIN = .w_ARTINI
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_ARTFIN))
          .link_2_9('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_DISINI = ""
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_DISINI))
          .link_2_13('Full')
        endif
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_DISFIN))
          .link_2_14('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_GRUMER))
          .link_2_16('Full')
        endif
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_GRUMERF))
          .link_2_19('Full')
        endif
        .DoRTCalc(33,34,.f.)
        if not(empty(.w_CODFAMA))
          .link_2_22('Full')
        endif
        .DoRTCalc(35,36,.f.)
        if not(empty(.w_CODFAMAF))
          .link_2_25('Full')
        endif
        .DoRTCalc(37,38,.f.)
        if not(empty(.w_CODCAT))
          .link_2_28('Full')
        endif
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_CODCATF))
          .link_2_31('Full')
        endif
        .DoRTCalc(41,42,.f.)
        if not(empty(.w_CODMAG))
          .link_2_34('Full')
        endif
        .DoRTCalc(43,44,.f.)
        if not(empty(.w_CODMAGF))
          .link_2_37('Full')
        endif
          .DoRTCalc(45,47,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(49,52,.f.)
        .w_DATSTA = i_DATSYS
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page3.oPag.oBtn_3_1.enabled = this.oPgFrm.Page3.oPag.oBtn_3_1.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_2.enabled = this.oPgFrm.Page3.oPag.oBtn_3_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_OPERAZ<>.w_OPERAZ
          .link_1_7('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOMCAR1.Calculate(.w_OLDCAR)
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(8,8,.t.)
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page3.oPag.ZoomSel.Calculate()
        .oPgFrm.Page3.oPag.ZoomSel1.Calculate()
        .DoRTCalc(10,18,.t.)
        if .o_OPERAZ<>.w_OPERAZ.or. .o_OLDCAR<>.w_OLDCAR.or. .o_CARINS<>.w_CARINS
            .w_NEWCAR = iif(.w_OPERAZ='S' and .w_CARINS='D',.w_OLDCAR,.w_NEWCAR)
          .link_1_23('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOMCAR.Calculate(.w_NEWCAR)
        .oPgFrm.Page3.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_7.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
        .DoRTCalc(20,22,.t.)
            .w_TIPGEN = ' '
        .DoRTCalc(24,25,.t.)
        if .o_ARTINI<>.w_ARTINI
            .w_ARTFIN = .w_ARTINI
          .link_2_9('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMCAR1.Calculate(.w_OLDCAR)
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page3.oPag.ZoomSel.Calculate()
        .oPgFrm.Page3.oPag.ZoomSel1.Calculate()
        .oPgFrm.Page1.oPag.ZOOMCAR.Calculate(.w_NEWCAR)
        .oPgFrm.Page3.oPag.oObj_3_6.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_7.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOLDCAR_1_7.enabled = this.oPgFrm.Page1.oPag.oOLDCAR_1_7.mCond()
    this.oPgFrm.Page1.oPag.oNEWCAR_1_23.enabled = this.oPgFrm.Page1.oPag.oNEWCAR_1_23.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_1.enabled = this.oPgFrm.Page3.oPag.oBtn_3_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDEFAULT_1_34.visible=!this.oPgFrm.Page1.oPag.oDEFAULT_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMCAR1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page3.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page3.oPag.ZoomSel1.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMCAR.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_6.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_7.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OLDCAR
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLDCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_OLDCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_TIPOCA;
                     ,'CCCODICE',trim(this.w_OLDCAR))
          select CCTIPOCA,CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLDCAR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_OLDCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_OLDCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStr(this.w_TIPOCA);

            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OLDCAR) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oOLDCAR_1_7'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLDCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_OLDCAR);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_TIPOCA;
                       ,'CCCODICE',this.w_OLDCAR)
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLDCAR = NVL(_Link_.CCCODICE,space(5))
      this.w_OLDDES = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OLDCAR = space(5)
      endif
      this.w_OLDDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLDCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGES
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPFLDIGE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_CODGES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_CODGES)
            select PPCODICE,PPFLDIGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGES = NVL(_Link_.PPCODICE,space(2))
      this.w_FLDIGE = NVL(_Link_.PPFLDIGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODGES = space(2)
      endif
      this.w_FLDIGE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWCAR
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_NEWCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_TIPOCA;
                     ,'CCCODICE',trim(this.w_NEWCAR))
          select CCTIPOCA,CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWCAR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_NEWCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_NEWCAR)+"%");
                   +" and CCTIPOCA="+cp_ToStr(this.w_TIPOCA);

            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NEWCAR) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oNEWCAR_1_23'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_NEWCAR);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_TIPOCA;
                       ,'CCCODICE',this.w_NEWCAR)
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWCAR = NVL(_Link_.CCCODICE,space(5))
      this.w_NEWDES = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NEWCAR = space(5)
      endif
      this.w_NEWDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ARTINI))
          select CACODICE,CADESART,CA__TIPO,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oARTINI_2_7'),i_cWhere,'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARTINI)
            select CACODICE,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_TIPRIG1 = NVL(_Link_.CA__TIPO,space(1))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_TIPRIG1 = space(1)
      this.w_DTOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG1='R' AND (.w_DTOBS1>.w_OBTEST OR EMPTY(.w_DTOBS1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice componente incongruente o obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_TIPRIG1 = space(1)
        this.w_DTOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ARTFIN))
          select CACODICE,CADESART,CA__TIPO,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oARTFIN_2_9'),i_cWhere,'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CA__TIPO,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARTFIN)
            select CACODICE,CADESART,CA__TIPO,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_TIPRIG2 = NVL(_Link_.CA__TIPO,space(1))
      this.w_DTOBS2 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_TIPRIG2 = space(1)
      this.w_DTOBS2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG2='R' AND (.w_DTOBS2>.w_OBTEST OR EMPTY(.w_DTOBS2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice componente incongruente o obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_TIPRIG2 = space(1)
        this.w_DTOBS2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISINI
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DISINI))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDISINI_2_13'),i_cWhere,'',"Distinta base",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DISINI)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISINI = NVL(_Link_.ARCODART,space(20))
      this.w_DBDESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DISINI = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DISINI = space(20)
      endif
      this.w_DBDESINI = space(40)
      this.w_DISINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DISINI) OR NOT EMPTY(.w_DISINI)) AND (empty(.w_DISFIN) OR .w_DISINI<=.w_DISFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_DISINI = space(20)
        this.w_DBDESINI = space(40)
        this.w_DISINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISFIN
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DISFIN))
          select ARCODART,ARDESART,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDISFIN_2_14'),i_cWhere,'',"Distinta base",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DISFIN)
            select ARCODART,ARDESART,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DBDESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DISFIN = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DISFIN = space(20)
      endif
      this.w_DBDESFIN = space(40)
      this.w_DISFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DISFIN) OR NOT EMPTY(.w_DISFIN)) AND (.w_DISFIN>=.w_DISINI or empty(.w_DISFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_DISFIN = space(20)
        this.w_DBDESFIN = space(40)
        this.w_DISFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_2_16'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GRUDES = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_GRUDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMERF
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMERF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMERF)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMERF))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMERF)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMERF) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMERF_2_19'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMERF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMERF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMERF)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMERF = NVL(_Link_.GMCODICE,space(5))
      this.w_GRUDESF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMERF = space(5)
      endif
      this.w_GRUDESF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUMER<=.w_GRUMERF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUMERF = space(5)
        this.w_GRUDESF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMERF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAMA
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAMA)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAMA))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAMA)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAMA) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAMA_2_22'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAMA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAMA)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAMA = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAMA = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAMAF
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAMAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAMAF)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAMAF))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAMAF)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAMAF) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAMAF_2_25'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAMAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAMAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAMAF)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAMAF = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAIF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAMAF = space(5)
      endif
      this.w_DESFAMAIF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODFAMA <= .w_CODFAMAF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFAMAF = space(5)
        this.w_DESFAMAIF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAMAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_2_28'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCATF
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCATF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCATF)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCATF))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCATF)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCATF) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCATF_2_31'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCATF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCATF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCATF)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCATF = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATIF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCATF = space(5)
      endif
      this.w_DESCATIF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCAT <= .w_CODCATF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCATF = space(5)
        this.w_DESCATIF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCATF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_2_34'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAGF
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAGF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAGF)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAGF))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAGF)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAGF) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAGF_2_37'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAGF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAGF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAGF)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAGF = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGIF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAGF = space(5)
      endif
      this.w_DESMAGIF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODMAG <= .w_CODMAGF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODMAGF = space(5)
        this.w_DESMAGIF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAGF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCARINS_1_1.RadioValue()==this.w_CARINS)
      this.oPgFrm.Page1.oPag.oCARINS_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPERAZ_1_2.RadioValue()==this.w_OPERAZ)
      this.oPgFrm.Page1.oPag.oOPERAZ_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGART_1_3.RadioValue()==this.w_AGGART)
      this.oPgFrm.Page1.oPag.oAGGART_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGDIS_1_4.RadioValue()==this.w_AGGDIS)
      this.oPgFrm.Page1.oPag.oAGGDIS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLDCAR_1_7.value==this.w_OLDCAR)
      this.oPgFrm.Page1.oPag.oOLDCAR_1_7.value=this.w_OLDCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oOLDDES_1_8.value==this.w_OLDDES)
      this.oPgFrm.Page1.oPag.oOLDDES_1_8.value=this.w_OLDDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZCAR1_1_20.RadioValue()==this.w_SELEZCAR1)
      this.oPgFrm.Page1.oPag.oSELEZCAR1_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oSELEZI_3_4.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page3.oPag.oSELEZI_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSTADIT_2_1.RadioValue()==this.w_STADIT)
      this.oPgFrm.Page2.oPag.oSTADIT_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPOARTI_2_2.RadioValue()==this.w_TIPOARTI)
      this.oPgFrm.Page2.oPag.oTIPOARTI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWCAR_1_23.value==this.w_NEWCAR)
      this.oPgFrm.Page1.oPag.oNEWCAR_1_23.value=this.w_NEWCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWDES_1_24.value==this.w_NEWDES)
      this.oPgFrm.Page1.oPag.oNEWDES_1_24.value=this.w_NEWDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZCAR_1_26.RadioValue()==this.w_SELEZCAR)
      this.oPgFrm.Page1.oPag.oSELEZCAR_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFAULT_1_34.RadioValue()==this.w_DEFAULT)
      this.oPgFrm.Page1.oPag.oDEFAULT_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARTINI_2_7.value==this.w_ARTINI)
      this.oPgFrm.Page2.oPag.oARTINI_2_7.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_8.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_8.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oARTFIN_2_9.value==this.w_ARTFIN)
      this.oPgFrm.Page2.oPag.oARTFIN_2_9.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_10.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_10.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDISINI_2_13.value==this.w_DISINI)
      this.oPgFrm.Page2.oPag.oDISINI_2_13.value=this.w_DISINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDISFIN_2_14.value==this.w_DISFIN)
      this.oPgFrm.Page2.oPag.oDISFIN_2_14.value=this.w_DISFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMER_2_16.value==this.w_GRUMER)
      this.oPgFrm.Page2.oPag.oGRUMER_2_16.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUDES_2_17.value==this.w_GRUDES)
      this.oPgFrm.Page2.oPag.oGRUDES_2_17.value=this.w_GRUDES
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMERF_2_19.value==this.w_GRUMERF)
      this.oPgFrm.Page2.oPag.oGRUMERF_2_19.value=this.w_GRUMERF
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUDESF_2_20.value==this.w_GRUDESF)
      this.oPgFrm.Page2.oPag.oGRUDESF_2_20.value=this.w_GRUDESF
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFAMA_2_22.value==this.w_CODFAMA)
      this.oPgFrm.Page2.oPag.oCODFAMA_2_22.value=this.w_CODFAMA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_23.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_23.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFAMAF_2_25.value==this.w_CODFAMAF)
      this.oPgFrm.Page2.oPag.oCODFAMAF_2_25.value=this.w_CODFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAIF_2_26.value==this.w_DESFAMAIF)
      this.oPgFrm.Page2.oPag.oDESFAMAIF_2_26.value=this.w_DESFAMAIF
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_28.value==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_28.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_29.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_29.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCATF_2_31.value==this.w_CODCATF)
      this.oPgFrm.Page2.oPag.oCODCATF_2_31.value=this.w_CODCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATIF_2_32.value==this.w_DESCATIF)
      this.oPgFrm.Page2.oPag.oDESCATIF_2_32.value=this.w_DESCATIF
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAG_2_34.value==this.w_CODMAG)
      this.oPgFrm.Page2.oPag.oCODMAG_2_34.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGI_2_35.value==this.w_DESMAGI)
      this.oPgFrm.Page2.oPag.oDESMAGI_2_35.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAGF_2_37.value==this.w_CODMAGF)
      this.oPgFrm.Page2.oPag.oCODMAGF_2_37.value=this.w_CODMAGF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGIF_2_38.value==this.w_DESMAGIF)
      this.oPgFrm.Page2.oPag.oDESMAGIF_2_38.value=this.w_DESMAGIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDBDESINI_2_41.value==this.w_DBDESINI)
      this.oPgFrm.Page2.oPag.oDBDESINI_2_41.value=this.w_DBDESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDBDESFIN_2_42.value==this.w_DBDESFIN)
      this.oPgFrm.Page2.oPag.oDBDESFIN_2_42.value=this.w_DBDESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPRIG1='R' AND (.w_DTOBS1>.w_OBTEST OR EMPTY(.w_DTOBS1)))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTINI_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice componente incongruente o obsoleto")
          case   not(.w_TIPRIG2='R' AND (.w_DTOBS2>.w_OBTEST OR EMPTY(.w_DTOBS2)))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice componente incongruente o obsoleto")
          case   not((EMPTY(.w_DISINI) OR NOT EMPTY(.w_DISINI)) AND (empty(.w_DISFIN) OR .w_DISINI<=.w_DISFIN) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISINI))))  and not(empty(.w_DISINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDISINI_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not((EMPTY(.w_DISFIN) OR NOT EMPTY(.w_DISFIN)) AND (.w_DISFIN>=.w_DISINI or empty(.w_DISFIN)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DISFIN))))  and not(empty(.w_DISFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDISFIN_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not(.w_GRUMER<=.w_GRUMERF)  and not(empty(.w_GRUMERF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUMERF_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODFAMA <= .w_CODFAMAF)  and not(empty(.w_CODFAMAF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFAMAF_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCAT <= .w_CODCATF)  and not(empty(.w_CODCATF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCATF_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODMAG <= .w_CODMAGF)  and not(empty(.w_CODMAGF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODMAGF_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CARINS = this.w_CARINS
    this.o_OPERAZ = this.w_OPERAZ
    this.o_OLDCAR = this.w_OLDCAR
    this.o_ARTINI = this.w_ARTINI
    return

enddefine

* --- Define pages as container
define class tgscr_klmPag1 as StdContainer
  Width  = 785
  height = 519
  stdWidth  = 785
  stdheight = 519
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCARINS_1_1 as StdCombo with uid="MVVUOBEYPO",rtseq=1,rtrep=.f.,left=126,top=13,width=146,height=21;
    , ToolTipText = "Selezione tipo aggiornamento da effettuare";
    , HelpContextID = 79368154;
    , cFormVar="w_CARINS",RowSource=""+"Caratteristica completa,"+"Dettaglio caratteristica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARINS_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oCARINS_1_1.GetRadio()
    this.Parent.oContained.w_CARINS = this.RadioValue()
    return .t.
  endfunc

  func oCARINS_1_1.SetRadio()
    this.Parent.oContained.w_CARINS=trim(this.Parent.oContained.w_CARINS)
    this.value = ;
      iif(this.Parent.oContained.w_CARINS=='C',1,;
      iif(this.Parent.oContained.w_CARINS=='D',2,;
      0))
  endfunc

  add object oOPERAZ_1_2 as StdRadio with uid="NKXKRTIXCA",rtseq=2,rtrep=.f.,left=381, top=31, width=125,height=53;
    , ToolTipText = "Selezione tipo operazione da effettuare";
    , cFormVar="w_OPERAZ", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oOPERAZ_1_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Sostituzione"
      this.Buttons(1).HelpContextID = 243453978
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Inserimento"
      this.Buttons(2).HelpContextID = 243453978
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Disattivazione"
      this.Buttons(3).HelpContextID = 243453978
      this.Buttons(3).Top=34
      this.SetAll("Width",123)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezione tipo operazione da effettuare")
      StdRadio::init()
    endproc

  func oOPERAZ_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oOPERAZ_1_2.GetRadio()
    this.Parent.oContained.w_OPERAZ = this.RadioValue()
    return .t.
  endfunc

  func oOPERAZ_1_2.SetRadio()
    this.Parent.oContained.w_OPERAZ=trim(this.Parent.oContained.w_OPERAZ)
    this.value = ;
      iif(this.Parent.oContained.w_OPERAZ=='S',1,;
      iif(this.Parent.oContained.w_OPERAZ=='I',2,;
      iif(this.Parent.oContained.w_OPERAZ=='D',3,;
      0)))
  endfunc

  add object oAGGART_1_3 as StdCheck with uid="LLXTZAHHQJ",rtseq=3,rtrep=.f.,left=589, top=31, caption="Anagrafica articolo",;
    ToolTipText = "Aggiornamento caratteristiche articolo",;
    HelpContextID = 58964474,;
    cFormVar="w_AGGART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGART_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAGGART_1_3.GetRadio()
    this.Parent.oContained.w_AGGART = this.RadioValue()
    return .t.
  endfunc

  func oAGGART_1_3.SetRadio()
    this.Parent.oContained.w_AGGART=trim(this.Parent.oContained.w_AGGART)
    this.value = ;
      iif(this.Parent.oContained.w_AGGART=='S',1,;
      0)
  endfunc

  add object oAGGDIS_1_4 as StdCheck with uid="FTBOARQPQY",rtseq=4,rtrep=.f.,left=589, top=55, caption="Legame distinta",;
    ToolTipText = "Aggiornamento caratteristiche legame distinta",;
    HelpContextID = 84982266,;
    cFormVar="w_AGGDIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGDIS_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAGGDIS_1_4.GetRadio()
    this.Parent.oContained.w_AGGDIS = this.RadioValue()
    return .t.
  endfunc

  func oAGGDIS_1_4.SetRadio()
    this.Parent.oContained.w_AGGDIS=trim(this.Parent.oContained.w_AGGDIS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGDIS=='S',1,;
      0)
  endfunc

  add object oOLDCAR_1_7 as StdField with uid="GLFUFLOGZZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OLDCAR", cQueryName = "OLDCAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica di riferimento",;
    HelpContextID = 110224410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=10, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_TIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_OLDCAR"

  func oOLDCAR_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_OPERAZ<>'I' and .w_CARINS='C') or (.w_CARINS='D'))
    endwith
   endif
  endfunc

  func oOLDCAR_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLDCAR_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLDCAR_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_TIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oOLDCAR_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
  endproc

  add object oOLDDES_1_8 as StdField with uid="CWGXEQDCXU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OLDDES", cQueryName = "OLDDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89187354,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=77, Top=118, InputMask=replicate('X',40)


  add object ZOOMCAR1 as cp_szoombox with uid="XSORXPJARG",left=7, top=138, width=385,height=303,;
    caption='ZOOMCAR1',;
   bGlobalFont=.t.,;
    bReadOnly=.f.,bQueryOnLoad=.t.,bOptions=.t.,cZoomFile="GSCR1KLM",cTable="CONF_CAR",bAdvOptions=.f.,cMenuFile="",cZoomOnZoom="",;
    nPag=1;
    , HelpContextID = 124203065


  add object oObj_1_13 as cp_runprogram with uid="MVOAHPZVUO",left=16, top=537, width=157,height=22,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('ZOOM')",;
    cEvent = "w_CARINS Changed,w_OPERAZ Changed, w_OLDCAR Changed",;
    nPag=1;
    , HelpContextID = 190649011

  add object oSELEZCAR1_1_20 as StdRadio with uid="RPDLWIZEIJ",rtseq=15,rtrep=.f.,left=4, top=441, width=373,height=23;
    , cFormVar="w_SELEZCAR1", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZCAR1_1_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 67069816
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 67069816
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 67069816
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Inverti selezione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZCAR1_1_20.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    iif(this.value =3,"I",;
    space(1)))))
  endfunc
  func oSELEZCAR1_1_20.GetRadio()
    this.Parent.oContained.w_SELEZCAR1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZCAR1_1_20.SetRadio()
    this.Parent.oContained.w_SELEZCAR1=trim(this.Parent.oContained.w_SELEZCAR1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZCAR1=="S",1,;
      iif(this.Parent.oContained.w_SELEZCAR1=="D",2,;
      iif(this.Parent.oContained.w_SELEZCAR1=="I",3,;
      0)))
  endfunc


  add object oObj_1_21 as cp_runprogram with uid="LMABTQAHEF",left=175, top=538, width=157,height=22,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('1SC')",;
    cEvent = "w_SELEZCAR1 Changed",;
    nPag=1;
    , HelpContextID = 190649011


  add object oObj_1_22 as cp_runprogram with uid="NLCBTZQFHB",left=335, top=537, width=157,height=22,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('SC')",;
    cEvent = "w_SELEZCAR Changed",;
    nPag=1;
    , HelpContextID = 190649011

  add object oNEWCAR_1_23 as StdField with uid="NRNNBXLOIO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NEWCAR", cQueryName = "NEWCAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica da inserire",;
    HelpContextID = 110148394,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=400, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_TIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_NEWCAR"

  func oNEWCAR_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_OPERAZ<>'D' and .w_CARINS='C' ))
    endwith
   endif
  endfunc

  func oNEWCAR_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWCAR_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWCAR_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_TIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oNEWCAR_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
  endproc

  add object oNEWDES_1_24 as StdField with uid="FOFWYWVGAY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NEWDES", cQueryName = "NEWDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89111338,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=466, Top=119, InputMask=replicate('X',40)


  add object ZOOMCAR as cp_szoombox with uid="HNWUFIKVWL",left=395, top=139, width=385,height=303,;
    caption='ZOOMCAR',;
   bGlobalFont=.t.,;
    bReadOnly=.f.,bQueryOnLoad=.t.,bOptions=.t.,cZoomFile="GSCR_KLM",cTable="CONF_CAR",bAdvOptions=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    nPag=1;
    , HelpContextID = 124203114

  add object oSELEZCAR_1_26 as StdRadio with uid="AMYNCTYEMP",rtseq=21,rtrep=.f.,left=398, top=442, width=366,height=23;
    , cFormVar="w_SELEZCAR", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZCAR_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 67070600
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 67070600
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 67070600
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Inverti selezione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZCAR_1_26.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    iif(this.value =3,"I",;
    space(1)))))
  endfunc
  func oSELEZCAR_1_26.GetRadio()
    this.Parent.oContained.w_SELEZCAR = this.RadioValue()
    return .t.
  endfunc

  func oSELEZCAR_1_26.SetRadio()
    this.Parent.oContained.w_SELEZCAR=trim(this.Parent.oContained.w_SELEZCAR)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZCAR=="S",1,;
      iif(this.Parent.oContained.w_SELEZCAR=="D",2,;
      iif(this.Parent.oContained.w_SELEZCAR=="I",3,;
      0)))
  endfunc


  add object oBtn_1_27 as StdButton with uid="PPJFABIUST",left=685, top=474, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza lista distinte base che soddisfano le selezioni impostate";
    , HelpContextID = 125680618;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSCR_BLM(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="MWXKLVEWEK",left=735, top=474, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 125680618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDEFAULT_1_34 as StdCheck with uid="KTJWUTGHBD",rtseq=22,rtrep=.f.,left=126, top=63, caption="Considera default dettaglio",;
    ToolTipText = "Filtra default dettaglio",;
    HelpContextID = 190041034,;
    cFormVar="w_DEFAULT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDEFAULT_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDEFAULT_1_34.GetRadio()
    this.Parent.oContained.w_DEFAULT = this.RadioValue()
    return .t.
  endfunc

  func oDEFAULT_1_34.SetRadio()
    this.Parent.oContained.w_DEFAULT=trim(this.Parent.oContained.w_DEFAULT)
    this.value = ;
      iif(this.Parent.oContained.w_DEFAULT=='S',1,;
      0)
  endfunc

  func oDEFAULT_1_34.mHide()
    with this.Parent.oContained
      return (.w_CARINS='C' or (.w_CARINS='D' and .w_OPERAZ<>'S'))
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="ELKXKUFSML",Visible=.t., Left=9, Top=92,;
    Alignment=0, Width=244, Height=18,;
    Caption="Codice caratteristica di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="XZAHMRLCFC",Visible=.t., Left=403, Top=92,;
    Alignment=0, Width=209, Height=18,;
    Caption="Codice caratteristica da inserire"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="AUVRPXOEZQ",Visible=.t., Left=589, Top=8,;
    Alignment=0, Width=142, Height=18,;
    Caption="Archivi da aggiornare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FIVHAJGSOV",Visible=.t., Left=381, Top=8,;
    Alignment=0, Width=126, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="VMASCTBXWF",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=114, Height=18,;
    Caption="Tipo aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="OTTRKDEJWA",left=2, top=86, width=783,height=26

  add object oBox_1_28 as StdBox with uid="OZNYPTYQKJ",left=392, top=86, width=3,height=385

  add object oBox_1_30 as StdBox with uid="NKPOTUOIAG",left=1, top=114, width=784,height=358
enddefine
define class tgscr_klmPag2 as StdContainer
  Width  = 785
  height = 519
  stdWidth  = 785
  stdheight = 519
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTADIT_2_1 as StdCombo with uid="OTBYURSNHI",value=1,rtseq=17,rtrep=.f.,left=127,top=215,width=151,height=21;
    , ToolTipText = "Stato distinte (non considerato per l'agg. articoli)";
    , HelpContextID = 68226010;
    , cFormVar="w_STADIT",RowSource=""+"Tutte,"+"Confermata,"+"Provvisoria", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSTADIT_2_1.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oSTADIT_2_1.GetRadio()
    this.Parent.oContained.w_STADIT = this.RadioValue()
    return .t.
  endfunc

  func oSTADIT_2_1.SetRadio()
    this.Parent.oContained.w_STADIT=trim(this.Parent.oContained.w_STADIT)
    this.value = ;
      iif(this.Parent.oContained.w_STADIT=='',1,;
      iif(this.Parent.oContained.w_STADIT=='S',2,;
      iif(this.Parent.oContained.w_STADIT=='P',3,;
      0)))
  endfunc


  add object oTIPOARTI_2_2 as StdCombo with uid="FFHAXDZFIP",value=1,rtseq=18,rtrep=.f.,left=417,top=215,width=151,height=21;
    , ToolTipText = "Tipo articolo/componente";
    , HelpContextID = 109389441;
    , cFormVar="w_TIPOARTI",RowSource=""+"Nessun filtro,"+"Prodotti finiti,"+"Semilavorati,"+"Materie prime,"+"Prodotti fantasma", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPOARTI_2_2.RadioValue()
    return(iif(this.value =1,"",;
    iif(this.value =2,"PF",;
    iif(this.value =3,"SE",;
    iif(this.value =4,"MP",;
    iif(this.value =5,"PH",;
    space(2)))))))
  endfunc
  func oTIPOARTI_2_2.GetRadio()
    this.Parent.oContained.w_TIPOARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPOARTI_2_2.SetRadio()
    this.Parent.oContained.w_TIPOARTI=trim(this.Parent.oContained.w_TIPOARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOARTI=="",1,;
      iif(this.Parent.oContained.w_TIPOARTI=="PF",2,;
      iif(this.Parent.oContained.w_TIPOARTI=="SE",3,;
      iif(this.Parent.oContained.w_TIPOARTI=="MP",4,;
      iif(this.Parent.oContained.w_TIPOARTI=="PH",5,;
      0)))))
  endfunc

  add object oARTINI_2_7 as StdField with uid="YLDHOMMPET",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice componente incongruente o obsoleto",;
    ToolTipText = "Codice iniziale componenti da implodere (spazio= no selezione)",;
    HelpContextID = 247127802,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=127, Top=53, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARTINI"

  func oARTINI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oARTINI_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oARTINI_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_2_8 as StdField with uid="YJQMLZHVQA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 247135178,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=53, InputMask=replicate('X',40)

  add object oARTFIN_2_9 as StdField with uid="VTFGOGXQDL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice componente incongruente o obsoleto",;
    ToolTipText = "Codice finale componenti da implodere (spazio= no selezione)",;
    HelpContextID = 168681210,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=127, Top=79, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oARTFIN_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco componenti",'GSDS_MDB.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oARTFIN_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_2_10 as StdField with uid="LKEWXBGBZO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168688586,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=79, InputMask=replicate('X',40)

  add object oDISINI_2_13 as StdField with uid="IDOGBMZGUZ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DISINI", cQueryName = "DISINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice distinta",;
    HelpContextID = 247134154,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=127, Top=161, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DISINI"

  func oDISINI_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISINI_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISINI_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDISINI_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Distinta base",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDISFIN_2_14 as StdField with uid="KRDXLDDFYF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DISFIN", cQueryName = "DISFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice distinta",;
    HelpContextID = 168687562,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=127, Top=186, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DISFIN"

  proc oDISFIN_2_14.mDefault
    with this.Parent.oContained
      if empty(.w_DISFIN)
        .w_DISFIN = .w_DISINI
      endif
    endwith
  endproc

  func oDISFIN_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISFIN_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISFIN_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDISFIN_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Distinta base",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oGRUMER_2_16 as StdField with uid="XMFOPADJJN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 105303706,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=252, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUDES_2_17 as StdField with uid="TYNDHAIZBA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_GRUDES", cQueryName = "GRUDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89116314,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=252, InputMask=replicate('X',35)

  add object oGRUMERF_2_19 as StdField with uid="JZBUNOCIYP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_GRUMERF", cQueryName = "GRUMERF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 105303706,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=278, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMERF"

  proc oGRUMERF_2_19.mDefault
    with this.Parent.oContained
      if empty(.w_GRUMERF)
        .w_GRUMERF = .w_GRUMER
      endif
    endwith
  endproc

  func oGRUMERF_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMERF_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMERF_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMERF_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUDESF_2_20 as StdField with uid="RGVVJKTMCC",rtseq=33,rtrep=.f.,;
    cFormVar = "w_GRUDESF", cQueryName = "GRUDESF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89116314,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=278, InputMask=replicate('X',35)

  add object oCODFAMA_2_22 as StdField with uid="BAUZFWPTWC",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODFAMA", cQueryName = "CODFAMA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo",;
    HelpContextID = 193913306,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAMA"

  func oCODFAMA_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAMA_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAMA_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAMA_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAI_2_23 as StdField with uid="AOYBYAMKXI",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193854337,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=304, InputMask=replicate('X',35)

  add object oCODFAMAF_2_25 as StdField with uid="YRHWLVYGJH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CODFAMAF", cQueryName = "CODFAMAF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo",;
    HelpContextID = 193913236,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=330, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAMAF"

  proc oCODFAMAF_2_25.mDefault
    with this.Parent.oContained
      if empty(.w_CODFAMAF)
        .w_CODFAMAF = .w_CODFAMA
      endif
    endwith
  endproc

  func oCODFAMAF_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAMAF_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAMAF_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAMAF_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAIF_2_26 as StdField with uid="NFJHFEKRDD",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESFAMAIF", cQueryName = "DESFAMAIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193853217,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=330, InputMask=replicate('X',35)

  add object oCODCAT_2_28 as StdField with uid="WQNLFQEXDD",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 76669402,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=356, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATI_2_29 as StdField with uid="ZFWYARIWSP",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 191824950,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=356, InputMask=replicate('X',35)

  add object oCODCATF_2_31 as StdField with uid="ZHPCUMRJRL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODCATF", cQueryName = "CODCATF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 76669402,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=382, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCATF"

  proc oCODCATF_2_31.mDefault
    with this.Parent.oContained
      if empty(.w_CODCATF)
        .w_CODCATF = .w_CODCAT
      endif
    endwith
  endproc

  func oCODCATF_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCATF_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCATF_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCATF_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATIF_2_32 as StdField with uid="HLFUVQJIVA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCATIF", cQueryName = "DESCATIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 191825020,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=382, InputMask=replicate('X',35)

  add object oCODMAG_2_34 as StdField with uid="WWAXPPIMPZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 25682394,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=434, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oDESMAGI_2_35 as StdField with uid="QONUNYBHAK",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 242811958,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=434, InputMask=replicate('X',30)

  add object oCODMAGF_2_37 as StdField with uid="SPOYBAMDZC",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODMAGF", cQueryName = "CODMAGF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 25682394,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=408, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAGF"

  proc oCODMAGF_2_37.mDefault
    with this.Parent.oContained
      if empty(.w_CODMAGF)
        .w_CODMAGF = .w_CODMAG
      endif
    endwith
  endproc

  func oCODMAGF_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAGF_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAGF_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAGF_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oDESMAGIF_2_38 as StdField with uid="COSHWVKTLA",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESMAGIF", cQueryName = "DESMAGIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 242812028,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=190, Top=408, InputMask=replicate('X',30)

  add object oDBDESINI_2_41 as StdField with uid="WPKRTVTAFT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DBDESINI", cQueryName = "DBDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26218879,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=161, InputMask=replicate('X',40)

  add object oDBDESFIN_2_42 as StdField with uid="VZICCICWSU",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DBDESFIN", cQueryName = "DBDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244322692,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=186, InputMask=replicate('X',40)

  add object oStr_2_3 as StdString with uid="PJHMIENCGT",Visible=.t., Left=18, Top=215,;
    Alignment=1, Width=99, Height=18,;
    Caption="Stato distinte:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="IXYAPLHJQU",Visible=.t., Left=335, Top=216,;
    Alignment=1, Width=76, Height=15,;
    Caption="Tipo articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="HVXSTNTYAL",Visible=.t., Left=3, Top=13,;
    Alignment=0, Width=273, Height=18,;
    Caption="Selezioni articoli e legami distinte"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="AZAYVNOACS",Visible=.t., Left=19, Top=55,;
    Alignment=1, Width=106, Height=18,;
    Caption="Da componente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="WTGVAEMBZQ",Visible=.t., Left=24, Top=81,;
    Alignment=1, Width=101, Height=18,;
    Caption="A componente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="HBQHQHYLHM",Visible=.t., Left=26, Top=254,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="PIVQMRHITC",Visible=.t., Left=41, Top=280,;
    Alignment=1, Width=84, Height=18,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=26, Top=306,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="EBKWVDHVTH",Visible=.t., Left=41, Top=332,;
    Alignment=1, Width=84, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=26, Top=358,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="GCNVGAQDZZ",Visible=.t., Left=42, Top=384,;
    Alignment=1, Width=83, Height=18,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=26, Top=436,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="NNJXDAMSOQ",Visible=.t., Left=41, Top=410,;
    Alignment=1, Width=84, Height=18,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="HYOXXXYPSO",Visible=.t., Left=29, Top=163,;
    Alignment=1, Width=96, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="FOOJZSLRIN",Visible=.t., Left=56, Top=188,;
    Alignment=1, Width=69, Height=18,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oBox_2_5 as StdBox with uid="LLARDRETCU",left=1, top=36, width=782,height=1
enddefine
define class tgscr_klmPag3 as StdContainer
  Width  = 785
  height = 519
  stdWidth  = 785
  stdheight = 519
  resizeXpos=372
  resizeYpos=242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_3_1 as StdButton with uid="SJTIKSWOLY",left=684, top=472, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per effettuare la sostituzione dei componenti";
    , HelpContextID = 125680618;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_1.Click()
      with this.Parent.oContained
        GSCR_BLM(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not (empty(.w_OLDCAR) or empty(.w_NEWCAR)) and .w_OPERAZ='S'and .w_CARINS='C') or (not empty(.w_OLDCAR) and .w_OPERAZ='D') or (not empty(.w_NEWCAR) and .w_OPERAZ='I'and .w_CARINS='C')or (.w_CARINS='D'))
      endwith
    endif
  endfunc


  add object oBtn_3_2 as StdButton with uid="DAGYNAJGDG",left=734, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 125680618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomSel as cp_szoombox with uid="RUQBJRKCIS",left=3, top=8, width=746,height=440,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCR_ZLM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CONF_ART",;
    cEvent = "Interroga1",;
    nPag=3;
    , HelpContextID = 38628890

  add object oSELEZI_3_4 as StdRadio with uid="KCMBJLDROQ",rtseq=16,rtrep=.f.,left=8, top=487, width=163,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oSELEZI_3_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 234842842
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 234842842
      this.Buttons(2).Top=15
      this.SetAll("Width",161)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_3_4.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_3_4.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_3_4.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object ZoomSel1 as cp_szoombox with uid="YFZKGBOJWK",left=3, top=8, width=778,height=456,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCR1ZLM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="CONF_DIS",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga2",;
    nPag=3;
    , HelpContextID = 38628890


  add object oObj_3_6 as cp_runprogram with uid="BHANWDYKJR",left=-2, top=531, width=147,height=28,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('SS')",;
    cEvent = "w_SELEZI Changed",;
    nPag=3;
    , HelpContextID = 190649011


  add object oObj_3_7 as cp_runprogram with uid="QSWCRGSMQA",left=312, top=531, width=147,height=28,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('INTERROGA')",;
    cEvent = "ActivatePage 3",;
    nPag=3;
    , HelpContextID = 190649011


  add object oObj_3_8 as cp_runprogram with uid="EBVGCFPGTX",left=152, top=531, width=147,height=28,;
    caption='GSCR_BLM',;
   bGlobalFont=.t.,;
    prg="GSCR_BLM('IMPSOS')",;
    cEvent = "ActivatePage 3",;
    nPag=3;
    , HelpContextID = 190649011
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscr_klm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
