* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bac                                                        *
*              Gestione conf articolo                                          *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-09                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bac",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_bac as StdBatch
  * --- Local variables
  w_OPER = space(10)
  GSMA_MAC = .NULL.
  GSMA_MAD = .NULL.
  GSMA_AMP = .NULL.
  w_GESCAR = space(1)
  w_ARCONCAR = space(1)
  * --- WorkFile variables
  CONF_ART_idx=0
  CONF_CAR_idx=0
  CONFDCAR_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Detail Caratt. di Conf. Articolo (GSMA_MAC)
    this.GSMA_MAC = this.oParentObject
    this.GSMA_MAD = this.GSMA_MAC.GSMA_MAD
    this.GSMA_AMP = this.GSMA_MAC.GSMA_AMP
    cCurMAD = this.GSMA_MAD.cTrsname
    do case
      case this.w_OPER="CAR_DETT"
        * --- Riempie il dettaglio
        do case
          case this.oParentObject.w_CCTIPOCA = "C"
            * --- Caratteristiche Fisse
            if not empty(cCurMAD) and used(cCurMAD)
              * --- Riempie il dettaglio delle caratteristiche fisse
              Select (cCurMAD) 
 gopos = RecNo()+1 
 nAbsRow = this.GSMA_MAD.oPgFrm.Page1.oPag.oBody.nAbsRow 
 nRelRow = this.GSMA_MAD.oPgFrm.Page1.oPag.oBody.nRelRow 
 delete all
              * --- Select from CONFDCAR
              i_nConn=i_TableProp[this.CONFDCAR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONFDCAR_idx,2],.t.,this.CONFDCAR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONFDCAR ";
                    +" where CCCODICE="+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+"";
                     ,"_Curs_CONFDCAR")
              else
                select * from (i_cTable);
                 where CCCODICE=this.oParentObject.w_CCCODICE;
                  into cursor _Curs_CONFDCAR
              endif
              if used('_Curs_CONFDCAR')
                select _Curs_CONFDCAR
                locate for 1=1
                do while not(eof())
                this.GSMA_MAD.InitRow()     
                this.GSMA_MAD.w_CPROWNUM = _Curs_CONFDCAR.CPROWNUM
                this.GSMA_MAD.w_CPROWORD = _Curs_CONFDCAR.CPROWORD
                this.GSMA_MAD.w_CCDETTAG = _Curs_CONFDCAR.CCDETTAG
                this.GSMA_MAD.w_CCDESSUP = NVL(_Curs_CONFDCAR.CCDESSUP,"")
                * --- Carica il Temporaneo dei Dati e skippa al record successivo
                this.GSMA_MAD.mCalc(.t.)     
                this.GSMA_MAD.TrsFromWork()     
                this.GSMA_MAD.ChildrenChangeRow()     
                  select _Curs_CONFDCAR
                  continue
                enddo
                use
              endif
              * --- Rinfrescamenti vari
              SELECT (cCurMAD) 
 GO iif(gopos<=RecCount(cCurMAD), gopos, 1) 
 With this.GSMA_MAD 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=nAbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow=nRelRow 
 .bUpDated = TRUE 
 EndWith
            endif
          case this.oParentObject.w_CCTIPOCA = "M"
            * --- Caratteristiche Parametriche
            if not empty(nvl(this.oParentObject.w_CCCODICE,""))
              * --- Inserisce il dettaglio dalle Caratteristiche Parametriche
              * --- Read from CONF_CAR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONF_CAR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONF_CAR_idx,2],.t.,this.CONF_CAR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "*"+;
                  " from "+i_cTable+" CONF_CAR where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                      +" and CCTIPOCA = "+cp_ToStrODBC("M");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  *;
                  from (i_cTable) where;
                      CCCODICE = this.oParentObject.w_CCCODICE;
                      and CCTIPOCA = "M";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.GSMA_AMP.w_CCFORMUL = NVL(cp_ToDate(_read_.CCFORMUL),cp_NullValue(_read_.CCFORMUL))
                this.GSMA_AMP.w_CCVAR101 = NVL(cp_ToDate(_read_.CCVAR101),cp_NullValue(_read_.CCVAR101))
                this.GSMA_AMP.w_CCQTA101 = NVL(cp_ToDate(_read_.CCQTA101),cp_NullValue(_read_.CCQTA101))
                this.GSMA_AMP.w_CCVAR102 = NVL(cp_ToDate(_read_.CCVAR102),cp_NullValue(_read_.CCVAR102))
                this.GSMA_AMP.w_CCQTA102 = NVL(cp_ToDate(_read_.CCQTA102),cp_NullValue(_read_.CCQTA102))
                this.GSMA_AMP.w_CCVAR103 = NVL(cp_ToDate(_read_.CCVAR103),cp_NullValue(_read_.CCVAR103))
                this.GSMA_AMP.w_CCQTA103 = NVL(cp_ToDate(_read_.CCQTA103),cp_NullValue(_read_.CCQTA103))
                this.GSMA_AMP.w_CCVAR104 = NVL(cp_ToDate(_read_.CCVAR104),cp_NullValue(_read_.CCVAR104))
                this.GSMA_AMP.w_CCQTA104 = NVL(cp_ToDate(_read_.CCQTA104),cp_NullValue(_read_.CCQTA104))
                this.GSMA_AMP.w_CCVAR105 = NVL(cp_ToDate(_read_.CCVAR105),cp_NullValue(_read_.CCVAR105))
                this.GSMA_AMP.w_CCQTA105 = NVL(cp_ToDate(_read_.CCQTA105),cp_NullValue(_read_.CCQTA105))
                this.GSMA_AMP.w_CCVAR106 = NVL(cp_ToDate(_read_.CCVAR106),cp_NullValue(_read_.CCVAR106))
                this.GSMA_AMP.w_CCQTA106 = NVL(cp_ToDate(_read_.CCQTA106),cp_NullValue(_read_.CCQTA106))
                this.GSMA_AMP.w_CCVAR107 = NVL(cp_ToDate(_read_.CCVAR107),cp_NullValue(_read_.CCVAR107))
                this.GSMA_AMP.w_CCQTA107 = NVL(cp_ToDate(_read_.CCQTA107),cp_NullValue(_read_.CCQTA107))
                this.GSMA_AMP.w_CCVAR108 = NVL(cp_ToDate(_read_.CCVAR108),cp_NullValue(_read_.CCVAR108))
                this.GSMA_AMP.w_CCQTA108 = NVL(cp_ToDate(_read_.CCQTA108),cp_NullValue(_read_.CCQTA108))
                this.GSMA_AMP.w_CCVAR109 = NVL(cp_ToDate(_read_.CCVAR109),cp_NullValue(_read_.CCVAR109))
                this.GSMA_AMP.w_CCQTA109 = NVL(cp_ToDate(_read_.CCQTA109),cp_NullValue(_read_.CCQTA109))
                this.GSMA_AMP.w_CCVAR110 = NVL(cp_ToDate(_read_.CCVAR110),cp_NullValue(_read_.CCVAR110))
                this.GSMA_AMP.w_CCQTA110 = NVL(cp_ToDate(_read_.CCQTA110),cp_NullValue(_read_.CCQTA110))
                this.GSMA_AMP.w_CCVAR111 = NVL(cp_ToDate(_read_.CCVAR111),cp_NullValue(_read_.CCVAR111))
                this.GSMA_AMP.w_CCQTA111 = NVL(cp_ToDate(_read_.CCQTA111),cp_NullValue(_read_.CCQTA111))
                this.GSMA_AMP.w_CCVAR112 = NVL(cp_ToDate(_read_.CCVAR112),cp_NullValue(_read_.CCVAR112))
                this.GSMA_AMP.w_CCQTA112 = NVL(cp_ToDate(_read_.CCQTA112),cp_NullValue(_read_.CCQTA112))
                this.GSMA_AMP.w_CCVAR113 = NVL(cp_ToDate(_read_.CCVAR113),cp_NullValue(_read_.CCVAR113))
                this.GSMA_AMP.w_CCQTA113 = NVL(cp_ToDate(_read_.CCQTA113),cp_NullValue(_read_.CCQTA113))
                this.GSMA_AMP.w_CCVAR114 = NVL(cp_ToDate(_read_.CCVAR114),cp_NullValue(_read_.CCVAR114))
                this.GSMA_AMP.w_CCQTA114 = NVL(cp_ToDate(_read_.CCQTA114),cp_NullValue(_read_.CCQTA114))
                this.GSMA_AMP.w_CCVAR115 = NVL(cp_ToDate(_read_.CCVAR115),cp_NullValue(_read_.CCVAR115))
                this.GSMA_AMP.w_CCQTA115 = NVL(cp_ToDate(_read_.CCQTA115),cp_NullValue(_read_.CCQTA115))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from CONF_CAR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONF_CAR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONF_CAR_idx,2],.t.,this.CONF_CAR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "*"+;
                  " from "+i_cTable+" CONF_CAR where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                      +" and CCTIPOCA = "+cp_ToStrODBC("M");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  *;
                  from (i_cTable) where;
                      CCCODICE = this.oParentObject.w_CCCODICE;
                      and CCTIPOCA = "M";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.GSMA_AMP.w_CCMIN101 = NVL(cp_ToDate(_read_.CCMIN101),cp_NullValue(_read_.CCMIN101))
                this.GSMA_AMP.w_CCMAX101 = NVL(cp_ToDate(_read_.CCMAX101),cp_NullValue(_read_.CCMAX101))
                this.GSMA_AMP.w_CCMIN102 = NVL(cp_ToDate(_read_.CCMIN102),cp_NullValue(_read_.CCMIN102))
                this.GSMA_AMP.w_CCMAX102 = NVL(cp_ToDate(_read_.CCMAX102),cp_NullValue(_read_.CCMAX102))
                this.GSMA_AMP.w_CCMIN103 = NVL(cp_ToDate(_read_.CCMIN103),cp_NullValue(_read_.CCMIN103))
                this.GSMA_AMP.w_CCMAX103 = NVL(cp_ToDate(_read_.CCMAX103),cp_NullValue(_read_.CCMAX103))
                this.GSMA_AMP.w_CCMIN104 = NVL(cp_ToDate(_read_.CCMIN104),cp_NullValue(_read_.CCMIN104))
                this.GSMA_AMP.w_CCMAX104 = NVL(cp_ToDate(_read_.CCMAX104),cp_NullValue(_read_.CCMAX104))
                this.GSMA_AMP.w_CCMIN105 = NVL(cp_ToDate(_read_.CCMIN105),cp_NullValue(_read_.CCMIN105))
                this.GSMA_AMP.w_CCMAX105 = NVL(cp_ToDate(_read_.CCMAX105),cp_NullValue(_read_.CCMAX105))
                this.GSMA_AMP.w_CCMIN106 = NVL(cp_ToDate(_read_.CCMIN106),cp_NullValue(_read_.CCMIN106))
                this.GSMA_AMP.w_CCMAX106 = NVL(cp_ToDate(_read_.CCMAX106),cp_NullValue(_read_.CCMAX106))
                this.GSMA_AMP.w_CCMIN107 = NVL(cp_ToDate(_read_.CCMIN107),cp_NullValue(_read_.CCMIN107))
                this.GSMA_AMP.w_CCMAX107 = NVL(cp_ToDate(_read_.CCMAX107),cp_NullValue(_read_.CCMAX107))
                this.GSMA_AMP.w_CCMIN108 = NVL(cp_ToDate(_read_.CCMIN108),cp_NullValue(_read_.CCMIN108))
                this.GSMA_AMP.w_CCMAX108 = NVL(cp_ToDate(_read_.CCMAX108),cp_NullValue(_read_.CCMAX108))
                this.GSMA_AMP.w_CCMIN109 = NVL(cp_ToDate(_read_.CCMIN109),cp_NullValue(_read_.CCMIN109))
                this.GSMA_AMP.w_CCMAX109 = NVL(cp_ToDate(_read_.CCMAX109),cp_NullValue(_read_.CCMAX109))
                this.GSMA_AMP.w_CCMIN110 = NVL(cp_ToDate(_read_.CCMIN110),cp_NullValue(_read_.CCMIN110))
                this.GSMA_AMP.w_CCMAX110 = NVL(cp_ToDate(_read_.CCMAX110),cp_NullValue(_read_.CCMAX110))
                this.GSMA_AMP.w_CCMIN111 = NVL(cp_ToDate(_read_.CCMIN111),cp_NullValue(_read_.CCMIN111))
                this.GSMA_AMP.w_CCMAX111 = NVL(cp_ToDate(_read_.CCMAX111),cp_NullValue(_read_.CCMAX111))
                this.GSMA_AMP.w_CCMIN112 = NVL(cp_ToDate(_read_.CCMIN112),cp_NullValue(_read_.CCMIN112))
                this.GSMA_AMP.w_CCMAX112 = NVL(cp_ToDate(_read_.CCMAX112),cp_NullValue(_read_.CCMAX112))
                this.GSMA_AMP.w_CCMIN113 = NVL(cp_ToDate(_read_.CCMIN113),cp_NullValue(_read_.CCMIN113))
                this.GSMA_AMP.w_CCMAX113 = NVL(cp_ToDate(_read_.CCMAX113),cp_NullValue(_read_.CCMAX113))
                this.GSMA_AMP.w_CCMIN114 = NVL(cp_ToDate(_read_.CCMIN114),cp_NullValue(_read_.CCMIN114))
                this.GSMA_AMP.w_CCMAX114 = NVL(cp_ToDate(_read_.CCMAX114),cp_NullValue(_read_.CCMAX114))
                this.GSMA_AMP.w_CCMIN115 = NVL(cp_ToDate(_read_.CCMIN115),cp_NullValue(_read_.CCMIN115))
                this.GSMA_AMP.w_CCMAX115 = NVL(cp_ToDate(_read_.CCMAX115),cp_NullValue(_read_.CCMAX115))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.GSMA_AMP.mCalc(True)     
              this.GSMA_AMP.SaveDependsOn()     
              this.GSMA_AMP.SetControlsValue()     
              this.GSMA_AMP.mHideControls()     
              this.GSMA_AMP.ChildrenChangeRow()     
            endif
        endcase
      case this.w_OPER="UPD_CCAR"
        this.w_ARCONCAR = this.GSMA_MAC.oParentObject.w_ARCONCAR
        this.w_GESCAR = "N"
        * --- Select from CONF_ART
        i_nConn=i_TableProp[this.CONF_ART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CCCODICE  from "+i_cTable+" CONF_ART ";
              +" where CCCODART="+cp_ToStrODBC(this.oParentObject.w_CCCODART)+"";
               ,"_Curs_CONF_ART")
        else
          select CCCODICE from (i_cTable);
           where CCCODART=this.oParentObject.w_CCCODART;
            into cursor _Curs_CONF_ART
        endif
        if used('_Curs_CONF_ART')
          select _Curs_CONF_ART
          locate for 1=1
          do while not(eof())
          this.w_GESCAR = "S"
            select _Curs_CONF_ART
            continue
          enddo
          use
        endif
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARGESCAR ="+cp_NullLink(cp_ToStrODBC(this.w_GESCAR),'ART_ICOL','ARGESCAR');
          +",ARCONCAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARCONCAR),'ART_ICOL','ARCONCAR');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
                 )
        else
          update (i_cTable) set;
              ARGESCAR = this.w_GESCAR;
              ,ARCONCAR = this.w_ARCONCAR;
              &i_ccchkf. ;
           where;
              ARCODART = this.oParentObject.w_CCCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONF_ART'
    this.cWorkTables[2]='CONF_CAR'
    this.cWorkTables[3]='CONFDCAR'
    this.cWorkTables[4]='ART_ICOL'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CONFDCAR')
      use in _Curs_CONFDCAR
    endif
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
