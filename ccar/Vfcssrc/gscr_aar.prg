* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_aar                                                        *
*              Articoli configurabili                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-24                                                      *
* Last revis.: 2016-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_aar"))

* --- Class definition
define class tgscr_aar as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 730
  Height = 297+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-22"
  HelpContextID=143226217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=62

  * --- Constant Properties
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CENCOST_IDX = 0
  KEY_ARTI_IDX = 0
  PAR_DISB_IDX = 0
  TIPCODIV_IDX = 0
  DISMBASE_IDX = 0
  PAR_DISB_IDX = 0
  ART_DIST_IDX = 0
  cFile = "ART_ICOL"
  cKeySelect = "ARCODART"
  cKeyWhere  = "ARCODART=this.w_ARCODART"
  cKeyWhereODBC = '"ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cKeyWhereODBCqualified = '"ART_ICOL.ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cPrg = "gscr_aar"
  cComment = "Articoli configurabili"
  icon = "anag.ico"
  cAutoZoom = 'GSCR0AAV'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_READPARPROD = space(2)
  w_DBCODVAR = space(5)
  w_UM_ORE = space(3)
  w_TIPOPE = space(4)
  w_ARCODART = space(20)
  o_ARCODART = space(20)
  w_ARDESART = space(40)
  w_ARDESSUP = space(0)
  w_ARCONCAR = space(1)
  w_ARCMPCAR = space(1)
  w_ARTIPART = space(2)
  o_ARTIPART = space(2)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  o_ARUNMIS2 = space(3)
  w_AROPERAT = space(1)
  w_ARMOLTIP = 0
  w_ARDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CODESE = space(4)
  w_FLSCM = space(1)
  w_FLPRO = space(1)
  w_ARDTINVA = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OMODKEY = space(10)
  w_NMODKEY = space(10)
  w_ARTIPBAR = space(1)
  w_ARPROPRE = space(1)
  w_ARTIPGES = space(1)
  w_ODATKEY = space(10)
  w_NDATKEY = space(10)
  w_ARCODCEN = space(15)
  w_CCDESPIA = space(40)
  w_ARPREZUM = space(1)
  w_ARDATINT = space(1)
  w_ARCATOPE = space(2)
  w_ARTIPOPE = space(10)
  w_ARCCRIFE = space(20)
  o_ARCCRIFE = space(20)
  w_DESART = space(40)
  w_ARCCCHAR = space(1)
  w_ARCCLUNG = 0
  o_ARCCLUNG = 0
  w_ARCCPROG = space(18)
  w_PROLEN = 0
  w_TIPART = space(2)
  w_CODVAR = space(5)
  w_UNMIS = space(3)
  w_CENCOS = space(15)
  w_UNMIS2 = space(3)
  w_OPERAT = 0
  w_MOLTIP = space(1)
  w_GESTGUID = space(14)
  w_ARCATCES = space(15)
  w_ARCODDIS = space(20)
  o_ARCODDIS = space(20)
  w_ARFLCOMP = space(1)
  w_DESDIS = space(40)
  w_ARKITIMB = space(1)
  o_ARKITIMB = space(1)
  w_DBOBSO = ctod('  /  /  ')
  w_DBINVA = ctod('  /  /  ')
  w_DISKIT = space(10)
  w_ARSALCOM = space(1)
  w_ARTIPPKR = space(2)

  * --- Children pointers
  GSMA_MAC = .NULL.
  GSMA_MLI = .NULL.
  GSMA_MDA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ART_ICOL','gscr_aar')
    stdPageFrame::Init()
    *set procedure to GSMA_MAC additive
    *set procedure to GSMA_MLI additive
    with this
      .Pages(1).addobject("oPag","tgscr_aarPag1","gscr_aar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(1).HelpContextID = 135843786
      .Pages(2).addobject("oPag","tgscr_aarPag2","gscr_aar",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Produzione funzioni avanzate")
      .Pages(2).HelpContextID = 193858732
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODART_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMA_MAC
    *release procedure GSMA_MLI
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='PAR_DISB'
    this.cWorkTables[5]='TIPCODIV'
    this.cWorkTables[6]='DISMBASE'
    this.cWorkTables[7]='PAR_DISB'
    this.cWorkTables[8]='ART_DIST'
    this.cWorkTables[9]='ART_ICOL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ART_ICOL_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MAC = CREATEOBJECT('stdLazyChild',this,'GSMA_MAC')
    this.GSMA_MLI = CREATEOBJECT('stdLazyChild',this,'GSMA_MLI')
    this.GSMA_MDA = CREATEOBJECT('stdDynamicChild',this,'GSMA_MDA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MAC)
      this.GSMA_MAC.DestroyChildrenChain()
      this.GSMA_MAC=.NULL.
    endif
    if !ISNULL(this.GSMA_MLI)
      this.GSMA_MLI.DestroyChildrenChain()
      this.GSMA_MLI=.NULL.
    endif
    if !ISNULL(this.GSMA_MDA)
      this.GSMA_MDA.DestroyChildrenChain()
      this.GSMA_MDA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MAC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_MDA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MAC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_MDA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MAC.NewDocument()
    this.GSMA_MLI.NewDocument()
    this.GSMA_MDA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MAC.SetKey(;
            .w_ARCODART,"CCCODART";
            )
      this.GSMA_MLI.SetKey(;
            .w_ARCODART,"LICODART";
            )
      this.GSMA_MDA.SetKey(;
            .w_ARCODART,"DICODART";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MAC.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"CCCODART";
             )
      .GSMA_MLI.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"LICODART";
             )
      .GSMA_MDA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"DICODART";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_MDA)
        i_f=.GSMA_MDA.BuildFilter()
        if !(i_f==.GSMA_MDA.cQueryFilter)
          i_fnidx=.GSMA_MDA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MDA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MDA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MDA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MDA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ARCODART = NVL(ARCODART,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_45_joined
    link_1_45_joined=.f.
    local link_1_55_joined
    link_1_55_joined=.f.
    local link_1_77_joined
    link_1_77_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ART_ICOL where ARCODART=KeySet.ARCODART
    *
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ART_ICOL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ART_ICOL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ART_ICOL '
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_55_joined=this.AddJoinedLink_1_55(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_77_joined=this.AddJoinedLink_1_77(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_READPARPROD = i_CODAZI
        .w_DBCODVAR = space(5)
        .w_UM_ORE = space(3)
        .w_FLSCM = space(1)
        .w_FLPRO = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CCDESPIA = space(40)
        .w_DESART = space(40)
        .w_PROLEN = 0
        .w_TIPART = space(2)
        .w_CODVAR = space(5)
        .w_UNMIS = space(3)
        .w_CENCOS = space(15)
        .w_UNMIS2 = space(3)
        .w_OPERAT = 0
        .w_MOLTIP = space(1)
        .w_DESDIS = space(40)
        .w_DBOBSO = ctod("  /  /  ")
        .w_DBINVA = ctod("  /  /  ")
        .w_DISKIT = space(10)
          .link_1_1('Load')
        .w_TIPOPE = this.cFunction
        .w_ARCODART = NVL(ARCODART,space(20))
        .w_ARDESART = NVL(ARDESART,space(40))
        .w_ARDESSUP = NVL(ARDESSUP,space(0))
        .w_ARCONCAR = NVL(ARCONCAR,space(1))
        .w_ARCMPCAR = NVL(ARCMPCAR,space(1))
        .w_ARTIPART = NVL(ARTIPART,space(2))
        .w_ARUNMIS1 = NVL(ARUNMIS1,space(3))
          * evitabile
          *.link_1_12('Load')
        .w_ARUNMIS2 = NVL(ARUNMIS2,space(3))
          * evitabile
          *.link_1_13('Load')
        .w_AROPERAT = NVL(AROPERAT,space(1))
        .w_ARMOLTIP = NVL(ARMOLTIP,0)
        .w_ARDTOBSO = NVL(cp_ToDate(ARDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_ARDTINVA = NVL(cp_ToDate(ARDTINVA),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .w_OMODKEY = .w_ARDESART+.w_ARDESSUP
        .w_NMODKEY = .w_ARDESART+.w_ARDESSUP
        .w_ARTIPBAR = NVL(ARTIPBAR,space(1))
        .w_ARPROPRE = NVL(ARPROPRE,space(1))
        .w_ARTIPGES = NVL(ARTIPGES,space(1))
        .w_ODATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        .w_NDATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        .w_ARCODCEN = NVL(ARCODCEN,space(15))
          if link_1_45_joined
            this.w_ARCODCEN = NVL(CC_CONTO145,NVL(this.w_ARCODCEN,space(15)))
            this.w_CCDESPIA = NVL(CCDESPIA145,space(40))
          else
          .link_1_45('Load')
          endif
        .w_ARPREZUM = NVL(ARPREZUM,space(1))
        .w_ARDATINT = NVL(ARDATINT,space(1))
        .w_ARCATOPE = NVL(ARCATOPE,space(2))
        .w_ARTIPOPE = NVL(ARTIPOPE,space(10))
          * evitabile
          *.link_1_51('Load')
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .w_ARCCRIFE = NVL(ARCCRIFE,space(20))
          if link_1_55_joined
            this.w_ARCCRIFE = NVL(ARCODART155,NVL(this.w_ARCCRIFE,space(20)))
            this.w_DESART = NVL(ARDESART155,space(40))
            this.w_UNMIS = NVL(ARUNMIS1155,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2155,space(3))
            this.w_OPERAT = NVL(AROPERAT155,0)
            this.w_MOLTIP = NVL(ARMOLTIP155,space(1))
            this.w_CENCOS = NVL(ARCODCEN155,space(15))
            this.w_TIPART = NVL(ARTIPART155,space(2))
          else
          .link_1_55('Load')
          endif
        .w_ARCCCHAR = NVL(ARCCCHAR,space(1))
        .w_ARCCLUNG = NVL(ARCCLUNG,0)
        .w_ARCCPROG = NVL(ARCCPROG,space(18))
        .w_GESTGUID = NVL(GESTGUID,space(14))
        .w_ARCATCES = NVL(ARCATCES,space(15))
        .w_ARCODDIS = NVL(ARCODDIS,space(20))
          if link_1_77_joined
            this.w_ARCODDIS = NVL(DBCODICE177,NVL(this.w_ARCODDIS,space(20)))
            this.w_DESDIS = NVL(DBDESCRI177,space(40))
            this.w_DBOBSO = NVL(cp_ToDate(DBDTOBSO177),ctod("  /  /  "))
            this.w_DBINVA = NVL(cp_ToDate(DBDTINVA177),ctod("  /  /  "))
            this.w_DISKIT = NVL(DBDISKIT177,space(10))
          else
          .link_1_77('Load')
          endif
        .w_ARFLCOMP = NVL(ARFLCOMP,space(1))
        .w_ARKITIMB = NVL(ARKITIMB,space(1))
        .w_ARSALCOM = NVL(ARSALCOM,space(1))
        .w_ARTIPPKR = NVL(ARTIPPKR,space(2))
        cp_LoadRecExtFlds(this,'ART_ICOL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscr_aar
    if NOT this.w_ARTIPART $ 'DC-PM-FA-PS-CC'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPARPROD = space(2)
      .w_DBCODVAR = space(5)
      .w_UM_ORE = space(3)
      .w_TIPOPE = space(4)
      .w_ARCODART = space(20)
      .w_ARDESART = space(40)
      .w_ARDESSUP = space(0)
      .w_ARCONCAR = space(1)
      .w_ARCMPCAR = space(1)
      .w_ARTIPART = space(2)
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS2 = space(3)
      .w_AROPERAT = space(1)
      .w_ARMOLTIP = 0
      .w_ARDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CODESE = space(4)
      .w_FLSCM = space(1)
      .w_FLPRO = space(1)
      .w_ARDTINVA = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_OMODKEY = space(10)
      .w_NMODKEY = space(10)
      .w_ARTIPBAR = space(1)
      .w_ARPROPRE = space(1)
      .w_ARTIPGES = space(1)
      .w_ODATKEY = space(10)
      .w_NDATKEY = space(10)
      .w_ARCODCEN = space(15)
      .w_CCDESPIA = space(40)
      .w_ARPREZUM = space(1)
      .w_ARDATINT = space(1)
      .w_ARCATOPE = space(2)
      .w_ARTIPOPE = space(10)
      .w_ARCCRIFE = space(20)
      .w_DESART = space(40)
      .w_ARCCCHAR = space(1)
      .w_ARCCLUNG = 0
      .w_ARCCPROG = space(18)
      .w_PROLEN = 0
      .w_TIPART = space(2)
      .w_CODVAR = space(5)
      .w_UNMIS = space(3)
      .w_CENCOS = space(15)
      .w_UNMIS2 = space(3)
      .w_OPERAT = 0
      .w_MOLTIP = space(1)
      .w_GESTGUID = space(14)
      .w_ARCATCES = space(15)
      .w_ARCODDIS = space(20)
      .w_ARFLCOMP = space(1)
      .w_DESDIS = space(40)
      .w_ARKITIMB = space(1)
      .w_DBOBSO = ctod("  /  /  ")
      .w_DBINVA = ctod("  /  /  ")
      .w_DISKIT = space(10)
      .w_ARSALCOM = space(1)
      .w_ARTIPPKR = space(2)
      if .cFunction<>"Filter"
        .w_READPARPROD = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_READPARPROD))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,3,.f.)
        .w_TIPOPE = this.cFunction
          .DoRTCalc(5,7,.f.)
        .w_ARCONCAR =  iif(.w_ARTIPART='CC','S', 'N')
        .w_ARCMPCAR =  iif(.w_ARTIPART='CC', .w_ARCMPCAR, 'N')
        .w_ARTIPART = 'CC'
        .w_ARUNMIS1 = iif(!EMPTY(nvl(.w_ARCCRIFE,'')),.w_UNMIS,.w_ARUNMIS1)
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_ARUNMIS1))
          .link_1_12('Full')
          endif
        .w_ARUNMIS2 = iif(!EMPTY(.w_ARCCRIFE),.w_UNMIS2,.w_ARUNMIS2)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_ARUNMIS2))
          .link_1_13('Full')
          endif
        .w_AROPERAT = iif(!EMPTY(.w_ARCCRIFE),.w_OPERAT,.w_AROPERAT)
        .w_ARMOLTIP = iif(!EMPTY(.w_ARCCRIFE),.w_MOLTIP,.w_ARMOLTIP)
          .DoRTCalc(15,19,.f.)
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
          .DoRTCalc(21,23,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
          .DoRTCalc(25,25,.f.)
        .w_OMODKEY = .w_ARDESART+.w_ARDESSUP
        .w_NMODKEY = .w_ARDESART+.w_ARDESSUP
          .DoRTCalc(28,28,.f.)
        .w_ARPROPRE = "I"
        .w_ARTIPGES = "F"
        .w_ODATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        .w_NDATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        .w_ARCODCEN = iif(!EMPTY(.w_ARCCRIFE),.w_CENCOS,.w_ARCODCEN)
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_ARCODCEN))
          .link_1_45('Full')
          endif
          .DoRTCalc(34,34,.f.)
        .w_ARPREZUM = 'N'
        .w_ARDATINT = 'F'
        .w_ARCATOPE = 'AR'
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_ARTIPOPE))
          .link_1_51('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_ARCCRIFE))
          .link_1_55('Full')
          endif
          .DoRTCalc(40,40,.f.)
        .w_ARCCCHAR = '|'
        .w_ARCCLUNG = iif(.w_PROLEN=0,7,.w_PROLEN)
          .DoRTCalc(43,53,.f.)
        .w_ARCODDIS = IIF(.w_ARKITIMB='N',.w_ARCODDIS, Space(20))
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_ARCODDIS))
          .link_1_77('Full')
          endif
        .w_ARFLCOMP = ' '
          .DoRTCalc(56,56,.f.)
        .w_ARKITIMB = 'N'
          .DoRTCalc(58,60,.f.)
        .w_ARSALCOM = ' '
      endif
    endwith
    cp_BlankRecExtFlds(this,'ART_ICOL')
    this.DoRTCalc(62,62,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oARCODART_1_5.enabled = i_bVal
      .Page1.oPag.oARDESART_1_6.enabled = i_bVal
      .Page1.oPag.oARDESSUP_1_7.enabled = i_bVal
      .Page1.oPag.oARCMPCAR_1_9.enabled = i_bVal
      .Page1.oPag.oARUNMIS1_1_12.enabled = i_bVal
      .Page1.oPag.oARUNMIS2_1_13.enabled = i_bVal
      .Page1.oPag.oAROPERAT_1_14.enabled = i_bVal
      .Page1.oPag.oARMOLTIP_1_15.enabled = i_bVal
      .Page1.oPag.oARCODCEN_1_45.enabled = i_bVal
      .Page1.oPag.oARCCRIFE_1_55.enabled = i_bVal
      .Page1.oPag.oARCCCHAR_1_58.enabled = i_bVal
      .Page1.oPag.oARCCLUNG_1_60.enabled = i_bVal
      .Page1.oPag.oARCODDIS_1_77.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oARCODART_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oARCODART_1_5.enabled = .t.
        .Page1.oPag.oARDESART_1_6.enabled = .t.
      endif
    endwith
    this.GSMA_MAC.SetStatus(i_cOp)
    this.GSMA_MLI.SetStatus(i_cOp)
    this.GSMA_MDA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ART_ICOL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MAC.SetChildrenStatus(i_cOp)
  *  this.GSMA_MLI.SetChildrenStatus(i_cOp)
  *  this.GSMA_MDA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODART,"ARCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESART,"ARDESART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESSUP,"ARDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCONCAR,"ARCONCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCMPCAR,"ARCMPCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPART,"ARTIPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS1,"ARUNMIS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS2,"ARUNMIS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AROPERAT,"AROPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLTIP,"ARMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTOBSO,"ARDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTINVA,"ARDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPBAR,"ARTIPBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPROPRE,"ARPROPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPGES,"ARTIPGES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCEN,"ARCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPREZUM,"ARPREZUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDATINT,"ARDATINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATOPE,"ARCATOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPOPE,"ARTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCCRIFE,"ARCCRIFE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCCCHAR,"ARCCCHAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCCLUNG,"ARCCLUNG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCCPROG,"ARCCPROG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GESTGUID,"GESTGUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATCES,"ARCATCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODDIS,"ARCODDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCOMP,"ARFLCOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARKITIMB,"ARKITIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSALCOM,"ARSALCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPPKR,"ARTIPPKR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    i_lTable = "ART_ICOL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ART_ICOL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValODBCExtFlds(this,'ART_ICOL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ARCODART,ARDESART,ARDESSUP,ARCONCAR,ARCMPCAR"+;
                  ",ARTIPART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
                  ",ARDTOBSO,UTCC,UTCV,UTDC,UTDV"+;
                  ",ARDTINVA,ARTIPBAR,ARPROPRE,ARTIPGES,ARCODCEN"+;
                  ",ARPREZUM,ARDATINT,ARCATOPE,ARTIPOPE,ARCCRIFE"+;
                  ",ARCCCHAR,ARCCLUNG,ARCCPROG,GESTGUID,ARCATCES"+;
                  ",ARCODDIS,ARFLCOMP,ARKITIMB,ARSALCOM,ARTIPPKR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ARCODART)+;
                  ","+cp_ToStrODBC(this.w_ARDESART)+;
                  ","+cp_ToStrODBC(this.w_ARDESSUP)+;
                  ","+cp_ToStrODBC(this.w_ARCONCAR)+;
                  ","+cp_ToStrODBC(this.w_ARCMPCAR)+;
                  ","+cp_ToStrODBC(this.w_ARTIPART)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
                  ","+cp_ToStrODBC(this.w_AROPERAT)+;
                  ","+cp_ToStrODBC(this.w_ARMOLTIP)+;
                  ","+cp_ToStrODBC(this.w_ARDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ARDTINVA)+;
                  ","+cp_ToStrODBC(this.w_ARTIPBAR)+;
                  ","+cp_ToStrODBC(this.w_ARPROPRE)+;
                  ","+cp_ToStrODBC(this.w_ARTIPGES)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODCEN)+;
                  ","+cp_ToStrODBC(this.w_ARPREZUM)+;
                  ","+cp_ToStrODBC(this.w_ARDATINT)+;
                  ","+cp_ToStrODBC(this.w_ARCATOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARCCRIFE)+;
                  ","+cp_ToStrODBC(this.w_ARCCCHAR)+;
                  ","+cp_ToStrODBC(this.w_ARCCLUNG)+;
                  ","+cp_ToStrODBC(this.w_ARCCPROG)+;
                  ","+cp_ToStrODBC(this.w_GESTGUID)+;
                  ","+cp_ToStrODBC(this.w_ARCATCES)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODDIS)+;
                  ","+cp_ToStrODBC(this.w_ARFLCOMP)+;
                  ","+cp_ToStrODBC(this.w_ARKITIMB)+;
                  ","+cp_ToStrODBC(this.w_ARSALCOM)+;
                  ","+cp_ToStrODBC(this.w_ARTIPPKR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValVFPExtFlds(this,'ART_ICOL')
        cp_CheckDeletedKey(i_cTable,0,'ARCODART',this.w_ARCODART)
        INSERT INTO (i_cTable);
              (ARCODART,ARDESART,ARDESSUP,ARCONCAR,ARCMPCAR,ARTIPART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARDTOBSO,UTCC,UTCV,UTDC,UTDV,ARDTINVA,ARTIPBAR,ARPROPRE,ARTIPGES,ARCODCEN,ARPREZUM,ARDATINT,ARCATOPE,ARTIPOPE,ARCCRIFE,ARCCCHAR,ARCCLUNG,ARCCPROG,GESTGUID,ARCATCES,ARCODDIS,ARFLCOMP,ARKITIMB,ARSALCOM,ARTIPPKR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ARCODART;
                  ,this.w_ARDESART;
                  ,this.w_ARDESSUP;
                  ,this.w_ARCONCAR;
                  ,this.w_ARCMPCAR;
                  ,this.w_ARTIPART;
                  ,this.w_ARUNMIS1;
                  ,this.w_ARUNMIS2;
                  ,this.w_AROPERAT;
                  ,this.w_ARMOLTIP;
                  ,this.w_ARDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ARDTINVA;
                  ,this.w_ARTIPBAR;
                  ,this.w_ARPROPRE;
                  ,this.w_ARTIPGES;
                  ,this.w_ARCODCEN;
                  ,this.w_ARPREZUM;
                  ,this.w_ARDATINT;
                  ,this.w_ARCATOPE;
                  ,this.w_ARTIPOPE;
                  ,this.w_ARCCRIFE;
                  ,this.w_ARCCCHAR;
                  ,this.w_ARCCLUNG;
                  ,this.w_ARCCPROG;
                  ,this.w_GESTGUID;
                  ,this.w_ARCATCES;
                  ,this.w_ARCODDIS;
                  ,this.w_ARFLCOMP;
                  ,this.w_ARKITIMB;
                  ,this.w_ARSALCOM;
                  ,this.w_ARTIPPKR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ART_ICOL_IDX,i_nConn)
      *
      * update ART_ICOL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ART_ICOL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ARDESART="+cp_ToStrODBC(this.w_ARDESART)+;
             ",ARDESSUP="+cp_ToStrODBC(this.w_ARDESSUP)+;
             ",ARCONCAR="+cp_ToStrODBC(this.w_ARCONCAR)+;
             ",ARCMPCAR="+cp_ToStrODBC(this.w_ARCMPCAR)+;
             ",ARTIPART="+cp_ToStrODBC(this.w_ARTIPART)+;
             ",ARUNMIS1="+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
             ",ARUNMIS2="+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
             ",AROPERAT="+cp_ToStrODBC(this.w_AROPERAT)+;
             ",ARMOLTIP="+cp_ToStrODBC(this.w_ARMOLTIP)+;
             ",ARDTOBSO="+cp_ToStrODBC(this.w_ARDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ARDTINVA="+cp_ToStrODBC(this.w_ARDTINVA)+;
             ",ARTIPBAR="+cp_ToStrODBC(this.w_ARTIPBAR)+;
             ",ARPROPRE="+cp_ToStrODBC(this.w_ARPROPRE)+;
             ",ARTIPGES="+cp_ToStrODBC(this.w_ARTIPGES)+;
             ",ARCODCEN="+cp_ToStrODBCNull(this.w_ARCODCEN)+;
             ",ARPREZUM="+cp_ToStrODBC(this.w_ARPREZUM)+;
             ",ARDATINT="+cp_ToStrODBC(this.w_ARDATINT)+;
             ",ARCATOPE="+cp_ToStrODBC(this.w_ARCATOPE)+;
             ",ARTIPOPE="+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
             ",ARCCRIFE="+cp_ToStrODBCNull(this.w_ARCCRIFE)+;
             ",ARCCCHAR="+cp_ToStrODBC(this.w_ARCCCHAR)+;
             ",ARCCLUNG="+cp_ToStrODBC(this.w_ARCCLUNG)+;
             ",ARCCPROG="+cp_ToStrODBC(this.w_ARCCPROG)+;
             ",GESTGUID="+cp_ToStrODBC(this.w_GESTGUID)+;
             ",ARCATCES="+cp_ToStrODBC(this.w_ARCATCES)+;
             ",ARCODDIS="+cp_ToStrODBCNull(this.w_ARCODDIS)+;
             ",ARFLCOMP="+cp_ToStrODBC(this.w_ARFLCOMP)+;
             ",ARKITIMB="+cp_ToStrODBC(this.w_ARKITIMB)+;
             ",ARSALCOM="+cp_ToStrODBC(this.w_ARSALCOM)+;
             ",ARTIPPKR="+cp_ToStrODBC(this.w_ARTIPPKR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ART_ICOL')
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        UPDATE (i_cTable) SET;
              ARDESART=this.w_ARDESART;
             ,ARDESSUP=this.w_ARDESSUP;
             ,ARCONCAR=this.w_ARCONCAR;
             ,ARCMPCAR=this.w_ARCMPCAR;
             ,ARTIPART=this.w_ARTIPART;
             ,ARUNMIS1=this.w_ARUNMIS1;
             ,ARUNMIS2=this.w_ARUNMIS2;
             ,AROPERAT=this.w_AROPERAT;
             ,ARMOLTIP=this.w_ARMOLTIP;
             ,ARDTOBSO=this.w_ARDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ARDTINVA=this.w_ARDTINVA;
             ,ARTIPBAR=this.w_ARTIPBAR;
             ,ARPROPRE=this.w_ARPROPRE;
             ,ARTIPGES=this.w_ARTIPGES;
             ,ARCODCEN=this.w_ARCODCEN;
             ,ARPREZUM=this.w_ARPREZUM;
             ,ARDATINT=this.w_ARDATINT;
             ,ARCATOPE=this.w_ARCATOPE;
             ,ARTIPOPE=this.w_ARTIPOPE;
             ,ARCCRIFE=this.w_ARCCRIFE;
             ,ARCCCHAR=this.w_ARCCCHAR;
             ,ARCCLUNG=this.w_ARCCLUNG;
             ,ARCCPROG=this.w_ARCCPROG;
             ,GESTGUID=this.w_GESTGUID;
             ,ARCATCES=this.w_ARCATCES;
             ,ARCODDIS=this.w_ARCODDIS;
             ,ARFLCOMP=this.w_ARFLCOMP;
             ,ARKITIMB=this.w_ARKITIMB;
             ,ARSALCOM=this.w_ARSALCOM;
             ,ARTIPPKR=this.w_ARTIPPKR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MAC : Saving
      this.GSMA_MAC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"CCCODART";
             )
      this.GSMA_MAC.mReplace()
      * --- GSMA_MLI : Saving
      this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"LICODART";
             )
      this.GSMA_MLI.mReplace()
      * --- GSMA_MDA : Saving
      this.GSMA_MDA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"DICODART";
             )
      this.GSMA_MDA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMA_MAC : Deleting
    this.GSMA_MAC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"CCCODART";
           )
    this.GSMA_MAC.mDelete()
    * --- GSMA_MLI : Deleting
    this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"LICODART";
           )
    this.GSMA_MLI.mDelete()
    * --- GSMA_MDA : Deleting
    this.GSMA_MDA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"DICODART";
           )
    this.GSMA_MDA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ART_ICOL_IDX,i_nConn)
      *
      * delete ART_ICOL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
            .w_TIPOPE = this.cFunction
        .DoRTCalc(5,7,.t.)
        if .o_ARCODART<>.w_ARCODART.or. .o_ARTIPART<>.w_ARTIPART
            .w_ARCONCAR =  iif(.w_ARTIPART='CC','S', 'N')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_ARCCRIFE<>.w_ARCCRIFE
            .w_ARUNMIS1 = iif(!EMPTY(nvl(.w_ARCCRIFE,'')),.w_UNMIS,.w_ARUNMIS1)
          .link_1_12('Full')
        endif
        if .o_ARTIPART<>.w_ARTIPART.or. .o_ARCCRIFE<>.w_ARCCRIFE
            .w_ARUNMIS2 = iif(!EMPTY(.w_ARCCRIFE),.w_UNMIS2,.w_ARUNMIS2)
          .link_1_13('Full')
        endif
        if .o_ARUNMIS2<>.w_ARUNMIS2
            .w_AROPERAT = iif(!EMPTY(.w_ARCCRIFE),.w_OPERAT,.w_AROPERAT)
        endif
        if .o_ARUNMIS2<>.w_ARUNMIS2
            .w_ARMOLTIP = iif(!EMPTY(.w_ARCCRIFE),.w_MOLTIP,.w_ARMOLTIP)
        endif
        .DoRTCalc(15,19,.t.)
            .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .DoRTCalc(21,25,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OMODKEY = .w_ARDESART+.w_ARDESSUP
        endif
            .w_NMODKEY = .w_ARDESART+.w_ARDESSUP
        .DoRTCalc(28,28,.t.)
            .w_ARPROPRE = "I"
            .w_ARTIPGES = "F"
        if .o_ARCODART<>.w_ARCODART
            .w_ODATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        endif
            .w_NDATKEY = DTOC(.w_ARDTOBSO)+DTOC(.w_ARDTINVA)
        if .o_ARCCRIFE<>.w_ARCCRIFE
            .w_ARCODCEN = iif(!EMPTY(.w_ARCCRIFE),.w_CENCOS,.w_ARCODCEN)
          .link_1_45('Full')
        endif
        .DoRTCalc(34,37,.t.)
          .link_1_51('Full')
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        if .o_ARCCRIFE<>.w_ARCCRIFE
          .Calculate_GVRAOGBLHL()
        endif
        .DoRTCalc(39,53,.t.)
        if .o_ARKITIMB<>.w_ARKITIMB
            .w_ARCODDIS = IIF(.w_ARKITIMB='N',.w_ARCODDIS, Space(20))
          .link_1_77('Full')
        endif
        if .o_ARCODDIS<>.w_ARCODDIS
            .w_ARFLCOMP = ' '
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(56,62,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
  return

  proc Calculate_GVRAOGBLHL()
    with this
          * --- Aggiorno la descrizione del CC
          GSCR_BAV(this;
              ,'V';
             )
    endwith
  endproc
  proc Calculate_ENFWKPGANH()
    with this
          * --- Controllo lunghezza codice
          GSCR_BAV(this;
              ,'L';
             )
    endwith
  endproc
  proc Calculate_YLGIVIHJXJ()
    with this
          * --- Controllo la tipologia articolo di riferimento
          GSCR_BAV(this;
              ,'R';
             )
    endwith
  endproc
  proc Calculate_YBHCACTYYL()
    with this
          * --- Tipo articolo (PK I.revolution)
          .w_ARTIPPKR = evl(.w_ARTIPPKR, .w_ARTIPART)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.enabled = this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.mCond()
    this.oPgFrm.Page1.oPag.oARUNMIS1_1_12.enabled = this.oPgFrm.Page1.oPag.oARUNMIS1_1_12.mCond()
    this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.enabled = this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.mCond()
    this.oPgFrm.Page1.oPag.oAROPERAT_1_14.enabled = this.oPgFrm.Page1.oPag.oAROPERAT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oARMOLTIP_1_15.enabled = this.oPgFrm.Page1.oPag.oARMOLTIP_1_15.mCond()
    this.oPgFrm.Page1.oPag.oARCODCEN_1_45.enabled = this.oPgFrm.Page1.oPag.oARCODCEN_1_45.mCond()
    this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.enabled = this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.mCond()
    this.oPgFrm.Page1.oPag.oARCODDIS_1_77.enabled = this.oPgFrm.Page1.oPag.oARCODDIS_1_77.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_10.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_86.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_86.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(g_PRFA<>"S")
    local i_show1
    i_show1=not(g_PRFA<>"S")
    this.oPgFrm.Pages(2).enabled=i_show1 and not(g_PRFA<>"S")
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Produzione funzioni avanzate"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.visible=!this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_10.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.visible=!this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_56.visible=!this.oPgFrm.Page1.oPag.oDESART_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oARCCCHAR_1_58.visible=!this.oPgFrm.Page1.oPag.oARCCCHAR_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oARCCLUNG_1_60.visible=!this.oPgFrm.Page1.oPag.oARCCLUNG_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oARCCPROG_1_63.visible=!this.oPgFrm.Page1.oPag.oARCCPROG_1_63.mHide()
    this.oPgFrm.Page1.oPag.oARCODDIS_1_77.visible=!this.oPgFrm.Page1.oPag.oARCODDIS_1_77.mHide()
    this.oPgFrm.Page1.oPag.oDESDIS_1_79.visible=!this.oPgFrm.Page1.oPag.oDESDIS_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
        if lower(cEvent)==lower("w_ARTIPART Changed")
          .Calculate_ENFWKPGANH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ARCCRIFE Changed")
          .Calculate_YLGIVIHJXJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YBHCACTYYL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscr_aar
    If cEvent = "Record Updated" OR cEvent = "Record Inserted"
          if g_SINC="S"
            * --- Se il modulo della sincronizzazione � attivo e l'articolo modificato non � presente sulle aziende client
            *     succete che 
            *     - l'articolo viene creato normalmente sulle aziende client
            *     - la routine di crezione dei codici di ricercanon parte perch� parte solo in inserimento
            *     - ci si ritova sulle aziende client con l'articolo, ma senza il codice di ricerca standard
            *     In questo caso occorre forzare la creazione dl codice di ricerca
            L_ELENCOCODICIDIRICERCA=SYS(2015)
            vq_exec("..\SINC\EXE\GSSI_SRC.VQR",this,L_ELENCOCODICIDIRICERCA)
            L_MASK = GSMA_ACA()
            L_MASK.VISIBLE = .F.
            SELECT ( L_ELENCOCODICIDIRICERCA )
            GO TOP
            SCAN
              L_WHERE = "CACODICE =" + cp_ToStrODBC(CACODICE)
              * --- Costruisco la frase di WHERE
              L_MASK.QueryKeySet(L_WHERE)
              L_MASK.LoadRecWarn()
              L_MASK.ecpEdit()
              L_MASK.bUpdated = .T.
              if TYPE("L_MASK.bHeaderUpdated")<>"U"
                L_MASK.bHeaderUpdated = .T.
              endif
              L_MASK.ecpSave()
              SELECT ( L_ELENCOCODICIDIRICERCA )
            ENDSCAN
            USE
            L_MASK.ecpQuit()
          endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPARPROD
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPARPROD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPARPROD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDPROLEN";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_READPARPROD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_READPARPROD)
            select PDCODAZI,PDPROLEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPARPROD = NVL(_Link_.PDCODAZI,space(2))
      this.w_PROLEN = NVL(_Link_.PDPROLEN,0)
    else
      if i_cCtrl<>'Load'
        this.w_READPARPROD = space(2)
      endif
      this.w_PROLEN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPARPROD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUNMIS1
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS1))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS1)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS1) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS1_1_12'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS1)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS1 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS1 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUNMIS2
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS2))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS2_1_13'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS2)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS2 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS2 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUNMIS2<>.w_ARUNMIS1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ARUNMIS2 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODCEN
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ARCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ARCODCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_ARCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_ARCODCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oARCODCEN_1_45'),i_cWhere,'GSCA_ACC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ARCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ARCODCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODCEN = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.CC_CONTO as CC_CONTO145"+ ",link_1_45.CCDESPIA as CCDESPIA145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on ART_ICOL.ARCODCEN=link_1_45.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODCEN=link_1_45.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTIPOPE
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,ARTIPOPE";
                   +" from "+i_cTable+" "+i_lTable+" where ARTIPOPE="+cp_ToStrODBC(this.w_ARTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ARCATOPE;
                       ,'ARTIPOPE',this.w_ARTIPOPE)
            select TI__TIPO,ARTIPOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPOPE = NVL(_Link_.ARTIPOPE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPOPE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.ARTIPOPE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCCRIFE
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCCRIFE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARCCRIFE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARCCRIFE))
          select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCCRIFE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCCRIFE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARCCRIFE_1_55'),i_cWhere,'GSMA_AAR',"ARTICOLI",'GSCR1AAV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCCRIFE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARCCRIFE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARCCRIFE)
            select ARCODART,ARDESART,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARCODCEN,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCCRIFE = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_UNMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,0)
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,space(1))
      this.w_CENCOS = NVL(_Link_.ARCODCEN,space(15))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ARCCRIFE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNMIS = space(3)
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = 0
      this.w_MOLTIP = space(1)
      this.w_CENCOS = space(15)
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCCRIFE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_55(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_55.ARCODART as ARCODART155"+ ",link_1_55.ARDESART as ARDESART155"+ ",link_1_55.ARUNMIS1 as ARUNMIS1155"+ ",link_1_55.ARUNMIS2 as ARUNMIS2155"+ ",link_1_55.AROPERAT as AROPERAT155"+ ",link_1_55.ARMOLTIP as ARMOLTIP155"+ ",link_1_55.ARCODCEN as ARCODCEN155"+ ",link_1_55.ARTIPART as ARTIPART155"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_55 on ART_ICOL.ARCCRIFE=link_1_55.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_55"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCCRIFE=link_1_55.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODDIS
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BAK',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_ARCODDIS)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_ARCODDIS))
          select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODDIS)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODDIS) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oARCODDIS_1_77'),i_cWhere,'GSAR_BAK',"Elenco distinte base - kit",'GSDS_AAR.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_ARCODDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_ARCODDIS)
            select DBCODICE,DBDESCRI,DBDTOBSO,DBDTINVA,DBDISKIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODDIS = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
      this.w_DBOBSO = NVL(cp_ToDate(_Link_.DBDTOBSO),ctod("  /  /  "))
      this.w_DBINVA = NVL(cp_ToDate(_Link_.DBDTINVA),ctod("  /  /  "))
      this.w_DISKIT = NVL(_Link_.DBDISKIT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODDIS = space(20)
      endif
      this.w_DESDIS = space(40)
      this.w_DBOBSO = ctod("  /  /  ")
      this.w_DBINVA = ctod("  /  /  ")
      this.w_DISKIT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DBOBSO) OR .w_DBOBSO>=i_DATSYS) AND (EMPTY(.w_DBINVA) OR .w_DBINVA<=i_DATSYS)) and ((g_VEFA = 'S' And .w_DISKIT = 'I') Or (g_DISB = 'S' And .w_DISKIT = 'D') Or ((g_VEFA = 'S' Or g_GPOS = 'S') And .w_DISKIT = 'K')) And .w_ARKITIMB='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati")
        endif
        this.w_ARCODDIS = space(20)
        this.w_DESDIS = space(40)
        this.w_DBOBSO = ctod("  /  /  ")
        this.w_DBINVA = ctod("  /  /  ")
        this.w_DISKIT = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_77(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DISMBASE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_77.DBCODICE as DBCODICE177"+ ",link_1_77.DBDESCRI as DBDESCRI177"+ ",link_1_77.DBDTOBSO as DBDTOBSO177"+ ",link_1_77.DBDTINVA as DBDTINVA177"+ ",link_1_77.DBDISKIT as DBDISKIT177"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_77 on ART_ICOL.ARCODDIS=link_1_77.DBCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_77"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODDIS=link_1_77.DBCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODART_1_5.value==this.w_ARCODART)
      this.oPgFrm.Page1.oPag.oARCODART_1_5.value=this.w_ARCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_6.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_6.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESSUP_1_7.value==this.w_ARDESSUP)
      this.oPgFrm.Page1.oPag.oARDESSUP_1_7.value=this.w_ARDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.RadioValue()==this.w_ARCMPCAR)
      this.oPgFrm.Page1.oPag.oARCMPCAR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPART_1_11.RadioValue()==this.w_ARTIPART)
      this.oPgFrm.Page1.oPag.oARTIPART_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_12.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_12.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.value==this.w_ARUNMIS2)
      this.oPgFrm.Page1.oPag.oARUNMIS2_1_13.value=this.w_ARUNMIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oAROPERAT_1_14.RadioValue()==this.w_AROPERAT)
      this.oPgFrm.Page1.oPag.oAROPERAT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARMOLTIP_1_15.value==this.w_ARMOLTIP)
      this.oPgFrm.Page1.oPag.oARMOLTIP_1_15.value=this.w_ARMOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODCEN_1_45.value==this.w_ARCODCEN)
      this.oPgFrm.Page1.oPag.oARCODCEN_1_45.value=this.w_ARCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_46.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.value==this.w_ARCCRIFE)
      this.oPgFrm.Page1.oPag.oARCCRIFE_1_55.value=this.w_ARCCRIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_56.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_56.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oARCCCHAR_1_58.value==this.w_ARCCCHAR)
      this.oPgFrm.Page1.oPag.oARCCCHAR_1_58.value=this.w_ARCCCHAR
    endif
    if not(this.oPgFrm.Page1.oPag.oARCCLUNG_1_60.value==this.w_ARCCLUNG)
      this.oPgFrm.Page1.oPag.oARCCLUNG_1_60.value=this.w_ARCCLUNG
    endif
    if not(this.oPgFrm.Page1.oPag.oARCCPROG_1_63.value==this.w_ARCCPROG)
      this.oPgFrm.Page1.oPag.oARCCPROG_1_63.value=this.w_ARCCPROG
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODDIS_1_77.value==this.w_ARCODDIS)
      this.oPgFrm.Page1.oPag.oARCODDIS_1_77.value=this.w_ARCODDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIS_1_79.value==this.w_DESDIS)
      this.oPgFrm.Page1.oPag.oDESDIS_1_79.value=this.w_DESDIS
    endif
    cp_SetControlsValueExtFlds(this,'ART_ICOL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ARCODART)) or not((.w_ARTIPART='CC' and .w_ARCCLUNG<=18-LEN(ALLTRIM(.w_ARCODART))) or .w_ARTIPART<>'CC'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODART_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ARCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice non congruente")
          case   (empty(.w_ARTIPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTIPART_1_11.SetFocus()
            i_bnoObbl = !empty(.w_ARTIPART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARUNMIS1))  and (!.w_ARTIPART$'DE-CC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS1_1_12.SetFocus()
            i_bnoObbl = !empty(.w_ARUNMIS1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARUNMIS2<>.w_ARUNMIS1)  and (!.w_ARTIPART$'DE-CC')  and not(empty(.w_ARUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARMOLTIP))  and (NOT EMPTY(.w_ARUNMIS2) and .w_ARTIPART<>'CC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARMOLTIP_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ARMOLTIP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARCCRIFE))  and not(.w_ARTIPART<>'CC')  and (.w_ARTIPART='CC' and UPPER(.w_TIPOPE)='LOAD')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCCRIFE_1_55.SetFocus()
            i_bnoObbl = !empty(.w_ARCCRIFE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre specificare un articolo di riferimento")
          case   (empty(.w_ARCCCHAR))  and not(.w_ARTIPART<>'CC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCCCHAR_1_58.SetFocus()
            i_bnoObbl = !empty(.w_ARCCCHAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre specificare un carattere di seprazione")
          case   ((empty(.w_ARCCLUNG)) or not((.w_ARTIPART='CC' and .w_ARCCLUNG<=19-LEN(ALLTRIM(.w_ARCODART))) or .w_ARTIPART<>'CC'))  and not(.w_ARTIPART<>'CC')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCCLUNG_1_60.SetFocus()
            i_bnoObbl = !empty(.w_ARCCLUNG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_DBOBSO) OR .w_DBOBSO>=i_DATSYS) AND (EMPTY(.w_DBINVA) OR .w_DBINVA<=i_DATSYS)) and ((g_VEFA = 'S' And .w_DISKIT = 'I') Or (g_DISB = 'S' And .w_DISKIT = 'D') Or ((g_VEFA = 'S' Or g_GPOS = 'S') And .w_DISKIT = 'K')) And .w_ARKITIMB='N')  and not(g_EACD<>'S' And g_GPOS <> 'S')  and ((g_EACD = 'S' Or g_GPOS = 'S') And .w_ARKITIMB='N')  and not(empty(.w_ARCODDIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODDIS_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MAC.CheckForm()
      if i_bres
        i_bres=  .GSMA_MAC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MLI.CheckForm()
      if i_bres
        i_bres=  .GSMA_MLI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_MDA.CheckForm()
      if i_bres
        i_bres=  .GSMA_MDA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ARCODART = this.w_ARCODART
    this.o_ARTIPART = this.w_ARTIPART
    this.o_ARUNMIS2 = this.w_ARUNMIS2
    this.o_ARCCRIFE = this.w_ARCCRIFE
    this.o_ARCCLUNG = this.w_ARCCLUNG
    this.o_ARCODDIS = this.w_ARCODDIS
    this.o_ARKITIMB = this.w_ARKITIMB
    * --- GSMA_MAC : Depends On
    this.GSMA_MAC.SaveDependsOn()
    * --- GSMA_MLI : Depends On
    this.GSMA_MLI.SaveDependsOn()
    * --- GSMA_MDA : Depends On
    this.GSMA_MDA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscr_aarPag1 as StdContainer
  Width  = 726
  height = 297
  stdWidth  = 726
  stdheight = 297
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODART_1_5 as StdField with uid="XHAYKWUIDG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARCODART", cQueryName = "ARCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice non congruente",;
    ToolTipText = "Codice",;
    HelpContextID = 49671846,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=178, Left=55, Top=10, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20)

  func oARCODART_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ARTIPART='CC' and .w_ARCCLUNG<=18-LEN(ALLTRIM(.w_ARCODART))) or .w_ARTIPART<>'CC')
    endwith
    return bRes
  endfunc

  add object oARDESART_1_6 as StdField with uid="PJGDMQYEXD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 34594470,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=321, Top=10, InputMask=replicate('X',40)

  add object oARDESSUP_1_7 as StdMemo with uid="BEBAHJCKLS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ARDESSUP", cQueryName = "ARDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 267395414,;
   bGlobalFont=.t.,;
    Height=69, Width=363, Left=321, Top=39

  add object oARCMPCAR_1_9 as StdCheck with uid="NVDIKNEPQE",rtseq=9,rtrep=.f.,left=55, top=67, caption="Componenti caratteristiche",;
    ToolTipText = "Se attivo, il componente della distinta base puo essere gestito a caratteristiche",;
    HelpContextID = 3665576,;
    cFormVar="w_ARCMPCAR", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oARCMPCAR_1_9.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oARCMPCAR_1_9.GetRadio()
    this.Parent.oContained.w_ARCMPCAR = this.RadioValue()
    return .t.
  endfunc

  func oARCMPCAR_1_9.SetRadio()
    this.Parent.oContained.w_ARCMPCAR=trim(this.Parent.oContained.w_ARCMPCAR)
    this.value = ;
      iif(this.Parent.oContained.w_ARCMPCAR=="S",1,;
      0)
  endfunc

  func oARCMPCAR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_ARCODART) and .w_ARTIPART='CC')
    endwith
   endif
  endfunc

  func oARCMPCAR_1_9.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc


  add object oLinkPC_1_10 as StdButton with uid="MHWXCUZNDK",left=266, top=67, width=48,height=45,;
    CpPicture="BMP\CARATTER.BMP", caption="", nPag=1;
    , ToolTipText = "Caratteristiche di default";
    , HelpContextID = 1517386;
    , caption='\<Caratter.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_10.Click()
      this.Parent.oContained.GSMA_MAC.LinkPCClick()
    endproc

  func oLinkPC_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ARCODART) and .w_ARTIPART='CC')
      endwith
    endif
  endfunc

  func oLinkPC_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
     endwith
    endif
  endfunc


  add object oARTIPART_1_11 as StdCombo with uid="NYKCWQBRAT",rtseq=10,rtrep=.f.,left=55,top=39,width=230,height=21, enabled=.f.;
    , ToolTipText = "Tipo item";
    , HelpContextID = 37412518;
    , cFormVar="w_ARTIPART",RowSource=""+"Da configurare,"+"Articolo configuratore caratteristiche", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPART_1_11.RadioValue()
    return(iif(this.value =1,'DC',;
    iif(this.value =2,'CC',;
    space(2))))
  endfunc
  func oARTIPART_1_11.GetRadio()
    this.Parent.oContained.w_ARTIPART = this.RadioValue()
    return .t.
  endfunc

  func oARTIPART_1_11.SetRadio()
    this.Parent.oContained.w_ARTIPART=trim(this.Parent.oContained.w_ARTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPART=='DC',1,;
      iif(this.Parent.oContained.w_ARTIPART=='CC',2,;
      0))
  endfunc

  add object oARUNMIS1_1_12 as StdField with uid="BTAFDWZYVJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 93991223,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=130, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS1"

  func oARUNMIS1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ARTIPART$'DE-CC')
    endwith
   endif
  endfunc

  func oARUNMIS1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS1_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS1_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS1_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUNMIS1_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS1
     i_obj.ecpSave()
  endproc

  add object oARUNMIS2_1_13 as StdField with uid="OZAEOAQKCE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ARUNMIS2", cQueryName = "ARUNMIS2",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale seconda unit� di misura",;
    HelpContextID = 93991224,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=266, Top=130, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS2"

  func oARUNMIS2_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ARTIPART$'DE-CC')
    endwith
   endif
  endfunc

  func oARUNMIS2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS2_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS2_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS2_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUNMIS2_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS2
     i_obj.ecpSave()
  endproc


  add object oAROPERAT_1_14 as StdCombo with uid="RLJZJAXXKW",rtseq=13,rtrep=.f.,left=327,top=130,width=35,height=21;
    , ToolTipText = "Operatore (moltip. o divis.) tra la 1^UM e la 2^UM";
    , HelpContextID = 31731366;
    , cFormVar="w_AROPERAT",RowSource=""+"X,"+"/", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAROPERAT_1_14.RadioValue()
    return(iif(this.value =1,'*',;
    iif(this.value =2,'/',;
    space(1))))
  endfunc
  func oAROPERAT_1_14.GetRadio()
    this.Parent.oContained.w_AROPERAT = this.RadioValue()
    return .t.
  endfunc

  func oAROPERAT_1_14.SetRadio()
    this.Parent.oContained.w_AROPERAT=trim(this.Parent.oContained.w_AROPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_AROPERAT=='*',1,;
      iif(this.Parent.oContained.w_AROPERAT=='/',2,;
      0))
  endfunc

  func oAROPERAT_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2) and .w_ARTIPART<>'CC')
    endwith
   endif
  endfunc

  add object oARMOLTIP_1_15 as StdField with uid="AUNETTJCMN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ARMOLTIP", cQueryName = "ARMOLTIP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di rapporto tra la 1^UM e la 2^UM",;
    HelpContextID = 9089366,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=375, Top=130, cSayPict='"99999.9999"', cGetPict='"99999.9999"'

  func oARMOLTIP_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2) and .w_ARTIPART<>'CC')
    endwith
   endif
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="AILOFBVURS",left=6, top=310, width=158,height=19,;
    caption='GSMA_BDV',;
   bGlobalFont=.t.,;
    prg="GSMA_BDV",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 262976188


  add object oObj_1_36 as cp_runprogram with uid="CJDGJIVITM",left=166, top=310, width=158,height=19,;
    caption='GSMA_BCV',;
   bGlobalFont=.t.,;
    prg='GSMA_BCV("I")',;
    cEvent = "Insert end",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 5459268


  add object oObj_1_37 as cp_runprogram with uid="SNLYLEWHFX",left=326, top=310, width=158,height=19,;
    caption='GSMA_BCV',;
   bGlobalFont=.t.,;
    prg='GSMA_BCV("U")',;
    cEvent = "Update start",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 5459268

  add object oARCODCEN_1_45 as StdField with uid="EOXGWJQNRX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ARCODCEN", cQueryName = "ARCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 252318036,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=135, Top=158, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ARCODCEN"

  func oARCODCEN_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCM='S' and .w_ARTIPART<>'CC')
    endwith
   endif
  endfunc

  func oARCODCEN_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODCEN_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODCEN_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oARCODCEN_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"",'',this.parent.oContained
  endproc
  proc oARCODCEN_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ARCODCEN
     i_obj.ecpSave()
  endproc

  add object oCCDESPIA_1_46 as StdField with uid="XGMNPYAPMH",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 217059943,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=276, Top=158, InputMask=replicate('X',40)


  add object oObj_1_52 as cp_runprogram with uid="HHTMFWICXQ",left=486, top=310, width=158,height=19,;
    caption='GSCR_BAV',;
   bGlobalFont=.t.,;
    prg='GSCR_BAV("I")',;
    cEvent = "Insert end",;
    nPag=1;
    , ToolTipText = "Inserisce ART_PROD alla conferma del  Caricamento";
    , HelpContextID = 4386116

  add object oARCCRIFE_1_55 as StdField with uid="HJHUKOAIWY",rtseq=39,rtrep=.f.,;
    cFormVar = "w_ARCCRIFE", cQueryName = "ARCCRIFE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Occorre specificare un articolo di riferimento",;
    HelpContextID = 98439499,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=158, Top=217, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_ARCCRIFE"

  func oARCCRIFE_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART='CC' and UPPER(.w_TIPOPE)='LOAD')
    endwith
   endif
  endfunc

  func oARCCRIFE_1_55.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  func oARCCRIFE_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCCRIFE_1_55.ecpDrop(oSource)
    this.Parent.oContained.link_1_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCCRIFE_1_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARCCRIFE_1_55'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"ARTICOLI",'GSCR1AAV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARCCRIFE_1_55.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARCCRIFE
     i_obj.ecpSave()
  endproc

  add object oDESART_1_56 as StdField with uid="GTBIBSVBTI",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 253950922,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=326, Top=217, InputMask=replicate('X',40)

  func oDESART_1_56.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oARCCCHAR_1_58 as StdField with uid="MDOAAANKBF",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ARCCCHAR", cQueryName = "ARCCCHAR",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Occorre specificare un carattere di seprazione",;
    HelpContextID = 202501800,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=158, Top=271, InputMask=replicate('X',1)

  func oARCCCHAR_1_58.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oARCCLUNG_1_60 as StdField with uid="SMVRTMMLVB",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ARCCLUNG", cQueryName = "ARCCLUNG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 243396275,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=326, Top=271

  func oARCCLUNG_1_60.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  func oARCCLUNG_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ARTIPART='CC' and .w_ARCCLUNG<=19-LEN(ALLTRIM(.w_ARCODART))) or .w_ARTIPART<>'CC')
    endwith
    return bRes
  endfunc

  add object oARCCPROG_1_63 as StdField with uid="BMLTYMHJBT",rtseq=43,rtrep=.f.,;
    cFormVar = "w_ARCCPROG", cQueryName = "ARCCPROG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 21098163,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=480, Top=271, InputMask=replicate('X',18)

  func oARCCPROG_1_63.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oARCODDIS_1_77 as StdField with uid="CJZVXWRMXW",rtseq=54,rtrep=.f.,;
    cFormVar = "w_ARCODDIS", cQueryName = "ARCODDIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base/kit inesistente, obsoleta, non valida o incongruente con moduli installati",;
    ToolTipText = "Codice della distinta base associata all'articolo",;
    HelpContextID = 659801,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=158, Top=244, InputMask=replicate('X',20), bHasZoom = .t. , cMenuFile="GSMA_AAR", cLinkFile="DISMBASE", cZoomOnZoom="GSAR_BAK", oKey_1_1="DBCODICE", oKey_1_2="this.w_ARCODDIS"

  func oARCODDIS_1_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_EACD = 'S' Or g_GPOS = 'S') And .w_ARKITIMB='N')
    endwith
   endif
  endfunc

  func oARCODDIS_1_77.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  func oARCODDIS_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODDIS_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODDIS_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oARCODDIS_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BAK',"Elenco distinte base - kit",'GSDS_AAR.DISMBASE_VZM',this.parent.oContained
  endproc
  proc oARCODDIS_1_77.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BAK()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DBCODICE=this.parent.oContained.w_ARCODDIS
     i_obj.ecpSave()
  endproc

  add object oDESDIS_1_79 as StdField with uid="MXLUPQUZTF",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11533258,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=326, Top=244, InputMask=replicate('X',40)

  func oDESDIS_1_79.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc


  add object oLinkPC_1_86 as StdButton with uid="ZTNCHVNEGR",left=636, top=133, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione dei listini";
    , HelpContextID = 58578762;
    , tabstop=.f., Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_86.Click()
      this.Parent.oContained.GSMA_MLI.LinkPCClick()
    endproc

  func oLinkPC_1_86.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  add object oStr_1_17 as StdString with uid="TLCMMEMTCE",Visible=.t., Left=9, Top=10,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="PTZXTNHGPW",Visible=.t., Left=237, Top=10,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WLZJGWBAQA",Visible=.t., Left=17, Top=39,;
    Alignment=1, Width=34, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XUKBNMPJOB",Visible=.t., Left=11, Top=130,;
    Alignment=1, Width=119, Height=15,;
    Caption="1^Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="TPXVKEXCIR",Visible=.t., Left=375, Top=112,;
    Alignment=0, Width=58, Height=15,;
    Caption="Parametro"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="OOORWSJXAI",Visible=.t., Left=327, Top=112,;
    Alignment=0, Width=30, Height=15,;
    Caption="Oper."  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="LQSKCURHXX",Visible=.t., Left=215, Top=130,;
    Alignment=1, Width=44, Height=15,;
    Caption="2^U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="FJJWUTPFXI",Visible=.t., Left=197, Top=130,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="OSIUPKWTLR",Visible=.t., Left=9, Top=158,;
    Alignment=1, Width=120, Height=15,;
    Caption="Centro costo/ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="QUFSJUYVRH",Visible=.t., Left=9, Top=193,;
    Alignment=0, Width=137, Height=18,;
    Caption="Parametri configuratore"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="DCOWESSEJW",Visible=.t., Left=7, Top=218,;
    Alignment=1, Width=147, Height=18,;
    Caption="Articolo di riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="OLFXMSMSWD",Visible=.t., Left=4, Top=272,;
    Alignment=1, Width=150, Height=18,;
    Caption="Carattere separatore:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="UNQTFZSXVV",Visible=.t., Left=181, Top=272,;
    Alignment=1, Width=144, Height=18,;
    Caption="Lunghezza progressivo:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="NOUVLBKVAI",Visible=.t., Left=376, Top=272,;
    Alignment=1, Width=98, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'CC')
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="NLVQYDEEUR",Visible=.t., Left=49, Top=247,;
    Alignment=1, Width=105, Height=18,;
    Caption="Distinta base\kit:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (g_EACD<>'S' And g_GPOS <> 'S')
    endwith
  endfunc

  add object oBox_1_54 as StdBox with uid="JEJGIPQDDK",left=7, top=210, width=622,height=1
enddefine
define class tgscr_aarPag2 as StdContainer
  Width  = 726
  height = 297
  stdWidth  = 726
  stdheight = 297
  resizeXpos=357
  resizeYpos=164
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="WIVRENOEAA",left=1, top=8, width=713, height=288, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mda",lower(this.oContained.GSMA_MDA.class))=0
        this.oContained.GSMA_MDA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscr_aar','ART_ICOL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ARCODART=ART_ICOL.ARCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
