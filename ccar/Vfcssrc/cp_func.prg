* --- Container for functions
* --- START CALCCPROD
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: calccprod                                                       *
*              CALCOLA PROGRESSIVO                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-05                                                      *
* Last revis.: 2014-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func calccprod
param pCODICE,pSTRLEN

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CODICE
  m.w_CODICE=space(20)
  private w_LUNG
  m.w_LUNG=0
  private w_IDX
  m.w_IDX=0
  private w_SNUM
  m.w_SNUM=space(20)
  private w_SCHAR
  m.w_SCHAR=space(20)
* --- WorkFile variables
  private ART_ICOL_idx
  ART_ICOL_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "calccprod"
if vartype(__calccprod_hook__)='O'
  __calccprod_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'calccprod('+Transform(pCODICE)+','+Transform(pSTRLEN)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calccprod')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if calccprod_OpenTables()
  calccprod_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'calccprod('+Transform(pCODICE)+','+Transform(pSTRLEN)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'calccprod')
Endif
*--- Activity log
if vartype(__calccprod_hook__)='O'
  __calccprod_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure calccprod_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Utilizzata per generazione codice articolo
  m.w_CODICE = ALLTRIM(NVL(m.pCODICE,""))
  m.w_LUNG = LEN(m.w_CODICE)
  if m.w_LUNG=0
    m.w_CODICE = REPL("0",m.pSTRLEN)
  else
    m.w_IDX = 1
    do while ASC(SUBSTR(m.w_CODICE,m.w_IDX,1)) > 57
      m.w_IDX = m.w_IDX + 1
    enddo
    m.w_SNUM = SUBSTR(m.w_CODICE,m.w_IDX,m.w_LUNG-m.w_IDX+1)
    m.w_SCHAR = SUBSTR(m.w_CODICE,1,m.w_IDX-1)
    if m.w_SNUM<>REPL("9",m.w_LUNG-m.w_IDX+1)
      m.w_SNUM = RIGHT(REPLICATE("0",LEN(m.w_SNUM))+ALLTRIM(STR(INT(VAL(m.w_SNUM))+1)),LEN(m.w_SNUM))
    else
      m.w_SNUM = REPLICATE("0",LEN(m.w_SNUM))
      if !EMPTY(m.w_SCHAR) and ASC(RIGHT(m.w_SCHAR,1)) < 90
        m.w_SCHAR = STUFF(m.w_SCHAR,LEN(m.w_SCHAR),1,CHR(ASC(RIGHT(m.w_SCHAR,1))+1))
      else
        m.w_SNUM = STUFF(m.w_SNUM,1,1,"A")
      endif
    endif
    m.w_CODICE = m.w_SCHAR+m.w_SNUM
  endif
  i_retcode = 'stop'
  i_retval = m.w_CODICE
  return
endproc


  function calccprod_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='ART_ICOL'
    return(cp_OpenFuncTables(1))
* --- END CALCCPROD
* --- START CHKDISB
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkdisb                                                         *
*              VERIFICA ESISTENZA DISTINTA                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-04                                                      *
* Last revis.: 2015-05-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chkdisb
param pPARAM,pCONF,pCOMP,pVMEM,pCODDIS,pCODART

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_NUMREC
  m.w_NUMREC=0
  private w_COMP
  m.w_COMP=space(20)
  private w_VMEM
  m.w_VMEM=space(1)
  private w_DIBAFOUND
  m.w_DIBAFOUND=.f.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chkdisb"
if vartype(__chkdisb_hook__)='O'
  __chkdisb_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chkdisb('+Transform(pPARAM)+','+Transform(pCONF)+','+Transform(pCOMP)+','+Transform(pVMEM)+','+Transform(pCODDIS)+','+Transform(pCODART)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkdisb')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
chkdisb_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chkdisb('+Transform(pPARAM)+','+Transform(pCONF)+','+Transform(pCOMP)+','+Transform(pVMEM)+','+Transform(pCODDIS)+','+Transform(pCODART)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkdisb')
Endif
*--- Activity log
if vartype(__chkdisb_hook__)='O'
  __chkdisb_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chkdisb_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato il cursore pPARAM cerca una distinta che soddisfi i componenti (Configuratore caratteristiche GSDB_BC4)
  m.w_VMEM = NVL(m.pVMEM,"")
  if m.w_VMEM="S"
    m.w_CCCONFIG = NVL(m.pCONF,"")
  else
    m.w_CCCONFIG = ""
  endif
  m.w_CODDIS = ""
  m.w_NUMREC = RECCOUNT(m.pPARAM)
  m.w_COMP = alltrim(NVL(m.pCOMP,""))
  m.w_DIBAFOUND = .F.
  if m.w_NUMREC > 0
    CURTOTAB(m.pPARAM,"TMP_COMP")
    * --- Select from GSCR5BC4
    do vq_exec with 'GSCR5BC4',createobject('cp_getfuncvar'),'_Curs_GSCR5BC4','',.f.,.t.
    if used('_Curs_GSCR5BC4')
      select _Curs_GSCR5BC4
      locate for 1=1
      do while not(eof())
      if left(_Curs_GSCR5BC4.CORIFDIS,len(m.w_COMP)) == m.w_COMP
        m.w_DIBAFOUND = .T.
        m.pCODDIS = _Curs_GSCR5BC4.CORIFDIS
        m.pCODART = _Curs_GSCR5BC4.CODART
      endif
        select _Curs_GSCR5BC4
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMP_COMP
    i_nIdx=cp_GetTableDefIdx('TMP_COMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_COMP')
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_DIBAFOUND
  return
endproc


* --- END CHKDISB
