* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bav                                                        *
*              Inserisce i dati articolo prouzione su par_rior                 *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-03                                                      *
* Last revis.: 2014-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bav",oParentObject,m.pPARAM)
return(i_retval)

define class tgscr_bav as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_OBJCTRL = .NULL.
  * --- WorkFile variables
  CENCOST_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i dati articolo prouzione su PAR_RIOR (da GSCR_AAV)
    do case
      case this.pPARAM="V"
        * --- Lanciato al cambio di ARCCRIFE
        * --- Read from CENCOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESPIA"+;
            " from "+i_cTable+" CENCOST where ";
                +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_ARCODCEN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESPIA;
            from (i_cTable) where;
                CC_CONTO = this.oParentObject.w_ARCODCEN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CCDESPIA = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pPARAM="L"
        if this.oParentObject.w_ARTIPART = "CC"
          if this.oParentObject.w_ARCCLUNG>18-LEN(ALLTRIM(this.oParentObject.w_ARCODART))
            ah_errormsg("Lunghezza codice incongruente",16)
            * --- Imposto il focus sul codice articolo
            this.w_OBJCTRL = this.oParentObject.GetCtrl("w_ARCODART")
            if this.w_OBJCTRL.Enabled
              this.w_OBJCTRL.setFocus()     
            endif
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='PAR_RIOR'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
