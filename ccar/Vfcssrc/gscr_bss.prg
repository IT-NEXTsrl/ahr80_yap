* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bss                                                        *
*              Gestione bottoni seleziona/deseleziona                          *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-24                                                      *
* Last revis.: 2014-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bss",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_bss as StdBatch
  * --- Local variables
  w_OPER = space(10)
  w_PADRE = .NULL.
  w_OLDSEL = 0
  w_OLDSEL = 0
  w_OLDPOS = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GESTIONE BOTTONI SELEZIONA/DESELEZIONA
    this.w_PADRE = this.oParentObject
    this.w_OLDSEL = Select()
    Select (this.w_PADRE.cTrsName)
    this.w_OLDPOS = RecNo()
    do case
      case this.w_OPER = "SEL"
        Update (this.w_PADRE.cTrsName) Set t_CC__DEFA=1, t_DEFA=1
      case this.w_OPER = "DES"
        Update (this.w_PADRE.cTrsName) Set t_CC__DEFA=0, t_DEFA=0
      case this.w_OPER = "INV"
        Update (this.w_PADRE.cTrsName) Set t_CC__DEFA=1-t_CC__DEFA, t_DEFA=1-t_DEFA
    endcase
    Sum (t_DEFA) to this.oParentObject.w_TOTALE
    * --- Attiva la checkform
    this.w_PADRE.bUpdated = True
    this.w_PADRE.oParentObject.bUpdated = True
    if this.w_PADRE.cFunction="Edit"
      Update (this.w_PADRE.cTrsName) Set i_srv="U" where i_srv<>"A"
    endif
    if this.w_OLDPOS > RECCOUNT()
      GO TOP
    else
      GO this.w_OLDPOS
    endif
    this.w_PADRE.WorkFromTrs()     
    Select (this.w_OLDSEL)
  endproc


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
