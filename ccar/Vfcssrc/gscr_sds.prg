* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_sds                                                        *
*              Stampe distinta base                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-04-27                                                      *
* Last revis.: 2015-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_sds",oParentObject))

* --- Class definition
define class tgscr_sds as StdForm
  Top    = 1
  Left   = 6

  * --- Standard Properties
  Width  = 799
  Height = 537+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-27"
  HelpContextID=74020201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=59

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  PAR_PROD_IDX = 0
  GRUMERC_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  DISTBASE_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gscr_sds"
  cComment = "Stampe distinta base"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_CODARTIN = space(20)
  o_CODARTIN = space(20)
  w_DBCODINI = space(20)
  o_DBCODINI = space(20)
  w_CODARTFI = space(20)
  w_DBCODFIN = space(21)
  w_DATSTA = ctod('  /  /  ')
  w_TIPOARTI = space(1)
  w_DISPRO = space(1)
  w_FLNEU = space(1)
  o_FLNEU = space(1)
  w_TipoStam = space(1)
  o_TipoStam = space(1)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_GRUMER = space(5)
  w_GRUDES = space(35)
  w_GRUMERF = space(5)
  w_GRUDESF = space(35)
  w_CODFAMA = space(5)
  w_DESFAMAI = space(35)
  w_CODFAMAF = space(5)
  w_DESFAMAIF = space(35)
  w_CODCAT = space(5)
  w_DESCATI = space(35)
  w_CODCATF = space(5)
  w_DESCATIF = space(35)
  w_CODMAG = space(5)
  w_DESMAGI = space(30)
  w_CODMAGF = space(5)
  w_DESMAGIF = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CODCOMP = space(20)
  w_CODCOMPF = space(20)
  w_FLCHKLP = space(1)
  w_TIPO = space(1)
  w_CODICE = space(5)
  o_CODICE = space(5)
  w_SELECTION = space(1)
  w_STRCONF = space(0)
  w_ROWORD = 0
  w_FLDEFA = space(1)
  o_FLDEFA = space(1)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_TIPO2 = space(1)
  w_CODICE2 = space(5)
  o_CODICE2 = space(5)
  w_TIPO3 = space(1)
  w_CODICE3 = space(5)
  o_CODICE3 = space(5)
  w_FILTERTYPE = space(1)
  w_MXCHK = 0
  w_CCCONFIG = space(0)
  w_MXCHK2 = 0
  w_MXCHK3 = 0
  w_CCCONFIG3 = space(0)
  w_DESARTIN = space(40)
  w_TIPARTI = space(10)
  w_TIPARTF = space(10)
  w_CONCARIN = space(1)
  w_CONCARFI = space(1)
  w_DESARTFI = space(40)
  w_DESCOMIN = space(40)
  w_DESCOMFI = space(40)
  w_COSTILOG = space(1)
  w_CARATTER = space(1)
  w_GSCR1SDS = .NULL.
  w_GSCR2SDS = .NULL.
  w_GSCR3SDS = .NULL.
  w_GSCR4SDS = .NULL.
  w_GSCR5SDS = .NULL.
  w_GSCR6SDS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_sdsPag1","gscr_sds",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezione distinta")
      .Pages(2).addobject("oPag","tgscr_sdsPag2","gscr_sds",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Caratteristiche fisse")
      .Pages(3).addobject("oPag","tgscr_sdsPag3","gscr_sds",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Modelli parametrici")
      .Pages(4).addobject("oPag","tgscr_sdsPag4","gscr_sds",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Caratteristiche descrittive")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODARTIN_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_GSCR1SDS = this.oPgFrm.Pages(2).oPag.GSCR1SDS
    this.w_GSCR2SDS = this.oPgFrm.Pages(2).oPag.GSCR2SDS
    this.w_GSCR3SDS = this.oPgFrm.Pages(3).oPag.GSCR3SDS
    this.w_GSCR4SDS = this.oPgFrm.Pages(3).oPag.GSCR4SDS
    this.w_GSCR5SDS = this.oPgFrm.Pages(4).oPag.GSCR5SDS
    this.w_GSCR6SDS = this.oPgFrm.Pages(4).oPag.GSCR6SDS
    DoDefault()
    proc Destroy()
      this.w_GSCR1SDS = .NULL.
      this.w_GSCR2SDS = .NULL.
      this.w_GSCR3SDS = .NULL.
      this.w_GSCR4SDS = .NULL.
      this.w_GSCR5SDS = .NULL.
      this.w_GSCR6SDS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='PAR_PROD'
    this.cWorkTables[3]='GRUMERC'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='DISTBASE'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='ART_ICOL'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCR_BDS with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_CODARTIN=space(20)
      .w_DBCODINI=space(20)
      .w_CODARTFI=space(20)
      .w_DBCODFIN=space(21)
      .w_DATSTA=ctod("  /  /  ")
      .w_TIPOARTI=space(1)
      .w_DISPRO=space(1)
      .w_FLNEU=space(1)
      .w_TipoStam=space(1)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_GRUMER=space(5)
      .w_GRUDES=space(35)
      .w_GRUMERF=space(5)
      .w_GRUDESF=space(35)
      .w_CODFAMA=space(5)
      .w_DESFAMAI=space(35)
      .w_CODFAMAF=space(5)
      .w_DESFAMAIF=space(35)
      .w_CODCAT=space(5)
      .w_DESCATI=space(35)
      .w_CODCATF=space(5)
      .w_DESCATIF=space(35)
      .w_CODMAG=space(5)
      .w_DESMAGI=space(30)
      .w_CODMAGF=space(5)
      .w_DESMAGIF=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODCOMP=space(20)
      .w_CODCOMPF=space(20)
      .w_FLCHKLP=space(1)
      .w_TIPO=space(1)
      .w_CODICE=space(5)
      .w_SELECTION=space(1)
      .w_STRCONF=space(0)
      .w_ROWORD=0
      .w_FLDEFA=space(1)
      .w_SELEZI=space(1)
      .w_TIPO2=space(1)
      .w_CODICE2=space(5)
      .w_TIPO3=space(1)
      .w_CODICE3=space(5)
      .w_FILTERTYPE=space(1)
      .w_MXCHK=0
      .w_CCCONFIG=space(0)
      .w_MXCHK2=0
      .w_MXCHK3=0
      .w_CCCONFIG3=space(0)
      .w_DESARTIN=space(40)
      .w_TIPARTI=space(10)
      .w_TIPARTF=space(10)
      .w_CONCARIN=space(1)
      .w_CONCARFI=space(1)
      .w_DESARTFI=space(40)
      .w_DESCOMIN=space(40)
      .w_DESCOMFI=space(40)
      .w_COSTILOG=space(1)
      .w_CARATTER=space(1)
        .w_PARAM = 'T'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODARTIN))
          .link_1_4('Full')
        endif
        .w_DBCODINI = .w_DBCODFIN
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_DBCODINI))
          .link_1_5('Full')
        endif
        .w_CODARTFI = .w_CODARTIN
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODARTFI))
          .link_1_6('Full')
        endif
        .w_DBCODFIN = .w_DBCODINI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DBCODFIN))
          .link_1_7('Full')
        endif
        .w_DATSTA = i_datsys
        .w_TIPOARTI = 'CC'
        .w_DISPRO = "S"
        .w_FLNEU = 'S'
        .w_TipoStam = "M"
        .DoRTCalc(11,13,.f.)
        if not(empty(.w_GRUMER))
          .link_1_24('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_GRUMERF))
          .link_1_27('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_CODFAMA))
          .link_1_30('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_CODFAMAF))
          .link_1_33('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_CODCAT))
          .link_1_36('Full')
        endif
        .DoRTCalc(22,23,.f.)
        if not(empty(.w_CODCATF))
          .link_1_39('Full')
        endif
        .DoRTCalc(24,25,.f.)
        if not(empty(.w_CODMAG))
          .link_1_44('Full')
        endif
        .DoRTCalc(26,27,.f.)
        if not(empty(.w_CODMAGF))
          .link_1_47('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CODCOMP))
          .link_1_51('Full')
        endif
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CODCOMPF))
          .link_1_53('Full')
        endif
        .w_FLCHKLP = g_CHKDIBALOOP
      .oPgFrm.Page2.oPag.GSCR1SDS.Calculate()
      .oPgFrm.Page2.oPag.GSCR2SDS.Calculate()
        .w_TIPO = 'C'
        .w_CODICE = .w_GSCR1SDS.getVar('CCCODICE')
        .w_SELECTION = 'N'
          .DoRTCalc(36,36,.f.)
        .w_ROWORD = .w_GSCR2SDS.getVar('CPROWORD')
        .w_FLDEFA = 'N'
        .w_SELEZI = "D"
        .w_TIPO2 = 'M'
        .w_CODICE2 = .w_GSCR3SDS.getVar('CCCODICE')
      .oPgFrm.Page3.oPag.GSCR3SDS.Calculate()
      .oPgFrm.Page3.oPag.GSCR4SDS.Calculate()
        .w_TIPO3 = 'D'
        .w_CODICE3 = .w_GSCR5SDS.getVar('CCCODICE')
      .oPgFrm.Page4.oPag.GSCR5SDS.Calculate()
      .oPgFrm.Page4.oPag.GSCR6SDS.Calculate()
        .w_FILTERTYPE = 'S'
        .w_MXCHK = .w_GSCR1SDS.getVar('XCHK')
        .w_CCCONFIG = .w_GSCR4SDS.getVar('CCCONFIG')
        .w_MXCHK2 = .w_GSCR3SDS.getVar('XCHK')
        .w_MXCHK3 = .w_GSCR5SDS.getVar('XCHK')
        .w_CCCONFIG3 = .w_GSCR6SDS.getVar('CCCONFIG')
          .DoRTCalc(50,50,.f.)
        .w_TIPARTI = 'CC'
        .w_TIPARTF = 'CC'
          .DoRTCalc(53,57,.f.)
        .w_COSTILOG = "N"
        .w_CARATTER = "S"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_7.enabled = this.oPgFrm.Page4.oPag.oBtn_4_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_5('Full')
        if .o_CODARTIN<>.w_CODARTIN
            .w_CODARTFI = .w_CODARTIN
          .link_1_6('Full')
        endif
        if .o_DBCODINI<>.w_DBCODINI
            .w_DBCODFIN = .w_DBCODINI
          .link_1_7('Full')
        endif
        if .o_FLNEU<>.w_FLNEU
          .Calculate_ZTRWARGXRE()
        endif
        .oPgFrm.Page2.oPag.GSCR1SDS.Calculate()
        .oPgFrm.Page2.oPag.GSCR2SDS.Calculate()
        .DoRTCalc(6,32,.t.)
            .w_TIPO = 'C'
            .w_CODICE = .w_GSCR1SDS.getVar('CCCODICE')
        if .o_FLDEFA<>.w_FLDEFA
          .Calculate_AEROEXXXYU()
        endif
        .DoRTCalc(35,36,.t.)
            .w_ROWORD = .w_GSCR2SDS.getVar('CPROWORD')
        if .o_SELEZI<>.w_SELEZI
          .Calculate_QODJCQMGFL()
        endif
        .DoRTCalc(38,39,.t.)
            .w_TIPO2 = 'M'
            .w_CODICE2 = .w_GSCR3SDS.getVar('CCCODICE')
        .oPgFrm.Page3.oPag.GSCR3SDS.Calculate()
        .oPgFrm.Page3.oPag.GSCR4SDS.Calculate()
            .w_TIPO3 = 'D'
            .w_CODICE3 = .w_GSCR5SDS.getVar('CCCODICE')
        .oPgFrm.Page4.oPag.GSCR5SDS.Calculate()
        .oPgFrm.Page4.oPag.GSCR6SDS.Calculate()
        .DoRTCalc(44,44,.t.)
            .w_MXCHK = .w_GSCR1SDS.getVar('XCHK')
        if .o_CODICE<>.w_CODICE
          .Calculate_RQVLGMZMDY()
        endif
            .w_CCCONFIG = .w_GSCR4SDS.getVar('CCCONFIG')
            .w_MXCHK2 = .w_GSCR3SDS.getVar('XCHK')
        if .o_CODICE2<>.w_CODICE2
          .Calculate_YHQVDJLPAZ()
        endif
        if .o_CODICE3<>.w_CODICE3
          .Calculate_KBXRVUSMAA()
        endif
            .w_MXCHK3 = .w_GSCR5SDS.getVar('XCHK')
            .w_CCCONFIG3 = .w_GSCR6SDS.getVar('CCCONFIG')
        .DoRTCalc(50,58,.t.)
        if .o_TipoStam<>.w_TipoStam
            .w_CARATTER = "S"
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.GSCR1SDS.Calculate()
        .oPgFrm.Page2.oPag.GSCR2SDS.Calculate()
        .oPgFrm.Page3.oPag.GSCR3SDS.Calculate()
        .oPgFrm.Page3.oPag.GSCR4SDS.Calculate()
        .oPgFrm.Page4.oPag.GSCR5SDS.Calculate()
        .oPgFrm.Page4.oPag.GSCR6SDS.Calculate()
    endwith
  return

  proc Calculate_NKNQUDGBJI()
    with this
          * --- Creazione tabella TMPCARAT
          GSCR1BSD(this;
              ,'AQ1';
             )
    endwith
  endproc
  proc Calculate_YMZFPAFUWZ()
    with this
          * --- Eliminazione TMPCARAT
          GSCR1BSD(this;
              ,'ESC';
             )
    endwith
  endproc
  proc Calculate_ZTRWARGXRE()
    with this
          * --- Aggiorno TMPCARAT
          GSCR1BSD(this;
              ,'NEU';
             )
    endwith
  endproc
  proc Calculate_YZFPODMNUH()
    with this
          * --- Check/Uncheck Dettagli
          GSCR1BSD(this;
              ,'CU2';
             )
    endwith
  endproc
  proc Calculate_SCIPBQXQFO()
    with this
          * --- Check/Uncheck caratteristica
          GSCR1BSD(this;
              ,'CU1';
             )
    endwith
  endproc
  proc Calculate_AEROEXXXYU()
    with this
          * --- Default
          GSCR1BSD(this;
              ,'DER';
             )
    endwith
  endproc
  proc Calculate_QODJCQMGFL()
    with this
          * --- Seleziona/Deseleziona
          GSCR1BSD(this;
              ,'SEL';
             )
    endwith
  endproc
  proc Calculate_JFIJXVPYTB()
    with this
          * --- Check/Uncheck Dettagli
          GSCR1BSD(this;
              ,'CU4';
             )
    endwith
  endproc
  proc Calculate_RBBJTLLYLX()
    with this
          * --- Check/Uncheck modello
          GSCR1BSD(this;
              ,'CU3';
             )
    endwith
  endproc
  proc Calculate_GWKXPDKPPS()
    with this
          * --- Check/Uncheck Dettagli
          GSCR1BSD(this;
              ,'CU6';
             )
    endwith
  endproc
  proc Calculate_LZNDQRDPJM()
    with this
          * --- Check/Uncheck modello
          GSCR1BSD(this;
              ,'CU5';
             )
    endwith
  endproc
  proc Calculate_RQVLGMZMDY()
    with this
          * --- Aggiorno zoom dettagli
          GSCR1BSD(this;
              ,'SE1';
             )
    endwith
  endproc
  proc Calculate_YHQVDJLPAZ()
    with this
          * --- Aggiorno zoom dettagli
          GSCR1BSD(this;
              ,'SE3';
             )
    endwith
  endproc
  proc Calculate_KBXRVUSMAA()
    with this
          * --- Aggiorno zoom dettagli
          GSCR1BSD(this;
              ,'SE5';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCARATTER_1_71.enabled = this.oPgFrm.Page1.oPag.oCARATTER_1_71.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oFLDEFA_2_14.visible=!this.oPgFrm.Page2.oPag.oFLDEFA_2_14.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_15.visible=!this.oPgFrm.Page2.oPag.oBtn_2_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NKNQUDGBJI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_YMZFPAFUWZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.GSCR1SDS.Event(cEvent)
      .oPgFrm.Page2.oPag.GSCR2SDS.Event(cEvent)
        if lower(cEvent)==lower("w_GSCR2SDS row checked") or lower(cEvent)==lower("w_GSCR2SDS row unchecked")
          .Calculate_YZFPODMNUH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_GSCR1SDS row checked") or lower(cEvent)==lower("w_GSCR1SDS row unchecked")
          .Calculate_SCIPBQXQFO()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.GSCR3SDS.Event(cEvent)
      .oPgFrm.Page3.oPag.GSCR4SDS.Event(cEvent)
        if lower(cEvent)==lower("w_GSCR4SDS row checked") or lower(cEvent)==lower("w_GSCR4SDS row unchecked")
          .Calculate_JFIJXVPYTB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_GSCR3SDS row checked") or lower(cEvent)==lower("w_GSCR3SDS row unchecked")
          .Calculate_RBBJTLLYLX()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.GSCR5SDS.Event(cEvent)
      .oPgFrm.Page4.oPag.GSCR6SDS.Event(cEvent)
        if lower(cEvent)==lower("w_GSCR6SDS row checked") or lower(cEvent)==lower("w_GSCR6SDS row unchecked")
          .Calculate_GWKXPDKPPS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_GSCR5SDS row checked") or lower(cEvent)==lower("w_GSCR5SDS row unchecked")
          .Calculate_LZNDQRDPJM()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODARTIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTIN))
          select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTIN_1_4'),i_cWhere,'',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTIN)
            select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESARTIN = NVL(_Link_.ARDESART,space(40))
      this.w_CONCARIN = NVL(_Link_.ARCONCAR,space(1))
      this.w_DBCODINI = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DESARTIN = space(40)
      this.w_CONCARIN = space(1)
      this.w_DBCODINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODARTIN) OR NOT EMPTY(.w_DBCODINI)) AND (empty(.w_CODARTFI) OR .w_CODARTIN<=.w_CODARTFI) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODARTIN = space(20)
        this.w_DESARTIN = space(40)
        this.w_CONCARIN = space(1)
        this.w_DBCODINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODINI)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.DBCODICE,space(20))
      this.w_DESINI = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_DBCODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTFI))
          select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTFI_1_6'),i_cWhere,'',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTFI)
            select ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.ARCODART,space(20))
      this.w_DESARTFI = NVL(_Link_.ARDESART,space(40))
      this.w_CONCARFI = NVL(_Link_.ARCONCAR,space(1))
      this.w_DBCODFIN = NVL(_Link_.ARCODDIS,space(21))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DESARTFI = space(40)
      this.w_CONCARFI = space(1)
      this.w_DBCODFIN = space(21)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODARTFI) OR NOT EMPTY(.w_DBCODFIN)) AND (.w_CODARTFI>=.w_CODARTIN or empty(.w_CODARTFI)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODFIN)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CODARTFI = space(20)
        this.w_DESARTFI = space(40)
        this.w_CONCARFI = space(1)
        this.w_DBCODFIN = space(21)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODFIN)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.DBCODICE,space(21))
      this.w_DESFIN = NVL(_Link_.DBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(21)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale maggiore di quello finale")
        endif
        this.w_DBCODFIN = space(21)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_1_24'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GRUDES = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_GRUDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMERF
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMERF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMERF)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMERF))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMERF)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMERF) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMERF_1_27'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMERF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMERF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMERF)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMERF = NVL(_Link_.GMCODICE,space(5))
      this.w_GRUDESF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMERF = space(5)
      endif
      this.w_GRUDESF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUMER<=.w_GRUMERF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUMERF = space(5)
        this.w_GRUDESF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMERF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAMA
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAMA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAMA)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAMA))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAMA)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAMA) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAMA_1_30'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAMA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAMA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAMA)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAMA = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAMA = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAMA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAMAF
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAMAF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAMAF)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAMAF))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAMAF)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAMAF) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAMAF_1_33'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAMAF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAMAF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAMAF)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAMAF = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAIF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAMAF = space(5)
      endif
      this.w_DESFAMAIF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODFAMA <= .w_CODFAMAF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFAMAF = space(5)
        this.w_DESFAMAIF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAMAF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_36'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCATF
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCATF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCATF)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCATF))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCATF)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCATF) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCATF_1_39'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCATF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCATF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCATF)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCATF = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATIF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCATF = space(5)
      endif
      this.w_DESCATIF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCAT <= .w_CODCATF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCATF = space(5)
        this.w_DESCATIF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCATF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_44'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAGF
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAGF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAGF)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAGF))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAGF)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAGF) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAGF_1_47'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAGF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAGF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAGF)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAGF = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGIF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAGF = space(5)
      endif
      this.w_DESMAGIF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODMAG <= .w_CODMAGF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODMAGF = space(5)
        this.w_DESMAGIF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAGF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMP
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODCOMP)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODCOMP))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMP)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMP) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODCOMP_1_51'),i_cWhere,'',"Componenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODCOMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODCOMP)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMP = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOMIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMP = space(20)
      endif
      this.w_DESCOMIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOMPF
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOMPF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODCOMPF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODCOMPF))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOMPF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOMPF) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODCOMPF_1_53'),i_cWhere,'',"Componenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOMPF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODCOMPF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODCOMPF)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOMPF = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOMFI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOMPF = space(20)
      endif
      this.w_DESCOMFI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODCOMP <= .w_CODCOMPF
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOMPF = space(20)
        this.w_DESCOMFI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOMPF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODARTIN_1_4.value==this.w_CODARTIN)
      this.oPgFrm.Page1.oPag.oCODARTIN_1_4.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODINI_1_5.value==this.w_DBCODINI)
      this.oPgFrm.Page1.oPag.oDBCODINI_1_5.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTFI_1_6.value==this.w_CODARTFI)
      this.oPgFrm.Page1.oPag.oCODARTFI_1_6.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODFIN_1_7.value==this.w_DBCODFIN)
      this.oPgFrm.Page1.oPag.oDBCODFIN_1_7.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_8.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_8.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPRO_1_10.RadioValue()==this.w_DISPRO)
      this.oPgFrm.Page1.oPag.oDISPRO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNEU_1_12.RadioValue()==this.w_FLNEU)
      this.oPgFrm.Page1.oPag.oFLNEU_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTipoStam_1_14.RadioValue()==this.w_TipoStam)
      this.oPgFrm.Page1.oPag.oTipoStam_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_18.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_18.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_19.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_19.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMER_1_24.value==this.w_GRUMER)
      this.oPgFrm.Page1.oPag.oGRUMER_1_24.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUDES_1_25.value==this.w_GRUDES)
      this.oPgFrm.Page1.oPag.oGRUDES_1_25.value=this.w_GRUDES
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMERF_1_27.value==this.w_GRUMERF)
      this.oPgFrm.Page1.oPag.oGRUMERF_1_27.value=this.w_GRUMERF
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUDESF_1_28.value==this.w_GRUDESF)
      this.oPgFrm.Page1.oPag.oGRUDESF_1_28.value=this.w_GRUDESF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAMA_1_30.value==this.w_CODFAMA)
      this.oPgFrm.Page1.oPag.oCODFAMA_1_30.value=this.w_CODFAMA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_31.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_31.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAMAF_1_33.value==this.w_CODFAMAF)
      this.oPgFrm.Page1.oPag.oCODFAMAF_1_33.value=this.w_CODFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAIF_1_34.value==this.w_DESFAMAIF)
      this.oPgFrm.Page1.oPag.oDESFAMAIF_1_34.value=this.w_DESFAMAIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_36.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_36.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_37.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_37.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCATF_1_39.value==this.w_CODCATF)
      this.oPgFrm.Page1.oPag.oCODCATF_1_39.value=this.w_CODCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATIF_1_40.value==this.w_DESCATIF)
      this.oPgFrm.Page1.oPag.oDESCATIF_1_40.value=this.w_DESCATIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_44.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_44.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_45.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_45.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAGF_1_47.value==this.w_CODMAGF)
      this.oPgFrm.Page1.oPag.oCODMAGF_1_47.value=this.w_CODMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGIF_1_48.value==this.w_DESMAGIF)
      this.oPgFrm.Page1.oPag.oDESMAGIF_1_48.value=this.w_DESMAGIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOMP_1_51.value==this.w_CODCOMP)
      this.oPgFrm.Page1.oPag.oCODCOMP_1_51.value=this.w_CODCOMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOMPF_1_53.value==this.w_CODCOMPF)
      this.oPgFrm.Page1.oPag.oCODCOMPF_1_53.value=this.w_CODCOMPF
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCHKLP_1_54.RadioValue()==this.w_FLCHKLP)
      this.oPgFrm.Page1.oPag.oFLCHKLP_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLDEFA_2_14.RadioValue()==this.w_FLDEFA)
      this.oPgFrm.Page2.oPag.oFLDEFA_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_16.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILTERTYPE_2_18.RadioValue()==this.w_FILTERTYPE)
      this.oPgFrm.Page2.oPag.oFILTERTYPE_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARTIN_1_60.value==this.w_DESARTIN)
      this.oPgFrm.Page1.oPag.oDESARTIN_1_60.value=this.w_DESARTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARTFI_1_66.value==this.w_DESARTFI)
      this.oPgFrm.Page1.oPag.oDESARTFI_1_66.value=this.w_DESARTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMIN_1_68.value==this.w_DESCOMIN)
      this.oPgFrm.Page1.oPag.oDESCOMIN_1_68.value=this.w_DESCOMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMFI_1_69.value==this.w_DESCOMFI)
      this.oPgFrm.Page1.oPag.oDESCOMFI_1_69.value=this.w_DESCOMFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSTILOG_1_70.RadioValue()==this.w_COSTILOG)
      this.oPgFrm.Page1.oPag.oCOSTILOG_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCARATTER_1_71.RadioValue()==this.w_CARATTER)
      this.oPgFrm.Page1.oPag.oCARATTER_1_71.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODARTIN) OR NOT EMPTY(.w_DBCODINI)) AND (empty(.w_CODARTFI) OR .w_CODARTIN<=.w_CODARTFI) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODINI)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODINI))))  and not(empty(.w_CODARTIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   not((EMPTY(.w_CODARTFI) OR NOT EMPTY(.w_DBCODFIN)) AND (.w_CODARTFI>=.w_CODARTIN or empty(.w_CODARTFI)) AND (looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODFIN)>.w_DATSTA or empty(looktab('DISMBASE','DBDTOBSO','DBCODICE',.w_DBCODFIN))))  and not(empty(.w_CODARTFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARTFI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBCODFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale maggiore di quello finale")
          case   (empty(.w_DATSTA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTA_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DATSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_TipoStam='E' and .w_FLNEU='N' and .w_SELECTION='S') OR .w_TipoStam<>'E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTipoStam_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUMER<=.w_GRUMERF)  and not(empty(.w_GRUMERF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUMERF_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODFAMA <= .w_CODFAMAF)  and not(empty(.w_CODFAMAF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFAMAF_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCAT <= .w_CODCATF)  and not(empty(.w_CODCATF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCATF_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODMAG <= .w_CODMAGF)  and not(empty(.w_CODMAGF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAGF_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODCOMP <= .w_CODCOMPF)  and not(empty(.w_CODCOMPF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCOMPF_1_53.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODARTIN = this.w_CODARTIN
    this.o_DBCODINI = this.w_DBCODINI
    this.o_FLNEU = this.w_FLNEU
    this.o_TipoStam = this.w_TipoStam
    this.o_CODICE = this.w_CODICE
    this.o_FLDEFA = this.w_FLDEFA
    this.o_SELEZI = this.w_SELEZI
    this.o_CODICE2 = this.w_CODICE2
    this.o_CODICE3 = this.w_CODICE3
    return

enddefine

* --- Define pages as container
define class tgscr_sdsPag1 as StdContainer
  Width  = 795
  height = 537
  stdWidth  = 795
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODARTIN_1_4 as StdField with uid="YLDHOMMPET",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 184803724,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=118, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDBCODINI_1_5 as StdField with uid="EGVKVVQLEU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice distinta di inizio selezione",;
    HelpContextID = 114687617,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=118, Top=36, InputMask=replicate('X',20), cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODARTFI_1_6 as StdField with uid="YCXLBITPAV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 83631727,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=118, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTFI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'GSDS_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDBCODFIN_1_7 as StdField with uid="VCEEWLYQYH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(21), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale maggiore di quello finale",;
    ToolTipText = "Codice distinta di fine selezione",;
    HelpContextID = 103416196,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=118, Top=84, InputMask=replicate('X',21), cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODFIN"

  func oDBCODFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_8 as StdField with uid="WRTRDBYIYW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di validit� dei legami",;
    HelpContextID = 231796682,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=118, Top=110

  add object oDISPRO_1_10 as StdCheck with uid="WNEHIXJZAB",rtseq=8,rtrep=.f.,left=45, top=166, caption="Provvisorie",;
    ToolTipText = "Stampa o ignora le distinte provvisorie",;
    HelpContextID = 267646922,;
    cFormVar="w_DISPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDISPRO_1_10.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDISPRO_1_10.GetRadio()
    this.Parent.oContained.w_DISPRO = this.RadioValue()
    return .t.
  endfunc

  func oDISPRO_1_10.SetRadio()
    this.Parent.oContained.w_DISPRO=trim(this.Parent.oContained.w_DISPRO)
    this.value = ;
      iif(this.Parent.oContained.w_DISPRO=="S",1,;
      0)
  endfunc

  add object oFLNEU_1_12 as StdCheck with uid="KXXMUQHZEA",rtseq=9,rtrep=.f.,left=44, top=224, caption="Distinte configurate",;
    ToolTipText = "Distinte configurate",;
    HelpContextID = 248464554,;
    cFormVar="w_FLNEU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNEU_1_12.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oFLNEU_1_12.GetRadio()
    this.Parent.oContained.w_FLNEU = this.RadioValue()
    return .t.
  endfunc

  func oFLNEU_1_12.SetRadio()
    this.Parent.oContained.w_FLNEU=trim(this.Parent.oContained.w_FLNEU)
    this.value = ;
      iif(this.Parent.oContained.w_FLNEU=='N',1,;
      0)
  endfunc


  add object oBtn_1_13 as StdButton with uid="BTFXAUOYBK",left=688, top=488, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa distinta base";
    , HelpContextID = 16925718;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSCR_BSD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTipoStam_1_14 as StdCombo with uid="JTKFKXIGVB",rtseq=10,rtrep=.f.,left=284,top=488,width=326,height=21;
    , HelpContextID = 87882147;
    , cFormVar="w_TipoStam",RowSource=""+"Indentata,"+"Monolivello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoStam_1_14.RadioValue()
    return(iif(this.value =1,"I",;
    iif(this.value =2,"M",;
    space(1))))
  endfunc
  func oTipoStam_1_14.GetRadio()
    this.Parent.oContained.w_TipoStam = this.RadioValue()
    return .t.
  endfunc

  func oTipoStam_1_14.SetRadio()
    this.Parent.oContained.w_TipoStam=trim(this.Parent.oContained.w_TipoStam)
    this.value = ;
      iif(this.Parent.oContained.w_TipoStam=="I",1,;
      iif(this.Parent.oContained.w_TipoStam=="M",2,;
      0))
  endfunc

  func oTipoStam_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TipoStam='E' and .w_FLNEU='N' and .w_SELECTION='S') OR .w_TipoStam<>'E')
    endwith
    return bRes
  endfunc


  add object oBtn_1_15 as StdButton with uid="YWLUVFHAJA",left=741, top=488, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 16925718;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESINI_1_18 as StdField with uid="UUVRTQQYWV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104528842,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=36, InputMask=replicate('X',40)

  add object oDESFIN_1_19 as StdField with uid="XSEHRGUHOI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26082250,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=84, InputMask=replicate('X',40)

  add object oGRUMER_1_24 as StdField with uid="XMFOPADJJN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 231132826,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=359, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUDES_1_25 as StdField with uid="TYNDHAIZBA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GRUDES", cQueryName = "GRUDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 214945434,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=172, Top=359, InputMask=replicate('X',35)

  add object oGRUMERF_1_27 as StdField with uid="JZBUNOCIYP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRUMERF", cQueryName = "GRUMERF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 37302630,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=499, Top=359, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMERF"

  proc oGRUMERF_1_27.mDefault
    with this.Parent.oContained
      if empty(.w_GRUMERF)
        .w_GRUMERF = .w_GRUMER
      endif
    endwith
  endproc

  func oGRUMERF_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMERF_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMERF_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMERF_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUDESF_1_28 as StdField with uid="RGVVJKTMCC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GRUDESF", cQueryName = "GRUDESF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 53490022,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=561, Top=359, InputMask=replicate('X',35)

  add object oCODFAMA_1_30 as StdField with uid="BAUZFWPTWC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODFAMA", cQueryName = "CODFAMA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo",;
    HelpContextID = 51306970,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=385, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAMA"

  func oCODFAMA_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAMA_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAMA_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAMA_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAI_1_31 as StdField with uid="AOYBYAMKXI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51248001,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=172, Top=385, InputMask=replicate('X',35)

  add object oCODFAMAF_1_33 as StdField with uid="YRHWLVYGJH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODFAMAF", cQueryName = "CODFAMAF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo",;
    HelpContextID = 51306900,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=499, Top=385, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAMAF"

  proc oCODFAMAF_1_33.mDefault
    with this.Parent.oContained
      if empty(.w_CODFAMAF)
        .w_CODFAMAF = .w_CODFAMA
      endif
    endwith
  endproc

  func oCODFAMAF_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAMAF_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAMAF_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAMAF_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oDESFAMAIF_1_34 as StdField with uid="NFJHFEKRDD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESFAMAIF", cQueryName = "DESFAMAIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51246881,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=561, Top=385, InputMask=replicate('X',35)

  add object oCODCAT_1_36 as StdField with uid="WQNLFQEXDD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 202498522,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=412, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATI_1_37 as StdField with uid="ZFWYARIWSP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202439626,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=172, Top=412, InputMask=replicate('X',35)

  add object oCODCATF_1_39 as StdField with uid="ZHPCUMRJRL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODCATF", cQueryName = "CODCATF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 65936934,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=499, Top=412, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCATF"

  proc oCODCATF_1_39.mDefault
    with this.Parent.oContained
      if empty(.w_CODCATF)
        .w_CODCATF = .w_CODCAT
      endif
    endwith
  endproc

  func oCODCATF_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCATF_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCATF_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCATF_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCATIF_1_40 as StdField with uid="HLFUVQJIVA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCATIF", cQueryName = "DESCATIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202439556,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=561, Top=412, InputMask=replicate('X',35)

  add object oCODMAG_1_44 as StdField with uid="WWAXPPIMPZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 151511514,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=438, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oDESMAGI_1_45 as StdField with uid="QONUNYBHAK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 116982838,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=172, Top=438, InputMask=replicate('X',30)

  add object oCODMAGF_1_47 as StdField with uid="SPOYBAMDZC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODMAGF", cQueryName = "CODMAGF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 116923942,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=499, Top=438, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAGF"

  proc oCODMAGF_1_47.mDefault
    with this.Parent.oContained
      if empty(.w_CODMAGF)
        .w_CODMAGF = .w_CODMAG
      endif
    endwith
  endproc

  func oCODMAGF_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAGF_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAGF_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAGF_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oDESMAGIF_1_48 as StdField with uid="COSHWVKTLA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESMAGIF", cQueryName = "DESMAGIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 116982908,;
   bGlobalFont=.t.,;
    Height=21, Width=224, Left=561, Top=438, InputMask=replicate('X',30)

  add object oCODCOMP_1_51 as StdField with uid="IDOGBMZGUZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODCOMP", cQueryName = "CODCOMP",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice componente",;
    HelpContextID = 36823514,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=108, Top=308, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODCOMP"

  func oCODCOMP_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMP_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMP_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODCOMP_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Componenti",'',this.parent.oContained
  endproc

  add object oCODCOMPF_1_53 as StdField with uid="KRDXLDDFYF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODCOMPF", cQueryName = "CODCOMPF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice componente",;
    HelpContextID = 36823444,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=108, Top=333, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODCOMPF"

  proc oCODCOMPF_1_53.mDefault
    with this.Parent.oContained
      if empty(.w_CODCOMPF)
        .w_CODCOMPF = .w_CODCOMP
      endif
    endwith
  endproc

  func oCODCOMPF_1_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOMPF_1_53.ecpDrop(oSource)
    this.Parent.oContained.link_1_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOMPF_1_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODCOMPF_1_53'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Componenti",'',this.parent.oContained
  endproc

  add object oFLCHKLP_1_54 as StdCheck with uid="SKYASPWUKP",rtseq=32,rtrep=.f.,left=224, top=166, caption="Loop distinta",;
    ToolTipText = "Se attivo abilita controllo del loop in distinta base",;
    HelpContextID = 57472170,;
    cFormVar="w_FLCHKLP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCHKLP_1_54.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oFLCHKLP_1_54.GetRadio()
    this.Parent.oContained.w_FLCHKLP = this.RadioValue()
    return .t.
  endfunc

  func oFLCHKLP_1_54.SetRadio()
    this.Parent.oContained.w_FLCHKLP=trim(this.Parent.oContained.w_FLCHKLP)
    this.value = ;
      iif(this.Parent.oContained.w_FLCHKLP=="S",1,;
      0)
  endfunc

  add object oDESARTIN_1_60 as StdField with uid="YJQMLZHVQA",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESARTIN", cQueryName = "DESARTIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 184744828,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=12, InputMask=replicate('X',40)

  add object oDESARTFI_1_66 as StdField with uid="PZWPBLGGXS",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESARTFI", cQueryName = "DESARTFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 83690623,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=60, InputMask=replicate('X',40)

  add object oDESCOMIN_1_68 as StdField with uid="LFZZDJEUEI",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESCOMIN", cQueryName = "DESCOMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 231670916,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=273, Top=308, InputMask=replicate('X',40)

  add object oDESCOMFI_1_69 as StdField with uid="AKFILAPYNR",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESCOMFI", cQueryName = "DESCOMFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 231670911,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=273, Top=333, InputMask=replicate('X',40)

  add object oCOSTILOG_1_70 as StdCheck with uid="AOSDRGZMLX",rtseq=58,rtrep=.f.,left=378, top=166, caption="Attiva scrittura log",;
    ToolTipText = "Attiva la scrittura del log di elaborazione costi",;
    HelpContextID = 58716563,;
    cFormVar="w_COSTILOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOSTILOG_1_70.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCOSTILOG_1_70.GetRadio()
    this.Parent.oContained.w_COSTILOG = this.RadioValue()
    return .t.
  endfunc

  func oCOSTILOG_1_70.SetRadio()
    this.Parent.oContained.w_COSTILOG=trim(this.Parent.oContained.w_COSTILOG)
    this.value = ;
      iif(this.Parent.oContained.w_COSTILOG=="S",1,;
      0)
  endfunc

  add object oCARATTER_1_71 as StdCheck with uid="DFDBGDEBQM",rtseq=59,rtrep=.f.,left=224, top=224, caption="Stampa caratteristiche",;
    ToolTipText = "Stampa le caratteristiche associate ai legami",;
    HelpContextID = 85782648,;
    cFormVar="w_CARATTER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCARATTER_1_71.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCARATTER_1_71.GetRadio()
    this.Parent.oContained.w_CARATTER = this.RadioValue()
    return .t.
  endfunc

  func oCARATTER_1_71.SetRadio()
    this.Parent.oContained.w_CARATTER=trim(this.Parent.oContained.w_CARATTER)
    this.value = ;
      iif(this.Parent.oContained.w_CARATTER=="S",1,;
      0)
  endfunc

  func oCARATTER_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TipoStam$"I-M-D")
    endwith
   endif
  endfunc

  add object oStr_1_2 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=0, Top=37,;
    Alignment=1, Width=114, Height=18,;
    Caption="Distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=2, Top=85,;
    Alignment=1, Width=112, Height=15,;
    Caption="A distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RTZOXGGRZO",Visible=.t., Left=6, Top=111,;
    Alignment=1, Width=109, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="KWSTLBHYKG",Visible=.t., Left=179, Top=488,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YCQOUAKEYM",Visible=.t., Left=45, Top=145,;
    Alignment=0, Width=169, Height=15,;
    Caption="Distinte non rilasciate"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HZSPMXYDMA",Visible=.t., Left=39, Top=201,;
    Alignment=0, Width=176, Height=15,;
    Caption="Caratteristiche configurazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="HBQHQHYLHM",Visible=.t., Left=5, Top=359,;
    Alignment=1, Width=103, Height=18,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PIVQMRHITC",Visible=.t., Left=405, Top=359,;
    Alignment=1, Width=92, Height=22,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=3, Top=385,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EBKWVDHVTH",Visible=.t., Left=404, Top=385,;
    Alignment=1, Width=93, Height=22,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=6, Top=412,;
    Alignment=1, Width=102, Height=18,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GCNVGAQDZZ",Visible=.t., Left=405, Top=412,;
    Alignment=1, Width=92, Height=22,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="LIGUQPQABF",Visible=.t., Left=11, Top=274,;
    Alignment=0, Width=264, Height=15,;
    Caption="Componenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=4, Top=438,;
    Alignment=1, Width=104, Height=18,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="NNJXDAMSOQ",Visible=.t., Left=404, Top=438,;
    Alignment=1, Width=93, Height=22,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="HYOXXXYPSO",Visible=.t., Left=4, Top=308,;
    Alignment=1, Width=104, Height=18,;
    Caption="Da cod. comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="FOOJZSLRIN",Visible=.t., Left=15, Top=337,;
    Alignment=1, Width=93, Height=18,;
    Caption="A cod comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="SJOQRQEYXN",Visible=.t., Left=224, Top=143,;
    Alignment=0, Width=71, Height=18,;
    Caption="Controllo"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="AZAYVNOACS",Visible=.t., Left=39, Top=15,;
    Alignment=1, Width=75, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="WWDHJJGYZQ",Visible=.t., Left=39, Top=63,;
    Alignment=1, Width=75, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_20 as StdBox with uid="RBXPPTRNPY",left=36, top=162, width=148,height=1

  add object oBox_1_22 as StdBox with uid="UPMSPAXYSB",left=12, top=220, width=773,height=1

  add object oBox_1_42 as StdBox with uid="HHMYMSDEQI",left=12, top=291, width=773,height=1

  add object oBox_1_56 as StdBox with uid="ZXUPTCHTYW",left=215, top=162, width=319,height=1
enddefine
define class tgscr_sdsPag2 as StdContainer
  Width  = 795
  height = 537
  stdWidth  = 795
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object GSCR1SDS as cp_szoombox with uid="OLCEWYGAFE",left=22, top=32, width=704,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONF_CAR",cZoomFile="GSCR1SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 103977446


  add object GSCR2SDS as cp_szoombox with uid="OQKSYKKTRL",left=21, top=272, width=704,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONFDDIS",cZoomFile="GSCR2SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga2",;
    nPag=2;
    , HelpContextID = 103977446


  add object oBtn_2_8 as StdButton with uid="OJIJKJBYEZ",left=735, top=41, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 74018282;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLDEFA_2_14 as StdCheck with uid="TVPMSRPOJU",rtseq=38,rtrep=.f.,left=224, top=253, caption="Default",;
    ToolTipText = "Applica il valore di default per la riga selezionata",;
    HelpContextID = 247456938,;
    cFormVar="w_FLDEFA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLDEFA_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDEFA_2_14.GetRadio()
    this.Parent.oContained.w_FLDEFA = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFA_2_14.SetRadio()
    this.Parent.oContained.w_FLDEFA=trim(this.Parent.oContained.w_FLDEFA)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFA=='S',1,;
      0)
  endfunc

  func oFLDEFA_2_14.mHide()
    with this.Parent.oContained
      return (.w_FLNEU<>'S')
    endwith
  endfunc


  add object oBtn_2_15 as StdButton with uid="WLQNZTPDNG",left=314, top=252, width=50,height=25,;
    caption="Def.", nPag=2;
    , ToolTipText = "Applica il default a tutti i records selezionati";
    , HelpContextID = 70560714;
  , bGlobalFont=.t.

    proc oBtn_2_15.Click()
      with this.Parent.oContained
        GSCR1BSD(this.Parent.oContained,"DEF")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLNEU<>'S')
     endwith
    endif
  endfunc

  add object oSELEZI_2_16 as StdRadio with uid="KCMBJLDROQ",rtseq=39,rtrep=.f.,left=21, top=493, width=239,height=23;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 92236506
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 92236506
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_16.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_16.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_16.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oFILTERTYPE_2_18 as StdCombo with uid="ZDKLVJEHZM",rtseq=44,rtrep=.f.,left=505,top=253,width=107,height=21;
    , ToolTipText = "Determina la tipologia di filtro per il dettaglio caratteristiche";
    , HelpContextID = 37741231;
    , cFormVar="w_FILTERTYPE",RowSource=""+"Singoli (OR),"+"Multipli (AND)", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFILTERTYPE_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oFILTERTYPE_2_18.GetRadio()
    this.Parent.oContained.w_FILTERTYPE = this.RadioValue()
    return .t.
  endfunc

  func oFILTERTYPE_2_18.SetRadio()
    this.Parent.oContained.w_FILTERTYPE=trim(this.Parent.oContained.w_FILTERTYPE)
    this.value = ;
      iif(this.Parent.oContained.w_FILTERTYPE=='S',1,;
      iif(this.Parent.oContained.w_FILTERTYPE=='M',2,;
      0))
  endfunc

  add object oStr_2_1 as StdString with uid="ULGHSYKQSF",Visible=.t., Left=20, Top=16,;
    Alignment=0, Width=701, Height=18,;
    Caption="Selezione caratteristiche"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="UDWJQERWXS",Visible=.t., Left=18, Top=257,;
    Alignment=0, Width=201, Height=18,;
    Caption="Selezione dettaglio caratteristiche"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="GVESSVUHIE",Visible=.t., Left=373, Top=257,;
    Alignment=1, Width=129, Height=17,;
    Caption="Tipologia filtro:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscr_sdsPag3 as StdContainer
  Width  = 795
  height = 537
  stdWidth  = 795
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object GSCR3SDS as cp_szoombox with uid="HTTJFWKUVR",left=22, top=32, width=704,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONF_CAR",cZoomFile="GSCR3SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga3",;
    nPag=3;
    , HelpContextID = 103977446


  add object GSCR4SDS as cp_szoombox with uid="RRVMDCTSHZ",left=21, top=272, width=704,height=249,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONFMEMO",cZoomFile="GSCR4SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga4",;
    nPag=3;
    , HelpContextID = 103977446


  add object oBtn_3_7 as StdButton with uid="DVPFEQQTUH",left=735, top=41, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 74018282;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_7.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga3')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_3_1 as StdString with uid="FMSKJNUTFB",Visible=.t., Left=20, Top=16,;
    Alignment=0, Width=697, Height=18,;
    Caption="Selezione modelli"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="YXQNZFYHPS",Visible=.t., Left=18, Top=257,;
    Alignment=0, Width=697, Height=18,;
    Caption="Selezione tipi modelli"  ;
  , bGlobalFont=.t.
enddefine
define class tgscr_sdsPag4 as StdContainer
  Width  = 795
  height = 537
  stdWidth  = 795
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object GSCR5SDS as cp_szoombox with uid="EIZEDOUHIM",left=22, top=32, width=704,height=216,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONF_CAR",cZoomFile="GSCR5SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Interroga5",;
    nPag=4;
    , HelpContextID = 103977446


  add object GSCR6SDS as cp_szoombox with uid="COWEFHEYLH",left=21, top=272, width=704,height=252,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONFMEMO",cZoomFile="GSCR6SDS",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Interroga6",;
    nPag=4;
    , HelpContextID = 103977446


  add object oBtn_4_7 as StdButton with uid="AYGNEJWRZN",left=735, top=41, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=4;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 74018282;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_7.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga5')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_4_1 as StdString with uid="KJHFPUYVLJ",Visible=.t., Left=20, Top=16,;
    Alignment=0, Width=701, Height=18,;
    Caption="Selezione caratteristiche"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="KIWVXDIJLF",Visible=.t., Left=18, Top=257,;
    Alignment=0, Width=655, Height=18,;
    Caption="Selezione descrizioni"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscr_sds','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
