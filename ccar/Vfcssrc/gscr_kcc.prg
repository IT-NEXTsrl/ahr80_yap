* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_kcc                                                        *
*              CONFIGURATORE CARATTERISTICHE                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-18                                                      *
* Last revis.: 2015-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_kcc",oParentObject))

* --- Class definition
define class tgscr_kcc as StdForm
  Top    = 6
  Left   = 3

  * --- Standard Properties
  Width  = 866
  Height = 582
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-22"
  HelpContextID=99186025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=93

  * --- Constant Properties
  _IDX = 0
  DISMBASE_IDX = 0
  ART_ICOL_IDX = 0
  CONF_CAR_IDX = 0
  AZIENDA_IDX = 0
  LINGUE_IDX = 0
  PAR_DISB_IDX = 0
  LISTINI_IDX = 0
  ANA_CONF_IDX = 0
  cPrg = "gscr_kcc"
  cComment = "CONFIGURATORE CARATTERISTICHE"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READPAR = space(2)
  w_PDRICONF = space(1)
  w_ARTIPART = space(2)
  w_ARCODART = space(20)
  o_ARCODART = space(20)
  w_DBCODICE = space(20)
  o_DBCODICE = space(20)
  w_TIPGEN = space(1)
  o_TIPGEN = space(1)
  w_DISMANU = space(1)
  w_DBDESCRI = space(40)
  w_PERCEN = 0
  w_EXPSL = space(1)
  w_TIPOG = space(1)
  w_CCFORMUL = space(5)
  w_RES = 0
  w_TIPOV = space(1)
  w_TESTCODE = space(1)
  w_cCursM = space(10)
  w_cRispDoc = space(10)
  w_TERMINA = .F.
  w_Modelli = space(10)
  w_cExpldb = space(10)
  w_cInitCur = space(10)
  w_cCheckCur = space(10)
  w_cTestInit = space(10)
  w_ARDESART = space(40)
  w_CCTIPOCA = space(1)
  o_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  o_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_KEYRIF = space(10)
  w_KEYCAR = space(16)
  w_DBCODART = space(20)
  w_ARCONCAR = space(1)
  w_RECNO = space(10)
  o_RECNO = space(10)
  w_CC__NOTE = space(0)
  w_CCCONFIG = space(0)
  w_LEVEL = 0
  w_ROWSEL = 0
  w_DADOC = .F.
  w_RICDOC = .F.
  w_NUMCAR = 0
  w_CCDETTAG = space(40)
  w_LIVCURS = 0
  w_LVLKEYTV = space(10)
  w_CCROWORD = 0
  w_CCCOMPON = space(41)
  w_CCAMBIO = .F.
  w_TOSTORE = space(1)
  w_cCursCaratt = space(10)
  w_FINE = .F.
  w_CCCONFIG = space(0)
  w_CC__DIBA = space(41)
  w_cModelli = space(10)
  w_Fisse = space(10)
  w_cFisse = space(10)
  w_Descrit = space(10)
  w_cDescrit = space(10)
  w_cCursZ = space(10)
  w_LVLKEY1 = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_CODLIN = space(3)
  w_CODLIS = space(5)
  w_LUDESCRI = space(30)
  w_DESLIS = space(40)
  w_RIC = space(1)
  w_DEFA = space(1)
  w_CONTINUECHECK = space(1)
  w_CCFLATTI = space(1)
  w_FLINIT = space(1)
  o_FLINIT = space(1)
  w_VERMEM = space(1)
  w_RICONF = space(1)
  w_cCursor = space(10)
  w_EXPSL = space(1)
  w_PERCEN = 0
  w_cCarParam = space(10)
  w_LSQUANTI = space(10)
  w_LSQUANTI = space(1)
  w_ARTIPART = space(10)
  w_DATSTA = ctod('  /  /  ')
  w_KEYRIF1 = space(10)
  w_Ret = .F.
  w_NODO = space(5)
  w_TIPONODO = space(1)
  w_DURINGINIT = .F.
  w_CCKEYSAL = space(20)
  w_TIPARTF = space(10)
  w_cCursTree = space(10)
  w_cCursBmp = space(10)
  w_CARADD = 0
  w_cCursCONF_DIS = space(10)
  w_cCursCONFDDIS = space(10)
  w_CCSERIAL = space(15)
  w_GENMOD = space(1)
  w_CCSERIAL1 = space(15)
  w_CCDESCR1 = space(40)
  w_Albero = .NULL.
  w_TREEVIEW = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscr_kcc
  cBmp1 = ''
  cBmp2 = ''
  cBmp3 = ''
  nThemeColorLevel = 0
  nThemeColor = 0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_kccPag1","gscr_kcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODART_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Albero = this.oPgFrm.Pages(1).oPag.Albero
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    DoDefault()
    proc Destroy()
      this.w_Albero = .NULL.
      this.w_TREEVIEW = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CONF_CAR'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='LINGUE'
    this.cWorkTables[6]='PAR_DISB'
    this.cWorkTables[7]='LISTINI'
    this.cWorkTables[8]='ANA_CONF'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSCR_BC4(this,"CREADIS_CLOSE")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READPAR=space(2)
      .w_PDRICONF=space(1)
      .w_ARTIPART=space(2)
      .w_ARCODART=space(20)
      .w_DBCODICE=space(20)
      .w_TIPGEN=space(1)
      .w_DISMANU=space(1)
      .w_DBDESCRI=space(40)
      .w_PERCEN=0
      .w_EXPSL=space(1)
      .w_TIPOG=space(1)
      .w_CCFORMUL=space(5)
      .w_RES=0
      .w_TIPOV=space(1)
      .w_TESTCODE=space(1)
      .w_cCursM=space(10)
      .w_cRispDoc=space(10)
      .w_TERMINA=.f.
      .w_Modelli=space(10)
      .w_cExpldb=space(10)
      .w_cInitCur=space(10)
      .w_cCheckCur=space(10)
      .w_cTestInit=space(10)
      .w_ARDESART=space(40)
      .w_CCTIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_KEYRIF=space(10)
      .w_KEYCAR=space(16)
      .w_DBCODART=space(20)
      .w_ARCONCAR=space(1)
      .w_RECNO=space(10)
      .w_CC__NOTE=space(0)
      .w_CCCONFIG=space(0)
      .w_LEVEL=0
      .w_ROWSEL=0
      .w_DADOC=.f.
      .w_RICDOC=.f.
      .w_NUMCAR=0
      .w_CCDETTAG=space(40)
      .w_LIVCURS=0
      .w_LVLKEYTV=space(10)
      .w_CCROWORD=0
      .w_CCCOMPON=space(41)
      .w_CCAMBIO=.f.
      .w_TOSTORE=space(1)
      .w_cCursCaratt=space(10)
      .w_FINE=.f.
      .w_CCCONFIG=space(0)
      .w_CC__DIBA=space(41)
      .w_cModelli=space(10)
      .w_Fisse=space(10)
      .w_cFisse=space(10)
      .w_Descrit=space(10)
      .w_cDescrit=space(10)
      .w_cCursZ=space(10)
      .w_LVLKEY1=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODLIN=space(3)
      .w_CODLIS=space(5)
      .w_LUDESCRI=space(30)
      .w_DESLIS=space(40)
      .w_RIC=space(1)
      .w_DEFA=space(1)
      .w_CONTINUECHECK=space(1)
      .w_CCFLATTI=space(1)
      .w_FLINIT=space(1)
      .w_VERMEM=space(1)
      .w_RICONF=space(1)
      .w_cCursor=space(10)
      .w_EXPSL=space(1)
      .w_PERCEN=0
      .w_cCarParam=space(10)
      .w_LSQUANTI=space(10)
      .w_LSQUANTI=space(1)
      .w_ARTIPART=space(10)
      .w_DATSTA=ctod("  /  /  ")
      .w_KEYRIF1=space(10)
      .w_Ret=.f.
      .w_NODO=space(5)
      .w_TIPONODO=space(1)
      .w_DURINGINIT=.f.
      .w_CCKEYSAL=space(20)
      .w_TIPARTF=space(10)
      .w_cCursTree=space(10)
      .w_cCursBmp=space(10)
      .w_CARADD=0
      .w_cCursCONF_DIS=space(10)
      .w_cCursCONFDDIS=space(10)
      .w_CCSERIAL=space(15)
      .w_GENMOD=space(1)
      .w_CCSERIAL1=space(15)
      .w_CCDESCR1=space(40)
        .w_READPAR = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READPAR))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_ARTIPART = 'CC'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ARCODART))
          .link_1_4('Full')
        endif
        .w_DBCODICE = .w_DBCODICE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DBCODICE))
          .link_1_5('Full')
        endif
        .w_TIPGEN = 'C'
        .w_DISMANU = ' '
          .DoRTCalc(8,9,.f.)
        .w_EXPSL = 'S'
          .DoRTCalc(11,14,.f.)
        .w_TESTCODE = 'S'
        .w_cCursM = SYS(2015)
        .w_cRispDoc = SYS(2015)
        .w_TERMINA = .F.
        .w_Modelli = SYS(2015)
        .w_cExpldb = SYS(2015)
        .w_cInitCur = SYS(2015)
        .w_cCheckCur = SYS(2015)
        .w_cTestInit = SYS(2015)
      .oPgFrm.Page1.oPag.Albero.Calculate()
          .DoRTCalc(24,24,.f.)
        .w_CCTIPOCA = .w_Albero.GetVar("CCTIPOCA")
        .w_CCCODICE = .w_Albero.GetVar("CCCODICE")
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CCCODICE))
          .link_1_36('Full')
        endif
        .w_CCDESCRI = .w_Albero.GetVar("CCDESCRI")
          .DoRTCalc(28,28,.f.)
        .w_KEYCAR = .w_CCTIPOCA+.w_CCCODICE
        .w_DBCODART = .w_ARCODART
          .DoRTCalc(31,31,.f.)
        .w_RECNO = RECNO(.w_Albero.cCursor)
        .w_CC__NOTE = .w_Albero.GetVar("CCDETTAG")
          .DoRTCalc(34,34,.f.)
        .w_LEVEL = .w_Albero.GetVar("LIVELLO")
        .w_ROWSEL = INT(.w_Albero.GetVar("CPROWORD"))
          .DoRTCalc(37,38,.f.)
        .w_NUMCAR = 0
        .w_CCDETTAG = .w_Albero.GetVar("CCDETTAG")
        .w_LIVCURS = .w_Albero.GetVar("LIVCURSM")
        .w_LVLKEYTV = .w_Albero.GetVar("LVLKEY")
        .w_CCROWORD = .w_Albero.GetVar("CCROWORD")
        .w_CCCOMPON = .w_Albero.GetVar("CCCOMPON")
        .w_CCAMBIO = .F.
        .w_TOSTORE = 'N'
        .w_cCursCaratt = SYS(2015)
      .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .w_FINE = .F.
          .DoRTCalc(49,49,.f.)
        .w_CC__DIBA = .w_Albero.GetVar("CC__DIBA")
        .w_cModelli = SYS(2015)
        .w_Fisse = SYS(2015)
        .w_cFisse = SYS(2015)
        .w_Descrit = SYS(2015)
        .w_cDescrit = SYS(2015)
        .w_cCursZ = SYS(2015)
        .w_LVLKEY1 = .w_Albero.GetVar("LVLKEY")
      .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .w_OBTEST = I_DATSYS
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_CODLIN))
          .link_1_76('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CODLIS))
          .link_1_77('Full')
        endif
          .DoRTCalc(61,62,.f.)
        .w_RIC = 'N'
        .w_DEFA = ' '
        .w_CONTINUECHECK = 'N'
        .w_CCFLATTI = 'S'
        .w_FLINIT = 'N'
        .w_VERMEM = .w_TIPOV
        .w_RICONF = .w_PDRICONF
        .w_cCursor = SYS(2015)
        .w_EXPSL = 'S'
          .DoRTCalc(72,72,.f.)
        .w_cCarParam = SYS(2015)
      .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
          .DoRTCalc(74,75,.f.)
        .w_ARTIPART = 'CC'
        .w_DATSTA = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .DoRTCalc(78,80,.f.)
        if not(empty(.w_NODO))
          .link_1_104('Full')
        endif
        .w_TIPONODO = 'C'
        .w_DURINGINIT = .F.
      .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
          .DoRTCalc(83,83,.f.)
        .w_TIPARTF = 'CC'
        .w_cCursTree = SYS(2015)
        .w_cCursBmp = SYS(2015)
        .w_CARADD = 0
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .w_cCursCONF_DIS = SYS(2015)
        .w_cCursCONFDDIS = SYS(2015)
        .w_CCSERIAL = SPACE(9)
        .w_GENMOD = 'N'
        .w_CCSERIAL1 = IIF(.w_FLINIT $ 'C-M', .w_CCSERIAL1, space(15))
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_CCSERIAL1))
          .link_1_125('Full')
        endif
    endwith
    this.DoRTCalc(93,93,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscr_kcc
    ENDPROC
    
    Proc Destroy
        This.cBmp1 = ""
        This.cBmp2 = ""
        This.cBmp3 = ""
        DoDefault()
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_READPAR = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_ARCODART<>.w_ARCODART.or. .o_DBCODICE<>.w_DBCODICE
            .w_DBCODICE = .w_DBCODICE
          .link_1_5('Full')
        endif
        if .o_DBCODICE<>.w_DBCODICE
            .w_TIPGEN = 'C'
        endif
        .oPgFrm.Page1.oPag.Albero.Calculate()
        .DoRTCalc(7,24,.t.)
            .w_CCTIPOCA = .w_Albero.GetVar("CCTIPOCA")
            .w_CCCODICE = .w_Albero.GetVar("CCCODICE")
          .link_1_36('Full')
            .w_CCDESCRI = .w_Albero.GetVar("CCDESCRI")
        .DoRTCalc(28,28,.t.)
        if .o_CCTIPOCA<>.w_CCTIPOCA.or. .o_CCCODICE<>.w_CCCODICE
            .w_KEYCAR = .w_CCTIPOCA+.w_CCCODICE
        endif
            .w_DBCODART = .w_ARCODART
        .DoRTCalc(31,31,.t.)
            .w_RECNO = RECNO(.w_Albero.cCursor)
        if .o_RECNO<>.w_RECNO
            .w_CC__NOTE = .w_Albero.GetVar("CCDETTAG")
        endif
        .DoRTCalc(34,34,.t.)
            .w_LEVEL = .w_Albero.GetVar("LIVELLO")
            .w_ROWSEL = INT(.w_Albero.GetVar("CPROWORD"))
        .DoRTCalc(37,39,.t.)
            .w_CCDETTAG = .w_Albero.GetVar("CCDETTAG")
            .w_LIVCURS = .w_Albero.GetVar("LIVCURSM")
            .w_LVLKEYTV = .w_Albero.GetVar("LVLKEY")
            .w_CCROWORD = .w_Albero.GetVar("CCROWORD")
            .w_CCCOMPON = .w_Albero.GetVar("CCCOMPON")
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .DoRTCalc(45,49,.t.)
            .w_CC__DIBA = .w_Albero.GetVar("CC__DIBA")
        .DoRTCalc(51,56,.t.)
            .w_LVLKEY1 = .w_Albero.GetVar("LVLKEY")
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .DoRTCalc(58,89,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_CCSERIAL = SPACE(9)
        endif
        .DoRTCalc(91,91,.t.)
        if .o_FLINIT<>.w_FLINIT.or. .o_ARCODART<>.w_ARCODART.or. .o_DBCODICE<>.w_DBCODICE
            .w_CCSERIAL1 = IIF(.w_FLINIT $ 'C-M', .w_CCSERIAL1, space(15))
          .link_1_125('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(93,93,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Albero.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
    endwith
  return

  proc Calculate_KQGGQCPUGR()
    with this
          * --- AggiornaTV
     if !.w_DURINGINIT
          GSCR_BC4(this;
              ,"TVDISTINTA";
             )
          .w_Ret = .w_TREEVIEW.ExpandAll(.T.)
     endif
    endwith
  endproc
  proc Calculate_UKIQYSIFCB()
    with this
          * --- AggiornaTV
     if !.w_DURINGINIT
          .w_Ret = .w_TREEVIEW.ExpandAll(.T.)
     endif
    endwith
  endproc
  proc Calculate_LNKFLEFQKX()
    with this
          * --- 
          GSCR_BC4(this;
              ,"CAMBIO";
             )
      if !.w_CCAMBIO and !.w_DURINGINIT
          .w_Ret = .w_Albero.Grd.SetFocus()
      endif
    endwith
  endproc
  proc Calculate_LYZRSBCSGI()
    with this
          * --- OPENCARDESC
          GSCR_BC4(this;
              ,"OPENCARDESC";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEXPSL_1_91.enabled = this.oPgFrm.Page1.oPag.oEXPSL_1_91.mCond()
    this.oPgFrm.Page1.oPag.oPERCEN_1_92.enabled = this.oPgFrm.Page1.oPag.oPERCEN_1_92.mCond()
    this.oPgFrm.Page1.oPag.oCCSERIAL1_1_125.enabled = this.oPgFrm.Page1.oPag.oCCSERIAL1_1_125.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscr_kcc
      if cEvent=="FormLoad"
        This.nThemeColorLevel = Rgb(179,7,27)
        This.nThemeColor = i_ThemesManager.getprop(109)    
        This.cBmp1 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"gd_plus.bmp", 16), "bmp"))
        This.cBmp2 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"gd_minus.bmp", 16),"bmp"))
        This.cBmp3 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"next.bmp", 10),"bmp"))
      endif
      if cEvent = 'w_albero after query' and this.w_Albero.grd.ColumnCount > 0
          LOCAL loColumn, lControlSource,lColumnOrder
          
          this.w_Albero.grd.RecordMark=.F.
          this.w_Albero.grd.HighlightRow= .F.
          this.w_Albero.grd.Highlight =.F.
          this.w_Albero.grd.HighlightRowLineWidth = 1
          this.w_Albero.grd.AllowRowsizing=.F.
          this.w_Albero.grd.AllowCellSelection = .t.
          this.w_Albero.grd.DeleteMark=.F.
          this.w_Albero.grd.FontBold=.F.
          this.w_Albero.grd.HighlightStyle=2
          this.w_Albero.grd.GridLines=2
          this.w_Albero.grd.GridLineColor = ThisForm.nThemeColor
          
    			FOR EACH m.loColumn IN this.w_Albero.grd.Columns
            lControlSource =  m.loColumn.Controlsource
            lColumnOrder = m.loColumn.ColumnOrder
            
                  IF lControlSource = 'DESCRITV'
                  
                    WITH m.loColumn
                      .Sparse=.f.
                      .ReadOnly=.f.
                      
                      IF !PEMSTATUS(m.loColumn, [Container1], 5)
                        .AddObject([Container1], [HdrRowGroup])
                      endif
    
                      IF !PEMSTATUS(m.loColumn, [ContainerL], 5)
                        .AddObject([ContainerL], [HdrlVLGroup])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerF], 5)
                        .AddObject([ContainerF], [DtlCarFisse])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerM], 5)
                        .AddObject([ContainerM], [DtlCarParam])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerD], 5)
                        .AddObject([ContainerD], [DtlCarDescr])
                      endif          
                      
                       .CurrentControl=""
                       .DynamicCurrentControl = "IIF(CCHDRLVL='S', 'ContainerL', IIF(CCHEADER='S', 'Container1' , IIF(CCTIPOCA='C', 'ContainerF' , IIF(CCTIPOCA='M', 'ContainerM' ,  IIF(CCTIPOCA='D', 'ContainerD', 'Text1')))))"
                       * RGB(206,231,255) AHR 7.0
                       * RGB(400,40,40) AHR 8.0
                       
                    ENDWITH
                       
                  ELSE
                    IF !lControlSource $ 'CCHDRLVL-CCHEADER-DESCRITV-CCVALMIN-CCVALMAX-CCFLINT'
                            m.loColumn.Visible=.f.
                    endif                
                 ENDIF
                    
          ENDFOR
          
        endif
            
    
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Albero.Event(cEvent)
        if lower(cEvent)==lower("AggiornaTV")
          .Calculate_KQGGQCPUGR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiornaTV")
          .Calculate_UKIQYSIFCB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
        if lower(cEvent)==lower("w_Albero row checked") or lower(cEvent)==lower("w_Albero row unchecked")
          .Calculate_LNKFLEFQKX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_99.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_101.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_108.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_109.Event(cEvent)
        if lower(cEvent)==lower("OPENCARDESC")
          .Calculate_LYZRSBCSGI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READPAR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_DISB_IDX,3]
    i_lTable = "PAR_DISB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2], .t., this.PAR_DISB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCODAZI,PDVERMEM,PDRICONF";
                   +" from "+i_cTable+" "+i_lTable+" where PDCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCODAZI',this.w_READPAR)
            select PDCODAZI,PDVERMEM,PDRICONF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.PDCODAZI,space(2))
      this.w_TIPOV = NVL(_Link_.PDVERMEM,space(1))
      this.w_PDRICONF = NVL(_Link_.PDRICONF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(2)
      endif
      this.w_TIPOV = space(1)
      this.w_PDRICONF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_DISB_IDX,2])+'\'+cp_ToStr(_Link_.PDCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_DISB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARCODART)+"%");
                   +" and ARTIPART="+cp_ToStrODBC(this.w_TIPARTF);

          i_ret=cp_SQL(i_nConn,"select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARTIPART,ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARTIPART',this.w_TIPARTF;
                     ,'ARCODART',trim(this.w_ARCODART))
          select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARTIPART,ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARTIPART,ARCODART',cp_AbsName(oSource.parent,'oARCODART_1_4'),i_cWhere,'',"Elenco articoli",'GSCR_SDS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPARTF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ARTIPART="+cp_ToStrODBC(this.w_TIPARTF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARTIPART',oSource.xKey(1);
                       ,'ARCODART',oSource.xKey(2))
            select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARCODART);
                   +" and ARTIPART="+cp_ToStrODBC(this.w_TIPARTF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARTIPART',this.w_TIPARTF;
                       ,'ARCODART',this.w_ARCODART)
            select ARTIPART,ARCODART,ARDESART,ARCONCAR,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODART = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESART = NVL(_Link_.ARDESART,space(40))
      this.w_ARCONCAR = NVL(_Link_.ARCONCAR,space(1))
      this.w_DBCODICE = NVL(_Link_.ARCODDIS,space(20))
      this.w_DBCODART = NVL(_Link_.ARCODART,space(20))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODART = space(20)
      endif
      this.w_ARDESART = space(40)
      this.w_ARCONCAR = space(1)
      this.w_DBCODICE = space(20)
      this.w_DBCODART = space(20)
      this.w_ARTIPART = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARCONCAR='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        endif
        this.w_ARCODART = space(20)
        this.w_ARDESART = space(40)
        this.w_ARCONCAR = space(1)
        this.w_DBCODICE = space(20)
        this.w_DBCODART = space(20)
        this.w_ARTIPART = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARTIPART,1)+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODICE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DBCODICE)
            select DBCODICE,DBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODICE = NVL(_Link_.DBCODICE,space(20))
      this.w_DBDESCRI = NVL(_Link_.DBDESCRI,space(40))
      this.w_CCKEYSAL = NVL(_Link_.DBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODICE = space(20)
      endif
      this.w_DBDESCRI = space(40)
      this.w_CCKEYSAL = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODICE
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_CCTIPOCA;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCTIPOCA,CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIN
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_CODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_CODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oCODLIN_1_76'),i_cWhere,'',"LINGUE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_CODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_CODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_LUDESCRI = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIN = space(3)
      endif
      this.w_LUDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSQUANTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CODLIS))
          select LSCODLIS,LSDESLIS,LSQUANTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCODLIS_1_77'),i_cWhere,'GSAR_ALI',"ANAGRAFICA LISTINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSQUANTI";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSDESLIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_LSQUANTI = NVL(_Link_.LSQUANTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_LSQUANTI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NODO
  func Link_1_104(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_NODO)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPONODO);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_TIPONODO;
                     ,'CCCODICE',trim(this.w_NODO))
          select CCTIPOCA,CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NODO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NODO) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oNODO_1_104'),i_cWhere,'GSPC_BZZ',"Elenco nodi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPONODO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPONODO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_NODO);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPONODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_TIPONODO;
                       ,'CCCODICE',this.w_NODO)
            select CCTIPOCA,CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NODO = NVL(_Link_.CCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NODO = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCSERIAL1
  func Link_1_125(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANA_CONF_IDX,3]
    i_lTable = "ANA_CONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANA_CONF_IDX,2], .t., this.ANA_CONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANA_CONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCSERIAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANA_CONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCSERIAL like "+cp_ToStrODBC(trim(this.w_CCSERIAL1)+"%");
                   +" and CCTICONF="+cp_ToStrODBC(this.w_FLINIT);
                   +" and CCFLATTI="+cp_ToStrODBC(this.w_CCFLATTI);
                   +" and CCCODART="+cp_ToStrODBC(this.w_ARCODART);
                   +" and CCCODDIS="+cp_ToStrODBC(this.w_DBCODICE);

          i_ret=cp_SQL(i_nConn,"select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTICONF',this.w_FLINIT;
                     ,'CCFLATTI',this.w_CCFLATTI;
                     ,'CCCODART',this.w_ARCODART;
                     ,'CCCODDIS',this.w_DBCODICE;
                     ,'CCSERIAL',trim(this.w_CCSERIAL1))
          select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCSERIAL1)==trim(_Link_.CCSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCSERIAL1) and !this.bDontReportError
            deferred_cp_zoom('ANA_CONF','*','CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL',cp_AbsName(oSource.parent,'oCCSERIAL1_1_125'),i_cWhere,'',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLINIT<>oSource.xKey(1);
           .or. this.w_CCFLATTI<>oSource.xKey(2);
           .or. this.w_ARCODART<>oSource.xKey(3);
           .or. this.w_DBCODICE<>oSource.xKey(4);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCSERIAL="+cp_ToStrODBC(oSource.xKey(5));
                     +" and CCTICONF="+cp_ToStrODBC(this.w_FLINIT);
                     +" and CCFLATTI="+cp_ToStrODBC(this.w_CCFLATTI);
                     +" and CCCODART="+cp_ToStrODBC(this.w_ARCODART);
                     +" and CCCODDIS="+cp_ToStrODBC(this.w_DBCODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTICONF',oSource.xKey(1);
                       ,'CCFLATTI',oSource.xKey(2);
                       ,'CCCODART',oSource.xKey(3);
                       ,'CCCODDIS',oSource.xKey(4);
                       ,'CCSERIAL',oSource.xKey(5))
            select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCSERIAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCSERIAL="+cp_ToStrODBC(this.w_CCSERIAL1);
                   +" and CCTICONF="+cp_ToStrODBC(this.w_FLINIT);
                   +" and CCFLATTI="+cp_ToStrODBC(this.w_CCFLATTI);
                   +" and CCCODART="+cp_ToStrODBC(this.w_ARCODART);
                   +" and CCCODDIS="+cp_ToStrODBC(this.w_DBCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTICONF',this.w_FLINIT;
                       ,'CCFLATTI',this.w_CCFLATTI;
                       ,'CCCODART',this.w_ARCODART;
                       ,'CCCODDIS',this.w_DBCODICE;
                       ,'CCSERIAL',this.w_CCSERIAL1)
            select CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCSERIAL1 = NVL(_Link_.CCSERIAL,space(15))
      this.w_CCDESCR1 = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CCSERIAL1 = space(15)
      endif
      this.w_CCDESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANA_CONF_IDX,2])+'\'+cp_ToStr(_Link_.CCTICONF,1)+'\'+cp_ToStr(_Link_.CCFLATTI,1)+'\'+cp_ToStr(_Link_.CCCODART,1)+'\'+cp_ToStr(_Link_.CCCODDIS,1)+'\'+cp_ToStr(_Link_.CCSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANA_CONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCSERIAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODART_1_4.value==this.w_ARCODART)
      this.oPgFrm.Page1.oPag.oARCODART_1_4.value=this.w_ARCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDBCODICE_1_5.value==this.w_DBCODICE)
      this.oPgFrm.Page1.oPag.oDBCODICE_1_5.value=this.w_DBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDBDESCRI_1_9.value==this.w_DBDESCRI)
      this.oPgFrm.Page1.oPag.oDBDESCRI_1_9.value=this.w_DBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_30.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_30.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCCTIPOCA_1_35.RadioValue()==this.w_CCTIPOCA)
      this.oPgFrm.Page1.oPag.oCCTIPOCA_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_36.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_36.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_37.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_37.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIN_1_76.value==this.w_CODLIN)
      this.oPgFrm.Page1.oPag.oCODLIN_1_76.value=this.w_CODLIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIS_1_77.value==this.w_CODLIS)
      this.oPgFrm.Page1.oPag.oCODLIS_1_77.value=this.w_CODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oLUDESCRI_1_78.value==this.w_LUDESCRI)
      this.oPgFrm.Page1.oPag.oLUDESCRI_1_78.value=this.w_LUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_80.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_80.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oRIC_1_82.RadioValue()==this.w_RIC)
      this.oPgFrm.Page1.oPag.oRIC_1_82.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFA_1_83.RadioValue()==this.w_DEFA)
      this.oPgFrm.Page1.oPag.oDEFA_1_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTINUECHECK_1_84.RadioValue()==this.w_CONTINUECHECK)
      this.oPgFrm.Page1.oPag.oCONTINUECHECK_1_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLINIT_1_86.RadioValue()==this.w_FLINIT)
      this.oPgFrm.Page1.oPag.oFLINIT_1_86.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVERMEM_1_87.RadioValue()==this.w_VERMEM)
      this.oPgFrm.Page1.oPag.oVERMEM_1_87.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRICONF_1_88.RadioValue()==this.w_RICONF)
      this.oPgFrm.Page1.oPag.oRICONF_1_88.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXPSL_1_91.RadioValue()==this.w_EXPSL)
      this.oPgFrm.Page1.oPag.oEXPSL_1_91.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCEN_1_92.value==this.w_PERCEN)
      this.oPgFrm.Page1.oPag.oPERCEN_1_92.value=this.w_PERCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oNODO_1_104.value==this.w_NODO)
      this.oPgFrm.Page1.oPag.oNODO_1_104.value=this.w_NODO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPONODO_1_106.RadioValue()==this.w_TIPONODO)
      this.oPgFrm.Page1.oPag.oTIPONODO_1_106.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGENMOD_1_123.RadioValue()==this.w_GENMOD)
      this.oPgFrm.Page1.oPag.oGENMOD_1_123.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCSERIAL1_1_125.value==this.w_CCSERIAL1)
      this.oPgFrm.Page1.oPag.oCCSERIAL1_1_125.value=this.w_CCSERIAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCR1_1_126.value==this.w_CCDESCR1)
      this.oPgFrm.Page1.oPag.oCCDESCR1_1_126.value=this.w_CCDESCR1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ARCONCAR='S')  and not(empty(.w_ARCODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODART_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale")
          case   not(-99.99<=.w_PERCEN and .w_PERCEN<1000000000)  and (!empty(.w_CODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERCEN_1_92.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire una percentuale compresa tra -99,99 e 999.999.999,99")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ARCODART = this.w_ARCODART
    this.o_DBCODICE = this.w_DBCODICE
    this.o_TIPGEN = this.w_TIPGEN
    this.o_CCTIPOCA = this.w_CCTIPOCA
    this.o_CCCODICE = this.w_CCCODICE
    this.o_RECNO = this.w_RECNO
    this.o_FLINIT = this.w_FLINIT
    return

enddefine

* --- Define pages as container
define class tgscr_kccPag1 as StdContainer
  Width  = 862
  height = 582
  stdWidth  = 862
  stdheight = 582
  resizeXpos=446
  resizeYpos=261
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODART_1_4 as StdField with uid="YLDHOMMPET",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ARCODART", cQueryName = "ARCODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 5631654,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=95, Top=8, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARTIPART", oKey_1_2="this.w_TIPARTF", oKey_2_1="ARCODART", oKey_2_2="this.w_ARCODART"

  func oARCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CCSERIAL1)
        bRes2=.link_1_125('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oARCODART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ART_ICOL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ARTIPART="+cp_ToStrODBC(this.Parent.oContained.w_TIPARTF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ARTIPART="+cp_ToStr(this.Parent.oContained.w_TIPARTF)
    endif
    do cp_zoom with 'ART_ICOL','*','ARTIPART,ARCODART',cp_AbsName(this.parent,'oARCODART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'GSCR_SDS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDBCODICE_1_5 as StdField with uid="KTNHYDCDRV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DBCODICE", cQueryName = "DBCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Distinta base inesistente oppure non riferita ad un articolo configurabile",;
    ToolTipText = "Codice distinta base",;
    HelpContextID = 128582011,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=95, Top=33, InputMask=replicate('X',20), cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DBCODICE"

  func oDBCODICE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CCSERIAL1)
        bRes2=.link_1_125('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oBtn_1_8 as StdButton with uid="LXSIITXWMU",left=802, top=8, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Aggiorna elenco";
    , HelpContextID = 210344345;
    , TabStop=.f., Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSCR_BC4(this.Parent.oContained,"VISUALIZZA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DBCODICE))
      endwith
    endif
  endfunc

  add object oDBDESCRI_1_9 as StdField with uid="JPHUNIBZTR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DBDESCRI", cQueryName = "DBDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione distinta base",;
    HelpContextID = 42996095,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=260, Top=33, InputMask=replicate('X',40)


  add object oBtn_1_13 as StdButton with uid="VQDZAIGZMR",left=748, top=527, width=48,height=45,;
    CpPicture="BMP\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare la distinta base";
    , HelpContextID = 260195350;
    , Caption='G\<enera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSCR_BC4(this.Parent.oContained,"CREADIS_CLOSE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DBCODICE))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="VFOUIPYUGL",left=801, top=527, width=48,height=45,;
    CpPicture="BMP\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 260195350;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="JLNTPCWIXN",left=639, top=527, width=48,height=45,;
    CpPicture="BMP\DrillD.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per stampare l'attuale configurazione";
    , HelpContextID = 260195350;
    , Caption='V\<erifiche';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSCR_BC4(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DBCODICE))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="OIBBVNTDRY",left=693, top=527, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per valorizzazione configurazione";
    , HelpContextID = 260195350;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSCR_BC4(this.Parent.oContained,"STAMPAP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_DBCODICE) and not empty(.w_CODLIS))
      endwith
    endif
  endfunc

  add object oARDESART_1_30 as StdField with uid="YJQMLZHVQA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 9445722,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=260, Top=8, InputMask=replicate('X',40)


  add object Albero as cp_zoomboxCartt with uid="BBULQFBEFC",left=12, top=153, width=552,height=369,;
    caption='Albero',;
   bGlobalFont=.t.,;
    cZoomFile="GSCR_KCC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="CONF_DIS",bQueryOnDblClick=.f.,cMenuFile="",cZoomOnZoom="",bSearchFilterOnClick=.f.,bNoZoomGridShape=.f.,bRetriveAllRows=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 10623750


  add object oCCTIPOCA_1_35 as StdCombo with uid="FPEZQDZUHF",rtseq=25,rtrep=.f.,left=500,top=525,width=61,height=19, enabled=.f.;
    , disabledbackcolor=rgb(255,255,255);
    , ToolTipText = "Tipo Caratteristica";
    , HelpContextID = 241504871;
    , cFormVar="w_CCTIPOCA",RowSource=""+"Caratteristica,"+"Modello,"+"Descrizione", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCCTIPOCA_1_35.RadioValue()
    return(iif(this.value =1,"C",;
    iif(this.value =2,"M",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oCCTIPOCA_1_35.GetRadio()
    this.Parent.oContained.w_CCTIPOCA = this.RadioValue()
    return .t.
  endfunc

  func oCCTIPOCA_1_35.SetRadio()
    this.Parent.oContained.w_CCTIPOCA=trim(this.Parent.oContained.w_CCTIPOCA)
    this.value = ;
      iif(this.Parent.oContained.w_CCTIPOCA=="C",1,;
      iif(this.Parent.oContained.w_CCTIPOCA=="M",2,;
      iif(this.Parent.oContained.w_CCTIPOCA=="D",3,;
      0)))
  endfunc

  func oCCTIPOCA_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CCCODICE)
        bRes2=.link_1_36('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCCCODICE_1_36 as StdField with uid="UKRNGQOPMF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Selezionato",;
    HelpContextID = 128582251,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=117, Top=525, InputMask=replicate('X',5), cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_CCTIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CCCODICE"

  func oCCCODICE_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCCDESCRI_1_37 as StdField with uid="RTDPFZLGJA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42996335,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=180, Top=525, InputMask=replicate('X',40)


  add object oObj_1_60 as cp_runprogram with uid="TSMJVPBBJK",left=6, top=592, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('ROWSEL')",;
    cEvent = "w_Albero row checked",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_61 as cp_runprogram with uid="FSYFHXZHSN",left=6, top=617, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('UNROWSEL')",;
    cEvent = "w_Albero row unchecked",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_63 as cp_runprogram with uid="WIVJTWTLRT",left=6, top=667, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('VISUALIZZA')",;
    cEvent = "Visualizza",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_74 as cp_runprogram with uid="GYPTBCKESM",left=6, top=692, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('SottoAlbero')",;
    cEvent = "SottoAlbero",;
    nPag=1;
    , HelpContextID = 39654042

  add object oCODLIN_1_76 as StdField with uid="NOPARPEWDA",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CODLIN", cQueryName = "CODLIN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice lingua nazionale",;
    HelpContextID = 50913754,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=95, Top=58, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", oKey_1_1="LUCODICE", oKey_1_2="this.w_CODLIN"

  func oCODLIN_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIN_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIN_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oCODLIN_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'',"LINGUE",'',this.parent.oContained
  endproc

  add object oCODLIS_1_77 as StdField with uid="AOPSNHHZEF",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CODLIS", cQueryName = "CODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 235463130,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=95, Top=83, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS"

  func oCODLIS_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIS_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIS_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCODLIS_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"ANAGRAFICA LISTINI",'',this.parent.oContained
  endproc
  proc oCODLIS_1_77.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLIS
     i_obj.ecpSave()
  endproc

  add object oLUDESCRI_1_78 as StdField with uid="XYFVDWVQCE",rtseq=61,rtrep=.f.,;
    cFormVar = "w_LUDESCRI", cQueryName = "LUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43001087,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=158, Top=58, InputMask=replicate('X',30)

  add object oDESLIS_1_80 as StdField with uid="DTSYNJMRFB",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235404234,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=158, Top=83, InputMask=replicate('X',40)

  add object oRIC_1_82 as StdCheck with uid="SPCIJWYDLL",rtseq=63,rtrep=.f.,left=575, top=7, caption="Carica risposte comuni",;
    HelpContextID = 98891498,;
    cFormVar="w_RIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRIC_1_82.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRIC_1_82.GetRadio()
    this.Parent.oContained.w_RIC = this.RadioValue()
    return .t.
  endfunc

  func oRIC_1_82.SetRadio()
    this.Parent.oContained.w_RIC=trim(this.Parent.oContained.w_RIC)
    this.value = ;
      iif(this.Parent.oContained.w_RIC=='S',1,;
      0)
  endfunc

  add object oDEFA_1_83 as StdCheck with uid="UBZNLXSTGC",rtseq=64,rtrep=.f.,left=703, top=133, caption="Carica default",;
    ToolTipText = "Carica scelte default",;
    HelpContextID = 94620618,;
    cFormVar="w_DEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDEFA_1_83.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDEFA_1_83.GetRadio()
    this.Parent.oContained.w_DEFA = this.RadioValue()
    return .t.
  endfunc

  func oDEFA_1_83.SetRadio()
    this.Parent.oContained.w_DEFA=trim(this.Parent.oContained.w_DEFA)
    this.value = ;
      iif(this.Parent.oContained.w_DEFA=='S',1,;
      0)
  endfunc

  add object oCONTINUECHECK_1_84 as StdCheck with uid="MVQTAONFDX",rtseq=65,rtrep=.f.,left=575, top=33, caption="Controllo interattivo",;
    HelpContextID = 32987803,;
    cFormVar="w_CONTINUECHECK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONTINUECHECK_1_84.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCONTINUECHECK_1_84.GetRadio()
    this.Parent.oContained.w_CONTINUECHECK = this.RadioValue()
    return .t.
  endfunc

  func oCONTINUECHECK_1_84.SetRadio()
    this.Parent.oContained.w_CONTINUECHECK=trim(this.Parent.oContained.w_CONTINUECHECK)
    this.value = ;
      iif(this.Parent.oContained.w_CONTINUECHECK=='S',1,;
      0)
  endfunc


  add object oFLINIT_1_86 as StdCombo with uid="OBPBMDCHBU",rtseq=67,rtrep=.f.,left=95,top=134,width=147,height=21;
    , ToolTipText = "Se attivo inizializza il valore della caratteristica a quello di init";
    , HelpContextID = 218535082;
    , cFormVar="w_FLINIT",RowSource=""+"Nessuna,"+"Da legami distinta,"+"Da modello,"+"Da configurazioni precedenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLINIT_1_86.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'L',;
    iif(this.value =3,'M',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oFLINIT_1_86.GetRadio()
    this.Parent.oContained.w_FLINIT = this.RadioValue()
    return .t.
  endfunc

  func oFLINIT_1_86.SetRadio()
    this.Parent.oContained.w_FLINIT=trim(this.Parent.oContained.w_FLINIT)
    this.value = ;
      iif(this.Parent.oContained.w_FLINIT=='N',1,;
      iif(this.Parent.oContained.w_FLINIT=='L',2,;
      iif(this.Parent.oContained.w_FLINIT=='M',3,;
      iif(this.Parent.oContained.w_FLINIT=='C',4,;
      0))))
  endfunc

  func oFLINIT_1_86.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CCSERIAL1)
        bRes2=.link_1_125('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oVERMEM_1_87 as StdCombo with uid="YVACZCVXDL",rtseq=68,rtrep=.f.,left=703,top=83,width=147,height=21;
    , ToolTipText = "Tipologia di verifica uguaglianza";
    , HelpContextID = 71764650;
    , cFormVar="w_VERMEM",RowSource=""+"Legami,"+"Legami + Configurazione,"+"Nessuna verifica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVERMEM_1_87.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oVERMEM_1_87.GetRadio()
    this.Parent.oContained.w_VERMEM = this.RadioValue()
    return .t.
  endfunc

  func oVERMEM_1_87.SetRadio()
    this.Parent.oContained.w_VERMEM=trim(this.Parent.oContained.w_VERMEM)
    this.value = ;
      iif(this.Parent.oContained.w_VERMEM=='N',1,;
      iif(this.Parent.oContained.w_VERMEM=='S',2,;
      iif(this.Parent.oContained.w_VERMEM=='T',3,;
      0)))
  endfunc


  add object oRICONF_1_88 as StdCombo with uid="PRFJBFABJR",rtseq=69,rtrep=.f.,left=703,top=108,width=62,height=21;
    , ToolTipText = "Richiesta conferma configurazione";
    , HelpContextID = 179697386;
    , cFormVar="w_RICONF",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRICONF_1_88.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oRICONF_1_88.GetRadio()
    this.Parent.oContained.w_RICONF = this.RadioValue()
    return .t.
  endfunc

  func oRICONF_1_88.SetRadio()
    this.Parent.oContained.w_RICONF=trim(this.Parent.oContained.w_RICONF)
    this.value = ;
      iif(this.Parent.oContained.w_RICONF=='S',1,;
      iif(this.Parent.oContained.w_RICONF=='N',2,;
      0))
  endfunc

  add object oEXPSL_1_91 as StdCheck with uid="BCTDHTZRVP",rtseq=71,rtrep=.f.,left=210, top=107, caption="Esplodi semilavorati non configurabili",;
    ToolTipText = "Se attivo esplode i semilavorati nella stampa di valorizzazione",;
    HelpContextID = 13703354,;
    cFormVar="w_EXPSL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXPSL_1_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEXPSL_1_91.GetRadio()
    this.Parent.oContained.w_EXPSL = this.RadioValue()
    return .t.
  endfunc

  func oEXPSL_1_91.SetRadio()
    this.Parent.oContained.w_EXPSL=trim(this.Parent.oContained.w_EXPSL)
    this.value = ;
      iif(this.Parent.oContained.w_EXPSL=='S',1,;
      0)
  endfunc

  func oEXPSL_1_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODLIS))
    endwith
   endif
  endfunc

  add object oPERCEN_1_92 as StdField with uid="KXSGXGMNUM",rtseq=72,rtrep=.f.,;
    cFormVar = "w_PERCEN", cQueryName = "PERCEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire una percentuale compresa tra -99,99 e 999.999.999,99",;
    ToolTipText = "Percentuale di ricarico",;
    HelpContextID = 55642890,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=95, Top=108, cSayPict='"999,999,999.99"', cGetPict='"999,999,999.99"'

  func oPERCEN_1_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_CODLIS))
    endwith
   endif
  endfunc

  func oPERCEN_1_92.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (-99.99<=.w_PERCEN and .w_PERCEN<1000000000)
    endwith
    return bRes
  endfunc


  add object oObj_1_94 as cp_runprogram with uid="RHDSBAYVZP",left=6, top=717, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4(cEvent)",;
    cEvent = "w_CCQTA Changed",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_99 as cp_runprogram with uid="HVWWMUFEEY",left=6, top=742, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('CLOSE')",;
    cEvent = "SALVADIS",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_101 as cp_runprogram with uid="DUVYNXCVUE",left=6, top=767, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('BLANK')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 39654042

  add object oNODO_1_104 as StdField with uid="PMFQVQVRKH",rtseq=80,rtrep=.f.,;
    cFormVar = "w_NODO", cQueryName = "NODO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Inserire l'elemento da ricercare",;
    HelpContextID = 93708586,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=117, Top=553, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", cZoomOnZoom="GSPC_BZZ", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_TIPONODO", oKey_2_1="CCCODICE", oKey_2_2="this.w_NODO"

  func oNODO_1_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_104('Part',this)
    endwith
    return bRes
  endfunc

  proc oNODO_1_104.ecpDrop(oSource)
    this.Parent.oContained.link_1_104('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNODO_1_104.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_TIPONODO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_TIPONODO)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oNODO_1_104'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco nodi",'',this.parent.oContained
  endproc
  proc oNODO_1_104.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CCTIPOCA=w_TIPONODO
     i_obj.w_CCCODICE=this.parent.oContained.w_NODO
     i_obj.ecpSave()
  endproc


  add object oBtn_1_105 as StdButton with uid="YPJVIZAYSI",left=175, top=554, width=20,height=20,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la ricerca";
    , HelpContextID = 98985002;
    , ImgSize=16;
  , bGlobalFont=.t.

    proc oBtn_1_105.Click()
      with this.Parent.oContained
        GSCR_BC4(this.Parent.oContained,"TROVANODO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_NODO) And Not Empty(Nvl(.w_TIPONODO,'')))
      endwith
    endif
  endfunc

  add object oTIPONODO_1_106 as StdRadio with uid="WCJWEXWMGJ",rtseq=81,rtrep=.f.,left=207, top=557, width=264,height=20;
    , ToolTipText = "Tipo Caratteristica";
    , cFormVar="w_TIPONODO", ButtonCount=3, bObbl=.f., nPag=1;
    , FntName="Arial", FntSize=8, FntBold=.f., FntItalic=.t., FntUnderline=.f., FntStrikeThru=.f.

    proc oTIPONODO_1_106.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Caratteristica"
      this.Buttons(1).HelpContextID = 239786373
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Caratteristica","Arial",8,"I")*FontMetric(6,"Arial",8,"I"))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Modello"
      this.Buttons(2).HelpContextID = 239786373
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Modello","Arial",8,"I")*FontMetric(6,"Arial",8,"I"))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Descrizione"
      this.Buttons(3).HelpContextID = 239786373
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Descrizione","Arial",8,"I")*FontMetric(6,"Arial",8,"I"))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo Caratteristica")
      StdRadio::init()
    endproc

  func oTIPONODO_1_106.RadioValue()
    return(iif(this.value =1,"C",;
    iif(this.value =2,"M",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oTIPONODO_1_106.GetRadio()
    this.Parent.oContained.w_TIPONODO = this.RadioValue()
    return .t.
  endfunc

  func oTIPONODO_1_106.SetRadio()
    this.Parent.oContained.w_TIPONODO=trim(this.Parent.oContained.w_TIPONODO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPONODO=="C",1,;
      iif(this.Parent.oContained.w_TIPONODO=="M",2,;
      iif(this.Parent.oContained.w_TIPONODO=="D",3,;
      0)))
  endfunc

  func oTIPONODO_1_106.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_NODO)
        bRes2=.link_1_104('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oObj_1_108 as cp_runprogram with uid="LXLPRPMYVC",left=258, top=692, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('UPDSEL')",;
    cEvent = "AggiornaDettaglio",;
    nPag=1;
    , HelpContextID = 39654042


  add object oObj_1_109 as cp_runprogram with uid="WGKXYXYXOE",left=6, top=792, width=231,height=23,;
    caption='GSCR_BC4',;
   bGlobalFont=.t.,;
    prg="GSCR_BC4('CLOSE')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 39654042


  add object TREEVIEW as cp_Treeview with uid="CTQALGUKMG",left=571, top=160, width=279,height=362,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeShowField="",cLeafShowField="",cNodeBmp="treedefa.bmp",cLeafBmp="",nIndent=20,cCursor="_tree_",cShowFields="ALLTRIM(CACODICE)+' - '+ALLTRIM(CADESART)",;
    cEvent = "AggiornaTV",;
    nPag=1;
    , HelpContextID = 78811622


  add object oGENMOD_1_123 as StdCombo with uid="BOFTKQCXIR",rtseq=91,rtrep=.f.,left=703,top=58,width=147,height=22;
    , ToolTipText = "Generazione modello";
    , HelpContextID = 212290458;
    , cFormVar="w_GENMOD",RowSource=""+"Nessuno,"+"Solo modello,"+"Modello + Conf.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGENMOD_1_123.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oGENMOD_1_123.GetRadio()
    this.Parent.oContained.w_GENMOD = this.RadioValue()
    return .t.
  endfunc

  func oGENMOD_1_123.SetRadio()
    this.Parent.oContained.w_GENMOD=trim(this.Parent.oContained.w_GENMOD)
    this.value = ;
      iif(this.Parent.oContained.w_GENMOD=='N',1,;
      iif(this.Parent.oContained.w_GENMOD=='M',2,;
      iif(this.Parent.oContained.w_GENMOD=='E',3,;
      0)))
  endfunc

  add object oCCSERIAL1_1_125 as StdField with uid="WDMVLZXHXN",rtseq=92,rtrep=.f.,;
    cFormVar = "w_CCSERIAL1", cQueryName = "CCSERIAL1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice � obsoleto, senza distinta o il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice articolo (associato a distinta base) di inizio selezione",;
    HelpContextID = 125762174,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=246, Top=134, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ANA_CONF", oKey_1_1="CCTICONF", oKey_1_2="this.w_FLINIT", oKey_2_1="CCFLATTI", oKey_2_2="this.w_CCFLATTI", oKey_3_1="CCCODART", oKey_3_2="this.w_ARCODART", oKey_4_1="CCCODDIS", oKey_4_2="this.w_DBCODICE", oKey_5_1="CCSERIAL", oKey_5_2="this.w_CCSERIAL1"

  func oCCSERIAL1_1_125.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLINIT $ 'C-M')
    endwith
   endif
  endfunc

  func oCCSERIAL1_1_125.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_125('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCSERIAL1_1_125.ecpDrop(oSource)
    this.Parent.oContained.link_1_125('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCSERIAL1_1_125.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANA_CONF_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTICONF="+cp_ToStrODBC(this.Parent.oContained.w_FLINIT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCFLATTI="+cp_ToStrODBC(this.Parent.oContained.w_CCFLATTI)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODART="+cp_ToStrODBC(this.Parent.oContained.w_ARCODART)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODDIS="+cp_ToStrODBC(this.Parent.oContained.w_DBCODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTICONF="+cp_ToStr(this.Parent.oContained.w_FLINIT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCFLATTI="+cp_ToStr(this.Parent.oContained.w_CCFLATTI)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODART="+cp_ToStr(this.Parent.oContained.w_ARCODART)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODDIS="+cp_ToStr(this.Parent.oContained.w_DBCODICE)
    endif
    do cp_zoom with 'ANA_CONF','*','CCTICONF,CCFLATTI,CCCODART,CCCODDIS,CCSERIAL',cp_AbsName(this.parent,'oCCSERIAL1_1_125'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'',this.parent.oContained
  endproc

  add object oCCDESCR1_1_126 as StdField with uid="IATUIMUDTE",rtseq=93,rtrep=.f.,;
    cFormVar = "w_CCDESCR1", cQueryName = "CCDESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42996311,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=375, Top=134, InputMask=replicate('X',40)

  add object oStr_1_10 as StdString with uid="HLJSZODPGH",Visible=.t., Left=3, Top=33,;
    Alignment=1, Width=90, Height=18,;
    Caption="Distinta base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="AZAYVNOACS",Visible=.t., Left=18, Top=11,;
    Alignment=1, Width=75, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="TVCCDMIWHT",Visible=.t., Left=55, Top=527,;
    Alignment=1, Width=60, Height=18,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="ZEBJDENDJY",Visible=.t., Left=469, Top=525,;
    Alignment=1, Width=27, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="GDXOJGVJEF",Visible=.t., Left=30, Top=58,;
    Alignment=1, Width=63, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="EDYXGLKDZQ",Visible=.t., Left=36, Top=86,;
    Alignment=1, Width=57, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="AMLZAQOUTM",Visible=.t., Left=6, Top=110,;
    Alignment=1, Width=87, Height=18,;
    Caption="% di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="OLTRTNHDMB",Visible=.t., Left=10, Top=555,;
    Alignment=1, Width=105, Height=15,;
    Caption="Ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="NYMOCNZRGI",Visible=.t., Left=570, Top=111,;
    Alignment=1, Width=128, Height=18,;
    Caption="Richieste conf.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="NFWBIAXCTZ",Visible=.t., Left=546, Top=84,;
    Alignment=1, Width=152, Height=18,;
    Caption="Verifica uguaglianza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_121 as StdString with uid="QALJSGXKQK",Visible=.t., Left=6, Top=136,;
    Alignment=1, Width=87, Height=18,;
    Caption="Inizializza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="VASNWWFOJX",Visible=.t., Left=565, Top=60,;
    Alignment=1, Width=133, Height=18,;
    Caption="Generazione modello:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscr_kcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscr_kcc
* ====================================================================================================
*                          DEFINIZIONE CLASSI GESTIONE TREEVIEW
* ====================================================================================================
Define Class cp_zoomboxCartt as cp_zoombox
nReco=0

      Proc grd.Destroy
        This.Column1.RemoveObject("Container1")
        This.Column1.RemoveObject("ContainerL")
        This.Column1.RemoveObject("ContainerF")
        This.Column1.RemoveObject("ContainerD")
        This.Column1.RemoveObject("ContainerM")
      	Dodefault()
     Endproc
     
      Proc grd.Refresh
        This.enabled=.f.
        This.Column1.Container1.Enabled=.F.
        This.Column1.ContainerL.Enabled=.F.
        This.Column1.ContainerF.Enabled=.F.
        This.Column1.ContainerD.Enabled=.F.
        This.Column1.ContainerM.Enabled=.F.
        DoDefault()
        This.column1.dynamicCurrentControl=This.column1.dynamicCurrentControl
        This.Column1.Container1.Enabled=.T.
        This.Column1.ContainerL.Enabled=.T.
        This.Column1.ContainerF.Enabled=.T.
        This.Column1.ContainerD.Enabled=.T.
        This.Column1.ContainerM.Enabled=.T.
        This.enabled=.t.
      EndPRoc

   Proc grd.nodeClick
      LOCAL livtreev
      LOCAL lvlkeytv, cExpanded
      m.cExpanded = ' '
      SELECT (this.RecordSource)
      ThisForm.LockScreen = .T.
      IF (CCHDRLVL="S" OR CCHEADER="S") AND NOT EMPTY(expanded)
        ThisForm.LockScreen = .T.
        m.lvlkeytv = TRIM(LVLKEYTV) && EVALUATE(KEY())
        IF expanded="S"
          replace expanded WITH "N"
          SKIP 
          REPLACE SHOWNODE WITH "N" while m.lvlkeytv $ TRIM(lvlkeytv)
        ELSE
          SELECT (this.recordSource)
          SET FILTER TO 
          replace expanded WITH "S"
          m.livtreev = livtreev
          SKIP
          SCAN REST WHILE  TRIM(m.lvlkeytv) $ TRIM(LVLKEYTV)
            IF livtreev = m.livtreev + 1
              replace SHOWNODE with 'S'
              cExpanded = Expanded
            ELSE
              IF m.cExpanded="S"
                replace SHOWNODE with 'S'
              ENDIF
            ENDIF
            SELECT (this.recordSource)
          ENDSCAN          
        ENDIF
        SELECT (This.RecordSource)
        SEEK m.lvlkeytv
        SET FILTER TO SHOWNODE="S"
        This.Refresh()
        This.column1.dynamicCurrentControl=This.column1.dynamicCurrentControl
        ThisForm.LockScreen = .F.
      ENDIF   
   EndProc


  Proc grd.dynamic
      local l_ContainerG, l_ContainerG, l_oLabbelGrp, l_lnBackColor

    If type("this.column1")="O"
        l_ContainerG = Evaluate(this.column1.dynamicCurrentControl)
        IF  m.l_ContainerG $ 'ContainerL-Container1'
          l_oLabbelGrp = Evaluate("this.column1."+ Evaluate(this.column1.dynamicCurrentControl)+".oCntBorder.oLblGrp")
          l_lnBackColor =  m.l_oLabbelGrp.Parent.Parent.lnBackColor
          IF Recno(this.RecordSource)=this.Parent.nReco
              m.l_oLabbelGrp.disabledBackColor = this.SelectedItemBackColor   && Rgb(10,36,106)
              m.l_oLabbelGrp.DisabledForeColor = This.SelectedItemForeColor  && Rgb(255,255,255)		
          else
            m.l_oLabbelGrp.disabledBackColor=Rgb(255,255,255)
            m.l_oLabbelGrp.DisabledForeColor = m.l_lnBackColor
          endif
        endif

        IF  m.l_ContainerG $ 'ContainerF-ContainerM-ContainerD'
          l_oLabbelGrp = Evaluate("this.column1."+ Evaluate(this.column1.dynamicCurrentControl)+".oCntBorder")
          l_lnBackColor =  m.l_oLabbelGrp.Parent.lnBackColor

          IF Recno(this.RecordSource)=this.Parent.nReco 
              m.l_oLabbelGrp.BackColor = this.SelectedItemBackColor   && Rgb(10,36,106)
              m.l_oLabbelGrp.BorderColor = Rgb(179,7,27) && this.SelectedItemBackColor
              * m.l_oLabbelGrp.DisabledForeColor = This.SelectedItemForeColor  && Rgb(255,255,255)		
          else
            IF (cctipoca $ 'C-D' and xchk=1) or cctipoca ='M'
               m.l_oLabbelGrp.BackColor = Rgb(255,255,198)
               m.l_oLabbelGrp.BorderColor = Rgb(255,255,198)  && Rgb(179,7,27) && this.SelectedItemBackColor
            ELSE
               m.l_oLabbelGrp.BackColor=Rgb(255,255,255)
               m.l_oLabbelGrp.BorderColor=Rgb(255,255,255)
              * m.l_oLabbelGrp.DisabledForeColor = m.l_lnBackColor
            ENDIF
          endif

          
        endif


    endif		
    RETURN RGB(255,255,255)  && IIF(CCHDRLVL="S" OR CCHEADER="S",  RGB(255,255,255) , IIF(CCTIPOCA $ 'C-D-M' , IIF( XCHK=1, RGB(255,255,128) , RGB(255,255,255)),  RGB(255,255,255) ))
  endproc
   
   
EndDefine

Define Class ConfCheckBox As Checkbox
nFirstSelect=0
nShift=0
nLastValue=0
BackStyle=0
oContained=.null.
oZoom=.NULL.
Proc Init
dodefault()
This.oContained = ThisForm && This.Parent.Parent.Parent.Parent.Parent.Parent.oContained
This.oZoom = This.oContained.w_Albero &&  This.Parent.Parent.Parent.Parent.Parent
	Proc When()
		*if lastkey() = 32 or lastkey() = 13
		*   this.click()
		*  endif
		Return(This.Value<>2)
	Proc Click()
		Local c_mCursor
		If Type('This.oContained')='O'
			This.nLastValue=This.Value
			If This.Value=1
				* --- lasciato per compatibilita'
				This.oContained.NotifyEvent(This.oZoom.Name+' row checked')
				* --- forma corretta
				This.oContained.NotifyEvent('w_'+Lower(This.oZoom.Name)+' row checked')
			Else
				* --- lasciato per compatibilita'
				This.oContained.NotifyEvent(This.oZoom.Name+' row unchecked')
				* --- forma corretta
				This.oContained.NotifyEvent('w_'+Lower(This.oZoom.Name)+' row unchecked')
			Endif
			c_mCursor=This.oZoom.cCursor
			Select (c_mCursor)
			This.nFirstSelect=Recno()
		Endif
  EndPRoc

  Procedure Destroy
    This.oZoom = (NULL)
    This.oContained = (NULL)
  EndPRoc

Enddefine

Define Class clsImage_PlusMinus As Container
    * --- Classe immagine per zoom
    BorderWidth=1
    BackStyle = 1
	Width = 18
	Height=18
	Visible=.t.
	cBmp1=""
	cBmp2=""
	nThemeColor=0
	nThemeColorLevel=0

	Add Object oImg_PlusMinus As Image With Top=1, Left=1, width=16, Height=16, Visible=.t.
	
	Procedure Init
		This.cBmp1 = ThisForm.cBmp1
		This.cBmp2 = ThisForm.cBmp2
		This.nThemeColor = ThisForm.nThemeColor
		This.nThemeColorLevel = ThisForm.nThemeColorLevel
		This.oImg_PlusMinus.PictureVal = This.cBmp2
	EndPRoc

  Proc MouseDown
    LPARAMETERS nButton, nShift, nXCoord, nYCoord
    IF nButton=1
      this.Parent.Parent.Parent.Parent.nodeClick
    ENDIF
  EndPRoc

  Proc oImg_PlusMinus.MouseDown
    LPARAMETERS nButton, nShift, nXCoord, nYCoord
    IF nButton=1
      this.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    ENDIF
  EndPRoc

  Proc BackStyle_Access
      This.BackColor = IIF(CCHDRLVL="S", This.nThemeColorLevel, IIF(CCHEADER="S", This.nThemeColor, This.BackColor))
      This.BorderColor = IIF(CCHDRLVL="S", This.nThemeColorLevel, IIF(CCHEADER="S", This.nThemeColor, This.BackColor))
      This.oImg_PlusMinus.PictureVal = IIF(EXPANDED='S',This.cBmp2, This.cBmp1)
      return This.BackStyle
  endproc

  Proc Destroy
    This.cBmp1=""
    This.cBmp2=""
    This.nThemeColor=0
    This.nThemeColorLevel=0  
  endproc
 

Enddefine

Define Class clsImage_FrecciaDX As Container
    * --- Classe immagine per zoom
	BorderWidth = 0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.
	cBmp3=""
	nThemeColor=0

	Add Object oImg_FrecciaDX As Image With Top=1, Left=1, width=16, Height=16, Visible=.t.
	
	Procedure Init
		This.nThemeColor = ThisForm.nThemeColor		
		This.cBmp3 = ThisForm.cBmp3
		This.oImg_FrecciaDX.PictureVal = This.cBmp3
	EndPRoc

  Proc BackStyle_Access
      This.oImg_FrecciaDX.PictureVal = This.cBmp3
      Return This.BackStyle
  endproc

  Proc Destroy
    This.cBmp3=""
    This.nThemeColor=0
  endproc

Enddefine


Define Class ConfTextBox As TextBox
Themes=.f.
BackStyle=0
BorderStyle=0
SpecialEffect=1
Margin=0
enabled=.f.
nThemeColor=0
cSayPict=''
cGetPict=''
bInInput=.F.

  Proc Init
    This.DisabledBackColor = This.BackColor
    This.DisabledForeColor = This.ForeColor
    This.nThemeColor = ThisForm.nThemeColor
    
		If Empty(This.cSayPict) And !Empty(This.cGetPict)
			This.cSayPict=This.cGetPict
		Endif
		If !Empty(This.cSayPict) And Empty(This.cGetPict)
			This.cGetPict=This.cSayPict
		Endif
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
  EndProc

	  Proc DblClick
	    This.Parent.DblClick
      * Parent.nodeClick  
	  EndProc

 * Proc DblClick
 *     this.parent.parent.parent.Parent.nodeClick
 * EndProc

EndDefine


Define Class ConfParamTextBox As ConfTextBox
oContained=.null.
OldValue=0
CurrValue = 0
MinValue=0
MaxValue=0
enabled=.T.    

  Proc Init
    This.cSayPict=['@Z ' + '999,999,999.999']
    This.cGetPict=['@Z ' +'999999999.999']
    Dodefault()
    This.oContained = ThisForm && This.Parent.Parent.Parent.Parent.Parent.Parent.oContained
  EndProc
  
  Proc GotFocus()
    This.OldValue = Eval(This.ControlSource)
    This.MinValue=ccvalmin
    This.MaxValue=ccvalmax

		This.bInInput=.T.
		* --- setta la picture di "get"
		If !Empty(This.cGetPict)
			i_im=Trim(cp_GetMask(This.cGetPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cGetPict)
		Endif

  EndProc

	Proc LostFocus()
		Local i_im
		If !Empty(This.cSayPict)
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
		This.bInInput=.F.
  EndProc

  Proc When()
  
  EndProc

  Proc Valid()
    local i_bUpd, i_nRes, i_bErr
    
    This.CurrValue = This.Value && Eval(This.ControlSource)
    
    i_bErr = This.CurrValue < This.MinValue or This.CurrValue > This.MaxValue

    If i_bErr
       This.Value = This.OldValue
    endif

    i_bUpd = This.OldValue<>This.CurrValue 
    i_nRes=Iif(i_bUpd,0,1)
    
    If Not(i_bErr) And i_bUpd
        i_nRes=1
        * --- lasciato per compatibilita'
        * This.oContained.w_CCQTA = Eval(This.ControlSource)
        This.oContained.NotifyEvent('w_CCQTA Changed')
    EndIf
        
    Return(i_nRes)
  EndProc

	Proc ProgrammaticChange()
		* --- questa routine gestisce le picture dinamiche
		Local i_im
		If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
			i_im=Trim(cp_GetMask(This.cSayPict))
			If !Empty(i_im)
				This.InputMask=i_im
			Endif
			This.Format='K'+cp_GetFormat(This.cSayPict)
		Endif
  EndProc

	Procedure KeyPress(nKeyCode, nShiftAltCtrl)
		Local pt
		If Vartype(This.Value)='N'
			pt=Set('point')
			Do Case
				Case nKeyCode=46 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=44 And nKeyCode<>Asc(pt)
					* Sostituzione carattere '.'
					Nodefault
					Keyboard pt
				Case nKeyCode=9
					* uscita con il tasto return, in modo da sovrascrivere sempre il contenuto
					Nodefault
					Keyboar '{ENTER}'
			Endcase
		Endif
	Endproc

  Procedure Destroy
    This.oContained = .NULL.
  EndPRoc

EndDefine

* Define Class clsCntBorder As Container
	* --- Classe immagine per zoom
*	BorderWidth=0
*	BackStyle = 0
*	Width = 18
*	Height=18
*	Visible=.t.

*	Add Object oImageDx as clsImage_FrecciaDX with left=1, top=3, width=10, height=10, BackStyle=0
  
* Enddefine

Define Class clsCntBorder As Container
	* --- Classe immagine per zoom
	BorderWidth=0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.

	* Add Object oImageDx as clsImage_FrecciaDX with left=1, top=3, width=10, height=10, BackStyle=0
	Add Object oImageDx as ConfTextBox with left=1, top=3, width=10, height=10
	
	Proc oImageDx.Init
		DoDefault()
		This.DisabledForeColor = This.nThemeColor
		This.FontName = "Wingdings"
		This.FontSize = 8
		This.FontBold = .T.
		This.Value = "�"
	  endproc
		

Enddefine


Define Class clsBorder As Shape
	* --- Classe Shape per catturare gli eventi
	BorderStyle=0    
	BorderWidth=0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.
	Enabled=.t.

	Proc DblClick
	    This.Parent.Parent.Parent.Parent.nodeClick  
	EndProc

	Proc RightClick
	    This.Parent.Parent.Parent.Parent.RightClick
	EndProc

Enddefine

Define Class HdrlVLGroup As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	clblControlSource="DESCRITV"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cImgControlSource="EXPANDED"	&& Mi dice se il nodo � aperto oppure chiuso
	cHdrControlSource="CCHDRLVL"	&& Mi dice se sono un header
	lnBackColor = Rgb(179,7,27)

	Add Object oCntBorder as clsCntBorder
	
	Procedure Init
		local l_clblControlSource, l_Column

		This.oCntBorder.AddObject("oImgPM", "clsImage_PlusMinus")
		This.oCntBorder.AddObject("oLblGrp", "ConfTextBox")
		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")

		This.cCursor = This.Parent.Parent.Parent.cCursor
		m.l_clblControlSource = This.cCursor + "." + This.clblControlSource

		This.oCntBorder.oLblGrp.ControlSource = This.clblControlSource && m.l_clblControlSource

		This.ResizeContainer()

		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = Thisform.nThemeColorLevel
    
		* This.oCntBorder.Visible=.t.
		This.oCntBorder.oImgPM.Visible=.t.
		This.oCntBorder.oLblGrp.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.

		* l_Column = This.Parent
		BINDEVENT(This.Parent,"Width",This,"ResizeContainer", 1)
	EndProc
  
	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
		This.Move(0, 0, max(0, This.Parent.Width), max(0, This.Parent.Parent.RowHeight))
		This.Draw()
		
		This.oCntBorder.Move(0, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), max(0, This.Parent.Parent.RowHeight - 1))
		This.oCntBorder.Draw()		
		
		This.oCntBorder.oImgPM.Move(0, 0, 18, 18)
    
		This.oCntBorder.oLblGrp.BackStyle=0
		This.oCntBorder.oLblGrp.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblGrp.DisabledForeColor = ThisForm.nThemeColorLevel
		This.oCntBorder.oLblGrp.Move(18 , 1, max(0, This.Parent.Width - This.oCntBorder.oLblGrp.Left - 2), max(0, This.Height - This.oCntBorder.oLblGrp.Top - 2))
		This.oCntBorder.oLblGrp.FontBold=.t.

		This.oCntBorder.oShapeEvent.Move(18 , 0, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top)
	EndProc
  
  
	Proc DblClick
	    This.Parent.Parent.nodeClick  
	EndProc

	  Proc Destroy
      UNBINDEVENTS(This.Parent,"Width",This,"ResizeContainer")
	  endproc
	  
EndDefine


Define Class HdrRowGroup As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	clblControlSource="DESCRITV"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cImgControlSource="EXPANDED"	&& Mi dice se il nodo � aperto oppure chiuso
	cHdrControlSource="CCHEADER"	&& Mi dice se sono un header
	lnBackColor = RGB(0,146,201)
	lnBackColorSel = RGB(137,89,171)
	
	Add Object oCntBorder as clsCntBorder

	Procedure Init
		local l_clblControlSource
		
		This.oCntBorder.AddObject("oImgPM", "clsImage_PlusMinus")
		This.oCntBorder.AddObject("oLblGrp", "ConfTextBox")
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")

		This.cCursor = This.Parent.Parent.Parent.cCursor
		m.l_clblControlSource = This.cCursor + "." + This.clblControlSource	
		
		This.oCntBorder.oLblGrp.ControlSource = This.clblControlSource && m.l_clblControlSource
  
		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		This.oCntBorder.BackColor = This.Parent.BackColor
		This.oCntBorder.BorderColor = Thisform.nThemeColor

		* This.oCntBorder.Visible=.t.
		This.oCntBorder.oImgPM.Visible=.t.
		This.oCntBorder.oLblGrp.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.

		* l_Column = This.Parent

		BINDEVENT(This.Parent,"Width",This,"ResizeContainer", 1)
		
	EndProc
  
	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
		This.Left=0
		This.Top=0
		This.Width = This.Parent.Width
		This.Height = This.Parent.Parent.RowHeight
		This.Draw()
    
		This.oCntBorder.left=18
		This.oCntBorder.Top=0
		This.oCntBorder.Width = max(0, This.Parent.Width - This.oCntBorder.Left - 1)
		This.oCntBorder.Height = This.Parent.Parent.RowHeight - 1
		This.oCntBorder.Draw()
		
		This.oCntBorder.oImgPM.Move(0, 0, 18, This.Height)
		This.oCntBorder.oImgPM.BackColor = This.lnBackColor
		This.oCntBorder.oImgPM.BorderColor = This.lnBackColor		
  
		* This.oCntBorder.oLblGrp.BackStyle=0
		This.oCntBorder.oLblGrp.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblGrp.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		
		This.oCntBorder.oLblGrp.Move(18, 1, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top - 2)
		This.oCntBorder.oLblGrp.FontBold=.t.

		This.oCntBorder.oShapeEvent.Move(18 , 0, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top )
	EndProc

	  Proc DblClick
	    This.Parent.Parent.nodeClick  
	  EndProc 
	  
	  Proc Destroy
      UNBINDEVENTS(This.Parent,"Width",This,"ResizeContainer")
	  endproc


EndDefine

Define Class DtlCarFisse As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	cChkControlSource="XCHK"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cValueControlSource="CPROWORD"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	lnBackColor = RGB(0,146,201)
	
	Add Object oCntBorder as clsCntBorder
  
	* Add Object oCntBlank1 As Container       With Top=0, Left=0, width=18, Heigth=16, Visible=.t.
	* Add Object oCntBlank2 As Container       With Top=0, Left=18, width=18, Heigth=16, Visible=.t.
	* Add Object oChkCar as ConfCheckBox       With Top=0, Left=37, Width=26,  Visible=.t., Caption=""
	* Add Object oLblValue as ConfTextBox      With Top=0, Left=64, Width=50,  Visible=.t.
	* Add Object oLblDescri as ConfTextBox     With Top=0, Left=115, Width=350, Visible=.t.
    
	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource
		
		This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oLblValue", "ConfTextBox")		
		This.oCntBorder.AddObject("oLblDescri", "ConfTextBox")		
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")

		This.cCursor = This.Parent.Parent.Parent.cCursor
    
		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = This.cChkControlSource &&  m.l_cChkControlSource
		This.oCntBorder.oLblValue.ControlSource = This.cValueControlSource && m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ This.cDettagControlSource + ')'

		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		This.oCntBorder.BackColor = This.Parent.BackColor
		This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oChkCar.Visible=.t.
		This.oCntBorder.oLblValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.

    
		local l_Column, l_Grid
		
		* l_Column = This.Parent
		* l_Grid = This.Parent.Parent
		BINDEVENT(This.Parent,"Width",This,"ResizeContainer", 1)

	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc RecordSource
		This.cCursor = This.Parent.Parent.Parent.cCursor
    
		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = This.cChkControlSource &&  m.l_cChkControlSource
		This.oCntBorder.oLblValue.ControlSource = This.cValueControlSource && m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ This.cDettagControlSource + ')'
	EndPRoc
  
	Proc ResizeContainer		
		*This.Left
		*This.Top=0
		*This.Width = This.Parent.Width
		*This.Height = This.Parent.Parent.RowHeight+1
		This.Move(0, 0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.Draw()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()
    
		This.oCntBorder.oChkCar.BackStyle=0
		This.oCntBorder.oChkCar.Move(18, 1, 26)
		This.oCntBorder.oChkCar.Caption=""

		This.oCntBorder.oLblValue.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblValue.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		
		This.oCntBorder.oLblValue.Move(35, 1, 50, This.oCntBorder.Height - This.oCntBorder.oLblValue.Top - 1)
		This.oCntBorder.oLblValue.InputMask = "999"
		This.oCntBorder.oLblValue.Alignment=0    
    
		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		This.oCntBorder.oLblDescri.Move(86, 1, MAX(0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1), This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
	EndProc

	  Proc Destroy
      UNBINDEVENTS(This.Parent,"Width",This,"ResizeContainer")
	  endproc

EndDefine


Define Class DtlCarParam As Container
	Left=0
	Top=0
	Width = 500
	Height = 19
	BackStyle=0
	BorderWidth=0
	cCursor=""
	cValueControlSource="CPROWORD"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	cValMinControlSource="CCVALMIN"	&& Mi dice se il nodo � aperto oppure chiuso
	cValMaxControlSource="CCVALMAX"	&& Mi dice se il nodo � aperto oppure chiuso

	lnBackColor = RGB(0,146,201)
	
	Add Object oCntBorder as clsCntBorder	

	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource
		local L_cValMinControlSource, l_cValMaxControlSource

		* This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oTextValue", "ConfParamTextBox")		
		This.oCntBorder.AddObject("oLblDescri", "ConfTextBox")			
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")
		
		This.cCursor = This.Parent.Parent.Parent.cCursor

		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		L_cValMinControlSource = This.cCursor + "." + This.cValMinControlSource
		l_cValMaxControlSource = This.cCursor + "." + This.cValMaxControlSource
	  
		This.oCntBorder.oTextValue.ControlSource = m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ m.l_cDettagControlSource + ')'

		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oTextValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.
		
		* l_Column = This.Parent

		BINDEVENT(This.Parent,"Width",This,"ResizeContainer", 1)
	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer		
		*This.Left
		*This.Top=0
		*This.Width = This.Parent.Width
		*This.Height = This.Parent.Parent.RowHeight+1
		This.Move(0,0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.BackColor = This.Parent.Parent.BackColor
		This.BorderColor = This.Parent.Parent.BackColor		
		This.Draw()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()
    
		This.oCntBorder.oTextValue.Enabled = .T.
		This.oCntBorder.oTextValue.BorderStyle = 1
		This.oCntBorder.oTextValue.BorderColor = This.lnBackColor
		This.oCntBorder.oTextValue.Move(18, 1, 101, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oTextValue.BackColor = Rgb(255,255,255)
		This.oCntBorder.oTextValue.ForeColor = This.lnBackColor
		* This.oCntBorder.oTextValue.Alignment=0    
		
		This.oCntBorder.oLblDescri.Move(125, 1, MAX(0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1), This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255)
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor
		This.oCntBorder.oShapeEvent.Move(125, 1, MAX(0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1), This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)

	EndProc

	  Proc Destroy
      UNBINDEVENTS(This.Parent,"Width",This,"ResizeContainer")
	  endproc

EndDefine

Define Class MyStdButton as CPCommandButton   
	ImgSize=10
	CpPicture="Caratter.ico"
	Caption=""

	Procedure Click
		ThisForm.NotifyEvent("OPENCARDESC")
	endproc

EndDefine

Define Class ConfEditBox As EditBox
Themes=.f.
BackStyle=0
BorderStyle=0
SpecialEffect=1
Margin=0
enabled=.f.
ScrollBars=0

  Proc Init
    This.DisabledBackColor = This.BackColor
    This.DisabledForeColor = This.ForeColor
  EndProc

  Proc DblClick
    This.Parent.DblClick
  EndProc

EndDefine

Define Class DtlCarDescr As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	lnBackColor = RGB(0,146,201)
	
	cChkControlSource="XCHK"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	
	Add Object oCntBorder as clsCntBorder
	
	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource


		This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oBtnValue", "MyStdButton")		
		This.oCntBorder.AddObject("oLblDescri", "ConfEditBox")		
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")		
		
		This.cCursor = This.Parent.Parent.Parent.cCursor

		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = m.l_cChkControlSource
		This.oCntBorder.oLblDescri.ControlSource = m.l_cDettagControlSource
		* --- 'cp_MemoFirstLine('+ m.l_cDettagControlSource + ')'
    
		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oChkCar.Visible=.t.
		This.oCntBorder.oBtnValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.
		
		* l_Column = This.Parent

		BINDEVENT(This.Parent,"Width",This,"ResizeContainer", 1)
	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
 		This.Move(0,0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.BackColor = This.Parent.Parent.BackColor
		This.BorderColor = This.Parent.Parent.BackColor
		This.Draw()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()

		This.oCntBorder.oChkCar.BackStyle=0
		This.oCntBorder.oChkCar.Move(18, 1, 18)
		This.oCntBorder.oChkCar.Caption=""

		This.oCntBorder.oBtnValue.Move(37, 1, 16 , 16) && This.Parent.Parent.RowHeight)

		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		This.oCntBorder.oLblDescri.Move(56, 0, MAX(0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1), This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oShapeEvent.Move(56, 0, MAX(0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1), This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
	
	EndProc

	  Proc Destroy
      UNBINDEVENTS(This.Parent,"Width",This,"ResizeContainer")
	  endproc
EndDefine

* --- Fine Area Manuale
