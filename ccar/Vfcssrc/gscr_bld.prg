* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bld                                                        *
*              Gestione conf legami                                            *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-09                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bld",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_bld as StdBatch
  * --- Local variables
  w_OPER = space(10)
  GSDS_MLC = .NULL.
  GSDS_MLD = .NULL.
  GSDS_ALP = .NULL.
  w_NROW = 0
  * --- WorkFile variables
  CONF_CAR_idx=0
  CONFDART_idx=0
  CONFDCAR_idx=0
  MODE_ART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Detail Caratt. di Conf. Articolo (GSDS_MLC)
    this.GSDS_MLC = this.oParentObject
    this.GSDS_MLD = this.GSDS_MLC.GSDS_MLD
    this.GSDS_ALP = this.GSDS_MLC.GSDS_ALP
    cCurMLC = this.GSDS_MLC.cTrsname
    cCurMLD = this.GSDS_MLD.cTrsname
    do case
      case this.oParentObject.w_CCTIPOCA = "C"
        * --- Caratteristiche
        if not empty(cCurMLD) and used(cCurMLD)
          * --- Riempie il dettaglio delle caratteristiche
          Select (cCurMLD) 
 gopos = RecNo()+1 
 nAbsRow = this.GSDS_MLD.oPgFrm.Page1.oPag.oBody.nAbsRow 
 nRelRow = this.GSDS_MLD.oPgFrm.Page1.oPag.oBody.nRelRow 
 delete all
          this.w_NROW = 0
          * --- Select from CONFDART
          i_nConn=i_TableProp[this.CONFDART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONFDART ";
                +" where CCCODART="+cp_ToStrODBC(this.oParentObject.w_CCCODART)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+" ";
                 ,"_Curs_CONFDART")
          else
            select * from (i_cTable);
             where CCCODART=this.oParentObject.w_CCCODART AND CCCODICE=this.oParentObject.w_CCCODICE ;
              into cursor _Curs_CONFDART
          endif
          if used('_Curs_CONFDART')
            select _Curs_CONFDART
            locate for 1=1
            do while not(eof())
            this.w_NROW = this.w_NROW + 1
            this.GSDS_MLD.InitRow()     
            this.GSDS_MLD.w_CPROWORD = _Curs_CONFDART.CPROWORD
            this.GSDS_MLD.w_CCDETTAG = _Curs_CONFDART.CCDETTAG
            this.GSDS_MLD.w_CC__DEFA = _Curs_CONFDART.CC__DEFA
            this.GSDS_MLD.w_CCOPERAT = _Curs_CONFDART.CCOPERAT
            this.GSDS_MLD.w_CC_COEFF = _Curs_CONFDART.CC_COEFF
            this.GSDS_MLD.w_CCDESSUP = NVL(_Curs_CONFDART.CCDESSUP,"")
            this.GSDS_MLD.w_CCDATINI = I_DATSYS
            this.GSDS_MLD.w_CCDATFIN = cp_CharToDate("31-12-2099")
            * --- Carica il Temporaneo dei Dati e skippa al record successivo
            this.GSDS_MLD.mCalc(.t.)     
            this.GSDS_MLD.TrsFromWork()     
            this.GSDS_MLD.ChildrenChangeRow()     
              select _Curs_CONFDART
              continue
            enddo
            use
          endif
          if this.w_NROW=0
            * --- Inserisce il dettaglio dalla Caratteristica generica
            * --- Select from CONFDCAR
            i_nConn=i_TableProp[this.CONFDCAR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONFDCAR_idx,2],.t.,this.CONFDCAR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONFDCAR ";
                  +" where CCCODICE="+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+"";
                   ,"_Curs_CONFDCAR")
            else
              select * from (i_cTable);
               where CCCODICE=this.oParentObject.w_CCCODICE;
                into cursor _Curs_CONFDCAR
            endif
            if used('_Curs_CONFDCAR')
              select _Curs_CONFDCAR
              locate for 1=1
              do while not(eof())
              this.GSDS_MLD.InitRow()     
              this.GSDS_MLD.w_CPROWORD = _Curs_CONFDCAR.CPROWORD
              this.GSDS_MLD.w_CCDETTAG = _Curs_CONFDCAR.CCDETTAG
              this.GSDS_MLD.w_CCDESSUP = NVL(_Curs_CONFDCAR.CCDESSUP,"")
              this.GSDS_MLD.w_CCDATINI = I_DATSYS
              this.GSDS_MLD.w_CCDATFIN = cp_CharToDate("31-12-2099")
              * --- Carica il Temporaneo dei Dati e skippa al record successivo
              this.GSDS_MLD.mCalc(.t.)     
              this.GSDS_MLD.TrsFromWork()     
                select _Curs_CONFDCAR
                continue
              enddo
              use
            endif
          endif
          * --- Rinfrescamenti vari
          SELECT (cCurMLD) 
 GO iif(gopos<=RecCount(cCurMLD), gopos, 1) 
 With this.GSDS_MLD 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=nAbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow=nRelRow 
 .bUpDated = TRUE 
 EndWith
        endif
      case this.oParentObject.w_CCTIPOCA = "M"
        * --- Modelli
        if not empty(nvl(this.oParentObject.w_CCCODICE,""))
          * --- Read from MODE_ART
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MODE_ART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MODE_ART_idx,2],.t.,this.MODE_ART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" MODE_ART where ";
                  +"CCCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
                  +" and CCTIPOCA = "+cp_ToStrODBC("M");
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CCCODART = this.oParentObject.w_CCCODART;
                  and CCTIPOCA = "M";
                  and CCCODICE = this.oParentObject.w_CCCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.GSDS_ALP.w_CCFORMUL = NVL(cp_ToDate(_read_.CCFORMUL),cp_NullValue(_read_.CCFORMUL))
            this.GSDS_ALP.w_CCVAR101 = NVL(cp_ToDate(_read_.CCVAR101),cp_NullValue(_read_.CCVAR101))
            this.GSDS_ALP.w_CCQTA101 = NVL(cp_ToDate(_read_.CCQTA101),cp_NullValue(_read_.CCQTA101))
            this.GSDS_ALP.w_CCVAR102 = NVL(cp_ToDate(_read_.CCVAR102),cp_NullValue(_read_.CCVAR102))
            this.GSDS_ALP.w_CCQTA102 = NVL(cp_ToDate(_read_.CCQTA102),cp_NullValue(_read_.CCQTA102))
            this.GSDS_ALP.w_CCVAR103 = NVL(cp_ToDate(_read_.CCVAR103),cp_NullValue(_read_.CCVAR103))
            this.GSDS_ALP.w_CCQTA103 = NVL(cp_ToDate(_read_.CCQTA103),cp_NullValue(_read_.CCQTA103))
            this.GSDS_ALP.w_CCVAR104 = NVL(cp_ToDate(_read_.CCVAR104),cp_NullValue(_read_.CCVAR104))
            this.GSDS_ALP.w_CCQTA104 = NVL(cp_ToDate(_read_.CCQTA104),cp_NullValue(_read_.CCQTA104))
            this.GSDS_ALP.w_CCVAR105 = NVL(cp_ToDate(_read_.CCVAR105),cp_NullValue(_read_.CCVAR105))
            this.GSDS_ALP.w_CCQTA105 = NVL(cp_ToDate(_read_.CCQTA105),cp_NullValue(_read_.CCQTA105))
            this.GSDS_ALP.w_CCVAR106 = NVL(cp_ToDate(_read_.CCVAR106),cp_NullValue(_read_.CCVAR106))
            this.GSDS_ALP.w_CCQTA106 = NVL(cp_ToDate(_read_.CCQTA106),cp_NullValue(_read_.CCQTA106))
            this.GSDS_ALP.w_CCVAR107 = NVL(cp_ToDate(_read_.CCVAR107),cp_NullValue(_read_.CCVAR107))
            this.GSDS_ALP.w_CCQTA107 = NVL(cp_ToDate(_read_.CCQTA107),cp_NullValue(_read_.CCQTA107))
            this.GSDS_ALP.w_CCVAR108 = NVL(cp_ToDate(_read_.CCVAR108),cp_NullValue(_read_.CCVAR108))
            this.GSDS_ALP.w_CCQTA108 = NVL(cp_ToDate(_read_.CCQTA108),cp_NullValue(_read_.CCQTA108))
            this.GSDS_ALP.w_CCVAR109 = NVL(cp_ToDate(_read_.CCVAR109),cp_NullValue(_read_.CCVAR109))
            this.GSDS_ALP.w_CCQTA109 = NVL(cp_ToDate(_read_.CCQTA109),cp_NullValue(_read_.CCQTA109))
            this.GSDS_ALP.w_CCVAR110 = NVL(cp_ToDate(_read_.CCVAR110),cp_NullValue(_read_.CCVAR110))
            this.GSDS_ALP.w_CCQTA110 = NVL(cp_ToDate(_read_.CCQTA110),cp_NullValue(_read_.CCQTA110))
            this.GSDS_ALP.w_CCVAR111 = NVL(cp_ToDate(_read_.CCVAR111),cp_NullValue(_read_.CCVAR111))
            this.GSDS_ALP.w_CCQTA111 = NVL(cp_ToDate(_read_.CCQTA111),cp_NullValue(_read_.CCQTA111))
            this.GSDS_ALP.w_CCVAR112 = NVL(cp_ToDate(_read_.CCVAR112),cp_NullValue(_read_.CCVAR112))
            this.GSDS_ALP.w_CCQTA112 = NVL(cp_ToDate(_read_.CCQTA112),cp_NullValue(_read_.CCQTA112))
            this.GSDS_ALP.w_CCVAR113 = NVL(cp_ToDate(_read_.CCVAR113),cp_NullValue(_read_.CCVAR113))
            this.GSDS_ALP.w_CCQTA113 = NVL(cp_ToDate(_read_.CCQTA113),cp_NullValue(_read_.CCQTA113))
            this.GSDS_ALP.w_CCVAR114 = NVL(cp_ToDate(_read_.CCVAR114),cp_NullValue(_read_.CCVAR114))
            this.GSDS_ALP.w_CCQTA114 = NVL(cp_ToDate(_read_.CCQTA114),cp_NullValue(_read_.CCQTA114))
            this.GSDS_ALP.w_CCVAR115 = NVL(cp_ToDate(_read_.CCVAR115),cp_NullValue(_read_.CCVAR115))
            this.GSDS_ALP.w_CCQTA115 = NVL(cp_ToDate(_read_.CCQTA115),cp_NullValue(_read_.CCQTA115))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows>0
            * --- Read from MODE_ART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MODE_ART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MODE_ART_idx,2],.t.,this.MODE_ART_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" MODE_ART where ";
                    +"CCCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
                    +" and CCTIPOCA = "+cp_ToStrODBC("M");
                    +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CCCODART = this.oParentObject.w_CCCODART;
                    and CCTIPOCA = "M";
                    and CCCODICE = this.oParentObject.w_CCCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.GSDS_ALP.w_CCMIN101 = NVL(cp_ToDate(_read_.CCMIN101),cp_NullValue(_read_.CCMIN101))
              this.GSDS_ALP.w_CCMAX101 = NVL(cp_ToDate(_read_.CCMAX101),cp_NullValue(_read_.CCMAX101))
              this.GSDS_ALP.w_CCMIN102 = NVL(cp_ToDate(_read_.CCMIN102),cp_NullValue(_read_.CCMIN102))
              this.GSDS_ALP.w_CCMAX102 = NVL(cp_ToDate(_read_.CCMAX102),cp_NullValue(_read_.CCMAX102))
              this.GSDS_ALP.w_CCMIN103 = NVL(cp_ToDate(_read_.CCMIN103),cp_NullValue(_read_.CCMIN103))
              this.GSDS_ALP.w_CCMAX103 = NVL(cp_ToDate(_read_.CCMAX103),cp_NullValue(_read_.CCMAX103))
              this.GSDS_ALP.w_CCMIN104 = NVL(cp_ToDate(_read_.CCMIN104),cp_NullValue(_read_.CCMIN104))
              this.GSDS_ALP.w_CCMAX104 = NVL(cp_ToDate(_read_.CCMAX104),cp_NullValue(_read_.CCMAX104))
              this.GSDS_ALP.w_CCMIN105 = NVL(cp_ToDate(_read_.CCMIN105),cp_NullValue(_read_.CCMIN105))
              this.GSDS_ALP.w_CCMAX105 = NVL(cp_ToDate(_read_.CCMAX105),cp_NullValue(_read_.CCMAX105))
              this.GSDS_ALP.w_CCMIN106 = NVL(cp_ToDate(_read_.CCMIN106),cp_NullValue(_read_.CCMIN106))
              this.GSDS_ALP.w_CCMAX106 = NVL(cp_ToDate(_read_.CCMAX106),cp_NullValue(_read_.CCMAX106))
              this.GSDS_ALP.w_CCMIN107 = NVL(cp_ToDate(_read_.CCMIN107),cp_NullValue(_read_.CCMIN107))
              this.GSDS_ALP.w_CCMAX107 = NVL(cp_ToDate(_read_.CCMAX107),cp_NullValue(_read_.CCMAX107))
              this.GSDS_ALP.w_CCMIN108 = NVL(cp_ToDate(_read_.CCMIN108),cp_NullValue(_read_.CCMIN108))
              this.GSDS_ALP.w_CCMAX108 = NVL(cp_ToDate(_read_.CCMAX108),cp_NullValue(_read_.CCMAX108))
              this.GSDS_ALP.w_CCMIN109 = NVL(cp_ToDate(_read_.CCMIN109),cp_NullValue(_read_.CCMIN109))
              this.GSDS_ALP.w_CCMAX109 = NVL(cp_ToDate(_read_.CCMAX109),cp_NullValue(_read_.CCMAX109))
              this.GSDS_ALP.w_CCMIN110 = NVL(cp_ToDate(_read_.CCMIN110),cp_NullValue(_read_.CCMIN110))
              this.GSDS_ALP.w_CCMAX110 = NVL(cp_ToDate(_read_.CCMAX110),cp_NullValue(_read_.CCMAX110))
              this.GSDS_ALP.w_CCMIN111 = NVL(cp_ToDate(_read_.CCMIN111),cp_NullValue(_read_.CCMIN111))
              this.GSDS_ALP.w_CCMAX111 = NVL(cp_ToDate(_read_.CCMAX111),cp_NullValue(_read_.CCMAX111))
              this.GSDS_ALP.w_CCMIN112 = NVL(cp_ToDate(_read_.CCMIN112),cp_NullValue(_read_.CCMIN112))
              this.GSDS_ALP.w_CCMAX112 = NVL(cp_ToDate(_read_.CCMAX112),cp_NullValue(_read_.CCMAX112))
              this.GSDS_ALP.w_CCMIN113 = NVL(cp_ToDate(_read_.CCMIN113),cp_NullValue(_read_.CCMIN113))
              this.GSDS_ALP.w_CCMAX113 = NVL(cp_ToDate(_read_.CCMAX113),cp_NullValue(_read_.CCMAX113))
              this.GSDS_ALP.w_CCMIN114 = NVL(cp_ToDate(_read_.CCMIN114),cp_NullValue(_read_.CCMIN114))
              this.GSDS_ALP.w_CCMAX114 = NVL(cp_ToDate(_read_.CCMAX114),cp_NullValue(_read_.CCMAX114))
              this.GSDS_ALP.w_CCMIN115 = NVL(cp_ToDate(_read_.CCMIN115),cp_NullValue(_read_.CCMIN115))
              this.GSDS_ALP.w_CCMAX115 = NVL(cp_ToDate(_read_.CCMAX115),cp_NullValue(_read_.CCMAX115))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Inserisce il dettaglio dalla Caratteristica generica
            * --- Read from CONF_CAR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONF_CAR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONF_CAR_idx,2],.t.,this.CONF_CAR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CONF_CAR where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                    +" and CCTIPOCA = "+cp_ToStrODBC("M");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CCCODICE = this.oParentObject.w_CCCODICE;
                    and CCTIPOCA = "M";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.GSDS_ALP.w_CCFORMUL = NVL(cp_ToDate(_read_.CCFORMUL),cp_NullValue(_read_.CCFORMUL))
              this.GSDS_ALP.w_CCVAR101 = NVL(cp_ToDate(_read_.CCVAR101),cp_NullValue(_read_.CCVAR101))
              this.GSDS_ALP.w_CCQTA101 = NVL(cp_ToDate(_read_.CCQTA101),cp_NullValue(_read_.CCQTA101))
              this.GSDS_ALP.w_CCVAR102 = NVL(cp_ToDate(_read_.CCVAR102),cp_NullValue(_read_.CCVAR102))
              this.GSDS_ALP.w_CCQTA102 = NVL(cp_ToDate(_read_.CCQTA102),cp_NullValue(_read_.CCQTA102))
              this.GSDS_ALP.w_CCVAR103 = NVL(cp_ToDate(_read_.CCVAR103),cp_NullValue(_read_.CCVAR103))
              this.GSDS_ALP.w_CCQTA103 = NVL(cp_ToDate(_read_.CCQTA103),cp_NullValue(_read_.CCQTA103))
              this.GSDS_ALP.w_CCVAR104 = NVL(cp_ToDate(_read_.CCVAR104),cp_NullValue(_read_.CCVAR104))
              this.GSDS_ALP.w_CCQTA104 = NVL(cp_ToDate(_read_.CCQTA104),cp_NullValue(_read_.CCQTA104))
              this.GSDS_ALP.w_CCVAR105 = NVL(cp_ToDate(_read_.CCVAR105),cp_NullValue(_read_.CCVAR105))
              this.GSDS_ALP.w_CCQTA105 = NVL(cp_ToDate(_read_.CCQTA105),cp_NullValue(_read_.CCQTA105))
              this.GSDS_ALP.w_CCVAR106 = NVL(cp_ToDate(_read_.CCVAR106),cp_NullValue(_read_.CCVAR106))
              this.GSDS_ALP.w_CCQTA106 = NVL(cp_ToDate(_read_.CCQTA106),cp_NullValue(_read_.CCQTA106))
              this.GSDS_ALP.w_CCVAR107 = NVL(cp_ToDate(_read_.CCVAR107),cp_NullValue(_read_.CCVAR107))
              this.GSDS_ALP.w_CCQTA107 = NVL(cp_ToDate(_read_.CCQTA107),cp_NullValue(_read_.CCQTA107))
              this.GSDS_ALP.w_CCVAR108 = NVL(cp_ToDate(_read_.CCVAR108),cp_NullValue(_read_.CCVAR108))
              this.GSDS_ALP.w_CCQTA108 = NVL(cp_ToDate(_read_.CCQTA108),cp_NullValue(_read_.CCQTA108))
              this.GSDS_ALP.w_CCVAR109 = NVL(cp_ToDate(_read_.CCVAR109),cp_NullValue(_read_.CCVAR109))
              this.GSDS_ALP.w_CCQTA109 = NVL(cp_ToDate(_read_.CCQTA109),cp_NullValue(_read_.CCQTA109))
              this.GSDS_ALP.w_CCVAR110 = NVL(cp_ToDate(_read_.CCVAR110),cp_NullValue(_read_.CCVAR110))
              this.GSDS_ALP.w_CCQTA110 = NVL(cp_ToDate(_read_.CCQTA110),cp_NullValue(_read_.CCQTA110))
              this.GSDS_ALP.w_CCVAR111 = NVL(cp_ToDate(_read_.CCVAR111),cp_NullValue(_read_.CCVAR111))
              this.GSDS_ALP.w_CCQTA111 = NVL(cp_ToDate(_read_.CCQTA111),cp_NullValue(_read_.CCQTA111))
              this.GSDS_ALP.w_CCVAR112 = NVL(cp_ToDate(_read_.CCVAR112),cp_NullValue(_read_.CCVAR112))
              this.GSDS_ALP.w_CCQTA112 = NVL(cp_ToDate(_read_.CCQTA112),cp_NullValue(_read_.CCQTA112))
              this.GSDS_ALP.w_CCVAR113 = NVL(cp_ToDate(_read_.CCVAR113),cp_NullValue(_read_.CCVAR113))
              this.GSDS_ALP.w_CCQTA113 = NVL(cp_ToDate(_read_.CCQTA113),cp_NullValue(_read_.CCQTA113))
              this.GSDS_ALP.w_CCVAR114 = NVL(cp_ToDate(_read_.CCVAR114),cp_NullValue(_read_.CCVAR114))
              this.GSDS_ALP.w_CCQTA114 = NVL(cp_ToDate(_read_.CCQTA114),cp_NullValue(_read_.CCQTA114))
              this.GSDS_ALP.w_CCVAR115 = NVL(cp_ToDate(_read_.CCVAR115),cp_NullValue(_read_.CCVAR115))
              this.GSDS_ALP.w_CCQTA115 = NVL(cp_ToDate(_read_.CCQTA115),cp_NullValue(_read_.CCQTA115))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from CONF_CAR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONF_CAR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONF_CAR_idx,2],.t.,this.CONF_CAR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CONF_CAR where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                    +" and CCTIPOCA = "+cp_ToStrODBC("M");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CCCODICE = this.oParentObject.w_CCCODICE;
                    and CCTIPOCA = "M";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.GSDS_ALP.w_CCMIN101 = NVL(cp_ToDate(_read_.CCMIN101),cp_NullValue(_read_.CCMIN101))
              this.GSDS_ALP.w_CCMAX101 = NVL(cp_ToDate(_read_.CCMAX101),cp_NullValue(_read_.CCMAX101))
              this.GSDS_ALP.w_CCMIN102 = NVL(cp_ToDate(_read_.CCMIN102),cp_NullValue(_read_.CCMIN102))
              this.GSDS_ALP.w_CCMAX102 = NVL(cp_ToDate(_read_.CCMAX102),cp_NullValue(_read_.CCMAX102))
              this.GSDS_ALP.w_CCMIN103 = NVL(cp_ToDate(_read_.CCMIN103),cp_NullValue(_read_.CCMIN103))
              this.GSDS_ALP.w_CCMAX103 = NVL(cp_ToDate(_read_.CCMAX103),cp_NullValue(_read_.CCMAX103))
              this.GSDS_ALP.w_CCMIN104 = NVL(cp_ToDate(_read_.CCMIN104),cp_NullValue(_read_.CCMIN104))
              this.GSDS_ALP.w_CCMAX104 = NVL(cp_ToDate(_read_.CCMAX104),cp_NullValue(_read_.CCMAX104))
              this.GSDS_ALP.w_CCMIN105 = NVL(cp_ToDate(_read_.CCMIN105),cp_NullValue(_read_.CCMIN105))
              this.GSDS_ALP.w_CCMAX105 = NVL(cp_ToDate(_read_.CCMAX105),cp_NullValue(_read_.CCMAX105))
              this.GSDS_ALP.w_CCMIN106 = NVL(cp_ToDate(_read_.CCMIN106),cp_NullValue(_read_.CCMIN106))
              this.GSDS_ALP.w_CCMAX106 = NVL(cp_ToDate(_read_.CCMAX106),cp_NullValue(_read_.CCMAX106))
              this.GSDS_ALP.w_CCMIN107 = NVL(cp_ToDate(_read_.CCMIN107),cp_NullValue(_read_.CCMIN107))
              this.GSDS_ALP.w_CCMAX107 = NVL(cp_ToDate(_read_.CCMAX107),cp_NullValue(_read_.CCMAX107))
              this.GSDS_ALP.w_CCMIN108 = NVL(cp_ToDate(_read_.CCMIN108),cp_NullValue(_read_.CCMIN108))
              this.GSDS_ALP.w_CCMAX108 = NVL(cp_ToDate(_read_.CCMAX108),cp_NullValue(_read_.CCMAX108))
              this.GSDS_ALP.w_CCMIN109 = NVL(cp_ToDate(_read_.CCMIN109),cp_NullValue(_read_.CCMIN109))
              this.GSDS_ALP.w_CCMAX109 = NVL(cp_ToDate(_read_.CCMAX109),cp_NullValue(_read_.CCMAX109))
              this.GSDS_ALP.w_CCMIN110 = NVL(cp_ToDate(_read_.CCMIN110),cp_NullValue(_read_.CCMIN110))
              this.GSDS_ALP.w_CCMAX110 = NVL(cp_ToDate(_read_.CCMAX110),cp_NullValue(_read_.CCMAX110))
              this.GSDS_ALP.w_CCMIN111 = NVL(cp_ToDate(_read_.CCMIN111),cp_NullValue(_read_.CCMIN111))
              this.GSDS_ALP.w_CCMAX111 = NVL(cp_ToDate(_read_.CCMAX111),cp_NullValue(_read_.CCMAX111))
              this.GSDS_ALP.w_CCMIN112 = NVL(cp_ToDate(_read_.CCMIN112),cp_NullValue(_read_.CCMIN112))
              this.GSDS_ALP.w_CCMAX112 = NVL(cp_ToDate(_read_.CCMAX112),cp_NullValue(_read_.CCMAX112))
              this.GSDS_ALP.w_CCMIN113 = NVL(cp_ToDate(_read_.CCMIN113),cp_NullValue(_read_.CCMIN113))
              this.GSDS_ALP.w_CCMAX113 = NVL(cp_ToDate(_read_.CCMAX113),cp_NullValue(_read_.CCMAX113))
              this.GSDS_ALP.w_CCMIN114 = NVL(cp_ToDate(_read_.CCMIN114),cp_NullValue(_read_.CCMIN114))
              this.GSDS_ALP.w_CCMAX114 = NVL(cp_ToDate(_read_.CCMAX114),cp_NullValue(_read_.CCMAX114))
              this.GSDS_ALP.w_CCMIN115 = NVL(cp_ToDate(_read_.CCMIN115),cp_NullValue(_read_.CCMIN115))
              this.GSDS_ALP.w_CCMAX115 = NVL(cp_ToDate(_read_.CCMAX115),cp_NullValue(_read_.CCMAX115))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.GSDS_ALP.mCalc(.t.)     
          this.GSDS_ALP.SaveDependsOn()     
          this.GSDS_ALP.mHideControls()     
          this.GSDS_ALP.ChildrenChangeRow()     
          this.GSDS_ALP.bUpdated = True
        endif
    endcase
  endproc


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONF_CAR'
    this.cWorkTables[2]='CONFDART'
    this.cWorkTables[3]='CONFDCAR'
    this.cWorkTables[4]='MODE_ART'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDCAR')
      use in _Curs_CONFDCAR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
