* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bc5                                                        *
*              Controllo apertura maschera configuratore                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-06                                                      *
* Last revis.: 2014-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bc5",oParentObject)
return(i_retval)

define class tgscr_bc5 as StdBatch
  * --- Local variables
  w_LOOP = 0
  w_ISOPEN = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo se � aperta la maschera del configuratore per evitare l'apertura di pi� finestre
    this.w_ISOPEN = .f.
    this.w_LOOP = 1
    if TYPE("this.oParentObject.class")="C" and UPPER(this.oParentObject.class)="TGSVE_BTA"
      do while this.w_LOOP<=_Screen.FormCount and !this.w_ISOPEN
        if VarType( _Screen.Forms[ this.w_LOOP ].cComment )="C" 
          if UPPER(_Screen.Forms( this.w_Loop ).Class)="TGSCR_KCC"
            this.w_ISOPEN = .t.
          endif
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    if !this.w_ISOPEN
      if TYPE("this.oParentObject.class")="C" and UPPER(this.oParentObject.class)="TGSVE_BTA"
        do GSCR_KCC with this.oParentObject.oParentObject
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        do GSCR_KCC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
