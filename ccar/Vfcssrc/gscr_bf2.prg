* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bf2                                                        *
*              Verifica formule coefficienti di impiego                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-24                                                      *
* Last revis.: 2014-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Evento
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bf2",oParentObject,m.Evento)
return(i_retval)

define class tgscr_bf2 as StdBatch
  * --- Local variables
  Evento = space(5)
  w_OCCUR = 0
  TmpSave = space(0)
  TmpSave1 = space(0)
  FuncCalc = space(0)
  w_OCCUR1 = 0
  w_DIFFE = 0
  w_VALORE = 0
  w_CNT = 0
  Func = space(0)
  CODVOC = space(15)
  INDICE = 0
  w_MESS = space(10)
  w_i = 0
  w_PADRE = .NULL.
  w_CURNON = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica della formula integrata nei Coefficienti di Impiego (da GSDS_ACI)
    Messaggio = "Corretta"
    this.oParentObject.w_TEST = "S"
    this.w_MESS = "Impossibile salvare sintassi errata"
    this.TmpSave = ALLTRIM(this.oParentObject.w_CCFORMUL)
    * --- Ricerco il segno '<' il quale indica che ho inserito un Parametro Variabile
    this.w_OCCUR = RATC("<",this.TmpSave)
    this.w_OCCUR1 = RATC(">",this.TmpSave)
    if this.w_OCCUR1<this.w_OCCUR
      ah_errormsg( this.w_MESS)
      this.oParentObject.w_TEST = "N"
      i_retcode = 'stop'
      return
    endif
    this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    this.w_CNT = 0
    do while this.w_OCCUR <> 0
      * --- Ciclo sulla stringa per verificarne la correttezza
      this.TmpSave1 = SUBSTR(this.TmpSave,1,this.w_OCCUR-1)
      this.TmpSave1 = this.TmpSave1 + "10"
      this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR1+1))
      * --- Verifico la correttezza del Parametro Variabile
      this.CODVOC = ALLTRIM(SUBSTR(this.TmpSave,this.w_OCCUR+1,this.w_DIFFE-1))
      do case
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR101," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR102," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR103," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR104," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR105," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR106," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR107," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR108," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR109," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR110," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR111," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR112," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR113," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR114," "))=this.CODVOC
        case ALLTRIM(NVL(this.oParentObject.w_CCVAR115," "))=this.CODVOC
        otherwise
          Messaggio = "Errata"
      endcase
      if Messaggio = "Corretta"
        this.w_CNT = this.w_CNT + 1
      else
        * --- Sostituisco il Parametro Variabile errato mettendolo tra ##
        this.TmpSave1 = SUBSTR(this.oParentObject.w_CCFORMUL,1,this.w_OCCUR)
        this.TmpSave1 = this.TmpSave1 + "#" + ALLTRIM(this.CODVOC) + "#"
        this.TmpSave1 = this.TmpSave1 + ALLTRIM(SUBSTR(this.oParentObject.w_CCFORMUL,this.w_OCCUR1))
        this.oParentObject.w_CCFORMUL = ALLTRIM(this.TmpSave1)
        this.oParentObject.w_ERRMSG = "T"
        this.oParentObject.w_TEST = "N"
        ah_errormsg(this.w_MESS)
        EXIT
      endif
      this.TmpSave = ALLTRIM(this.TmpSave1)
      this.w_OCCUR = RATC("<",this.TmpSave)
      this.w_OCCUR1 = RATC(">",this.TmpSave)
      * --- --Catturato errore delimitatore finale non iserito
      if this.w_OCCUR1<this.w_OCCUR
        ah_errormsg( this.w_MESS)
        this.oParentObject.w_TEST = "N"
        i_retcode = 'stop'
        return
      endif
      this.w_DIFFE = this.w_OCCUR1-this.w_OCCUR
    enddo
    if Messaggio = "Corretta"
      * --- Prima verifica della formula inserita
      this.Func = this.oParentObject.w_CCFORMUL
      this.FuncCalc =  ALLTRIM(this.TmpSave)
      ON ERROR Messaggio = "Errata"
      TmpVal = this.FuncCalc
      * --- Ulteriore verifica dei dati
      this.w_PADRE = this.oparentobject
      myformula = this.oParentObject.w_CCFORMUL
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR101)+">", str(this.w_PADRE.oparentobject.w_CCQTA101,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR102)+">", str(this.w_PADRE.oparentobject.w_CCQTA102,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR103)+">", str(this.w_PADRE.oparentobject.w_CCQTA103,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR104)+">", str(this.w_PADRE.oparentobject.w_CCQTA104,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR105)+">", str(this.w_PADRE.oparentobject.w_CCQTA105,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR106)+">", str(this.w_PADRE.oparentobject.w_CCQTA106,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR107)+">", str(this.w_PADRE.oparentobject.w_CCQTA107,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR108)+">", str(this.w_PADRE.oparentobject.w_CCQTA108,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR109)+">", str(this.w_PADRE.oparentobject.w_CCQTA109,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR110)+">", str(this.w_PADRE.oparentobject.w_CCQTA110,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR111)+">", str(this.w_PADRE.oparentobject.w_CCQTA111,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR112)+">", str(this.w_PADRE.oparentobject.w_CCQTA112,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR113)+">", str(this.w_PADRE.oparentobject.w_CCQTA113,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR114)+">", str(this.w_PADRE.oparentobject.w_CCQTA114,10,3))
      myformula = strtran(myformula, "<"+ALLTRIM(this.oParentObject.w_CCVAR115)+">", str(this.w_PADRE.oparentobject.w_CCQTA115,10,3))
      myformula = strtran(myformula, "," , ".")
      * --- Valutazione dell'errore
      this.w_VALORE = &TmpVal
      this.w_VALORE = eval(myformula)
      ON ERROR
      if Messaggio="Errata"
        this.oParentObject.w_ERRMSG = "N"
        this.oParentObject.w_TEST = "N"
        ah_errormsg(this.w_MESS)
      else
        this.oParentObject.w_ERRMSG = "S"
      endif
    endif
    if Messaggio<>"Errata" AND this.w_CNT>15
      Messaggio = "Errata"
      this.w_MESS = "Inserire al Massimo 15 Parametri Variabili!"
    endif
    WAIT CLEAR
    if Messaggio = "Errata" AND this.Evento = "Save"
      ah_errormsg( this.w_MESS)
      this.oParentObject.w_TEST = "N"
      i_retcode = 'stop'
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Evento)
    this.Evento=Evento
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Evento"
endproc
