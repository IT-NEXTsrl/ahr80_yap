* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscrikcc                                                        *
*              Caratteristica non specificata                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-19                                                      *
* Last revis.: 2015-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gscrikcc
    Declare Integer SetCursor In WIN32API Integer 
    Declare Integer LoadCursor In WIN32API Integer, Integer 

* --- Fine Area Manuale
return(createobject("tgscrikcc",oParentObject))

* --- Class definition
define class tgscrikcc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 567
  Height = 366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-24"
  HelpContextID=98530665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  CONF_CAR_IDX = 0
  cPrg = "gscrikcc"
  cComment = "Caratteristica non specificata"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CONFKME = .F.
  w_ANNULLA = .F.
  w_ROWSEL = 0
  w_NUMCAR = 0
  w_CCDETTAG = space(40)
  w_LIVCURS = 0
  w_LVLKEYTV = space(10)
  w_CCROWORD = 0
  w_CCCOMPON = space(41)
  w_CCAMBIO = .F.
  w_CCFORMUL = space(5)
  w_CC__DIBA = space(41)
  w_ROWSELE = 0
  w_LVLKEYCUR = space(200)
  w_LVLKEY = space(200)
  w_Albero = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscrikcc
  cBmp1 = ''
  cBmp2 = ''
  cBmp3 = ''
  nThemeColorLevel = 0
  nThemeColor = 0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscrikccPag1","gscrikcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Albero = this.oPgFrm.Pages(1).oPag.Albero
    DoDefault()
    proc Destroy()
      this.w_Albero = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONF_CAR'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCTIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_CONFKME=.f.
      .w_ANNULLA=.f.
      .w_ROWSEL=0
      .w_NUMCAR=0
      .w_CCDETTAG=space(40)
      .w_LIVCURS=0
      .w_LVLKEYTV=space(10)
      .w_CCROWORD=0
      .w_CCCOMPON=space(41)
      .w_CCAMBIO=.f.
      .w_CCFORMUL=space(5)
      .w_CC__DIBA=space(41)
      .w_ROWSELE=0
      .w_LVLKEYCUR=space(200)
      .w_LVLKEY=space(200)
      .w_CONFKME=oParentObject.w_CONFKME
      .w_ANNULLA=oParentObject.w_ANNULLA
      .w_ROWSELE=oParentObject.w_ROWSELE
      .w_LVLKEYCUR=oParentObject.w_LVLKEYCUR
      .w_LVLKEY=oParentObject.w_LVLKEY
        .w_CCTIPOCA = .w_Albero.GetVar("CCTIPOCA")
        .w_CCCODICE = .w_Albero.GetVar("CCCODICE")
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CCCODICE))
          .link_1_2('Full')
        endif
        .w_CCDESCRI = .w_Albero.GetVar("CCDESCRI")
        .w_CONFKME = .F.
        .w_ANNULLA = .F.
      .oPgFrm.Page1.oPag.Albero.Calculate()
          .DoRTCalc(6,6,.f.)
        .w_NUMCAR = 0
        .w_CCDETTAG = .w_Albero.GetVar("CCDETTAG")
        .w_LIVCURS = .w_Albero.GetVar("LIVCURSM")
        .w_LVLKEYTV = .w_Albero.GetVar("LVLKEY")
        .w_CCROWORD = .w_Albero.GetVar("CCROWORD")
        .w_CCCOMPON = .w_Albero.GetVar("CCCOMPON")
        .w_CCAMBIO = .F.
          .DoRTCalc(14,14,.f.)
        .w_CC__DIBA = .w_Albero.GetVar("CC__DIBA")
    endwith
    this.DoRTCalc(16,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CONFKME=.w_CONFKME
      .oParentObject.w_ANNULLA=.w_ANNULLA
      .oParentObject.w_ROWSELE=.w_ROWSELE
      .oParentObject.w_LVLKEYCUR=.w_LVLKEYCUR
      .oParentObject.w_LVLKEY=.w_LVLKEY
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CCTIPOCA = .w_Albero.GetVar("CCTIPOCA")
            .w_CCCODICE = .w_Albero.GetVar("CCCODICE")
          .link_1_2('Full')
            .w_CCDESCRI = .w_Albero.GetVar("CCDESCRI")
        .oPgFrm.Page1.oPag.Albero.Calculate()
        .DoRTCalc(4,7,.t.)
            .w_CCDETTAG = .w_Albero.GetVar("CCDETTAG")
            .w_LIVCURS = .w_Albero.GetVar("LIVCURSM")
            .w_LVLKEYTV = .w_Albero.GetVar("LVLKEY")
            .w_CCROWORD = .w_Albero.GetVar("CCROWORD")
            .w_CCCOMPON = .w_Albero.GetVar("CCCOMPON")
        .DoRTCalc(13,14,.t.)
            .w_CC__DIBA = .w_Albero.GetVar("CC__DIBA")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Albero.Calculate()
    endwith
  return

  proc Calculate_WATKUFZELH()
    with this
          * --- Update start
          .w_CONFKME = .T.
          .w_ANNULLA = .F.
          .w_ROWSELE = .w_ROWSEL
          .w_LVLKEYCUR = .w_LVLKEY
          .oParentObject.w_ROWSELE = .w_ROWSEL
          .oParentObject.w_LVLKEYCUR = .w_LVLKEY
    endwith
  endproc
  proc Calculate_PQHAXFELYZ()
    with this
          * --- Edit Aborted
          .w_CONFKME = .F.
          .w_ANNULLA = .T.
          .w_ROWSEL = 0
          .w_LVLKEY = SPACE(200)
          .oParentObject.w_ROWSELE = .w_ROWSEL
          .oParentObject.w_LVLKEYCUR = .w_LVLKEY
          .oParentObject.w_CONFKME = .w_CONFKME
          .oParentObject.w_ANNULLA = .w_ANNULLA
    endwith
  endproc
  proc Calculate_BRPVWZJDJM()
    with this
          * --- w_Albero row checked
          .w_ROWSEL = INT(.w_Albero.GetVar("CPROWORD"))
          .w_LVLKEY = .w_Albero.GetVar("LVLKEY")
          GSCRIBCC(this;
              ,"ROWSEL";
             )
    endwith
  endproc
  proc Calculate_VAONIDBXZW()
    with this
          * --- w_Albero row unchecked
          .w_ROWSEL = 0
          .w_LVLKEY = SPACE(200)
          GSCRIBCC(this;
              ,"UNROWSEL";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Update start")
          .Calculate_WATKUFZELH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_PQHAXFELYZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.Albero.Event(cEvent)
        if lower(cEvent)==lower("w_Albero row checked")
          .Calculate_BRPVWZJDJM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_Albero row unchecked")
          .Calculate_VAONIDBXZW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscrikcc
      if cEvent=="FormLoad"
        This.nThemeColorLevel = Rgb(179,7,27)
        This.nThemeColor = i_ThemesManager.getprop(109)    
        This.cBmp1 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"gd_plus.bmp", 16), "bmp"))
        This.cBmp2 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"gd_minus.bmp", 16),"bmp"))
        This.cBmp3 = filetostr(Forceext(i_ThemesManager.RetBmpFromIco(i_cBmpPath+"next.bmp", 10),"bmp"))
      endif
      if cEvent = 'w_albero after query' and this.w_Albero.grd.ColumnCount > 0
          LOCAL loColumn, lControlSource,lColumnOrder
          
          this.w_Albero.grd.RecordMark=.F.
          this.w_Albero.grd.HighlightRow= .F.
          this.w_Albero.grd.Highlight =.F.
          this.w_Albero.grd.HighlightRowLineWidth = 1
          this.w_Albero.grd.AllowRowsizing=.F.
          this.w_Albero.grd.AllowCellSelection = .t.
          this.w_Albero.grd.DeleteMark=.F.
          this.w_Albero.grd.FontBold=.F.
          this.w_Albero.grd.HighlightStyle=2
          this.w_Albero.grd.GridLines=2
          this.w_Albero.grd.GridLineColor = ThisForm.nThemeColor
          
    			FOR EACH m.loColumn IN this.w_Albero.grd.Columns
            lControlSource =  m.loColumn.Controlsource
            lColumnOrder = m.loColumn.ColumnOrder
            
                  IF lControlSource = 'DESCRITV'
                  
                    WITH m.loColumn
                      .Sparse=.f.
                      .ReadOnly=.f.
                      
                      IF !PEMSTATUS(m.loColumn, [Container1], 5)
                        .AddObject([Container1], [HdrRowGroup])
                      endif
    
                      IF !PEMSTATUS(m.loColumn, [ContainerL], 5)
                        .AddObject([ContainerL], [HdrlVLGroup])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerF], 5)
                        .AddObject([ContainerF], [DtlCarFisse])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerM], 5)
                        .AddObject([ContainerM], [DtlCarParam])
                      endif
                      
                      IF !PEMSTATUS(m.loColumn, [ContainerD], 5)
                        .AddObject([ContainerD], [DtlCarDescr])
                      endif          
                      
                       .CurrentControl=""
    
                      Do case
                        Case .Controlsource = "CCHDRLVL"
                            .DynamicCurrentControl = "IIF(CCHDRLVL='S', IIF(EXPANDED='S' , 'ImgO', 'ImgC') , 'ShapeB')"
                        Case .Controlsource = "CCHEADER"
                            .DynamicCurrentControl = "IIF(CCHEADER='S', IIF(EXPANDED='S' , 'ImgO', 'ImgC') , 'Shape1')"
                          *  .CurrentControl='Shape1'
                        Case .Controlsource = "DESCRITV"
                            .DynamicCurrentControl = "IIF(CCHDRLVL='S', 'ContainerL', IIF(CCHEADER='S', 'Container1' , IIF(CCTIPOCA='C', 'ContainerF' , IIF(CCTIPOCA='M', 'ContainerM' ,  IIF(CCTIPOCA='D', 'ContainerD', 'Text1')))))"
                            * .DynamicBackColor = [ThisForm.w_Albero.grd.dynamic()]
                        otherwise
                             .CurrentControl="text1"
                      EndCase
    
                       * RGB(206,231,255) AHR 7.0
                       * RGB(400,40,40) AHR 8.0
                       
                    ENDWITH
                       
                  ELSE
                    IF !lControlSource $ 'CCHDRLVL-CCHEADER-DESCRITV-CCVALMIN-CCVALMAX-CCFLINT'
                            m.loColumn.Visible=.f.
                    endif                
                 ENDIF
                    
          ENDFOR
          
        endif
            
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_CCTIPOCA;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCTIPOCA,CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCTIPOCA_1_1.RadioValue()==this.w_CCTIPOCA)
      this.oPgFrm.Page1.oPag.oCCTIPOCA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value=this.w_CCDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscrikccPag1 as StdContainer
  Width  = 563
  height = 366
  stdWidth  = 563
  stdheight = 366
  resizeXpos=296
  resizeYpos=179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCCTIPOCA_1_1 as StdCombo with uid="FPEZQDZUHF",rtseq=1,rtrep=.f.,left=464,top=8,width=61,height=19, enabled=.f.;
    , disabledbackcolor=rgb(255,255,255);
    , ToolTipText = "Tipo Caratteristica";
    , HelpContextID = 242160231;
    , cFormVar="w_CCTIPOCA",RowSource=""+"Caratteristica,"+"Modello,"+"Descrizione", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCCTIPOCA_1_1.RadioValue()
    return(iif(this.value =1,"C",;
    iif(this.value =2,"M",;
    iif(this.value =3,"D",;
    space(1)))))
  endfunc
  func oCCTIPOCA_1_1.GetRadio()
    this.Parent.oContained.w_CCTIPOCA = this.RadioValue()
    return .t.
  endfunc

  func oCCTIPOCA_1_1.SetRadio()
    this.Parent.oContained.w_CCTIPOCA=trim(this.Parent.oContained.w_CCTIPOCA)
    this.value = ;
      iif(this.Parent.oContained.w_CCTIPOCA=="C",1,;
      iif(this.Parent.oContained.w_CCTIPOCA=="M",2,;
      iif(this.Parent.oContained.w_CCTIPOCA=="D",3,;
      0)))
  endfunc

  func oCCTIPOCA_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CCCODICE)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCCCODICE_1_2 as StdField with uid="UKRNGQOPMF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Selezionato",;
    HelpContextID = 129237611,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=81, Top=8, InputMask=replicate('X',5), cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_CCTIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CCCODICE"

  func oCCCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCCDESCRI_1_3 as StdField with uid="RTDPFZLGJA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 43651695,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=144, Top=8, InputMask=replicate('X',40)


  add object oBtn_1_6 as StdButton with uid="ZXMNIRYPDF",left=449, top=314, width=48,height=45,;
    CpPicture="BMP\GENERA.bmp", caption="", nPag=1;
    , ToolTipText = "Memorizza la formula impostata";
    , HelpContextID = 98510106;
    , Caption='G\<enera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="ZYBCAUTBZV",left=506, top=314, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 90467066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Albero as cp_zoomboxCartt with uid="BBULQFBEFC",left=4, top=31, width=552,height=280,;
    caption='Albero',;
   bGlobalFont=.t.,;
    cZoomFile="GSCR_KCC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="CONF_DIS",bQueryOnDblClick=.f.,cMenuFile="",cZoomOnZoom="",bSearchFilterOnClick=.f.,bNoZoomGridShape=.f.,bRetriveAllRows=.f.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 11279110

  add object oStr_1_4 as StdString with uid="TVCCDMIWHT",Visible=.t., Left=19, Top=8,;
    Alignment=1, Width=60, Height=18,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="ZEBJDENDJY",Visible=.t., Left=433, Top=8,;
    Alignment=1, Width=27, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscrikcc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscrikcc
* ====================================================================================================
*                          DEFINIZIONE CLASSI GESTIONE TREEVIEW
* ====================================================================================================
Define Class cp_zoomboxCartt as cp_zoombox
nReco=0

      Proc grd.Refresh
        This.enabled=.f.
        This.Column1.Container1.Enabled=.F.
        This.Column1.ContainerL.Enabled=.F.
        This.Column1.ContainerF.Enabled=.F.
        This.Column1.ContainerD.Enabled=.F.
        This.Column1.ContainerM.Enabled=.F.
        DoDefault()
        This.Column1.Container1.Enabled=.T.
        This.Column1.ContainerL.Enabled=.T.
        This.Column1.ContainerF.Enabled=.T.
        This.Column1.ContainerD.Enabled=.T.
        This.Column1.ContainerM.Enabled=.T.
        This.enabled=.t.
      EndPRoc

*!*     Proc grd.Scrolled
*!*         LPARAMETERS nDirection
*!*      SetCursor(LoadCursor(0,32512))
*!*         ThisForm.lockScreen= .T.
*!*         DoDefault()
*!*      This.Refresh()
*!*         ThisForm.lockScreen= .F.
*!*      SetCursor(LoadCursor(0,32512))
*!*      EndProc

*!*	  Proc grd.AfterRowColChange(nIdx)
*!*	      SetCursor(LoadCursor(0,32512))
*!*	      DoDefault()
*!*	      SetCursor(LoadCursor(0,32512))
*!*	   EndProc

*!*	  Proc grd.AfterRowColChange(nIdx)
*!*	      This.Parent.nReco=Recno(this.RecordSource)
*!*	      SetCursor(LoadCursor(0,32512))
*!*	      DoDefault()
*!*	      ThisForm.lockScreen= .T.
*!*	      This.Refresh()
*!*	      ThisForm.lockScreen= .F.
*!*	      SetCursor(LoadCursor(0,32512))
*!*	   EndProc
   
   Proc grd.nodeClick
      LOCAL livtreev
      LOCAL lvlkeytv, cExpanded
      m.cExpanded = ' '
      SELECT (this.RecordSource)
      ThisForm.LockScreen = .T.
      IF (CCHDRLVL="S" OR CCHEADER="S") AND NOT EMPTY(expanded)
        ThisForm.LockScreen = .T.
        m.lvlkeytv = TRIM(LVLKEYTV) && EVALUATE(KEY())
        IF expanded="S"
          replace expanded WITH "N"
          SKIP 
          REPLACE SHOWNODE WITH "N" while m.lvlkeytv $ TRIM(lvlkeytv)
        ELSE
          SELECT (this.recordSource)
          SET FILTER TO 
          replace expanded WITH "S"
          m.livtreev = livtreev
          SKIP
          SCAN REST WHILE  TRIM(m.lvlkeytv) $ TRIM(LVLKEYTV)
            IF livtreev = m.livtreev + 1
              replace SHOWNODE with 'S'
              cExpanded = Expanded
            ELSE
              IF m.cExpanded="S"
                replace SHOWNODE with 'S'
              ENDIF
            ENDIF
            SELECT (this.recordSource)
          ENDSCAN          
        ENDIF
        SELECT (This.RecordSource)
        SEEK m.lvlkeytv
        SET FILTER TO SHOWNODE="S"
        This.Refresh()
        This.column1.dynamicCurrentControl=This.column1.dynamicCurrentControl
        ThisForm.LockScreen = .F.
      ENDIF   
   EndProc


  Proc grd.dynamic
      local l_ContainerG, l_ContainerG, l_oLabbelGrp, l_lnBackColor

    SetCursor(LoadCursor(0,32512))

    If type("this.column1")="O"
        l_ContainerG = Evaluate(this.column1.dynamicCurrentControl)
        IF  m.l_ContainerG $ 'ContainerL-Container1'
          l_oLabbelGrp = Evaluate("this.column1."+ Evaluate(this.column1.dynamicCurrentControl)+".oCntBorder.oLblGrp")
          l_lnBackColor =  m.l_oLabbelGrp.Parent.Parent.lnBackColor
          IF Recno(this.RecordSource)=this.Parent.nReco
              m.l_oLabbelGrp.disabledBackColor = this.SelectedItemBackColor   && Rgb(10,36,106)
              m.l_oLabbelGrp.DisabledForeColor = This.SelectedItemForeColor  && Rgb(255,255,255)		
          else
            m.l_oLabbelGrp.disabledBackColor=Rgb(255,255,255)
            m.l_oLabbelGrp.DisabledForeColor = m.l_lnBackColor
          endif
        endif

        IF  m.l_ContainerG $ 'ContainerF-ContainerM-ContainerD'
          l_oLabbelGrp = Evaluate("this.column1."+ Evaluate(this.column1.dynamicCurrentControl)+".oCntBorder")
          l_lnBackColor =  m.l_oLabbelGrp.Parent.lnBackColor

          IF Recno(this.RecordSource)=this.Parent.nReco 
              m.l_oLabbelGrp.BackColor = this.SelectedItemBackColor   && Rgb(10,36,106)
              m.l_oLabbelGrp.BorderColor = Rgb(179,7,27) && this.SelectedItemBackColor
              * m.l_oLabbelGrp.DisabledForeColor = This.SelectedItemForeColor  && Rgb(255,255,255)		
          else
            IF (cctipoca $ 'C-D' and xchk=1) or cctipoca ='M'
               m.l_oLabbelGrp.BackColor = Rgb(255,255,198)
               m.l_oLabbelGrp.BorderColor = Rgb(255,255,198)  && Rgb(179,7,27) && this.SelectedItemBackColor
            ELSE
               m.l_oLabbelGrp.BackColor=Rgb(255,255,255)
               m.l_oLabbelGrp.BorderColor=Rgb(255,255,255)
              * m.l_oLabbelGrp.DisabledForeColor = m.l_lnBackColor
            ENDIF
          endif

          
        endif


    endif		
    SetCursor(LoadCursor(0,32512))
    RETURN RGB(255,255,255)  && IIF(CCHDRLVL="S" OR CCHEADER="S",  RGB(255,255,255) , IIF(CCTIPOCA $ 'C-D-M' , IIF( XCHK=1, RGB(255,255,128) , RGB(255,255,255)),  RGB(255,255,255) ))
  endproc
   
   
EndDefine

Define Class ConfCheckBox As Checkbox
	nFirstSelect=0
	nShift=0
	nLastValue=0
  BackStyle=0
  oContained=.null.
  oZoom=.NULL.
  Proc Init
    dodefault()
    This.oContained = ThisForm && This.Parent.Parent.Parent.Parent.Parent.Parent.oContained
    This.oZoom = This.oContained.w_Albero &&  This.Parent.Parent.Parent.Parent.Parent
	Proc When()
		*if lastkey() = 32 or lastkey() = 13
		*   this.click()
		*  endif
		Return(This.Value<>2)
	Proc Click()
		Local c_mCursor
		If Type('This.oContained')='O'
			This.nLastValue=This.Value
			If This.Value=1
				* --- lasciato per compatibilita'
				This.oContained.NotifyEvent(This.oZoom.Name+' row checked')
				* --- forma corretta
				This.oContained.NotifyEvent('w_'+Lower(This.oZoom.Name)+' row checked')
			Else
				* --- lasciato per compatibilita'
				This.oContained.NotifyEvent(This.oZoom.Name+' row unchecked')
				* --- forma corretta
				This.oContained.NotifyEvent('w_'+Lower(This.oZoom.Name)+' row unchecked')
			Endif
			c_mCursor=This.oZoom.cCursor
			Select (c_mCursor)
			This.nFirstSelect=Recno()
		Endif

Enddefine

Define Class clsImage_PlusMinus As Container
    * --- Classe immagine per zoom
    BorderWidth=1
    BackStyle = 1
	Width = 18
	Height=18
	Visible=.t.

	Add Object oImg_PlusMinus As Image With Top=1, Left=1, width=16, Height=16, Visible=.t.
	
	Procedure Init
		This.oImg_PlusMinus.PictureVal = ThisForm.cBmp2
    SetCursor(LoadCursor(0,32512))
	EndPRoc

  Proc MouseDown
    LPARAMETERS nButton, nShift, nXCoord, nYCoord
    IF nButton=1
      this.Parent.Parent.Parent.Parent.nodeClick
    ENDIF
  EndPRoc

  Proc oImg_PlusMinus.MouseDown
    LPARAMETERS nButton, nShift, nXCoord, nYCoord
    IF nButton=1
      this.Parent.MouseDown(nButton, nShift, nXCoord, nYCoord)
    ENDIF
  EndPRoc

  Proc BackStyle_Access
      SetCursor(LoadCursor(0,32512))
      This.oImg_PlusMinus.PictureVal = IIF(EXPANDED='S',ThisForm.cBmp2, ThisForm.cBmp1)
  	  This.BackColor = IIF(CCHDRLVL="S", ThisForm.nThemeColorLevel, IIF(CCHEADER="S", ThisForm.nThemeColor, This.BackColor))
	    This.BorderColor = IIF(CCHDRLVL="S", ThisForm.nThemeColorLevel, IIF(CCHEADER="S", ThisForm.nThemeColor, This.BackColor))
      SetCursor(LoadCursor(0,32512))
      return This.BackStyle
  endproc

Enddefine

Define Class clsImage_FrecciaDX As Container
    * --- Classe immagine per zoom
	BorderWidth = 0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.

	Add Object oImg_FrecciaDX As Image With Top=1, Left=1, width=16, Height=16, Visible=.t.
	
	Procedure Init
		This.oImg_FrecciaDX.PictureVal = ThisForm.cBmp3
	EndPRoc

  Proc BackStyle_Access
      SetCursor(LoadCursor(0,32512))
      This.oImg_FrecciaDX.PictureVal = ThisForm.cBmp3
      SetCursor(LoadCursor(0,32512))
      Return This.BackStyle
  endproc

Enddefine


Define Class ConfTextBox As TextBox
Themes=.f.
BackStyle=0
BorderStyle=0
SpecialEffect=1
Margin=0
enabled=.f.    

  Proc Init
    This.DisabledBackColor = This.BackColor
    This.DisabledForeColor = This.ForeColor
  EndProc

	  Proc DblClick
	    This.Parent.DblClick
      * Parent.nodeClick  
	  EndProc

 * Proc DblClick
 *     this.parent.parent.parent.Parent.nodeClick
 * EndProc

EndDefine


Define Class ConfParamTextBox As ConfTextBox
oContained=.null.
OldValue=0
CurrValue = 0
MinValue=0
MaxValue=0
enabled=.T.    

  Proc Init
    Dodefault()
    This.oContained = ThisForm && This.Parent.Parent.Parent.Parent.Parent.Parent.oContained
  EndProc
  
  Proc GotFocus()
    This.OldValue = Eval(This.ControlSource)
    This.MinValue=ccvalmin
    This.MaxValue=ccvalmax
  EndProc

  Proc When()
  
  EndProc

  Proc Valid()
    local i_bUpd, i_nRes, i_bErr
    
    This.CurrValue = This.Value && Eval(This.ControlSource)
    
    i_bErr = This.CurrValue < This.MinValue or This.CurrValue > This.MaxValue

    If i_bErr
       This.Value = This.OldValue
    endif

    i_bUpd = This.OldValue<>This.CurrValue 
    i_nRes=Iif(i_bUpd,0,1)
    
    If Not(i_bErr) And i_bUpd
        i_nRes=1
				* --- lasciato per compatibilita'
        * This.oContained.w_CCQTA = Eval(This.ControlSource)
				This.oContained.NotifyEvent('w_CCQTA Changed')
    EndIf
        
    Return(i_nRes)
  EndProc
  

EndDefine

Define Class clsCntBorder As Container
	* --- Classe immagine per zoom
	BorderWidth=0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.

	Add Object oImageDx as clsImage_FrecciaDX with left=1, top=3, width=10, height=10, BackStyle=0
  
Enddefine

Define Class clsBorder As Shape
	* --- Classe Shape per catturare gli eventi
	BorderStyle=0    
	BorderWidth=0
	BackStyle = 0
	Width = 18
	Height=18
	Visible=.t.
	Enabled=.t.

	Proc DblClick
	    This.Parent.Parent.Parent.Parent.nodeClick  
	EndProc

	Proc RightClick
	    This.Parent.Parent.Parent.Parent.RightClick
	EndProc

Enddefine

Define Class HdrlVLGroup As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	clblControlSource="DESCRITV"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cImgControlSource="EXPANDED"	&& Mi dice se il nodo � aperto oppure chiuso
	cHdrControlSource="CCHDRLVL"	&& Mi dice se sono un header
	lnBackColor = Rgb(179,7,27)

	Add Object oCntBorder as clsCntBorder
	
	Procedure Init
		local l_clblControlSource, l_Column

		This.oCntBorder.AddObject("oImgPM", "clsImage_PlusMinus")
		This.oCntBorder.AddObject("oLblGrp", "ConfTextBox")
		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")

		This.cCursor = This.Parent.Parent.Parent.cCursor
		m.l_clblControlSource = This.cCursor + "." + This.clblControlSource

		This.oCntBorder.oLblGrp.ControlSource = This.clblControlSource && m.l_clblControlSource

		This.ResizeContainer()

		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = Thisform.nThemeColorLevel
    
		* This.oCntBorder.Visible=.t.
		This.oCntBorder.oImgPM.Visible=.t.
		This.oCntBorder.oLblGrp.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.

		l_Column = This.Parent

		BINDEVENT(m.l_Column,"Width",This,"ResizeContainer", 1)
	EndProc
  
	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
		This.Move(0, 0, max(0, This.Parent.Width), max(0, This.Parent.Parent.RowHeight))
		This.Draw()
		
		This.oCntBorder.Move(0, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), max(0, This.Parent.Parent.RowHeight - 1))
		This.oCntBorder.Draw()		
		
		This.oCntBorder.oImgPM.Move(0, 0, 18, 18)
    
		* This.oCntBorder.oImgPM.left=0
		* This.oCntBorder.oImgPM.Top=0
		* This.oCntBorder.oImgPM.Width = 18
		* This.oCntBorder.oImgPM.Height = This.Height
		* This.oCntBorder.oImgPM.BackColor = This.lnBackColor
		* This.oCntBorder.oImgPM.BorderColor = This.lnBackColor
		
		This.oCntBorder.oLblGrp.BackStyle=0
		This.oCntBorder.oLblGrp.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblGrp.DisabledForeColor = ThisForm.nThemeColorLevel
		*This.oCntBorder.oLblGrp.Top= 1
		*This.oCntBorder.oLblGrp.Left = 18
		*This.oCntBorder.oLblGrp.Width = max(0, This.Parent.Width - This.oCntBorder.oLblGrp.Left - 2)
		*This.oCntBorder.oLblGrp.Height = max(0, This.Height - This.oCntBorder.oLblGrp.Top - 2)
		This.oCntBorder.oLblGrp.Move(18 , 1, max(0, This.Parent.Width - This.oCntBorder.oLblGrp.Left - 2), max(0, This.Height - This.oCntBorder.oLblGrp.Top - 2))
		This.oCntBorder.oLblGrp.FontBold=.t.

		This.oCntBorder.oShapeEvent.Move(18 , 0, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top)
		This.Refresh()
	EndProc
  
  
	Proc DblClick
	    This.Parent.Parent.nodeClick  
	EndProc
    
EndDefine


Define Class HdrRowGroup As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	clblControlSource="DESCRITV"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cImgControlSource="EXPANDED"	&& Mi dice se il nodo � aperto oppure chiuso
	cHdrControlSource="CCHEADER"	&& Mi dice se sono un header
	lnBackColor = RGB(0,146,201)
	lnBackColorSel = RGB(137,89,171)
	
	Add Object oCntBorder as clsCntBorder

	Procedure Init
		local l_clblControlSource
		
		This.oCntBorder.AddObject("oImgPM", "clsImage_PlusMinus")
		This.oCntBorder.AddObject("oLblGrp", "ConfTextBox")
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")
		

		This.cCursor = This.Parent.Parent.Parent.cCursor
		m.l_clblControlSource = This.cCursor + "." + This.clblControlSource	
		
		This.oCntBorder.oLblGrp.ControlSource = This.clblControlSource && m.l_clblControlSource
 
 
		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		This.oCntBorder.BackColor = This.Parent.BackColor
		This.oCntBorder.BorderColor = Thisform.nThemeColor

		* This.oCntBorder.Visible=.t.
		This.oCntBorder.oImgPM.Visible=.t.
		This.oCntBorder.oLblGrp.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.
		
		l_Column = This.Parent

		BINDEVENT(m.l_Column,"Width",This,"ResizeContainer", 1)
		
	EndProc
  
	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
		This.Left=0
		This.Top=0
		This.Width = This.Parent.Width
		This.Height = This.Parent.Parent.RowHeight
		This.Draw()
    
		This.oCntBorder.left=18
		This.oCntBorder.Top=0
		This.oCntBorder.Width = max(0, This.Parent.Width - This.oCntBorder.Left - 1)
		This.oCntBorder.Height = This.Parent.Parent.RowHeight - 1
		This.oCntBorder.Draw()
		
		This.oCntBorder.oImgPM.Move(0, 0, 18, This.Height)
		This.oCntBorder.oImgPM.BackColor = This.lnBackColor
		This.oCntBorder.oImgPM.BorderColor = This.lnBackColor		
  
		* This.oCntBorder.oLblGrp.BackStyle=0
		This.oCntBorder.oLblGrp.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblGrp.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		
		This.oCntBorder.oLblGrp.Move(18, 1, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top - 2)
		This.oCntBorder.oLblGrp.FontBold=.t.
		
		This.oCntBorder.oShapeEvent.Move(18 , 0, max(0, This.Parent.Width - This.oCntBorder.Left - This.oCntBorder.oLblGrp.Left - 2), This.Height - This.oCntBorder.oLblGrp.Top )
		This.oCntBorder.Refresh()
		This.Refresh()

	EndProc

	  Proc DblClick
	    This.Parent.Parent.nodeClick  
	  EndProc 
  
EndDefine

Define Class DtlCarFisse As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	cChkControlSource="XCHK"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cValueControlSource="CPROWORD"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	lnBackColor = RGB(0,146,201)
	
	Add Object oCntBorder as clsCntBorder
  
	* Add Object oCntBlank1 As Container       With Top=0, Left=0, width=18, Heigth=16, Visible=.t.
	* Add Object oCntBlank2 As Container       With Top=0, Left=18, width=18, Heigth=16, Visible=.t.
	* Add Object oChkCar as ConfCheckBox       With Top=0, Left=37, Width=26,  Visible=.t., Caption=""
	* Add Object oLblValue as ConfTextBox      With Top=0, Left=64, Width=50,  Visible=.t.
	* Add Object oLblDescri as ConfTextBox     With Top=0, Left=115, Width=350, Visible=.t.
    
	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource
		
		This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oLblValue", "ConfTextBox")		
		This.oCntBorder.AddObject("oLblDescri", "ConfTextBox")		
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")

		This.cCursor = This.Parent.Parent.Parent.cCursor
    
		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = This.cChkControlSource &&  m.l_cChkControlSource
		This.oCntBorder.oLblValue.ControlSource = This.cValueControlSource && m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ This.cDettagControlSource + ')'

		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		This.oCntBorder.BackColor = This.Parent.BackColor
		This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oChkCar.Visible=.t.
		This.oCntBorder.oLblValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.

    
		local l_Column, l_Grid
		
		l_Column = This.Parent
		l_Grid = This.Parent.Parent

		BINDEVENT(m.l_Column,"Width",This,"ResizeContainer", 1)
	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc RecordSource
		This.cCursor = This.Parent.Parent.Parent.cCursor
    
		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = This.cChkControlSource &&  m.l_cChkControlSource
		This.oCntBorder.oLblValue.ControlSource = This.cValueControlSource && m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ This.cDettagControlSource + ')'
	EndPRoc
  
	Proc ResizeContainer		
		*This.Left
		*This.Top=0
		*This.Width = This.Parent.Width
		*This.Height = This.Parent.Parent.RowHeight+1
		This.Move(0, 0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.Draw()
		This.Refresh()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()
		This.oCntBorder.Refresh()
    
		This.oCntBorder.oChkCar.BackStyle=0
		This.oCntBorder.oChkCar.Move(18, 1, 26)
		This.oCntBorder.oChkCar.Caption=""

		This.oCntBorder.oLblValue.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblValue.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		
		This.oCntBorder.oLblValue.Move(35, 1, 50, This.oCntBorder.Height - This.oCntBorder.oLblValue.Top - 1)
		This.oCntBorder.oLblValue.InputMask = "999"
		This.oCntBorder.oLblValue.Alignment=0    
    
		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		This.oCntBorder.oLblDescri.Move(86, 1, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
	EndProc
	
EndDefine


Define Class DtlCarParam As Container
	Left=0
	Top=0
	Width = 500
	Height = 19
	BackStyle=0
	BorderWidth=0
	cCursor=""
	cValueControlSource="CPROWORD"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	cValMinControlSource="CCVALMIN"	&& Mi dice se il nodo � aperto oppure chiuso
	cValMaxControlSource="CCVALMAX"	&& Mi dice se il nodo � aperto oppure chiuso

	lnBackColor = RGB(0,146,201)
	
	Add Object oCntBorder as clsCntBorder	

	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource
		local L_cValMinControlSource, l_cValMaxControlSource

		* This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oTextValue", "ConfParamTextBox")		
		This.oCntBorder.AddObject("oLblDescri", "ConfTextBox")			
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")
		
		This.cCursor = This.Parent.Parent.Parent.cCursor

		l_cValueControlSource = This.cCursor + "." + This.cValueControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		L_cValMinControlSource = This.cCursor + "." + This.cValMinControlSource
		l_cValMaxControlSource = This.cCursor + "." + This.cValMaxControlSource
	  
		This.oCntBorder.oTextValue.ControlSource = m.l_cValueControlSource
		This.oCntBorder.oLblDescri.ControlSource =  'cp_MemoFirstLine('+ m.l_cDettagControlSource + ')'

		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oTextValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.
		
		l_Column = This.Parent

		BINDEVENT(m.l_Column,"Width",This,"ResizeContainer", 1)
	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer		
		*This.Left
		*This.Top=0
		*This.Width = This.Parent.Width
		*This.Height = This.Parent.Parent.RowHeight+1
		This.Move(0,0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.BackColor = This.Parent.Parent.BackColor
		This.BorderColor = This.Parent.Parent.BackColor		
		This.Draw()
		This.Refresh()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()
		This.oCntBorder.Refresh()    
    
		This.oCntBorder.oTextValue.Enabled = .T.
		This.oCntBorder.oTextValue.BorderStyle = 1
		This.oCntBorder.oTextValue.BorderColor = This.lnBackColor
		This.oCntBorder.oTextValue.Move(18, 1, 101, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oTextValue.BackColor = Rgb(255,255,255)
		This.oCntBorder.oTextValue.ForeColor = This.lnBackColor
		* This.oCntBorder.oTextValue.Alignment=0    
		
		This.oCntBorder.oLblDescri.Move(125, 1, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255)
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor
		This.oCntBorder.oShapeEvent.Move(125, 1, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)

	EndProc
	

EndDefine

Define Class MyStdButton as CPCommandButton   
	ImgSize=10
	CpPicture="Caratter.ico"
	Caption=""

	Procedure Click
    ThisForm.NotifyEvent("OPENCARDESC")
	endproc

EndDefine

Define Class ConfEditBox As EditBox
Themes=.f.
BackStyle=0
BorderStyle=0
SpecialEffect=1
Margin=0
enabled=.f.
ScrollBars=0

  Proc Init
    This.DisabledBackColor = This.BackColor
    This.DisabledForeColor = This.ForeColor
  EndProc

  Proc DblClick
	This.Parent.DblClick
  EndProc

EndDefine

Define Class DtlCarDescr As Container
	BackStyle=0
	BorderWidth=0
	cCursor=""
	lnBackColor = RGB(0,146,201)
	
	cChkControlSource="XCHK"		&& Descrizione del gruppo (Caratteristiche fisse, parametriche, descrittive)
	cEnabControlSource="ENABLED"	&& Mi dice se il nodo � aperto oppure chiuso
	cDettagControlSource="CCDETTAG"	&& Mi dice se il nodo � aperto oppure chiuso
	
	Add Object oCntBorder as clsCntBorder
	
	Procedure Init
		local l_cChkControlSource, l_cEnabControlSource, l_cValueControlSource, l_cDettagControlSource


		This.oCntBorder.AddObject("oChkCar", "ConfCheckBox")
		This.oCntBorder.AddObject("oBtnValue", "MyStdButton")		
		This.oCntBorder.AddObject("oLblDescri", "ConfEditBox")		
 		This.oCntBorder.AddObject("oShapeEvent", "clsBorder")		
		
		This.cCursor = This.Parent.Parent.Parent.cCursor

		l_cChkControlSource = This.cCursor + "." + This.cChkControlSource
		l_cEnabControlSource = This.cCursor + "." + This.cEnabControlSource
		l_cDettagControlSource = This.cCursor + "." + This.cDettagControlSource
		
		This.oCntBorder.oChkCar.ControlSource = m.l_cChkControlSource
		This.oCntBorder.oLblDescri.ControlSource = m.l_cDettagControlSource
		* --- 'cp_MemoFirstLine('+ m.l_cDettagControlSource + ')'
    
		This.ResizeContainer()
		
		*--- Imposto il colore di sfondo del container come quello della colonna
		This.BackColor = This.Parent.BackColor
		This.BorderColor = This.Parent.BackColor
		*--- Imposto il colore del bordo del contenitore figlio
		* This.oCntBorder.BackColor = This.Parent.BackColor
		* This.oCntBorder.BorderColor = This.Parent.BackColor && This.lnBackColor

		This.oCntBorder.Visible=.t.
		This.oCntBorder.oChkCar.Visible=.t.
		This.oCntBorder.oBtnValue.Visible=.t.
		This.oCntBorder.oLblDescri.Visible=.t.
		This.oCntBorder.oShapeEvent.Visible=.t.
		
		l_Column = This.Parent

		BINDEVENT(m.l_Column,"Width",This,"ResizeContainer", 1)
	EndProc

	  Proc Resize
	    This.ResizeContainer()
	  EndProc
  
	Proc ResizeContainer
 		This.Move(0,0, This.Parent.Width, This.Parent.Parent.RowHeight)
		This.BackColor = This.Parent.Parent.BackColor
		This.BorderColor = This.Parent.Parent.BackColor
		This.Draw()
		This.Refresh()
    
		This.oCntBorder.Move(37, 0, max(0, This.Parent.Width - This.oCntBorder.Left - 1), This.Parent.Parent.RowHeight - 1)
		This.oCntBorder.Draw()
		This.oCntBorder.Refresh()

		This.oCntBorder.oChkCar.BackStyle=0
		This.oCntBorder.oChkCar.Move(18, 1, 18)
		This.oCntBorder.oChkCar.Caption=""

		This.oCntBorder.oBtnValue.Move(37, 1, 16 , 16) && This.Parent.Parent.RowHeight)

		This.oCntBorder.oLblDescri.DisabledBackColor = Rgb(255,255,255) && This.oLblGrp.BackColor
		This.oCntBorder.oLblDescri.DisabledForeColor = This.lnBackColor && This.oLblGrp.ForeColor
		This.oCntBorder.oLblDescri.Move(56, 0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
		This.oCntBorder.oShapeEvent.Move(56, 0, This.oCntBorder.Width - This.oCntBorder.oLblDescri.Left - 1, This.oCntBorder.Height - This.oCntBorder.oLblDescri.Top - 1)
	
	EndProc
	


EndDefine

* --- Fine Area Manuale
