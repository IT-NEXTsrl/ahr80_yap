* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bkk                                                        *
*              Gestione caratteristiche                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-28                                                      *
* Last revis.: 2014-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bkk",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_bkk as StdBatch
  * --- Local variables
  w_OPER = space(10)
  o_CPROWORD = 0
  w_CONTA = 0
  w_MESS = space(50)
  w_ROWORD = 0
  w_NUMRIGA = 0
  w_DETTCAR = space(40)
  w_CODICE = space(5)
  * --- WorkFile variables
  CONFDART_idx=0
  CONFDDIS_idx=0
  CONFDCAR_idx=0
  MODE_ART_idx=0
  MODE_DIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Caratteristiche (da GSDS_MCC)
    do case
      case this.oParentObject.w_CCTIPOCA = "C"
        * --- --Quando si inserisce una nuova scelta, questa viene riportata in tutti i legami 
        *     con gli articoli che la contengono.
        do case
          case this.w_OPER = "INSERT"
            * --- Insert into CONFDART
            i_nConn=i_TableProp[this.CONFDART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\CCAR\EXE\QUERY\GSCR_BKK",this.CONFDART_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          case this.w_OPER = "DELETE"
            * --- --La cancellazione viene consentita solo nel caso in cui la caratteristica
            *     non � impegata 
            this.w_CONTA = 0
            * --- Select from CONFDDIS
            i_nConn=i_TableProp[this.CONFDDIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CC__DIBA  from "+i_cTable+" CONFDDIS ";
                  +" where CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_CCTIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_CCCODICE)+"";
                  +" order by CC__DIBA";
                   ,"_Curs_CONFDDIS")
            else
              select CC__DIBA from (i_cTable);
               where CCTIPOCA=this.oParentObject.w_CCTIPOCA AND CCCODICE=this.oParentObject.w_CCCODICE;
               order by CC__DIBA;
                into cursor _Curs_CONFDDIS
            endif
            if used('_Curs_CONFDDIS')
              select _Curs_CONFDDIS
              locate for 1=1
              do while not(eof())
              this.w_CONTA = this.w_CONTA+1
                select _Curs_CONFDDIS
                continue
              enddo
              use
            endif
            if this.w_CONTA>0
              this.w_MESS = "Cancellazione non consentita: caratteristica impiegata in almeno una distinta"
              ah_ErrorMsg(this.w_MESS,16)
            else
              * --- Delete from CONFDART
              i_nConn=i_TableProp[this.CONFDART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_CCTIPOCA);
                      +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                      +" and CPROWORD = "+cp_ToStrODBC(this.oParentObject.w_CPROWORD);
                       )
              else
                delete from (i_cTable) where;
                      CCTIPOCA = this.oParentObject.w_CCTIPOCA;
                      and CCCODICE = this.oParentObject.w_CCCODICE;
                      and CPROWORD = this.oParentObject.w_CPROWORD;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            endif
          case this.w_OPER = "UPDATE"
            * --- --La modifica di una scelta della catatteristica viene riportata solo nei legami con l'articolo
            * --- Read from CONFDCAR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONFDCAR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONFDCAR_idx,2],.t.,this.CONFDCAR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPROWORD"+;
                " from "+i_cTable+" CONFDCAR where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                    +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_CCTIPOCA);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPROWORD;
                from (i_cTable) where;
                    CCCODICE = this.oParentObject.w_CCCODICE;
                    and CCTIPOCA = this.oParentObject.w_CCTIPOCA;
                    and CPROWNUM = this.oParentObject.w_CPROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.o_CPROWORD = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Write into CONFDART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CONFDART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CONFDART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'CONFDART','CPROWORD');
              +",CCDETTAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCDETTAG),'CONFDART','CCDETTAG');
                  +i_ccchkf ;
              +" where ";
                  +"CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_CCTIPOCA);
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                  +" and CPROWORD = "+cp_ToStrODBC(this.o_CPROWORD);
                     )
            else
              update (i_cTable) set;
                  CPROWORD = this.oParentObject.w_CPROWORD;
                  ,CCDETTAG = this.oParentObject.w_CCDETTAG;
                  &i_ccchkf. ;
               where;
                  CCTIPOCA = this.oParentObject.w_CCTIPOCA;
                  and CCCODICE = this.oParentObject.w_CCCODICE;
                  and CPROWORD = this.o_CPROWORD;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.w_OPER = "SEARCH"
            vx_exec("..\CCAR\EXE\QUERY\GSCR_KFC.VZM",this)
            if this.w_NUMRIGA>0 and not empty(nvl(this.w_DETTCAR, " "))
              With this.oParentObject
              select(.ctrsname) 
 lRow = recno() 
 locate for t_CCDETTAG=this.w_DETTCAR And cprownum=this.w_NUMRIGA
              if !Found()
                select(.ctrsname) 
 go lRow
              else
                .oPgFrm.Page1.oPag.oBody.SetCurrentRow() 
 .SetControlsValue() 
 .LockScreen = .f. 
 .oPgFrm.Page1.oPag.oBody.Refresh()
              endif
              EndWith
            endif
        endcase
      case this.oParentObject.w_CCTIPOCA = "M" and this.w_OPER = "UPDATE"
        * --- Write into MODE_ART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MODE_ART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODE_ART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MODE_ART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCVAR101 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR101),'MODE_ART','CCVAR101');
          +",CCVAR102 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR102),'MODE_ART','CCVAR102');
          +",CCVAR103 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR103),'MODE_ART','CCVAR103');
          +",CCVAR104 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR104),'MODE_ART','CCVAR104');
          +",CCVAR105 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR105),'MODE_ART','CCVAR105');
          +",CCVAR106 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR106),'MODE_ART','CCVAR106');
          +",CCVAR107 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR107),'MODE_ART','CCVAR107');
          +",CCVAR108 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR108),'MODE_ART','CCVAR108');
          +",CCVAR109 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR109),'MODE_ART','CCVAR109');
          +",CCVAR110 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR110),'MODE_ART','CCVAR110');
          +",CCVAR111 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR111),'MODE_ART','CCVAR111');
          +",CCVAR112 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR112),'MODE_ART','CCVAR112');
          +",CCVAR113 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR113),'MODE_ART','CCVAR113');
          +",CCVAR114 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR114),'MODE_ART','CCVAR114');
          +",CCVAR115 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCVAR115),'MODE_ART','CCVAR115');
              +i_ccchkf ;
          +" where ";
              +"CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_CCTIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CCCODICE);
                 )
        else
          update (i_cTable) set;
              CCVAR101 = this.oParentObject.w_CCVAR101;
              ,CCVAR102 = this.oParentObject.w_CCVAR102;
              ,CCVAR103 = this.oParentObject.w_CCVAR103;
              ,CCVAR104 = this.oParentObject.w_CCVAR104;
              ,CCVAR105 = this.oParentObject.w_CCVAR105;
              ,CCVAR106 = this.oParentObject.w_CCVAR106;
              ,CCVAR107 = this.oParentObject.w_CCVAR107;
              ,CCVAR108 = this.oParentObject.w_CCVAR108;
              ,CCVAR109 = this.oParentObject.w_CCVAR109;
              ,CCVAR110 = this.oParentObject.w_CCVAR110;
              ,CCVAR111 = this.oParentObject.w_CCVAR111;
              ,CCVAR112 = this.oParentObject.w_CCVAR112;
              ,CCVAR113 = this.oParentObject.w_CCVAR113;
              ,CCVAR114 = this.oParentObject.w_CCVAR114;
              ,CCVAR115 = this.oParentObject.w_CCVAR115;
              &i_ccchkf. ;
           where;
              CCTIPOCA = this.oParentObject.w_CCTIPOCA;
              and CCCODICE = this.oParentObject.w_CCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONFDART'
    this.cWorkTables[2]='CONFDDIS'
    this.cWorkTables[3]='CONFDCAR'
    this.cWorkTables[4]='MODE_ART'
    this.cWorkTables[5]='MODE_DIS'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
