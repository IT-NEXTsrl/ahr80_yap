* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bmc                                                        *
*              Selezione rapida caratteristiche                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-03                                                      *
* Last revis.: 2015-04-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bmc",oParentObject,m.pAzione)
return(i_retval)

define class tgscr_bmc as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  GSDB_MLD = .NULL.
  GSDB_MLC = .NULL.
  NC = space(10)
  TRSCODICE = space(5)
  TRSDESCRI = space(40)
  TRSDETTAG = space(40)
  TRSROW = 0
  TRSORD = 0
  TRSDEFA = space(1)
  TRSOPER = space(1)
  TRSCOEFF = 0
  w_MODIF = space(1)
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  w_DIBA = space(1)
  w_CONTACAR = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione Rapida Caratteristiche (Da GSDB_KMC)
    * --- Riferimento al padre (GSCR_KMC)
    this.Padre = this.oParentObject
    * --- Riferimento al cursore dello zoom del padre
    this.NC = this.Padre.w_ZoomSel.cCursor
    * --- Riferimento al padre del padre (GSDB_MLD/GSMA_MAD)
    this.GSDB_MLD = this.Padre.oParentObject
    * --- Riferimento al padre di GSDB_MLD (GSDB_MLC/GSMA_MAC)
    this.GSDB_MLC = this.GSDB_MLD.oParentObject
    * --- Padre chiamante -Articoli o Distinta Base- (Se w_DIBA='S' GSMA_MAD, altrimenti GSDB_MLD)
    this.w_DIBA = iif(alltrim(upper(this.GSDB_MLD.Class))="TCGSMA_MAD","N","S")
    this.w_MODIF = "N"
    this.w_CONTACAR = 0
    if this.pAzione $ "AG-IN"
      * --- Verifica validit� dati inseriti
      select (this.NC) 
 locate for xchk=1 and (! upper(CCDEFAULT) $ "SN" or (! empty(CCOPERA) and ! CCOPERA $ "X/"))
      if found()
        ah_ErrorMsg("Inserire S o N sul default e X o / o space sull'operatore",48)
        i_retcode = 'stop'
        return
      endif
    endif
    do case
      case this.pAzione = "AG"
        select (this.NC) 
 scan for xchk=1
        this.w_MODIF = "S"
        this.TRSDETTAG = CCDETTAG
        this.TRSROW = CPROWNUM
        this.TRSDEFA = upper(CCDEFAULT)
        this.TRSOPER = CCOPERA 
        this.TRSCOEFF = CCCOEFF
        this.w_CONTACAR = this.w_CONTACAR+1
        if this.w_DIBA="S"
          Select (this.GSDB_MLD.ctrsname)
          locate for t_CCDETTAG=this.TRSDETTAG
          * --- locate for  t_CCDETTAG=TRSDETTAG and CPROWNUM=TRSROW
          this.w_RECPOS = recno()
          if t_CC__DEFA=1 and upper(this.TRSDEFA)<>"S"
            Select (this.GSDB_MLD.ctrsname) 
 locate for t_cc__defa=1 and recno()<>this.w_RECPOS
            if ! found()
              ah_ErrorMsg("Impossibile eliminare l'unico default presente sulla caratteristica",48)
              this.TRSDEFA = "S"
              this.w_CONTACAR = this.w_CONTACAR-1
            endif
            Select (this.GSDB_MLD.ctrsname) 
 go this.w_RECPOS
          endif
        endif
        update (this.GSDB_MLD.ctrsname) set ; 
 t_CC__DEFA=iif(this.TRSDEFA="N",0,1), t_DEFA=iif(this.TRSDEFA="N",0,1), t_CCOPERAT=iif(empty(this.TRSOPER),0,iif(this.TRSOPER="X",1,2)) , ; 
 t_CC_COEFF=this.TRSCOEFF, I_SRV=iif(empty(I_SRV),"U",I_SRV)where t_CCDETTAG=this.TRSDETTAG
        * --- update (GSDB_MLD.ctrsname) set ;
        *     t_CC__DEFA=iif(TRSDEFA='N',0,1), t_CCOPERAT=iif(empty(TRSOPER),0,iif(TRSOPER='X',1,2)) , t_CC_COEFF=TRSCOEFF ;
        *     where t_CCDETTAG=TRSDETTAG and CPROWNUM=TRSROW
        select (this.NC)
        if this.w_MODIF="S"
          this.GSDB_MLD.oPgFrm.Page1.oPag.oBody.Refresh()     
          this.GSDB_MLD.WorkFromTrs()     
          this.GSDB_MLD.SaveDependsOn()     
          this.GSDB_MLD.SetControlsValue()     
          this.GSDB_MLD.mHideControls()     
          this.GSDB_MLD.ChildrenChangeRow()     
        endif
        endscan
        if this.w_CONTACAR>0
          ah_ErrorMsg("Aggiornamento caratteristiche eseguito con successo",48)
        endif
      case this.pAzione = "IN"
        * --- Prima di effettuare l'aggiornamento verifico la presenza dei default sulle righe delle caratteristiche nuove (se ce ne sono) - Solo se vengo dalla distinta 
        if this.w_DIBA="S"
          Select distinct cccodice from (this.NC) where xchk=1 and cccodice not in (Select t_cccodice from (this.GSDB_MLC.ctrsname)) ; 
 and cccodice not in (Select cccodice from (this.NC) where upper(CCDEFAULT)="S") into cursor ccdefault
          if used("ccdefault")
            if reccount("ccdefault")>0
              ah_ErrorMsg("Inserire almeno un default sulle caratteristiche aggiunte",48)
              i_retcode = 'stop'
              return
            endif
            use in ccdefault
          endif
        endif
        select (this.NC) 
 scan for xchk=1
        riga=recno()
        this.TRSDETTAG = CCDETTAG
        this.TRSDESCRI = CCDESCRI
        this.TRSROW = CPROWNUM
        this.TRSCODICE = CCCODICE
        * --- Verifico se la caratteristica esiste gi� in GSDB_MLC.
        *     Se esiste inserisco i dati su di essa, altrimenti creo la nuova riga di testata per la caratteristica in esame.
        Select (this.GSDB_MLC.ctrsname) 
 locate for t_cccodice=this.TRSCODICE
        if found()
          * --- Mi posiziono sulla riga ed eseguo un refresh del dettaglio
          Select (this.GSDB_MLC.ctrsname)
          this.GSDB_MLC.oPgFrm.Page1.oPag.oBody.Refresh()     
          this.GSDB_MLC.WorkFromTrs()     
          this.GSDB_MLC.SaveDependsOn()     
          this.GSDB_MLC.SetControlsValue()     
          this.GSDB_MLC.mHideControls()     
          this.GSDB_MLC.ChildrenChangeRow()     
          this.GSDB_MLC.bUpdated = .t.
          this.GSDB_MLC.oPgFRm.Page1.oPag.oBody.oBodyCol.SetFocus()     
        else
          * --- Carico la nuova caratteristica
          Select (this.GSDB_MLC.ctrsname) 
 go bottom
          if empty(nvl(t_cccodice," "))
            * --- Uso la riga di append vuota gi� presente
            if this.w_DIBA="S"
              replace t_cccodice with this.TRSCODICE, t_ccdescri with this.TRSDESCRI,t_ccdatini with i_datsys,t_ccdatfin with cp_CharToDate("31-12-2099")
            else
              replace t_cccodice with this.TRSCODICE, t_ccdescri with this.TRSDESCRI
            endif
            * --- Eseguo un refresh del dettaglio
            this.GSDB_MLC.oPgFrm.Page1.oPag.oBody.Refresh()     
            this.GSDB_MLC.WorkFromTrs()     
            this.GSDB_MLC.SaveDependsOn()     
            this.GSDB_MLC.SetControlsValue()     
            this.GSDB_MLC.mHideControls()     
            this.GSDB_MLC.ChildrenChangeRow()     
            this.GSDB_MLC.oPgFRm.Page1.oPag.oBody.oBodyCol.SetFocus()     
            this.GSDB_MLC.bUpdated = .t.
          else
            * --- Creo una nuova riga
            this.GSDB_MLC.InitRow()     
            this.GSDB_MLC.w_CCCODICE = this.TRSCODICE
            this.GSDB_MLC.w_CCDESCRI = this.TRSDESCRI
            if this.w_DIBA="S"
              this.GSDB_MLC.w_CCDATINI = i_datsys
              this.GSDB_MLC.w_CCDATFIN = cp_CharToDate("31-12-2099")
            endif
            this.w_AbsRow = this.GSDB_MLC.oPgFrm.Page1.oPag.oBody.nAbsRow
            this.w_nRelRow = this.GSDB_MLC.oPgFrm.Page1.oPag.oBody.nRelRow
            Select (this.GSDB_MLC.ctrsname)
            this.w_RECPOS = IIF( Eof() , RECNO(this.GSDB_MLC.cTrsName)-1 , RECNO(this.GSDB_MLC.cTrsName) )
            * --- Eseguo un refresh del dettaglio
            * --- Questa Parte derivata dal Metodo LoadRec
            Select (this.GSDB_MLC.ctrsname)
            GO this.w_RECPOS
            With this.GSDB_MLC
            .TrsFromWork()
            .SaveDependsOn()
            .SetControlsValue()
            .ChildrenChangeRow()
            .oPgFrm.Page1.oPag.oBody.Refresh()
            .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_AbsRow
            .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_nRelRow
            EndWith
            this.GSDB_MLC.oPgFRm.Page1.oPag.oBody.oBodyCol.SetFocus()     
          endif
        endif
        * --- Aggiorno le scelte relative alla caratteristica selezionata o inserita
        select (this.NC) 
 go riga
        this.TRSDETTAG = CCDETTAG
        this.TRSROW = CPROWNUM
        this.TRSORD = CPROWORD
        this.TRSDEFA = upper(CCDEFAULT)
        this.TRSOPER = CCOPERA
        this.TRSCOEFF = CCCOEFF
        * --- Verifico se la riga � gi� presente
        Select (this.GSDB_MLD.ctrsname) 
 conta=0 
 count for t_ccdettag=this.TRSDETTAG to conta
        * --- count for t_ccdettag=TRSDETTAG and CPROWNUM=TRSROW to conta 
        if conta=0
          * --- Carico le nuove scelte
          Select (this.GSDB_MLD.ctrsname) 
 go bottom
          if empty(nvl(t_ccdettag," "))
            * --- Uso la riga di append vuota gi� presente
            if this.w_DIBA="S"
              replace t_ccdettag with this.TRSDETTAG, t_cproword with this.TRSORD, t_cc__defa with iif(this.TRSDEFA="N",0,1), ; 
 t_defa with iif(this.TRSDEFA="N",0,1), t_ccoperat with iif(empty(this.TRSOPER),0,iif(this.TRSOPER="X",1,2)) , t_cc_coeff with this.TRSCOEFF, ; 
 t_ccdatini with i_datsys, t_ccdatfin with cp_CharToDate("31-12-2099")
            else
              replace t_ccdettag with this.TRSDETTAG, t_cproword with this.TRSORD, t_cc__defa with iif(this.TRSDEFA="N",0,1), ; 
 t_defa with iif(this.TRSDEFA="N",0,1), t_ccoperat with iif(empty(this.TRSOPER),0,iif(this.TRSOPER="X",1,2)) , t_cc_coeff with this.TRSCOEFF
            endif
            * --- Eseguo un refresh del dettaglio
            this.GSDB_MLD.oPgFrm.Page1.oPag.oBody.Refresh()     
            this.GSDB_MLD.WorkFromTrs()     
            this.GSDB_MLD.SaveDependsOn()     
            this.GSDB_MLD.SetControlsValue()     
            this.GSDB_MLD.mHideControls()     
            this.GSDB_MLD.ChildrenChangeRow()     
            this.GSDB_MLD.w_TOTALE = this.GSDB_MLD.w_TOTALE+this.GSDB_MLD.w_DEFA
            this.GSDB_MLD.mCalc(.t.)     
          else
            * --- Creo una nuova riga
            this.GSDB_MLD.InitRow()     
            this.GSDB_MLD.w_CCDETTAG = this.TRSDETTAG
            this.GSDB_MLD.w_CPROWORD = this.TRSORD
            this.GSDB_MLD.w_CC__DEFA = this.TRSDEFA
            this.GSDB_MLD.w_DEFA = iif(this.TRSDEFA="N",0,1)
            this.GSDB_MLD.w_TOTALE = this.GSDB_MLD.w_TOTALE+iif(this.TRSDEFA="N",0,1)
            this.GSDB_MLD.w_CCOPERAT = iif(this.TRSOPER="X","*",this.TRSOPER)
            this.GSDB_MLD.w_CC_COEFF = this.TRSCOEFF
            if this.w_DIBA="S"
              this.GSDB_MLD.w_CCDATINI = i_datsys
              this.GSDB_MLD.w_CCDATFIN = cp_CharToDate("31-12-2099")
            endif
            * --- Eseguo un refresh del dettaglio
            this.GSDB_MLD.oPgFrm.Page1.oPag.oBody.Refresh()     
            this.GSDB_MLD.TrsFromWork()     
            this.GSDB_MLD.SaveDependsOn()     
            this.GSDB_MLD.SetControlsValue()     
            this.GSDB_MLD.mHideControls()     
            this.GSDB_MLD.ChildrenChangeRow()     
            this.GSDB_MLD.mCalc(.t.)     
          endif
          this.w_CONTACAR = this.w_CONTACAR+1
        else
          ah_ErrorMsg("La scelta %1 � gi� presente e verr� scartata",48," ",alltrim(nvl(this.TRSDETTAG," ")))
        endif
        select (this.NC) 
 go riga
        endscan
        * --- Ripristino il Focus sulla maschera
        this.Padre.w_ZOOMSEL.SetFocus()     
        if this.w_CONTACAR>0
          ah_ErrorMsg("Inserimento caratteristiche eseguito con successo",48)
        endif
      case this.pAzione = "EL"
        select (this.NC) 
 scan for xchk=1
        this.TRSDETTAG = CCDETTAG
        this.TRSROW = CPROWNUM
        if this.w_DIBA="S"
          Select (this.GSDB_MLD.ctrsname)
          locate for t_CCDETTAG=this.TRSDETTAG
          * --- locate for  t_CCDETTAG=TRSDETTAG and CPROWNUM=TRSROW
          this.w_RECPOS = recno()
          if t_CC__DEFA=1
            Select (this.GSDB_MLD.ctrsname) 
 locate for t_cc__defa=1 and recno()<>this.w_RECPOS
            if found()
              this.w_MODIF = "S"
            else
              ah_ErrorMsg("Impossibile eliminare l'unico default presente sulla caratteristica",48)
              if this.w_MODIF="S"
                Select (this.GSDB_MLD.ctrsname) 
 go this.w_RECPOS
                this.GSDB_MLD.oPgFrm.Page1.oPag.oBody.Refresh()     
                this.GSDB_MLD.WorkFromTrs()     
                this.GSDB_MLD.SaveDependsOn()     
                this.GSDB_MLD.SetControlsValue()     
                this.GSDB_MLD.mHideControls()     
                this.GSDB_MLD.ChildrenChangeRow()     
              endif
              i_retcode = 'stop'
              return
            endif
            Select (this.GSDB_MLD.ctrsname) 
 go this.w_RECPOS
          else
            this.w_MODIF = "S"
          endif
        else
          this.w_MODIF = "S"
        endif
        delete from (this.GSDB_MLD.ctrsname) where t_CCDETTAG=this.TRSDETTAG
        * --- delete from (GSDB_MLD.ctrsname) where t_CCDETTAG=TRSDETTAG and CPROWNUM=TRSROW
        select (this.NC)
        endscan
        if this.w_MODIF="S"
          select (this.GSDB_MLD.ctrsname)
          COUNT FOR NOT DELETED() TO RIGHEOK
          this.GSDB_MLD.oPgFrm.Page1.oPag.oBody.Refresh()     
          this.GSDB_MLD.WorkFromTrs()     
          this.GSDB_MLD.SaveDependsOn()     
          this.GSDB_MLD.SetControlsValue()     
          this.GSDB_MLD.mHideControls()     
          this.GSDB_MLD.ChildrenChangeRow()     
          if RIGHEOK=0
            this.GSDB_MLD.InitRow()     
          endif
        endif
        ah_ErrorMsg("Eliminazione caratteristiche eseguita con successo",48)
      case this.pAzione = "SS"
        if used(this.NC)
          do case
            case this.oParentObject.w_SELEZI="S"
              * --- Seleziona tutte le righe dello zoom
              UPDATE (this.NC) SET xChk=1
            case this.oParentObject.w_SELEZI="D"
              * --- Deseleziona tutte le righe dello zoom
              UPDATE (this.NC) SET xChk=0
            otherwise
              * --- Inverte Selezione
              UPDATE (this.NC) SET xChk=abs(xChk-1)
          endcase
        endif
      case this.pAzione = "AE"
        if ! empty(this.oParentObject.w_CAOPER) and (empty(this.oParentObject.w_CACOEFF) or NVL(this.oParentObject.w_CACOEFF,0)=0)
          ah_ErrorMsg("Il valore del coefficiente deve essere diverso da 0",48)
          i_retcode = 'stop'
          return
        endif
        * --- Aggiorno il Cursore dello Zoom
        update (this.NC) set CCDEFAULT=this.oParentObject.w_CADEFA,CCOPERA=this.oParentObject.w_CAOPER,CCCOEFF=this.oParentObject.w_CACOEFF where xchk=1
      case this.pAzione = "ST"
        * --- Alla init inizializzo la maschera
        if ! empty(this.GSDB_MLC.w_CCCODICE)
          this.oParentObject.w_CODINI = this.GSDB_MLC.w_CCCODICE
          this.oParentObject.w_CODFIN = this.GSDB_MLC.w_CCCODICE
          this.Padre.NotifyEvent("Interroga")     
          * --- Aggiorno il Default, l'Operatore ed il Coefficiente per le righe gi� presenti in GSDB_MLD
          select (this.GSDB_MLD.ctrsname) 
 scan
          this.TRSDETTAG = t_CCDETTAG
          this.TRSROW = CPROWNUM
          this.TRSDEFA = iif(t_CC__DEFA=0,"N","S")
          this.TRSOPER = iif(t_CCOPERAT=0," ",iif(t_CCOPERAT=1,"X","/"))
          this.TRSCOEFF = t_CC_COEFF
          update (this.NC) set CCDEFAULT= this.TRSDEFA,CCOPERA=this.TRSOPER,CCCOEFF=this.TRSCOEFF,XCHK=1 where CCDETTAG=this.TRSDETTAG
          * --- update (NC) set CCDEFAULT= TRSDEFA,CCOPERA=TRSOPER,CCCOEFF=TRSCOEFF,XCHK=1 where CCDETTAG=TRSDETTAG and CPROWNUM=TRSROW
          endscan
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
