* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_amd                                                        *
*              Caratteristiche descrittive                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-20                                                      *
* Last revis.: 2014-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_amd"))

* --- Class definition
define class tgscr_amd as StdForm
  Top    = 8
  Left   = 12

  * --- Standard Properties
  Width  = 642
  Height = 322+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-14"
  HelpContextID=210335081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CONF_CAR_IDX = 0
  cFile = "CONF_CAR"
  cKeySelect = "CCTIPOCA,CCCODICE"
  cQueryFilter="CCTIPOCA='D'"
  cKeyWhere  = "CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CONF_CAR.CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CONF_CAR.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cPrg = "gscr_amd"
  cComment = "Caratteristiche descrittive"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CC__NOTE = space(0)

  * --- Children pointers
  GSCR_MTD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONF_CAR','gscr_amd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_amdPag1","gscr_amd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Caratteristiche descrittive")
      .Pages(1).HelpContextID = 28233076
      .Pages(2).addobject("oPag","tgscr_amdPag2","gscr_amd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Traduzioni")
      .Pages(2).HelpContextID = 181304699
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONF_CAR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONF_CAR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONF_CAR_IDX,3]
  return

  function CreateChildren()
    this.GSCR_MTD = CREATEOBJECT('stdDynamicChild',this,'GSCR_MTD',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCR_MTD)
      this.GSCR_MTD.DestroyChildrenChain()
      this.GSCR_MTD=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCR_MTD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCR_MTD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCR_MTD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCR_MTD.SetKey(;
            .w_CCCODICE,"DICODICE";
            ,.w_CCTIPOCA,"DITIPOCA";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCR_MTD.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"DICODICE";
             ,.w_CCTIPOCA,"DITIPOCA";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCR_MTD)
        i_f=.GSCR_MTD.BuildFilter()
        if !(i_f==.GSCR_MTD.cQueryFilter)
          i_fnidx=.GSCR_MTD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCR_MTD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCR_MTD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCR_MTD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCR_MTD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
      .w_CCCODICE = NVL(CCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONF_CAR where CCTIPOCA=KeySet.CCTIPOCA
    *                            and CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONF_CAR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONF_CAR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONF_CAR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .w_CC__NOTE = NVL(CC__NOTE,space(0))
        cp_LoadRecExtFlds(this,'CONF_CAR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCTIPOCA = space(1)
      .w_CCCODICE = space(5)
      .w_CCDESCRI = space(40)
      .w_CC__NOTE = space(0)
      if .cFunction<>"Filter"
        .w_CCTIPOCA = "D"
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONF_CAR')
    this.DoRTCalc(2,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCC__NOTE_1_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_2.enabled = .t.
        .Page1.oPag.oCCDESCRI_1_3.enabled = .t.
      endif
    endwith
    this.GSCR_MTD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONF_CAR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCR_MTD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPOCA,"CCTIPOCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CC__NOTE,"CC__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    i_lTable = "CONF_CAR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONF_CAR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONF_CAR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONF_CAR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONF_CAR')
        i_extval=cp_InsertValODBCExtFlds(this,'CONF_CAR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CCTIPOCA)+;
                  ","+cp_ToStrODBC(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_CC__NOTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONF_CAR')
        i_extval=cp_InsertValVFPExtFlds(this,'CONF_CAR')
        cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCTIPOCA;
                  ,this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,this.w_CC__NOTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONF_CAR_IDX,i_nConn)
      *
      * update CONF_CAR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_CAR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CC__NOTE="+cp_ToStrODBC(this.w_CC__NOTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_CAR')
        i_cWhere = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
        UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CC__NOTE=this.w_CC__NOTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCR_MTD : Saving
      this.GSCR_MTD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"DICODICE";
             ,this.w_CCTIPOCA,"DITIPOCA";
             )
      this.GSCR_MTD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCR_MTD : Deleting
    this.GSCR_MTD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"DICODICE";
           ,this.w_CCTIPOCA,"DITIPOCA";
           )
    this.GSCR_MTD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONF_CAR_IDX,i_nConn)
      *
      * delete CONF_CAR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_3.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCC__NOTE_1_6.value==this.w_CC__NOTE)
      this.oPgFrm.Page1.oPag.oCC__NOTE_1_6.value=this.w_CC__NOTE
    endif
    cp_SetControlsValueExtFlds(this,'CONF_CAR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CCCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCR_MTD.CheckForm()
      if i_bres
        i_bres=  .GSCR_MTD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCR_MTD : Depends On
    this.GSCR_MTD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscr_amdPag1 as StdContainer
  Width  = 638
  height = 322
  stdWidth  = 638
  stdheight = 322
  resizeXpos=313
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_2 as StdField with uid="PADGZBJAJP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCTIPOCA,CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica descrittiva",;
    HelpContextID = 17433195,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=54, Left=97, Top=15, InputMask=replicate('X',5)

  add object oCCDESCRI_1_3 as StdField with uid="HKIXCEVTUI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 200282735,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=97, Top=40, InputMask=replicate('X',40)

  add object oCC__NOTE_1_6 as StdMemo with uid="ESSYAVVPTI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CC__NOTE", cQueryName = "CC__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 129745515,;
   bGlobalFont=.t.,;
    Height=219, Width=593, Left=12, Top=91

  add object oStr_1_4 as StdString with uid="UMCMXBGHTC",Visible=.t., Left=44, Top=19,;
    Alignment=1, Width=50, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="WQPWJKMIHJ",Visible=.t., Left=12, Top=44,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="VUTNMDFOYN",Visible=.t., Left=15, Top=73,;
    Alignment=0, Width=47, Height=15,;
    Caption="Note"  ;
  , bGlobalFont=.t.
enddefine
define class tgscr_amdPag2 as StdContainer
  Width  = 638
  height = 322
  stdWidth  = 638
  stdheight = 322
  resizeXpos=550
  resizeYpos=272
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="AJNCDSNRLC",left=3, top=7, width=632, height=305, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscr_mtd",lower(this.oContained.GSCR_MTD.class))=0
        this.oContained.GSCR_MTD.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="CCTIPOCA='D'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".CCTIPOCA=CONF_CAR.CCTIPOCA";
  +" and "+i_cAliasName+".CCCODICE=CONF_CAR.CCCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gscr_amd','CONF_CAR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCTIPOCA=CONF_CAR.CCTIPOCA";
  +" and "+i_cAliasName2+".CCCODICE=CONF_CAR.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
