* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_kmc                                                        *
*              Selezione rapida caratteristiche                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-03                                                      *
* Last revis.: 2014-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_kmc",oParentObject))

* --- Class definition
define class tgscr_kmc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 784
  Height = 530
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-15"
  HelpContextID=199849321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  CONF_CAR_IDX = 0
  cPrg = "gscr_kmc"
  cComment = "Selezione rapida caratteristiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOCA = space(1)
  w_CODINI = space(5)
  w_DESINI = space(40)
  w_CODFIN = space(5)
  w_DESFIN = space(40)
  w_SELEZI = space(1)
  w_CADEFA = space(1)
  w_CAOPER = space(1)
  o_CAOPER = space(1)
  w_CACOEFF = 0
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_kmcPag1","gscr_kmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONF_CAR'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscr_kmc
    *Rendo non visibili le proprietÓ che non mi servono e sposto le
    *altre nello spazio lasciato vuoto da esse
    this.w_zoomsel.dir.visible=.F.
    this.w_zoomsel.sel.visible=.F.
    this.w_zoomsel.rep.visible=.F.
    this.w_zoomsel.ref.left=241
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOCA=space(1)
      .w_CODINI=space(5)
      .w_DESINI=space(40)
      .w_CODFIN=space(5)
      .w_DESFIN=space(40)
      .w_SELEZI=space(1)
      .w_CADEFA=space(1)
      .w_CAOPER=space(1)
      .w_CACOEFF=0
        .w_TIPOCA = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODINI))
          .link_1_4('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_CODFIN))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_SELEZI = "D"
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_CADEFA = 'N'
          .DoRTCalc(8,8,.f.)
        .w_CACOEFF = iif(empty(.w_CAOPER),0,.w_CACOEFF)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(1,8,.t.)
        if .o_CAOPER<>.w_CAOPER
            .w_CACOEFF = iif(empty(.w_CAOPER),0,.w_CACOEFF)
        endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACOEFF_1_20.enabled = this.oPgFrm.Page1.oPag.oCACOEFF_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_TIPOCA;
                     ,'CCCODICE',trim(this.w_CODINI))
          select CCTIPOCA,CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oCODINI_1_4'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_TIPOCA;
                       ,'CCCODICE',this.w_CODINI)
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINI = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(5)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(5)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_TIPOCA;
                     ,'CCCODICE',trim(this.w_CODFIN))
          select CCTIPOCA,CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_6'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_TIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_TIPOCA;
                       ,'CCCODICE',this.w_CODFIN)
            select CCTIPOCA,CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CCCODICE,space(5))
      this.w_DESFIN = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(5)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(5)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_4.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_4.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_5.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_5.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_6.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_6.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_7.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_7.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_9.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADEFA_1_16.RadioValue()==this.w_CADEFA)
      this.oPgFrm.Page1.oPag.oCADEFA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOPER_1_17.RadioValue()==this.w_CAOPER)
      this.oPgFrm.Page1.oPag.oCAOPER_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACOEFF_1_20.value==this.w_CACOEFF)
      this.oPgFrm.Page1.oPag.oCACOEFF_1_20.value=this.w_CACOEFF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAOPER = this.w_CAOPER
    return

enddefine

* --- Define pages as container
define class tgscr_kmcPag1 as StdContainer
  Width  = 780
  height = 530
  stdWidth  = 780
  stdheight = 530
  resizeXpos=528
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_4 as StdField with uid="DSBERAGFGK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica di inizio selezione",;
    HelpContextID = 230416858,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=4, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_TIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_TIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oCODINI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
  endproc

  add object oDESINI_1_5 as StdField with uid="AZRRLLDVSD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 230357962,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=231, Top=4, InputMask=replicate('X',40)

  add object oCODFIN_1_6 as StdField with uid="XASNXFVBRH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica di fine selezione",;
    HelpContextID = 151970266,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=27, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_TIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CODFIN"

  proc oCODFIN_1_6.mDefault
    with this.Parent.oContained
      if empty(.w_CODFIN)
        .w_CODFIN = .w_CODINI
      endif
    endwith
  endproc

  func oCODFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_TIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oCODFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
  endproc

  add object oDESFIN_1_7 as StdField with uid="ANCSPSMLJE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 151911370,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=231, Top=27, InputMask=replicate('X',40)


  add object oBtn_1_8 as StdButton with uid="PPJFABIUST",left=715, top=4, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza le caratteristiche che soddisfano le selezioni impostate";
    , HelpContextID = 159532054;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .NotifyEvent("Interroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_9 as StdRadio with uid="KCMBJLDROQ",rtseq=6,rtrep=.f.,left=3, top=501, width=401,height=23;
    , cFormVar="w_SELEZI", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_9.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 218065626
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 218065626
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Inverti selezione"
      this.Buttons(3).HelpContextID = 218065626
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Inverti selezione","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_9.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    iif(this.value =3,"I",;
    space(1)))))
  endfunc
  func oSELEZI_1_9.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_9.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      iif(this.Parent.oContained.w_SELEZI=="I",3,;
      0)))
  endfunc


  add object oBtn_1_10 as StdButton with uid="REBWMPPOUT",left=660, top=477, width=48,height=45,;
    CpPicture="BMP\ELIMINA2.BMP", caption="", nPag=1;
    , ToolTipText = "Elimina le caratteristiche selezionate";
    , HelpContextID = 159532054;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSCR_BMC(this.Parent.oContained,"EL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="DQZIMLCEHR",left=715, top=477, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 159532054;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomSel as cp_szoombox with uid="AEPLHZWOQV",left=3, top=58, width=775,height=413,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONF_CAR",cZoomFile="GSCR_KMC",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 246583782


  add object oObj_1_13 as cp_runprogram with uid="BHANWDYKJR",left=4, top=533, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCR_BMC("SS")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 246583782


  add object oBtn_1_14 as StdButton with uid="MFGEFMCSPO",left=605, top=477, width=48,height=45,;
    CpPicture="BMP\INS_RAP.BMP", caption="", nPag=1;
    , ToolTipText = "Inserisci le caratteristiche selezionate";
    , HelpContextID = 159532054;
    , Caption='\<Inserisci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSCR_BMC(this.Parent.oContained,"IN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="AQIWVZIFXU",left=550, top=477, width=48,height=45,;
    CpPicture="BMP\REFRESH.BMP", caption="", nPag=1;
    , ToolTipText = "Sostituisci le caratteristiche selezionate";
    , HelpContextID = 159532054;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSCR_BMC(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCADEFA_1_16 as StdCheck with uid="MFONPQFWER",rtseq=7,rtrep=.f.,left=3, top=476, caption="Default",;
    ToolTipText = "Default",;
    HelpContextID = 104853466,;
    cFormVar="w_CADEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCADEFA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCADEFA_1_16.GetRadio()
    this.Parent.oContained.w_CADEFA = this.RadioValue()
    return .t.
  endfunc

  func oCADEFA_1_16.SetRadio()
    this.Parent.oContained.w_CADEFA=trim(this.Parent.oContained.w_CADEFA)
    this.value = ;
      iif(this.Parent.oContained.w_CADEFA=='S',1,;
      0)
  endfunc


  add object oCAOPER_1_17 as StdCombo with uid="HVPAAEVODY",value=3,rtseq=8,rtrep=.f.,left=153,top=477,width=42,height=21;
    , ToolTipText = "Operatore";
    , HelpContextID = 88358874;
    , cFormVar="w_CAOPER",RowSource=""+"X,"+"/,"+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAOPER_1_17.RadioValue()
    return(iif(this.value =1,"X",;
    iif(this.value =2,"/",;
    iif(this.value =3," ",;
    space(1)))))
  endfunc
  func oCAOPER_1_17.GetRadio()
    this.Parent.oContained.w_CAOPER = this.RadioValue()
    return .t.
  endfunc

  func oCAOPER_1_17.SetRadio()
    this.Parent.oContained.w_CAOPER=trim(this.Parent.oContained.w_CAOPER)
    this.value = ;
      iif(this.Parent.oContained.w_CAOPER=="X",1,;
      iif(this.Parent.oContained.w_CAOPER=="/",2,;
      iif(this.Parent.oContained.w_CAOPER=="",3,;
      0)))
  endfunc

  add object oCACOEFF_1_20 as StdField with uid="ESSYQBOGZX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CACOEFF", cQueryName = "CACOEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente",;
    HelpContextID = 247070758,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=291, Top=477, cSayPict='"@z" + v_pq(32)', cGetPict="v_gq(32)"

  func oCACOEFF_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CAOPER))
    endwith
   endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="YYPTJKWBJA",left=393, top=477, width=19,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Aggiorna le caratteristiche selezionate sull'elenco";
    , HelpContextID = 199648298;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSCR_BMC(this.Parent.oContained,"AE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_22 as cp_runprogram with uid="MOFZWGTTVZ",left=134, top=533, width=130,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCR_BMC("ST")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 246583782

  add object oStr_1_2 as StdString with uid="FRDWFVZLST",Visible=.t., Left=4, Top=4,;
    Alignment=1, Width=149, Height=16,;
    Caption="Da codice caratteristica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="EAWKWPBTIM",Visible=.t., Left=4, Top=27,;
    Alignment=1, Width=149, Height=18,;
    Caption="A codice caratteristica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CRBOCQJJLE",Visible=.t., Left=83, Top=477,;
    Alignment=1, Width=68, Height=18,;
    Caption="Operatore"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="SSWZGYXEQX",Visible=.t., Left=198, Top=477,;
    Alignment=1, Width=90, Height=18,;
    Caption="Coefficiente"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscr_kmc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
