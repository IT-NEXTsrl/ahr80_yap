* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_blm                                                        *
*              Sostituzione/variazione caratteristiche                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-10                                                      *
* Last revis.: 2015-04-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_blm",oParentObject,m.pAZIONE)
return(i_retval)

define class tgscr_blm as StdBatch
  * --- Local variables
  pAZIONE = space(10)
  Padre = .NULL.
  w_nRecSel = 0
  NC = space(10)
  NC1 = space(10)
  NC2 = space(10)
  w_SOSTIT = 0
  w_TIPOREC = space(1)
  w_RIGA = 0
  w_CCCODART = space(20)
  w_DISTINTA = space(41)
  w_KEYRIC = space(41)
  w_STATDI = space(1)
  w_RIGDIS = 0
  w_RECNO = 0
  w_DESART = space(40)
  w_CORIFCFN = 0
  w_COCHKRIG = space(10)
  w_RIFRIGA = 0
  w_RIFNUM = 0
  w_DETTAGLIO = space(40)
  w_INTER = .f.
  w_OLDCARAP = space(5)
  w_RECINIT = 0
  w_conta = 0
  w_DETLEG = .f.
  w_DEFA = space(1)
  * --- WorkFile variables
  CONF_ART_idx=0
  CONFDART_idx=0
  KEY_ARTI_idx=0
  CONF_DIS_idx=0
  CONFDDIS_idx=0
  DISTBASE_idx=0
  CONFDCAR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- da GSCR_KLM (Sostituzione di massa delle caratteristiche)
    * --- Assegnamenti
    this.Padre = this.oParentObject
    this.w_SOSTIT = 0
    * --- Nome cursore collegato allo zoom di selezione
    if this.oParentObject.w_operaz="I" and this.oParentObject.w_CARINS="C"
      this.NC = this.Padre.w_ZoomSel1.cCursor
    else
      this.NC = this.Padre.w_ZoomSel.cCursor
    endif
    * --- --Dettaglio caratteristiche NEW
    this.NC1 = this.Padre.w_ZoomCar.cCursor
    * --- --Dettaglio caratteristiche OLD
    this.NC2 = this.Padre.w_ZoomCar1.cCursor
    do case
      case this.pAZIONE="INTERROGA"
        * --- Visualizza Zoom
        this.w_INTER = .T.
        if this.oParentObject.w_CARINS="D"
          select count(*) as Numero from (this.NC2) where xChk=1 into cursor __temp__
          go top
          this.w_nRecSel = NVL(__temp__.Numero,0)
          use in __Temp__
          if this.w_nRecSel=0
            this.w_INTER = .F.
          endif
        endif
        if this.oParentObject.w_operaz="I" and this.oParentObject.w_CARINS="C"
          this.Padre.NotifyEvent("Interroga2")     
        else
          if this.w_INTER
            this.Padre.NotifyEvent("Interroga1")     
          else
            this.w_OLDCARAP = this.oParentObject.w_OLDCAR
            this.oParentObject.w_OLDCAR = Space(5)
            this.Padre.NotifyEvent("Interroga1")     
            this.oParentObject.w_OLDCAR = this.w_OLDCARAP
          endif
        endif
        * --- Attiva la pagina 3 automaticamente
        if this.oParentObject.w_operaz="S" and this.oParentObject.w_CARINS="D"
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Padre.oPgFrm.ActivePage = 3
        this.Padre.Refresh()     
      case this.pAZIONE= "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAZIONE="AG"
        * --- Controlla selezioni
        * --- --Aggiornamento Caratteristica Completo
        this.w_INTER = .T.
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        if this.w_nRecSel>0
          * --- Verifica validit� dati inseriti per le scelte
          if this.oParentObject.w_CARINS="C" 
            * --- --Se sostiuisco il dettaglio occorre che nelle nuove scelte ci sia associato almeno un default.
            select (this.NC1) 
 locate for xchk=1 and (! upper(CCDEFAULT) $ "SN" or (! empty(CCOPERA) and ! CCOPERA $ "X/"))
          endif
          if found()
            ah_ErrorMsg("Inserire S o N sul default e X o / o space sull'operatore",48)
            i_retcode = 'stop'
            return
          endif
          * --- --Se sostiuisco il dettaglio occorre che nelle nuove scelte ci sia associato almeno un default.
          select (this.NC1) 
 locate for xchk=1 and !(upper(CCFLINIT) $ "SN")
          if found()
            ah_ErrorMsg("Inserire S o N sul campo inizializza",48)
            i_retcode = 'stop'
            return
          endif
          select (this.NC1) 
 count for XCHK=1 AND upper(CCFLINIT)="S" to this.w_RECINIT
          if this.w_RECINIT > 1
            ah_ErrorMsg("Non � possibile specificare pi� di un valore di inizializzazione",48)
            i_retcode = 'stop'
            return
          endif
          select (this.NC2) 
 locate for xchk=1 and !(upper(CCFLINIT) $ "SN")
          if found()
            ah_ErrorMsg("Inserire S o N sul campo inizializza",48)
            i_retcode = 'stop'
            return
          endif
          select (this.NC2) 
 count for XCHK=1 AND upper(CCFLINIT)="S" to this.w_RECINIT
          if this.w_RECINIT > 1
            ah_ErrorMsg("Non � possibile specificare pi� di un valore di inizializzazione",48)
            i_retcode = 'stop'
            return
          endif
          if this.oParentObject.w_operaz<>"D"
            if this.oParentObject.w_CARINS="C" or (this.oParentObject.w_CARINS="D" and this.oParentObject.w_operaz="S")
              select (this.NC1) 
 locate for xchk=1
            else
              select (this.NC2) 
 locate for xchk=1
            endif
            if ! found()
              ah_ErrorMsg("Deve essere selezionata almeno una scelta",48)
              i_retcode = 'stop'
              return
            endif
            select (this.NC) 
 locate for xChk=1 and TIPO="Distinta"
            if found()
              if this.oParentObject.w_CARINS="C" or (this.oParentObject.w_CARINS="D" and this.oParentObject.w_operaz="S")
                select (this.NC1) 
 locate for xchk=1 and upper(CCDEFAULT) = "S"
              else
                if this.oParentObject.w_CARINS="C" 
                  select (this.NC2) 
 locate for xchk=1 and upper(CCDEFAULT) = "S"
                endif
              endif
              if ! found() AND this.oParentObject.w_CARINS="C"
                ah_ErrorMsg("Inserire almeno un default su una scelta",48)
                i_retcode = 'stop'
                return
              endif
            endif
          endif
          * --- Cursore di stampa
          CREATE CURSOR _AGG_ ; 
 (DISTINTA C(41),ARTICOLO C(41),ARDESCRI C(40),TIPO C(8),OPERAZ C(1))
          do case
            case this.oParentObject.w_operaz="S"
              if this.oParentObject.w_CARINS="C"
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              do case
                case this.w_SOSTIT=-1
                  ah_ErrorMsg("Non � stato possibile sostituire una o pi� caratteristiche",16)
                case this.w_SOSTIT=0
                  ah_ErrorMsg("Attenzione: nessuna caratteristica sostituita",48)
                case this.w_SOSTIT>0
                  ah_ErrorMsg("Sono state sostituite %1 caratteristiche",48,"",alltrim(str(this.w_SOSTIT,4)))
              endcase
            case this.oParentObject.w_operaz="I"
              if this.oParentObject.w_CARINS="C"
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              do case
                case this.w_SOSTIT=-1
                  ah_ErrorMsg("Non � stato possibile inserire una o pi� caratteristiche",16)
                case this.w_SOSTIT=0
                  ah_ErrorMsg("Attenzione: nessuna caratteristica inserita",48)
                case this.w_SOSTIT>0
                  ah_ErrorMsg("Sono state inserite %1 caratteristiche",48,"",alltrim(str(this.w_SOSTIT,4)))
              endcase
            case this.oParentObject.w_operaz="D"
              if this.oParentObject.w_CARINS="C"
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              do case
                case this.w_SOSTIT=-1
                  ah_ErrorMsg("Non � stato possibile disattivare una o pi� caratteristiche",16)
                case this.w_SOSTIT=0
                  ah_ErrorMsg("Attenzione: nessuna caratteristica disattivata",48)
                case this.w_SOSTIT>0
                  ah_ErrorMsg("Sono state disattivate %1 caratteristiche",48,"",alltrim(str(this.w_SOSTIT,4)))
              endcase
          endcase
          * --- Lancia stampa riassuntiva delle operazioni compiute
          if this.w_SOSTIT>0
            select space(41) as DISTINTA, space(41) as ARTICOLO, space(40) as ARDESCRI, space(8) as TIPO, "G" as OPERAZ,; 
 this.oParentObject.w_NEWCAR as newcar,this.oParentObject.w_NEWDES as newdes,this.oParentObject.w_OLDCAR as oldcar,this.oParentObject.w_OLDDES as olddes,; 
 CPROWORD,CCDETTAG,CCDEFAULT,CCFLINIT,CCOPERA,CCCOEFF from (this.NC2) where xchk=1; 
 union; 
 select space(41) as DISTINTA, space(41) as ARTICOLO, space(40) as ARDESCRI, space(8) as TIPO, "A" as OPERAZ,; 
 this.oParentObject.w_NEWCAR as newcar,this.oParentObject.w_NEWDES as newdes,this.oParentObject.w_OLDCAR as oldcar,this.oParentObject.w_OLDDES as olddes,; 
 CPROWORD,CCDETTAG,CCDEFAULT,CCFLINIT,CCOPERA,CCCOEFF from (this.NC1) where xchk=1; 
 union; 
 select *,this.oParentObject.w_NEWCAR as newcar,this.oParentObject.w_NEWDES as newdes,this.oParentObject.w_OLDCAR as oldcar,this.oParentObject.w_OLDDES as olddes,; 
 99999*0 as CPROWORD,space(40) as CCDETTAG," " as CCDEFAULT," " as CCFLINIT," " as CCOPERA,; 
 99999.99999*0 as CCCOEFF from _AGG_ ; 
 into cursor __tmp__ order by 4, 2
            CREATE CURSOR _AGG_ ; 
 (DISTINTA C(41),ARTICOLO C(41),ARDESCRI C(40),TIPO C(8),OPERAZ C(1))
            CP_CHPRN("..\CCAR\EXE\QUERY\GSCR_BLM","",this.oParentObject)
          endif
          if used("__tmp__")
            USE IN __tmp__
          endif
          if used("_AGG_")
            USE IN _AGG_
          endif
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare",48)
          this.w_INTER = .F.
        endif
        * --- Riesegue l'interrogazione
        if this.w_INTER
          if this.oParentObject.w_operaz="I" and this.oParentObject.w_CARINS="C"
            this.Padre.NotifyEvent("Interroga2")     
          else
            this.Padre.NotifyEvent("Interroga1")     
          endif
          if this.oParentObject.w_operaz="S" and this.oParentObject.w_CARINS="D"
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.Padre.Refresh()     
      case this.pAZIONE="ZOOM"
        if this.oParentObject.w_OPERAZ="I" and this.oParentObject.w_CARINS="C"
          this.oParentObject.w_OLDCAR = SPACE(5)
          this.Padre.w_ZoomSel.Visible = .F.
          this.Padre.w_ZoomSel1.Visible = .T.
        else
          this.Padre.w_ZoomSel.Visible = .T.
          this.Padre.w_ZoomSel1.Visible = .F.
        endif
        if this.oParentObject.w_OPERAZ="D" and this.oParentObject.w_CARINS="C"
          this.oParentObject.w_NEWCAR = SPACE(5)
        endif
        if this.oParentObject.w_OPERAZ<>"S" and this.oParentObject.w_CARINS="D"
          * --- --Occorre sbiancare il codice newcar
          this.oParentObject.w_NEWCAR = SPACE(5)
        endif
      case this.pAZIONE= "SC"
        if used(this.NC1)
          do case
            case this.oParentObject.w_SELEZCAR="S"
              * --- Seleziona tutte le righe dello zoom
              UPDATE (this.NC1) SET xChk=1
            case this.oParentObject.w_SELEZCAR="D"
              * --- Deseleziona tutte le righe dello zoom
              UPDATE (this.NC1) SET xChk=0
            otherwise
              * --- Inverte Selezione
              UPDATE (this.NC1) SET xChk=abs(xChk-1)
          endcase
        endif
      case this.pAZIONE= "1SC"
        if used(this.NC2)
          do case
            case this.oParentObject.w_SELEZCAR1="S"
              * --- Seleziona tutte le righe dello zoom
              UPDATE (this.NC2) SET xChk=1
            case this.oParentObject.w_SELEZCAR1="D"
              * --- Deseleziona tutte le righe dello zoom
              UPDATE (this.NC2) SET xChk=0
            otherwise
              * --- Inverte Selezione
              UPDATE (this.NC2) SET xChk=abs(xChk-1)
          endcase
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sostituisco la caratteristica nell'Articolo/Distinta (Elimino la vecchia caratteristica ed inserisco la nuova)
    * --- Try
    local bErr_03ADF950
    bErr_03ADF950=bTrsErr
    this.Try_03ADF950()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_SOSTIT = -1
    endif
    bTrsErr=bTrsErr or bErr_03ADF950
    * --- End
  endproc
  proc Try_03ADF950()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziona i componenti da sostituire
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    if this.w_TIPOREC="Articoli"
      * --- Sostituzione record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Eliminazione Vecchia Caratteristica
      * --- Delete from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Inserimento Nuova Caratteristica
      * --- Select from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select max(CPROWORD) as CPROWORD  from "+i_cTable+" CONF_ART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+"";
             ,"_Curs_CONF_ART")
      else
        select max(CPROWORD) as CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART;
          into cursor _Curs_CONF_ART
      endif
      if used('_Curs_CONF_ART')
        select _Curs_CONF_ART
        locate for 1=1
        do while not(eof())
        this.w_RIGA = nvl(_Curs_CONF_ART.CPROWORD,0) + 10
          select _Curs_CONF_ART
          continue
        enddo
        use
      endif
      * --- Insert into CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_ART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONF_ART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONF_ART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONF_ART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'CONF_ART','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CONF_ART','CPROWORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CCDESCRI',this.oParentObject.w_NEWDES,'CPROWORD',this.w_RIGA)
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,this.oParentObject.w_NEWDES;
             ,this.w_RIGA;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Dati da cursore zoom
      select (this.NC1) 
 scan for xchk=1
      * --- Insert into CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCFLINIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONFDART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONFDART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CPROWNUM),'CONFDART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
        +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDART','CC_COEFF');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CPROWNUM',CPROWNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCFLINIT',upper(CCFLINIT))
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCFLINIT &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,CPROWNUM;
             ,CPROWORD;
             ,CCDETTAG;
             ,upper(CCDEFAULT);
             ,iif(CCOPERA="X","*",CCOPERA);
             ,CCCOEFF ;
             ,upper(CCFLINIT);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    else
      * --- Sostituzione record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Eliminazione Vecchia Caratteristica
      * --- Delete from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Inserimento Nuova Caratteristica
      * --- Select from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select max(CPROWORD) as CPROWORD  from "+i_cTable+" CONF_ART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+"";
             ,"_Curs_CONF_ART")
      else
        select max(CPROWORD) as CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART;
          into cursor _Curs_CONF_ART
      endif
      if used('_Curs_CONF_ART')
        select _Curs_CONF_ART
        locate for 1=1
        do while not(eof())
        this.w_RIGA = nvl(_Curs_CONF_ART.CPROWORD,0) + 10
          select _Curs_CONF_ART
          continue
        enddo
        use
      endif
      * --- Insert into CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_ART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONF_ART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONF_ART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONF_ART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'CONF_ART','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CONF_ART','CPROWORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CCDESCRI',this.oParentObject.w_NEWDES,'CPROWORD',this.w_RIGA)
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,this.oParentObject.w_NEWDES;
             ,this.w_RIGA;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Dati da cursore zoom
      select (this.NC1) 
 scan for xchk=1
      * --- Insert into CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCFLINIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONFDART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONFDART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CPROWNUM),'CONFDART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
        +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDART','CC_COEFF');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CPROWNUM',CPROWNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCFLINIT',upper(CCFLINIT))
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCFLINIT &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,CPROWNUM;
             ,CPROWORD;
             ,CCDETTAG;
             ,upper(CCDEFAULT);
             ,iif(CCOPERA="X","*",CCOPERA);
             ,CCCOEFF ;
             ,upper(CCFLINIT);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
      * --- Sostituzione record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Eliminazione Vecchia Caratteristica
      * --- Delete from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Inserimento Nuova Caratteristica
      * --- Select from CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2],.t.,this.CONF_DIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select max(CPROWORD) as CPROWORD  from "+i_cTable+" CONF_DIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" and CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" and CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+"";
             ,"_Curs_CONF_DIS")
      else
        select max(CPROWORD) as CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA and CCROWORD=this.w_RIGDIS and CCCOMPON=this.w_KEYRIC;
          into cursor _Curs_CONF_DIS
      endif
      if used('_Curs_CONF_DIS')
        select _Curs_CONF_DIS
        locate for 1=1
        do while not(eof())
        this.w_RIGA = nvl(_Curs_CONF_DIS.CPROWORD,0) + 10
          select _Curs_CONF_DIS
          continue
        enddo
        use
      endif
      * --- Insert into CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_DIS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+",CCDATINI"+",CCDATFIN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONF_DIS','CC__DIBA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONF_DIS','CCROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONF_DIS','CCCOMPON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONF_DIS','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONF_DIS','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONF_DIS','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'CONF_DIS','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CONF_DIS','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONF_DIS','CCDATINI');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONF_DIS','CCDATFIN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CCDESCRI',this.oParentObject.w_NEWDES,'CPROWORD',this.w_RIGA,'CCDATINI',i_datsys,'CCDATFIN',cp_CharToDate("31-12-2099"))
        insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD,CCDATINI,CCDATFIN &i_ccchkf. );
           values (;
             this.w_DISTINTA;
             ,this.w_RIGDIS;
             ,this.w_KEYRIC;
             ,this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,this.oParentObject.w_NEWDES;
             ,this.w_RIGA;
             ,i_datsys;
             ,cp_CharToDate("31-12-2099");
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      select (this.NC1) 
 scan for xchk=1
      * --- Insert into CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDDIS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCDATINI"+",CCDATFIN"+",CCFLINIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONFDDIS','CC__DIBA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONFDDIS','CCROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONFDDIS','CCCOMPON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDDIS','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONFDDIS','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CPROWNUM),'CONFDDIS','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDDIS','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
        +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDDIS','CCOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDDIS','CCFLINIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CPROWNUM',CPROWNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCDATINI',i_datsys)
        insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDATINI,CCDATFIN,CCFLINIT &i_ccchkf. );
           values (;
             this.w_DISTINTA;
             ,this.w_RIGDIS;
             ,this.w_KEYRIC;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,CPROWNUM;
             ,CPROWORD;
             ,CCDETTAG;
             ,upper(CCDEFAULT);
             ,iif(CCOPERA="X","*",CCOPERA);
             ,CCCOEFF ;
             ,i_datsys;
             ,cp_CharToDate("31-12-2099");
             ,upper(CCFLINIT);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    if this.w_SOSTIT < 0
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    else
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino la caratteristica nell'Articolo/Distinta
    * --- Try
    local bErr_03B107E0
    bErr_03B107E0=bTrsErr
    this.Try_03B107E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_SOSTIT = -1
    endif
    bTrsErr=bTrsErr or bErr_03B107E0
    * --- End
  endproc
  proc Try_03B107E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziona i componenti da eliminare
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    if this.w_TIPOREC="Articoli"
      * --- Eliminazione record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Delete from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      select (this.NC) 
 go this.w_RECNO
    else
      * --- Eliminazione record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Delete from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
               )
      else
        delete from (i_cTable) where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      select (this.NC) 
 go this.w_RECNO
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisco la caratteristica nell'Articolo/Distinta
    * --- Try
    local bErr_03B24050
    bErr_03B24050=bTrsErr
    this.Try_03B24050()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_SOSTIT = -1
    endif
    bTrsErr=bTrsErr or bErr_03B24050
    * --- End
  endproc
  proc Try_03B24050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziona i componenti da inserire
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(ARCODART,20)
    this.w_KEYRIC = ARCODART
    this.w_DESART = ARDESART
    this.w_DISTINTA = DBCODICE
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    this.w_COCHKRIG = sys(2015)
    * --- --Leggo riferimento del legame
    * --- Read from DISTBASE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CPROWNUM"+;
        " from "+i_cTable+" DISTBASE where ";
            +"DBCODICE = "+cp_ToStrODBC(this.w_DISTINTA);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGDIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CPROWNUM;
        from (i_cTable) where;
            DBCODICE = this.w_DISTINTA;
            and CPROWNUM = this.w_RIGDIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CORIFCFN = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TIPOREC="Articoli"
      * --- Inserimento record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Select from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select max(CPROWORD) as CPROWORD  from "+i_cTable+" CONF_ART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+"";
             ,"_Curs_CONF_ART")
      else
        select max(CPROWORD) as CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART;
          into cursor _Curs_CONF_ART
      endif
      if used('_Curs_CONF_ART')
        select _Curs_CONF_ART
        locate for 1=1
        do while not(eof())
        this.w_RIGA = nvl(_Curs_CONF_ART.CPROWORD,0) + 10
          select _Curs_CONF_ART
          continue
        enddo
        use
      endif
      * --- Insert into CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_ART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONF_ART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONF_ART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONF_ART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'CONF_ART','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CONF_ART','CPROWORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CCDESCRI',this.oParentObject.w_NEWDES,'CPROWORD',this.w_RIGA)
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,this.oParentObject.w_NEWDES;
             ,this.w_RIGA;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Aggiorno anche il flag sugli articoli
      * --- Write into KEY_ARTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CACMPCAR ="+cp_NullLink(cp_ToStrODBC("S"),'KEY_ARTI','CACMPCAR');
            +i_ccchkf ;
        +" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_KEYRIC);
               )
      else
        update (i_cTable) set;
            CACMPCAR = "S";
            &i_ccchkf. ;
         where;
            CACODICE = this.w_KEYRIC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Dati da cursore zoom
      select (this.NC1) 
 scan for xchk=1
      * --- Insert into CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCFLINIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONFDART','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDART','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONFDART','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CPROWNUM),'CONFDART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
        +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDART','CC_COEFF');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CPROWNUM',CPROWNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCFLINIT',upper(CCFLINIT))
        insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCFLINIT &i_ccchkf. );
           values (;
             this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,CPROWNUM;
             ,CPROWORD;
             ,CCDETTAG;
             ,upper(CCDEFAULT);
             ,iif(CCOPERA="X","*",CCOPERA);
             ,CCCOEFF ;
             ,upper(CCFLINIT);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    else
      * --- Inserimento record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Select from CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2],.t.,this.CONF_DIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select max(CPROWORD) as CPROWORD  from "+i_cTable+" CONF_DIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" and CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" and CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+"";
             ,"_Curs_CONF_DIS")
      else
        select max(CPROWORD) as CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA and CCROWORD=this.w_RIGDIS and CCCOMPON=this.w_KEYRIC;
          into cursor _Curs_CONF_DIS
      endif
      if used('_Curs_CONF_DIS')
        select _Curs_CONF_DIS
        locate for 1=1
        do while not(eof())
        this.w_RIGA = nvl(_Curs_CONF_DIS.CPROWORD,0) + 10
          select _Curs_CONF_DIS
          continue
        enddo
        use
      endif
      * --- --Occorre inizializzare il corifcnf contenente il riferimento del legame per qul componente
      *     presente in distinta
      if this.w_CORIFCFN=0
        this.w_CORIFCFN = this.w_RIGDIS
        * --- Write into DISTBASE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DISTBASE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'DISTBASE','CPROWNUM');
              +i_ccchkf ;
          +" where ";
              +"DBCODICE = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGDIS);
                 )
        else
          update (i_cTable) set;
              CPROWNUM = this.w_RIGDIS;
              &i_ccchkf. ;
           where;
              DBCODICE = this.w_DISTINTA;
              and CPROWNUM = this.w_RIGDIS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_CORIFCFN=0 AND this.w_STATDI="M"
          * --- Write into DISTBASE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DISTBASE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CORIFCFN ="+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'DISTBASE','CORIFCFN');
            +",COCHKRIG ="+cp_NullLink(cp_ToStrODBC(this.w_COCHKRIG),'DISTBASE','COCHKRIG');
                +i_ccchkf ;
            +" where ";
                +"CORIFDIS = "+cp_ToStrODBC(this.w_DISTINTA);
                +" and COTIPGES = "+cp_ToStrODBC(this.oParentObject.w_TIPGES);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGDIS);
                   )
          else
            update (i_cTable) set;
                CORIFCFN = this.w_RIGDIS;
                ,COCHKRIG = this.w_COCHKRIG;
                &i_ccchkf. ;
             where;
                CORIFDIS = this.w_DISTINTA;
                and COTIPGES = this.oParentObject.w_TIPGES;
                and CPROWNUM = this.w_RIGDIS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_CORIFCFN = this.w_RIGDIS
        endif
      endif
      if this.w_CORIFCFN<>this.w_RIGDIS 
        this.w_RIGDIS = this.w_CORIFCFN
      endif
      * --- Insert into CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_DIS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+",CCDATINI"+",CCDATFIN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONF_DIS','CC__DIBA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONF_DIS','CCROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONF_DIS','CCCOMPON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONF_DIS','CCCODART');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONF_DIS','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONF_DIS','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'CONF_DIS','CCDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGA),'CONF_DIS','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONF_DIS','CCDATINI');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONF_DIS','CCDATFIN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CCDESCRI',this.oParentObject.w_NEWDES,'CPROWORD',this.w_RIGA,'CCDATINI',i_datsys,'CCDATFIN',cp_CharToDate("31-12-2099"))
        insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD,CCDATINI,CCDATFIN &i_ccchkf. );
           values (;
             this.w_DISTINTA;
             ,this.w_RIGDIS;
             ,this.w_KEYRIC;
             ,this.w_CCCODART;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,this.oParentObject.w_NEWDES;
             ,this.w_RIGA;
             ,i_datsys;
             ,cp_CharToDate("31-12-2099");
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      select (this.NC1) 
 scan for xchk=1
      * --- Insert into CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDDIS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCDATINI"+",CCDATFIN"+",CCFLINIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONFDDIS','CC__DIBA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONFDDIS','CCROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONFDDIS','CCCOMPON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDDIS','CCTIPOCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWCAR),'CONFDDIS','CCCODICE');
        +","+cp_NullLink(cp_ToStrODBC(CPROWNUM),'CONFDDIS','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDDIS','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
        +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDDIS','CCOPERAT');
        +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDDIS','CCFLINIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_NEWCAR,'CPROWNUM',CPROWNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCDATINI',i_datsys)
        insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDATINI,CCDATFIN,CCFLINIT &i_ccchkf. );
           values (;
             this.w_DISTINTA;
             ,this.w_RIGDIS;
             ,this.w_KEYRIC;
             ,this.oParentObject.w_TIPOCA;
             ,this.oParentObject.w_NEWCAR;
             ,CPROWNUM;
             ,CPROWORD;
             ,CCDETTAG;
             ,upper(CCDEFAULT);
             ,iif(CCOPERA="X","*",CCOPERA);
             ,CCCOEFF ;
             ,i_datsys;
             ,cp_CharToDate("31-12-2099");
             ,upper(CCFLINIT);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Inserimento Dettaglio
    *     Lo zoom di riferimento del dettaglio caratteristiche si trova in NC2
    * --- Try
    local bErr_03B43EC0
    bErr_03B43EC0=bTrsErr
    this.Try_03B43EC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_SOSTIT = -1
    endif
    bTrsErr=bTrsErr or bErr_03B43EC0
    * --- End
  endproc
  proc Try_03B43EC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    this.w_RIFRIGA = 0
    this.w_RIFNUM = 0
    if this.w_TIPOREC="Articoli"
      * --- Inserimento record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Dati da cursore zoom
      select (this.NC2) 
 scan for xchk=1
      this.w_RIFRIGA = 0
      this.w_RIFNUM = 0
      this.w_DETTAGLIO = CCDETTAG
      * --- Seleziona i componenti da inserire
      * --- Select from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM ,CPROWORD  from "+i_cTable+" CONFDART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDART")
      else
        select CPROWNUM ,CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDART
      endif
      if used('_Curs_CONFDART')
        select _Curs_CONFDART
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDART.CPROWORD
        this.w_RIFNUM = _Curs_CONFDART.CPROWNUM
          select _Curs_CONFDART
          continue
        enddo
        use
      endif
      if this.w_RIFRIGA=0 AND this.w_RIFNUM=0
        * --- --Occorre inserire il nuovo dettaglio 
        * --- Select from CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM ,MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" CONFDART ";
              +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" ";
               ,"_Curs_CONFDART")
        else
          select MAX(CPROWNUM) AS CPROWNUM ,MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR ;
            into cursor _Curs_CONFDART
        endif
        if used('_Curs_CONFDART')
          select _Curs_CONFDART
          locate for 1=1
          do while not(eof())
          this.w_RIFRIGA = nvl(_Curs_CONFDART.CPROWORD,0) + 10
          this.w_RIFNUM = nvl(_Curs_CONFDART.CPROWNUM,0)+1
            select _Curs_CONFDART
            continue
          enddo
          use
        endif
        select (this.NC2)
        * --- Insert into CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCFLINIT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONFDART','CCCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDART','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDCAR),'CONFDART','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFNUM),'CONFDART','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
          +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDART','CC_COEFF');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_OLDCAR,'CPROWNUM',this.w_RIFNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCFLINIT',upper(CCFLINIT))
          insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCFLINIT &i_ccchkf. );
             values (;
               this.w_CCCODART;
               ,this.oParentObject.w_TIPOCA;
               ,this.oParentObject.w_OLDCAR;
               ,this.w_RIFNUM;
               ,CPROWORD;
               ,CCDETTAG;
               ,upper(CCDEFAULT);
               ,iif(CCOPERA="X","*",CCOPERA);
               ,CCCOEFF ;
               ,upper(CCFLINIT);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- --Il dettaglio � gi� presente si effettua update
        select (this.NC2)
        * --- Write into CONFDART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONFDART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
          +",CCDETTAG ="+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
          +",CC__DEFA ="+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
          +",CCOPERAT ="+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
          +",CC_COEFF ="+cp_NullLink(cp_ToStrODBC(CCCOEFF),'CONFDART','CC_COEFF');
          +",CCFLINIT ="+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
              +i_ccchkf ;
          +" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                 )
        else
          update (i_cTable) set;
              CPROWORD = CPROWORD;
              ,CCDETTAG = CCDETTAG;
              ,CC__DEFA = upper(CCDEFAULT);
              ,CCOPERAT = iif(CCOPERA="X","*",CCOPERA);
              ,CC_COEFF = CCCOEFF;
              ,CCFLINIT = upper(CCFLINIT);
              &i_ccchkf. ;
           where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;
              and CPROWNUM = this.w_RIFNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    else
      * --- Inserimento record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      select (this.NC2) 
 scan for xchk=1
      this.w_DETTAGLIO = CCDETTAG
      if UPPER(CCFLINIT)="S"
        this.w_RIFRIGA = 0
        * --- Select from CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CCROWORD  from "+i_cTable+" CONFDDIS ";
              +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCFLINIT='S' AND CCDETTAG<>"+cp_ToStrODBC(this.w_DETTAGLIO)+"";
               ,"_Curs_CONFDDIS")
        else
          select CCROWORD from (i_cTable);
           where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCFLINIT="S" AND CCDETTAG<>this.w_DETTAGLIO;
            into cursor _Curs_CONFDDIS
        endif
        if used('_Curs_CONFDDIS')
          select _Curs_CONFDDIS
          locate for 1=1
          do while not(eof())
          this.w_RIFRIGA = _Curs_CONFDDIS.CCROWORD
            select _Curs_CONFDDIS
            continue
          enddo
          use
        endif
        if this.w_RIFRIGA<>0
          ah_errormsg("E' presente in distinta un valore di inizializzazione per la stessa caratteristica.")
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          i_retcode = 'stop'
          return
        endif
      endif
      this.w_SOSTIT = this.w_SOSTIT+1
      this.w_RIFRIGA = 0
      this.w_RIFNUM = 0
      * --- Select from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM AS CPROWNUM ,CPROWORD AS CPROWORD  from "+i_cTable+" CONFDDIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDDIS")
      else
        select CPROWNUM AS CPROWNUM ,CPROWORD AS CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDDIS
      endif
      if used('_Curs_CONFDDIS')
        select _Curs_CONFDDIS
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDDIS.CPROWORD
        this.w_RIFNUM = _Curs_CONFDDIS.CPROWNUM
          select _Curs_CONFDDIS
          continue
        enddo
        use
      endif
      if this.w_RIFRIGA=0 AND this.w_RIFNUM=0
        * --- Select from CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM,MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" CONFDDIS ";
              +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+"";
               ,"_Curs_CONFDDIS")
        else
          select MAX(CPROWNUM) AS CPROWNUM,MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR;
            into cursor _Curs_CONFDDIS
        endif
        if used('_Curs_CONFDDIS')
          select _Curs_CONFDDIS
          locate for 1=1
          do while not(eof())
          this.w_RIFRIGA = NVL(_Curs_CONFDDIS.CPROWORD,0)+10
          this.w_RIFNUM = NVL(_Curs_CONFDDIS.CPROWNUM,0)+1
            select _Curs_CONFDDIS
            continue
          enddo
          use
        endif
        select (this.NC2)
        * --- Insert into CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDDIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCDATINI"+",CCDATFIN"+",CCFLINIT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONFDDIS','CC__DIBA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONFDDIS','CCROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONFDDIS','CCCOMPON');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDDIS','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDCAR),'CONFDDIS','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFRIGA),'CONFDDIS','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDDIS','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
          +","+cp_NullLink(cp_ToStrODBC(CCOPERA),'CONFDDIS','CCOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDDIS','CCFLINIT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_OLDCAR,'CPROWNUM',this.w_RIFRIGA,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',CCOPERA,'CC_COEFF',CCCOEFF ,'CCDATINI',i_datsys)
          insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDATINI,CCDATFIN,CCFLINIT &i_ccchkf. );
             values (;
               this.w_DISTINTA;
               ,this.w_RIGDIS;
               ,this.w_KEYRIC;
               ,this.oParentObject.w_TIPOCA;
               ,this.oParentObject.w_OLDCAR;
               ,this.w_RIFRIGA;
               ,CPROWORD;
               ,CCDETTAG;
               ,upper(CCDEFAULT);
               ,CCOPERA;
               ,CCCOEFF ;
               ,i_datsys;
               ,cp_CharToDate("31-12-2099");
               ,upper(CCFLINIT);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        select (this.NC2)
        * --- Write into CONFDDIS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONFDDIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDETTAG ="+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
          +",CC__DEFA ="+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
          +",CCOPERAT ="+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDDIS','CCOPERAT');
          +",CC_COEFF ="+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
          +",CCDATINI ="+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
          +",CCDATFIN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
          +",CCFLINIT ="+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDDIS','CCFLINIT');
              +i_ccchkf ;
          +" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                 )
        else
          update (i_cTable) set;
              CCDETTAG = CCDETTAG;
              ,CC__DEFA = upper(CCDEFAULT);
              ,CCOPERAT = iif(CCOPERA="X","*",CCOPERA);
              ,CC_COEFF = CCCOEFF ;
              ,CCDATINI = i_datsys;
              ,CCDATFIN = cp_CharToDate("31-12-2099");
              ,CCFLINIT = upper(CCFLINIT);
              &i_ccchkf. ;
           where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;
              and CPROWNUM = this.w_RIFNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Sostituzione  Dattaglio
    * --- Try
    local bErr_03B78030
    bErr_03B78030=bTrsErr
    this.Try_03B78030()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_SOSTIT = -1
    endif
    bTrsErr=bTrsErr or bErr_03B78030
    * --- End
  endproc
  proc Try_03B78030()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziona i componenti da sostituire
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    this.w_RIFRIGA = 0
    this.w_RIFNUM = 0
    if this.w_TIPOREC="Articoli"
      * --- Sostituzione record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Eliminazione Vecchia Caratteristica
      select (this.NC2) 
 scan for xchk=1
      this.w_DETTAGLIO = CCDETTAG
      * --- Select from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD  from "+i_cTable+" CONFDART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDART")
      else
        select CPROWNUM,CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDART
      endif
      if used('_Curs_CONFDART')
        select _Curs_CONFDART
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDART.CPROWORD
        this.w_RIFNUM = _Curs_CONFDART.CPROWNUM
        * --- Delete from CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
                +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
                +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_RIFRIGA);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                +" and CCDETTAG = "+cp_ToStrODBC(this.w_DETTAGLIO);
                 )
        else
          delete from (i_cTable) where;
                CCCODART = this.w_CCCODART;
                and CCTIPOCA = this.oParentObject.w_TIPOCA;
                and CCCODICE = this.oParentObject.w_OLDCAR;
                and CPROWORD = this.w_RIFRIGA;
                and CPROWNUM = this.w_RIFNUM;
                and CCDETTAG = this.w_DETTAGLIO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
          select _Curs_CONFDART
          continue
        enddo
        use
      endif
      endscan
      * --- --Inserimento nuovo Dettaglio
      * --- --Si individua se il dettaglio � gi� presente
      select (this.NC1) 
 scan for xchk=1
      this.w_DETTAGLIO = CCDETTAG
      this.w_RIFRIGA = 0
      this.w_RIFNUM = 0
      * --- Select from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM ,CPROWORD  from "+i_cTable+" CONFDART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDART")
      else
        select CPROWNUM ,CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDART
      endif
      if used('_Curs_CONFDART')
        select _Curs_CONFDART
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDART.CPROWORD
        this.w_RIFNUM = _Curs_CONFDART.CPROWNUM
          select _Curs_CONFDART
          continue
        enddo
        use
      endif
      if this.w_RIFRIGA<>0 AND this.w_RIFNUM<>0
        select (this.NC1)
        * --- Modifica Caratteristica
        * --- Write into CONFDART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONFDART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
          +",CCDETTAG ="+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
          +",CC__DEFA ="+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
          +",CCOPERAT ="+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
          +",CC_COEFF ="+cp_NullLink(cp_ToStrODBC(CCCOEFF),'CONFDART','CC_COEFF');
          +",CCFLINIT ="+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
              +i_ccchkf ;
          +" where ";
              +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                 )
        else
          update (i_cTable) set;
              CPROWORD = CPROWORD;
              ,CCDETTAG = CCDETTAG;
              ,CC__DEFA = upper(CCDEFAULT);
              ,CCOPERAT = iif(CCOPERA="X","*",CCOPERA);
              ,CC_COEFF = CCCOEFF;
              ,CCFLINIT = upper(CCFLINIT);
              &i_ccchkf. ;
           where;
              CCCODART = this.w_CCCODART;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;
              and CPROWNUM = this.w_RIFNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Select from CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM ,MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" CONFDART ";
              +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" ";
               ,"_Curs_CONFDART")
        else
          select MAX(CPROWNUM) AS CPROWNUM ,MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR ;
            into cursor _Curs_CONFDART
        endif
        if used('_Curs_CONFDART')
          select _Curs_CONFDART
          locate for 1=1
          do while not(eof())
          this.w_RIFRIGA = nvl(_Curs_CONFDART.CPROWORD,0) + 10
          this.w_RIFNUM = nvl(_Curs_CONFDART.CPROWNUM,0)+1
            select _Curs_CONFDART
            continue
          enddo
          use
        endif
        select (this.NC1)
        * --- Insert into CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCFLINIT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CCCODART),'CONFDART','CCCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDART','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDCAR),'CONFDART','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFNUM),'CONFDART','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDART','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDART','CCDETTAG');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDART','CC__DEFA');
          +","+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDART','CCOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDART','CC_COEFF');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDART','CCFLINIT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_OLDCAR,'CPROWNUM',this.w_RIFNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',iif(CCOPERA="X","*",CCOPERA),'CC_COEFF',CCCOEFF ,'CCFLINIT',upper(CCFLINIT))
          insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCFLINIT &i_ccchkf. );
             values (;
               this.w_CCCODART;
               ,this.oParentObject.w_TIPOCA;
               ,this.oParentObject.w_OLDCAR;
               ,this.w_RIFNUM;
               ,CPROWORD;
               ,CCDETTAG;
               ,upper(CCDEFAULT);
               ,iif(CCOPERA="X","*",CCOPERA);
               ,CCCOEFF ;
               ,upper(CCFLINIT);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      endscan
      * --- Dati da cursore zoom
      select (this.NC) 
 go this.w_RECNO
    else
      * --- Sostituzione record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      this.w_SOSTIT = this.w_SOSTIT+1
      * --- Eliminazione Vecchia Caratteristica
      select (this.NC2) 
 scan for xchk=1
      * --- Cancello Vecchia Caratteristica
      this.w_DETTAGLIO = CCDETTAG
      * --- Select from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD  from "+i_cTable+" CONFDDIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_NEWCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDDIS")
      else
        select CPROWNUM,CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_NEWCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDDIS
      endif
      if used('_Curs_CONFDDIS')
        select _Curs_CONFDDIS
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDDIS.CPROWORD
        this.w_RIFNUM = _Curs_CONFDDIS.CPROWNUM
        * --- Delete from CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
                +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
                +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
                +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
                +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                +" and CPROWORD = "+cp_ToStrODBC(this.w_RIFRIGA);
                +" and CCDETTAG = "+cp_ToStrODBC(this.w_DETTAGLIO);
                 )
        else
          delete from (i_cTable) where;
                CC__DIBA = this.w_DISTINTA;
                and CCROWORD = this.w_RIGDIS;
                and CCCOMPON = this.w_KEYRIC;
                and CCTIPOCA = this.oParentObject.w_TIPOCA;
                and CCCODICE = this.oParentObject.w_OLDCAR;
                and CPROWNUM = this.w_RIFNUM;
                and CPROWORD = this.w_RIFRIGA;
                and CCDETTAG = this.w_DETTAGLIO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
          select _Curs_CONFDDIS
          continue
        enddo
        use
      endif
      endscan
      * --- --INSERISCO NUOVO DETTAGLIO
      select (this.NC1) 
 scan for xchk=1
      this.w_RIFRIGA = 0
      this.w_RIFNUM = 0
      this.w_DETTAGLIO = CCDETTAG
      * --- Select from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD  from "+i_cTable+" CONFDDIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_NEWCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDDIS")
      else
        select CPROWNUM,CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_NEWCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDDIS
      endif
      if used('_Curs_CONFDDIS')
        select _Curs_CONFDDIS
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDDIS.CPROWORD
        this.w_RIFNUM = _Curs_CONFDDIS.CPROWNUM
          select _Curs_CONFDDIS
          continue
        enddo
        use
      endif
      if this.w_RIFRIGA<>0 AND this.w_RIFNUM<>0
        select (this.NC1)
        * --- Inserimento Nuova Caratteristica
        * --- Write into CONFDDIS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
          i_cOp7=cp_SetTrsOp(,'CCFLINIT','upper(CCFLINIT)',upper(CCFLINIT),'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONFDDIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDETTAG ="+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
          +",CC__DEFA ="+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
          +",CCOPERAT ="+cp_NullLink(cp_ToStrODBC(iif(CCOPERA="X","*",CCOPERA)),'CONFDDIS','CCOPERAT');
          +",CC_COEFF ="+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
          +",CCDATINI ="+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
          +",CCDATFIN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
          +",CCFLINIT ="+cp_NullLink(i_cOp7,'CONFDDIS','CCFLINIT');
              +i_ccchkf ;
          +" where ";
              +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
              +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
              +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                 )
        else
          update (i_cTable) set;
              CCDETTAG = CCDETTAG;
              ,CC__DEFA = upper(CCDEFAULT);
              ,CCOPERAT = iif(CCOPERA="X","*",CCOPERA);
              ,CC_COEFF = CCCOEFF ;
              ,CCDATINI = i_datsys;
              ,CCDATFIN = cp_CharToDate("31-12-2099");
              ,CCFLINIT = &i_cOp7.;
              &i_ccchkf. ;
           where;
              CC__DIBA = this.w_DISTINTA;
              and CCROWORD = this.w_RIGDIS;
              and CCCOMPON = this.w_KEYRIC;
              and CCTIPOCA = this.oParentObject.w_TIPOCA;
              and CCCODICE = this.oParentObject.w_OLDCAR;
              and CPROWNUM = this.w_RIFNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Select from CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM,MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" CONFDDIS ";
              +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+"";
               ,"_Curs_CONFDDIS")
        else
          select MAX(CPROWNUM) AS CPROWNUM,MAX(CPROWORD) AS CPROWORD from (i_cTable);
           where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR;
            into cursor _Curs_CONFDDIS
        endif
        if used('_Curs_CONFDDIS')
          select _Curs_CONFDDIS
          locate for 1=1
          do while not(eof())
          this.w_RIFRIGA = NVL(_Curs_CONFDDIS.CPROWORD,0)+10
          this.w_RIFNUM = NVL(_Curs_CONFDDIS.CPROWNUM,0)+1
            select _Curs_CONFDDIS
            continue
          enddo
          use
        endif
        select (this.NC1)
        * --- Insert into CONFDDIS
        i_nConn=i_TableProp[this.CONFDDIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDDIS_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CC__DIBA"+",CCROWORD"+",CCCOMPON"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCDATINI"+",CCDATFIN"+",CCFLINIT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DISTINTA),'CONFDDIS','CC__DIBA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGDIS),'CONFDDIS','CCROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_KEYRIC),'CONFDDIS','CCCOMPON');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOCA),'CONFDDIS','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDCAR),'CONFDDIS','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RIFNUM),'CONFDDIS','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(CPROWORD),'CONFDDIS','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(CCDETTAG),'CONFDDIS','CCDETTAG');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCDEFAULT)),'CONFDDIS','CC__DEFA');
          +","+cp_NullLink(cp_ToStrODBC(CCOPERA),'CONFDDIS','CCOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(CCCOEFF ),'CONFDDIS','CC_COEFF');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'CONFDDIS','CCDATINI');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("31-12-2099")),'CONFDDIS','CCDATFIN');
          +","+cp_NullLink(cp_ToStrODBC(upper(CCFLINIT)),'CONFDDIS','CCFLINIT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CC__DIBA',this.w_DISTINTA,'CCROWORD',this.w_RIGDIS,'CCCOMPON',this.w_KEYRIC,'CCTIPOCA',this.oParentObject.w_TIPOCA,'CCCODICE',this.oParentObject.w_OLDCAR,'CPROWNUM',this.w_RIFNUM,'CPROWORD',CPROWORD,'CCDETTAG',CCDETTAG,'CC__DEFA',upper(CCDEFAULT),'CCOPERAT',CCOPERA,'CC_COEFF',CCCOEFF ,'CCDATINI',i_datsys)
          insert into (i_cTable) (CC__DIBA,CCROWORD,CCCOMPON,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDATINI,CCDATFIN,CCFLINIT &i_ccchkf. );
             values (;
               this.w_DISTINTA;
               ,this.w_RIGDIS;
               ,this.w_KEYRIC;
               ,this.oParentObject.w_TIPOCA;
               ,this.oParentObject.w_OLDCAR;
               ,this.w_RIFNUM;
               ,CPROWORD;
               ,CCDETTAG;
               ,upper(CCDEFAULT);
               ,CCOPERA;
               ,CCCOEFF ;
               ,i_datsys;
               ,cp_CharToDate("31-12-2099");
               ,upper(CCFLINIT);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --ELiminazione Dettaglio
    * --- Elimino la caratteristica nell'Articolo/Distinta
    select (this.NC2) 
 locate for xchk=0
    if !(found())
      * --- --Sto cercando di cancellare tutto il dettaglio associato alla codice caratteristica
      ah_ErrorMsg("Si sta cercando di cancellare tutte le scelte associate operazione non consentita!.",48," ")
      i_retcode = 'stop'
      return
    else
      * --- Try
      local bErr_03BA7BF0
      bErr_03BA7BF0=bTrsErr
      this.Try_03BA7BF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_SOSTIT = -1
      endif
      bTrsErr=bTrsErr or bErr_03BA7BF0
      * --- End
    endif
  endproc
  proc Try_03BA7BF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seleziona i componenti da eliminare
    select (this.NC) 
 go top 
 scan for xChk=1
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    if this.w_TIPOREC="Articoli"
      * --- Eliminazione record caratteristica sugli ARTICOLI (CONF_ART e CONFDART)
      this.w_SOSTIT = this.w_SOSTIT+1
      select (this.NC2) 
 scan for xchk=1
      this.w_DETTAGLIO = CCDETTAG
      * --- Select from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM AS CPROWNUM ,CPROWORD  from "+i_cTable+" CONFDART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+"";
             ,"_Curs_CONFDART")
      else
        select CPROWNUM AS CPROWNUM ,CPROWORD from (i_cTable);
         where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO;
          into cursor _Curs_CONFDART
      endif
      if used('_Curs_CONFDART')
        select _Curs_CONFDART
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDART.CPROWORD
        this.w_RIFNUM = _Curs_CONFDART.CPROWNUM
        if this.w_RIFRIGA<>0 AND this.w_RIFNUM<>0
          * --- Delete from CONFDART
          i_nConn=i_TableProp[this.CONFDART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCCODART = "+cp_ToStrODBC(this.w_CCCODART);
                  +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
                  +" and CPROWORD = "+cp_ToStrODBC(this.w_RIFRIGA);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                  +" and CCDETTAG = "+cp_ToStrODBC(this.w_DETTAGLIO);
                   )
          else
            delete from (i_cTable) where;
                  CCCODART = this.w_CCCODART;
                  and CCTIPOCA = this.oParentObject.w_TIPOCA;
                  and CCCODICE = this.oParentObject.w_OLDCAR;
                  and CPROWORD = this.w_RIFRIGA;
                  and CPROWNUM = this.w_RIFNUM;
                  and CCDETTAG = this.w_DETTAGLIO;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
          select _Curs_CONFDART
          continue
        enddo
        use
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
      * --- Select from CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2],.t.,this.CONFDART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CUNT(CPROWNUM) AS CONTA  from "+i_cTable+" CONFDART ";
            +" where CCCODART="+cp_ToStrODBC(this.w_CCCODART)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CCDETTAG="+cp_ToStrODBC(this.w_DETTAGLIO)+" AND CC__DEFA='S'";
             ,"_Curs_CONFDART")
      else
        select CUNT(CPROWNUM) AS CONTA from (i_cTable);
         where CCCODART=this.w_CCCODART AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CCDETTAG=this.w_DETTAGLIO AND CC__DEFA="S";
          into cursor _Curs_CONFDART
      endif
      if used('_Curs_CONFDART')
        select _Curs_CONFDART
        locate for 1=1
        do while not(eof())
        this.w_conta = _Curs_CONFDART.CONTA
          select _Curs_CONFDART
          continue
        enddo
        use
      endif
    else
      * --- Eliminazione record caratteristica sui componenti distinta base (CONF_DIS e CONFDDIS)
      this.w_SOSTIT = this.w_SOSTIT+1
      select (this.NC2) 
 scan for xchk=1
      this.w_DETTAGLIO = CCDETTAG
      * --- Select from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD  from "+i_cTable+" CONFDDIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+"";
             ,"_Curs_CONFDDIS")
      else
        select CPROWNUM,CPROWORD from (i_cTable);
         where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR;
          into cursor _Curs_CONFDDIS
      endif
      if used('_Curs_CONFDDIS')
        select _Curs_CONFDDIS
        locate for 1=1
        do while not(eof())
        this.w_RIFRIGA = _Curs_CONFDDIS.CPROWORD
        this.w_RIFNUM = _Curs_CONFDDIS.CPROWNUM
        if this.w_RIFRIGA<>0 AND this.w_RIFNUM<>0
          * --- Delete from CONFDDIS
          i_nConn=i_TableProp[this.CONFDDIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CC__DIBA = "+cp_ToStrODBC(this.w_DISTINTA);
                  +" and CCROWORD = "+cp_ToStrODBC(this.w_RIGDIS);
                  +" and CCCOMPON = "+cp_ToStrODBC(this.w_KEYRIC);
                  +" and CCTIPOCA = "+cp_ToStrODBC(this.oParentObject.w_TIPOCA);
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLDCAR);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFNUM);
                  +" and CCDETTAG = "+cp_ToStrODBC(this.w_DETTAGLIO);
                   )
          else
            delete from (i_cTable) where;
                  CC__DIBA = this.w_DISTINTA;
                  and CCROWORD = this.w_RIGDIS;
                  and CCCOMPON = this.w_KEYRIC;
                  and CCTIPOCA = this.oParentObject.w_TIPOCA;
                  and CCCODICE = this.oParentObject.w_OLDCAR;
                  and CPROWNUM = this.w_RIFNUM;
                  and CCDETTAG = this.w_DETTAGLIO;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
          select _Curs_CONFDDIS
          continue
        enddo
        use
      endif
      endscan
      select (this.NC) 
 go this.w_RECNO
      * --- --Si controlla se Tutto il dettaglio � stato eliminato evidenziandolo nel report di
      *     stampa
      * --- Select from CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2],.t.,this.CONFDDIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(CPROWNUM) as CONTA  from "+i_cTable+" CONFDDIS ";
            +" where CC__DIBA="+cp_ToStrODBC(this.w_DISTINTA)+" AND CCROWORD="+cp_ToStrODBC(this.w_RIGDIS)+" AND CCCOMPON="+cp_ToStrODBC(this.w_KEYRIC)+" AND CCTIPOCA="+cp_ToStrODBC(this.oParentObject.w_TIPOCA)+" AND CCCODICE="+cp_ToStrODBC(this.oParentObject.w_OLDCAR)+" AND CC__DEFA='S'";
             ,"_Curs_CONFDDIS")
      else
        select Count(CPROWNUM) as CONTA from (i_cTable);
         where CC__DIBA=this.w_DISTINTA AND CCROWORD=this.w_RIGDIS AND CCCOMPON=this.w_KEYRIC AND CCTIPOCA=this.oParentObject.w_TIPOCA AND CCCODICE=this.oParentObject.w_OLDCAR AND CC__DEFA="S";
          into cursor _Curs_CONFDDIS
      endif
      if used('_Curs_CONFDDIS')
        select _Curs_CONFDDIS
        locate for 1=1
        do while not(eof())
        this.w_conta = _Curs_CONFDDIS.CONTA
          select _Curs_CONFDDIS
          continue
        enddo
        use
      endif
    endif
    if this.w_conta=0
      insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,"Z")
    endif
    insert into _AGG_ (DISTINTA,ARTICOLO,ARDESCRI,TIPO,OPERAZ) values(this.w_DISTINTA,this.w_KEYRIC,this.w_DESART,this.w_TIPOREC,this.oParentObject.w_operaz)
    select (this.NC)
    go this.w_RECNO
    endscan
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Pulitora zomm valori di dettaglio
    select (this.NC) 
 go top 
 SCAN
    this.w_TIPOREC = TIPO
    this.w_CCCODART = left(CCCOMPON,20)
    this.w_KEYRIC = CCCOMPON
    this.w_DESART = ARDESART
    this.w_DISTINTA = CC__DIBA 
    this.w_STATDI = iif(STATO="R",STATO,"M")
    this.w_RIGDIS = RIGDIS
    this.w_RECNO = recno()
    this.w_RIFRIGA = 0
    this.w_RIFNUM = 0
    this.w_DETLEG = .F.
    * --- --Per ogni elemento contenuto nello zoom occorre  verificare se ha il dettaglio
    *     selezionato nel legame
    select (this.NC2) 
 Go top 
 scan for xChk=1
    this.w_DETTAGLIO = CCDETTAG
    if this.oParentObject.w_DEFAULT="S"
      this.w_DEFA = CCDEFAULT
    else
      this.w_DEFA = " "
    endif
    if this.w_TIPOREC="Articoli"
      * --- --Viene controllata l'appartenenza del dettaglio al legame
      vq_exec("..\CCAR\EXE\QUERY\GSCR_KCC3", this, "cCurs")
    else
      vq_exec("..\CCAR\EXE\QUERY\GSCR_KCC2", this, "cCurs")
    endif
    if reccount("cCurs")=0
      * --- --La scelta non � presente e quindi non puo essere sostituita
      *     viene cancell'ato l'articolo o la distinta dallo zoom
      this.w_DETLEG = .T.
    endif
    if used("cCursM")
      use in cCurs
    endif
    endscan
    if (this.w_DETLEG)
      if this.w_tiporec="Disitinta"
        SELECT(this.NC) 
 DELETE for CC__DIBA=this.w_DISTINTA AND CCCOMPON=this.w_KEYRIC AND TIPO=this.w_TIPOREC AND STATO=this.w_STATDI
      else
        SELECT(this.NC) 
 DELETE for CC__DIBA=this.w_DISTINTA AND CCCOMPON=this.w_KEYRIC AND TIPO=this.w_TIPOREC 
      endif
      go this.w_RECNO
    endif
    this.w_DETLEG = .F.
    select (this.NC)
    go this.w_RECNO
    endscan
    select (this.NC) 
 Go top
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CONF_ART'
    this.cWorkTables[2]='CONFDART'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='CONF_DIS'
    this.cWorkTables[5]='CONFDDIS'
    this.cWorkTables[6]='DISTBASE'
    this.cWorkTables[7]='CONFDCAR'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    if used('_Curs_CONF_DIS')
      use in _Curs_CONF_DIS
    endif
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    if used('_Curs_CONF_DIS')
      use in _Curs_CONF_DIS
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDART')
      use in _Curs_CONFDART
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    if used('_Curs_CONFDDIS')
      use in _Curs_CONFDDIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
