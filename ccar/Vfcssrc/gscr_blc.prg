* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_blc                                                        *
*              Gestione conf legami                                            *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-09                                                      *
* Last revis.: 2015-09-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_blc",oParentObject,m.w_OPER)
return(i_retval)

define class tgscr_blc as StdBatch
  * --- Local variables
  w_OPER = space(10)
  w_DISTINTA = space(41)
  w_CCOMPON = space(41)
  w_CODICE = space(5)
  w_CCODART = space(20)
  w_RIGADIBA = 0
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_CCTIPOCA = space(1)
  w_CC__DEFA = space(1)
  w_CC_COEFF = 0
  w_CCOPERAT = space(1)
  w_CCDETTAG = space(40)
  GSDS_MLC = .NULL.
  GSDS_MLD = .NULL.
  GSDS_ALP = .NULL.
  w_GESCAR = space(1)
  w_TIPOCA = space(2)
  w_CODICE = space(5)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_DETTAG = space(40)
  w_DEFA = space(1)
  w_OPERAT = space(1)
  w_COEFF = 0
  w_DESSUP = space(80)
  w_nRec = 0
  w_ARGESCAR = space(1)
  GSDS_MLD = .NULL.
  * --- WorkFile variables
  CONF_ART_idx=0
  KEY_ARTI_idx=0
  CONF_DIS_idx=0
  CONFDART_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Detail Caratt. di Conf. Articolo (GSDS_MLC)
    this.GSDS_MLC = this.oParentObject
    this.w_DISTINTA = this.oParentObject.w_CC__DIBA
    this.w_CCOMPON = this.oParentObject.w_CCCOMPON
    this.w_CCODART = this.oParentObject.w_CCCODART
    this.w_RIGADIBA = this.GSDS_MLC.w_ccroword
    cCurMLC = this.GSDS_MLC.cTrsname
    do case
      case this.w_OPER = "CCCOMPON"
        * --- Riempie l'elenco delle caratteristiche
        if not empty(cCurMLC) and used(cCurMLC)
          Select (cCurMLC) 
 delete all
          this.GSDS_MLC.InitRow()     
          * --- Select from CONF_ART
          i_nConn=i_TableProp[this.CONF_ART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CCTIPOCA,CPROWORD,CCCODICE,CCDESCRI  from "+i_cTable+" CONF_ART ";
                +" where CCCODART="+cp_ToStrODBC(this.oParentObject.w_CCCODART)+"";
                +" order by CPROWORD";
                 ,"_Curs_CONF_ART")
          else
            select CCTIPOCA,CPROWORD,CCCODICE,CCDESCRI from (i_cTable);
             where CCCODART=this.oParentObject.w_CCCODART;
             order by CPROWORD;
              into cursor _Curs_CONF_ART
          endif
          if used('_Curs_CONF_ART')
            select _Curs_CONF_ART
            locate for 1=1
            do while not(eof())
            this.GSDS_MLC.AddRow()     
            this.GSDS_MLC.w_CCTIPOCA = _Curs_CONF_ART.CCTIPOCA
            this.GSDS_MLC.w_TIPOCA = _Curs_CONF_ART.CCTIPOCA
            this.GSDS_MLC.w_CPROWORD = _Curs_CONF_ART.CPROWORD
            this.GSDS_MLC.w_CCCODICE = _Curs_CONF_ART.CCCODICE
            this.GSDS_MLC.w_CCDESCRI = _Curs_CONF_ART.CCDESCRI
            this.GSDS_MLC.w_CCDATINI = I_DATSYS
            this.GSDS_MLC.w_CCDATFIN = cp_CharToDate("31-12-2099")
            * --- Carica il Temporaneo dei Dati e skippa al record successivo
            this.GSDS_MLC.TrsFromWork()     
            this.GSDS_MLC.ChildrenChangeRow()     
            this.GSDS_MLC.NotifyEvent("w_CCCODICE Changed")     
              select _Curs_CONF_ART
              continue
            enddo
            use
          endif
          * --- Rinfrescamenti vari
          SELECT (cCurMLC) 
 GO top 
 With this.GSDS_MLC 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=0 
 .oPgFrm.Page1.oPag.oBody.nRelRow=0 
 .bUpDated = TRUE 
 EndWith
        endif
      case this.w_OPER = "CONFCAR"
        * --- Select from CONF_DIS
        i_nConn=i_TableProp[this.CONF_DIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2],.t.,this.CONF_DIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS NREC  from "+i_cTable+" CONF_DIS ";
              +" where CC__DIBA="+cp_ToStrODBC(this.oParentObject.w_CC__DIBA)+" AND CCCOMPON="+cp_ToStrODBC(this.oParentObject.w_CCCOMPON)+"";
               ,"_Curs_CONF_DIS")
        else
          select COUNT(*) AS NREC from (i_cTable);
           where CC__DIBA=this.oParentObject.w_CC__DIBA AND CCCOMPON=this.oParentObject.w_CCCOMPON;
            into cursor _Curs_CONF_DIS
        endif
        if used('_Curs_CONF_DIS')
          select _Curs_CONF_DIS
          locate for 1=1
          do while not(eof())
          this.w_nRec = NVL(_Curs_CONF_DIS.NREC, 0)
            select _Curs_CONF_DIS
            continue
          enddo
          use
        endif
        if this.w_nRec > 0
          * --- Select from ART_ICOL
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ARGESCAR AS ARGESCAR  from "+i_cTable+" ART_ICOL ";
                +" where ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART)+"";
                 ,"_Curs_ART_ICOL")
          else
            select ARGESCAR AS ARGESCAR from (i_cTable);
             where ARCODART = this.oParentObject.w_CCCODART;
              into cursor _Curs_ART_ICOL
          endif
          if used('_Curs_ART_ICOL')
            select _Curs_ART_ICOL
            locate for 1=1
            do while not(eof())
            this.w_ARGESCAR = NVL(_Curs_ART_ICOL.ARGESCAR, "N")
            if this.w_ARGESCAR<>"S"
              if ah_yesno("Si vuole importare i legami anche sull'articolo%0Caricare ora?")
                * --- --Primo inserimento
                * --- Riempie le caratterestiche associate all'articolo
                * --- Try
                local bErr_03C11B08
                bErr_03C11B08=bTrsErr
                this.Try_03C11B08()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                  ah_ErrorMsg("Impossibile inserire le caratteristiche sull'articolo",16)
                endif
                bTrsErr=bTrsErr or bErr_03C11B08
                * --- End
              else
                i_retcode = 'stop'
                return
              endif
            endif
              select _Curs_ART_ICOL
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_03C11B08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from CONF_DIS
    i_nConn=i_TableProp[this.CONF_DIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2],.t.,this.CONF_DIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CPROWORD,CCTIPOCA,CCCODICE,CCDESCRI,CCCOMPON,CC__DIBA  from "+i_cTable+" CONF_DIS ";
          +" where CC__DIBA="+cp_ToStrODBC(this.oParentObject.w_CC__DIBA)+" AND CCCOMPON="+cp_ToStrODBC(this.oParentObject.w_CCCOMPON)+"";
           ,"_Curs_CONF_DIS")
    else
      select CPROWORD,CCTIPOCA,CCCODICE,CCDESCRI,CCCOMPON,CC__DIBA from (i_cTable);
       where CC__DIBA=this.oParentObject.w_CC__DIBA AND CCCOMPON=this.oParentObject.w_CCCOMPON;
        into cursor _Curs_CONF_DIS
    endif
    if used('_Curs_CONF_DIS')
      select _Curs_CONF_DIS
      locate for 1=1
      do while not(eof())
      this.w_CCTIPOCA = _Curs_CONF_DIS.CCTIPOCA
      this.w_CCCODICE = _Curs_CONF_DIS.CCCODICE
      this.w_CCDESCRI = _Curs_CONF_DIS.CCDESCRI
      this.w_CPROWORD = _Curs_CONF_DIS.CPROWORD
      this.w_CODICE = _Curs_CONF_DIS.CCCODICE
      * --- --Si controlla se la caratteristica � gia presente
      * --- Try
      local bErr_03C126D8
      bErr_03C126D8=bTrsErr
      this.Try_03C126D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into CONF_ART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONF_ART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONF_ART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CONF_ART','CCDESCRI');
          +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CONF_ART','CPROWORD');
              +i_ccchkf ;
          +" where ";
              +"CCCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
              +" and CCTIPOCA = "+cp_ToStrODBC(this.w_CCTIPOCA);
              +" and CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
                 )
        else
          update (i_cTable) set;
              CCDESCRI = this.w_CCDESCRI;
              ,CPROWORD = this.w_CPROWORD;
              &i_ccchkf. ;
           where;
              CCCODART = this.oParentObject.w_CCCODART;
              and CCTIPOCA = this.w_CCTIPOCA;
              and CCCODICE = this.w_CCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_03C126D8
      * --- End
      * --- Try
      local bErr_03C12618
      bErr_03C12618=bTrsErr
      this.Try_03C12618()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03C12618
      * --- End
        select _Curs_CONF_DIS
        continue
      enddo
      use
    endif
    * --- --AGGIORNAMENTO CARATTERISTICHE PER GESTIONE ARTICOLO
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARGESCAR ="+cp_NullLink(cp_ToStrODBC("S"),'ART_ICOL','ARGESCAR');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
             )
    else
      update (i_cTable) set;
          ARGESCAR = "S";
          &i_ccchkf. ;
       where;
          ARCODART = this.oParentObject.w_CCCODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03C126D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONF_ART
    i_nConn=i_TableProp[this.CONF_ART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONF_ART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CCDESCRI"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CCCODART),'CONF_ART','CCCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPOCA),'CONF_ART','CCTIPOCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CONF_ART','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CONF_ART','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CONF_ART','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.oParentObject.w_CCCODART,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CPROWORD',this.w_CPROWORD)
      insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CCDESCRI,CPROWORD &i_ccchkf. );
         values (;
           this.oParentObject.w_CCCODART;
           ,this.w_CCTIPOCA;
           ,this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03C12618()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_CCTIPOCA<>"C"
      * --- Insert into CONFDART
      i_nConn=i_TableProp[this.CONFDART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSCR_BLC",this.CONFDART_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      this.GSDS_MLD = this.GSDS_MLC.GSDS_MLD
      this.GSDS_MLD.MarkPos()     
      this.GSDS_MLD.FirstRow()     
      do while Not this.GSDS_MLD.Eof_Trs()
        this.GSDS_MLD.SetRow()     
        this.w_ROWNUM = this.GSDS_MLD.w_CPROWNUM
        this.w_ROWORD = this.GSDS_MLD.w_CPROWORD
        this.w_DETTAG = this.GSDS_MLD.w_CCDETTAG
        this.w_DEFA = this.GSDS_MLD.w_CC__DEFA
        this.w_OPERAT = this.GSDS_MLD.w_CCOPERAT
        this.w_COEFF = this.GSDS_MLD.w_CC_COEFF
        this.w_DESSUP = this.GSDS_MLD.w_CCDESSUP
        * --- Insert into CONFDART
        i_nConn=i_TableProp[this.CONFDART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONFDART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONFDART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCCODART"+",CCTIPOCA"+",CCCODICE"+",CPROWNUM"+",CPROWORD"+",CCDETTAG"+",CC__DEFA"+",CCOPERAT"+",CC_COEFF"+",CCDESSUP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CCODART),'CONFDART','CCCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPOCA),'CONFDART','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CONFDART','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CONFDART','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'CONFDART','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DETTAG),'CONFDART','CCDETTAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEFA),'CONFDART','CC__DEFA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OPERAT),'CONFDART','CCOPERAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COEFF),'CONFDART','CC_COEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'CONFDART','CCDESSUP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCODART,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE,'CPROWNUM',this.w_ROWNUM,'CPROWORD',this.w_ROWORD,'CCDETTAG',this.w_DETTAG,'CC__DEFA',this.w_DEFA,'CCOPERAT',this.w_OPERAT,'CC_COEFF',this.w_COEFF,'CCDESSUP',this.w_DESSUP)
          insert into (i_cTable) (CCCODART,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDESSUP &i_ccchkf. );
             values (;
               this.w_CCODART;
               ,this.w_CCTIPOCA;
               ,this.w_CCCODICE;
               ,this.w_ROWNUM;
               ,this.w_ROWORD;
               ,this.w_DETTAG;
               ,this.w_DEFA;
               ,this.w_OPERAT;
               ,this.w_COEFF;
               ,this.w_DESSUP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.GSDS_MLD.NextRow()     
      enddo
      this.GSDS_MLD.RePos()     
    endif
    return


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONF_ART'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='CONF_DIS'
    this.cWorkTables[4]='CONFDART'
    this.cWorkTables[5]='ART_ICOL'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    if used('_Curs_CONF_DIS')
      use in _Curs_CONF_DIS
    endif
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    if used('_Curs_CONF_DIS')
      use in _Curs_CONF_DIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
