* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr1bsd                                                        *
*              Gestione eventi stampa distinte                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-08                                                      *
* Last revis.: 2015-03-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr1bsd",oParentObject,m.pParam)
return(i_retval)

define class tgscr1bsd as StdBatch
  * --- Local variables
  pParam = space(3)
  w_PADOBJ = .NULL.
  w_TIPOCA = space(1)
  w_CODCAR = space(5)
  w_CC__DIBA = space(41)
  w_CCROWORD = 0
  w_STRCONF = space(0)
  w_STRCONF2 = space(0)
  w_ZOOM = .NULL.
  w_VALDEFA = space(1)
  w_LROWORD = 0
  w_COUNTSEL = 0
  w_COUNTER = 0
  * --- WorkFile variables
  CONFDDIS_idx=0
  MODE_DIS_idx=0
  TMPCARAT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione eventi selezioni caratteristiche da GSCR_SDS (Stampa distinte), GSDB_SDS e GSCR_SIM (Stampa implosione)
    *     pParam:
    *     'AQ1'=  Zoom GSCR1SDI After Query
    *     'CU1'= Check/Uncheck Row Zoom GSCR1SDI
    *     'CU2'= Check/Uncheck Row Zoom GSCR2SDI
    *     'CU3'= Check/Uncheck Row Zoom GSCR3SDI
    *     'CU4'= Check/Uncheck Row Zoom GSCR4SDI
    *     'CU5'= Check/Uncheck Row Zoom GSCR5SDI
    *     'CU6'= Check/Uncheck Row Zoom GSCR6SDI
    *     'ESC'= Uscita
    * --- Variabili per scrittura su temporaneo
    if .f.
      * --- Create temporary table TMPCARAT
      i_nIdx=cp_AddTableDef('TMPCARAT') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPCARAT_proto';
            )
      this.TMPCARAT_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    NUMREC = 0
    this.w_PADOBJ = this.oParentObject
    this.TMPCARAT_idx=cp_GetTableDefIdx("TMPCARAT")
    do case
      case this.pParam="AQ1"
        * --- Creazione della tabella temporanea TMPCARAT
        * --- Create temporary table TMPCARAT
        i_nIdx=cp_AddTableDef('TMPCARAT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPCARAT_proto';
              )
        this.TMPCARAT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      case this.pParam="CU1"
        NC= this.w_PADOBJ.w_GSCR1SDS.cCursor
        if &NC..XCHK=1
          this.w_STRCONF = "C"+alltrim(NVL(CCCODICE,""))
          * --- Insert into TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("C"),'TMPCARAT','CCTIPOCA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE),'TMPCARAT','CCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"C",'CCCODICE',this.oParentObject.w_CODICE,'STCONFIG',this.w_STRCONF)
            insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
               values (;
                 "C";
                 ,this.oParentObject.w_CODICE;
                 ,this.w_STRCONF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Delete from TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                  +" and CCTIPOCA = "+cp_ToStrODBC("C");
                   )
          else
            delete from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_CODICE;
                  and CCTIPOCA = "C";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      case this.pParam="SE1"
        this.w_PADOBJ.w_GSCR2SDS.cCpQueryName = "..\CCAR\EXE\QUERY\GSCR2ASDSI"
        this.w_PADOBJ.NotifyEvent("Interroga2")     
        NC= this.w_PADOBJ.w_GSCR2SDS.cCursor
        UPDATE (NC) set XCHK=1 where !EMPTY(NVL(TEST,""))
      case this.pParam="CU2"
        NC= this.w_PADOBJ.w_GSCR2SDS.cCursor
        Select (NC) 
 GO TOP 
 SCAN
        this.w_STRCONF = "C"+alltrim(NVL(CCCODICE,""))+"("+ALLTRIM(STR(NVL(CPROWORD,0)))+")"
        if XCHK=1
          * --- Delete from TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCTIPOCA = "+cp_ToStrODBC("C");
                  +" and CCCODICE = "+cp_ToStrODBC(CCCODICE);
                  +" and CPROWORD is null ";
                   )
          else
            delete from (i_cTable) where;
                  CCTIPOCA = "C";
                  and CCCODICE = CCCODICE;
                  and CPROWORD is null ;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Insert into TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+",CPROWORD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("C"),'TMPCARAT','CCTIPOCA');
            +","+cp_NullLink(cp_ToStrODBC(CCCODICE),'TMPCARAT','CCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
            +","+cp_NullLink(cp_ToStrODBC(NVL(CPROWORD,0)),'TMPCARAT','CPROWORD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"C",'CCCODICE',CCCODICE,'STCONFIG',this.w_STRCONF,'CPROWORD',NVL(CPROWORD,0))
            insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG,CPROWORD &i_ccchkf. );
               values (;
                 "C";
                 ,CCCODICE;
                 ,this.w_STRCONF;
                 ,NVL(CPROWORD,0);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Delete from TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCTIPOCA = "+cp_ToStrODBC("C");
                  +" and CCCODICE = "+cp_ToStrODBC(CCCODICE);
                  +" and CPROWORD = "+cp_ToStrODBC(CPROWORD);
                   )
          else
            delete from (i_cTable) where;
                  CCTIPOCA = "C";
                  and CCCODICE = CCCODICE;
                  and CPROWORD = CPROWORD;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          if this.oParentObject.w_MXCHK=1
            * --- Read from TMPCARAT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TMPCARAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CCCODICE"+;
                " from "+i_cTable+" TMPCARAT where ";
                    +"CCTIPOCA = "+cp_ToStrODBC("C");
                    +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CCCODICE;
                from (i_cTable) where;
                    CCTIPOCA = "C";
                    and CCCODICE = this.oParentObject.w_CODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODCAR = NVL(cp_ToDate(_read_.CCCODICE),cp_NullValue(_read_.CCCODICE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if EMPTY(this.w_CODCAR)
              * --- Insert into TMPCARAT
              i_nConn=i_TableProp[this.TMPCARAT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC("C"),'TMPCARAT','CCTIPOCA');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE),'TMPCARAT','CCCODICE');
                +","+cp_NullLink(cp_ToStrODBC("C"+alltrim(this.oParentObject.w_CODICE)),'TMPCARAT','STCONFIG');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"C",'CCCODICE',this.oParentObject.w_CODICE,'STCONFIG',"C"+alltrim(this.oParentObject.w_CODICE))
                insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
                   values (;
                     "C";
                     ,this.oParentObject.w_CODICE;
                     ,"C"+alltrim(this.oParentObject.w_CODICE);
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
        endif
        ENDSCAN
      case this.pParam="CU3"
        NC= this.w_PADOBJ.w_GSCR3SDS.cCursor
        if &NC..XCHK=1
          this.w_STRCONF = "M"+alltrim(this.oParentObject.w_CODICE2)+"("
          * --- Insert into TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("M"),'TMPCARAT','CCTIPOCA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE2),'TMPCARAT','CCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"M",'CCCODICE',this.oParentObject.w_CODICE2,'STCONFIG',this.w_STRCONF)
            insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
               values (;
                 "M";
                 ,this.oParentObject.w_CODICE2;
                 ,this.w_STRCONF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Delete from TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE2);
                  +" and CCTIPOCA = "+cp_ToStrODBC("M");
                   )
          else
            delete from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_CODICE2;
                  and CCTIPOCA = "M";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      case this.pParam="SE3"
        this.w_PADOBJ.NotifyEvent("Interroga4")     
        NC= this.w_PADOBJ.w_GSCR4SDS.cCursor
        Select (NC)
        NUMREC=RECCOUNT(NC)
        GO TOP 
 SCAN while recno()<=NUMREC
        RECPOS=recno()
        this.w_STRCONF = NVL(CCCONFIG,"")
        this.w_STRCONF2 = SUBSTR(SUBSTR(CCCONFIG,AT("M"+alltrim(this.oParentObject.w_CODICE2),CCCONFIG)),AT("(",SUBSTR(CCCONFIG,AT("M"+alltrim(this.oParentObject.w_CODICE2),CCCONFIG)))+1,AT(")",SUBSTR(CCCONFIG,AT("M"+alltrim(this.oParentObject.w_CODICE2),CCCONFIG)))-AT("(",SUBSTR(CCCONFIG,AT("M"+alltrim(this.oParentObject.w_CODICE2),CCCONFIG)))-1)
        Locate for ALLTRIM(CCCONFIG)==ALLTRIM(this.w_STRCONF2)
        if !found()
          Insert into (NC) (CCCONFIG) values (this.w_STRCONF2)
        endif
        Delete from (NC) where ALLTRIM(CCCONFIG)==ALLTRIM(this.w_STRCONF)
        GOTO RECPOS
        ENDSCAN
        Delete from (NC) where EMPTY(NVL(CCCONFIG,""))
        NUMREC=0
        * --- Select from TMPCARAT
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CCCODICE,STCONFIG  from "+i_cTable+" TMPCARAT ";
              +" where CCCODICE="+cp_ToStrODBC(this.oParentObject.w_CODICE2)+" and CCTIPOCA='M'";
               ,"_Curs_TMPCARAT")
        else
          select CCCODICE,STCONFIG from (i_cTable);
           where CCCODICE=this.oParentObject.w_CODICE2 and CCTIPOCA="M";
            into cursor _Curs_TMPCARAT
        endif
        if used('_Curs_TMPCARAT')
          select _Curs_TMPCARAT
          locate for 1=1
          do while not(eof())
          this.w_STRCONF2 = NVL(_Curs_TMPCARAT.STCONFIG,"")
          UPDATE (NC) set XCHK=1 where "M"+alltrim(this.oParentObject.w_CODICE2)+"("+alltrim(CCCONFIG) like this.w_STRCONF2
            select _Curs_TMPCARAT
            continue
          enddo
          use
        endif
        if .f.
        endif
        Select (NC) 
 GO TOP
        this.w_PADOBJ.w_GSCR4SDS.grd.refresh()     
      case this.pParam="CU4"
        * --- Delete from TMPCARAT
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCTIPOCA = "+cp_ToStrODBC("M");
                +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE2);
                 )
        else
          delete from (i_cTable) where;
                CCTIPOCA = "M";
                and CCCODICE = this.oParentObject.w_CODICE2;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        NC= this.w_PADOBJ.w_GSCR4SDS.cCursor
        Select (NC) 
 Scan for XCHK=1
        this.w_STRCONF = "M"+ALLTRIM(this.oParentObject.w_CODICE2)+"("+ALLTRIM(NVL(CCCONFIG,""))
        * --- Insert into TMPCARAT
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC("M"),'TMPCARAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE2),'TMPCARAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"M",'CCCODICE',this.oParentObject.w_CODICE2,'STCONFIG',this.w_STRCONF)
          insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
             values (;
               "M";
               ,this.oParentObject.w_CODICE2;
               ,this.w_STRCONF;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        EndScan
        if this.oParentObject.w_MXCHK2=1
          * --- Read from TMPCARAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCCODICE"+;
              " from "+i_cTable+" TMPCARAT where ";
                  +"CCTIPOCA = "+cp_ToStrODBC("M");
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCCODICE;
              from (i_cTable) where;
                  CCTIPOCA = "M";
                  and CCCODICE = this.oParentObject.w_CODICE2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCAR = NVL(cp_ToDate(_read_.CCCODICE),cp_NullValue(_read_.CCCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(this.w_CODCAR)
            * --- Insert into TMPCARAT
            i_nConn=i_TableProp[this.TMPCARAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("M"),'TMPCARAT','CCTIPOCA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE2),'TMPCARAT','CCCODICE');
              +","+cp_NullLink(cp_ToStrODBC("M"+alltrim(this.oParentObject.w_CODICE2)),'TMPCARAT','STCONFIG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"M",'CCCODICE',this.oParentObject.w_CODICE2,'STCONFIG',"M"+alltrim(this.oParentObject.w_CODICE2))
              insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
                 values (;
                   "M";
                   ,this.oParentObject.w_CODICE2;
                   ,"M"+alltrim(this.oParentObject.w_CODICE2);
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      case this.pParam="CU5"
        NC= this.w_PADOBJ.w_GSCR5SDS.cCursor
        if &NC..XCHK=1
          this.w_STRCONF = "D"+ALLTRIM(this.oParentObject.w_CODICE3)
          * --- Insert into TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("D"),'TMPCARAT','CCTIPOCA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE3),'TMPCARAT','CCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"D",'CCCODICE',this.oParentObject.w_CODICE3,'STCONFIG',this.w_STRCONF)
            insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
               values (;
                 "D";
                 ,this.oParentObject.w_CODICE3;
                 ,this.w_STRCONF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Delete from TMPCARAT
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE3);
                  +" and CCTIPOCA = "+cp_ToStrODBC("D");
                   )
          else
            delete from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_CODICE3;
                  and CCTIPOCA = "D";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      case this.pParam="SE5"
        this.w_PADOBJ.NotifyEvent("Interroga6")     
        NC= this.w_PADOBJ.w_GSCR6SDS.cCursor
        Select (NC)
        NUMREC=RECCOUNT(NC)
        GO TOP 
 SCAN while recno()<=NUMREC
        RECPOS=recno()
        this.w_STRCONF = NVL(CCCONFIG,"")
        this.w_STRCONF2 = SUBSTR(SUBSTR(CCCONFIG,AT("D"+alltrim(this.oParentObject.w_CODICE3),CCCONFIG)),AT("(",SUBSTR(CCCONFIG,AT("D"+alltrim(this.oParentObject.w_CODICE3),CCCONFIG)))+1,AT(")",SUBSTR(CCCONFIG,AT("D"+alltrim(this.oParentObject.w_CODICE3),CCCONFIG)))-AT("(",SUBSTR(CCCONFIG,AT("D"+alltrim(this.oParentObject.w_CODICE3),CCCONFIG)))-1)
        Locate for ALLTRIM(CCCONFIG)==ALLTRIM(this.w_STRCONF2)
        if !found()
          Insert into (NC) (CCCONFIG, SSCONFIG) values (this.w_STRCONF2, STRTRAN(this.w_STRCONF2,chr(13),""))
        endif
        Delete from (NC) where ALLTRIM(CCCONFIG)==ALLTRIM(this.w_STRCONF)
        * --- Select from GSCR24SDS
        do vq_exec with 'GSCR24SDS',this,'_Curs_GSCR24SDS','',.f.,.t.
        if used('_Curs_GSCR24SDS')
          select _Curs_GSCR24SDS
          locate for 1=1
          do while not(eof())
          UPDATE (NC) set xchk=1 where CCCONFIG like this.w_STRCONF2
            select _Curs_GSCR24SDS
            continue
          enddo
          use
        endif
        Select (NC)
        GOTO RECPOS
        ENDSCAN
        Delete from (NC) where EMPTY(NVL(CCCONFIG,""))
        Select (NC) 
 GO TOP
        this.w_PADOBJ.w_GSCR6SDS.grd.refresh()     
      case this.pParam="CU6"
        * --- Delete from TMPCARAT
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CCTIPOCA = "+cp_ToStrODBC("D");
                +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE3);
                 )
        else
          delete from (i_cTable) where;
                CCTIPOCA = "D";
                and CCCODICE = this.oParentObject.w_CODICE3;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        NC= this.w_PADOBJ.w_GSCR6SDS.cCursor
        Select (NC) 
 Scan for XCHK=1
        this.w_STRCONF = "D"+ALLTRIM(this.oParentObject.w_CODICE3)+"("+ALLTRIM(NVL(SSCONFIG,""))
        * --- Insert into TMPCARAT
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC("D"),'TMPCARAT','CCTIPOCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE3),'TMPCARAT','CCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_STRCONF),'TMPCARAT','STCONFIG');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"D",'CCCODICE',this.oParentObject.w_CODICE3,'STCONFIG',this.w_STRCONF)
          insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
             values (;
               "D";
               ,this.oParentObject.w_CODICE3;
               ,this.w_STRCONF;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        EndScan
        if this.oParentObject.w_MXCHK3=1
          * --- Read from TMPCARAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMPCARAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CCCODICE"+;
              " from "+i_cTable+" TMPCARAT where ";
                  +"CCTIPOCA = "+cp_ToStrODBC("D");
                  +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE3);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CCCODICE;
              from (i_cTable) where;
                  CCTIPOCA = "D";
                  and CCCODICE = this.oParentObject.w_CODICE3;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCAR = NVL(cp_ToDate(_read_.CCCODICE),cp_NullValue(_read_.CCCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if EMPTY(this.w_CODCAR)
            * --- Insert into TMPCARAT
            i_nConn=i_TableProp[this.TMPCARAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPCARAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CCTIPOCA"+",CCCODICE"+",STCONFIG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC("D"),'TMPCARAT','CCTIPOCA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE3),'TMPCARAT','CCCODICE');
              +","+cp_NullLink(cp_ToStrODBC("D"+alltrim(this.oParentObject.w_CODICE3)),'TMPCARAT','STCONFIG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',"D",'CCCODICE',this.oParentObject.w_CODICE3,'STCONFIG',"D"+alltrim(this.oParentObject.w_CODICE3))
              insert into (i_cTable) (CCTIPOCA,CCCODICE,STCONFIG &i_ccchkf. );
                 values (;
                   "D";
                   ,this.oParentObject.w_CODICE3;
                   ,"D"+alltrim(this.oParentObject.w_CODICE3);
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      case this.pParam="ESC"
        if this.TMPCARAT_idx > 0
          * --- Drop temporary table TMPCARAT
          i_nIdx=cp_GetTableDefIdx('TMPCARAT')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPCARAT')
          endif
          this.bUpdateParentObject=.f.
        endif
      case this.pParam="DEF"
        this.w_ZOOM = this.w_PADOBJ.w_GSCR2SDS.grd
        ND= this.w_PADOBJ.w_GSCR2SDS.cCursor
        SELECT (ND) 
 NUMREC=0 
 NUMREC=recno()
        Select (ND) 
 GO TOP 
 SCAN
        this.oParentObject.w_CODICE = NVL(CCCODICE,"")
        this.w_LROWORD = NVL(CPROWORD,0)
        * --- Write into TMPCARAT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCARAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLDEFA ="+cp_NullLink(cp_ToStrODBC("S"),'TMPCARAT','CCFLDEFA');
              +i_ccchkf ;
          +" where ";
              +"CCTIPOCA = "+cp_ToStrODBC("C");
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
              +" and CPROWORD = "+cp_ToStrODBC(this.w_LROWORD);
                 )
        else
          update (i_cTable) set;
              CCFLDEFA = "S";
              &i_ccchkf. ;
           where;
              CCTIPOCA = "C";
              and CCCODICE = this.oParentObject.w_CODICE;
              and CPROWORD = this.w_LROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ENDSCAN
        UPDATE (ND) set CCFLDEFA="S" where XCHK=1
        SELECT (ND)
        FOR I=1 TO this.w_ZOOM.ColumnCount
        NC = ALLTRIM(STR(I))
        if UPPER(this.w_ZOOM.Column&NC..ControlSource)<>"XCHK"
          this.w_ZOOM.Column&NC..DynamicBackColor = "IIF(CCFLDEFA='S', RGB(255,100,100), RGB(255,255,255))"
        endif
        ENDFOR
        if NUMREC<>0
          Select (ND) 
 if reccount()>=NUMREC 
 GOTO NUMREC 
 endif
        endif
      case this.pParam="DER"
        * --- Write into TMPCARAT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCARAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLDEFA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLDEFA),'TMPCARAT','CCFLDEFA');
              +i_ccchkf ;
          +" where ";
              +"CCTIPOCA = "+cp_ToStrODBC("C");
              +" and CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
              +" and CPROWORD = "+cp_ToStrODBC(this.oParentObject.w_ROWORD);
                 )
        else
          update (i_cTable) set;
              CCFLDEFA = this.oParentObject.w_FLDEFA;
              &i_ccchkf. ;
           where;
              CCTIPOCA = "C";
              and CCCODICE = this.oParentObject.w_CODICE;
              and CPROWORD = this.oParentObject.w_ROWORD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_ZOOM = this.w_PADOBJ.w_GSCR2SDS.grd
        ND= this.w_PADOBJ.w_GSCR2SDS.cCursor
        UPDATE (ND) set CCFLDEFA=this.oParentObject.w_FLDEFA where CPROWORD=this.oParentObject.w_ROWORD and XCHK=1
        SELECT (ND)
        FOR I=1 TO this.w_ZOOM.ColumnCount
        NC = ALLTRIM(STR(I))
        if UPPER(this.w_ZOOM.Column&NC..ControlSource)<>"XCHK"
          this.w_ZOOM.Column&NC..DynamicBackColor = "IIF(CCFLDEFA='S', RGB(255,100,100), RGB(255,255,255))"
        endif
        ENDFOR
        if NUMREC<>0
          Select (ND) 
 if reccount()>=NUMREC 
 GOTO NUMREC 
 endif
        endif
      case this.pParam="NEU"
        * --- Write into TMPCARAT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPCARAT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCARAT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCFLDEFA ="+cp_NullLink(cp_ToStrODBC(""),'TMPCARAT','CCFLDEFA');
              +i_ccchkf ;
          +" where ";
              +"1 = "+cp_ToStrODBC(1);
                 )
        else
          update (i_cTable) set;
              CCFLDEFA = "";
              &i_ccchkf. ;
           where;
              1 = 1;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.pParam="SEL"
        ND= this.w_PADOBJ.w_GSCR2SDS.cCursor
        UPDATE (ND) SET XCHK=iif(this.oParentObject.w_SELEZI="S",1,0)
        this.oParentObject.NotifyEvent("w_GSCR2SDS row checked")
    endcase
    if this.pParam <> "ESC"
      this.w_COUNTSEL = 0
      * --- Select from TMPCARAT
      i_nConn=i_TableProp[this.TMPCARAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count(CCCODICE) as conta  from "+i_cTable+" TMPCARAT ";
             ,"_Curs_TMPCARAT")
      else
        select count(CCCODICE) as conta from (i_cTable);
          into cursor _Curs_TMPCARAT
      endif
      if used('_Curs_TMPCARAT')
        select _Curs_TMPCARAT
        locate for 1=1
        do while not(eof())
        this.w_COUNTSEL = _Curs_TMPCARAT.CONTA
          select _Curs_TMPCARAT
          continue
        enddo
        use
      endif
      if this.w_COUNTER > 0
        Select (NC) 
 GOTO this.w_COUNTER
      endif
      this.oParentObject.w_SELECTION = iif(this.w_COUNTSEL=0,"N","S")
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONFDDIS'
    this.cWorkTables[2]='MODE_DIS'
    this.cWorkTables[3]='*TMPCARAT'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_TMPCARAT')
      use in _Curs_TMPCARAT
    endif
    if used('_Curs_GSCR24SDS')
      use in _Curs_GSCR24SDS
    endif
    if used('_Curs_TMPCARAT')
      use in _Curs_TMPCARAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
