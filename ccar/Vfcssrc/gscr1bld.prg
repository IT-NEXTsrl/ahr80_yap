* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr1bld                                                        *
*              Eventi da GSDS_MLD                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-31                                                      *
* Last revis.: 2015-04-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr1bld",oParentObject,m.pParam)
return(i_retval)

define class tgscr1bld as StdBatch
  * --- Local variables
  pParam = space(3)
  w_OBJ = .NULL.
  w_CURROW = 0
  w_COUNTER = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine chiamata al changed della variabile w_CCFLINIT dai detail GSDS_MLD e GSMA_MAD
    this.w_OBJ = this.oParentObject
    NC=this.w_OBJ.ctrsName
    do case
      case this.pParam="CHK"
        Select (NC)
        this.w_CURROW = RECNO()
        if this.oParentObject.w_CCFLINIT="S"
          Select (NC) 
 Count for NVL(t_CCFLINIT,0)=1 to this.w_COUNTER
          if this.w_COUNTER>1
            ah_errormsg("E' possibile specificare un solo valore di inizializzazione")
            this.oParentObject.w_CCFLINIT = "N"
          endif
          Select (NC) 
 GOTO this.w_CURROW
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
