* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_mcc                                                        *
*              Caratteristiche fisse                                           *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-08                                                      *
* Last revis.: 2014-12-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscr_mcc"))

* --- Class definition
define class tgscr_mcc as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 401
  Height = 355+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-10"
  HelpContextID=97088873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CONF_CAR_IDX = 0
  CONFDCAR_IDX = 0
  cFile = "CONF_CAR"
  cFileDetail = "CONFDCAR"
  cKeySelect = "CCTIPOCA,CCCODICE"
  cQueryFilter="CCTIPOCA='C'"
  cKeyWhere  = "CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyDetail  = "CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyDetailWhereODBC = '"CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CONFDCAR.CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CONFDCAR.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONFDCAR.CPROWORD'
  cPrg = "gscr_mcc"
  cComment = "Caratteristiche fisse"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_CPROWORD = 0
  w_CCDETTAG = space(40)
  w_CCDESSUP = space(80)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscr_mcc
  PROC F6()
     * --- disabilita F6
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONF_CAR','gscr_mcc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscr_mccPag1","gscr_mcc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Caratteristiche fisse")
      .Pages(1).HelpContextID = 189426088
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONF_CAR'
    this.cWorkTables[2]='CONFDCAR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONF_CAR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONF_CAR_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
      .w_CCCODICE = NVL(CCCODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CONF_CAR where CCTIPOCA=KeySet.CCTIPOCA
    *                            and CCCODICE=KeySet.CCCODICE
    *
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2],this.bLoadRecFilter,this.CONF_CAR_IDX,"gscr_mcc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONF_CAR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONF_CAR.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CONFDCAR.","CONF_CAR.")
      i_cTable = i_cTable+' CONF_CAR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
        .w_CCCODICE = NVL(CCCODICE,space(5))
        .w_CCDESCRI = NVL(CCDESCRI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONF_CAR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CONFDCAR where CCTIPOCA=KeySet.CCTIPOCA
      *                            and CCCODICE=KeySet.CCCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CONFDCAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONFDCAR_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CONFDCAR')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CONFDCAR.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CONFDCAR"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
        select * from (i_cTable) CONFDCAR where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CCDETTAG = NVL(CCDETTAG,space(40))
          .w_CCDESSUP = NVL(CCDESSUP,space(80))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_9.enabled = .oPgFrm.Page1.oPag.oBtn_1_9.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CCTIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_CPROWORD=10
      .w_CCDETTAG=space(40)
      .w_CCDESSUP=space(80)
      if .cFunction<>"Filter"
        .w_CCTIPOCA = 'C'
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONF_CAR')
    this.DoRTCalc(2,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oCCDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oCCDESSUP_2_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      .Page1.oPag.oObj_1_6.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_2.enabled = .t.
        .Page1.oPag.oCCDESCRI_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CONF_CAR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPOCA,"CCTIPOCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCDESCRI,"CCDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    i_lTable = "CONF_CAR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONF_CAR_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CCDETTAG C(40);
      ,t_CCDESSUP C(80);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscr_mccbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_2.controlsource=this.cTrsName+'.t_CCDETTAG'
    this.oPgFRm.Page1.oPag.oCCDESSUP_2_3.controlsource=this.cTrsName+'.t_CCDESSUP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONF_CAR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONF_CAR')
        i_extval=cp_InsertValODBCExtFlds(this,'CONF_CAR')
        local i_cFld
        i_cFld=" "+;
                  "(CCTIPOCA,CCCODICE,CCDESCRI"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CCTIPOCA)+;
                    ","+cp_ToStrODBC(this.w_CCCODICE)+;
                    ","+cp_ToStrODBC(this.w_CCDESCRI)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONF_CAR')
        i_extval=cp_InsertValVFPExtFlds(this,'CONF_CAR')
        cp_CheckDeletedKey(i_cTable,0,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable);
              (CCTIPOCA,CCCODICE,CCDESCRI &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CCTIPOCA;
                  ,this.w_CCCODICE;
                  ,this.w_CCDESCRI;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONFDCAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONFDCAR_IDX,2])
      *
      * insert into CONFDCAR
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CCTIPOCA,CCCODICE,CPROWORD,CCDETTAG,CCDESSUP,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCTIPOCA)+","+cp_ToStrODBC(this.w_CCCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CCDETTAG)+","+cp_ToStrODBC(this.w_CCDESSUP)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CCTIPOCA,this.w_CCCODICE,this.w_CPROWORD,this.w_CCDETTAG,this.w_CCDESSUP,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CONF_CAR
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_CAR')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_CAR')
          i_cWhere = cp_PKFox(i_cTable  ,'CCTIPOCA',this.w_CCTIPOCA  ,'CCCODICE',this.w_CCCODICE  )
          UPDATE (i_cTable) SET;
              CCDESCRI=this.w_CCDESCRI;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CCDETTAG))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CONFDCAR_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CONFDCAR_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CONFDCAR
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONFDCAR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CCDETTAG="+cp_ToStrODBC(this.w_CCDETTAG)+;
                     ",CCDESSUP="+cp_ToStrODBC(this.w_CCDESSUP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CCDETTAG=this.w_CCDETTAG;
                     ,CCDESSUP=this.w_CCDESSUP;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CCDETTAG))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CONFDCAR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CONFDCAR_IDX,2])
        *
        * delete CONFDCAR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
        *
        * delete CONF_CAR
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CCDETTAG))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_2.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_4.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_4.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESSUP_2_3.value==this.w_CCDESSUP)
      this.oPgFrm.Page1.oPag.oCCDESSUP_2_3.value=this.w_CCDESSUP
      replace t_CCDESSUP with this.oPgFrm.Page1.oPag.oCCDESSUP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_2.value==this.w_CCDETTAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_2.value=this.w_CCDETTAG
      replace t_CCDETTAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDETTAG_2_2.value
    endif
    cp_SetControlsValueExtFlds(this,'CONF_CAR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CCCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCCCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) and not(Empty(t_CCDETTAG)));
        into cursor __chk__
    if not(2<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"2"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not(Empty(.w_CCDETTAG))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not(Empty(t_CCDETTAG)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CCDETTAG=space(40)
      .w_CCDESSUP=space(80)
    endwith
    this.DoRTCalc(1,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CCDETTAG = t_CCDETTAG
    this.w_CCDESSUP = t_CCDESSUP
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CCDETTAG with this.w_CCDETTAG
    replace t_CCDESSUP with this.w_CCDESSUP
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscr_mccPag1 as StdContainer
  Width  = 397
  height = 355
  stdWidth  = 397
  stdheight = 355
  resizeXpos=261
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_2 as StdField with uid="THHYQVBSXR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCTIPOCA,CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice caratteristica",;
    HelpContextID = 130679403,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=62, Left=90, Top=17, InputMask=replicate('X',5)

  add object oCCDESCRI_1_4 as StdField with uid="QENZANMWWG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione caratteristica",;
    HelpContextID = 45093487,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=90, Top=40, InputMask=replicate('X',40)


  add object oObj_1_6 as cp_runprogram with uid="OCQKPXZTAM",left=410, top=78, width=173,height=21,;
    caption='GSCR_BKK',;
   bGlobalFont=.t.,;
    prg="GSCR_BKK('INSERT')",;
    cEvent = "Insert row end",;
    nPag=1;
    , HelpContextID = 226684239


  add object oObj_1_7 as cp_runprogram with uid="YZDQSJGLLI",left=410, top=106, width=173,height=21,;
    caption='GSCR_BKK',;
   bGlobalFont=.t.,;
    prg="GSCR_BKK('DELETE')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 226684239


  add object oObj_1_8 as cp_runprogram with uid="VXZFXUBLYH",left=410, top=134, width=173,height=21,;
    caption='GSCR_BKK',;
   bGlobalFont=.t.,;
    prg="GSCR_BKK('UPDATE')",;
    cEvent = "Update row start",;
    nPag=1;
    , HelpContextID = 226684239


  add object oBtn_1_9 as StdButton with uid="UDFITTPMOK",left=157, top=17, width=23,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 96887850;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSCR_BKK(this.Parent.oContained,"SEARCH")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_CCCODICE) and .cfunction="Query" and .bloaded)
    endwith
  endfunc

  func oBtn_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_CCCODICE) or .cfunction<>"Query" or not .BLOADED)
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=72, width=369,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="CPROWORD",Label1="Posizione",Field2="CCDETTAG",Label2="Dettaglio caratteristica",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 219001978

  add object oStr_1_3 as StdString with uid="BORLNDBGMM",Visible=.t., Left=39, Top=21,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="OIKQXOJBLQ",Visible=.t., Left=5, Top=44,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=91,;
    width=365+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=92,width=364+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCCDESSUP_2_3.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCCDESSUP_2_3 as StdTrsField with uid="RLMERVMYTY",rtseq=6,rtrep=.t.,;
    cFormVar="w_CCDESSUP",value=space(80),;
    ToolTipText = "Note di riga della distinta base",;
    HelpContextID = 45093494,;
    cTotal="", bFixedPos=.t., cQueryName = "CCDESSUP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=333, Left=59, Top=327, InputMask=replicate('X',80)

  add object oStr_2_4 as StdString with uid="UYHGFJHVQP",Visible=.t., Left=6, Top=329,;
    Alignment=1, Width=50, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscr_mccBodyRow as CPBodyRowCnt
  Width=355
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="MOVYRVXMJJ",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    HelpContextID = 251330410,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=56, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCCDETTAG_2_2 as StdTrsField with uid="KNNLIJGOSI",rtseq=5,rtrep=.t.,;
    cFormVar="w_CCDETTAG",value=space(40),;
    HelpContextID = 62919277,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=57, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCCDETTAG_2_2.When()
    return(.t.)
  proc oCCDETTAG_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCCDETTAG_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="CCTIPOCA='C'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".CCTIPOCA=CONFDCAR.CCTIPOCA";
  +" and "+i_cAliasName+".CCCODICE=CONFDCAR.CCCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gscr_mcc','CONF_CAR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCTIPOCA=CONF_CAR.CCTIPOCA";
  +" and "+i_cAliasName2+".CCCODICE=CONF_CAR.CCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
