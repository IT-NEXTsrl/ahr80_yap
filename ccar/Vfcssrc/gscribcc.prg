* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscribcc                                                        *
*              Configuratore caratteristiche                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-16                                                      *
* Last revis.: 2015-04-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscribcc",oParentObject,m.w_OPER)
return(i_retval)

define class tgscribcc as StdBatch
  * --- Local variables
  w_OPER = space(10)
  GSCRIKCC = .NULL.
  w_nTMP = 0
  w_CCCODICE = space(5)
  w_LVLKEY = space(200)
  w_LVLKEYP = space(200)
  w_lvlkeycur = space(200)
  w_LIVELLO = 0
  w_nRec = 0
  w_MSGDESC = space(200)
  Albero = .NULL.
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONF_DIS_idx=0
  CONFDDIS_idx=0
  DISMBASE_idx=0
  DISTBASE_idx=0
  KEY_ARTI_idx=0
  PAR_DISB_idx=0
  CONFDCAR_idx=0
  CONF_CAR_idx=0
  MODE_DIS_idx=0
  CONFSTAT_idx=0
  CONFMEMO_idx=0
  CONFTRAD_idx=0
  CONFTRDE_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Configuratore Caratteristiche (da GSCRIKCC)
    * --- Dal padre
    * --- -- Variabili contenenti il nome dei cursore per consentire di aprire pi� istanze del configuratore --
    * --- Locali...
    * --- Variabili VFox
    * --- Contiene il nome del cursore dell'esplosione distinta base
    cCursD = space(10)
    * --- Assegnamenti
    *     -----------------------
    * --- Variabili locali
    * --- Puntatore a Maschera Padre
    this.GSCRIKCC = this.oParentObject
    * --- Tree-view
    this.Albero = this.GSCRIKCC.w_Albero
    cCursD = this.Albero.cCursor
    * --- Tipo Esplosione
    do case
      case this.w_OPER = "ROWSEL"
        this.GSCRIKCC.LockScreen = .T.
        this.w_nRec = 0
        * --- Aggiorna la testata con la selezione impostata
        * --- --vedo se la risposta data in precedenza � diversa 
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        this.w_lvlkey = this.oParentObject.w_LVLKEYTV
        this.w_CCCODICE = this.GSCRIKCC.w_CCCODICE
        Select (cCursD)
        this.w_nTMP = RecNo(cCursD)
        this.w_LVLKEYP = LVLKEYP
        this.w_lvlkey = LVLKEYP
        SET FILTER TO
        Select (cCursD)
        GO TOP
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        * --- --Le scelte vengono riportate nel dettaglio
        Select (cCursD)
        Update (cCursD) set xchk=0 where CPROWORD<>this.oParentObject.w_ROWSEL AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
        Select (cCursD)
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        SELECT(cCursD)
        GO TOP
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.GSCRIKCC.LockScreen = .F.
        this.Albero.Grd.Refresh()     
        this.GSCRIKCC.__dummy__.Enabled = .t.
        this.GSCRIKCC.__dummy__.SetFocus()     
        this.GSCRIKCC.__dummy__.Enabled = .f.
        this.Albero.Grd.SetFocus()     
      case this.w_OPER = "UNROWSEL"
        * --- --occorre cancellare la scelta anche dall'albero LEGATA a quella caratteristica
        this.GSCRIKCC.LockScreen = .T.
        Select (cCursD)
        this.w_lvlkeycur = this.oParentObject.w_LVLKEYTV
        this.w_CCCODICE = this.GSCRIKCC.w_CCCODICE
        this.w_LIVELLO = this.oParentObject.w_LIVCURS
        Update (cCursD) set xchk=0 where CPROWORD<>this.oParentObject.w_ROWSEL AND ccheader<>"S" AND lvlkeyp = this.w_LVLKEYP
        Select (cCursD)
        =SEEK(this.w_lvlkeycur , (cCursD) , "LVLKEY")
        this.GSCRIKCC.LockScreen = .F.
        this.Albero.Grd.Refresh()     
        this.GSCRIKCC.__dummy__.Enabled = .t.
        this.GSCRIKCC.__dummy__.SetFocus()     
        this.GSCRIKCC.__dummy__.Enabled = .f.
        this.Albero.Grd.SetFocus()     
    endcase
    this.Albero = .NULL.
    this.GSCRIKCC = .NULL.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONF_DIS'
    this.cWorkTables[3]='CONFDDIS'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DISTBASE'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='PAR_DISB'
    this.cWorkTables[8]='CONFDCAR'
    this.cWorkTables[9]='CONF_CAR'
    this.cWorkTables[10]='MODE_DIS'
    this.cWorkTables[11]='CONFSTAT'
    this.cWorkTables[12]='CONFMEMO'
    this.cWorkTables[13]='CONFTRAD'
    this.cWorkTables[14]='CONFTRDE'
    this.cWorkTables[15]='PAR_RIOR'
    return(this.OpenAllTables(15))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
