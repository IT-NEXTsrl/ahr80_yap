* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bsd                                                        *
*              Stampe distinta base                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-16                                                      *
* Last revis.: 2016-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bsd",oParentObject)
return(i_retval)

define class tgscr_bsd as StdBatch
  * --- Local variables
  w_DELETEALL = .f.
  OutCursor = space(10)
  w_KEYRIF = space(10)
  w_CODIAR = space(20)
  w_VARIAR = space(20)
  w_LenLvl = 0
  w_CONFDES = space(0)
  w_KEYGROUP = space(10)
  w_CONTA = 0
  w_DESSUP = space(0)
  w_GRUPPO = space(3)
  w_ACTIVEREC = 0
  w_MESS = space(40)
  w_DISTINTA = space(41)
  w_CCTIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CC__DIBA = space(40)
  w_COCODCOM = space(40)
  w_CPROWNUM = 0
  w_CODCAR = space(5)
  w_STRCONF = space(0)
  w_COUNTREC = 0
  w_DEFA = space(1)
  w_DISBASE = space(41)
  w_KEYSAL = space(40)
  w_cont = 0
  w_cont1 = 0
  w_STRCONF = space(10)
  w_RISPO = space(10)
  w_LISTPAR = space(10)
  w_PARAM = space(10)
  w_VALO = 0
  w_FINE = .f.
  w_APCODICE = space(10)
  w_COD = space(1)
  w_TIPO = space(5)
  w_CODICECA = space(10)
  w_NOME = space(41)
  Trovato = .f.
  conta = 0
  point = 0
  chiave = space(30)
  w_DESFISSE = space(0)
  w_DESPARAM = space(0)
  w_DESDESCRI = space(0)
  w_DETTAG = space(254)
  w_CONFIG = space(254)
  * --- WorkFile variables
  ART_TEMP_idx=0
  lis_scag_idx=0
  TMPCARAT_idx=0
  CONFDCAR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampe delle Distinte Base (da GSDB_SDI+GSDB_SCG)
    this.OutCursor = "__bsi__"
    this.w_KEYRIF = SYS(2015)
    this.w_LenLvl = 4
    * --- Dichiara le varibili private per le stampe
    private l_CODINI,l_CODFIN,l_DATRIF,l_tipges,l_fldige,l_dispro,l_disman,l_flgpre,l_descriz,l_chkLoop,l_desfisse,l_desparam,l_desdescri 
 l_CODINI = this.oParentObject.w_CODARTIN 
 l_CODFIN = this.oParentObject.w_CODARTFI 
 l_DATRIF = this.oParentObject.w_DATSTA 
 l_dispro = this.oParentObject.w_dispro 
 l_tipart = this.oParentObject.w_TIPOARTI 
 L_OUT = this.OutCursor 
 l_grumer=this.oParentObject.w_GRUMER 
 l_grumerf=this.oParentObject.w_GRUMERF 
 l_codfama=this.oParentObject.w_CODFAMA 
 l_codfamaf=this.oParentObject.w_CODFAMAF 
 l_codcat=this.oParentObject.w_CODCAT 
 l_codcatf=this.oParentObject.w_CODCATF 
 l_codmag=this.oParentObject.w_CODMAG 
 l_codmagf=this.oParentObject.w_CODMAGF 
 l_codcomp=this.oParentObject.w_CODCOMP 
 l_codcompf=this.oParentObject.w_CODCOMPF 
 l_chkLoop = this.oParentObject.w_FLCHKLP 
 l_desfisse="" 
 l_desparam="" 
 l_desdescri=""
    if this.oParentObject.w_TIPOSTAM="E" and (this.oParentObject.w_SELECTION<>"S" or this.oParentObject.w_FLNEU<>"N")
      ah_errormsg("Tipo di stampa incongruente per le selezioni")
      i_retcode = 'stop'
      return
    endif
    do case
      case this.oParentObject.w_TIPOSTAM $ "I-E"
        * --- Stampa Indentata
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPOSTAM $ "D-M"
        * --- Stampa Monolivello
        l_descriz=(this.oParentObject.w_TIPOSTAM="D")
        * --- Distinte Confermate e/o Provvisorie
        VQ_EXEC("..\CCAR\EXE\QUERY\GSCR1BSD.VQR",this,"Rilasci")
        if this.oParentObject.w_CARATTER = "S"
          * --- --Inserimento Distinta base
          SELECT DISTINCT dbcodice AS cacodart FROM Rilasci INTO CURSOR ArtDist 
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          select distinct cocodcom, cproword from Rilasci into cursor _tmpRil_
          select Caratter.* from Caratter INNER JOIN _tmpRil_ on CCCOMPON=COCODCOM and cproword=ccroword into cursor _tmpCar_
        else
          * --- Delete from ART_TEMP
          i_nConn=i_TableProp[this.ART_TEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                   )
          else
            delete from (i_cTable) where;
                  CAKEYRIF = this.w_KEYRIF;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_BSD1.VQR",this,"_tmpCar_")
        endif
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if used("Rilasci") and RecCount("Rilasci")>0
          if this.oParentObject.w_CARATTER = "S"
            SELECT * , 0 as grp FROM Rilasci LEFT OUTER JOIN _tmpCar_ ON 1=0 INTO CURSOR _temp3_ nofilter
            =wrcursor("_temp3_")
            SELECT _tmpCar_.*, cocodcom FROM Rilasci;
            RIGHT OUTER JOIN _tmpCar_ ON (not("#" $ cocodcom) and dbcodice=cc__diba and cocodcom=cccompon and cproword=ccroword) ;
             UNION ;
            SELECT _tmpCar_.*, cocodcom FROM Rilasci;
            RIGHT OUTER JOIN _tmpCar_ ON ("#" $ cocodcom and padr(left(cocodcom,at("#",cocodcom)-1),20)=cccompon) ;
            INTO CURSOR _temp4_
            * --- Aggiunge i record delle caratteristiche
            SELECT _temp4_
            cctipoca = " " 
 ccrowor3 = 0 
 ccroword = 0 
 ccvar101 = space(5)
            this.w_KEYGROUP = space(10)
            this.w_CONTA = 0
            SCAN
            if m.cctipoca<>cctipoca or (cctipoca="C" and m.ccrowor3<>ccrowor3) or (cctipoca="C" and m.ccroword<>ccroword) or (cctipoca="M" and m.ccvar101<>ccvar101) or cctipoca="D" or (cctipoca="C" and m.cccodice<>cccodice) or cccompon<>m.cccompon
              scatter memvar memo
              cprownum0 = ccrowor2
              cproword0 = ccrowor2
              cproword1 = ccrowor3
              if cctipoca+cccodice+alltrim(str(ccroword))=this.w_KEYGROUP
                this.w_CONTA = this.w_CONTA+1
              else
                this.w_CONTA = 1
              endif
              cproword = 9999
              cproword0 = 9999
              cproword1 = 9999
              this.w_KEYGROUP = cctipoca+cccodice+alltrim(str(ccroword))
              grp = 4 
              dbcodice=cc__diba
              SELECT _temp3_
              append blank
              gather memvar memo
            endif
            SELECT _temp4_
            ENDSCAN
            * --- Aggiorna la descrizione supplementare (che comprende le note di configurazione)
            SELECT _temp3_
            SCAN FOR NOT EMPTY(NVL(DBDESSUP," "))
            this.w_DISBASE = NVL(DBCODICE,"")
            this.w_DESSUP = dbdessup
            this.w_ACTIVEREC = recno()
            update _temp3_ set DBDESSUP=this.w_DESSUP where empty(nvl(DBDESSUP," ")) and DBCODICE=this.w_DISBASE
            go this.w_ACTIVEREC
            ENDSCAN
            SELECT * FROM _temp3_ ORDER BY dbcodice,grp,ccroword,cccompon,ccrowor2,ccrowor3 INTO CURSOR __tmp__
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            CP_CHPRN("..\CCAR\EXE\QUERY\GSCR1SDS","",this.oParentObject)
          else
            SELECT * FROM Rilasci INTO CURSOR __tmp__
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            CP_CHPRN("..\CCAR\EXE\QUERY\GSCR_SDS","",this.oParentObject)
          endif
          if used("_temp1_")
            USE IN _temp1_
          endif
          if used("_temp2_")
            USE IN _temp2_
          endif
          if used("_temp3_")
            USE IN _temp3_
          endif
          if used("_temp4_")
            USE IN _temp4_
          endif
          if used("Caratter")
            USE IN Caratter
          endif
          if used("_tmpcar_")
            USE IN _tmpcar_
          endif
          if used("Configu")
            USE IN Configu
          endif
          if used("_tmpRil_")
            USE IN _tmpRil_
          endif
        else
          this.w_MESS = "Nessun componente della distinta verifica le selezioni impostate ..."
          ah_ErrorMsg(this.w_MESS,16)
          i_retcode = 'stop'
          return
        endif
    endcase
    * --- Chiude i cursori
    if used(this.OutCursor)
      USE IN (this.OutCursor)
    endif
    if USED("SchRis")
      USE IN SchRis
    endif
    if USED("ScRisor")
      USE IN ScRisor
    endif
    if used("Rilasci")
      USE IN "Rilasci"
    endif
    if used("__tmp__")
      USE IN ("__tmp__")
    endif
    if used("_tmp1_")
      USE IN ("_tmp1_")
    endif
    if used("Caratter")
      USE IN ("Caratter")
    endif
    if used("_appo_")
      USE IN ("_appo_")
    endif
    if used("dismag")
      USE IN ("dismag")
    endif
    if used("componenti")
      USE IN ("componenti")
    endif
    if used("Cic_lavo")
      USE IN ("Cic_lavo")
    endif
    if used("_dettfasi_")
      USE IN ("_dettfasi_")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Indentata
    * --- Usare le variabili private per chiamare GSDB_BTV, altrimenti non c'e' abbastanza spazio per generare il codice)
    GSDS_BEX(this,"V", SPACE(1), "N", this.oParentObject.w_CODARTIN, this.oParentObject.w_CODARTFI, SPACE(2), this.OutCursor, "N", "N", this.oParentObject.w_DATSTA, 1, 999, "N", "S", "N", "N", "P")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if not used(this.OutCursor)
      i_retcode = 'stop'
      return
    endif
    SELECT DISTINCT cacodice as cacodart from (this.OutCursor) where not empty(corifdis) INTO CURSOR ArtDist
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("..\CCAR\EXE\QUERY\GSCRFBTV", this, "_compF_")
    SELECT * FROM (this.OutCursor) LEFT OUTER JOIN _compF_ ON (cacodice=_compF_.codice ) INTO CURSOR _appo_
    * --- Cancella gli articoli inseriti
    * --- Delete from ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
             )
    else
      delete from (i_cTable) where;
            CAKEYRIF = this.w_KEYRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select * from _appo_ into cursor (this.OutCursor) 
 =wrcursor(this.OutCursor)
    lenlvlkey=len(&L_OUT..lvlkey)
    * --- Calcola il livello dei componenti (utilizzando lvlkey)
    UPDATE (this.OutCursor) SET livello = (1+len(rtrim(lvlkey)))/this.w_LenLvl-1, ; 
 cpbmpname = iif(at("@",codicediba)=0,codicediba,left(codicediba,at("@",codicediba)-1))
    if this.oParentObject.w_CARATTER = "S"
      * --- Inserisce nella tabella ART_TEMP i codici di ricerca delle distinte (fino a @)
      SELECT distinct left(cpbmpname,20) AS cacodart FROM (this.OutCursor) INTO CURSOR ArtDist
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_CARATTER = "S"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Delete from ART_TEMP
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                 )
        else
          delete from (i_cTable) where;
                CAKEYRIF = this.w_KEYRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_BSD1.VQR",this,"Caratter")
      endif
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
       Select cacodice,cadesart,cacodart,caoperat, ; 
 artipart,corifdis,cprownum,&l_out..cproword,cocodcom, ; 
 cocodart,counimis,cocoeimp,cocoeum1,coscapro, ; 
 corecsca,cosfrido,corecsfr,coriffas, ; 
 cocorlea,coperric,coattges,codisges,tippro, ; 
 lvlkey, ; 
 qtacomp,qtacompn, qtacompl,stadis,costo,livello, ; 
 left(lvlkey,3) AS dataord, 1 AS datagru, dbtipgen FROM (this.OutCursor) ; 
 ORDER BY dataord,datagru,24,18 INTO CURSOR _temp1_
      if .F.
        Select cacodice,cadesart,cacodart,cacodvar,caoperat,caconfig, ;
        caconato,artipart,corifdis,cprownum,&l_out..cproword,cocodcom, ;
        cocodart,cocodvar,counimis,cocoeimp,cocoeum1,coscapro, ;
        corecsca,cosfrido,corecsfr,coidegru,coopelog,coflpref,coriffas, ;
        cocorlea,coperric,coatt&L_tipg ,codis&L_tipg ,coult&L_tipg ,coopum&L_tip ,tippro, ;
        lvlkey, ;
        qtacomp,qtacompn, qtacompl,stadis,costo,livello, ;
        left(lvlkey, 3) AS dataord, 1 AS datagru, dbtipgen FROM (this.OutCursor) ;
        ORDER BY dataord,datagru,33,25 INTO CURSOR _temp1_
      endif
      * --- 33 = lvlkey   <>   25 = coriffas=ScRisor.cproword
      select distinct Caratter.cc__diba, Caratter.dbdessup, dataord as datakey from _temp1_; 
 inner join Caratter ON cc__diba=cacodice where nvl(cast(dbdessup as varchar),"XXX")<>"XXX"; 
 into cursor Caratter2
      SELECT * FROM _temp1_ LEFT OUTER JOIN Caratter ON 1=0 INTO CURSOR _temp3_ nofilter
      if this.oParentObject.w_CARATTER = "S"
        * --- Aggiunge dettaglio Caratteristiche
        =wrcursor("_temp3_")
        SELECT Caratter.*, lvlkey, livello, dataord, cocodcom FROM _temp1_ ;
        RIGHT OUTER JOIN Caratter ON (not("#" $ cocodcom) and corifdis=cc__diba and cocodcom=cccompon and cproword=ccroword) ;
        where datagru=1 UNION ;
        SELECT Caratter.*, lvlkey, livello, dataord, cocodcom FROM _temp1_ ;
        RIGHT OUTER JOIN Caratter ON ("#" $ cocodcom and corifdis=cc__diba and padr(left(cocodcom,at("#",cocodcom)-1),20)=cccompon) ;
        where datagru=1 INTO CURSOR _temp2_
        * --- Aggiunge i record delle caratteristiche
        SELECT _temp2_
        cctipoca = " " 
 ccrowor3 = 0 
 ccroword = 0 
 ccvar101 = space(5)
        this.w_KEYGROUP = space(10)
        this.w_CONTA = 0
        SCAN
        if m.cctipoca<>cctipoca or (cctipoca="C" and m.ccrowor3<>ccrowor3) or (cctipoca="C" and m.ccroword<>ccroword) or (cctipoca="M" and m.ccvar101<>ccvar101) or (cctipoca="C" and m.cccodice<>cccodice) or cccompon<>m.cccompon
          scatter memvar memo
          cprownum0 = ccrowor2
          cproword0 = ccrowor2
          cproword1 = ccrowor3
          if cctipoca+cc__diba+cccodice+alltrim(str(ccroword))=this.w_KEYGROUP
            this.w_CONTA = this.w_CONTA+1
          else
            this.w_CONTA = 1
          endif
          * --- Chiave di riga - w_KEYGROUP � la chiave del gruppo, uso cproword per identificare univocamente la singola riga del gruppo
          cproword = this.w_conta
          this.w_KEYGROUP = cctipoca+cc__diba+cccodice+alltrim(str(ccroword))
          datagru = 1
          SELECT _temp3_
          append blank
          gather memvar memo
        endif
        SELECT _temp2_
        ENDSCAN
        USE IN _temp2_
      endif
      if USED("Caratter2")
        Select Caratter2 
 GO TOP 
 SCAN 
 tmpmemo=dbdessup 
 this.chiave=datakey 
 update _temp3_ set dbdessup=tmpmemo where dataord=this.chiave 
 ENDSCAN
        USE IN Caratter2
      endif
      * --- Ordina i dati per la stampa
      USE IN _temp1_
      USE IN Caratter
      SELECT * FROM _temp3_ INTO CURSOR __tmp__ ORDER BY dataord,datagru,lvlkey,coriffas, ccrowor2, cccodice
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      CP_CHPRN("..\CCAR\EXE\QUERY\GSCR_SIS","",this.oParentObject)
      USE IN _temp3_
    else
      SELECT * FROM (this.OutCursor) ORDER BY lvlkey INTO CURSOR __tmp__
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.oParentObject.w_TIPOSTAM="I"
        CP_CHPRN("..\DIBA\EXE\QUERY\GSDB_SDI","",this.oParentObject)
      else
        CP_CHPRN("..\DIBA\EXE\QUERY\GSCI2SIS","",this.oParentObject)
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Sommarizzata con Controllo delle scorte
    * --- (da GSDB_SCG)
    * --- Usare le variabili private per chiamare GSDB_BTV, altrimenti non c'e' abbastanza spazio per generare il codice)
    GSDB_BTV(this,"E"," ",l_CODINI,l_CODFIN,l_TIPART,l_TIPGES+l_DISPRO+l_DISMAN+"S",l_FLGPRE," "," ",l_DATRIF,this.OutCursor,this.oParentObject.w_QUANTI)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used(this.OutCursor) and RecCount(this.OutCursor)>1
      if g_MMRP="S"
        * --- Esclude il padre supremo e gli articoli PH, PS, DC
        SELECT left(lvlkey,len(rtrim(lvlkey))-3) AS lvlkey,coidegru,cacodice,cadesart,cacodart,cacodvar,arunmis1, ; 
 sum(QtaCompN*(cocoeum1/cocoeimp)) as somma, sum(QtaCompL*(cocoeum1/cocoeimp)) as sommaL ; 
 FROM (this.OutCursor) WHERE not(ARTIPART $ "PH-PS-DC") and at(".",LVLKEY)>0 ; 
 GROUP BY cacodart INTO CURSOR appogg NOFILTER
      else
        * --- Filtra solo gli articoli Esterni
        SELECT left(lvlkey,len(rtrim(lvlkey))-3) AS lvlkey,coidegru,cacodice,cadesart,cacodart,cacodvar,arunmis1, ; 
 sum(QtaCompN*(cocoeum1/cocoeimp)) as somma, sum(QtaCompL*(cocoeum1/cocoeimp)) as sommaL ; 
 FROM (this.OutCursor) WHERE arpropre="E" GROUP BY cacodart INTO CURSOR appogg NOFILTER
      endif
      SELECT * from appogg ORDER BY lvlkey,coidegru INTO CURSOR (this.OutCursor) NOFILTER
      if used("appogg")
        USE IN appogg
      endif
      * --- Inserisce nella tabella ART_TEMP gli articoli della distinta
      SELECT cacodart+iif(empty(nvl(cacodvar,"")),repl("#",20),cacodvar) AS cacodart FROM (this.OutCursor) INTO CURSOR ArtDist
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcola la disponibilit� degli articoli
      vq_exec("..\DIBA\EXE\QUERY\GSDB2BTV", this, "dismag") 
 SELECT * FROM (this.OutCursor) LEFT OUTER JOIN dismag ON (cacodart=dismag.slcodart and cacodvar=dismag.slcodvar) ; 
 ORDER BY cacodart,cacodvar INTO CURSOR __tmp__
      * --- Cancella gli articoli inseriti
      * --- Delete from ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
               )
      else
        delete from (i_cTable) where;
              CAKEYRIF = this.w_KEYRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Lancia il report di Stampa
      l_QUANTI = this.oParentObject.w_QUANTI
      l_MAGAZZ = iif(this.oParentObject.w_TIPMAG="T","Tutti", iif(this.oParentObject.w_TIPMAG="N", "Nettificabili", this.oParentObject.w_MAGAZZ))
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      CP_CHPRN("..\DIBA\EXE\QUERY\GSDB_SDC","",this.oParentObject)
    else
      ah_ErrorMsg("Distinta base non valida", 48)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce i codici degli articoli in ART_TEMP
    * --- (questa operazione serve per fare un'unica interrogazione al database)
    * --- Usare il cursore ArtDist per passare i codici
    this.w_VARIAR = SPACE(21)
    * --- begin transaction
    cp_BeginTrs()
    SELECT ("ArtDist")
    if Type("ArtDist.cakeysal")="C"
      SCAN
      this.w_CODIAR = padr(ArtDist.Cacodart,20)
      this.w_KEYSAL = ArtDist.Cakeysal
      * --- Try
      local bErr_04C60EE0
      bErr_04C60EE0=bTrsErr
      this.Try_04C60EE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into ART_TEMP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_TEMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_TEMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CAKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
              +i_ccchkf ;
          +" where ";
              +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
              +" and CACODRIC = "+cp_ToStrODBC(this.w_CODIAR);
                 )
        else
          update (i_cTable) set;
              CAKEYSAL = this.w_KEYSAL;
              &i_ccchkf. ;
           where;
              CAKEYRIF = this.w_KEYRIF;
              and CACODRIC = this.w_CODIAR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04C60EE0
      * --- End
      ENDSCAN
    else
      SCAN
      this.w_CODIAR = padr(ArtDist.Cacodart,20)
      * --- Insert into ART_TEMP
      i_nConn=i_TableProp[this.ART_TEMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CAKEYRIF"+",CACODRIC"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
        +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CPROWNUM',0,'CACODDIS',SPACE(20))
        insert into (i_cTable) (CAKEYRIF,CACODRIC,CPROWNUM,CACODDIS &i_ccchkf. );
           values (;
             this.w_KEYRIF;
             ,this.w_CODIAR;
             ,0;
             ,SPACE(20);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    if used("ArtDist")
      USE IN ("ArtDist")
    endif
  endproc
  proc Try_04C60EE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_TEMP
    i_nConn=i_TableProp[this.ART_TEMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_TEMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CAKEYRIF"+",CACODRIC"+",CAKEYSAL"+",CPROWNUM"+",CACODDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYRIF),'ART_TEMP','CAKEYRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIAR),'ART_TEMP','CACODRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_KEYSAL),'ART_TEMP','CAKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_TEMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'ART_TEMP','CACODDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CAKEYRIF',this.w_KEYRIF,'CACODRIC',this.w_CODIAR,'CAKEYSAL',this.w_KEYSAL,'CPROWNUM',0,'CACODDIS',SPACE(20))
      insert into (i_cTable) (CAKEYRIF,CACODRIC,CAKEYSAL,CPROWNUM,CACODDIS &i_ccchkf. );
         values (;
           this.w_KEYRIF;
           ,this.w_CODIAR;
           ,this.w_KEYSAL;
           ,0;
           ,SPACE(20);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caratteristiche
    local cCont
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_BSD3.VQR",this,"Configu")
    if RecCount("Configu")>0
      Select cccodric as cacodart, iif(artipart<>"CC",padr(left(cccodric, at("#",cccodric)-1),20),arccrife) as cakeysal from Configu into cursor DistNeut nofilter 
 Select * from DistNeut into cursor ArtDist
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      CREATE CURSOR _MOD_ (Distinta C(41),Codice C(5),Tipo C(1))
      CREATE CURSOR _CAR_ (Distinta C(41),Codice C(5),Tipo C(1))
      select("Configu") 
 Go Top
      SCAN
      this.w_FINE = .T.
      this.w_NOME = Configu.cccodric
      this.w_CODICECA = Configu.ccconfig
      do while this.w_FINE
        this.w_APCODICE = LEFT(this.w_CODICECA,AT("(",this.w_CODICECA) -1)
        this.w_CODICECA = SUBSTR(this.w_CODICECA,AT(";",this.w_CODICECA) +1) 
        this.w_TIPO = SUBSTR(this.w_APCODICE,1,1) 
        this.w_COD = SUBSTR(this.w_APCODICE,2,7) 
        do case
          case this.w_TIPO="M"
            INSERT INTO _MOD_ (DISTINTA,CODICE,TIPO) VALUES (this.w_NOME,this.w_COD,this.w_TIPO) 
          case this.w_TIPO="C" or this.w_TIPO="D"
            INSERT INTO _CAR_ (DISTINTA,CODICE,TIPO) VALUES (this.w_NOME,this.w_COD,this.w_TIPO) 
        endcase
        if EMPTY(this.w_CODICECA)
          this.w_FINE = .F.
        endif
      enddo
      ENDSCAN
    endif
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_BSD1.VQR",this,"Caratter")
    VQ_EXEC("..\CCAR\EXE\QUERY\GSCR_BSD2.VQR",this,"Modelli")
    if USED ("_CAR_") AND RecCount("_CAR_")>0
      Select Caratter.* from Caratter INNER JOIN _CAR_ ON Caratter.cc__diba=_car_.distinta and Caratter.cctipoca=_car_.tipo and Caratter.cccodice=_car_.codice into cursor Caratter1
    endif
    if USED ("_MOD_") AND RecCount("_MOD_")>0
      Select Modelli.* from Modelli INNER JOIN _MOD_ ON Modelli.cc__diba=_Mod_.distinta and Modelli.cctipoca=_MOD_.tipo and Modelli.cccodice=_MOD_.codice into cursor Modelli1
    endif
    if RecCount("Configu")>0
      Select DistNeut
      scan
      if USED ("Caratter") AND RecCount("Caratter")>0
        delete from caratter where caratter.cc__diba=DistNeut.cacodart
      endif
      if USED ("Modelli") AND RecCount("Modelli")>0
        delete from modelli where modelli.cc__diba=DistNeut.cacodart
      endif
      endscan
      if USED ("Caratter") AND RecCount("Caratter")>0
        select * from Caratter UNION ALL select * FROM Caratter1 into cursor Caratter 
        =wrcursor("Caratter")
      endif
      if USED ("Modelli") AND RecCount("Modelli")>0
        select * from Modelli UNION ALL select * FROM Modelli1 into cursor Modelli 
      endif
    endif
    if used("_CAR_")
      USE IN ("_CAR_")
    endif
    if used("_MOD_")
      USE IN ("_MOD_")
    endif
    if used("Modelli1")
      USE IN ("Modelli1")
    endif
    if used("Caratter1")
      USE IN ("Caratter1")
    endif
    * --- Aggiunge i Modelli (un parametro per record)
    Select Modelli
    scan
    Scatter memvar
    this.w_cont = 1
    cCont = "01"
    CCVAR101 = nvl(Modelli.ccvar101,"")
    do while not empty(m.CCVAR101)
      CCQTA101 = Modelli.ccqta1&cCont
      CCMIN101 = Modelli.ccmin1&cCont
      CCMAX101 = Modelli.ccmax1&cCont
      Select Caratter 
 append blank 
 gather memvar
      this.w_cont = this.w_cont + 1
      cCont = padl(alltrim(str(this.w_cont)),2,"0")
      CCVAR101 = iif(this.w_cont<=15, nvl(Modelli.ccvar1&cCont ,""), "")
    enddo
    Select Caratter 
 append blank 
 gather memvar 
 Replace ccformul with Modelli.ccformul
    Select Modelli
    endscan
    Use in Modelli
    if RecCount("Configu")>0
      * --- Aggiorna le Risposte
      Select Configu
      Scan
      this.w_STRCONF = rtrim(ccconfig)
      do while not empty(this.w_STRCONF)
        this.w_cont = AT(";", this.w_STRCONF)
        this.w_RISPO = left(this.w_STRCONF, this.w_cont-1)
        this.w_STRCONF = substr(this.w_STRCONF, this.w_cont+1)
        do case
          case this.w_RISPO = "C"
            this.w_cont = AT("(", this.w_RISPO)
            this.w_VALO = val(substr(this.w_RISPO, this.w_cont+1))
            this.w_RISPO = padr(substr(this.w_RISPO, 2, this.w_cont-2),5)
            Update Caratter set CCRISPO="@" where cc__diba=Configu.cccodric and cccodice=this.w_RISPO and ccrowor3=this.w_VALO
          case this.w_RISPO = "M"
            this.w_cont = AT("(", this.w_RISPO)
            this.w_LISTPAR = substr(this.w_RISPO, this.w_cont+1, AT(")", this.w_RISPO)-this.w_CONT-1)+":"
            this.w_RISPO = padr(substr(this.w_RISPO, 2, this.w_cont-2),5)
            do while not empty(this.w_LISTPAR)
              this.w_cont = AT("=", this.w_LISTPAR)
              this.w_PARAM = padr(left(this.w_LISTPAR, this.w_cont-1),5)
              this.w_cont1 = AT(":", this.w_LISTPAR)
              this.w_VALO = val(substr(this.w_LISTPAR, this.w_cont+1, this.w_cont1-this.w_cont))
              this.w_LISTPAR = substr(this.w_LISTPAR, this.w_cont1+1)
              Update Caratter set CCQTARIS=this.w_VALO where cc__diba=Configu.cccodric and cccodice=this.w_RISPO and ccvar101=this.w_PARAM
            enddo
          case this.w_RISPO = "D"
            this.w_cont = AT("(", this.w_RISPO)
            this.w_CONFDES = substr(this.w_RISPO,this.w_cont+1,len(alltrim(this.w_RISPO))-(this.w_cont+1))
            this.w_RISPO = padr(substr(this.w_RISPO, 2, this.w_cont-2),5)
            Update Caratter set CC__NOTE=this.w_CONFDES where cc__diba=Configu.cccodric and cccodice=this.w_RISPO
            * --- and ccrowor3=w_VALO
        endcase
      enddo
      Endscan
      Use in Configu 
 Use in DistNeut
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Eliminazione componenti che non verificano i flitri inseriti dall'utente
    this.conta = 0
    this.point = 0
    Select _appo_ 
 =wrcursor("_appo_") 
 go bottom
    do while not BOF()
      if empty(nvl(codice," "))
        this.point=recno()
        this.chiave = cacodice
        do while not this.trovato and not EOF()
          if this.chiave=corifdis
            this.Trovato = .t.
          endif
          skip
        enddo
        go this.point
        if not this.trovato
          delete
        endif
        skip-1
      else
        skip-1
      endif
      this.Trovato = .f.
    enddo
    if used("_compf_")
      USE IN ("_compf_")
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- TMPCARAT contiene il codice distinte rispondenti alle selezioni delle caratteristice:
    this.TMPCARAT_idx=cp_GetTableDefIdx("TMPCARAT")
    * --- Select from TMPCARAT
    i_nConn=i_TableProp[this.TMPCARAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCARAT ";
           ,"_Curs_TMPCARAT")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPCARAT
    endif
    if used('_Curs_TMPCARAT')
      select _Curs_TMPCARAT
      locate for 1=1
      do while not(eof())
      this.w_CODCAR = NVL(CCCODICE,"")
        select _Curs_TMPCARAT
        continue
      enddo
      use
    endif
    if USED("__ctmp__")
      USE IN ("__ctmp__")
    endif
    create cursor _todel_ (CC__DIBA C(41), COCODCOM C(41), CPROWORD N(4))
    create cursor _todel2_ (CC__DIBA C(41))
    create cursor _todel3_ (CC__DIBA C(41))
    =wrcursor("__tmp__")
    if !EMPTY(this.w_CODCAR)
      * --- Select from TMPCARAT
      i_nConn=i_TableProp[this.TMPCARAT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPCARAT_idx,2],.t.,this.TMPCARAT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPCARAT ";
            +" order by CCTIPOCA,CCCODICE";
             ,"_Curs_TMPCARAT")
      else
        select * from (i_cTable);
         order by CCTIPOCA,CCCODICE;
          into cursor _Curs_TMPCARAT
      endif
      if used('_Curs_TMPCARAT')
        select _Curs_TMPCARAT
        locate for 1=1
        do while not(eof())
        this.w_COUNTREC = this.w_COUNTREC+1
        this.w_CCTIPOCA = NVL(CCTIPOCA,"")
        this.w_CCCODICE = NVL(CCCODICE,"")
        this.w_CPROWNUM = NVL(CPROWORD,0)
        this.w_CONFIG = NVL(STCONFIG,"")
        this.w_STRCONF = iif(EMPTY(NVL(STCONFIG,"")),this.w_CCTIPOCA+alltrim(this.w_CCCODICE),NVL(STCONFIG,""))
        this.w_DEFA = NVL(CCFLDEFA,"")
        do case
          case this.w_CCTIPOCA="C"
            if this.w_CPROWNUM<>0
              * --- Read from CONFDCAR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONFDCAR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONFDCAR_idx,2],.t.,this.CONFDCAR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CCDETTAG"+;
                  " from "+i_cTable+" CONFDCAR where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
                      +" and CCTIPOCA = "+cp_ToStrODBC(this.w_CCTIPOCA);
                      +" and CPROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CCDETTAG;
                  from (i_cTable) where;
                      CCCODICE = this.w_CCCODICE;
                      and CCTIPOCA = this.w_CCTIPOCA;
                      and CPROWORD = this.w_CPROWNUM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DETTAG = NVL(cp_ToDate(_read_.CCDETTAG),cp_NullValue(_read_.CCDETTAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_DESFISSE = alltrim(this.w_DESFISSE)+iif(!EMPTY(this.w_DESFISSE),", ","")+alltrim(this.w_CCCODICE)+"("+alltrim(this.w_DETTAG)+")"
            else
              this.w_DESFISSE = alltrim(this.w_DESFISSE)+iif(!EMPTY(this.w_DESFISSE),", ","")+alltrim(this.w_CCCODICE)+"()"
            endif
          case this.w_CCTIPOCA="M"
            this.w_DESPARAM = alltrim(this.w_DESPARAM)+iif(!EMPTY(this.w_DESPARAM),", ","")+alltrim(substr(alltrim(this.w_CONFIG),2,len(this.w_CONFIG)))+")"
          case this.w_CCTIPOCA="D"
            this.w_DESDESCRI = alltrim(this.w_DESDESCRI)+iif(!EMPTY(this.w_DESDESCRI),", ","")+alltrim(substr(alltrim(this.w_CONFIG),2,len(this.w_CONFIG)))+iif(ATC("(",this.w_CONFIG)>0,")","()")
        endcase
        if this.w_CCTIPOCA="C"
          if this.oParentObject.w_FLNEU="S"
            vq_exec("..\CCAR\EXE\QUERY\GSCR12BSD", this, "_tcarat_")
          else
            vq_exec("..\CCAR\EXE\QUERY\GSCR15BSD", this, "_tcarat_")
          endif
          =wrcursor("_tcarat_")
          if reccount("_tcarat_")>0 
            if this.oParentObject.w_FILTERTYPE="M"
              if reccount("_todel_")>0
                delete from _todel_ where alltrim(CC__DIBA)+LEFT(alltrim(nvl(cocodcom,""))+repl("@",20),20) not in (select alltrim(CC__DIBA)+LEFT(alltrim(nvl(cocodcom,""))+repl("@",20),20) from _tcarat_)
                Select _tcarat_ 
 GO TOP 
 SCAN
                select _todel_ 
 locate for CC__DIBA=_tcarat_.CC__DIBA
                if found()
                  insert into _todel_ (CC__DIBA,COCODCOM,CPROWORD) values (_tcarat_.CC__DIBA,NVL(_tcarat_.COCODCOM,""),_tcarat_.CPROWORD)
                endif
                ENDSCAN
              else
                if this.w_COUNTREC=1
                  Select _tcarat_ 
 GO TOP 
 SCAN 
 insert into _todel_ (CC__DIBA,COCODCOM,CPROWORD) values (_tcarat_.CC__DIBA,NVL(_tcarat_.COCODCOM,""),_tcarat_.CPROWORD) 
 ENDSCAN
                endif
              endif
            else
              Select _tcarat_ 
 GO TOP 
 SCAN 
 insert into _todel_ (CC__DIBA,COCODCOM,CPROWORD) values (_tcarat_.CC__DIBA,NVL(_tcarat_.COCODCOM,""),_tcarat_.CPROWORD) 
 ENDSCAN
            endif
          else
            if this.oParentObject.w_FILTERTYPE="M"
              this.w_DELETEALL = .t.
            endif
          endif
        else
          if this.oParentObject.w_FLNEU<>"S"
            vq_exec("..\CCAR\EXE\QUERY\GSCR7BSD", this, "_tcarat_")
          else
            if this.w_CCTIPOCA="M"
              vq_exec("..\CCAR\EXE\QUERY\GSCR20BSD", this, "_tcarat_")
            else
              vq_exec("..\CCAR\EXE\QUERY\GSCR19BSD", this, "_tcarat_")
            endif
          endif
          =wrcursor("_tcarat_")
          if this.w_CCTIPOCA="M"
            Select _tcarat_ 
 GO TOP 
 SCAN 
 insert into _todel2_ (CC__DIBA) values (_tcarat_.CC__DIBA) 
 ENDSCAN
          else
            Select _tcarat_ 
 GO TOP 
 SCAN 
 insert into _todel3_ (CC__DIBA) values (_tcarat_.CC__DIBA) 
 ENDSCAN
          endif
        endif
        if USED("_tcarat_")
          USE IN ("_tcarat_")
        endif
          select _Curs_TMPCARAT
          continue
        enddo
        use
      endif
    else
      if this.oParentObject.w_FLNEU="S"
        do case
          case this.oParentObject.w_TIPOSTAM="I"
            select distinct IIF(ATC(".",lvlkey)>0,LEFT(lvlkey,ATC(".",lvlkey)-1),lvlkey) as dataord from __tmp__ where at("#",corifdis)>0 or dbtipgen$"C-V" into cursor _todel3_
            Delete from __tmp__ where IIF(ATC(".",lvlkey)>0,LEFT(lvlkey,ATC(".",lvlkey)-1),lvlkey) in (select dataord from _todel3_)
          case this.oParentObject.w_TIPOSTAM="S"
            select distinct raggr from __tmp__ where at("#",corifdis)>0 or dbtipgen$"C-V" into cursor _todel3_
            Delete from __tmp__ where raggr in (select raggr from _todel3_)
          otherwise
            Delete from __tmp__ where at("#",dbcodice)>0 or dbcodice in (Select dbcodice from __tmp__ where !EMPTY(NVL(dbtipgen,"")))
        endcase
        if Used("_todel3_")
          Select _todel3_ 
 USE
        endif
      endif
    endif
    if USED("__tmp__")
      if !EMPTY(this.w_CODCAR)
        * --- Metto in join i cursori per recuperare le sole combinazioni possibili
        if reccount("_todel_")=0
          if reccoun("_todel2_")=0
            Select * from _todel3_ into cursor _todel4_
          else
            if reccoun("_todel3_")=0
              Select * from _todel2_ into cursor _todel4_
            else
              Select _todel2_.cc__diba from _todel2_ inner join _todel3_ on _todel2_.cc__diba =_todel3_.cc__diba into cursor _todel4_
            endif
          endif
        else
          if reccoun("_todel2_")=0
            if reccoun("_todel3_")=0
              Select _todel_.cc__diba from _todel_ into cursor _todel4_
            else
              Select _todel_.cc__diba from _todel_ inner join _todel3_ on _todel_.cc__diba =_todel3_.cc__diba into cursor _todel4_
            endif
          else
            if reccoun("_todel3_")=0
              Select _todel_.cc__diba from _todel_ inner join _todel2_ on _todel_.cc__diba =_todel2_.cc__diba into cursor _todel4_
            else
              Select _todel_.cc__diba from _todel_ inner join _todel2_ on _todel_.cc__diba =_todel2_.cc__diba inner join _todel3_; 
 on _todel_.cc__diba =_todel3_.cc__diba into cursor _todel4_
            endif
          endif
        endif
        if reccount("_todel4_")=0 and !EMPTY(this.w_CODCAR)
          delete from __tmp__
        endif
        if this.oParentObject.w_TIPOSTAM$"M-D"
          if RECCOUNT("_todel4_")>0
            Select * from __tmp__ where dbcodice in (select cc__diba from _todel4_) into cursor __ctmp__ order by dbcodice,cproword
            if this.oParentObject.w_FLNEU="N"
              select * from __tmp__ where nvl(__tmp__.dbcodice,"") in (select nvl(_todel4_.CC__DIBA,"") from _todel4_) into cursor __ctmp__ ORDER BY dbcodice,cproword
            endif
          else
            Select * from __tmp__ into cursor __ctmp__ ORDER BY dbcodice
          endif
        else
          if RECCOUNT("_todel4_")>0
            if this.oParentObject.w_TIPOSTAM="I"
               select * from __tmp__ where left(lvlkey,3) in (Select left(lvlkey,3) from __tmp__ where cacodice in (Select cc__diba from _todel4_)) into cursor __ctmp__
              =wrcursor("__ctmp__")
            else
              if this.oParentObject.w_TIPOSTAM="S"
                 select * from __tmp__ where raggr in (Select raggr from __tmp__ where corifdis in (Select cc__diba from _todel4_)) into cursor __ctmp__
              else
                 select * from __tmp__ where left(lvlkey,3) in (Select left(lvlkey,3) from __tmp__ where corifdis in (Select cc__diba from _todel4_)) into cursor __ctmp__
              endif
            endif
          else
            if USED("__ctmp__")
              =wrcursor("__ctmp__")
            endif
            if this.oParentObject.w_CARATTER="S"
              SELECT * FROM __tmp__ INTO CURSOR __ctmp__
            else
              SELECT * FROM __tmp__ INTO CURSOR __ctmp__ ORDER BY lvlkey
            endif
          endif
        endif
      endif
    endif
    if USED("_todel_")
      USE IN ("_todel_")
    endif
    if USED("_todel2_")
      USE IN ("_todel2_")
    endif
    if USED("_todel3_")
      USE IN ("_todel3_")
    endif
    if USED("_todel4_")
      USE IN ("_todel4_")
    endif
    if !USED("__ctmp__") or reccount("__ctmp__")=0 or this.w_DELETEALL
      if this.oParentObject.w_SELECTION="S"
        * --- Sono attive le selezioni sulle caratteristiche ma la tabella TMPCARAT � vuota....
        =wrcursor("__tmp__")
        delete from __tmp__
      endif
    else
      if !EMPTY(this.w_CODCAR)
        if USED("__tmp__")
          USE IN ("__tmp__")
        endif
        if this.oParentObject.w_TIPOSTAM="I"
          if this.oParentObject.w_CARATTER="S"
            Select distinct * from __ctmp__ into cursor __tmp__ ORDER BY dataord,datagru,lvlkey,coriffas, ccrowor2, cccodice
          else
            Select distinct * from __ctmp__ into cursor __tmp__ order by lvlkey
          endif
        else
          if this.oParentObject.w_TIPOSTAM$"E-S"
            Select distinct * from __ctmp__ into cursor __tmp__ order by lvlkey
          else
            if this.oParentObject.w_CARATTER="S"
              =wrcursor("__ctmp__")
              Delete from __ctmp__ where cctipoca="D" and EMPTY(NVL(COCODCOM,"")) 
              Select distinct * from __ctmp__ into cursor __tmp__ order by dbcodice, ccroword
            else
              Select distinct * from __ctmp__ into cursor __tmp__ order by dbcodice
            endif
          endif
        endif
        l_DESFISSE=alltrim(this.w_DESFISSE)
        l_DESPARAM=alltrim(this.w_DESPARAM)
        l_DESDESCRI=alltrim(this.w_DESDESCRI)
      endif
      USE IN ("__ctmp__")
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_TEMP'
    this.cWorkTables[2]='lis_scag'
    this.cWorkTables[3]='TMPCARAT'
    this.cWorkTables[4]='CONFDCAR'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_TMPCARAT')
      use in _Curs_TMPCARAT
    endif
    if used('_Curs_TMPCARAT')
      use in _Curs_TMPCARAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
