* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscr_bll                                                        *
*              Duplica caratteristiche                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-09-30                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscr_bll",oParentObject)
return(i_retval)

define class tgscr_bll as StdBatch
  * --- Local variables
  GSCR_MLC = .NULL.
  GSDS_adb = .NULL.
  w_Msg = space(40)
  Errore = .f.
  w_CONF = .f.
  w_REC = 0
  w_TRSNAME = space(15)
  w_DISTINTA = space(41)
  w_COCODCOM = space(41)
  w_RIGA = 0
  w_STATO = space(1)
  w_DBSTADIS = space(1)
  w_TIPOCA = space(1)
  w_CCODICE = space(5)
  w_APPDATA = ctod("  /  /  ")
  w_APPDATAF = ctod("  /  /  ")
  * --- WorkFile variables
  CONF_DIS_idx=0
  CONFDDIS_idx=0
  MODE_DIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Modifica stato legami
    * --- Dal padre ...
    * --- Puntatore ad oggetto padre (anagrafica DIBA)
    this.GSCR_MLC = this.oParentObject
    this.w_APPDATA = looktab("CONF_DIS","CCDATINI","CC__DIBA",this.oParentObject.w_CC__DIBA,"CCCOMPON",this.oParentObject.w_CCCOMPON,"CCROWORD",this.oParentObject.w_CCROWORD)
    this.w_APPDATAF = looktab("CONF_DIS","CCDATFIN","CC__DIBA",this.oParentObject.w_CC__DIBA,"CCCOMPON",this.oParentObject.w_CCCOMPON,"CCROWORD",this.oParentObject.w_CCROWORD)
    Private nc 
 nc = this.GSCR_MLC.cTrsName
    SELECT (NC) 
 go top
    * --- --Duplicazione righe legami il componente � gi� presente occorre duplicare il legame
    this.w_DISTINTA = this.oParentObject.w_CC__DIBA
    this.w_COCODCOM = this.oParentObject.w_CCCOMPON
    this.w_RIGA = this.oParentObject.w_CCROWORD
    this.w_STATO = "R"
    * --- Try
    local bErr_03439900
    bErr_03439900=bTrsErr
    this.Try_03439900()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03439900
    * --- End
    SELECT (NC) 
 go top
    Scan
    Replace t_DINIVAL1 With iif(t_TIPGES="G",t_DINIVALG,iif (t_TIPGES="T",t_DINIVALT,t_DINIVALP)) 
 Replace t_DINIVAL With this.w_APPDATA 
 Replace t_CCDATINI With iif(t_DINIVAL>t_DINIVAL1,t_DINIVAL,t_DINIVAL1) 
 Replace t_DIFINVAL1 With iif(t_TIPGES="G",t_DFINVALG,iif (t_TIPGES="T",t_DFINVALT,t_DFNIVALP)) 
 Replace t_DIFINVAL With this.w_APPDATAF
    Endscan
    SELECT (NC) 
 GO top 
 With this.GSCR_MLC 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=0 
 .oPgFrm.Page1.oPag.oBody.nRelRow=0 
 EndWith
  endproc
  proc Try_03439900()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONF_DIS
    i_nConn=i_TableProp[this.CONF_DIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsds_bml",this.CONF_DIS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into CONFDDIS
    i_nConn=i_TableProp[this.CONFDDIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsds1bml",this.CONFDDIS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into MODE_DIS
    i_nConn=i_TableProp[this.MODE_DIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODE_DIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsds2bml",this.MODE_DIS_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT (NC) 
 GO top 
 With this.GSCR_MLC 
 .loadrec() 
 EndWith
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONF_DIS'
    this.cWorkTables[2]='CONFDDIS'
    this.cWorkTables[3]='MODE_DIS'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
