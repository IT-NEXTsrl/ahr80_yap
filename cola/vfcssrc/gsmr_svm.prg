* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_svm                                                        *
*              Stampa messaggi ripianificazione                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-17                                                      *
* Last revis.: 2010-02-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_svm",oParentObject))

* --- Class definition
define class tgsmr_svm as StdForm
  Top    = 6
  Left   = 3

  * --- Standard Properties
  Width  = 818
  Height = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-02-01"
  HelpContextID=227972247
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  _IDX = 0
  MRP_MESS_IDX = 0
  ODL_MAST_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsmr_svm"
  cComment = "Stampa messaggi ripianificazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MRCODRIN = space(20)
  w_MRCODRIF = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_LLCINI = 0
  o_LLCINI = 0
  w_LLCFIN = 0
  w_MRTIPO = space(1)
  w_MRESEGUI = space(1)
  w_MRTIPOMS = space(2)
  w_MAGINI = space(5)
  w_MAGFIN = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_MRCODODI = space(15)
  w_MRCODODF = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DAFINI = ctod('  /  /  ')
  w_DAFFIN = ctod('  /  /  ')
  w_FLSUG = space(1)
  w_FLLAN = space(1)
  w_FLPIA = space(1)
  w_CADESARF = space(40)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_TIPO = space(2)
  w_TIPO1 = space(2)
  w_CADESART = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_svmPag1","gsmr_svm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMRCODRIN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='MRP_MESS'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='ATTIVITA'
    this.cWorkTables[9]='CAN_TIER'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MRCODRIN=space(20)
      .w_MRCODRIF=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_LLCINI=0
      .w_LLCFIN=0
      .w_MRTIPO=space(1)
      .w_MRESEGUI=space(1)
      .w_MRTIPOMS=space(2)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_MRCODODI=space(15)
      .w_MRCODODF=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DAFINI=ctod("  /  /  ")
      .w_DAFFIN=ctod("  /  /  ")
      .w_FLSUG=space(1)
      .w_FLLAN=space(1)
      .w_FLPIA=space(1)
      .w_CADESARF=space(40)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_TIPO=space(2)
      .w_TIPO1=space(2)
      .w_CADESART=space(40)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MRCODRIN))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_MRCODRIF))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CATINI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CATFIN))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_GRUINI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_LLCFIN = iif(.w_LLCINI=0, 999, .w_LLCINI)
        .w_MRTIPO = "T"
        .w_MRESEGUI = "T"
        .w_MRTIPOMS = "TU"
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MAGINI))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODCOM))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODATT))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_MRCODODI))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MRCODODF))
          .link_1_19('Full')
        endif
          .DoRTCalc(20,23,.f.)
        .w_FLSUG = iif(g_MMRP='S','M','.')
        .w_FLLAN = 'L'
        .w_FLPIA = 'P'
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(27,37,.f.)
        .w_TIPCON = 'F'
        .w_OBTEST = i_DATSYS
        .w_TIPATT = 'A'
        .w_TIPO = iif(.w_MRTIPOMS='TU', '  ' , .w_MRTIPOMS)
        .w_TIPO1 = iif(.w_MRESEGUI='T', ' ' , .w_MRESEGUI)
    endwith
    this.DoRTCalc(43,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_LLCINI<>.w_LLCINI
            .w_LLCFIN = iif(.w_LLCINI=0, 999, .w_LLCINI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(11,40,.t.)
            .w_TIPO = iif(.w_MRTIPOMS='TU', '  ' , .w_MRTIPOMS)
            .w_TIPO1 = iif(.w_MRESEGUI='T', ' ' , .w_MRESEGUI)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(43,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_16.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_17.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oFLSUG_1_24.enabled = this.oPgFrm.Page1.oPag.oFLSUG_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODRIN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODRIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MRCODRIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MRCODRIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODRIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MRCODRIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MRCODRIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODRIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMRCODRIN_1_1'),i_cWhere,'',"Articoli",'GSMR_ACA.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODRIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MRCODRIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MRCODRIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODRIN = NVL(_Link_.CACODICE,space(20))
      this.w_CADESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODRIN = space(20)
      endif
      this.w_CADESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODRIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODRIF
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MRCODRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MRCODRIF))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODRIF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MRCODRIF)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MRCODRIF)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MRCODRIF) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMRCODRIF_1_2'),i_cWhere,'',"Articoli",'GSMR_ACA.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MRCODRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MRCODRIF)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODRIF = NVL(_Link_.CACODICE,space(20))
      this.w_CADESARF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODRIF = space(20)
      endif
      this.w_CADESARF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MRCODRIN<=.w_MRCODRIF or empty(.w_MRCODRIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MRCODRIF = space(20)
        this.w_CADESARF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_3'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_4'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_5'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_6'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_7'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_8'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_14'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_15'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_16'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_17'),i_cWhere,'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODODI
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_MRCODODI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_MRCODODI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODODI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODODI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oMRCODODI_1_18'),i_cWhere,'',"Anagrafica ordini di produzione",'GSMR_AOL.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_MRCODODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_MRCODODI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODODI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODODI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODODF
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODODF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_MRCODODF)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_MRCODODF))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODODF)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODODF) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oMRCODODF_1_19'),i_cWhere,'',"Anagrafica ordini di produzione",'GSMR_AOL.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODODF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_MRCODODF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_MRCODODF)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODODF = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODODF = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MRCODODI<=.w_MRCODODF or empty(.w_MRCODODI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MRCODODF = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODODF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMRCODRIN_1_1.value==this.w_MRCODRIN)
      this.oPgFrm.Page1.oPag.oMRCODRIN_1_1.value=this.w_MRCODRIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODRIF_1_2.value==this.w_MRCODRIF)
      this.oPgFrm.Page1.oPag.oMRCODRIF_1_2.value=this.w_MRCODRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_3.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_3.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_4.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_4.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_5.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_5.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_6.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_6.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_7.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_7.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_8.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_8.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLLCINI_1_9.value==this.w_LLCINI)
      this.oPgFrm.Page1.oPag.oLLCINI_1_9.value=this.w_LLCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLLCFIN_1_10.value==this.w_LLCFIN)
      this.oPgFrm.Page1.oPag.oLLCFIN_1_10.value=this.w_LLCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMRTIPO_1_11.RadioValue()==this.w_MRTIPO)
      this.oPgFrm.Page1.oPag.oMRTIPO_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRESEGUI_1_12.RadioValue()==this.w_MRESEGUI)
      this.oPgFrm.Page1.oPag.oMRESEGUI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRTIPOMS_1_13.RadioValue()==this.w_MRTIPOMS)
      this.oPgFrm.Page1.oPag.oMRTIPOMS_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_14.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_14.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_15.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_15.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_16.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_16.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_17.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_17.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODODI_1_18.value==this.w_MRCODODI)
      this.oPgFrm.Page1.oPag.oMRCODODI_1_18.value=this.w_MRCODODI
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODODF_1_19.value==this.w_MRCODODF)
      this.oPgFrm.Page1.oPag.oMRCODODF_1_19.value=this.w_MRCODODF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_20.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_20.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_21.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_21.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFINI_1_22.value==this.w_DAFINI)
      this.oPgFrm.Page1.oPag.oDAFINI_1_22.value=this.w_DAFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFFIN_1_23.value==this.w_DAFFIN)
      this.oPgFrm.Page1.oPag.oDAFFIN_1_23.value=this.w_DAFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSUG_1_24.RadioValue()==this.w_FLSUG)
      this.oPgFrm.Page1.oPag.oFLSUG_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLAN_1_25.RadioValue()==this.w_FLLAN)
      this.oPgFrm.Page1.oPag.oFLLAN_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPIA_1_26.RadioValue()==this.w_FLPIA)
      this.oPgFrm.Page1.oPag.oFLPIA_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESARF_1_29.value==this.w_CADESARF)
      this.oPgFrm.Page1.oPag.oCADESARF_1_29.value=this.w_CADESARF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_42.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_42.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_43.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_43.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_44.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_44.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_48.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_48.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_49.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_49.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_50.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_50.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_55.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_55.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_57.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_57.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_59.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_59.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_66.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_66.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESART_1_74.value==this.w_CADESART)
      this.oPgFrm.Page1.oPag.oCADESART_1_74.value=this.w_CADESART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_MRCODRIN<=.w_MRCODRIF or empty(.w_MRCODRIN))  and not(empty(.w_MRCODRIF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODRIF_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN)  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MRCODODI<=.w_MRCODODF or empty(.w_MRCODODI))  and not(empty(.w_MRCODODF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODODF_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAFINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAFFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LLCINI = this.w_LLCINI
    return

enddefine

* --- Define pages as container
define class tgsmr_svmPag1 as StdContainer
  Width  = 814
  height = 472
  stdWidth  = 814
  stdheight = 472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMRCODRIN_1_1 as StdField with uid="XTGMGDXNLB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MRCODRIN", cQueryName = "MRCODRIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 69868564,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=106, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MRCODRIN"

  func oMRCODRIN_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODRIN_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODRIN_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMRCODRIN_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMR_ACA.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oMRCODRIF_1_2 as StdField with uid="SXIZCWXORH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MRCODRIF", cQueryName = "MRCODRIF",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 69868556,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=106, Top=59, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MRCODRIF"

  proc oMRCODRIF_1_2.mDefault
    with this.Parent.oContained
      if empty(.w_MRCODRIF)
        .w_MRCODRIF = .w_MRCODRIN
      endif
    endwith
  endproc

  func oMRCODRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODRIF_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODRIF_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMRCODRIF_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMR_ACA.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_1_3 as StdField with uid="BAUZFWPTWC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 7121494,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=83, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_4 as StdField with uid="GHDDKOKVIG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 188525142,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_1_4.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oCATINI_1_5 as StdField with uid="WQNLFQEXDD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 70968794,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=507, Top=83, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_6 as StdField with uid="WHQQEZYTLE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 260957658,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=507, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_1_6.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oGRUINI_1_7 as StdField with uid="VYQYSTJFCH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 70960282,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=133, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_8 as StdField with uid="BKTBROMCMD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 260949146,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oLLCINI_1_9 as StdField with uid="BAHIFKOQEG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LLCINI", cQueryName = "LLCINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo LLC",;
    HelpContextID = 71035466,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=507, Top=131, cSayPict='"999"', cGetPict='"999"'

  add object oLLCFIN_1_10 as StdField with uid="MOQFEMYNNQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_LLCFIN", cQueryName = "LLCFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo LLC",;
    HelpContextID = 261024330,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=603, Top=131, cSayPict='"999"', cGetPict='"999"'


  add object oMRTIPO_1_11 as StdCombo with uid="BWXXSLLMLC",rtseq=11,rtrep=.f.,left=691,top=131,width=116,height=21;
    , ToolTipText = "Selezionare il tipo ordine";
    , HelpContextID = 236639290;
    , cFormVar="w_MRTIPO",RowSource=""+"Tutti,"+"ODL,"+"OCL,"+"ODA", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRTIPO_1_11.RadioValue()
    return(iif(this.value =1,"T",;
    iif(this.value =2,"I",;
    iif(this.value =3,"L",;
    iif(this.value =4,"E",;
    space(1))))))
  endfunc
  func oMRTIPO_1_11.GetRadio()
    this.Parent.oContained.w_MRTIPO = this.RadioValue()
    return .t.
  endfunc

  func oMRTIPO_1_11.SetRadio()
    this.Parent.oContained.w_MRTIPO=trim(this.Parent.oContained.w_MRTIPO)
    this.value = ;
      iif(this.Parent.oContained.w_MRTIPO=="T",1,;
      iif(this.Parent.oContained.w_MRTIPO=="I",2,;
      iif(this.Parent.oContained.w_MRTIPO=="L",3,;
      iif(this.Parent.oContained.w_MRTIPO=="E",4,;
      0))))
  endfunc


  add object oMRESEGUI_1_12 as StdCombo with uid="LMBQWYKWLR",rtseq=12,rtrep=.f.,left=507,top=167,width=133,height=21;
    , ToolTipText = "Selezione stato messaggi";
    , HelpContextID = 113361905;
    , cFormVar="w_MRESEGUI",RowSource=""+"Tutti,"+"Eseguito,"+"Non eseguito,"+"Errore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRESEGUI_1_12.RadioValue()
    return(iif(this.value =1,"T",;
    iif(this.value =2,"S",;
    iif(this.value =3,"N",;
    iif(this.value =4,"E",;
    space(1))))))
  endfunc
  func oMRESEGUI_1_12.GetRadio()
    this.Parent.oContained.w_MRESEGUI = this.RadioValue()
    return .t.
  endfunc

  func oMRESEGUI_1_12.SetRadio()
    this.Parent.oContained.w_MRESEGUI=trim(this.Parent.oContained.w_MRESEGUI)
    this.value = ;
      iif(this.Parent.oContained.w_MRESEGUI=="T",1,;
      iif(this.Parent.oContained.w_MRESEGUI=="S",2,;
      iif(this.Parent.oContained.w_MRESEGUI=="N",3,;
      iif(this.Parent.oContained.w_MRESEGUI=="E",4,;
      0))))
  endfunc


  add object oMRTIPOMS_1_13 as StdCombo with uid="NECDIRSMVO",rtseq=13,rtrep=.f.,left=507,top=193,width=289,height=21;
    , ToolTipText = "Selezione tipo messaggi";
    , HelpContextID = 31796249;
    , cFormVar="w_MRTIPOMS",RowSource=""+"Tutti,"+"Anticipare,"+"Posticipare,"+"Annullare,"+"Non � possibile anticipare la data,"+"Non � possibile posticipare: quantit� insufficiente,"+"Esistono fabbisogni non coperti,"+"Errori elaborazione MRP", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMRTIPOMS_1_13.RadioValue()
    return(iif(this.value =1,"TU",;
    iif(this.value =2,"AN",;
    iif(this.value =3,"PO",;
    iif(this.value =4,"CA",;
    iif(this.value =5,"AX",;
    iif(this.value =6,"PX",;
    iif(this.value =7,"MR",;
    iif(this.value =8,"WR",;
    space(2))))))))))
  endfunc
  func oMRTIPOMS_1_13.GetRadio()
    this.Parent.oContained.w_MRTIPOMS = this.RadioValue()
    return .t.
  endfunc

  func oMRTIPOMS_1_13.SetRadio()
    this.Parent.oContained.w_MRTIPOMS=trim(this.Parent.oContained.w_MRTIPOMS)
    this.value = ;
      iif(this.Parent.oContained.w_MRTIPOMS=="TU",1,;
      iif(this.Parent.oContained.w_MRTIPOMS=="AN",2,;
      iif(this.Parent.oContained.w_MRTIPOMS=="PO",3,;
      iif(this.Parent.oContained.w_MRTIPOMS=="CA",4,;
      iif(this.Parent.oContained.w_MRTIPOMS=="AX",5,;
      iif(this.Parent.oContained.w_MRTIPOMS=="PX",6,;
      iif(this.Parent.oContained.w_MRTIPOMS=="MR",7,;
      iif(this.Parent.oContained.w_MRTIPOMS=="WR",8,;
      0))))))))
  endfunc

  add object oMAGINI_1_14 as StdField with uid="WWAXPPIMPZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 71021882,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=232, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_1_15 as StdField with uid="IPBLQAUROZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 261010746,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=505, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_15.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oCODCOM_1_16 as StdField with uid="GGQGKFAWGY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 3266522,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=106, Top=258, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func oCODCOM_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_17 as StdField with uid="BJCGFCEHYO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 149149658,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=106, Top=284, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCOM) and g_COMM='S')
    endwith
   endif
  endfunc

  func oCODATT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oMRCODODI_1_18 as StdField with uid="LZDDMRGNCV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MRCODODI", cQueryName = "MRCODODI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona il codice ordine",;
    HelpContextID = 248898545,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=120, Top=345, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_MRCODODI"

  func oMRCODODI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODODI_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODODI_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oMRCODODI_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica ordini di produzione",'GSMR_AOL.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oMRCODODF_1_19 as StdField with uid="YJLRNHJMKT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MRCODODF", cQueryName = "MRCODODF",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona il codice ordine",;
    HelpContextID = 248898548,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=120, Top=369, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_MRCODODF"

  proc oMRCODODF_1_19.mDefault
    with this.Parent.oContained
      if empty(.w_MRCODODF)
        .w_MRCODODF = .w_MRCODODI
      endif
    endwith
  endproc

  func oMRCODODF_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODODF_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODODF_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oMRCODODF_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Anagrafica ordini di produzione",'GSMR_AOL.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATINI_1_20 as StdField with uid="KADSLBXBRA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 70968778,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=376, Top=345

  func oDATINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_21 as StdField with uid="ODRUJEKWCF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 260957642,;
   bGlobalFont=.t.,;
    Height=22, Width=71, Left=376, Top=369

  func oDATFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDAFINI_1_22 as StdField with uid="CKVGPKUDPP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DAFINI", cQueryName = "DAFINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 71026122,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=558, Top=345

  func oDAFINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
    endwith
    return bRes
  endfunc

  add object oDAFFIN_1_23 as StdField with uid="PWYQVYUXKE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DAFFIN", cQueryName = "DAFFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 261014986,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=558, Top=369

  func oDAFFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DAFINI<=.w_DAFFIN or empty(.w_DAFFIN))
    endwith
    return bRes
  endfunc

  add object oFLSUG_1_24 as StdCheck with uid="TAPGSEINNN",rtseq=24,rtrep=.f.,left=662, top=340, caption="Suggerito MRP",;
    ToolTipText = "Ordini 'suggeriti MRP'",;
    HelpContextID = 39916886,;
    cFormVar="w_FLSUG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSUG_1_24.RadioValue()
    return(iif(this.value =1,'M',;
    '.'))
  endfunc
  func oFLSUG_1_24.GetRadio()
    this.Parent.oContained.w_FLSUG = this.RadioValue()
    return .t.
  endfunc

  func oFLSUG_1_24.SetRadio()
    this.Parent.oContained.w_FLSUG=trim(this.Parent.oContained.w_FLSUG)
    this.value = ;
      iif(this.Parent.oContained.w_FLSUG=='M',1,;
      0)
  endfunc

  func oFLSUG_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MMRP='S')
    endwith
   endif
  endfunc

  add object oFLLAN_1_25 as StdCheck with uid="SPHINUGANI",rtseq=25,rtrep=.f.,left=662, top=362, caption="Lanciato",;
    ToolTipText = "Ordini 'lanciati' o 'da ordinare'",;
    HelpContextID = 45917526,;
    cFormVar="w_FLLAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLLAN_1_25.RadioValue()
    return(iif(this.value =1,'L',;
    '.'))
  endfunc
  func oFLLAN_1_25.GetRadio()
    this.Parent.oContained.w_FLLAN = this.RadioValue()
    return .t.
  endfunc

  func oFLLAN_1_25.SetRadio()
    this.Parent.oContained.w_FLLAN=trim(this.Parent.oContained.w_FLLAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLLAN=='L',1,;
      0)
  endfunc

  add object oFLPIA_1_26 as StdCheck with uid="EDXMXNACPM",rtseq=26,rtrep=.f.,left=662, top=381, caption="Pianificato",;
    ToolTipText = "Ordini 'pianificati' o 'ordinati'",;
    HelpContextID = 32826710,;
    cFormVar="w_FLPIA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPIA_1_26.RadioValue()
    return(iif(this.value =1,'P',;
    '.'))
  endfunc
  func oFLPIA_1_26.GetRadio()
    this.Parent.oContained.w_FLPIA = this.RadioValue()
    return .t.
  endfunc

  func oFLPIA_1_26.SetRadio()
    this.Parent.oContained.w_FLPIA=trim(this.Parent.oContained.w_FLPIA)
    this.value = ;
      iif(this.Parent.oContained.w_FLPIA=='P',1,;
      0)
  endfunc

  add object oCADESARF_1_29 as StdField with uid="GAONTSDYHO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CADESARF", cQueryName = "CADESARF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200271252,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=271, Top=59, InputMask=replicate('X',40)


  add object oObj_1_32 as cp_outputCombo with uid="ZLERXIVPWT",left=187, top=422, width=441,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 130901018


  add object oBtn_1_33 as StdButton with uid="IDQLKXKZHR",left=693, top=413, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa messaggi MRP";
    , HelpContextID = 217952746;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) AND .checkform())
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="UKWSGSSBLX",left=746, top=413, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 217952746;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESFAMAI_1_42 as StdField with uid="AOYBYAMKXI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17691009,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=168, Top=83, InputMask=replicate('X',35)

  add object oDESGRUI_1_43 as StdField with uid="ZABORREMDH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134417974,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=168, Top=131, InputMask=replicate('X',35)

  add object oDESCATI_1_44 as StdField with uid="ZFWYARIWSP",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99552822,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=569, Top=83, InputMask=replicate('X',35)

  add object oDESFAMAF_1_48 as StdField with uid="WOCDSQXKSA",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17691012,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=168, Top=107, InputMask=replicate('X',35)

  add object oDESGRUF_1_49 as StdField with uid="PYGQUHDJMJ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134417974,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=168, Top=157, InputMask=replicate('X',35)

  add object oDESCATF_1_50 as StdField with uid="SXSGGSHCII",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99552822,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=569, Top=107, InputMask=replicate('X',35)

  add object oDESMAGI_1_55 as StdField with uid="QONUNYBHAK",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 150539830,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=170, Top=232, InputMask=replicate('X',30)

  add object oDESMAGF_1_57 as StdField with uid="EBXVFVWORM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 117895626,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=567, Top=231, InputMask=replicate('X',30)

  add object oDESCOM_1_59 as StdField with uid="BCJHQPAVGJ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 3207626,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=241, Top=258, InputMask=replicate('X',30)

  add object oDESATT_1_66 as StdField with uid="TTNMSCEJIW",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 149090762,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=241, Top=284, InputMask=replicate('X',30)

  add object oCADESART_1_74 as StdField with uid="OLXKYKLWLT",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CADESART", cQueryName = "CADESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200271238,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=271, Top=35, InputMask=replicate('X',40)

  add object oStr_1_27 as StdString with uid="COFMRRIZUL",Visible=.t., Left=8, Top=345,;
    Alignment=1, Width=108, Height=18,;
    Caption="Da codice ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NZGZFHZUAA",Visible=.t., Left=421, Top=193,;
    Alignment=1, Width=84, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TEPMOXPXNZ",Visible=.t., Left=421, Top=167,;
    Alignment=1, Width=84, Height=18,;
    Caption="Messaggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="PGZKNKALVF",Visible=.t., Left=76, Top=422,;
    Alignment=1, Width=106, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="TTWCNWQOUV",Visible=.t., Left=17, Top=369,;
    Alignment=1, Width=99, Height=18,;
    Caption="A codice ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WHGWHEWFXM",Visible=.t., Left=4, Top=3,;
    Alignment=0, Width=277, Height=18,;
    Caption="Dati articolo, tipo e stato messaggio"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="CGKMUWGJNM",Visible=.t., Left=265, Top=345,;
    Alignment=1, Width=109, Height=18,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DFAYSLSKFL",Visible=.t., Left=268, Top=369,;
    Alignment=1, Width=106, Height=18,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="QZYVSVNFWE",Visible=.t., Left=462, Top=345,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="GNOODMLOOO",Visible=.t., Left=465, Top=369,;
    Alignment=1, Width=92, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=3, Top=83,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=3, Top=131,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=414, Top=83,;
    Alignment=1, Width=91, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="BETNLUNOYI",Visible=.t., Left=3, Top=107,;
    Alignment=1, Width=99, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=3, Top=157,;
    Alignment=1, Width=99, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=413, Top=107,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=3, Top=232,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=419, Top=231,;
    Alignment=1, Width=84, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=3, Top=258,;
    Alignment=1, Width=99, Height=17,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="FDKAMZOQXS",Visible=.t., Left=9, Top=202,;
    Alignment=0, Width=140, Height=15,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="WCWNTHPNKF",Visible=.t., Left=6, Top=313,;
    Alignment=0, Width=140, Height=18,;
    Caption="Riferimento ODL"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="MSVOMVFWHL",Visible=.t., Left=3, Top=284,;
    Alignment=1, Width=99, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="JWNXIBDPQK",Visible=.t., Left=421, Top=135,;
    Alignment=1, Width=84, Height=18,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="RTWKOYSRLV",Visible=.t., Left=549, Top=135,;
    Alignment=1, Width=49, Height=18,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="YQQPZKBETJ",Visible=.t., Left=647, Top=135,;
    Alignment=1, Width=41, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="RRVAHKPCGR",Visible=.t., Left=6, Top=35,;
    Alignment=1, Width=96, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="YAJEXUDDMQ",Visible=.t., Left=6, Top=59,;
    Alignment=1, Width=96, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_36 as StdBox with uid="AVMMROXYXX",left=6, top=22, width=803,height=1

  add object oBox_1_61 as StdBox with uid="CICMJYRPQM",left=6, top=225, width=803,height=1

  add object oBox_1_62 as StdBox with uid="UUOOJKZEEF",left=6, top=406, width=803,height=1

  add object oBox_1_64 as StdBox with uid="TPOFLNVJUE",left=6, top=332, width=803,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_svm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
