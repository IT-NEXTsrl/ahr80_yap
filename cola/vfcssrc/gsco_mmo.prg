* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mmo                                                        *
*              Lista materiali di output                                       *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-05                                                      *
* Last revis.: 2015-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mmo")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mmo")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mmo")
  return

* --- Class definition
define class tgsco_mmo as StdPCForm
  Width  = 862
  Height = 453
  Top    = -2
  Left   = 0
  cComment = "Lista materiali di output"
  cPrg = "gsco_mmo"
  HelpContextID=70670999
  add object cnt as tcgsco_mmo
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mmo as PCContext
  w_MOCODODL = space(15)
  w_CPROWORD = 0
  w_MOCODICE = space(20)
  w_MOCODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_MOUNIMIS = space(3)
  w_MOCOEIMP = 0
  w_MOCOEIM1 = 0
  w_DESCOD = space(40)
  w_DATOBSO = space(8)
  w_OBTEST = space(8)
  w_CODCICLO = space(10)
  w_ARMAGPRE = space(10)
  w_MAGPRE = space(10)
  w_MOCODMAG = space(5)
  w_EDITROW = space(1)
  w_EDITCHK = space(1)
  w_ReadPar = space(2)
  w_MAGPRO = space(5)
  w_MOFASRIF = 0
  w_MOROWNUM = 0
  w_QTAMOV = 0
  proc Save(i_oFrom)
    this.w_MOCODODL = i_oFrom.w_MOCODODL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MOCODICE = i_oFrom.w_MOCODICE
    this.w_MOCODART = i_oFrom.w_MOCODART
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_MOUNIMIS = i_oFrom.w_MOUNIMIS
    this.w_MOCOEIMP = i_oFrom.w_MOCOEIMP
    this.w_MOCOEIM1 = i_oFrom.w_MOCOEIM1
    this.w_DESCOD = i_oFrom.w_DESCOD
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CODCICLO = i_oFrom.w_CODCICLO
    this.w_ARMAGPRE = i_oFrom.w_ARMAGPRE
    this.w_MAGPRE = i_oFrom.w_MAGPRE
    this.w_MOCODMAG = i_oFrom.w_MOCODMAG
    this.w_EDITROW = i_oFrom.w_EDITROW
    this.w_EDITCHK = i_oFrom.w_EDITCHK
    this.w_ReadPar = i_oFrom.w_ReadPar
    this.w_MAGPRO = i_oFrom.w_MAGPRO
    this.w_MOFASRIF = i_oFrom.w_MOFASRIF
    this.w_MOROWNUM = i_oFrom.w_MOROWNUM
    this.w_QTAMOV = i_oFrom.w_QTAMOV
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MOCODODL = this.w_MOCODODL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MOCODICE = this.w_MOCODICE
    i_oTo.w_MOCODART = this.w_MOCODART
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_MOUNIMIS = this.w_MOUNIMIS
    i_oTo.w_MOCOEIMP = this.w_MOCOEIMP
    i_oTo.w_MOCOEIM1 = this.w_MOCOEIM1
    i_oTo.w_DESCOD = this.w_DESCOD
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CODCICLO = this.w_CODCICLO
    i_oTo.w_ARMAGPRE = this.w_ARMAGPRE
    i_oTo.w_MAGPRE = this.w_MAGPRE
    i_oTo.w_MOCODMAG = this.w_MOCODMAG
    i_oTo.w_EDITROW = this.w_EDITROW
    i_oTo.w_EDITCHK = this.w_EDITCHK
    i_oTo.w_ReadPar = this.w_ReadPar
    i_oTo.w_MAGPRO = this.w_MAGPRO
    i_oTo.w_MOFASRIF = this.w_MOFASRIF
    i_oTo.w_MOROWNUM = this.w_MOROWNUM
    i_oTo.w_QTAMOV = this.w_QTAMOV
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mmo as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 862
  Height = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-16"
  HelpContextID=70670999
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ODL_MOUT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  MAGAZZIN_IDX = 0
  PAR_PROD_IDX = 0
  cFile = "ODL_MOUT"
  cKeySelect = "MOCODODL"
  cKeyWhere  = "MOCODODL=this.w_MOCODODL"
  cKeyDetail  = "MOCODODL=this.w_MOCODODL"
  cKeyWhereODBC = '"MOCODODL="+cp_ToStrODBC(this.w_MOCODODL)';

  cKeyDetailWhereODBC = '"MOCODODL="+cp_ToStrODBC(this.w_MOCODODL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"ODL_MOUT.MOCODODL="+cp_ToStrODBC(this.w_MOCODODL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ODL_MOUT.CPROWORD '
  cPrg = "gsco_mmo"
  cComment = "Lista materiali di output"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 22
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MOCODODL = space(15)
  w_CPROWORD = 0
  w_MOCODICE = space(20)
  o_MOCODICE = space(20)
  w_MOCODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_MOUNIMIS = space(3)
  w_MOCOEIMP = 0
  o_MOCOEIMP = 0
  w_MOCOEIM1 = 0
  w_DESCOD = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODCICLO = space(10)
  w_ARMAGPRE = space(10)
  w_MAGPRE = space(10)
  w_MOCODMAG = space(5)
  w_EDITROW = .F.
  w_EDITCHK = .F.
  w_ReadPar = space(2)
  w_MAGPRO = space(5)
  w_MOFASRIF = 0
  w_MOROWNUM = 0
  w_QTAMOV = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mmoPag1","gsco_mmo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='PAR_PROD'
    this.cWorkTables[6]='ODL_MOUT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODL_MOUT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODL_MOUT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mmo'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ODL_MOUT where MOCODODL=KeySet.MOCODODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2],this.bLoadRecFilter,this.ODL_MOUT_IDX,"gsco_mmo")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODL_MOUT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODL_MOUT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ODL_MOUT '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOCODODL',this.w_MOCODODL  )
      select * from (i_cTable) ODL_MOUT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATOBSO = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_MOCODODL = NVL(MOCODODL,space(15))
        .w_CODCICLO = this.oParentObject.w_OLTSECIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_EDITROW = .t.
        .w_EDITCHK = .t.
        cp_LoadRecExtFlds(this,'ODL_MOUT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_UNMIS3 = space(3)
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_DESCOD = space(40)
          .w_ARMAGPRE = space(10)
        .w_ReadPar = 'PP'
          .w_MAGPRO = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MOCODICE = NVL(MOCODICE,space(20))
          if link_2_2_joined
            this.w_MOCODICE = NVL(CACODICE202,NVL(this.w_MOCODICE,space(20)))
            this.w_DESCOD = NVL(CADESART202,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOCODART = NVL(CACODART202,space(20))
          else
          .link_2_2('Load')
          endif
          .w_MOCODART = NVL(MOCODART,space(20))
          if link_2_3_joined
            this.w_MOCODART = NVL(ARCODART203,NVL(this.w_MOCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_OPERAT = NVL(AROPERAT203,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP203,0)
            this.w_UNMIS2 = NVL(ARUNMIS2203,space(3))
            this.w_ARMAGPRE = NVL(ARMAGPRE203,space(10))
          else
          .link_2_3('Load')
          endif
          .w_MOUNIMIS = NVL(MOUNIMIS,space(3))
          * evitabile
          *.link_2_11('Load')
          .w_MOCOEIMP = NVL(MOCOEIMP,0)
          .w_MOCOEIM1 = NVL(MOCOEIM1,0)
        .w_MAGPRE = iif(empty(.w_ARMAGPRE), iif(empty(.w_MAGPRO),g_MAGAZI, .w_MAGPRO),.w_ARMAGPRE)
          .w_MOCODMAG = NVL(MOCODMAG,space(5))
          * evitabile
          *.link_2_18('Load')
          .link_1_7('Load')
          .w_MOFASRIF = NVL(MOFASRIF,0)
          .w_MOROWNUM = NVL(MOROWNUM,0)
        .w_QTAMOV = this.oParentObject.w_OLTQTOD1*.w_MOCOEIMP
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODCICLO = this.oParentObject.w_OLTSECIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_EDITROW = .t.
        .w_EDITCHK = .t.
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MOCODODL=space(15)
      .w_CPROWORD=10
      .w_MOCODICE=space(20)
      .w_MOCODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_MOUNIMIS=space(3)
      .w_MOCOEIMP=0
      .w_MOCOEIM1=0
      .w_DESCOD=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CODCICLO=space(10)
      .w_ARMAGPRE=space(10)
      .w_MAGPRE=space(10)
      .w_MOCODMAG=space(5)
      .w_EDITROW=.f.
      .w_EDITCHK=.f.
      .w_ReadPar=space(2)
      .w_MAGPRO=space(5)
      .w_MOFASRIF=0
      .w_MOROWNUM=0
      .w_QTAMOV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_MOCODICE))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MOCODART))
         .link_2_3('Full')
        endif
        .DoRTCalc(5,11,.f.)
        .w_MOUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MOUNIMIS))
         .link_2_11('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_MOCOEIM1 = CALQTA(.w_MOCOEIMP,.w_MOUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N','', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,g_PERPQD)
        .DoRTCalc(15,16,.f.)
        .w_OBTEST = i_datsys
        .w_CODCICLO = this.oParentObject.w_OLTSECIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(19,19,.f.)
        .w_MAGPRE = iif(empty(.w_ARMAGPRE), iif(empty(.w_MAGPRO),g_MAGAZI, .w_MAGPRO),.w_ARMAGPRE)
        .w_MOCODMAG = .w_MAGPRE
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_MOCODMAG))
         .link_2_18('Full')
        endif
        .w_EDITROW = .t.
        .w_EDITCHK = .t.
        .w_ReadPar = 'PP'
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_ReadPar))
         .link_1_7('Full')
        endif
        .DoRTCalc(25,27,.f.)
        .w_QTAMOV = this.oParentObject.w_OLTQTOD1*.w_MOCOEIMP
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODL_MOUT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ODL_MOUT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODODL,"MOCODODL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MOCODICE C(20);
      ,t_MOUNIMIS C(3);
      ,t_MOCOEIMP N(12,5);
      ,t_DESCOD C(40);
      ,t_MOCODMAG C(5);
      ,t_MOFASRIF N(5);
      ,t_QTAMOV N(12,5);
      ,CPROWNUM N(10);
      ,t_MOCODART C(20);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_MOLTIP N(10,4);
      ,t_OPERAT C(1);
      ,t_UNMIS3 C(3);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_MOCOEIM1 N(12,5);
      ,t_ARMAGPRE C(10);
      ,t_MAGPRE C(10);
      ,t_ReadPar C(2);
      ,t_MAGPRO C(5);
      ,t_MOROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mmobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODICE_2_2.controlsource=this.cTrsName+'.t_MOCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMOUNIMIS_2_11.controlsource=this.cTrsName+'.t_MOUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMOCOEIMP_2_12.controlsource=this.cTrsName+'.t_MOCOEIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOD_2_14.controlsource=this.cTrsName+'.t_DESCOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODMAG_2_18.controlsource=this.cTrsName+'.t_MOCODMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMOFASRIF_2_19.controlsource=this.cTrsName+'.t_MOFASRIF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oQTAMOV_2_21.controlsource=this.cTrsName+'.t_QTAMOV'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(58)
    this.AddVLine(214)
    this.AddVLine(468)
    this.AddVLine(512)
    this.AddVLine(626)
    this.AddVLine(733)
    this.AddVLine(784)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2])
      *
      * insert into ODL_MOUT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODL_MOUT')
        i_extval=cp_InsertValODBCExtFlds(this,'ODL_MOUT')
        i_cFldBody=" "+;
                  "(MOCODODL,CPROWORD,MOCODICE,MOCODART,MOUNIMIS"+;
                  ",MOCOEIMP,MOCOEIM1,MOCODMAG,MOFASRIF,MOROWNUM,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MOCODODL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MOCODICE)+","+cp_ToStrODBCNull(this.w_MOCODART)+","+cp_ToStrODBCNull(this.w_MOUNIMIS)+;
             ","+cp_ToStrODBC(this.w_MOCOEIMP)+","+cp_ToStrODBC(this.w_MOCOEIM1)+","+cp_ToStrODBCNull(this.w_MOCODMAG)+","+cp_ToStrODBC(this.w_MOFASRIF)+","+cp_ToStrODBC(this.w_MOROWNUM)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODL_MOUT')
        i_extval=cp_InsertValVFPExtFlds(this,'ODL_MOUT')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MOCODODL',this.w_MOCODODL)
        INSERT INTO (i_cTable) (;
                   MOCODODL;
                  ,CPROWORD;
                  ,MOCODICE;
                  ,MOCODART;
                  ,MOUNIMIS;
                  ,MOCOEIMP;
                  ,MOCOEIM1;
                  ,MOCODMAG;
                  ,MOFASRIF;
                  ,MOROWNUM;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MOCODODL;
                  ,this.w_CPROWORD;
                  ,this.w_MOCODICE;
                  ,this.w_MOCODART;
                  ,this.w_MOUNIMIS;
                  ,this.w_MOCOEIMP;
                  ,this.w_MOCOEIM1;
                  ,this.w_MOCODMAG;
                  ,this.w_MOFASRIF;
                  ,this.w_MOROWNUM;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 and not empty(t_MOCODICE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_MOUT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_MOUT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 and not empty(t_MOCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ODL_MOUT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_MOUT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MOCODICE="+cp_ToStrODBCNull(this.w_MOCODICE)+;
                     ",MOCODART="+cp_ToStrODBCNull(this.w_MOCODART)+;
                     ",MOUNIMIS="+cp_ToStrODBCNull(this.w_MOUNIMIS)+;
                     ",MOCOEIMP="+cp_ToStrODBC(this.w_MOCOEIMP)+;
                     ",MOCOEIM1="+cp_ToStrODBC(this.w_MOCOEIM1)+;
                     ",MOCODMAG="+cp_ToStrODBCNull(this.w_MOCODMAG)+;
                     ",MOFASRIF="+cp_ToStrODBC(this.w_MOFASRIF)+;
                     ",MOROWNUM="+cp_ToStrODBC(this.w_MOROWNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_MOUT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MOCODICE=this.w_MOCODICE;
                     ,MOCODART=this.w_MOCODART;
                     ,MOUNIMIS=this.w_MOUNIMIS;
                     ,MOCOEIMP=this.w_MOCOEIMP;
                     ,MOCOEIM1=this.w_MOCOEIM1;
                     ,MOCODMAG=this.w_MOCODMAG;
                     ,MOFASRIF=this.w_MOFASRIF;
                     ,MOROWNUM=this.w_MOROWNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 and not empty(t_MOCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ODL_MOUT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 and not empty(t_MOCODICE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_MOUT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MOUT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_2_3('Full')
        .DoRTCalc(5,11,.t.)
        if .o_MOCODICE<>.w_MOCODICE
          .w_MOUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_11('Full')
        endif
        .DoRTCalc(13,13,.t.)
          .w_MOCOEIM1 = CALQTA(.w_MOCOEIMP,.w_MOUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N','', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,g_PERPQD)
        .DoRTCalc(15,17,.t.)
          .w_CODCICLO = this.oParentObject.w_OLTSECIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(19,19,.t.)
          .w_MAGPRE = iif(empty(.w_ARMAGPRE), iif(empty(.w_MAGPRO),g_MAGAZI, .w_MAGPRO),.w_ARMAGPRE)
        if .o_MOCODICE<>.w_MOCODICE
          .w_MOCODMAG = .w_MAGPRE
          .link_2_18('Full')
        endif
          .w_EDITROW = .t.
          .w_EDITCHK = .t.
          .link_1_7('Full')
        .DoRTCalc(25,27,.t.)
        if .o_MOCOEIMP<>.w_MOCOEIMP
          .w_QTAMOV = this.oParentObject.w_OLTQTOD1*.w_MOCOEIMP
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MOCODART with this.w_MOCODART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_MOCOEIM1 with this.w_MOCOEIM1
      replace t_ARMAGPRE with this.w_ARMAGPRE
      replace t_MAGPRE with this.w_MAGPRE
      replace t_ReadPar with this.w_ReadPar
      replace t_MAGPRO with this.w_MAGPRO
      replace t_MOROWNUM with this.w_MOROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCODICE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCODICE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOUNIMIS_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOUNIMIS_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCOEIMP_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCOEIMP_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCODMAG_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOCODMAG_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOFASRIF_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMOFASRIF_2_19.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MOCODICE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MOCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MOCODICE))
          select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MOCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MOCODICE)+"%");

            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMOCODICE_2_2'),i_cWhere,'',"Codici di ricerca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MOCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MOCODICE)
            select CACODICE,CADESART,CAUNIMIS,CAMOLTIP,CAOPERAT,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOD = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOCODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODICE = space(20)
      endif
      this.w_DESCOD = space(40)
      this.w_UNMIS3 = space(3)
      this.w_MOLTI3 = 0
      this.w_OPERA3 = space(1)
      this.w_MOCODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CACODART as CACODART202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on ODL_MOUT.MOCODICE=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and ODL_MOUT.MOCODICE=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MOCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MOCODART)
            select ARCODART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARMAGPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_ARMAGPRE = NVL(_Link_.ARMAGPRE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_ARMAGPRE = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.AROPERAT as AROPERAT203"+ ",link_2_3.ARMOLTIP as ARMOLTIP203"+ ",link_2_3.ARUNMIS2 as ARUNMIS2203"+ ",link_2_3.ARMAGPRE as ARMAGPRE203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on ODL_MOUT.MOCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and ODL_MOUT.MOCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOUNIMIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_MOUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_MOUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oMOUNIMIS_2_11'),i_cWhere,'',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MOUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MOUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_MOUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODMAG
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MOCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MOCODMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMOCODMAG_2_18'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MOCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MOCODMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ReadPar
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPMAGPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ReadPar)
            select PPCODICE,PPMAGPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PPCODICE,space(2))
      this.w_MAGPRO = NVL(_Link_.PPMAGPRO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(2)
      endif
      this.w_MAGPRO = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODICE_2_2.value==this.w_MOCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODICE_2_2.value=this.w_MOCODICE
      replace t_MOCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOUNIMIS_2_11.value==this.w_MOUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOUNIMIS_2_11.value=this.w_MOUNIMIS
      replace t_MOUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOUNIMIS_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCOEIMP_2_12.value==this.w_MOCOEIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCOEIMP_2_12.value=this.w_MOCOEIMP
      replace t_MOCOEIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCOEIMP_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOD_2_14.value==this.w_DESCOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOD_2_14.value=this.w_DESCOD
      replace t_DESCOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOD_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODMAG_2_18.value==this.w_MOCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODMAG_2_18.value=this.w_MOCODMAG
      replace t_MOCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCODMAG_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOFASRIF_2_19.value==this.w_MOFASRIF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOFASRIF_2_19.value=this.w_MOFASRIF
      replace t_MOFASRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOFASRIF_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAMOV_2_21.value==this.w_QTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAMOV_2_21.value=this.w_QTAMOV
      replace t_QTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oQTAMOV_2_21.value
    endif
    cp_SetControlsValueExtFlds(this,'ODL_MOUT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_MOUNIMIS) or not(CHKUNIMI(.w_MOUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (not empty(.w_MOCODICE) and (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK)) and (.w_CPROWORD<>0 and not empty(.w_MOCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOUNIMIS_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_MOCOEIMP>0) and ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK)) and (.w_CPROWORD<>0 and not empty(.w_MOCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMOCOEIMP_2_12
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_CPROWORD<>0 and not empty(.w_MOCODICE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MOCODICE = this.w_MOCODICE
    this.o_MOCOEIMP = this.w_MOCOEIMP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 and not empty(t_MOCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MOCODICE=space(20)
      .w_MOCODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_MOUNIMIS=space(3)
      .w_MOCOEIMP=0
      .w_MOCOEIM1=0
      .w_DESCOD=space(40)
      .w_ARMAGPRE=space(10)
      .w_MAGPRE=space(10)
      .w_MOCODMAG=space(5)
      .w_ReadPar=space(2)
      .w_MAGPRO=space(5)
      .w_MOFASRIF=0
      .w_MOROWNUM=0
      .w_QTAMOV=0
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_MOCODICE))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
      if not(empty(.w_MOCODART))
        .link_2_3('Full')
      endif
      .DoRTCalc(5,11,.f.)
        .w_MOUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_MOUNIMIS))
        .link_2_11('Full')
      endif
      .DoRTCalc(13,13,.f.)
        .w_MOCOEIM1 = CALQTA(.w_MOCOEIMP,.w_MOUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N','', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,g_PERPQD)
      .DoRTCalc(15,19,.f.)
        .w_MAGPRE = iif(empty(.w_ARMAGPRE), iif(empty(.w_MAGPRO),g_MAGAZI, .w_MAGPRO),.w_ARMAGPRE)
        .w_MOCODMAG = .w_MAGPRE
      .DoRTCalc(21,21,.f.)
      if not(empty(.w_MOCODMAG))
        .link_2_18('Full')
      endif
      .DoRTCalc(22,23,.f.)
        .w_ReadPar = 'PP'
      .DoRTCalc(24,24,.f.)
      if not(empty(.w_ReadPar))
        .link_1_7('Full')
      endif
      .DoRTCalc(25,27,.f.)
        .w_QTAMOV = this.oParentObject.w_OLTQTOD1*.w_MOCOEIMP
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MOCODICE = t_MOCODICE
    this.w_MOCODART = t_MOCODART
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_UNMIS3 = t_UNMIS3
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_MOUNIMIS = t_MOUNIMIS
    this.w_MOCOEIMP = t_MOCOEIMP
    this.w_MOCOEIM1 = t_MOCOEIM1
    this.w_DESCOD = t_DESCOD
    this.w_ARMAGPRE = t_ARMAGPRE
    this.w_MAGPRE = t_MAGPRE
    this.w_MOCODMAG = t_MOCODMAG
    this.w_ReadPar = t_ReadPar
    this.w_MAGPRO = t_MAGPRO
    this.w_MOFASRIF = t_MOFASRIF
    this.w_MOROWNUM = t_MOROWNUM
    this.w_QTAMOV = t_QTAMOV
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MOCODICE with this.w_MOCODICE
    replace t_MOCODART with this.w_MOCODART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_MOUNIMIS with this.w_MOUNIMIS
    replace t_MOCOEIMP with this.w_MOCOEIMP
    replace t_MOCOEIM1 with this.w_MOCOEIM1
    replace t_DESCOD with this.w_DESCOD
    replace t_ARMAGPRE with this.w_ARMAGPRE
    replace t_MAGPRE with this.w_MAGPRE
    replace t_MOCODMAG with this.w_MOCODMAG
    replace t_ReadPar with this.w_ReadPar
    replace t_MAGPRO with this.w_MAGPRO
    replace t_MOFASRIF with this.w_MOFASRIF
    replace t_MOROWNUM with this.w_MOROWNUM
    replace t_QTAMOV with this.w_QTAMOV
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mmoPag1 as StdContainer
  Width  = 858
  height = 453
  stdWidth  = 858
  stdheight = 453
  resizeXpos=534
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=6, width=842,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Pos.",Field2="MOCODICE",Label2="Codice",Field3="DESCOD",Label3="Descrizione",Field4="MOUNIMIS",Label4="UM",Field5="MOCOEIMP",Label5="Coeff.",Field6="QTAMOV",Label6="Quantit�",Field7="MOCODMAG",Label7="Magazz.",Field8="MOFASRIF",Label8="Fase",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 150109062

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=23,;
    width=838+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*22*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=24,width=837+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*22*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|MAGAZZIN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMOCODICE_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oMOUNIMIS_2_11
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oMOCODMAG_2_18
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsco_mmoBodyRow as CPBodyRowCnt
  Width=828
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="VLDSBMDRKI",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 117780630,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["@Z 99999"], cGetPict=["99999"]

  add object oMOCODICE_2_2 as StdTrsField with uid="IZVBDBYYVZ",rtseq=3,rtrep=.t.,;
    cFormVar="w_MOCODICE",value=space(20),;
    HelpContextID = 238428405,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=49, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MOCODICE"

  func oMOCODICE_2_2.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oMOCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODICE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMOCODICE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMOCODICE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'',this.parent.oContained
  endproc

  add object oMOUNIMIS_2_11 as StdTrsField with uid="MIELQCSXZO",rtseq=12,rtrep=.t.,;
    cFormVar="w_MOUNIMIS",value=space(3),;
    HelpContextID = 102367001,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=459, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_MOUNIMIS"

  func oMOUNIMIS_2_11.mCond()
    with this.Parent.oContained
      return (not empty(.w_MOCODICE) and (.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oMOUNIMIS_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOUNIMIS_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMOUNIMIS_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oMOUNIMIS_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'',this.parent.oContained
  endproc

  add object oMOCOEIMP_2_12 as StdTrsField with uid="JZPBCBCIDD",rtseq=13,rtrep=.t.,;
    cFormVar="w_MOCOEIMP",value=0,;
    HelpContextID = 237379818,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=503, Top=0, cSayPict=[v_PQ(32)], cGetPict=[v_GQ(32)]

  func oMOCOEIMP_2_12.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oMOCOEIMP_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MOCOEIMP>0)
    endwith
    return bRes
  endfunc

  add object oDESCOD_2_14 as StdTrsField with uid="RXHYIXCHKX",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESCOD",value=space(40),enabled=.f.,;
    HelpContextID = 43068362,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=251, Left=205, Top=0, InputMask=replicate('X',40)

  add object oMOCODMAG_2_18 as StdTrsField with uid="KJSPJYJBRH",rtseq=21,rtrep=.t.,;
    cFormVar="w_MOCODMAG",value=space(5),;
    HelpContextID = 171319539,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=724, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MOCODMAG"

  func oMOCODMAG_2_18.mCond()
    with this.Parent.oContained
      return ((.w_EDITROW And .w_EDITCHK Or !.w_EDITROW And .w_EDITCHK))
    endwith
  endfunc

  func oMOCODMAG_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODMAG_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMOCODMAG_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMOCODMAG_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oMOFASRIF_2_19 as StdTrsField with uid="NWOHPYEDUZ",rtseq=26,rtrep=.t.,;
    cFormVar="w_MOFASRIF",value=0,;
    ToolTipText = "Fase di riferimento",;
    HelpContextID = 195825420,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=775, Top=0

  func oMOFASRIF_2_19.mCond()
    with this.Parent.oContained
      return (g_PRFA='S' and g_CICLILAV='S' and not empty(nvl(.w_CODCICLO,'')))
    endwith
  endfunc

  add object oQTAMOV_2_21 as StdTrsField with uid="VLYNLKWPYZ",rtseq=28,rtrep=.t.,;
    cFormVar="w_QTAMOV",value=0,enabled=.f.,;
    HelpContextID = 8928250,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=104, Left=617, Top=0, cSayPict=[v_GQ(32)], cGetPict=[v_GQ(32)]
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=21
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mmo','ODL_MOUT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOCODODL=ODL_MOUT.MOCODODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
