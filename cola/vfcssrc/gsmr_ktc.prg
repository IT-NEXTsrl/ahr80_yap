* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_ktc                                                        *
*              Tracciabilit� commessa sui saldi                                *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-24                                                      *
* Last revis.: 2016-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_ktc",oParentObject))

* --- Class definition
define class tgsmr_ktc as StdForm
  Top    = 0
  Left   = 6

  * --- Standard Properties
  Width  = 790
  Height = 537+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-08-31"
  HelpContextID=82406249
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=89

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  BUSIUNIT_IDX = 0
  ATTIVITA_IDX = 0
  ODL_MAST_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  AZIENDA_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsmr_ktc"
  cComment = "Tracciabilit� commessa sui saldi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_pCODCOM = space(15)
  w_DESCAN = space(30)
  w_pCODICE = space(41)
  w_pCODCLI = space(15)
  w_pDATINI = ctod('  /  /  ')
  w_pDATFIN = ctod('  /  /  ')
  w_IMPOR = space(1)
  w_VISDOC = space(1)
  o_VISDOC = space(1)
  w_VISORD = space(1)
  o_VISORD = space(1)
  w_VISSAL = space(1)
  w_NSALZERO = space(1)
  w_APEVA = space(1)
  w_DESRIC = space(40)
  w_TIPCLI = space(1)
  w_DESCLI = space(40)
  w_pTIPCLI = space(1)
  w_TIPFOR = space(1)
  w_DESART = space(41)
  w_TIPO = space(3)
  w_PARAME = space(3)
  w_MVSERIAL = space(10)
  w_OLCODODL = space(15)
  w_PDSERIAL = space(10)
  w_PDROWNUM = 0
  w_CODART = space(20)
  w_ARCODART = space(20)
  w_CODMAG = space(10)
  w_CDCOMM = space(15)
  w_CODAZI = space(5)
  w_AZIENDA = space(5)
  w_gestbu = space(1)
  w_TIPATT = space(1)
  w_FLVEACQ = space(1)
  o_FLVEACQ = space(1)
  w_TIPDOC = space(5)
  w_NUMINI = 0
  w_SERIE1 = space(2)
  w_NUMFIN = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_CODMAGA = space(5)
  w_CODBUS = space(3)
  w_FORSEL = space(15)
  w_FORSEL = space(15)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_TIPORD = space(1)
  w_STAORD = space(1)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_COMODL = space(15)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESDISI = space(40)
  w_DESDISF = space(40)
  w_DESFOR = space(40)
  w_DESDOC = space(35)
  w_SERIE2 = space(2)
  w_DESCOML = space(30)
  w_LLCINI = 0
  o_LLCINI = 0
  w_LLCFIN = 0
  w_DATOBSO = ctod('  /  /  ')
  w_TIPCON = space(10)
  w_MVCODCON = space(10)
  w_FLAGSART = space(10)
  w_MVCODART = space(20)
  w_MVCODVAR = space(20)
  w_SLCODVAR = space(20)
  w_MVCODMAG = space(5)
  w_FLVEAC = space(2)
  w_MVTIPCON = space(1)
  w_CODICE = space(20)
  w_FLCOMM = space(1)
  w_ELENCO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_ktcPag1","gsmr_ktc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsmr_ktcPag2","gsmr_ktc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.opCODCOM_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ELENCO = this.oPgFrm.Pages(1).oPag.ELENCO
    DoDefault()
    proc Destroy()
      this.w_ELENCO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='BUSIUNIT'
    this.cWorkTables[7]='ATTIVITA'
    this.cWorkTables[8]='ODL_MAST'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='CATEGOMO'
    this.cWorkTables[12]='AZIENDA'
    this.cWorkTables[13]='ART_ICOL'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec("..\COLA\EXE\QUERY\GSMR4KTC.VQR",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_pCODCOM=space(15)
      .w_DESCAN=space(30)
      .w_pCODICE=space(41)
      .w_pCODCLI=space(15)
      .w_pDATINI=ctod("  /  /  ")
      .w_pDATFIN=ctod("  /  /  ")
      .w_IMPOR=space(1)
      .w_VISDOC=space(1)
      .w_VISORD=space(1)
      .w_VISSAL=space(1)
      .w_NSALZERO=space(1)
      .w_APEVA=space(1)
      .w_DESRIC=space(40)
      .w_TIPCLI=space(1)
      .w_DESCLI=space(40)
      .w_pTIPCLI=space(1)
      .w_TIPFOR=space(1)
      .w_DESART=space(41)
      .w_TIPO=space(3)
      .w_PARAME=space(3)
      .w_MVSERIAL=space(10)
      .w_OLCODODL=space(15)
      .w_PDSERIAL=space(10)
      .w_PDROWNUM=0
      .w_CODART=space(20)
      .w_ARCODART=space(20)
      .w_CODMAG=space(10)
      .w_CDCOMM=space(15)
      .w_CODAZI=space(5)
      .w_AZIENDA=space(5)
      .w_gestbu=space(1)
      .w_TIPATT=space(1)
      .w_FLVEACQ=space(1)
      .w_TIPDOC=space(5)
      .w_NUMINI=0
      .w_SERIE1=space(2)
      .w_NUMFIN=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_CODMAGA=space(5)
      .w_CODBUS=space(3)
      .w_FORSEL=space(15)
      .w_FORSEL=space(15)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_TIPORD=space(1)
      .w_STAORD=space(1)
      .w_OLCODINI=space(15)
      .w_OLCODFIN=space(15)
      .w_COMODL=space(15)
      .w_DBCODINI=space(20)
      .w_DBCODFIN=space(20)
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESDISI=space(40)
      .w_DESDISF=space(40)
      .w_DESFOR=space(40)
      .w_DESDOC=space(35)
      .w_SERIE2=space(2)
      .w_DESCOML=space(30)
      .w_LLCINI=0
      .w_LLCFIN=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPCON=space(10)
      .w_MVCODCON=space(10)
      .w_FLAGSART=space(10)
      .w_MVCODART=space(20)
      .w_MVCODVAR=space(20)
      .w_SLCODVAR=space(20)
      .w_MVCODMAG=space(5)
      .w_FLVEAC=space(2)
      .w_MVTIPCON=space(1)
      .w_CODICE=space(20)
      .w_FLCOMM=space(1)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_pCODCOM))
          .link_1_4('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_pCODICE))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_pCODCLI))
          .link_1_7('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_IMPOR = 'T'
        .w_VISDOC = 'S'
        .w_VISORD = 'S'
        .w_VISSAL = 'S'
        .w_NSALZERO = "S"
        .w_APEVA = 'T'
          .DoRTCalc(14,14,.f.)
        .w_TIPCLI = "C"
          .DoRTCalc(16,16,.f.)
        .w_pTIPCLI = iif(empty(.w_pCODCLI), " ", "C")
        .w_TIPFOR = "F"
        .w_DESART = .w_ELENCO.getvar("DESCRI")
        .w_TIPO = .w_ELENCO.getvar("TIPO")
        .w_PARAME = .w_ELENCO.getvar("MVPARAM")
        .w_MVSERIAL = .w_ELENCO.getvar("MVSERIAL")
        .w_OLCODODL = iif(.w_TIPO $ "ODL-OCL-ODA-IMP-ORD", .w_ELENCO.getvar("OLCODODL"), space(15))
        .w_PDSERIAL = iif(.w_TIPO="PDA", left(.w_ELENCO.getvar("OLCODODL"),10), space(10))
        .w_PDROWNUM = iif(.w_TIPO="ODM", .w_ELENCO.getvar("PDROWNUM"), 0)
          .DoRTCalc(26,26,.f.)
        .w_ARCODART = .w_ELENCO.getvar("CODART")
        .w_CODMAG = .w_ELENCO.getvar("CODMAG")
        .w_CDCOMM = NVL(.w_ELENCO.getvar("CODCOM"),"")
        .w_CODAZI = i_codazi
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_AZIENDA))
          .link_2_2('Full')
        endif
          .DoRTCalc(32,32,.f.)
        .w_TIPATT = "A"
        .w_FLVEACQ = "T"
        .w_TIPDOC = ' '
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_TIPDOC))
          .link_2_6('Full')
        endif
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999
          .DoRTCalc(39,39,.f.)
        .w_DATFIN = .w_DATINI
          .DoRTCalc(41,41,.f.)
        .w_DOCFIN = .w_DOCINI
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CODMAGA))
          .link_2_15('Full')
        endif
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODBUS))
          .link_2_16('Full')
        endif
        .w_FORSEL = ' '
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_FORSEL))
          .link_2_17('Full')
        endif
        .w_FORSEL = ' '
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_FORSEL))
          .link_2_18('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CODCOM))
          .link_2_19('Full')
        endif
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_CODATT))
          .link_2_20('Full')
        endif
        .w_TIPORD = 'T'
        .w_STAORD = 'T'
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_OLCODINI))
          .link_2_23('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_OLCODFIN))
          .link_2_24('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_COMODL))
          .link_2_25('Full')
        endif
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_DBCODINI))
          .link_2_26('Full')
        endif
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_DBCODFIN))
          .link_2_27('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_FAMAINI))
          .link_2_28('Full')
        endif
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_FAMAFIN))
          .link_2_29('Full')
        endif
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_GRUINI))
          .link_2_30('Full')
        endif
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_GRUFIN))
          .link_2_31('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CATINI))
          .link_2_32('Full')
        endif
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_CATFIN))
          .link_2_33('Full')
        endif
          .DoRTCalc(62,73,.f.)
        .w_SERIE2 = ''
          .DoRTCalc(75,75,.f.)
        .w_LLCINI = 0
        .w_LLCFIN = 999
          .DoRTCalc(78,78,.f.)
        .w_TIPCON = iif(.w_FLVEACQ='A','F',iif(.w_FLVEACQ='V','C',' '))
          .DoRTCalc(80,80,.f.)
        .w_FLAGSART = .F.
        .w_MVCODART = .w_ELENCO.getvar("CODART")
          .DoRTCalc(83,83,.f.)
        .w_SLCODVAR = .w_ELENCO.getvar("CODVAR")
          .DoRTCalc(85,85,.f.)
        .w_FLVEAC = 'A'
          .DoRTCalc(87,87,.f.)
        .w_CODICE = .w_ELENCO.getvar("CODICE")
      .oPgFrm.Page1.oPag.ELENCO.Calculate()
    endwith
    this.DoRTCalc(89,89,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_VISORD<>.w_VISORD.or. .o_VISDOC<>.w_VISDOC
            .w_APEVA = 'T'
        endif
        .DoRTCalc(14,16,.t.)
            .w_pTIPCLI = iif(empty(.w_pCODCLI), " ", "C")
        .DoRTCalc(18,18,.t.)
            .w_DESART = .w_ELENCO.getvar("DESCRI")
            .w_TIPO = .w_ELENCO.getvar("TIPO")
            .w_PARAME = .w_ELENCO.getvar("MVPARAM")
            .w_MVSERIAL = .w_ELENCO.getvar("MVSERIAL")
            .w_OLCODODL = iif(.w_TIPO $ "ODL-OCL-ODA-IMP-ORD", .w_ELENCO.getvar("OLCODODL"), space(15))
            .w_PDSERIAL = iif(.w_TIPO="PDA", left(.w_ELENCO.getvar("OLCODODL"),10), space(10))
            .w_PDROWNUM = iif(.w_TIPO="ODM", .w_ELENCO.getvar("PDROWNUM"), 0)
        .DoRTCalc(26,26,.t.)
            .w_ARCODART = .w_ELENCO.getvar("CODART")
            .w_CODMAG = .w_ELENCO.getvar("CODMAG")
            .w_CDCOMM = NVL(.w_ELENCO.getvar("CODCOM"),"")
        .DoRTCalc(30,30,.t.)
          .link_2_2('Full')
        .DoRTCalc(32,34,.t.)
        if .o_FLVEACQ<>.w_FLVEACQ
            .w_TIPDOC = ' '
          .link_2_6('Full')
        endif
        .DoRTCalc(36,39,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = .w_DATINI
        endif
        .DoRTCalc(41,41,.t.)
        if .o_DOCINI<>.w_DOCINI
            .w_DOCFIN = .w_DOCINI
        endif
        .DoRTCalc(43,44,.t.)
        if .o_FLVEACQ<>.w_FLVEACQ
            .w_FORSEL = ' '
          .link_2_17('Full')
        endif
        if .o_FLVEACQ<>.w_FLVEACQ
            .w_FORSEL = ' '
          .link_2_18('Full')
        endif
        .DoRTCalc(47,76,.t.)
        if .o_LLCINI<>.w_LLCINI
            .w_LLCFIN = 999
        endif
        .DoRTCalc(78,78,.t.)
        if .o_FLVEACQ<>.w_FLVEACQ
            .w_TIPCON = iif(.w_FLVEACQ='A','F',iif(.w_FLVEACQ='V','C',' '))
        endif
        .DoRTCalc(80,81,.t.)
            .w_MVCODART = .w_ELENCO.getvar("CODART")
        .DoRTCalc(83,83,.t.)
            .w_SLCODVAR = .w_ELENCO.getvar("CODVAR")
        .DoRTCalc(85,87,.t.)
            .w_CODICE = .w_ELENCO.getvar("CODICE")
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(89,89,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNSALZERO_1_14.enabled = this.oPgFrm.Page1.oPag.oNSALZERO_1_14.mCond()
    this.oPgFrm.Page1.oPag.oAPEVA_1_15.enabled = this.oPgFrm.Page1.oPag.oAPEVA_1_15.mCond()
    this.oPgFrm.Page2.oPag.oCODBUS_2_16.enabled = this.oPgFrm.Page2.oPag.oCODBUS_2_16.mCond()
    this.oPgFrm.Page2.oPag.oCODATT_2_20.enabled = this.oPgFrm.Page2.oPag.oCODATT_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page2.oPag.oFORSEL_2_17.visible=!this.oPgFrm.Page2.oPag.oFORSEL_2_17.mHide()
    this.oPgFrm.Page2.oPag.oFORSEL_2_18.visible=!this.oPgFrm.Page2.oPag.oFORSEL_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_70.visible=!this.oPgFrm.Page2.oPag.oStr_2_70.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ELENCO.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=pCODCOM
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_pCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_pCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_pCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_pCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_pCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'opCODCOM_1_4'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_pCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_pCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_pCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODICE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_pCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARSALCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_pCODICE))
          select ARCODART,ARDESART,ARSALCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_pCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_pCODICE)+"%");

            select ARCODART,ARDESART,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_pCODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'opCODICE_1_6'),i_cWhere,'',"Codici articolo",'gsmr_ztc.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARSALCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_pCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_pCODICE)
            select ARCODART,ARDESART,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODICE = NVL(_Link_.ARCODART,space(41))
      this.w_DESRIC = NVL(_Link_.ARDESART,space(40))
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_FLCOMM = NVL(_Link_.ARSALCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_pCODICE = space(41)
      endif
      this.w_DESRIC = space(40)
      this.w_CODART = space(20)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMM='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_pCODICE = space(41)
        this.w_DESRIC = space(40)
        this.w_CODART = space(20)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODCLI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_pCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_pCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_pCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'opCODCLI_1_7'),i_cWhere,'',"Clienti",'GSAR0ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_pCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_pCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_pCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZIENDA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_2_6'),i_cWhere,'',"Causali documenti",'GSMR_ZCD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAGA
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAGA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAGA)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAGA))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAGA)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAGA) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAGA_2_15'),i_cWhere,'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAGA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAGA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAGA)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAGA = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAGA = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAGA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUS
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUS)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_CODBUS))
          select BUCODAZI,BUCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUS)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUS) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUS_2_16'),i_cWhere,'',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUS);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_CODBUS)
            select BUCODAZI,BUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUS = NVL(_Link_.BUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_2_17'),i_cWhere,'',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");

          i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_2_18'),i_cWhere,'',"Conti",'gsmr_zcf.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',oSource.xKey(1))
            select ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANCODICE',this.w_FORSEL)
            select ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (looktab('CONTI','ANTIPCON','ANCODICE',.w_FORSEL) $ 'C-F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FORSEL = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_19'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_2_20'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODINI
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODINI_2_23'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLCODFIN
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLCODFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLCODFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLCODFIN_2_24'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLCODFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLCODFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLCODFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLCODFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMODL
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMODL)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMODL))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMODL)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMODL)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMODL)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMODL) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMODL_2_25'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMODL)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMODL = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOML = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMODL = space(15)
      endif
      this.w_DESCOML = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODINI
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DBCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DBCODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDBCODINI_2_26'),i_cWhere,'',"Codici di ricerca",'gsmr_ztc.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DBCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DBCODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESDISI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DBCODFIN
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DBCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DBCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DBCODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DBCODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DBCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDBCODFIN_2_27'),i_cWhere,'',"Codici di ricerca",'gsmr_ztc.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DBCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DBCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DBCODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DBCODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESDISF = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DBCODFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DBCODFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DBCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_2_28'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_2_29'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_2_30'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_2_31'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_2_32'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_2_33'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.opCODCOM_1_4.value==this.w_pCODCOM)
      this.oPgFrm.Page1.oPag.opCODCOM_1_4.value=this.w_pCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_5.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_5.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.opCODICE_1_6.value==this.w_pCODICE)
      this.oPgFrm.Page1.oPag.opCODICE_1_6.value=this.w_pCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.opCODCLI_1_7.value==this.w_pCODCLI)
      this.oPgFrm.Page1.oPag.opCODCLI_1_7.value=this.w_pCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.opDATINI_1_8.value==this.w_pDATINI)
      this.oPgFrm.Page1.oPag.opDATINI_1_8.value=this.w_pDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.opDATFIN_1_9.value==this.w_pDATFIN)
      this.oPgFrm.Page1.oPag.opDATFIN_1_9.value=this.w_pDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPOR_1_10.RadioValue()==this.w_IMPOR)
      this.oPgFrm.Page1.oPag.oIMPOR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISDOC_1_11.RadioValue()==this.w_VISDOC)
      this.oPgFrm.Page1.oPag.oVISDOC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISORD_1_12.RadioValue()==this.w_VISORD)
      this.oPgFrm.Page1.oPag.oVISORD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISSAL_1_13.RadioValue()==this.w_VISSAL)
      this.oPgFrm.Page1.oPag.oVISSAL_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNSALZERO_1_14.RadioValue()==this.w_NSALZERO)
      this.oPgFrm.Page1.oPag.oNSALZERO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAPEVA_1_15.RadioValue()==this.w_APEVA)
      this.oPgFrm.Page1.oPag.oAPEVA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIC_1_16.value==this.w_DESRIC)
      this.oPgFrm.Page1.oPag.oDESRIC_1_16.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_18.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_18.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_22.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_22.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMM_1_39.value==this.w_CDCOMM)
      this.oPgFrm.Page1.oPag.oCDCOMM_1_39.value=this.w_CDCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oFLVEACQ_2_5.RadioValue()==this.w_FLVEACQ)
      this.oPgFrm.Page2.oPag.oFLVEACQ_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDOC_2_6.value==this.w_TIPDOC)
      this.oPgFrm.Page2.oPag.oTIPDOC_2_6.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMINI_2_7.value==this.w_NUMINI)
      this.oPgFrm.Page2.oPag.oNUMINI_2_7.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE1_2_8.value==this.w_SERIE1)
      this.oPgFrm.Page2.oPag.oSERIE1_2_8.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMFIN_2_9.value==this.w_NUMFIN)
      this.oPgFrm.Page2.oPag.oNUMFIN_2_9.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_11.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_11.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_12.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDOCINI_2_13.value==this.w_DOCINI)
      this.oPgFrm.Page2.oPag.oDOCINI_2_13.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDOCFIN_2_14.value==this.w_DOCFIN)
      this.oPgFrm.Page2.oPag.oDOCFIN_2_14.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAGA_2_15.value==this.w_CODMAGA)
      this.oPgFrm.Page2.oPag.oCODMAGA_2_15.value=this.w_CODMAGA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODBUS_2_16.value==this.w_CODBUS)
      this.oPgFrm.Page2.oPag.oCODBUS_2_16.value=this.w_CODBUS
    endif
    if not(this.oPgFrm.Page2.oPag.oFORSEL_2_17.value==this.w_FORSEL)
      this.oPgFrm.Page2.oPag.oFORSEL_2_17.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oFORSEL_2_18.value==this.w_FORSEL)
      this.oPgFrm.Page2.oPag.oFORSEL_2_18.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_19.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_19.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT_2_20.value==this.w_CODATT)
      this.oPgFrm.Page2.oPag.oCODATT_2_20.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPORD_2_21.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page2.oPag.oTIPORD_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSTAORD_2_22.RadioValue()==this.w_STAORD)
      this.oPgFrm.Page2.oPag.oSTAORD_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOLCODINI_2_23.value==this.w_OLCODINI)
      this.oPgFrm.Page2.oPag.oOLCODINI_2_23.value=this.w_OLCODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oOLCODFIN_2_24.value==this.w_OLCODFIN)
      this.oPgFrm.Page2.oPag.oOLCODFIN_2_24.value=this.w_OLCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMODL_2_25.value==this.w_COMODL)
      this.oPgFrm.Page2.oPag.oCOMODL_2_25.value=this.w_COMODL
    endif
    if not(this.oPgFrm.Page2.oPag.oDBCODINI_2_26.value==this.w_DBCODINI)
      this.oPgFrm.Page2.oPag.oDBCODINI_2_26.value=this.w_DBCODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDBCODFIN_2_27.value==this.w_DBCODFIN)
      this.oPgFrm.Page2.oPag.oDBCODFIN_2_27.value=this.w_DBCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMAINI_2_28.value==this.w_FAMAINI)
      this.oPgFrm.Page2.oPag.oFAMAINI_2_28.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMAFIN_2_29.value==this.w_FAMAFIN)
      this.oPgFrm.Page2.oPag.oFAMAFIN_2_29.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUINI_2_30.value==this.w_GRUINI)
      this.oPgFrm.Page2.oPag.oGRUINI_2_30.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUFIN_2_31.value==this.w_GRUFIN)
      this.oPgFrm.Page2.oPag.oGRUFIN_2_31.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCATINI_2_32.value==this.w_CATINI)
      this.oPgFrm.Page2.oPag.oCATINI_2_32.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCATFIN_2_33.value==this.w_CATFIN)
      this.oPgFrm.Page2.oPag.oCATFIN_2_33.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_38.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_38.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_40.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_40.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_41.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_41.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUI_2_42.value==this.w_DESGRUI)
      this.oPgFrm.Page2.oPag.oDESGRUI_2_42.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_43.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_43.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAF_2_47.value==this.w_DESFAMAF)
      this.oPgFrm.Page2.oPag.oDESFAMAF_2_47.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUF_2_48.value==this.w_DESGRUF)
      this.oPgFrm.Page2.oPag.oDESGRUF_2_48.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATF_2_49.value==this.w_DESCATF)
      this.oPgFrm.Page2.oPag.oDESCATF_2_49.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDISI_2_53.value==this.w_DESDISI)
      this.oPgFrm.Page2.oPag.oDESDISI_2_53.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDISF_2_54.value==this.w_DESDISF)
      this.oPgFrm.Page2.oPag.oDESDISF_2_54.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_60.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_60.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC_2_62.value==this.w_DESDOC)
      this.oPgFrm.Page2.oPag.oDESDOC_2_62.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE2_2_73.value==this.w_SERIE2)
      this.oPgFrm.Page2.oPag.oSERIE2_2_73.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOML_2_78.value==this.w_DESCOML)
      this.oPgFrm.Page2.oPag.oDESCOML_2_78.value=this.w_DESCOML
    endif
    if not(this.oPgFrm.Page2.oPag.oLLCINI_2_81.value==this.w_LLCINI)
      this.oPgFrm.Page2.oPag.oLLCINI_2_81.value=this.w_LLCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oLLCFIN_2_82.value==this.w_LLCFIN)
      this.oPgFrm.Page2.oPag.oLLCFIN_2_82.value=this.w_LLCFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_FLCOMM='S')  and not(empty(.w_pCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opCODICE_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_pDATFIN) or (.w_pDATINI<=.w_pDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opDATINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_pDATINI) or (.w_pDATINI<=.w_pDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opDATFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMINI_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE1_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATINI_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFIN_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDOCINI_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDOCFIN_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(.w_FLVEACQ='T')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORSEL_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) and (looktab('CONTI','ANTIPCON','ANCODICE',.w_FORSEL) $ 'C-F'))  and not(.w_FLVEACQ<>'T')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORSEL_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODFIN))  and not(empty(.w_OLCODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLCODINI_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OLCODINI<=.w_OLCODFIN or empty(.w_OLCODINI))  and not(empty(.w_OLCODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLCODFIN_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODFIN))  and not(empty(.w_DBCODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDBCODINI_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DBCODINI<=.w_DBCODFIN or empty(.w_DBCODINI))  and not(empty(.w_DBCODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDBCODFIN_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMAINI_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAMAFIN_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUINI_2_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUFIN_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATINI_2_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATFIN_2_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE2_2_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLLCINI_2_81.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LLCINI<=.w_LLCFIN)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLLCFIN_2_82.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VISDOC = this.w_VISDOC
    this.o_VISORD = this.w_VISORD
    this.o_FLVEACQ = this.w_FLVEACQ
    this.o_DATINI = this.w_DATINI
    this.o_DOCINI = this.w_DOCINI
    this.o_LLCINI = this.w_LLCINI
    return

enddefine

* --- Define pages as container
define class tgsmr_ktcPag1 as StdContainer
  Width  = 786
  height = 537
  stdWidth  = 786
  stdheight = 537
  resizeXpos=539
  resizeYpos=229
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object opCODCOM_1_4 as StdField with uid="YLQVWTCUFO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_pCODCOM", cQueryName = "pCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 24129802,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=123, Top=7, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_pCODCOM"

  func opCODCOM_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODCOM_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODCOM_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'opCODCOM_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDESCAN_1_5 as StdField with uid="PJHERXXTYJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43053514,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=278, Top=7, InputMask=replicate('X',30)

  add object opCODICE_1_6 as StdField with uid="WXSMCRBREQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_pCODICE", cQueryName = "pCODICE",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 49270518,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=123, Top=30, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_pCODICE"

  func opCODICE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODICE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODICE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'opCODICE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici articolo",'gsmr_ztc.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object opCODCLI_1_7 as StdField with uid="HRPIORURYL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_pCODCLI", cQueryName = "pCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 74461450,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=123, Top=53, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_pCODCLI"

  func opCODCLI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODCLI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODCLI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'opCODCLI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti",'GSAR0ACL.CONTI_VZM',this.parent.oContained
  endproc

  add object opDATINI_1_8 as StdField with uid="GDNIVEXSZA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_pDATINI", cQueryName = "pDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data evasione di inizio selezione",;
    HelpContextID = 33624074,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=123, Top=76

  func opDATINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_pDATFIN) or (.w_pDATINI<=.w_pDATFIN))
    endwith
    return bRes
  endfunc

  add object opDATFIN_1_9 as StdField with uid="FYJFDJERZO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_pDATFIN", cQueryName = "pDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data evasione di fine selezione",;
    HelpContextID = 120655882,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=288, Top=76

  func opDATFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_pDATINI) or (.w_pDATINI<=.w_pDATFIN))
    endwith
    return bRes
  endfunc


  add object oIMPOR_1_10 as StdCombo with uid="KLTXNPKPJS",rtseq=8,rtrep=.f.,left=525,top=76,width=123,height=21;
    , ToolTipText = "Selezione impegnato/ordinato";
    , HelpContextID = 259332474;
    , cFormVar="w_IMPOR",RowSource=""+"Impegnato,"+"Ordinato,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIMPOR_1_10.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oIMPOR_1_10.GetRadio()
    this.Parent.oContained.w_IMPOR = this.RadioValue()
    return .t.
  endfunc

  func oIMPOR_1_10.SetRadio()
    this.Parent.oContained.w_IMPOR=trim(this.Parent.oContained.w_IMPOR)
    this.value = ;
      iif(this.Parent.oContained.w_IMPOR=='I',1,;
      iif(this.Parent.oContained.w_IMPOR=='O',2,;
      iif(this.Parent.oContained.w_IMPOR=='T',3,;
      0)))
  endfunc

  add object oVISDOC_1_11 as StdCheck with uid="UBQQLZYYOS",rtseq=9,rtrep=.f.,left=33, top=104, caption="Documenti",;
    ToolTipText = "Selezione documenti",;
    HelpContextID = 212855978,;
    cFormVar="w_VISDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISDOC_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISDOC_1_11.GetRadio()
    this.Parent.oContained.w_VISDOC = this.RadioValue()
    return .t.
  endfunc

  func oVISDOC_1_11.SetRadio()
    this.Parent.oContained.w_VISDOC=trim(this.Parent.oContained.w_VISDOC)
    this.value = ;
      iif(this.Parent.oContained.w_VISDOC=='S',1,;
      0)
  endfunc

  add object oVISORD_1_12 as StdCheck with uid="BCLRPDFQUP",rtseq=10,rtrep=.f.,left=158, top=104, caption="Ordini",;
    ToolTipText = "Selezione ODL, OCL, ODA",;
    HelpContextID = 192212138,;
    cFormVar="w_VISORD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISORD_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISORD_1_12.GetRadio()
    this.Parent.oContained.w_VISORD = this.RadioValue()
    return .t.
  endfunc

  func oVISORD_1_12.SetRadio()
    this.Parent.oContained.w_VISORD=trim(this.Parent.oContained.w_VISORD)
    this.value = ;
      iif(this.Parent.oContained.w_VISORD=='S',1,;
      0)
  endfunc

  add object oVISSAL_1_13 as StdCheck with uid="HOJBLQNPBP",rtseq=11,rtrep=.f.,left=273, top=104, caption="Saldi",;
    ToolTipText = "Selezione saldi articolo per commessa",;
    HelpContextID = 75558058,;
    cFormVar="w_VISSAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISSAL_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISSAL_1_13.GetRadio()
    this.Parent.oContained.w_VISSAL = this.RadioValue()
    return .t.
  endfunc

  func oVISSAL_1_13.SetRadio()
    this.Parent.oContained.w_VISSAL=trim(this.Parent.oContained.w_VISSAL)
    this.value = ;
      iif(this.Parent.oContained.w_VISSAL=='S',1,;
      0)
  endfunc

  add object oNSALZERO_1_14 as StdCheck with uid="SXVQFBKZCG",rtseq=12,rtrep=.f.,left=417, top=104, caption="Nascondi saldi a zero",;
    ToolTipText = "Visualizza esclusivamente saldi con esistenza diversa da 0",;
    HelpContextID = 101121317,;
    cFormVar="w_NSALZERO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNSALZERO_1_14.RadioValue()
    return(iif(this.value =1,"S",;
    "X"))
  endfunc
  func oNSALZERO_1_14.GetRadio()
    this.Parent.oContained.w_NSALZERO = this.RadioValue()
    return .t.
  endfunc

  func oNSALZERO_1_14.SetRadio()
    this.Parent.oContained.w_NSALZERO=trim(this.Parent.oContained.w_NSALZERO)
    this.value = ;
      iif(this.Parent.oContained.w_NSALZERO=="S",1,;
      0)
  endfunc

  func oNSALZERO_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VISSAL='S')
    endwith
   endif
  endfunc


  add object oAPEVA_1_15 as StdCombo with uid="YFSSOKBUBD",rtseq=13,rtrep=.f.,left=573,top=104,width=75,height=21;
    , ToolTipText = "Selezione ordini aperti ed evasi";
    , HelpContextID = 8308474;
    , cFormVar="w_APEVA",RowSource=""+"Tutti,"+"Aperti,"+"Evasi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAPEVA_1_15.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oAPEVA_1_15.GetRadio()
    this.Parent.oContained.w_APEVA = this.RadioValue()
    return .t.
  endfunc

  func oAPEVA_1_15.SetRadio()
    this.Parent.oContained.w_APEVA=trim(this.Parent.oContained.w_APEVA)
    this.value = ;
      iif(this.Parent.oContained.w_APEVA=='T',1,;
      iif(this.Parent.oContained.w_APEVA=='A',2,;
      iif(this.Parent.oContained.w_APEVA=='E',3,;
      0)))
  endfunc

  func oAPEVA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VISDOC='S' or .w_VISORD='S')
    endwith
   endif
  endfunc

  add object oDESRIC_1_16 as StdField with uid="YEFXVVACGA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218231242,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=278, Top=30, InputMask=replicate('X',40)

  add object oDESCLI_1_18 as StdField with uid="LGMOHBWFDT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115405258,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=278, Top=53, InputMask=replicate('X',40)


  add object oBtn_1_21 as StdButton with uid="FTEUPABWGN",left=730, top=76, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 94515990;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESART_1_22 as StdField with uid="SPNPVQNITU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 75304502,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=275, Top=488, InputMask=replicate('X',41)


  add object oBtn_1_30 as StdButton with uid="XJVYSVMPXO",left=7, top=489, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 144556230;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MVSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_MVSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="SRAVMDUWII",left=7, top=489, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza l'ODL/OCL/ODA della riga selezionata";
    , HelpContextID = 82076186;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSCO_BOR(this.Parent.oContained,.w_OLCODODL,nvl(.w_PDROWNUM,0))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OLCODODL))
      endwith
    endif
  endfunc

  func oBtn_1_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OLCODODL))
     endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="VSLVHZSIND",left=7, top=489, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il saldo associato alla riga selezionata";
    , HelpContextID = 233719002;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        do GSMA_KVC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPO='MAG')
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPO<>'MAG')
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="HPTWQLNNJS",left=65, top=489, width=48,height=45,;
    CpPicture="BMP\ABBINA.bmp", caption="", nPag=1;
    , ToolTipText = "Apre la visualizzazione del Pegging";
    , HelpContextID = 267096330;
    , Caption='\<Pegging';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSMR_BTC(this.Parent.oContained,"PE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_MVCODART))
      endwith
    endif
  endfunc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_MVCODART))
     endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="LTUDPJPPGY",left=123, top=489, width=48,height=45,;
    CpPicture="BMP\ABBINA.bmp", caption="", nPag=1;
    , ToolTipText = "Apre la visualizzazione dei movimenti di magazzino";
    , HelpContextID = 219244602;
    , Caption='\<Visualizza Movimenti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSMR_BTC(this.Parent.oContained,"MM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_MVCODART))
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_MVCODART))
     endwith
    endif
  endfunc

  add object oCDCOMM_1_39 as StdField with uid="KSKNTHPXUX",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CDCOMM", cQueryName = "CDCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 46527194,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=275, Top=512, InputMask=replicate('X',15)


  add object oBtn_1_40 as StdButton with uid="VIALZUPESJ",left=674, top=489, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 59383334;
    , tabStop=.f.,Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      vx_exec("..\COLA\EXE\QUERY\GSMR4KTC.VQR",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_41 as StdButton with uid="IPBTJCBBGJ",left=729, top=489, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75088826;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ELENCO as cp_zoombox with uid="TTPLCLMYLW",left=7, top=134, width=775,height=349,;
    caption='ELENCO',;
   bGlobalFont=.t.,;
    cZoomFile="GSMR_KTC",bOptions=.t.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="ODL_MAST",bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 23513786

  add object oStr_1_1 as StdString with uid="XCTGVZYJLK",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=109, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="MJWHEOMANN",Visible=.t., Left=10, Top=32,;
    Alignment=1, Width=109, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="PVSBNWZPLZ",Visible=.t., Left=213, Top=490,;
    Alignment=1, Width=59, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="MFXBUDKSWC",Visible=.t., Left=182, Top=514,;
    Alignment=1, Width=90, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="SPLPETRBCZ",Visible=.t., Left=408, Top=78,;
    Alignment=1, Width=112, Height=18,;
    Caption="Impegnato/Ordinato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="FWXRPONXZD",Visible=.t., Left=32, Top=55,;
    Alignment=1, Width=87, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="AXZWKQBIUS",Visible=.t., Left=41, Top=78,;
    Alignment=1, Width=78, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="XUUNKNNRZR",Visible=.t., Left=207, Top=78,;
    Alignment=1, Width=78, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsmr_ktcPag2 as StdContainer
  Width  = 786
  height = 537
  stdWidth  = 786
  stdheight = 537
  resizeXpos=296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEACQ_2_5 as StdCombo with uid="VNKCJROECU",rtseq=34,rtrep=.f.,left=111,top=37,width=74,height=21;
    , ToolTipText = "Selezione ciclo (vendite/acquisti)";
    , HelpContextID = 40977750;
    , cFormVar="w_FLVEACQ",RowSource=""+"Acquisti,"+"Vendite,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLVEACQ_2_5.RadioValue()
    return(iif(this.value =1,"A",;
    iif(this.value =2,"V",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oFLVEACQ_2_5.GetRadio()
    this.Parent.oContained.w_FLVEACQ = this.RadioValue()
    return .t.
  endfunc

  func oFLVEACQ_2_5.SetRadio()
    this.Parent.oContained.w_FLVEACQ=trim(this.Parent.oContained.w_FLVEACQ)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEACQ=="A",1,;
      iif(this.Parent.oContained.w_FLVEACQ=="V",2,;
      iif(this.Parent.oContained.w_FLVEACQ=="T",3,;
      0)))
  endfunc

  add object oTIPDOC_2_6 as StdField with uid="FYFYYJENFJ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento",;
    HelpContextID = 212868298,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=327, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documenti",'GSMR_ZCD.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oNUMINI_2_7 as StdField with uid="IKHMXRTVNM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 112935210,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=61, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oSERIE1_2_8 as StdField with uid="CPLEKOYGGO",rtseq=37,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Numero del documento iniziale selezionato",;
    HelpContextID = 256573658,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=179, Top=61, InputMask=replicate('X',2)

  func oSERIE1_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_2_9 as StdField with uid="ZQAMWANEZG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 34488618,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=111, Top=84, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oDATINI_2_11 as StdField with uid="QMGDPWFMND",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 112911818,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=327, Top=61

  func oDATINI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_2_12 as StdField with uid="VRSTSJQFDH",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 34465226,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=327, Top=84

  func oDATFIN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDOCINI_2_13 as StdField with uid="NLSHJRFBFX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 112977866,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=526, Top=61

  func oDOCINI_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_2_14 as StdField with uid="WKYDEMVJUZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 34531274,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=526, Top=84

  func oDOCFIN_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oCODMAGA_2_15 as StdField with uid="WWAXPPIMPZ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODMAGA", cQueryName = "CODMAGA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 108537894,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=720, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAGA"

  func oCODMAGA_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAGA_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAGA_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAGA_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'GSDB_SCG.MAGAZZIN_VZM',this.parent.oContained
  endproc

  add object oCODBUS_2_16 as StdField with uid="GBEJIHWQCD",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODBUS", cQueryName = "CODBUS",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit",;
    HelpContextID = 61679654,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=720, Top=84, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUS"

  func oCODBUS_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_gestbu $ 'ET'))
    endwith
   endif
  endfunc

  func oCODBUS_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUS_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUS_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUS_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Business Unit",'',this.parent.oContained
  endproc

  add object oFORSEL_2_17 as StdField with uid="GRXGWWUYUW",rtseq=45,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 71366570,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_2_17.mHide()
    with this.Parent.oContained
      return (.w_FLVEACQ='T')
    endwith
  endfunc

  func oFORSEL_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti",'',this.parent.oContained
  endproc

  add object oFORSEL_2_18 as StdField with uid="GVTZDQGQGN",rtseq=46,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice conto",;
    HelpContextID = 71366570,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=107, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANCODICE", oKey_1_2="this.w_FORSEL"

  func oFORSEL_2_18.mHide()
    with this.Parent.oContained
      return (.w_FLVEACQ<>'T')
    endwith
  endfunc

  proc oFORSEL_2_18.mAfter
    with this.Parent.oContained
      .w_FORSEL=CALCZER(.w_FORSEL, 'CONTI')
    endwith
  endproc

  func oFORSEL_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CONTI','*','ANCODICE',cp_AbsName(this.parent,'oFORSEL_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti",'gsmr_zcf.CONTI_VZM',this.parent.oContained
  endproc

  add object oCODCOM_2_19 as StdField with uid="EQLQPPQMYT",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 45209562,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=130, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_2_20 as StdField with uid="CMRHBCMAKU",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 77342758,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=153, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc


  add object oTIPORD_2_21 as StdCombo with uid="DKTMCCYJQZ",rtseq=49,rtrep=.f.,left=111,top=211,width=129,height=21;
    , HelpContextID = 192224458;
    , cFormVar="w_TIPORD",RowSource=""+"OCL,"+"ODL,"+"ODA,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPORD_2_21.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'I',;
    iif(this.value =3,'E',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oTIPORD_2_21.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_2_21.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='L',1,;
      iif(this.Parent.oContained.w_TIPORD=='I',2,;
      iif(this.Parent.oContained.w_TIPORD=='E',3,;
      iif(this.Parent.oContained.w_TIPORD=='T',4,;
      0))))
  endfunc


  add object oSTAORD_2_22 as StdCombo with uid="EDIOOKNJHC",rtseq=50,rtrep=.f.,left=349,top=211,width=129,height=21;
    , HelpContextID = 192283098;
    , cFormVar="w_STAORD",RowSource=""+"Suggeriti MRP,"+"Pianificati/Da ordinare,"+"Lanciati/Ordinati,"+"Finiti,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSTAORD_2_22.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'P',;
    iif(this.value =3,'L',;
    iif(this.value =4,'F',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oSTAORD_2_22.GetRadio()
    this.Parent.oContained.w_STAORD = this.RadioValue()
    return .t.
  endfunc

  func oSTAORD_2_22.SetRadio()
    this.Parent.oContained.w_STAORD=trim(this.Parent.oContained.w_STAORD)
    this.value = ;
      iif(this.Parent.oContained.w_STAORD=='M',1,;
      iif(this.Parent.oContained.w_STAORD=='P',2,;
      iif(this.Parent.oContained.w_STAORD=='L',3,;
      iif(this.Parent.oContained.w_STAORD=='F',4,;
      iif(this.Parent.oContained.w_STAORD=='T',5,;
      0)))))
  endfunc

  add object oOLCODINI_2_23 as StdField with uid="QBFYXJIXEG",rtseq=51,rtrep=.f.,;
    cFormVar = "w_OLCODINI", cQueryName = "OLCODINI",nZero=15,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL di inizio selezione",;
    HelpContextID = 123070929,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=236, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODINI"

  func oOLCODINI_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODINI_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODINI_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODINI_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oOLCODFIN_2_24 as StdField with uid="RYBOKFOTRV",rtseq=52,rtrep=.f.,;
    cFormVar = "w_OLCODFIN", cQueryName = "OLCODFIN",nZero=15,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL/OCL di inizio selezione",;
    HelpContextID = 173402572,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=349, Top=236, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLCODFIN"

  proc oOLCODFIN_2_24.mDefault
    with this.Parent.oContained
      if empty(.w_OLCODFIN)
        .w_OLCODFIN = .w_OLCODINI
      endif
    endwith
  endproc

  func oOLCODFIN_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLCODFIN_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLCODFIN_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLCODFIN_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCOMODL_2_25 as StdField with uid="JDHYNCICPU",rtseq=53,rtrep=.f.,;
    cFormVar = "w_COMODL", cQueryName = "COMODL",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa ordine",;
    HelpContextID = 72697818,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=111, Top=260, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMODL"

  func oCOMODL_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMODL_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMODL_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMODL_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDBCODINI_2_26 as StdField with uid="EGVKVVQLEU",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DBCODINI", cQueryName = "DBCODINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di inizio selezione",;
    HelpContextID = 123073665,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=112, Top=320, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DBCODINI"

  func oDBCODINI_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODINI_2_26.ecpDrop(oSource)
    this.Parent.oContained.link_2_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODINI_2_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDBCODINI_2_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'gsmr_ztc.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDBCODFIN_2_27 as StdField with uid="VCEEWLYQYH",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DBCODFIN", cQueryName = "DBCODFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo di fine selezione",;
    HelpContextID = 173405308,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=112, Top=343, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DBCODFIN"

  proc oDBCODFIN_2_27.mDefault
    with this.Parent.oContained
      if empty(.w_DBCODFIN)
        .w_DBCODFIN = .w_DBCODINI
      endif
    endwith
  endproc

  func oDBCODFIN_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oDBCODFIN_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDBCODFIN_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDBCODFIN_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'gsmr_ztc.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oFAMAINI_2_28 as StdField with uid="BAUZFWPTWC",rtseq=56,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 34821546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=366, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_2_29 as StdField with uid="GHDDKOKVIG",rtseq=57,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 121853354,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=494, Top=366, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  proc oFAMAFIN_2_29.mDefault
    with this.Parent.oContained
      if empty(.w_FAMAFIN)
        .w_FAMAFIN = .w_FAMAINI
      endif
    endwith
  endproc

  func oFAMAFIN_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_2_30 as StdField with uid="VYQYSTJFCH",rtseq=58,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 112903322,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=389, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_2_31 as StdField with uid="BKTBROMCMD",rtseq=59,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 34456730,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=494, Top=389, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  proc oGRUFIN_2_31.mDefault
    with this.Parent.oContained
      if empty(.w_GRUFIN)
        .w_GRUFIN = .w_GRUINI
      endif
    endwith
  endproc

  func oGRUFIN_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_2_32 as StdField with uid="WQNLFQEXDD",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 112911834,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=412, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_2_33 as StdField with uid="WHQQEZYTLE",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 34465242,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=494, Top=412, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  proc oCATFIN_2_33.mDefault
    with this.Parent.oContained
      if empty(.w_CATFIN)
        .w_CATFIN = .w_CATINI
      endif
    endwith
  endproc

  func oCATFIN_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oDESCOM_2_38 as StdField with uid="LMOPSEOVLR",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 45150666,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=246, Top=130, InputMask=replicate('X',30)

  add object oDESATT_2_40 as StdField with uid="JRUPUPZCWV",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 77401654,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=246, Top=153, InputMask=replicate('X',30)

  add object oDESFAMAI_2_41 as StdField with uid="AOYBYAMKXI",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 208801407,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=178, Top=366, InputMask=replicate('X',35)

  add object oDESGRUI_2_42 as StdField with uid="ZABORREMDH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 175960522,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=178, Top=389, InputMask=replicate('X',35)

  add object oDESCATI_2_43 as StdField with uid="ZFWYARIWSP",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 210825674,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=178, Top=412, InputMask=replicate('X',35)

  add object oDESFAMAF_2_47 as StdField with uid="WOCDSQXKSA",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 208801404,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=559, Top=366, InputMask=replicate('X',35)

  add object oDESGRUF_2_48 as StdField with uid="PYGQUHDJMJ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92474934,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=559, Top=389, InputMask=replicate('X',35)

  add object oDESCATF_2_49 as StdField with uid="SXSGGSHCII",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57609782,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=559, Top=412, InputMask=replicate('X',35)

  add object oDESDISI_2_53 as StdField with uid="QSEMNVVIFP",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219148746,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=273, Top=320, InputMask=replicate('X',40)

  add object oDESDISF_2_54 as StdField with uid="BLVZKOKGVG",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49286710,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=273, Top=343, InputMask=replicate('X',40)

  add object oDESFOR_2_60 as StdField with uid="RJLIHNJXRO",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38932022,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=246, Top=107, InputMask=replicate('X',40)

  add object oDESDOC_2_62 as StdField with uid="RMROTHRADO",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212857290,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=401, Top=38, InputMask=replicate('X',35)

  add object oSERIE2_2_73 as StdField with uid="DHTTXQQJBV",rtseq=74,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Numero del documento finale selezionato",;
    HelpContextID = 239796442,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=179, Top=84, InputMask=replicate('X',2)

  func oSERIE2_2_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDESCOML_2_78 as StdField with uid="PSJFTRZIYY",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESCOML", cQueryName = "DESCOML",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 45150666,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=245, Top=260, InputMask=replicate('X',30)

  add object oLLCINI_2_81 as StdField with uid="RKYWPPNDHG",rtseq=76,rtrep=.f.,;
    cFormVar = "w_LLCINI", cQueryName = "LLCINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di inizio selezione",;
    HelpContextID = 112978506,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=112, Top=435, cSayPict='"999"', cGetPict='"999"'

  func oLLCINI_2_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)
    endwith
    return bRes
  endfunc

  add object oLLCFIN_2_82 as StdField with uid="ACQHHCIYYT",rtseq=77,rtrep=.f.,;
    cFormVar = "w_LLCFIN", cQueryName = "LLCFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di fine selezione",;
    HelpContextID = 34531914,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=235, Top=435, cSayPict='"999"', cGetPict='"999"'

  func oLLCFIN_2_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN)
    endwith
    return bRes
  endfunc

  add object oStr_2_10 as StdString with uid="CGEEJWMTRL",Visible=.t., Left=172, Top=84,;
    Alignment=0, Width=5, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="XRXCRISBDD",Visible=.t., Left=279, Top=38,;
    Alignment=1, Width=45, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="OLHXGJTWFV",Visible=.t., Left=44, Top=320,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="IEHZXMOABQ",Visible=.t., Left=53, Top=343,;
    Alignment=1, Width=55, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="BKQWSHAOGE",Visible=.t., Left=39, Top=130,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="QLYZPBJZIB",Visible=.t., Left=55, Top=153,;
    Alignment=1, Width=53, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=38, Top=366,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=25, Top=389,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=18, Top=412,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="BETNLUNOYI",Visible=.t., Left=421, Top=366,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=404, Top=389,;
    Alignment=1, Width=87, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=411, Top=412,;
    Alignment=1, Width=80, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="ILMDRJPSBA",Visible=.t., Left=4, Top=12,;
    Alignment=0, Width=152, Height=18,;
    Caption="Selezioni documenti"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="PEISJRCHUW",Visible=.t., Left=4, Top=295,;
    Alignment=0, Width=152, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=37, Top=107,;
    Alignment=1, Width=71, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (.w_FLVEACQ<>'A')
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="DLAOKFKNHU",Visible=.t., Left=63, Top=38,;
    Alignment=1, Width=45, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="HRQFQNITXV",Visible=.t., Left=236, Top=61,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="DNNAWKFMZY",Visible=.t., Left=250, Top=84,;
    Alignment=1, Width=74, Height=15,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="PYKVJJBJUN",Visible=.t., Left=435, Top=61,;
    Alignment=1, Width=88, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_66 as StdString with uid="BGHQBDDCZG",Visible=.t., Left=450, Top=84,;
    Alignment=1, Width=73, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_67 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=651, Top=61,;
    Alignment=1, Width=66, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_68 as StdString with uid="ZCPERDUXTM",Visible=.t., Left=634, Top=84,;
    Alignment=1, Width=83, Height=15,;
    Caption="Business Unit:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="UTVCTDGVMU",Visible=.t., Left=37, Top=107,;
    Alignment=1, Width=71, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (.w_FLVEACQ<>'V')
    endwith
  endfunc

  add object oStr_2_70 as StdString with uid="UZSDVBSZPX",Visible=.t., Left=5, Top=107,;
    Alignment=1, Width=103, Height=15,;
    Caption="Cliente\fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_70.mHide()
    with this.Parent.oContained
      return (.w_FLVEACQ<>'T')
    endwith
  endfunc

  add object oStr_2_71 as StdString with uid="BKVUFSLWMR",Visible=.t., Left=18, Top=236,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_72 as StdString with uid="HGANHIHTKB",Visible=.t., Left=258, Top=236,;
    Alignment=1, Width=90, Height=15,;
    Caption="A ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="VQZWRBIBXU",Visible=.t., Left=13, Top=61,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_75 as StdString with uid="PMVZWORABY",Visible=.t., Left=22, Top=84,;
    Alignment=1, Width=86, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="UKWJKYAMUK",Visible=.t., Left=172, Top=61,;
    Alignment=0, Width=5, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="HWGEPIQTHQ",Visible=.t., Left=39, Top=260,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="AWXIFQTAIY",Visible=.t., Left=4, Top=188,;
    Alignment=0, Width=162, Height=18,;
    Caption="Selezioni ordini di lavorazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_83 as StdString with uid="GKIMPOMLOZ",Visible=.t., Left=62, Top=435,;
    Alignment=1, Width=45, Height=15,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_84 as StdString with uid="EUAAIOZOQT",Visible=.t., Left=185, Top=435,;
    Alignment=1, Width=45, Height=15,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="UQBOKJCDTT",Visible=.t., Left=18, Top=213,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="GLLKRISBEM",Visible=.t., Left=258, Top=213,;
    Alignment=1, Width=90, Height=18,;
    Caption="Stato ordine:"  ;
  , bGlobalFont=.t.

  add object oBox_2_56 as StdBox with uid="OTTRKDEJWA",left=4, top=31, width=779,height=1

  add object oBox_2_58 as StdBox with uid="BQDUBJBORW",left=4, top=314, width=779,height=1

  add object oBox_2_80 as StdBox with uid="YSBHQDJAGS",left=4, top=207, width=779,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_ktc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
