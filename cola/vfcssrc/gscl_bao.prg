* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscl_bao                                                        *
*              Abbinamento OCL                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_960]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-27                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscl_bao",oParentObject)
return(i_retval)

define class tgscl_bao as StdBatch
  * --- Local variables
  w_OLTSTATO = space(1)
  w_OLTPROVE = space(1)
  w_OLTUNMIS = space(3)
  w_OLTQTODL = 0
  w_OLTQTOD1 = 0
  w_OLTQTSAL = 0
  w_OLTDTMPS = ctod("  /  /  ")
  w_OLTDINRIC = ctod("  /  /  ")
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLTCODIC = space(20)
  w_OLTCOART = space(20)
  w_OLTCOMAG = space(5)
  w_OLTCAMAG = space(5)
  w_OLTKEYSA = space(20)
  w_OLTEMLAV = 0
  w_OLTLEMPS = 0
  w_OLTFLORD = space(1)
  w_OLTFLIMP = space(1)
  w_OLTFLEVA = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_AGRUMER = space(5)
  w_ATIPGES = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_LOTRIO = 0
  w_LOTECO = 0
  w_LOTMED = 0
  w_LEAVAR = 0
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_SALCOM = space(1)
  w_OLTCOMME = space(15)
  w_CPROWNUM = 0
  w_OLCODICE = space(20)
  w_OLDATRIC = ctod("  /  /  ")
  w_OLUNIMIS = space(3)
  w_OLQTAMOV = 0
  w_OLQTAUM1 = 0
  w_OLFLEVAS = space(1)
  w_OLQTAEVA = 0
  w_OLQTAEV1 = 0
  w_OLQTASAL = 0
  w_OLFLPREV = space(1)
  w_OLTIPPRE = space(1)
  w_OLCAUMAG = space(5)
  w_OLFLORDI = space(1)
  w_OLFLIMPE = space(1)
  w_OLKEYSAL = space(20)
  w_OLCODMAG = space(5)
  w_OLEVAAUT = space(1)
  w_OLDCAU = space(5)
  w_OQTSAL = 0
  TmpC = space(100)
  TmpN = 0
  TmpL = .f.
  TmpD = ctod("  /  /  ")
  TmpN1 = 0
  w_Continua = .f.
  w_TROV = .f.
  w_PRCOSSTA = 0
  w_VACAOVAL = 0
  w_VADECTOT = 0
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OP = space(1)
  w_MOLT = 0
  w_OP3 = space(1)
  w_MOLT3 = 0
  w_OLDLEADTIME = 0
  w_QTALOT = 0
  w_QTAMIN = 0
  w_GIOAPP = 0
  w_CODART = space(20)
  w_CODGRU = space(5)
  w_CODFOR = space(15)
  w_OK0 = .f.
  w_OK1 = .f.
  w_QTALOT0 = 0
  w_QTAMIN0 = 0
  w_GIOAPP0 = 0
  w_QTALOT1 = 0
  w_QTAMIN1 = 0
  w_GIOAPP1 = 0
  w_APPO = space(15)
  w_MAGWIP = space(5)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_TRAD_idx=0
  KEY_ARTI_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_RIOR_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamenti OCL (da GSCL_BET)
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTSTATO,OLTUNMIS,OLTQTODL,OLTQTOD1,OLTDTMPS,OLTDINRIC,OLTDTRIC,OLTCOART,OLTEMLAV,OLTLEMPS,OLTCODIC,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTKEYSA,OLTCAMAG,OLTPROVE,OLTFLEVA,OLTQTSAL,OLTCOMME"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTSTATO,OLTUNMIS,OLTQTODL,OLTQTOD1,OLTDTMPS,OLTDINRIC,OLTDTRIC,OLTCOART,OLTEMLAV,OLTLEMPS,OLTCODIC,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTKEYSA,OLTCAMAG,OLTPROVE,OLTFLEVA,OLTQTSAL,OLTCOMME;
        from (i_cTable) where;
            OLCODODL = this.oParentObject.w_OLCODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
      this.w_OLTUNMIS = NVL(cp_ToDate(_read_.OLTUNMIS),cp_NullValue(_read_.OLTUNMIS))
      this.w_OLTQTODL = NVL(cp_ToDate(_read_.OLTQTODL),cp_NullValue(_read_.OLTQTODL))
      this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
      this.w_OLTDTMPS = NVL(cp_ToDate(_read_.OLTDTMPS),cp_NullValue(_read_.OLTDTMPS))
      this.w_OLTDINRIC = NVL(cp_ToDate(_read_.OLTDINRIC),cp_NullValue(_read_.OLTDINRIC))
      this.w_OLTDTRIC = NVL(cp_ToDate(_read_.OLTDTRIC),cp_NullValue(_read_.OLTDTRIC))
      this.w_OLTCOART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
      this.w_OLTEMLAV = NVL(cp_ToDate(_read_.OLTEMLAV),cp_NullValue(_read_.OLTEMLAV))
      this.w_OLTLEMPS = NVL(cp_ToDate(_read_.OLTLEMPS),cp_NullValue(_read_.OLTLEMPS))
      this.w_OLTCODIC = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
      this.w_OLTFLORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
      this.w_OLTFLIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
      this.w_OLTCOMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
      this.w_OLTKEYSA = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
      this.w_OLTCAMAG = NVL(cp_ToDate(_read_.OLTCAMAG),cp_NullValue(_read_.OLTCAMAG))
      this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
      this.w_OLTFLEVA = NVL(cp_ToDate(_read_.OLTFLEVA),cp_NullValue(_read_.OLTFLEVA))
      this.w_OQTSAL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
      this.w_OLTCOMME = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARTIPGES,ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARGRUMER,ARTIPGES,ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.w_AGRUMER = NVL(cp_ToDate(_read_.ARGRUMER),cp_NullValue(_read_.ARGRUMER))
      this.w_ATIPGES = NVL(cp_ToDate(_read_.ARTIPGES),cp_NullValue(_read_.ARTIPGES))
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_OLTCODIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.w_OLTCODIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PAR_RIOR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PRLOTRIO,PRQTAMIN,PRLOTMED,PRCOEFLT"+;
        " from "+i_cTable+" PAR_RIOR where ";
            +"PRCODART = "+cp_ToStrODBC(this.w_OLTCOART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PRLOTRIO,PRQTAMIN,PRLOTMED,PRCOEFLT;
        from (i_cTable) where;
            PRCODART = this.w_OLTCOART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
      this.w_LOTECO = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
      this.w_LOTMED = NVL(cp_ToDate(_read_.PRLOTMED),cp_NullValue(_read_.PRLOTMED))
      this.w_LEAVAR = NVL(cp_ToDate(_read_.PRCOEFLT),cp_NullValue(_read_.PRCOEFLT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OLTQTSAL = IIF(this.w_OLTFLEVA="S", 0, this.w_OLTQTOD1)
    if this.w_OLTPROVE="L" and !EMPTY(this.oParentObject.w_OLTCOFOR)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANMAGTER"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC("F");
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_OLTCOFOR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANMAGTER;
          from (i_cTable) where;
              ANTIPCON = "F";
              and ANCODICE = this.oParentObject.w_OLTCOFOR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MAGWIP = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTDINRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDINRIC),'ODL_MAST','OLTDINRIC');
      +",OLTDTMPS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTMPS),'ODL_MAST','OLTDTMPS');
      +",OLTQTODL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTODL),'ODL_MAST','OLTQTODL');
      +",OLTQTOD1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTOD1),'ODL_MAST','OLTQTOD1');
      +",OLTEMLAV ="+cp_NullLink(cp_ToStrODBC(this.w_OLTEMLAV),'ODL_MAST','OLTEMLAV');
      +",OLTDTRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTRIC),'ODL_MAST','OLTDTRIC');
      +",OLTCOFOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCOFOR),'ODL_MAST','OLTCOFOR');
      +",OLTCONTR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLTCONTR),'ODL_MAST','OLTCONTR');
      +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTQTSAL),'ODL_MAST','OLTQTSAL');
      +",OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
             )
    else
      update (i_cTable) set;
          OLTDINRIC = this.w_OLTDINRIC;
          ,OLTDTMPS = this.w_OLTDTMPS;
          ,OLTQTODL = this.w_OLTQTODL;
          ,OLTQTOD1 = this.w_OLTQTOD1;
          ,OLTEMLAV = this.w_OLTEMLAV;
          ,OLTDTRIC = this.w_OLTDTRIC;
          ,OLTCOFOR = this.oParentObject.w_OLTCOFOR;
          ,OLTCONTR = this.oParentObject.w_OLTCONTR;
          ,OLTQTSAL = this.w_OLTQTSAL;
          ,OLTSTATO = "P";
          &i_ccchkf. ;
       where;
          OLCODODL = this.oParentObject.w_OLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna i saldi 
    this.w_OLTQTSAL = this.w_OLTQTSAL - this.w_OQTSAL
    if empty(nvl(this.w_OLTCOMME,""))
      this.w_COMMAPPO = this.w_COMMDEFA
    else
      this.w_COMMAPPO = this.w_OLTCOMME
    endif
    if this.w_OLTQTSAL<>0 AND NOT EMPTY(this.w_OLTCOMAG) AND NOT EMPTY(this.w_OLTKEYSA) AND (NOT EMPTY(this.w_OLTFLORD) OR NOT EMPTY(this.w_OLTFLIMP))
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_OLTKEYSA;
            and SLCODMAG = this.w_OLTCOMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_SALCOM="S"
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SCQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SCQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_OLTKEYSA);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_OLTKEYSA;
              and SCCODMAG = this.w_OLTCOMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa'
          return
        endif
      endif
    endif
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina Qta e Tempi per C/L
    * --- Verifica se deve ricalcolare LT e QTA
    this.w_OLDLEADTIME = this.w_OLTEMLAV
    this.w_QTALOT = 0
    this.w_QTAMIN = 0
    this.w_GIOAPP = 0
    if this.w_ATIPGES<>"S" AND this.w_OLTPROVE="L"
      * --- Provenienza C/Lavoro (Tranne Gestione Scorta)
      this.w_CODART = this.w_OLTCOART
      this.w_CODGRU = this.w_AGRUMER
      if NOT EMPTY(this.oParentObject.w_OLTCONTR)
        * --- Query su contratti
        this.w_OK0 = .F.
        this.w_OK1 = .F.
        * --- Select from CON_TRAD
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2],.t.,this.CON_TRAD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COCODART,COGRUMER,COGIOAPP,COQTAMIN,COLOTMUL,CPROWNUM  from "+i_cTable+" CON_TRAD ";
              +" where CONUMERO="+cp_ToStrODBC(this.oParentObject.w_OLTCONTR)+" AND (COGRUMER="+cp_ToStrODBC(this.w_CODGRU)+" OR COCODART="+cp_ToStrODBC(this.w_CODART)+")";
               ,"_Curs_CON_TRAD")
        else
          select COCODART,COGRUMER,COGIOAPP,COQTAMIN,COLOTMUL,CPROWNUM from (i_cTable);
           where CONUMERO=this.oParentObject.w_OLTCONTR AND (COGRUMER=this.w_CODGRU OR COCODART=this.w_CODART);
            into cursor _Curs_CON_TRAD
        endif
        if used('_Curs_CON_TRAD')
          select _Curs_CON_TRAD
          locate for 1=1
          do while not(eof())
          do case
            case NOT EMPTY(NVL(_Curs_CON_TRAD.COCODART, " "))
              * --- Cerca il Contratto su Articolo (Preferenziale)
              this.w_QTALOT0 = NVL(_Curs_CON_TRAD.COLOTMUL, 0)
              this.w_QTAMIN0 = NVL(_Curs_CON_TRAD.COQTAMIN, 0)
              this.w_GIOAPP0 = NVL(_Curs_CON_TRAD.COGIOAPP, 0)
              this.w_OK0 = .T.
            case NOT EMPTY(NVL(_Curs_CON_TRAD.COGRUMER, " "))
              * --- Oppure Quello per Gruppo Merceologico (Alternativo all'Articolo)
              this.w_QTALOT1 = NVL(_Curs_CON_TRAD.COLOTMUL, 0)
              this.w_QTAMIN1 = NVL(_Curs_CON_TRAD.COQTAMIN, 0)
              this.w_GIOAPP1 = NVL(_Curs_CON_TRAD.COGIOAPP, 0)
              this.w_OK1 = .T.
          endcase
            select _Curs_CON_TRAD
            continue
          enddo
          use
        endif
        do case
          case this.w_OK0=.T.
            this.w_QTALOT = this.w_QTALOT0
            this.w_QTAMIN = this.w_QTAMIN0
            this.w_GIOAPP = this.w_GIOAPP0
          case this.w_OK1=.T.
            this.w_QTALOT = this.w_QTALOT1
            this.w_QTAMIN = this.w_QTAMIN1
            this.w_GIOAPP = this.w_GIOAPP1
          otherwise
            * --- Se non esiste il Contratto per l'Articolo azzera 
            this.oParentObject.w_OLTCONTR = SPACE(15)
        endcase
      endif
      if EMPTY(this.oParentObject.w_OLTCONTR)
        * --- Legge parametri da dati-magazzino
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRGIOAPP,PRLOTRIO,PRQTAMIN,PRCODFOR"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRGIOAPP,PRLOTRIO,PRQTAMIN,PRCODFOR;
            from (i_cTable) where;
                PRCODART = this.w_OLTCOART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
          this.w_QTALOT = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
          this.w_QTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
          this.w_CODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Setta valori su
        if EMPTY(this.w_CODFOR) OR this.w_CODFOR = this.oParentObject.w_OLTCOFOR
          * --- Accetta i parametri letti
        else
          * --- Azzera  i parametri letti perch� non corrispondono al fornitore che c'� sull'ordine
          this.w_QTALOT = 0
          this.w_QTAMIN = 0
          this.w_GIOAPP = 0
        endif
      endif
    else
      if this.w_ATIPGES="F"
        this.w_QTALOT = this.w_LOTRIO
        this.w_QTAMIN = this.w_LOTECO
        this.w_GIOAPP = this.w_GIOAPP + iif(this.w_LOTMED=0, 0, this.w_GIOAPP * this.w_LEAVAR * ((this.w_OLTQTOD1/this.w_LOTMED) - 1))
      endif
    endif
    * --- ADATTA QTA ALLA POLITICA DI RIORDINO LETTA
    this.TmpN = this.w_OLTQTODL
    this.TmpN1 = this.w_OLTQTOD1
    if this.w_ATIPGES = "F"
      if this.w_QTAMIN+this.w_QTALOT<>0 AND this.TmpN1>0
        if this.TmpN1 < this.w_QTAMIN AND this.w_QTAMIN>0
          this.TmpN1 = this.w_QTAMIN
        endif
        if this.w_QTALOT>0
          this.TmpN1 = IIF(MOD(this.TmpN1, this.w_QTALOT)=0, this.TmpN1, (INT(this.TmpN1 / this.w_QTALOT) + 1) * this.w_QTALOT)
        endif
      endif
    else
      this.TmpN1 = this.w_LOTRIO
    endif
    * --- Se variata la qta nella 1^UM la ricalcola nella seconda UM 
    if this.w_OLTQTOD1 <> this.TmpN1
      if this.w_OLTUNMIS <> this.w_UNMIS1
        this.w_UM1 = iif(this.w_OLTUNMIS=this.w_UNMIS3 , this.w_UNMIS3, this.w_UNMIS2)
        this.w_UM2 = this.w_UNMIS1
        this.w_MOLT = iif(this.w_OLTUNMIS=this.w_UNMIS3, this.w_MOLTI3 , this.w_MOLTIP)
        this.w_OP = iif(this.w_OLTUNMIS=this.w_UNMIS3, this.w_OPERA3 , this.w_OPERAT)
        this.w_OP = iif(this.w_OP="*", "/", "*")
        this.w_UM3 = "   "
        this.w_OP3 = " "
        this.w_MOLT3 = 0
        this.TmpN = CALQTA(this.TmpN1,this.w_UM2, this.w_UM2,this.w_OP, this.w_MOLT, " ", "N", " ", "", this.w_UM3,this.w_OP3,this.w_MOLT3)
      else
        this.TmpN = this.TmpN1
      endif
    endif
    * --- E' cambiato il fornitore o la provenienza
    this.TmpC = " "
    if this.w_OLTQTOD1 <> this.TmpN1
      this.TmpC = "Quantit� ordine non congruente con politica di lottizzazione%0Quantit� originaria: %1%0Quantit� corretta: %2%0Premere <s�> per accettare la quantit� corretta; <no> per mantenere quella originaria"
      if ah_YesNo(this.TmpC,"",this.w_OLTUNMIS+tran(this.w_OLTQTODL,v_PQ(12)), this.w_OLTUNMIS+tran(this.TmpN,v_PQ(12)) )
        this.w_OLTQTODL = this.TmpN
        this.w_OLTQTOD1 = this.TmpN1
      endif
    endif
    * --- ASSEGNA NUOVO LEAD-TIME
    this.w_OLTEMLAV = this.w_GIOAPP + IIF(this.w_LOTMED=0 OR this.w_ATIPGES<>"S", 0, this.w_GIOAPP * this.w_LEAVAR * (this.w_OLTQTOD1/this.w_LOTMED - 1))
    if this.w_OLTEMLAV<>this.w_OLDLEADTIME
      if this.w_ATIPGES="S"
        if NOT EMPTY(this.w_OLTDINRIC)
          * --- Variato Lead Time di Produzione
          if ah_YesNo("OCL: %1%0� stato variato il lead-time presente sull'ordine%0Desideri che venga ricalcolata la data di fine produzione?","", this.oParentObject.w_OLCODODL)
            * --- Ricalcola data di fine (Produzione)
            this.w_OLTDTRIC = COCALCLT(this.w_OLTDINRIC, this.w_OLTEMLAV, "A", .F., "")
          endif
        endif
      else
        if NOT EMPTY(this.w_OLTDTRIC)
          * --- Variato Lead Time di Produzione
          if ah_YesNo("OCL: %1%0� stato variato il lead-time presente sull'ordine%0Desideri che venga ricalcolata la data di inizio produzione?","", this.oParentObject.w_OLCODODL)
            * --- Ricalcola data di inizio (Produzione)
            this.w_OLTDINRIC = COCALCLT(this.w_OLTDTRIC, this.w_OLTEMLAV, "I", .F., "")
            * --- Ricalcola data di inizio (MPS)
            this.w_OLTDTMPS = COCALCLT(this.w_OLTDTRIC, this.w_OLTLEMPS, "I", .F., "")
          endif
        endif
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- AGGIORNA DETTAGLIO ODL
    if this.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) 
      * --- Legge Eventuali Materiali del Terzista
      vq_exec ("..\COLA\EXE\QUERY\GSCO_QMT", this, "_MatTer_")
    endif
    * --- Esegue ricostruzione dettaglio ...
    vq_exec ("..\COLA\EXE\QUERY\GSCO_QAO", this, "_RicDett_")
    if USED("_RicDett_")
      * --- Aggiorna Qta Cursore ...
      SELECT _RicDett_
      go top
      SCAN FOR NOT EMPTY(NVL(OLCODODL,"")) AND NVL(CPROWNUM,0)<>0
      this.w_OQTSAL = NVL(OLQTASAL, 0)
      this.w_OLCODICE = NVL(OLCODICE," ")
      this.w_CPROWNUM = CPROWNUM
      this.w_OLDATRIC = this.w_OLTDINRIC
      this.w_OLUNIMIS = NVL(OLUNIMIS ," ")
      this.w_OLKEYSAL = NVL(OLKEYSAL," ")
      this.w_OLCODMAG = NVL(OLCODMAG, SPACE(5))
      this.w_OLQTAMOV = this.w_OLTQTOD1 * NVL(OLCOEIMP, 0)
      this.w_OLQTAUM1 = this.w_OLQTAMOV
      this.w_OLFLIMPE = NVL(OLFLIMPE, " ")
      this.w_OLTIPPRE = IIF(NVL(ARTIPPRE, "N") $ "AM", ARTIPPRE, "N")
      this.w_UM1 = NVL(ARUNMIS1, SPACE(3))
      this.w_SALCOM = NVL(ARSALCOM, SPACE(1))
      if this.w_OLUNIMIS <> this.w_UM1
        this.w_UM2 = NVL(ARUNMIS2, SPACE(3))
        this.w_UM3 = NVL(CAUNIMIS, SPACE(3))
        this.w_OP = NVL(AROPERAT, " ")
        this.w_MOLT = NVL(ARMOLTIP, 0)
        this.w_OP3 = NVL(CAOPERAT, " ")
        this.w_MOLT3 = NVL(CAMOLTIP, 0)
        this.w_OLQTAUM1 = CALQTA(this.w_OLQTAMOV,this.w_OLUNIMIS, this.w_UM2,this.w_OP, this.w_MOLT, " ", "N", " ", "", this.w_UM3,this.w_OP3,this.w_MOLT3)
      endif
      this.w_OLFLEVAS = " "
      this.w_OLEVAAUT = " "
      if this.w_OLTPROVE="L" AND NOT EMPTY(this.oParentObject.w_OLTCONTR) AND USED("_MatTer_")
        * --- Legge Eventuali Materiali del Terzista
        select _MatTer_
        go top
        LOCATE FOR NVL(MCCODICE, SPACE(20))=this.w_OLCODICE
        if FOUND()
          * --- Se il componente e' presente nei materiali del Terzista non deve eseguire alcun Impegno ne Trasferimento
          this.w_OLFLEVAS = "S"
          this.w_OLTIPPRE = "N"
          this.w_OLEVAAUT = "S"
        endif
      endif
      * --- Si prevede Evaso Totalmente o Niente
      this.w_OLQTAEVA = IIF(this.w_OLFLEVAS="S", this.w_OLQTAMOV, 0)
      this.w_OLQTAEV1 = IIF(this.w_OLFLEVAS="S", this.w_OLQTAUM1, 0)
      this.w_OLQTASAL = IIF(this.w_OLFLEVAS="S", 0, this.w_OLQTAUM1)
      * --- Aggiorna ODL_DETT
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLDATRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLDATRIC),'ODL_DETT','OLDATRIC');
        +",OLQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAMOV),'ODL_DETT','OLQTAMOV');
        +",OLQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAUM1),'ODL_DETT','OLQTAUM1');
        +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLFLEVAS),'ODL_DETT','OLFLEVAS');
        +",OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEVA),'ODL_DETT','OLQTAEVA');
        +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTAEV1),'ODL_DETT','OLQTAEV1');
        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_OLQTASAL),'ODL_DETT','OLQTASAL');
        +",OLTIPPRE ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPPRE),'ODL_DETT','OLTIPPRE');
        +",OLEVAAUT ="+cp_NullLink(cp_ToStrODBC(this.w_OLEVAAUT),'ODL_DETT','OLEVAAUT');
        +",OLMAGWIP ="+cp_NullLink(cp_ToStrODBC(this.w_MAGWIP),'ODL_DETT','OLMAGWIP');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.oParentObject.w_OLCODODL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            OLDATRIC = this.w_OLDATRIC;
            ,OLQTAMOV = this.w_OLQTAMOV;
            ,OLQTAUM1 = this.w_OLQTAUM1;
            ,OLFLEVAS = this.w_OLFLEVAS;
            ,OLQTAEVA = this.w_OLQTAEVA;
            ,OLQTAEV1 = this.w_OLQTAEV1;
            ,OLQTASAL = this.w_OLQTASAL;
            ,OLTIPPRE = this.w_OLTIPPRE;
            ,OLEVAAUT = this.w_OLEVAAUT;
            ,OLMAGWIP = this.w_MAGWIP;
            &i_ccchkf. ;
         where;
            OLCODODL = this.oParentObject.w_OLCODODL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna i saldi 
      this.w_OLQTASAL = this.w_OLQTASAL-this.w_OQTSAL
      if NOT EMPTY(this.w_OLKEYSAL) AND NOT EMPTY(this.w_OLCODMAG) AND this.w_OLQTASAL<>0 AND NOT EMPTY(this.w_OLFLIMPE)
        * --- Try
        local bErr_0415F978
        bErr_0415F978=bTrsErr
        this.Try_0415F978()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0415F978
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_OLQTASAL);
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTIPER = SLQTIPER + this.w_OLQTASAL;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_OLKEYSAL;
              and SLCODMAG = this.w_OLCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_SALCOM="S"
          * --- Try
          local bErr_04151848
          bErr_04151848=bTrsErr
          this.Try_04151848()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04151848
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_OLQTASAL);
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTIPER = SCQTIPER + this.w_OLQTASAL;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_OLKEYSAL;
                and SCCODMAG = this.w_OLCODMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
      SELECT _RicDett_
      ENDSCAN
      SELECT _RicDett_
      USE
    endif
    if USED("_MatTer_")
      select _MatTer_
      USE
    endif
  endproc
  proc Try_0415F978()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLKEYSAL,'SLCODMAG',this.w_OLCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04151848()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLKEYSAL,'SCCODMAG',this.w_OLCODMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.w_OLKEYSAL)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLCODMAG;
           ,this.w_COMMAPPO;
           ,this.w_OLKEYSAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_TRAD'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ODL_DETT'
    this.cWorkTables[5]='ODL_MAST'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='SALDIART'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='SALDICOM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_CON_TRAD')
      use in _Curs_CON_TRAD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
