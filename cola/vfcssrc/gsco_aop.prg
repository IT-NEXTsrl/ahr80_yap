* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_aop                                                        *
*              Gestione ODL/OCL/ODA                                            *
*                                                                              *
*      Author: TAM Software Srl & CodeLab Srl                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_759]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-08-17                                                      *
* Last revis.: 2018-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsco_aop
PARAMETERS pParentObject, pTipGes
* --- Fine Area Manuale
return(createobject("tgsco_aop"))

* --- Class definition
define class tgsco_aop as StdForm
  Top    = -1
  Left   = 10

  * --- Standard Properties
  Width  = 858
  Height = 473+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-10-26"
  HelpContextID=91642519
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=306

  * --- Constant Properties
  ODL_MAST_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  MAGAZZIN_IDX = 0
  CAM_AGAZ_IDX = 0
  SALDIART_IDX = 0
  UNIMIS_IDX = 0
  PAR_RIOR_IDX = 0
  DISMBASE_IDX = 0
  CONTI_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  MA_COSTI_IDX = 0
  TABMCICL_IDX = 0
  CON_TRAM_IDX = 0
  DOC_MAST_IDX = 0
  ODL_SMPL_IDX = 0
  CIC_MAST_IDX = 0
  ODL_CICL_IDX = 0
  cFile = "ODL_MAST"
  cKeySelect = "OLCODODL"
  cKeyWhere  = "OLCODODL=this.w_OLCODODL"
  cKeyWhereODBC = '"OLCODODL="+cp_ToStrODBC(this.w_OLCODODL)';

  cKeyWhereODBCqualified = '"ODL_MAST.OLCODODL="+cp_ToStrODBC(this.w_OLCODODL)';

  cPrg = "gsco_aop"
  cComment = "Gestione ODL/OCL/ODA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPGES = space(1)
  w_PAROBJ = space(1)
  w_ReadPar = space(2)
  w_MAGPAR = space(5)
  w_MAGWIP = space(5)
  w_GESWIP = space(1)
  w_STATOGES = space(10)
  w_OLCODODL = space(15)
  o_OLCODODL = space(15)
  w_OLDATODP = ctod('  /  /  ')
  w_OLDATODL = ctod('  /  /  ')
  w_OLTSTATO = space(1)
  o_OLTSTATO = space(1)
  w_OLTSTATO = space(1)
  w_OLTSTATO = space(1)
  w_OLTFAODL = 0
  w_OLOPEODP = 0
  w_OLOPEODL = 0
  w_OLTCODIC = space(20)
  o_OLTCODIC = space(20)
  w_DESCOD = space(40)
  w_OLTCOART = space(20)
  o_OLTCOART = space(20)
  w_ARTIPART = space(1)
  o_ARTIPART = space(1)
  w_CODART = space(20)
  w_LOTRIO = 0
  w_PUNRIO = 0
  w_LEAVAR = 0
  w_LOTECO = 0
  w_LEAFIS = 0
  w_LOTMED = 0
  w_DESART = space(40)
  w_OLTPROVE = space(1)
  o_OLTPROVE = space(1)
  w_ATIPGES = space(1)
  w_CHKAGG = .F.
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_UNMIS2 = space(3)
  w_MOLTI3 = 0
  w_UNMIS1 = space(3)
  w_MAGPRE = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OLTCOMAG = space(5)
  w_TDESMAG = space(30)
  w_AGRUMER = space(5)
  w_CAUORD = space(5)
  w_CAUIMP = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_AFLCOMM = space(1)
  w_AFLORDI = space(1)
  w_AFLIMPE = space(1)
  w_OLTCAMAG = space(5)
  o_OLTCAMAG = space(5)
  w_OLTFLORD = space(1)
  w_OLTFLIMP = space(1)
  w_FLCOMM = space(1)
  w_OLTKEYSA = space(40)
  w_OLTUNMIS = space(3)
  o_OLTUNMIS = space(3)
  w_OLTDTCON = ctod('  /  /  ')
  o_OLTDTCON = ctod('  /  /  ')
  w_OLTQTODL = 0
  o_OLTQTODL = 0
  w_OLTQTOEV = 0
  o_OLTQTOEV = 0
  w_OLTQTOD1 = 0
  w_OLTQTOE1 = 0
  w_OLTFLEVA = space(1)
  w_OLTQTSAL = 0
  w_OLTCOMAG = space(5)
  w_OLTLEMPS = 0
  w_OLTDTMPS = ctod('  /  /  ')
  w_OLTDTLAN = ctod('  /  /  ')
  w_OLTEMLAV = 0
  w_OLTDTRIC = ctod('  /  /  ')
  o_OLTDTRIC = ctod('  /  /  ')
  w_OLTDINRIC = ctod('  /  /  ')
  o_OLTDINRIC = ctod('  /  /  ')
  w_OLTDTINI = ctod('  /  /  ')
  w_OLTDTFIN = ctod('  /  /  ')
  w_OLTPERAS = space(3)
  w_OLTTIPAT = space(1)
  w_OLTDISBA = space(20)
  o_OLTDISBA = space(20)
  w_OLTCICLO = space(15)
  o_OLTCICLO = space(15)
  w_DESDIS = space(40)
  w_OLTTICON = space(1)
  w_OLTCOFOR = space(15)
  o_OLTCOFOR = space(15)
  w_DESFOR = space(40)
  w_OLTCONTR = space(15)
  o_OLTCONTR = space(15)
  w_OLTCOCEN = space(15)
  w_DESCON = space(40)
  w_OLTVOCEN = space(15)
  w_VOCDES = space(40)
  w_CODCOS = space(5)
  w_OLTCOMME = space(15)
  w_OLTCOATT = space(15)
  o_OLTCOATT = space(15)
  w_DTOBSO = ctod('  /  /  ')
  w_TIPVOC = space(1)
  w_DTOBVO = ctod('  /  /  ')
  w_DESCAN = space(40)
  w_DESATT = space(30)
  w_COCODVAL = space(3)
  w_OLTFLIMC = space(1)
  o_OLTFLIMC = space(1)
  w_OLTIMCOM = 0
  w_OLTSEDOC = space(10)
  w_OLTCPDOC = 0
  w_OLSTAODL = space(1)
  w_OLTCOCOS = space(5)
  w_AFORCO = space(1)
  w_AFCOCO = space(1)
  w_OLTFORCO = space(1)
  w_OLTFCOCO = space(1)
  w_DESCIC = space(40)
  w_TESTFORM = .F.
  w_HASEVENT = .F.
  w_ORIGQTA = 0
  w_ORIGDATA = ctod('  /  /  ')
  w_TDINRIC = ctod('  /  /  ')
  w_CHKDETT = .F.
  w_FORABI = space(15)
  w_OLDODP = space(43)
  w_DATCON = ctod('  /  /  ')
  w_CONDES = space(50)
  w_OSTATO = space(1)
  w_MAGTER = space(5)
  w_TDISMAG = space(1)
  w_OLTVARIA = space(1)
  w_DEFCICLO = space(15)
  w_CALCOLA = space(10)
  w_DBCODRIS = space(15)
  w_ARDTOBSO = ctod('  /  /  ')
  w_FLUSEP = space(1)
  w_MODUM2 = space(1)
  w_PREZUM = space(1)
  w_ARCODCEN = space(15)
  w_ANCODVAL = space(5)
  w_ANCATCOM = space(3)
  w_ANSCORPO = space(1)
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_CV = space(3)
  w_IVACON = space(1)
  w_ARPROPRE = space(1)
  w_CHGECONT = .F.
  w_CALCDATA = .F.
  w_ERDATINI = .F.
  w_CALCCONT = .F.
  w_LMAGTER = space(5)
  w_PPCCSODA = space(15)
  w_PPCCSODL = space(15)
  w_OLCRIFOR = space(1)
  w_OLTDTOBS = ctod('  /  /  ')
  w_OLTFLSUG = space(1)
  w_OLTFLCON = space(1)
  w_OLTFLDAP = space(1)
  w_OLTFLPIA = space(1)
  w_OLTFLLAN = space(1)
  w_OGGMPS = space(1)
  w_OLTMSPINI = ctod('  /  /  ')
  w_OLTMSPFIN = ctod('  /  /  ')
  w_ARFLCOMM = space(1)
  w_OLTCRELA = space(1)
  w_OLTSAFLT = 0
  w_OLSERMRP = space(10)
  w_CACODFAS = space(66)
  w_OLTDISBA = space(20)
  w_DESDIS = space(40)
  w_OLTSECIC = space(10)
  o_OLTSECIC = space(10)
  w_OLTCILAV = space(20)
  w_DESCILAV = space(40)
  w_OLTSEODL = space(15)
  w_NUMFASE = 0
  w_CLINDCIC = 0
  w_HAILCIC = .F.
  w_AGGOCLFA = .F.
  w_OLVARCIC = space(1)
  w_ORIGCICL = space(10)
  w_ARFSRIFE = space(20)
  w_OLFLMODO = space(1)
  o_OLFLMODO = space(1)
  w_OLFLMODC = space(1)
  w_OLTSECPR = space(15)
  w_OLTCOPOI = space(1)
  w_OLTFAOUT = space(1)
  w_OLTULFAS = space(1)
  w_DELOCLFA = .F.
  w_DESFASE = space(40)
  w_TIPIMP = space(1)
  w_MAGIMP = space(5)
  w_PPMATINP = space(1)
  w_OLCODVAL = space(3)
  w_OLTMGWIP = space(5)
  w_OL__NOTE = space(0)
  w_LEVELTMP = 0
  w_TMPTYPE = space(3)
  w_ST = space(1)
  w_LEVEL = 0
  w_CLADOC = space(2)
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_TYPE = space(3)
  w_STATO = space(10)
  w_ROWNUM = 0
  w_CODODL2 = space(10)
  w_ST1 = space(1)
  w_STATOR = space(15)
  w_FirstTime = .F.
  w_COD3 = space(41)
  w_RIF2 = 0
  w_CODODL1 = space(10)
  w_RIF3 = space(15)
  w_TIPMAG = space(5)
  w_NUMINI = 0
  w_SERIE1 = space(2)
  w_NUMFIN = 0
  w_SERIE2 = space(2)
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_NUMREI = 0
  w_NUMREF = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODMAG = space(5)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_FLEVAS = space(1)
  w_FLVEAC1 = space(1)
  w_DBCODINI = space(41)
  w_DBCODFIN = space(41)
  w_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  w_GRUINI = space(5)
  w_GRUFIN = space(5)
  w_CATINI = space(5)
  w_CATFIN = space(5)
  w_FAMINI = space(5)
  w_FAMFIN = space(5)
  w_PIAINI = space(5)
  w_PIAFIN = space(5)
  w_CODBUS = space(3)
  w_DATIODL = ctod('  /  /  ')
  w_DATFODL = ctod('  /  /  ')
  w_SELODL = space(1)
  w_CATDOC = space(2)
  o_CATDOC = space(2)
  w_DATODI = ctod('  /  /  ')
  w_DATODF = ctod('  /  /  ')
  w_SELOCL = space(1)
  w_FORSEL = space(15)
  w_TIPFOR = space(1)
  w_TIPDOC = space(5)
  w_SELODA = space(1)
  w_OLCODINI = space(15)
  w_OLCODFIN = space(15)
  w_CODRIC = space(20)
  w_CADESAR = space(40)
  w_QTAORD = 0
  w_MAGAZ = space(5)
  w_DATEMS = ctod('  /  /  ')
  w_DATEVF = ctod('  /  /  ')
  w_CAUMAG = space(5)
  w_MAGAZ = space(5)
  w_FORNIT = space(15)
  w_QTASCA = 0
  w_QTARES = 0
  w_NUMREG = 0
  w_DATEVA = ctod('  /  /  ')
  w_FORNIT = space(15)
  w_RIF = space(15)
  w_QTAORD = 0
  w_QTAEVA = 0
  w_UNIMIS1 = space(3)
  w_CODODL_ = space(15)
  w_CPROWNUM = 0
  w_CODODL = space(15)
  w_QTAORD = 0
  w_UNIMIS = space(3)
  w_DATEVF = ctod('  /  /  ')
  w_QTAEVA = 0
  w_QTARES = 0
  w_MAGAZ = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(3)
  w_DATEVA = ctod('  /  /  ')
  w_NUMREG = 0
  w_FORNIT = space(15)
  w_CODODL = space(15)
  w_QTAORD = 0
  w_UNIMIS = space(3)
  w_DATEVF = ctod('  /  /  ')
  w_QTAEVA = 0
  w_QTARES = 0
  w_MAGAZ = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(3)
  w_DATEVA = ctod('  /  /  ')
  w_NUMREG = 0
  w_FORNIT = space(15)
  w_NUMREG = 0
  w_DATFINE = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_PV = space(1)
  w_PROVE = space(10)
  w_OLTQTOSC = 0
  o_OLTQTOSC = 0
  w_OLTQTOS1 = 0
  w_OLTQTPRO = 0
  o_OLTQTPRO = 0
  w_OLTQTPR1 = 0
  w_OLTPRIOR = 0
  w_OLTSOSPE = space(1)
  w_PUNLOT = 0
  w_LOTMAX = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_OLCODODL = this.W_OLCODODL

  * --- Children pointers
  GSCO_MOL = .NULL.
  GSCO_MCS = .NULL.
  GSCO_MCL = .NULL.
  GSCO_MRF = .NULL.
  GSCO_MMO = .NULL.
  w_TREEV = .NULL.
  w_StatoODL = .NULL.
  w_StatoDOC = .NULL.
  w_ProvODLF = .NULL.
  w_ZPF = .NULL.
  w_ZFP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_aop
  pFLGEST=' '
  pPAROBJ=.NULL.
  
  * --- Questo cursore contiene gli OCL di fase da eliminare
  * ---  in caso di modifica di una fase esterna GSCI_MCL
  * --- w_bOclfadadel indica se ci sono degli OCL di fase da eliminare
  w_Cur_Del_Ocl = Space(10)
  w_bOclfadadel=.f.
  
  w_Cur_Cic_Odl = Space(10)
  
  * --- KEYRIF utilizzato per le oprazioni di controllo su TMPODL_CICL
  w_CLKEYRIF = SYS(2015)
  
  
  * --- GestiSCE la modifica
  func ah_HasCPEvents(i_cOp)
    if this.cFunction="Query" and Upper(i_cop)="ECPEDIT"
       * This.w_HASEVCOP=i_cop
       This.NotifyEvent("HasEvent")
       Return ( This.w_HASEVENT )
    Else
       * --- Se l'evento non � modifica prosegue
       Return(.t.)
    Endif
  EndFunc
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
   IF TYPE('PPARENT.cZoomOnZoom')='C'
    return("GSCO_AOP,.null.','"+IIF(TYPE("this.pFLGEST")<>'C', " ",this.pFLGEST))
    else
     return("GSCO_AOP,"+'"+'+".null.,"+'"'+IIF(TYPE("this.pFLGEST")<>'C','',this.pFLGEST))
    endif
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=11, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ODL_MAST','gsco_aop')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_aopPag1","gsco_aop",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ordine")
      .Pages(1).HelpContextID = 238677530
      .Pages(2).addobject("oPag","tgsco_aopPag2","gsco_aop",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Altri dati")
      .Pages(2).HelpContextID = 58688601
      .Pages(3).addobject("oPag","tgsco_aopPag3","gsco_aop",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Materiali")
      .Pages(3).HelpContextID = 167573566
      .Pages(4).addobject("oPag","tgsco_aopPag4","gsco_aop",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Risorse")
      .Pages(4).HelpContextID = 850198
      .Pages(5).addobject("oPag","tgsco_aopPag5","gsco_aop",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Ciclo di lavorazione")
      .Pages(5).HelpContextID = 112315545
      .Pages(6).addobject("oPag","tgsco_aopPag6","gsco_aop",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Risorse di fase")
      .Pages(6).HelpContextID = 159737974
      .Pages(7).addobject("oPag","tgsco_aopPag7","gsco_aop",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Materiali di output")
      .Pages(7).HelpContextID = 142990282
      .Pages(8).addobject("oPag","tgsco_aopPag8","gsco_aop",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Pegging")
      .Pages(8).HelpContextID = 175387894
      .Pages(9).addobject("oPag","tgsco_aopPag9","gsco_aop",9)
      .Pages(9).oPag.Visible=.t.
      .Pages(9).Caption=cp_Translate("Tracciabilit�")
      .Pages(9).HelpContextID = 59434280
      .Pages(10).addobject("oPag","tgsco_aopPag10","gsco_aop",10)
      .Pages(10).oPag.Visible=.t.
      .Pages(10).Caption=cp_Translate("Note")
      .Pages(10).HelpContextID = 98766550
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOLCODODL_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsco_aop
    this.parent.pFlGEST=pTipGes
    this.parent.pPAROBJ=pParentObject
    
    BindEvent(This, "Click", This.Parent, "oPgFrmClick", 1)
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_TREEV = this.oPgFrm.Pages(9).oPag.TREEV
      this.w_StatoODL = this.oPgFrm.Pages(9).oPag.StatoODL
      this.w_StatoDOC = this.oPgFrm.Pages(9).oPag.StatoDOC
      this.w_ProvODLF = this.oPgFrm.Pages(9).oPag.ProvODLF
      this.w_ZPF = this.oPgFrm.Pages(8).oPag.ZPF
      this.w_ZFP = this.oPgFrm.Pages(8).oPag.ZFP
      DoDefault()
    proc Destroy()
      this.w_TREEV = .NULL.
      this.w_StatoODL = .NULL.
      this.w_StatoDOC = .NULL.
      this.w_ProvODLF = .NULL.
      this.w_ZPF = .NULL.
      this.w_ZFP = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[22]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAM_AGAZ'
    this.cWorkTables[6]='SALDIART'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='PAR_RIOR'
    this.cWorkTables[9]='DISMBASE'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='VOC_COST'
    this.cWorkTables[13]='CAN_TIER'
    this.cWorkTables[14]='ATTIVITA'
    this.cWorkTables[15]='MA_COSTI'
    this.cWorkTables[16]='TABMCICL'
    this.cWorkTables[17]='CON_TRAM'
    this.cWorkTables[18]='DOC_MAST'
    this.cWorkTables[19]='ODL_SMPL'
    this.cWorkTables[20]='CIC_MAST'
    this.cWorkTables[21]='ODL_CICL'
    this.cWorkTables[22]='ODL_MAST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(22))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ODL_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ODL_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCO_MOL = CREATEOBJECT('stdDynamicChild',this,'GSCO_MOL',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSCO_MOL.createrealchild()
    this.GSCO_MCS = CREATEOBJECT('stdDynamicChild',this,'GSCO_MCS',this.oPgFrm.Page4.oPag.oLinkPC_4_1)
    this.GSCO_MCS.createrealchild()
    this.GSCO_MCL = CREATEOBJECT('stdDynamicChild',this,'GSCO_MCL',this.oPgFrm.Page5.oPag.oLinkPC_5_1)
    this.GSCO_MCL.createrealchild()
    this.GSCO_MRF = CREATEOBJECT('stdDynamicChild',this,'GSCO_MRF',this.oPgFrm.Page6.oPag.oLinkPC_6_1)
    this.GSCO_MMO = CREATEOBJECT('stdDynamicChild',this,'GSCO_MMO',this.oPgFrm.Page7.oPag.oLinkPC_7_1)
    this.GSCO_MMO.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCO_MOL)
      this.GSCO_MOL.DestroyChildrenChain()
      this.GSCO_MOL=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSCO_MCS)
      this.GSCO_MCS.DestroyChildrenChain()
      this.GSCO_MCS=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSCO_MCL)
      this.GSCO_MCL.DestroyChildrenChain()
      this.GSCO_MCL=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_1')
    if !ISNULL(this.GSCO_MRF)
      this.GSCO_MRF.DestroyChildrenChain()
      this.GSCO_MRF=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_1')
    if !ISNULL(this.GSCO_MMO)
      this.GSCO_MMO.DestroyChildrenChain()
      this.GSCO_MMO=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCO_MOL.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MCS.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MCL.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MRF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCO_MMO.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCO_MOL.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MCS.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MCL.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MRF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCO_MMO.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCO_MOL.NewDocument()
    this.GSCO_MCS.NewDocument()
    this.GSCO_MCL.NewDocument()
    this.GSCO_MRF.NewDocument()
    this.GSCO_MMO.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCO_MOL.SetKey(;
            .w_OLCODODL,"OLCODODL";
            )
      this.GSCO_MCS.SetKey(;
            .w_OLCODODL,"SMCODODL";
            )
      this.GSCO_MCL.SetKey(;
            .w_OLCODODL,"CLCODODL";
            )
      this.GSCO_MRF.SetKey(;
            .w_OLCODODL,"RFCODODL";
            )
      this.GSCO_MMO.SetKey(;
            .w_OLCODODL,"MOCODODL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCO_MOL.ChangeRow(this.cRowID+'      1',1;
             ,.w_OLCODODL,"OLCODODL";
             )
      .GSCO_MCS.ChangeRow(this.cRowID+'      1',1;
             ,.w_OLCODODL,"SMCODODL";
             )
      .GSCO_MCL.ChangeRow(this.cRowID+'      1',1;
             ,.w_OLCODODL,"CLCODODL";
             )
      .GSCO_MRF.ChangeRow(this.cRowID+'      1',1;
             ,.w_OLCODODL,"RFCODODL";
             )
      .GSCO_MMO.ChangeRow(this.cRowID+'      1',1;
             ,.w_OLCODODL,"MOCODODL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCO_MOL)
        i_f=.GSCO_MOL.BuildFilter()
        if !(i_f==.GSCO_MOL.cQueryFilter)
          i_fnidx=.GSCO_MOL.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MOL.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MOL.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MOL.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MOL.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MCS)
        i_f=.GSCO_MCS.BuildFilter()
        if !(i_f==.GSCO_MCS.cQueryFilter)
          i_fnidx=.GSCO_MCS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MCS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MCS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MCS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MCS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MCL)
        i_f=.GSCO_MCL.BuildFilter()
        if !(i_f==.GSCO_MCL.cQueryFilter)
          i_fnidx=.GSCO_MCL.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MCL.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MCL.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MCL.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MCL.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MRF)
        i_f=.GSCO_MRF.BuildFilter()
        if !(i_f==.GSCO_MRF.cQueryFilter)
          i_fnidx=.GSCO_MRF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MRF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MRF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MRF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MRF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCO_MMO)
        i_f=.GSCO_MMO.BuildFilter()
        if !(i_f==.GSCO_MMO.cQueryFilter)
          i_fnidx=.GSCO_MMO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCO_MMO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCO_MMO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCO_MMO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCO_MMO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_OLCODODL = NVL(OLCODODL,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_50_joined
    link_1_50_joined=.f.
    local link_1_62_joined
    link_1_62_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_22_joined
    link_2_22_joined=.f.
    local link_2_26_joined
    link_2_26_joined=.f.
    local link_2_28_joined
    link_2_28_joined=.f.
    local link_1_170_joined
    link_1_170_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ODL_MAST where OLCODODL=KeySet.OLCODODL
    *
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ODL_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ODL_MAST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ODL_MAST '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_50_joined=this.AddJoinedLink_1_50(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_62_joined=this.AddJoinedLink_1_62(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_22_joined=this.AddJoinedLink_2_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_26_joined=this.AddJoinedLink_2_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_28_joined=this.AddJoinedLink_2_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_170_joined=this.AddJoinedLink_1_170(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OLCODODL',this.w_OLCODODL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPGES = this.pFLGEST
        .w_PAROBJ = this.pPAROBJ
        .w_MAGPAR = space(5)
        .w_MAGWIP = space(5)
        .w_GESWIP = space(1)
        .w_DESCOD = space(40)
        .w_ARTIPART = space(1)
        .w_LOTRIO = 0
        .w_PUNRIO = 0
        .w_LEAVAR = 0
        .w_LOTECO = 0
        .w_LEAFIS = 0
        .w_LOTMED = 0
        .w_DESART = space(40)
        .w_ATIPGES = space(1)
        .w_UNMIS3 = space(3)
        .w_OPERA3 = space(1)
        .w_OPERAT = space(1)
        .w_MOLTIP = 0
        .w_UNMIS2 = space(3)
        .w_MOLTI3 = 0
        .w_UNMIS1 = space(3)
        .w_MAGPRE = space(5)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TDESMAG = space(30)
        .w_AGRUMER = space(5)
        .w_CAUORD = space(5)
        .w_CAUIMP = space(5)
        .w_AFLCOMM = space(1)
        .w_AFLORDI = space(1)
        .w_AFLIMPE = space(1)
        .w_FLCOMM = space(1)
        .w_DESDIS = space(40)
        .w_DESFOR = space(40)
        .w_DESCON = space(40)
        .w_VOCDES = space(40)
        .w_CODCOS = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_TIPVOC = space(1)
        .w_DTOBVO = ctod("  /  /  ")
        .w_DESCAN = space(40)
        .w_DESATT = space(30)
        .w_COCODVAL = space(3)
        .w_DESCIC = space(40)
        .w_TESTFORM = .T.
        .w_HASEVENT = .f.
        .w_ORIGQTA = 0
        .w_ORIGDATA = ctod("  /  /  ")
        .w_CHKDETT = .F.
        .w_FORABI = space(15)
        .w_DATCON = ctod("  /  /  ")
        .w_CONDES = space(50)
        .w_MAGTER = space(5)
        .w_TDISMAG = space(1)
        .w_DEFCICLO = space(15)
        .w_ARDTOBSO = ctod("  /  /  ")
        .w_FLUSEP = space(1)
        .w_MODUM2 = space(1)
        .w_PREZUM = space(1)
        .w_ARCODCEN = space(15)
        .w_ANCODVAL = space(5)
        .w_ANCATCOM = space(3)
        .w_ANSCORPO = space(1)
        .w_CT = space(1)
        .w_CC = space(15)
        .w_CM = space(3)
        .w_CI = ctod("  /  /  ")
        .w_CF = ctod("  /  /  ")
        .w_CV = space(3)
        .w_IVACON = space(1)
        .w_ARPROPRE = space(1)
        .w_CHGECONT = .f.
        .w_CALCDATA = .T.
        .w_ERDATINI = .F.
        .w_CALCCONT = .F.
        .w_LMAGTER = space(5)
        .w_PPCCSODA = space(15)
        .w_PPCCSODL = space(15)
        .w_OLTDTOBS = ctod("  /  /  ")
        .w_OGGMPS = space(1)
        .w_ARFLCOMM = space(1)
        .w_CACODFAS = space(66)
        .w_DESDIS = space(40)
        .w_OLTCILAV = space(20)
        .w_DESCILAV = space(40)
        .w_NUMFASE = 0
        .w_CLINDCIC = 0
        .w_HAILCIC = .F.
        .w_AGGOCLFA = .f.
        .w_OLVARCIC = 'N'
        .w_ORIGCICL = space(10)
        .w_ARFSRIFE = space(20)
        .w_OLFLMODO = space(1)
        .w_DELOCLFA = False
        .w_DESFASE = space(40)
        .w_TIPIMP = space(1)
        .w_MAGIMP = space(5)
        .w_PPMATINP = space(1)
        .w_ST1 = space(1)
        .w_FirstTime = .T.
        .w_TIPMAG = space(5)
        .w_NUMINI = 0
        .w_SERIE1 = ''
        .w_NUMFIN = 999999
        .w_SERIE2 = ''
        .w_DOCINI = ctod("  /  /  ")
        .w_DOCFIN = ctod("  /  /  ")
        .w_NUMREI = 1
        .w_NUMREF = 999999
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_CODMAG = space(5)
        .w_CODCOM = space(15)
        .w_CODATT = space(15)
        .w_DBCODINI = space(41)
        .w_DBCODFIN = space(41)
        .w_FAMAINI = space(5)
        .w_FAMAFIN = space(5)
        .w_GRUINI = space(5)
        .w_GRUFIN = space(5)
        .w_CATINI = space(5)
        .w_CATFIN = space(5)
        .w_FAMINI = space(5)
        .w_FAMFIN = space(5)
        .w_PIAINI = space(5)
        .w_PIAFIN = space(5)
        .w_CODBUS = space(3)
        .w_DATIODL = ctod("  /  /  ")
        .w_DATFODL = ctod("  /  /  ")
        .w_SELODL = "T"
        .w_CATDOC = "XX"
        .w_DATODI = ctod("  /  /  ")
        .w_DATODF = ctod("  /  /  ")
        .w_SELOCL = "T"
        .w_FORSEL = ' '
        .w_TIPFOR = 'F'
        .w_TIPDOC = ' '
        .w_SELODA = "T"
        .w_CADESAR = space(40)
        .w_DATFINE = ctod("  /  /  ")
        .w_DATFIN1 = ctod("  /  /  ")
        .w_PUNLOT = 0
        .w_LOTMAX = 0
        .w_ReadPar = 'PP'
          .link_1_3('Load')
        .w_STATOGES = upper(this.cFunction)
        .w_OLCODODL = NVL(OLCODODL,space(15))
        .op_OLCODODL = .w_OLCODODL
        .w_OLDATODP = NVL(cp_ToDate(OLDATODP),ctod("  /  /  "))
        .w_OLDATODL = NVL(cp_ToDate(OLDATODL),ctod("  /  /  "))
        .w_OLTSTATO = NVL(OLTSTATO,space(1))
        .w_OLTSTATO = NVL(OLTSTATO,space(1))
        .w_OLTSTATO = NVL(OLTSTATO,space(1))
        .w_OLTFAODL = NVL(OLTFAODL,0)
          if link_1_16_joined
            this.w_OLTFAODL = NVL(CPROWNUM116,NVL(this.w_OLTFAODL,0))
            this.w_NUMFASE = NVL(CLROWORD116,0)
            this.w_DESFASE = NVL(CLDESFAS116,space(40))
          else
          .link_1_16('Load')
          endif
        .w_OLOPEODP = NVL(OLOPEODP,0)
        .w_OLOPEODL = NVL(OLOPEODL,0)
        .w_OLTCODIC = NVL(OLTCODIC,space(20))
          if link_1_21_joined
            this.w_OLTCODIC = NVL(CACODICE121,NVL(this.w_OLTCODIC,space(20)))
            this.w_DESCOD = NVL(CADESART121,space(40))
            this.w_CACODFAS = NVL(CACODFAS121,space(66))
            this.w_OLTCOART = NVL(CACODART121,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS121,space(3))
            this.w_OPERA3 = NVL(CAOPERAT121,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP121,0)
            this.w_OLTDTOBS = NVL(cp_ToDate(CADTOBSO121),ctod("  /  /  "))
          else
          .link_1_21('Load')
          endif
        .w_OLTCOART = NVL(OLTCOART,space(20))
          if link_1_23_joined
            this.w_OLTCOART = NVL(ARCODART123,NVL(this.w_OLTCOART,space(20)))
            this.w_DESART = NVL(ARDESART123,space(40))
            this.w_UNMIS1 = NVL(ARUNMIS1123,space(3))
            this.w_OPERAT = NVL(AROPERAT123,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP123,0)
            this.w_UNMIS2 = NVL(ARUNMIS2123,space(3))
            this.w_OLTVOCEN = NVL(ARVOCCEN123,space(15))
            this.w_ARPROPRE = NVL(ARPROPRE123,space(1))
            this.w_ATIPGES = NVL(ARTIPGES123,space(1))
            this.w_AGRUMER = NVL(ARGRUMER123,space(5))
            this.w_OLTDISBA = NVL(ARCODDIS123,space(20))
            this.w_MAGPRE = NVL(ARMAGPRE123,space(5))
            this.w_ARDTOBSO = NVL(cp_ToDate(ARDTOBSO123),ctod("  /  /  "))
            this.w_FLUSEP = NVL(ARFLUSEP123,space(1))
            this.w_PREZUM = NVL(ARPREZUM123,space(1))
            this.w_ARCODCEN = NVL(ARCODCEN123,space(15))
            this.w_ARFLCOMM = NVL(ARSALCOM123,space(1))
            this.w_ARTIPART = NVL(ARTIPART123,space(1))
            this.w_ARFSRIFE = NVL(ARFSRIFE123,space(20))
            this.w_TIPIMP = NVL(ARTIPIMP123,space(1))
            this.w_MAGIMP = NVL(ARMAGIMP123,space(5))
          else
          .link_1_23('Load')
          endif
        .w_CODART = .w_OLTCOART
          .link_1_26('Load')
        .w_OLTPROVE = NVL(OLTPROVE,space(1))
        .w_CHKAGG = Inlist(.cFunction, 'Load', 'Edit') and (.w_OLTSTATO<>"L" or  not .w_OLTPROVE $ "E-L") and .w_ARTIPART<>'FS'
          .link_1_45('Load')
        .w_OLTCOMAG = NVL(OLTCOMAG,space(5))
          if link_1_50_joined
            this.w_OLTCOMAG = NVL(MGCODMAG150,NVL(this.w_OLTCOMAG,space(5)))
            this.w_TDESMAG = NVL(MGDESMAG150,space(30))
            this.w_TDISMAG = NVL(MGDISMAG150,space(1))
          else
          .link_1_50('Load')
          endif
          .link_1_53('Load')
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_OLTCAMAG = NVL(OLTCAMAG,space(5))
          if link_1_62_joined
            this.w_OLTCAMAG = NVL(CMCODICE162,NVL(this.w_OLTCAMAG,space(5)))
            this.w_OLTFLORD = NVL(CMFLORDI162,space(1))
            this.w_OLTFLIMP = NVL(CMFLIMPE162,space(1))
            this.w_FLCOMM = NVL(CMFLCOMM162,space(1))
          else
          .link_1_62('Load')
          endif
        .w_OLTFLORD = NVL(OLTFLORD,space(1))
        .w_OLTFLIMP = NVL(OLTFLIMP,space(1))
        .w_OLTKEYSA = NVL(OLTKEYSA,space(40))
        .w_OLTUNMIS = NVL(OLTUNMIS,space(3))
          * evitabile
          *.link_1_67('Load')
        .w_OLTDTCON = NVL(cp_ToDate(OLTDTCON),ctod("  /  /  "))
        .w_OLTQTODL = NVL(OLTQTODL,0)
        .w_OLTQTOEV = NVL(OLTQTOEV,0)
        .w_OLTQTOD1 = NVL(OLTQTOD1,0)
        .w_OLTQTOE1 = NVL(OLTQTOE1,0)
        .w_OLTFLEVA = NVL(OLTFLEVA,space(1))
        .w_OLTQTSAL = NVL(OLTQTSAL,0)
        .w_OLTCOMAG = NVL(OLTCOMAG,space(5))
          * evitabile
          *.link_1_78('Load')
        .w_OLTLEMPS = NVL(OLTLEMPS,0)
        .w_OLTDTMPS = NVL(cp_ToDate(OLTDTMPS),ctod("  /  /  "))
        .w_OLTDTLAN = NVL(cp_ToDate(OLTDTLAN),ctod("  /  /  "))
        .w_OLTEMLAV = NVL(OLTEMLAV,0)
        .w_OLTDTRIC = NVL(cp_ToDate(OLTDTRIC),ctod("  /  /  "))
        .w_OLTDINRIC = NVL(cp_ToDate(OLTDINRIC),ctod("  /  /  "))
        .w_OLTDTINI = NVL(cp_ToDate(OLTDTINI),ctod("  /  /  "))
        .w_OLTDTFIN = NVL(cp_ToDate(OLTDTFIN),ctod("  /  /  "))
        .w_OLTPERAS = NVL(OLTPERAS,space(3))
        .w_OLTTIPAT = NVL(OLTTIPAT,space(1))
        .w_OLTDISBA = NVL(OLTDISBA,space(20))
          if link_2_2_joined
            this.w_OLTDISBA = NVL(DBCODICE202,NVL(this.w_OLTDISBA,space(20)))
            this.w_DESDIS = NVL(DBDESCRI202,space(40))
            this.w_DEFCICLO = NVL(DBCODRIS202,space(15))
          else
          .link_2_2('Load')
          endif
        .w_OLTCICLO = NVL(OLTCICLO,space(15))
          if link_2_3_joined
            this.w_OLTCICLO = NVL(CSCODICE203,NVL(this.w_OLTCICLO,space(15)))
            this.w_DESCIC = NVL(CSDESCRI203,space(40))
          else
          .link_2_3('Load')
          endif
        .w_OLTTICON = NVL(OLTTICON,space(1))
        .w_OLTCOFOR = NVL(OLTCOFOR,space(15))
          .link_2_13('Load')
        .w_OLTCONTR = NVL(OLTCONTR,space(15))
          .link_2_16('Load')
        .w_OLTCOCEN = NVL(OLTCOCEN,space(15))
          if link_2_19_joined
            this.w_OLTCOCEN = NVL(CC_CONTO219,NVL(this.w_OLTCOCEN,space(15)))
            this.w_DESCON = NVL(CCDESPIA219,space(40))
          else
          .link_2_19('Load')
          endif
        .w_OLTVOCEN = NVL(OLTVOCEN,space(15))
          if link_2_22_joined
            this.w_OLTVOCEN = NVL(VCCODICE222,NVL(this.w_OLTVOCEN,space(15)))
            this.w_VOCDES = NVL(VCDESCRI222,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC222,space(1))
            this.w_DTOBVO = NVL(cp_ToDate(VCDTOBSO222),ctod("  /  /  "))
            this.w_CODCOS = NVL(VCTIPCOS222,space(5))
          else
          .link_2_22('Load')
          endif
        .w_OLTCOMME = NVL(OLTCOMME,space(15))
          if link_2_26_joined
            this.w_OLTCOMME = NVL(CNCODCAN226,NVL(this.w_OLTCOMME,space(15)))
            this.w_DTOBSO = NVL(cp_ToDate(CNDTOBSO226),ctod("  /  /  "))
            this.w_DESCAN = NVL(CNDESCAN226,space(40))
            this.w_COCODVAL = NVL(CNCODVAL226,space(3))
          else
          .link_2_26('Load')
          endif
        .w_OLTCOATT = NVL(OLTCOATT,space(15))
          if link_2_28_joined
            this.w_OLTCOATT = NVL(ATCODATT228,NVL(this.w_OLTCOATT,space(15)))
            this.w_DESATT = NVL(ATDESCRI228,space(30))
          else
          .link_2_28('Load')
          endif
        .w_OLTFLIMC = NVL(OLTFLIMC,space(1))
        .w_OLTIMCOM = NVL(OLTIMCOM,0)
        .w_OLTSEDOC = NVL(OLTSEDOC,space(10))
        .w_OLTCPDOC = NVL(OLTCPDOC,0)
        .w_OLSTAODL = NVL(OLSTAODL,space(1))
        .w_OLTCOCOS = NVL(OLTCOCOS,space(5))
          * evitabile
          *.link_2_40('Load')
        .w_AFORCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='I','+',IIF(.w_AFLCOMM='D','-',' ')),' ')
        .w_AFCOCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='C','+',IIF(.w_AFLCOMM='S','-',' ')),' ')
        .w_OLTFORCO = NVL(OLTFORCO,space(1))
        .w_OLTFCOCO = NVL(OLTFCOCO,space(1))
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_105.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_106.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        .w_TDINRIC = .w_OLTDINRIC
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .w_OLDODP = LEFT(.w_OLTCODIC+SPACE(20),20)+LEFT(.w_OLTUNMIS+'   ',3)+STR(.w_OLTQTODL,12,3)+DTOS(.w_OLTDINRIC)
        .w_OSTATO = .w_OLTSTATO
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .w_OLTVARIA = NVL(OLTVARIA,space(1))
        .w_CALCOLA = ' '
        .w_DBCODRIS = .w_OLTCICLO
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Lead time di produzione:", "N. gg per appr.:")))
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data inizio produzione:", "Data ordine:")))
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data fine produzione:", "Data evasione:")))
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate(AH_MsgFormat(iif(.w_TIPGES="P","Emissione ODP:",iif(.w_TIPGES="F","Emissione Ricambi:",iif(.w_TIPGES="T","Emissione ODF:",IIF(.w_OLTPROVE="I", "Emissione ODL:", IIF(.w_OLTPROVE="L", "Emissione OCL:" , "Emissione ODA:")))))))
        .w_OLCRIFOR = NVL(OLCRIFOR,space(1))
        .w_OLTFLSUG = NVL(OLTFLSUG,space(1))
        .w_OLTFLCON = NVL(OLTFLCON,space(1))
        .w_OLTFLDAP = NVL(OLTFLDAP,space(1))
        .w_OLTFLPIA = NVL(OLTFLPIA,space(1))
        .w_OLTFLLAN = NVL(OLTFLLAN,space(1))
        .w_OLTMSPINI = NVL(cp_ToDate(OLTMSPINI),ctod("  /  /  "))
        .w_OLTMSPFIN = NVL(cp_ToDate(OLTMSPFIN),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_157.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_159.Calculate()
        .w_OLTCRELA = NVL(OLTCRELA,space(1))
        .w_OLTSAFLT = NVL(OLTSAFLT,0)
        .w_OLSERMRP = NVL(OLSERMRP,space(10))
        .w_OLTDISBA = NVL(OLTDISBA,space(20))
          if link_1_170_joined
            this.w_OLTDISBA = NVL(DBCODICE270,NVL(this.w_OLTDISBA,space(20)))
            this.w_DESDIS = NVL(DBDESCRI270,space(40))
            this.w_DEFCICLO = NVL(DBCODRIS270,space(15))
          else
          .link_1_170('Load')
          endif
        .w_OLTSECIC = NVL(OLTSECIC,space(10))
          .link_1_172('Load')
        .w_OLTSEODL = NVL(OLTSEODL,space(15))
          * evitabile
          *.link_1_178('Load')
        .oPgFrm.Page1.oPag.oObj_1_184.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .w_OLFLMODC = .w_OLFLMODO
        .w_OLTSECPR = NVL(OLTSECPR,space(15))
          * evitabile
          *.link_1_192('Load')
        .w_OLTCOPOI = NVL(OLTCOPOI,space(1))
        .w_OLTFAOUT = NVL(OLTFAOUT,space(1))
        .w_OLTULFAS = NVL(OLTULFAS,space(1))
        .w_OLCODVAL = IIF(empty(.w_OLCODVAL), g_PERVAL, .w_OLCODVAL)
        .w_OLTMGWIP = NVL(OLTMGWIP,space(5))
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
        .w_OL__NOTE = NVL(OL__NOTE,space(0))
        .oPgFrm.Page9.oPag.TREEV.Calculate()
        .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
        .w_TMPTYPE = .w_TREEV.getvar("TIPO")
        .w_ST = .w_TREEV.getvar("OLTSTATO")
        .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
        .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
        .w_FLVEAC = 'T'
        .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
        .w_STATO = AH_MsgFormat(iif(.w_ST="M","Suggerito",iif(.w_ST="P","Pianificato",iif(.w_ST="L",iif(.w_Type="ODL","Lanciato","Ordinato"),iif(.w_ST="C","Confermato",iif(.w_ST="D","Da pianificare",iif(.w_ST="S","Suggerito",iif(.w_ST="F","Finito"," "))))))))
        .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
        .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
          .link_9_11('Load')
        .w_STATOR = iif(.w_TYPE <> 'DPI',iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' ,'Confermato','')),'')
        .w_COD3 = .w_TREEV.getvar("OLTCODIC ")
        .w_RIF2 = .w_TREEV.getvar("MVNUMDOC ")
        .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
        .w_RIF3 = iif(.w_type$'DIM-DIR',.w_TREEV.getvar("ardesart "),' ')
        .w_FLEVAS = .w_TREEV.getvar("OLTFLEVA")
        .w_FLVEAC1 = .w_TREEV.getvar("MVFLVEAC")
        .oPgFrm.Page9.oPag.oObj_9_59.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_60.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_61.Calculate()
        .w_OLCODINI = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        .w_OLCODFIN = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        .oPgFrm.Page9.oPag.oObj_9_64.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_65.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_66.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_67.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_68.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_69.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_70.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_71.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_72.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_73.Calculate()
        .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
          .link_9_74('Load')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_DATEMS = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC "),cp_CharToDate('  -  -  '))
        .w_CAUMAG = .w_TREEV.getvar("MVCAUMAG")
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_QTASCA = nvl(.w_TREEV.getvar("OLTQTOBF"),0)
        .w_QTARES = iif((nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) )>0,nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0)
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_RIF = .w_TREEV.getvar("PADRE")
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .oPgFrm.Page9.oPag.StatoODL.Calculate(.w_STATO)
        .w_UNIMIS1 = nvl(.w_TREEV.getvar("OLTUNMIS"),' ')
        .w_CODODL_ = IIF(.w_type='ODL',.w_TREEV.getvar("OLCODODL"),.w_TREEV.getvar("PADRE"))
        .w_CPROWNUM = nvl(.w_TREEV.getvar("CPROWNUM"),0)
        .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
        .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .oPgFrm.Page9.oPag.StatoDOC.Calculate(.w_STATOR)
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
        .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_PV = .w_TREEV.getvar("OLTPROVE")
        .w_PROVE = iif(.w_TYPE='ODF',iif(.w_PV='I','Fase interna',iif(.w_PV='L','Fase esterna','')),'')
        .oPgFrm.Page9.oPag.ProvODLF.Calculate(.w_PROVE)
        .oPgFrm.Page8.oPag.ZPF.Calculate()
        .oPgFrm.Page8.oPag.ZFP.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_5.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_6.Calculate()
        .w_OLTQTOSC = NVL(OLTQTOSC,0)
        .w_OLTQTOS1 = NVL(OLTQTOS1,0)
        .w_OLTQTPRO = NVL(OLTQTPRO,0)
        .w_OLTQTPR1 = NVL(OLTQTPR1,0)
        .w_OLTPRIOR = NVL(OLTPRIOR,0)
        .w_OLTSOSPE = NVL(OLTSOSPE,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ODL_MAST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsco_aop
    do case
      case this.w_TIPGES='P'
        * ODP
        if this.w_OGGMPS<>'S'
         this.BlankRec()
        endif
     case  this.w_TIPGES='F'
        * ODR
        if this.w_OGGMPS<>'R'
          this.BlankRec()
        endif
     case this.w_TIPGES='L'
        * ODL (L)
        if this.w_OLTPROVE='L'
          this.BlankRec()
        endif
     case this.w_TIPGES='Z'
        * OCL (Z)
        if this.w_OLTPROVE<>'L'
          this.BlankRec()
        endif
       otherwise
        * ODA (E)
        if this.w_OLTPROVE<>'E'
          this.BlankRec()
        endif
    endcase
    * Setta valori iniziali
    this.w_ORIGQTA = this.w_OLTQTOD1
    this.w_ORIGDATA = this.w_OLTDTRIC
    this.w_ORIGCICL = this.w_OLTSECIC
    
    * This.mEnableControls()
    
    * --- Chiusura Cursore OCL di fase da eliminare e resetto variabile
    this.w_bOclfadadel=.f.
    If Used(this.w_Cur_Del_Ocl)
       Select(this.w_Cur_Del_Ocl)
       Use
    Endif
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPGES = space(1)
      .w_PAROBJ = space(1)
      .w_ReadPar = space(2)
      .w_MAGPAR = space(5)
      .w_MAGWIP = space(5)
      .w_GESWIP = space(1)
      .w_STATOGES = space(10)
      .w_OLCODODL = space(15)
      .w_OLDATODP = ctod("  /  /  ")
      .w_OLDATODL = ctod("  /  /  ")
      .w_OLTSTATO = space(1)
      .w_OLTSTATO = space(1)
      .w_OLTSTATO = space(1)
      .w_OLTFAODL = 0
      .w_OLOPEODP = 0
      .w_OLOPEODL = 0
      .w_OLTCODIC = space(20)
      .w_DESCOD = space(40)
      .w_OLTCOART = space(20)
      .w_ARTIPART = space(1)
      .w_CODART = space(20)
      .w_LOTRIO = 0
      .w_PUNRIO = 0
      .w_LEAVAR = 0
      .w_LOTECO = 0
      .w_LEAFIS = 0
      .w_LOTMED = 0
      .w_DESART = space(40)
      .w_OLTPROVE = space(1)
      .w_ATIPGES = space(1)
      .w_CHKAGG = .f.
      .w_UNMIS3 = space(3)
      .w_OPERA3 = space(1)
      .w_OPERAT = space(1)
      .w_MOLTIP = 0
      .w_UNMIS2 = space(3)
      .w_MOLTI3 = 0
      .w_UNMIS1 = space(3)
      .w_MAGPRE = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_OLTCOMAG = space(5)
      .w_TDESMAG = space(30)
      .w_AGRUMER = space(5)
      .w_CAUORD = space(5)
      .w_CAUIMP = space(5)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_AFLCOMM = space(1)
      .w_AFLORDI = space(1)
      .w_AFLIMPE = space(1)
      .w_OLTCAMAG = space(5)
      .w_OLTFLORD = space(1)
      .w_OLTFLIMP = space(1)
      .w_FLCOMM = space(1)
      .w_OLTKEYSA = space(40)
      .w_OLTUNMIS = space(3)
      .w_OLTDTCON = ctod("  /  /  ")
      .w_OLTQTODL = 0
      .w_OLTQTOEV = 0
      .w_OLTQTOD1 = 0
      .w_OLTQTOE1 = 0
      .w_OLTFLEVA = space(1)
      .w_OLTQTSAL = 0
      .w_OLTCOMAG = space(5)
      .w_OLTLEMPS = 0
      .w_OLTDTMPS = ctod("  /  /  ")
      .w_OLTDTLAN = ctod("  /  /  ")
      .w_OLTEMLAV = 0
      .w_OLTDTRIC = ctod("  /  /  ")
      .w_OLTDINRIC = ctod("  /  /  ")
      .w_OLTDTINI = ctod("  /  /  ")
      .w_OLTDTFIN = ctod("  /  /  ")
      .w_OLTPERAS = space(3)
      .w_OLTTIPAT = space(1)
      .w_OLTDISBA = space(20)
      .w_OLTCICLO = space(15)
      .w_DESDIS = space(40)
      .w_OLTTICON = space(1)
      .w_OLTCOFOR = space(15)
      .w_DESFOR = space(40)
      .w_OLTCONTR = space(15)
      .w_OLTCOCEN = space(15)
      .w_DESCON = space(40)
      .w_OLTVOCEN = space(15)
      .w_VOCDES = space(40)
      .w_CODCOS = space(5)
      .w_OLTCOMME = space(15)
      .w_OLTCOATT = space(15)
      .w_DTOBSO = ctod("  /  /  ")
      .w_TIPVOC = space(1)
      .w_DTOBVO = ctod("  /  /  ")
      .w_DESCAN = space(40)
      .w_DESATT = space(30)
      .w_COCODVAL = space(3)
      .w_OLTFLIMC = space(1)
      .w_OLTIMCOM = 0
      .w_OLTSEDOC = space(10)
      .w_OLTCPDOC = 0
      .w_OLSTAODL = space(1)
      .w_OLTCOCOS = space(5)
      .w_AFORCO = space(1)
      .w_AFCOCO = space(1)
      .w_OLTFORCO = space(1)
      .w_OLTFCOCO = space(1)
      .w_DESCIC = space(40)
      .w_TESTFORM = .f.
      .w_HASEVENT = .f.
      .w_ORIGQTA = 0
      .w_ORIGDATA = ctod("  /  /  ")
      .w_TDINRIC = ctod("  /  /  ")
      .w_CHKDETT = .f.
      .w_FORABI = space(15)
      .w_OLDODP = space(43)
      .w_DATCON = ctod("  /  /  ")
      .w_CONDES = space(50)
      .w_OSTATO = space(1)
      .w_MAGTER = space(5)
      .w_TDISMAG = space(1)
      .w_OLTVARIA = space(1)
      .w_DEFCICLO = space(15)
      .w_CALCOLA = space(10)
      .w_DBCODRIS = space(15)
      .w_ARDTOBSO = ctod("  /  /  ")
      .w_FLUSEP = space(1)
      .w_MODUM2 = space(1)
      .w_PREZUM = space(1)
      .w_ARCODCEN = space(15)
      .w_ANCODVAL = space(5)
      .w_ANCATCOM = space(3)
      .w_ANSCORPO = space(1)
      .w_CT = space(1)
      .w_CC = space(15)
      .w_CM = space(3)
      .w_CI = ctod("  /  /  ")
      .w_CF = ctod("  /  /  ")
      .w_CV = space(3)
      .w_IVACON = space(1)
      .w_ARPROPRE = space(1)
      .w_CHGECONT = .f.
      .w_CALCDATA = .f.
      .w_ERDATINI = .f.
      .w_CALCCONT = .f.
      .w_LMAGTER = space(5)
      .w_PPCCSODA = space(15)
      .w_PPCCSODL = space(15)
      .w_OLCRIFOR = space(1)
      .w_OLTDTOBS = ctod("  /  /  ")
      .w_OLTFLSUG = space(1)
      .w_OLTFLCON = space(1)
      .w_OLTFLDAP = space(1)
      .w_OLTFLPIA = space(1)
      .w_OLTFLLAN = space(1)
      .w_OGGMPS = space(1)
      .w_OLTMSPINI = ctod("  /  /  ")
      .w_OLTMSPFIN = ctod("  /  /  ")
      .w_ARFLCOMM = space(1)
      .w_OLTCRELA = space(1)
      .w_OLTSAFLT = 0
      .w_OLSERMRP = space(10)
      .w_CACODFAS = space(66)
      .w_OLTDISBA = space(20)
      .w_DESDIS = space(40)
      .w_OLTSECIC = space(10)
      .w_OLTCILAV = space(20)
      .w_DESCILAV = space(40)
      .w_OLTSEODL = space(15)
      .w_NUMFASE = 0
      .w_CLINDCIC = 0
      .w_HAILCIC = .f.
      .w_AGGOCLFA = .f.
      .w_OLVARCIC = space(1)
      .w_ORIGCICL = space(10)
      .w_ARFSRIFE = space(20)
      .w_OLFLMODO = space(1)
      .w_OLFLMODC = space(1)
      .w_OLTSECPR = space(15)
      .w_OLTCOPOI = space(1)
      .w_OLTFAOUT = space(1)
      .w_OLTULFAS = space(1)
      .w_DELOCLFA = .f.
      .w_DESFASE = space(40)
      .w_TIPIMP = space(1)
      .w_MAGIMP = space(5)
      .w_PPMATINP = space(1)
      .w_OLCODVAL = space(3)
      .w_OLTMGWIP = space(5)
      .w_OL__NOTE = space(0)
      .w_LEVELTMP = 0
      .w_TMPTYPE = space(3)
      .w_ST = space(1)
      .w_LEVEL = 0
      .w_CLADOC = space(2)
      .w_FLVEAC = space(1)
      .w_TYPE = space(3)
      .w_STATO = space(10)
      .w_ROWNUM = 0
      .w_CODODL2 = space(10)
      .w_ST1 = space(1)
      .w_STATOR = space(15)
      .w_FirstTime = .f.
      .w_COD3 = space(41)
      .w_RIF2 = 0
      .w_CODODL1 = space(10)
      .w_RIF3 = space(15)
      .w_TIPMAG = space(5)
      .w_NUMINI = 0
      .w_SERIE1 = space(2)
      .w_NUMFIN = 0
      .w_SERIE2 = space(2)
      .w_DOCINI = ctod("  /  /  ")
      .w_DOCFIN = ctod("  /  /  ")
      .w_NUMREI = 0
      .w_NUMREF = 0
      .w_DATINI = ctod("  /  /  ")
      .w_DATFIN = ctod("  /  /  ")
      .w_CODMAG = space(5)
      .w_CODCOM = space(15)
      .w_CODATT = space(15)
      .w_FLEVAS = space(1)
      .w_FLVEAC1 = space(1)
      .w_DBCODINI = space(41)
      .w_DBCODFIN = space(41)
      .w_FAMAINI = space(5)
      .w_FAMAFIN = space(5)
      .w_GRUINI = space(5)
      .w_GRUFIN = space(5)
      .w_CATINI = space(5)
      .w_CATFIN = space(5)
      .w_FAMINI = space(5)
      .w_FAMFIN = space(5)
      .w_PIAINI = space(5)
      .w_PIAFIN = space(5)
      .w_CODBUS = space(3)
      .w_DATIODL = ctod("  /  /  ")
      .w_DATFODL = ctod("  /  /  ")
      .w_SELODL = space(1)
      .w_CATDOC = space(2)
      .w_DATODI = ctod("  /  /  ")
      .w_DATODF = ctod("  /  /  ")
      .w_SELOCL = space(1)
      .w_FORSEL = space(15)
      .w_TIPFOR = space(1)
      .w_TIPDOC = space(5)
      .w_SELODA = space(1)
      .w_OLCODINI = space(15)
      .w_OLCODFIN = space(15)
      .w_CODRIC = space(20)
      .w_CADESAR = space(40)
      .w_QTAORD = 0
      .w_MAGAZ = space(5)
      .w_DATEMS = ctod("  /  /  ")
      .w_DATEVF = ctod("  /  /  ")
      .w_CAUMAG = space(5)
      .w_MAGAZ = space(5)
      .w_FORNIT = space(15)
      .w_QTASCA = 0
      .w_QTARES = 0
      .w_NUMREG = 0
      .w_DATEVA = ctod("  /  /  ")
      .w_FORNIT = space(15)
      .w_RIF = space(15)
      .w_QTAORD = 0
      .w_QTAEVA = 0
      .w_UNIMIS1 = space(3)
      .w_CODODL_ = space(15)
      .w_CPROWNUM = 0
      .w_CODODL = space(15)
      .w_QTAORD = 0
      .w_UNIMIS = space(3)
      .w_DATEVF = ctod("  /  /  ")
      .w_QTAEVA = 0
      .w_QTARES = 0
      .w_MAGAZ = space(5)
      .w_NUMDOC = 0
      .w_ALFDOC = space(3)
      .w_DATEVA = ctod("  /  /  ")
      .w_NUMREG = 0
      .w_FORNIT = space(15)
      .w_CODODL = space(15)
      .w_QTAORD = 0
      .w_UNIMIS = space(3)
      .w_DATEVF = ctod("  /  /  ")
      .w_QTAEVA = 0
      .w_QTARES = 0
      .w_MAGAZ = space(5)
      .w_NUMDOC = 0
      .w_ALFDOC = space(3)
      .w_DATEVA = ctod("  /  /  ")
      .w_NUMREG = 0
      .w_FORNIT = space(15)
      .w_NUMREG = 0
      .w_DATFINE = ctod("  /  /  ")
      .w_DATFIN1 = ctod("  /  /  ")
      .w_PV = space(1)
      .w_PROVE = space(10)
      .w_OLTQTOSC = 0
      .w_OLTQTOS1 = 0
      .w_OLTQTPRO = 0
      .w_OLTQTPR1 = 0
      .w_OLTPRIOR = 0
      .w_OLTSOSPE = space(1)
      .w_PUNLOT = 0
      .w_LOTMAX = 0
      if .cFunction<>"Filter"
        .w_TIPGES = this.pFLGEST
        .w_PAROBJ = this.pPAROBJ
        .w_ReadPar = 'PP'
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_ReadPar))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,6,.f.)
        .w_STATOGES = upper(this.cFunction)
          .DoRTCalc(8,8,.f.)
        .w_OLDATODP = i_DATSYS
        .w_OLDATODL = i_DATSYS
        .w_OLTSTATO = iif(.w_TIPGES$"PFT","C","P")
        .w_OLTSTATO = iif(.w_TIPGES$"PFT","C","P")
        .w_OLTSTATO = iif(.w_TIPGES$"PFT","C","P")
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_OLTFAODL))
          .link_1_16('Full')
          endif
        .w_OLOPEODP = i_CODUTE
        .w_OLOPEODL = i_CODUTE
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_OLTCODIC))
          .link_1_21('Full')
          endif
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_OLTCOART))
          .link_1_23('Full')
          endif
          .DoRTCalc(20,20,.f.)
        .w_CODART = .w_OLTCOART
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_CODART))
          .link_1_26('Full')
          endif
          .DoRTCalc(22,28,.f.)
        .w_OLTPROVE = IIF(empty(.w_OLTCODIC), IIF(.w_TIPGES="E", "E", IIF(.w_TIPGES="Z","L", IIF(.w_TIPGES="L", "I", "E"))) , IIF(.w_ARPROPRE='I' and .w_TIPGES='L','I', IIF(.w_ARPROPRE='L' and .w_TIPGES='Z','L',IIF(.w_ARPROPRE='E' and .w_TIPGES='E','E',.w_ARPROPRE))))
          .DoRTCalc(30,30,.f.)
        .w_CHKAGG = Inlist(.cFunction, 'Load', 'Edit') and (.w_OLTSTATO<>"L" or  not .w_OLTPROVE $ "E-L") and .w_ARTIPART<>'FS'
        .DoRTCalc(32,38,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_45('Full')
          endif
          .DoRTCalc(39,39,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(41,41,.f.)
        .w_OLTCOMAG = IIF(EMPTY(.w_MAGPRE), .w_MAGPAR, .w_MAGPRE)
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_OLTCOMAG))
          .link_1_50('Full')
          endif
        .DoRTCalc(43,45,.f.)
          if not(empty(.w_CAUORD))
          .link_1_53('Full')
          endif
          .DoRTCalc(46,53,.f.)
        .w_OLTCAMAG = .w_CAUORD
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_OLTCAMAG))
          .link_1_62('Full')
          endif
        .w_OLTFLORD = iif(.w_OLTSTATO="L" and .w_OLTPROVE$"L-E", " ", .w_OLTFLORD)
          .DoRTCalc(56,57,.f.)
        .w_OLTKEYSA = .w_OLTCOART
        .w_OLTUNMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(59,59,.f.)
          if not(empty(.w_OLTUNMIS))
          .link_1_67('Full')
          endif
          .DoRTCalc(60,62,.f.)
        .w_OLTQTOD1 = CALQTAADV(.w_OLTQTODL,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTODL")
        .w_OLTQTOE1 = CALQTAADV(.w_OLTQTOEV,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTOEV")
          .DoRTCalc(65,65,.f.)
        .w_OLTQTSAL = IIF(.w_OLTFLEVA="S", 0, MAX(.w_OLTQTOD1-.w_OLTQTOE1, 0))
        .DoRTCalc(67,67,.f.)
          if not(empty(.w_OLTCOMAG))
          .link_1_78('Full')
          endif
          .DoRTCalc(68,70,.f.)
        .w_OLTEMLAV = COSETLT(.w_OLTCODIC, .w_OLTPROVE, .w_OLTQTODL, .w_OLTQTOD1, .w_OLTCOFOR+.w_OLTCONTR, .w_OLTDTRIC, .w_OLTUNMIS)
          .DoRTCalc(72,76,.f.)
        .w_OLTTIPAT = 'A'
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_OLTDISBA))
          .link_2_2('Full')
          endif
        .w_OLTCICLO = iif((g_PRFA='S' and g_CICLILAV<>'S' or g_PRFA<>'S') and .w_OLTPROVE="I" , .w_DEFCICLO , SPACE(15))
        .DoRTCalc(79,79,.f.)
          if not(empty(.w_OLTCICLO))
          .link_2_3('Full')
          endif
          .DoRTCalc(80,80,.f.)
        .w_OLTTICON = 'F'
        .w_OLTCOFOR = IIF(.w_OLTPROVE $ 'E-L' , .w_OLTCOFOR, SPACE(15))
        .DoRTCalc(82,82,.f.)
          if not(empty(.w_OLTCOFOR))
          .link_2_13('Full')
          endif
        .DoRTCalc(83,84,.f.)
          if not(empty(.w_OLTCONTR))
          .link_2_16('Full')
          endif
        .w_OLTCOCEN = IIF(!EMPTY(.w_ARCODCEN) , .w_ARCODCEN , IIF(.w_OLTPROVE='E', .w_PPCCSODA , .w_PPCCSODL))
        .DoRTCalc(85,85,.f.)
          if not(empty(.w_OLTCOCEN))
          .link_2_19('Full')
          endif
        .DoRTCalc(86,87,.f.)
          if not(empty(.w_OLTVOCEN))
          .link_2_22('Full')
          endif
        .DoRTCalc(88,90,.f.)
          if not(empty(.w_OLTCOMME))
          .link_2_26('Full')
          endif
        .DoRTCalc(91,91,.f.)
          if not(empty(.w_OLTCOATT))
          .link_2_28('Full')
          endif
          .DoRTCalc(92,97,.f.)
        .w_OLTFLIMC = IIF(EMPTY(.w_OLTCOATT), "N", "S")
          .DoRTCalc(99,101,.f.)
        .w_OLSTAODL = 'O'
        .w_OLTCOCOS = IIF(EMPTY(.w_OLTCOATT), SPACE(5), .w_CODCOS)
        .DoRTCalc(103,103,.f.)
          if not(empty(.w_OLTCOCOS))
          .link_2_40('Full')
          endif
        .w_AFORCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='I','+',IIF(.w_AFLCOMM='D','-',' ')),' ')
        .w_AFCOCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='C','+',IIF(.w_AFLCOMM='S','-',' ')),' ')
        .w_OLTFORCO = IIF(g_COMM='S' AND .w_OLTFLIMC="S" AND NOT EMPTY(.w_OLTCOCOS), IIF(.w_FLCOMM='I','+', IIF(.w_FLCOMM='D','-',' ')),' ')
        .w_OLTFCOCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" AND NOT EMPTY(.w_OLTCOCOS), IIF(.w_FLCOMM='C','+', IIF(.w_FLCOMM='S','-',' ')),' ')
          .DoRTCalc(108,108,.f.)
        .w_TESTFORM = .T.
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_105.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_106.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
          .DoRTCalc(110,112,.f.)
        .w_TDINRIC = .w_OLTDINRIC
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .w_CHKDETT = .F.
          .DoRTCalc(115,115,.f.)
        .w_OLDODP = LEFT(.w_OLTCODIC+SPACE(20),20)+LEFT(.w_OLTUNMIS+'   ',3)+STR(.w_OLTQTODL,12,3)+DTOS(.w_OLTDINRIC)
          .DoRTCalc(117,118,.f.)
        .w_OSTATO = .w_OLTSTATO
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
          .DoRTCalc(120,121,.f.)
        .w_OLTVARIA = 'S'
          .DoRTCalc(123,123,.f.)
        .w_CALCOLA = ' '
        .w_DBCODRIS = .w_OLTCICLO
          .DoRTCalc(126,141,.f.)
        .w_CHGECONT = .f.
        .w_CALCDATA = .T.
        .w_ERDATINI = .F.
        .w_CALCCONT = .F.
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Lead time di produzione:", "N. gg per appr.:")))
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data inizio produzione:", "Data ordine:")))
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data fine produzione:", "Data evasione:")))
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate(AH_MsgFormat(iif(.w_TIPGES="P","Emissione ODP:",iif(.w_TIPGES="F","Emissione Ricambi:",iif(.w_TIPGES="T","Emissione ODF:",IIF(.w_OLTPROVE="I", "Emissione ODL:", IIF(.w_OLTPROVE="L", "Emissione OCL:" , "Emissione ODA:")))))))
          .DoRTCalc(146,148,.f.)
        .w_OLCRIFOR = "M"
          .DoRTCalc(150,150,.f.)
        .w_OLTFLSUG = iif(.w_OLTSTATO="S","+"," ")
        .w_OLTFLCON = iif(.w_OLTSTATO="C" and .w_TIPGES<>'T',"+"," ")
        .w_OLTFLDAP = iif(.w_OLTSTATO="D" or (.w_OLTSTATO="C" and .w_TIPGES="T"),"+"," ")
        .w_OLTFLPIA = iif(.w_OLTSTATO $ "MP","+"," ")
        .w_OLTFLLAN = iif(.w_OLTSTATO="L","+"," ")
        .oPgFrm.Page1.oPag.oObj_1_157.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_159.Calculate()
          .DoRTCalc(156,159,.f.)
        .w_OLTCRELA = 'N'
        .DoRTCalc(161,164,.f.)
          if not(empty(.w_OLTDISBA))
          .link_1_170('Full')
          endif
          .DoRTCalc(165,165,.f.)
        .w_OLTSECIC = IIF(.w_OLTPROVE<>'E' , .w_OLTSECIC, SPACE(10))
        .DoRTCalc(166,166,.f.)
          if not(empty(.w_OLTSECIC))
          .link_1_172('Full')
          endif
        .DoRTCalc(167,169,.f.)
          if not(empty(.w_OLTSEODL))
          .link_1_178('Full')
          endif
          .DoRTCalc(170,171,.f.)
        .w_HAILCIC = .F.
        .oPgFrm.Page1.oPag.oObj_1_184.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
          .DoRTCalc(173,173,.f.)
        .w_OLVARCIC = 'N'
          .DoRTCalc(175,177,.f.)
        .w_OLFLMODC = .w_OLFLMODO
        .DoRTCalc(179,179,.f.)
          if not(empty(.w_OLTSECPR))
          .link_1_192('Full')
          endif
          .DoRTCalc(180,182,.f.)
        .w_DELOCLFA = False
          .DoRTCalc(184,187,.f.)
        .w_OLCODVAL = IIF(empty(.w_OLCODVAL), g_PERVAL, .w_OLCODVAL)
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
        .oPgFrm.Page9.oPag.TREEV.Calculate()
          .DoRTCalc(189,190,.f.)
        .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
        .w_TMPTYPE = .w_TREEV.getvar("TIPO")
        .w_ST = .w_TREEV.getvar("OLTSTATO")
        .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
        .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
        .w_FLVEAC = 'T'
        .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
        .w_STATO = AH_MsgFormat(iif(.w_ST="M","Suggerito",iif(.w_ST="P","Pianificato",iif(.w_ST="L",iif(.w_Type="ODL","Lanciato","Ordinato"),iif(.w_ST="C","Confermato",iif(.w_ST="D","Da pianificare",iif(.w_ST="S","Suggerito",iif(.w_ST="F","Finito"," "))))))))
        .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
        .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
        .DoRTCalc(200,200,.f.)
          if not(empty(.w_CODODL2))
          .link_9_11('Full')
          endif
          .DoRTCalc(201,201,.f.)
        .w_STATOR = iif(.w_TYPE <> 'DPI',iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' ,'Confermato','')),'')
        .w_FirstTime = .T.
        .w_COD3 = .w_TREEV.getvar("OLTCODIC ")
        .w_RIF2 = .w_TREEV.getvar("MVNUMDOC ")
        .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
        .w_RIF3 = iif(.w_type$'DIM-DIR',.w_TREEV.getvar("ardesart "),' ')
          .DoRTCalc(208,208,.f.)
        .w_NUMINI = 0
        .w_SERIE1 = ''
        .w_NUMFIN = 999999
        .w_SERIE2 = ''
          .DoRTCalc(213,214,.f.)
        .w_NUMREI = 1
        .w_NUMREF = 999999
          .DoRTCalc(217,221,.f.)
        .w_FLEVAS = .w_TREEV.getvar("OLTFLEVA")
        .w_FLVEAC1 = .w_TREEV.getvar("MVFLVEAC")
          .DoRTCalc(224,238,.f.)
        .w_SELODL = "T"
        .w_CATDOC = "XX"
          .DoRTCalc(241,242,.f.)
        .w_SELOCL = "T"
        .w_FORSEL = ' '
        .w_TIPFOR = 'F'
        .w_TIPDOC = ' '
        .w_SELODA = "T"
        .oPgFrm.Page9.oPag.oObj_9_59.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_60.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_61.Calculate()
        .w_OLCODINI = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        .w_OLCODFIN = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        .oPgFrm.Page9.oPag.oObj_9_64.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_65.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_66.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_67.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_68.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_69.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_70.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_71.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_72.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_73.Calculate()
        .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
        .DoRTCalc(250,250,.f.)
          if not(empty(.w_CODRIC))
          .link_9_74('Full')
          endif
          .DoRTCalc(251,251,.f.)
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_DATEMS = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC "),cp_CharToDate('  -  -  '))
        .w_CAUMAG = .w_TREEV.getvar("MVCAUMAG")
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_QTASCA = nvl(.w_TREEV.getvar("OLTQTOBF"),0)
        .w_QTARES = iif((nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) )>0,nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0)
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_RIF = .w_TREEV.getvar("PADRE")
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .oPgFrm.Page9.oPag.StatoODL.Calculate(.w_STATO)
        .w_UNIMIS1 = nvl(.w_TREEV.getvar("OLTUNMIS"),' ')
        .w_CODODL_ = IIF(.w_type='ODL',.w_TREEV.getvar("OLCODODL"),.w_TREEV.getvar("PADRE"))
        .w_CPROWNUM = nvl(.w_TREEV.getvar("CPROWNUM"),0)
        .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
        .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .oPgFrm.Page9.oPag.StatoDOC.Calculate(.w_STATOR)
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
        .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
        .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
        .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
        .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
        .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
        .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
        .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
        .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
          .DoRTCalc(295,296,.f.)
        .w_PV = .w_TREEV.getvar("OLTPROVE")
        .w_PROVE = iif(.w_TYPE='ODF',iif(.w_PV='I','Fase interna',iif(.w_PV='L','Fase esterna','')),'')
        .oPgFrm.Page9.oPag.ProvODLF.Calculate(.w_PROVE)
        .oPgFrm.Page8.oPag.ZPF.Calculate()
        .oPgFrm.Page8.oPag.ZFP.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_5.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_6.Calculate()
          .DoRTCalc(299,299,.f.)
        .w_OLTQTOS1 = CALQTAADV(.w_OLTQTOSC,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N', '', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTOSC")
          .DoRTCalc(301,301,.f.)
        .w_OLTQTPR1 = CALQTAADV(.w_OLTQTPRO,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N', '', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTPRO")
      endif
    endwith
    cp_BlankRecExtFlds(this,'ODL_MAST')
    this.DoRTCalc(303,306,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsco_aop
        * --- Chiusura Cursore OCL di fase da eliminare e resetto variabile
        this.w_bOclfadadel=.f.
        Use in Select(this.w_Cur_Del_Ocl)
        Use in Select(this.w_Cur_Cic_Odl)
    endproc
    
    proc oPgFrmClick()
    
    		If This.oPgFrm.nPageActive = This.oPgFrm.ActivePage
    			Local i_nZoomPage
          i_nZoomPage=This.oPgFrm.ActivePage
           if Type('this.oPgFrm.pages(i_nZoomPage).autozoom')="O"
    					This.oPgFrm.Pages(i_nZoomPage).autozoom.cZoomOnZoom = "GSCO_AOP(.NULL.,'"+ THIS.pFLGEST + "')" && This.getsecuritycode()
              UNBINDEVENTS(This.oPgFrm, "Click", This, "oPgFrmClick")
          endif
        endif
        
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRODL","i_CODAZI,w_OLCODODL")
      .op_CODAZI = .w_CODAZI
      .op_OLCODODL = .w_OLCODODL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oOLCODODL_1_8.enabled = i_bVal
      .Page1.oPag.oOLTSTATO_1_13.enabled = i_bVal
      .Page1.oPag.oOLTSTATO_1_14.enabled = i_bVal
      .Page1.oPag.oOLTSTATO_1_15.enabled = i_bVal
      .Page1.oPag.oOLTCODIC_1_21.enabled = i_bVal
      .Page1.oPag.oOLTPROVE_1_34.enabled = i_bVal
      .Page1.oPag.oOLTCOMAG_1_50.enabled = i_bVal
      .Page1.oPag.oOLTUNMIS_1_67.enabled = i_bVal
      .Page1.oPag.oOLTDTCON_1_68.enabled = i_bVal
      .Page1.oPag.oOLTQTODL_1_70.enabled = i_bVal
      .Page1.oPag.oOLTDTRIC_1_88.enabled = i_bVal
      .Page1.oPag.oOLTDINRIC_1_89.enabled = i_bVal
      .Page2.oPag.oOLTCICLO_2_3.enabled = i_bVal
      .Page2.oPag.oOLTCOFOR_2_13.enabled = i_bVal
      .Page2.oPag.oOLTCONTR_2_16.enabled = i_bVal
      .Page2.oPag.oOLTCOCEN_2_19.enabled = i_bVal
      .Page2.oPag.oOLTVOCEN_2_22.enabled = i_bVal
      .Page2.oPag.oOLTCOMME_2_26.enabled = i_bVal
      .Page2.oPag.oOLTCOATT_2_28.enabled = i_bVal
      .Page2.oPag.oOLCRIFOR_2_64.enabled = i_bVal
      .Page1.oPag.oOLTDISBA_1_170.enabled = i_bVal
      .Page1.oPag.oOLTSECIC_1_172.enabled = i_bVal
      .Page1.oPag.oOLTSEODL_1_178.enabled = i_bVal
      .Page1.oPag.oOLTSECPR_1_192.enabled = i_bVal
      .Page10.oPag.oOL__NOTE_10_1.enabled = i_bVal
      .Page1.oPag.oOLTPRIOR_1_214.enabled = i_bVal
      .Page1.oPag.oOLTSOSPE_1_216.enabled = i_bVal
      .Page1.oPag.oBtn_1_117.enabled = i_bVal
      .Page1.oPag.oBtn_1_118.enabled = i_bVal
      .Page1.oPag.oObj_1_101.enabled = i_bVal
      .Page1.oPag.oObj_1_102.enabled = i_bVal
      .Page1.oPag.oObj_1_105.enabled = i_bVal
      .Page1.oPag.oObj_1_106.enabled = i_bVal
      .Page1.oPag.oObj_1_107.enabled = i_bVal
      .Page1.oPag.oObj_1_108.enabled = i_bVal
      .Page1.oPag.oObj_1_109.enabled = i_bVal
      .Page1.oPag.oObj_1_110.enabled = i_bVal
      .Page1.oPag.oObj_1_112.enabled = i_bVal
      .Page1.oPag.oObj_1_113.enabled = i_bVal
      .Page1.oPag.oObj_1_114.enabled = i_bVal
      .Page1.oPag.oObj_1_115.enabled = i_bVal
      .Page1.oPag.oObj_1_116.enabled = i_bVal
      .Page1.oPag.oObj_1_123.enabled = i_bVal
      .Page1.oPag.oObj_1_157.enabled = i_bVal
      .Page1.oPag.oObj_1_159.enabled = i_bVal
      .Page1.oPag.oObj_1_184.enabled = i_bVal
      .Page1.oPag.oObj_1_185.enabled = i_bVal
      .Page2.oPag.oObj_2_67.enabled = i_bVal
      .Page9.oPag.TREEV.enabled = i_bVal
      .Page9.oPag.oObj_9_59.enabled = i_bVal
      .Page9.oPag.oObj_9_60.enabled = i_bVal
      .Page9.oPag.oObj_9_61.enabled = i_bVal
      .Page9.oPag.oObj_9_64.enabled = i_bVal
      .Page9.oPag.oObj_9_65.enabled = i_bVal
      .Page9.oPag.oObj_9_66.enabled = i_bVal
      .Page9.oPag.oObj_9_67.enabled = i_bVal
      .Page9.oPag.oObj_9_68.enabled = i_bVal
      .Page9.oPag.oObj_9_69.enabled = i_bVal
      .Page9.oPag.oObj_9_70.enabled = i_bVal
      .Page9.oPag.oObj_9_71.enabled = i_bVal
      .Page9.oPag.oObj_9_72.enabled = i_bVal
      .Page9.oPag.oObj_9_73.enabled = i_bVal
      .Page8.oPag.oObj_8_5.enabled = i_bVal
      .Page8.oPag.oObj_8_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oOLCODODL_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oOLCODODL_1_8.enabled = .t.
        .Page1.oPag.oOLTCODIC_1_21.enabled = .t.
      endif
    endwith
    this.GSCO_MOL.SetStatus(i_cOp)
    this.GSCO_MCS.SetStatus(i_cOp)
    this.GSCO_MCL.SetStatus(i_cOp)
    this.GSCO_MRF.SetStatus(i_cOp)
    this.GSCO_MMO.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ODL_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsco_aop
    this.w_STATOGES=UPPER(this.cFunction)
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCO_MOL.SetChildrenStatus(i_cOp)
  *  this.GSCO_MCS.SetChildrenStatus(i_cOp)
  *  this.GSCO_MCL.SetChildrenStatus(i_cOp)
  *  this.GSCO_MRF.SetChildrenStatus(i_cOp)
  *  this.GSCO_MMO.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLCODODL,"OLCODODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLDATODP,"OLDATODP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLDATODL,"OLDATODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSTATO,"OLTSTATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSTATO,"OLTSTATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSTATO,"OLTSTATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFAODL,"OLTFAODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLOPEODP,"OLOPEODP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLOPEODL,"OLOPEODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCODIC,"OLTCODIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOART,"OLTCOART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTPROVE,"OLTPROVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOMAG,"OLTCOMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCAMAG,"OLTCAMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLORD,"OLTFLORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLIMP,"OLTFLIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTKEYSA,"OLTKEYSA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTUNMIS,"OLTUNMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTCON,"OLTDTCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTODL,"OLTQTODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTOEV,"OLTQTOEV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTOD1,"OLTQTOD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTOE1,"OLTQTOE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLEVA,"OLTFLEVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTSAL,"OLTQTSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOMAG,"OLTCOMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTLEMPS,"OLTLEMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTMPS,"OLTDTMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTLAN,"OLTDTLAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTEMLAV,"OLTEMLAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTRIC,"OLTDTRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDINRIC,"OLTDINRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTINI,"OLTDTINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDTFIN,"OLTDTFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTPERAS,"OLTPERAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTTIPAT,"OLTTIPAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDISBA,"OLTDISBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCICLO,"OLTCICLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTTICON,"OLTTICON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOFOR,"OLTCOFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCONTR,"OLTCONTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOCEN,"OLTCOCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTVOCEN,"OLTVOCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOMME,"OLTCOMME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOATT,"OLTCOATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLIMC,"OLTFLIMC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTIMCOM,"OLTIMCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSEDOC,"OLTSEDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCPDOC,"OLTCPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLSTAODL,"OLSTAODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOCOS,"OLTCOCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFORCO,"OLTFORCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFCOCO,"OLTFCOCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTVARIA,"OLTVARIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLCRIFOR,"OLCRIFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLSUG,"OLTFLSUG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLCON,"OLTFLCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLDAP,"OLTFLDAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLPIA,"OLTFLPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFLLAN,"OLTFLLAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTMSPINI,"OLTMSPINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTMSPFIN,"OLTMSPFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCRELA,"OLTCRELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSAFLT,"OLTSAFLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLSERMRP,"OLSERMRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTDISBA,"OLTDISBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSECIC,"OLTSECIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSEODL,"OLTSEODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSECPR,"OLTSECPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTCOPOI,"OLTCOPOI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTFAOUT,"OLTFAOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTULFAS,"OLTULFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTMGWIP,"OLTMGWIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OL__NOTE,"OL__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTOSC,"OLTQTOSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTOS1,"OLTQTOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTPRO,"OLTQTPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTQTPR1,"OLTQTPR1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTPRIOR,"OLTPRIOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OLTSOSPE,"OLTSOSPE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsco_aop
    * --- aggiunge alla Chiave ulteriore filtro sulla Provenienza
    * --- l_Prove viene impostato a blank per gli ODP/ODR
    local l_Prove
    l_Prove=''
    IF NOT EMPTY(i_cWhere)
       l_Prove = IIF(this.pFlGEST='L',"'I'", IIF(this.pFlGEST='Z',"'L'",IIF(this.pFlGEST='S',"'S'",IIF(this.pFlGEST='E',"'E'",""))))
       i_cWhere=i_cWhere + IIF(empty(l_Prove), '', " and OLTPROVE="+ l_Prove)
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    i_lTable = "ODL_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ODL_MAST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PRODL","i_CODAZI,w_OLCODODL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ODL_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ODL_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'ODL_MAST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(OLCODODL,OLDATODP,OLDATODL,OLTSTATO,OLTFAODL"+;
                  ",OLOPEODP,OLOPEODL,OLTCODIC,OLTCOART,OLTPROVE"+;
                  ",OLTCOMAG,UTCC,UTCV,UTDC,UTDV"+;
                  ",OLTCAMAG,OLTFLORD,OLTFLIMP,OLTKEYSA,OLTUNMIS"+;
                  ",OLTDTCON,OLTQTODL,OLTQTOEV,OLTQTOD1,OLTQTOE1"+;
                  ",OLTFLEVA,OLTQTSAL,OLTLEMPS,OLTDTMPS,OLTDTLAN"+;
                  ",OLTEMLAV,OLTDTRIC,OLTDINRIC,OLTDTINI,OLTDTFIN"+;
                  ",OLTPERAS,OLTTIPAT,OLTCICLO,OLTTICON,OLTCOFOR"+;
                  ",OLTCONTR,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT"+;
                  ",OLTFLIMC,OLTIMCOM,OLTSEDOC,OLTCPDOC,OLSTAODL"+;
                  ",OLTCOCOS,OLTFORCO,OLTFCOCO,OLTVARIA,OLCRIFOR"+;
                  ",OLTFLSUG,OLTFLCON,OLTFLDAP,OLTFLPIA,OLTFLLAN"+;
                  ",OLTMSPINI,OLTMSPFIN,OLTCRELA,OLTSAFLT,OLSERMRP"+;
                  ",OLTDISBA,OLTSECIC,OLTSEODL,OLTSECPR,OLTCOPOI"+;
                  ",OLTFAOUT,OLTULFAS,OLTMGWIP,OL__NOTE,OLTQTOSC"+;
                  ",OLTQTOS1,OLTQTPRO,OLTQTPR1,OLTPRIOR,OLTSOSPE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_OLCODODL)+;
                  ","+cp_ToStrODBC(this.w_OLDATODP)+;
                  ","+cp_ToStrODBC(this.w_OLDATODL)+;
                  ","+cp_ToStrODBC(this.w_OLTSTATO)+;
                  ","+cp_ToStrODBCNull(this.w_OLTFAODL)+;
                  ","+cp_ToStrODBC(this.w_OLOPEODP)+;
                  ","+cp_ToStrODBC(this.w_OLOPEODL)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCODIC)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOART)+;
                  ","+cp_ToStrODBC(this.w_OLTPROVE)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOMAG)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCAMAG)+;
                  ","+cp_ToStrODBC(this.w_OLTFLORD)+;
                  ","+cp_ToStrODBC(this.w_OLTFLIMP)+;
                  ","+cp_ToStrODBC(this.w_OLTKEYSA)+;
                  ","+cp_ToStrODBCNull(this.w_OLTUNMIS)+;
                  ","+cp_ToStrODBC(this.w_OLTDTCON)+;
                  ","+cp_ToStrODBC(this.w_OLTQTODL)+;
                  ","+cp_ToStrODBC(this.w_OLTQTOEV)+;
                  ","+cp_ToStrODBC(this.w_OLTQTOD1)+;
                  ","+cp_ToStrODBC(this.w_OLTQTOE1)+;
                  ","+cp_ToStrODBC(this.w_OLTFLEVA)+;
                  ","+cp_ToStrODBC(this.w_OLTQTSAL)+;
                  ","+cp_ToStrODBC(this.w_OLTLEMPS)+;
                  ","+cp_ToStrODBC(this.w_OLTDTMPS)+;
                  ","+cp_ToStrODBC(this.w_OLTDTLAN)+;
                  ","+cp_ToStrODBC(this.w_OLTEMLAV)+;
                  ","+cp_ToStrODBC(this.w_OLTDTRIC)+;
                  ","+cp_ToStrODBC(this.w_OLTDINRIC)+;
                  ","+cp_ToStrODBC(this.w_OLTDTINI)+;
                  ","+cp_ToStrODBC(this.w_OLTDTFIN)+;
                  ","+cp_ToStrODBC(this.w_OLTPERAS)+;
                  ","+cp_ToStrODBC(this.w_OLTTIPAT)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCICLO)+;
                  ","+cp_ToStrODBC(this.w_OLTTICON)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOFOR)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCONTR)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOCEN)+;
                  ","+cp_ToStrODBCNull(this.w_OLTVOCEN)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOMME)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOATT)+;
                  ","+cp_ToStrODBC(this.w_OLTFLIMC)+;
                  ","+cp_ToStrODBC(this.w_OLTIMCOM)+;
                  ","+cp_ToStrODBC(this.w_OLTSEDOC)+;
                  ","+cp_ToStrODBC(this.w_OLTCPDOC)+;
                  ","+cp_ToStrODBC(this.w_OLSTAODL)+;
                  ","+cp_ToStrODBCNull(this.w_OLTCOCOS)+;
                  ","+cp_ToStrODBC(this.w_OLTFORCO)+;
                  ","+cp_ToStrODBC(this.w_OLTFCOCO)+;
                  ","+cp_ToStrODBC(this.w_OLTVARIA)+;
                  ","+cp_ToStrODBC(this.w_OLCRIFOR)+;
                  ","+cp_ToStrODBC(this.w_OLTFLSUG)+;
                  ","+cp_ToStrODBC(this.w_OLTFLCON)+;
                  ","+cp_ToStrODBC(this.w_OLTFLDAP)+;
                  ","+cp_ToStrODBC(this.w_OLTFLPIA)+;
                  ","+cp_ToStrODBC(this.w_OLTFLLAN)+;
                  ","+cp_ToStrODBC(this.w_OLTMSPINI)+;
                  ","+cp_ToStrODBC(this.w_OLTMSPFIN)+;
                  ","+cp_ToStrODBC(this.w_OLTCRELA)+;
                  ","+cp_ToStrODBC(this.w_OLTSAFLT)+;
                  ","+cp_ToStrODBC(this.w_OLSERMRP)+;
                  ","+cp_ToStrODBCNull(this.w_OLTDISBA)+;
                  ","+cp_ToStrODBCNull(this.w_OLTSECIC)+;
                  ","+cp_ToStrODBCNull(this.w_OLTSEODL)+;
                  ","+cp_ToStrODBCNull(this.w_OLTSECPR)+;
                  ","+cp_ToStrODBC(this.w_OLTCOPOI)+;
                  ","+cp_ToStrODBC(this.w_OLTFAOUT)+;
                  ","+cp_ToStrODBC(this.w_OLTULFAS)+;
                  ","+cp_ToStrODBC(this.w_OLTMGWIP)+;
                  ","+cp_ToStrODBC(this.w_OL__NOTE)+;
                  ","+cp_ToStrODBC(this.w_OLTQTOSC)+;
                  ","+cp_ToStrODBC(this.w_OLTQTOS1)+;
                  ","+cp_ToStrODBC(this.w_OLTQTPRO)+;
                  ","+cp_ToStrODBC(this.w_OLTQTPR1)+;
                  ","+cp_ToStrODBC(this.w_OLTPRIOR)+;
                  ","+cp_ToStrODBC(this.w_OLTSOSPE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ODL_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'ODL_MAST')
        cp_CheckDeletedKey(i_cTable,0,'OLCODODL',this.w_OLCODODL)
        INSERT INTO (i_cTable);
              (OLCODODL,OLDATODP,OLDATODL,OLTSTATO,OLTFAODL,OLOPEODP,OLOPEODL,OLTCODIC,OLTCOART,OLTPROVE,OLTCOMAG,UTCC,UTCV,UTDC,UTDV,OLTCAMAG,OLTFLORD,OLTFLIMP,OLTKEYSA,OLTUNMIS,OLTDTCON,OLTQTODL,OLTQTOEV,OLTQTOD1,OLTQTOE1,OLTFLEVA,OLTQTSAL,OLTLEMPS,OLTDTMPS,OLTDTLAN,OLTEMLAV,OLTDTRIC,OLTDINRIC,OLTDTINI,OLTDTFIN,OLTPERAS,OLTTIPAT,OLTCICLO,OLTTICON,OLTCOFOR,OLTCONTR,OLTCOCEN,OLTVOCEN,OLTCOMME,OLTCOATT,OLTFLIMC,OLTIMCOM,OLTSEDOC,OLTCPDOC,OLSTAODL,OLTCOCOS,OLTFORCO,OLTFCOCO,OLTVARIA,OLCRIFOR,OLTFLSUG,OLTFLCON,OLTFLDAP,OLTFLPIA,OLTFLLAN,OLTMSPINI,OLTMSPFIN,OLTCRELA,OLTSAFLT,OLSERMRP,OLTDISBA,OLTSECIC,OLTSEODL,OLTSECPR,OLTCOPOI,OLTFAOUT,OLTULFAS,OLTMGWIP,OL__NOTE,OLTQTOSC,OLTQTOS1,OLTQTPRO,OLTQTPR1,OLTPRIOR,OLTSOSPE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_OLCODODL;
                  ,this.w_OLDATODP;
                  ,this.w_OLDATODL;
                  ,this.w_OLTSTATO;
                  ,this.w_OLTFAODL;
                  ,this.w_OLOPEODP;
                  ,this.w_OLOPEODL;
                  ,this.w_OLTCODIC;
                  ,this.w_OLTCOART;
                  ,this.w_OLTPROVE;
                  ,this.w_OLTCOMAG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_OLTCAMAG;
                  ,this.w_OLTFLORD;
                  ,this.w_OLTFLIMP;
                  ,this.w_OLTKEYSA;
                  ,this.w_OLTUNMIS;
                  ,this.w_OLTDTCON;
                  ,this.w_OLTQTODL;
                  ,this.w_OLTQTOEV;
                  ,this.w_OLTQTOD1;
                  ,this.w_OLTQTOE1;
                  ,this.w_OLTFLEVA;
                  ,this.w_OLTQTSAL;
                  ,this.w_OLTLEMPS;
                  ,this.w_OLTDTMPS;
                  ,this.w_OLTDTLAN;
                  ,this.w_OLTEMLAV;
                  ,this.w_OLTDTRIC;
                  ,this.w_OLTDINRIC;
                  ,this.w_OLTDTINI;
                  ,this.w_OLTDTFIN;
                  ,this.w_OLTPERAS;
                  ,this.w_OLTTIPAT;
                  ,this.w_OLTCICLO;
                  ,this.w_OLTTICON;
                  ,this.w_OLTCOFOR;
                  ,this.w_OLTCONTR;
                  ,this.w_OLTCOCEN;
                  ,this.w_OLTVOCEN;
                  ,this.w_OLTCOMME;
                  ,this.w_OLTCOATT;
                  ,this.w_OLTFLIMC;
                  ,this.w_OLTIMCOM;
                  ,this.w_OLTSEDOC;
                  ,this.w_OLTCPDOC;
                  ,this.w_OLSTAODL;
                  ,this.w_OLTCOCOS;
                  ,this.w_OLTFORCO;
                  ,this.w_OLTFCOCO;
                  ,this.w_OLTVARIA;
                  ,this.w_OLCRIFOR;
                  ,this.w_OLTFLSUG;
                  ,this.w_OLTFLCON;
                  ,this.w_OLTFLDAP;
                  ,this.w_OLTFLPIA;
                  ,this.w_OLTFLLAN;
                  ,this.w_OLTMSPINI;
                  ,this.w_OLTMSPFIN;
                  ,this.w_OLTCRELA;
                  ,this.w_OLTSAFLT;
                  ,this.w_OLSERMRP;
                  ,this.w_OLTDISBA;
                  ,this.w_OLTSECIC;
                  ,this.w_OLTSEODL;
                  ,this.w_OLTSECPR;
                  ,this.w_OLTCOPOI;
                  ,this.w_OLTFAOUT;
                  ,this.w_OLTULFAS;
                  ,this.w_OLTMGWIP;
                  ,this.w_OL__NOTE;
                  ,this.w_OLTQTOSC;
                  ,this.w_OLTQTOS1;
                  ,this.w_OLTQTPRO;
                  ,this.w_OLTQTPR1;
                  ,this.w_OLTPRIOR;
                  ,this.w_OLTSOSPE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsco_aop
    this.w_AGGOCLFA = .F.
    * Quando viene variato un ODL suggeritoMRP => diventa pianificato
    if this.w_OLTSTATO="M"
       this.w_OLTSTATO = "P"
       this.w_AGGOCLFA = (g_PRFA="S" and g_CICLILAV="S")
    endif
    * Metto a posto i flag dei saldi MPS
    this.w_OLTFLSUG = iif(this.w_OLTSTATO="S","+"," ")
    this.w_OLTFLCON = iif(this.w_TIPGES<>"T", iif(this.w_OLTSTATO="C","+"," ")," ")
    this.w_OLTFLDAP = iif(this.w_TIPGES<>"T", iif(this.w_OLTSTATO="D","+"," "),iif(this.w_OLTSTATO="C","+"," "))
    this.w_OLTFLPIA = iif(this.w_OLTSTATO $ "MP","+"," ")
    this.w_OLTFLLAN = iif(this.w_OLTSTATO="L","+"," ")
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ODL_MAST_IDX,i_nConn)
      *
      * update ODL_MAST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ODL_MAST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " OLDATODP="+cp_ToStrODBC(this.w_OLDATODP)+;
             ",OLDATODL="+cp_ToStrODBC(this.w_OLDATODL)+;
             ",OLTSTATO="+cp_ToStrODBC(this.w_OLTSTATO)+;
             ",OLTFAODL="+cp_ToStrODBCNull(this.w_OLTFAODL)+;
             ",OLOPEODP="+cp_ToStrODBC(this.w_OLOPEODP)+;
             ",OLOPEODL="+cp_ToStrODBC(this.w_OLOPEODL)+;
             ",OLTCODIC="+cp_ToStrODBCNull(this.w_OLTCODIC)+;
             ",OLTCOART="+cp_ToStrODBCNull(this.w_OLTCOART)+;
             ",OLTPROVE="+cp_ToStrODBC(this.w_OLTPROVE)+;
             ",OLTCOMAG="+cp_ToStrODBCNull(this.w_OLTCOMAG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",OLTCAMAG="+cp_ToStrODBCNull(this.w_OLTCAMAG)+;
             ",OLTFLORD="+cp_ToStrODBC(this.w_OLTFLORD)+;
             ",OLTFLIMP="+cp_ToStrODBC(this.w_OLTFLIMP)+;
             ",OLTKEYSA="+cp_ToStrODBC(this.w_OLTKEYSA)+;
             ",OLTUNMIS="+cp_ToStrODBCNull(this.w_OLTUNMIS)+;
             ",OLTDTCON="+cp_ToStrODBC(this.w_OLTDTCON)+;
             ",OLTQTODL="+cp_ToStrODBC(this.w_OLTQTODL)+;
             ",OLTQTOEV="+cp_ToStrODBC(this.w_OLTQTOEV)+;
             ",OLTQTOD1="+cp_ToStrODBC(this.w_OLTQTOD1)+;
             ",OLTQTOE1="+cp_ToStrODBC(this.w_OLTQTOE1)+;
             ",OLTFLEVA="+cp_ToStrODBC(this.w_OLTFLEVA)+;
             ",OLTQTSAL="+cp_ToStrODBC(this.w_OLTQTSAL)+;
             ",OLTLEMPS="+cp_ToStrODBC(this.w_OLTLEMPS)+;
             ",OLTDTMPS="+cp_ToStrODBC(this.w_OLTDTMPS)+;
             ",OLTDTLAN="+cp_ToStrODBC(this.w_OLTDTLAN)+;
             ",OLTEMLAV="+cp_ToStrODBC(this.w_OLTEMLAV)+;
             ",OLTDTRIC="+cp_ToStrODBC(this.w_OLTDTRIC)+;
             ",OLTDINRIC="+cp_ToStrODBC(this.w_OLTDINRIC)+;
             ",OLTDTINI="+cp_ToStrODBC(this.w_OLTDTINI)+;
             ",OLTDTFIN="+cp_ToStrODBC(this.w_OLTDTFIN)+;
             ",OLTPERAS="+cp_ToStrODBC(this.w_OLTPERAS)+;
             ",OLTTIPAT="+cp_ToStrODBC(this.w_OLTTIPAT)+;
             ",OLTCICLO="+cp_ToStrODBCNull(this.w_OLTCICLO)+;
             ",OLTTICON="+cp_ToStrODBC(this.w_OLTTICON)+;
             ",OLTCOFOR="+cp_ToStrODBCNull(this.w_OLTCOFOR)+;
             ",OLTCONTR="+cp_ToStrODBCNull(this.w_OLTCONTR)+;
             ",OLTCOCEN="+cp_ToStrODBCNull(this.w_OLTCOCEN)+;
             ",OLTVOCEN="+cp_ToStrODBCNull(this.w_OLTVOCEN)+;
             ",OLTCOMME="+cp_ToStrODBCNull(this.w_OLTCOMME)+;
             ",OLTCOATT="+cp_ToStrODBCNull(this.w_OLTCOATT)+;
             ",OLTFLIMC="+cp_ToStrODBC(this.w_OLTFLIMC)+;
             ",OLTIMCOM="+cp_ToStrODBC(this.w_OLTIMCOM)+;
             ",OLTSEDOC="+cp_ToStrODBC(this.w_OLTSEDOC)+;
             ",OLTCPDOC="+cp_ToStrODBC(this.w_OLTCPDOC)+;
             ",OLSTAODL="+cp_ToStrODBC(this.w_OLSTAODL)+;
             ",OLTCOCOS="+cp_ToStrODBCNull(this.w_OLTCOCOS)+;
             ",OLTFORCO="+cp_ToStrODBC(this.w_OLTFORCO)+;
             ",OLTFCOCO="+cp_ToStrODBC(this.w_OLTFCOCO)+;
             ",OLTVARIA="+cp_ToStrODBC(this.w_OLTVARIA)+;
             ",OLCRIFOR="+cp_ToStrODBC(this.w_OLCRIFOR)+;
             ",OLTFLSUG="+cp_ToStrODBC(this.w_OLTFLSUG)+;
             ",OLTFLCON="+cp_ToStrODBC(this.w_OLTFLCON)+;
             ",OLTFLDAP="+cp_ToStrODBC(this.w_OLTFLDAP)+;
             ",OLTFLPIA="+cp_ToStrODBC(this.w_OLTFLPIA)+;
             ",OLTFLLAN="+cp_ToStrODBC(this.w_OLTFLLAN)+;
             ",OLTMSPINI="+cp_ToStrODBC(this.w_OLTMSPINI)+;
             ",OLTMSPFIN="+cp_ToStrODBC(this.w_OLTMSPFIN)+;
             ",OLTCRELA="+cp_ToStrODBC(this.w_OLTCRELA)+;
             ",OLTSAFLT="+cp_ToStrODBC(this.w_OLTSAFLT)+;
             ",OLSERMRP="+cp_ToStrODBC(this.w_OLSERMRP)+;
             ",OLTDISBA="+cp_ToStrODBCNull(this.w_OLTDISBA)+;
             ",OLTSECIC="+cp_ToStrODBCNull(this.w_OLTSECIC)+;
             ",OLTSEODL="+cp_ToStrODBCNull(this.w_OLTSEODL)+;
             ",OLTSECPR="+cp_ToStrODBCNull(this.w_OLTSECPR)+;
             ",OLTCOPOI="+cp_ToStrODBC(this.w_OLTCOPOI)+;
             ",OLTFAOUT="+cp_ToStrODBC(this.w_OLTFAOUT)+;
             ",OLTULFAS="+cp_ToStrODBC(this.w_OLTULFAS)+;
             ",OLTMGWIP="+cp_ToStrODBC(this.w_OLTMGWIP)+;
             ",OL__NOTE="+cp_ToStrODBC(this.w_OL__NOTE)+;
             ",OLTQTOSC="+cp_ToStrODBC(this.w_OLTQTOSC)+;
             ",OLTQTOS1="+cp_ToStrODBC(this.w_OLTQTOS1)+;
             ",OLTQTPRO="+cp_ToStrODBC(this.w_OLTQTPRO)+;
             ",OLTQTPR1="+cp_ToStrODBC(this.w_OLTQTPR1)+;
             ",OLTPRIOR="+cp_ToStrODBC(this.w_OLTPRIOR)+;
             ",OLTSOSPE="+cp_ToStrODBC(this.w_OLTSOSPE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ODL_MAST')
        i_cWhere = cp_PKFox(i_cTable  ,'OLCODODL',this.w_OLCODODL  )
        UPDATE (i_cTable) SET;
              OLDATODP=this.w_OLDATODP;
             ,OLDATODL=this.w_OLDATODL;
             ,OLTSTATO=this.w_OLTSTATO;
             ,OLTFAODL=this.w_OLTFAODL;
             ,OLOPEODP=this.w_OLOPEODP;
             ,OLOPEODL=this.w_OLOPEODL;
             ,OLTCODIC=this.w_OLTCODIC;
             ,OLTCOART=this.w_OLTCOART;
             ,OLTPROVE=this.w_OLTPROVE;
             ,OLTCOMAG=this.w_OLTCOMAG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,OLTCAMAG=this.w_OLTCAMAG;
             ,OLTFLORD=this.w_OLTFLORD;
             ,OLTFLIMP=this.w_OLTFLIMP;
             ,OLTKEYSA=this.w_OLTKEYSA;
             ,OLTUNMIS=this.w_OLTUNMIS;
             ,OLTDTCON=this.w_OLTDTCON;
             ,OLTQTODL=this.w_OLTQTODL;
             ,OLTQTOEV=this.w_OLTQTOEV;
             ,OLTQTOD1=this.w_OLTQTOD1;
             ,OLTQTOE1=this.w_OLTQTOE1;
             ,OLTFLEVA=this.w_OLTFLEVA;
             ,OLTQTSAL=this.w_OLTQTSAL;
             ,OLTLEMPS=this.w_OLTLEMPS;
             ,OLTDTMPS=this.w_OLTDTMPS;
             ,OLTDTLAN=this.w_OLTDTLAN;
             ,OLTEMLAV=this.w_OLTEMLAV;
             ,OLTDTRIC=this.w_OLTDTRIC;
             ,OLTDINRIC=this.w_OLTDINRIC;
             ,OLTDTINI=this.w_OLTDTINI;
             ,OLTDTFIN=this.w_OLTDTFIN;
             ,OLTPERAS=this.w_OLTPERAS;
             ,OLTTIPAT=this.w_OLTTIPAT;
             ,OLTCICLO=this.w_OLTCICLO;
             ,OLTTICON=this.w_OLTTICON;
             ,OLTCOFOR=this.w_OLTCOFOR;
             ,OLTCONTR=this.w_OLTCONTR;
             ,OLTCOCEN=this.w_OLTCOCEN;
             ,OLTVOCEN=this.w_OLTVOCEN;
             ,OLTCOMME=this.w_OLTCOMME;
             ,OLTCOATT=this.w_OLTCOATT;
             ,OLTFLIMC=this.w_OLTFLIMC;
             ,OLTIMCOM=this.w_OLTIMCOM;
             ,OLTSEDOC=this.w_OLTSEDOC;
             ,OLTCPDOC=this.w_OLTCPDOC;
             ,OLSTAODL=this.w_OLSTAODL;
             ,OLTCOCOS=this.w_OLTCOCOS;
             ,OLTFORCO=this.w_OLTFORCO;
             ,OLTFCOCO=this.w_OLTFCOCO;
             ,OLTVARIA=this.w_OLTVARIA;
             ,OLCRIFOR=this.w_OLCRIFOR;
             ,OLTFLSUG=this.w_OLTFLSUG;
             ,OLTFLCON=this.w_OLTFLCON;
             ,OLTFLDAP=this.w_OLTFLDAP;
             ,OLTFLPIA=this.w_OLTFLPIA;
             ,OLTFLLAN=this.w_OLTFLLAN;
             ,OLTMSPINI=this.w_OLTMSPINI;
             ,OLTMSPFIN=this.w_OLTMSPFIN;
             ,OLTCRELA=this.w_OLTCRELA;
             ,OLTSAFLT=this.w_OLTSAFLT;
             ,OLSERMRP=this.w_OLSERMRP;
             ,OLTDISBA=this.w_OLTDISBA;
             ,OLTSECIC=this.w_OLTSECIC;
             ,OLTSEODL=this.w_OLTSEODL;
             ,OLTSECPR=this.w_OLTSECPR;
             ,OLTCOPOI=this.w_OLTCOPOI;
             ,OLTFAOUT=this.w_OLTFAOUT;
             ,OLTULFAS=this.w_OLTULFAS;
             ,OLTMGWIP=this.w_OLTMGWIP;
             ,OL__NOTE=this.w_OL__NOTE;
             ,OLTQTOSC=this.w_OLTQTOSC;
             ,OLTQTOS1=this.w_OLTQTOS1;
             ,OLTQTPRO=this.w_OLTQTPRO;
             ,OLTQTPR1=this.w_OLTQTPR1;
             ,OLTPRIOR=this.w_OLTPRIOR;
             ,OLTSOSPE=this.w_OLTSOSPE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCO_MOL : Saving
      this.GSCO_MOL.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OLCODODL,"OLCODODL";
             )
      this.GSCO_MOL.mReplace()
      * --- GSCO_MCS : Saving
      this.GSCO_MCS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OLCODODL,"SMCODODL";
             )
      this.GSCO_MCS.mReplace()
      * --- GSCO_MCL : Saving
      this.GSCO_MCL.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OLCODODL,"CLCODODL";
             )
      this.GSCO_MCL.mReplace()
      * --- GSCO_MRF : Saving
      this.GSCO_MRF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OLCODODL,"RFCODODL";
             )
      this.GSCO_MRF.mReplace()
      * --- GSCO_MMO : Saving
      this.GSCO_MMO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_OLCODODL,"MOCODODL";
             )
      this.GSCO_MMO.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsco_aop
    if not(bTrsErr)
        this.NotifyEvent('AggiornaOrdiniDiFase')
        this.NotifyEvent('AggiornaRisorse')
    EndIf
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Transaction
  proc mRestoreTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nConn,i_cTable
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..OLTFLORD,space(1))==this.w_OLTFLORD;
              and NVL(&i_cF..OLTQTSAL,0)==this.w_OLTQTSAL;
              and NVL(&i_cF..OLTFLIMP,space(1))==this.w_OLTFLIMP;
              and NVL(&i_cF..OLTQTSAL,0)==this.w_OLTQTSAL;
              and NVL(&i_cF..OLTCOMAG,space(5))==this.w_OLTCOMAG;
              and NVL(&i_cF..OLTKEYSA,space(40))==this.w_OLTKEYSA;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..OLTFLORD,space(1)),'SLQTOPER','',NVL(&i_cF..OLTQTSAL,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..OLTFLIMP,space(1)),'SLQTIPER','',NVL(&i_cF..OLTQTSAL,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..OLTCOMAG,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLQTOPER="+i_cOp1+","           +" SLQTIPER="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(NVL(&i_cF..OLTCOMAG,space(5)));
             +" AND SLCODICE="+cp_ToStrODBC(NVL(&i_cF..OLTKEYSA,space(40)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..OLTFLORD,'SLQTOPER',i_cF+'.OLTQTSAL',&i_cF..OLTQTSAL,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..OLTFLIMP,'SLQTIPER',i_cF+'.OLTQTSAL',&i_cF..OLTQTSAL,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',&i_cF..OLTKEYSA;
                 ,'SLCODMAG',&i_cF..OLTCOMAG)
      UPDATE (i_cTable) SET ;
           SLQTOPER=&i_cOp1.  ,;
           SLQTIPER=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..OLTFCOCO,space(1))==this.w_OLTFCOCO;
              and NVL(&i_cF..OLTIMCOM,0)==this.w_OLTIMCOM;
              and NVL(&i_cF..OLTFORCO,space(1))==this.w_OLTFORCO;
              and NVL(&i_cF..OLTIMCOM,0)==this.w_OLTIMCOM;
              and NVL(&i_cF..OLTCOCOS,space(5))==this.w_OLTCOCOS;
              and NVL(&i_cF..OLTCOMME,space(15))==this.w_OLTCOMME;
              and NVL(&i_cF..OLTTIPAT,space(1))==this.w_OLTTIPAT;
              and NVL(&i_cF..OLTCOATT,space(15))==this.w_OLTCOATT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..OLTFCOCO,space(1)),'CSCONSUN','',NVL(&i_cF..OLTIMCOM,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..OLTFORCO,space(1)),'CSORDIN','',NVL(&i_cF..OLTIMCOM,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..OLTCOCOS,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" CSCONSUN="+i_cOp1+","           +" CSORDIN="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CSCODCOS="+cp_ToStrODBC(NVL(&i_cF..OLTCOCOS,space(5)));
             +" AND CSCODCOM="+cp_ToStrODBC(NVL(&i_cF..OLTCOMME,space(15)));
             +" AND CSTIPSTR="+cp_ToStrODBC(NVL(&i_cF..OLTTIPAT,space(1)));
             +" AND CSCODMAT="+cp_ToStrODBC(NVL(&i_cF..OLTCOATT,space(15)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..OLTFCOCO,'CSCONSUN',i_cF+'.OLTIMCOM',&i_cF..OLTIMCOM,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..OLTFORCO,'CSORDIN',i_cF+'.OLTIMCOM',&i_cF..OLTIMCOM,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CSCODCOM',&i_cF..OLTCOMME;
                 ,'CSTIPSTR',&i_cF..OLTTIPAT;
                 ,'CSCODMAT',&i_cF..OLTCOATT;
                 ,'CSCODCOS',&i_cF..OLTCOCOS)
      UPDATE (i_cTable) SET ;
           CSCONSUN=&i_cOp1.  ,;
           CSORDIN=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Transaction
  proc mUpdateTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nModRow,i_nConn,i_cTable
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..OLTFLORD,space(1))==this.w_OLTFLORD;
              and NVL(&i_cF..OLTQTSAL,0)==this.w_OLTQTSAL;
              and NVL(&i_cF..OLTFLIMP,space(1))==this.w_OLTFLIMP;
              and NVL(&i_cF..OLTQTSAL,0)==this.w_OLTQTSAL;
              and NVL(&i_cF..OLTCOMAG,space(5))==this.w_OLTCOMAG;
              and NVL(&i_cF..OLTKEYSA,space(40))==this.w_OLTKEYSA;

      i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_OLTCOMAG)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLQTOPER="+i_cOp1  +",";
         +" SLQTIPER="+i_cOp2  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(this.w_OLTCOMAG);
           +" AND SLCODICE="+cp_ToStrODBC(this.w_OLTKEYSA);
           )
        if i_nModRow<1 .and. .not. empty(this.w_OLTCOMAG)
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODMAG,SLCODICE  ;
             ,SLQTOPER"+",SLQTIPER"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_OLTCOMAG)+","+cp_ToStrODBC(this.w_OLTKEYSA)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',this.w_OLTKEYSA;
                 ,'SLCODMAG',this.w_OLTCOMAG)
      UPDATE (i_cTable) SET;
           SLQTOPER=&i_cOp1.  ,;
           SLQTIPER=&i_cOp2.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_OLTCOMAG)
        i_cOp1=cp_SetTrsOp(this.w_OLTFLORD,'SLQTOPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_OLTFLIMP,'SLQTIPER','this.w_OLTQTSAL',this.w_OLTQTSAL,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLTKEYSA,'SLCODMAG',this.w_OLTCOMAG)
        INSERT INTO (i_cTable) (SLCODMAG,SLCODICE  ;
         ,SLQTOPER,SLQTIPER,UTCV,UTDV,CPCCCHK) VALUES (this.w_OLTCOMAG,this.w_OLTKEYSA  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..OLTFCOCO,space(1))==this.w_OLTFCOCO;
              and NVL(&i_cF..OLTIMCOM,0)==this.w_OLTIMCOM;
              and NVL(&i_cF..OLTFORCO,space(1))==this.w_OLTFORCO;
              and NVL(&i_cF..OLTIMCOM,0)==this.w_OLTIMCOM;
              and NVL(&i_cF..OLTCOCOS,space(5))==this.w_OLTCOCOS;
              and NVL(&i_cF..OLTCOMME,space(15))==this.w_OLTCOMME;
              and NVL(&i_cF..OLTTIPAT,space(1))==this.w_OLTTIPAT;
              and NVL(&i_cF..OLTCOATT,space(15))==this.w_OLTCOATT;

      i_cOp1=cp_SetTrsOp(this.w_OLTFCOCO,'CSCONSUN','this.w_OLTIMCOM',this.w_OLTIMCOM,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_OLTFORCO,'CSORDIN','this.w_OLTIMCOM',this.w_OLTIMCOM,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_OLTCOCOS)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" CSCONSUN="+i_cOp1  +",";
         +" CSORDIN="+i_cOp2  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE CSCODCOS="+cp_ToStrODBC(this.w_OLTCOCOS);
           +" AND CSCODCOM="+cp_ToStrODBC(this.w_OLTCOMME);
           +" AND CSTIPSTR="+cp_ToStrODBC(this.w_OLTTIPAT);
           +" AND CSCODMAT="+cp_ToStrODBC(this.w_OLTCOATT);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_OLTFCOCO,'CSCONSUN','this.w_OLTIMCOM',this.w_OLTIMCOM,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_OLTFORCO,'CSORDIN','this.w_OLTIMCOM',this.w_OLTIMCOM,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'CSCODCOM',this.w_OLTCOMME;
                 ,'CSTIPSTR',this.w_OLTTIPAT;
                 ,'CSCODMAT',this.w_OLTCOATT;
                 ,'CSCODCOS',this.w_OLTCOCOS)
      UPDATE (i_cTable) SET;
           CSCONSUN=&i_cOp1.  ,;
           CSORDIN=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCO_MOL : Deleting
    this.GSCO_MOL.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OLCODODL,"OLCODODL";
           )
    this.GSCO_MOL.mDelete()
    * --- GSCO_MCS : Deleting
    this.GSCO_MCS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OLCODODL,"SMCODODL";
           )
    this.GSCO_MCS.mDelete()
    * --- GSCO_MCL : Deleting
    this.GSCO_MCL.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OLCODODL,"CLCODODL";
           )
    this.GSCO_MCL.mDelete()
    * --- GSCO_MRF : Deleting
    this.GSCO_MRF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OLCODODL,"RFCODODL";
           )
    this.GSCO_MRF.mDelete()
    * --- GSCO_MMO : Deleting
    this.GSCO_MMO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_OLCODODL,"MOCODODL";
           )
    this.GSCO_MMO.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ODL_MAST_IDX,i_nConn)
      *
      * delete ODL_MAST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'OLCODODL',this.w_OLCODODL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_OLCODODL<>.w_OLCODODL
            .w_ReadPar = 'PP'
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.t.)
            .w_STATOGES = upper(this.cFunction)
        .DoRTCalc(8,13,.t.)
          .link_1_16('Full')
        .DoRTCalc(15,18,.t.)
        if .o_OLTCODIC<>.w_OLTCODIC
          .link_1_23('Full')
        endif
        .DoRTCalc(20,20,.t.)
            .w_CODART = .w_OLTCOART
          .link_1_26('Full')
        .DoRTCalc(22,28,.t.)
        if .o_OLTCODIC<>.w_OLTCODIC
            .w_OLTPROVE = IIF(empty(.w_OLTCODIC), IIF(.w_TIPGES="E", "E", IIF(.w_TIPGES="Z","L", IIF(.w_TIPGES="L", "I", "E"))) , IIF(.w_ARPROPRE='I' and .w_TIPGES='L','I', IIF(.w_ARPROPRE='L' and .w_TIPGES='Z','L',IIF(.w_ARPROPRE='E' and .w_TIPGES='E','E',.w_ARPROPRE))))
        endif
        .DoRTCalc(30,30,.t.)
        if .o_OLCODODL<>.w_OLCODODL.or. .o_ARTIPART<>.w_ARTIPART
            .w_CHKAGG = Inlist(.cFunction, 'Load', 'Edit') and (.w_OLTSTATO<>"L" or  not .w_OLTPROVE $ "E-L") and .w_ARTIPART<>'FS'
        endif
        .DoRTCalc(32,37,.t.)
          .link_1_45('Full')
        .DoRTCalc(39,41,.t.)
        if .o_OLTCODIC<>.w_OLTCODIC.or. .o_OLTPROVE<>.w_OLTPROVE
            .w_OLTCOMAG = IIF(EMPTY(.w_MAGPRE), .w_MAGPAR, .w_MAGPRE)
          .link_1_50('Full')
        endif
        .DoRTCalc(43,44,.t.)
          .link_1_53('Full')
        .DoRTCalc(46,53,.t.)
            .w_OLTCAMAG = .w_CAUORD
          .link_1_62('Full')
            .w_OLTFLORD = iif(.w_OLTSTATO="L" and .w_OLTPROVE$"L-E", " ", .w_OLTFLORD)
        .DoRTCalc(56,57,.t.)
            .w_OLTKEYSA = .w_OLTCOART
        if .o_OLTCODIC<>.w_OLTCODIC
            .w_OLTUNMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_1_67('Full')
        endif
        .DoRTCalc(60,62,.t.)
        if .o_OLTQTODL<>.w_OLTQTODL.or. .o_OLTUNMIS<>.w_OLTUNMIS
            .w_OLTQTOD1 = CALQTAADV(.w_OLTQTODL,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTODL")
        endif
        if .o_OLTQTOEV<>.w_OLTQTOEV.or. .o_OLTUNMIS<>.w_OLTUNMIS
            .w_OLTQTOE1 = CALQTAADV(.w_OLTQTOEV,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, 'N', .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTOEV")
        endif
        .DoRTCalc(65,65,.t.)
            .w_OLTQTSAL = IIF(.w_OLTFLEVA="S", 0, MAX(.w_OLTQTOD1-.w_OLTQTOE1, 0))
        if .o_OLCODODL<>.w_OLCODODL
          .link_1_78('Full')
        endif
        .DoRTCalc(68,70,.t.)
        if .o_OLTCOART<>.w_OLTCOART.or. .o_OLTPROVE<>.w_OLTPROVE.or. .o_OLTCOFOR<>.w_OLTCOFOR.or. .o_OLTDTRIC<>.w_OLTDTRIC.or. .o_OLTCONTR<>.w_OLTCONTR
            .w_OLTEMLAV = COSETLT(.w_OLTCODIC, .w_OLTPROVE, .w_OLTQTODL, .w_OLTQTOD1, .w_OLTCOFOR+.w_OLTCONTR, .w_OLTDTRIC, .w_OLTUNMIS)
        endif
        .DoRTCalc(72,77,.t.)
        if .o_OLTCOART<>.w_OLTCOART.or. .o_OLTCODIC<>.w_OLTCODIC
          .link_2_2('Full')
        endif
        if .o_OLTPROVE<>.w_OLTPROVE.or. .o_OLTDISBA<>.w_OLTDISBA.or. .o_OLTCODIC<>.w_OLTCODIC
            .w_OLTCICLO = iif((g_PRFA='S' and g_CICLILAV<>'S' or g_PRFA<>'S') and .w_OLTPROVE="I" , .w_DEFCICLO , SPACE(15))
          .link_2_3('Full')
        endif
        .DoRTCalc(80,80,.t.)
            .w_OLTTICON = 'F'
        if .o_OLTPROVE<>.w_OLTPROVE
            .w_OLTCOFOR = IIF(.w_OLTPROVE $ 'E-L' , .w_OLTCOFOR, SPACE(15))
          .link_2_13('Full')
        endif
        .DoRTCalc(83,84,.t.)
        if .o_OLTCOART<>.w_OLTCOART.or. .o_OLTPROVE<>.w_OLTPROVE
            .w_OLTCOCEN = IIF(!EMPTY(.w_ARCODCEN) , .w_ARCODCEN , IIF(.w_OLTPROVE='E', .w_PPCCSODA , .w_PPCCSODL))
          .link_2_19('Full')
        endif
        .DoRTCalc(86,86,.t.)
        if .o_OLTCOART<>.w_OLTCOART
          .link_2_22('Full')
        endif
        .DoRTCalc(88,97,.t.)
            .w_OLTFLIMC = IIF(EMPTY(.w_OLTCOATT), "N", "S")
        .DoRTCalc(99,102,.t.)
        if .o_OLTCOATT<>.w_OLTCOATT.or. .o_OLTCODIC<>.w_OLTCODIC.or. .o_OLTCOART<>.w_OLTCOART
            .w_OLTCOCOS = IIF(EMPTY(.w_OLTCOATT), SPACE(5), .w_CODCOS)
          .link_2_40('Full')
        endif
            .w_AFORCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='I','+',IIF(.w_AFLCOMM='D','-',' ')),' ')
            .w_AFCOCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" and !empty(.w_CODCOS) and !empty(.w_OLTCOATT),iif(.w_AFLCOMM='C','+',IIF(.w_AFLCOMM='S','-',' ')),' ')
        if .o_OLTCAMAG<>.w_OLTCAMAG.or. .o_OLTCODIC<>.w_OLTCODIC.or. .o_OLTCOATT<>.w_OLTCOATT.or. .o_OLTFLIMC<>.w_OLTFLIMC
            .w_OLTFORCO = IIF(g_COMM='S' AND .w_OLTFLIMC="S" AND NOT EMPTY(.w_OLTCOCOS), IIF(.w_FLCOMM='I','+', IIF(.w_FLCOMM='D','-',' ')),' ')
        endif
        if .o_OLTCAMAG<>.w_OLTCAMAG.or. .o_OLTCODIC<>.w_OLTCODIC.or. .o_OLTCOATT<>.w_OLTCOATT.or. .o_OLTFLIMC<>.w_OLTFLIMC
            .w_OLTFCOCO = IIF(g_COMM='S' and .w_OLTFLIMC="S" AND NOT EMPTY(.w_OLTCOCOS), IIF(.w_FLCOMM='C','+', IIF(.w_FLCOMM='S','-',' ')),' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_105.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_106.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        .DoRTCalc(108,112,.t.)
            .w_TDINRIC = .w_OLTDINRIC
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .DoRTCalc(114,115,.t.)
        if .o_OLCODODL<>.w_OLCODODL
            .w_OLDODP = LEFT(.w_OLTCODIC+SPACE(20),20)+LEFT(.w_OLTUNMIS+'   ',3)+STR(.w_OLTQTODL,12,3)+DTOS(.w_OLTDINRIC)
        endif
        .DoRTCalc(117,118,.t.)
        if .o_OLCODODL<>.w_OLCODODL
            .w_OSTATO = .w_OLTSTATO
        endif
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .DoRTCalc(120,121,.t.)
            .w_OLTVARIA = 'S'
        .DoRTCalc(123,123,.t.)
        if .o_OLTCICLO<>.w_OLTCICLO
            .w_CALCOLA = ' '
        endif
        if .o_OLTCICLO<>.w_OLTCICLO
          .Calculate_IQVWUALGNT()
        endif
        if .o_OLTCICLO<>.w_OLTCICLO
            .w_DBCODRIS = .w_OLTCICLO
        endif
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Lead time di produzione:", "N. gg per appr.:")))
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data inizio produzione:", "Data ordine:")))
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data fine produzione:", "Data evasione:")))
        if .o_OLTPROVE<>.w_OLTPROVE
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate(AH_MsgFormat(iif(.w_TIPGES="P","Emissione ODP:",iif(.w_TIPGES="F","Emissione Ricambi:",iif(.w_TIPGES="T","Emissione ODF:",IIF(.w_OLTPROVE="I", "Emissione ODL:", IIF(.w_OLTPROVE="L", "Emissione OCL:" , "Emissione ODA:")))))))
        endif
        .DoRTCalc(126,150,.t.)
        if .o_OLTSTATO<>.w_OLTSTATO
            .w_OLTFLSUG = iif(.w_OLTSTATO="S","+"," ")
        endif
        if .o_OLTSTATO<>.w_OLTSTATO
            .w_OLTFLCON = iif(.w_OLTSTATO="C" and .w_TIPGES<>'T',"+"," ")
        endif
        if .o_OLTSTATO<>.w_OLTSTATO
            .w_OLTFLDAP = iif(.w_OLTSTATO="D" or (.w_OLTSTATO="C" and .w_TIPGES="T"),"+"," ")
        endif
        if .o_OLTSTATO<>.w_OLTSTATO
            .w_OLTFLPIA = iif(.w_OLTSTATO $ "MP","+"," ")
        endif
        if .o_OLTSTATO<>.w_OLTSTATO
            .w_OLTFLLAN = iif(.w_OLTSTATO="L","+"," ")
        endif
        .oPgFrm.Page1.oPag.oObj_1_157.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_159.Calculate()
        .DoRTCalc(156,163,.t.)
        if .o_OLTCOART<>.w_OLTCOART.or. .o_OLTCODIC<>.w_OLTCODIC
          .link_1_170('Full')
        endif
        .DoRTCalc(165,165,.t.)
        if .o_OLTPROVE<>.w_OLTPROVE.or. .o_OLTDISBA<>.w_OLTDISBA.or. .o_OLTCODIC<>.w_OLTCODIC
            .w_OLTSECIC = IIF(.w_OLTPROVE<>'E' , .w_OLTSECIC, SPACE(10))
          .link_1_172('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_184.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .DoRTCalc(167,177,.t.)
        if .o_OLFLMODO<>.w_OLFLMODO
            .w_OLFLMODC = .w_OLFLMODO
        endif
        .DoRTCalc(179,187,.t.)
            .w_OLCODVAL = IIF(empty(.w_OLCODVAL), g_PERVAL, .w_OLCODVAL)
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
        .oPgFrm.Page9.oPag.TREEV.Calculate()
        .DoRTCalc(189,190,.t.)
            .w_LEVELTMP = .w_TREEV.getvar("LIVELLO")
            .w_TMPTYPE = .w_TREEV.getvar("TIPO")
            .w_ST = .w_TREEV.getvar("OLTSTATO")
            .w_LEVEL = iif(vartype(.w_LEVELTMP)='N',.w_LEVELTMP,0)
            .w_CLADOC = .w_TREEV.getvar("MVCLADOC")
            .w_FLVEAC = 'T'
            .w_TYPE = iif(vartype(.w_TMPTYPE)='C',.w_TMPTYPE,' ')
            .w_STATO = AH_MsgFormat(iif(.w_ST="M","Suggerito",iif(.w_ST="P","Pianificato",iif(.w_ST="L",iif(.w_Type="ODL","Lanciato","Ordinato"),iif(.w_ST="C","Confermato",iif(.w_ST="D","Da pianificare",iif(.w_ST="S","Suggerito",iif(.w_ST="F","Finito"," "))))))))
            .w_ROWNUM = .w_TREEV.getvar("CPROWNUM")
            .w_CODODL2 = nvl(.w_TREEV.getvar("MVSERIAL"),'')
          .link_9_11('Full')
        .DoRTCalc(201,201,.t.)
            .w_STATOR = iif(.w_TYPE <> 'DPI',iif(.w_ST1='S','Provvisorio',iif(.w_ST1='N' ,'Confermato','')),'')
        .DoRTCalc(203,203,.t.)
            .w_COD3 = .w_TREEV.getvar("OLTCODIC ")
            .w_RIF2 = .w_TREEV.getvar("MVNUMDOC ")
            .w_CODODL1 = .w_TREEV.getvar("MVSERIAL")
            .w_RIF3 = iif(.w_type$'DIM-DIR',.w_TREEV.getvar("ardesart "),' ')
        .DoRTCalc(208,221,.t.)
            .w_FLEVAS = .w_TREEV.getvar("OLTFLEVA")
            .w_FLVEAC1 = .w_TREEV.getvar("MVFLVEAC")
        .oPgFrm.Page9.oPag.oObj_9_59.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_60.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_61.Calculate()
        .DoRTCalc(224,247,.t.)
        if .o_OLCODODL<>.w_OLCODODL
            .w_OLCODINI = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        endif
        if .o_OLCODODL<>.w_OLCODODL
            .w_OLCODFIN = iif(empty(.w_OLCODODL),'XXXXXXXXXXXXXXX',.w_OLCODODL)
        endif
        .oPgFrm.Page9.oPag.oObj_9_64.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_65.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_66.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_67.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_68.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_69.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_70.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_71.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_72.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_73.Calculate()
            .w_CODRIC = nvl(.w_TREEV.getvar("OLTCODIC"),'')
          .link_9_74('Full')
        .DoRTCalc(251,251,.t.)
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_DATEMS = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
            .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC "),cp_CharToDate('  -  -  '))
            .w_CAUMAG = .w_TREEV.getvar("MVCAUMAG")
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_QTASCA = nvl(.w_TREEV.getvar("OLTQTOBF"),0)
            .w_QTARES = iif((nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) )>0,nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0)
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
            .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_RIF = .w_TREEV.getvar("PADRE")
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
        .oPgFrm.Page9.oPag.StatoODL.Calculate(.w_STATO)
            .w_UNIMIS1 = nvl(.w_TREEV.getvar("OLTUNMIS"),' ')
            .w_CODODL_ = IIF(.w_type='ODL',.w_TREEV.getvar("OLCODODL"),.w_TREEV.getvar("PADRE"))
            .w_CPROWNUM = nvl(.w_TREEV.getvar("CPROWNUM"),0)
            .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
            .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
            .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
            .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
            .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
        .oPgFrm.Page9.oPag.StatoDOC.Calculate(.w_STATOR)
            .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_CODODL = nvl(iif(.w_TYPE='ODL',.w_TREEV.getvar("OLCODODL1"),.w_TREEV.getvar("OLCODODL")),' ')
            .w_QTAORD = nvl(.w_TREEV.getvar("OLTQTODL"),0)
            .w_UNIMIS = nvl(.w_TREEV.getvar("UNIMIS"),' ')
            .w_DATEVF = nvl(.w_TREEV.getvar("OLTDTRIC"),cp_CharToDate('  -  -  '))
            .w_QTAEVA = nvl(.w_TREEV.getvar("OLTQTOEV"),0)
            .w_QTARES = iif(nvl(.w_FLEVAS,'')<>'S',iif(.w_TYPE<>'DTV',nvl(.w_QTAORD,0) - nvl(.w_QTAEVA,0) ,0),0)
            .w_MAGAZ = nvl(.w_TREEV.getvar("OLTCOMAG"),' ')
            .w_NUMDOC = nvl(.w_TREEV.getvar("MVNUMDOC"),0)
            .w_ALFDOC = nvl(.w_TREEV.getvar("MVALFDOC"),' ')
            .w_DATEVA = nvl(.w_TREEV.getvar("OLTDINRIC"),cp_CharToDate('  -  -  '))
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
            .w_FORNIT = nvl(.w_TREEV.getvar("OLTCOFOR"),' ')
            .w_NUMREG = nvl(.w_TREEV.getvar("MVNUMREG"),0)
        .DoRTCalc(295,296,.t.)
            .w_PV = .w_TREEV.getvar("OLTPROVE")
            .w_PROVE = iif(.w_TYPE='ODF',iif(.w_PV='I','Fase interna',iif(.w_PV='L','Fase esterna','')),'')
        .oPgFrm.Page9.oPag.ProvODLF.Calculate(.w_PROVE)
        .oPgFrm.Page8.oPag.ZPF.Calculate()
        .oPgFrm.Page8.oPag.ZFP.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_5.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_6.Calculate()
        .DoRTCalc(299,299,.t.)
        if .o_OLTUNMIS<>.w_OLTUNMIS.or. .o_OLTQTOSC<>.w_OLTQTOSC
            .w_OLTQTOS1 = CALQTAADV(.w_OLTQTOSC,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N', '', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTOSC")
        endif
        .DoRTCalc(301,301,.t.)
        if .o_OLTUNMIS<>.w_OLTUNMIS.or. .o_OLTQTPRO<>.w_OLTQTPRO
            .w_OLTQTPR1 = CALQTAADV(.w_OLTQTPRO,.w_OLTUNMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, '', 'N', '', '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,,This, "OLTQTPRO")
        endif
        if .o_OLTDTCON<>.w_OLTDTCON.or. .o_OLTDTRIC<>.w_OLTDTRIC.or. .o_OLTDINRIC<>.w_OLTDINRIC
          .Calculate_NVKCLVEAEH()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"PRODL","i_CODAZI,w_OLCODODL")
          .op_OLCODODL = .w_OLCODODL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(303,306,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_105.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_106.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_112.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_113.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_114.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_116.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_140.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Lead time di produzione:", "N. gg per appr.:")))
        .oPgFrm.Page1.oPag.oObj_1_141.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data inizio produzione:", "Data ordine:")))
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate(AH_MsgFormat(IIF(.w_OLTPROVE<>"E", "Data fine produzione:", "Data evasione:")))
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate(AH_MsgFormat(iif(.w_TIPGES="P","Emissione ODP:",iif(.w_TIPGES="F","Emissione Ricambi:",iif(.w_TIPGES="T","Emissione ODF:",IIF(.w_OLTPROVE="I", "Emissione ODL:", IIF(.w_OLTPROVE="L", "Emissione OCL:" , "Emissione ODA:")))))))
        .oPgFrm.Page1.oPag.oObj_1_157.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_159.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_184.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_67.Calculate()
        .oPgFrm.Page9.oPag.TREEV.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_59.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_60.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_61.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_64.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_65.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_66.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_67.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_68.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_69.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_70.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_71.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_72.Calculate()
        .oPgFrm.Page9.oPag.oObj_9_73.Calculate()
        .oPgFrm.Page9.oPag.StatoODL.Calculate(.w_STATO)
        .oPgFrm.Page9.oPag.StatoDOC.Calculate(.w_STATOR)
        .oPgFrm.Page9.oPag.ProvODLF.Calculate(.w_PROVE)
        .oPgFrm.Page8.oPag.ZPF.Calculate()
        .oPgFrm.Page8.oPag.ZFP.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_5.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_6.Calculate()
    endwith
  return

  proc Calculate_QCRHENVFDD()
    with this
          * --- GSCO_BOL -> FormLoad
          .w_CLKEYRIF = SYS(2015)
          GSCO_BOL(this;
              ,"CREATMP";
             )
    endwith
  endproc
  proc Calculate_IQVWUALGNT()
    with this
          * --- Modificato il ciclo semplificato
          GSCO_BOL(this;
              ,"CICLOVAR";
             )
    endwith
  endproc
  proc Calculate_CNPARONRYX()
    with this
          * --- Setto la variabile che ho cambiato il contratto manualmente
          .w_CHGECONT = .T.
          GSCO_BOL(this;
              ,"CHANGECONT";
             )
          .w_CHGECONT = .f.
    endwith
  endproc
  proc Calculate_HCVBMEISIP()
    with this
          * --- Ricalcolo il contratto se cambio le date manualmente
          .w_CHGECONT = .F.
          GSCO_BOL(this;
              ,"CHANGE";
             )
          .w_CHGECONT = .f.
          .w_CALCDATA = .T.
          .w_ERDATINI = .F.
    endwith
  endproc
  proc Calculate_XIHLSWQOQC()
    with this
          * --- GSCO_BOL -> Verifica presenza ciclo di lavorazione
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCO_BOL(this;
              ,"HAILCIC";
             )
     endif
    endwith
  endproc
  proc Calculate_TYPDPOXVBF()
    with this
          * --- GSCO_BOL -> Variazione del ciclo di lavorazione
     if g_PRFA='S' AND g_CICLILAV='S'
          GSCO_BOL(this;
              ,"CICLI";
             )
     endif
    endwith
  endproc
  proc Calculate_YKCSCADRSF()
    with this
          * --- GSCO_BOL -> Done
          GSCO_BOL(this;
              ,"DROPTMP";
             )
    endwith
  endproc
  proc Calculate_SQFCBOXANG()
    with this
          * --- Cambio la configurazione del cMenufile sulla treevie in base al tipo di gestione
          .w_TREEV.cMenuFile = IIF(.w_TIPGES='Z', 'GSCL_KTO' , IIF(.w_TIPGES='E' , 'GSCO1KTO' , 'GSCO_KTO'))
    endwith
  endproc
  proc Calculate_NVKCLVEAEH()
    with this
          * --- 
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.enabled = this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.mCond()
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.enabled = this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.mCond()
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.enabled = this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.mCond()
    this.oPgFrm.Page1.oPag.oOLTCODIC_1_21.enabled = this.oPgFrm.Page1.oPag.oOLTCODIC_1_21.mCond()
    this.oPgFrm.Page1.oPag.oOLTPROVE_1_34.enabled = this.oPgFrm.Page1.oPag.oOLTPROVE_1_34.mCond()
    this.oPgFrm.Page1.oPag.oOLTCOMAG_1_50.enabled = this.oPgFrm.Page1.oPag.oOLTCOMAG_1_50.mCond()
    this.oPgFrm.Page1.oPag.oOLTUNMIS_1_67.enabled = this.oPgFrm.Page1.oPag.oOLTUNMIS_1_67.mCond()
    this.oPgFrm.Page1.oPag.oOLTDTCON_1_68.enabled = this.oPgFrm.Page1.oPag.oOLTDTCON_1_68.mCond()
    this.oPgFrm.Page1.oPag.oOLTQTODL_1_70.enabled = this.oPgFrm.Page1.oPag.oOLTQTODL_1_70.mCond()
    this.oPgFrm.Page1.oPag.oOLTDTRIC_1_88.enabled = this.oPgFrm.Page1.oPag.oOLTDTRIC_1_88.mCond()
    this.oPgFrm.Page1.oPag.oOLTDINRIC_1_89.enabled = this.oPgFrm.Page1.oPag.oOLTDINRIC_1_89.mCond()
    this.oPgFrm.Page2.oPag.oOLTCICLO_2_3.enabled = this.oPgFrm.Page2.oPag.oOLTCICLO_2_3.mCond()
    this.oPgFrm.Page2.oPag.oOLTCOFOR_2_13.enabled = this.oPgFrm.Page2.oPag.oOLTCOFOR_2_13.mCond()
    this.oPgFrm.Page2.oPag.oOLTCONTR_2_16.enabled = this.oPgFrm.Page2.oPag.oOLTCONTR_2_16.mCond()
    this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.enabled = this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.mCond()
    this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.enabled = this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.mCond()
    this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.enabled = this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.mCond()
    this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.enabled = this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.mCond()
    this.oPgFrm.Page2.oPag.oOLCRIFOR_2_64.enabled = this.oPgFrm.Page2.oPag.oOLCRIFOR_2_64.mCond()
    this.oPgFrm.Page1.oPag.oOLTDISBA_1_170.enabled = this.oPgFrm.Page1.oPag.oOLTDISBA_1_170.mCond()
    this.oPgFrm.Page1.oPag.oOLTSECIC_1_172.enabled = this.oPgFrm.Page1.oPag.oOLTSECIC_1_172.mCond()
    this.oPgFrm.Page1.oPag.oOLTSEODL_1_178.enabled = this.oPgFrm.Page1.oPag.oOLTSEODL_1_178.mCond()
    this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.enabled = this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.mCond()
    this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.enabled = this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_118.enabled = this.oPgFrm.Page1.oPag.oBtn_1_118.mCond()
    this.GSCO_MOL.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_1.mCond()
    this.GSCO_MCS.enabled = this.oPgFrm.Page4.oPag.oLinkPC_4_1.mCond()
    this.GSCO_MCL.enabled = this.oPgFrm.Page5.oPag.oLinkPC_5_1.mCond()
    this.GSCO_MMO.enabled = this.oPgFrm.Page7.oPag.oLinkPC_7_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(4).enabled=not(g_PRFA='S' AND g_CICLILAV='S')
    this.oPgFrm.Pages(5).enabled=not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR !Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(6).enabled=not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(9).enabled=not(this.w_OLTSTATO $ "SCD" )
    local i_show3
    i_show3=not(g_PRFA='S' AND g_CICLILAV='S')
    this.oPgFrm.Pages(4).enabled=i_show3 and not(g_PRFA='S' AND g_CICLILAV='S')
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Risorse"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    local i_show4
    i_show4=not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR !Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(5).enabled=i_show4 and not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR !Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Ciclo di lavorazione"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    local i_show5
    i_show5=not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(6).enabled=i_show5 and not(g_PRFA<>'S' OR g_CICLILAV<>'S' OR Empty(this.w_OLTSEODL))
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Risorse di fase"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.visible=!this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.mHide()
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.visible=!this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.visible=!this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.visible=!this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.mHide()
    this.oPgFrm.Page2.oPag.oDESCON_2_20.visible=!this.oPgFrm.Page2.oPag.oDESCON_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.visible=!this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.mHide()
    this.oPgFrm.Page2.oPag.oVOCDES_2_23.visible=!this.oPgFrm.Page2.oPag.oVOCDES_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.visible=!this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.visible=!this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.mHide()
    this.oPgFrm.Page2.oPag.oDESCAN_2_32.visible=!this.oPgFrm.Page2.oPag.oDESCAN_2_32.mHide()
    this.oPgFrm.Page2.oPag.oDESATT_2_33.visible=!this.oPgFrm.Page2.oPag.oDESATT_2_33.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_3_1.visible=!this.oPgFrm.Page3.oPag.oLinkPC_3_1.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_117.visible=!this.oPgFrm.Page1.oPag.oBtn_1_117.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_118.visible=!this.oPgFrm.Page1.oPag.oBtn_1_118.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_3.visible=!this.oPgFrm.Page3.oPag.oStr_3_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_1.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_1.mHide()
    this.oPgFrm.Page1.oPag.oOLSERMRP_1_164.visible=!this.oPgFrm.Page1.oPag.oOLSERMRP_1_164.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_165.visible=!this.oPgFrm.Page1.oPag.oStr_1_165.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_2.visible=!this.oPgFrm.Page4.oPag.oStr_4_2.mHide()
    this.oPgFrm.Page1.oPag.oCACODFAS_1_167.visible=!this.oPgFrm.Page1.oPag.oCACODFAS_1_167.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_168.visible=!this.oPgFrm.Page1.oPag.oStr_1_168.mHide()
    this.oPgFrm.Page5.oPag.oLinkPC_5_1.visible=!this.oPgFrm.Page5.oPag.oLinkPC_5_1.mHide()
    this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.visible=!this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.mHide()
    this.oPgFrm.Page1.oPag.oOLTCOPOI_1_193.visible=!this.oPgFrm.Page1.oPag.oOLTCOPOI_1_193.mHide()
    this.oPgFrm.Page1.oPag.oOLTFAOUT_1_194.visible=!this.oPgFrm.Page1.oPag.oOLTFAOUT_1_194.mHide()
    this.oPgFrm.Page1.oPag.oOLTULFAS_1_195.visible=!this.oPgFrm.Page1.oPag.oOLTULFAS_1_195.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_2.visible=!this.oPgFrm.Page5.oPag.oStr_5_2.mHide()
    this.oPgFrm.Page9.oPag.oCODRIC_9_74.visible=!this.oPgFrm.Page9.oPag.oCODRIC_9_74.mHide()
    this.oPgFrm.Page9.oPag.oCADESAR_9_75.visible=!this.oPgFrm.Page9.oPag.oCADESAR_9_75.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_76.visible=!this.oPgFrm.Page9.oPag.oStr_9_76.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_77.visible=!this.oPgFrm.Page9.oPag.oStr_9_77.mHide()
    this.oPgFrm.Page9.oPag.oQTAORD_9_78.visible=!this.oPgFrm.Page9.oPag.oQTAORD_9_78.mHide()
    this.oPgFrm.Page9.oPag.oMAGAZ_9_79.visible=!this.oPgFrm.Page9.oPag.oMAGAZ_9_79.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_80.visible=!this.oPgFrm.Page9.oPag.oStr_9_80.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_81.visible=!this.oPgFrm.Page9.oPag.oStr_9_81.mHide()
    this.oPgFrm.Page9.oPag.oDATEMS_9_82.visible=!this.oPgFrm.Page9.oPag.oDATEMS_9_82.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_83.visible=!this.oPgFrm.Page9.oPag.oStr_9_83.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_84.visible=!this.oPgFrm.Page9.oPag.oStr_9_84.mHide()
    this.oPgFrm.Page9.oPag.oDATEVF_9_85.visible=!this.oPgFrm.Page9.oPag.oDATEVF_9_85.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_86.visible=!this.oPgFrm.Page9.oPag.oStr_9_86.mHide()
    this.oPgFrm.Page9.oPag.oCAUMAG_9_87.visible=!this.oPgFrm.Page9.oPag.oCAUMAG_9_87.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_88.visible=!this.oPgFrm.Page9.oPag.oStr_9_88.mHide()
    this.oPgFrm.Page9.oPag.oMAGAZ_9_89.visible=!this.oPgFrm.Page9.oPag.oMAGAZ_9_89.mHide()
    this.oPgFrm.Page9.oPag.oFORNIT_9_90.visible=!this.oPgFrm.Page9.oPag.oFORNIT_9_90.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_91.visible=!this.oPgFrm.Page9.oPag.oStr_9_91.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_92.visible=!this.oPgFrm.Page9.oPag.oStr_9_92.mHide()
    this.oPgFrm.Page9.oPag.oQTASCA_9_93.visible=!this.oPgFrm.Page9.oPag.oQTASCA_9_93.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_94.visible=!this.oPgFrm.Page9.oPag.oStr_9_94.mHide()
    this.oPgFrm.Page9.oPag.oQTARES_9_95.visible=!this.oPgFrm.Page9.oPag.oQTARES_9_95.mHide()
    this.oPgFrm.Page9.oPag.oNUMREG_9_96.visible=!this.oPgFrm.Page9.oPag.oNUMREG_9_96.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_97.visible=!this.oPgFrm.Page9.oPag.oStr_9_97.mHide()
    this.oPgFrm.Page9.oPag.oDATEVA_9_98.visible=!this.oPgFrm.Page9.oPag.oDATEVA_9_98.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_99.visible=!this.oPgFrm.Page9.oPag.oStr_9_99.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_100.visible=!this.oPgFrm.Page9.oPag.oStr_9_100.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_101.visible=!this.oPgFrm.Page9.oPag.oStr_9_101.mHide()
    this.oPgFrm.Page9.oPag.oFORNIT_9_102.visible=!this.oPgFrm.Page9.oPag.oFORNIT_9_102.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_103.visible=!this.oPgFrm.Page9.oPag.oStr_9_103.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_104.visible=!this.oPgFrm.Page9.oPag.oStr_9_104.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_105.visible=!this.oPgFrm.Page9.oPag.oStr_9_105.mHide()
    this.oPgFrm.Page9.oPag.oRIF_9_106.visible=!this.oPgFrm.Page9.oPag.oRIF_9_106.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_107.visible=!this.oPgFrm.Page9.oPag.oStr_9_107.mHide()
    this.oPgFrm.Page9.oPag.oQTAORD_9_108.visible=!this.oPgFrm.Page9.oPag.oQTAORD_9_108.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_109.visible=!this.oPgFrm.Page9.oPag.oStr_9_109.mHide()
    this.oPgFrm.Page9.oPag.oQTAEVA_9_110.visible=!this.oPgFrm.Page9.oPag.oQTAEVA_9_110.mHide()
    this.oPgFrm.Page9.oPag.oUNIMIS1_9_112.visible=!this.oPgFrm.Page9.oPag.oUNIMIS1_9_112.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_113.visible=!this.oPgFrm.Page9.oPag.oStr_9_113.mHide()
    this.oPgFrm.Page9.oPag.oCODODL__9_114.visible=!this.oPgFrm.Page9.oPag.oCODODL__9_114.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_115.visible=!this.oPgFrm.Page9.oPag.oStr_9_115.mHide()
    this.oPgFrm.Page9.oPag.oCPROWNUM_9_116.visible=!this.oPgFrm.Page9.oPag.oCPROWNUM_9_116.mHide()
    this.oPgFrm.Page9.oPag.oCODODL_9_117.visible=!this.oPgFrm.Page9.oPag.oCODODL_9_117.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_118.visible=!this.oPgFrm.Page9.oPag.oStr_9_118.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_119.visible=!this.oPgFrm.Page9.oPag.oStr_9_119.mHide()
    this.oPgFrm.Page9.oPag.oQTAORD_9_120.visible=!this.oPgFrm.Page9.oPag.oQTAORD_9_120.mHide()
    this.oPgFrm.Page9.oPag.oUNIMIS_9_121.visible=!this.oPgFrm.Page9.oPag.oUNIMIS_9_121.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_122.visible=!this.oPgFrm.Page9.oPag.oStr_9_122.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_123.visible=!this.oPgFrm.Page9.oPag.oStr_9_123.mHide()
    this.oPgFrm.Page9.oPag.oDATEVF_9_124.visible=!this.oPgFrm.Page9.oPag.oDATEVF_9_124.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_125.visible=!this.oPgFrm.Page9.oPag.oStr_9_125.mHide()
    this.oPgFrm.Page9.oPag.oQTAEVA_9_126.visible=!this.oPgFrm.Page9.oPag.oQTAEVA_9_126.mHide()
    this.oPgFrm.Page9.oPag.oQTARES_9_127.visible=!this.oPgFrm.Page9.oPag.oQTARES_9_127.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_128.visible=!this.oPgFrm.Page9.oPag.oStr_9_128.mHide()
    this.oPgFrm.Page9.oPag.oMAGAZ_9_129.visible=!this.oPgFrm.Page9.oPag.oMAGAZ_9_129.mHide()
    this.oPgFrm.Page9.oPag.oNUMDOC_9_130.visible=!this.oPgFrm.Page9.oPag.oNUMDOC_9_130.mHide()
    this.oPgFrm.Page9.oPag.oALFDOC_9_131.visible=!this.oPgFrm.Page9.oPag.oALFDOC_9_131.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_132.visible=!this.oPgFrm.Page9.oPag.oStr_9_132.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_133.visible=!this.oPgFrm.Page9.oPag.oStr_9_133.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_135.visible=!this.oPgFrm.Page9.oPag.oStr_9_135.mHide()
    this.oPgFrm.Page9.oPag.oDATEVA_9_136.visible=!this.oPgFrm.Page9.oPag.oDATEVA_9_136.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_137.visible=!this.oPgFrm.Page9.oPag.oStr_9_137.mHide()
    this.oPgFrm.Page9.oPag.oNUMREG_9_138.visible=!this.oPgFrm.Page9.oPag.oNUMREG_9_138.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_139.visible=!this.oPgFrm.Page9.oPag.oStr_9_139.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_140.visible=!this.oPgFrm.Page9.oPag.oStr_9_140.mHide()
    this.oPgFrm.Page9.oPag.oFORNIT_9_141.visible=!this.oPgFrm.Page9.oPag.oFORNIT_9_141.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_142.visible=!this.oPgFrm.Page9.oPag.oStr_9_142.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_143.visible=!this.oPgFrm.Page9.oPag.oStr_9_143.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_144.visible=!this.oPgFrm.Page9.oPag.oStr_9_144.mHide()
    this.oPgFrm.Page9.oPag.oCODODL_9_145.visible=!this.oPgFrm.Page9.oPag.oCODODL_9_145.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_146.visible=!this.oPgFrm.Page9.oPag.oStr_9_146.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_147.visible=!this.oPgFrm.Page9.oPag.oStr_9_147.mHide()
    this.oPgFrm.Page9.oPag.oQTAORD_9_148.visible=!this.oPgFrm.Page9.oPag.oQTAORD_9_148.mHide()
    this.oPgFrm.Page9.oPag.oUNIMIS_9_149.visible=!this.oPgFrm.Page9.oPag.oUNIMIS_9_149.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_150.visible=!this.oPgFrm.Page9.oPag.oStr_9_150.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_151.visible=!this.oPgFrm.Page9.oPag.oStr_9_151.mHide()
    this.oPgFrm.Page9.oPag.oDATEVF_9_152.visible=!this.oPgFrm.Page9.oPag.oDATEVF_9_152.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_153.visible=!this.oPgFrm.Page9.oPag.oStr_9_153.mHide()
    this.oPgFrm.Page9.oPag.oQTAEVA_9_154.visible=!this.oPgFrm.Page9.oPag.oQTAEVA_9_154.mHide()
    this.oPgFrm.Page9.oPag.oQTARES_9_155.visible=!this.oPgFrm.Page9.oPag.oQTARES_9_155.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_156.visible=!this.oPgFrm.Page9.oPag.oStr_9_156.mHide()
    this.oPgFrm.Page9.oPag.oMAGAZ_9_157.visible=!this.oPgFrm.Page9.oPag.oMAGAZ_9_157.mHide()
    this.oPgFrm.Page9.oPag.oNUMDOC_9_158.visible=!this.oPgFrm.Page9.oPag.oNUMDOC_9_158.mHide()
    this.oPgFrm.Page9.oPag.oALFDOC_9_159.visible=!this.oPgFrm.Page9.oPag.oALFDOC_9_159.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_160.visible=!this.oPgFrm.Page9.oPag.oStr_9_160.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_161.visible=!this.oPgFrm.Page9.oPag.oStr_9_161.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_162.visible=!this.oPgFrm.Page9.oPag.oStr_9_162.mHide()
    this.oPgFrm.Page9.oPag.oDATEVA_9_163.visible=!this.oPgFrm.Page9.oPag.oDATEVA_9_163.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_164.visible=!this.oPgFrm.Page9.oPag.oStr_9_164.mHide()
    this.oPgFrm.Page9.oPag.oNUMREG_9_165.visible=!this.oPgFrm.Page9.oPag.oNUMREG_9_165.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_166.visible=!this.oPgFrm.Page9.oPag.oStr_9_166.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_167.visible=!this.oPgFrm.Page9.oPag.oStr_9_167.mHide()
    this.oPgFrm.Page9.oPag.oFORNIT_9_168.visible=!this.oPgFrm.Page9.oPag.oFORNIT_9_168.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_169.visible=!this.oPgFrm.Page9.oPag.oStr_9_169.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_170.visible=!this.oPgFrm.Page9.oPag.oStr_9_170.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_171.visible=!this.oPgFrm.Page9.oPag.oStr_9_171.mHide()
    this.oPgFrm.Page9.oPag.oNUMREG_9_172.visible=!this.oPgFrm.Page9.oPag.oNUMREG_9_172.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_173.visible=!this.oPgFrm.Page9.oPag.oStr_9_173.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_208.visible=!this.oPgFrm.Page1.oPag.oStr_1_208.mHide()
    this.oPgFrm.Page1.oPag.oOLTQTOSC_1_209.visible=!this.oPgFrm.Page1.oPag.oOLTQTOSC_1_209.mHide()
    this.oPgFrm.Page1.oPag.oOLTQTPRO_1_211.visible=!this.oPgFrm.Page1.oPag.oOLTQTPRO_1_211.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_212.visible=!this.oPgFrm.Page1.oPag.oStr_1_212.mHide()
    this.oPgFrm.Page7.oPag.oLinkPC_7_1.visible=!this.oPgFrm.Page7.oPag.oLinkPC_7_1.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_2.visible=!this.oPgFrm.Page7.oPag.oStr_7_2.mHide()
    this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.visible=!this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsco_aop
      * instanzio il figlio della quarta pagina immediatamente
            * viene richiamato per i calcoli
            if g_PRFA='S'
              IF Upper(CEVENT)='INIT'
               if Upper(this.GSCO_MCL.class)='STDDYNAMICCHILD'
                 This.oPgFrm.Pages[4].opag.uienable(.T.)
                 This.oPgFrm.ActivePage=1
               Endif
              Endif 
            else
              IF Upper(CEVENT)='INIT'
               if Upper(this.GSCO_MCS.class)='STDDYNAMICCHILD'
                 This.oPgFrm.Pages[4].opag.uienable(.T.)
                 This.oPgFrm.ActivePage=1
               Endif
              Endif 
            Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_QCRHENVFDD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_101.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_105.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_106.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_107.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_108.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_109.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_110.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_112.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_113.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_114.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_115.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_116.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_123.Event(cEvent)
        if lower(cEvent)==lower("w_OLTCICLO Changed")
          .Calculate_IQVWUALGNT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OLTCONTR Changed")
          .Calculate_CNPARONRYX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OLTDINRIC Changed") or lower(cEvent)==lower("w_OLTDTRIC Changed") or lower(cEvent)==lower("w_OLTDTCON Changed")
          .Calculate_HCVBMEISIP()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_140.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_141.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_142.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_143.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_157.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_159.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_184.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_185.Event(cEvent)
        if lower(cEvent)==lower("CycleExist") or lower(cEvent)==lower("Load") or lower(cEvent)==lower("w_OLTDISBA Changed")
          .Calculate_XIHLSWQOQC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_OLTSECIC Changed")
          .Calculate_TYPDPOXVBF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_YKCSCADRSF()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_67.Event(cEvent)
      .oPgFrm.Page9.oPag.TREEV.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_59.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_60.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_61.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_64.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_65.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_66.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_67.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_68.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_69.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_70.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_71.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_72.Event(cEvent)
      .oPgFrm.Page9.oPag.oObj_9_73.Event(cEvent)
      .oPgFrm.Page9.oPag.StatoODL.Event(cEvent)
      .oPgFrm.Page9.oPag.StatoDOC.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_SQFCBOXANG()
          bRefresh=.t.
        endif
      .oPgFrm.Page9.oPag.ProvODLF.Event(cEvent)
      .oPgFrm.Page8.oPag.ZPF.Event(cEvent)
      .oPgFrm.Page8.oPag.ZFP.Event(cEvent)
      .oPgFrm.Page8.oPag.oObj_8_5.Event(cEvent)
      .oPgFrm.Page8.oPag.oObj_8_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadPar
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadPar) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadPar)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCAUORD,PPCAUIMP,PPMAGPRO,PPMAGWIP,PPCENCOS,PPGESWIP,PPCCSODA,PPMATINP";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ReadPar);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ReadPar)
            select PPCODICE,PPCAUORD,PPCAUIMP,PPMAGPRO,PPMAGWIP,PPCENCOS,PPGESWIP,PPCCSODA,PPMATINP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadPar = NVL(_Link_.PPCODICE,space(2))
      this.w_CAUORD = NVL(_Link_.PPCAUORD,space(5))
      this.w_CAUIMP = NVL(_Link_.PPCAUIMP,space(5))
      this.w_MAGPAR = NVL(_Link_.PPMAGPRO,space(5))
      this.w_MAGWIP = NVL(_Link_.PPMAGWIP,space(5))
      this.w_PPCCSODL = NVL(_Link_.PPCENCOS,space(15))
      this.w_GESWIP = NVL(_Link_.PPGESWIP,space(1))
      this.w_PPCCSODA = NVL(_Link_.PPCCSODA,space(15))
      this.w_PPMATINP = NVL(_Link_.PPMATINP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadPar = space(2)
      endif
      this.w_CAUORD = space(5)
      this.w_CAUIMP = space(5)
      this.w_MAGPAR = space(5)
      this.w_MAGWIP = space(5)
      this.w_PPCCSODL = space(15)
      this.w_GESWIP = space(1)
      this.w_PPCCSODA = space(15)
      this.w_PPMATINP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadPar Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTFAODL
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_CICL_IDX,3]
    i_lTable = "ODL_CICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2], .t., this.ODL_CICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTFAODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTFAODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODODL,CPROWNUM,CLROWORD,CLDESFAS";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_OLTFAODL);
                   +" and CLCODODL="+cp_ToStrODBC(this.w_OLTSEODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODODL',this.w_OLTSEODL;
                       ,'CPROWNUM',this.w_OLTFAODL)
            select CLCODODL,CPROWNUM,CLROWORD,CLDESFAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTFAODL = NVL(_Link_.CPROWNUM,0)
      this.w_NUMFASE = NVL(_Link_.CLROWORD,0)
      this.w_DESFASE = NVL(_Link_.CLDESFAS,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OLTFAODL = 0
      endif
      this.w_NUMFASE = 0
      this.w_DESFASE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])+'\'+cp_ToStr(_Link_.CLCODODL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.ODL_CICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTFAODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ODL_CICL_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ODL_CICL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.CPROWNUM as CPROWNUM116"+ ",link_1_16.CLROWORD as CLROWORD116"+ ",link_1_16.CLDESFAS as CLDESFAS116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on ODL_MAST.OLTFAODL=link_1_16.CPROWNUM"+" and ODL_MAST.OLTSEODL=link_1_16.CLCODODL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTFAODL=link_1_16.CPROWNUM(+)"'+'+" and ODL_MAST.OLTSEODL=link_1_16.CLCODODL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCODIC
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCODIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_OLTCODIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_OLTCODIC))
          select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCODIC)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_OLTCODIC)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_OLTCODIC)+"%");

            select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStrODBC(trim(this.w_OLTCODIC)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODFAS like "+cp_ToStr(trim(this.w_OLTCODIC)+"%");

            select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OLTCODIC) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oOLTCODIC_1_21'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCODIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_OLTCODIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_OLTCODIC)
            select CACODICE,CADESART,CACODFAS,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCODIC = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOD = NVL(_Link_.CADESART,space(40))
      this.w_CACODFAS = NVL(_Link_.CACODFAS,space(66))
      this.w_OLTCOART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_OLTDTOBS = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCODIC = space(20)
      endif
      this.w_DESCOD = space(40)
      this.w_CACODFAS = space(66)
      this.w_OLTCOART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_OLTDTOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCODIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.CACODICE as CACODICE121"+ ",link_1_21.CADESART as CADESART121"+ ",link_1_21.CACODFAS as CACODFAS121"+ ",link_1_21.CACODART as CACODART121"+ ",link_1_21.CAUNIMIS as CAUNIMIS121"+ ",link_1_21.CAOPERAT as CAOPERAT121"+ ",link_1_21.CAMOLTIP as CAMOLTIP121"+ ",link_1_21.CADTOBSO as CADTOBSO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on ODL_MAST.OLTCODIC=link_1_21.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCODIC=link_1_21.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOART
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARVOCCEN,ARPROPRE,ARTIPGES,ARGRUMER,ARCODDIS,ARMAGPRE,ARDTOBSO,ARFLUSEP,ARPREZUM,ARCODCEN,ARSALCOM,ARTIPART,ARFSRIFE,ARTIPIMP,ARMAGIMP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_OLTCOART)
            select ARCODART,ARDESART,ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARVOCCEN,ARPROPRE,ARTIPGES,ARGRUMER,ARCODDIS,ARMAGPRE,ARDTOBSO,ARFLUSEP,ARPREZUM,ARCODCEN,ARSALCOM,ARTIPART,ARFSRIFE,ARTIPIMP,ARMAGIMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OLTVOCEN = NVL(_Link_.ARVOCCEN,space(15))
      this.w_ARPROPRE = NVL(_Link_.ARPROPRE,space(1))
      this.w_ATIPGES = NVL(_Link_.ARTIPGES,space(1))
      this.w_AGRUMER = NVL(_Link_.ARGRUMER,space(5))
      this.w_OLTDISBA = NVL(_Link_.ARCODDIS,space(20))
      this.w_MAGPRE = NVL(_Link_.ARMAGPRE,space(5))
      this.w_ARDTOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_ARCODCEN = NVL(_Link_.ARCODCEN,space(15))
      this.w_ARFLCOMM = NVL(_Link_.ARSALCOM,space(1))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(1))
      this.w_ARFSRIFE = NVL(_Link_.ARFSRIFE,space(20))
      this.w_TIPIMP = NVL(_Link_.ARTIPIMP,space(1))
      this.w_MAGIMP = NVL(_Link_.ARMAGIMP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_UNMIS1 = space(3)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OLTVOCEN = space(15)
      this.w_ARPROPRE = space(1)
      this.w_ATIPGES = space(1)
      this.w_AGRUMER = space(5)
      this.w_OLTDISBA = space(20)
      this.w_MAGPRE = space(5)
      this.w_ARDTOBSO = ctod("  /  /  ")
      this.w_FLUSEP = space(1)
      this.w_PREZUM = space(1)
      this.w_ARCODCEN = space(15)
      this.w_ARFLCOMM = space(1)
      this.w_ARTIPART = space(1)
      this.w_ARFSRIFE = space(20)
      this.w_TIPIMP = space(1)
      this.w_MAGIMP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 21 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+21<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ARCODART as ARCODART123"+ ",link_1_23.ARDESART as ARDESART123"+ ",link_1_23.ARUNMIS1 as ARUNMIS1123"+ ",link_1_23.AROPERAT as AROPERAT123"+ ",link_1_23.ARMOLTIP as ARMOLTIP123"+ ",link_1_23.ARUNMIS2 as ARUNMIS2123"+ ",link_1_23.ARVOCCEN as ARVOCCEN123"+ ",link_1_23.ARPROPRE as ARPROPRE123"+ ",link_1_23.ARTIPGES as ARTIPGES123"+ ",link_1_23.ARGRUMER as ARGRUMER123"+ ",link_1_23.ARCODDIS as ARCODDIS123"+ ",link_1_23.ARMAGPRE as ARMAGPRE123"+ ",link_1_23.ARDTOBSO as ARDTOBSO123"+ ",link_1_23.ARFLUSEP as ARFLUSEP123"+ ",link_1_23.ARPREZUM as ARPREZUM123"+ ",link_1_23.ARCODCEN as ARCODCEN123"+ ",link_1_23.ARSALCOM as ARSALCOM123"+ ",link_1_23.ARTIPART as ARTIPART123"+ ",link_1_23.ARFSRIFE as ARFSRIFE123"+ ",link_1_23.ARTIPIMP as ARTIPIMP123"+ ",link_1_23.ARMAGIMP as ARMAGIMP123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on ODL_MAST.OLTCOART=link_1_23.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+21
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCOART=link_1_23.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+21
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRLEAMPS,PRLOTRIO,PRQTAMIN,PRGIOAPP,PRPUNRIO,PRLOTMED,PRCOEFLT,PRCODFOR,PROGGMPS,PRSAFELT,PRQTAMAX";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART)
            select PRCODART,PRLEAMPS,PRLOTRIO,PRQTAMIN,PRGIOAPP,PRPUNRIO,PRLOTMED,PRCOEFLT,PRCODFOR,PROGGMPS,PRSAFELT,PRQTAMAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.PRCODART,space(20))
      this.w_OLTLEMPS = NVL(_Link_.PRLEAMPS,0)
      this.w_LOTRIO = NVL(_Link_.PRLOTRIO,0)
      this.w_LOTECO = NVL(_Link_.PRQTAMIN,0)
      this.w_LEAFIS = NVL(_Link_.PRGIOAPP,0)
      this.w_PUNRIO = NVL(_Link_.PRPUNRIO,0)
      this.w_PUNLOT = NVL(_Link_.PRLOTRIO,0)
      this.w_LOTMED = NVL(_Link_.PRLOTMED,0)
      this.w_LEAVAR = NVL(_Link_.PRCOEFLT,0)
      this.w_FORABI = NVL(_Link_.PRCODFOR,space(15))
      this.w_OGGMPS = NVL(_Link_.PROGGMPS,space(1))
      this.w_OLTSAFLT = NVL(_Link_.PRSAFELT,0)
      this.w_LOTMAX = NVL(_Link_.PRQTAMAX,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_OLTLEMPS = 0
      this.w_LOTRIO = 0
      this.w_LOTECO = 0
      this.w_LEAFIS = 0
      this.w_PUNRIO = 0
      this.w_PUNLOT = 0
      this.w_LOTMED = 0
      this.w_LEAVAR = 0
      this.w_FORABI = space(15)
      this.w_OGGMPS = space(1)
      this.w_OLTSAFLT = 0
      this.w_LOTMAX = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTCOMAG
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_OLTCOMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_OLTCOMAG))
          select MGCODMAG,MGDESMAG,MGDISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCOMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCOMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oOLTCOMAG_1_50'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_OLTCOMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_OLTCOMAG)
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_TDESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_TDISMAG = NVL(_Link_.MGDISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOMAG = space(5)
      endif
      this.w_TDESMAG = space(30)
      this.w_TDISMAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDISMAG='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o non nettificabile")
        endif
        this.w_OLTCOMAG = space(5)
        this.w_TDESMAG = space(30)
        this.w_TDISMAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_50(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_50.MGCODMAG as MGCODMAG150"+ ",link_1_50.MGDESMAG as MGDESMAG150"+ ",link_1_50.MGDISMAG as MGDISMAG150"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_50 on ODL_MAST.OLTCOMAG=link_1_50.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_50"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCOMAG=link_1_50.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAUORD
  func Link_1_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUORD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUORD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLORDI,CMFLIMPE,CMFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CAUORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CAUORD)
            select CMCODICE,CMFLORDI,CMFLIMPE,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUORD = NVL(_Link_.CMCODICE,space(5))
      this.w_AFLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_AFLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_AFLCOMM = NVL(_Link_.CMFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUORD = space(5)
      endif
      this.w_AFLORDI = space(1)
      this.w_AFLIMPE = space(1)
      this.w_AFLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUORD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTCAMAG
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLORDI,CMFLIMPE,CMFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_OLTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_OLTCAMAG)
            select CMCODICE,CMFLORDI,CMFLIMPE,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_OLTFLORD = NVL(_Link_.CMFLORDI,space(1))
      this.w_OLTFLIMP = NVL(_Link_.CMFLIMPE,space(1))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCAMAG = space(5)
      endif
      this.w_OLTFLORD = space(1)
      this.w_OLTFLIMP = space(1)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_62(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_62.CMCODICE as CMCODICE162"+ ",link_1_62.CMFLORDI as CMFLORDI162"+ ",link_1_62.CMFLIMPE as CMFLIMPE162"+ ",link_1_62.CMFLCOMM as CMFLCOMM162"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_62 on ODL_MAST.OLTCAMAG=link_1_62.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_62"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCAMAG=link_1_62.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTUNMIS
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTUNMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_OLTUNMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_OLTUNMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTUNMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTUNMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oOLTUNMIS_1_67'),i_cWhere,'GSAR_AUM',"Unita di misura",'GSMA_MVM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTUNMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_OLTUNMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_OLTUNMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTUNMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_OLTUNMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_OLTUNMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_OLTUNMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTUNMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTCOMAG
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_OLTCOMAG);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_OLTKEYSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_OLTKEYSA;
                       ,'SLCODMAG',this.w_OLTCOMAG)
            select SLCODICE,SLCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTDISBA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTDISBA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTDISBA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_OLTDISBA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_OLTDISBA)
            select DBCODICE,DBDESCRI,DBCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTDISBA = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
      this.w_DEFCICLO = NVL(_Link_.DBCODRIS,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLTDISBA = space(20)
      endif
      this.w_DESDIS = space(40)
      this.w_DEFCICLO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTDISBA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DISMBASE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.DBCODICE as DBCODICE202"+ ",link_2_2.DBDESCRI as DBDESCRI202"+ ",link_2_2.DBCODRIS as DBCODRIS202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on ODL_MAST.OLTDISBA=link_2_2.DBCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTDISBA=link_2_2.DBCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCICLO
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TABMCICL_IDX,3]
    i_lTable = "TABMCICL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2], .t., this.TABMCICL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCICLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDS_MCS',True,'TABMCICL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_OLTCICLO)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_OLTCICLO))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCICLO)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCICLO) and !this.bDontReportError
            deferred_cp_zoom('TABMCICL','*','CSCODICE',cp_AbsName(oSource.parent,'oOLTCICLO_2_3'),i_cWhere,'GSDS_MCS',"Cicli semplificati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCICLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_OLTCICLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_OLTCICLO)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCICLO = NVL(_Link_.CSCODICE,space(15))
      this.w_DESCIC = NVL(_Link_.CSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCICLO = space(15)
      endif
      this.w_DESCIC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.TABMCICL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCICLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TABMCICL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TABMCICL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CSCODICE as CSCODICE203"+ ",link_2_3.CSDESCRI as CSDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on ODL_MAST.OLTCICLO=link_2_3.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCICLO=link_2_3.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOFOR
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_OLTCOFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_OLTTICON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_OLTTICON;
                     ,'ANCODICE',trim(this.w_OLTCOFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCOFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCOFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oOLTCOFOR_2_13'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OLTTICON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_OLTTICON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_OLTCOFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_OLTTICON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_OLTTICON;
                       ,'ANCODICE',this.w_OLTCOFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANCODVAL,ANCATCOM,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
      this.w_ANCODVAL = NVL(_Link_.ANCODVAL,space(5))
      this.w_ANCATCOM = NVL(_Link_.ANCATCOM,space(3))
      this.w_ANSCORPO = NVL(_Link_.ANSCORPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOFOR = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_MAGTER = space(5)
      this.w_ANCODVAL = space(5)
      this.w_ANCATCOM = space(3)
      this.w_ANSCORPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_OLTPROVE='L' and not empty(.w_MAGTER)) or .w_OLTPROVE='E'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
        endif
        this.w_OLTCOFOR = space(15)
        this.w_DESFOR = space(40)
        this.w_MAGTER = space(5)
        this.w_ANCODVAL = space(5)
        this.w_ANCATCOM = space(3)
        this.w_ANSCORPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTCONTR
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCONTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_OLTCONTR)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_OLTTICON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_OLTTICON;
                     ,'CONUMERO',trim(this.w_OLTCONTR))
          select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCONTR)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCONTR) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oOLTCONTR_2_16'),i_cWhere,'',"",'GSCO_AOP.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OLTTICON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_OLTTICON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCONTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_OLTCONTR);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_OLTTICON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_OLTTICON;
                       ,'CONUMERO',this.w_OLTCONTR)
            select COTIPCLF,CONUMERO,CODESCON,CODATCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCONTR = NVL(_Link_.CONUMERO,space(15))
      this.w_CONDES = NVL(_Link_.CODESCON,space(50))
      this.w_DATCON = NVL(cp_ToDate(_Link_.CODATCON),ctod("  /  /  "))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCONTR = space(15)
      endif
      this.w_CONDES = space(50)
      this.w_DATCON = ctod("  /  /  ")
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_OLTCONTR,.w_OLTTICON,.w_OLTCOFOR,.w_ANCATCOM,.w_ANSCORPO,.w_ANCODVAL,.w_OLTDTRIC,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_OLTCONTR = space(15)
        this.w_CONDES = space(50)
        this.w_DATCON = ctod("  /  /  ")
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCONTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTCOCEN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_OLTCOCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_OLTCOCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCOCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_OLTCOCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_OLTCOCEN)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OLTCOCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oOLTCOCEN_2_19'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_OLTCOCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_OLTCOCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCON = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOCEN = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.CC_CONTO as CC_CONTO219"+ ",link_2_19.CCDESPIA as CCDESPIA219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on ODL_MAST.OLTCOCEN=link_2_19.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCOCEN=link_2_19.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTVOCEN
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTVOCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_OLTVOCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_OLTVOCEN))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTVOCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTVOCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oOLTVOCEN_2_22'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTVOCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_OLTVOCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_OLTVOCEN)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTVOCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_VOCDES = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_DTOBVO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OLTVOCEN = space(15)
      endif
      this.w_VOCDES = space(40)
      this.w_TIPVOC = space(1)
      this.w_DTOBVO = ctod("  /  /  ")
      this.w_CODCOS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_OLTVOCEN = space(15)
        this.w_VOCDES = space(40)
        this.w_TIPVOC = space(1)
        this.w_DTOBVO = ctod("  /  /  ")
        this.w_CODCOS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTVOCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_22.VCCODICE as VCCODICE222"+ ",link_2_22.VCDESCRI as VCDESCRI222"+ ",link_2_22.VCTIPVOC as VCTIPVOC222"+ ",link_2_22.VCDTOBSO as VCDTOBSO222"+ ",link_2_22.VCTIPCOS as VCTIPCOS222"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_22 on ODL_MAST.OLTVOCEN=link_2_22.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_22"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTVOCEN=link_2_22.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOMME
  func Link_2_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOMME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_OLTCOMME)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_OLTCOMME))
          select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCOMME)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCOMME) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oOLTCOMME_2_26'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOMME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_OLTCOMME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_OLTCOMME)
            select CNCODCAN,CNDTOBSO,CNDESCAN,CNCODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOMME = NVL(_Link_.CNCODCAN,space(15))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(40))
      this.w_COCODVAL = NVL(_Link_.CNCODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOMME = space(15)
      endif
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DESCAN = space(40)
      this.w_COCODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_OLTCOMME = space(15)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_DESCAN = space(40)
        this.w_COCODVAL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOMME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_26.CNCODCAN as CNCODCAN226"+ ",link_2_26.CNDTOBSO as CNDTOBSO226"+ ",link_2_26.CNDESCAN as CNDESCAN226"+ ",link_2_26.CNCODVAL as CNCODVAL226"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_26 on ODL_MAST.OLTCOMME=link_2_26.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_26"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCOMME=link_2_26.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOATT
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_OLTCOATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_OLTCOMME);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_OLTTIPAT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_OLTCOMME;
                     ,'ATTIPATT',this.w_OLTTIPAT;
                     ,'ATCODATT',trim(this.w_OLTCOATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTCOATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTCOATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oOLTCOATT_2_28'),i_cWhere,'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OLTCOMME<>oSource.xKey(1);
           .or. this.w_OLTTIPAT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_OLTCOMME);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_OLTTIPAT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_OLTCOATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_OLTCOMME);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_OLTTIPAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_OLTCOMME;
                       ,'ATTIPATT',this.w_OLTTIPAT;
                       ,'ATCODATT',this.w_OLTCOATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_OLTCOATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_OLTCOMME))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� inesistente o incongruente")
        endif
        this.w_OLTCOATT = space(15)
        this.w_DESATT = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIVITA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_28.ATCODATT as ATCODATT228"+ ",link_2_28.ATDESCRI as ATDESCRI228"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_28 on ODL_MAST.OLTCOATT=link_2_28.ATCODATT"+" and ODL_MAST.OLTCOMME=link_2_28.ATCODCOM"+" and ODL_MAST.OLTTIPAT=link_2_28.ATTIPATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_28"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTCOATT=link_2_28.ATCODATT(+)"'+'+" and ODL_MAST.OLTCOMME=link_2_28.ATCODCOM(+)"'+'+" and ODL_MAST.OLTTIPAT=link_2_28.ATTIPATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTCOCOS
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MA_COSTI_IDX,3]
    i_lTable = "MA_COSTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2], .t., this.MA_COSTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTCOCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTCOCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCOS="+cp_ToStrODBC(this.w_OLTCOCOS);
                   +" and CSCODCOM="+cp_ToStrODBC(this.w_OLTCOMME);
                   +" and CSTIPSTR="+cp_ToStrODBC(this.w_OLTTIPAT);
                   +" and CSCODMAT="+cp_ToStrODBC(this.w_OLTCOATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCOM',this.w_OLTCOMME;
                       ,'CSTIPSTR',this.w_OLTTIPAT;
                       ,'CSCODMAT',this.w_OLTCOATT;
                       ,'CSCODCOS',this.w_OLTCOCOS)
            select CSCODCOM,CSTIPSTR,CSCODMAT,CSCODCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTCOCOS = NVL(_Link_.CSCODCOS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_OLTCOCOS = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MA_COSTI_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCOM,1)+'\'+cp_ToStr(_Link_.CSTIPSTR,1)+'\'+cp_ToStr(_Link_.CSCODMAT,1)+'\'+cp_ToStr(_Link_.CSCODCOS,1)
      cp_ShowWarn(i_cKey,this.MA_COSTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTCOCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTDISBA
  func Link_1_170(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTDISBA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_OLTDISBA)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBCODRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_OLTDISBA))
          select DBCODICE,DBDESCRI,DBCODRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTDISBA)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTDISBA) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oOLTDISBA_1_170'),i_cWhere,'',"",'GSCO_AOP.DISMBASE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBCODRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTDISBA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBCODRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_OLTDISBA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_OLTDISBA)
            select DBCODICE,DBDESCRI,DBCODRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTDISBA = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
      this.w_DEFCICLO = NVL(_Link_.DBCODRIS,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLTDISBA = space(20)
      endif
      this.w_DESDIS = space(40)
      this.w_DEFCICLO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTDISBA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_170(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DISMBASE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_170.DBCODICE as DBCODICE270"+ ",link_1_170.DBDESCRI as DBDESCRI270"+ ",link_1_170.DBCODRIS as DBCODRIS270"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_170 on ODL_MAST.OLTDISBA=link_1_170.DBCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_170"
          i_cKey=i_cKey+'+" and ODL_MAST.OLTDISBA=link_1_170.DBCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OLTSECIC
  func Link_1_172(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CIC_MAST_IDX,3]
    i_lTable = "CIC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2], .t., this.CIC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTSECIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCI_MCL',True,'CIC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CLSERIAL like "+cp_ToStrODBC(trim(this.w_OLTSECIC)+"%");
                   +" and CLCODART="+cp_ToStrODBC(this.w_OLTCOART);
                   +" and CLCODDIS="+cp_ToStrODBC(this.w_OLTDISBA);

          i_ret=cp_SQL(i_nConn,"select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CLCODART,CLCODDIS,CLSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CLCODART',this.w_OLTCOART;
                     ,'CLCODDIS',this.w_OLTDISBA;
                     ,'CLSERIAL',trim(this.w_OLTSECIC))
          select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CLCODART,CLCODDIS,CLSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTSECIC)==trim(_Link_.CLSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CLCODCIC like "+cp_ToStrODBC(trim(this.w_OLTSECIC)+"%");
                   +" and CLCODART="+cp_ToStrODBC(this.w_OLTCOART);
                   +" and CLCODDIS="+cp_ToStrODBC(this.w_OLTDISBA);

            i_ret=cp_SQL(i_nConn,"select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CLCODCIC like "+cp_ToStr(trim(this.w_OLTSECIC)+"%");
                   +" and CLCODART="+cp_ToStr(this.w_OLTCOART);
                   +" and CLCODDIS="+cp_ToStr(this.w_OLTDISBA);

            select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OLTSECIC) and !this.bDontReportError
            deferred_cp_zoom('CIC_MAST','*','CLCODART,CLCODDIS,CLSERIAL',cp_AbsName(oSource.parent,'oOLTSECIC_1_172'),i_cWhere,'GSCI_MCL',"Cicli di lavorazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_OLTCOART<>oSource.xKey(1);
           .or. this.w_OLTDISBA<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD";
                     +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(oSource.xKey(3));
                     +" and CLCODART="+cp_ToStrODBC(this.w_OLTCOART);
                     +" and CLCODDIS="+cp_ToStrODBC(this.w_OLTDISBA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODART',oSource.xKey(1);
                       ,'CLCODDIS',oSource.xKey(2);
                       ,'CLSERIAL',oSource.xKey(3))
            select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTSECIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD";
                   +" from "+i_cTable+" "+i_lTable+" where CLSERIAL="+cp_ToStrODBC(this.w_OLTSECIC);
                   +" and CLCODART="+cp_ToStrODBC(this.w_OLTCOART);
                   +" and CLCODDIS="+cp_ToStrODBC(this.w_OLTDISBA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CLCODART',this.w_OLTCOART;
                       ,'CLCODDIS',this.w_OLTDISBA;
                       ,'CLSERIAL',this.w_OLTSECIC)
            select CLCODART,CLCODDIS,CLSERIAL,CLCODCIC,CLDESCIC,CLFLCMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTSECIC = NVL(_Link_.CLSERIAL,space(10))
      this.w_OLTCILAV = NVL(_Link_.CLCODCIC,space(20))
      this.w_DESCILAV = NVL(_Link_.CLDESCIC,space(40))
      this.w_OLFLMODC = NVL(_Link_.CLFLCMOD,space(1))
      this.w_OLFLMODO = NVL(_Link_.CLFLCMOD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OLTSECIC = space(10)
      endif
      this.w_OLTCILAV = space(20)
      this.w_DESCILAV = space(40)
      this.w_OLFLMODC = space(1)
      this.w_OLFLMODO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CIC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CLCODART,1)+'\'+cp_ToStr(_Link_.CLCODDIS,1)+'\'+cp_ToStr(_Link_.CLSERIAL,1)
      cp_ShowWarn(i_cKey,this.CIC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTSECIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTSEODL
  func Link_1_178(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTSEODL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_OLTSEODL)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_OLTSEODL))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTSEODL)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTSEODL) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oOLTSEODL_1_178'),i_cWhere,'GSCO_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTSEODL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_OLTSEODL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_OLTSEODL)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTSEODL = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLTSEODL = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTSEODL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OLTSECPR
  func Link_1_192(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OLTSECPR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCO_BZA',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLTSECPR like "+cp_ToStrODBC(trim(this.w_OLTSECPR)+"%");

          i_ret=cp_SQL(i_nConn,"select OLTSECPR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLTSECPR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLTSECPR',trim(this.w_OLTSECPR))
          select OLTSECPR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLTSECPR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OLTSECPR)==trim(_Link_.OLTSECPR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OLTSECPR) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLTSECPR',cp_AbsName(oSource.parent,'oOLTSECPR_1_192'),i_cWhere,'GSCO_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLTSECPR";
                     +" from "+i_cTable+" "+i_lTable+" where OLTSECPR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLTSECPR',oSource.xKey(1))
            select OLTSECPR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OLTSECPR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLTSECPR";
                   +" from "+i_cTable+" "+i_lTable+" where OLTSECPR="+cp_ToStrODBC(this.w_OLTSECPR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLTSECPR',this.w_OLTSECPR)
            select OLTSECPR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OLTSECPR = NVL(_Link_.OLTSECPR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_OLTSECPR = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLTSECPR,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OLTSECPR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODODL2
  func Link_9_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODODL2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODODL2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVFLPROV";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_CODODL2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_CODODL2)
            select MVSERIAL,MVFLPROV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODODL2 = NVL(_Link_.MVSERIAL,space(10))
      this.w_ST1 = NVL(_Link_.MVFLPROV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODODL2 = space(10)
      endif
      this.w_ST1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODODL2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIC
  func Link_9_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODRIC)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_CADESAR = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIC = space(20)
      endif
      this.w_CADESAR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOLCODODL_1_8.value==this.w_OLCODODL)
      this.oPgFrm.Page1.oPag.oOLCODODL_1_8.value=this.w_OLCODODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLDATODL_1_11.value==this.w_OLDATODL)
      this.oPgFrm.Page1.oPag.oOLDATODL_1_11.value=this.w_OLDATODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.RadioValue()==this.w_OLTSTATO)
      this.oPgFrm.Page1.oPag.oOLTSTATO_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.RadioValue()==this.w_OLTSTATO)
      this.oPgFrm.Page1.oPag.oOLTSTATO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.RadioValue()==this.w_OLTSTATO)
      this.oPgFrm.Page1.oPag.oOLTSTATO_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLOPEODL_1_19.value==this.w_OLOPEODL)
      this.oPgFrm.Page1.oPag.oOLOPEODL_1_19.value=this.w_OLOPEODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCODIC_1_21.value==this.w_OLTCODIC)
      this.oPgFrm.Page1.oPag.oOLTCODIC_1_21.value=this.w_OLTCODIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOD_1_22.value==this.w_DESCOD)
      this.oPgFrm.Page1.oPag.oDESCOD_1_22.value=this.w_DESCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCOART_1_23.value==this.w_OLTCOART)
      this.oPgFrm.Page1.oPag.oOLTCOART_1_23.value=this.w_OLTCOART
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPART_1_24.RadioValue()==this.w_ARTIPART)
      this.oPgFrm.Page1.oPag.oARTIPART_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_33.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_33.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTPROVE_1_34.RadioValue()==this.w_OLTPROVE)
      this.oPgFrm.Page1.oPag.oOLTPROVE_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPGES_1_36.RadioValue()==this.w_ATIPGES)
      this.oPgFrm.Page1.oPag.oATIPGES_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCOMAG_1_50.value==this.w_OLTCOMAG)
      this.oPgFrm.Page1.oPag.oOLTCOMAG_1_50.value=this.w_OLTCOMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oTDESMAG_1_51.value==this.w_TDESMAG)
      this.oPgFrm.Page1.oPag.oTDESMAG_1_51.value=this.w_TDESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTUNMIS_1_67.value==this.w_OLTUNMIS)
      this.oPgFrm.Page1.oPag.oOLTUNMIS_1_67.value=this.w_OLTUNMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTCON_1_68.value==this.w_OLTDTCON)
      this.oPgFrm.Page1.oPag.oOLTDTCON_1_68.value=this.w_OLTDTCON
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTODL_1_70.value==this.w_OLTQTODL)
      this.oPgFrm.Page1.oPag.oOLTQTODL_1_70.value=this.w_OLTQTODL
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTOEV_1_72.value==this.w_OLTQTOEV)
      this.oPgFrm.Page1.oPag.oOLTQTOEV_1_72.value=this.w_OLTQTOEV
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTFLEVA_1_76.RadioValue()==this.w_OLTFLEVA)
      this.oPgFrm.Page1.oPag.oOLTFLEVA_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTLEMPS_1_84.value==this.w_OLTLEMPS)
      this.oPgFrm.Page1.oPag.oOLTLEMPS_1_84.value=this.w_OLTLEMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTMPS_1_85.value==this.w_OLTDTMPS)
      this.oPgFrm.Page1.oPag.oOLTDTMPS_1_85.value=this.w_OLTDTMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTLAN_1_86.value==this.w_OLTDTLAN)
      this.oPgFrm.Page1.oPag.oOLTDTLAN_1_86.value=this.w_OLTDTLAN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTEMLAV_1_87.value==this.w_OLTEMLAV)
      this.oPgFrm.Page1.oPag.oOLTEMLAV_1_87.value=this.w_OLTEMLAV
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTRIC_1_88.value==this.w_OLTDTRIC)
      this.oPgFrm.Page1.oPag.oOLTDTRIC_1_88.value=this.w_OLTDTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDINRIC_1_89.value==this.w_OLTDINRIC)
      this.oPgFrm.Page1.oPag.oOLTDINRIC_1_89.value=this.w_OLTDINRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTINI_1_91.value==this.w_OLTDTINI)
      this.oPgFrm.Page1.oPag.oOLTDTINI_1_91.value=this.w_OLTDTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDTFIN_1_92.value==this.w_OLTDTFIN)
      this.oPgFrm.Page1.oPag.oOLTDTFIN_1_92.value=this.w_OLTDTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTDISBA_2_2.value==this.w_OLTDISBA)
      this.oPgFrm.Page2.oPag.oOLTDISBA_2_2.value=this.w_OLTDISBA
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCICLO_2_3.value==this.w_OLTCICLO)
      this.oPgFrm.Page2.oPag.oOLTCICLO_2_3.value=this.w_OLTCICLO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDIS_2_4.value==this.w_DESDIS)
      this.oPgFrm.Page2.oPag.oDESDIS_2_4.value=this.w_DESDIS
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCOFOR_2_13.value==this.w_OLTCOFOR)
      this.oPgFrm.Page2.oPag.oOLTCOFOR_2_13.value=this.w_OLTCOFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_14.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_14.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCONTR_2_16.value==this.w_OLTCONTR)
      this.oPgFrm.Page2.oPag.oOLTCONTR_2_16.value=this.w_OLTCONTR
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.value==this.w_OLTCOCEN)
      this.oPgFrm.Page2.oPag.oOLTCOCEN_2_19.value=this.w_OLTCOCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_20.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_20.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.value==this.w_OLTVOCEN)
      this.oPgFrm.Page2.oPag.oOLTVOCEN_2_22.value=this.w_OLTVOCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oVOCDES_2_23.value==this.w_VOCDES)
      this.oPgFrm.Page2.oPag.oVOCDES_2_23.value=this.w_VOCDES
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.value==this.w_OLTCOMME)
      this.oPgFrm.Page2.oPag.oOLTCOMME_2_26.value=this.w_OLTCOMME
    endif
    if not(this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.value==this.w_OLTCOATT)
      this.oPgFrm.Page2.oPag.oOLTCOATT_2_28.value=this.w_OLTCOATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAN_2_32.value==this.w_DESCAN)
      this.oPgFrm.Page2.oPag.oDESCAN_2_32.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_33.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_33.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCIC_2_45.value==this.w_DESCIC)
      this.oPgFrm.Page2.oPag.oDESCIC_2_45.value=this.w_DESCIC
    endif
    if not(this.oPgFrm.Page2.oPag.oDATCON_2_46.value==this.w_DATCON)
      this.oPgFrm.Page2.oPag.oDATCON_2_46.value=this.w_DATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCONDES_2_47.value==this.w_CONDES)
      this.oPgFrm.Page2.oPag.oCONDES_2_47.value=this.w_CONDES
    endif
    if not(this.oPgFrm.Page2.oPag.oOLCRIFOR_2_64.RadioValue()==this.w_OLCRIFOR)
      this.oPgFrm.Page2.oPag.oOLCRIFOR_2_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTMSPINI_1_152.value==this.w_OLTMSPINI)
      this.oPgFrm.Page1.oPag.oOLTMSPINI_1_152.value=this.w_OLTMSPINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTMSPFIN_1_154.value==this.w_OLTMSPFIN)
      this.oPgFrm.Page1.oPag.oOLTMSPFIN_1_154.value=this.w_OLTMSPFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCRELA_1_160.RadioValue()==this.w_OLTCRELA)
      this.oPgFrm.Page1.oPag.oOLTCRELA_1_160.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSAFLT_1_163.value==this.w_OLTSAFLT)
      this.oPgFrm.Page1.oPag.oOLTSAFLT_1_163.value=this.w_OLTSAFLT
    endif
    if not(this.oPgFrm.Page1.oPag.oOLSERMRP_1_164.value==this.w_OLSERMRP)
      this.oPgFrm.Page1.oPag.oOLSERMRP_1_164.value=this.w_OLSERMRP
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODFAS_1_167.value==this.w_CACODFAS)
      this.oPgFrm.Page1.oPag.oCACODFAS_1_167.value=this.w_CACODFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTDISBA_1_170.value==this.w_OLTDISBA)
      this.oPgFrm.Page1.oPag.oOLTDISBA_1_170.value=this.w_OLTDISBA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDIS_1_171.value==this.w_DESDIS)
      this.oPgFrm.Page1.oPag.oDESDIS_1_171.value=this.w_DESDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSECIC_1_172.value==this.w_OLTSECIC)
      this.oPgFrm.Page1.oPag.oOLTSECIC_1_172.value=this.w_OLTSECIC
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCILAV_1_173.value==this.w_OLTCILAV)
      this.oPgFrm.Page1.oPag.oOLTCILAV_1_173.value=this.w_OLTCILAV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCILAV_1_176.value==this.w_DESCILAV)
      this.oPgFrm.Page1.oPag.oDESCILAV_1_176.value=this.w_DESCILAV
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSEODL_1_178.value==this.w_OLTSEODL)
      this.oPgFrm.Page1.oPag.oOLTSEODL_1_178.value=this.w_OLTSEODL
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFASE_1_180.value==this.w_NUMFASE)
      this.oPgFrm.Page1.oPag.oNUMFASE_1_180.value=this.w_NUMFASE
    endif
    if not(this.oPgFrm.Page1.oPag.oOLFLMODC_1_191.RadioValue()==this.w_OLFLMODC)
      this.oPgFrm.Page1.oPag.oOLFLMODC_1_191.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.value==this.w_OLTSECPR)
      this.oPgFrm.Page1.oPag.oOLTSECPR_1_192.value=this.w_OLTSECPR
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTCOPOI_1_193.RadioValue()==this.w_OLTCOPOI)
      this.oPgFrm.Page1.oPag.oOLTCOPOI_1_193.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTFAOUT_1_194.RadioValue()==this.w_OLTFAOUT)
      this.oPgFrm.Page1.oPag.oOLTFAOUT_1_194.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTULFAS_1_195.RadioValue()==this.w_OLTULFAS)
      this.oPgFrm.Page1.oPag.oOLTULFAS_1_195.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFASE_1_200.value==this.w_DESFASE)
      this.oPgFrm.Page1.oPag.oDESFASE_1_200.value=this.w_DESFASE
    endif
    if not(this.oPgFrm.Page10.oPag.oOL__NOTE_10_1.value==this.w_OL__NOTE)
      this.oPgFrm.Page10.oPag.oOL__NOTE_10_1.value=this.w_OL__NOTE
    endif
    if not(this.oPgFrm.Page9.oPag.oCODRIC_9_74.value==this.w_CODRIC)
      this.oPgFrm.Page9.oPag.oCODRIC_9_74.value=this.w_CODRIC
    endif
    if not(this.oPgFrm.Page9.oPag.oCADESAR_9_75.value==this.w_CADESAR)
      this.oPgFrm.Page9.oPag.oCADESAR_9_75.value=this.w_CADESAR
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAORD_9_78.value==this.w_QTAORD)
      this.oPgFrm.Page9.oPag.oQTAORD_9_78.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page9.oPag.oMAGAZ_9_79.value==this.w_MAGAZ)
      this.oPgFrm.Page9.oPag.oMAGAZ_9_79.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEMS_9_82.value==this.w_DATEMS)
      this.oPgFrm.Page9.oPag.oDATEMS_9_82.value=this.w_DATEMS
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVF_9_85.value==this.w_DATEVF)
      this.oPgFrm.Page9.oPag.oDATEVF_9_85.value=this.w_DATEVF
    endif
    if not(this.oPgFrm.Page9.oPag.oCAUMAG_9_87.value==this.w_CAUMAG)
      this.oPgFrm.Page9.oPag.oCAUMAG_9_87.value=this.w_CAUMAG
    endif
    if not(this.oPgFrm.Page9.oPag.oMAGAZ_9_89.value==this.w_MAGAZ)
      this.oPgFrm.Page9.oPag.oMAGAZ_9_89.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page9.oPag.oFORNIT_9_90.value==this.w_FORNIT)
      this.oPgFrm.Page9.oPag.oFORNIT_9_90.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page9.oPag.oQTASCA_9_93.value==this.w_QTASCA)
      this.oPgFrm.Page9.oPag.oQTASCA_9_93.value=this.w_QTASCA
    endif
    if not(this.oPgFrm.Page9.oPag.oQTARES_9_95.value==this.w_QTARES)
      this.oPgFrm.Page9.oPag.oQTARES_9_95.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMREG_9_96.value==this.w_NUMREG)
      this.oPgFrm.Page9.oPag.oNUMREG_9_96.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVA_9_98.value==this.w_DATEVA)
      this.oPgFrm.Page9.oPag.oDATEVA_9_98.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oFORNIT_9_102.value==this.w_FORNIT)
      this.oPgFrm.Page9.oPag.oFORNIT_9_102.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page9.oPag.oRIF_9_106.value==this.w_RIF)
      this.oPgFrm.Page9.oPag.oRIF_9_106.value=this.w_RIF
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAORD_9_108.value==this.w_QTAORD)
      this.oPgFrm.Page9.oPag.oQTAORD_9_108.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAEVA_9_110.value==this.w_QTAEVA)
      this.oPgFrm.Page9.oPag.oQTAEVA_9_110.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oUNIMIS1_9_112.value==this.w_UNIMIS1)
      this.oPgFrm.Page9.oPag.oUNIMIS1_9_112.value=this.w_UNIMIS1
    endif
    if not(this.oPgFrm.Page9.oPag.oCODODL__9_114.value==this.w_CODODL_)
      this.oPgFrm.Page9.oPag.oCODODL__9_114.value=this.w_CODODL_
    endif
    if not(this.oPgFrm.Page9.oPag.oCPROWNUM_9_116.value==this.w_CPROWNUM)
      this.oPgFrm.Page9.oPag.oCPROWNUM_9_116.value=this.w_CPROWNUM
    endif
    if not(this.oPgFrm.Page9.oPag.oCODODL_9_117.value==this.w_CODODL)
      this.oPgFrm.Page9.oPag.oCODODL_9_117.value=this.w_CODODL
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAORD_9_120.value==this.w_QTAORD)
      this.oPgFrm.Page9.oPag.oQTAORD_9_120.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page9.oPag.oUNIMIS_9_121.value==this.w_UNIMIS)
      this.oPgFrm.Page9.oPag.oUNIMIS_9_121.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVF_9_124.value==this.w_DATEVF)
      this.oPgFrm.Page9.oPag.oDATEVF_9_124.value=this.w_DATEVF
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAEVA_9_126.value==this.w_QTAEVA)
      this.oPgFrm.Page9.oPag.oQTAEVA_9_126.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oQTARES_9_127.value==this.w_QTARES)
      this.oPgFrm.Page9.oPag.oQTARES_9_127.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page9.oPag.oMAGAZ_9_129.value==this.w_MAGAZ)
      this.oPgFrm.Page9.oPag.oMAGAZ_9_129.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMDOC_9_130.value==this.w_NUMDOC)
      this.oPgFrm.Page9.oPag.oNUMDOC_9_130.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page9.oPag.oALFDOC_9_131.value==this.w_ALFDOC)
      this.oPgFrm.Page9.oPag.oALFDOC_9_131.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVA_9_136.value==this.w_DATEVA)
      this.oPgFrm.Page9.oPag.oDATEVA_9_136.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMREG_9_138.value==this.w_NUMREG)
      this.oPgFrm.Page9.oPag.oNUMREG_9_138.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page9.oPag.oFORNIT_9_141.value==this.w_FORNIT)
      this.oPgFrm.Page9.oPag.oFORNIT_9_141.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page9.oPag.oCODODL_9_145.value==this.w_CODODL)
      this.oPgFrm.Page9.oPag.oCODODL_9_145.value=this.w_CODODL
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAORD_9_148.value==this.w_QTAORD)
      this.oPgFrm.Page9.oPag.oQTAORD_9_148.value=this.w_QTAORD
    endif
    if not(this.oPgFrm.Page9.oPag.oUNIMIS_9_149.value==this.w_UNIMIS)
      this.oPgFrm.Page9.oPag.oUNIMIS_9_149.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVF_9_152.value==this.w_DATEVF)
      this.oPgFrm.Page9.oPag.oDATEVF_9_152.value=this.w_DATEVF
    endif
    if not(this.oPgFrm.Page9.oPag.oQTAEVA_9_154.value==this.w_QTAEVA)
      this.oPgFrm.Page9.oPag.oQTAEVA_9_154.value=this.w_QTAEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oQTARES_9_155.value==this.w_QTARES)
      this.oPgFrm.Page9.oPag.oQTARES_9_155.value=this.w_QTARES
    endif
    if not(this.oPgFrm.Page9.oPag.oMAGAZ_9_157.value==this.w_MAGAZ)
      this.oPgFrm.Page9.oPag.oMAGAZ_9_157.value=this.w_MAGAZ
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMDOC_9_158.value==this.w_NUMDOC)
      this.oPgFrm.Page9.oPag.oNUMDOC_9_158.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page9.oPag.oALFDOC_9_159.value==this.w_ALFDOC)
      this.oPgFrm.Page9.oPag.oALFDOC_9_159.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page9.oPag.oDATEVA_9_163.value==this.w_DATEVA)
      this.oPgFrm.Page9.oPag.oDATEVA_9_163.value=this.w_DATEVA
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMREG_9_165.value==this.w_NUMREG)
      this.oPgFrm.Page9.oPag.oNUMREG_9_165.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page9.oPag.oFORNIT_9_168.value==this.w_FORNIT)
      this.oPgFrm.Page9.oPag.oFORNIT_9_168.value=this.w_FORNIT
    endif
    if not(this.oPgFrm.Page9.oPag.oNUMREG_9_172.value==this.w_NUMREG)
      this.oPgFrm.Page9.oPag.oNUMREG_9_172.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTOSC_1_209.value==this.w_OLTQTOSC)
      this.oPgFrm.Page1.oPag.oOLTQTOSC_1_209.value=this.w_OLTQTOSC
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTQTPRO_1_211.value==this.w_OLTQTPRO)
      this.oPgFrm.Page1.oPag.oOLTQTPRO_1_211.value=this.w_OLTQTPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTPRIOR_1_214.value==this.w_OLTPRIOR)
      this.oPgFrm.Page1.oPag.oOLTPRIOR_1_214.value=this.w_OLTPRIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.RadioValue()==this.w_OLTSOSPE)
      this.oPgFrm.Page1.oPag.oOLTSOSPE_1_216.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ODL_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_OSTATO<>'M' OR .w_OLTSTATO $ 'MP')  and not(.w_OLTSTATO $ "SCD")  and (.w_OSTATO='M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTSTATO_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare suggerito o pianificato")
          case   not(.w_TIPGES<>"T" or .w_OLTSTATO $ 'SC')  and not(not .w_OLTSTATO $ "SCD" or .w_TIPGES="T")  and (.w_OSTATO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTSTATO_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare suggerito o confermato")
          case   not(.w_TIPGES<>"T" or .w_OLTSTATO $ 'SC')  and not(not .w_OLTSTATO $ "SC" or .w_TIPGES<>"T")  and (.w_OSTATO='S' and .cFunction<>"Query")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTSTATO_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare suggerito o confermato")
          case   (empty(.w_OLTCODIC))  and (.w_CHKAGG AND .w_STATOGES="LOAD" AND .w_OLTSTATO $ 'P-C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTCODIC_1_21.SetFocus()
            i_bnoObbl = !empty(.w_OLTCODIC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo non valido, di provenienza esterna o non associato ad una distinta base confermata")
          case   ((empty(.w_OLTCOMAG)) or not(.w_TDISMAG='S'))  and ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and !.w_OLTPROVE$'L-E'))  and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTCOMAG_1_50.SetFocus()
            i_bnoObbl = !empty(.w_OLTCOMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o non nettificabile")
          case   ((empty(.w_OLTUNMIS)) or not(CHKUNIMI(.w_OLTUNMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)))  and (EMPTY(.w_OLTUNMIS) OR (.w_OLTSTATO $ 'SCDMPL' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTUNMIS_1_67.SetFocus()
            i_bnoObbl = !empty(.w_OLTUNMIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
          case   (empty(.w_OLTDTCON))  and (Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTDTCON_1_68.SetFocus()
            i_bnoObbl = !empty(.w_OLTDTCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_OLTQTODL)) or not(.w_OLTQTODL>0))  and (NOT EMPTY(.w_OLTUNMIS) and (.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E'))  and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTQTODL_1_70.SetFocus()
            i_bnoObbl = !empty(.w_OLTQTODL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_OLTDTRIC))  and ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTDTRIC_1_88.SetFocus()
            i_bnoObbl = !empty(.w_OLTDTRIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(not empty(.w_OLTDINRIC))  and ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTDINRIC_1_89.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di previsto inizio lavorazione non definita")
          case   not((.w_OLTPROVE='L' and not empty(.w_MAGTER)) or .w_OLTPROVE='E')  and (.w_OLTPROVE $ 'E-L' AND .w_OLTSTATO $ 'MPSCD')  and not(empty(.w_OLTCOFOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLTCOFOR_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o magazzino terzista non definito")
          case   not(CHKCONTR(.w_OLTCONTR,.w_OLTTICON,.w_OLTCOFOR,.w_ANCATCOM,.w_ANSCORPO,.w_ANCODVAL,.w_OLTDTRIC,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and (NOT EMPTY(.w_OLTCOFOR) AND .w_OLTPROVE $ 'E-L' and .w_OLTSTATO $ 'M-P-S-C-D')  and not(empty(.w_OLTCONTR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLTCONTR_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPVOC<>'R' and (EMPTY(.w_DTOBVO) OR .w_DTOBVO>.w_OBTEST))  and not(g_PERCCR<>'S')  and (g_PERCCR='S' AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')))  and not(empty(.w_OLTVOCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLTVOCEN_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)  and not(g_PERCAN<>'S' and g_COMM<>'S')  and ((g_PERCAN='S' or g_COMM='S') AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')))  and not(empty(.w_OLTCOMME))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLTCOMME_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(NOT EMPTY(.w_OLTCOATT) OR NOT (g_COMM='S' AND NOT EMPTY(.w_OLTCOMME)))  and not(g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))  and ((g_PERCAN='S' or g_COMM='S') AND NOT EMPTY(.w_OLTCOMME) AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) )  and not(empty(.w_OLTCOATT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oOLTCOATT_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� inesistente o incongruente")
          case   not(.w_OLTPRIOR>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLTPRIOR_1_214.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCO_MOL.CheckForm()
      if i_bres
        i_bres=  .GSCO_MOL.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MCS.CheckForm()
      if i_bres
        i_bres=  .GSCO_MCS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MCL.CheckForm()
      if i_bres
        i_bres=  .GSCO_MCL.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MRF.CheckForm()
      if i_bres
        i_bres=  .GSCO_MRF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      *i_bRes = i_bRes .and. .GSCO_MMO.CheckForm()
      if i_bres
        i_bres=  .GSCO_MMO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsco_aop
      * Prima esegue i controlli finali
      .w_TESTFORM=.T.
      if i_bRes
         .NotifyEvent('Controlli Finali')
         i_bRes = .w_TESTFORM
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OLCODODL = this.w_OLCODODL
    this.o_OLTSTATO = this.w_OLTSTATO
    this.o_OLTCODIC = this.w_OLTCODIC
    this.o_OLTCOART = this.w_OLTCOART
    this.o_ARTIPART = this.w_ARTIPART
    this.o_OLTPROVE = this.w_OLTPROVE
    this.o_OLTCAMAG = this.w_OLTCAMAG
    this.o_OLTUNMIS = this.w_OLTUNMIS
    this.o_OLTDTCON = this.w_OLTDTCON
    this.o_OLTQTODL = this.w_OLTQTODL
    this.o_OLTQTOEV = this.w_OLTQTOEV
    this.o_OLTDTRIC = this.w_OLTDTRIC
    this.o_OLTDINRIC = this.w_OLTDINRIC
    this.o_OLTDISBA = this.w_OLTDISBA
    this.o_OLTCICLO = this.w_OLTCICLO
    this.o_OLTCOFOR = this.w_OLTCOFOR
    this.o_OLTCONTR = this.w_OLTCONTR
    this.o_OLTCOATT = this.w_OLTCOATT
    this.o_OLTFLIMC = this.w_OLTFLIMC
    this.o_OLTSECIC = this.w_OLTSECIC
    this.o_OLFLMODO = this.w_OLFLMODO
    this.o_FLVEAC = this.w_FLVEAC
    this.o_DOCINI = this.w_DOCINI
    this.o_DATINI = this.w_DATINI
    this.o_CATDOC = this.w_CATDOC
    this.o_OLTQTOSC = this.w_OLTQTOSC
    this.o_OLTQTPRO = this.w_OLTQTPRO
    * --- GSCO_MOL : Depends On
    this.GSCO_MOL.SaveDependsOn()
    * --- GSCO_MCS : Depends On
    this.GSCO_MCS.SaveDependsOn()
    * --- GSCO_MCL : Depends On
    this.GSCO_MCL.SaveDependsOn()
    * --- GSCO_MRF : Depends On
    this.GSCO_MRF.SaveDependsOn()
    * --- GSCO_MMO : Depends On
    this.GSCO_MMO.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsco_aopPag1 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOLCODODL_1_8 as StdField with uid="JYHDWBXNBQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OLCODODL", cQueryName = "OLCODODL",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ordine",;
    HelpContextID = 151641138,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=128, Left=109, Top=10, cSayPict='"999999999999999"', cGetPict='"999999999999999"', InputMask=replicate('X',15)

  add object oOLDATODL_1_11 as StdField with uid="CVPWDIYIVS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OLDATODL", cQueryName = "OLDATODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione ordine",;
    HelpContextID = 167504946,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=355, Top=10


  add object oOLTSTATO_1_13 as StdCombo with uid="WBGAHSFZTV",rtseq=11,rtrep=.f.,left=713,top=10,width=122,height=21;
    , ToolTipText = "Stato ordine";
    , HelpContextID = 66130891;
    , cFormVar="w_OLTSTATO",RowSource=""+"Suggerito,"+"Pianificato,"+"Lanciato,"+"Finito", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Selezionare suggerito o pianificato";
  , bGlobalFont=.t.


  func oOLTSTATO_1_13.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'P',;
    iif(this.value =3,'L',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oOLTSTATO_1_13.GetRadio()
    this.Parent.oContained.w_OLTSTATO = this.RadioValue()
    return .t.
  endfunc

  func oOLTSTATO_1_13.SetRadio()
    this.Parent.oContained.w_OLTSTATO=trim(this.Parent.oContained.w_OLTSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_OLTSTATO=='M',1,;
      iif(this.Parent.oContained.w_OLTSTATO=='P',2,;
      iif(this.Parent.oContained.w_OLTSTATO=='L',3,;
      iif(this.Parent.oContained.w_OLTSTATO=='F',4,;
      0))))
  endfunc

  func oOLTSTATO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTATO='M')
    endwith
   endif
  endfunc

  func oOLTSTATO_1_13.mHide()
    with this.Parent.oContained
      return (.w_OLTSTATO $ "SCD")
    endwith
  endfunc

  func oOLTSTATO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_OSTATO<>'M' OR .w_OLTSTATO $ 'MP')
    endwith
    return bRes
  endfunc


  add object oOLTSTATO_1_14 as StdCombo with uid="GSNNGIBOTH",rtseq=12,rtrep=.f.,left=713,top=10,width=122,height=21;
    , ToolTipText = "Stato ordine";
    , HelpContextID = 66130891;
    , cFormVar="w_OLTSTATO",RowSource=""+"Suggerito,"+"Confermato,"+"Da pianificare", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Selezionare suggerito o confermato";
  , bGlobalFont=.t.


  func oOLTSTATO_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oOLTSTATO_1_14.GetRadio()
    this.Parent.oContained.w_OLTSTATO = this.RadioValue()
    return .t.
  endfunc

  func oOLTSTATO_1_14.SetRadio()
    this.Parent.oContained.w_OLTSTATO=trim(this.Parent.oContained.w_OLTSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_OLTSTATO=='S',1,;
      iif(this.Parent.oContained.w_OLTSTATO=='C',2,;
      iif(this.Parent.oContained.w_OLTSTATO=='D',3,;
      0)))
  endfunc

  func oOLTSTATO_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTATO='S')
    endwith
   endif
  endfunc

  func oOLTSTATO_1_14.mHide()
    with this.Parent.oContained
      return (not .w_OLTSTATO $ "SCD" or .w_TIPGES="T")
    endwith
  endfunc

  func oOLTSTATO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TIPGES<>"T" or .w_OLTSTATO $ 'SC')
    endwith
    return bRes
  endfunc


  add object oOLTSTATO_1_15 as StdCombo with uid="TBTSDVRRYW",rtseq=13,rtrep=.f.,left=713,top=10,width=122,height=21;
    , ToolTipText = "Stato ordine";
    , HelpContextID = 66130891;
    , cFormVar="w_OLTSTATO",RowSource=""+"Suggerito,"+"Confermato", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Selezionare suggerito o confermato";
  , bGlobalFont=.t.


  func oOLTSTATO_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oOLTSTATO_1_15.GetRadio()
    this.Parent.oContained.w_OLTSTATO = this.RadioValue()
    return .t.
  endfunc

  func oOLTSTATO_1_15.SetRadio()
    this.Parent.oContained.w_OLTSTATO=trim(this.Parent.oContained.w_OLTSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_OLTSTATO=='S',1,;
      iif(this.Parent.oContained.w_OLTSTATO=='C',2,;
      0))
  endfunc

  func oOLTSTATO_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OSTATO='S' and .cFunction<>"Query")
    endwith
   endif
  endfunc

  func oOLTSTATO_1_15.mHide()
    with this.Parent.oContained
      return (not .w_OLTSTATO $ "SC" or .w_TIPGES<>"T")
    endwith
  endfunc

  func oOLTSTATO_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TIPGES<>"T" or .w_OLTSTATO $ 'SC')
    endwith
    return bRes
  endfunc

  add object oOLOPEODL_1_19 as StdField with uid="DKULDRTMPK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_OLOPEODL", cQueryName = "OLOPEODL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operatore che ha emesso l'ordine",;
    HelpContextID = 152804402,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=352, Top=35, cSayPict='"9999"', cGetPict='"9999"'

  add object oOLTCODIC_1_21 as StdField with uid="FTIHOJUKQS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_OLTCODIC", cQueryName = "OLTCODIC",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo non valido, di provenienza esterna o non associato ad una distinta base confermata",;
    ToolTipText = "Codice richiesto",;
    HelpContextID = 246344745,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_OLTCODIC"

  func oOLTCODIC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHKAGG AND .w_STATOGES="LOAD" AND .w_OLTSTATO $ 'P-C')
    endwith
   endif
  endfunc

  func oOLTCODIC_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCODIC_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCODIC_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oOLTCODIC_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oOLTCODIC_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_OLTCODIC
     i_obj.ecpSave()
  endproc

  add object oDESCOD_1_22 as StdField with uid="FSSMKMRFVU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCOD", cQueryName = "DESCOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 22096842,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=266, Top=60, InputMask=replicate('X',40)

  add object oOLTCOART_1_23 as StdField with uid="ALMGETYVTX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_OLTCOART", cQueryName = "OLTCOART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 72422342,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=85, InputMask=replicate('X',20), cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_OLTCOART"

  func oOLTCOART_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_OLTSECIC)
        bRes2=.link_1_172('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oARTIPART_1_24 as StdCombo with uid="LBROIAIJQP",rtseq=20,rtrep=.f.,left=713,top=110,width=122,height=21, enabled=.f.;
    , ToolTipText = "Tipo articolo";
    , HelpContextID = 70979238;
    , cFormVar="w_ARTIPART",RowSource=""+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma,"+"Articolo di fase", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPART_1_24.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'MP',;
    iif(this.value =4,'PH',;
    iif(this.value =5,'FS',;
    space(1)))))))
  endfunc
  func oARTIPART_1_24.GetRadio()
    this.Parent.oContained.w_ARTIPART = this.RadioValue()
    return .t.
  endfunc

  func oARTIPART_1_24.SetRadio()
    this.Parent.oContained.w_ARTIPART=trim(this.Parent.oContained.w_ARTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPART=='PF',1,;
      iif(this.Parent.oContained.w_ARTIPART=='SE',2,;
      iif(this.Parent.oContained.w_ARTIPART=='MP',3,;
      iif(this.Parent.oContained.w_ARTIPART=='PH',4,;
      iif(this.Parent.oContained.w_ARTIPART=='FS',5,;
      0)))))
  endfunc

  add object oDESART_1_33 as StdField with uid="RSNBXTBVUL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 19082186,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=266, Top=85, InputMask=replicate('X',40)


  add object oOLTPROVE_1_34 as StdCombo with uid="ANIOLEHIQU",rtseq=29,rtrep=.f.,left=713,top=35,width=122,height=21;
    , ToolTipText = "Provenienza: interna, esterna o conto lavoro";
    , HelpContextID = 166456363;
    , cFormVar="w_OLTPROVE",RowSource=""+"Interna,"+"Esterna,"+"Conto lavoro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOLTPROVE_1_34.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    space(1)))))
  endfunc
  func oOLTPROVE_1_34.GetRadio()
    this.Parent.oContained.w_OLTPROVE = this.RadioValue()
    return .t.
  endfunc

  func oOLTPROVE_1_34.SetRadio()
    this.Parent.oContained.w_OLTPROVE=trim(this.Parent.oContained.w_OLTPROVE)
    this.value = ;
      iif(this.Parent.oContained.w_OLTPROVE=='I',1,;
      iif(this.Parent.oContained.w_OLTPROVE=='E',2,;
      iif(this.Parent.oContained.w_OLTPROVE=='L',3,;
      0)))
  endfunc

  func oOLTPROVE_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLTSTATO $ 'SCDMP' and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc


  add object oATIPGES_1_36 as StdCombo with uid="AWNRSYPFWB",rtseq=30,rtrep=.f.,left=713,top=60,width=122,height=21, enabled=.f.;
    , ToolTipText = "Tipo di gestione";
    , HelpContextID = 12893434;
    , cFormVar="w_ATIPGES",RowSource=""+"a scorta,"+"a fabbisogno,"+"a consumo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATIPGES_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'F',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oATIPGES_1_36.GetRadio()
    this.Parent.oContained.w_ATIPGES = this.RadioValue()
    return .t.
  endfunc

  func oATIPGES_1_36.SetRadio()
    this.Parent.oContained.w_ATIPGES=trim(this.Parent.oContained.w_ATIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_ATIPGES=='S',1,;
      iif(this.Parent.oContained.w_ATIPGES=='F',2,;
      iif(this.Parent.oContained.w_ATIPGES=='C',3,;
      0)))
  endfunc

  add object oOLTCOMAG_1_50 as StdField with uid="CJAEOOURKM",rtseq=42,rtrep=.f.,;
    cFormVar = "w_OLTCOMAG", cQueryName = "OLTCOMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o non nettificabile",;
    ToolTipText = "Codice magazzino produzione",;
    HelpContextID = 139531219,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=109, Top=257, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_OLTCOMAG"

  func oOLTCOMAG_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and !.w_OLTPROVE$'L-E'))  and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oOLTCOMAG_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCOMAG_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCOMAG_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oOLTCOMAG_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oOLTCOMAG_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_OLTCOMAG
     i_obj.ecpSave()
  endproc

  add object oTDESMAG_1_51 as StdField with uid="FZBNCPKMCC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_TDESMAG", cQueryName = "TDESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 194901046,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=167, Top=257, InputMask=replicate('X',30)

  add object oOLTUNMIS_1_67 as StdField with uid="QUZXOUGRNP",rtseq=59,rtrep=.f.,;
    cFormVar = "w_OLTUNMIS", cQueryName = "OLTUNMIS",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
    HelpContextID = 129035321,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=475, Top=257, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_OLTUNMIS"

  func oOLTUNMIS_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_OLTUNMIS) OR (.w_OLTSTATO $ 'SCDMPL' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3))))
    endwith
   endif
  endfunc

  func oOLTUNMIS_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTUNMIS_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTUNMIS_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oOLTUNMIS_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'GSMA_MVM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oOLTUNMIS_1_67.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_OLTUNMIS
     i_obj.ecpSave()
  endproc

  add object oOLTDTCON_1_68 as StdField with uid="XMACHGFXRC",rtseq=60,rtrep=.f.,;
    cFormVar = "w_OLTDTCON", cQueryName = "OLTDTCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data richiesta disponibilit� materiale richiesto",;
    HelpContextID = 33559500,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=762, Top=257

  func oOLTDTCON_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  add object oOLTQTODL_1_70 as StdField with uid="MLCNKQZHUI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_OLTQTODL", cQueryName = "OLTQTODL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� da produrre (ordinata)",;
    HelpContextID = 168619058,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=109, Top=283, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  proc oOLTQTODL_1_70.mDefault
    with this.Parent.oContained
      if empty(.w_OLTQTODL)
        .w_OLTQTODL = iif(.w_ATIPGES='S', .w_LOTRIO, .w_LOTECO)
      endif
    endwith
  endproc

  func oOLTQTODL_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_OLTUNMIS) and (.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E'))  and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oOLTQTODL_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_OLTQTODL>0)
    endwith
    return bRes
  endfunc

  add object oOLTQTOEV_1_72 as StdField with uid="OKSRKBMHNS",rtseq=62,rtrep=.f.,;
    cFormVar = "w_OLTQTOEV", cQueryName = "OLTQTOEV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� gi� prodotta",;
    HelpContextID = 168619068,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=286, Top=283, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  add object oOLTFLEVA_1_76 as StdCheck with uid="AFRCDQEPBT",rtseq=65,rtrep=.f.,left=555, top=257, caption="Completato", enabled=.f.,;
    ToolTipText = "Se attivo: ordine completato",;
    HelpContextID = 260172839,;
    cFormVar="w_OLTFLEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLTFLEVA_1_76.RadioValue()
    return(iif(this.value =1,"S",;
    ' '))
  endfunc
  func oOLTFLEVA_1_76.GetRadio()
    this.Parent.oContained.w_OLTFLEVA = this.RadioValue()
    return .t.
  endfunc

  func oOLTFLEVA_1_76.SetRadio()
    this.Parent.oContained.w_OLTFLEVA=trim(this.Parent.oContained.w_OLTFLEVA)
    this.value = ;
      iif(this.Parent.oContained.w_OLTFLEVA=="S",1,;
      0)
  endfunc

  add object oOLTLEMPS_1_84 as StdField with uid="JJAHDQLKCA",rtseq=68,rtrep=.f.,;
    cFormVar = "w_OLTLEMPS", cQueryName = "OLTLEMPS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lead time globale (verifiche temporali)",;
    HelpContextID = 149427143,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=174, Top=336, cSayPict='"999999"', cGetPict='"999999"'

  add object oOLTDTMPS_1_85 as StdField with uid="ZJIPWTHOCD",rtseq=69,rtrep=.f.,;
    cFormVar = "w_OLTDTMPS", cQueryName = "OLTDTMPS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di previsto inizio per verifiche temporali del piano",;
    HelpContextID = 134222791,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=512, Top=336

  add object oOLTDTLAN_1_86 as StdField with uid="ZDOROXNAXK",rtseq=70,rtrep=.f.,;
    cFormVar = "w_OLTDTLAN", cQueryName = "OLTDTLAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data lancio ODL",;
    HelpContextID = 151000012,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=762, Top=336

  add object oOLTEMLAV_1_87 as StdField with uid="JNRKKCTJLL",rtseq=71,rtrep=.f.,;
    cFormVar = "w_OLTEMLAV", cQueryName = "OLTEMLAV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lead time complessivo di produzione (fisso + variabile)",;
    HelpContextID = 158274500,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=174, Top=363, cSayPict='"999999"', cGetPict='"999999"'

  add object oOLTDTRIC_1_88 as StdField with uid="NYBWVBVKBS",rtseq=72,rtrep=.f.,;
    cFormVar = "w_OLTDTRIC", cQueryName = "OLTDTRIC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine prevista lavorazione",;
    HelpContextID = 218098729,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=512, Top=390

  func oOLTDTRIC_1_88.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  add object oOLTDINRIC_1_89 as StdField with uid="PLQQJEFRVF",rtseq=73,rtrep=.f.,;
    cFormVar = "w_OLTDINRIC", cQueryName = "OLTDINRIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di previsto inizio lavorazione non definita",;
    ToolTipText = "Data di previsto inizio lavorazione",;
    HelpContextID = 128978849,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=512, Top=363

  func oOLTDINRIC_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_OLTSTATO $ 'SCDMP' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) and Inlist(.cFunction, 'Load', 'Edit') and .w_ARTIPART<>'FS')
    endwith
   endif
  endfunc

  func oOLTDINRIC_1_89.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_OLTDINRIC))
    endwith
    return bRes
  endfunc

  add object oOLTDTINI_1_91 as StdField with uid="GLRHJJINHL",rtseq=74,rtrep=.f.,;
    cFormVar = "w_OLTDTINI", cQueryName = "OLTDTINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio effettiva ODL",;
    HelpContextID = 201331665,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=762, Top=363

  add object oOLTDTFIN_1_92 as StdField with uid="CDNEFBBCZH",rtseq=75,rtrep=.f.,;
    cFormVar = "w_OLTDTFIN", cQueryName = "OLTDTFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine ODL",;
    HelpContextID = 16772148,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=762, Top=390


  add object oObj_1_101 as cp_runprogram with uid="XIMJWGGOWY",left=9, top=509, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('TITOLO')",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riassegna titolo e zoom a gestione";
    , HelpContextID = 38149454


  add object oObj_1_102 as cp_runprogram with uid="YJMSCAUSBL",left=9, top=529, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('CONTROLLI')",;
    cEvent = "Controlli Finali",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_105 as cp_runprogram with uid="PYGBDGOSMT",left=9, top=549, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('DEL<=END')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_106 as cp_runprogram with uid="GDAQIMISQS",left=9, top=569, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('EDITSTARTED')",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_107 as cp_runprogram with uid="LLHADEODOQ",left=9, top=489, width=607,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('LEGGECOSTA')",;
    cEvent = "w_OLTCODIC Changed,w_OLTCOMAG Changed,w_OLTCOMME Changed,w_OLTQTODL Changed,w_OLTUNMIS Changed",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_108 as cp_runprogram with uid="GQALRTXRLJ",left=195, top=509, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('DATAFINVAR')",;
    cEvent = "w_OLTDTRIC Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo il periodo (oltperas) e data inizio (oltdinric)";
    , HelpContextID = 38149454


  add object oObj_1_109 as cp_runprogram with uid="IFHHJUYFFP",left=195, top=529, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('DATAINIVAR')",;
    cEvent = "w_OLTDINRIC Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo il periodo (oltperas) e data inizio (oltdinric)";
    , HelpContextID = 38149454


  add object oObj_1_110 as cp_runprogram with uid="TEYQJAZVKA",left=195, top=549, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('DATACONVAR')",;
    cEvent = "w_OLTDTCON Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo il periodo (oltperas) e data inizio (oltdinric)";
    , HelpContextID = 38149454


  add object oObj_1_112 as cp_runprogram with uid="TJOWLBPOCX",left=9, top=589, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('CHIUSURA')",;
    cEvent = "Done",;
    nPag=1;
    , ToolTipText = "Eventualmente aggiorna zoom sottostante...";
    , HelpContextID = 38149454


  add object oObj_1_113 as cp_runprogram with uid="SIYYGYFHQY",left=195, top=569, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('CHANGEFOR')",;
    cEvent = "w_OLTCOFOR Changed",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_114 as cp_runprogram with uid="DENAKWBADL",left=195, top=589, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('PROVENIENZA')",;
    cEvent = "w_OLTPROVE Changed",;
    nPag=1;
    , HelpContextID = 38149454


  add object oObj_1_115 as cp_runprogram with uid="AYFIVTCTVG",left=195, top=609, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('QTAVAR')",;
    cEvent = "w_OLTQTODL Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo LT produzione e data inizio (oltdinric)";
    , HelpContextID = 38149454


  add object oObj_1_116 as cp_runprogram with uid="KPWYULIIPM",left=195, top=629, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('CODICEVAR')",;
    cEvent = "w_OLTCODIC Changed",;
    nPag=1;
    , HelpContextID = 38149454


  add object oBtn_1_117 as StdButton with uid="PDKTCPJBUY",left=809, top=163, width=7,height=9,;
    caption="Button", nPag=1;
    , HelpContextID = 85847018;
  , bGlobalFont=.t.


  func oBtn_1_117.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STATOGES<>"EDIT")
     endwith
    endif
  endfunc


  add object oBtn_1_118 as StdButton with uid="HCEKVHIICD",left=788, top=135, width=48,height=45,;
    CpPicture="bmp\esplodi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per esplodere i componenti collegati all'ODL";
    , HelpContextID = 14275142;
    , tabstop=.f., Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_118.Click()
      with this.Parent.oContained
        GSCO_BOL(this.Parent.oContained,"CARICA_DETT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_118.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OLTCODIC) and not empty(.w_OLTDTRIC) and .w_OLTSTATO $ "MP" and .w_OLTQTODL>0 and not empty(.w_OLTUNMIS) and .cFunction<>"Query" and .w_ARTIPART<>'FS')
      endwith
    endif
  endfunc

  func oBtn_1_118.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OLTSTATO $ 'SCD' OR .w_OLTPROVE="E")
     endwith
    endif
  endfunc


  add object oObj_1_123 as cp_runprogram with uid="MCANUIWZKV",left=9, top=609, width=178,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('NUOVO')",;
    cEvent = "New record",;
    nPag=1;
    , ToolTipText = "Riassegna titolo e zoom a gestione";
    , HelpContextID = 38149454


  add object oObj_1_140 as cp_calclbl with uid="FZFOVLMYWA",left=33, top=365, width=138,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Lead Time complessivo:",Alignment=1,;
    nPag=1;
    , HelpContextID = 267230746


  add object oObj_1_141 as cp_calclbl with uid="OILLIAYIPG",left=371, top=365, width=138,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Data inizio produzione:",Alignment=1,;
    nPag=1;
    , HelpContextID = 267230746


  add object oObj_1_142 as cp_calclbl with uid="SATULDMSTK",left=371, top=391, width=138,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Data fine produzione:",Alignment=1,;
    nPag=1;
    , HelpContextID = 267230746


  add object oObj_1_143 as cp_calclbl with uid="WUFJTGKVEI",left=244, top=12, width=109,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Emissione:",Alignment=1,;
    nPag=1;
    , HelpContextID = 267230746

  add object oOLTMSPINI_1_152 as StdField with uid="OFABKCZSIS",rtseq=157,rtrep=.f.,;
    cFormVar = "w_OLTMSPINI", cQueryName = "OLTMSPINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio MSProject",;
    HelpContextID = 184086724,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=97, Top=442

  add object oOLTMSPFIN_1_154 as StdField with uid="VKOHCYKCMR",rtseq=158,rtrep=.f.,;
    cFormVar = "w_OLTMSPFIN", cQueryName = "OLTMSPFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine MSProject",;
    HelpContextID = 184086799,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=273, Top=443


  add object oObj_1_157 as cp_runprogram with uid="GICREGRZNQ",left=437, top=509, width=221,height=19,;
    caption='GSCO_BOL(SALDIMPS)',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('SALDIMPS')",;
    cEvent = "Delete start,Insert start,Update start",;
    nPag=1;
    , HelpContextID = 79006030


  add object oObj_1_159 as cp_runprogram with uid="LGPINFSPBV",left=437, top=529, width=221,height=19,;
    caption='GSCO_BOL(SALDICOM)',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('SALDICOM')",;
    cEvent = "Delete start,Insert start,Update start",;
    nPag=1;
    , HelpContextID = 189429329


  add object oOLTCRELA_1_160 as StdCombo with uid="IRAONKGDPR",rtseq=160,rtrep=.f.,left=713,top=85,width=122,height=21, enabled=.f.;
    , HelpContextID = 266267687;
    , cFormVar="w_OLTCRELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOLTCRELA_1_160.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oOLTCRELA_1_160.GetRadio()
    this.Parent.oContained.w_OLTCRELA = this.RadioValue()
    return .t.
  endfunc

  func oOLTCRELA_1_160.SetRadio()
    this.Parent.oContained.w_OLTCRELA=trim(this.Parent.oContained.w_OLTCRELA)
    this.value = ;
      iif(this.Parent.oContained.w_OLTCRELA=='A',1,;
      iif(this.Parent.oContained.w_OLTCRELA=='M',2,;
      iif(this.Parent.oContained.w_OLTCRELA=='G',3,;
      iif(this.Parent.oContained.w_OLTCRELA=='N',4,;
      0))))
  endfunc

  add object oOLTSAFLT_1_163 as StdField with uid="QYOFLSAJEG",rtseq=161,rtrep=.f.,;
    cFormVar = "w_OLTSAFLT", cQueryName = "OLTSAFLT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 266267706,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=174, Top=390

  add object oOLSERMRP_1_164 as StdField with uid="BCEYQVTDRH",rtseq=162,rtrep=.f.,;
    cFormVar = "w_OLSERMRP", cQueryName = "OLSERMRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale di elaborazione MRP",;
    HelpContextID = 136258506,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=154, Top=35, InputMask=replicate('X',10)

  func oOLSERMRP_1_164.mHide()
    with this.Parent.oContained
      return (g_MMRP<>'S' or .w_TIPGES='S' or (.w_OGGMPS='S' and .w_OLTSTATO$'SCD'))
    endwith
  endfunc

  add object oCACODFAS_1_167 as StdField with uid="TNJCIDKFXW",rtseq=163,rtrep=.f.,;
    cFormVar = "w_CACODFAS", cQueryName = "",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(66), bMultilanguage =  .f.,;
    HelpContextID = 267792263,;
   bGlobalFont=.t.,;
    Height=21, Width=450, Left=109, Top=110, InputMask=replicate('X',66)

  func oCACODFAS_1_167.mHide()
    with this.Parent.oContained
      return (g_PRFA<>'S' OR .w_ARTIPART<>'FS')
    endwith
  endfunc

  add object oOLTDISBA_1_170 as StdField with uid="SXIPTTNCDQ",rtseq=164,rtrep=.f.,;
    cFormVar = "w_OLTDISBA", cQueryName = "OLTDISBA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta base associata all'articolo",;
    HelpContextID = 45093849,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=109, Top=135, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_OLTDISBA"

  func oOLTDISBA_1_170.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' AND g_CICLILAV='S' AND Empty(.w_OLTSEODL) and .w_CHKAGG)
    endwith
   endif
  endfunc

  func oOLTDISBA_1_170.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_170('Part',this)
      if .not. empty(.w_OLTSECIC)
        bRes2=.link_1_172('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oOLTDISBA_1_170.ecpDrop(oSource)
    this.Parent.oContained.link_1_170('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTDISBA_1_170.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oOLTDISBA_1_170'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCO_AOP.DISMBASE_VZM',this.parent.oContained
  endproc

  add object oDESDIS_1_171 as StdField with uid="DYTGUCEUEM",rtseq=165,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45099978,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=266, Top=135, InputMask=replicate('X',40)

  add object oOLTSECIC_1_172 as StdField with uid="GKVPUAKVOV",rtseq=166,rtrep=.f.,;
    cFormVar = "w_OLTSECIC", cQueryName = "OLTSECIC",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 220130345,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=109, Top=183, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CIC_MAST", cZoomOnZoom="GSCI_MCL", oKey_1_1="CLCODART", oKey_1_2="this.w_OLTCOART", oKey_2_1="CLCODDIS", oKey_2_2="this.w_OLTDISBA", oKey_3_1="CLSERIAL", oKey_3_2="this.w_OLTSECIC"

  func oOLTSECIC_1_172.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PRFA='S' and g_CICLILAV='S' and .w_HAILCIC and .w_OLTSTATO $ 'SCDMP' and not empty(.w_OLTUNMIS) and .cFunction<>"Query" and .w_OLTQTODL>0 and .w_OLTPROVE<>"E" and .w_OLTQTOEV=0 AND Empty(.w_OLTSEODL))
    endwith
   endif
  endfunc

  func oOLTSECIC_1_172.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_172('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTSECIC_1_172.ecpDrop(oSource)
    this.Parent.oContained.link_1_172('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTSECIC_1_172.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CIC_MAST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CLCODART="+cp_ToStrODBC(this.Parent.oContained.w_OLTCOART)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CLCODDIS="+cp_ToStrODBC(this.Parent.oContained.w_OLTDISBA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CLCODART="+cp_ToStr(this.Parent.oContained.w_OLTCOART)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CLCODDIS="+cp_ToStr(this.Parent.oContained.w_OLTDISBA)
    endif
    do cp_zoom with 'CIC_MAST','*','CLCODART,CLCODDIS,CLSERIAL',cp_AbsName(this.parent,'oOLTSECIC_1_172'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCI_MCL',"Cicli di lavorazione",'',this.parent.oContained
  endproc
  proc oOLTSECIC_1_172.mZoomOnZoom
    local i_obj
    i_obj=GSCI_MCL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CLCODART=w_OLTCOART
    i_obj.CLCODDIS=w_OLTDISBA
     i_obj.w_CLSERIAL=this.parent.oContained.w_OLTSECIC
     i_obj.ecpSave()
  endproc

  add object oOLTCILAV_1_173 as StdField with uid="PCJYXUVICL",rtseq=167,rtrep=.f.,;
    cFormVar = "w_OLTCILAV", cQueryName = "OLTCILAV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo di lavorazione",;
    HelpContextID = 162599876,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=201, Top=183, InputMask=replicate('X',20)

  add object oDESCILAV_1_176 as StdField with uid="KDOEPTRXZB",rtseq=168,rtrep=.f.,;
    cFormVar = "w_DESCILAV", cQueryName = "DESCILAV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162605940,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=371, Top=183, InputMask=replicate('X',40)

  add object oOLTSEODL_1_178 as StdField with uid="BRDHKYJCRI",rtseq=169,rtrep=.f.,;
    cFormVar = "w_OLTSEODL", cQueryName = "OLTSEODL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'ODL che ha generato l'ordine di fase",;
    HelpContextID = 153021490,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=110, Top=209, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLCODODL", oKey_1_2="this.w_OLTSEODL"

  func oOLTSEODL_1_178.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=2)
    endwith
   endif
  endfunc

  func oOLTSEODL_1_178.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_178('Part',this)
      if .not. empty(.w_OLTFAODL)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oOLTSEODL_1_178.ecpDrop(oSource)
    this.Parent.oContained.link_1_178('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTSEODL_1_178.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oOLTSEODL_1_178'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"",'',this.parent.oContained
  endproc
  proc oOLTSEODL_1_178.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLCODODL=this.parent.oContained.w_OLTSEODL
     i_obj.ecpSave()
  endproc

  add object oNUMFASE_1_180 as StdField with uid="DDUXSEXTBV",rtseq=170,rtrep=.f.,;
    cFormVar = "w_NUMFASE", cQueryName = "NUMFASE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Fase dell'ODL che ha generato C/Lavoro di fase (OCL)",;
    HelpContextID = 215057622,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=280, Top=209, cSayPict='"@Z 99999"', cGetPict='"99999"'


  add object oObj_1_184 as cp_runprogram with uid="XVKTHTTLGZ",left=437, top=549, width=221,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('AggiornaOrdiniDiFase')",;
    cEvent = "AggiornaOrdiniDiFase",;
    nPag=1;
    , ToolTipText = "Evento richiamato nell'area manuale replace end (per evitare autolock)";
    , HelpContextID = 38149454


  add object oObj_1_185 as cp_runprogram with uid="BRMRZQHMFX",left=437, top=569, width=221,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('AggiornaRisorse')",;
    cEvent = "AggiornaRisorse",;
    nPag=1;
    , HelpContextID = 38149454

  add object oOLFLMODC_1_191 as StdCheck with uid="DWKJWLSHJF",rtseq=178,rtrep=.f.,left=636, top=183, caption="Consenti modifica fasi", enabled=.f.,;
    ToolTipText = "Se attivo: consente di modificare il ciclo di lavoro sull'ordine",;
    HelpContextID = 160893993,;
    cFormVar="w_OLFLMODC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLFLMODC_1_191.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oOLFLMODC_1_191.GetRadio()
    this.Parent.oContained.w_OLFLMODC = this.RadioValue()
    return .t.
  endfunc

  func oOLFLMODC_1_191.SetRadio()
    this.Parent.oContained.w_OLFLMODC=trim(this.Parent.oContained.w_OLFLMODC)
    this.value = ;
      iif(this.Parent.oContained.w_OLFLMODC=='S',1,;
      0)
  endfunc

  add object oOLTSECPR_1_192 as StdField with uid="YVPXLOKEAN",rtseq=179,rtrep=.f.,;
    cFormVar = "w_OLTSECPR", cQueryName = "OLTSECPR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 48305096,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=704, Top=209, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", cZoomOnZoom="GSCO_BZA", oKey_1_1="OLTSECPR", oKey_1_2="this.w_OLTSECPR"

  func oOLTSECPR_1_192.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=2)
    endwith
   endif
  endfunc

  func oOLTSECPR_1_192.mHide()
    with this.Parent.oContained
      return (g_PRFA<>"S")
    endwith
  endfunc

  func oOLTSECPR_1_192.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_192('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTSECPR_1_192.ecpDrop(oSource)
    this.Parent.oContained.link_1_192('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTSECPR_1_192.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLTSECPR',cp_AbsName(this.parent,'oOLTSECPR_1_192'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCO_BZA',"",'',this.parent.oContained
  endproc
  proc oOLTSECPR_1_192.mZoomOnZoom
    local i_obj
    i_obj=GSCO_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OLTSECPR=this.parent.oContained.w_OLTSECPR
     i_obj.ecpSave()
  endproc

  add object oOLTCOPOI_1_193 as StdCheck with uid="WVOVVTMQUR",rtseq=180,rtrep=.f.,left=110, top=231, caption="Fase count point", enabled=.f.,;
    HelpContextID = 89199569,;
    cFormVar="w_OLTCOPOI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLTCOPOI_1_193.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oOLTCOPOI_1_193.GetRadio()
    this.Parent.oContained.w_OLTCOPOI = this.RadioValue()
    return .t.
  endfunc

  func oOLTCOPOI_1_193.SetRadio()
    this.Parent.oContained.w_OLTCOPOI=trim(this.Parent.oContained.w_OLTCOPOI)
    this.value = ;
      iif(this.Parent.oContained.w_OLTCOPOI=="S",1,;
      0)
  endfunc

  func oOLTCOPOI_1_193.mHide()
    with this.Parent.oContained
      return (g_PRFA<>"S")
    endwith
  endfunc

  add object oOLTFAOUT_1_194 as StdCheck with uid="XRXOOOKGEK",rtseq=181,rtrep=.f.,left=257, top=231, caption="Fase di output", enabled=.f.,;
    HelpContextID = 147975226,;
    cFormVar="w_OLTFAOUT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLTFAOUT_1_194.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oOLTFAOUT_1_194.GetRadio()
    this.Parent.oContained.w_OLTFAOUT = this.RadioValue()
    return .t.
  endfunc

  func oOLTFAOUT_1_194.SetRadio()
    this.Parent.oContained.w_OLTFAOUT=trim(this.Parent.oContained.w_OLTFAOUT)
    this.value = ;
      iif(this.Parent.oContained.w_OLTFAOUT=="S",1,;
      0)
  endfunc

  func oOLTFAOUT_1_194.mHide()
    with this.Parent.oContained
      return (g_PRFA<>"S")
    endwith
  endfunc

  add object oOLTULFAS_1_195 as StdCheck with uid="RXVWPMSLHQ",rtseq=182,rtrep=.f.,left=396, top=231, caption="Ultima fase", enabled=.f.,;
    HelpContextID = 258937799,;
    cFormVar="w_OLTULFAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLTULFAS_1_195.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oOLTULFAS_1_195.GetRadio()
    this.Parent.oContained.w_OLTULFAS = this.RadioValue()
    return .t.
  endfunc

  func oOLTULFAS_1_195.SetRadio()
    this.Parent.oContained.w_OLTULFAS=trim(this.Parent.oContained.w_OLTULFAS)
    this.value = ;
      iif(this.Parent.oContained.w_OLTULFAS=="S",1,;
      0)
  endfunc

  func oOLTULFAS_1_195.mHide()
    with this.Parent.oContained
      return (g_PRFA<>"S")
    endwith
  endfunc

  add object oDESFASE_1_200 as StdField with uid="WISFWEBZJI",rtseq=184,rtrep=.f.,;
    cFormVar = "w_DESFASE", cQueryName = "DESFASE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 215077942,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=333, Top=209, InputMask=replicate('X',40)

  add object oOLTQTOSC_1_209 as StdField with uid="RCSDBZLITF",rtseq=299,rtrep=.f.,;
    cFormVar = "w_OLTQTOSC", cQueryName = "OLTQTOSC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� scarta",;
    HelpContextID = 99816407,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=666, Top=283, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func oOLTQTOSC_1_209.mHide()
    with this.Parent.oContained
      return (.w_OLTPROVE="E")
    endwith
  endfunc

  add object oOLTQTPRO_1_211 as StdField with uid="MOLMYDLQKB",rtseq=301,rtrep=.f.,;
    cFormVar = "w_OLTQTPRO", cQueryName = "OLTQTPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� scarta",;
    HelpContextID = 83039179,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=475, Top=283, cSayPict="v_PQ(12)", cGetPict="v_PQ(12)"

  func oOLTQTPRO_1_211.mHide()
    with this.Parent.oContained
      return (.w_OLTPROVE="E")
    endwith
  endfunc

  add object oOLTPRIOR_1_214 as StdField with uid="LAJKSOGTDS",rtseq=303,rtrep=.f.,;
    cFormVar = "w_OLTPRIOR", cQueryName = "OLTPRIOR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit�",;
    HelpContextID = 202642376,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=532, Top=10, cSayPict='"9999"', cGetPict='"9999"'

  func oOLTPRIOR_1_214.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_OLTPRIOR>=0)
    endwith
    return bRes
  endfunc

  add object oOLTSOSPE_1_216 as StdCheck with uid="YIGIPXLMQN",rtseq=304,rtrep=.f.,left=490, top=35, caption="ODL sospeso",;
    HelpContextID = 37819349,;
    cFormVar="w_OLTSOSPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOLTSOSPE_1_216.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oOLTSOSPE_1_216.GetRadio()
    this.Parent.oContained.w_OLTSOSPE = this.RadioValue()
    return .t.
  endfunc

  func oOLTSOSPE_1_216.SetRadio()
    this.Parent.oContained.w_OLTSOSPE=trim(this.Parent.oContained.w_OLTSOSPE)
    this.value = ;
      iif(this.Parent.oContained.w_OLTSOSPE=='S',1,;
      0)
  endfunc

  func oOLTSOSPE_1_216.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLTSTATO$'M-P-L')
    endwith
   endif
  endfunc

  func oOLTSOSPE_1_216.mHide()
    with this.Parent.oContained
      return (.w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="PSUJNLMIEH",Visible=.t., Left=49, Top=11,;
    Alignment=1, Width=57, Height=15,;
    Caption="Ordine:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="ZRZPGARMOT",Visible=.t., Left=645, Top=10,;
    Alignment=1, Width=65, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SWHTUPFCVN",Visible=.t., Left=286, Top=37,;
    Alignment=1, Width=63, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BMHBEQZKXC",Visible=.t., Left=49, Top=63,;
    Alignment=1, Width=57, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="IXRXGZNCTH",Visible=.t., Left=49, Top=88,;
    Alignment=1, Width=57, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="VKZMUKSIAZ",Visible=.t., Left=636, Top=35,;
    Alignment=1, Width=74, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="YWGJMBNUGG",Visible=.t., Left=636, Top=62,;
    Alignment=1, Width=74, Height=18,;
    Caption="Tipo gest.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="CLUZGRALEB",Visible=.t., Left=36, Top=259,;
    Alignment=1, Width=70, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="RMWUWNXGSO",Visible=.t., Left=430, Top=259,;
    Alignment=1, Width=42, Height=15,;
    Caption="UM:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="RZVHFBWORF",Visible=.t., Left=29, Top=285,;
    Alignment=1, Width=77, Height=15,;
    Caption="Pianificata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="IZKADZIYEQ",Visible=.t., Left=226, Top=285,;
    Alignment=1, Width=55, Height=15,;
    Caption="Evasa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="OCUJWHYMTB",Visible=.t., Left=6, Top=308,;
    Alignment=0, Width=264, Height=18,;
    Caption="Tempo e date previste"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="UUVXTLGZOA",Visible=.t., Left=686, Top=308,;
    Alignment=0, Width=114, Height=18,;
    Caption="Date effettive"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="ZYFSDEDFQA",Visible=.t., Left=48, Top=336,;
    Alignment=1, Width=123, Height=18,;
    Caption="Lead time globale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="AFDXHNSEVV",Visible=.t., Left=664, Top=363,;
    Alignment=1, Width=96, Height=15,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="VSSBUSDTWD",Visible=.t., Left=664, Top=390,;
    Alignment=1, Width=96, Height=15,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="LKAMJOUOXH",Visible=.t., Left=664, Top=336,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data lancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="CQWGAOXHRF",Visible=.t., Left=390, Top=336,;
    Alignment=1, Width=119, Height=18,;
    Caption="Data inizio piano:"  ;
  , bGlobalFont=.t.

  add object oStr_1_151 as StdString with uid="HIEJTFLDTI",Visible=.t., Left=6, Top=420,;
    Alignment=0, Width=175, Height=18,;
    Caption="Date schedulate MSProject"  ;
  , bGlobalFont=.t.

  add object oStr_1_153 as StdString with uid="MNLFRMRDLZ",Visible=.t., Left=8, Top=445,;
    Alignment=1, Width=85, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_155 as StdString with uid="OFAMSJKHBA",Visible=.t., Left=187, Top=445,;
    Alignment=1, Width=82, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_161 as StdString with uid="UFIDBOWQXC",Visible=.t., Left=636, Top=87,;
    Alignment=1, Width=74, Height=18,;
    Caption="Crit. di elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_162 as StdString with uid="HXFKUIHHHJ",Visible=.t., Left=673, Top=259,;
    Alignment=1, Width=87, Height=15,;
    Caption="Data richiesta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_165 as StdString with uid="DPKCOSOVME",Visible=.t., Left=33, Top=37,;
    Alignment=1, Width=119, Height=15,;
    Caption="Seriale elab. MRP:"  ;
  , bGlobalFont=.t.

  func oStr_1_165.mHide()
    with this.Parent.oContained
      return (g_MMRP<>'S' or .w_TIPGES='S' or (.w_OGGMPS='S' and .w_OLTSTATO$'SCD'))
    endwith
  endfunc

  add object oStr_1_166 as StdString with uid="MSHAVZSMHB",Visible=.t., Left=48, Top=392,;
    Alignment=1, Width=123, Height=18,;
    Caption="Lead time sicurezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_168 as StdString with uid="QLWDKFDNFK",Visible=.t., Left=31, Top=114,;
    Alignment=1, Width=75, Height=18,;
    Caption="Codice fase:"  ;
  , bGlobalFont=.t.

  func oStr_1_168.mHide()
    with this.Parent.oContained
      return (g_PRFA<>'S' OR .w_ARTIPART<>'FS')
    endwith
  endfunc

  add object oStr_1_169 as StdString with uid="KOCPNDGTMD",Visible=.t., Left=630, Top=111,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo articolo.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_174 as StdString with uid="UOVZUJVNSH",Visible=.t., Left=2, Top=139,;
    Alignment=1, Width=104, Height=18,;
    Caption="Distinta base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_175 as StdString with uid="ERMLXUTXVQ",Visible=.t., Left=2, Top=186,;
    Alignment=1, Width=104, Height=18,;
    Caption="Codice ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_177 as StdString with uid="CFYYBLPJAT",Visible=.t., Left=53, Top=212,;
    Alignment=1, Width=53, Height=15,;
    Caption="Rif.ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_179 as StdString with uid="TDBMPTUQDR",Visible=.t., Left=236, Top=212,;
    Alignment=1, Width=41, Height=15,;
    Caption="Fase:"  ;
  , bGlobalFont=.t.

  add object oStr_1_198 as StdString with uid="EMQLPACRAZ",Visible=.t., Left=6, Top=163,;
    Alignment=0, Width=264, Height=16,;
    Caption="Dati ciclo di lavorazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_207 as StdString with uid="FSWHQFTPKG",Visible=.t., Left=621, Top=212,;
    Alignment=1, Width=78, Height=15,;
    Caption="Fase prec.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_208 as StdString with uid="DVLHJUTWYK",Visible=.t., Left=622, Top=285,;
    Alignment=1, Width=41, Height=15,;
    Caption="Scarta:"  ;
  , bGlobalFont=.t.

  func oStr_1_208.mHide()
    with this.Parent.oContained
      return (.w_OLTPROVE="E")
    endwith
  endfunc

  add object oStr_1_212 as StdString with uid="DWCLCHPIKH",Visible=.t., Left=417, Top=285,;
    Alignment=1, Width=55, Height=15,;
    Caption="Prodotta:"  ;
  , bGlobalFont=.t.

  func oStr_1_212.mHide()
    with this.Parent.oContained
      return (.w_OLTPROVE="E")
    endwith
  endfunc

  add object oStr_1_215 as StdString with uid="QSMRVGXUGA",Visible=.t., Left=473, Top=11,;
    Alignment=1, Width=55, Height=15,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oBox_1_79 as StdBox with uid="JKAORMOAIE",left=4, top=328, width=582,height=2

  add object oBox_1_82 as StdBox with uid="ADYZTYMCJM",left=685, top=328, width=149,height=2

  add object oBox_1_156 as StdBox with uid="BHFHTHZQYA",left=4, top=437, width=357,height=1

  add object oBox_1_197 as StdBox with uid="RXOBOMMGWW",left=4, top=178, width=764,height=2

  add object oBox_1_199 as StdBox with uid="QDGVEXIQET",left=4, top=251, width=827,height=2
enddefine
define class tgsco_aopPag2 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOLTDISBA_2_2 as StdField with uid="VQIPKUQPTJ",rtseq=78,rtrep=.f.,;
    cFormVar = "w_OLTDISBA", cQueryName = "OLTDISBA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice distinta base associata all'articolo",;
    HelpContextID = 45093849,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=112, Top=44, InputMask=replicate('X',20), cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_OLTDISBA"

  func oOLTDISBA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_OLTSECIC)
        bRes2=.link_1_172('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oOLTCICLO_2_3 as StdField with uid="NOYQJQUYOT",rtseq=79,rtrep=.f.,;
    cFormVar = "w_OLTCICLO", cQueryName = "OLTCICLO",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ciclo semplificato",;
    HelpContextID = 223276085,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=112, Top=72, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TABMCICL", cZoomOnZoom="GSDS_MCS", oKey_1_1="CSCODICE", oKey_1_2="this.w_OLTCICLO"

  func oOLTCICLO_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLTPROVE="I"  and .w_OLTQTOEV=0 and (g_PRFA='S' and g_CICLILAV<>'S' or g_PRFA<>'S'))
    endwith
   endif
  endfunc

  func oOLTCICLO_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCICLO_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCICLO_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TABMCICL','*','CSCODICE',cp_AbsName(this.parent,'oOLTCICLO_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDS_MCS',"Cicli semplificati",'',this.parent.oContained
  endproc
  proc oOLTCICLO_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSDS_MCS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_OLTCICLO
     i_obj.ecpSave()
  endproc

  add object oDESDIS_2_4 as StdField with uid="NTCCOAUOXG",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESDIS", cQueryName = "DESDIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45099978,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=262, Top=44, InputMask=replicate('X',40)

  add object oOLTCOFOR_2_13 as StdField with uid="TBNNQINOCF",rtseq=82,rtrep=.f.,;
    cFormVar = "w_OLTCOFOR", cQueryName = "OLTCOFOR",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente o magazzino terzista non definito",;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 256971720,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=112, Top=141, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_OLTTICON", oKey_2_1="ANCODICE", oKey_2_2="this.w_OLTCOFOR"

  func oOLTCOFOR_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLTPROVE $ 'E-L' AND .w_OLTSTATO $ 'MPSCD')
    endwith
   endif
  endfunc

  func oOLTCOFOR_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCOFOR_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCOFOR_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_OLTTICON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_OLTTICON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oOLTCOFOR_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oOLTCOFOR_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_OLTTICON
     i_obj.w_ANCODICE=this.parent.oContained.w_OLTCOFOR
     i_obj.ecpSave()
  endproc

  add object oDESFOR_2_14 as StdField with uid="QMNSZMLOON",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55454666,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=262, Top=141, InputMask=replicate('X',40)

  add object oOLTCONTR_2_16 as StdField with uid="VCMQNMMLDM",rtseq=84,rtrep=.f.,;
    cFormVar = "w_OLTCONTR", cQueryName = "OLTCONTR",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contratto applicato all'OCL",;
    HelpContextID = 145681464,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=112, Top=169, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", oKey_1_1="COTIPCLF", oKey_1_2="this.w_OLTTICON", oKey_2_1="CONUMERO", oKey_2_2="this.w_OLTCONTR"

  func oOLTCONTR_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_OLTCOFOR) AND .w_OLTPROVE $ 'E-L' and .w_OLTSTATO $ 'M-P-S-C-D')
    endwith
   endif
  endfunc

  func oOLTCONTR_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCONTR_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCONTR_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_OLTTICON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_OLTTICON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oOLTCONTR_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSCO_AOP.CON_TRAM_VZM',this.parent.oContained
  endproc

  add object oOLTCOCEN_2_19 as StdField with uid="SMAGWUUUCR",rtseq=85,rtrep=.f.,;
    cFormVar = "w_OLTCOCEN", cQueryName = "OLTCOCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo associato",;
    HelpContextID = 229567540,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=112, Top=261, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_OLTCOCEN"

  func oOLTCOCEN_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')))
    endwith
   endif
  endfunc

  func oOLTCOCEN_2_19.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oOLTCOCEN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTCOCEN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCOCEN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oOLTCOCEN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oOLTCOCEN_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_OLTCOCEN
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_20 as StdField with uid="ETZQFDWCRD",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 122760138,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=262, Top=261, InputMask=replicate('X',40)

  func oDESCON_2_20.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oOLTVOCEN_2_22 as StdField with uid="ZAQEESVTFE",rtseq=87,rtrep=.f.,;
    cFormVar = "w_OLTVOCEN", cQueryName = "OLTVOCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    ToolTipText = "Voce di costo analitica abbinata all'articolo",;
    HelpContextID = 230812724,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=112, Top=289, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_OLTVOCEN"

  func oOLTVOCEN_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')))
    endwith
   endif
  endfunc

  func oOLTVOCEN_2_22.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oOLTVOCEN_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oOLTVOCEN_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTVOCEN_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oOLTVOCEN_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMA_AAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oOLTVOCEN_2_22.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_OLTVOCEN
     i_obj.ecpSave()
  endproc

  add object oVOCDES_2_23 as StdField with uid="FNEYLPUUBZ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_VOCDES", cQueryName = "VOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49356970,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=262, Top=289, InputMask=replicate('X',40)

  func oVOCDES_2_23.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oOLTCOMME_2_26 as StdField with uid="EOKQCWHWCJ",rtseq=90,rtrep=.f.,;
    cFormVar = "w_OLTCOMME", cQueryName = "OLTCOMME",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice della commessa associata al centro di costo",;
    HelpContextID = 139531221,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=112, Top=317, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_OLTCOMME"

  func oOLTCOMME_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')))
    endwith
   endif
  endfunc

  func oOLTCOMME_2_26.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  func oOLTCOMME_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_26('Part',this)
      if .not. empty(.w_OLTCOATT)
        bRes2=.link_2_28('Full')
      endif
      if .not. empty(.w_OLTCOCOS)
        bRes2=.link_2_40('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oOLTCOMME_2_26.ecpDrop(oSource)
    this.Parent.oContained.link_2_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCOMME_2_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oOLTCOMME_2_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oOLTCOMME_2_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_OLTCOMME
     i_obj.ecpSave()
  endproc

  add object oOLTCOATT_2_28 as StdField with uid="VVYUUEIBCE",rtseq=91,rtrep=.f.,;
    cFormVar = "w_OLTCOATT", cQueryName = "OLTCOATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� inesistente o incongruente",;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 72422342,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=112, Top=345, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_OLTCOMME", oKey_2_1="ATTIPATT", oKey_2_2="this.w_OLTTIPAT", oKey_3_1="ATCODATT", oKey_3_2="this.w_OLTCOATT"

  func oOLTCOATT_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') AND NOT EMPTY(.w_OLTCOMME) AND (.w_OLTSTATO $ 'MPSCD' or (.w_OLTSTATO='L' and ! .w_OLTPROVE$'L-E')) )
    endwith
   endif
  endfunc

  func oOLTCOATT_2_28.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  func oOLTCOATT_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
      if .not. empty(.w_OLTCOCOS)
        bRes2=.link_2_40('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oOLTCOATT_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOLTCOATT_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_OLTCOMME)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_OLTTIPAT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_OLTCOMME)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_OLTTIPAT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oOLTCOATT_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oOLTCOATT_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_OLTCOMME
    i_obj.ATTIPATT=w_OLTTIPAT
     i_obj.w_ATCODATT=this.parent.oContained.w_OLTCOATT
     i_obj.ecpSave()
  endproc

  add object oDESCAN_2_32 as StdField with uid="YHHBBWWUZS",rtseq=95,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 137440202,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=262, Top=317, InputMask=replicate('X',40)

  func oDESCAN_2_32.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oDESATT_2_33 as StdField with uid="BDHTVMWFVL",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 16985034,;
   bGlobalFont=.t.,;
    Height=21, Width=304, Left=262, Top=345, InputMask=replicate('X',30)

  func oDESATT_2_33.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oDESCIC_2_45 as StdField with uid="QNJLRUTFMZ",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESCIC", cQueryName = "DESCIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 45165514,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=262, Top=72, InputMask=replicate('X',40)

  add object oDATCON_2_46 as StdField with uid="DFWNVTDPKJ",rtseq=117,rtrep=.f.,;
    cFormVar = "w_DATCON", cQueryName = "DATCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 122757066,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=673, Top=169

  add object oCONDES_2_47 as StdField with uid="CWZVCRVABF",rtseq=118,rtrep=.f.,;
    cFormVar = "w_CONDES", cQueryName = "CONDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 49312218,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=262, Top=169, InputMask=replicate('X',50)


  add object oOLCRIFOR_2_64 as StdCombo with uid="KLWDMEZWKB",rtseq=149,rtrep=.f.,left=112,top=197,width=118,height=21;
    , ToolTipText = "Criterio di scelta fornitore";
    , HelpContextID = 262349768;
    , cFormVar="w_OLCRIFOR",RowSource=""+"Priorit�,"+"Priorit�/Prezzo,"+"Priorit�/Tempo,"+"Priorit� di dettaglio,"+"Tempo,"+"Prezzo,"+"Manuale,"+"Affidabilit�", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOLCRIFOR_2_64.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'R',;
    iif(this.value =3,'P',;
    iif(this.value =4,'D',;
    iif(this.value =5,'T',;
    iif(this.value =6,'Z',;
    iif(this.value =7,'M',;
    iif(this.value =8,'A',;
    space(1))))))))))
  endfunc
  func oOLCRIFOR_2_64.GetRadio()
    this.Parent.oContained.w_OLCRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oOLCRIFOR_2_64.SetRadio()
    this.Parent.oContained.w_OLCRIFOR=trim(this.Parent.oContained.w_OLCRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_OLCRIFOR=='I',1,;
      iif(this.Parent.oContained.w_OLCRIFOR=='R',2,;
      iif(this.Parent.oContained.w_OLCRIFOR=='P',3,;
      iif(this.Parent.oContained.w_OLCRIFOR=='D',4,;
      iif(this.Parent.oContained.w_OLCRIFOR=='T',5,;
      iif(this.Parent.oContained.w_OLCRIFOR=='Z',6,;
      iif(this.Parent.oContained.w_OLCRIFOR=='M',7,;
      iif(this.Parent.oContained.w_OLCRIFOR=='A',8,;
      0))))))))
  endfunc

  func oOLCRIFOR_2_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=0)
    endwith
   endif
  endfunc


  add object oObj_2_67 as cp_runprogram with uid="TPZPDDMTFZ",left=1, top=530, width=234,height=19,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('CHANGECOM')",;
    cEvent = "w_OLTCOMME Changed",;
    nPag=2;
    , HelpContextID = 38149454

  add object oStr_2_5 as StdString with uid="QCRWBGWEIW",Visible=.t., Left=8, Top=14,;
    Alignment=0, Width=184, Height=18,;
    Caption="Distinta base e ciclo"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="DMOIJMCOTL",Visible=.t., Left=6, Top=44,;
    Alignment=1, Width=104, Height=18,;
    Caption="Distinta base:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="BYNMDFPYHD",Visible=.t., Left=6, Top=72,;
    Alignment=1, Width=104, Height=18,;
    Caption="Codice ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="OOBKUVKCAD",Visible=.t., Left=8, Top=111,;
    Alignment=0, Width=184, Height=18,;
    Caption="Fornitore"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="LOAHVGVQKE",Visible=.t., Left=8, Top=231,;
    Alignment=0, Width=184, Height=18,;
    Caption="Dati analitica"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="RPPJYZFBXX",Visible=.t., Left=6, Top=141,;
    Alignment=1, Width=104, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="IZVAYWDWWU",Visible=.t., Left=6, Top=169,;
    Alignment=1, Width=104, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="OHIYQZEKQI",Visible=.t., Left=6, Top=261,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="PVEMQUYUNU",Visible=.t., Left=6, Top=289,;
    Alignment=1, Width=104, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="MNYUTFZFML",Visible=.t., Left=6, Top=317,;
    Alignment=1, Width=104, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  add object oStr_2_27 as StdString with uid="CTCXTYMFHC",Visible=.t., Left=6, Top=345,;
    Alignment=1, Width=104, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S' and (g_COAN<>'S' or g_PERCAN<>'S'))
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="GNHRLCBCBU",Visible=.t., Left=639, Top=169,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="BGQSDGSTHG",Visible=.t., Left=5, Top=199,;
    Alignment=1, Width=105, Height=15,;
    Caption="Criterio di scelta:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (.w_OLTPROVE<>"E")
    endwith
  endfunc

  add object oBox_2_6 as StdBox with uid="DEISDBPPYY",left=4, top=33, width=825,height=2

  add object oBox_2_10 as StdBox with uid="YVOZUEUZGP",left=4, top=128, width=825,height=2

  add object oBox_2_12 as StdBox with uid="OANOEOLUAB",left=4, top=249, width=825,height=2
enddefine
define class tgsco_aopPag3 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=247
  resizeYpos=283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="RDAFQBZLWF",left=0, top=4, width=848, height=467, bOnScreen=.t.;


  func oLinkPC_3_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OLTSTATO $ "MPL" and .w_OLTPROVE<>"E")
      endwith
    endif
  endfunc

  func oLinkPC_3_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OLTPROVE="E" or .w_OLTSTATO $ "SCD" )
     endwith
    endif
  endfunc

  add object oStr_3_3 as StdString with uid="HSLXMOAWFY",Visible=.t., Left=280, Top=154,;
    Alignment=0, Width=213, Height=15,;
    Caption="Lista materiali non disponibile"  ;
  , bGlobalFont=.t.

  func oStr_3_3.mHide()
    with this.Parent.oContained
      return (.w_OLTSTATO $ "MPLF" and .w_OLTPROVE<>"E")
    endwith
  endfunc
enddefine
define class tgsco_aopPag4 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=386
  resizeYpos=134
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="WAYGOWPOGZ",left=1, top=6, width=846, height=448, bOnScreen=.t.;


  func oLinkPC_4_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPGES $ "ELZ" and ((.cFunction="Query" or (.w_OLTSTATO <> "F")) Or (inlist(.cFunction , "Load" , "Edit"))) and (.w_OLTSTATO $ "MP" or (.w_OLTSTATO="L" and ! .w_OLTPROVE$'L-E')) and (g_PRFA<>'S' or g_CICLILAV<>'S'))
      endwith
    endif
  endfunc

  func oLinkPC_4_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OLTPROVE="E" or .w_OLTSTATO $ "SCD" or (g_PRFA='S' and g_CICLILAV='S'))
     endwith
    endif
  endfunc

  add object oStr_4_2 as StdString with uid="AARLCKUWPQ",Visible=.t., Left=287, Top=180,;
    Alignment=0, Width=234, Height=15,;
    Caption="Lista lavorazioni non disponibile"  ;
  , bGlobalFont=.t.

  func oStr_4_2.mHide()
    with this.Parent.oContained
      return (.w_OLTSTATO $ "MPLF" and .w_OLTPROVE<>"E")
    endwith
  endfunc
enddefine
define class tgsco_aopPag5 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=479
  resizeYpos=138
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="EMHCDWANHD",left=0, top=6, width=843, height=470, bOnScreen=.t.;


  func oLinkPC_5_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_PRFA='S' and g_CICLILAV='S' and .w_TIPGES $ "ELZ" and !.w_OLTPROVE $ 'E-L' and !Empty(.w_OLTSECIC))
      endwith
    endif
  endfunc

  func oLinkPC_5_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_PRFA<>'S' or g_CICLILAV<>'S' or .w_OLTPROVE="E" or .w_OLTSTATO $ "SCD")
     endwith
    endif
  endfunc

  add object oStr_5_2 as StdString with uid="ZZODWDVLAF",Visible=.t., Left=314, Top=220,;
    Alignment=0, Width=234, Height=15,;
    Caption="Lista lavorazioni non disponibile"  ;
  , bGlobalFont=.t.

  func oStr_5_2.mHide()
    with this.Parent.oContained
      return (.w_OLTSTATO $ "MPLF" and .w_OLTPROVE<>"E")
    endwith
  endfunc
enddefine
define class tgsco_aopPag6 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=741
  resizeYpos=189
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_6_1 as stdDynamicChildContainer with uid="MBYVUDEPWI",left=1, top=6, width=838, height=462, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsco_mrf",lower(this.oContained.GSCO_MRF.class))=0
        this.oContained.GSCO_MRF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsco_aopPag7 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=759
  resizeYpos=143
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_7_1 as stdDynamicChildContainer with uid="DTQCDBYISR",left=-2, top=3, width=856, height=461, bOnScreen=.t.;


  func oLinkPC_7_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_OLTSTATO $ "MPL" and !.w_OLTPROVE="E")
      endwith
    endif
  endfunc

  func oLinkPC_7_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_OLTPROVE<>"I" or .w_OLTSTATO $ "SCD" )
     endwith
    endif
  endfunc

  add object oStr_7_2 as StdString with uid="QONUCZPRHK",Visible=.t., Left=280, Top=151,;
    Alignment=0, Width=234, Height=18,;
    Caption="Lista materiali non disponibile"  ;
  , bGlobalFont=.t.

  func oStr_7_2.mHide()
    with this.Parent.oContained
      return (.w_OLTSTATO $ "MPLF" and .w_OLTPROVE="I")
    endwith
  endfunc
enddefine
define class tgsco_aopPag8 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=800
  resizeYpos=348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZPF as cp_zoombox with uid="MPPPHFKSRC",left=1, top=18, width=826,height=203,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCO_ZPF",bOptions=.f.,bAdvOptions=.f.,cTable="PEG_SELI",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 8",;
    nPag=8;
    , HelpContextID = 267230746


  add object ZFP as cp_zoombox with uid="LAVHIKRWZJ",left=1, top=237, width=826,height=235,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCO_ZFP",bOptions=.f.,bAdvOptions=.f.,cTable="PEG_SELI",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 8",;
    nPag=8;
    , HelpContextID = 267230746


  add object oObj_8_5 as cp_runprogram with uid="YLUEVJOBYO",left=2, top=482, width=167,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('PEGPF')",;
    cEvent = "w_zpf selected",;
    nPag=8;
    , HelpContextID = 267230746


  add object oObj_8_6 as cp_runprogram with uid="STKANUWKRD",left=180, top=482, width=167,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('PEGFP')",;
    cEvent = "w_zfp selected",;
    nPag=8;
    , HelpContextID = 267230746

  add object oStr_8_2 as StdString with uid="AEVUPNKEVV",Visible=.t., Left=9, Top=4,;
    Alignment=0, Width=73, Height=18,;
    Caption="Padre - figlio"  ;
  , bGlobalFont=.t.

  add object oStr_8_4 as StdString with uid="GZJNPFLBYW",Visible=.t., Left=9, Top=225,;
    Alignment=0, Width=77, Height=18,;
    Caption="Figlio - padre"  ;
  , bGlobalFont=.t.
enddefine
define class tgsco_aopPag9 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  resizeXpos=777
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object TREEV as cp_Treeview with uid="HYPQVVLBAI",left=1, top=6, width=846,height=368,;
    caption='TREEV',;
   bGlobalFont=.t.,;
    cCursor="_tree_",cShowFields="DESCRI",cNodeShowField="",cLeafShowField="",cNodeBmp="odll.bmp",cLeafBmp="",cMenuFile="",bNoContextMenu=.f.,nIndent=0,;
    cEvent = "updtreev",;
    nPag=9;
    , HelpContextID = 186647094


  add object oObj_9_59 as cp_runprogram with uid="MZGAYZOGYN",left=148, top=514, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('Page2')",;
    cEvent = "TVPage6L",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_60 as cp_runprogram with uid="HOWPMBAFJF",left=446, top=514, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('EXPNODE')",;
    cEvent = "TVEXPNODEL",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_61 as cp_runprogram with uid="HVBHBGZDDS",left=-1, top=514, width=143,height=22,;
    caption='GSCO_BTO',;
   bGlobalFont=.t.,;
    prg="GSCO_BTO('Done')",;
    cEvent = "TVDoneL",;
    nPag=9;
    , HelpContextID = 38149451


  add object oObj_9_64 as cp_runprogram with uid="YUYKWCNKZA",left=148, top=540, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCL_BTO('Page2')",;
    cEvent = "TVPage6Z",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_65 as cp_runprogram with uid="AQWTIUYGJL",left=446, top=540, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCL_BTO('EXPNODE')",;
    cEvent = "TVEXPNODEZ",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_66 as cp_runprogram with uid="ZQLZEDGQJQ",left=-1, top=540, width=143,height=22,;
    caption='GSCL_BTO',;
   bGlobalFont=.t.,;
    prg="GSCL_BTO('Done')",;
    cEvent = "TVDoneZ",;
    nPag=9;
    , HelpContextID = 38346059


  add object oObj_9_67 as cp_runprogram with uid="BRECDGDASY",left=148, top=566, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('Page2')",;
    cEvent = "TVPage6E",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_68 as cp_runprogram with uid="RZIEJARGAP",left=446, top=566, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('EXPNODE')",;
    cEvent = "TVEXPNODEE",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_69 as cp_runprogram with uid="KBEUGKJQZY",left=-1, top=566, width=143,height=22,;
    caption='GSCO1BTO',;
   bGlobalFont=.t.,;
    prg="GSCO1BTO('Done')",;
    cEvent = "TVDoneE",;
    nPag=9;
    , HelpContextID = 86383947


  add object oObj_9_70 as cp_runprogram with uid="RKZJZNHNNG",left=-1, top=488, width=143,height=22,;
    caption='GSCO_BOL',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('TVDone')",;
    cEvent = "Done",;
    nPag=9;
    , HelpContextID = 38149454


  add object oObj_9_71 as cp_runprogram with uid="VYDQBWXDGJ",left=148, top=488, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('TVPage6')",;
    cEvent = "ActivatePage 9",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_72 as cp_runprogram with uid="TOQJASOPED",left=446, top=488, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('TVEXPNODE')",;
    cEvent = "w_treev Expanded",;
    nPag=9;
    , HelpContextID = 267230746


  add object oObj_9_73 as cp_runprogram with uid="ILAINYWOFF",left=-1, top=592, width=143,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCO_BOL('HContr')",;
    cEvent = "w_treev NodeSelected",;
    nPag=9;
    , HelpContextID = 267230746

  add object oCODRIC_9_74 as StdField with uid="MHRCMUSCKI",rtseq=250,rtrep=.f.,;
    cFormVar = "w_CODRIC", cQueryName = "CODRIC",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo",;
    HelpContextID = 44241370,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=65, Top=432, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODRIC"

  func oCODRIC_9_74.mHide()
    with this.Parent.oContained
      return ((.w_TIPGES='L' and !.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DCC-DRD-DIR-FDO-DPM-DMM') or (.w_TIPGES $ 'Z-E' and .w_type='DPI'))
    endwith
  endfunc

  func oCODRIC_9_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESAR_9_75 as StdField with uid="YULKYGFCYJ",rtseq=251,rtrep=.f.,;
    cFormVar = "w_CADESAR", cQueryName = "CADESAR",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 68165594,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=234, Top=432, InputMask=replicate('X',40)

  func oCADESAR_9_75.mHide()
    with this.Parent.oContained
      return ((.w_TIPGES='L'and !.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DRD-DIR-FDO-DCC-DPM-DMM') or (.w_TIPGES $ 'Z-E' and .w_type='DPI'))
    endwith
  endfunc

  add object oQTAORD_9_78 as StdField with uid="CIKFKCNDOQ",rtseq=252,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 18234362,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=167, Top=407, cSayPict="V_PQ(14)"

  func oQTAORD_9_78.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DMM-MRS-DPM-MRS' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oMAGAZ_9_79 as StdField with uid="FJHBRPNLMX",rtseq=253,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino",;
    HelpContextID = 190582982,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=606, Top=380, InputMask=replicate('X',5)

  func oMAGAZ_9_79.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG-MOS-DMT-DMM-DPT-DPM-MRS' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oDATEMS_9_82 as StdField with uid="ZGQBHRJUXV",rtseq=254,rtrep=.f.,;
    cFormVar = "w_DATEMS", cQueryName = "DATEMS",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 40837066,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=424, Top=380

  func oDATEMS_9_82.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oDATEVF_9_85 as StdField with uid="QUYKEGEEHG",rtseq=255,rtrep=.f.,;
    cFormVar = "w_DATEVF", cQueryName = "DATEVF",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine",;
    HelpContextID = 249503690,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=586, Top=380

  func oDATEVF_9_85.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oCAUMAG_9_87 as StdField with uid="VEUIWGVDFN",rtseq=256,rtrep=.f.,;
    cFormVar = "w_CAUMAG", cQueryName = "CAUMAG",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale magazzino",;
    HelpContextID = 254218202,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=445, Top=380, InputMask=replicate('X',5)

  func oCAUMAG_9_87.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-MOS--MRS-DMT-DMM-DPT-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oMAGAZ_9_89 as StdField with uid="XONRNLHYXQ",rtseq=257,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino collegato",;
    HelpContextID = 190582982,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=665, Top=433, InputMask=replicate('X',5)

  func oMAGAZ_9_89.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oFORNIT_9_90 as StdField with uid="RXCSMKUJAD",rtseq=258,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Fornitore",;
    HelpContextID = 27668906,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=665, Top=433, InputMask=replicate('X',15)

  func oFORNIT_9_90.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oQTASCA_9_93 as StdField with uid="YIEKBAJFSY",rtseq=259,rtrep=.f.,;
    cFormVar = "w_QTASCA", cQueryName = "QTASCA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84032506,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=407, cSayPict="V_PQ(14)"

  func oQTASCA_9_93.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-DCC-DDO-DRD-DIR-FDO' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oQTARES_9_95 as StdField with uid="QUGHPZKABU",rtseq=260,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� residua",;
    HelpContextID = 48446458,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=641, Top=407, cSayPict="V_PQ(14)", cGetPict='"999999.99999"'

  func oQTARES_9_95.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oNUMREG_9_96 as StdField with uid="KEFALZJZEE",rtseq=261,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 249723690,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=65, Top=380, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_9_96.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPT-DPM-DMM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oDATEVA_9_98 as StdField with uid="BSQKALHKGH",rtseq=262,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data evasione",;
    HelpContextID = 64954314,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=243, Top=381

  func oDATEVA_9_98.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-DDP-MOS-DPT-DIM-MRS-DDO-DRD-DIR-FDO-DCC-DMT-DMM-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oFORNIT_9_102 as StdField with uid="QTYPKUIGEA",rtseq=263,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 27668906,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=407, InputMask=replicate('X',15)

  func oFORNIT_9_102.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oRIF_9_106 as StdField with uid="BQKTHSILPC",rtseq=264,rtrep=.f.,;
    cFormVar = "w_RIF", cQueryName = "RIF",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento",;
    HelpContextID = 91949334,;
   bGlobalFont=.t.,;
    Height=20, Width=116, Left=65, Top=381, InputMask=replicate('X',15)

  func oRIF_9_106.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'DDO-DRD-DCC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oQTAORD_9_108 as StdField with uid="TYOFLPZLJH",rtseq=265,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 18234362,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=170, Top=407, cSayPict="V_PQ(14)"

  func oQTAORD_9_108.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-FDO-DCC-DDO-DRD-DIR' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oQTAEVA_9_110 as StdField with uid="MREMFAQXCR",rtseq=266,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 65027066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=407, cSayPict="V_PQ(14)"

  func oQTAEVA_9_110.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc


  add object StatoODL as cp_calclbl with uid="IQSVWPVDSV",left=220, top=380, width=100,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=9;
    , ToolTipText = "Stato ODL";
    , HelpContextID = 267230746

  add object oUNIMIS1_9_112 as StdField with uid="EYJBYERHZF",rtseq=267,rtrep=.f.,;
    cFormVar = "w_UNIMIS1", cQueryName = "UNIMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 44548538,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=65, Top=407, InputMask=replicate('X',3)

  func oUNIMIS1_9_112.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF-RIG-FSC-DDP-DMM-MRS-DPM-DDO-DIM-DRD-DIR-FDO-DCC-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oCODODL__9_114 as StdField with uid="EPZSCAOFTU",rtseq=268,rtrep=.f.,;
    cFormVar = "w_CODODL_", cQueryName = "CODODL_",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ODL",;
    HelpContextID = 167121370,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=65, Top=381, InputMask=replicate('X',15)

  func oCODODL__9_114.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'FSC-ODL-ODF'  or .w_LEVEL>2 or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oCPROWNUM_9_116 as StdField with uid="GHQCXYVCTM",rtseq=269,rtrep=.f.,;
    cFormVar = "w_CPROWNUM", cQueryName = "CPROWNUM",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 154849139,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=126, Top=380, cSayPict='"9999"', cGetPict='"9999"'

  func oCPROWNUM_9_116.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPT-DPM-DMM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oCODODL_9_117 as StdField with uid="TGHUFNKEYN",rtseq=270,rtrep=.f.,;
    cFormVar = "w_CODODL", cQueryName = "CODODL",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 167121370,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=65, Top=381, InputMask=replicate('X',15)

  func oCODODL_9_117.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1 or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oQTAORD_9_120 as StdField with uid="QNQZTCZIIU",rtseq=271,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 18234362,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=407, cSayPict="V_PQ(14)"

  func oQTAORD_9_120.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oUNIMIS_9_121 as StdField with uid="JLWLMDUMVQ",rtseq=272,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 44548538,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=234, Top=407, InputMask=replicate('X',3)

  func oUNIMIS_9_121.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oDATEVF_9_124 as StdField with uid="ZLGNDQXSHR",rtseq=273,rtrep=.f.,;
    cFormVar = "w_DATEVF", cQueryName = "DATEVF",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 249503690,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=639, Top=380

  func oDATEVF_9_124.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oQTAEVA_9_126 as StdField with uid="JQUGXWGXFY",rtseq=274,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 65027066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=488, Top=407, cSayPict="V_PQ(14)"

  func oQTAEVA_9_126.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oQTARES_9_127 as StdField with uid="LRCKLTLTIA",rtseq=275,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48446458,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=641, Top=407, cSayPict="V_PQ(14)"

  func oQTARES_9_127.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oMAGAZ_9_129 as StdField with uid="DAJSMBPKIA",rtseq=276,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 190582982,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=665, Top=433, InputMask=replicate('X',5)

  func oMAGAZ_9_129.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oNUMDOC_9_130 as StdField with uid="AXLSXKBPKF",rtseq=277,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 38828842,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=65, Top=381, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMDOC_9_130.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oALFDOC_9_131 as StdField with uid="MTTOWKBOFA",rtseq=278,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 38860026,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=135, Top=380, InputMask=replicate('X',3)

  func oALFDOC_9_131.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or empty(.w_ALFDOC) or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc


  add object StatoDOC as cp_calclbl with uid="RUMFYNCSZU",left=290, top=380, width=89,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=9;
    , ToolTipText = "Statodocumento";
    , HelpContextID = 267230746

  add object oDATEVA_9_136 as StdField with uid="YGIVCYEXJL",rtseq=279,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 64954314,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=473, Top=380

  func oDATEVA_9_136.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oNUMREG_9_138 as StdField with uid="ZVRNYSQLUA",rtseq=280,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 249723690,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=232, Top=380, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_9_138.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oFORNIT_9_141 as StdField with uid="IJTVHQXZFJ",rtseq=281,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 27668906,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=65, Top=407, InputMask=replicate('X',15)

  func oFORNIT_9_141.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oCODODL_9_145 as StdField with uid="ATKKXOWULI",rtseq=282,rtrep=.f.,;
    cFormVar = "w_CODODL", cQueryName = "CODODL",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 167121370,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=65, Top=381, InputMask=replicate('X',15)

  func oCODODL_9_145.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>0 or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oQTAORD_9_148 as StdField with uid="RJJSKEOENW",rtseq=283,rtrep=.f.,;
    cFormVar = "w_QTAORD", cQueryName = "QTAORD",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 18234362,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=341, Top=407, cSayPict="V_PQ(14)"

  func oQTAORD_9_148.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oUNIMIS_9_149 as StdField with uid="NTXSNCSQZR",rtseq=284,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 44548538,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=234, Top=407, InputMask=replicate('X',3)

  func oUNIMIS_9_149.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oDATEVF_9_152 as StdField with uid="KIGMPULCHC",rtseq=285,rtrep=.f.,;
    cFormVar = "w_DATEVF", cQueryName = "DATEVF",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 249503690,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=639, Top=380

  func oDATEVF_9_152.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oQTAEVA_9_154 as StdField with uid="MUUOOTLDQX",rtseq=286,rtrep=.f.,;
    cFormVar = "w_QTAEVA", cQueryName = "QTAEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 65027066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=488, Top=407, cSayPict="V_PQ(14)"

  func oQTAEVA_9_154.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oQTARES_9_155 as StdField with uid="YRURNWMFOB",rtseq=287,rtrep=.f.,;
    cFormVar = "w_QTARES", cQueryName = "QTARES",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48446458,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=641, Top=407, cSayPict="V_PQ(14)"

  func oQTARES_9_155.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oMAGAZ_9_157 as StdField with uid="NXWVGIRSKW",rtseq=288,rtrep=.f.,;
    cFormVar = "w_MAGAZ", cQueryName = "MAGAZ",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 190582982,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=665, Top=433, InputMask=replicate('X',5)

  func oMAGAZ_9_157.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oNUMDOC_9_158 as StdField with uid="MNSMWJETKF",rtseq=289,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 38828842,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=65, Top=381, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMDOC_9_158.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1 or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oALFDOC_9_159 as StdField with uid="ALMKXWQOMO",rtseq=290,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 38860026,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=135, Top=381, InputMask=replicate('X',3)

  func oALFDOC_9_159.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1 or empty(.w_ALFDOC) or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oDATEVA_9_163 as StdField with uid="YZQAPAJPFB",rtseq=291,rtrep=.f.,;
    cFormVar = "w_DATEVA", cQueryName = "DATEVA",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 64954314,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=473, Top=380

  func oDATEVA_9_163.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oNUMREG_9_165 as StdField with uid="VADSQUXKFZ",rtseq=292,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 249723690,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=232, Top=381, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_9_165.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1 or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oFORNIT_9_168 as StdField with uid="FQEHAPIUZO",rtseq=293,rtrep=.f.,;
    cFormVar = "w_FORNIT", cQueryName = "FORNIT",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 27668906,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=65, Top=407, InputMask=replicate('X',15)

  func oFORNIT_9_168.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oNUMREG_9_172 as StdField with uid="IPEHSELAPI",rtseq=294,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=0, bMultilanguage =  .f.,;
    HelpContextID = 249723690,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=162, Top=381, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMREG_9_172.mHide()
    with this.Parent.oContained
      return (.w_TYPE<>'DPI' or .w_TIPGES<>'E')
    endwith
  endfunc


  add object ProvODLF as cp_calclbl with uid="COYEHRTAIE",left=65, top=456, width=90,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,fontname="Arial",alignment=0,;
    nPag=9;
    , ToolTipText = "Provenienza fase";
    , HelpContextID = 267230746

  add object oStr_9_76 as StdString with uid="TOIZCSNPXH",Visible=.t., Left=22, Top=432,;
    Alignment=1, Width=44, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_9_76.mHide()
    with this.Parent.oContained
      return ((.w_TIPGES='L' and !.w_type $ 'ODL-ODF-RIG-FSC-DDP-MRS-DDO-DIM-DRD-DIR-FDO-DCC-DPM-DMM') or (.w_TIPGES $ 'Z-E' and .w_type='DPI'))
    endwith
  endfunc

  add object oStr_9_77 as StdString with uid="LNPBQXGYSE",Visible=.t., Left=37, Top=379,;
    Alignment=1, Width=29, Height=19,;
    Caption="ODL:"  ;
  , bGlobalFont=.t.

  func oStr_9_77.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1 or !.w_type $ 'ODL-ODF-FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_80 as StdString with uid="ZAWCOWNSPU",Visible=.t., Left=23, Top=380,;
    Alignment=1, Width=43, Height=18,;
    Caption="N.reg.:"  ;
  , bGlobalFont=.t.

  func oStr_9_80.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or !.w_type $ 'MOV-RIG-MOS-MRS-DMT-DMM-DPT-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_81 as StdString with uid="AWVZFBFXIH",Visible=.t., Left=500, Top=380,;
    Alignment=1, Width=83, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  func oStr_9_81.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_83 as StdString with uid="BAVGLVXIVI",Visible=.t., Left=347, Top=380,;
    Alignment=1, Width=71, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  func oStr_9_83.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_84 as StdString with uid="HYVSEDVIXU",Visible=.t., Left=276, Top=407,;
    Alignment=1, Width=63, Height=18,;
    Caption="Q.t� eva:"  ;
  , bGlobalFont=.t.

  func oStr_9_84.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_86 as StdString with uid="FOVOLXFJON",Visible=.t., Left=327, Top=380,;
    Alignment=1, Width=112, Height=18,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_9_86.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG-MOS-MRS-DMT-DMM-DPT-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_88 as StdString with uid="FOGQXXFRHB",Visible=.t., Left=519, Top=380,;
    Alignment=1, Width=84, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_9_88.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-MOS--MRS-DMT-DMM-DPT-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_91 as StdString with uid="YPAVWQQCKU",Visible=.t., Left=567, Top=433,;
    Alignment=1, Width=95, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_9_91.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_92 as StdString with uid="CIMBCOXNYW",Visible=.t., Left=39, Top=407,;
    Alignment=1, Width=27, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_9_92.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'ODL-ODF-RIG-FSC-DDP-DMM-MRS-DPM-DDO-DIM-DRD-DIR-FDO-DCC-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_94 as StdString with uid="AXDKMNHTXH",Visible=.t., Left=99, Top=407,;
    Alignment=1, Width=65, Height=18,;
    Caption="Q.t� ord:"  ;
  , bGlobalFont=.t.

  func oStr_9_94.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_97 as StdString with uid="CGJVDRJKCM",Visible=.t., Left=201, Top=382,;
    Alignment=1, Width=39, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_9_97.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'MOV-RIG-DDP-MOS-DPT-DMT-DPT-DDO-DIM-DRD-DIR-FDO-MRS-DMM-DCC-DPM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_99 as StdString with uid="ODOMFDIDNA",Visible=.t., Left=578, Top=433,;
    Alignment=1, Width=84, Height=18,;
    Caption="Mag collegato:"  ;
  , bGlobalFont=.t.

  func oStr_9_99.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'MOV-RIG' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_100 as StdString with uid="DNVDYULWMP",Visible=.t., Left=111, Top=407,;
    Alignment=1, Width=53, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  func oStr_9_100.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'RIG-FSC-DMM-MRS-DPM-MRS' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_101 as StdString with uid="EUCUTMUKPH",Visible=.t., Left=278, Top=407,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_9_101.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_103 as StdString with uid="IRNULFWJWC",Visible=.t., Left=24, Top=380,;
    Alignment=1, Width=42, Height=18,;
    Caption="N. doc:"  ;
  , bGlobalFont=.t.

  func oStr_9_103.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2  or !.w_type $ 'DDP-DDO-DIM-DCC-DRD-DIR-FDO' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_104 as StdString with uid="LBMNHRUWBC",Visible=.t., Left=276, Top=407,;
    Alignment=1, Width=63, Height=18,;
    Caption="Q.t� sca.:"  ;
  , bGlobalFont=.t.

  func oStr_9_104.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DDO-DIM-DCC-DIR-DRD-FDO' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_105 as StdString with uid="HRTTOJBITT",Visible=.t., Left=574, Top=407,;
    Alignment=1, Width=65, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_9_105.mHide()
    with this.Parent.oContained
      return ( !.w_type $ 'ODL-ODF' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_107 as StdString with uid="SUSEBBJZGT",Visible=.t., Left=103, Top=407,;
    Alignment=1, Width=66, Height=18,;
    Caption="Q.t� prod.:"  ;
  , bGlobalFont=.t.

  func oStr_9_107.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDP-DIM-DCC-FDO' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_109 as StdString with uid="QIZYYDPLCC",Visible=.t., Left=106, Top=407,;
    Alignment=1, Width=62, Height=18,;
    Caption="Q.t� vers.:"  ;
  , bGlobalFont=.t.

  func oStr_9_109.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'DDO-DRD-DIR' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_113 as StdString with uid="DLJCIZGDAU",Visible=.t., Left=38, Top=380,;
    Alignment=1, Width=28, Height=18,;
    Caption="OCL:"  ;
  , bGlobalFont=.t.

  func oStr_9_113.mHide()
    with this.Parent.oContained
      return (!.w_type $ 'FSC' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_115 as StdString with uid="UEHMLWEEWM",Visible=.t., Left=119, Top=380,;
    Alignment=0, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_9_115.mHide()
    with this.Parent.oContained
      return (!.w_TYPE$'MOV-RIG-DIM-DDP-DMT-MOS-DPT-MRS-DIR-FDO-DPT-DPM-DMM' or .w_TIPGES<>'L')
    endwith
  endfunc

  add object oStr_9_118 as StdString with uid="QSLVARBHZZ",Visible=.t., Left=9, Top=380,;
    Alignment=1, Width=57, Height=18,;
    Caption="ODL/OCL:"  ;
  , bGlobalFont=.t.

  func oStr_9_118.mHide()
    with this.Parent.oContained
      return (.w_LEVEL>1 or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_119 as StdString with uid="CFDUABSKFZ",Visible=.t., Left=208, Top=407,;
    Alignment=1, Width=27, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_9_119.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_122 as StdString with uid="FZAKCPYABY",Visible=.t., Left=268, Top=407,;
    Alignment=1, Width=71, Height=18,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  func oStr_9_122.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_123 as StdString with uid="OXKIJYFBEY",Visible=.t., Left=552, Top=380,;
    Alignment=1, Width=88, Height=18,;
    Caption="Evasione effet.:"  ;
  , bGlobalFont=.t.

  func oStr_9_123.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_125 as StdString with uid="AZDSZYWOIN",Visible=.t., Left=429, Top=407,;
    Alignment=1, Width=56, Height=15,;
    Caption="Evaso:"  ;
  , bGlobalFont=.t.

  func oStr_9_125.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_128 as StdString with uid="WYIKLTVKTB",Visible=.t., Left=621, Top=433,;
    Alignment=1, Width=43, Height=18,;
    Caption="Mag.:"  ;
  , bGlobalFont=.t.

  func oStr_9_128.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_132 as StdString with uid="LQCVITQTGE",Visible=.t., Left=129, Top=380,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_9_132.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or empty(.w_ALFDOC) or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_133 as StdString with uid="CDUARJMRAD",Visible=.t., Left=8, Top=380,;
    Alignment=1, Width=58, Height=18,;
    Caption="Num.doc.:"  ;
  , bGlobalFont=.t.

  func oStr_9_133.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2 or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_135 as StdString with uid="UKEJIQKHGQ",Visible=.t., Left=386, Top=380,;
    Alignment=1, Width=85, Height=18,;
    Caption="Evasione prev.:"  ;
  , bGlobalFont=.t.

  func oStr_9_135.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_137 as StdString with uid="RXBATOXXVL",Visible=.t., Left=578, Top=407,;
    Alignment=1, Width=61, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_9_137.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODL-OCL-OCF-DTV' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_139 as StdString with uid="DFBRFHQJOX",Visible=.t., Left=171, Top=380,;
    Alignment=1, Width=59, Height=18,;
    Caption="Num.reg.:"  ;
  , bGlobalFont=.t.

  func oStr_9_139.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<2  or .w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_140 as StdString with uid="WKANGMEWOO",Visible=.t., Left=6, Top=407,;
    Alignment=1, Width=60, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_9_140.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_142 as StdString with uid="WHBDPDJJZJ",Visible=.t., Left=572, Top=380,;
    Alignment=1, Width=68, Height=18,;
    Caption="Data reg.:"  ;
  , bGlobalFont=.t.

  func oStr_9_142.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_143 as StdString with uid="QJLHKHHQBD",Visible=.t., Left=398, Top=380,;
    Alignment=1, Width=73, Height=18,;
    Caption="Data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_9_143.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_144 as StdString with uid="KJOIZJFKZQ",Visible=.t., Left=289, Top=407,;
    Alignment=1, Width=50, Height=18,;
    Caption="Q.t�:"  ;
  , bGlobalFont=.t.

  func oStr_9_144.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODL-OCL-OCF-DPI' or .w_TIPGES<>'Z')
    endwith
  endfunc

  add object oStr_9_146 as StdString with uid="RVKNABAGKN",Visible=.t., Left=9, Top=380,;
    Alignment=1, Width=57, Height=18,;
    Caption="ODA:"  ;
  , bGlobalFont=.t.

  func oStr_9_146.mHide()
    with this.Parent.oContained
      return (.w_LEVEL=>1 or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_147 as StdString with uid="OFMJEDXDYT",Visible=.t., Left=208, Top=407,;
    Alignment=1, Width=27, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_9_147.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_150 as StdString with uid="HFOVASGHXF",Visible=.t., Left=268, Top=407,;
    Alignment=1, Width=71, Height=18,;
    Caption="Ordinato:"  ;
  , bGlobalFont=.t.

  func oStr_9_150.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_151 as StdString with uid="ERJVXOQLSX",Visible=.t., Left=552, Top=380,;
    Alignment=1, Width=88, Height=18,;
    Caption="Evasione effet.:"  ;
  , bGlobalFont=.t.

  func oStr_9_151.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_153 as StdString with uid="WOUWZMRGFO",Visible=.t., Left=429, Top=407,;
    Alignment=1, Width=56, Height=15,;
    Caption="Evaso:"  ;
  , bGlobalFont=.t.

  func oStr_9_153.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_156 as StdString with uid="FHGQFBAXVR",Visible=.t., Left=628, Top=433,;
    Alignment=1, Width=36, Height=18,;
    Caption="Mag.:"  ;
  , bGlobalFont=.t.

  func oStr_9_156.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_160 as StdString with uid="MQLTFKZJAI",Visible=.t., Left=129, Top=380,;
    Alignment=0, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_9_160.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1 or empty(.w_ALFDOC) or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_161 as StdString with uid="ZEQFLQCKSE",Visible=.t., Left=8, Top=380,;
    Alignment=1, Width=58, Height=18,;
    Caption="Num.doc.:"  ;
  , bGlobalFont=.t.

  func oStr_9_161.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1 or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_162 as StdString with uid="JMSXEFSYAT",Visible=.t., Left=386, Top=380,;
    Alignment=1, Width=85, Height=18,;
    Caption="Evasione prev.:"  ;
  , bGlobalFont=.t.

  func oStr_9_162.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_164 as StdString with uid="RIBWKMBTQU",Visible=.t., Left=578, Top=407,;
    Alignment=1, Width=61, Height=15,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_9_164.mHide()
    with this.Parent.oContained
      return (! .w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_166 as StdString with uid="RNRHFCGXKK",Visible=.t., Left=171, Top=380,;
    Alignment=1, Width=59, Height=18,;
    Caption="Num.reg.:"  ;
  , bGlobalFont=.t.

  func oStr_9_166.mHide()
    with this.Parent.oContained
      return (.w_LEVEL<1  or .w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_167 as StdString with uid="RSXGZSHCRF",Visible=.t., Left=6, Top=407,;
    Alignment=1, Width=60, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_9_167.mHide()
    with this.Parent.oContained
      return (.w_TYPE='DPI' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_169 as StdString with uid="WXUWTJLDDR",Visible=.t., Left=572, Top=380,;
    Alignment=1, Width=68, Height=18,;
    Caption="Data reg.:"  ;
  , bGlobalFont=.t.

  func oStr_9_169.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_170 as StdString with uid="UJUCZGTEDJ",Visible=.t., Left=398, Top=380,;
    Alignment=1, Width=73, Height=18,;
    Caption="Data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_9_170.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_171 as StdString with uid="IAOGNYKQQR",Visible=.t., Left=289, Top=407,;
    Alignment=1, Width=50, Height=18,;
    Caption="Q.t�:"  ;
  , bGlobalFont=.t.

  func oStr_9_171.mHide()
    with this.Parent.oContained
      return (.w_TYPE $ 'ORD-ODA' or .w_TIPGES<>'E')
    endwith
  endfunc

  add object oStr_9_173 as StdString with uid="ACFRCPVBAE",Visible=.t., Left=417, Top=380,;
    Alignment=1, Width=73, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_9_173.mHide()
    with this.Parent.oContained
      return (.w_TYPE<>'DPI' or .w_TIPGES<>'E')
    endwith
  endfunc
enddefine
define class tgsco_aopPag10 as StdContainer
  Width  = 854
  height = 473
  stdWidth  = 854
  stdheight = 473
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOL__NOTE_10_1 as StdMemo with uid="KBSOCDKPLF",rtseq=190,rtrep=.f.,;
    cFormVar = "w_OL__NOTE", cQueryName = "OL__NOTE",;
    bObbl = .f. , nPag = 10, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 163290155,;
   bGlobalFont=.t.,;
    Height=468, Width=845, Left=2, Top=5
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_aop','ODL_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OLCODODL=ODL_MAST.OLCODODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
