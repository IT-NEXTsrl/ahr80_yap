* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bex                                                        *
*              Esegui messaggi MRP                                             *
*                                                                              *
*      Author: Zucchetti SpA (DB)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-18                                                      *
* Last revis.: 2018-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bex",oParentObject,m.pAzione)
return(i_retval)

define class tgsmr_bex as StdBatch
  * --- Local variables
  pAzione = space(1)
  w_MRSERIAL = space(10)
  w_MRTIPOMS = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_OLCODODL = space(15)
  w_CPROWNUM = 0
  w_ZOOM = .NULL.
  w_SELODL = space(15)
  w_OLTDTRIC = ctod("  /  /  ")
  w_OLTDINRIC = ctod("  /  /  ")
  w_ODLOK = .f.
  w_MREXERRO = space(80)
  w_FLEVAS = space(1)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MSG = space(10)
  w_MSGR = space(10)
  w_DBCODICE = space(41)
  w_DBDATCRE = ctod("  /  /  ")
  W_DBDATDIS = ctod("  /  /  ")
  w_OLTQTOD1 = 0
  w_OLTPERAS = space(3)
  w_OLTLEMPS = 0
  w_OLTDTMPS = ctod("  /  /  ")
  w_OLTDTCON = ctod("  /  /  ")
  w_OLTSFLT = 0
  w_OLTPROVE = space(1)
  w_OLTSTATO = space(1)
  TmpL = .f.
  TmpC = space(100)
  TmpD = ctod("  /  /  ")
  TmpN1 = 0
  w_OLTEMLAV = 0
  w_DTABBI = ctod("  /  /  ")
  w_DATRIF = ctod("  /  /  ")
  w_COCORLEA = 0
  w_OLDATRIC = ctod("  /  /  ")
  w_OLCODODL = space(15)
  w_LTFASE = 0
  w_DATINIZ = ctod("  /  /  ")
  w_DATFINE = ctod("  /  /  ")
  w_ANSWER = 0
  w_OLTSEDOC = space(10)
  w_OLTCPDOC = 0
  w_ULTIMAFASE = .f.
  w_CODODL = space(15)
  w_CODMAG = space(5)
  w_QTAODL = 0
  w_KEYSAL = space(20)
  w_PERASS = space(3)
  w_FLGORD = space(1)
  w_FLGIMP = space(1)
  w_FLAORD = space(1)
  w_FLAIMP = space(1)
  w_FLARIS = space(1)
  w_SERDOCL = space(10)
  w_CPRDOCL = 0
  w_OLTCODIC = space(41)
  w_FLCOMM = space(1)
  w_CODCOM = space(15)
  w_CODART = space(20)
  w_MVRIFESP = space(10)
  w_MVRIFESC = space(10)
  w_DOCEVA = space(10)
  w_MVTIPDOC = space(5)
  w_MVDATREG = ctod("  /  /  ")
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVTCOLIS = space(5)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVCODESE = space(4)
  w_OLDTES = space(35)
  w_MVVALNAZ = space(3)
  w_MVCODMAG = space(5)
  w_MVTIPIMB = space(1)
  w_MAXLEV = 0
  w_VALCOM = space(1)
  w_CAUCOD = space(5)
  w_CAUPFI = space(5)
  w_FLARCO = space(1)
  w_TDFLEXPL = space(1)
  w_TDEXPAUT = space(1)
  w_TDCOSEPL = space(1)
  w_EVAKEY = space(20)
  w_EVAMAG = space(5)
  w_EVAMAT = space(5)
  w_EVAQTA = 0
  w_EVAQT1 = 0
  w_EVACAR = space(1)
  w_EVAORD = space(1)
  w_EVAIMP = space(1)
  w_EVARIS = space(1)
  w_EV2CAR = space(1)
  w_EV2ORD = space(1)
  w_EV2IMP = space(1)
  w_EV2RIS = space(1)
  w_EVADAT = ctod("  /  /  ")
  w_EVANUM = 0
  w_EVAALF = space(2)
  w_EVASER = space(10)
  w_EVAROW = 0
  w_EVAFLA = space(1)
  w_EVAFLE = space(1)
  w_EVAKEY2 = space(20)
  w_EVAMAG2 = space(5)
  w_EVQIMP = 0
  w_EVQIM1 = 0
  w_EVFLORDI = space(1)
  w_EVFLIMPE = space(1)
  w_EVFLRISE = space(1)
  w_EVEVAKEY2 = space(20)
  w_EVEVAMAG2 = space(5)
  w_EVOQTAEVA = 0
  w_EVOQTAEV1 = 0
  w_EVOQTAMOV = 0
  w_EVOQTAUM1 = 0
  w_EVOQTASAL = 0
  w_EVNQTAEVA = 0
  w_EVNQTAEV1 = 0
  w_EVNQTASAL = 0
  w_OK = .f.
  w_EVNUMRIF = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  MAGAZZIN_idx=0
  MPS_TFOR_idx=0
  MRP_MESS_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  SALDIART_idx=0
  ART_ICOL_idx=0
  DIC_PROD_idx=0
  DISTBASE_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Messaggi di ripianificazoine
    * --- Parametri di stampa
    this.Padre = this.oParentObject
    * --- Punta al padre
    if this.pAzione <> "MRP"
      this.NC = this.Padre.w_sZoom.cCursor
      * --- Nome cursore collegato allo zoom
      this.w_ZOOM = this.Padre.w_sZoom
      * --- Zoom
    endif
    do case
      case this.pAzione = "SS"
        if used(this.NC)
          * --- Seleziona tutte le righe dello zoom
          UPDATE (this.NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
        endif
      case this.pAzione = "AG"
        Select (this.NC)
        Scan for xchk=1
        this.w_MRSERIAL = MRSERIAL
        this.w_MRTIPOMS = MRTIPOMS
        this.w_SELODL = MRDOCODL
        this.w_OLTDTCON = MRDATAFI
        if "." $ this.w_SELODL
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Read from ODL_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ODL_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "OLCODODL,OLTQTOD1,OLTPERAS,OLTLEMPS,OLTDTMPS,OLTEMLAV,OLTSAFLT,OLTPROVE,OLTSTATO,OLTQTSAL,OLTKEYSA,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTCODIC,OLTCOART,OLTCOMME"+;
              " from "+i_cTable+" ODL_MAST where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_SELODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              OLCODODL,OLTQTOD1,OLTPERAS,OLTLEMPS,OLTDTMPS,OLTEMLAV,OLTSAFLT,OLTPROVE,OLTSTATO,OLTQTSAL,OLTKEYSA,OLTFLORD,OLTFLIMP,OLTCOMAG,OLTCODIC,OLTCOART,OLTCOMME;
              from (i_cTable) where;
                  OLCODODL = this.w_SELODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OLCODODL = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
            this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
            this.w_OLTPERAS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
            this.w_OLTLEMPS = NVL(cp_ToDate(_read_.OLTLEMPS),cp_NullValue(_read_.OLTLEMPS))
            this.w_OLTDTMPS = NVL(cp_ToDate(_read_.OLTDTMPS),cp_NullValue(_read_.OLTDTMPS))
            this.w_OLTEMLAV = NVL(cp_ToDate(_read_.OLTEMLAV),cp_NullValue(_read_.OLTEMLAV))
            this.w_OLTSFLT = NVL(cp_ToDate(_read_.OLTSAFLT),cp_NullValue(_read_.OLTSAFLT))
            this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
            this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
            this.w_QTAODL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
            this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
            this.w_FLGORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
            this.w_FLGIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
            this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
            this.w_OLTCODIC = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
            this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
            this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVSERIAL,CPROWNUM"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVCODODL = "+cp_ToStrODBC(this.w_SELODL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVSERIAL,CPROWNUM;
              from (i_cTable) where;
                  MVCODODL = this.w_SELODL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERDOCL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            this.w_CPRDOCL = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case inlist(this.w_MRTIPOMS, "PO", "AN")
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_MRTIPOMS = "CA"
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
        endif
        this.w_MSG = "Aggiornamento completato"
        EndScan
        if not empty(this.w_MSG)
          ah_ErrorMsg(this.w_MSG,64)
        endif
        this.Padre.NotifyEvent("Interroga")     
      case this.pAzione = "MRP"
        * --- Select from MRP_MESS
        i_nConn=i_TableProp[this.MRP_MESS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2],.t.,this.MRP_MESS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MRSERIAL,MRTIPOMS,MRCODODL,MRDATAFI  from "+i_cTable+" MRP_MESS ";
              +" where MRTIPOMS IN ('PO','AN','CA')";
               ,"_Curs_MRP_MESS")
        else
          select MRSERIAL,MRTIPOMS,MRCODODL,MRDATAFI from (i_cTable);
           where MRTIPOMS IN ("PO","AN","CA");
            into cursor _Curs_MRP_MESS
        endif
        if used('_Curs_MRP_MESS')
          select _Curs_MRP_MESS
          locate for 1=1
          do while not(eof())
          this.w_MRSERIAL = _Curs_MRP_MESS.MRSERIAL
          this.w_MRTIPOMS = _Curs_MRP_MESS.MRTIPOMS
          this.w_SELODL = _Curs_MRP_MESS.MRCODODL
          this.w_OLTDTCON = _Curs_MRP_MESS.MRDATAFI
          if "." $ this.w_SELODL
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLCODODL,OLTQTOD1,OLTPERAS,OLTLEMPS,OLTDTMPS,OLTEMLAV,OLTSAFLT,OLTPROVE,OLTSTATO,OLTQTSAL,OLTKEYSA,OLTFLORD,OLTFLIMP,OLTSEDOC,OLTCPDOC,OLTCOMAG,OLTCODIC,OLTCOART,OLTCOMME"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_SELODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLCODODL,OLTQTOD1,OLTPERAS,OLTLEMPS,OLTDTMPS,OLTEMLAV,OLTSAFLT,OLTPROVE,OLTSTATO,OLTQTSAL,OLTKEYSA,OLTFLORD,OLTFLIMP,OLTSEDOC,OLTCPDOC,OLTCOMAG,OLTCODIC,OLTCOART,OLTCOMME;
                from (i_cTable) where;
                    OLCODODL = this.w_SELODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLCODODL = NVL(cp_ToDate(_read_.OLCODODL),cp_NullValue(_read_.OLCODODL))
              this.w_OLTQTOD1 = NVL(cp_ToDate(_read_.OLTQTOD1),cp_NullValue(_read_.OLTQTOD1))
              this.w_OLTPERAS = NVL(cp_ToDate(_read_.OLTPERAS),cp_NullValue(_read_.OLTPERAS))
              this.w_OLTLEMPS = NVL(cp_ToDate(_read_.OLTLEMPS),cp_NullValue(_read_.OLTLEMPS))
              this.w_OLTDTMPS = NVL(cp_ToDate(_read_.OLTDTMPS),cp_NullValue(_read_.OLTDTMPS))
              this.w_OLTEMLAV = NVL(cp_ToDate(_read_.OLTEMLAV),cp_NullValue(_read_.OLTEMLAV))
              this.w_OLTSFLT = NVL(cp_ToDate(_read_.OLTSAFLT),cp_NullValue(_read_.OLTSAFLT))
              this.w_OLTPROVE = NVL(cp_ToDate(_read_.OLTPROVE),cp_NullValue(_read_.OLTPROVE))
              this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
              this.w_QTAODL = NVL(cp_ToDate(_read_.OLTQTSAL),cp_NullValue(_read_.OLTQTSAL))
              this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
              this.w_FLGORD = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
              this.w_FLGIMP = NVL(cp_ToDate(_read_.OLTFLIMP),cp_NullValue(_read_.OLTFLIMP))
              this.w_SERDOCL = NVL(cp_ToDate(_read_.OLTSEDOC),cp_NullValue(_read_.OLTSEDOC))
              this.w_CPRDOCL = NVL(cp_ToDate(_read_.OLTCPDOC),cp_NullValue(_read_.OLTCPDOC))
              this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
              this.w_OLTCODIC = NVL(cp_ToDate(_read_.OLTCODIC),cp_NullValue(_read_.OLTCODIC))
              this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
              this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case inlist(this.w_MRTIPOMS, "PO", "AN")
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.w_MRTIPOMS = "CA"
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
          endif
            select _Curs_MRP_MESS
            continue
          enddo
          use
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variazione Data ODL
    this.w_ODLOK = True
    this.w_CODODL = this.w_SELODL
    this.w_PERASS = this.w_OLTPERAS
    do case
      case empty(this.w_OLCODODL)
        this.w_ODLOK = False
        this.w_MREXERRO = ah_msgformat("Codice ODL non valido")
      case this.w_OLTSTATO = "F"
        this.w_ODLOK = False
        this.w_MREXERRO = ah_msgformat("ODL finito")
      case this.w_OLTSTATO = "L"
        * --- Read from DIC_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIC_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_PROD_idx,2],.t.,this.DIC_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPSERIAL"+;
            " from "+i_cTable+" DIC_PROD where ";
                +"DPCODODL = "+cp_ToStrODBC(this.w_SELODL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPSERIAL;
            from (i_cTable) where;
                DPCODODL = this.w_SELODL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.TmpC = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(NVL(this.TmpC,""))
          this.w_ODLOK = False
          this.w_MREXERRO = ah_msgformat("Impossibile spostare/annullare ODL gi� dichiarato")
        endif
      case this.w_OLTSTATO $ "MP"
    endcase
    if this.w_ODLOK and this.w_MRTIPOMS <> "CA"
      * --- Controlla che l'ODL non sia spostato fuori dell'intervallo di validit� DB
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARCODDIS"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARCODDIS;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DBCODICE = NVL(cp_ToDate(_read_.ARCODDIS),cp_NullValue(_read_.ARCODDIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Select from DISTBASE
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select min(DBINIVAL) as DBINIVAL, max(DBFINVAL) as DBFINVAL  from "+i_cTable+" DISTBASE ";
            +" where DBCODICE="+cp_ToStrODBC(this.w_DBCODICE)+"";
             ,"_Curs_DISTBASE")
      else
        select min(DBINIVAL) as DBINIVAL, max(DBFINVAL) as DBFINVAL from (i_cTable);
         where DBCODICE=this.w_DBCODICE;
          into cursor _Curs_DISTBASE
      endif
      if used('_Curs_DISTBASE')
        select _Curs_DISTBASE
        locate for 1=1
        do while not(eof())
        this.w_DBDATCRE = _Curs_DISTBASE.DBINIVAL
        this.w_DBDATDIS = _Curs_DISTBASE.DBFINVAL
          select _Curs_DISTBASE
          continue
        enddo
        use
      endif
      if this.w_OLTDTRIC<this.w_DBDATCRE or this.w_OLTDTRIC>this.w_DBDATDIS
        this.w_ODLOK = False
        this.w_MREXERRO = ah_msgformat("Impossibile spostare l'ODL fuori dell'intervallo di validit� della DB")
      endif
    endif
    if not this.w_ODLOK and this.pAzione = "MRP"
      * --- Log Errori MRP
      Insert into LogErrori (ODL, Codric, Quan, datini, datfin, Messagg) values ;
      (this.w_SELODL, this.w_OLTCODIC, this.w_OLTQTOD1, this.w_OLTDINRIC, this.w_OLTDTRIC, this.w_MREXERRO)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variazione Data ODL
    if not empty(this.w_OLTDTCON)
      * --- Ricalcola data di fine produzione
      if this.w_OLTSFLT>0
        this.TmpD = COCALCLT(this.w_OLTDTCON,this.w_OLTSFLT,"I",.F.,"")
        this.w_OLTDTRIC = this.TmpD
      else
        this.w_OLTDTRIC = this.w_OLTDTCON
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ODLOK
        * --- Determina periodo di appartenenza dell'ODL
        this.w_DATRIF = this.w_OLTDTRIC
        vq_exec ("..\cola\exe\query\GSCO_BOL", this, "__Temp__")
        if used("__Temp__")
          * --- Preleva periodo assoluto
          select __Temp__
          go top
          this.w_OLTPERAS = __Temp__.TPPERASS
          * --- Chiude cursore
          USE IN __Temp__
        endif
        * --- determina nuove date
        * --- Ricalcola data di inizio (MPS)
        if not empty(this.w_OLTDTMPS)
          if this.w_OLTLEMPS>0
            this.TmpD = COCALCLT(this.w_OLTDTRIC, this.w_OLTLEMPS, "I", .F., "")
            this.w_OLTDTMPS = this.TmpD
          else
            this.w_OLTDTMPS = this.w_OLTDTRIC
          endif
        endif
        * --- Ricalcola data di inizio (Produzione)
        if this.w_OLTEMLAV>0
          this.TmpD = COCALCLT(this.w_OLTDTRIC, this.w_OLTEMLAV, "I", .F., "")
          this.w_OLTDINRIC = this.TmpD
        else
          this.w_OLTDINRIC = this.w_OLTDTRIC
        endif
        * --- Try
        local bErr_04D57E28
        bErr_04D57E28=bTrsErr
        this.Try_04D57E28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          i_errmsg = iif(empty(i_errmsg), message(), i_errmsg)
          * --- Write into MRP_MESS
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MRP_MESS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("E"),'MRP_MESS','MRESEGUI');
            +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
            +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
            +",MREXERRO ="+cp_NullLink(cp_ToStrODBC(padr(i_errmsg, 80)),'MRP_MESS','MREXERRO');
                +i_ccchkf ;
            +" where ";
                +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
                   )
          else
            update (i_cTable) set;
                MRESEGUI = "E";
                ,MRDATAEX = i_DATSYS;
                ,MRUTEXEC = i_CODUTE;
                ,MREXERRO = padr(i_errmsg, 80);
                &i_ccchkf. ;
             where;
                MRSERIAL = this.w_MRSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04D57E28
        * --- End
      else
        * --- Write into MRP_MESS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MRP_MESS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("E"),'MRP_MESS','MRESEGUI');
          +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
          +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
          +",MREXERRO ="+cp_NullLink(cp_ToStrODBC(this.w_MREXERRO),'MRP_MESS','MREXERRO');
              +i_ccchkf ;
          +" where ";
              +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
                 )
        else
          update (i_cTable) set;
              MRESEGUI = "E";
              ,MRDATAEX = i_DATSYS;
              ,MRUTEXEC = i_CODUTE;
              ,MREXERRO = this.w_MREXERRO;
              &i_ccchkf. ;
           where;
              MRSERIAL = this.w_MRSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc
  proc Try_04D57E28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna ODL - Materiali - Fasi - Documenti
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorna ODL
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTPERAS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTPERAS),'ODL_MAST','OLTPERAS');
      +",OLTDTMPS ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTMPS),'ODL_MAST','OLTDTMPS');
      +",OLTDINRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDINRIC),'ODL_MAST','OLTDINRIC');
      +",OLTDTRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTRIC),'ODL_MAST','OLTDTRIC');
      +",OLTDTCON ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTCON),'ODL_MAST','OLTDTCON');
      +",OLTVARIA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTVARIA');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_SELODL);
             )
    else
      update (i_cTable) set;
          OLTPERAS = this.w_OLTPERAS;
          ,OLTDTMPS = this.w_OLTDTMPS;
          ,OLTDINRIC = this.w_OLTDINRIC;
          ,OLTDTRIC = this.w_OLTDTRIC;
          ,OLTDTCON = this.w_OLTDTCON;
          ,OLTVARIA = "S";
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_SELODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna Lista Materiali
    * --- Select from ODL_DETT
    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
          +" where OLCODODL="+cp_ToStrODBC(this.w_SELODL)+"";
           ,"_Curs_ODL_DETT")
    else
      select * from (i_cTable);
       where OLCODODL=this.w_SELODL;
        into cursor _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      select _Curs_ODL_DETT
      locate for 1=1
      do while not(eof())
      * --- Aggiorna data su Materiali
      this.w_COCORLEA = _Curs_ODL_DETT.OLCORRLT
      if this.w_COCORLEA <> 0
        * --- Ricalcola data di inizio (Produzione)
        this.TmpD = COCALCLT(this.w_OLTDinRIC,Ceiling(this.w_COCORLEA),"C",.F.,"")
        this.w_OLDATRIC = this.TmpD
      else
        this.w_OLDATRIC = this.w_OLTDINRIC
      endif
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLDATRIC ="+cp_NullLink(cp_ToStrODBC(this.w_OLDATRIC),'ODL_DETT','OLDATRIC');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_SELODL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
               )
      else
        update (i_cTable) set;
            OLDATRIC = this.w_OLDATRIC;
            &i_ccchkf. ;
         where;
            OLCODODL = this.w_SELODL;
            and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ODL_DETT
        continue
      enddo
      use
    endif
    * --- Provenienza Esterna - Modifica le date del Documento
    this.w_DATFINE = this.w_OLTDTRIC
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_DATFINE),'DOC_DETT','MVDATEVA');
          +i_ccchkf ;
      +" where ";
          +"MVCODODL = "+cp_ToStrODBC(this.w_SELODL);
          +" and MVNUMRIF = "+cp_ToStrODBC(-20);
             )
    else
      update (i_cTable) set;
          MVDATEVA = this.w_DATFINE;
          &i_ccchkf. ;
       where;
          MVCODODL = this.w_SELODL;
          and MVNUMRIF = -20;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into MRP_MESS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MRP_MESS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("S"),'MRP_MESS','MRESEGUI');
      +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
      +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
             )
    else
      update (i_cTable) set;
          MRESEGUI = "S";
          ,MRDATAEX = i_DATSYS;
          ,MRUTEXEC = i_CODUTE;
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_MRSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina / Chiude ODL
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_ODLOK
      * --- Try
      local bErr_04D87050
      bErr_04D87050=bTrsErr
      this.Try_04D87050()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        i_errmsg = iif(empty(i_errmsg), message(), i_errmsg)
        * --- Write into MRP_MESS
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MRP_MESS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("E"),'MRP_MESS','MRESEGUI');
          +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
          +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
          +",MREXERRO ="+cp_NullLink(cp_ToStrODBC(padr(i_errmsg, 80)),'MRP_MESS','MREXERRO');
              +i_ccchkf ;
          +" where ";
              +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
                 )
        else
          update (i_cTable) set;
              MRESEGUI = "E";
              ,MRDATAEX = i_DATSYS;
              ,MRUTEXEC = i_CODUTE;
              ,MREXERRO = padr(i_errmsg, 80);
              &i_ccchkf. ;
           where;
              MRSERIAL = this.w_MRSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04D87050
      * --- End
    else
      * --- Write into MRP_MESS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MRP_MESS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("E"),'MRP_MESS','MRESEGUI');
        +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
        +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
        +",MREXERRO ="+cp_NullLink(cp_ToStrODBC(this.w_MREXERRO),'MRP_MESS','MREXERRO');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
               )
      else
        update (i_cTable) set;
            MRESEGUI = "E";
            ,MRDATAEX = i_DATSYS;
            ,MRUTEXEC = i_CODUTE;
            ,MREXERRO = this.w_MREXERRO;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_MRSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_04D87050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna ODL - Materiali - Fasi - Documenti
    * --- begin transaction
    cp_BeginTrs()
    do case
      case this.w_OLTPROVE $ "LE" AND this.w_OLTSTATO = "L"
        * --- CHIUSURA ORDINI DI C/LAVORO IN LAVORAZIONE
        * --- I riferimenti al documento ORDINE collegato sono w_SERDOCL e w_CPRDOCL
        * --- Aggiorna OCL (nota: non occorre aggiornare i saldi, perch� i saldi vengono gestiti dal DOCUMENTO ORDINE)
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "F";
              ,OLTQTSAL = 0;
              ,OLTFLEVA = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorna Documento
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL,MVCODMAG,MVCODCOM,MVCODART"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPRDOCL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL,MVCODMAG,MVCODCOM,MVCODART;
            from (i_cTable) where;
                MVSERIAL = this.w_SERDOCL;
                and CPROWNUM = this.w_CPRDOCL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLAORD = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
          this.w_FLAIMP = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
          this.w_FLARIS = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
          this.TmpN1 = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
          this.w_CODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
          this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
          this.w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
          +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPRDOCL);
                 )
        else
          update (i_cTable) set;
              MVQTASAL = 0;
              ,MVFLEVAS = "S";
              ,MVEFFEVA = i_DATSYS;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERDOCL;
              and CPROWNUM = this.w_CPRDOCL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLCOMM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLCOMM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aggiorna Saldi
        this.w_FLAORD = iif(this.w_FLAORD="+","-",iif(this.w_FLAORD="-","+"," "))
        this.w_FLAIMP = iif(this.w_FLAIMP="+","-",iif(this.w_FLAIMP="-","+"," "))
        this.w_FLARIS = iif(this.w_FLARIS="+","-",iif(this.w_FLARIS="-","+"," "))
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','this.TmpN1',this.TmpN1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','this.TmpN1',this.TmpN1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','this.TmpN1',this.TmpN1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              ,SLQTRPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_CODART;
              and SLCODMAG = this.w_CODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
          * --- Aggiorna i saldi commessa
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','this.TmpN1',this.TmpN1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','this.TmpN1',this.TmpN1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','this.TmpN1',this.TmpN1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                ,SCQTRPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_CODART;
                and SCCODMAG = this.w_CODMAG;
                and SCCODCAN = this.w_CODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Aggiorna Saldi MPS
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.TmpN1);
          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.TmpN1);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_CODART);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSLAN = FMMPSLAN - this.TmpN1;
              ,FMMPSTOT = FMMPSTOT - this.TmpN1;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_CODART;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Chiude eventuale impegnato dettaglio OCL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+" and OLQTASAL>0";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_CODODL and OLQTASAL>0;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = _Curs_ODL_DETT.OLCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARFLCOMM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARFLCOMM;
              from (i_cTable) where;
                  ARCODART = _Curs_ODL_DETT.OLCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_DISMAG="S"
            * --- Aggiorna Saldi dei magazzini nettificabili
            this.w_FLAORD = nvl(_Curs_ODL_DETT.OLFLORDI," ")
            this.w_FLAORD = iif(this.w_FLAORD="+","-",iif(this.w_FLAORD="-","+"," "))
            this.w_FLAIMP = nvl(_Curs_ODL_DETT.OLFLIMPE," ")
            this.w_FLAIMP = iif(this.w_FLAIMP="+","-",iif(this.w_FLAIMP="-","+"," "))
            this.w_FLARIS = nvl(_Curs_ODL_DETT.OLFLRISE," ")
            this.w_FLARIS = iif(this.w_FLARIS="+","-",iif(this.w_FLARIS="-","+"," "))
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  ,SLQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = _Curs_ODL_DETT.OLCODART;
                  and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
              * --- Aggiorna i saldi commessa
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    ,SCQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                    and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- Aggiorna dettaglio OCL
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
            +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                OLFLEVAS = "S";
                ,OLQTASAL = 0;
                &i_ccchkf. ;
             where;
                OLCODODL = _Curs_ODL_DETT.OLCODODL;
                and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
      case this.w_OLTPROVE = "I" and this.w_OLTSTATO $ "LP"
        * --- CHIUSURA ORDINI DI LAVORAZIONE
        * --- Aggiorna ODL
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "F";
              ,OLTQTSAL = 0;
              ,OLTFLEVA = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Aggiorna Saldi MPS
        do case
          case this.w_OLTSTATO="L"
            * --- Write into MPS_TFOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FMMPSLAN =FMMPSLAN- "+cp_ToStrODBC(this.w_QTAODL);
              +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                  +i_ccchkf ;
              +" where ";
                  +"FMCODART = "+cp_ToStrODBC(this.w_CODART);
                  +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                     )
            else
              update (i_cTable) set;
                  FMMPSLAN = FMMPSLAN - this.w_QTAODL;
                  ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                  &i_ccchkf. ;
               where;
                  FMCODART = this.w_CODART;
                  and FMPERASS = this.w_PERASS;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.w_OLTSTATO="P"
            * --- Stato = 'P'
            * --- Write into MPS_TFOR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_QTAODL);
              +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
                  +i_ccchkf ;
              +" where ";
                  +"FMCODART = "+cp_ToStrODBC(this.w_CODART);
                  +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                     )
            else
              update (i_cTable) set;
                  FMMPSPIA = FMMPSPIA - this.w_QTAODL;
                  ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
                  &i_ccchkf. ;
               where;
                  FMCODART = this.w_CODART;
                  and FMPERASS = this.w_PERASS;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLCOMM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLCOMM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_CODART;
              and SLCODMAG = this.w_CODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
          * --- Aggiorna i saldi commessa
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SCQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SCQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_CODART;
                and SCCODMAG = this.w_CODMAG;
                and SCCODCAN = this.w_CODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Chiude eventuale impegnato dettaglio ODL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+" and OLQTASAL>0";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_CODODL and OLQTASAL>0;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = _Curs_ODL_DETT.OLCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARFLCOMM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARFLCOMM;
              from (i_cTable) where;
                  ARCODART = _Curs_ODL_DETT.OLCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_DISMAG="S"
            * --- Aggiorna Saldi dei magazzini nettificabili
            this.w_FLAORD = nvl(_Curs_ODL_DETT.OLFLORDI," ")
            this.w_FLAORD = iif(this.w_FLAORD="+","-",iif(this.w_FLAORD="-","+"," "))
            this.w_FLAIMP = nvl(_Curs_ODL_DETT.OLFLIMPE," ")
            this.w_FLAIMP = iif(this.w_FLAIMP="+","-",iif(this.w_FLAIMP="-","+"," "))
            this.w_FLARIS = nvl(_Curs_ODL_DETT.OLFLRISE," ")
            this.w_FLARIS = iif(this.w_FLARIS="+","-",iif(this.w_FLARIS="-","+"," "))
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  ,SLQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = _Curs_ODL_DETT.OLCODART;
                  and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
              * --- Aggiorna i saldi commessa
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    ,SCQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                    and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- Aggiorna dettaglio OCL
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
            +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                OLFLEVAS = "S";
                ,OLQTASAL = 0;
                &i_ccchkf. ;
             where;
                OLCODODL = _Curs_ODL_DETT.OLCODODL;
                and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
      case this.w_OLTSTATO = "M"
        * --- Elimina ODL Suggeriti
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLCOMM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLCOMM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aggiorna Saldi
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_CODART;
              and SLCODMAG = this.w_CODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Chiude eventuale impegnato dettaglio ODL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+" and OLQTASAL>0";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_CODODL and OLQTASAL>0;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = _Curs_ODL_DETT.OLCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARFLCOMM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARFLCOMM;
              from (i_cTable) where;
                  ARCODART = _Curs_ODL_DETT.OLCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_DISMAG="S"
            * --- Aggiorna Saldi dei magazzini nettificabili
            this.w_FLAORD = nvl(_Curs_ODL_DETT.OLFLORDI," ")
            this.w_FLAORD = iif(this.w_FLAORD="+","-",iif(this.w_FLAORD="-","+"," "))
            this.w_FLAIMP = nvl(_Curs_ODL_DETT.OLFLIMPE," ")
            this.w_FLAIMP = iif(this.w_FLAIMP="+","-",iif(this.w_FLAIMP="-","+"," "))
            this.w_FLARIS = nvl(_Curs_ODL_DETT.OLFLRISE," ")
            this.w_FLARIS = iif(this.w_FLARIS="+","-",iif(this.w_FLARIS="-","+"," "))
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  ,SLQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = _Curs_ODL_DETT.OLCODART;
                  and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
              * --- Aggiorna i saldi commessa
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    ,SCQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                    and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
        * --- Delete from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          delete from (i_cTable) where;
                OLCODODL = this.w_CODODL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from ODL_MAST
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          delete from (i_cTable) where;
                OLCODODL = this.w_CODODL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_OLTPROVE $ "LE" AND this.w_OLTSTATO = "P"
        * --- I riferimenti al documento ORDINE collegato sono w_SERDOCL e w_CPRDOCL
        * --- Aggiorna Saldi MPS
        * --- Write into MPS_TFOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MPS_TFOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MPS_TFOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MPS_TFOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FMMPSPIA =FMMPSPIA- "+cp_ToStrODBC(this.w_QTAODL);
          +",FMMPSTOT =FMMPSTOT- "+cp_ToStrODBC(this.w_QTAODL);
              +i_ccchkf ;
          +" where ";
              +"FMCODART = "+cp_ToStrODBC(this.w_CODART);
              +" and FMPERASS = "+cp_ToStrODBC(this.w_PERASS);
                 )
        else
          update (i_cTable) set;
              FMMPSPIA = FMMPSPIA - this.w_QTAODL;
              ,FMMPSTOT = FMMPSTOT - this.w_QTAODL;
              &i_ccchkf. ;
           where;
              FMCODART = this.w_CODART;
              and FMPERASS = this.w_PERASS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARFLCOMM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARFLCOMM;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              ,SLQTIPER = &i_cOp2.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_CODART;
              and SLCODMAG = this.w_CODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
          * --- Aggiorna i saldi commessa
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SCQTOPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SCQTIPER','-this.w_QTAODL',-this.w_QTAODL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_CODART);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                ,SCQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_CODART;
                and SCCODMAG = this.w_CODMAG;
                and SCCODCAN = this.w_CODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Chiude eventuale impegnato dettaglio ODL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+"";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_CODODL;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          * --- Read from MAGAZZIN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MGDISMAG"+;
              " from "+i_cTable+" MAGAZZIN where ";
                  +"MGCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MGDISMAG;
              from (i_cTable) where;
                  MGCODMAG = _Curs_ODL_DETT.OLCODMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARFLCOMM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARFLCOMM;
              from (i_cTable) where;
                  ARCODART = _Curs_ODL_DETT.OLCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOMM = NVL(cp_ToDate(_read_.ARFLCOMM),cp_NullValue(_read_.ARFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if w_DISMAG="S"
            * --- Aggiorna Saldi dei magazzini nettificabili
            this.w_FLAORD = nvl(_Curs_ODL_DETT.OLFLORDI," ")
            this.w_FLAORD = iif(this.w_FLAORD="+","-",iif(this.w_FLAORD="-","+"," "))
            this.w_FLAIMP = nvl(_Curs_ODL_DETT.OLFLIMPE," ")
            this.w_FLAIMP = iif(this.w_FLAIMP="+","-",iif(this.w_FLAIMP="-","+"," "))
            this.w_FLARIS = nvl(_Curs_ODL_DETT.OLFLRISE," ")
            this.w_FLARIS = iif(this.w_FLARIS="+","-",iif(this.w_FLARIS="-","+"," "))
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  ,SLQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = _Curs_ODL_DETT.OLCODART;
                  and SLCODMAG = _Curs_ODL_DETT.OLCODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if NVL(this.w_FLCOMM,"N")="S" and ! empty(nvl(this.w_CODCOM," "))
              * --- Aggiorna i saldi commessa
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','_Curs_ODL_DETT.OLQTASAL',_Curs_ODL_DETT.OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(_Curs_ODL_DETT.OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                       )
              else
                update (i_cTable) set;
                    SCQTOPER = &i_cOp1.;
                    ,SCQTIPER = &i_cOp2.;
                    ,SCQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = _Curs_ODL_DETT.OLKEYSAL;
                    and SCCODMAG = _Curs_ODL_DETT.OLCODMAG;
                    and SCCODCAN = this.w_CODCOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          * --- Elimina dettaglio OCL
          * --- Delete from ODL_DETT
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            delete from (i_cTable) where;
                  OLCODODL = _Curs_ODL_DETT.OLCODODL;
                  and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
        * --- Delete from ODL_MAST
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          delete from (i_cTable) where;
                OLCODODL = this.w_CODODL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
    * --- Write into MRP_MESS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MRP_MESS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("S"),'MRP_MESS','MRESEGUI');
      +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
      +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
             )
    else
      update (i_cTable) set;
          MRESEGUI = "S";
          ,MRDATAEX = i_DATSYS;
          ,MRUTEXEC = i_CODUTE;
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_MRSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Documenti di acquisto
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVSERIAL,CPROWNUM,MVFLEVAS,MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL,MVCODMAG,MVKEYSAL,MVCODART,MVCODCOM,MVRIFESC"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(left(this.w_SELODL,10));
            +" and CPROWNUM = "+cp_ToStrODBC(val(substr(this.w_SELODL,12)));
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVSERIAL,CPROWNUM,MVFLEVAS,MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL,MVCODMAG,MVKEYSAL,MVCODART,MVCODCOM,MVRIFESC;
        from (i_cTable) where;
            MVSERIAL = left(this.w_SELODL,10);
            and CPROWNUM = val(substr(this.w_SELODL,12));
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
      this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      this.w_FLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
      this.w_FLAORD = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
      this.w_FLAIMP = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
      this.w_FLARIS = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
      this.TmpN1 = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
      this.w_CODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
      this.w_KEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
      this.w_CODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
      this.w_CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
      this.w_MVRIFESC = NVL(cp_ToDate(_read_.MVRIFESC),cp_NullValue(_read_.MVRIFESC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVRIFESP,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON,MVDATDOC,MVTCOLIS,MVNUMDOC,MVALFDOC,MVCODESE,MVVALNAZ"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(left(this.w_SELODL,10));
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVRIFESP,MVTIPDOC,MVDATREG,MVTIPCON,MVCODCON,MVDATDOC,MVTCOLIS,MVNUMDOC,MVALFDOC,MVCODESE,MVVALNAZ;
        from (i_cTable) where;
            MVSERIAL = left(this.w_SELODL,10);
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVRIFESP = NVL(cp_ToDate(_read_.MVRIFESP),cp_NullValue(_read_.MVRIFESP))
      this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
      this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
      this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_MVTCOLIS = NVL(cp_ToDate(_read_.MVTCOLIS),cp_NullValue(_read_.MVTCOLIS))
      this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_MVCODESE = NVL(cp_ToDate(_read_.MVCODESE),cp_NullValue(_read_.MVCODESE))
      this.w_MVVALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MREXERRO = ""
    if this.w_CPROWNUM=0
      this.w_MREXERRO = ah_msgformat("Riga documento non valida")
    else
      do case
        case inlist(this.w_MRTIPOMS, "PO", "AN")
          if this.w_FLEVAS="S"
            this.w_MREXERRO = ah_msgformat("Riga documento evasa")
          else
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTCON),'DOC_DETT','MVDATEVA');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  MVDATEVA = this.w_OLTDTCON;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_MVSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Gestione documenti di esplosione collegati al documento in oggetto (Magazzino produzione)
            if not empty(nvl(this.w_MVRIFESP," "))
              * --- Esiste un documento di esplosione collegato alla testata, modifico la data
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERIAL="+cp_ToStrODBC(this.w_MVRIFESP)+"";
                     ,"_Curs_DOC_DETT")
              else
                select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC from (i_cTable);
                 where MVSERIAL=this.w_MVRIFESP;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                if _Curs_DOC_DETT.MVFLEVAS<>"S"
                  * --- && and empty(nvl(DOC_DETT->MVFLCASC,' '))
                  * --- Write into DOC_DETT
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OLTDTCON),'DOC_DETT','MVDATEVA');
                        +i_ccchkf ;
                    +" where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_MVRIFESP);
                        +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DOC_DETT.CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        MVDATEVA = this.w_OLTDTCON;
                        &i_ccchkf. ;
                     where;
                        MVSERIAL = this.w_MVRIFESP;
                        and CPROWNUM = _Curs_DOC_DETT.CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
            endif
            if not empty(nvl(this.w_MVRIFESC," "))
              * --- Esiste un documento di esplosione collegato al dettaglio, lo ricreo perch� i componenti validi alla nuova data potrebbero essere diversi
              *     da quelli originari
              * --- Select from gsmr_bex
              do vq_exec with 'gsmr_bex',this,'_Curs_gsmr_bex','',.f.,.t.
              if used('_Curs_gsmr_bex')
                select _Curs_gsmr_bex
                locate for 1=1
                do while not(eof())
                if _Curs_gsmr_bex.MVFLEVAS<>"S"
                  * --- and empty(nvl(gsmr_bex->MVFLCASC,' '))
                  * --- Elimino il documento
                  this.w_DOCEVA = this.w_MVRIFESC
                  this.w_OK = .t.
                  this.Page_6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if this.w_OK
                    * --- Generazione ordini con distinta base
                    this.w_CAUPFI = space(5)
                    this.w_MVCODMAG = this.w_CODMAG
                    * --- Read from TIP_DOCU
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "TDCAUCOD,TDFLARCO,TDTIPIMB,TDVALCOM,TDCOSEPL,TDFLEXPL,TDEXPAUT"+;
                        " from "+i_cTable+" TIP_DOCU where ";
                            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        TDCAUCOD,TDFLARCO,TDTIPIMB,TDVALCOM,TDCOSEPL,TDFLEXPL,TDEXPAUT;
                        from (i_cTable) where;
                            TDTIPDOC = this.w_MVTIPDOC;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_CAUCOD = NVL(cp_ToDate(_read_.TDCAUCOD),cp_NullValue(_read_.TDCAUCOD))
                      this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
                      this.w_MVTIPIMB = NVL(cp_ToDate(_read_.TDTIPIMB),cp_NullValue(_read_.TDTIPIMB))
                      this.w_VALCOM = NVL(cp_ToDate(_read_.TDVALCOM),cp_NullValue(_read_.TDVALCOM))
                      this.w_TDCOSEPL = NVL(cp_ToDate(_read_.TDCOSEPL),cp_NullValue(_read_.TDCOSEPL))
                      this.w_TDFLEXPL = NVL(cp_ToDate(_read_.TDFLEXPL),cp_NullValue(_read_.TDFLEXPL))
                      this.w_TDEXPAUT = NVL(cp_ToDate(_read_.TDEXPAUT),cp_NullValue(_read_.TDEXPAUT))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if not(Empty(this.w_MVSERIAL))
                      if this.w_FLARCO="S" and NOT EMPTY(this.w_CAUCOD)
                        do GSAR_BEA with this
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                      endif
                    endif
                  endif
                endif
                  select _Curs_gsmr_bex
                  continue
                enddo
                use
              endif
            endif
          endif
        case this.w_MRTIPOMS="CA"
          if this.w_FLEVAS<>"S"
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
              +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  MVQTASAL = 0;
                  ,MVFLEVAS = "S";
                  ,MVEFFEVA = i_DATSYS;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_MVSERIAL;
                  and CPROWNUM = this.w_CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if not empty(nvl(this.w_CODODL,""))
              * --- Write into ODL_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
                +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
                +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       )
              else
                update (i_cTable) set;
                    OLTSTATO = "F";
                    ,OLTQTSAL = 0;
                    ,OLTFLEVA = "S";
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','-this.TmpN1',-this.TmpN1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','-this.TmpN1',-this.TmpN1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','-this.TmpN1',-this.TmpN1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_CODART);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTOPER = &i_cOp1.;
                  ,SLQTIPER = &i_cOp2.;
                  ,SLQTRPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_CODART;
                  and SLCODMAG = this.w_CODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Gestione documenti di esplosione collegati al documento in oggetto (Magazzino produzione)
          if not empty(nvl(this.w_MVRIFESP," "))
            * --- Esiste un documento di esplosione collegato alla testata, lo evado
            * --- Select from DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC  from "+i_cTable+" DOC_DETT ";
                  +" where MVSERIAL="+cp_ToStrODBC(this.w_MVRIFESP)+"";
                   ,"_Curs_DOC_DETT")
            else
              select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC from (i_cTable);
               where MVSERIAL=this.w_MVRIFESP;
                into cursor _Curs_DOC_DETT
            endif
            if used('_Curs_DOC_DETT')
              select _Curs_DOC_DETT
              locate for 1=1
              do while not(eof())
              if _Curs_DOC_DETT.MVFLEVAS<>"S"
                * --- && and empty(nvl(DOC_DETT->MVFLCASC,' '))
                * --- Write into DOC_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
                  +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVRIFESP);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DOC_DETT.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      MVQTASAL = 0;
                      ,MVFLEVAS = "S";
                      ,MVEFFEVA = i_DATSYS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVRIFESP;
                      and CPROWNUM = _Curs_DOC_DETT.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(_Curs_DOC_DETT.MVFLORDI,'SLQTOPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(_Curs_DOC_DETT.MVFLIMPE,'SLQTIPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(_Curs_DOC_DETT.MVFLRISE,'SLQTRPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(_Curs_DOC_DETT.MVCODART);
                      +" and SLCODMAG = "+cp_ToStrODBC(_Curs_DOC_DETT.MVCODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = _Curs_DOC_DETT.MVCODART;
                      and SLCODMAG = _Curs_DOC_DETT.MVCODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
                select _Curs_DOC_DETT
                continue
              enddo
              use
            endif
          endif
          if not empty(nvl(this.w_MVRIFESC," "))
            * --- Esiste un documento di esplosione collegato al dettaglio, lo evado
            * --- Select from DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC  from "+i_cTable+" DOC_DETT ";
                  +" where MVSERIAL="+cp_ToStrODBC(this.w_MVRIFESC)+"";
                   ,"_Curs_DOC_DETT")
            else
              select MVSERIAL,CPROWNUM,MVFLORDI,MVFLIMPE,MVFLRISE,MVCODART,MVCODMAG,MVQTASAL,MVFLEVAS,MVFLCASC from (i_cTable);
               where MVSERIAL=this.w_MVRIFESC;
                into cursor _Curs_DOC_DETT
            endif
            if used('_Curs_DOC_DETT')
              select _Curs_DOC_DETT
              locate for 1=1
              do while not(eof())
              if _Curs_DOC_DETT.MVFLEVAS<>"S"
                * --- &&  and empty(nvl(DOC_DETT->MVFLCASC,' '))
                * --- Write into DOC_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
                  +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
                  +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVRIFESC);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_DOC_DETT.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      MVQTASAL = 0;
                      ,MVFLEVAS = "S";
                      ,MVEFFEVA = i_DATSYS;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVRIFESC;
                      and CPROWNUM = _Curs_DOC_DETT.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(_Curs_DOC_DETT.MVFLORDI,'SLQTOPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(_Curs_DOC_DETT.MVFLIMPE,'SLQTIPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(_Curs_DOC_DETT.MVFLRISE,'SLQTRPER','-_Curs_DOC_DETT.MVQTASAL',-_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(_Curs_DOC_DETT.MVCODART);
                      +" and SLCODMAG = "+cp_ToStrODBC(_Curs_DOC_DETT.MVCODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = _Curs_DOC_DETT.MVCODART;
                      and SLCODMAG = _Curs_DOC_DETT.MVCODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
                select _Curs_DOC_DETT
                continue
              enddo
              use
            endif
          endif
      endcase
    endif
    if EMPTY(this.w_MREXERRO)
      * --- Write into MRP_MESS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MRP_MESS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("S"),'MRP_MESS','MRESEGUI');
        +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
        +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
               )
      else
        update (i_cTable) set;
            MRESEGUI = "S";
            ,MRDATAEX = i_DATSYS;
            ,MRUTEXEC = i_CODUTE;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_MRSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into MRP_MESS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MRP_MESS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MRP_MESS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MRP_MESS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRESEGUI ="+cp_NullLink(cp_ToStrODBC("E"),'MRP_MESS','MRESEGUI');
        +",MRDATAEX ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'MRP_MESS','MRDATAEX');
        +",MRUTEXEC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MRP_MESS','MRUTEXEC');
        +",MREXERRO ="+cp_NullLink(cp_ToStrODBC(this.w_MREXERRO),'MRP_MESS','MREXERRO');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
               )
      else
        update (i_cTable) set;
            MRESEGUI = "E";
            ,MRDATAEX = i_DATSYS;
            ,MRUTEXEC = i_CODUTE;
            ,MREXERRO = this.w_MREXERRO;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.w_MRSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Evasione Componenti/Prodotti Finiti
    *     Ripresa da pag.8 GSVE_BMK.BTCDEF
    * --- Aggiorna prima i saldi - Poi cancella dettaglio e testata
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVQTAEV1,MVIMPEVA,MVCODMAG,MVCODMAT,MVQTAMOV,MVQTAUM1,MVQTAIMP,MVQTAIM1,MVSERRIF,MVROWRIF,MVFLARIF,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVKEYSAL,MVNUMRIF,MVFLERIF  from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_DOCEVA)+"";
           ,"_Curs_DOC_DETT")
    else
      select MVQTAEV1,MVIMPEVA,MVCODMAG,MVCODMAT,MVQTAMOV,MVQTAUM1,MVQTAIMP,MVQTAIM1,MVSERRIF,MVROWRIF,MVFLARIF,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVKEYSAL,MVNUMRIF,MVFLERIF from (i_cTable);
       where MVSERIAL=this.w_DOCEVA;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      if NVL(_Curs_DOC_DETT.MVQTAEV1, 0)<>0 OR NVL(_Curs_DOC_DETT.MVIMPEVA, 0)<>0
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATDOC,MVNUMDOC,MVALFDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_DOCEVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATDOC,MVNUMDOC,MVALFDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_DOCEVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EVADAT = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_EVANUM = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_EVAALF = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MREXERRO = ah_Msgformat("Documento di evasione componenti n.%1 del %2 evaso da altri documenti", ALLTRIM(STR(this.w_EVANUM)) + IIF(EMPTY(this.w_EVAALF), " ", "/"+Alltrim(this.w_EVAALF)) , DTOC(this.w_EVADAT) )
        this.w_OK = .F.
      endif
      if this.w_OK
        this.w_EVAKEY = NVL(_Curs_DOC_DETT.MVKEYSAL, "")
        this.w_EVAMAG = NVL(_Curs_DOC_DETT.MVCODMAG,"")
        this.w_EVAMAT = NVL(_Curs_DOC_DETT.MVCODMAT,"")
        this.w_EVAQTA = NVL(_Curs_DOC_DETT.MVQTAMOV, 0)
        this.w_EVAQT1 = NVL(_Curs_DOC_DETT.MVQTAUM1, 0)
        this.w_EVQIMP = NVL(_Curs_DOC_DETT.MVQTAIMP, 0)
        this.w_EVQIM1 = NVL(_Curs_DOC_DETT.MVQTAIM1, 0)
        this.w_EVASER = NVL(_Curs_DOC_DETT.MVSERRIF,"")
        this.w_EVAROW = NVL(_Curs_DOC_DETT.MVROWRIF, 0)
        this.w_EVAFLA = NVL(_Curs_DOC_DETT.MVFLARIF, " ")
        this.w_EVAFLE = NVL(_Curs_DOC_DETT.MVFLERIF, " ")
        this.w_EVACAR = IIF(_Curs_DOC_DETT.MVFLCASC="-","+",IIF(_Curs_DOC_DETT.MVFLCASC="+","-"," "))
        this.w_EVAORD = IIF(_Curs_DOC_DETT.MVFLORDI="-","+",IIF(_Curs_DOC_DETT.MVFLORDI="+","-"," "))
        this.w_EVAIMP = IIF(_Curs_DOC_DETT.MVFLIMPE="-","+",IIF(_Curs_DOC_DETT.MVFLIMPE="+","-"," "))
        this.w_EVARIS = IIF(_Curs_DOC_DETT.MVFLRISE="-","+",IIF(_Curs_DOC_DETT.MVFLRISE="+","-"," "))
        this.w_EV2CAR = IIF(_Curs_DOC_DETT.MVF2CASC="-","+",IIF(_Curs_DOC_DETT.MVF2CASC="+","-"," "))
        this.w_EV2ORD = IIF(_Curs_DOC_DETT.MVF2ORDI="-","+",IIF(_Curs_DOC_DETT.MVF2ORDI="+","-"," "))
        this.w_EV2IMP = IIF(_Curs_DOC_DETT.MVF2IMPE="-","+",IIF(_Curs_DOC_DETT.MVF2IMPE="+","-"," "))
        this.w_EV2RIS = IIF(_Curs_DOC_DETT.MVF2RISE="-","+",IIF(_Curs_DOC_DETT.MVF2RISE="+","-"," "))
        this.w_EVNUMRIF = _Curs_DOC_DETT.MVNUMRIF
        if NOT EMPTY(this.w_EVAKEY) AND NOT EMPTY(this.w_EVAMAG)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_EVAORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_EVAIMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_EVAKEY;
                and SLCODMAG = this.w_EVAMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NOT EMPTY(this.w_EVAMAT) AND NOT EMPTY(this.w_EV2CAR+this.w_EV2ORD+this.w_EV2IMP+this.w_EV2RIS)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EV2ORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_EV2IMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
              +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                     )
            else
              update (i_cTable) set;
                  SLQTAPER = &i_cOp1.;
                  ,SLQTRPER = &i_cOp2.;
                  ,SLQTOPER = &i_cOp3.;
                  ,SLQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_EVAKEY;
                  and SLCODMAG = this.w_EVAMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        if NOT EMPTY(this.w_EVASER) AND this.w_EVAROW<>0 AND this.w_EVAFLA="+"
          * --- Storna Evasione Documento di Origine
          this.w_EVFLORDI = " "
          this.w_EVFLIMPE = " "
          this.w_EVFLRISE = " "
          this.w_EVEVAKEY2 = " "
          this.w_EVEVAMAG2 = " "
          this.w_EVOQTAEVA = 0
          this.w_EVOQTAEV1 = 0
          this.w_EVOQTAMOV = 0
          this.w_EVOQTAUM1 = 0
          this.w_EVOQTASAL = 0
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_EVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL;
              from (i_cTable) where;
                  MVSERIAL = this.w_EVASER;
                  and CPROWNUM = this.w_EVAROW;
                  and MVNUMRIF = this.w_EVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_EVFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
            this.w_EVFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
            this.w_EVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
            this.w_EVEVAKEY2 = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
            this.w_EVEVAMAG2 = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
            this.w_EVOQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
            this.w_EVOQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
            this.w_EVOQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
            this.w_EVOQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
            this.w_EVOQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_EVNQTAEVA = this.w_EVOQTAEVA - this.w_EVQIMP
          this.w_EVNQTAEV1 = this.w_EVOQTAEV1 - this.w_EVQIM1
          this.w_EVNQTASAL = IIF(this.w_EVNQTAEV1>this.w_EVOQTAUM1, 0, this.w_EVOQTAUM1-this.w_EVNQTAEV1)
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTAEVA),'DOC_DETT','MVQTAEVA');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTAEV1),'DOC_DETT','MVQTAEV1');
            +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_EVNQTASAL),'DOC_DETT','MVQTASAL');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_EVNUMRIF);
                   )
          else
            update (i_cTable) set;
                MVQTAEVA = this.w_EVNQTAEVA;
                ,MVQTAEV1 = this.w_EVNQTAEV1;
                ,MVQTASAL = this.w_EVNQTASAL;
                ,MVFLEVAS = " ";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_EVASER;
                and CPROWNUM = this.w_EVAROW;
                and MVNUMRIF = this.w_EVNUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NOT EMPTY(this.w_EVEVAKEY2) AND NOT EMPTY(this.w_EVEVAMAG2) AND NOT EMPTY(this.w_EVFLORDI+this.w_EVFLIMPE+this.w_EVFLRISE)
            this.w_EVNQTASAL = this.w_EVNQTASAL - this.w_EVOQTASAL
            * --- Se il documento di evasione componenti resta evaso non devo stornare solo la qtaeva 
            *     ma quanto rimasto aperto del documento ovvero w_NQTASAL
            this.w_EVQIM1 = IIF(this.w_EVAFLE="S",this.w_EVNQTASAL,this.w_EVQIM1)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SLQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EVFLORDI,'SLQTOPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EVFLIMPE,'SLQTIPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
              +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
              +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_EVEVAKEY2);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVEVAMAG2);
                     )
            else
              update (i_cTable) set;
                  SLQTRPER = &i_cOp1.;
                  ,SLQTOPER = &i_cOp2.;
                  ,SLQTIPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_EVEVAKEY2;
                  and SLCODMAG = this.w_EVEVAMAG2;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Aggiorna saldi lotti  prima di eliminare i documenti
    if g_MADV="S"
      GSMD_BRL (this, this.w_DOCEVA , "V" , "-" , ,.T.)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Elimina Documento
    if this.w_OK
      * --- Elimino il documento di evasione componenti
      GSAR_BED(this,this.w_DOCEVA, -20)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFESC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVRIFESC');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            MVRIFESC = SPACE(10);
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_MREXERRO = ah_Msgformat("Documento di evasione componenti n.%1 del %2; impossibile eliminare", ALLTRIM(STR(this.w_EVANUM)) + IIF(EMPTY(this.w_EVAALF), " ", "/"+Alltrim(this.w_EVAALF)) , DTOC(this.w_EVADAT) )
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='MPS_TFOR'
    this.cWorkTables[4]='MRP_MESS'
    this.cWorkTables[5]='ODL_DETT'
    this.cWorkTables[6]='ODL_MAST'
    this.cWorkTables[7]='SALDIART'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='DIC_PROD'
    this.cWorkTables[10]='DISTBASE'
    this.cWorkTables[11]='DOC_MAST'
    this.cWorkTables[12]='TIP_DOCU'
    this.cWorkTables[13]='SALDICOM'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_MRP_MESS')
      use in _Curs_MRP_MESS
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_gsmr_bex')
      use in _Curs_gsmr_bex
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
