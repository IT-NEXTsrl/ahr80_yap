* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bpa                                                        *
*              Parametri messaggi MRP                                          *
*                                                                              *
*      Author: ZUCCHETTI SPA (DB)                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-11                                                      *
* Last revis.: 2009-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bpa",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsmr_bpa as StdBatch
  * --- Local variables
  w_PARAM = space(10)
  * --- WorkFile variables
  PARA_MRP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i Parametri MRP sul database (da GSMR_KPA)
    * --- ODL Suggeriti
    * --- ODL Pianificati
    * --- ODL Lanciati
    * --- OCL Suggeriti
    * --- OCL Da ordinare
    * --- OCL Ordinati
    * --- ODA Suggeriti
    * --- ODA Da ordinare
    * --- ODA Ordinati
    do case
      case this.w_PARAM="READ"
        * --- Read from PARA_MRP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PARA_MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PARA_MRP_idx,2],.t.,this.PARA_MRP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" PARA_MRP where ";
                +"PMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                PMCODICE = this.oParentObject.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PMPERCON = NVL(cp_ToDate(_read_.PMPERCON),cp_NullValue(_read_.PMPERCON))
          this.oParentObject.w_PMODLSUG = NVL(cp_ToDate(_read_.PMODLSUG),cp_NullValue(_read_.PMODLSUG))
          this.oParentObject.w_PMSPOSTI = NVL(cp_ToDate(_read_.PMSPOSTI),cp_NullValue(_read_.PMSPOSTI))
          this.oParentObject.w_PMSPO_GG = NVL(cp_ToDate(_read_.PMSPO_GG),cp_NullValue(_read_.PMSPO_GG))
          this.oParentObject.w_PMSPOCOP = NVL(cp_ToDate(_read_.PMSPOCOP),cp_NullValue(_read_.PMSPOCOP))
          this.oParentObject.w_PMSANTIC = NVL(cp_ToDate(_read_.PMSANTIC),cp_NullValue(_read_.PMSANTIC))
          this.oParentObject.w_PMSANCOP = NVL(cp_ToDate(_read_.PMSANCOP),cp_NullValue(_read_.PMSANCOP))
          this.oParentObject.w_PMSANNUL = NVL(cp_ToDate(_read_.PMSANNUL),cp_NullValue(_read_.PMSANNUL))
          this.oParentObject.w_PMODLPIA = NVL(cp_ToDate(_read_.PMODLPIA),cp_NullValue(_read_.PMODLPIA))
          this.oParentObject.w_PMPPOSTI = NVL(cp_ToDate(_read_.PMPPOSTI),cp_NullValue(_read_.PMPPOSTI))
          this.oParentObject.w_PMPPO_GG = NVL(cp_ToDate(_read_.PMPPO_GG),cp_NullValue(_read_.PMPPO_GG))
          this.oParentObject.w_PMPPOCOP = NVL(cp_ToDate(_read_.PMPPOCOP),cp_NullValue(_read_.PMPPOCOP))
          this.oParentObject.w_PMPANTIC = NVL(cp_ToDate(_read_.PMPANTIC),cp_NullValue(_read_.PMPANTIC))
          this.oParentObject.w_PMPANCOP = NVL(cp_ToDate(_read_.PMPANCOP),cp_NullValue(_read_.PMPANCOP))
          this.oParentObject.w_PMPANNUL = NVL(cp_ToDate(_read_.PMPANNUL),cp_NullValue(_read_.PMPANNUL))
          this.oParentObject.w_PMODLLAN = NVL(cp_ToDate(_read_.PMODLLAN),cp_NullValue(_read_.PMODLLAN))
          this.oParentObject.w_PMLPOSTI = NVL(cp_ToDate(_read_.PMLPOSTI),cp_NullValue(_read_.PMLPOSTI))
          this.oParentObject.w_PMLPO_GG = NVL(cp_ToDate(_read_.PMLPO_GG),cp_NullValue(_read_.PMLPO_GG))
          this.oParentObject.w_PMLCHIUD = NVL(cp_ToDate(_read_.PMLCHIUD),cp_NullValue(_read_.PMLCHIUD))
          this.oParentObject.w_PMOCLSUG = NVL(cp_ToDate(_read_.PMOCLSUG),cp_NullValue(_read_.PMOCLSUG))
          this.oParentObject.w_PMCPOSTI = NVL(cp_ToDate(_read_.PMCPOSTI),cp_NullValue(_read_.PMCPOSTI))
          this.oParentObject.w_PMCPO_GG = NVL(cp_ToDate(_read_.PMCPO_GG),cp_NullValue(_read_.PMCPO_GG))
          this.oParentObject.w_PMCPOCOP = NVL(cp_ToDate(_read_.PMCPOCOP),cp_NullValue(_read_.PMCPOCOP))
          this.oParentObject.w_PMCANTIC = NVL(cp_ToDate(_read_.PMCANTIC),cp_NullValue(_read_.PMCANTIC))
          this.oParentObject.w_PMCANCOP = NVL(cp_ToDate(_read_.PMCANCOP),cp_NullValue(_read_.PMCANCOP))
          this.oParentObject.w_PMCANNUL = NVL(cp_ToDate(_read_.PMCANNUL),cp_NullValue(_read_.PMCANNUL))
          this.oParentObject.w_PMOCLPIA = NVL(cp_ToDate(_read_.PMOCLPIA),cp_NullValue(_read_.PMOCLPIA))
          this.oParentObject.w_PMIPOSTI = NVL(cp_ToDate(_read_.PMIPOSTI),cp_NullValue(_read_.PMIPOSTI))
          this.oParentObject.w_PMIPO_GG = NVL(cp_ToDate(_read_.PMIPO_GG),cp_NullValue(_read_.PMIPO_GG))
          this.oParentObject.w_PMIPOCOP = NVL(cp_ToDate(_read_.PMIPOCOP),cp_NullValue(_read_.PMIPOCOP))
          this.oParentObject.w_PMIANTIC = NVL(cp_ToDate(_read_.PMIANTIC),cp_NullValue(_read_.PMIANTIC))
          this.oParentObject.w_PMIANCOP = NVL(cp_ToDate(_read_.PMIANCOP),cp_NullValue(_read_.PMIANCOP))
          this.oParentObject.w_PMIANNUL = NVL(cp_ToDate(_read_.PMIANNUL),cp_NullValue(_read_.PMIANNUL))
          this.oParentObject.w_PMOCLORD = NVL(cp_ToDate(_read_.PMOCLORD),cp_NullValue(_read_.PMOCLORD))
          this.oParentObject.w_PMOPOSTI = NVL(cp_ToDate(_read_.PMOPOSTI),cp_NullValue(_read_.PMOPOSTI))
          this.oParentObject.w_PMOPO_GG = NVL(cp_ToDate(_read_.PMOPO_GG),cp_NullValue(_read_.PMOPO_GG))
          this.oParentObject.w_PMOPOCOP = NVL(cp_ToDate(_read_.PMOPOCOP),cp_NullValue(_read_.PMOPOCOP))
          this.oParentObject.w_PMOANTIC = NVL(cp_ToDate(_read_.PMOANTIC),cp_NullValue(_read_.PMOANTIC))
          this.oParentObject.w_PMOANCOP = NVL(cp_ToDate(_read_.PMOANCOP),cp_NullValue(_read_.PMOANCOP))
          this.oParentObject.w_PMOANNUL = NVL(cp_ToDate(_read_.PMOANNUL),cp_NullValue(_read_.PMOANNUL))
          this.oParentObject.w_PMODASUG = NVL(cp_ToDate(_read_.PMODASUG),cp_NullValue(_read_.PMODASUG))
          this.oParentObject.w_PMMPOSTI = NVL(cp_ToDate(_read_.PMMPOSTI),cp_NullValue(_read_.PMMPOSTI))
          this.oParentObject.w_PMMPO_GG = NVL(cp_ToDate(_read_.PMMPO_GG),cp_NullValue(_read_.PMMPO_GG))
          this.oParentObject.w_PMMPOCOP = NVL(cp_ToDate(_read_.PMMPOCOP),cp_NullValue(_read_.PMMPOCOP))
          this.oParentObject.w_PMMANTIC = NVL(cp_ToDate(_read_.PMMANTIC),cp_NullValue(_read_.PMMANTIC))
          this.oParentObject.w_PMMANCOP = NVL(cp_ToDate(_read_.PMMANCOP),cp_NullValue(_read_.PMMANCOP))
          this.oParentObject.w_PMMANNUL = NVL(cp_ToDate(_read_.PMMANNUL),cp_NullValue(_read_.PMMANNUL))
          this.oParentObject.w_PMODAPIA = NVL(cp_ToDate(_read_.PMODAPIA),cp_NullValue(_read_.PMODAPIA))
          this.oParentObject.w_PMDPOSTI = NVL(cp_ToDate(_read_.PMDPOSTI),cp_NullValue(_read_.PMDPOSTI))
          this.oParentObject.w_PMDPO_GG = NVL(cp_ToDate(_read_.PMDPO_GG),cp_NullValue(_read_.PMDPO_GG))
          this.oParentObject.w_PMDPOCOP = NVL(cp_ToDate(_read_.PMDPOCOP),cp_NullValue(_read_.PMDPOCOP))
          this.oParentObject.w_PMDANTIC = NVL(cp_ToDate(_read_.PMDANTIC),cp_NullValue(_read_.PMDANTIC))
          this.oParentObject.w_PMDANCOP = NVL(cp_ToDate(_read_.PMDANCOP),cp_NullValue(_read_.PMDANCOP))
          this.oParentObject.w_PMDANNUL = NVL(cp_ToDate(_read_.PMDANNUL),cp_NullValue(_read_.PMDANNUL))
          this.oParentObject.w_PMODAORD = NVL(cp_ToDate(_read_.PMODALAN),cp_NullValue(_read_.PMODALAN))
          this.oParentObject.w_PMRPOSTI = NVL(cp_ToDate(_read_.PMRPOSTI),cp_NullValue(_read_.PMRPOSTI))
          this.oParentObject.w_PMRPO_GG = NVL(cp_ToDate(_read_.PMRPO_GG),cp_NullValue(_read_.PMRPO_GG))
          this.oParentObject.w_PMRPOCOP = NVL(cp_ToDate(_read_.PMRPOCOP),cp_NullValue(_read_.PMRPOCOP))
          this.oParentObject.w_PMRANTIC = NVL(cp_ToDate(_read_.PMRANTIC),cp_NullValue(_read_.PMRANTIC))
          this.oParentObject.w_PMRANCOP = NVL(cp_ToDate(_read_.PMRANCOP),cp_NullValue(_read_.PMRANCOP))
          this.oParentObject.w_PMRANNUL = NVL(cp_ToDate(_read_.PMRANNUL),cp_NullValue(_read_.PMRANNUL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.w_PARAM="WRITE" OR this.w_PARAM="CHECK"
        * --- Verifica i dati inseriti
        * --- ODL Suggeriti
        if this.oParentObject.w_PMODLSUG<>"S"
          this.oParentObject.w_PMSPOSTI = "N"
          this.oParentObject.w_PMSANTIC = "N"
          this.oParentObject.w_PMSANNUL = "N"
        endif
        if this.oParentObject.w_PMSPOSTI<>"S"
          this.oParentObject.w_PMSPO_GG = 0
          this.oParentObject.w_PMSPOCOP = "S"
        endif
        if this.oParentObject.w_PMSANTIC<>"S"
          this.oParentObject.w_PMSANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMSPOSTI+this.oParentObject.w_PMSANTIC+this.oParentObject.w_PMSANNUL
          this.oParentObject.w_PMODLSUG = "N"
        endif
        * --- ODL Pianificati
        if this.oParentObject.w_PMODLPIA<>"S"
          this.oParentObject.w_PMPPOSTI = "N"
          this.oParentObject.w_PMPANTIC = "N"
          this.oParentObject.w_PMPANNUL = "N"
        endif
        if this.oParentObject.w_PMPPOSTI<>"S"
          this.oParentObject.w_PMPPO_GG = 0
          this.oParentObject.w_PMPPOCOP = "S"
        endif
        if this.oParentObject.w_PMPANTIC<>"S"
          this.oParentObject.w_PMPANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMPPOSTI+this.oParentObject.w_PMPANTIC+this.oParentObject.w_PMPANNUL
          this.oParentObject.w_PMODLPIA = "N"
        endif
        * --- ODL Lanciati
        if this.oParentObject.w_PMODLLAN<>"S"
          this.oParentObject.w_PMLPOSTI = "N"
          this.oParentObject.w_PMLCHIUD = "N"
        endif
        if this.oParentObject.w_PMLPOSTI<>"S"
          this.oParentObject.w_PMLPO_GG = 0
        endif
        if not "S" $ this.oParentObject.w_PMLPOSTI+this.oParentObject.w_PMLCHIUD
          this.oParentObject.w_PMODLLAN = "N"
        endif
        * --- OCL Suggeriti
        if this.oParentObject.w_PMOCLSUG<>"S"
          this.oParentObject.w_PMCPOSTI = "N"
          this.oParentObject.w_PMCANTIC = "N"
          this.oParentObject.w_PMCANNUL = "N"
        endif
        if this.oParentObject.w_PMCPOSTI<>"S"
          this.oParentObject.w_PMCPO_GG = 0
          this.oParentObject.w_PMCPOCOP = "S"
        endif
        if this.oParentObject.w_PMCANTIC<>"S"
          this.oParentObject.w_PMCANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMCPOSTI+this.oParentObject.w_PMCANTIC+this.oParentObject.w_PMCANNUL
          this.oParentObject.w_PMOCLSUG = "N"
        endif
        * --- OCL Da ordinare
        if this.oParentObject.w_PMOCLPIA<>"S"
          this.oParentObject.w_PMIPOSTI = "N"
          this.oParentObject.w_PMIANTIC = "N"
          this.oParentObject.w_PMIANNUL = "N"
        endif
        if this.oParentObject.w_PMIPOSTI<>"S"
          this.oParentObject.w_PMIPO_GG = 0
          this.oParentObject.w_PMIPOCOP = "S"
        endif
        if this.oParentObject.w_PMIANTIC<>"S"
          this.oParentObject.w_PMIANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMIPOSTI+this.oParentObject.w_PMIANTIC+this.oParentObject.w_PMIANNUL
          this.oParentObject.w_PMOCLPIA = "N"
        endif
        * --- OCL Ordinati
        if this.oParentObject.w_PMOCLORD<>"S"
          this.oParentObject.w_PMOPOSTI = "N"
          this.oParentObject.w_PMOANTIC = "N"
          this.oParentObject.w_PMOANNUL = "N"
        endif
        if this.oParentObject.w_PMOPOSTI<>"S"
          this.oParentObject.w_PMOPO_GG = 0
          this.oParentObject.w_PMOPOCOP = "S"
        endif
        if this.oParentObject.w_PMOANTIC<>"S"
          this.oParentObject.w_PMOANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMOPOSTI+this.oParentObject.w_PMOANTIC+this.oParentObject.w_PMOANNUL
          this.oParentObject.w_PMOCLORD = "N"
        endif
        * --- ODA Suggeriti
        if this.oParentObject.w_PMODASUG<>"S"
          this.oParentObject.w_PMMPOSTI = "N"
          this.oParentObject.w_PMMANTIC = "N"
          this.oParentObject.w_PMMANNUL = "N"
        endif
        if this.oParentObject.w_PMMPOSTI<>"S"
          this.oParentObject.w_PMMPO_GG = 0
          this.oParentObject.w_PMMPOCOP = "S"
        endif
        if this.oParentObject.w_PMMANTIC<>"S"
          this.oParentObject.w_PMMANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMMPOSTI+this.oParentObject.w_PMMANTIC+this.oParentObject.w_PMMANNUL
          this.oParentObject.w_PMODASUG = "N"
        endif
        * --- ODA Da ordinare
        if this.oParentObject.w_PMODAPIA<>"S"
          this.oParentObject.w_PMDPOSTI = "N"
          this.oParentObject.w_PMDANTIC = "N"
          this.oParentObject.w_PMDANNUL = "N"
        endif
        if this.oParentObject.w_PMDPOSTI<>"S"
          this.oParentObject.w_PMDPO_GG = 0
          this.oParentObject.w_PMDPOCOP = "S"
        endif
        if this.oParentObject.w_PMDANTIC<>"S"
          this.oParentObject.w_PMDANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMDPOSTI+this.oParentObject.w_PMDANTIC+this.oParentObject.w_PMDANNUL
          this.oParentObject.w_PMODAPIA = "N"
        endif
        * --- ODA Ordinati
        if this.oParentObject.w_PMODAORD<>"S"
          this.oParentObject.w_PMRPOSTI = "N"
          this.oParentObject.w_PMRANTIC = "N"
          this.oParentObject.w_PMRANNUL = "N"
        endif
        if this.oParentObject.w_PMRPOSTI<>"S"
          this.oParentObject.w_PMRPO_GG = 0
          this.oParentObject.w_PMRPOCOP = "S"
        endif
        if this.oParentObject.w_PMRANTIC<>"S"
          this.oParentObject.w_PMRANCOP = "S"
        endif
        if not "S" $ this.oParentObject.w_PMRPOSTI+this.oParentObject.w_PMRANTIC+this.oParentObject.w_PMRANNUL
          this.oParentObject.w_PMODAORD = "N"
        endif
        if this.w_PARAM="WRITE"
          * --- Try
          local bErr_0366F1B8
          bErr_0366F1B8=bTrsErr
          this.Try_0366F1B8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0366F1B8
          * --- End
          * --- Write into PARA_MRP
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PARA_MRP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PARA_MRP_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PARA_MRP_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PMPERCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPERCON),'PARA_MRP','PMPERCON');
            +",PMODLSUG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODLSUG),'PARA_MRP','PMODLSUG');
            +",PMSPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSPOSTI),'PARA_MRP','PMSPOSTI');
            +",PMSPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSPO_GG),'PARA_MRP','PMSPO_GG');
            +",PMSPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSPOCOP),'PARA_MRP','PMSPOCOP');
            +",PMSANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSANTIC),'PARA_MRP','PMSANTIC');
            +",PMSANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSANCOP),'PARA_MRP','PMSANCOP');
            +",PMSANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMSANNUL),'PARA_MRP','PMSANNUL');
            +",PMODLPIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODLPIA),'PARA_MRP','PMODLPIA');
            +",PMPPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPPOSTI),'PARA_MRP','PMPPOSTI');
            +",PMPPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPPO_GG),'PARA_MRP','PMPPO_GG');
            +",PMPPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPPOCOP),'PARA_MRP','PMPPOCOP');
            +",PMPANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPANTIC),'PARA_MRP','PMPANTIC');
            +",PMPANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPANCOP),'PARA_MRP','PMPANCOP');
            +",PMPANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMPANNUL),'PARA_MRP','PMPANNUL');
            +",PMODLLAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODLLAN),'PARA_MRP','PMODLLAN');
            +",PMLPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMLPOSTI),'PARA_MRP','PMLPOSTI');
            +",PMLPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMLPO_GG),'PARA_MRP','PMLPO_GG');
            +",PMLCHIUD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMLCHIUD),'PARA_MRP','PMLCHIUD');
            +",PMOCLSUG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOCLSUG),'PARA_MRP','PMOCLSUG');
            +",PMCPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCPOSTI),'PARA_MRP','PMCPOSTI');
            +",PMCPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCPO_GG),'PARA_MRP','PMCPO_GG');
            +",PMCPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCPOCOP),'PARA_MRP','PMCPOCOP');
            +",PMCANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCANTIC),'PARA_MRP','PMCANTIC');
            +",PMCANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCANCOP),'PARA_MRP','PMCANCOP');
            +",PMCANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMCANNUL),'PARA_MRP','PMCANNUL');
            +",PMOCLPIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOCLPIA),'PARA_MRP','PMOCLPIA');
            +",PMIPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIPOSTI),'PARA_MRP','PMIPOSTI');
            +",PMIPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIPO_GG),'PARA_MRP','PMIPO_GG');
            +",PMIPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIPOCOP),'PARA_MRP','PMIPOCOP');
            +",PMIANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIANTIC),'PARA_MRP','PMIANTIC');
            +",PMIANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIANCOP),'PARA_MRP','PMIANCOP');
            +",PMIANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMIANNUL),'PARA_MRP','PMIANNUL');
            +",PMOCLORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOCLORD),'PARA_MRP','PMOCLORD');
            +",PMOPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOPOSTI),'PARA_MRP','PMOPOSTI');
            +",PMOPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOPO_GG),'PARA_MRP','PMOPO_GG');
            +",PMOPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOPOCOP),'PARA_MRP','PMOPOCOP');
            +",PMOANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOANTIC),'PARA_MRP','PMOANTIC');
            +",PMOANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOANCOP),'PARA_MRP','PMOANCOP');
            +",PMOANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMOANNUL),'PARA_MRP','PMOANNUL');
            +",PMODASUG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODASUG),'PARA_MRP','PMODASUG');
            +",PMMPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMMPOSTI),'PARA_MRP','PMMPOSTI');
            +",PMMPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMMPOCOP),'PARA_MRP','PMMPOCOP');
            +",PMMANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMMANTIC),'PARA_MRP','PMMANTIC');
            +",PMMANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMMANCOP),'PARA_MRP','PMMANCOP');
            +",PMMANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMMANNUL),'PARA_MRP','PMMANNUL');
            +",PMODAPIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODAPIA),'PARA_MRP','PMODAPIA');
            +",PMDPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDPOSTI),'PARA_MRP','PMDPOSTI');
            +",PMDPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDPO_GG),'PARA_MRP','PMDPO_GG');
            +",PMDPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDPOCOP),'PARA_MRP','PMDPOCOP');
            +",PMDANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDANTIC),'PARA_MRP','PMDANTIC');
            +",PMDANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDANCOP),'PARA_MRP','PMDANCOP');
            +",PMDANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMDANNUL),'PARA_MRP','PMDANNUL');
            +",PMODALAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMODAORD),'PARA_MRP','PMODALAN');
            +",PMRPOSTI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRPOSTI),'PARA_MRP','PMRPOSTI');
            +",PMRPO_GG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRPO_GG),'PARA_MRP','PMRPO_GG');
            +",PMRPOCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRPOCOP),'PARA_MRP','PMRPOCOP');
            +",PMRANTIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRANTIC),'PARA_MRP','PMRANTIC');
            +",PMRANCOP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRANCOP),'PARA_MRP','PMRANCOP');
            +",PMRANNUL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PMRANNUL),'PARA_MRP','PMRANNUL');
                +i_ccchkf ;
            +" where ";
                +"PMCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                   )
          else
            update (i_cTable) set;
                PMPERCON = this.oParentObject.w_PMPERCON;
                ,PMODLSUG = this.oParentObject.w_PMODLSUG;
                ,PMSPOSTI = this.oParentObject.w_PMSPOSTI;
                ,PMSPO_GG = this.oParentObject.w_PMSPO_GG;
                ,PMSPOCOP = this.oParentObject.w_PMSPOCOP;
                ,PMSANTIC = this.oParentObject.w_PMSANTIC;
                ,PMSANCOP = this.oParentObject.w_PMSANCOP;
                ,PMSANNUL = this.oParentObject.w_PMSANNUL;
                ,PMODLPIA = this.oParentObject.w_PMODLPIA;
                ,PMPPOSTI = this.oParentObject.w_PMPPOSTI;
                ,PMPPO_GG = this.oParentObject.w_PMPPO_GG;
                ,PMPPOCOP = this.oParentObject.w_PMPPOCOP;
                ,PMPANTIC = this.oParentObject.w_PMPANTIC;
                ,PMPANCOP = this.oParentObject.w_PMPANCOP;
                ,PMPANNUL = this.oParentObject.w_PMPANNUL;
                ,PMODLLAN = this.oParentObject.w_PMODLLAN;
                ,PMLPOSTI = this.oParentObject.w_PMLPOSTI;
                ,PMLPO_GG = this.oParentObject.w_PMLPO_GG;
                ,PMLCHIUD = this.oParentObject.w_PMLCHIUD;
                ,PMOCLSUG = this.oParentObject.w_PMOCLSUG;
                ,PMCPOSTI = this.oParentObject.w_PMCPOSTI;
                ,PMCPO_GG = this.oParentObject.w_PMCPO_GG;
                ,PMCPOCOP = this.oParentObject.w_PMCPOCOP;
                ,PMCANTIC = this.oParentObject.w_PMCANTIC;
                ,PMCANCOP = this.oParentObject.w_PMCANCOP;
                ,PMCANNUL = this.oParentObject.w_PMCANNUL;
                ,PMOCLPIA = this.oParentObject.w_PMOCLPIA;
                ,PMIPOSTI = this.oParentObject.w_PMIPOSTI;
                ,PMIPO_GG = this.oParentObject.w_PMIPO_GG;
                ,PMIPOCOP = this.oParentObject.w_PMIPOCOP;
                ,PMIANTIC = this.oParentObject.w_PMIANTIC;
                ,PMIANCOP = this.oParentObject.w_PMIANCOP;
                ,PMIANNUL = this.oParentObject.w_PMIANNUL;
                ,PMOCLORD = this.oParentObject.w_PMOCLORD;
                ,PMOPOSTI = this.oParentObject.w_PMOPOSTI;
                ,PMOPO_GG = this.oParentObject.w_PMOPO_GG;
                ,PMOPOCOP = this.oParentObject.w_PMOPOCOP;
                ,PMOANTIC = this.oParentObject.w_PMOANTIC;
                ,PMOANCOP = this.oParentObject.w_PMOANCOP;
                ,PMOANNUL = this.oParentObject.w_PMOANNUL;
                ,PMODASUG = this.oParentObject.w_PMODASUG;
                ,PMMPOSTI = this.oParentObject.w_PMMPOSTI;
                ,PMMPOCOP = this.oParentObject.w_PMMPOCOP;
                ,PMMANTIC = this.oParentObject.w_PMMANTIC;
                ,PMMANCOP = this.oParentObject.w_PMMANCOP;
                ,PMMANNUL = this.oParentObject.w_PMMANNUL;
                ,PMODAPIA = this.oParentObject.w_PMODAPIA;
                ,PMDPOSTI = this.oParentObject.w_PMDPOSTI;
                ,PMDPO_GG = this.oParentObject.w_PMDPO_GG;
                ,PMDPOCOP = this.oParentObject.w_PMDPOCOP;
                ,PMDANTIC = this.oParentObject.w_PMDANTIC;
                ,PMDANCOP = this.oParentObject.w_PMDANCOP;
                ,PMDANNUL = this.oParentObject.w_PMDANNUL;
                ,PMODALAN = this.oParentObject.w_PMODAORD;
                ,PMRPOSTI = this.oParentObject.w_PMRPOSTI;
                ,PMRPO_GG = this.oParentObject.w_PMRPO_GG;
                ,PMRPOCOP = this.oParentObject.w_PMRPOCOP;
                ,PMRANTIC = this.oParentObject.w_PMRANTIC;
                ,PMRANCOP = this.oParentObject.w_PMRANCOP;
                ,PMRANNUL = this.oParentObject.w_PMRANNUL;
                &i_ccchkf. ;
             where;
                PMCODICE = this.oParentObject.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          AH_ErrorMsg("Aggiornamento completato",48)
          this.oParentObject.ecpQuit()
        endif
    endcase
  endproc
  proc Try_0366F1B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PARA_MRP
    i_nConn=i_TableProp[this.PARA_MRP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARA_MRP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PARA_MRP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PMCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODICE),'PARA_MRP','PMCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PMCODICE',this.oParentObject.w_CODICE)
      insert into (i_cTable) (PMCODICE &i_ccchkf. );
         values (;
           this.oParentObject.w_CODICE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PARA_MRP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
