* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_kpm                                                        *
*              Previsioni mensili                                              *
*                                                                              *
*      Author: TAM Software srl                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-04                                                      *
* Last revis.: 2012-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdb_kpm",oParentObject))

* --- Class definition
define class tgsdb_kpm as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 640
  Height = 283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-26"
  HelpContextID=118852503
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=45

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  DET_VARI_IDX = 0
  cPrg = "gsdb_kpm"
  cComment = "Previsioni mensili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_STATO = space(6)
  w_OBTEST = ctod('  /  /  ')
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_FLCOMM = space(1)
  w_PVUNIMIS = space(3)
  w_ANNO = 0
  w_MESE1 = 0
  w_MESE2 = 0
  w_MESE3 = 0
  w_MESE4 = 0
  w_MESE5 = 0
  w_MESE6 = 0
  w_MESE7 = 0
  w_MESE8 = 0
  w_MESE9 = 0
  w_MESE10 = 0
  w_MESE11 = 0
  w_MESE12 = 0
  w_MESE1H = .F.
  w_MESE2H = .F.
  w_MESE3H = .F.
  w_MESE4H = .F.
  w_MESE5H = .F.
  w_MESE6H = .F.
  w_MESE7H = .F.
  w_MESE8H = .F.
  w_MESE9H = .F.
  w_MESE10H = .F.
  w_MESE11H = .F.
  w_MESE12H = .F.
  w_EDITOK1 = .F.
  w_EDITOK = .F.
  w_MESE1O = 0
  w_MESE2O = 0
  w_MESE3O = 0
  w_MESE4O = 0
  w_MESE5O = 0
  w_MESE6O = 0
  w_MESE7O = 0
  w_MESE8O = 0
  w_MESE9O = 0
  w_MESE10O = 0
  w_MESE11O = 0
  w_MESE12O = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdb_kpmPag1","gsdb_kpm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODART_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='DET_VARI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDB_BPM(this,"A")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STATO=space(6)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_FLCOMM=space(1)
      .w_PVUNIMIS=space(3)
      .w_ANNO=0
      .w_MESE1=0
      .w_MESE2=0
      .w_MESE3=0
      .w_MESE4=0
      .w_MESE5=0
      .w_MESE6=0
      .w_MESE7=0
      .w_MESE8=0
      .w_MESE9=0
      .w_MESE10=0
      .w_MESE11=0
      .w_MESE12=0
      .w_MESE1H=.f.
      .w_MESE2H=.f.
      .w_MESE3H=.f.
      .w_MESE4H=.f.
      .w_MESE5H=.f.
      .w_MESE6H=.f.
      .w_MESE7H=.f.
      .w_MESE8H=.f.
      .w_MESE9H=.f.
      .w_MESE10H=.f.
      .w_MESE11H=.f.
      .w_MESE12H=.f.
      .w_EDITOK1=.f.
      .w_EDITOK=.f.
      .w_MESE1O=0
      .w_MESE2O=0
      .w_MESE3O=0
      .w_MESE4O=0
      .w_MESE5O=0
      .w_MESE6O=0
      .w_MESE7O=0
      .w_MESE8O=0
      .w_MESE9O=0
      .w_MESE10O=0
      .w_MESE11O=0
      .w_MESE12O=0
      .w_STATO=oParentObject.w_STATO
      .w_ANNO=oParentObject.w_ANNO
          .DoRTCalc(1,1,.f.)
        .w_OBTEST = i_DATSYS
        .w_CODART = iif(.w_STATO="Load",space(20),this.oParentObject .w_PAR_CODI)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODART))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
          .DoRTCalc(4,19,.f.)
        .w_MESE1H = .T.
        .w_MESE2H = .T.
        .w_MESE3H = .T.
        .w_MESE4H = .T.
        .w_MESE5H = .T.
        .w_MESE6H = .T.
        .w_MESE7H = .T.
        .w_MESE8H = .T.
        .w_MESE9H = .T.
        .w_MESE10H = .T.
        .w_MESE11H = .T.
        .w_MESE12H = .T.
        .w_EDITOK1 = .w_MESE1H or  .w_MESE2H or .w_MESE3H or .w_MESE4H or .w_MESE5H or .w_MESE6H or .w_MESE7H or .w_MESE8H
        .w_EDITOK = .w_MESE9H or  .w_MESE10H or .w_MESE11H or .w_MESE12H or .w_EDITOK1
    endwith
    this.DoRTCalc(34,45,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_STATO=.w_STATO
      .oParentObject.w_ANNO=.w_ANNO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(1,31,.t.)
            .w_EDITOK1 = .w_MESE1H or  .w_MESE2H or .w_MESE3H or .w_MESE4H or .w_MESE5H or .w_MESE6H or .w_MESE7H or .w_MESE8H
            .w_EDITOK = .w_MESE9H or  .w_MESE10H or .w_MESE11H or .w_MESE12H or .w_EDITOK1
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,45,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMESE1_1_10.enabled = this.oPgFrm.Page1.oPag.oMESE1_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMESE2_1_12.enabled = this.oPgFrm.Page1.oPag.oMESE2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oMESE3_1_19.enabled = this.oPgFrm.Page1.oPag.oMESE3_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMESE4_1_21.enabled = this.oPgFrm.Page1.oPag.oMESE4_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMESE5_1_23.enabled = this.oPgFrm.Page1.oPag.oMESE5_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMESE6_1_25.enabled = this.oPgFrm.Page1.oPag.oMESE6_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMESE7_1_27.enabled = this.oPgFrm.Page1.oPag.oMESE7_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMESE8_1_29.enabled = this.oPgFrm.Page1.oPag.oMESE8_1_29.mCond()
    this.oPgFrm.Page1.oPag.oMESE9_1_31.enabled = this.oPgFrm.Page1.oPag.oMESE9_1_31.mCond()
    this.oPgFrm.Page1.oPag.oMESE10_1_37.enabled = this.oPgFrm.Page1.oPag.oMESE10_1_37.mCond()
    this.oPgFrm.Page1.oPag.oMESE11_1_39.enabled = this.oPgFrm.Page1.oPag.oMESE11_1_39.mCond()
    this.oPgFrm.Page1.oPag.oMESE12_1_41.enabled = this.oPgFrm.Page1.oPag.oMESE12_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_4'),i_cWhere,'',"Articoli",'GSDB_KPM.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARFLCOMM,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_FLCOMM = NVL(_Link_.ARFLCOMM,space(1))
      this.w_PVUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_FLCOMM = space(1)
      this.w_PVUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMM<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_FLCOMM = space(1)
        this.w_PVUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODART_1_4.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_4.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_5.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_5.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oPVUNIMIS_1_7.value==this.w_PVUNIMIS)
      this.oPgFrm.Page1.oPag.oPVUNIMIS_1_7.value=this.w_PVUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE1_1_10.value==this.w_MESE1)
      this.oPgFrm.Page1.oPag.oMESE1_1_10.value=this.w_MESE1
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE2_1_12.value==this.w_MESE2)
      this.oPgFrm.Page1.oPag.oMESE2_1_12.value=this.w_MESE2
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE3_1_19.value==this.w_MESE3)
      this.oPgFrm.Page1.oPag.oMESE3_1_19.value=this.w_MESE3
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE4_1_21.value==this.w_MESE4)
      this.oPgFrm.Page1.oPag.oMESE4_1_21.value=this.w_MESE4
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE5_1_23.value==this.w_MESE5)
      this.oPgFrm.Page1.oPag.oMESE5_1_23.value=this.w_MESE5
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE6_1_25.value==this.w_MESE6)
      this.oPgFrm.Page1.oPag.oMESE6_1_25.value=this.w_MESE6
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE7_1_27.value==this.w_MESE7)
      this.oPgFrm.Page1.oPag.oMESE7_1_27.value=this.w_MESE7
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE8_1_29.value==this.w_MESE8)
      this.oPgFrm.Page1.oPag.oMESE8_1_29.value=this.w_MESE8
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE9_1_31.value==this.w_MESE9)
      this.oPgFrm.Page1.oPag.oMESE9_1_31.value=this.w_MESE9
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE10_1_37.value==this.w_MESE10)
      this.oPgFrm.Page1.oPag.oMESE10_1_37.value=this.w_MESE10
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE11_1_39.value==this.w_MESE11)
      this.oPgFrm.Page1.oPag.oMESE11_1_39.value=this.w_MESE11
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE12_1_41.value==this.w_MESE12)
      this.oPgFrm.Page1.oPag.oMESE12_1_41.value=this.w_MESE12
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_FLCOMM<>'S')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANNO)) or not(.w_ANNO>=1990 and .w_ANNO<=2099))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore compreso tra 1990 e 2099")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    return

enddefine

* --- Define pages as container
define class tgsdb_kpmPag1 as StdContainer
  Width  = 636
  height = 283
  stdWidth  = 636
  stdheight = 283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_1_4 as StdField with uid="KCENTTHDIV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 260366554,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=137, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSDB_KPM.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART_1_5 as StdField with uid="KGPOGQHOPS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260307658,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=289, Top=12, InputMask=replicate('X',40)

  add object oPVUNIMIS_1_7 as StdField with uid="QSYLQEIBUV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PVUNIMIS", cQueryName = "PVUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "U.M.",;
    HelpContextID = 150550345,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=289, Top=40, InputMask=replicate('X',3)

  add object oANNO_1_8 as StdField with uid="SANEXCALQT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore compreso tra 1990 e 2099",;
    ToolTipText = "Anno",;
    HelpContextID = 124370438,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=137, Top=40, cSayPict='"9999"', cGetPict='"9999"'

  func oANNO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>=1990 and .w_ANNO<=2099)
    endwith
    return bRes
  endfunc

  add object oMESE1_1_10 as StdField with uid="HQTJMHMBSI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MESE1", cQueryName = "MESE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Gennaio",;
    HelpContextID = 175113670,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=68, Top=107, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE1_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE1H)
    endwith
   endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="KPPRNTHAED",left=184, top=107, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE2_1_12 as StdField with uid="YSPLUJSMRI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MESE2", cQueryName = "MESE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Febbraio",;
    HelpContextID = 176162246,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=277, Top=107, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE2H)
    endwith
   endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="HXRSBZGPHB",left=393, top=107, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",2)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE3_1_19 as StdField with uid="UVFMQECGWH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MESE3", cQueryName = "MESE3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Marzo",;
    HelpContextID = 177210822,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=491, Top=107, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE3_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE3H)
    endwith
   endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="NZAQUBXJYM",left=607, top=107, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",3)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE4_1_21 as StdField with uid="CHRONEFTKS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MESE4", cQueryName = "MESE4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Aprile",;
    HelpContextID = 178259398,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=68, Top=139, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE4_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE4H)
    endwith
   endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="YLYTNNRPZO",left=184, top=139, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",4)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE5_1_23 as StdField with uid="AUWBLTXNEY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MESE5", cQueryName = "MESE5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Maggio",;
    HelpContextID = 179307974,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=277, Top=139, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE5_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE5H)
    endwith
   endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="RPEVSHUXLJ",left=393, top=139, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",5)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE6_1_25 as StdField with uid="PTNKKTSGAH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MESE6", cQueryName = "MESE6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giugno",;
    HelpContextID = 180356550,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=491, Top=139, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE6_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE6H)
    endwith
   endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="TCDVRFSEVX",left=607, top=139, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",6)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE7_1_27 as StdField with uid="LFZMHLSBAI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MESE7", cQueryName = "MESE7",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Luglio",;
    HelpContextID = 181405126,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=68, Top=171, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE7_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE7H)
    endwith
   endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="MWYZEGIBNP",left=184, top=171, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",7)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE8_1_29 as StdField with uid="HGRUDUCNFH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MESE8", cQueryName = "MESE8",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Agosto",;
    HelpContextID = 182453702,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=277, Top=171, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE8_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE8H)
    endwith
   endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="GHHOVGBVQU",left=393, top=171, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",8)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE9_1_31 as StdField with uid="NVQGFTNCSU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MESE9", cQueryName = "MESE9",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Settembre",;
    HelpContextID = 183502278,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=491, Top=171, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE9_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE9H)
    endwith
   endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="EZMVBIGBER",left=607, top=171, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",9)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE10_1_37 as StdField with uid="TQEOUYDBFT",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MESE10", cQueryName = "MESE10",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ottobre",;
    HelpContextID = 175113670,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=68, Top=203, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE10_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE10H)
    endwith
   endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="QWTZXPFVFS",left=184, top=203, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",10)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE11_1_39 as StdField with uid="NWCRZQRWZD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MESE11", cQueryName = "MESE11",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Novembre",;
    HelpContextID = 191890886,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=277, Top=203, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE11_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE11H)
    endwith
   endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="BYKDGCBJLP",left=393, top=203, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",11)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMESE12_1_41 as StdField with uid="VWNWSSPXML",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MESE12", cQueryName = "MESE12",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dicembre",;
    HelpContextID = 208668102,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=491, Top=203, cSayPict="v_pQ(12)", cGetPict="v_gQ(12)"

  func oMESE12_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MESE12H)
    endwith
   endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="NOQKJRBAIB",left=607, top=203, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per visualizzare i periodi settimanali";
    , HelpContextID = 119053526;
  , bGlobalFont=.t.

    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"S",12)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_46 as cp_runprogram with uid="YXEVVOAAMY",left=4, top=446, width=163,height=29,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSDB_BPM("I")',;
    cEvent = "Init,w_CODART Changed,w_ANNO Changed",;
    nPag=1;
    , HelpContextID = 240020762


  add object oBtn_1_61 as StdButton with uid="VQDZAIGZMR",left=530, top=230, width=48,height=45,;
    CpPicture="BMP\SAVE.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per memorizzare le previsioni di vendita";
    , HelpContextID = 58637034;
    , Caption='\<Memorizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      with this.Parent.oContained
        GSDB_BPM(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODART) and not empty(.w_ANNO) and .w_EDITOK)
      endwith
    endif
  endfunc


  add object oBtn_1_62 as StdButton with uid="VFOUIPYUGL",left=580, top=230, width=48,height=45,;
    CpPicture="BMP\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58637034;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="KPFZZGWMVI",Visible=.t., Left=25, Top=12,;
    Alignment=1, Width=110, Height=15,;
    Caption="Codice articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XBFFUFTGTZ",Visible=.t., Left=95, Top=40,;
    Alignment=1, Width=40, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="UZTTRZIALG",Visible=.t., Left=12, Top=107,;
    Alignment=1, Width=55, Height=15,;
    Caption="Gennaio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DEZOEKASOY",Visible=.t., Left=223, Top=107,;
    Alignment=1, Width=52, Height=15,;
    Caption="Febbraio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="SWDNPZLTUK",Visible=.t., Left=451, Top=107,;
    Alignment=1, Width=38, Height=15,;
    Caption="Marzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ZONOWARAJR",Visible=.t., Left=33, Top=139,;
    Alignment=1, Width=34, Height=15,;
    Caption="Aprile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="CSNILOTNGG",Visible=.t., Left=232, Top=139,;
    Alignment=1, Width=43, Height=15,;
    Caption="Maggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="MUASKPCBUA",Visible=.t., Left=446, Top=139,;
    Alignment=1, Width=43, Height=15,;
    Caption="Giugno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TRPCYVYPKA",Visible=.t., Left=24, Top=171,;
    Alignment=1, Width=43, Height=15,;
    Caption="Luglio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="JUXZLEWAIG",Visible=.t., Left=232, Top=171,;
    Alignment=1, Width=43, Height=15,;
    Caption="Agosto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VSWYMEHVVL",Visible=.t., Left=420, Top=171,;
    Alignment=1, Width=69, Height=15,;
    Caption="Settembre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="EXGJISEMDZ",Visible=.t., Left=19, Top=203,;
    Alignment=1, Width=48, Height=15,;
    Caption="Ottobre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="HGKCWIQNQG",Visible=.t., Left=210, Top=203,;
    Alignment=1, Width=65, Height=15,;
    Caption="Novembre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="XUHLQHYAZE",Visible=.t., Left=426, Top=203,;
    Alignment=1, Width=63, Height=15,;
    Caption="Dicembre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="PTFRQQFSIJ",Visible=.t., Left=265, Top=40,;
    Alignment=1, Width=21, Height=15,;
    Caption="UM:"  ;
  , bGlobalFont=.t.

  add object oBox_1_76 as StdBox with uid="ZPXDLWPCZB",left=8, top=93, width=617,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdb_kpm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
