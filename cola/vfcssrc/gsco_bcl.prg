* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bcl                                                        *
*              Chiusura ODL lanciati                                           *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_525]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-28                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pTIPGES
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bcl",oParentObject,m.pAzione,m.pTIPGES)
return(i_retval)

define class tgsco_bcl as StdBatch
  * --- Local variables
  pAzione = space(2)
  pTIPGES = space(1)
  w_CODODL = space(15)
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_SERDOCL = space(10)
  w_CPRDOCL = 0
  w_FLAORD = space(1)
  w_FLAIMP = space(4)
  w_FLARIS = space(4)
  w_QTASAL = 0
  w_MAGIMP = space(5)
  w_KEYIMP = space(20)
  w_QTAIMP = 0
  w_DISMAG = space(1)
  Padre = .NULL.
  NC = space(10)
  TmpC = space(100)
  w_nRecSel = 0
  w_nRecEla = 0
  w_LNumErr = 0
  w_LOggErr = space(20)
  w_LErrore = space(80)
  w_LTesMes = space(0)
  w_FLGORD = space(1)
  w_FLGIMP = space(1)
  w_QTAODL = 0
  w_OLTCOART = space(20)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_SALCOM = space(1)
  w_OLTCOMME = space(15)
  w_RECFAS = 0
  w_FASCOL = space(15)
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  DOC_DETT_idx=0
  MAGAZZIN_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  SALDIART_idx=0
  RIF_GODL_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di Chiusura ODL (da GSCO_KCL)
    * --- Dichiarazione Variabili LOCALI
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.Padre = this.oParentObject
    * --- Nome cursore collegato allo zoom
    if nvl(this.oParentObject.w_SELEZI,"")="Z"
      * --- Solo se si arriva dal Pegging di secondo Livello (GSMR_BPE)
      this.NC = "GSCOSBCL"
    else
      this.NC = this.Padre.w_ZoomSel.cCursor
    endif
    this.w_LNumErr = 0
    * --- Test parametro
    do case
      case this.pAzione = "INTESTAZIONI"
        this.Padre.cComment = cp_translate("Chiusura ordini")
        do case
          case this.pTIPGES = "I"
            this.Padre.cComment = cp_translate("Chiusura ODL lanciati")
            this.Padre.w_ZoomSel.cZoomFile = "GSCO_ZCI"
            this.Padre.w_ZoomSel.cCpQueryName = "..\COLA\EXE\QUERY\GSCO_ZCI"
          case this.pTIPGES = "L"
            this.Padre.cComment = cp_translate("Chiusura OCL ordinati")
            this.Padre.w_ZoomSel.cZoomFile = "GSCO_ZCL"
            this.Padre.w_ZoomSel.cCpQueryName = "..\COLA\EXE\QUERY\GSCO_ZCL"
          otherwise
            this.Padre.cComment = cp_translate("Chiusura ODA ordinati")
            this.Padre.w_ZoomSel.cZoomFile = "GSCO_ZCE"
            this.Padre.w_ZoomSel.cCpQueryName = "..\COLA\EXE\QUERY\GSCO_ZCE"
        endcase
        this.Padre.Caption = this.Padre.cComment
      case this.pAzione = "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "INTERROGA"
        * --- Visualizza Zoom Elendo ODP periodo
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione = "AG"
        * --- Controlla selezioni
        SELECT (this.NC)
        GO TOP
        COUNT FOR xChk=1 TO this.w_nRecSel
        if this.w_nRecSel>0
          * --- Crea il File delle Messaggistiche di Errore
          CREATE CURSOR MessErr (NUMERR N(10,0), OGGERR C(15), ERRORE C(80), TESMES M(10))
          this.w_nRecSel = 0
          this.w_nRecEla = 0
          * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
          this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
          * --- Legge cursore di selezione ...
          SELECT (this.NC)
          GO TOP
          SCAN FOR XCHK=1 AND NOT EMPTY(NVL(OLCODODL,""))
          * --- Legge dati di interesse ....
          this.w_CODODL = OLCODODL
          this.w_KEYSAL = NVL(OLTKEYSA, SPACE(20))
          this.w_CODMAG = NVL(OLTCOMAG, SPACE(5))
          this.w_OLTCOMME = NVL(OLTCOMME, SPACE(15))
          this.w_OLTCOART = NVL(OLTCOART, SPACE(20))
          this.w_QTAODL = NVL(OLTQTSAL, 0)
          this.w_FLGORD = NVL(OLTFLORD," ")
          this.w_FLGORD = IIF(this.w_FLGORD="+", "-", IIF(this.w_FLGORD="-", "+", " "))
          this.w_FLGIMP = NVL(OLTFLIMP, " ")
          this.w_FLGIMP = IIF(this.w_FLGIMP="+", "-", IIF(this.w_FLGIMP="-", "+", " "))
          * --- Azione ...
          this.w_nRecSel = this.w_nRecSel + 1
          * --- Try
          local bErr_041BAB20
          bErr_041BAB20=bTrsErr
          this.Try_041BAB20()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            * --- Incrementa numero errori
            this.w_LNumErr = this.w_LNumErr + 1
            this.w_LOggErr = this.w_CODODL
            do case
              case this.pTIPGES = "I"
                this.w_LErrore = ah_Msgformat("Chiusura ODL non possibile")
              case this.pTIPGES = "L"
                this.w_LErrore = ah_Msgformat("Chiusura OCL non possibile")
              otherwise
                this.w_LErrore = ah_Msgformat("Chiusura ODA non possibile")
            endcase
            this.w_LTesMes = "Message()= "+message()
            * --- Scrive LOG
            INSERT INTO MessErr (NUMERR, OGGERR, ERRORE, TESMES) VALUES ;
            (this.w_LNumErr, this.w_LOggErr, this.w_LErrore, this.w_LTesMes)
          endif
          bTrsErr=bTrsErr or bErr_041BAB20
          * --- End
          SELECT (this.NC)
          ENDSCAN
          this.w_oPart = this.w_oMess.AddMsgPartNL("Elaborazione terminata%0N.%1 records elaborati%0Su %2 records selezionati")
          this.w_oPart.AddParam(alltrim(str(this.w_nRecEla,5,0)))     
          this.w_oPart.AddParam(alltrim(str(this.w_nRecSel,5,0)))     
          if USED("MessErr") AND this.w_LNumErr>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("%0Si sono verificati errori (%1) durante l'elaborazione%0Desideri la stampa dell'elenco degli errori?")
            this.w_oPart.AddParam(alltrim(str(this.w_LNumErr,5,0)))     
            if this.w_oMess.ah_YesNo()
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("..\COLA\EXE\QUERY\GSCO_SER.FRX", " ", this)
            endif
          else
            this.w_oMess.ah_ErrorMsg()
          endif
          * --- Chiusura Cursori
          if USED("MessErr")
            SELECT MessErr
            USE
          endif
          if USED("__TMP__")
            SELECT __TMP__
            USE
          endif
          * --- Riesegue Interrograzione
          this.Padre.NotifyEvent("Interroga")     
          this.oParentObject.w_SELEZI = "D"
        else
          ah_ErrorMsg("Non sono stati selezionati elementi da elaborare","!","")
        endif
    endcase
  endproc
  proc Try_041BAB20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do case
      case this.pTIPGES = "I"
        * --- CHIUSURA ORDINI DI LAVORAZIONE
        * --- Aggiorna ODL
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "F";
              ,OLTQTSAL = 0;
              ,OLTFLEVA = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura ODL_MAST'
          return
        endif
        if NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG) AND this.w_QTAODL<>0
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura SALDIART (1)'
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SCQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SCQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (1)'
              return
            endif
          endif
        endif
        * --- Chiude eventuale impegnato dettaglio ODL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+"";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_CODODL;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          this.w_MAGIMP = NVL(_Curs_ODL_DETT.OLCODMAG, SPACE(5))
          this.w_KEYIMP = NVL(_Curs_ODL_DETT.OLKEYSAL, SPACE(20))
          this.w_QTAIMP = NVL(_Curs_ODL_DETT.OLQTASAL, 0)
          this.w_OLTCOART = NVL(_Curs_ODL_DETT.OLCODART, SPACE(20))
          this.w_DISMAG = " "
          if NOT EMPTY(this.w_MAGIMP) AND NOT EMPTY(this.w_KEYIMP) AND this.w_QTAIMP<>0
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDISMAG"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDISMAG;
                from (i_cTable) where;
                    MGCODMAG = this.w_MAGIMP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = 'Errore Lettura MAGAZZIN'
              return
            endif
            select (i_nOldArea)
            if this.w_DISMAG="S"
              * --- Aggiorna Saldi dei magazzini nettificabili
              this.w_FLAORD = NVL(_Curs_ODL_DETT.OLFLORDI, " ")
              this.w_FLAORD = IIF(this.w_FLAORD="+", "-", IIF(this.w_FLAORD="-", "+", " "))
              this.w_FLAIMP = NVL(_Curs_ODL_DETT.OLFLIMPE, " ")
              this.w_FLAIMP = IIF(this.w_FLAIMP="+", "-", IIF(this.w_FLAIMP="-", "+", " "))
              this.w_FLARIS = NVL(_Curs_ODL_DETT.OLFLRISE, " ")
              this.w_FLARIS = IIF(this.w_FLARIS="+", "-", IIF(this.w_FLARIS="-", "+", " "))
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    ,SLQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYIMP;
                    and SLCODMAG = this.w_MAGIMP;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore scrittura SALDIART (2)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OLTCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OLTCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      ,SCQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYIMP;
                      and SCCODMAG = this.w_MAGIMP;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa (2)'
                  return
                endif
              endif
            endif
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
      case this.pTIPGES $ "E-L"
        * --- CHIUSURA ORDINI DI C/LAVORO IN LAVORAZIONE
        * --- I riferimenti al documento ORDINE collegato sono w_SERDOCL e w_CPRDOCL
        * --- Aggiorna OCL (nota: non occorre aggiornare i saldi, perch� i saldi vengono gestiti dal DOCUMENTO ORDINE)
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "F";
              ,OLTQTSAL = 0;
              ,OLTFLEVA = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura ODL_MAST'
          return
        endif
        * --- Aggiorna Documento
        * --- Select from RIF_GODL
        i_nConn=i_TableProp[this.RIF_GODL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2],.t.,this.RIF_GODL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIF_GODL ";
              +" where PDCODODL="+cp_ToStrODBC(this.w_CODODL)+" AND PDROWODL=-1 AND PDTIPGEN='OR'";
               ,"_Curs_RIF_GODL")
        else
          select * from (i_cTable);
           where PDCODODL=this.w_CODODL AND PDROWODL=-1 AND PDTIPGEN="OR";
            into cursor _Curs_RIF_GODL
        endif
        if used('_Curs_RIF_GODL')
          select _Curs_RIF_GODL
          locate for 1=1
          do while not(eof())
          this.w_FLAORD = " "
          this.w_FLAIMP = " "
          this.w_FLARIS = " "
          this.w_QTASAL = 0
          this.w_SERDOCL = NVL(_Curs_RIF_GODL.PDSERDOC, SPACE(10))
          this.w_CPRDOCL = NVL(_Curs_RIF_GODL.PDROWDOC, 0)
          if NOT EMPTY(this.w_SERDOCL) AND this.w_CPRDOCL>0
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPRDOCL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVFLORDI,MVFLIMPE,MVFLRISE,MVQTASAL;
                from (i_cTable) where;
                    MVSERIAL = this.w_SERDOCL;
                    and CPROWNUM = this.w_CPRDOCL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLAORD = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
              this.w_FLAIMP = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
              this.w_FLARIS = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
              this.w_QTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTASAL');
              +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
              +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPRDOCL);
                     )
            else
              update (i_cTable) set;
                  MVQTASAL = 0;
                  ,MVFLEVAS = "S";
                  ,MVEFFEVA = i_DATSYS;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERDOCL;
                  and CPROWNUM = this.w_CPRDOCL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorna Saldi
            if NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG) AND this.w_QTASAL<>0
              this.w_FLAORD = IIF(this.w_FLAORD="+", "-", IIF(this.w_FLAORD="-", "+", " "))
              this.w_FLAIMP = IIF(this.w_FLAIMP="+", "-", IIF(this.w_FLAIMP="-", "+", " "))
              this.w_FLARIS = IIF(this.w_FLARIS="+", "-", IIF(this.w_FLARIS="-", "+", " "))
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    ,SLQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore scrittura SALDIART (1)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OLTCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OLTCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','this.w_QTASAL',this.w_QTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      ,SCQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa (1)'
                  return
                endif
              endif
            endif
          endif
            select _Curs_RIF_GODL
            continue
          enddo
          use
        endif
        if this.pTIPGES = "L"
          * --- Chiude eventuale impegnato dettaglio OCL
          * --- Select from ODL_DETT
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
                +" where OLCODODL="+cp_ToStrODBC(this.w_CODODL)+"";
                 ,"_Curs_ODL_DETT")
          else
            select * from (i_cTable);
             where OLCODODL=this.w_CODODL;
              into cursor _Curs_ODL_DETT
          endif
          if used('_Curs_ODL_DETT')
            select _Curs_ODL_DETT
            locate for 1=1
            do while not(eof())
            this.w_MAGIMP = NVL(_Curs_ODL_DETT.OLCODMAG, SPACE(5))
            this.w_KEYIMP = NVL(_Curs_ODL_DETT.OLKEYSAL, SPACE(20))
            this.w_QTAIMP = NVL(_Curs_ODL_DETT.OLQTASAL, 0)
            this.w_OLTCOART = NVL(_Curs_ODL_DETT.OLCODART, SPACE(20))
            this.w_DISMAG = " "
            if NOT EMPTY(this.w_MAGIMP) AND NOT EMPTY(this.w_KEYIMP) AND this.w_QTAIMP<>0
              * --- Read from MAGAZZIN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MGDISMAG"+;
                  " from "+i_cTable+" MAGAZZIN where ";
                      +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MGDISMAG;
                  from (i_cTable) where;
                      MGCODMAG = this.w_MAGIMP;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = 'Errore Lettura MAGAZZIN'
                return
              endif
              select (i_nOldArea)
              if this.w_DISMAG="S"
                * --- Aggiorna Saldi dei magazzini nettificabili
                this.w_FLAORD = NVL(_Curs_ODL_DETT.OLFLORDI, " ")
                this.w_FLAORD = IIF(this.w_FLAORD="+", "-", IIF(this.w_FLAORD="-", "+", " "))
                this.w_FLAIMP = NVL(_Curs_ODL_DETT.OLFLIMPE, " ")
                this.w_FLAIMP = IIF(this.w_FLAIMP="+", "-", IIF(this.w_FLAIMP="-", "+", " "))
                this.w_FLARIS = NVL(_Curs_ODL_DETT.OLFLRISE, " ")
                this.w_FLARIS = IIF(this.w_FLARIS="+", "-", IIF(this.w_FLARIS="-", "+", " "))
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_KEYIMP;
                      and SLCODMAG = this.w_MAGIMP;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore scrittura SALDIART (2)'
                  return
                endif
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARSALCOM"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARSALCOM;
                    from (i_cTable) where;
                        ARCODART = this.w_OLTCOART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_SALCOM="S"
                  if empty(nvl(this.w_OLTCOMME,""))
                    this.w_COMMAPPO = this.w_COMMDEFA
                  else
                    this.w_COMMAPPO = this.w_OLTCOMME
                  endif
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                    i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTOPER = &i_cOp1.;
                        ,SCQTIPER = &i_cOp2.;
                        ,SCQTRPER = &i_cOp3.;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_KEYIMP;
                        and SCCODMAG = this.w_MAGIMP;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa (2)'
                    return
                  endif
                endif
              endif
            endif
              select _Curs_ODL_DETT
              continue
            enddo
            use
          endif
        endif
    endcase
    if this.pTIPGES $ "I-L"
      * --- Aggiorna dettaglio ordini
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        update (i_cTable) set;
            OLFLEVAS = "S";
            ,OLQTASAL = 0;
            &i_ccchkf. ;
         where;
            OLCODODL = this.w_CODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore scrittura ODL_DETT'
        return
      endif
    endif
    if this.pTIPGES = "I"
      vq_exec("..\COLA\EXE\QUERY\GSCO1BGL.VQR",this,"FASCOL")
      this.w_RECFAS = reccount("FASCOL")
      if this.w_RECFAS>0
        * --- L'ODL che sto analizzando ha delle fasi
        Select FASCOL 
 GO TOP 
 SCAN
        this.w_FASCOL = FASCOL.OLCODODL
        this.w_KEYSAL = NVL(FASCOL.OLTKEYSA, SPACE(20))
        this.w_CODMAG = NVL(FASCOL.OLTCOMAG, SPACE(5))
        this.w_OLTCOMME = NVL(FASCOL.OLTCOMME, SPACE(15))
        this.w_OLTCOART = NVL(FASCOL.OLTKEYSA, SPACE(20))
        this.w_QTAODL = NVL(FASCOL.OLTQTSAL, 0)
        this.w_FLGORD = NVL(FASCOL.OLTFLORD," ")
        this.w_FLGORD = IIF(this.w_FLGORD="+", "-", IIF(this.w_FLGORD="-", "+", " "))
        this.w_FLGIMP = NVL(FASCOL.OLTFLIMP, " ")
        this.w_FLGIMP = IIF(this.w_FLGIMP="+", "-", IIF(this.w_FLGIMP="-", "+", " "))
        * --- CHIUSURA ORDINI DI LAVORAZIONE
        * --- Aggiorna ODL
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTSTATO');
          +",OLTQTSAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTSAL');
          +",OLTFLEVA ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_MAST','OLTFLEVA');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_FASCOL);
                 )
        else
          update (i_cTable) set;
              OLTSTATO = "F";
              ,OLTQTSAL = 0;
              ,OLTFLEVA = "S";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_FASCOL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura ODL_MAST'
          return
        endif
        if NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG) AND this.w_QTAODL<>0
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SLQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SLQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = &i_cOp1.;
                ,SLQTIPER = &i_cOp2.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore scrittura SALDIART (1)'
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_OLTCOART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_OLTCOMME,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_OLTCOMME
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLGORD,'SCQTOPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLGIMP,'SCQTIPER','this.w_QTAODL',this.w_QTAODL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = &i_cOp1.;
                  ,SCQTIPER = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (1)'
              return
            endif
          endif
        endif
        * --- Chiude eventuale impegnato dettaglio ODL
        * --- Select from ODL_DETT
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ODL_DETT ";
              +" where OLCODODL="+cp_ToStrODBC(this.w_FASCOL)+"";
               ,"_Curs_ODL_DETT")
        else
          select * from (i_cTable);
           where OLCODODL=this.w_FASCOL;
            into cursor _Curs_ODL_DETT
        endif
        if used('_Curs_ODL_DETT')
          select _Curs_ODL_DETT
          locate for 1=1
          do while not(eof())
          this.w_MAGIMP = NVL(_Curs_ODL_DETT.OLCODMAG, SPACE(5))
          this.w_KEYIMP = NVL(_Curs_ODL_DETT.OLKEYSAL, SPACE(20))
          this.w_QTAIMP = NVL(_Curs_ODL_DETT.OLQTASAL, 0)
          this.w_OLTCOART = NVL(_Curs_ODL_DETT.OLCODART, SPACE(20))
          this.w_DISMAG = " "
          if NOT EMPTY(this.w_MAGIMP) AND NOT EMPTY(this.w_KEYIMP) AND this.w_QTAIMP<>0
            * --- Read from MAGAZZIN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MGDISMAG"+;
                " from "+i_cTable+" MAGAZZIN where ";
                    +"MGCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MGDISMAG;
                from (i_cTable) where;
                    MGCODMAG = this.w_MAGIMP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DISMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = 'Errore Lettura MAGAZZIN'
              return
            endif
            select (i_nOldArea)
            if this.w_DISMAG="S"
              * --- Aggiorna Saldi dei magazzini nettificabili
              this.w_FLAORD = NVL(_Curs_ODL_DETT.OLFLORDI, " ")
              this.w_FLAORD = IIF(this.w_FLAORD="+", "-", IIF(this.w_FLAORD="-", "+", " "))
              this.w_FLAIMP = NVL(_Curs_ODL_DETT.OLFLIMPE, " ")
              this.w_FLAIMP = IIF(this.w_FLAIMP="+", "-", IIF(this.w_FLAIMP="-", "+", " "))
              this.w_FLARIS = NVL(_Curs_ODL_DETT.OLFLRISE, " ")
              this.w_FLARIS = IIF(this.w_FLARIS="+", "-", IIF(this.w_FLARIS="-", "+", " "))
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SLQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SLQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SLQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = &i_cOp1.;
                    ,SLQTIPER = &i_cOp2.;
                    ,SLQTRPER = &i_cOp3.;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYIMP;
                    and SLCODMAG = this.w_MAGIMP;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore scrittura SALDIART (2)'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_OLTCOART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_OLTCOART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_OLTCOMME,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_OLTCOMME
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLAORD,'SCQTOPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_FLAIMP,'SCQTIPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_FLARIS,'SCQTRPER','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                  +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                  +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYIMP);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGIMP);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = &i_cOp1.;
                      ,SCQTIPER = &i_cOp2.;
                      ,SCQTRPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYIMP;
                      and SCCODMAG = this.w_MAGIMP;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa (2)'
                  return
                endif
              endif
            endif
          endif
            select _Curs_ODL_DETT
            continue
          enddo
          use
        endif
        * --- Aggiorna dettaglio ordini
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'ODL_DETT','OLFLEVAS');
          +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTASAL');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_FASCOL);
                 )
        else
          update (i_cTable) set;
              OLFLEVAS = "S";
              ,OLQTASAL = 0;
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_FASCOL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura ODL_DETT'
          return
        endif
        ENDSCAN
        USE IN Select ("FASCOL")
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_nRecEla = this.w_nRecEla + 1
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione,pTIPGES)
    this.pAzione=pAzione
    this.pTIPGES=pTIPGES
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='ODL_DETT'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='RIF_GODL'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='SALDICOM'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_RIF_GODL')
      use in _Curs_RIF_GODL
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    if used('_Curs_ODL_DETT')
      use in _Curs_ODL_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pTIPGES"
endproc
