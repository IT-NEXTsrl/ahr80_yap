* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscl_brc                                                        *
*              Rivalorizzazione DDT C/Lavoro                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-09                                                      *
* Last revis.: 2015-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscl_brc",oParentObject)
return(i_retval)

define class tgscl_brc as StdBatch
  * --- Local variables
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVIMPNAZ = 0
  w_OLQTAEV1 = 0
  w_COSTO = 0
  w_CONTA = 0
  w_CONTADDT = 0
  w_ERRO = .f.
  w_MESS = space(10)
  w_SERDDT = space(10)
  w_ROWDDT = 0
  w_CODODL = space(15)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_ULTFAS = space(1)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  ODL_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rivalorizzazione DDT di conto lavoro (da GSCL_KRC)
    this.w_ERRO = .F.
    this.w_CONTA = 0
    this.w_MVSERIAL = SPACE(10)
    this.w_CPROWNUM = 0
    * --- Lancia Queries di Ricerca in funzione del Criterio di Elaborazione
    if this.oParentObject.w_CRIVAL="US"
      vq_exec("..\COLA\EXE\QUERY\GSCO1BRC.VQR",this,"ELABORA")
    else
      vq_exec("..\COLA\EXE\QUERY\GSCO_BRC.VQR",this,"ELABORA")
    endif
    if USED("ELABORA")
      SELECT ELABORA
      if RECCOUNT()>0
        * --- Try
        local bErr_039A8868
        bErr_039A8868=bTrsErr
        this.Try_039A8868()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante l'aggiornamento dei dati: operazione annullata","STOP","")
          this.w_ERRO = .T.
        endif
        bTrsErr=bTrsErr or bErr_039A8868
        * --- End
      endif
      SELECT ELABORA
      USE
    endif
    if this.w_ERRO=.F.
      if this.w_CONTA=0
        ah_ErrorMsg("Non ci sono dati da elaborare","!","")
      else
        ah_ErrorMsg("Sono stati rivalorizzati %1 DDT (%1 righe)","!","", alltrim(str(this.w_CONTADDT)), alltrim(str(this.w_CONTA)) )
      endif
    endif
  endproc
  proc Try_039A8868()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT ELABORA
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MVSERIAL,"")) AND NVL(CPROWNUM, 0)<>0
    if this.w_MVSERIAL<>MVSERIAL OR this.w_CPROWNUM<>CPROWNUM
      this.w_CONTADDT = this.w_CONTADDT + IIF(this.w_MVSERIAL<>MVSERIAL, 1, 0)
      if NOT EMPTY(this.w_MVSERIAL) AND this.w_CPROWNUM<>0
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT ELABORA
      this.w_MVSERIAL = MVSERIAL
      this.w_CPROWNUM = CPROWNUM
      this.w_MVIMPNAZ = NVL(MVIMPNAZ, 0)
    endif
    this.w_OLQTAEV1 = NVL(OLQTAEV1, 0)
    do case
      case this.oParentObject.w_CRIVAL="CS"
        * --- Costo a standard
        this.w_COSTO = NVL(DICOSSTA, 0)
      case this.oParentObject.w_CRIVAL="CM"
        * --- Costo medio ponderato
        this.w_COSTO = NVL(DICOSMPP, 0)
      case this.oParentObject.w_CRIVAL="CA"
        * --- Costo medio ponderato annuo
        this.w_COSTO = NVL(DICOSMPA, 0)
      case this.oParentObject.w_CRIVAL="UC"
        * --- Costo ultimo
        this.w_COSTO = NVL(DICOSULT, 0)
      case this.oParentObject.w_CRIVAL="US"
        * --- Ultimo Costo Standard (Articolo)
        this.w_COSTO = NVL(PRCOSSTA, 0)
    endcase
    this.w_MVIMPNAZ = this.w_MVIMPNAZ + this.w_OLQTAEV1 * this.w_COSTO
    SELECT ELABORA
    ENDSCAN
    * --- Aggiorna i dati sui documenti (l'ultimo della selezione)
    if NOT EMPTY(this.w_MVSERIAL) AND this.w_CPROWNUM<>0
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna i dati sui documenti
    this.w_MVIMPNAZ = cp_ROUND(this.w_MVIMPNAZ, g_PERPVL)
    * --- Imposta il valore di MVIMPNAZ e il flag MVFLRVCL (riga rivalorizzata)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +",MVFLRVCL ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRVCL');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          MVIMPNAZ = this.w_MVIMPNAZ;
          ,MVFLRVCL = "S";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in scrittura DOC_DETT (DDT)'
      return
    endif
    * --- Imposta il flag MVFLBLOC sui documenti che hanno evaso il DDT (non sar� pi� possibile modificarli)
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVSERIAL  from "+i_cTable+" DOC_DETT ";
          +" where MVSERRIF="+cp_ToStrODBC(this.w_MVSERIAL)+" AND MVROWRIF="+cp_ToStrODBC(this.w_CPROWNUM)+"";
           ,"_Curs_DOC_DETT")
    else
      select MVSERIAL from (i_cTable);
       where MVSERRIF=this.w_MVSERIAL AND MVROWRIF=this.w_CPROWNUM;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLBLOC ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLBLOC');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(_Curs_DOC_DETT.MVSERIAL);
               )
      else
        update (i_cTable) set;
            MVFLBLOC = "S";
            &i_ccchkf. ;
         where;
            MVSERIAL = _Curs_DOC_DETT.MVSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore in scrittura DOC_MAST (Fattura)'
        return
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    this.w_CONTA = this.w_CONTA + 1
    if g_PRFA="S" AND g_CICLILAV="S"
      * --- Sull'ultima fase devo aggiornare il codice del padre (Documento interno associato al DDT)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERRIF,MVROWRIF"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERRIF,MVROWRIF;
          from (i_cTable) where;
              MVSERIAL = this.w_MVSERIAL;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERRIF = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
        this.w_ROWRIF = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODODL"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODODL;
          from (i_cTable) where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODODL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTULFAS"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTULFAS;
          from (i_cTable) where;
              OLCODODL = this.w_CODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ULTFAS = NVL(cp_ToDate(_read_.OLTULFAS),cp_NullValue(_read_.OLTULFAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NVL(this.w_ULTFAS,"N")="S"
        this.w_SERDDT = space(10)
        this.w_ROWDDT = 0
        * --- Dovrebbe restituirmi una sola riga
        vq_exec("..\COLA\EXE\QUERY\GSCL_BRC.VQR",this,"SWITCH")
        if USED("SWITCH")
          SELECT SWITCH
          if RECCOUNT()>0
            SELECT SWITCH
            GO TOP
            SCAN FOR NOT EMPTY(NVL(MVSERIAL,"")) AND NVL(CPROWNUM, 0)<>0
            this.w_SERDDT = MVSERIAL
            this.w_ROWDDT = CPROWNUM
            ENDSCAN
          endif
          SELECT SWITCH
          USE
        endif
        if NOT EMPTY(NVL(this.w_SERDDT,"")) AND NVL(this.w_ROWDDT, 0)<>0
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
            +",MVFLRVCL ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRVCL');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDDT);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWDDT);
                   )
          else
            update (i_cTable) set;
                MVIMPNAZ = this.w_MVIMPNAZ;
                ,MVFLRVCL = "S";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERDDT;
                and CPROWNUM = this.w_ROWDDT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura DOC_DETT (DDT)'
            return
          endif
          this.w_CONTA = this.w_CONTA + 1
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='ODL_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
