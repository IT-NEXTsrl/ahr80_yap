* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_apu                                                        *
*              Parametri pianificazione utente                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-05                                                      *
* Last revis.: 2018-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_apu"))

* --- Class definition
define class tgsmr_apu as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 816
  Height = 597+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-15"
  HelpContextID=160000873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=125

  * --- Constant Properties
  PAR__MRP_IDX = 0
  CPUSERS_IDX = 0
  cpgroups_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  PARA_MRP_IDX = 0
  PAR_PROD_IDX = 0
  MARCHI_IDX = 0
  CAN_TIER_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "PAR__MRP"
  cKeySelect = "PUNUMKEY"
  cKeyWhere  = "PUNUMKEY=this.w_PUNUMKEY"
  cKeyWhereODBC = '"PUNUMKEY="+cp_ToStrODBC(this.w_PUNUMKEY)';

  cKeyWhereODBCqualified = '"PAR__MRP.PUNUMKEY="+cp_ToStrODBC(this.w_PUNUMKEY)';

  cPrg = "gsmr_apu"
  cComment = "Parametri pianificazione utente"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PUCODUTE = 0
  o_PUCODUTE = 0
  w_PUCODGRP = 0
  o_PUCODGRP = 0
  w_PUTIPPAR = space(1)
  w_PUUTEELA = 0
  w_DESUTE = space(20)
  w_PUNUMKEY = 0
  w_DESGRP = space(20)
  w_PUTIPDOC = space(0)
  w_PPCODICE = space(2)
  w_AACODICE = space(2)
  w_CRIFORN = space(1)
  w_MSGMRP = space(2)
  w_PMOCLORD = space(1)
  o_PMOCLORD = space(1)
  w_PMODLPIA = space(1)
  o_PMODLPIA = space(1)
  w_PMODLLAN = space(1)
  o_PMODLLAN = space(1)
  w_PMODAPIA = space(1)
  o_PMODAPIA = space(1)
  w_PMODALAN = space(1)
  o_PMODALAN = space(1)
  w_CRIFORM = space(1)
  w_PPCRIELA = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLCOMM = space(1)
  w_PUMODELA = space(1)
  o_PUMODELA = space(1)
  w_PUCHKDAT = space(1)
  w_PUMRPLOG = space(1)
  w_PUMSGRIP = space(1)
  w_PUMICOMP = space(1)
  o_PUMICOMP = space(1)
  w_PUMAGFOR = space(5)
  w_PUMRFODL = space(1)
  o_PUMRFODL = space(1)
  w_PUMAGFOC = space(5)
  w_PUMRFOCL = space(1)
  o_PUMRFOCL = space(1)
  w_PUMAGFOL = space(5)
  w_PUGENPDA = space(1)
  o_PUGENPDA = space(1)
  w_PUMRFODA = space(1)
  o_PUMRFODA = space(1)
  w_PUMAGFOA = space(5)
  w_PUGENODA = space(1)
  w_PUDISMAG = space(1)
  o_PUDISMAG = space(1)
  w_PUGIANEG = space(1)
  w_PUORDMPS = space(1)
  w_PUSTAORD = space(1)
  w_DESMFR = space(30)
  w_DESMOC = space(30)
  w_DESMOL = space(30)
  w_DESMOA = space(30)
  w_PUCRIFOR = space(1)
  w_PUPERPIA = space(1)
  w_PUCRIELA = space(1)
  o_PUCRIELA = space(1)
  w_PUPIAPUN = space(1)
  w_PULISMAG = space(0)
  w_PUELABID = space(1)
  w_TIPCON = space(1)
  w_PUNOTEEL = space(0)
  w_NOTMRP = space(0)
  w_PUCODINI = space(20)
  o_PUCODINI = space(20)
  w_PUCODFIN = space(20)
  o_PUCODFIN = space(20)
  w_PULLCINI = 0
  o_PULLCINI = 0
  w_PULLCFIN = 0
  o_PULLCFIN = 0
  w_PUFAMAIN = space(5)
  o_PUFAMAIN = space(5)
  w_PUFAMAFI = space(5)
  o_PUFAMAFI = space(5)
  w_PUGRUINI = space(5)
  o_PUGRUINI = space(5)
  w_PUGRUFIN = space(5)
  o_PUGRUFIN = space(5)
  w_PUCATINI = space(5)
  o_PUCATINI = space(5)
  w_PUCATFIN = space(5)
  o_PUCATFIN = space(5)
  w_PUMAGINI = space(5)
  o_PUMAGINI = space(5)
  w_PUMAGFIN = space(5)
  o_PUMAGFIN = space(5)
  w_PUMARINI = space(5)
  o_PUMARINI = space(5)
  w_PUMARFIN = space(5)
  o_PUMARFIN = space(5)
  w_PUPROFIN = space(2)
  o_PUPROFIN = space(2)
  w_PUSEMLAV = space(2)
  o_PUSEMLAV = space(2)
  w_PUMATPRI = space(2)
  o_PUMATPRI = space(2)
  w_STIPART = space(1)
  o_STIPART = space(1)
  w_ED1 = .F.
  o_ED1 = .F.
  w_ED = .F.
  o_ED = .F.
  w_PUELACAT = space(1)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMARI = space(35)
  w_DESMARF = space(35)
  w_PUCOMINI = space(15)
  o_PUCOMINI = space(15)
  w_PUCOMFIN = space(15)
  o_PUCOMFIN = space(15)
  w_PUCOMODL = space(1)
  w_PUNUMINI = 0
  o_PUNUMINI = 0
  w_PUSERIE1 = space(10)
  o_PUSERIE1 = space(10)
  w_PUNUMFIN = 0
  o_PUNUMFIN = 0
  w_PUSERIE2 = space(10)
  o_PUSERIE2 = space(10)
  w_PUDOCINI = ctod('  /  /  ')
  o_PUDOCINI = ctod('  /  /  ')
  w_PUDOCFIN = ctod('  /  /  ')
  o_PUDOCFIN = ctod('  /  /  ')
  w_PUINICLI = space(15)
  o_PUINICLI = space(15)
  w_PUFINCLI = space(15)
  o_PUFINCLI = space(15)
  w_PUINIELA = ctod('  /  /  ')
  o_PUINIELA = ctod('  /  /  ')
  w_PUFINELA = ctod('  /  /  ')
  o_PUFINELA = ctod('  /  /  ')
  w_PUORIODL = space(1)
  w_SELEZI = space(1)
  w_DESCLII = space(40)
  w_DESCLIF = space(40)
  w_DESCOMI = space(30)
  w_DESCOMF = space(30)
  w_PUSELIMP = space(1)
  o_PUSELIMP = space(1)
  w_DISMAGFO = space(1)
  w_CAUSALI = 0
  o_CAUSALI = 0
  w_STIPART = space(1)
  w_ED = .F.
  w_PPPERPIA = space(1)
  w_PPPIAPUN = space(1)
  w_PUORDPRD = space(1)
  w_PUIMPPRD = space(1)
  w_TIPDOCU = space(0)
  w_RET = 0
  w_PUDELORD = space(1)
  w_MAGAZZINI = 0
  w_LISTMAGA = space(0)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_nColXCHK = 0
  w_PUELPDAF = space(1)
  w_PUELPDAS = space(1)
  w_FLAROB = space(1)
  w_PUFLAROB = space(1)
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  w_SZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmr_apu
  w_KEYRIF=''
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR__MRP','gsmr_apu')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_apuPag1","gsmr_apu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modalit� di elaborazione")
      .Pages(1).HelpContextID = 254134621
      .Pages(2).addobject("oPag","tgsmr_apuPag2","gsmr_apu",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).HelpContextID = 126010916
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPUNUMKEY_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZOOMMAGA = this.oPgFrm.Pages(1).oPag.ZOOMMAGA
      this.w_LBLMAGA = this.oPgFrm.Pages(1).oPag.LBLMAGA
      this.w_SZOOM = this.oPgFrm.Pages(2).oPag.SZOOM
      DoDefault()
    proc Destroy()
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      this.w_SZOOM = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='cpgroups'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='MAGAZZIN'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='PARA_MRP'
    this.cWorkTables[10]='PAR_PROD'
    this.cWorkTables[11]='MARCHI'
    this.cWorkTables[12]='CAN_TIER'
    this.cWorkTables[13]='ART_ICOL'
    this.cWorkTables[14]='PAR__MRP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(14))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR__MRP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR__MRP_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PUNUMKEY = NVL(PUNUMKEY,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_1_33_joined
    link_1_33_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_12_joined
    link_2_12_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_18_joined
    link_2_18_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_54_joined
    link_2_54_joined=.f.
    local link_2_55_joined
    link_2_55_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR__MRP where PUNUMKEY=KeySet.PUNUMKEY
    *
    i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR__MRP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR__MRP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR__MRP '
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_33_joined=this.AddJoinedLink_1_33(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_12_joined=this.AddJoinedLink_2_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_18_joined=this.AddJoinedLink_2_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_54_joined=this.AddJoinedLink_2_54(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_55_joined=this.AddJoinedLink_2_55(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PUNUMKEY',this.w_PUNUMKEY  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESUTE = space(20)
        .w_DESGRP = space(20)
        .w_PPCODICE = "PP"
        .w_AACODICE = "AA"
        .w_CRIFORN = space(1)
        .w_PMOCLORD = space(1)
        .w_PMODLPIA = space(1)
        .w_PMODLLAN = space(1)
        .w_PMODAPIA = space(1)
        .w_PMODALAN = space(1)
        .w_CRIFORM = space(1)
        .w_PPCRIELA = space(1)
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLCOMM = space(1)
        .w_DESMFR = space(30)
        .w_DESMOC = space(30)
        .w_DESMOL = space(30)
        .w_DESMOA = space(30)
        .w_TIPCON = 'C'
        .w_DESMAGI = space(30)
        .w_DESMAGF = space(30)
        .w_DESINI = space(40)
        .w_DESFIN = space(40)
        .w_DESFAMAI = space(35)
        .w_DESGRUI = space(35)
        .w_DESCATI = space(35)
        .w_DESFAMAF = space(35)
        .w_DESGRUF = space(35)
        .w_DESCATF = space(35)
        .w_DESMARI = space(35)
        .w_DESMARF = space(35)
        .w_SELEZI = "S"
        .w_DESCLII = space(40)
        .w_DESCLIF = space(40)
        .w_DESCOMI = space(30)
        .w_DESCOMF = space(30)
        .w_DISMAGFO = space(1)
        .w_CAUSALI = 0
        .w_PPPERPIA = space(1)
        .w_PPPIAPUN = space(1)
        .w_TIPDOCU = space(0)
        .w_RET = 0
        .w_MAGAZZINI = 0
        .w_LISTMAGA = space(0)
        .w_SELEZM = "S"
        .w_nColXCHK = 0
        .w_FLAROB = space(1)
        .w_PUCODUTE = NVL(PUCODUTE,0)
          .link_1_1('Load')
        .w_PUCODGRP = NVL(PUCODGRP,0)
          .link_1_2('Load')
        .w_PUTIPPAR = NVL(PUTIPPAR,space(1))
        .w_PUUTEELA = NVL(PUUTEELA,0)
        .w_PUNUMKEY = NVL(PUNUMKEY,0)
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .w_PUTIPDOC = NVL(PUTIPDOC,space(0))
          .link_1_12('Load')
          .link_1_13('Load')
        .w_MSGMRP = "MR"
          .link_1_15('Load')
        .w_PUMODELA = NVL(PUMODELA,space(1))
        .w_PUCHKDAT = NVL(PUCHKDAT,space(1))
        .w_PUMRPLOG = NVL(PUMRPLOG,space(1))
        .w_PUMSGRIP = NVL(PUMSGRIP,space(1))
        .w_PUMICOMP = NVL(PUMICOMP,space(1))
        .w_PUMAGFOR = NVL(PUMAGFOR,space(5))
          if link_1_31_joined
            this.w_PUMAGFOR = NVL(MGCODMAG131,NVL(this.w_PUMAGFOR,space(5)))
            this.w_DESMFR = NVL(MGDESMAG131,space(30))
            this.w_DISMAGFO = NVL(MGDISMAG131,space(1))
          else
          .link_1_31('Load')
          endif
        .w_PUMRFODL = NVL(PUMRFODL,space(1))
        .w_PUMAGFOC = NVL(PUMAGFOC,space(5))
          if link_1_33_joined
            this.w_PUMAGFOC = NVL(MGCODMAG133,NVL(this.w_PUMAGFOC,space(5)))
            this.w_DESMOC = NVL(MGDESMAG133,space(30))
          else
          .link_1_33('Load')
          endif
        .w_PUMRFOCL = NVL(PUMRFOCL,space(1))
        .w_PUMAGFOL = NVL(PUMAGFOL,space(5))
          if link_1_35_joined
            this.w_PUMAGFOL = NVL(MGCODMAG135,NVL(this.w_PUMAGFOL,space(5)))
            this.w_DESMOL = NVL(MGDESMAG135,space(30))
          else
          .link_1_35('Load')
          endif
        .w_PUGENPDA = NVL(PUGENPDA,space(1))
        .w_PUMRFODA = NVL(PUMRFODA,space(1))
        .w_PUMAGFOA = NVL(PUMAGFOA,space(5))
          if link_1_38_joined
            this.w_PUMAGFOA = NVL(MGCODMAG138,NVL(this.w_PUMAGFOA,space(5)))
            this.w_DESMOA = NVL(MGDESMAG138,space(30))
          else
          .link_1_38('Load')
          endif
        .w_PUGENODA = NVL(PUGENODA,space(1))
        .w_PUDISMAG = NVL(PUDISMAG,space(1))
        .w_PUGIANEG = NVL(PUGIANEG,space(1))
        .w_PUORDMPS = NVL(PUORDMPS,space(1))
        .w_PUSTAORD = NVL(PUSTAORD,space(1))
        .w_PUCRIFOR = NVL(PUCRIFOR,space(1))
        .w_PUPERPIA = NVL(PUPERPIA,space(1))
        .w_PUCRIELA = NVL(PUCRIELA,space(1))
        .w_PUPIAPUN = NVL(PUPIAPUN,space(1))
        .w_PULISMAG = NVL(PULISMAG,space(0))
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_PUCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .w_PUELABID = NVL(PUELABID,space(1))
        .w_PUNOTEEL = NVL(PUNOTEEL,space(0))
        .w_NOTMRP = .w_PUNOTEEL
        .w_PUCODINI = NVL(PUCODINI,space(20))
          if link_2_6_joined
            this.w_PUCODINI = NVL(ARCODART206,NVL(this.w_PUCODINI,space(20)))
            this.w_DESINI = NVL(ARDESART206,space(40))
          else
          .link_2_6('Load')
          endif
        .w_PUCODFIN = NVL(PUCODFIN,space(20))
          if link_2_7_joined
            this.w_PUCODFIN = NVL(ARCODART207,NVL(this.w_PUCODFIN,space(20)))
            this.w_DESFIN = NVL(ARDESART207,space(40))
          else
          .link_2_7('Load')
          endif
        .w_PULLCINI = NVL(PULLCINI,0)
        .w_PULLCFIN = NVL(PULLCFIN,0)
        .w_PUFAMAIN = NVL(PUFAMAIN,space(5))
          if link_2_10_joined
            this.w_PUFAMAIN = NVL(FACODICE210,NVL(this.w_PUFAMAIN,space(5)))
            this.w_DESFAMAI = NVL(FADESCRI210,space(35))
          else
          .link_2_10('Load')
          endif
        .w_PUFAMAFI = NVL(PUFAMAFI,space(5))
          if link_2_11_joined
            this.w_PUFAMAFI = NVL(FACODICE211,NVL(this.w_PUFAMAFI,space(5)))
            this.w_DESFAMAF = NVL(FADESCRI211,space(35))
          else
          .link_2_11('Load')
          endif
        .w_PUGRUINI = NVL(PUGRUINI,space(5))
          if link_2_12_joined
            this.w_PUGRUINI = NVL(GMCODICE212,NVL(this.w_PUGRUINI,space(5)))
            this.w_DESGRUI = NVL(GMDESCRI212,space(35))
          else
          .link_2_12('Load')
          endif
        .w_PUGRUFIN = NVL(PUGRUFIN,space(5))
          if link_2_13_joined
            this.w_PUGRUFIN = NVL(GMCODICE213,NVL(this.w_PUGRUFIN,space(5)))
            this.w_DESGRUF = NVL(GMDESCRI213,space(35))
          else
          .link_2_13('Load')
          endif
        .w_PUCATINI = NVL(PUCATINI,space(5))
          if link_2_14_joined
            this.w_PUCATINI = NVL(OMCODICE214,NVL(this.w_PUCATINI,space(5)))
            this.w_DESCATI = NVL(OMDESCRI214,space(35))
          else
          .link_2_14('Load')
          endif
        .w_PUCATFIN = NVL(PUCATFIN,space(5))
          if link_2_15_joined
            this.w_PUCATFIN = NVL(OMCODICE215,NVL(this.w_PUCATFIN,space(5)))
            this.w_DESCATF = NVL(OMDESCRI215,space(35))
          else
          .link_2_15('Load')
          endif
        .w_PUMAGINI = NVL(PUMAGINI,space(5))
          if link_2_16_joined
            this.w_PUMAGINI = NVL(MGCODMAG216,NVL(this.w_PUMAGINI,space(5)))
            this.w_DESMAGI = NVL(MGDESMAG216,space(30))
          else
          .link_2_16('Load')
          endif
        .w_PUMAGFIN = NVL(PUMAGFIN,space(5))
          if link_2_17_joined
            this.w_PUMAGFIN = NVL(MGCODMAG217,NVL(this.w_PUMAGFIN,space(5)))
            this.w_DESMAGF = NVL(MGDESMAG217,space(30))
          else
          .link_2_17('Load')
          endif
        .w_PUMARINI = NVL(PUMARINI,space(5))
          if link_2_18_joined
            this.w_PUMARINI = NVL(MACODICE218,NVL(this.w_PUMARINI,space(5)))
            this.w_DESMARI = NVL(MADESCRI218,space(35))
          else
          .link_2_18('Load')
          endif
        .w_PUMARFIN = NVL(PUMARFIN,space(5))
          if link_2_19_joined
            this.w_PUMARFIN = NVL(MACODICE219,NVL(this.w_PUMARFIN,space(5)))
            this.w_DESMARF = NVL(MADESCRI219,space(35))
          else
          .link_2_19('Load')
          endif
        .w_PUPROFIN = NVL(PUPROFIN,space(2))
        .w_PUSEMLAV = NVL(PUSEMLAV,space(2))
        .w_PUMATPRI = NVL(PUMATPRI,space(2))
        .w_STIPART = iif((empty(.w_PUPROFIN) or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV) or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        .w_ED1 = empty(.w_PUCODINI) and empty (.w_PUCODFIN) and empty(.w_PUFAMAIN) and empty(.w_PUFAMAFI) and empty(.w_PUGRUINI) and empty(.w_PUGRUFIN) and empty(.w_PUCATINI) and empty(.w_PUCATFIN) and .w_PULLCINI=0 and .w_PULLCFIN=999 and empty(.w_PUMAGINI) and empty(.w_PUMAGFIN) and empty(.w_PUMARINI)and empty(.w_PUMARFIN) and empty(.w_PUCOMINI)and empty(.w_PUCOMFIN)and(.w_PUNUMINI=1 or empty(.w_PUNUMINI))and(.w_PUNUMFIN=999999999999999 or empty(.w_PUNUMFIN))and empty(.w_PUSERIE1)and empty(.w_PUSERIE2)and empty(.w_PUDOCINI)and empty(.w_PUDOCFIN) and empty(.w_PUINICLI)and empty(.w_PUFINCLI)and empty(.w_CAUSALI)and (.w_PUINIELA=ctod('01-01-1900') or empty(.w_PUINIELA)) and (.w_PUFINELA=ctod('31-12-2099') or empty(.w_PUFINELA)) and .w_CAUSALI=0 and .w_STIPART<>'N'
        .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
        .w_PUELACAT = NVL(PUELACAT,space(1))
        .w_PUCOMINI = NVL(PUCOMINI,space(15))
          if link_2_54_joined
            this.w_PUCOMINI = NVL(CNCODCAN254,NVL(this.w_PUCOMINI,space(15)))
            this.w_DESCOMI = NVL(CNDESCAN254,space(30))
          else
          .link_2_54('Load')
          endif
        .w_PUCOMFIN = NVL(PUCOMFIN,space(15))
          if link_2_55_joined
            this.w_PUCOMFIN = NVL(CNCODCAN255,NVL(this.w_PUCOMFIN,space(15)))
            this.w_DESCOMF = NVL(CNDESCAN255,space(30))
          else
          .link_2_55('Load')
          endif
        .w_PUCOMODL = NVL(PUCOMODL,space(1))
        .w_PUNUMINI = NVL(PUNUMINI,0)
        .w_PUSERIE1 = NVL(PUSERIE1,space(10))
        .w_PUNUMFIN = NVL(PUNUMFIN,0)
        .w_PUSERIE2 = NVL(PUSERIE2,space(10))
        .w_PUDOCINI = NVL(cp_ToDate(PUDOCINI),ctod("  /  /  "))
        .w_PUDOCFIN = NVL(cp_ToDate(PUDOCFIN),ctod("  /  /  "))
        .w_PUINICLI = NVL(PUINICLI,space(15))
          .link_2_63('Load')
        .w_PUFINCLI = NVL(PUFINCLI,space(15))
          .link_2_64('Load')
        .w_PUINIELA = NVL(cp_ToDate(PUINIELA),ctod("  /  /  "))
        .w_PUFINELA = NVL(cp_ToDate(PUFINELA),ctod("  /  /  "))
        .w_PUORIODL = NVL(PUORIODL,space(1))
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
        .w_PUSELIMP = NVL(PUSELIMP,space(1))
        .w_STIPART = iif((empty(.w_PUPROFIN)or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV)or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
        .w_PUORDPRD = NVL(PUORDPRD,space(1))
        .w_PUIMPPRD = NVL(PUIMPPRD,space(1))
        .w_PUDELORD = NVL(PUDELORD,space(1))
        .w_PUELPDAF = NVL(PUELPDAF,space(1))
        .w_PUELPDAS = NVL(PUELPDAS,space(1))
        .w_PUFLAROB = NVL(PUFLAROB,space(1))
        cp_LoadRecExtFlds(this,'PAR__MRP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PUCODUTE = 0
      .w_PUCODGRP = 0
      .w_PUTIPPAR = space(1)
      .w_PUUTEELA = 0
      .w_DESUTE = space(20)
      .w_PUNUMKEY = 0
      .w_DESGRP = space(20)
      .w_PUTIPDOC = space(0)
      .w_PPCODICE = space(2)
      .w_AACODICE = space(2)
      .w_CRIFORN = space(1)
      .w_MSGMRP = space(2)
      .w_PMOCLORD = space(1)
      .w_PMODLPIA = space(1)
      .w_PMODLLAN = space(1)
      .w_PMODAPIA = space(1)
      .w_PMODALAN = space(1)
      .w_CRIFORM = space(1)
      .w_PPCRIELA = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_FLCOMM = space(1)
      .w_PUMODELA = space(1)
      .w_PUCHKDAT = space(1)
      .w_PUMRPLOG = space(1)
      .w_PUMSGRIP = space(1)
      .w_PUMICOMP = space(1)
      .w_PUMAGFOR = space(5)
      .w_PUMRFODL = space(1)
      .w_PUMAGFOC = space(5)
      .w_PUMRFOCL = space(1)
      .w_PUMAGFOL = space(5)
      .w_PUGENPDA = space(1)
      .w_PUMRFODA = space(1)
      .w_PUMAGFOA = space(5)
      .w_PUGENODA = space(1)
      .w_PUDISMAG = space(1)
      .w_PUGIANEG = space(1)
      .w_PUORDMPS = space(1)
      .w_PUSTAORD = space(1)
      .w_DESMFR = space(30)
      .w_DESMOC = space(30)
      .w_DESMOL = space(30)
      .w_DESMOA = space(30)
      .w_PUCRIFOR = space(1)
      .w_PUPERPIA = space(1)
      .w_PUCRIELA = space(1)
      .w_PUPIAPUN = space(1)
      .w_PULISMAG = space(0)
      .w_PUELABID = space(1)
      .w_TIPCON = space(1)
      .w_PUNOTEEL = space(0)
      .w_NOTMRP = space(0)
      .w_PUCODINI = space(20)
      .w_PUCODFIN = space(20)
      .w_PULLCINI = 0
      .w_PULLCFIN = 0
      .w_PUFAMAIN = space(5)
      .w_PUFAMAFI = space(5)
      .w_PUGRUINI = space(5)
      .w_PUGRUFIN = space(5)
      .w_PUCATINI = space(5)
      .w_PUCATFIN = space(5)
      .w_PUMAGINI = space(5)
      .w_PUMAGFIN = space(5)
      .w_PUMARINI = space(5)
      .w_PUMARFIN = space(5)
      .w_PUPROFIN = space(2)
      .w_PUSEMLAV = space(2)
      .w_PUMATPRI = space(2)
      .w_STIPART = space(1)
      .w_ED1 = .f.
      .w_ED = .f.
      .w_PUELACAT = space(1)
      .w_DESMAGI = space(30)
      .w_DESMAGF = space(30)
      .w_DESINI = space(40)
      .w_DESFIN = space(40)
      .w_DESFAMAI = space(35)
      .w_DESGRUI = space(35)
      .w_DESCATI = space(35)
      .w_DESFAMAF = space(35)
      .w_DESGRUF = space(35)
      .w_DESCATF = space(35)
      .w_DESMARI = space(35)
      .w_DESMARF = space(35)
      .w_PUCOMINI = space(15)
      .w_PUCOMFIN = space(15)
      .w_PUCOMODL = space(1)
      .w_PUNUMINI = 0
      .w_PUSERIE1 = space(10)
      .w_PUNUMFIN = 0
      .w_PUSERIE2 = space(10)
      .w_PUDOCINI = ctod("  /  /  ")
      .w_PUDOCFIN = ctod("  /  /  ")
      .w_PUINICLI = space(15)
      .w_PUFINCLI = space(15)
      .w_PUINIELA = ctod("  /  /  ")
      .w_PUFINELA = ctod("  /  /  ")
      .w_PUORIODL = space(1)
      .w_SELEZI = space(1)
      .w_DESCLII = space(40)
      .w_DESCLIF = space(40)
      .w_DESCOMI = space(30)
      .w_DESCOMF = space(30)
      .w_PUSELIMP = space(1)
      .w_DISMAGFO = space(1)
      .w_CAUSALI = 0
      .w_STIPART = space(1)
      .w_ED = .f.
      .w_PPPERPIA = space(1)
      .w_PPPIAPUN = space(1)
      .w_PUORDPRD = space(1)
      .w_PUIMPPRD = space(1)
      .w_TIPDOCU = space(0)
      .w_RET = 0
      .w_PUDELORD = space(1)
      .w_MAGAZZINI = 0
      .w_LISTMAGA = space(0)
      .w_SELEZM = space(1)
      .w_nColXCHK = 0
      .w_PUELPDAF = space(1)
      .w_PUELPDAS = space(1)
      .w_FLAROB = space(1)
      .w_PUFLAROB = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_PUCODUTE))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_PUCODGRP))
          .link_1_2('Full')
          endif
        .w_PUTIPPAR = 'D'
        .w_PUUTEELA = i_codute
          .DoRTCalc(5,5,.f.)
        .w_PUNUMKEY = .w_PUCODUTE+(.w_PUCODGRP*10000)
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
          .DoRTCalc(7,8,.f.)
        .w_PPCODICE = "PP"
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_PPCODICE))
          .link_1_12('Full')
          endif
        .w_AACODICE = "AA"
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_AACODICE))
          .link_1_13('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_MSGMRP = "MR"
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_MSGMRP))
          .link_1_15('Full')
          endif
          .DoRTCalc(13,19,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(21,22,.f.)
        .w_PUMODELA = 'R'
        .w_PUCHKDAT = "S"
        .w_PUMRPLOG = g_ATTIVAMRPLOG
        .w_PUMSGRIP = iif("S" $ nvl(.w_PMOCLORD+.w_PMODLPIA+.w_PMODLLAN+.w_PMODAPIA+.w_PMODALAN," "), "S", "N")
        .w_PUMICOMP = IIF(.w_PUCRIELA='M' , 'T', 'M')
        .w_PUMAGFOR = SPACE(5)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_PUMAGFOR))
          .link_1_31('Full')
          endif
        .w_PUMRFODL = 'M'
        .w_PUMAGFOC = SPACE(5)
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_PUMAGFOC))
          .link_1_33('Full')
          endif
        .w_PUMRFOCL = 'M'
        .w_PUMAGFOL = SPACE(5)
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_PUMAGFOL))
          .link_1_35('Full')
          endif
        .w_PUGENPDA = 'N'
        .w_PUMRFODA = 'M'
        .w_PUMAGFOA = SPACE(5)
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_PUMAGFOA))
          .link_1_38('Full')
          endif
        .w_PUGENODA = 'N'
        .w_PUDISMAG = 'S'
        .w_PUGIANEG = iif(.w_PUDISMAG='S','S','N')
        .w_PUORDMPS = 'S'
        .w_PUSTAORD = 'S'
          .DoRTCalc(41,44,.f.)
        .w_PUCRIFOR = iif(g_MODA<>'S',.w_CRIFORN,.w_CRIFORM)
        .w_PUPERPIA = .w_PPPERPIA
        .w_PUCRIELA = .w_PPCRIELA
        .w_PUPIAPUN = .w_PPPIAPUN
        .w_PULISMAG = .w_PULISMAG
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_PUCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .w_PUELABID = iif(.w_ED, 'N',.w_PUELABID)
        .w_TIPCON = 'C'
        .w_PUNOTEEL = .w_NOTMRP
        .w_NOTMRP = .w_PUNOTEEL
        .w_PUCODINI = ' '
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_PUCODINI))
          .link_2_6('Full')
          endif
        .w_PUCODFIN = .w_PUCODINI
        .DoRTCalc(55,55,.f.)
          if not(empty(.w_PUCODFIN))
          .link_2_7('Full')
          endif
        .w_PULLCINI = 0
        .w_PULLCFIN = 999
        .w_PUFAMAIN = ' '
        .DoRTCalc(58,58,.f.)
          if not(empty(.w_PUFAMAIN))
          .link_2_10('Full')
          endif
        .w_PUFAMAFI = .w_PUFAMAIN
        .DoRTCalc(59,59,.f.)
          if not(empty(.w_PUFAMAFI))
          .link_2_11('Full')
          endif
        .w_PUGRUINI = ' '
        .DoRTCalc(60,60,.f.)
          if not(empty(.w_PUGRUINI))
          .link_2_12('Full')
          endif
        .w_PUGRUFIN = .w_PUGRUINI
        .DoRTCalc(61,61,.f.)
          if not(empty(.w_PUGRUFIN))
          .link_2_13('Full')
          endif
        .w_PUCATINI = ' '
        .DoRTCalc(62,62,.f.)
          if not(empty(.w_PUCATINI))
          .link_2_14('Full')
          endif
        .w_PUCATFIN = .w_PUCATINI
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_PUCATFIN))
          .link_2_15('Full')
          endif
        .w_PUMAGINI = ' '
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_PUMAGINI))
          .link_2_16('Full')
          endif
        .w_PUMAGFIN = .w_PUMAGINI
        .DoRTCalc(65,65,.f.)
          if not(empty(.w_PUMAGFIN))
          .link_2_17('Full')
          endif
        .w_PUMARINI = ' '
        .DoRTCalc(66,66,.f.)
          if not(empty(.w_PUMARINI))
          .link_2_18('Full')
          endif
        .w_PUMARFIN = .w_PUMARINI
        .DoRTCalc(67,67,.f.)
          if not(empty(.w_PUMARFIN))
          .link_2_19('Full')
          endif
        .w_PUPROFIN = 'PF'
        .w_PUSEMLAV = 'SE'
        .w_PUMATPRI = 'MP'
        .w_STIPART = iif((empty(.w_PUPROFIN) or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV) or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        .w_ED1 = empty(.w_PUCODINI) and empty (.w_PUCODFIN) and empty(.w_PUFAMAIN) and empty(.w_PUFAMAFI) and empty(.w_PUGRUINI) and empty(.w_PUGRUFIN) and empty(.w_PUCATINI) and empty(.w_PUCATFIN) and .w_PULLCINI=0 and .w_PULLCFIN=999 and empty(.w_PUMAGINI) and empty(.w_PUMAGFIN) and empty(.w_PUMARINI)and empty(.w_PUMARFIN) and empty(.w_PUCOMINI)and empty(.w_PUCOMFIN)and(.w_PUNUMINI=1 or empty(.w_PUNUMINI))and(.w_PUNUMFIN=999999999999999 or empty(.w_PUNUMFIN))and empty(.w_PUSERIE1)and empty(.w_PUSERIE2)and empty(.w_PUDOCINI)and empty(.w_PUDOCFIN) and empty(.w_PUINICLI)and empty(.w_PUFINCLI)and empty(.w_CAUSALI)and (.w_PUINIELA=ctod('01-01-1900') or empty(.w_PUINIELA)) and (.w_PUFINELA=ctod('31-12-2099') or empty(.w_PUFINELA)) and .w_CAUSALI=0 and .w_STIPART<>'N'
        .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
        .w_PUELACAT = iif(! .w_ED, 'N','S')
          .DoRTCalc(75,86,.f.)
        .w_PUCOMINI = space(15)
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_PUCOMINI))
          .link_2_54('Full')
          endif
        .w_PUCOMFIN = .w_PUCOMINI
        .DoRTCalc(88,88,.f.)
          if not(empty(.w_PUCOMFIN))
          .link_2_55('Full')
          endif
        .w_PUCOMODL = 'T'
        .w_PUNUMINI = 1
        .w_PUSERIE1 = ''
        .w_PUNUMFIN = 999999999999999
        .w_PUSERIE2 = ''
        .w_PUDOCINI = ctod('')
        .w_PUDOCFIN = .w_PUDOCINI
        .w_PUINICLI = ' '
        .DoRTCalc(96,96,.f.)
          if not(empty(.w_PUINICLI))
          .link_2_63('Full')
          endif
        .w_PUFINCLI = .w_PUINICLI
        .DoRTCalc(97,97,.f.)
          if not(empty(.w_PUFINCLI))
          .link_2_64('Full')
          endif
        .w_PUINIELA = i_INIDAT
        .w_PUFINELA = cp_CharToDate('31-12-2099')
        .w_PUORIODL = 'T'
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
        .w_SELEZI = "S"
          .DoRTCalc(102,105,.f.)
        .w_PUSELIMP = 'A'
          .DoRTCalc(107,107,.f.)
        .w_CAUSALI = 0
        .w_STIPART = iif((empty(.w_PUPROFIN)or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV)or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
          .DoRTCalc(111,112,.f.)
        .w_PUORDPRD = 'S'
        .w_PUIMPPRD = 'S'
          .DoRTCalc(115,116,.f.)
        .w_PUDELORD = IIF(.w_PUCRIELA='A', 'T', IIF(Empty(.w_PUDELORD), 'P', .w_PUDELORD))
        .w_MAGAZZINI = 0
          .DoRTCalc(119,119,.f.)
        .w_SELEZM = "S"
          .DoRTCalc(121,121,.f.)
        .w_PUELPDAF = 'F'
        .w_PUELPDAS = 'S'
          .DoRTCalc(124,124,.f.)
        .w_PUFLAROB = .w_FLAROB
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR__MRP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsmr_apu
    EndProc
    
    Proc SetEnabledZoom
         This.NotifyEvent("SetEnabledZoom")
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPUNUMKEY_1_7.enabled = !i_bVal
      .Page1.oPag.oPUCODUTE_1_1.enabled = i_bVal
      .Page1.oPag.oPUCODGRP_1_2.enabled = i_bVal
      .Page1.oPag.oPUTIPPAR_1_3.enabled = i_bVal
      .Page1.oPag.oPUMODELA_1_26.enabled_(i_bVal)
      .Page1.oPag.oPUCHKDAT_1_27.enabled = i_bVal
      .Page1.oPag.oPUMRPLOG_1_28.enabled = i_bVal
      .Page1.oPag.oPUMSGRIP_1_29.enabled = i_bVal
      .Page1.oPag.oPUMICOMP_1_30.enabled = i_bVal
      .Page1.oPag.oPUMAGFOR_1_31.enabled = i_bVal
      .Page1.oPag.oPUMRFODL_1_32.enabled = i_bVal
      .Page1.oPag.oPUMAGFOC_1_33.enabled = i_bVal
      .Page1.oPag.oPUMRFOCL_1_34.enabled = i_bVal
      .Page1.oPag.oPUMAGFOL_1_35.enabled = i_bVal
      .Page1.oPag.oPUGENPDA_1_36.enabled = i_bVal
      .Page1.oPag.oPUMRFODA_1_37.enabled = i_bVal
      .Page1.oPag.oPUMAGFOA_1_38.enabled = i_bVal
      .Page1.oPag.oPUGENODA_1_39.enabled = i_bVal
      .Page1.oPag.oPUDISMAG_1_40.enabled = i_bVal
      .Page1.oPag.oPUGIANEG_1_41.enabled = i_bVal
      .Page1.oPag.oPUORDMPS_1_42.enabled = i_bVal
      .Page1.oPag.oPUSTAORD_1_43.enabled = i_bVal
      .Page1.oPag.oPUCRIFOR_1_64.enabled = i_bVal
      .Page1.oPag.oPUPERPIA_1_65.enabled = i_bVal
      .Page1.oPag.oPUCRIELA_1_66.enabled = i_bVal
      .Page1.oPag.oPUPIAPUN_1_67.enabled = i_bVal
      .Page1.oPag.oPUELABID_1_72.enabled = i_bVal
      .Page2.oPag.oPUCODINI_2_6.enabled = i_bVal
      .Page2.oPag.oPUCODFIN_2_7.enabled = i_bVal
      .Page2.oPag.oPULLCINI_2_8.enabled = i_bVal
      .Page2.oPag.oPULLCFIN_2_9.enabled = i_bVal
      .Page2.oPag.oPUFAMAIN_2_10.enabled = i_bVal
      .Page2.oPag.oPUFAMAFI_2_11.enabled = i_bVal
      .Page2.oPag.oPUGRUINI_2_12.enabled = i_bVal
      .Page2.oPag.oPUGRUFIN_2_13.enabled = i_bVal
      .Page2.oPag.oPUCATINI_2_14.enabled = i_bVal
      .Page2.oPag.oPUCATFIN_2_15.enabled = i_bVal
      .Page2.oPag.oPUMAGINI_2_16.enabled = i_bVal
      .Page2.oPag.oPUMAGFIN_2_17.enabled = i_bVal
      .Page2.oPag.oPUMARINI_2_18.enabled = i_bVal
      .Page2.oPag.oPUMARFIN_2_19.enabled = i_bVal
      .Page2.oPag.oPUPROFIN_2_20.enabled = i_bVal
      .Page2.oPag.oPUSEMLAV_2_21.enabled = i_bVal
      .Page2.oPag.oPUMATPRI_2_22.enabled = i_bVal
      .Page2.oPag.oPUELACAT_2_26.enabled = i_bVal
      .Page2.oPag.oPUCOMINI_2_54.enabled = i_bVal
      .Page2.oPag.oPUCOMFIN_2_55.enabled = i_bVal
      .Page2.oPag.oPUCOMODL_2_56.enabled_(i_bVal)
      .Page2.oPag.oPUNUMINI_2_57.enabled = i_bVal
      .Page2.oPag.oPUSERIE1_2_58.enabled = i_bVal
      .Page2.oPag.oPUNUMFIN_2_59.enabled = i_bVal
      .Page2.oPag.oPUSERIE2_2_60.enabled = i_bVal
      .Page2.oPag.oPUDOCINI_2_61.enabled = i_bVal
      .Page2.oPag.oPUDOCFIN_2_62.enabled = i_bVal
      .Page2.oPag.oPUINICLI_2_63.enabled = i_bVal
      .Page2.oPag.oPUFINCLI_2_64.enabled = i_bVal
      .Page2.oPag.oPUINIELA_2_65.enabled = i_bVal
      .Page2.oPag.oPUFINELA_2_66.enabled = i_bVal
      .Page2.oPag.oPUORIODL_2_67.enabled_(i_bVal)
      .Page2.oPag.oSELEZI_2_69.enabled_(i_bVal)
      .Page2.oPag.oPUSELIMP_2_88.enabled = i_bVal
      .Page2.oPag.oPUORDPRD_2_94.enabled = i_bVal
      .Page2.oPag.oPUIMPPRD_2_95.enabled = i_bVal
      .Page1.oPag.oPUDELORD_1_89.enabled = i_bVal
      .Page1.oPag.oSELEZM_1_93.enabled_(i_bVal)
      .Page1.oPag.oPUELPDAF_1_104.enabled = i_bVal
      .Page1.oPag.oPUELPDAS_1_105.enabled = i_bVal
      .Page1.oPag.oPUFLAROB_1_107.enabled = i_bVal
      .Page1.oPag.oBtn_1_73.enabled = .Page1.oPag.oBtn_1_73.mCond()
      .Page2.oPag.oObj_2_1.enabled = i_bVal
      .Page2.oPag.oObj_2_2.enabled = i_bVal
      .Page2.oPag.oObj_2_3.enabled = i_bVal
      .Page2.oPag.oObj_2_4.enabled = i_bVal
      .Page1.oPag.ZOOMMAGA.enabled = i_bVal
      .Page1.oPag.LBLMAGA.enabled = i_bVal
      .Page2.oPag.SZOOM.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPUCODUTE_1_1.enabled = .t.
        .Page1.oPag.oPUCODGRP_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PAR__MRP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCODUTE,"PUCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCODGRP,"PUCODGRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUTIPPAR,"PUTIPPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUUTEELA,"PUUTEELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUNUMKEY,"PUNUMKEY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUTIPDOC,"PUTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMODELA,"PUMODELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCHKDAT,"PUCHKDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMRPLOG,"PUMRPLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMSGRIP,"PUMSGRIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMICOMP,"PUMICOMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGFOR,"PUMAGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMRFODL,"PUMRFODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGFOC,"PUMAGFOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMRFOCL,"PUMRFOCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGFOL,"PUMAGFOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUGENPDA,"PUGENPDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMRFODA,"PUMRFODA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGFOA,"PUMAGFOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUGENODA,"PUGENODA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDISMAG,"PUDISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUGIANEG,"PUGIANEG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUORDMPS,"PUORDMPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUSTAORD,"PUSTAORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCRIFOR,"PUCRIFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUPERPIA,"PUPERPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCRIELA,"PUCRIELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUPIAPUN,"PUPIAPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PULISMAG,"PULISMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUELABID,"PUELABID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUNOTEEL,"PUNOTEEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCODINI,"PUCODINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCODFIN,"PUCODFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PULLCINI,"PULLCINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PULLCFIN,"PULLCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFAMAIN,"PUFAMAIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFAMAFI,"PUFAMAFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUGRUINI,"PUGRUINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUGRUFIN,"PUGRUFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCATINI,"PUCATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCATFIN,"PUCATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGINI,"PUMAGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMAGFIN,"PUMAGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMARINI,"PUMARINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMARFIN,"PUMARFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUPROFIN,"PUPROFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUSEMLAV,"PUSEMLAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUMATPRI,"PUMATPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUELACAT,"PUELACAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCOMINI,"PUCOMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCOMFIN,"PUCOMFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUCOMODL,"PUCOMODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUNUMINI,"PUNUMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUSERIE1,"PUSERIE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUNUMFIN,"PUNUMFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUSERIE2,"PUSERIE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDOCINI,"PUDOCINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDOCFIN,"PUDOCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUINICLI,"PUINICLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFINCLI,"PUFINCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUINIELA,"PUINIELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFINELA,"PUFINELA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUORIODL,"PUORIODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUSELIMP,"PUSELIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUORDPRD,"PUORDPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUIMPPRD,"PUIMPPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUDELORD,"PUDELORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUELPDAF,"PUELPDAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUELPDAS,"PUELPDAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PUFLAROB,"PUFLAROB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
    i_lTable = "PAR__MRP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR__MRP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR__MRP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR__MRP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR__MRP')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR__MRP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PUCODUTE,PUCODGRP,PUTIPPAR,PUUTEELA,PUNUMKEY"+;
                  ",PUTIPDOC,PUMODELA,PUCHKDAT,PUMRPLOG,PUMSGRIP"+;
                  ",PUMICOMP,PUMAGFOR,PUMRFODL,PUMAGFOC,PUMRFOCL"+;
                  ",PUMAGFOL,PUGENPDA,PUMRFODA,PUMAGFOA,PUGENODA"+;
                  ",PUDISMAG,PUGIANEG,PUORDMPS,PUSTAORD,PUCRIFOR"+;
                  ",PUPERPIA,PUCRIELA,PUPIAPUN,PULISMAG,PUELABID"+;
                  ",PUNOTEEL,PUCODINI,PUCODFIN,PULLCINI,PULLCFIN"+;
                  ",PUFAMAIN,PUFAMAFI,PUGRUINI,PUGRUFIN,PUCATINI"+;
                  ",PUCATFIN,PUMAGINI,PUMAGFIN,PUMARINI,PUMARFIN"+;
                  ",PUPROFIN,PUSEMLAV,PUMATPRI,PUELACAT,PUCOMINI"+;
                  ",PUCOMFIN,PUCOMODL,PUNUMINI,PUSERIE1,PUNUMFIN"+;
                  ",PUSERIE2,PUDOCINI,PUDOCFIN,PUINICLI,PUFINCLI"+;
                  ",PUINIELA,PUFINELA,PUORIODL,PUSELIMP,PUORDPRD"+;
                  ",PUIMPPRD,PUDELORD,PUELPDAF,PUELPDAS,PUFLAROB "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PUCODUTE)+;
                  ","+cp_ToStrODBCNull(this.w_PUCODGRP)+;
                  ","+cp_ToStrODBC(this.w_PUTIPPAR)+;
                  ","+cp_ToStrODBC(this.w_PUUTEELA)+;
                  ","+cp_ToStrODBC(this.w_PUNUMKEY)+;
                  ","+cp_ToStrODBC(this.w_PUTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_PUMODELA)+;
                  ","+cp_ToStrODBC(this.w_PUCHKDAT)+;
                  ","+cp_ToStrODBC(this.w_PUMRPLOG)+;
                  ","+cp_ToStrODBC(this.w_PUMSGRIP)+;
                  ","+cp_ToStrODBC(this.w_PUMICOMP)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGFOR)+;
                  ","+cp_ToStrODBC(this.w_PUMRFODL)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGFOC)+;
                  ","+cp_ToStrODBC(this.w_PUMRFOCL)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGFOL)+;
                  ","+cp_ToStrODBC(this.w_PUGENPDA)+;
                  ","+cp_ToStrODBC(this.w_PUMRFODA)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGFOA)+;
                  ","+cp_ToStrODBC(this.w_PUGENODA)+;
                  ","+cp_ToStrODBC(this.w_PUDISMAG)+;
                  ","+cp_ToStrODBC(this.w_PUGIANEG)+;
                  ","+cp_ToStrODBC(this.w_PUORDMPS)+;
                  ","+cp_ToStrODBC(this.w_PUSTAORD)+;
                  ","+cp_ToStrODBC(this.w_PUCRIFOR)+;
                  ","+cp_ToStrODBC(this.w_PUPERPIA)+;
                  ","+cp_ToStrODBC(this.w_PUCRIELA)+;
                  ","+cp_ToStrODBC(this.w_PUPIAPUN)+;
                  ","+cp_ToStrODBC(this.w_PULISMAG)+;
                  ","+cp_ToStrODBC(this.w_PUELABID)+;
                  ","+cp_ToStrODBC(this.w_PUNOTEEL)+;
                  ","+cp_ToStrODBCNull(this.w_PUCODINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUCODFIN)+;
                  ","+cp_ToStrODBC(this.w_PULLCINI)+;
                  ","+cp_ToStrODBC(this.w_PULLCFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUFAMAIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUFAMAFI)+;
                  ","+cp_ToStrODBCNull(this.w_PUGRUINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUGRUFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUCATINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUCATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUMAGFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUMARINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUMARFIN)+;
                  ","+cp_ToStrODBC(this.w_PUPROFIN)+;
                  ","+cp_ToStrODBC(this.w_PUSEMLAV)+;
                  ","+cp_ToStrODBC(this.w_PUMATPRI)+;
                  ","+cp_ToStrODBC(this.w_PUELACAT)+;
                  ","+cp_ToStrODBCNull(this.w_PUCOMINI)+;
                  ","+cp_ToStrODBCNull(this.w_PUCOMFIN)+;
                  ","+cp_ToStrODBC(this.w_PUCOMODL)+;
                  ","+cp_ToStrODBC(this.w_PUNUMINI)+;
                  ","+cp_ToStrODBC(this.w_PUSERIE1)+;
                  ","+cp_ToStrODBC(this.w_PUNUMFIN)+;
                  ","+cp_ToStrODBC(this.w_PUSERIE2)+;
                  ","+cp_ToStrODBC(this.w_PUDOCINI)+;
                  ","+cp_ToStrODBC(this.w_PUDOCFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PUINICLI)+;
                  ","+cp_ToStrODBCNull(this.w_PUFINCLI)+;
                  ","+cp_ToStrODBC(this.w_PUINIELA)+;
                  ","+cp_ToStrODBC(this.w_PUFINELA)+;
                  ","+cp_ToStrODBC(this.w_PUORIODL)+;
                  ","+cp_ToStrODBC(this.w_PUSELIMP)+;
                  ","+cp_ToStrODBC(this.w_PUORDPRD)+;
                  ","+cp_ToStrODBC(this.w_PUIMPPRD)+;
                  ","+cp_ToStrODBC(this.w_PUDELORD)+;
                  ","+cp_ToStrODBC(this.w_PUELPDAF)+;
                  ","+cp_ToStrODBC(this.w_PUELPDAS)+;
                  ","+cp_ToStrODBC(this.w_PUFLAROB)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR__MRP')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR__MRP')
        cp_CheckDeletedKey(i_cTable,0,'PUNUMKEY',this.w_PUNUMKEY)
        INSERT INTO (i_cTable);
              (PUCODUTE,PUCODGRP,PUTIPPAR,PUUTEELA,PUNUMKEY,PUTIPDOC,PUMODELA,PUCHKDAT,PUMRPLOG,PUMSGRIP,PUMICOMP,PUMAGFOR,PUMRFODL,PUMAGFOC,PUMRFOCL,PUMAGFOL,PUGENPDA,PUMRFODA,PUMAGFOA,PUGENODA,PUDISMAG,PUGIANEG,PUORDMPS,PUSTAORD,PUCRIFOR,PUPERPIA,PUCRIELA,PUPIAPUN,PULISMAG,PUELABID,PUNOTEEL,PUCODINI,PUCODFIN,PULLCINI,PULLCFIN,PUFAMAIN,PUFAMAFI,PUGRUINI,PUGRUFIN,PUCATINI,PUCATFIN,PUMAGINI,PUMAGFIN,PUMARINI,PUMARFIN,PUPROFIN,PUSEMLAV,PUMATPRI,PUELACAT,PUCOMINI,PUCOMFIN,PUCOMODL,PUNUMINI,PUSERIE1,PUNUMFIN,PUSERIE2,PUDOCINI,PUDOCFIN,PUINICLI,PUFINCLI,PUINIELA,PUFINELA,PUORIODL,PUSELIMP,PUORDPRD,PUIMPPRD,PUDELORD,PUELPDAF,PUELPDAS,PUFLAROB  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PUCODUTE;
                  ,this.w_PUCODGRP;
                  ,this.w_PUTIPPAR;
                  ,this.w_PUUTEELA;
                  ,this.w_PUNUMKEY;
                  ,this.w_PUTIPDOC;
                  ,this.w_PUMODELA;
                  ,this.w_PUCHKDAT;
                  ,this.w_PUMRPLOG;
                  ,this.w_PUMSGRIP;
                  ,this.w_PUMICOMP;
                  ,this.w_PUMAGFOR;
                  ,this.w_PUMRFODL;
                  ,this.w_PUMAGFOC;
                  ,this.w_PUMRFOCL;
                  ,this.w_PUMAGFOL;
                  ,this.w_PUGENPDA;
                  ,this.w_PUMRFODA;
                  ,this.w_PUMAGFOA;
                  ,this.w_PUGENODA;
                  ,this.w_PUDISMAG;
                  ,this.w_PUGIANEG;
                  ,this.w_PUORDMPS;
                  ,this.w_PUSTAORD;
                  ,this.w_PUCRIFOR;
                  ,this.w_PUPERPIA;
                  ,this.w_PUCRIELA;
                  ,this.w_PUPIAPUN;
                  ,this.w_PULISMAG;
                  ,this.w_PUELABID;
                  ,this.w_PUNOTEEL;
                  ,this.w_PUCODINI;
                  ,this.w_PUCODFIN;
                  ,this.w_PULLCINI;
                  ,this.w_PULLCFIN;
                  ,this.w_PUFAMAIN;
                  ,this.w_PUFAMAFI;
                  ,this.w_PUGRUINI;
                  ,this.w_PUGRUFIN;
                  ,this.w_PUCATINI;
                  ,this.w_PUCATFIN;
                  ,this.w_PUMAGINI;
                  ,this.w_PUMAGFIN;
                  ,this.w_PUMARINI;
                  ,this.w_PUMARFIN;
                  ,this.w_PUPROFIN;
                  ,this.w_PUSEMLAV;
                  ,this.w_PUMATPRI;
                  ,this.w_PUELACAT;
                  ,this.w_PUCOMINI;
                  ,this.w_PUCOMFIN;
                  ,this.w_PUCOMODL;
                  ,this.w_PUNUMINI;
                  ,this.w_PUSERIE1;
                  ,this.w_PUNUMFIN;
                  ,this.w_PUSERIE2;
                  ,this.w_PUDOCINI;
                  ,this.w_PUDOCFIN;
                  ,this.w_PUINICLI;
                  ,this.w_PUFINCLI;
                  ,this.w_PUINIELA;
                  ,this.w_PUFINELA;
                  ,this.w_PUORIODL;
                  ,this.w_PUSELIMP;
                  ,this.w_PUORDPRD;
                  ,this.w_PUIMPPRD;
                  ,this.w_PUDELORD;
                  ,this.w_PUELPDAF;
                  ,this.w_PUELPDAS;
                  ,this.w_PUFLAROB;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR__MRP_IDX,i_nConn)
      *
      * update PAR__MRP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR__MRP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PUCODUTE="+cp_ToStrODBCNull(this.w_PUCODUTE)+;
             ",PUCODGRP="+cp_ToStrODBCNull(this.w_PUCODGRP)+;
             ",PUTIPPAR="+cp_ToStrODBC(this.w_PUTIPPAR)+;
             ",PUUTEELA="+cp_ToStrODBC(this.w_PUUTEELA)+;
             ",PUTIPDOC="+cp_ToStrODBC(this.w_PUTIPDOC)+;
             ",PUMODELA="+cp_ToStrODBC(this.w_PUMODELA)+;
             ",PUCHKDAT="+cp_ToStrODBC(this.w_PUCHKDAT)+;
             ",PUMRPLOG="+cp_ToStrODBC(this.w_PUMRPLOG)+;
             ",PUMSGRIP="+cp_ToStrODBC(this.w_PUMSGRIP)+;
             ",PUMICOMP="+cp_ToStrODBC(this.w_PUMICOMP)+;
             ",PUMAGFOR="+cp_ToStrODBCNull(this.w_PUMAGFOR)+;
             ",PUMRFODL="+cp_ToStrODBC(this.w_PUMRFODL)+;
             ",PUMAGFOC="+cp_ToStrODBCNull(this.w_PUMAGFOC)+;
             ",PUMRFOCL="+cp_ToStrODBC(this.w_PUMRFOCL)+;
             ",PUMAGFOL="+cp_ToStrODBCNull(this.w_PUMAGFOL)+;
             ",PUGENPDA="+cp_ToStrODBC(this.w_PUGENPDA)+;
             ",PUMRFODA="+cp_ToStrODBC(this.w_PUMRFODA)+;
             ",PUMAGFOA="+cp_ToStrODBCNull(this.w_PUMAGFOA)+;
             ",PUGENODA="+cp_ToStrODBC(this.w_PUGENODA)+;
             ",PUDISMAG="+cp_ToStrODBC(this.w_PUDISMAG)+;
             ",PUGIANEG="+cp_ToStrODBC(this.w_PUGIANEG)+;
             ",PUORDMPS="+cp_ToStrODBC(this.w_PUORDMPS)+;
             ",PUSTAORD="+cp_ToStrODBC(this.w_PUSTAORD)+;
             ",PUCRIFOR="+cp_ToStrODBC(this.w_PUCRIFOR)+;
             ",PUPERPIA="+cp_ToStrODBC(this.w_PUPERPIA)+;
             ",PUCRIELA="+cp_ToStrODBC(this.w_PUCRIELA)+;
             ",PUPIAPUN="+cp_ToStrODBC(this.w_PUPIAPUN)+;
             ",PULISMAG="+cp_ToStrODBC(this.w_PULISMAG)+;
             ",PUELABID="+cp_ToStrODBC(this.w_PUELABID)+;
             ",PUNOTEEL="+cp_ToStrODBC(this.w_PUNOTEEL)+;
             ",PUCODINI="+cp_ToStrODBCNull(this.w_PUCODINI)+;
             ",PUCODFIN="+cp_ToStrODBCNull(this.w_PUCODFIN)+;
             ",PULLCINI="+cp_ToStrODBC(this.w_PULLCINI)+;
             ",PULLCFIN="+cp_ToStrODBC(this.w_PULLCFIN)+;
             ",PUFAMAIN="+cp_ToStrODBCNull(this.w_PUFAMAIN)+;
             ",PUFAMAFI="+cp_ToStrODBCNull(this.w_PUFAMAFI)+;
             ",PUGRUINI="+cp_ToStrODBCNull(this.w_PUGRUINI)+;
             ",PUGRUFIN="+cp_ToStrODBCNull(this.w_PUGRUFIN)+;
             ",PUCATINI="+cp_ToStrODBCNull(this.w_PUCATINI)+;
             ",PUCATFIN="+cp_ToStrODBCNull(this.w_PUCATFIN)+;
             ",PUMAGINI="+cp_ToStrODBCNull(this.w_PUMAGINI)+;
             ",PUMAGFIN="+cp_ToStrODBCNull(this.w_PUMAGFIN)+;
             ",PUMARINI="+cp_ToStrODBCNull(this.w_PUMARINI)+;
             ",PUMARFIN="+cp_ToStrODBCNull(this.w_PUMARFIN)+;
             ",PUPROFIN="+cp_ToStrODBC(this.w_PUPROFIN)+;
             ",PUSEMLAV="+cp_ToStrODBC(this.w_PUSEMLAV)+;
             ",PUMATPRI="+cp_ToStrODBC(this.w_PUMATPRI)+;
             ",PUELACAT="+cp_ToStrODBC(this.w_PUELACAT)+;
             ",PUCOMINI="+cp_ToStrODBCNull(this.w_PUCOMINI)+;
             ",PUCOMFIN="+cp_ToStrODBCNull(this.w_PUCOMFIN)+;
             ",PUCOMODL="+cp_ToStrODBC(this.w_PUCOMODL)+;
             ",PUNUMINI="+cp_ToStrODBC(this.w_PUNUMINI)+;
             ",PUSERIE1="+cp_ToStrODBC(this.w_PUSERIE1)+;
             ",PUNUMFIN="+cp_ToStrODBC(this.w_PUNUMFIN)+;
             ",PUSERIE2="+cp_ToStrODBC(this.w_PUSERIE2)+;
             ",PUDOCINI="+cp_ToStrODBC(this.w_PUDOCINI)+;
             ",PUDOCFIN="+cp_ToStrODBC(this.w_PUDOCFIN)+;
             ",PUINICLI="+cp_ToStrODBCNull(this.w_PUINICLI)+;
             ",PUFINCLI="+cp_ToStrODBCNull(this.w_PUFINCLI)+;
             ",PUINIELA="+cp_ToStrODBC(this.w_PUINIELA)+;
             ",PUFINELA="+cp_ToStrODBC(this.w_PUFINELA)+;
             ",PUORIODL="+cp_ToStrODBC(this.w_PUORIODL)+;
             ",PUSELIMP="+cp_ToStrODBC(this.w_PUSELIMP)+;
             ",PUORDPRD="+cp_ToStrODBC(this.w_PUORDPRD)+;
             ",PUIMPPRD="+cp_ToStrODBC(this.w_PUIMPPRD)+;
             ",PUDELORD="+cp_ToStrODBC(this.w_PUDELORD)+;
             ",PUELPDAF="+cp_ToStrODBC(this.w_PUELPDAF)+;
             ",PUELPDAS="+cp_ToStrODBC(this.w_PUELPDAS)+;
             ",PUFLAROB="+cp_ToStrODBC(this.w_PUFLAROB)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR__MRP')
        i_cWhere = cp_PKFox(i_cTable  ,'PUNUMKEY',this.w_PUNUMKEY  )
        UPDATE (i_cTable) SET;
              PUCODUTE=this.w_PUCODUTE;
             ,PUCODGRP=this.w_PUCODGRP;
             ,PUTIPPAR=this.w_PUTIPPAR;
             ,PUUTEELA=this.w_PUUTEELA;
             ,PUTIPDOC=this.w_PUTIPDOC;
             ,PUMODELA=this.w_PUMODELA;
             ,PUCHKDAT=this.w_PUCHKDAT;
             ,PUMRPLOG=this.w_PUMRPLOG;
             ,PUMSGRIP=this.w_PUMSGRIP;
             ,PUMICOMP=this.w_PUMICOMP;
             ,PUMAGFOR=this.w_PUMAGFOR;
             ,PUMRFODL=this.w_PUMRFODL;
             ,PUMAGFOC=this.w_PUMAGFOC;
             ,PUMRFOCL=this.w_PUMRFOCL;
             ,PUMAGFOL=this.w_PUMAGFOL;
             ,PUGENPDA=this.w_PUGENPDA;
             ,PUMRFODA=this.w_PUMRFODA;
             ,PUMAGFOA=this.w_PUMAGFOA;
             ,PUGENODA=this.w_PUGENODA;
             ,PUDISMAG=this.w_PUDISMAG;
             ,PUGIANEG=this.w_PUGIANEG;
             ,PUORDMPS=this.w_PUORDMPS;
             ,PUSTAORD=this.w_PUSTAORD;
             ,PUCRIFOR=this.w_PUCRIFOR;
             ,PUPERPIA=this.w_PUPERPIA;
             ,PUCRIELA=this.w_PUCRIELA;
             ,PUPIAPUN=this.w_PUPIAPUN;
             ,PULISMAG=this.w_PULISMAG;
             ,PUELABID=this.w_PUELABID;
             ,PUNOTEEL=this.w_PUNOTEEL;
             ,PUCODINI=this.w_PUCODINI;
             ,PUCODFIN=this.w_PUCODFIN;
             ,PULLCINI=this.w_PULLCINI;
             ,PULLCFIN=this.w_PULLCFIN;
             ,PUFAMAIN=this.w_PUFAMAIN;
             ,PUFAMAFI=this.w_PUFAMAFI;
             ,PUGRUINI=this.w_PUGRUINI;
             ,PUGRUFIN=this.w_PUGRUFIN;
             ,PUCATINI=this.w_PUCATINI;
             ,PUCATFIN=this.w_PUCATFIN;
             ,PUMAGINI=this.w_PUMAGINI;
             ,PUMAGFIN=this.w_PUMAGFIN;
             ,PUMARINI=this.w_PUMARINI;
             ,PUMARFIN=this.w_PUMARFIN;
             ,PUPROFIN=this.w_PUPROFIN;
             ,PUSEMLAV=this.w_PUSEMLAV;
             ,PUMATPRI=this.w_PUMATPRI;
             ,PUELACAT=this.w_PUELACAT;
             ,PUCOMINI=this.w_PUCOMINI;
             ,PUCOMFIN=this.w_PUCOMFIN;
             ,PUCOMODL=this.w_PUCOMODL;
             ,PUNUMINI=this.w_PUNUMINI;
             ,PUSERIE1=this.w_PUSERIE1;
             ,PUNUMFIN=this.w_PUNUMFIN;
             ,PUSERIE2=this.w_PUSERIE2;
             ,PUDOCINI=this.w_PUDOCINI;
             ,PUDOCFIN=this.w_PUDOCFIN;
             ,PUINICLI=this.w_PUINICLI;
             ,PUFINCLI=this.w_PUFINCLI;
             ,PUINIELA=this.w_PUINIELA;
             ,PUFINELA=this.w_PUFINELA;
             ,PUORIODL=this.w_PUORIODL;
             ,PUSELIMP=this.w_PUSELIMP;
             ,PUORDPRD=this.w_PUORDPRD;
             ,PUIMPPRD=this.w_PUIMPPRD;
             ,PUDELORD=this.w_PUDELORD;
             ,PUELPDAF=this.w_PUELPDAF;
             ,PUELPDAS=this.w_PUELPDAS;
             ,PUFLAROB=this.w_PUFLAROB;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR__MRP_IDX,i_nConn)
      *
      * delete PAR__MRP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PUNUMKEY',this.w_PUNUMKEY  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR__MRP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR__MRP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_PUCODGRP<>.w_PUCODGRP.or. .o_PUCODUTE<>.w_PUCODUTE
            .w_PUNUMKEY = .w_PUCODUTE+(.w_PUCODGRP*10000)
        endif
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .DoRTCalc(7,8,.t.)
          .link_1_12('Full')
          .link_1_13('Full')
        .DoRTCalc(11,11,.t.)
            .w_MSGMRP = "MR"
          .link_1_15('Full')
        .DoRTCalc(13,25,.t.)
        if .o_PMOCLORD<>.w_PMOCLORD.or. .o_PMODLLAN<>.w_PMODLLAN.or. .o_PMODLPIA<>.w_PMODLPIA.or. .o_PMODAPIA<>.w_PMODAPIA.or. .o_PMODALAN<>.w_PMODALAN
            .w_PUMSGRIP = iif("S" $ nvl(.w_PMOCLORD+.w_PMODLPIA+.w_PMODLLAN+.w_PMODAPIA+.w_PMODALAN," "), "S", "N")
        endif
        if .o_PUCRIELA<>.w_PUCRIELA
            .w_PUMICOMP = IIF(.w_PUCRIELA='M' , 'T', 'M')
        endif
        if .o_PUMICOMP<>.w_PUMICOMP
            .w_PUMAGFOR = SPACE(5)
          .link_1_31('Full')
        endif
        .DoRTCalc(29,29,.t.)
        if .o_PUMRFODL<>.w_PUMRFODL
            .w_PUMAGFOC = SPACE(5)
          .link_1_33('Full')
        endif
        .DoRTCalc(31,31,.t.)
        if .o_PUMRFOCL<>.w_PUMRFOCL
            .w_PUMAGFOL = SPACE(5)
          .link_1_35('Full')
        endif
        .DoRTCalc(33,34,.t.)
        if .o_PUMRFODA<>.w_PUMRFODA
            .w_PUMAGFOA = SPACE(5)
          .link_1_38('Full')
        endif
        if .o_PUGENPDA<>.w_PUGENPDA
            .w_PUGENODA = 'N'
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUDISMAG = 'S'
        endif
        if .o_PUDISMAG<>.w_PUDISMAG.or. .o_PUMODELA<>.w_PUMODELA
            .w_PUGIANEG = iif(.w_PUDISMAG='S','S','N')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUORDMPS = 'S'
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUSTAORD = 'S'
        endif
        .DoRTCalc(41,48,.t.)
            .w_PULISMAG = .w_PULISMAG
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_PUCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4,l_Dep5,l_Dep6,l_Dep7
        l_Dep1= .o_PUMODELA<>.w_PUMODELA .or. .o_PUINICLI<>.w_PUINICLI .or. .o_PUFINCLI<>.w_PUFINCLI .or. .o_PUINIELA<>.w_PUINIELA .or. .o_PUFINELA<>.w_PUFINELA        l_Dep2= .o_CAUSALI<>.w_CAUSALI .or. .o_ED<>.w_ED .or. .o_PUCODINI<>.w_PUCODINI .or. .o_PUCODFIN<>.w_PUCODFIN .or. .o_PUFAMAIN<>.w_PUFAMAIN        l_Dep3= .o_PUFAMAFI<>.w_PUFAMAFI .or. .o_PUGRUINI<>.w_PUGRUINI .or. .o_PUGRUFIN<>.w_PUGRUFIN .or. .o_PUCATINI<>.w_PUCATINI .or. .o_PUCATFIN<>.w_PUCATFIN        l_Dep4= .o_PULLCINI<>.w_PULLCINI .or. .o_PULLCFIN<>.w_PULLCFIN .or. .o_PUMAGINI<>.w_PUMAGINI .or. .o_PUMAGFIN<>.w_PUMAGFIN .or. .o_PUMARINI<>.w_PUMARINI        l_Dep5= .o_PUMARFIN<>.w_PUMARFIN .or. .o_PUCOMINI<>.w_PUCOMINI .or. .o_PUCOMFIN<>.w_PUCOMFIN .or. .o_PUNUMINI<>.w_PUNUMINI .or. .o_PUNUMFIN<>.w_PUNUMFIN        l_Dep6= .o_PUSERIE1<>.w_PUSERIE1 .or. .o_PUSERIE2<>.w_PUSERIE2 .or. .o_PUDOCINI<>.w_PUDOCINI .or. .o_PUDOCFIN<>.w_PUDOCFIN .or. .o_STIPART<>.w_STIPART        l_Dep7= .o_PUPROFIN<>.w_PUPROFIN .or. .o_PUSEMLAV<>.w_PUSEMLAV .or. .o_PUMATPRI<>.w_PUMATPRI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4 .or. m.l_Dep5 .or. m.l_Dep6 .or. m.l_Dep7
            .w_PUELABID = iif(.w_ED, 'N',.w_PUELABID)
        endif
        .DoRTCalc(51,51,.t.)
            .w_PUNOTEEL = .w_NOTMRP
            .w_NOTMRP = .w_PUNOTEEL
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUCODINI = ' '
          .link_2_6('Full')
        endif
        if .o_PUCODINI<>.w_PUCODINI.or. .o_PUMODELA<>.w_PUMODELA
            .w_PUCODFIN = .w_PUCODINI
          .link_2_7('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PULLCINI = 0
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PULLCINI<>.w_PULLCINI
            .w_PULLCFIN = 999
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUFAMAIN = ' '
          .link_2_10('Full')
        endif
        if .o_PUFAMAIN<>.w_PUFAMAIN.or. .o_PUMODELA<>.w_PUMODELA
            .w_PUFAMAFI = .w_PUFAMAIN
          .link_2_11('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUGRUINI = ' '
          .link_2_12('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUGRUINI<>.w_PUGRUINI
            .w_PUGRUFIN = .w_PUGRUINI
          .link_2_13('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUCATINI = ' '
          .link_2_14('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUCATINI<>.w_PUCATINI
            .w_PUCATFIN = .w_PUCATINI
          .link_2_15('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUMAGINI = ' '
          .link_2_16('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUMAGINI<>.w_PUMAGINI
            .w_PUMAGFIN = .w_PUMAGINI
          .link_2_17('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUMARINI = ' '
          .link_2_18('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUMARINI<>.w_PUMARINI
            .w_PUMARFIN = .w_PUMARINI
          .link_2_19('Full')
        endif
        .DoRTCalc(68,70,.t.)
        if .o_PUPROFIN<>.w_PUPROFIN.or. .o_PUSEMLAV<>.w_PUSEMLAV.or. .o_PUMATPRI<>.w_PUMATPRI
            .w_STIPART = iif((empty(.w_PUPROFIN) or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV) or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4,l_Dep5,l_Dep6,l_Dep7
        l_Dep1= .o_PUCODINI<>.w_PUCODINI .or. .o_PUCODFIN<>.w_PUCODFIN .or. .o_PUFAMAIN<>.w_PUFAMAIN .or. .o_PUFAMAFI<>.w_PUFAMAFI .or. .o_PUGRUINI<>.w_PUGRUINI        l_Dep2= .o_PUGRUFIN<>.w_PUGRUFIN .or. .o_PUCATINI<>.w_PUCATINI .or. .o_PUCATFIN<>.w_PUCATFIN .or. .o_PULLCINI<>.w_PULLCINI .or. .o_PULLCFIN<>.w_PULLCFIN        l_Dep3= .o_PUMAGINI<>.w_PUMAGINI .or. .o_PUMAGFIN<>.w_PUMAGFIN .or. .o_PUMARINI<>.w_PUMARINI .or. .o_PUMARFIN<>.w_PUMARFIN .or. .o_STIPART<>.w_STIPART        l_Dep4= .o_PUPROFIN<>.w_PUPROFIN .or. .o_PUSEMLAV<>.w_PUSEMLAV .or. .o_PUMATPRI<>.w_PUMATPRI .or. .o_PUCOMINI<>.w_PUCOMINI .or. .o_PUCOMFIN<>.w_PUCOMFIN        l_Dep5= .o_PUNUMINI<>.w_PUNUMINI .or. .o_PUNUMFIN<>.w_PUNUMFIN .or. .o_PUSERIE1<>.w_PUSERIE1 .or. .o_PUSERIE2<>.w_PUSERIE2 .or. .o_PUDOCINI<>.w_PUDOCINI        l_Dep6= .o_PUDOCFIN<>.w_PUDOCFIN .or. .o_PUINICLI<>.w_PUINICLI .or. .o_PUFINCLI<>.w_PUFINCLI .or. .o_PUINIELA<>.w_PUINIELA .or. .o_PUFINELA<>.w_PUFINELA        l_Dep7= .o_CAUSALI<>.w_CAUSALI .or. .o_PUSELIMP<>.w_PUSELIMP
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4 .or. m.l_Dep5 .or. m.l_Dep6 .or. m.l_Dep7
            .w_ED1 = empty(.w_PUCODINI) and empty (.w_PUCODFIN) and empty(.w_PUFAMAIN) and empty(.w_PUFAMAFI) and empty(.w_PUGRUINI) and empty(.w_PUGRUFIN) and empty(.w_PUCATINI) and empty(.w_PUCATFIN) and .w_PULLCINI=0 and .w_PULLCFIN=999 and empty(.w_PUMAGINI) and empty(.w_PUMAGFIN) and empty(.w_PUMARINI)and empty(.w_PUMARFIN) and empty(.w_PUCOMINI)and empty(.w_PUCOMFIN)and(.w_PUNUMINI=1 or empty(.w_PUNUMINI))and(.w_PUNUMFIN=999999999999999 or empty(.w_PUNUMFIN))and empty(.w_PUSERIE1)and empty(.w_PUSERIE2)and empty(.w_PUDOCINI)and empty(.w_PUDOCFIN) and empty(.w_PUINICLI)and empty(.w_PUFINCLI)and empty(.w_CAUSALI)and (.w_PUINIELA=ctod('01-01-1900') or empty(.w_PUINIELA)) and (.w_PUFINELA=ctod('31-12-2099') or empty(.w_PUFINELA)) and .w_CAUSALI=0 and .w_STIPART<>'N'
        endif
        if .o_ED1<>.w_ED1.or. .o_PUSELIMP<>.w_PUSELIMP
            .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4
        l_Dep1= .o_PUMODELA<>.w_PUMODELA .or. .o_ED<>.w_ED .or. .o_PUCODINI<>.w_PUCODINI .or. .o_PUCODFIN<>.w_PUCODFIN .or. .o_PUFAMAIN<>.w_PUFAMAIN        l_Dep2= .o_PUFAMAFI<>.w_PUFAMAFI .or. .o_PUGRUINI<>.w_PUGRUINI .or. .o_PUGRUFIN<>.w_PUGRUFIN .or. .o_PUCATINI<>.w_PUCATINI .or. .o_PUCATFIN<>.w_PUCATFIN        l_Dep3= .o_PULLCINI<>.w_PULLCINI .or. .o_PULLCFIN<>.w_PULLCFIN .or. .o_PUMAGINI<>.w_PUMAGINI .or. .o_PUMAGFIN<>.w_PUMAGFIN .or. .o_PUMARINI<>.w_PUMARINI        l_Dep4= .o_PUMARFIN<>.w_PUMARFIN .or. .o_STIPART<>.w_STIPART .or. .o_PUPROFIN<>.w_PUPROFIN .or. .o_PUSEMLAV<>.w_PUSEMLAV .or. .o_PUMATPRI<>.w_PUMATPRI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4
            .w_PUELACAT = iif(! .w_ED, 'N','S')
        endif
        .DoRTCalc(75,86,.t.)
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUCOMINI = space(15)
          .link_2_54('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUCOMINI<>.w_PUCOMINI
            .w_PUCOMFIN = .w_PUCOMINI
          .link_2_55('Full')
        endif
        if .o_PUCOMINI<>.w_PUCOMINI.or. .o_PUCOMFIN<>.w_PUCOMFIN
            .w_PUCOMODL = 'T'
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUNUMINI = 1
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUSERIE1 = ''
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUNUMFIN = 999999999999999
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUSERIE2 = ''
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUDOCINI = ctod('')
        endif
        if .o_PUDOCINI<>.w_PUDOCINI.or. .o_PUMODELA<>.w_PUMODELA
            .w_PUDOCFIN = .w_PUDOCINI
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUINICLI = ' '
          .link_2_63('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA.or. .o_PUINICLI<>.w_PUINICLI
            .w_PUFINCLI = .w_PUINICLI
          .link_2_64('Full')
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUINIELA = i_INIDAT
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUFINELA = cp_CharToDate('31-12-2099')
        endif
        if .o_PUINIELA<>.w_PUINIELA.or. .o_PUFINELA<>.w_PUFINELA
            .w_PUORIODL = 'T'
        endif
        if .o_PUMODELA<>.w_PUMODELA
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
        endif
        .DoRTCalc(101,108,.t.)
        if .o_PUPROFIN<>.w_PUPROFIN.or. .o_PUSEMLAV<>.w_PUSEMLAV.or. .o_PUMATPRI<>.w_PUMATPRI
            .w_STIPART = iif((empty(.w_PUPROFIN)or .w_PUPROFIN='PF') and (empty(.w_PUSEMLAV)or .w_PUSEMLAV='SE') and (empty(.w_PUMATPRI)or .w_PUMATPRI='MP'),'S','N')
        endif
        if .o_ED1<>.w_ED1.or. .o_PUSELIMP<>.w_PUSELIMP
            .w_ED = IIF(!.w_ED1, .w_PUSELIMP='I', .w_ED1)
        endif
        .DoRTCalc(111,112,.t.)
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUORDPRD = 'S'
        endif
        if .o_PUMODELA<>.w_PUMODELA
            .w_PUIMPPRD = 'S'
        endif
        .DoRTCalc(115,116,.t.)
        if .o_PUCRIELA<>.w_PUCRIELA
            .w_PUDELORD = IIF(.w_PUCRIELA='A', 'T', IIF(Empty(.w_PUDELORD), 'P', .w_PUDELORD))
        endif
        if .o_PUCRIELA <>.w_PUCRIELA 
          .Calculate_GGOBXLBQED()
        endif
        if .o_PUCRIELA<>.w_PUCRIELA
          .Calculate_VASGIKTUPU()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(118,125,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_PUCRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
    endwith
  return

  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          .w_ZOOMMAGA.cZoomFile = IIF(.w_PUCRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_PUCRIELA = 'G', "QUERY\GSVEGNKGF" , "QUERY\GSVEFNKGF")
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP - menucheck
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_PUCRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP - Done
          .w_RET = UNBINDEVENT(This.oPgFrm.Page1.oPag.ZOOMMAGA.Grd,"Enabled",This,"SetEnabledZoom")
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_PUCRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_VASGIKTUPU()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA - Apertura
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_PUCRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_PUCRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_PUCRIELA $ 'G-M'
          .w_RET = .NotifyEvent("InterrogaMaga")
      if .w_PUCRIELA $ 'G-M'
          .w_RET = .w_ZOOMMAGA.CheckAll()
      endif
      if .w_PUCRIELA = 'A'
          .w_RET = .w_ZOOMMAGA.UnCheckAll()
      endif
    endwith
  endproc
  proc Calculate_AFLAEMOZDZ()
    with this
          * --- Scrollbars Zoom Magazzini
          .w_ZOOMMAGA.GRD.SCROLLBARS = 2
    endwith
  endproc
  proc Calculate_YRRAPCTJJG()
    with this
          * --- Valorizzazione w_KEYRIF
          .w_RET = BINDEVENT(This.oPgFrm.Page1.oPag.ZOOMMAGA.Grd,"Enabled",This,"SetEnabledZoom", 1)
          .w_KEYRIF = SYS(2015)
    endwith
  endproc
  proc Calculate_QYNAVQZUKT()
    with this
          * --- GSMA_BFM - Load
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_PUCRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.enabled = .w_PUCRIELA $ 'G-M'
          .w_ZOOMMAGA.grd.enabled = .w_PUCRIELA $ 'G-M'
          .w_ZOOMMAGA.cZoomFile = IIF(.w_PUCRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_PUCRIELA = 'G', "QUERY\GSVEGNKGF" , "QUERY\GSVEFNKGF")
      if .w_PUCRIELA $ 'G-M' AND !empty(.w_PULISMAG)
          GSAR_BFM(this;
              ,"CARICADATO";
              ,.w_KEYRIF;
              ,.w_PUCRIELA;
              ,"N";
              ,.w_PULISMAG;
             )
      endif
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_FTUTTITKPH()
    with this
          * --- GSMA_BFM - Load
          .w_nColXCHK = .w_ZOOMMAGA.Grd.ColumnCount
      if .w_nColXCHK > 0
          .W_ZOOMMAGA.grd.Columns(.w_nColXCHK).Enabled = Not Inlist(.cFunction, "Query", "FIlter")
      endif
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- Gestione filtri magazzino ZOOMMAGA
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPUCODUTE_1_1.enabled = this.oPgFrm.Page1.oPag.oPUCODUTE_1_1.mCond()
    this.oPgFrm.Page1.oPag.oPUCODGRP_1_2.enabled = this.oPgFrm.Page1.oPag.oPUCODGRP_1_2.mCond()
    this.oPgFrm.Page1.oPag.oPUMSGRIP_1_29.enabled = this.oPgFrm.Page1.oPag.oPUMSGRIP_1_29.mCond()
    this.oPgFrm.Page1.oPag.oPUMICOMP_1_30.enabled = this.oPgFrm.Page1.oPag.oPUMICOMP_1_30.mCond()
    this.oPgFrm.Page1.oPag.oPUMAGFOR_1_31.enabled = this.oPgFrm.Page1.oPag.oPUMAGFOR_1_31.mCond()
    this.oPgFrm.Page1.oPag.oPUMRFODL_1_32.enabled = this.oPgFrm.Page1.oPag.oPUMRFODL_1_32.mCond()
    this.oPgFrm.Page1.oPag.oPUMAGFOC_1_33.enabled = this.oPgFrm.Page1.oPag.oPUMAGFOC_1_33.mCond()
    this.oPgFrm.Page1.oPag.oPUMRFOCL_1_34.enabled = this.oPgFrm.Page1.oPag.oPUMRFOCL_1_34.mCond()
    this.oPgFrm.Page1.oPag.oPUMAGFOL_1_35.enabled = this.oPgFrm.Page1.oPag.oPUMAGFOL_1_35.mCond()
    this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.enabled = this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.mCond()
    this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.enabled = this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.mCond()
    this.oPgFrm.Page1.oPag.oPUGENODA_1_39.enabled = this.oPgFrm.Page1.oPag.oPUGENODA_1_39.mCond()
    this.oPgFrm.Page1.oPag.oPUDISMAG_1_40.enabled = this.oPgFrm.Page1.oPag.oPUDISMAG_1_40.mCond()
    this.oPgFrm.Page1.oPag.oPUGIANEG_1_41.enabled = this.oPgFrm.Page1.oPag.oPUGIANEG_1_41.mCond()
    this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.enabled = this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.mCond()
    this.oPgFrm.Page1.oPag.oPUSTAORD_1_43.enabled = this.oPgFrm.Page1.oPag.oPUSTAORD_1_43.mCond()
    this.oPgFrm.Page1.oPag.oPUCRIFOR_1_64.enabled = this.oPgFrm.Page1.oPag.oPUCRIFOR_1_64.mCond()
    this.oPgFrm.Page1.oPag.oPUPERPIA_1_65.enabled = this.oPgFrm.Page1.oPag.oPUPERPIA_1_65.mCond()
    this.oPgFrm.Page1.oPag.oPUCRIELA_1_66.enabled = this.oPgFrm.Page1.oPag.oPUCRIELA_1_66.mCond()
    this.oPgFrm.Page1.oPag.oPUPIAPUN_1_67.enabled = this.oPgFrm.Page1.oPag.oPUPIAPUN_1_67.mCond()
    this.oPgFrm.Page1.oPag.oPUELABID_1_72.enabled = this.oPgFrm.Page1.oPag.oPUELABID_1_72.mCond()
    this.oPgFrm.Page2.oPag.oPUCODINI_2_6.enabled = this.oPgFrm.Page2.oPag.oPUCODINI_2_6.mCond()
    this.oPgFrm.Page2.oPag.oPUCODFIN_2_7.enabled = this.oPgFrm.Page2.oPag.oPUCODFIN_2_7.mCond()
    this.oPgFrm.Page2.oPag.oPULLCINI_2_8.enabled = this.oPgFrm.Page2.oPag.oPULLCINI_2_8.mCond()
    this.oPgFrm.Page2.oPag.oPULLCFIN_2_9.enabled = this.oPgFrm.Page2.oPag.oPULLCFIN_2_9.mCond()
    this.oPgFrm.Page2.oPag.oPUFAMAIN_2_10.enabled = this.oPgFrm.Page2.oPag.oPUFAMAIN_2_10.mCond()
    this.oPgFrm.Page2.oPag.oPUFAMAFI_2_11.enabled = this.oPgFrm.Page2.oPag.oPUFAMAFI_2_11.mCond()
    this.oPgFrm.Page2.oPag.oPUGRUINI_2_12.enabled = this.oPgFrm.Page2.oPag.oPUGRUINI_2_12.mCond()
    this.oPgFrm.Page2.oPag.oPUGRUFIN_2_13.enabled = this.oPgFrm.Page2.oPag.oPUGRUFIN_2_13.mCond()
    this.oPgFrm.Page2.oPag.oPUCATINI_2_14.enabled = this.oPgFrm.Page2.oPag.oPUCATINI_2_14.mCond()
    this.oPgFrm.Page2.oPag.oPUCATFIN_2_15.enabled = this.oPgFrm.Page2.oPag.oPUCATFIN_2_15.mCond()
    this.oPgFrm.Page2.oPag.oPUMAGINI_2_16.enabled = this.oPgFrm.Page2.oPag.oPUMAGINI_2_16.mCond()
    this.oPgFrm.Page2.oPag.oPUMAGFIN_2_17.enabled = this.oPgFrm.Page2.oPag.oPUMAGFIN_2_17.mCond()
    this.oPgFrm.Page2.oPag.oPUMARINI_2_18.enabled = this.oPgFrm.Page2.oPag.oPUMARINI_2_18.mCond()
    this.oPgFrm.Page2.oPag.oPUMARFIN_2_19.enabled = this.oPgFrm.Page2.oPag.oPUMARFIN_2_19.mCond()
    this.oPgFrm.Page2.oPag.oPUELACAT_2_26.enabled = this.oPgFrm.Page2.oPag.oPUELACAT_2_26.mCond()
    this.oPgFrm.Page2.oPag.oPUCOMINI_2_54.enabled = this.oPgFrm.Page2.oPag.oPUCOMINI_2_54.mCond()
    this.oPgFrm.Page2.oPag.oPUCOMFIN_2_55.enabled = this.oPgFrm.Page2.oPag.oPUCOMFIN_2_55.mCond()
    this.oPgFrm.Page2.oPag.oPUCOMODL_2_56.enabled_(this.oPgFrm.Page2.oPag.oPUCOMODL_2_56.mCond())
    this.oPgFrm.Page2.oPag.oPUNUMINI_2_57.enabled = this.oPgFrm.Page2.oPag.oPUNUMINI_2_57.mCond()
    this.oPgFrm.Page2.oPag.oPUSERIE1_2_58.enabled = this.oPgFrm.Page2.oPag.oPUSERIE1_2_58.mCond()
    this.oPgFrm.Page2.oPag.oPUNUMFIN_2_59.enabled = this.oPgFrm.Page2.oPag.oPUNUMFIN_2_59.mCond()
    this.oPgFrm.Page2.oPag.oPUSERIE2_2_60.enabled = this.oPgFrm.Page2.oPag.oPUSERIE2_2_60.mCond()
    this.oPgFrm.Page2.oPag.oPUDOCINI_2_61.enabled = this.oPgFrm.Page2.oPag.oPUDOCINI_2_61.mCond()
    this.oPgFrm.Page2.oPag.oPUDOCFIN_2_62.enabled = this.oPgFrm.Page2.oPag.oPUDOCFIN_2_62.mCond()
    this.oPgFrm.Page2.oPag.oPUINICLI_2_63.enabled = this.oPgFrm.Page2.oPag.oPUINICLI_2_63.mCond()
    this.oPgFrm.Page2.oPag.oPUFINCLI_2_64.enabled = this.oPgFrm.Page2.oPag.oPUFINCLI_2_64.mCond()
    this.oPgFrm.Page2.oPag.oPUINIELA_2_65.enabled = this.oPgFrm.Page2.oPag.oPUINIELA_2_65.mCond()
    this.oPgFrm.Page2.oPag.oPUFINELA_2_66.enabled = this.oPgFrm.Page2.oPag.oPUFINELA_2_66.mCond()
    this.oPgFrm.Page2.oPag.oPUORIODL_2_67.enabled_(this.oPgFrm.Page2.oPag.oPUORIODL_2_67.mCond())
    this.oPgFrm.Page2.oPag.oSELEZI_2_69.enabled_(this.oPgFrm.Page2.oPag.oSELEZI_2_69.mCond())
    this.oPgFrm.Page2.oPag.oPUORDPRD_2_94.enabled = this.oPgFrm.Page2.oPag.oPUORDPRD_2_94.mCond()
    this.oPgFrm.Page2.oPag.oPUIMPPRD_2_95.enabled = this.oPgFrm.Page2.oPag.oPUIMPPRD_2_95.mCond()
    this.oPgFrm.Page1.oPag.oPUDELORD_1_89.enabled = this.oPgFrm.Page1.oPag.oPUDELORD_1_89.mCond()
    this.oPgFrm.Page1.oPag.oSELEZM_1_93.enabled_(this.oPgFrm.Page1.oPag.oSELEZM_1_93.mCond())
    this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.enabled = this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.mCond()
    this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.enabled = this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.mCond()
    this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.enabled = this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.visible=!this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.mHide()
    this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.visible=!this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.mHide()
    this.oPgFrm.Page1.oPag.oPUGENODA_1_39.visible=!this.oPgFrm.Page1.oPag.oPUGENODA_1_39.mHide()
    this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.visible=!this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDESMOA_1_61.visible=!this.oPgFrm.Page1.oPag.oDESMOA_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.visible=!this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.mHide()
    this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.visible=!this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.mHide()
    this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.visible=!this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_1.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_2.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_3.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_4.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLMAGA.Event(cEvent)
      .oPgFrm.Page2.oPag.SZOOM.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_PUCRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoommaga menucheck") or lower(cEvent)==lower("w_zoommaga row checked") or lower(cEvent)==lower("w_zoommaga row unchecked") or lower(cEvent)==lower("w_zoommaga rowcheckall") or lower(cEvent)==lower("w_zoommaga rowuncheckall")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_VASGIKTUPU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_AFLAEMOZDZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_YRRAPCTJJG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_QYNAVQZUKT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Edit Aborted") or lower(cEvent)==lower("Edit Restarted") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load") or lower(cEvent)==lower("New record") or lower(cEvent)==lower("New record aborted") or lower(cEvent)==lower("SetEnabledZoom")
          .Calculate_FTUTTITKPH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PUCODUTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PUCODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PUCODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PUCODUTE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oPUCODUTE_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PUCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PUCODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PUCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PUCODGRP
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_PUCODGRP);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_PUCODGRP)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_PUCODGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','CODE',cp_AbsName(oSource.parent,'oPUCODGRP_1_2'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_PUCODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_PUCODGRP)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCODGRP = NVL(_Link_.CODE,0)
      this.w_DESGRP = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PUCODGRP = 0
      endif
      this.w_DESGRP = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODICE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPVERSUG,PPCRIELA,PPPERPIA,PPPIAPUN";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPVERSUG,PPCRIELA,PPPERPIA,PPPIAPUN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CRIFORM = NVL(_Link_.PPVERSUG,space(1))
      this.w_PPCRIELA = NVL(_Link_.PPCRIELA,space(1))
      this.w_PPPERPIA = NVL(_Link_.PPPERPIA,space(1))
      this.w_PPPIAPUN = NVL(_Link_.PPPIAPUN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_CRIFORM = space(1)
      this.w_PPCRIELA = space(1)
      this.w_PPPERPIA = space(1)
      this.w_PPPIAPUN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AACODICE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPCRIFOR,PPFLAROB";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_AACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_AACODICE)
            select PPCODICE,PPCRIFOR,PPFLAROB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AACODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CRIFORN = NVL(_Link_.PPCRIFOR,space(1))
      this.w_FLAROB = NVL(_Link_.PPFLAROB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AACODICE = space(2)
      endif
      this.w_CRIFORN = space(1)
      this.w_FLAROB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MSGMRP
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_MRP_IDX,3]
    i_lTable = "PARA_MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2], .t., this.PARA_MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MSGMRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MSGMRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PMCODICE,PMODLPIA,PMODLLAN,PMOCLORD,PMODAPIA,PMODALAN";
                   +" from "+i_cTable+" "+i_lTable+" where PMCODICE="+cp_ToStrODBC(this.w_MSGMRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PMCODICE',this.w_MSGMRP)
            select PMCODICE,PMODLPIA,PMODLLAN,PMOCLORD,PMODAPIA,PMODALAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MSGMRP = NVL(_Link_.PMCODICE,space(2))
      this.w_PMODLPIA = NVL(_Link_.PMODLPIA,space(1))
      this.w_PMODLLAN = NVL(_Link_.PMODLLAN,space(1))
      this.w_PMOCLORD = NVL(_Link_.PMOCLORD,space(1))
      this.w_PMODAPIA = NVL(_Link_.PMODAPIA,space(1))
      this.w_PMODALAN = NVL(_Link_.PMODALAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MSGMRP = space(2)
      endif
      this.w_PMODLPIA = space(1)
      this.w_PMODLLAN = space(1)
      this.w_PMOCLORD = space(1)
      this.w_PMODAPIA = space(1)
      this.w_PMODALAN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])+'\'+cp_ToStr(_Link_.PMCODICE,1)
      cp_ShowWarn(i_cKey,this.PARA_MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MSGMRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PUMAGFOR
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGFOR)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGFOR))
          select MGCODMAG,MGDESMAG,MGDISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGFOR)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGFOR) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGFOR_1_31'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGFOR)
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGFOR = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMFR = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAGFO = NVL(_Link_.MGDISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGFOR = space(5)
      endif
      this.w_DESMFR = space(30)
      this.w_DISMAGFO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.MGCODMAG as MGCODMAG131"+ ",link_1_31.MGDESMAG as MGDESMAG131"+ ",link_1_31.MGDISMAG as MGDISMAG131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on PAR__MRP.PUMAGFOR=link_1_31.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGFOR=link_1_31.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMAGFOC
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGFOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGFOC)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGFOC))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGFOC)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGFOC) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGFOC_1_33'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGFOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGFOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGFOC)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGFOC = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOC = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGFOC = space(5)
      endif
      this.w_DESMOC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGFOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_33(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_33.MGCODMAG as MGCODMAG133"+ ",link_1_33.MGDESMAG as MGDESMAG133"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_33 on PAR__MRP.PUMAGFOC=link_1_33.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_33"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGFOC=link_1_33.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMAGFOL
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGFOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGFOL)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGFOL))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGFOL)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGFOL) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGFOL_1_35'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGFOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGFOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGFOL)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGFOL = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOL = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGFOL = space(5)
      endif
      this.w_DESMOL = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGFOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.MGCODMAG as MGCODMAG135"+ ",link_1_35.MGDESMAG as MGDESMAG135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on PAR__MRP.PUMAGFOL=link_1_35.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGFOL=link_1_35.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMAGFOA
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGFOA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGFOA)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGFOA))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGFOA)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGFOA) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGFOA_1_38'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGFOA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGFOA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGFOA)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGFOA = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMOA = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGFOA = space(5)
      endif
      this.w_DESMOA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGFOA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.MGCODMAG as MGCODMAG138"+ ",link_1_38.MGDESMAG as MGDESMAG138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on PAR__MRP.PUMAGFOA=link_1_38.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGFOA=link_1_38.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCODINI
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PUCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PUCODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_PUCODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_PUCODINI)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PUCODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPUCODINI_2_6'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PUCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PUCODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUCODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUCODINI<=.w_PUCODFIN or empty(.w_PUCODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.ARCODART as ARCODART206"+ ",link_2_6.ARDESART as ARDESART206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on PAR__MRP.PUCODINI=link_2_6.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCODINI=link_2_6.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCODFIN
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_PUCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_PUCODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_PUCODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_PUCODFIN)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PUCODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oPUCODFIN_2_7'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PUCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PUCODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUCODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PUCODINI<=.w_PUCODFIN or empty(.w_PUCODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.ARCODART as ARCODART207"+ ",link_2_7.ARDESART as ARDESART207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on PAR__MRP.PUCODFIN=link_2_7.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCODFIN=link_2_7.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUFAMAIN
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUFAMAIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PUFAMAIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PUFAMAIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUFAMAIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUFAMAIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPUFAMAIN_2_10'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUFAMAIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PUFAMAIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PUFAMAIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUFAMAIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUFAMAIN = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUFAMAIN <= .w_PUFAMAFI OR EMPTY(.w_PUFAMAFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUFAMAIN = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUFAMAIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.FACODICE as FACODICE210"+ ","+cp_TransLinkFldName('link_2_10.FADESCRI')+" as FADESCRI210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on PAR__MRP.PUFAMAIN=link_2_10.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and PAR__MRP.PUFAMAIN=link_2_10.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUFAMAFI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUFAMAFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PUFAMAFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PUFAMAFI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUFAMAFI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUFAMAFI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPUFAMAFI_2_11'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUFAMAFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PUFAMAFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PUFAMAFI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUFAMAFI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUFAMAFI = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUFAMAIN <= .w_PUFAMAFI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUFAMAFI = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUFAMAFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FAM_ARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.FACODICE as FACODICE211"+ ","+cp_TransLinkFldName('link_2_11.FADESCRI')+" as FADESCRI211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on PAR__MRP.PUFAMAFI=link_2_11.FACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and PAR__MRP.PUFAMAFI=link_2_11.FACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUGRUINI
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PUGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PUGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPUGRUINI_2_12'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PUGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PUGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUGRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUGRUINI <= .w_PUGRUFIN OR EMPTY(.w_PUGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUGRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_12.GMCODICE as GMCODICE212"+ ","+cp_TransLinkFldName('link_2_12.GMDESCRI')+" as GMDESCRI212"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_12 on PAR__MRP.PUGRUINI=link_2_12.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_12"
          i_cKey=i_cKey+'+" and PAR__MRP.PUGRUINI=link_2_12.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUGRUFIN
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PUGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PUGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPUGRUFIN_2_13'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PUGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PUGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUGRUINI <= .w_PUGRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUGRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.GMCODICE as GMCODICE213"+ ","+cp_TransLinkFldName('link_2_13.GMDESCRI')+" as GMDESCRI213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on PAR__MRP.PUGRUFIN=link_2_13.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and PAR__MRP.PUGRUFIN=link_2_13.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCATINI
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PUCATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PUCATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUCATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPUCATINI_2_14'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PUCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PUCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUCATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUCATINI <= .w_PUCATFIN OR EMPTY(.w_PUCATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.OMCODICE as OMCODICE214"+ ","+cp_TransLinkFldName('link_2_14.OMDESCRI')+" as OMDESCRI214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on PAR__MRP.PUCATINI=link_2_14.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCATINI=link_2_14.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCATFIN
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PUCATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PUCATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPUCATFIN_2_15'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PUCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PUCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUCATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUCATINI <= .w_PUCATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATEGOMO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.OMCODICE as OMCODICE215"+ ","+cp_TransLinkFldName('link_2_15.OMDESCRI')+" as OMDESCRI215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on PAR__MRP.PUCATFIN=link_2_15.OMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCATFIN=link_2_15.OMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMAGINI
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGINI_2_16'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUMAGINI <= .w_PUMAGFIN OR EMPTY(.w_PUMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.MGCODMAG as MGCODMAG216"+ ",link_2_16.MGDESMAG as MGDESMAG216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on PAR__MRP.PUMAGINI=link_2_16.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGINI=link_2_16.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMAGFIN
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PUMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PUMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPUMAGFIN_2_17'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PUMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PUMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUMAGINI <= .w_PUMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.MGCODMAG as MGCODMAG217"+ ",link_2_17.MGDESMAG as MGDESMAG217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on PAR__MRP.PUMAGFIN=link_2_17.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMAGFIN=link_2_17.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMARINI
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PUMARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PUMARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPUMARINI_2_18'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PUMARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PUMARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUMARINI = space(5)
      endif
      this.w_DESMARI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUMARINI <= .w_PUMARFIN OR EMPTY(.w_PUMARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUMARINI = space(5)
        this.w_DESMARI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_18.MACODICE as MACODICE218"+ ",link_2_18.MADESCRI as MADESCRI218"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_18 on PAR__MRP.PUMARINI=link_2_18.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_18"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMARINI=link_2_18.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUMARFIN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUMARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PUMARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PUMARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUMARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUMARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPUMARFIN_2_19'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUMARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PUMARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PUMARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUMARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PUMARFIN = space(5)
      endif
      this.w_DESMARF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUMARINI <= .w_PUMARFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUMARFIN = space(5)
        this.w_DESMARF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUMARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MARCHI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.MACODICE as MACODICE219"+ ",link_2_19.MADESCRI as MADESCRI219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on PAR__MRP.PUMARFIN=link_2_19.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and PAR__MRP.PUMARFIN=link_2_19.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCOMINI
  func Link_2_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCOMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PUCOMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PUCOMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCOMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUCOMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPUCOMINI_2_54'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCOMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PUCOMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PUCOMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCOMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUCOMINI = space(15)
      endif
      this.w_DESCOMI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUCOMINI <= .w_PUCOMFIN OR EMPTY(.w_PUCOMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCOMINI = space(15)
        this.w_DESCOMI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCOMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_54(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_54.CNCODCAN as CNCODCAN254"+ ",link_2_54.CNDESCAN as CNDESCAN254"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_54 on PAR__MRP.PUCOMINI=link_2_54.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_54"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCOMINI=link_2_54.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUCOMFIN
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUCOMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PUCOMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PUCOMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUCOMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUCOMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPUCOMFIN_2_55'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUCOMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PUCOMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PUCOMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUCOMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PUCOMFIN = space(15)
      endif
      this.w_DESCOMF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PUCOMINI <= .w_PUCOMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUCOMFIN = space(15)
        this.w_DESCOMF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUCOMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_55(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_55.CNCODCAN as CNCODCAN255"+ ",link_2_55.CNDESCAN as CNDESCAN255"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_55 on PAR__MRP.PUCOMFIN=link_2_55.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_55"
          i_cKey=i_cKey+'+" and PAR__MRP.PUCOMFIN=link_2_55.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PUINICLI
  func Link_2_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUINICLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PUINICLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PUINICLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUINICLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUINICLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPUINICLI_2_63'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUINICLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PUINICLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PUINICLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUINICLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLII = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUINICLI = space(15)
      endif
      this.w_DESCLII = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_PUFINCLI) or .w_PUFINCLI>=.w_PUINICLI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUINICLI = space(15)
        this.w_DESCLII = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUINICLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PUFINCLI
  func Link_2_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PUFINCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PUFINCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PUFINCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PUFINCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PUFINCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPUFINCLI_2_64'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PUFINCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PUFINCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PUFINCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PUFINCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PUFINCLI = space(15)
      endif
      this.w_DESCLIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_PUINICLI) or .w_PUFINCLI>=.w_PUINICLI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PUFINCLI = space(15)
        this.w_DESCLIF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PUFINCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPUCODUTE_1_1.value==this.w_PUCODUTE)
      this.oPgFrm.Page1.oPag.oPUCODUTE_1_1.value=this.w_PUCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPUCODGRP_1_2.value==this.w_PUCODGRP)
      this.oPgFrm.Page1.oPag.oPUCODGRP_1_2.value=this.w_PUCODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oPUTIPPAR_1_3.RadioValue()==this.w_PUTIPPAR)
      this.oPgFrm.Page1.oPag.oPUTIPPAR_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_6.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_6.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPUNUMKEY_1_7.value==this.w_PUNUMKEY)
      this.oPgFrm.Page1.oPag.oPUNUMKEY_1_7.value=this.w_PUNUMKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRP_1_9.value==this.w_DESGRP)
      this.oPgFrm.Page1.oPag.oDESGRP_1_9.value=this.w_DESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMODELA_1_26.RadioValue()==this.w_PUMODELA)
      this.oPgFrm.Page1.oPag.oPUMODELA_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUCHKDAT_1_27.RadioValue()==this.w_PUCHKDAT)
      this.oPgFrm.Page1.oPag.oPUCHKDAT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMRPLOG_1_28.RadioValue()==this.w_PUMRPLOG)
      this.oPgFrm.Page1.oPag.oPUMRPLOG_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMSGRIP_1_29.RadioValue()==this.w_PUMSGRIP)
      this.oPgFrm.Page1.oPag.oPUMSGRIP_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMICOMP_1_30.RadioValue()==this.w_PUMICOMP)
      this.oPgFrm.Page1.oPag.oPUMICOMP_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMAGFOR_1_31.value==this.w_PUMAGFOR)
      this.oPgFrm.Page1.oPag.oPUMAGFOR_1_31.value=this.w_PUMAGFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMRFODL_1_32.RadioValue()==this.w_PUMRFODL)
      this.oPgFrm.Page1.oPag.oPUMRFODL_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMAGFOC_1_33.value==this.w_PUMAGFOC)
      this.oPgFrm.Page1.oPag.oPUMAGFOC_1_33.value=this.w_PUMAGFOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMRFOCL_1_34.RadioValue()==this.w_PUMRFOCL)
      this.oPgFrm.Page1.oPag.oPUMRFOCL_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMAGFOL_1_35.value==this.w_PUMAGFOL)
      this.oPgFrm.Page1.oPag.oPUMAGFOL_1_35.value=this.w_PUMAGFOL
    endif
    if not(this.oPgFrm.Page1.oPag.oPUGENPDA_1_36.RadioValue()==this.w_PUGENPDA)
      this.oPgFrm.Page1.oPag.oPUGENPDA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.RadioValue()==this.w_PUMRFODA)
      this.oPgFrm.Page1.oPag.oPUMRFODA_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.value==this.w_PUMAGFOA)
      this.oPgFrm.Page1.oPag.oPUMAGFOA_1_38.value=this.w_PUMAGFOA
    endif
    if not(this.oPgFrm.Page1.oPag.oPUGENODA_1_39.RadioValue()==this.w_PUGENODA)
      this.oPgFrm.Page1.oPag.oPUGENODA_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUDISMAG_1_40.RadioValue()==this.w_PUDISMAG)
      this.oPgFrm.Page1.oPag.oPUDISMAG_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUGIANEG_1_41.RadioValue()==this.w_PUGIANEG)
      this.oPgFrm.Page1.oPag.oPUGIANEG_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.RadioValue()==this.w_PUORDMPS)
      this.oPgFrm.Page1.oPag.oPUORDMPS_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUSTAORD_1_43.RadioValue()==this.w_PUSTAORD)
      this.oPgFrm.Page1.oPag.oPUSTAORD_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMFR_1_58.value==this.w_DESMFR)
      this.oPgFrm.Page1.oPag.oDESMFR_1_58.value=this.w_DESMFR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOC_1_59.value==this.w_DESMOC)
      this.oPgFrm.Page1.oPag.oDESMOC_1_59.value=this.w_DESMOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOL_1_60.value==this.w_DESMOL)
      this.oPgFrm.Page1.oPag.oDESMOL_1_60.value=this.w_DESMOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOA_1_61.value==this.w_DESMOA)
      this.oPgFrm.Page1.oPag.oDESMOA_1_61.value=this.w_DESMOA
    endif
    if not(this.oPgFrm.Page1.oPag.oPUCRIFOR_1_64.RadioValue()==this.w_PUCRIFOR)
      this.oPgFrm.Page1.oPag.oPUCRIFOR_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUPERPIA_1_65.RadioValue()==this.w_PUPERPIA)
      this.oPgFrm.Page1.oPag.oPUPERPIA_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUCRIELA_1_66.RadioValue()==this.w_PUCRIELA)
      this.oPgFrm.Page1.oPag.oPUCRIELA_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUPIAPUN_1_67.RadioValue()==this.w_PUPIAPUN)
      this.oPgFrm.Page1.oPag.oPUPIAPUN_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUELABID_1_72.RadioValue()==this.w_PUELABID)
      this.oPgFrm.Page1.oPag.oPUELABID_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCODINI_2_6.value==this.w_PUCODINI)
      this.oPgFrm.Page2.oPag.oPUCODINI_2_6.value=this.w_PUCODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCODFIN_2_7.value==this.w_PUCODFIN)
      this.oPgFrm.Page2.oPag.oPUCODFIN_2_7.value=this.w_PUCODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPULLCINI_2_8.value==this.w_PULLCINI)
      this.oPgFrm.Page2.oPag.oPULLCINI_2_8.value=this.w_PULLCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPULLCFIN_2_9.value==this.w_PULLCFIN)
      this.oPgFrm.Page2.oPag.oPULLCFIN_2_9.value=this.w_PULLCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUFAMAIN_2_10.value==this.w_PUFAMAIN)
      this.oPgFrm.Page2.oPag.oPUFAMAIN_2_10.value=this.w_PUFAMAIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUFAMAFI_2_11.value==this.w_PUFAMAFI)
      this.oPgFrm.Page2.oPag.oPUFAMAFI_2_11.value=this.w_PUFAMAFI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUGRUINI_2_12.value==this.w_PUGRUINI)
      this.oPgFrm.Page2.oPag.oPUGRUINI_2_12.value=this.w_PUGRUINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUGRUFIN_2_13.value==this.w_PUGRUFIN)
      this.oPgFrm.Page2.oPag.oPUGRUFIN_2_13.value=this.w_PUGRUFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCATINI_2_14.value==this.w_PUCATINI)
      this.oPgFrm.Page2.oPag.oPUCATINI_2_14.value=this.w_PUCATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCATFIN_2_15.value==this.w_PUCATFIN)
      this.oPgFrm.Page2.oPag.oPUCATFIN_2_15.value=this.w_PUCATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUMAGINI_2_16.value==this.w_PUMAGINI)
      this.oPgFrm.Page2.oPag.oPUMAGINI_2_16.value=this.w_PUMAGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUMAGFIN_2_17.value==this.w_PUMAGFIN)
      this.oPgFrm.Page2.oPag.oPUMAGFIN_2_17.value=this.w_PUMAGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUMARINI_2_18.value==this.w_PUMARINI)
      this.oPgFrm.Page2.oPag.oPUMARINI_2_18.value=this.w_PUMARINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUMARFIN_2_19.value==this.w_PUMARFIN)
      this.oPgFrm.Page2.oPag.oPUMARFIN_2_19.value=this.w_PUMARFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUPROFIN_2_20.RadioValue()==this.w_PUPROFIN)
      this.oPgFrm.Page2.oPag.oPUPROFIN_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUSEMLAV_2_21.RadioValue()==this.w_PUSEMLAV)
      this.oPgFrm.Page2.oPag.oPUSEMLAV_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUMATPRI_2_22.RadioValue()==this.w_PUMATPRI)
      this.oPgFrm.Page2.oPag.oPUMATPRI_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUELACAT_2_26.RadioValue()==this.w_PUELACAT)
      this.oPgFrm.Page2.oPag.oPUELACAT_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGI_2_28.value==this.w_DESMAGI)
      this.oPgFrm.Page2.oPag.oDESMAGI_2_28.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAGF_2_30.value==this.w_DESMAGF)
      this.oPgFrm.Page2.oPag.oDESMAGF_2_30.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_31.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_31.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_33.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_33.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAI_2_35.value==this.w_DESFAMAI)
      this.oPgFrm.Page2.oPag.oDESFAMAI_2_35.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUI_2_36.value==this.w_DESGRUI)
      this.oPgFrm.Page2.oPag.oDESGRUI_2_36.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATI_2_37.value==this.w_DESCATI)
      this.oPgFrm.Page2.oPag.oDESCATI_2_37.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAMAF_2_41.value==this.w_DESFAMAF)
      this.oPgFrm.Page2.oPag.oDESFAMAF_2_41.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUF_2_42.value==this.w_DESGRUF)
      this.oPgFrm.Page2.oPag.oDESGRUF_2_42.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCATF_2_43.value==this.w_DESCATF)
      this.oPgFrm.Page2.oPag.oDESCATF_2_43.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARI_2_51.value==this.w_DESMARI)
      this.oPgFrm.Page2.oPag.oDESMARI_2_51.value=this.w_DESMARI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMARF_2_53.value==this.w_DESMARF)
      this.oPgFrm.Page2.oPag.oDESMARF_2_53.value=this.w_DESMARF
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCOMINI_2_54.value==this.w_PUCOMINI)
      this.oPgFrm.Page2.oPag.oPUCOMINI_2_54.value=this.w_PUCOMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCOMFIN_2_55.value==this.w_PUCOMFIN)
      this.oPgFrm.Page2.oPag.oPUCOMFIN_2_55.value=this.w_PUCOMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUCOMODL_2_56.RadioValue()==this.w_PUCOMODL)
      this.oPgFrm.Page2.oPag.oPUCOMODL_2_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUNUMINI_2_57.value==this.w_PUNUMINI)
      this.oPgFrm.Page2.oPag.oPUNUMINI_2_57.value=this.w_PUNUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUSERIE1_2_58.value==this.w_PUSERIE1)
      this.oPgFrm.Page2.oPag.oPUSERIE1_2_58.value=this.w_PUSERIE1
    endif
    if not(this.oPgFrm.Page2.oPag.oPUNUMFIN_2_59.value==this.w_PUNUMFIN)
      this.oPgFrm.Page2.oPag.oPUNUMFIN_2_59.value=this.w_PUNUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUSERIE2_2_60.value==this.w_PUSERIE2)
      this.oPgFrm.Page2.oPag.oPUSERIE2_2_60.value=this.w_PUSERIE2
    endif
    if not(this.oPgFrm.Page2.oPag.oPUDOCINI_2_61.value==this.w_PUDOCINI)
      this.oPgFrm.Page2.oPag.oPUDOCINI_2_61.value=this.w_PUDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUDOCFIN_2_62.value==this.w_PUDOCFIN)
      this.oPgFrm.Page2.oPag.oPUDOCFIN_2_62.value=this.w_PUDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUINICLI_2_63.value==this.w_PUINICLI)
      this.oPgFrm.Page2.oPag.oPUINICLI_2_63.value=this.w_PUINICLI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUFINCLI_2_64.value==this.w_PUFINCLI)
      this.oPgFrm.Page2.oPag.oPUFINCLI_2_64.value=this.w_PUFINCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oPUINIELA_2_65.value==this.w_PUINIELA)
      this.oPgFrm.Page2.oPag.oPUINIELA_2_65.value=this.w_PUINIELA
    endif
    if not(this.oPgFrm.Page2.oPag.oPUFINELA_2_66.value==this.w_PUFINELA)
      this.oPgFrm.Page2.oPag.oPUFINELA_2_66.value=this.w_PUFINELA
    endif
    if not(this.oPgFrm.Page2.oPag.oPUORIODL_2_67.RadioValue()==this.w_PUORIODL)
      this.oPgFrm.Page2.oPag.oPUORIODL_2_67.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_69.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_69.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLII_2_75.value==this.w_DESCLII)
      this.oPgFrm.Page2.oPag.oDESCLII_2_75.value=this.w_DESCLII
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLIF_2_76.value==this.w_DESCLIF)
      this.oPgFrm.Page2.oPag.oDESCLIF_2_76.value=this.w_DESCLIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMI_2_78.value==this.w_DESCOMI)
      this.oPgFrm.Page2.oPag.oDESCOMI_2_78.value=this.w_DESCOMI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMF_2_80.value==this.w_DESCOMF)
      this.oPgFrm.Page2.oPag.oDESCOMF_2_80.value=this.w_DESCOMF
    endif
    if not(this.oPgFrm.Page2.oPag.oPUSELIMP_2_88.RadioValue()==this.w_PUSELIMP)
      this.oPgFrm.Page2.oPag.oPUSELIMP_2_88.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUORDPRD_2_94.RadioValue()==this.w_PUORDPRD)
      this.oPgFrm.Page2.oPag.oPUORDPRD_2_94.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUIMPPRD_2_95.RadioValue()==this.w_PUIMPPRD)
      this.oPgFrm.Page2.oPag.oPUIMPPRD_2_95.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUDELORD_1_89.RadioValue()==this.w_PUDELORD)
      this.oPgFrm.Page1.oPag.oPUDELORD_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZM_1_93.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page1.oPag.oSELEZM_1_93.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.RadioValue()==this.w_PUELPDAF)
      this.oPgFrm.Page1.oPag.oPUELPDAF_1_104.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.RadioValue()==this.w_PUELPDAS)
      this.oPgFrm.Page1.oPag.oPUELPDAS_1_105.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.RadioValue()==this.w_PUFLAROB)
      this.oPgFrm.Page1.oPag.oPUFLAROB_1_107.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAR__MRP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PUMAGFOR))  and (.w_PUMICOMP='F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUMAGFOR_1_31.SetFocus()
            i_bnoObbl = !empty(.w_PUMAGFOR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PUMAGFOC))  and (.w_PUMRFODL='F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUMAGFOC_1_33.SetFocus()
            i_bnoObbl = !empty(.w_PUMAGFOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PUMAGFOL))  and (.w_PUMRFOCL='F' and g_COLA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUMAGFOL_1_35.SetFocus()
            i_bnoObbl = !empty(.w_PUMAGFOL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PUMAGFOA))  and not(g_MODA<>'S')  and (.w_PUMRFODA='F' and g_MODA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPUMAGFOA_1_38.SetFocus()
            i_bnoObbl = !empty(.w_PUMAGFOA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUCODINI<=.w_PUCODFIN or empty(.w_PUCODFIN))  and (.w_PUMODELA='R')  and not(empty(.w_PUCODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCODINI_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PUCODINI<=.w_PUCODFIN or empty(.w_PUCODINI)))  and (.w_PUMODELA='R')  and not(empty(.w_PUCODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCODFIN_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PULLCINI<=.w_PULLCFIN and .w_PULLCINI>=0)  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPULLCINI_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PULLCINI<=.w_PULLCFIN)  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPULLCFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUFAMAIN <= .w_PUFAMAFI OR EMPTY(.w_PUFAMAFI))  and (.w_PUMODELA='R')  and not(empty(.w_PUFAMAIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUFAMAIN_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUFAMAIN <= .w_PUFAMAFI)  and (.w_PUMODELA='R')  and not(empty(.w_PUFAMAFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUFAMAFI_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUGRUINI <= .w_PUGRUFIN OR EMPTY(.w_PUGRUFIN))  and (.w_PUMODELA='R')  and not(empty(.w_PUGRUINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUGRUINI_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUGRUINI <= .w_PUGRUFIN)  and (.w_PUMODELA='R')  and not(empty(.w_PUGRUFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUGRUFIN_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUCATINI <= .w_PUCATFIN OR EMPTY(.w_PUCATFIN))  and (.w_PUMODELA='R')  and not(empty(.w_PUCATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCATINI_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUCATINI <= .w_PUCATFIN)  and (.w_PUMODELA='R')  and not(empty(.w_PUCATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCATFIN_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUMAGINI <= .w_PUMAGFIN OR EMPTY(.w_PUMAGFIN))  and (.w_PUMODELA='R')  and not(empty(.w_PUMAGINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUMAGINI_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUMAGINI <= .w_PUMAGFIN)  and (.w_PUMODELA='R')  and not(empty(.w_PUMAGFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUMAGFIN_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUMARINI <= .w_PUMARFIN OR EMPTY(.w_PUMARFIN))  and (.w_PUMODELA='R')  and not(empty(.w_PUMARINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUMARINI_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUMARINI <= .w_PUMARFIN)  and (.w_PUMODELA='R')  and not(empty(.w_PUMARFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUMARFIN_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUCOMINI <= .w_PUCOMFIN OR EMPTY(.w_PUCOMFIN))  and ((g_PERCAN='S' or g_COMM='S') and .w_PUMODELA='R')  and not(empty(.w_PUCOMINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCOMINI_2_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUCOMINI <= .w_PUCOMFIN)  and ((g_PERCAN='S' or g_COMM='S') and .w_PUMODELA='R')  and not(empty(.w_PUCOMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUCOMFIN_2_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PUNUMINI)) or not(.w_PUNUMINI<=.w_PUNUMFIN and .w_PUNUMINI>0))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUNUMINI_2_57.SetFocus()
            i_bnoObbl = !empty(.w_PUNUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_PUSERIE2)) OR  (.w_PUSERIE1<=.w_PUSERIE2))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUSERIE1_2_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PUNUMFIN)) or not(.w_PUNUMINI<=.w_PUNUMFIN and .w_PUNUMFIN>0))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUNUMFIN_2_59.SetFocus()
            i_bnoObbl = !empty(.w_PUNUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PUSERIE2>=.w_PUSERIE1) or (empty(.w_PUSERIE1)))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUSERIE2_2_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUDOCINI<=.w_PUDOCFIN or empty(.w_PUDOCFIN))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUDOCINI_2_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PUDOCINI<=.w_PUDOCFIN)  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUDOCFIN_2_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_PUFINCLI) or .w_PUFINCLI>=.w_PUINICLI)  and (.w_PUMODELA='R')  and not(empty(.w_PUINICLI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUINICLI_2_63.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_PUINICLI) or .w_PUFINCLI>=.w_PUINICLI)  and (.w_PUMODELA='R')  and not(empty(.w_PUFINCLI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUFINCLI_2_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PUINIELA)) or not(not empty(.w_PUINIELA) and .w_PUFINELA>=.w_PUINIELA))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUINIELA_2_65.SetFocus()
            i_bnoObbl = !empty(.w_PUINIELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o maggiore della data di fine elaborazione")
          case   ((empty(.w_PUFINELA)) or not(not empty(.w_PUFINELA) and .w_PUFINELA>=.w_PUINIELA))  and (.w_PUMODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPUFINELA_2_66.SetFocus()
            i_bnoObbl = !empty(.w_PUFINELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o minore della data di fine elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PUCODUTE = this.w_PUCODUTE
    this.o_PUCODGRP = this.w_PUCODGRP
    this.o_PMOCLORD = this.w_PMOCLORD
    this.o_PMODLPIA = this.w_PMODLPIA
    this.o_PMODLLAN = this.w_PMODLLAN
    this.o_PMODAPIA = this.w_PMODAPIA
    this.o_PMODALAN = this.w_PMODALAN
    this.o_PUMODELA = this.w_PUMODELA
    this.o_PUMICOMP = this.w_PUMICOMP
    this.o_PUMRFODL = this.w_PUMRFODL
    this.o_PUMRFOCL = this.w_PUMRFOCL
    this.o_PUGENPDA = this.w_PUGENPDA
    this.o_PUMRFODA = this.w_PUMRFODA
    this.o_PUDISMAG = this.w_PUDISMAG
    this.o_PUCRIELA = this.w_PUCRIELA
    this.o_PUCODINI = this.w_PUCODINI
    this.o_PUCODFIN = this.w_PUCODFIN
    this.o_PULLCINI = this.w_PULLCINI
    this.o_PULLCFIN = this.w_PULLCFIN
    this.o_PUFAMAIN = this.w_PUFAMAIN
    this.o_PUFAMAFI = this.w_PUFAMAFI
    this.o_PUGRUINI = this.w_PUGRUINI
    this.o_PUGRUFIN = this.w_PUGRUFIN
    this.o_PUCATINI = this.w_PUCATINI
    this.o_PUCATFIN = this.w_PUCATFIN
    this.o_PUMAGINI = this.w_PUMAGINI
    this.o_PUMAGFIN = this.w_PUMAGFIN
    this.o_PUMARINI = this.w_PUMARINI
    this.o_PUMARFIN = this.w_PUMARFIN
    this.o_PUPROFIN = this.w_PUPROFIN
    this.o_PUSEMLAV = this.w_PUSEMLAV
    this.o_PUMATPRI = this.w_PUMATPRI
    this.o_STIPART = this.w_STIPART
    this.o_ED1 = this.w_ED1
    this.o_ED = this.w_ED
    this.o_PUCOMINI = this.w_PUCOMINI
    this.o_PUCOMFIN = this.w_PUCOMFIN
    this.o_PUNUMINI = this.w_PUNUMINI
    this.o_PUSERIE1 = this.w_PUSERIE1
    this.o_PUNUMFIN = this.w_PUNUMFIN
    this.o_PUSERIE2 = this.w_PUSERIE2
    this.o_PUDOCINI = this.w_PUDOCINI
    this.o_PUDOCFIN = this.w_PUDOCFIN
    this.o_PUINICLI = this.w_PUINICLI
    this.o_PUFINCLI = this.w_PUFINCLI
    this.o_PUINIELA = this.w_PUINIELA
    this.o_PUFINELA = this.w_PUFINELA
    this.o_PUSELIMP = this.w_PUSELIMP
    this.o_CAUSALI = this.w_CAUSALI
    this.o_SELEZM = this.w_SELEZM
    return

enddefine

* --- Define pages as container
define class tgsmr_apuPag1 as StdContainer
  Width  = 812
  height = 597
  stdWidth  = 812
  stdheight = 597
  resizeXpos=446
  resizeYpos=385
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPUCODUTE_1_1 as StdField with uid="FONCZKEDOT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PUCODUTE", cQueryName = "PUCODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 663355,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=296, Top=8, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_PUCODUTE"

  func oPUCODUTE_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUCODGRP=0)
    endwith
   endif
  endfunc

  func oPUCODUTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCODUTE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCODUTE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oPUCODUTE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oPUCODGRP_1_2 as StdField with uid="IOQOFBIONV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PUCODGRP", cQueryName = "PUCODUTE,PUCODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 34217798,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=296, Top=37, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="CODE", oKey_1_2="this.w_PUCODGRP"

  func oPUCODGRP_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUCODUTE=0)
    endwith
   endif
  endfunc

  func oPUCODGRP_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCODGRP_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCODGRP_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','CODE',cp_AbsName(this.parent,'oPUCODGRP_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'',this.parent.oContained
  endproc


  add object oPUTIPPAR_1_3 as StdCombo with uid="GZHHOIFLLJ",rtseq=3,rtrep=.f.,left=623,top=9,width=161,height=21;
    , ToolTipText = "Tipo parametro";
    , HelpContextID = 197472072;
    , cFormVar="w_PUTIPPAR",RowSource=""+"Parametri modificabili,"+"Parametri non modificabili", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUTIPPAR_1_3.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPUTIPPAR_1_3.GetRadio()
    this.Parent.oContained.w_PUTIPPAR = this.RadioValue()
    return .t.
  endfunc

  func oPUTIPPAR_1_3.SetRadio()
    this.Parent.oContained.w_PUTIPPAR=trim(this.Parent.oContained.w_PUTIPPAR)
    this.value = ;
      iif(this.Parent.oContained.w_PUTIPPAR=='D',1,;
      iif(this.Parent.oContained.w_PUTIPPAR=='N',2,;
      0))
  endfunc

  add object oDESUTE_1_6 as StdField with uid="XXRKGFCXKH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 17894966,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=354, Top=8, InputMask=replicate('X',20)

  add object oPUNUMKEY_1_7 as StdField with uid="AAMXCVFZBD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PUNUMKEY", cQueryName = "PUNUMKEY",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Chiave primaria",;
    HelpContextID = 111202127,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=72, Top=8

  add object oDESGRP_1_9 as StdField with uid="QSLANXRRLO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESGRP", cQueryName = "DESGRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 199429686,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=354, Top=37, InputMask=replicate('X',20)

  add object oPUMODELA_1_26 as StdRadio with uid="IAYNGAFPBC",rtseq=23,rtrep=.f.,left=23, top=68, width=108,height=35;
    , ToolTipText = "Modalit� di elaborazione";
    , cFormVar="w_PUMODELA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPUMODELA_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Rigenerativa"
      this.Buttons(1).HelpContextID = 267731145
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Net-change"
      this.Buttons(2).HelpContextID = 267731145
      this.Buttons(2).Top=16
      this.SetAll("Width",106)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Modalit� di elaborazione")
      StdRadio::init()
    endproc

  func oPUMODELA_1_26.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPUMODELA_1_26.GetRadio()
    this.Parent.oContained.w_PUMODELA = this.RadioValue()
    return .t.
  endfunc

  func oPUMODELA_1_26.SetRadio()
    this.Parent.oContained.w_PUMODELA=trim(this.Parent.oContained.w_PUMODELA)
    this.value = ;
      iif(this.Parent.oContained.w_PUMODELA=='R',1,;
      iif(this.Parent.oContained.w_PUMODELA=='N',2,;
      0))
  endfunc

  add object oPUCHKDAT_1_27 as StdCheck with uid="LHIUVIMPMY",rtseq=24,rtrep=.f.,left=23, top=393, caption="Test correttezza dati",;
    ToolTipText = "Verifica di correttezza dei dati necessari per l'elaborazione",;
    HelpContextID = 259202890,;
    cFormVar="w_PUCHKDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUCHKDAT_1_27.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPUCHKDAT_1_27.GetRadio()
    this.Parent.oContained.w_PUCHKDAT = this.RadioValue()
    return .t.
  endfunc

  func oPUCHKDAT_1_27.SetRadio()
    this.Parent.oContained.w_PUCHKDAT=trim(this.Parent.oContained.w_PUCHKDAT)
    this.value = ;
      iif(this.Parent.oContained.w_PUCHKDAT=="S",1,;
      0)
  endfunc

  add object oPUMRPLOG_1_28 as StdCheck with uid="KYWQCBJRJS",rtseq=25,rtrep=.f.,left=23, top=418, caption="Attiva scrittura log elaborazione",;
    ToolTipText = "Attiva la scrittura del log di elaborazione MRP",;
    HelpContextID = 130924349,;
    cFormVar="w_PUMRPLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUMRPLOG_1_28.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPUMRPLOG_1_28.GetRadio()
    this.Parent.oContained.w_PUMRPLOG = this.RadioValue()
    return .t.
  endfunc

  func oPUMRPLOG_1_28.SetRadio()
    this.Parent.oContained.w_PUMRPLOG=trim(this.Parent.oContained.w_PUMRPLOG)
    this.value = ;
      iif(this.Parent.oContained.w_PUMRPLOG=="S",1,;
      0)
  endfunc

  add object oPUMSGRIP_1_29 as StdCheck with uid="UOOBFZJPIG",rtseq=26,rtrep=.f.,left=23, top=552, caption="Messaggi ripianificazione",;
    ToolTipText = "Abilita i messaggi ripianificazione",;
    HelpContextID = 46219450,;
    cFormVar="w_PUMSGRIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUMSGRIP_1_29.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPUMSGRIP_1_29.GetRadio()
    this.Parent.oContained.w_PUMSGRIP = this.RadioValue()
    return .t.
  endfunc

  func oPUMSGRIP_1_29.SetRadio()
    this.Parent.oContained.w_PUMSGRIP=trim(this.Parent.oContained.w_PUMSGRIP)
    this.value = ;
      iif(this.Parent.oContained.w_PUMSGRIP=="S",1,;
      0)
  endfunc

  func oPUMSGRIP_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ("S" $ nvl(.w_PMOCLORD+.w_PMODLPIA+.w_PMODLLAN+.w_PMODAPIA+.w_PMODALAN," "))
    endwith
   endif
  endfunc


  add object oPUMICOMP_1_30 as StdCombo with uid="SRNAAGBEFD",rtseq=27,rtrep=.f.,left=224,top=109,width=157,height=21;
    , ToolTipText = "Magazzino di impegno componenti";
    , HelpContextID = 101400762;
    , cFormVar="w_PUMICOMP",RowSource=""+"Magazzino preferenziale,"+"Da anagrafica articoli,"+"Da testata odine,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUMICOMP_1_30.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'A',;
    iif(this.value =3,'T',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oPUMICOMP_1_30.GetRadio()
    this.Parent.oContained.w_PUMICOMP = this.RadioValue()
    return .t.
  endfunc

  func oPUMICOMP_1_30.SetRadio()
    this.Parent.oContained.w_PUMICOMP=trim(this.Parent.oContained.w_PUMICOMP)
    this.value = ;
      iif(this.Parent.oContained.w_PUMICOMP=='M',1,;
      iif(this.Parent.oContained.w_PUMICOMP=='A',2,;
      iif(this.Parent.oContained.w_PUMICOMP=='T',3,;
      iif(this.Parent.oContained.w_PUMICOMP=='F',4,;
      0))))
  endfunc

  func oPUMICOMP_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oPUMAGFOR_1_31 as StdField with uid="JKCCZXUOVE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PUMAGFOR", cQueryName = "PUMAGFOR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di impegno per componenti",;
    HelpContextID = 19709768,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=511, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGFOR"

  func oPUMAGFOR_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMICOMP='F')
    endwith
   endif
  endfunc

  func oPUMAGFOR_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGFOR_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGFOR_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGFOR_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc


  add object oPUMRFODL_1_32 as StdCombo with uid="AUAHSRLOHO",rtseq=29,rtrep=.f.,left=224,top=134,width=157,height=21;
    , ToolTipText = "Magazzino di riferimento ODL";
    , HelpContextID = 170770242;
    , cFormVar="w_PUMRFODL",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUMRFODL_1_32.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oPUMRFODL_1_32.GetRadio()
    this.Parent.oContained.w_PUMRFODL = this.RadioValue()
    return .t.
  endfunc

  func oPUMRFODL_1_32.SetRadio()
    this.Parent.oContained.w_PUMRFODL=trim(this.Parent.oContained.w_PUMRFODL)
    this.value = ;
      iif(this.Parent.oContained.w_PUMRFODL=='M',1,;
      iif(this.Parent.oContained.w_PUMRFODL=='C',2,;
      iif(this.Parent.oContained.w_PUMRFODL=='F',3,;
      0)))
  endfunc

  func oPUMRFODL_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oPUMAGFOC_1_33 as StdField with uid="HAVLAOFVAV",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PUMAGFOC", cQueryName = "PUMAGFOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento ODL",;
    HelpContextID = 19709753,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=511, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGFOC"

  func oPUMAGFOC_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMRFODL='F')
    endwith
   endif
  endfunc

  func oPUMAGFOC_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGFOC_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGFOC_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGFOC_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc


  add object oPUMRFOCL_1_34 as StdCombo with uid="BBLKKVFXWJ",rtseq=31,rtrep=.f.,left=224,top=159,width=157,height=21;
    , ToolTipText = "Magazzino di riferimento OCL";
    , HelpContextID = 170770242;
    , cFormVar="w_PUMRFOCL",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUMRFOCL_1_34.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oPUMRFOCL_1_34.GetRadio()
    this.Parent.oContained.w_PUMRFOCL = this.RadioValue()
    return .t.
  endfunc

  func oPUMRFOCL_1_34.SetRadio()
    this.Parent.oContained.w_PUMRFOCL=trim(this.Parent.oContained.w_PUMRFOCL)
    this.value = ;
      iif(this.Parent.oContained.w_PUMRFOCL=='M',1,;
      iif(this.Parent.oContained.w_PUMRFOCL=='C',2,;
      iif(this.Parent.oContained.w_PUMRFOCL=='F',3,;
      0)))
  endfunc

  func oPUMRFOCL_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R' and g_COLA='S')
    endwith
   endif
  endfunc

  add object oPUMAGFOL_1_35 as StdField with uid="RNISWUMNCL",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PUMAGFOL", cQueryName = "PUMAGFOL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento OCL",;
    HelpContextID = 19709762,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=511, Top=159, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGFOL"

  func oPUMAGFOL_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMRFOCL='F' and g_COLA='S')
    endwith
   endif
  endfunc

  func oPUMAGFOL_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGFOL_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGFOL_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGFOL_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oPUGENPDA_1_36 as StdCheck with uid="NKJOLKZOYZ",rtseq=33,rtrep=.f.,left=23, top=205, caption="Elabora",;
    ToolTipText = "Se attivo: genera proposte d'ordine per materiali d'acquisto",;
    HelpContextID = 195059511,;
    cFormVar="w_PUGENPDA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUGENPDA_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPUGENPDA_1_36.GetRadio()
    this.Parent.oContained.w_PUGENPDA = this.RadioValue()
    return .t.
  endfunc

  func oPUGENPDA_1_36.SetRadio()
    this.Parent.oContained.w_PUGENPDA=trim(this.Parent.oContained.w_PUGENPDA)
    this.value = ;
      iif(this.Parent.oContained.w_PUGENPDA=='S',1,;
      0)
  endfunc


  add object oPUMRFODA_1_37 as StdCombo with uid="FDINBJKTUF",rtseq=34,rtrep=.f.,left=224,top=204,width=157,height=21;
    , ToolTipText = "Magazzino di riferimento ODA";
    , HelpContextID = 170770231;
    , cFormVar="w_PUMRFODA",RowSource=""+"Magazzino preferenziale,"+"Criterio di pianificazione,"+"Forza magazzino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUMRFODA_1_37.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oPUMRFODA_1_37.GetRadio()
    this.Parent.oContained.w_PUMRFODA = this.RadioValue()
    return .t.
  endfunc

  func oPUMRFODA_1_37.SetRadio()
    this.Parent.oContained.w_PUMRFODA=trim(this.Parent.oContained.w_PUMRFODA)
    this.value = ;
      iif(this.Parent.oContained.w_PUMRFODA=='M',1,;
      iif(this.Parent.oContained.w_PUMRFODA=='C',2,;
      iif(this.Parent.oContained.w_PUMRFODA=='F',3,;
      0)))
  endfunc

  func oPUMRFODA_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R' and g_MODA='S')
    endwith
   endif
  endfunc

  func oPUMRFODA_1_37.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc

  add object oPUMAGFOA_1_38 as StdField with uid="XEHOGPYVZN",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PUMAGFOA", cQueryName = "PUMAGFOA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino di riferimento ODA",;
    HelpContextID = 19709751,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=511, Top=205, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGFOA"

  func oPUMAGFOA_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMRFODA='F' and g_MODA='S')
    endwith
   endif
  endfunc

  func oPUMAGFOA_1_38.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc

  func oPUMAGFOA_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGFOA_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGFOA_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGFOA_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oPUGENODA_1_39 as StdCheck with uid="XQDNSFTQLZ",rtseq=36,rtrep=.f.,left=23, top=229, caption="Genera esclusivamente gli ODA",;
    ToolTipText = "Genera esclusivamente gli ODA",;
    HelpContextID = 178282295,;
    cFormVar="w_PUGENODA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUGENODA_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPUGENODA_1_39.GetRadio()
    this.Parent.oContained.w_PUGENODA = this.RadioValue()
    return .t.
  endfunc

  func oPUGENODA_1_39.SetRadio()
    this.Parent.oContained.w_PUGENODA=trim(this.Parent.oContained.w_PUGENODA)
    this.value = ;
      iif(this.Parent.oContained.w_PUGENODA=='S',1,;
      0)
  endfunc

  func oPUGENODA_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUGENPDA='S')
    endwith
   endif
  endfunc

  func oPUGENODA_1_39.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc

  add object oPUDISMAG_1_40 as StdCheck with uid="BILRKZZCAA",rtseq=37,rtrep=.f.,left=23, top=468, caption="Considera disponibilit� magazzino",;
    ToolTipText = "Considera disponibilit� magazzino",;
    HelpContextID = 150220605,;
    cFormVar="w_PUDISMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUDISMAG_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPUDISMAG_1_40.GetRadio()
    this.Parent.oContained.w_PUDISMAG = this.RadioValue()
    return .t.
  endfunc

  func oPUDISMAG_1_40.SetRadio()
    this.Parent.oContained.w_PUDISMAG=trim(this.Parent.oContained.w_PUDISMAG)
    this.value = ;
      iif(this.Parent.oContained.w_PUDISMAG=='S',1,;
      0)
  endfunc

  func oPUDISMAG_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oPUGIANEG_1_41 as StdCheck with uid="FKREULHMYC",rtseq=38,rtrep=.f.,left=23, top=443, caption="Considera giacenze negative",;
    ToolTipText = "Considera le giacenze negative",;
    HelpContextID = 148135741,;
    cFormVar="w_PUGIANEG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUGIANEG_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPUGIANEG_1_41.GetRadio()
    this.Parent.oContained.w_PUGIANEG = this.RadioValue()
    return .t.
  endfunc

  func oPUGIANEG_1_41.SetRadio()
    this.Parent.oContained.w_PUGIANEG=trim(this.Parent.oContained.w_PUGIANEG)
    this.value = ;
      iif(this.Parent.oContained.w_PUGIANEG=='S',1,;
      0)
  endfunc

  func oPUGIANEG_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUDISMAG='S' and .w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oPUORDMPS_1_42 as StdCheck with uid="NUTKYGIZWK",rtseq=39,rtrep=.f.,left=23, top=492, caption="Considera ordini MPS da pianificare",;
    ToolTipText = "Considera ordini MPS da pianificare",;
    HelpContextID = 135126857,;
    cFormVar="w_PUORDMPS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUORDMPS_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPUORDMPS_1_42.GetRadio()
    this.Parent.oContained.w_PUORDMPS = this.RadioValue()
    return .t.
  endfunc

  func oPUORDMPS_1_42.SetRadio()
    this.Parent.oContained.w_PUORDMPS=trim(this.Parent.oContained.w_PUORDMPS)
    this.value = ;
      iif(this.Parent.oContained.w_PUORDMPS=='S',1,;
      0)
  endfunc

  func oPUORDMPS_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUORDMPS_1_42.mHide()
    with this.Parent.oContained
      return (g_MMPS<>'S')
    endwith
  endfunc


  add object oPUSTAORD_1_43 as StdCombo with uid="GFGQGKCOSW",rtseq=40,rtrep=.f.,left=224,top=338,width=157,height=21;
    , ToolTipText = "Stato ordini generati da MRP";
    , HelpContextID = 165683002;
    , cFormVar="w_PUSTAORD",RowSource=""+"Suggerito,"+"Pianificato,"+"Pianificato (da articolo)", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUSTAORD_1_43.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'P',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPUSTAORD_1_43.GetRadio()
    this.Parent.oContained.w_PUSTAORD = this.RadioValue()
    return .t.
  endfunc

  func oPUSTAORD_1_43.SetRadio()
    this.Parent.oContained.w_PUSTAORD=trim(this.Parent.oContained.w_PUSTAORD)
    this.value = ;
      iif(this.Parent.oContained.w_PUSTAORD=='S',1,;
      iif(this.Parent.oContained.w_PUSTAORD=='P',2,;
      iif(this.Parent.oContained.w_PUSTAORD=='A',3,;
      0)))
  endfunc

  func oPUSTAORD_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oDESMFR_1_58 as StdField with uid="PGNQOTCUXR",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESMFR", cQueryName = "DESMFR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 220794422,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=573, Top=109, InputMask=replicate('X',30)

  add object oDESMOC_1_59 as StdField with uid="CDATHAQKUR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESMOC", cQueryName = "DESMOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 247008822,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=573, Top=134, InputMask=replicate('X',30)

  add object oDESMOL_1_60 as StdField with uid="GEBWEFFQVC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESMOL", cQueryName = "DESMOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 129568310,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=573, Top=159, InputMask=replicate('X',30)

  add object oDESMOA_1_61 as StdField with uid="XCJRMLCMJC",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESMOA", cQueryName = "DESMOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 213454390,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=573, Top=205, InputMask=replicate('X',30)

  func oDESMOA_1_61.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc


  add object oPUCRIFOR_1_64 as StdCombo with uid="XRETRZSGJB",rtseq=45,rtrep=.f.,left=511,top=231,width=142,height=21;
    , ToolTipText = "Criterio di scelta del miglior fornitore in base ai contratti in essere";
    , HelpContextID = 22880072;
    , cFormVar="w_PUCRIFOR",RowSource=""+"Priorit�,"+"Tempo,"+"Prezzo,"+"Affidabilit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUCRIFOR_1_64.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oPUCRIFOR_1_64.GetRadio()
    this.Parent.oContained.w_PUCRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oPUCRIFOR_1_64.SetRadio()
    this.Parent.oContained.w_PUCRIFOR=trim(this.Parent.oContained.w_PUCRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_PUCRIFOR=='I',1,;
      iif(this.Parent.oContained.w_PUCRIFOR=='T',2,;
      iif(this.Parent.oContained.w_PUCRIFOR=='Z',3,;
      iif(this.Parent.oContained.w_PUCRIFOR=='A',4,;
      0))))
  endfunc

  func oPUCRIFOR_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUGENPDA='S')
    endwith
   endif
  endfunc


  add object oPUPERPIA_1_65 as StdCombo with uid="DRHNEVIUIN",rtseq=46,rtrep=.f.,left=224,top=263,width=157,height=21;
    , ToolTipText = "Pianifica per periodo";
    , HelpContextID = 69144777;
    , cFormVar="w_PUPERPIA",RowSource=""+"Giorno,"+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUPERPIA_1_65.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'F',;
    iif(this.value =6,'S',;
    iif(this.value =7,'A',;
    space(1)))))))))
  endfunc
  func oPUPERPIA_1_65.GetRadio()
    this.Parent.oContained.w_PUPERPIA = this.RadioValue()
    return .t.
  endfunc

  func oPUPERPIA_1_65.SetRadio()
    this.Parent.oContained.w_PUPERPIA=trim(this.Parent.oContained.w_PUPERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PUPERPIA=='D',1,;
      iif(this.Parent.oContained.w_PUPERPIA=='W',2,;
      iif(this.Parent.oContained.w_PUPERPIA=='M',3,;
      iif(this.Parent.oContained.w_PUPERPIA=='Q',4,;
      iif(this.Parent.oContained.w_PUPERPIA=='F',5,;
      iif(this.Parent.oContained.w_PUPERPIA=='S',6,;
      iif(this.Parent.oContained.w_PUPERPIA=='A',7,;
      0)))))))
  endfunc

  func oPUPERPIA_1_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc


  add object oPUCRIELA_1_66 as StdCombo with uid="VSDAZQQBWZ",rtseq=47,rtrep=.f.,left=224,top=288,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 262332617;
    , cFormVar="w_PUCRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUCRIELA_1_66.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oPUCRIELA_1_66.GetRadio()
    this.Parent.oContained.w_PUCRIELA = this.RadioValue()
    return .t.
  endfunc

  func oPUCRIELA_1_66.SetRadio()
    this.Parent.oContained.w_PUCRIELA=trim(this.Parent.oContained.w_PUCRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_PUCRIELA=='A',1,;
      iif(this.Parent.oContained.w_PUCRIELA=='M',2,;
      iif(this.Parent.oContained.w_PUCRIELA=='G',3,;
      0)))
  endfunc

  func oPUCRIELA_1_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc


  add object oPUPIAPUN_1_67 as StdCombo with uid="QHVQIVCNUN",rtseq=48,rtrep=.f.,left=224,top=313,width=157,height=21;
    , ToolTipText = "Pianificazione puntuale";
    , HelpContextID = 181727044;
    , cFormVar="w_PUPIAPUN",RowSource=""+"Si,"+"No,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUPIAPUN_1_67.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPUPIAPUN_1_67.GetRadio()
    this.Parent.oContained.w_PUPIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oPUPIAPUN_1_67.SetRadio()
    this.Parent.oContained.w_PUPIAPUN=trim(this.Parent.oContained.w_PUPIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_PUPIAPUN=='S',1,;
      iif(this.Parent.oContained.w_PUPIAPUN=='N',2,;
      iif(this.Parent.oContained.w_PUPIAPUN=='A',3,;
      0)))
  endfunc

  func oPUPIAPUN_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=436, top=275, width=373,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=1;
    , HelpContextID = 17996774


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=441, top=265, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=1;
    , HelpContextID = 17996774

  add object oPUELABID_1_72 as StdCheck with uid="LAPRLXLQTW",rtseq=50,rtrep=.f.,left=23, top=517, caption="Rigenera i bidoni temporali per tutti gli ordini",;
    ToolTipText = "Rigenera i bidoni temporali per tutti gli ordini",;
    HelpContextID = 53002438,;
    cFormVar="w_PUELABID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUELABID_1_72.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPUELABID_1_72.GetRadio()
    this.Parent.oContained.w_PUELABID = this.RadioValue()
    return .t.
  endfunc

  func oPUELABID_1_72.SetRadio()
    this.Parent.oContained.w_PUELABID=trim(this.Parent.oContained.w_PUELABID)
    this.value = ;
      iif(this.Parent.oContained.w_PUELABID=='S',1,;
      0)
  endfunc

  func oPUELABID_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R' and ! .w_ED)
    endwith
   endif
  endfunc


  add object oBtn_1_73 as StdButton with uid="LUXLLMGPOJ",left=761, top=551, width=48,height=45,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=1;
    , ToolTipText = "Parametri messaggi MRP";
    , HelpContextID = 152876842;
    , Caption='\<Note elab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      do GSCO_KNE with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPUDELORD_1_89 as StdCombo with uid="JFOLGBXAQP",rtseq=117,rtrep=.f.,left=224,top=363,width=157,height=21;
    , ToolTipText = "Indica quali ordini suggeriti dall'MRP deve eliminare";
    , HelpContextID = 176172858;
    , cFormVar="w_PUDELORD",RowSource=""+"Tutti gli ordini,"+"Da criterio di pianificazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUDELORD_1_89.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oPUDELORD_1_89.GetRadio()
    this.Parent.oContained.w_PUDELORD = this.RadioValue()
    return .t.
  endfunc

  func oPUDELORD_1_89.SetRadio()
    this.Parent.oContained.w_PUDELORD=trim(this.Parent.oContained.w_PUDELORD)
    this.value = ;
      iif(this.Parent.oContained.w_PUDELORD=='T',1,;
      iif(this.Parent.oContained.w_PUDELORD=='P',2,;
      0))
  endfunc

  func oPUDELORD_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUCRIELA<>'A')
    endwith
   endif
  endfunc

  add object oSELEZM_1_93 as StdRadio with uid="LTQWYYFJNE",rtseq=120,rtrep=.f.,left=572, top=526, width=239,height=20;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZM_1_93.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 157327142
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 157327142
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_1_93.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_1_93.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_1_93.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_1_93.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUCRIELA $ 'G-M')
    endwith
   endif
  endfunc

  add object oPUELPDAF_1_104 as StdCheck with uid="HKUREFTMVX",rtseq=122,rtrep=.f.,left=224, top=203, caption="a fabbisogno",;
    ToolTipText = "Elabora articoli a fabbisogno",;
    HelpContextID = 264716092,;
    cFormVar="w_PUELPDAF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUELPDAF_1_104.RadioValue()
    return(iif(this.value =1,'F',;
    '.'))
  endfunc
  func oPUELPDAF_1_104.GetRadio()
    this.Parent.oContained.w_PUELPDAF = this.RadioValue()
    return .t.
  endfunc

  func oPUELPDAF_1_104.SetRadio()
    this.Parent.oContained.w_PUELPDAF=trim(this.Parent.oContained.w_PUELPDAF)
    this.value = ;
      iif(this.Parent.oContained.w_PUELPDAF=='F',1,;
      0)
  endfunc

  func oPUELPDAF_1_104.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUGENPDA='S')
    endwith
   endif
  endfunc

  func oPUELPDAF_1_104.mHide()
    with this.Parent.oContained
      return (g_MODA='S')
    endwith
  endfunc

  add object oPUELPDAS_1_105 as StdCheck with uid="FGYUVIBBUI",rtseq=123,rtrep=.f.,left=224, top=228, caption="a scorta",;
    ToolTipText = "Elabora articoli a quantit� costante",;
    HelpContextID = 264716105,;
    cFormVar="w_PUELPDAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUELPDAS_1_105.RadioValue()
    return(iif(this.value =1,'S',;
    '.'))
  endfunc
  func oPUELPDAS_1_105.GetRadio()
    this.Parent.oContained.w_PUELPDAS = this.RadioValue()
    return .t.
  endfunc

  func oPUELPDAS_1_105.SetRadio()
    this.Parent.oContained.w_PUELPDAS=trim(this.Parent.oContained.w_PUELPDAS)
    this.value = ;
      iif(this.Parent.oContained.w_PUELPDAS=='S',1,;
      0)
  endfunc

  func oPUELPDAS_1_105.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUGENPDA='S')
    endwith
   endif
  endfunc

  func oPUELPDAS_1_105.mHide()
    with this.Parent.oContained
      return (g_MODA='S')
    endwith
  endfunc

  add object oPUFLAROB_1_107 as StdCheck with uid="TITLJSWEZG",rtseq=125,rtrep=.f.,left=511, top=205, caption="Pianifica articoli obsoleti",;
    ToolTipText = "Pianifica proposte di acquisto per articoli obsoleti",;
    HelpContextID = 215437112,;
    cFormVar="w_PUFLAROB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPUFLAROB_1_107.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPUFLAROB_1_107.GetRadio()
    this.Parent.oContained.w_PUFLAROB = this.RadioValue()
    return .t.
  endfunc

  func oPUFLAROB_1_107.SetRadio()
    this.Parent.oContained.w_PUFLAROB=trim(this.Parent.oContained.w_PUFLAROB)
    this.value = ;
      iif(this.Parent.oContained.w_PUFLAROB=='S',1,;
      0)
  endfunc

  func oPUFLAROB_1_107.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUGENPDA='S')
    endwith
   endif
  endfunc

  func oPUFLAROB_1_107.mHide()
    with this.Parent.oContained
      return (g_MODA='S')
    endwith
  endfunc

  add object oStr_1_5 as StdString with uid="ZQNOZWMOYQ",Visible=.t., Left=236, Top=8,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="BOYTUIICFE",Visible=.t., Left=241, Top=37,;
    Alignment=1, Width=48, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JSZCENZMFZ",Visible=.t., Left=19, Top=8,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WIBVOKLZQD",Visible=.t., Left=573, Top=8,;
    Alignment=1, Width=41, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CAOFDPYAQW",Visible=.t., Left=138, Top=68,;
    Alignment=0, Width=438, Height=15,;
    Caption="(Elaborazione completa di tutti ODP/ODR da pianificare presenti nel MPS)"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="WJWUAYAAWL",Visible=.t., Left=138, Top=84,;
    Alignment=0, Width=422, Height=15,;
    Caption="(Elaborazione limitata ai soli ODP/ODR da pianificare e ODL variati)"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="IXROEWEKDY",Visible=.t., Left=120, Top=341,;
    Alignment=1, Width=99, Height=15,;
    Caption="Stato ordini:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="MKNVPNHKGR",Visible=.t., Left=18, Top=182,;
    Alignment=0, Width=272, Height=15,;
    Caption="Proposte d'ordine per materiali d'acquisto"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="XDQOYSXVSN",Visible=.t., Left=324, Top=233,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di scelta fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="MHFQZKQMOH",Visible=.t., Left=11, Top=109,;
    Alignment=1, Width=208, Height=15,;
    Caption="Magazzino di impegno componenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="TVPYODRMUO",Visible=.t., Left=100, Top=134,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino ODL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="QVSIYQLJNF",Visible=.t., Left=100, Top=159,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino OCL:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="OEOKFOXMGZ",Visible=.t., Left=387, Top=134,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="KAQYZWMMPQ",Visible=.t., Left=387, Top=159,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KNLRCHTZXJ",Visible=.t., Left=387, Top=109,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="NTDKUOUTZZ",Visible=.t., Left=387, Top=205,;
    Alignment=1, Width=119, Height=15,;
    Caption="Codice magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="KDGLPCVNDJ",Visible=.t., Left=100, Top=206,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino ODA:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (g_MODA<>'S')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=37, Top=290,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="VSKSWONFNJ",Visible=.t., Left=37, Top=266,;
    Alignment=1, Width=182, Height=18,;
    Caption="Periodo di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="UYQPTYMPSQ",Visible=.t., Left=37, Top=315,;
    Alignment=1, Width=182, Height=17,;
    Caption="Pianificazione puntuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="MAGTHPKFQX",Visible=.t., Left=76, Top=366,;
    Alignment=1, Width=143, Height=18,;
    Caption="Elimina ordini suggeriti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="YIGIVNUWEN",Visible=.t., Left=124, Top=206,;
    Alignment=1, Width=95, Height=18,;
    Caption="Elabora articoli:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (g_MODA='S')
    endwith
  endfunc

  add object oBox_1_46 as StdBox with uid="AUQPTDFFZX",left=17, top=104, width=785,height=1

  add object oBox_1_48 as StdBox with uid="UMKYACLDHV",left=17, top=256, width=785,height=1

  add object oBox_1_50 as StdBox with uid="LVPJKZGHJE",left=17, top=198, width=785,height=1

  add object oBox_1_74 as StdBox with uid="NMBAMDFHHV",left=17, top=439, width=410,height=1

  add object oBox_1_75 as StdBox with uid="UHZJXIBASP",left=17, top=513, width=410,height=1

  add object oBox_1_76 as StdBox with uid="CFNEYTNLYK",left=17, top=389, width=410,height=1

  add object oBox_1_80 as StdBox with uid="JTUAECCGDY",left=17, top=64, width=785,height=1
enddefine
define class tgsmr_apuPag2 as StdContainer
  Width  = 812
  height = 597
  stdWidth  = 812
  stdheight = 597
  resizeXpos=461
  resizeYpos=466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_1 as cp_runprogram with uid="IQXQPNKAVZ",left=215, top=648, width=209,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMR2BGP("Z")',;
    cEvent = "Load",;
    nPag=2;
    , HelpContextID = 17996774


  add object oObj_2_2 as cp_runprogram with uid="BHANWDYKJR",left=1, top=615, width=209,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMR_BEX("SS")',;
    cEvent = "w_SELEZI Changed,Blank",;
    nPag=2;
    , HelpContextID = 17996774


  add object oObj_2_3 as cp_runprogram with uid="MLJVNLRHPU",left=1, top=648, width=209,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMR2BGP("S")',;
    cEvent = "w_szoom row unchecked,w_szoom row checked,w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 17996774


  add object oObj_2_4 as cp_runprogram with uid="PXPKNWAJLB",left=215, top=615, width=209,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMR2BGP("R")',;
    cEvent = "Record Inserted,Record Updated",;
    nPag=2;
    , HelpContextID = 17996774

  add object oPUCODINI_2_6 as StdField with uid="DZDJHVMUJE",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PUCODINI", cQueryName = "PUCODINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo di inizio selezione",;
    HelpContextID = 200663233,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=106, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PUCODINI"

  func oPUCODINI_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCODINI_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCODINI_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCODINI_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPUCODINI_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oPUCODFIN_2_7 as StdField with uid="UNDISPFWGH",rtseq=55,rtrep=.f.,;
    cFormVar = "w_PUCODFIN", cQueryName = "PUCODFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articoli di fine selezione",;
    HelpContextID = 250994876,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=106, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_PUCODFIN"

  func oPUCODFIN_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCODFIN_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCODFIN_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCODFIN_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oPUCODFIN_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oPULLCINI_2_8 as StdField with uid="RKYWPPNDHG",rtseq=56,rtrep=.f.,;
    cFormVar = "w_PULLCINI", cQueryName = "PULLCINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di inizio selezione",;
    HelpContextID = 201871553,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=767, Top=34, cSayPict='"999"', cGetPict='"999"'

  func oPULLCINI_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPULLCINI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PULLCINI<=.w_PULLCFIN and .w_PULLCINI>=0)
    endwith
    return bRes
  endfunc

  add object oPULLCFIN_2_9 as StdField with uid="ACQHHCIYYT",rtseq=57,rtrep=.f.,;
    cFormVar = "w_PULLCFIN", cQueryName = "PULLCFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di fine selezione",;
    HelpContextID = 252203196,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=767, Top=57, cSayPict='"999"', cGetPict='"999"'

  func oPULLCFIN_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPULLCFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PULLCINI<=.w_PULLCFIN)
    endwith
    return bRes
  endfunc

  add object oPUFAMAIN_2_10 as StdField with uid="BAUZFWPTWC",rtseq=58,rtrep=.f.,;
    cFormVar = "w_PUFAMAIN", cQueryName = "PUFAMAIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 57913532,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=85, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PUFAMAIN"

  func oPUFAMAIN_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUFAMAIN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUFAMAIN_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUFAMAIN_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPUFAMAIN_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oPUFAMAFI_2_11 as StdField with uid="GHDDKOKVIG",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PUFAMAFI", cQueryName = "PUFAMAFI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 210521919,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=509, Top=85, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PUFAMAFI"

  func oPUFAMAFI_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUFAMAFI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUFAMAFI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUFAMAFI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPUFAMAFI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oPUGRUINI_2_12 as StdField with uid="VYQYSTJFCH",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PUGRUINI", cQueryName = "PUGRUINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 182624449,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PUGRUINI"

  func oPUGRUINI_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUGRUINI_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUGRUINI_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUGRUINI_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPUGRUINI_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oPUGRUFIN_2_13 as StdField with uid="BKTBROMCMD",rtseq=61,rtrep=.f.,;
    cFormVar = "w_PUGRUFIN", cQueryName = "PUGRUFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 232956092,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=509, Top=110, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PUGRUFIN"

  func oPUGRUFIN_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUGRUFIN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUGRUFIN_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUGRUFIN_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPUGRUFIN_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oPUCATINI_2_14 as StdField with uid="WQNLFQEXDD",rtseq=62,rtrep=.f.,;
    cFormVar = "w_PUCATINI", cQueryName = "PUCATINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 184803521,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=135, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PUCATINI"

  func oPUCATINI_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCATINI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCATINI_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCATINI_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPUCATINI_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oPUCATFIN_2_15 as StdField with uid="WHQQEZYTLE",rtseq=63,rtrep=.f.,;
    cFormVar = "w_PUCATFIN", cQueryName = "PUCATFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 235135164,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=509, Top=135, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PUCATFIN"

  func oPUCATFIN_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCATFIN_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCATFIN_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCATFIN_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPUCATFIN_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oPUMAGINI_2_16 as StdField with uid="WWAXPPIMPZ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PUMAGINI", cQueryName = "PUMAGINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di inizio selezione",;
    HelpContextID = 198394049,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGINI"

  func oPUMAGINI_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUMAGINI_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGINI_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGINI_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGINI_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oPUMAGFIN_2_17 as StdField with uid="IPBLQAUROZ",rtseq=65,rtrep=.f.,;
    cFormVar = "w_PUMAGFIN", cQueryName = "PUMAGFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di fine selezione",;
    HelpContextID = 248725692,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=509, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PUMAGFIN"

  func oPUMAGFIN_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUMAGFIN_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMAGFIN_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMAGFIN_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPUMAGFIN_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oPUMARINI_2_18 as StdField with uid="YZOGKGSXPU",rtseq=66,rtrep=.f.,;
    cFormVar = "w_PUMARINI", cQueryName = "PUMARINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 186859713,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=185, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PUMARINI"

  func oPUMARINI_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUMARINI_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMARINI_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMARINI_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPUMARINI_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object oPUMARFIN_2_19 as StdField with uid="NRZQAZMNEF",rtseq=67,rtrep=.f.,;
    cFormVar = "w_PUMARFIN", cQueryName = "PUMARFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 237191356,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=509, Top=185, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PUMARFIN"

  func oPUMARFIN_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUMARFIN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUMARFIN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUMARFIN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPUMARFIN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object oPUPROFIN_2_20 as StdCheck with uid="IHWROQAYED",rtseq=68,rtrep=.f.,left=106, top=210, caption="Prodotti finiti",;
    ToolTipText = "Se attivo, elabora i prodotti finiti",;
    HelpContextID = 239210684,;
    cFormVar="w_PUPROFIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUPROFIN_2_20.RadioValue()
    return(iif(this.value =1,'PF',;
    'XX'))
  endfunc
  func oPUPROFIN_2_20.GetRadio()
    this.Parent.oContained.w_PUPROFIN = this.RadioValue()
    return .t.
  endfunc

  func oPUPROFIN_2_20.SetRadio()
    this.Parent.oContained.w_PUPROFIN=trim(this.Parent.oContained.w_PUPROFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PUPROFIN=='PF',1,;
      0)
  endfunc

  add object oPUSEMLAV_2_21 as StdCheck with uid="KTWFDIHQED",rtseq=69,rtrep=.f.,left=263, top=210, caption="Semilavorati",;
    ToolTipText = "Se attivo, elabora i semilavorati",;
    HelpContextID = 126951244,;
    cFormVar="w_PUSEMLAV", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUSEMLAV_2_21.RadioValue()
    return(iif(this.value =1,'SE',;
    'XX'))
  endfunc
  func oPUSEMLAV_2_21.GetRadio()
    this.Parent.oContained.w_PUSEMLAV = this.RadioValue()
    return .t.
  endfunc

  func oPUSEMLAV_2_21.SetRadio()
    this.Parent.oContained.w_PUSEMLAV=trim(this.Parent.oContained.w_PUSEMLAV)
    this.value = ;
      iif(this.Parent.oContained.w_PUSEMLAV=='SE',1,;
      0)
  endfunc

  add object oPUMATPRI_2_22 as StdCheck with uid="SHIEYEJPCW",rtseq=70,rtrep=.f.,left=384, top=210, caption="Materie prime",;
    ToolTipText = "Se attivo, elabora le materie prime",;
    HelpContextID = 201113407,;
    cFormVar="w_PUMATPRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUMATPRI_2_22.RadioValue()
    return(iif(this.value =1,'MP',;
    'XX'))
  endfunc
  func oPUMATPRI_2_22.GetRadio()
    this.Parent.oContained.w_PUMATPRI = this.RadioValue()
    return .t.
  endfunc

  func oPUMATPRI_2_22.SetRadio()
    this.Parent.oContained.w_PUMATPRI=trim(this.Parent.oContained.w_PUMATPRI)
    this.value = ;
      iif(this.Parent.oContained.w_PUMATPRI=='MP',1,;
      0)
  endfunc


  add object oPUELACAT_2_26 as StdCombo with uid="HLYKAPXQGN",rtseq=74,rtrep=.f.,left=660,top=210,width=53,height=21;
    , ToolTipText = "Elabora tutta la catena degli impegni degli ordini che rispettano i filtri di selezione sull'articolo";
    , HelpContextID = 232210250;
    , cFormVar="w_PUELACAT",RowSource=""+"S�,"+"No", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPUELACAT_2_26.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPUELACAT_2_26.GetRadio()
    this.Parent.oContained.w_PUELACAT = this.RadioValue()
    return .t.
  endfunc

  func oPUELACAT_2_26.SetRadio()
    this.Parent.oContained.w_PUELACAT=trim(this.Parent.oContained.w_PUELACAT)
    this.value = ;
      iif(this.Parent.oContained.w_PUELACAT=='S',1,;
      iif(this.Parent.oContained.w_PUELACAT=='N',2,;
      0))
  endfunc

  func oPUELACAT_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R' and ! .w_ED)
    endwith
   endif
  endfunc

  add object oDESMAGI_2_28 as StdField with uid="QONUNYBHAK",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 237433290,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=160, InputMask=replicate('X',30)

  add object oDESMAGF_2_30 as StdField with uid="EBXVFVWORM",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 31002166,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=575, Top=160, InputMask=replicate('X',30)

  add object oDESINI_2_31 as StdField with uid="SSNEPWHMKY",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 77925942,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=35, InputMask=replicate('X',40)

  add object oDESFIN_2_33 as StdField with uid="LMJMLCZYUA",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156372534,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=282, Top=60, InputMask=replicate('X',40)

  add object oDESFAMAI_2_35 as StdField with uid="AOYBYAMKXI",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 131206783,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=85, InputMask=replicate('X',35)

  add object oDESGRUI_2_36 as StdField with uid="ZABORREMDH",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253555146,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=110, InputMask=replicate('X',35)

  add object oDESCATI_2_37 as StdField with uid="ZFWYARIWSP",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 19984842,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=135, InputMask=replicate('X',35)

  add object oDESFAMAF_2_41 as StdField with uid="WOCDSQXKSA",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 131206780,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=575, Top=85, InputMask=replicate('X',35)

  add object oDESGRUF_2_42 as StdField with uid="PYGQUHDJMJ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253555146,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=575, Top=110, InputMask=replicate('X',35)

  add object oDESCATF_2_43 as StdField with uid="SXSGGSHCII",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248450614,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=575, Top=135, InputMask=replicate('X',35)

  add object oDESMARI_2_51 as StdField with uid="MXNLJGFEDS",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESMARI", cQueryName = "DESMARI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52883914,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=173, Top=184, InputMask=replicate('X',35)

  add object oDESMARF_2_53 as StdField with uid="UFBSQRHGVN",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESMARF", cQueryName = "DESMARF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215551542,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=575, Top=184, InputMask=replicate('X',35)

  add object oPUCOMINI_2_54 as StdField with uid="YEMXFLGGUY",rtseq=87,rtrep=.f.,;
    cFormVar = "w_PUCOMINI", cQueryName = "PUCOMINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 191226049,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=137, Top=254, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PUCOMINI"

  func oPUCOMINI_2_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') and .w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCOMINI_2_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCOMINI_2_54.ecpDrop(oSource)
    this.Parent.oContained.link_2_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCOMINI_2_54.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPUCOMINI_2_54'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oPUCOMFIN_2_55 as StdField with uid="TQLURNPFWN",rtseq=88,rtrep=.f.,;
    cFormVar = "w_PUCOMFIN", cQueryName = "PUCOMFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 241557692,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=137, Top=279, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PUCOMFIN"

  func oPUCOMFIN_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') and .w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUCOMFIN_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUCOMFIN_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUCOMFIN_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPUCOMFIN_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oPUCOMODL_2_56 as StdRadio with uid="FZFMTLUTAJ",rtseq=89,rtrep=.f.,left=577, top=254, width=153,height=51;
    , ToolTipText = "Validit� filtro commessa";
    , cFormVar="w_PUCOMODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPUCOMODL_2_56.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 177872706
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 177872706
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 177872706
      this.Buttons(3).Top=32
      this.SetAll("Width",151)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validit� filtro commessa")
      StdRadio::init()
    endproc

  func oPUCOMODL_2_56.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPUCOMODL_2_56.GetRadio()
    this.Parent.oContained.w_PUCOMODL = this.RadioValue()
    return .t.
  endfunc

  func oPUCOMODL_2_56.SetRadio()
    this.Parent.oContained.w_PUCOMODL=trim(this.Parent.oContained.w_PUCOMODL)
    this.value = ;
      iif(this.Parent.oContained.w_PUCOMODL=='O',1,;
      iif(this.Parent.oContained.w_PUCOMODL=='D',2,;
      iif(this.Parent.oContained.w_PUCOMODL=='T',3,;
      0)))
  endfunc

  func oPUCOMODL_2_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_PUCOMINI) or not empty(.w_PUCOMFIN))
    endwith
   endif
  endfunc

  add object oPUNUMINI_2_57 as StdField with uid="YWFWDUKRHP",rtseq=90,rtrep=.f.,;
    cFormVar = "w_PUNUMINI", cQueryName = "PUNUMINI",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 190787777,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=137, Top=304, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oPUNUMINI_2_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUNUMINI_2_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PUNUMINI<=.w_PUNUMFIN and .w_PUNUMINI>0)
    endwith
    return bRes
  endfunc

  add object oPUSERIE1_2_58 as StdField with uid="VCUBODDNSC",rtseq=91,rtrep=.f.,;
    cFormVar = "w_PUSERIE1", cQueryName = "PUSERIE1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di inzio selezione",;
    HelpContextID = 81862439,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=284, Top=304, InputMask=replicate('X',10)

  func oPUSERIE1_2_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUSERIE1_2_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_PUSERIE2)) OR  (.w_PUSERIE1<=.w_PUSERIE2))
    endwith
    return bRes
  endfunc

  add object oPUNUMFIN_2_59 as StdField with uid="RCINERZEKD",rtseq=92,rtrep=.f.,;
    cFormVar = "w_PUNUMFIN", cQueryName = "PUNUMFIN",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 241119420,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=137, Top=329, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oPUNUMFIN_2_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUNUMFIN_2_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PUNUMINI<=.w_PUNUMFIN and .w_PUNUMFIN>0)
    endwith
    return bRes
  endfunc

  add object oPUSERIE2_2_60 as StdField with uid="HJOOAQPZOB",rtseq=93,rtrep=.f.,;
    cFormVar = "w_PUSERIE2", cQueryName = "PUSERIE2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento  di fine selezione",;
    HelpContextID = 81862440,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=284, Top=328, InputMask=replicate('X',10)

  func oPUSERIE2_2_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUSERIE2_2_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PUSERIE2>=.w_PUSERIE1) or (empty(.w_PUSERIE1)))
    endwith
    return bRes
  endfunc

  add object oPUDOCINI_2_61 as StdField with uid="FSPDAHXDHY",rtseq=94,rtrep=.f.,;
    cFormVar = "w_PUDOCINI", cQueryName = "PUDOCINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 201707713,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=480, Top=304

  func oPUDOCINI_2_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUDOCINI_2_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PUDOCINI<=.w_PUDOCFIN or empty(.w_PUDOCFIN))
    endwith
    return bRes
  endfunc

  add object oPUDOCFIN_2_62 as StdField with uid="JBFVSEPHWX",rtseq=95,rtrep=.f.,;
    cFormVar = "w_PUDOCFIN", cQueryName = "PUDOCFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 252039356,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=480, Top=329

  func oPUDOCFIN_2_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUDOCFIN_2_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PUDOCINI<=.w_PUDOCFIN)
    endwith
    return bRes
  endfunc

  add object oPUINICLI_2_63 as StdField with uid="XPINBWSWBI",rtseq=96,rtrep=.f.,;
    cFormVar = "w_PUINICLI", cQueryName = "PUINICLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di inizio selezione",;
    HelpContextID = 27689153,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=137, Top=354, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PUINICLI"

  func oPUINICLI_2_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUINICLI_2_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUINICLI_2_63.ecpDrop(oSource)
    this.Parent.oContained.link_2_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUINICLI_2_63.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPUINICLI_2_63'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPUFINCLI_2_64 as StdField with uid="DGDVSYNEUI",rtseq=97,rtrep=.f.,;
    cFormVar = "w_PUFINCLI", cQueryName = "PUFINCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di fine selezione",;
    HelpContextID = 22786241,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=137, Top=379, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PUFINCLI"

  func oPUFINCLI_2_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUFINCLI_2_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oPUFINCLI_2_64.ecpDrop(oSource)
    this.Parent.oContained.link_2_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPUFINCLI_2_64.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPUFINCLI_2_64'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPUINIELA_2_65 as StdField with uid="MDRRUIPSZB",rtseq=98,rtrep=.f.,;
    cFormVar = "w_PUINIELA", cQueryName = "PUINIELA",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o maggiore della data di fine elaborazione",;
    ToolTipText = "Data di inizio dell'orizzonte temporale di elaborazione",;
    HelpContextID = 262570185,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=137, Top=405

  func oPUINIELA_2_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUINIELA_2_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_PUINIELA) and .w_PUFINELA>=.w_PUINIELA)
    endwith
    return bRes
  endfunc

  add object oPUFINELA_2_66 as StdField with uid="NPUVVATDRF",rtseq=99,rtrep=.f.,;
    cFormVar = "w_PUFINELA", cQueryName = "PUFINELA",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o minore della data di fine elaborazione",;
    ToolTipText = "Data di fine dell'orizzonte temporale di elaborazione",;
    HelpContextID = 257667273,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=137, Top=430

  func oPUFINELA_2_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  func oPUFINELA_2_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_PUFINELA) and .w_PUFINELA>=.w_PUINIELA)
    endwith
    return bRes
  endfunc

  add object oPUORIODL_2_67 as StdRadio with uid="GLGTRBWTXT",rtseq=100,rtrep=.f.,left=225, top=405, width=156,height=50;
    , ToolTipText = "Validit� filtro orizzonte di elaborazione";
    , cFormVar="w_PUORIODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oPUORIODL_2_67.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 173924162
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 173924162
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 173924162
      this.Buttons(3).Top=32
      this.SetAll("Width",154)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validit� filtro orizzonte di elaborazione")
      StdRadio::init()
    endproc

  func oPUORIODL_2_67.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPUORIODL_2_67.GetRadio()
    this.Parent.oContained.w_PUORIODL = this.RadioValue()
    return .t.
  endfunc

  func oPUORIODL_2_67.SetRadio()
    this.Parent.oContained.w_PUORIODL=trim(this.Parent.oContained.w_PUORIODL)
    this.value = ;
      iif(this.Parent.oContained.w_PUORIODL=='O',1,;
      iif(this.Parent.oContained.w_PUORIODL=='D',2,;
      iif(this.Parent.oContained.w_PUORIODL=='T',3,;
      0)))
  endfunc

  func oPUORIODL_2_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUINIELA<>ctod('01-01-1900') or .w_PUFINELA<>ctod('31-12-2099'))
    endwith
   endif
  endfunc


  add object SZOOM as cp_szoombox with uid="JCMGIWLCXL",left=406, top=401, width=402,height=168,;
    caption='SZOOM',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCO_KGP",cTable="TIP_DOCU",bOptions=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , ToolTipText = "Causali documento di impegno";
    , HelpContextID = 73735130

  add object oSELEZI_2_69 as StdRadio with uid="SMNXMFNXXW",rtseq=101,rtrep=.f.,left=571, top=579, width=239,height=20;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_69.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 90218278
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 90218278
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_69.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_69.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_69.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  func oSELEZI_2_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oDESCLII_2_75 as StdField with uid="QWFZIJERXR",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESCLII", cQueryName = "DESCLII",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192999882,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=284, Top=354, InputMask=replicate('X',40)

  add object oDESCLIF_2_76 as StdField with uid="BPAUTKZZUX",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESCLIF", cQueryName = "DESCLIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 75435574,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=284, Top=379, InputMask=replicate('X',40)

  add object oDESCOMI_2_78 as StdField with uid="ZUHXZOJHTP",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESCOMI", cQueryName = "DESCOMI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 122745290,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=284, Top=254, InputMask=replicate('X',30)

  add object oDESCOMF_2_80 as StdField with uid="GOPFERZBTM",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESCOMF", cQueryName = "DESCOMF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 145690166,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=284, Top=279, InputMask=replicate('X',30)


  add object oPUSELIMP_2_88 as StdCombo with uid="OHXXBCHADO",rtseq=106,rtrep=.f.,left=137,top=511,width=146,height=21;
    , HelpContextID = 192864442;
    , cFormVar="w_PUSELIMP",RowSource=""+"Impegni e articoli,"+"Impegni,"+"Articoli", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPUSELIMP_2_88.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPUSELIMP_2_88.GetRadio()
    this.Parent.oContained.w_PUSELIMP = this.RadioValue()
    return .t.
  endfunc

  func oPUSELIMP_2_88.SetRadio()
    this.Parent.oContained.w_PUSELIMP=trim(this.Parent.oContained.w_PUSELIMP)
    this.value = ;
      iif(this.Parent.oContained.w_PUSELIMP=='E',1,;
      iif(this.Parent.oContained.w_PUSELIMP=='I',2,;
      iif(this.Parent.oContained.w_PUSELIMP=='A',3,;
      0)))
  endfunc

  add object oPUORDPRD_2_94 as StdCheck with uid="VPCQFAZNXC",rtseq=113,rtrep=.f.,left=137, top=460, caption="Considera l'ordinato da produzione",;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 185458490,;
    cFormVar="w_PUORDPRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUORDPRD_2_94.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPUORDPRD_2_94.GetRadio()
    this.Parent.oContained.w_PUORDPRD = this.RadioValue()
    return .t.
  endfunc

  func oPUORDPRD_2_94.SetRadio()
    this.Parent.oContained.w_PUORDPRD=trim(this.Parent.oContained.w_PUORDPRD)
    this.value = ;
      iif(this.Parent.oContained.w_PUORDPRD=='S',1,;
      0)
  endfunc

  func oPUORDPRD_2_94.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oPUIMPPRD_2_95 as StdCheck with uid="RYJDILNBMU",rtseq=114,rtrep=.f.,left=137, top=482, caption="Considera l'impegnato da produzione",;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 197689146,;
    cFormVar="w_PUIMPPRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUIMPPRD_2_95.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPUIMPPRD_2_95.GetRadio()
    this.Parent.oContained.w_PUIMPPRD = this.RadioValue()
    return .t.
  endfunc

  func oPUIMPPRD_2_95.SetRadio()
    this.Parent.oContained.w_PUIMPPRD=trim(this.Parent.oContained.w_PUIMPPRD)
    this.value = ;
      iif(this.Parent.oContained.w_PUIMPPRD=='S',1,;
      0)
  endfunc

  func oPUIMPPRD_2_95.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PUMODELA='R')
    endwith
   endif
  endfunc

  add object oStr_2_27 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=6, Top=160,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=419, Top=160,;
    Alignment=1, Width=89, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="NMLJSVRZGE",Visible=.t., Left=6, Top=35,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="XRBSMYKOYR",Visible=.t., Left=6, Top=60,;
    Alignment=1, Width=99, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=6, Top=85,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=6, Top=110,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=6, Top=135,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="BETNLUNOYI",Visible=.t., Left=419, Top=85,;
    Alignment=1, Width=89, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=419, Top=110,;
    Alignment=1, Width=89, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=416, Top=135,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="GKIMPOMLOZ",Visible=.t., Left=663, Top=36,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="EUAAIOZOQT",Visible=.t., Left=713, Top=59,;
    Alignment=1, Width=49, Height=15,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="QTYVSGEGTT",Visible=.t., Left=509, Top=214,;
    Alignment=1, Width=145, Height=21,;
    Caption="Elabora livelli di distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=6, Top=184,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="ZYLPGFCGXX",Visible=.t., Left=419, Top=184,;
    Alignment=1, Width=89, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_70 as StdString with uid="XMTZKDDJKA",Visible=.t., Left=16, Top=405,;
    Alignment=1, Width=117, Height=15,;
    Caption="Data inizio elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="ZFEHJZWTEA",Visible=.t., Left=10, Top=430,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data fine elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_72 as StdString with uid="ZEAIRXNLNG",Visible=.t., Left=294, Top=551,;
    Alignment=1, Width=105, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="LRYBARYNYB",Visible=.t., Left=28, Top=354,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="UVHEDKVHXW",Visible=.t., Left=28, Top=379,;
    Alignment=1, Width=105, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_77 as StdString with uid="FRFBQXOYXD",Visible=.t., Left=28, Top=254,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="FWPPGBGFDX",Visible=.t., Left=28, Top=279,;
    Alignment=1, Width=105, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="QPHXLBLWMU",Visible=.t., Left=271, Top=328,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_82 as StdString with uid="QUWNGCOYRD",Visible=.t., Left=384, Top=306,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_83 as StdString with uid="FSAEEGKAFC",Visible=.t., Left=391, Top=331,;
    Alignment=1, Width=88, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_84 as StdString with uid="TGVEUCHXYZ",Visible=.t., Left=28, Top=304,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_85 as StdString with uid="VZMOFHLMWU",Visible=.t., Left=28, Top=329,;
    Alignment=1, Width=105, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="KLVNNVVWGD",Visible=.t., Left=271, Top=304,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_87 as StdString with uid="JUEPGIFVRE",Visible=.t., Left=9, Top=513,;
    Alignment=1, Width=124, Height=18,;
    Caption="Filtri impegni/articoli:"  ;
  , bGlobalFont=.t.

  add object oStr_2_90 as StdString with uid="NTWHWPMLOA",Visible=.t., Left=18, Top=8,;
    Alignment=0, Width=331, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_2_92 as StdString with uid="CGZLNFUULM",Visible=.t., Left=20, Top=233,;
    Alignment=0, Width=331, Height=18,;
    Caption="Ulteriori selezioni"  ;
  , bGlobalFont=.t.

  add object oBox_2_89 as StdBox with uid="UDTMWKFXRP",left=11, top=25, width=785,height=1

  add object oBox_2_91 as StdBox with uid="MVBPXCGGGS",left=11, top=249, width=785,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_apu','PAR__MRP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PUNUMKEY=PAR__MRP.PUNUMKEY";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
