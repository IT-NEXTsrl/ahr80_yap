* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_kmm                                                        *
*              Messaggi di ripianificazione                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_57]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-08-01                                                      *
* Last revis.: 2018-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsmr_kmm",oParentObject))

* --- Class definition
define class tgsmr_kmm as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 814
  Height = 472+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-19"
  HelpContextID=68588695
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=137

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  cpusers_IDX = 0
  KEY_ARTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  MARCHI_IDX = 0
  CATEGOMO_IDX = 0
  MAGAZZIN_IDX = 0
  PARA_MRP_IDX = 0
  ART_ICOL_IDX = 0
  CAN_TIER_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsmr_kmm"
  cComment = "Messaggi di ripianificazione"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PPCODICE = space(2)
  o_PPCODICE = space(2)
  w_AACODICE = space(2)
  w_CRIFORN = space(1)
  w_PEGGING2 = space(1)
  w_INTERN = .F.
  w_MSGMRP = space(2)
  w_UPDELA = .F.
  w_CRIFORM = space(1)
  w_PPCRIELA = space(1)
  w_PPPERPIA = space(1)
  w_PPPIAPUN = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_MAGTER = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_SALINI = space(40)
  w_SALFIN = space(40)
  w_CONFIGIN = space(1)
  w_CONFIGFI = space(1)
  w_TIPGES = space(1)
  w_TIPOARTI = space(2)
  w_ELABDBF = space(1)
  w_FLCOMM = space(1)
  w_RIGALIS = 0
  w_CHECKINIT = 0
  w_MODELA = space(1)
  o_MODELA = space(1)
  w_MRPLOG = space(1)
  w_MESSRIPIA = space(1)
  w_MICOM = space(1)
  o_MICOM = space(1)
  w_MAGFOR = space(5)
  w_MIODL = space(1)
  o_MIODL = space(1)
  w_MAGFOC = space(5)
  w_MIOCL = space(1)
  w_MAGFOL = space(5)
  w_GENPDA = space(1)
  o_GENPDA = space(1)
  w_MIODA = space(1)
  o_MIODA = space(1)
  w_MAGFOA = space(5)
  w_GENODA = space(1)
  w_DISMAG = space(1)
  o_DISMAG = space(1)
  w_ORDINATO = space(1)
  w_GIANEG = space(1)
  w_ORDMPS = space(1)
  w_STAORD = space(1)
  w_DISMAGFO = space(1)
  w_CRITFORN = space(1)
  w_PMOCLORD = space(1)
  w_PMODLPIA = space(1)
  w_PMODLLAN = space(1)
  w_PMODLSUG = space(1)
  w_PMODAPIA = space(1)
  o_PMODAPIA = space(1)
  w_PMODALAN = space(1)
  o_PMODALAN = space(1)
  w_PMODASUG = space(1)
  w_PMOCLPIA = space(1)
  w_PMOCLSUG = space(1)
  w_PERPIA = space(1)
  w_CRIELA = space(1)
  o_CRIELA = space(1)
  w_PIAPUN = space(1)
  w_SELEZM = space(1)
  o_SELEZM = space(1)
  w_ELABID = space(1)
  w_COMINI = space(15)
  o_COMINI = space(15)
  w_COMFIN = space(15)
  o_COMFIN = space(15)
  w_COMODL = space(1)
  w_NUMINI = 0
  o_NUMINI = 0
  w_SERIE1 = space(10)
  o_SERIE1 = space(10)
  w_NUMFIN = 0
  o_NUMFIN = 0
  w_SERIE2 = space(10)
  o_SERIE2 = space(10)
  w_DOCINI = ctod('  /  /  ')
  o_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  o_DOCFIN = ctod('  /  /  ')
  w_INICLI = space(15)
  o_INICLI = space(15)
  w_FINCLI = space(15)
  o_FINCLI = space(15)
  w_INIELA = ctod('  /  /  ')
  o_INIELA = ctod('  /  /  ')
  w_FINELA = ctod('  /  /  ')
  o_FINELA = ctod('  /  /  ')
  w_ORIODL = space(1)
  w_SELEZI = space(1)
  w_DESCLII = space(40)
  w_DESCLIF = space(40)
  w_DESCOMI = space(30)
  w_DESCOMF = space(30)
  w_SELEIMPE = space(1)
  w_TIPCON = space(1)
  w_CODINI = space(20)
  o_CODINI = space(20)
  w_CODFIN = space(20)
  o_CODFIN = space(20)
  w_LLCINI = 0
  o_LLCINI = 0
  w_LLCFIN = 0
  o_LLCFIN = 0
  w_FAMAINI = space(5)
  o_FAMAINI = space(5)
  w_FAMAFIN = space(5)
  o_FAMAFIN = space(5)
  w_GRUINI = space(5)
  o_GRUINI = space(5)
  w_GRUFIN = space(5)
  o_GRUFIN = space(5)
  w_CATINI = space(5)
  o_CATINI = space(5)
  w_CATFIN = space(5)
  o_CATFIN = space(5)
  w_MAGINI = space(5)
  o_MAGINI = space(5)
  w_MAGFIN = space(5)
  o_MAGFIN = space(5)
  w_MARINI = space(5)
  o_MARINI = space(5)
  w_MARFIN = space(5)
  o_MARFIN = space(5)
  w_PROFIN = space(2)
  o_PROFIN = space(2)
  w_SEMLAV = space(2)
  o_SEMLAV = space(2)
  w_MATPRI = space(2)
  o_MATPRI = space(2)
  w_STIPART = space(1)
  o_STIPART = space(1)
  w_ED = .F.
  o_ED = .F.
  w_ELACAT = space(1)
  w_DESMAGI = space(30)
  w_DESMAGF = space(30)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DESMARI = space(35)
  w_DESMARF = space(35)
  w_ELAODL = ctod('  /  /  ')
  w_ORAODL = space(8)
  w_OPEODL = 0
  w_ELAMPS = ctod('  /  /  ')
  w_ORAMPS = space(8)
  w_OPEMPS = 0
  w_DESUTE = space(20)
  w_CAUSALI = 0
  o_CAUSALI = 0
  w_TIPDOCU = space(0)
  w_ORDIPROD = space(1)
  w_IMPEPROD = space(1)
  w_PPMATOUP = space(1)
  w_DESMFR = space(30)
  w_DESMOC = space(30)
  w_DESMOL = space(30)
  w_DESMOA = space(30)
  w_NOTMRP = space(0)
  w_CHECKDATI = space(1)
  w_TIPPAR = space(1)
  w_RET = .F.
  w_MAGAZZINI = 0
  w_LISTMAGA = space(0)
  w_RET = .F.
  w_MAGAZZINI = 0
  w_LISTMAGA = space(0)
  w_ZOOMMAGA = .NULL.
  w_LBLMAGA = .NULL.
  w_SZOOM = .NULL.
  w_oLinePB = .NULL.
  w_PROGBAR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsmr_kmm
  w_KEYRIF = ""
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsmr_kmmPag1","gsmr_kmm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modalità di elaborazione")
      .Pages(2).addobject("oPag","tgsmr_kmmPag2","gsmr_kmm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMRPLOG_1_29
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMMAGA = this.oPgFrm.Pages(1).oPag.ZOOMMAGA
    this.w_LBLMAGA = this.oPgFrm.Pages(1).oPag.LBLMAGA
    this.w_SZOOM = this.oPgFrm.Pages(2).oPag.SZOOM
    this.w_oLinePB = this.oPgFrm.Pages(1).oPag.oLinePB
    this.w_PROGBAR = this.oPgFrm.Pages(1).oPag.PROGBAR
    DoDefault()
    proc Destroy()
      this.w_ZOOMMAGA = .NULL.
      this.w_LBLMAGA = .NULL.
      this.w_SZOOM = .NULL.
      this.w_oLinePB = .NULL.
      this.w_PROGBAR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='CATEGOMO'
    this.cWorkTables[8]='MAGAZZIN'
    this.cWorkTables[9]='PARA_MRP'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='CAN_TIER'
    this.cWorkTables[12]='CONTI'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSMR2BGP(this,"GSMR_BGP")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE=space(2)
      .w_AACODICE=space(2)
      .w_CRIFORN=space(1)
      .w_PEGGING2=space(1)
      .w_INTERN=.f.
      .w_MSGMRP=space(2)
      .w_UPDELA=.f.
      .w_CRIFORM=space(1)
      .w_PPCRIELA=space(1)
      .w_PPPERPIA=space(1)
      .w_PPPIAPUN=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_MAGTER=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_SALINI=space(40)
      .w_SALFIN=space(40)
      .w_CONFIGIN=space(1)
      .w_CONFIGFI=space(1)
      .w_TIPGES=space(1)
      .w_TIPOARTI=space(2)
      .w_ELABDBF=space(1)
      .w_FLCOMM=space(1)
      .w_RIGALIS=0
      .w_CHECKINIT=0
      .w_MODELA=space(1)
      .w_MRPLOG=space(1)
      .w_MESSRIPIA=space(1)
      .w_MICOM=space(1)
      .w_MAGFOR=space(5)
      .w_MIODL=space(1)
      .w_MAGFOC=space(5)
      .w_MIOCL=space(1)
      .w_MAGFOL=space(5)
      .w_GENPDA=space(1)
      .w_MIODA=space(1)
      .w_MAGFOA=space(5)
      .w_GENODA=space(1)
      .w_DISMAG=space(1)
      .w_ORDINATO=space(1)
      .w_GIANEG=space(1)
      .w_ORDMPS=space(1)
      .w_STAORD=space(1)
      .w_DISMAGFO=space(1)
      .w_CRITFORN=space(1)
      .w_PMOCLORD=space(1)
      .w_PMODLPIA=space(1)
      .w_PMODLLAN=space(1)
      .w_PMODLSUG=space(1)
      .w_PMODAPIA=space(1)
      .w_PMODALAN=space(1)
      .w_PMODASUG=space(1)
      .w_PMOCLPIA=space(1)
      .w_PMOCLSUG=space(1)
      .w_PERPIA=space(1)
      .w_CRIELA=space(1)
      .w_PIAPUN=space(1)
      .w_SELEZM=space(1)
      .w_ELABID=space(1)
      .w_COMINI=space(15)
      .w_COMFIN=space(15)
      .w_COMODL=space(1)
      .w_NUMINI=0
      .w_SERIE1=space(10)
      .w_NUMFIN=0
      .w_SERIE2=space(10)
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_INICLI=space(15)
      .w_FINCLI=space(15)
      .w_INIELA=ctod("  /  /  ")
      .w_FINELA=ctod("  /  /  ")
      .w_ORIODL=space(1)
      .w_SELEZI=space(1)
      .w_DESCLII=space(40)
      .w_DESCLIF=space(40)
      .w_DESCOMI=space(30)
      .w_DESCOMF=space(30)
      .w_SELEIMPE=space(1)
      .w_TIPCON=space(1)
      .w_CODINI=space(20)
      .w_CODFIN=space(20)
      .w_LLCINI=0
      .w_LLCFIN=0
      .w_FAMAINI=space(5)
      .w_FAMAFIN=space(5)
      .w_GRUINI=space(5)
      .w_GRUFIN=space(5)
      .w_CATINI=space(5)
      .w_CATFIN=space(5)
      .w_MAGINI=space(5)
      .w_MAGFIN=space(5)
      .w_MARINI=space(5)
      .w_MARFIN=space(5)
      .w_PROFIN=space(2)
      .w_SEMLAV=space(2)
      .w_MATPRI=space(2)
      .w_STIPART=space(1)
      .w_ED=.f.
      .w_ELACAT=space(1)
      .w_DESMAGI=space(30)
      .w_DESMAGF=space(30)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DESMARI=space(35)
      .w_DESMARF=space(35)
      .w_ELAODL=ctod("  /  /  ")
      .w_ORAODL=space(8)
      .w_OPEODL=0
      .w_ELAMPS=ctod("  /  /  ")
      .w_ORAMPS=space(8)
      .w_OPEMPS=0
      .w_DESUTE=space(20)
      .w_CAUSALI=0
      .w_TIPDOCU=space(0)
      .w_ORDIPROD=space(1)
      .w_IMPEPROD=space(1)
      .w_PPMATOUP=space(1)
      .w_DESMFR=space(30)
      .w_DESMOC=space(30)
      .w_DESMOL=space(30)
      .w_DESMOA=space(30)
      .w_NOTMRP=space(0)
      .w_CHECKDATI=space(1)
      .w_TIPPAR=space(1)
      .w_RET=.f.
      .w_MAGAZZINI=0
      .w_LISTMAGA=space(0)
      .w_RET=.f.
      .w_MAGAZZINI=0
      .w_LISTMAGA=space(0)
        .w_PPCODICE = "PP"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_1('Full')
        endif
        .w_AACODICE = "AA"
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AACODICE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_PEGGING2 = "N"
        .w_INTERN = FALSE
        .w_MSGMRP = "EX"
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MSGMRP))
          .link_1_8('Full')
        endif
        .w_UPDELA = FALSE
          .DoRTCalc(8,11,.f.)
        .w_OBTEST = i_DATSYS
        .w_TIPATT = 'A'
          .DoRTCalc(14,19,.f.)
        .w_TIPGES = "G"
          .DoRTCalc(21,21,.f.)
        .w_ELABDBF = 'S'
          .DoRTCalc(23,24,.f.)
        .w_CHECKINIT = 0
        .w_MODELA = 'R'
        .w_MRPLOG = g_ATTIVAMRPLOG
        .w_MESSRIPIA = 'S'
        .w_MICOM = 'M'
        .w_MAGFOR = SPACE(5)
        .w_MIODL = 'M'
        .w_MAGFOC = SPACE(5)
        .w_MIOCL = 'M'
        .w_MAGFOL = SPACE(5)
        .w_GENPDA = 'S'
        .w_MIODA = 'M'
        .w_MAGFOA = SPACE(5)
        .w_GENODA = 'N'
        .w_DISMAG = 'S'
        .w_ORDINATO = 'S'
        .w_GIANEG = 'S'
        .w_ORDMPS = 'N'
        .w_STAORD = 'S'
          .DoRTCalc(44,44,.f.)
        .w_CRITFORN = 'T'
          .DoRTCalc(46,54,.f.)
        .w_PERPIA = .w_PPPERPIA
        .w_CRIELA = .w_PPCRIELA
        .w_PIAPUN = .w_PPPIAPUN
      .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
      .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .w_SELEZM = "S"
        .w_ELABID = 'N'
        .w_COMINI = space(15)
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_COMINI))
          .link_2_1('Full')
        endif
        .w_COMFIN = .w_COMINI
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_COMFIN))
          .link_2_2('Full')
        endif
        .w_COMODL = 'T'
        .w_NUMINI = 1
        .w_SERIE1 = ''
        .w_NUMFIN = 999999999999999
        .w_SERIE2 = ''
        .w_DOCINI = ctod('')
        .w_DOCFIN = .w_DOCINI
        .w_INICLI = ' '
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_INICLI))
          .link_2_10('Full')
        endif
        .w_FINCLI = .w_INICLI
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_FINCLI))
          .link_2_11('Full')
        endif
        .w_INIELA = i_INIDAT
        .w_FINELA = cp_CharToDate('31-12-2099')
        .w_ORIODL = 'T'
      .oPgFrm.Page2.oPag.SZOOM.Calculate()
        .w_SELEZI = "S"
          .DoRTCalc(75,78,.f.)
        .w_SELEIMPE = 'A'
        .w_TIPCON = 'C'
        .w_CODINI = ' '
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_CODINI))
          .link_1_67('Full')
        endif
        .w_CODFIN = .w_CODINI
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_CODFIN))
          .link_1_68('Full')
        endif
        .w_LLCINI = 0
        .w_LLCFIN = 999
        .w_FAMAINI = ' '
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_FAMAINI))
          .link_1_71('Full')
        endif
        .w_FAMAFIN = .w_FAMAINI
        .DoRTCalc(86,86,.f.)
        if not(empty(.w_FAMAFIN))
          .link_1_72('Full')
        endif
        .w_GRUINI = ' '
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_GRUINI))
          .link_1_73('Full')
        endif
        .w_GRUFIN = .w_GRUINI
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_GRUFIN))
          .link_1_74('Full')
        endif
        .w_CATINI = ' '
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_CATINI))
          .link_1_75('Full')
        endif
        .w_CATFIN = .w_CATINI
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_CATFIN))
          .link_1_76('Full')
        endif
        .w_MAGINI = ' '
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_MAGINI))
          .link_1_77('Full')
        endif
        .w_MAGFIN = .w_MAGINI
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_78('Full')
        endif
        .w_MARINI = ' '
        .DoRTCalc(93,93,.f.)
        if not(empty(.w_MARINI))
          .link_1_79('Full')
        endif
        .w_MARFIN = .w_MARINI
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_MARFIN))
          .link_1_80('Full')
        endif
        .w_PROFIN = 'PF'
        .w_SEMLAV = 'SE'
        .w_MATPRI = 'MP'
        .w_STIPART = iif((empty(.w_PROFIN)or .w_PROFIN='PF') and (empty(.w_SEMLAV)or .w_SEMLAV='SE') and (empty(.w_MATPRI)or .w_MATPRI='MP'),'S','N')
        .w_ED = empty(.w_CODINI) and empty (.w_CODFIN) and empty(.w_FAMAINI) and empty(.w_FAMAFIN) and empty(.w_GRUINI) and empty(.w_GRUFIN) and empty(.w_CATINI) and empty(.w_CATFIN) and .w_LLCINI=0 and .w_LLCFIN=999 and empty(.w_MAGINI) and empty(.w_MAGFIN) and empty(.w_MARINI)and empty(.w_MARFIN) and .w_STIPART<>'N'
        .w_ELACAT = iif(! .w_ED, 'N','S')
          .DoRTCalc(101,119,.f.)
        .w_CAUSALI = 0
        .w_TIPDOCU = " ' ' "
        .w_ORDIPROD = 'S'
        .w_IMPEPROD = 'S'
      .oPgFrm.Page1.oPag.oLinePB.Calculate()
      .oPgFrm.Page1.oPag.PROGBAR.Calculate()
          .DoRTCalc(124,129,.f.)
        .w_CHECKDATI = "N"
        .w_TIPPAR = 'D'
          .DoRTCalc(132,132,.f.)
        .w_MAGAZZINI = 0
        .w_LISTMAGA = " ' ' "
          .DoRTCalc(135,135,.f.)
        .w_MAGAZZINI = 0
        .w_LISTMAGA = ""
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_43.enabled = this.oPgFrm.Page2.oPag.oBtn_2_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,5,.t.)
            .w_MSGMRP = "EX"
          .link_1_8('Full')
        .DoRTCalc(7,27,.t.)
        if .o_PMODAPIA<>.w_PMODAPIA.or. .o_PMODALAN<>.w_PMODALAN
            .w_MESSRIPIA = 'S'
        endif
        .DoRTCalc(29,29,.t.)
        if .o_MICOM<>.w_MICOM
            .w_MAGFOR = SPACE(5)
        endif
        .DoRTCalc(31,31,.t.)
        if .o_MIODL<>.w_MIODL
            .w_MAGFOC = SPACE(5)
        endif
        .DoRTCalc(33,33,.t.)
            .w_MAGFOL = SPACE(5)
        .DoRTCalc(35,36,.t.)
        if .o_MIODA<>.w_MIODA
            .w_MAGFOA = SPACE(5)
        endif
        if .o_GENPDA<>.w_GENPDA
            .w_GENODA = 'N'
        endif
        if .o_MODELA<>.w_MODELA
            .w_DISMAG = 'S'
        endif
        if .o_MODELA<>.w_MODELA
            .w_ORDINATO = 'S'
        endif
        if .o_DISMAG<>.w_DISMAG.or. .o_MODELA<>.w_MODELA
            .w_GIANEG = 'S'
        endif
        if .o_MODELA<>.w_MODELA
            .w_ORDMPS = 'N'
        endif
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .DoRTCalc(43,58,.t.)
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4,l_Dep5,l_Dep6,l_Dep7
        l_Dep1= .o_MODELA<>.w_MODELA .or. .o_INICLI<>.w_INICLI .or. .o_FINCLI<>.w_FINCLI .or. .o_INIELA<>.w_INIELA .or. .o_FINELA<>.w_FINELA        l_Dep2= .o_CAUSALI<>.w_CAUSALI .or. .o_ED<>.w_ED .or. .o_CODINI<>.w_CODINI .or. .o_CODFIN<>.w_CODFIN .or. .o_FAMAINI<>.w_FAMAINI        l_Dep3= .o_FAMAFIN<>.w_FAMAFIN .or. .o_GRUINI<>.w_GRUINI .or. .o_GRUFIN<>.w_GRUFIN .or. .o_CATINI<>.w_CATINI .or. .o_CATFIN<>.w_CATFIN        l_Dep4= .o_LLCINI<>.w_LLCINI .or. .o_LLCFIN<>.w_LLCFIN .or. .o_MAGINI<>.w_MAGINI .or. .o_MAGFIN<>.w_MAGFIN .or. .o_MARINI<>.w_MARINI        l_Dep5= .o_MARFIN<>.w_MARFIN .or. .o_COMINI<>.w_COMINI .or. .o_COMFIN<>.w_COMFIN .or. .o_NUMINI<>.w_NUMINI .or. .o_NUMFIN<>.w_NUMFIN        l_Dep6= .o_SERIE1<>.w_SERIE1 .or. .o_SERIE2<>.w_SERIE2 .or. .o_DOCINI<>.w_DOCINI .or. .o_DOCFIN<>.w_DOCFIN .or. .o_STIPART<>.w_STIPART        l_Dep7= .o_PROFIN<>.w_PROFIN .or. .o_SEMLAV<>.w_SEMLAV .or. .o_MATPRI<>.w_MATPRI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4 .or. m.l_Dep5 .or. m.l_Dep6 .or. m.l_Dep7
            .w_ELABID = 'N'
        endif
        if .o_MODELA<>.w_MODELA
            .w_COMINI = space(15)
          .link_2_1('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_COMINI<>.w_COMINI
            .w_COMFIN = .w_COMINI
          .link_2_2('Full')
        endif
        if .o_COMINI<>.w_COMINI.or. .o_COMFIN<>.w_COMFIN
            .w_COMODL = 'T'
        endif
        if .o_MODELA<>.w_MODELA
            .w_NUMINI = 1
        endif
        if .o_MODELA<>.w_MODELA
            .w_SERIE1 = ''
        endif
        if .o_MODELA<>.w_MODELA
            .w_NUMFIN = 999999999999999
        endif
        if .o_MODELA<>.w_MODELA
            .w_SERIE2 = ''
        endif
        if .o_MODELA<>.w_MODELA
            .w_DOCINI = ctod('')
        endif
        if .o_DOCINI<>.w_DOCINI.or. .o_MODELA<>.w_MODELA
            .w_DOCFIN = .w_DOCINI
        endif
        if .o_MODELA<>.w_MODELA
            .w_INICLI = ' '
          .link_2_10('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_INICLI<>.w_INICLI
            .w_FINCLI = .w_INICLI
          .link_2_11('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_INIELA = i_INIDAT
        endif
        if .o_MODELA<>.w_MODELA
            .w_FINELA = cp_CharToDate('31-12-2099')
        endif
        if .o_INIELA<>.w_INIELA.or. .o_FINELA<>.w_FINELA
            .w_ORIODL = 'T'
        endif
        if .o_MODELA<>.w_MODELA
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
        endif
        .DoRTCalc(74,80,.t.)
        if .o_MODELA<>.w_MODELA
            .w_CODINI = ' '
          .link_1_67('Full')
        endif
        if .o_CODINI<>.w_CODINI.or. .o_MODELA<>.w_MODELA
            .w_CODFIN = .w_CODINI
          .link_1_68('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_LLCINI = 0
        endif
        if .o_MODELA<>.w_MODELA.or. .o_LLCINI<>.w_LLCINI
            .w_LLCFIN = 999
        endif
        if .o_MODELA<>.w_MODELA
            .w_FAMAINI = ' '
          .link_1_71('Full')
        endif
        if .o_FAMAINI<>.w_FAMAINI.or. .o_MODELA<>.w_MODELA
            .w_FAMAFIN = .w_FAMAINI
          .link_1_72('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_GRUINI = ' '
          .link_1_73('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_GRUINI<>.w_GRUINI
            .w_GRUFIN = .w_GRUINI
          .link_1_74('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_CATINI = ' '
          .link_1_75('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_CATINI<>.w_CATINI
            .w_CATFIN = .w_CATINI
          .link_1_76('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_MAGINI = ' '
          .link_1_77('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_MAGINI<>.w_MAGINI
            .w_MAGFIN = .w_MAGINI
          .link_1_78('Full')
        endif
        if .o_MODELA<>.w_MODELA
            .w_MARINI = ' '
          .link_1_79('Full')
        endif
        if .o_MODELA<>.w_MODELA.or. .o_MARINI<>.w_MARINI
            .w_MARFIN = .w_MARINI
          .link_1_80('Full')
        endif
        .DoRTCalc(95,97,.t.)
        if .o_PROFIN<>.w_PROFIN.or. .o_SEMLAV<>.w_SEMLAV.or. .o_MATPRI<>.w_MATPRI
            .w_STIPART = iif((empty(.w_PROFIN)or .w_PROFIN='PF') and (empty(.w_SEMLAV)or .w_SEMLAV='SE') and (empty(.w_MATPRI)or .w_MATPRI='MP'),'S','N')
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4
        l_Dep1= .o_CODINI<>.w_CODINI .or. .o_CODFIN<>.w_CODFIN .or. .o_FAMAINI<>.w_FAMAINI .or. .o_FAMAFIN<>.w_FAMAFIN .or. .o_GRUINI<>.w_GRUINI        l_Dep2= .o_GRUFIN<>.w_GRUFIN .or. .o_CATINI<>.w_CATINI .or. .o_CATFIN<>.w_CATFIN .or. .o_LLCINI<>.w_LLCINI .or. .o_LLCFIN<>.w_LLCFIN        l_Dep3= .o_MAGINI<>.w_MAGINI .or. .o_MAGFIN<>.w_MAGFIN .or. .o_MARINI<>.w_MARINI .or. .o_MARFIN<>.w_MARFIN .or. .o_STIPART<>.w_STIPART        l_Dep4= .o_PROFIN<>.w_PROFIN .or. .o_SEMLAV<>.w_SEMLAV .or. .o_MATPRI<>.w_MATPRI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4
            .w_ED = empty(.w_CODINI) and empty (.w_CODFIN) and empty(.w_FAMAINI) and empty(.w_FAMAFIN) and empty(.w_GRUINI) and empty(.w_GRUFIN) and empty(.w_CATINI) and empty(.w_CATFIN) and .w_LLCINI=0 and .w_LLCFIN=999 and empty(.w_MAGINI) and empty(.w_MAGFIN) and empty(.w_MARINI)and empty(.w_MARFIN) and .w_STIPART<>'N'
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4
        l_Dep1= .o_MODELA<>.w_MODELA .or. .o_ED<>.w_ED .or. .o_CODINI<>.w_CODINI .or. .o_CODFIN<>.w_CODFIN .or. .o_FAMAINI<>.w_FAMAINI        l_Dep2= .o_FAMAFIN<>.w_FAMAFIN .or. .o_GRUINI<>.w_GRUINI .or. .o_GRUFIN<>.w_GRUFIN .or. .o_CATINI<>.w_CATINI .or. .o_CATFIN<>.w_CATFIN        l_Dep3= .o_LLCINI<>.w_LLCINI .or. .o_LLCFIN<>.w_LLCFIN .or. .o_MAGINI<>.w_MAGINI .or. .o_MAGFIN<>.w_MAGFIN .or. .o_MARINI<>.w_MARINI        l_Dep4= .o_MARFIN<>.w_MARFIN .or. .o_STIPART<>.w_STIPART .or. .o_PROFIN<>.w_PROFIN .or. .o_SEMLAV<>.w_SEMLAV .or. .o_MATPRI<>.w_MATPRI
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4
            .w_ELACAT = iif(! .w_ED, 'N','S')
        endif
        .DoRTCalc(101,121,.t.)
        if .o_MODELA<>.w_MODELA
            .w_ORDIPROD = 'S'
        endif
        if .o_MODELA<>.w_MODELA
            .w_IMPEPROD = 'S'
        endif
        .oPgFrm.Page1.oPag.oLinePB.Calculate()
        .oPgFrm.Page1.oPag.PROGBAR.Calculate()
        if .o_CRIELA<>.w_CRIELA
          .Calculate_GGOBXLBQED()
        endif
        if .o_CRIELA<>.w_CRIELA
          .Calculate_UTEOLFGEUH()
        endif
        if .o_SELEZM<>.w_SELEZM
          .Calculate_LOGQKNSSGF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(124,137,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMMAGA.Calculate()
        .oPgFrm.Page1.oPag.LBLMAGA.Calculate(AH_msgFormat(iif(.w_CRIELA='G', "Gruppi di Magazzino", "Magazzini di impegno")))
        .oPgFrm.Page2.oPag.SZOOM.Calculate()
        .oPgFrm.Page1.oPag.oLinePB.Calculate()
        .oPgFrm.Page1.oPag.PROGBAR.Calculate()
    endwith
  return

  proc Calculate_KXINMQZUHU()
    with this
          * --- GSMR2BGP("S") - w_szoom row unchecked,w_szoom row checked,w_SELEZI Changed
          GSMR2BGP(this;
              ,"S";
             )
    endwith
  endproc
  proc Calculate_GFKRVWEZOJ()
    with this
          * --- GSMR_BEX("SS") - w_SELEZI Changed,Blank
          GSMR_BEX(this;
              ,"SS";
             )
    endwith
  endproc
  proc Calculate_GGOBXLBQED()
    with this
          * --- Condizione di editing Label e zoom
          GSAR_BFM(this;
              ,"APERTURA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
             )
          .w_ZOOMMAGA.cZoomFile = IIF(.w_CRIELA = 'G', "GSVEGKGF" , "GSVEMKGF")
          .w_ZOOMMAGA.cCpQueryName = IIF(.w_CRIELA = 'G', "QUERY\GSVEGNKGF" , "QUERY\GSVEFNKGF")
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_QYNAVQZUKT()
    with this
          * --- GSMR2BGP("I") - GSMA_BFM - Init
          GSMR2BGP(this;
              ,"I";
             )
    endwith
  endproc
  proc Calculate_EQFKMCSOLG()
    with this
          * --- GSMR2BGP("I") - GSMA_BFM - Init
      if .w_CRIELA $ 'G-M' AND !Empty(.w_LISTMAGA)
          GSAR_BFM(this;
              ,"CARICADATO2";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,"N";
              ,.w_LISTMAGA;
             )
      endif
      if Empty(.w_LISTMAGA)
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
          .w_RET = .NotifyEvent("InterrogaMaga")
    endwith
  endproc
  proc Calculate_YRRAPCTJJG()
    with this
          * --- Valorizzazione w_KEYRIF
          .w_KEYRIF = SYS(2015)
    endwith
  endproc
  proc Calculate_XLADYXDWQO()
    with this
          * --- Visualizza la progressbar
          .w_PROGBAR.Visible = g_UseProgBar
    endwith
  endproc
  proc Calculate_LYAVVYYQCV()
    with this
          * --- Done
          GSAR_BFM(this;
              ,"ESCI";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_XOHCHPGRKK()
    with this
          * --- Condizione di editing Label e zoom
          .w_LBLMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.Enabled = IIF(.w_CRIELA $ 'G-M', .T., .F.)
          .w_ZOOMMAGA.GRD.Enabled = .w_ZOOMMAGA.Enabled
    endwith
  endproc
  proc Calculate_UTEOLFGEUH()
    with this
          * --- w_CRIELA Changed - Forza seleziona tutto magaz
          .o_SELEZM = "D"
          .w_SELEZM = "S"
    endwith
  endproc
  proc Calculate_LOGQKNSSGF()
    with this
          * --- w_SELEZM Changed - Esegue CheckAll su Magaz
      if .w_SELEZM='S'
          .w_RET = .w_ZOOMMAGA.checkall()
      endif
      if .w_SELEZM='D'
          .w_RET = .w_ZOOMMAGA.uncheckall()
      endif
    endwith
  endproc
  proc Calculate_LRBMCECHHD()
    with this
          * --- Gestione filtri magazzino MAGA_TEMP
          GSAR_BFM(this;
              ,"AGGIORNA";
              ,.w_KEYRIF;
              ,.w_CRIELA;
              ,'N';
             )
          GSMR2BGP(this;
              ,"S";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSELEZM_1_64.enabled_(this.oPgFrm.Page1.oPag.oSELEZM_1_64.mCond())
    this.oPgFrm.Page2.oPag.oCOMINI_2_1.enabled = this.oPgFrm.Page2.oPag.oCOMINI_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCOMFIN_2_2.enabled = this.oPgFrm.Page2.oPag.oCOMFIN_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCOMODL_2_3.enabled_(this.oPgFrm.Page2.oPag.oCOMODL_2_3.mCond())
    this.oPgFrm.Page2.oPag.oNUMINI_2_4.enabled = this.oPgFrm.Page2.oPag.oNUMINI_2_4.mCond()
    this.oPgFrm.Page2.oPag.oSERIE1_2_5.enabled = this.oPgFrm.Page2.oPag.oSERIE1_2_5.mCond()
    this.oPgFrm.Page2.oPag.oNUMFIN_2_6.enabled = this.oPgFrm.Page2.oPag.oNUMFIN_2_6.mCond()
    this.oPgFrm.Page2.oPag.oSERIE2_2_7.enabled = this.oPgFrm.Page2.oPag.oSERIE2_2_7.mCond()
    this.oPgFrm.Page2.oPag.oDOCINI_2_8.enabled = this.oPgFrm.Page2.oPag.oDOCINI_2_8.mCond()
    this.oPgFrm.Page2.oPag.oDOCFIN_2_9.enabled = this.oPgFrm.Page2.oPag.oDOCFIN_2_9.mCond()
    this.oPgFrm.Page2.oPag.oINICLI_2_10.enabled = this.oPgFrm.Page2.oPag.oINICLI_2_10.mCond()
    this.oPgFrm.Page2.oPag.oFINCLI_2_11.enabled = this.oPgFrm.Page2.oPag.oFINCLI_2_11.mCond()
    this.oPgFrm.Page2.oPag.oINIELA_2_12.enabled = this.oPgFrm.Page2.oPag.oINIELA_2_12.mCond()
    this.oPgFrm.Page2.oPag.oFINELA_2_13.enabled = this.oPgFrm.Page2.oPag.oFINELA_2_13.mCond()
    this.oPgFrm.Page2.oPag.oORIODL_2_14.enabled_(this.oPgFrm.Page2.oPag.oORIODL_2_14.mCond())
    this.oPgFrm.Page2.oPag.oSELEZI_2_16.enabled_(this.oPgFrm.Page2.oPag.oSELEZI_2_16.mCond())
    this.oPgFrm.Page1.oPag.oCODINI_1_67.enabled = this.oPgFrm.Page1.oPag.oCODINI_1_67.mCond()
    this.oPgFrm.Page1.oPag.oCODFIN_1_68.enabled = this.oPgFrm.Page1.oPag.oCODFIN_1_68.mCond()
    this.oPgFrm.Page1.oPag.oLLCINI_1_69.enabled = this.oPgFrm.Page1.oPag.oLLCINI_1_69.mCond()
    this.oPgFrm.Page1.oPag.oLLCFIN_1_70.enabled = this.oPgFrm.Page1.oPag.oLLCFIN_1_70.mCond()
    this.oPgFrm.Page1.oPag.oFAMAINI_1_71.enabled = this.oPgFrm.Page1.oPag.oFAMAINI_1_71.mCond()
    this.oPgFrm.Page1.oPag.oFAMAFIN_1_72.enabled = this.oPgFrm.Page1.oPag.oFAMAFIN_1_72.mCond()
    this.oPgFrm.Page1.oPag.oGRUINI_1_73.enabled = this.oPgFrm.Page1.oPag.oGRUINI_1_73.mCond()
    this.oPgFrm.Page1.oPag.oGRUFIN_1_74.enabled = this.oPgFrm.Page1.oPag.oGRUFIN_1_74.mCond()
    this.oPgFrm.Page1.oPag.oCATINI_1_75.enabled = this.oPgFrm.Page1.oPag.oCATINI_1_75.mCond()
    this.oPgFrm.Page1.oPag.oCATFIN_1_76.enabled = this.oPgFrm.Page1.oPag.oCATFIN_1_76.mCond()
    this.oPgFrm.Page1.oPag.oMAGINI_1_77.enabled = this.oPgFrm.Page1.oPag.oMAGINI_1_77.mCond()
    this.oPgFrm.Page1.oPag.oMAGFIN_1_78.enabled = this.oPgFrm.Page1.oPag.oMAGFIN_1_78.mCond()
    this.oPgFrm.Page1.oPag.oMARINI_1_79.enabled = this.oPgFrm.Page1.oPag.oMARINI_1_79.mCond()
    this.oPgFrm.Page1.oPag.oMARFIN_1_80.enabled = this.oPgFrm.Page1.oPag.oMARFIN_1_80.mCond()
    this.oPgFrm.Page1.oPag.oELACAT_1_86.enabled = this.oPgFrm.Page1.oPag.oELACAT_1_86.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMMAGA.Event(cEvent)
      .oPgFrm.Page1.oPag.LBLMAGA.Event(cEvent)
      .oPgFrm.Page2.oPag.SZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_szoom row unchecked") or lower(cEvent)==lower("w_szoom row checked") or lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_KXINMQZUHU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZI Changed") or lower(cEvent)==lower("Blank")
          .Calculate_GFKRVWEZOJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oLinePB.Event(cEvent)
      .oPgFrm.Page1.oPag.PROGBAR.Event(cEvent)
        if lower(cEvent)==lower("AGZMAG") or lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_GGOBXLBQED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_QYNAVQZUKT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CFGLoaded") or lower(cEvent)==lower("Init")
          .Calculate_EQFKMCSOLG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_YRRAPCTJJG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_XLADYXDWQO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_LYAVVYYQCV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AGZMAG") or lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_CRIELA Changed") or lower(cEvent)==lower("w_ZOOMMAGA after query")
          .Calculate_XOHCHPGRKK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CRIELA Changed")
          .Calculate_UTEOLFGEUH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZM Changed")
          .Calculate_LOGQKNSSGF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMMAGA menucheck") or lower(cEvent)==lower("w_ZOOMMAGA row checked") or lower(cEvent)==lower("w_ZOOMMAGA row unchecked")
          .Calculate_LRBMCECHHD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsmr_kmm
    if upper(cEvent)='FORMLOAD'
      this.w_ZOOMMAGA.GRD.SCROLLBARS=2
      this.w_SZOOM.GRD.SCROLLBARS=2
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPELAMPS,PPORAMPS,PPELAODL,PPORAODL,PPOPEELA,PPOPEODL,PPVERSUG,PPCRIELA,PPPERPIA,PPPIAPUN,PPMATOUP";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPELAMPS,PPORAMPS,PPELAODL,PPORAODL,PPOPEELA,PPOPEODL,PPVERSUG,PPCRIELA,PPPERPIA,PPPIAPUN,PPMATOUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_ELAMPS = NVL(cp_ToDate(_Link_.PPELAMPS),ctod("  /  /  "))
      this.w_ORAMPS = NVL(_Link_.PPORAMPS,space(8))
      this.w_ELAODL = NVL(cp_ToDate(_Link_.PPELAODL),ctod("  /  /  "))
      this.w_ORAODL = NVL(_Link_.PPORAODL,space(8))
      this.w_OPEMPS = NVL(_Link_.PPOPEELA,0)
      this.w_OPEODL = NVL(_Link_.PPOPEODL,0)
      this.w_CRIFORM = NVL(_Link_.PPVERSUG,space(1))
      this.w_PPCRIELA = NVL(_Link_.PPCRIELA,space(1))
      this.w_PPPERPIA = NVL(_Link_.PPPERPIA,space(1))
      this.w_PPPIAPUN = NVL(_Link_.PPPIAPUN,space(1))
      this.w_PPMATOUP = NVL(_Link_.PPMATOUP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_ELAMPS = ctod("  /  /  ")
      this.w_ORAMPS = space(8)
      this.w_ELAODL = ctod("  /  /  ")
      this.w_ORAODL = space(8)
      this.w_OPEMPS = 0
      this.w_OPEODL = 0
      this.w_CRIFORM = space(1)
      this.w_PPCRIELA = space(1)
      this.w_PPPERPIA = space(1)
      this.w_PPPIAPUN = space(1)
      this.w_PPMATOUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AACODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPFLDIGE";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_AACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_AACODICE)
            select PPCODICE,PPFLDIGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AACODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_CRIFORN = NVL(_Link_.PPFLDIGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AACODICE = space(2)
      endif
      this.w_CRIFORN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MSGMRP
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PARA_MRP_IDX,3]
    i_lTable = "PARA_MRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2], .t., this.PARA_MRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MSGMRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MSGMRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PMCODICE,PMODLPIA,PMODLLAN,PMOCLSUG,PMOCLPIA,PMOCLORD,PMODLSUG,PMODAPIA,PMODALAN,PMODASUG";
                   +" from "+i_cTable+" "+i_lTable+" where PMCODICE="+cp_ToStrODBC(this.w_MSGMRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PMCODICE',this.w_MSGMRP)
            select PMCODICE,PMODLPIA,PMODLLAN,PMOCLSUG,PMOCLPIA,PMOCLORD,PMODLSUG,PMODAPIA,PMODALAN,PMODASUG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MSGMRP = NVL(_Link_.PMCODICE,space(2))
      this.w_PMODLPIA = NVL(_Link_.PMODLPIA,space(1))
      this.w_PMODLLAN = NVL(_Link_.PMODLLAN,space(1))
      this.w_PMOCLSUG = NVL(_Link_.PMOCLSUG,space(1))
      this.w_PMOCLPIA = NVL(_Link_.PMOCLPIA,space(1))
      this.w_PMOCLORD = NVL(_Link_.PMOCLORD,space(1))
      this.w_PMODLSUG = NVL(_Link_.PMODLSUG,space(1))
      this.w_PMODAPIA = NVL(_Link_.PMODAPIA,space(1))
      this.w_PMODALAN = NVL(_Link_.PMODALAN,space(1))
      this.w_PMODASUG = NVL(_Link_.PMODASUG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MSGMRP = space(2)
      endif
      this.w_PMODLPIA = space(1)
      this.w_PMODLLAN = space(1)
      this.w_PMOCLSUG = space(1)
      this.w_PMOCLPIA = space(1)
      this.w_PMOCLORD = space(1)
      this.w_PMODLSUG = space(1)
      this.w_PMODAPIA = space(1)
      this.w_PMODALAN = space(1)
      this.w_PMODASUG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PARA_MRP_IDX,2])+'\'+cp_ToStr(_Link_.PMCODICE,1)
      cp_ShowWarn(i_cKey,this.PARA_MRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MSGMRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMINI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMINI))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMINI)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMINI) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMINI_2_1'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMINI)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMINI = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMI = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMINI = space(15)
      endif
      this.w_DESCOMI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COMINI <= .w_COMFIN OR EMPTY(.w_COMFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMINI = space(15)
        this.w_DESCOMI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMFIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMFIN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMFIN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMFIN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMFIN_2_2'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMFIN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMFIN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMF = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COMFIN = space(15)
      endif
      this.w_DESCOMF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COMINI <= .w_COMFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COMFIN = space(15)
        this.w_DESCOMF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INICLI
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INICLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_INICLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_INICLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INICLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INICLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oINICLI_2_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INICLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_INICLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_INICLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INICLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLII = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_INICLI = space(15)
      endif
      this.w_DESCLII = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_FINCLI) or .w_FINCLI>=.w_INICLI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_INICLI = space(15)
        this.w_DESCLII = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INICLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FINCLI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FINCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FINCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FINCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FINCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FINCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFINCLI_2_11'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FINCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FINCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FINCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FINCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLIF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FINCLI = space(15)
      endif
      this.w_DESCLIF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_INICLI) or .w_FINCLI>=.w_INICLI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FINCLI = space(15)
        this.w_DESCLIF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FINCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODINI))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODINI_1_67'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODINI)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODFIN))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODFIN_1_68'),i_cWhere,'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODFIN)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAINI
  func Link_1_71(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAINI_1_71'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAINI = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAINI = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMAFIN
  func Link_1_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMAFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMAFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMAFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMAFIN_1_72'),i_cWhere,'',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMAFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMAFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMAFIN = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FAMAINI <= .w_FAMAFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FAMAFIN = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUINI
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUINI_1_73'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUFIN
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUFIN_1_74'),i_cWhere,'',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GRUINI <= .w_GRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATINI_1_75'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATFIN_1_76'),i_cWhere,'',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATINI <= .w_CATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_77'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_78'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGINI <= .w_MAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARINI
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARINI_1_79'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARINI = space(5)
      endif
      this.w_DESMARI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARINI = space(5)
        this.w_DESMARI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIN
  func Link_1_80(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIN_1_80'),i_cWhere,'',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMARF = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIN = space(5)
      endif
      this.w_DESMARF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MARINI <= .w_MARFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MARFIN = space(5)
        this.w_DESMARF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMRPLOG_1_29.RadioValue()==this.w_MRPLOG)
      this.oPgFrm.Page1.oPag.oMRPLOG_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERPIA_1_58.RadioValue()==this.w_PERPIA)
      this.oPgFrm.Page1.oPag.oPERPIA_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIELA_1_59.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page1.oPag.oCRIELA_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPIAPUN_1_60.RadioValue()==this.w_PIAPUN)
      this.oPgFrm.Page1.oPag.oPIAPUN_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZM_1_64.RadioValue()==this.w_SELEZM)
      this.oPgFrm.Page1.oPag.oSELEZM_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMINI_2_1.value==this.w_COMINI)
      this.oPgFrm.Page2.oPag.oCOMINI_2_1.value=this.w_COMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMFIN_2_2.value==this.w_COMFIN)
      this.oPgFrm.Page2.oPag.oCOMFIN_2_2.value=this.w_COMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMODL_2_3.RadioValue()==this.w_COMODL)
      this.oPgFrm.Page2.oPag.oCOMODL_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMINI_2_4.value==this.w_NUMINI)
      this.oPgFrm.Page2.oPag.oNUMINI_2_4.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE1_2_5.value==this.w_SERIE1)
      this.oPgFrm.Page2.oPag.oSERIE1_2_5.value=this.w_SERIE1
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMFIN_2_6.value==this.w_NUMFIN)
      this.oPgFrm.Page2.oPag.oNUMFIN_2_6.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSERIE2_2_7.value==this.w_SERIE2)
      this.oPgFrm.Page2.oPag.oSERIE2_2_7.value=this.w_SERIE2
    endif
    if not(this.oPgFrm.Page2.oPag.oDOCINI_2_8.value==this.w_DOCINI)
      this.oPgFrm.Page2.oPag.oDOCINI_2_8.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDOCFIN_2_9.value==this.w_DOCFIN)
      this.oPgFrm.Page2.oPag.oDOCFIN_2_9.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oINICLI_2_10.value==this.w_INICLI)
      this.oPgFrm.Page2.oPag.oINICLI_2_10.value=this.w_INICLI
    endif
    if not(this.oPgFrm.Page2.oPag.oFINCLI_2_11.value==this.w_FINCLI)
      this.oPgFrm.Page2.oPag.oFINCLI_2_11.value=this.w_FINCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oINIELA_2_12.value==this.w_INIELA)
      this.oPgFrm.Page2.oPag.oINIELA_2_12.value=this.w_INIELA
    endif
    if not(this.oPgFrm.Page2.oPag.oFINELA_2_13.value==this.w_FINELA)
      this.oPgFrm.Page2.oPag.oFINELA_2_13.value=this.w_FINELA
    endif
    if not(this.oPgFrm.Page2.oPag.oORIODL_2_14.RadioValue()==this.w_ORIODL)
      this.oPgFrm.Page2.oPag.oORIODL_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_16.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLII_2_22.value==this.w_DESCLII)
      this.oPgFrm.Page2.oPag.oDESCLII_2_22.value=this.w_DESCLII
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLIF_2_23.value==this.w_DESCLIF)
      this.oPgFrm.Page2.oPag.oDESCLIF_2_23.value=this.w_DESCLIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMI_2_25.value==this.w_DESCOMI)
      this.oPgFrm.Page2.oPag.oDESCOMI_2_25.value=this.w_DESCOMI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMF_2_27.value==this.w_DESCOMF)
      this.oPgFrm.Page2.oPag.oDESCOMF_2_27.value=this.w_DESCOMF
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEIMPE_2_35.RadioValue()==this.w_SELEIMPE)
      this.oPgFrm.Page2.oPag.oSELEIMPE_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_67.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_67.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_68.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_68.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLLCINI_1_69.value==this.w_LLCINI)
      this.oPgFrm.Page1.oPag.oLLCINI_1_69.value=this.w_LLCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oLLCFIN_1_70.value==this.w_LLCFIN)
      this.oPgFrm.Page1.oPag.oLLCFIN_1_70.value=this.w_LLCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAINI_1_71.value==this.w_FAMAINI)
      this.oPgFrm.Page1.oPag.oFAMAINI_1_71.value=this.w_FAMAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMAFIN_1_72.value==this.w_FAMAFIN)
      this.oPgFrm.Page1.oPag.oFAMAFIN_1_72.value=this.w_FAMAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUINI_1_73.value==this.w_GRUINI)
      this.oPgFrm.Page1.oPag.oGRUINI_1_73.value=this.w_GRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUFIN_1_74.value==this.w_GRUFIN)
      this.oPgFrm.Page1.oPag.oGRUFIN_1_74.value=this.w_GRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_75.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_75.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_76.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_76.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_77.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_77.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_78.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_78.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMARINI_1_79.value==this.w_MARINI)
      this.oPgFrm.Page1.oPag.oMARINI_1_79.value=this.w_MARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMARFIN_1_80.value==this.w_MARFIN)
      this.oPgFrm.Page1.oPag.oMARFIN_1_80.value=this.w_MARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPROFIN_1_81.RadioValue()==this.w_PROFIN)
      this.oPgFrm.Page1.oPag.oPROFIN_1_81.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEMLAV_1_82.RadioValue()==this.w_SEMLAV)
      this.oPgFrm.Page1.oPag.oSEMLAV_1_82.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMATPRI_1_83.RadioValue()==this.w_MATPRI)
      this.oPgFrm.Page1.oPag.oMATPRI_1_83.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELACAT_1_86.RadioValue()==this.w_ELACAT)
      this.oPgFrm.Page1.oPag.oELACAT_1_86.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_88.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_88.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_90.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_90.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_91.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_91.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_93.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_93.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_95.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_95.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_96.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_96.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_97.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_97.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_101.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_101.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_102.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_102.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_103.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_103.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARI_1_111.value==this.w_DESMARI)
      this.oPgFrm.Page1.oPag.oDESMARI_1_111.value=this.w_DESMARI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMARF_1_113.value==this.w_DESMARF)
      this.oPgFrm.Page1.oPag.oDESMARF_1_113.value=this.w_DESMARF
    endif
    if not(this.oPgFrm.Page2.oPag.oORDIPROD_2_40.RadioValue()==this.w_ORDIPROD)
      this.oPgFrm.Page2.oPag.oORDIPROD_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPEPROD_2_41.RadioValue()==this.w_IMPEPROD)
      this.oPgFrm.Page2.oPag.oIMPEPROD_2_41.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MAGFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFOR_1_32.SetFocus()
            i_bnoObbl = !empty(.w_MAGFOR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MAGFOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFOC_1_34.SetFocus()
            i_bnoObbl = !empty(.w_MAGFOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MAGFOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFOL_1_36.SetFocus()
            i_bnoObbl = !empty(.w_MAGFOL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COMINI <= .w_COMFIN OR EMPTY(.w_COMFIN))  and ((g_PERCAN='S' or g_COMM='S') and .w_MODELA='R')  and not(empty(.w_COMINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOMINI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COMINI <= .w_COMFIN)  and ((g_PERCAN='S' or g_COMM='S') and .w_MODELA='R')  and not(empty(.w_COMFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOMFIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMINI)) or not(.w_numini<=.w_numfin and .w_numini>0))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMINI_2_4.SetFocus()
            i_bnoObbl = !empty(.w_NUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE1_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMFIN)) or not(.w_numini<=.w_numfin and .w_numfin>0))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNUMFIN_2_6.SetFocus()
            i_bnoObbl = !empty(.w_NUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSERIE2_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDOCINI_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DOCINI<=.w_DOCFIN)  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDOCFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_FINCLI) or .w_FINCLI>=.w_INICLI)  and (.w_MODELA='R')  and not(empty(.w_INICLI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oINICLI_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INICLI) or .w_FINCLI>=.w_INICLI)  and (.w_MODELA='R')  and not(empty(.w_FINCLI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFINCLI_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_INIELA)) or not(not empty(.w_INIELA) and .w_FINELA>=.w_INIELA))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oINIELA_2_12.SetFocus()
            i_bnoObbl = !empty(.w_INIELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o maggiore della data di fine elaborazione")
          case   ((empty(.w_FINELA)) or not(not empty(.w_FINELA) and .w_FINELA>=.w_INIELA))  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFINELA_2_13.SetFocus()
            i_bnoObbl = !empty(.w_FINELA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data vuota o minore della data di fine elaborazione")
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and (.w_MODELA='R')  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CODINI<=.w_CODFIN or empty(.w_CODINI)))  and (.w_MODELA='R')  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_68.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLLCINI_1_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LLCINI<=.w_LLCFIN)  and (.w_MODELA='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLLCFIN_1_70.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN OR EMPTY(.w_FAMAFIN))  and (.w_MODELA='R')  and not(empty(.w_FAMAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAINI_1_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FAMAINI <= .w_FAMAFIN)  and (.w_MODELA='R')  and not(empty(.w_FAMAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFAMAFIN_1_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN OR EMPTY(.w_GRUFIN))  and (.w_MODELA='R')  and not(empty(.w_GRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUINI_1_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRUINI <= .w_GRUFIN)  and (.w_MODELA='R')  and not(empty(.w_GRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUFIN_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN OR EMPTY(.w_CATFIN))  and (.w_MODELA='R')  and not(empty(.w_CATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATINI_1_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATINI <= .w_CATFIN)  and (.w_MODELA='R')  and not(empty(.w_CATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATFIN_1_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN OR EMPTY(.w_MAGFIN))  and (.w_MODELA='R')  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MAGINI <= .w_MAGFIN)  and (.w_MODELA='R')  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN OR EMPTY(.w_MARFIN))  and (.w_MODELA='R')  and not(empty(.w_MARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARINI_1_79.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARINI <= .w_MARFIN)  and (.w_MODELA='R')  and not(empty(.w_MARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMARFIN_1_80.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PPCODICE = this.w_PPCODICE
    this.o_MODELA = this.w_MODELA
    this.o_MICOM = this.w_MICOM
    this.o_MIODL = this.w_MIODL
    this.o_GENPDA = this.w_GENPDA
    this.o_MIODA = this.w_MIODA
    this.o_DISMAG = this.w_DISMAG
    this.o_PMODAPIA = this.w_PMODAPIA
    this.o_PMODALAN = this.w_PMODALAN
    this.o_CRIELA = this.w_CRIELA
    this.o_SELEZM = this.w_SELEZM
    this.o_COMINI = this.w_COMINI
    this.o_COMFIN = this.w_COMFIN
    this.o_NUMINI = this.w_NUMINI
    this.o_SERIE1 = this.w_SERIE1
    this.o_NUMFIN = this.w_NUMFIN
    this.o_SERIE2 = this.w_SERIE2
    this.o_DOCINI = this.w_DOCINI
    this.o_DOCFIN = this.w_DOCFIN
    this.o_INICLI = this.w_INICLI
    this.o_FINCLI = this.w_FINCLI
    this.o_INIELA = this.w_INIELA
    this.o_FINELA = this.w_FINELA
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    this.o_LLCINI = this.w_LLCINI
    this.o_LLCFIN = this.w_LLCFIN
    this.o_FAMAINI = this.w_FAMAINI
    this.o_FAMAFIN = this.w_FAMAFIN
    this.o_GRUINI = this.w_GRUINI
    this.o_GRUFIN = this.w_GRUFIN
    this.o_CATINI = this.w_CATINI
    this.o_CATFIN = this.w_CATFIN
    this.o_MAGINI = this.w_MAGINI
    this.o_MAGFIN = this.w_MAGFIN
    this.o_MARINI = this.w_MARINI
    this.o_MARFIN = this.w_MARFIN
    this.o_PROFIN = this.w_PROFIN
    this.o_SEMLAV = this.w_SEMLAV
    this.o_MATPRI = this.w_MATPRI
    this.o_STIPART = this.w_STIPART
    this.o_ED = this.w_ED
    this.o_CAUSALI = this.w_CAUSALI
    return

enddefine

* --- Define pages as container
define class tgsmr_kmmPag1 as StdContainer
  Width  = 810
  height = 473
  stdWidth  = 810
  stdheight = 473
  resizeXpos=557
  resizeYpos=394
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_6 as StdButton with uid="XAOFRYAIID",left=705, top=426, width=48,height=45,;
    CpPicture="BMP\GENERA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 68617446;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSMR2BGP(this.Parent.oContained,"GSMR_BGP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ("S" $ nvl(.w_PMOCLSUG+.w_PMOCLPIA+.w_PMOCLORD+.w_PMODLPIA+.w_PMODLLAN+.w_PMODLSUG+.w_PMODAPIA+.w_PMODALAN+.w_PMODASUG," "))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="KYQESPJULE",left=757, top=426, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75906118;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMRPLOG_1_29 as StdCheck with uid="KYWQCBJRJS",rtseq=27,rtrep=.f.,left=350, top=437, caption="Attiva scrittura log elaborazione",;
    ToolTipText = "Attiva la scrittura del log di elaborazione MRP",;
    HelpContextID = 262673466,;
    cFormVar="w_MRPLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMRPLOG_1_29.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oMRPLOG_1_29.GetRadio()
    this.Parent.oContained.w_MRPLOG = this.RadioValue()
    return .t.
  endfunc

  func oMRPLOG_1_29.SetRadio()
    this.Parent.oContained.w_MRPLOG=trim(this.Parent.oContained.w_MRPLOG)
    this.value = ;
      iif(this.Parent.oContained.w_MRPLOG=="S",1,;
      0)
  endfunc


  add object oBtn_1_37 as StdButton with uid="EARFNCPNLL",left=652, top=426, width=48,height=45,;
    CpPicture="BMP\DIBA8.BMP", caption="", nPag=1;
    , ToolTipText = "Parametri messaggi MRP";
    , HelpContextID = 108900842;
    , Caption='\<Parametri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSMR_KPA("EX")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPERPIA_1_58 as StdCombo with uid="DRHNEVIUIN",rtseq=55,rtrep=.f.,left=649,top=84,width=157,height=21;
    , ToolTipText = "Pianifica per periodo";
    , HelpContextID = 100925706;
    , cFormVar="w_PERPIA",RowSource=""+"Giornaliero,"+"Settimanale,"+"Mensile,"+"Semestrale,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPERPIA_1_58.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'W',;
    iif(this.value =3,'M',;
    iif(this.value =4,'S',;
    iif(this.value =5,'A',;
    space(1)))))))
  endfunc
  func oPERPIA_1_58.GetRadio()
    this.Parent.oContained.w_PERPIA = this.RadioValue()
    return .t.
  endfunc

  func oPERPIA_1_58.SetRadio()
    this.Parent.oContained.w_PERPIA=trim(this.Parent.oContained.w_PERPIA)
    this.value = ;
      iif(this.Parent.oContained.w_PERPIA=='D',1,;
      iif(this.Parent.oContained.w_PERPIA=='W',2,;
      iif(this.Parent.oContained.w_PERPIA=='M',3,;
      iif(this.Parent.oContained.w_PERPIA=='S',4,;
      iif(this.Parent.oContained.w_PERPIA=='A',5,;
      0)))))
  endfunc


  add object oCRIELA_1_59 as StdCombo with uid="VSDAZQQBWZ",rtseq=56,rtrep=.f.,left=649,top=108,width=157,height=21;
    , ToolTipText = "Criterio di pianificazione";
    , HelpContextID = 98534618;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIELA_1_59.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_1_59.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_1_59.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc


  add object oPIAPUN_1_60 as StdCombo with uid="QHVQIVCNUN",rtseq=57,rtrep=.f.,left=649,top=133,width=157,height=21;
    , ToolTipText = "Pianificazione puntuale";
    , HelpContextID = 138743050;
    , cFormVar="w_PIAPUN",RowSource=""+"Si,"+"No,"+"Da anagrafica articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPIAPUN_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPIAPUN_1_60.GetRadio()
    this.Parent.oContained.w_PIAPUN = this.RadioValue()
    return .t.
  endfunc

  func oPIAPUN_1_60.SetRadio()
    this.Parent.oContained.w_PIAPUN=trim(this.Parent.oContained.w_PIAPUN)
    this.value = ;
      iif(this.Parent.oContained.w_PIAPUN=='S',1,;
      iif(this.Parent.oContained.w_PIAPUN=='N',2,;
      iif(this.Parent.oContained.w_PIAPUN=='A',3,;
      0)))
  endfunc


  add object ZOOMMAGA as cp_szoombox with uid="VPMEOXAFHG",left=432, top=156, width=373,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.t.,cZoomFile="GSVEMKGF",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="MAGAZZIN",bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "InterrogaMaga",;
    nPag=1;
    , HelpContextID = 21849114


  add object LBLMAGA as cp_calclbl with uid="UIGUTRTDRQ",left=437, top=146, width=222,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=0,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=1;
    , HelpContextID = 21849114

  add object oSELEZM_1_64 as StdRadio with uid="LTQWYYFJNE",rtseq=58,rtrep=.f.,left=568, top=407, width=239,height=18;
    , cFormVar="w_SELEZM", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZM_1_64.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 150954202
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 150954202
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",18)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZM_1_64.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZM_1_64.GetRadio()
    this.Parent.oContained.w_SELEZM = this.RadioValue()
    return .t.
  endfunc

  func oSELEZM_1_64.SetRadio()
    this.Parent.oContained.w_SELEZM=trim(this.Parent.oContained.w_SELEZM)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZM=="S",1,;
      iif(this.Parent.oContained.w_SELEZM=="D",2,;
      0))
  endfunc

  func oSELEZM_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CRIELA $ 'G-M')
    endwith
   endif
  endfunc

  add object oCODINI_1_67 as StdField with uid="DZDJHVMUJE",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo di inizio selezione",;
    HelpContextID = 230414298,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=104, Top=34, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODINI"

  func oCODINI_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oCODINI_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_67.ecpDrop(oSource)
    this.Parent.oContained.link_1_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_67.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODINI_1_67'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODFIN_1_68 as StdField with uid="UNDISPFWGH",rtseq=82,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articoli di fine selezione",;
    HelpContextID = 151967706,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=104, Top=59, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oCODFIN_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_68('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_68.ecpDrop(oSource)
    this.Parent.oContained.link_1_68('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_68.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODFIN_1_68'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0AAR.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oLLCINI_1_69 as StdField with uid="RKYWPPNDHG",rtseq=83,rtrep=.f.,;
    cFormVar = "w_LLCINI", cQueryName = "LLCINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di inizio selezione",;
    HelpContextID = 230419018,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=765, Top=33, cSayPict='"999"', cGetPict='"999"'

  func oLLCINI_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oLLCINI_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN and .w_LLCINI>=0)
    endwith
    return bRes
  endfunc

  add object oLLCFIN_1_70 as StdField with uid="ACQHHCIYYT",rtseq=84,rtrep=.f.,;
    cFormVar = "w_LLCFIN", cQueryName = "LLCFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Low level code di fine selezione",;
    HelpContextID = 151972426,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=765, Top=56, cSayPict='"999"', cGetPict='"999"'

  func oLLCFIN_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oLLCFIN_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LLCINI<=.w_LLCFIN)
    endwith
    return bRes
  endfunc

  add object oFAMAINI_1_71 as StdField with uid="BAUZFWPTWC",rtseq=85,rtrep=.f.,;
    cFormVar = "w_FAMAINI", cQueryName = "FAMAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 116173398,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAINI"

  func oFAMAINI_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oFAMAINI_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_71('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAINI_1_71.ecpDrop(oSource)
    this.Parent.oContained.link_1_71('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAINI_1_71.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAINI_1_71'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oFAMAFIN_1_72 as StdField with uid="GHDDKOKVIG",rtseq=86,rtrep=.f.,;
    cFormVar = "w_FAMAFIN", cQueryName = "FAMAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 29141590,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMAFIN"

  func oFAMAFIN_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oFAMAFIN_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_72('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMAFIN_1_72.ecpDrop(oSource)
    this.Parent.oContained.link_1_72('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMAFIN_1_72.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMAFIN_1_72'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie articoli",'',this.parent.oContained
  endproc

  add object oGRUINI_1_73 as StdField with uid="VYQYSTJFCH",rtseq=87,rtrep=.f.,;
    cFormVar = "w_GRUINI", cQueryName = "GRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 230343834,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=133, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUINI"

  func oGRUINI_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oGRUINI_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUINI_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUINI_1_73.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUINI_1_73'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oGRUFIN_1_74 as StdField with uid="BKTBROMCMD",rtseq=88,rtrep=.f.,;
    cFormVar = "w_GRUFIN", cQueryName = "GRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 151897242,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUFIN"

  func oGRUFIN_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oGRUFIN_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_74('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUFIN_1_74.ecpDrop(oSource)
    this.Parent.oContained.link_1_74('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUFIN_1_74.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUFIN_1_74'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCATINI_1_75 as StdField with uid="WQNLFQEXDD",rtseq=89,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 230352346,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oCATINI_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_75('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_75.ecpDrop(oSource)
    this.Parent.oContained.link_1_75('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_75.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATINI_1_75'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oCATFIN_1_76 as StdField with uid="WHQQEZYTLE",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 151905754,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=206, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATFIN"

  func oCATFIN_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oCATFIN_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATFIN_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie omogenee",'',this.parent.oContained
  endproc

  add object oMAGINI_1_77 as StdField with uid="WWAXPPIMPZ",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di inizio selezione",;
    HelpContextID = 230405434,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oMAGINI_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMAGFIN_1_78 as StdField with uid="IPBLQAUROZ",rtseq=92,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino preferenziale articolo di fine selezione",;
    HelpContextID = 151958842,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=255, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  func oMAGFIN_1_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oMAGFIN_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_78('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_78.ecpDrop(oSource)
    this.Parent.oContained.link_1_78('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_78.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_78'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oMARINI_1_79 as StdField with uid="YZOGKGSXPU",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MARINI", cQueryName = "MARINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di inizio selezione",;
    HelpContextID = 230360378,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_MARINI"

  func oMARINI_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oMARINI_1_79.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_79('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARINI_1_79.ecpDrop(oSource)
    this.Parent.oContained.link_1_79('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARINI_1_79.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARINI_1_79'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object oMARFIN_1_80 as StdField with uid="NRZQAZMNEF",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MARFIN", cQueryName = "MARFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca di fine selezione",;
    HelpContextID = 151913786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIN"

  func oMARFIN_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oMARFIN_1_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_80('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIN_1_80.ecpDrop(oSource)
    this.Parent.oContained.link_1_80('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIN_1_80.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIN_1_80'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Marchi",'',this.parent.oContained
  endproc

  add object oPROFIN_1_81 as StdCheck with uid="IHWROQAYED",rtseq=95,rtrep=.f.,left=104, top=328, caption="Prodotti finiti",;
    ToolTipText = "Se attivo, elabora i prodotti finiti",;
    HelpContextID = 151921674,;
    cFormVar="w_PROFIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPROFIN_1_81.RadioValue()
    return(iif(this.value =1,'PF',;
    'XX'))
  endfunc
  func oPROFIN_1_81.GetRadio()
    this.Parent.oContained.w_PROFIN = this.RadioValue()
    return .t.
  endfunc

  func oPROFIN_1_81.SetRadio()
    this.Parent.oContained.w_PROFIN=trim(this.Parent.oContained.w_PROFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PROFIN=='PF',1,;
      0)
  endfunc

  add object oSEMLAV_1_82 as StdCheck with uid="KTWFDIHQED",rtseq=96,rtrep=.f.,left=104, top=351, caption="Semilavorati",;
    ToolTipText = "Se attivo, elabora i semilavorati",;
    HelpContextID = 25710810,;
    cFormVar="w_SEMLAV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEMLAV_1_82.RadioValue()
    return(iif(this.value =1,'SE',;
    'XX'))
  endfunc
  func oSEMLAV_1_82.GetRadio()
    this.Parent.oContained.w_SEMLAV = this.RadioValue()
    return .t.
  endfunc

  func oSEMLAV_1_82.SetRadio()
    this.Parent.oContained.w_SEMLAV=trim(this.Parent.oContained.w_SEMLAV)
    this.value = ;
      iif(this.Parent.oContained.w_SEMLAV=='SE',1,;
      0)
  endfunc

  add object oMATPRI_1_83 as StdCheck with uid="SHIEYEJPCW",rtseq=97,rtrep=.f.,left=104, top=374, caption="Materie prime",;
    ToolTipText = "Se attivo, elabora le materie prime",;
    HelpContextID = 225699130,;
    cFormVar="w_MATPRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMATPRI_1_83.RadioValue()
    return(iif(this.value =1,'MP',;
    'XX'))
  endfunc
  func oMATPRI_1_83.GetRadio()
    this.Parent.oContained.w_MATPRI = this.RadioValue()
    return .t.
  endfunc

  func oMATPRI_1_83.SetRadio()
    this.Parent.oContained.w_MATPRI=trim(this.Parent.oContained.w_MATPRI)
    this.value = ;
      iif(this.Parent.oContained.w_MATPRI=='MP',1,;
      0)
  endfunc


  add object oELACAT_1_86 as StdCombo with uid="HLYKAPXQGN",rtseq=100,rtrep=.f.,left=359,top=352,width=53,height=21;
    , ToolTipText = "Elabora tutta la catena degli impegni degli ordini che rispettano i filtri di selezione sull'articolo";
    , HelpContextID = 59902650;
    , cFormVar="w_ELACAT",RowSource=""+"Ś,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oELACAT_1_86.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oELACAT_1_86.GetRadio()
    this.Parent.oContained.w_ELACAT = this.RadioValue()
    return .t.
  endfunc

  func oELACAT_1_86.SetRadio()
    this.Parent.oContained.w_ELACAT=trim(this.Parent.oContained.w_ELACAT)
    this.value = ;
      iif(this.Parent.oContained.w_ELACAT=='S',1,;
      iif(this.Parent.oContained.w_ELACAT=='N',2,;
      0))
  endfunc

  func oELACAT_1_86.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! .w_ED)
    endwith
   endif
  endfunc

  add object oDESMAGI_1_88 as StdField with uid="QONUNYBHAK",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 259591734,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=231, InputMask=replicate('X',30)

  add object oDESMAGF_1_90 as StdField with uid="EBXVFVWORM",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 8843722,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=255, InputMask=replicate('X',30)

  add object oDESINI_1_91 as StdField with uid="SSNEPWHMKY",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 230355402,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=280, Top=34, InputMask=replicate('X',40)

  add object oDESFIN_1_93 as StdField with uid="LMJMLCZYUA",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151908810,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=280, Top=59, InputMask=replicate('X',40)

  add object oDESFAMAI_1_95 as StdField with uid="AOYBYAMKXI",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177074561,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=84, InputMask=replicate('X',35)

  add object oDESGRUI_1_96 as StdField with uid="ZABORREMDH",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 243469878,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=133, InputMask=replicate('X',35)

  add object oDESCATI_1_97 as StdField with uid="ZFWYARIWSP",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 208604726,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=182, InputMask=replicate('X',35)

  add object oDESFAMAF_1_101 as StdField with uid="WOCDSQXKSA",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177074564,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=108, InputMask=replicate('X',35)

  add object oDESGRUF_1_102 as StdField with uid="PYGQUHDJMJ",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24965578,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=157, InputMask=replicate('X',35)

  add object oDESCATF_1_103 as StdField with uid="SXSGGSHCII",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59830730,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=206, InputMask=replicate('X',35)

  add object oDESMARI_1_111 as StdField with uid="MXNLJGFEDS",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DESMARI", cQueryName = "DESMARI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 175705654,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=280, InputMask=replicate('X',35)

  add object oDESMARF_1_113 as StdField with uid="UFBSQRHGVN",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESMARF", cQueryName = "DESMARF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 92729802,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=171, Top=304, InputMask=replicate('X',35)


  add object oLinePB as LineMrp with uid="GRSDEDIBQX",left=9, top=403, width=420,height=1,;
    caption='',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 68588790


  add object PROGBAR as TAM_progressbar with uid="RMEVXPDIDT",left=9, top=408, width=316,height=60,;
    caption='',;
   bGlobalFont=.t.,;
    Min=0,Max=100,Value=0,bNoInsideForm=.f.,bNoMultiProgBar=.f.,Visible=.t.,;
    nPag=1;
    , HelpContextID = 68588790


  add object oBtn_1_127 as StdButton with uid="ODISEJSZBN",left=598, top=426, width=48,height=45,;
    CpPicture="BMP\DIBA3.BMP", caption="", nPag=1,tabstop=.f.;
    , HelpContextID = 39602189;
    , Caption='\<Interrompi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_127.Click()
      with this.Parent.oContained
        i_CANCELROUTINE = .T.
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_61 as StdString with uid="TJZDCQSUZV",Visible=.t., Left=464, Top=110,;
    Alignment=1, Width=182, Height=15,;
    Caption="Criterio di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=4, Top=233,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=14, Top=257,;
    Alignment=1, Width=89, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="NMLJSVRZGE",Visible=.t., Left=4, Top=34,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="XRBSMYKOYR",Visible=.t., Left=4, Top=59,;
    Alignment=1, Width=99, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=4, Top=86,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=4, Top=135,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=4, Top=184,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_104 as StdString with uid="BETNLUNOYI",Visible=.t., Left=14, Top=110,;
    Alignment=1, Width=89, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=14, Top=159,;
    Alignment=1, Width=89, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_106 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=11, Top=208,;
    Alignment=1, Width=92, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="GKIMPOMLOZ",Visible=.t., Left=661, Top=35,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="EUAAIOZOQT",Visible=.t., Left=711, Top=58,;
    Alignment=1, Width=49, Height=15,;
    Caption="A LLC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_109 as StdString with uid="QTYVSGEGTT",Visible=.t., Left=256, Top=354,;
    Alignment=1, Width=99, Height=15,;
    Caption="Elabora catena:"  ;
  , bGlobalFont=.t.

  add object oStr_1_110 as StdString with uid="SGSTIMCHVV",Visible=.t., Left=4, Top=282,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="ZYLPGFCGXX",Visible=.t., Left=14, Top=306,;
    Alignment=1, Width=89, Height=15,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="NTWHWPMLOA",Visible=.t., Left=16, Top=7,;
    Alignment=0, Width=331, Height=18,;
    Caption="Selezioni articolo"  ;
  , bGlobalFont=.t.

  add object oStr_1_123 as StdString with uid="VSKSWONFNJ",Visible=.t., Left=464, Top=86,;
    Alignment=1, Width=182, Height=18,;
    Caption="Periodo di pianificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="UYQPTYMPSQ",Visible=.t., Left=464, Top=135,;
    Alignment=1, Width=182, Height=17,;
    Caption="Pianificazione puntuale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_114 as StdBox with uid="UDTMWKFXRP",left=9, top=24, width=785,height=1
enddefine
define class tgsmr_kmmPag2 as StdContainer
  Width  = 810
  height = 473
  stdWidth  = 810
  stdheight = 473
  resizeXpos=688
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOMINI_2_1 as StdField with uid="YEMXFLGGUY",rtseq=60,rtrep=.f.,;
    cFormVar = "w_COMINI", cQueryName = "COMINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di inizio selezione",;
    HelpContextID = 230377434,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMINI"

  func oCOMINI_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') and .w_MODELA='R')
    endwith
   endif
  endfunc

  func oCOMINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMINI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMINI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMINI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCOMFIN_2_2 as StdField with uid="TQLURNPFWN",rtseq=61,rtrep=.f.,;
    cFormVar = "w_COMFIN", cQueryName = "COMFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di fine selezione",;
    HelpContextID = 151930842,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=34, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMFIN"

  func oCOMFIN_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCAN='S' or g_COMM='S') and .w_MODELA='R')
    endwith
   endif
  endfunc

  func oCOMFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMFIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMFIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMFIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCOMODL_2_3 as StdRadio with uid="FZFMTLUTAJ",rtseq=62,rtrep=.f.,left=574, top=9, width=153,height=51;
    , ToolTipText = "Validità filtro commessa";
    , cFormVar="w_COMODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCOMODL_2_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 190138330
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 190138330
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 190138330
      this.Buttons(3).Top=32
      this.SetAll("Width",151)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validità filtro commessa")
      StdRadio::init()
    endproc

  func oCOMODL_2_3.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCOMODL_2_3.GetRadio()
    this.Parent.oContained.w_COMODL = this.RadioValue()
    return .t.
  endfunc

  func oCOMODL_2_3.SetRadio()
    this.Parent.oContained.w_COMODL=trim(this.Parent.oContained.w_COMODL)
    this.value = ;
      iif(this.Parent.oContained.w_COMODL=='O',1,;
      iif(this.Parent.oContained.w_COMODL=='D',2,;
      iif(this.Parent.oContained.w_COMODL=='T',3,;
      0)))
  endfunc

  func oCOMODL_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_COMINI) or not empty(.w_COMFIN))
    endwith
   endif
  endfunc

  add object oNUMINI_2_4 as StdField with uid="YWFWDUKRHP",rtseq=63,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 230375722,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=134, Top=59, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMINI_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oNUMINI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin and .w_numini>0)
    endwith
    return bRes
  endfunc

  add object oSERIE1_2_5 as StdField with uid="VCUBODDNSC",rtseq=64,rtrep=.f.,;
    cFormVar = "w_SERIE1", cQueryName = "SERIE1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento di inzio selezione",;
    HelpContextID = 162856742,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=281, Top=59, InputMask=replicate('X',10)

  func oSERIE1_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oSERIE1_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_2_6 as StdField with uid="RCINERZEKD",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 151929130,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=134, Top=84, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMFIN_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oNUMFIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin and .w_numfin>0)
    endwith
    return bRes
  endfunc

  add object oSERIE2_2_7 as StdField with uid="HJOOAQPZOB",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SERIE2", cQueryName = "SERIE2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento  di fine selezione",;
    HelpContextID = 179633958,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=281, Top=83, InputMask=replicate('X',10)

  func oSERIE2_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oSERIE2_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oDOCINI_2_8 as StdField with uid="FSPDAHXDHY",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 230418378,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=477, Top=59

  func oDOCINI_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oDOCINI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN or empty(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_2_9 as StdField with uid="JBFVSEPHWX",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 151971786,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=477, Top=84

  func oDOCFIN_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oDOCFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oINICLI_2_10 as StdField with uid="XPINBWSWBI",rtseq=69,rtrep=.f.,;
    cFormVar = "w_INICLI", cQueryName = "INICLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di inizio selezione",;
    HelpContextID = 232884346,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=109, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_INICLI"

  func oINICLI_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oINICLI_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oINICLI_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINICLI_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oINICLI_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oFINCLI_2_11 as StdField with uid="DGDVSYNEUI",rtseq=70,rtrep=.f.,;
    cFormVar = "w_FINCLI", cQueryName = "FINCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente intestatario dell'impegno di fine selezione",;
    HelpContextID = 232865194,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=134, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FINCLI"

  func oFINCLI_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oFINCLI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oFINCLI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFINCLI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFINCLI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oINIELA_2_12 as StdField with uid="MDRRUIPSZB",rtseq=71,rtrep=.f.,;
    cFormVar = "w_INIELA", cQueryName = "INIELA",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o maggiore della data di fine elaborazione",;
    ToolTipText = "Data di inizio dell'orizzonte temporale di elaborazione",;
    HelpContextID = 98535546,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=160

  func oINIELA_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oINIELA_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_INIELA) and .w_FINELA>=.w_INIELA)
    endwith
    return bRes
  endfunc

  add object oFINELA_2_13 as StdField with uid="NPUVVATDRF",rtseq=72,rtrep=.f.,;
    cFormVar = "w_FINELA", cQueryName = "FINELA",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data vuota o minore della data di fine elaborazione",;
    ToolTipText = "Data di fine dell'orizzonte temporale di elaborazione",;
    HelpContextID = 98516394,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=185

  func oFINELA_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  func oFINELA_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FINELA) and .w_FINELA>=.w_INIELA)
    endwith
    return bRes
  endfunc

  add object oORIODL_2_14 as StdRadio with uid="GLGTRBWTXT",rtseq=73,rtrep=.f.,left=222, top=160, width=156,height=50;
    , ToolTipText = "Validità filtro orizzonte di elaborazione";
    , cFormVar="w_ORIODL", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oORIODL_2_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo su ODL"
      this.Buttons(1).HelpContextID = 190153754
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo su documenti"
      this.Buttons(2).HelpContextID = 190153754
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 190153754
      this.Buttons(3).Top=32
      this.SetAll("Width",154)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Validità filtro orizzonte di elaborazione")
      StdRadio::init()
    endproc

  func oORIODL_2_14.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oORIODL_2_14.GetRadio()
    this.Parent.oContained.w_ORIODL = this.RadioValue()
    return .t.
  endfunc

  func oORIODL_2_14.SetRadio()
    this.Parent.oContained.w_ORIODL=trim(this.Parent.oContained.w_ORIODL)
    this.value = ;
      iif(this.Parent.oContained.w_ORIODL=='O',1,;
      iif(this.Parent.oContained.w_ORIODL=='D',2,;
      iif(this.Parent.oContained.w_ORIODL=='T',3,;
      0)))
  endfunc

  func oORIODL_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INIELA<>ctod('01-01-1900') or .w_FINELA<>ctod('31-12-2099'))
    endwith
   endif
  endfunc


  add object SZOOM as cp_szoombox with uid="JCMGIWLCXL",left=281, top=267, width=402,height=184,;
    caption='SZOOM',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCO_KGP",cTable="TIP_DOCU",bOptions=.f.,cMenuFile="",cZoomOnZoom="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=2;
    , ToolTipText = "Causali documento di impegno";
    , HelpContextID = 154854438

  add object oSELEZI_2_16 as StdRadio with uid="SMNXMFNXXW",rtseq=74,rtrep=.f.,left=446, top=455, width=239,height=20;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 218063066
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 218063066
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",20)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_16.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_16.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_16.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc

  func oSELEZI_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODELA='R')
    endwith
   endif
  endfunc

  add object oDESCLII_2_22 as StdField with uid="QWFZIJERXR",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESCLII", cQueryName = "DESCLII",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 35589686,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=281, Top=109, InputMask=replicate('X',40)

  add object oDESCLIF_2_23 as StdField with uid="BPAUTKZZUX",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESCLIF", cQueryName = "DESCLIF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 232845770,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=281, Top=134, InputMask=replicate('X',40)

  add object oDESCOMI_2_25 as StdField with uid="ZUHXZOJHTP",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESCOMI", cQueryName = "DESCOMI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 105844278,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=281, Top=9, InputMask=replicate('X',30)

  add object oDESCOMF_2_27 as StdField with uid="GOPFERZBTM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESCOMF", cQueryName = "DESCOMF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 162591178,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=281, Top=34, InputMask=replicate('X',30)


  add object oSELEIMPE_2_35 as StdCombo with uid="OHXXBCHADO",rtseq=79,rtrep=.f.,left=134,top=260,width=146,height=21;
    , HelpContextID = 168779925;
    , cFormVar="w_SELEIMPE",RowSource=""+"Impegni e articoli,"+"Impegni,"+"Articoli", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSELEIMPE_2_35.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'I',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oSELEIMPE_2_35.GetRadio()
    this.Parent.oContained.w_SELEIMPE = this.RadioValue()
    return .t.
  endfunc

  func oSELEIMPE_2_35.SetRadio()
    this.Parent.oContained.w_SELEIMPE=trim(this.Parent.oContained.w_SELEIMPE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEIMPE=='E',1,;
      iif(this.Parent.oContained.w_SELEIMPE=='I',2,;
      iif(this.Parent.oContained.w_SELEIMPE=='A',3,;
      0)))
  endfunc

  add object oORDIPROD_2_40 as StdCheck with uid="VPCQFAZNXC",rtseq=122,rtrep=.f.,left=133, top=210, caption="Considera l'ordinato da produzione",;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 77321174,;
    cFormVar="w_ORDIPROD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oORDIPROD_2_40.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oORDIPROD_2_40.GetRadio()
    this.Parent.oContained.w_ORDIPROD = this.RadioValue()
    return .t.
  endfunc

  func oORDIPROD_2_40.SetRadio()
    this.Parent.oContained.w_ORDIPROD=trim(this.Parent.oContained.w_ORDIPROD)
    this.value = ;
      iif(this.Parent.oContained.w_ORDIPROD=='S',1,;
      0)
  endfunc

  add object oIMPEPROD_2_41 as StdCheck with uid="RYJDILNBMU",rtseq=123,rtrep=.f.,left=133, top=232, caption="Considera l'impegnato da produzione",;
    ToolTipText = "Considera l'ordinato durante l'elaborazione",;
    HelpContextID = 77535542,;
    cFormVar="w_IMPEPROD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPEPROD_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oIMPEPROD_2_41.GetRadio()
    this.Parent.oContained.w_IMPEPROD = this.RadioValue()
    return .t.
  endfunc

  func oIMPEPROD_2_41.SetRadio()
    this.Parent.oContained.w_IMPEPROD=trim(this.Parent.oContained.w_IMPEPROD)
    this.value = ;
      iif(this.Parent.oContained.w_IMPEPROD=='S',1,;
      0)
  endfunc


  add object oBtn_2_42 as StdButton with uid="XLVQQIKCCZ",left=704, top=426, width=48,height=45,;
    CpPicture="BMP\GENERA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per inizio elaborazione";
    , HelpContextID = 68617446;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      with this.Parent.oContained
        GSMR2BGP(this.Parent.oContained,"GSMR_BGP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ("S" $ nvl(.w_PMOCLSUG+.w_PMOCLPIA+.w_PMOCLORD+.w_PMODLPIA+.w_PMODLLAN+.w_PMODLSUG+.w_PMODAPIA+.w_PMODALAN+.w_PMODASUG," "))
      endwith
    endif
  endfunc


  add object oBtn_2_43 as StdButton with uid="HQJKUHYERD",left=756, top=426, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75906118;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_43.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_17 as StdString with uid="XMTZKDDJKA",Visible=.t., Left=13, Top=160,;
    Alignment=1, Width=117, Height=15,;
    Caption="Data inizio elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="ZFEHJZWTEA",Visible=.t., Left=7, Top=185,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data fine elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ZEAIRXNLNG",Visible=.t., Left=169, Top=433,;
    Alignment=1, Width=105, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="LRYBARYNYB",Visible=.t., Left=25, Top=109,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="UVHEDKVHXW",Visible=.t., Left=25, Top=134,;
    Alignment=1, Width=105, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="FRFBQXOYXD",Visible=.t., Left=25, Top=9,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="FWPPGBGFDX",Visible=.t., Left=25, Top=34,;
    Alignment=1, Width=105, Height=15,;
    Caption="A commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="QPHXLBLWMU",Visible=.t., Left=268, Top=83,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="QUWNGCOYRD",Visible=.t., Left=381, Top=61,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="FSAEEGKAFC",Visible=.t., Left=388, Top=86,;
    Alignment=1, Width=88, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="TGVEUCHXYZ",Visible=.t., Left=25, Top=59,;
    Alignment=1, Width=105, Height=15,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="VZMOFHLMWU",Visible=.t., Left=25, Top=84,;
    Alignment=1, Width=105, Height=15,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="KLVNNVVWGD",Visible=.t., Left=268, Top=59,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="JUEPGIFVRE",Visible=.t., Left=6, Top=262,;
    Alignment=1, Width=124, Height=18,;
    Caption="Filtri impegni/articoli:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsmr_kmm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsmr_kmm
define class LineMrp as StdBox
  Visible=.t.
  
  proc calculate
  
  endproc

  proc event(cEvent)
  
  endproc
  
  
enddefine
* --- Fine Area Manuale
