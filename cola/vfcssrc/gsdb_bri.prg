* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bri                                                        *
*              Generazione MPS (ODP)                                           *
*                                                                              *
*      Author: TAM Software Srl (SM)                                           *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-08-16                                                      *
* Last revis.: 2017-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bri",oParentObject)
return(i_retval)

define class tgsdb_bri as StdBatch
  * --- Local variables
  w_GFATHER = .NULL.
  w_CATFIN = space(5)
  w_CATINI = space(5)
  w_CODFIN = space(41)
  w_CODINI = space(41)
  w_FAMAFIN = space(5)
  w_FAMAINI = space(5)
  w_FAMFIN = space(5)
  w_FAMINI = space(5)
  w_GRUFIN = space(5)
  w_GRUINI = space(5)
  w_MAGFIN = space(5)
  w_MAGINI = space(5)
  w_MARFIN = space(5)
  w_MARINI = space(5)
  w_PIAFIN = space(5)
  w_PIAINI = space(5)
  w_ELAODF = space(1)
  w_ELAMPS = space(1)
  w_CRIFOR = space(1)
  w_PPTIPDTF = space(1)
  pTIPGES = space(1)
  w_PRGIOCOP = 0
  w_appo1 = space(10)
  cCURKEY = space(20)
  tmpN = 0
  w_OK = .f.
  cKEYSAL = space(20)
  TmpC = space(10)
  w_OLSERIAL = space(15)
  cCODART = space(20)
  TmpD = ctod("  /  /  ")
  w_PP___DTF = 0
  cCODVAR = space(20)
  cCODCOM = space(15)
  cCODATT = space(15)
  i = 0
  cPERDTF = space(3)
  cPERDTFG = space(3)
  cCODICE = space(20)
  Calendario = space(5)
  DataDiBase = ctod("  /  /  ")
  W_ALL = space(1)
  cMAGPRE = space(5)
  cUNIMIS = space(3)
  cUNIMIS2 = space(3)
  w_NUMPER = 0
  cLEAMPS = 0
  cCURCOD = space(20)
  cDatIniMPS = ctod("  /  /  ")
  cLEAFIS = 0
  cPERSCA = 0
  cDatIniPRO = ctod("  /  /  ")
  cDatFinMPS = ctod("  /  /  ")
  cLOTMED = 0
  cCOEFLT = 0
  cLEAPRO = 0
  w_DatErr = .f.
  cMPSLEA = 0
  cLOTECO = 0
  cLOTMAX = 0
  DatRisul = ctod("  /  /  ")
  cPREVIS = 0
  cCICRID = space(41)
  pCodice = space(40)
  cORDINI = 0
  w_MAGPROD = space(5)
  pDatIni = ctod("  /  /  ")
  cRESPRE = 0
  w_OLTCOMAG = space(5)
  pDatFin = ctod("  /  /  ")
  cDOMANDA = 0
  cTIPGES = space(1)
  cQTASAL = 0
  cORDFOR = 0
  cPUNRIO = 0
  cPROPRE = space(1)
  cMPSCON = 0
  cPUNLOT = 0
  w_OLTCOFOR = space(15)
  cONHAND = 0
  w_CODCENTRO = space(20)
  cSCOMIN = 0
  cORDMPS = space(1)
  w_OLTPROVE = space(1)
  cPERIODO = space(3)
  w_OLTSECIC = space(10)
  cSUMCUMRES = 0
  cMPSDAP = 0
  w_CODICE = space(41)
  cGRUMER = space(5)
  cMPSPIA = 0
  cMPSLAN = 0
  w_GIOAPP = 0
  w_PRCODVAR = space(20)
  cMPSSUG = 0
  w_QTAMIN = 0
  w_QTAMAX = 0
  w_PRQTAMIN = 0
  w_PRQTAMAX = 0
  cMPSTOT = 0
  w_QTALOT = 0
  w_PRLOTRIO = 0
  cPERASS = space(3)
  w_DATELA = ctod("  /  /  ")
  w_PRGIOAPP = 0
  cPABCUM = 0
  w_LEADTIME = 0
  w_PRCODFOR = space(15)
  cODPCOR = 0
  pDataRif = ctod("  /  /  ")
  cPRIGIOLAV = ctod("  /  /  ")
  cPRIGIOPER = ctod("  /  /  ")
  cFAMPRO = space(5)
  cLOTPRO = 0
  cFLCOMM = space(1)
  pDETTMPS = space(1)
  cDATINI = ctod("  /  /  ")
  cTIPCOS = space(5)
  w_PPCARVAL = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_IMPCOM = 0
  w_OLTFLIMC = space(1)
  w_COSTO = 0
  w_PESERORD = space(15)
  w_PEQTAABB = 0
  w_PERESIDUO = 0
  w_OLTCONTR = space(5)
  cCOERSS = 0
  w_DATRSS = ctod("  /  /  ")
  w_PERRSS = space(3)
  w_OLDIDX = space(58)
  w_NEWIDX = space(58)
  w_NRECELAB = 0
  w_NRECCUR = 0
  cPREZUM = space(1)
  cOPERAT = space(1)
  cMOLTIP = 0
  cUNMIS2 = space(3)
  cFLCOM1 = space(1)
  w_QTA = 0
  cSUGTOT = 0
  w_STATO = space(1)
  w_SERGEN = space(10)
  w_FLSUG = space(1)
  w_FLPIA = space(1)
  w_FLCON = space(1)
  w_FLDAP = space(1)
  w_FLORD = space(1)
  cARPREZUM = space(1)
  w_DL__GUID = space(10)
  w_DATLAN = ctod("  /  /  ")
  w_PRCRIFOR = space(1)
  w_PZ = 0
  w_S1 = 0
  w_S2 = 0
  w_S3 = 0
  w_S4 = 0
  w_PZ = 0
  w_VAL = space(3)
  w_CAO = 0
  w_CODCON = space(15)
  cCODDIS = space(20)
  w_CONUMERO = space(15)
  w_LOTRIO = 0
  cCICLAV = space(15)
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  cTIPDTF = space(3)
  cGIODTF = 0
  * --- WorkFile variables
  CAN_TIER_idx=0
  DOC_DETT_idx=0
  MPS_TPER_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  PAR_RIOR_idx=0
  VALUTE_idx=0
  ART_ICOL_idx=0
  SALDIART_idx=0
  CONTI_idx=0
  SALDICOM_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Periodi orizzonte MPS
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPNUMPER,PPCALSTA,PP___DTF,PPMAGPRO,PPCARVAL"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPNUMPER,PPCALSTA,PP___DTF,PPMAGPRO,PPCARVAL;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMPER = NVL(cp_ToDate(_read_.PPNUMPER),cp_NullValue(_read_.PPNUMPER))
      this.Calendario = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.w_PP___DTF = NVL(cp_ToDate(_read_.PP___DTF),cp_NullValue(_read_.PP___DTF))
      this.w_MAGPROD = NVL(cp_ToDate(_read_.PPMAGPRO),cp_NullValue(_read_.PPMAGPRO))
      this.w_PPCARVAL = NVL(cp_ToDate(_read_.PPCARVAL),cp_NullValue(_read_.PPCARVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if TYPE("this.oParentObject.oParentObject.class")="C" and UPPER(this.oParentObject.oParentObject.class) = "TGSDB_KGF"
      * --- Se da Generazione MPS applico i filtri della maschera GSDB_KGF
      this.w_GFATHER = this.oParentObject.oParentObject
      this.w_CATFIN = this.w_GFATHER.w_CATFIN
      this.w_CATINI = this.w_GFATHER.w_CATINI
      this.w_CODFIN = this.w_GFATHER.w_CODFIN
      this.w_CODINI = this.w_GFATHER.w_CODINI
      this.w_FAMAFIN = this.w_GFATHER.w_FAMAFIN
      this.w_FAMAINI = this.w_GFATHER.w_FAMAINI
      this.w_GRUFIN = this.w_GFATHER.w_GRUFIN
      this.w_GRUINI = this.w_GFATHER.w_GRUINI
      this.w_MAGFIN = this.w_GFATHER.w_MAGFIN
      this.w_MAGINI = this.w_GFATHER.w_MAGINI
      this.w_MARFIN = this.w_GFATHER.w_MARFIN
      this.w_MARINI = this.w_GFATHER.w_MARINI
      this.w_ELAODF = this.w_GFATHER.w_ELAODF
      this.w_ELAMPS = this.w_GFATHER.w_ELAMPS
      this.w_CRIFOR = this.w_GFATHER.w_CRIFOR
      this.w_PPTIPDTF = this.w_GFATHER.w_PPTIPDTF
      this.w_PP___DTF = this.w_GFATHER.w_PP___DTF
    endif
    * --- Crea array periodi
    dimension PeriodiMPS(100)
    vq_exec ("..\COLA\exe\query\GSDB_BRI", this, "_PeriodiMPS_")
    Cur=WrCursor("_PeriodiMPS_")
    select * from _PeriodiMPS_ into array PeriodiMPS
    if g_PRFA="S" and g_CICLILAV="S" 
      ah_msg("Lettura cicli di lavorazione...")
      vq_exec("..\PRFA\exe\query\GSCI_BRI", this, "_CicloPref_")
      if g_COLA = "S"
        vq_exec("..\COLA\exe\query\GSCOFBCS", this, "_CicExtMono_")
        vq_exec("..\COLA\exe\query\GSCOEBCS", this, "Terzista")
      endif
    endif
    * --- Cursore del Calendario
    vq_exec("..\COLA\exe\query\GSDBiBLT", this, "_Calend_") 
 Select *, dtos(cagiorno) as datidx from _Calend_ into cursor Calend 
 =wrcursor("Calend") 
 index on datidx tag datidx 
 set order to 1 
 use in _Calend_
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Generazione MPS per gestione a fabbisogno
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("Calend")
      Use in Calend
    endif
    if used("_CicloPref_")
      Use in _CicloPref_
    endif
    if used("_CicExtMono_")
      Use in _CicExtMono_
    endif
    if used("Terzista")
      Use in Terzista
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge saldi ....
    ah_msg("Lettura dati giacenze in corso...") 
 vq_exec("..\COLA\exe\query\GSDBPBRI", this, "Saldi") 
 Select keysal,sum(qtasal) as qtasal from Saldi Group by keysal into cursor _TempGiac_ 
 Use in Saldi
    * --- Crea indice
    this.tmpN = WRCursor("_TempGiac_")
    select _TempGiac_ 
 index on KEYSAL tag KEYSAL 
 set order to 1
    * --- Legge scorte di sicurezza ...
    ah_msg("Lettura dati articoli in corso...")
    vq_exec ("..\COLA\exe\query\GSDB2BRI", this, "_TempSMin_")
    * --- Crea indice
    this.tmpN = WRCursor("_TempSMin_")
    select _TempSMin_ 
 index on KEYSAL tag KEYSAL 
 set order to 1
    * --- Crea Cursori
    this.pTIPGES = "F"
    * --- Cursore con gli ORDINI CLIENTE da documenti (SOLO MAGAZZINI PRODUZIONE)
    ah_msg("Lettura dati in corso...")
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARFLCOMM = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ARFLCOMM = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows>0
      * --- Esiste almeno un articolo gestito a Commessa personaliizata
      do vq_exec WITH "..\COLA\exe\query\GSDBHBRI", this, "Commessa"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      Select KEYSAL,PERASS,DATINI,PREVIS,(ORDINI+FABBIS) as ORDINI,DOMANDA,ORDFOR,MPSSUG,MPSCON,; 
 MPSDAP,MPSPIA,MPSLAN,MPSPAB,CODCOM,TIPATT,CODATT,ARFLCOM1,ARCODDIS, PRTIPDTF, PRGIODTF ; 
 from Commessa order by KEYSAL,PERASS,DATINI,CODCOM into cursor _Globale_
      if used("Commessa")
        Use in Commessa
      endif
    else
      * --- Semplifica l'estrazione dei dati (sbianca le commesse)
      do vq_exec WITH "..\COLA\exe\query\GSDB1BRI", this, "NoComm"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      Select KEYSAL,PERASS,DATINI,PREVIS,(ORDINI+FABBIS) as ORDINI,DOMANDA,ORDFOR,MPSSUG,MPSCON,; 
 MPSDAP,MPSPIA,MPSLAN,MPSPAB,CODCOM,TIPATT,CODATT,ARFLCOM1,ARCODDIS, PRTIPDTF, PRGIODTF; 
 from NoComm order by KEYSAL,PERASS,DATINI,CODCOM into cursor _Globale_
      if used("NoComm")
        Use in NoComm
      endif
    endif
    * --- Tiene conto del DTF (Demand Time Fence)
    if this.w_PP___DTF=0
      this.cPERDTFG = "000"
    else
      * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
      this.TmpD = COCALCLT(i_DATSYS,this.w_PP___DTF,"A",TRUE,"")
      * --- Select from MPS_TPER
      i_nConn=i_TableProp[this.MPS_TPER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select TPPERASS  from "+i_cTable+" MPS_TPER ";
            +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" AND "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
             ,"_Curs_MPS_TPER")
      else
        select TPPERASS from (i_cTable);
         where TPDATINI<=this.TmpD AND this.TmpD<=TPDATFIN;
          into cursor _Curs_MPS_TPER
      endif
      if used('_Curs_MPS_TPER')
        select _Curs_MPS_TPER
        locate for 1=1
        do while not(eof())
        this.cPERDTFG = nvl(_Curs_MPS_TPER.TPPERASS,"000")
          select _Curs_MPS_TPER
          continue
        enddo
        use
      endif
    endif
    * --- Esegue scan inversa cursore
    ah_msg("Determinazione domanda di periodo in corso...")
    this.cSUMCUMRES = 0
    this.cCURKEY = repl("z",20)
    select _Globale_ 
 xx=WrCursor("_Globale_")
    go botto
    do while not bof()
      * --- Calcola Domanda
      this.cKEYSAL = nvl(_Globale_.KEYSAL,space(41))
      this.cPERASS = nvl(_Globale_.PERASS, space(3))
      this.cCODCOM = nvl(_Globale_.CODCOM, space(15))
      this.cFLCOM1 = nvl(_Globale_.ARFLCOM1, space(1))
      this.cCODDIS = nvl(_Globale_.ARCODDIS, space(20))
      this.cTIPDTF = nvl(_Globale_.PRTIPDTF, "E")
      this.cGIODTF = nvl(_Globale_.PRGIODTF, this.w_PP___DTF)
      if this.w_PPTIPDTF="F" or (this.w_PPTIPDTF<>"F" AND this.cTIPDTF="E")
        this.cPERDTF = this.cPERDTFG
      else
        * --- Lo ricalcolo olo se cambia articolo
        if this.cKEYSAL # this.cCURKEY
          this.cPERDTF = "000"
          * --- Determina il giorno fino al quale non deve tenere conto delle previsioni
          if this.cPERDTF = "000" and this.cGIODTF>0
            this.TmpD = COCALCLT(i_DATSYS,this.cGIODTF,"A",TRUE,"")
            * --- Select from MPS_TPER
            i_nConn=i_TableProp[this.MPS_TPER_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MPS_TPER_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select TPPERASS  from "+i_cTable+" MPS_TPER ";
                  +" where TPDATINI<="+cp_ToStrODBC(this.TmpD)+" AND "+cp_ToStrODBC(this.TmpD)+"<=TPDATFIN";
                   ,"_Curs_MPS_TPER")
            else
              select TPPERASS from (i_cTable);
               where TPDATINI<=this.TmpD AND this.TmpD<=TPDATFIN;
                into cursor _Curs_MPS_TPER
            endif
            if used('_Curs_MPS_TPER')
              select _Curs_MPS_TPER
              locate for 1=1
              do while not(eof())
              this.cPERDTF = nvl(_Curs_MPS_TPER.TPPERASS,"000")
                select _Curs_MPS_TPER
                continue
              enddo
              use
            endif
            select _Globale_
          endif
        endif
      endif
      * --- Test cambio codice
      if this.cKEYSAL # this.cCURKEY
        this.cCURKEY = this.cKEYSAL
        this.cSUMCUMRES = 0
      endif
      * --- Legge Previsioni e ordini ...
      this.cPREVIS = iif(this.cPERASS>this.cPERDTF, nvl(_Globale_.PREVIS,0), 0)
      this.cORDINI = nvl(_Globale_.ORDINI,0)
      if this.cPREVIS + this.cORDINI <> 0
        if empty(this.cCODCOM)
          * --- Calcola Residuo
          this.cRESPRE = this.cPREVIS - this.cORDINI - this.cSUMCUMRES
          if this.cRESPRE < 0
            this.cSUMCUMRES = - this.cRESPRE
            this.cRESPRE = 0
          else
            this.cSUMCUMRES = 0
          endif
        else
          this.cRESPRE = 0
        endif
        * --- Domanda ...
        this.cDOMANDA = this.cORDINI + this.cRESPRE
        if this.cDOMANDA > 0
          replace DOMANDA with this.cDOMANDA
        endif
      endif
      * --- Passa al record precedente
      if not bof()
        skip -1
      endif
    enddo
    =wrcursor("_Globale_") 
 index on KEYSAL+PERASS+CODCOM TAG IDX1 
 set order to IDX1
    this.cCURKEY = repl("z",20)
    * --- Determina MPS
    select _Globale_
    Scan
    * --- Test cambio codice
    this.cKEYSAL = nvl(_Globale_.KEYSAL,space(41))
    this.cPERASS = nvl(_Globale_.PERASS, space(3))
    this.cCODCOM = nvl(_Globale_.CODCOM, space(15))
    this.cFLCOM1 = nvl(_Globale_.ARFLCOM1, space(1))
    this.cCODDIS = nvl(_Globale_.ARCODDIS, space(20))
    this.cCODATT = nvl(_Globale_.CODATT, space(15))
    this.cDATINI = _Globale_.DATINI
    if this.cKEYSAL # this.cCURKEY
      * --- Se cambia articolo rilegge i dati articolo produzione
      this.w_OLDIDX = this.cKEYSAL+this.cPERASS+this.cCODCOM
      this.w_NEWIDX = space(58)
      select _TempSMin_ 
 seek this.cKEYSAL
      this.cSCOMIN = nvl(_TempSMin_.SCOMIN,0)
      this.cCOERSS = nvl(_TempSMin_.COERSS,0)
      this.cCODICE = nvl(_TempSmin_.CODART,space(20))
      this.cCODART = nvl(_TempSmin_.CODART,space(20))
      this.cCODVAR = nvl(_TempSmin_.CODVAR,space(20))
      this.cUNIMIS = nvl(_TempSmin_.UNMIS1,space(3))
      this.cUNIMIS2 = nvl(_TempSmin_.UNMIS2,space(3))
      this.cMPSLEA = nvl(_TempSmin_.LEAMPS,0)
      this.cFAMPRO = nvl(_TempSmin_.FAMPRO, space(5))
      this.cLOTECO = nvl(_TempSmin_.LOTECO,0)
      this.cLOTMAX = nvl(_TempSmin_.LOTMAX,0)
      this.cLOTPRO = nvl(_TempSmin_.LOTPRO,0)
      this.cPERSCA = nvl(_TempSmin_.PERSCA,0)
      this.cMAGPRE = nvl(_TempSmin_.MAGPRE,space(5))
      this.cLEAMPS = nvl(_TempSmin_.LEAMPS,0)
      this.cLEAFIS = nvl(_TempSmin_.LEAFIS,0)
      this.cCOEFLT = nvl(_TempSmin_.COEFLT,0)
      this.cLOTMED = nvl(_TempSmin_.LOTMED,0)
      this.cPROPRE = nvl(_TempSmin_.PROPRE," ")
      this.cGRUMER = nvl(_TempSmin_.GRUMER," ")
      this.cFLCOMM = nvl(_TempSmin_.FLCOMM," ")
      this.cTIPCOS = nvl(_TempSmin_.TIPCOS,space(5))
      this.cPREZUM = nvl(_TempSmin_.PREZUM,space(1))
      this.cOPERAT = nvl(_TempSmin_.OPERAT,space(1))
      this.cMOLTIP = nvl(_TempSmin_.MOLTIP,0)
      this.cUNMIS2 = nvl(_TempSmin_.UNMIS2,space(3))
      this.cORDMPS = nvl(_TempSmin_.ORDMPS,"S")
      this.cARPREZUM = nvl(_TempSmin_.ARPREZUM,"N")
      this.cCICLAV = nvl(_TempSmin_.DBCODRIS,space(15))
      if this.cFLCOMM="S"
        this.cSCOMIN = 0
        this.cLOTECO = 0
        this.cLOTMAX = 0
        this.cLOTPRO = 0
      endif
      if this.cSCOMIN>0
        * --- Calcola il periodo per il ripristino della Scorta di sicurezza
        if this.cCOERSS*this.cLEAFIS>0
          this.w_DATRSS = COCALCLT(i_DATSYS,this.cCOERSS*this.cLEAFIS,"A",False,"")
          Select _PeriodiMPS_ 
 locate for tpdatini<=this.w_DATRSS and this.w_DATRSS<=tpdatfin
          if found()
            this.w_PERRSS = tpperass
          else
            this.w_PERRSS = "001"
          endif
        else
          this.w_PERRSS = "001"
        endif
        this.w_NEWIDX = this.cKEYSAL+this.w_PERRSS+this.cCODCOM
      endif
      do case
        case empty(nvl(this.w_PPCARVAL,"")) or this.w_PPCARVAL="CS"
          this.w_COSTO = nvl(_TempSmin_.COSSTA,0)
        case this.w_PPCARVAL="CM"
          this.w_COSTO = nvl(_TempSmin_.COSMED,0)
        case this.w_PPCARVAL="UC"
          this.w_COSTO = nvl(_TempSmin_.COSULT,0)
        case this.w_PPCARVAL="CL"
          this.w_COSTO = nvl(_TempSmin_.COSLIS,0)
      endcase
      * --- Messaggio Video ...
      ah_msg("Esame articolo: %1/%2 in corso...",.t.,.f.,.f., alltrim(this.cCODART), alltrim(this.cCODVAR))
      * --- Legge dato giacenza fisica
      select _TempGiac_
      this.cONHAND = iif(seek(this.cKEYSAL), nvl(_TempGiac_.QTASAL,0), 0)
      * --- PAB Iniziale
      this.cPABCUM = 0
      this.cCURKEY = this.cKEYSAL
      this.TmpC = "000"
      this.cCURCOD = this.cCODICE
      * --- Cerca il ciclo preferenziale
      this.w_OLTSECIC = space(10)
      if g_PRFA="S" and g_CICLILAV="S" and used("_CicloPref_")
        this.cCODDIS = nvl(_Globale_.ARCODDIS, space(20))
        Select _CicloPref_ 
 go top 
 locate for clcoddis=this.cCODDIS and clcodart=this.cCODART
        this.w_OLTSECIC = iif(found(), _CicloPref_.CLSERIAL, space(10))
      endif
      * --- Determina provenienza
      this.w_OLTPROVE = "I"
      this.w_OLTCOFOR = space(15)
      this.w_PRQTAMIN = 0
      this.w_PRQTAMAX = 0
      this.w_PRLOTRIO = 0
      this.w_PRGIOAPP = 0
      if g_COLA = "S"
        * --- Articolo con provenienza preferenziale CL
        if g_PRFA="S" and g_CICLILAV="S" and not empty(this.w_OLTSECIC)
          * --- Con il modulo cicli, occorre verificare il caso particolare di ciclo monofase su CL esterno (come salto codice)
          * --- Usa come filtro w_OLTSECIC
          if used("_CicExtMono_")
            select _CicExtMono_ 
 go top 
 locate for clserial=this.w_OLTSECIC
            if found()
              this.w_OLTPROVE = "L"
              this.w_CODCENTRO = nvl(_CicExtMono_.LRCODRIS ," ")
              this.w_OLTCOFOR = space(15)
              if used("Terzista")
                Select Terzista 
 go top 
 locate for lfcodice=this.w_CODCENTRO
                this.w_OLTCOFOR = iif(found(), lfcodfor, space(15))
              endif
            endif
          endif
          if this.w_OLTPROVE="L" and not empty(this.w_OLTCOFOR)
            this.w_PRCODVAR = iif(empty(this.cCODVAR),"#",this.cCODVAR)
            * --- Read from PAR_RIOR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PRGIOAPP,PRLOTRIO,PRQTAMIN,PRCODFOR,PRQTAMAX"+;
                " from "+i_cTable+" PAR_RIOR where ";
                    +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PRGIOAPP,PRLOTRIO,PRQTAMIN,PRCODFOR,PRQTAMAX;
                from (i_cTable) where;
                    PRCODART = this.cCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PRGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
              this.w_PRLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
              this.w_PRQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
              this.w_PRCODFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
              this.w_PRQTAMAX = NVL(cp_ToDate(_read_.PRQTAMAX),cp_NullValue(_read_.PRQTAMAX))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if not (this.w_OLTCOFOR=this.w_PRCODFOR or empty(this.w_PRCODFOR))
              this.w_PRQTAMIN = 0
              this.w_PRQTAMAX = 0
              this.w_PRLOTRIO = 0
              this.w_PRGIOAPP = 0
            endif
          endif
        else
          * --- Il modulo Cicli non c'� o non � abilitato => E' un articolo di CL con salto di codice => mette prov. CL
          if this.cPROPRE = "L"
            this.w_PRCODVAR = iif(empty(this.cCODVAR),"#",this.cCODVAR)
            * --- Read from PAR_RIOR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PRCODFOR,PRLOTRIO,PRQTAMIN,PRGIOAPP,PRQTAMAX"+;
                " from "+i_cTable+" PAR_RIOR where ";
                    +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PRCODFOR,PRLOTRIO,PRQTAMIN,PRGIOAPP,PRQTAMAX;
                from (i_cTable) where;
                    PRCODART = this.cCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
              this.w_PRLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
              this.w_PRQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
              this.w_PRGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
              this.w_PRQTAMAX = NVL(cp_ToDate(_read_.PRQTAMAX),cp_NullValue(_read_.PRQTAMAX))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OLTPROVE = "L"
          endif
        endif
      endif
      Select _globale_
      if not empty(this.w_NEWIDX)
        if not seek(this.w_NEWIDX)
          Insert into _globale_ (KEYSAL,PERASS,DATINI,CODCOM,TIPATT,CODATT,ARCODDIS) ; 
 values (this.cKEYSAL, this.w_PERRSS, this.w_DATRSS, this.cCODCOM, "A", this.cCODATT,this.cCODDIS)
        endif
        Seek this.w_OLDIDX
      endif
    endif
    * --- Nettizza la domanda
    this.cMPSSUG = 0
    this.cORDFOR = nvl(_Globale_.ORDFOR,0)
    this.cMPSCON = nvl(_Globale_.MPSCON,0)
    this.cMPSDAP = nvl(_Globale_.MPSDAP,0)
    this.cMPSPIA = nvl(_Globale_.MPSPIA,0)
    this.cMPSLAN = nvl(_Globale_.MPSLAN,0)
    this.cODPCOR = this.cMPSCON+ this.cMPSDAP+ this.cMPSPIA+ this.cMPSLAN
    if empty(this.cCODCOM)
      if this.cONHAND > nvl(_Globale_.DOMANDA,0) + iif(this.cPERASS=this.w_PERRSS, this.cSCOMIN, 0)
        this.cONHAND = this.cONHAND - nvl(_Globale_.DOMANDA,0) - iif(this.cPERASS=this.w_PERRSS, this.cSCOMIN, 0)
        this.cDOMANDA = 0
      else
        this.cDOMANDA = nvl(_Globale_.DOMANDA,0) + iif(this.cPERASS=this.w_PERRSS, this.cSCOMIN, 0) - this.cONHAND
        this.cONHAND = 0
        this.cDOMANDA = this.cDOMANDA * (1+this.cPERSCA/100)
      endif
      * --- Calcola la scorta disponibile consideranto PAB regresso
      this.tmpN = this.cPABCUM+this.cODPCOR+this.cORDFOR
      if this.cPERASS=this.w_PERRSS
        * --- Azzera la scorta di sicurezza (impegna solo nel periodo scaduto)
        this.cSCOMIN = 0
      endif
    else
      this.cDOMANDA = nvl(_Globale_.DOMANDA,0)
      this.tmpN = this.cODPCOR+this.cORDFOR
    endif
    * --- Suggerimento
    this.cSUGTOT = 0
    this.cMPSSUG = max(this.cDOMANDA-this.TmpN,0)
    * --- Emette ODP
    if this.cMPSSUG > 0
      * --- Determina giorni
      this.tmpN = int(val(this.cPERASS))
      this.cPRIGIOPER = cp_ToDate(PeriodiMPS(this.TmpN+1,3))
      this.cPRIGIOLAV = cp_ToDate(PeriodiMPS(this.TmpN+1,5))
      * --- Conteggia Lead Time MPS
      this.cDatIniMPS = cp_CharToDate("  -  -  ")
      this.w_OLTFLIMC = "N"
      if empty(this.cCODCOM)
        this.cDatFinMPS = iif(empty(this.cPRIGIOLAV),this.cPRIGIOPER,this.cPRIGIOLAV)
      else
        * --- Gestione Commessa
        this.cDatFinMPS = this.cDATINI
        this.w_IMPCOM = cp_round(this.cMPSSUG * this.w_COSTO, g_PERPVL)
        this.w_CODVAL = " "
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODVAL"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.cCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODVAL;
            from (i_cTable) where;
                CNCODCAN = this.cCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CODVAL<>g_PERVAL
          * --- Conversione valuta
          this.w_CAOVAL = 0
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT,VACAOVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT,VACAOVAL;
              from (i_cTable) where;
                  VACODVAL = this.w_CODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            this.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_IMPCOM = VAL2CAM(this.w_IMPCOM,g_PERVAL,this.w_CODVAL,this.w_CAOVAL,this.cDatFinMPS,w_DECTOT)
        endif
        this.w_OLTFLIMC = iif(empty(this.cCODATT) or this.w_IMPCOM=0,"N","S")
      endif
      this.w_LEADTIME = this.cLEAMPS
      if this.w_LEADTIME>0
        this.cDatIniMPS = COCALCLT(this.cDatFinMPS,this.cLEAMPS,"I",False,"Calend")
      else
        this.cDatIniMPS = this.cDatFinMPS
      endif
      * --- Determina qta e tempi
      this.cLEAPRO = 0
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Conteggia Lead Time
      this.w_LEADTIME = this.cLEAPRO
      if this.w_LEADTIME>0
        this.cDatIniPRO = COCALCLT(this.cDatFinMPS,this.cLEAPRO,"I",False,"Calend")
      else
        this.cDatIniPRO = this.cDatFinMPS
      endif
      * --- EMETTE ODP
      * --- Try
      local bErr_04C9CE80
      bErr_04C9CE80=bTrsErr
      this.Try_04C9CE80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_DatErr = TRUE
      endif
      bTrsErr=bTrsErr or bErr_04C9CE80
      * --- End
    endif
    if not this.w_DatErr and empty(this.cCODCOM)
      * --- Aggiorna PAB Cumulativo
      this.cPABCUM = this.cPABCUM + this.cODPCOR + this.cSUGTOT + this.cORDFOR - this.cDOMANDA
    endif
    this.w_DatErr = FALSE
    select _Globale_
    EndScan
    * --- Chiude Cursori
    if used("_TempGiac_")
      USE IN _TempGiac_
    endif
    if used("_TempSMin_")
      USE IN _TempSMin_
    endif
    if used("_Globale_")
      USE IN _Globale_
    endif
    if used("_Contratti_")
      USE IN _Contratti_
    endif
    if used("ContrattiA")
      USE IN ContrattiA
    endif
    use in _PeriodiMPS_
  endproc
  proc Try_04C9CE80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Inserisce ODP
    this.w_OLTCOMAG = iif(empty(this.cMAGPRE),iif(empty(this.w_MAGPROD),g_MAGAZI,this.w_MAGPROD),this.cMAGPRE)
    this.w_QTA = this.cMPSSUG
    this.cSUGTOT = 0
    do while this.w_QTA>0
      * --- Questo ciclo serve a spezzare gli ODP che non rispettano il vincolo di quantit� massima, se il vincolo non c'� la procedura fa sempre un solo 'giro'
      if g_COLA = "S" and this.w_OLTPROVE="L"
        if this.w_QTAMAX<>0 and this.w_QTA>this.w_QTAMAX
          * --- Applico politica di lottizzazione quantit� massima
          this.cMPSSUG = this.w_QTAMAX
          * --- Fabbisogno residuo (da valutare al prossimo giro)
          this.w_QTA = this.w_QTA-this.cMPSSUG
        else
          if this.w_QTAMAX=0
            * --- Se la quantit� massima non � gestita faccio un solo giro nello while
            this.w_QTA = 0
          else
            if this.w_QTALOT<>0
              if this.w_QTAMIN<>0 and this.w_QTA<this.w_QTAMIN
                * --- Se � gestita in contemporanea la quantit� massima con la quantit� minima e il lotto multiplo e 
                *     sono all'ultimo giro devo riverificare le ultime due pol. lott.
                this.cMPSSUG = this.w_QTAMIN
                this.cMPSSUG = iif( Mod(this.cMPSSUG,this.w_QTALOT)=0, this.cMPSSUG, (Int(this.cMPSSUG/this.w_QTALOT)+1)*this.w_QTALOT)
                this.w_QTA = 0
              else
                this.cMPSSUG = this.w_QTA
                this.w_QTA = 0
              endif
            else
              this.cMPSSUG = this.w_QTA
              this.w_QTA = 0
            endif
          endif
        endif
      else
        if this.cLOTMAX<>0 and this.w_QTA>this.cLOTMAX
          * --- Applico politica di lottizzazione quantit� massima
          this.cMPSSUG = this.cLOTMAX
          * --- Fabbisogno residuo (da valutare al prossimo giro)
          this.w_QTA = this.w_QTA-this.cMPSSUG
        else
          if this.cLOTMAX=0
            * --- Se la quantit� massima non � gestita faccio un solo giro nello while
            this.w_QTA = 0
          else
            * --- Se sono all'ultimo giro tengo l'avanzo
            this.cMPSSUG = this.w_QTA
            this.w_QTA = 0
          endif
        endif
      endif
      * --- Assegna Progressivo ODP
      this.w_OLSERIAL = space(15)
      this.w_OLSERIAL = cp_GetProg("ODL_MAST","PRODL",this.w_OLSERIAL,i_CODAZI)
      this.cSUGTOT = this.cSUGTOT+this.cMPSSUG
      * --- Se chiamato da Generazione MPS setto lo stato impostato nella maschera...
      if TYPE("this.oParentObject.oParentObject.class") = "C" and UPPER(this.oParentObject.oParentObject.class)="TGSDB_KGF"
        if this.oParentObject.oParentObject.w_PARAM="F"
          this.w_STATO = this.oParentObject.oParentObject.w_STATO
        else
          if this.cPROPRE="E"
            this.w_STATO = this.oParentObject.oParentObject.w_STATO1
          else
            this.w_STATO = this.oParentObject.oParentObject.w_STATO
          endif
        endif
      else
        this.w_STATO = "S"
      endif
      this.w_STATO = iif (this.w_STATO="A",this.cORDMPS,this.w_STATO)
      this.w_FLSUG = iif(this.w_STATO="S","+"," ")
      this.w_FLCON = iif(this.w_STATO="C" and this.cPROPRE<>"E","+"," ")
      this.w_FLDAP = iif((this.w_STATO="D" and this.cPROPRE<>"E") or (this.w_STATO="C" and this.cPROPRE="E"),"+"," ")
      this.w_FLORD = iif(this.w_STATO="S"," ","+")
      this.w_FLPIA = iif(this.w_STATO="P","+"," ")
      this.w_DATLAN = iif(this.w_FLPIA="+",i_DATSYS,cp_CharToDate("  -  -    "))
      this.w_CODCON = ""
      if this.cPROPRE="E" 
        this.w_OLTPROVE = "E"
        this.w_PRCODVAR = iif(empty(this.cCODVAR),"#",this.cCODVAR)
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRCODFOR,PRGIOCOP,PRQTAMAX"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.cCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRCODFOR,PRGIOCOP,PRQTAMAX;
            from (i_cTable) where;
                PRCODART = this.cCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLTCOFOR = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
          this.w_PRGIOCOP = NVL(cp_ToDate(_read_.PRGIOCOP),cp_NullValue(_read_.PRGIOCOP))
          this.cLOTMAX = NVL(cp_ToDate(_read_.PRQTAMAX),cp_NullValue(_read_.PRQTAMAX))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se non c'�, crea il cursore dei contratti di acquisto
        if not used("ContrattiA")
          this.w_DATELA = i_DATSYS
          vq_exec("..\COLA\exe\query\GSDB4BRI", this, "ContrattiA")
        endif
        * --- Cerca il miglior fornitore dai ContrattiA
        * --- Seleziona Criterio di scelta
        do case
          case this.w_CRIFOR="A"
            * --- Affidabilit�
            OrdinaPer = "Order by coaffida desc"
          case this.w_CRIFOR="T"
            * --- Tempo = giorni di approvvigionamento
            OrdinaPer = "Order by cogioapp"
          case this.w_CRIFOR="R"
            * --- Ripartizione
            OrdinaPer = "Order by coperrip"
          case this.w_CRIFOR="I"
            * --- Priorit�
            OrdinaPer = "Order by copriori desc"
        endcase
        if this.w_CRIFOR<>"Z"
          * --- Seleziona i ContrattiA validi e li ordina per il criterio di scelta
          Select * from ContrattiA ;
          where fbcodart=this.cCODART and codatini<=i_DatSys and i_DatSys<=codatfin and (cocodclf=this.w_OLTCOFOR or empty(nvl(this.w_OLTCOFOR,"")));
          &OrdinaPer into cursor tempor
        else
          * --- Calcola Prezzo da Contratto
          * --- Prima genera un temporaneo contenente le righe articolo interessate dei ContrattiA validi
          * --- ordinate per Fornitore + Qta Scaglione
          * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
          Select * from ContrattiA ;
          where fbcodart=this.cCODART and codatini<=i_DatSys and i_DatSys<=codatfin ;
          and nvl(coquanti,0)>=this.cMPSSUG and not empty(nvl(cocodclf," ")) and (cocodclf=this.w_OLTCOFOR or empty(nvl(this.w_OLTCOFOR,"")));
          order by cocodclf, coquanti into cursor Contprez
          * --- Calcola Prezzo da Contratto
          * --- Elabora Criterio per Miglior Prezzo
          =WrCursor("Contprez")
          Index On cocodclf+str(coquanti,12,3) Tag Codice
          * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
          this.w_appo1 = "######"
          select Contprez
          go top
          scan
          if cocodclf <> this.w_APPO1
            * --- Nuovo Fornitore, Calcola Prezzo da Contratto
            this.w_PZ = NVL(COPREZZO, 0)
            this.w_S1 = NVL(COSCONT1, 0)
            this.w_S2 = NVL(COSCONT2, 0)
            this.w_S3 = NVL(COSCONT3, 0)
            this.w_S4 = NVL(COSCONT4, 0)
            this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
            this.w_VAL = NVL(COCODVAL, g_PERVAL)
            this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
            if this.w_VAL<>g_PERVAL
              * --- Se altra valuta, riporta alla Valuta di conto
              if this.w_CAO<>0
                this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
                replace coprezzo with this.w_PZ
              else
                * --- Cambio incongruente, record non utilizzabile
                this.w_PZ = -999
              endif
              select Contprez
            endif
            if this.w_PZ=-999
              delete
            else
              this.w_appo1 = cocodclf
            endif
          else
            * --- Elimina record
            delete
          endif
          endscan
          * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
          * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
          * --- Ordino per il prezzo piu' basso
          Select * from ContPrez where not deleted() ;
          Order By Coprezzo into cursor tempor
          if used("ContPrez")
            Select contPrez
            use
          endif
        endif
        Select tempor
        if reccount()=0
          * --- Nessun contratto valido trovato
          if empty(this.w_CODCON)
            this.w_CODCON = space(15)
            this.w_CONUMERO = space(15)
          endif
        else
          go top
          this.w_CODCON = nvl(cocodclf,space(15))
          this.w_QTAMIN = nvl(coqtamin,0)
          this.w_LOTRIO = nvl(colotmul,0)
          this.w_GIOAPP = nvl(cogioapp,0)
          this.w_OLTCONTR = nvl(conumero,space(15))
          if this.cMPSSUG<this.w_QTAMIN
            this.cMPSSUG = this.w_QTAMIN
          endif
          if nvl(this.w_LOTRIO,0)>0
            this.cMPSSUG = ceiling(this.cMPSSUG/this.w_LOTRIO)*this.w_LOTRIO
          endif
          * --- Il lead time articolo � gi� stato calcolato in alto, qui lo rivalorizzo ed aggiusto il calcolo della data.
          this.cLEAPRO = iif(nvl(this.cLEAPRO,0)=0,this.w_GIOAPP,this.w_GIOAPP-this.cLEAPRO)
          if this.cLEAPRO<>0
            this.cDatIniPRO = COCALCLT(this.cDatIniPro,Ceiling(this.cLEAPRO),"C", FALSE ,"")
          endif
          this.cLEAPRO = this.w_GIOAPP
        endif
      endif
      this.w_CODCON = iif(empty(this.w_CODCON),this.w_OLTCOFOR,this.w_CODCON)
      * --- Insert into ODL_MAST
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"OLTCONTR"+",OLCODODL"+",OLCRIFOR"+",OLDATODP"+",OLOPEODP"+",OLTCOART"+",OLTCOATT"+",OLTCOCOS"+",OLTCODIC"+",OLTCOFOR"+",OLTCOMAG"+",OLTCOMME"+",OLTDINRIC"+",OLTDTLAN"+",OLTDTMPS"+",OLTDTRIC"+",OLTEMLAV"+",OLTFCOCO"+",OLTFLCON"+",OLTFLDAP"+",OLTFLEVA"+",OLTFLIMC"+",OLTFLIMP"+",OLTFLLAN"+",OLTFLORD"+",OLTFLPIA"+",OLTFLSUG"+",OLTFORCO"+",OLTIMCOM"+",OLTKEYSA"+",OLTLEMPS"+",OLTPERAS"+",OLTPROVE"+",OLTQTOD1"+",OLTQTODL"+",OLTQTOE1"+",OLTQTOEV"+",OLTQTSAL"+",OLTSTATO"+",OLTTICON"+",OLTTIPAT"+",OLTUNMIS"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",OLTDISBA"+",OLTCICLO"+",OLTSECIC"+",OLTDTCON"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_OLTCONTR),'ODL_MAST','OLTCONTR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLSERIAL),'ODL_MAST','OLCODODL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CRIFOR),'ODL_MAST','OLCRIFOR');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'ODL_MAST','OLDATODP');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','OLOPEODP');
        +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'ODL_MAST','OLTCOART');
        +","+cp_NullLink(cp_ToStrODBC(this.cCODATT),'ODL_MAST','OLTCOATT');
        +","+cp_NullLink(cp_ToStrODBC(this.cTIPCOS),'ODL_MAST','OLTCOCOS');
        +","+cp_NullLink(cp_ToStrODBC(iif(empty(this.cCODICE),this.cCODART,this.cCODICE)),'ODL_MAST','OLTCODIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'ODL_MAST','OLTCOFOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'ODL_MAST','OLTCOMAG');
        +","+cp_NullLink(cp_ToStrODBC(this.cCODCOM),'ODL_MAST','OLTCOMME');
        +","+cp_NullLink(cp_ToStrODBC(this.cDatIniPRO),'ODL_MAST','OLTDINRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATLAN),'ODL_MAST','OLTDTLAN');
        +","+cp_NullLink(cp_ToStrODBC(this.cDatIniMPS),'ODL_MAST','OLTDTMPS');
        +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTRIC');
        +","+cp_NullLink(cp_ToStrODBC(this.cLEAPRO),'ODL_MAST','OLTEMLAV');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFCOCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLCON),'ODL_MAST','OLTFLCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLDAP),'ODL_MAST','OLTFLDAP');
        +","+cp_NullLink(cp_ToStrODBC("N"),'ODL_MAST','OLTFLEVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTFLIMC),'ODL_MAST','OLTFLIMC');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLLAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLORD),'ODL_MAST','OLTFLORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLPIA),'ODL_MAST','OLTFLPIA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLSUG),'ODL_MAST','OLTFLSUG');
        +","+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFORCO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCOM),'ODL_MAST','OLTIMCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'ODL_MAST','OLTKEYSA');
        +","+cp_NullLink(cp_ToStrODBC(this.cLEAMPS),'ODL_MAST','OLTLEMPS');
        +","+cp_NullLink(cp_ToStrODBC(this.cPERASS),'ODL_MAST','OLTPERAS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTPROVE),'ODL_MAST','OLTPROVE');
        +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTOD1');
        +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTODL');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOE1');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','OLTQTOEV');
        +","+cp_NullLink(cp_ToStrODBC(this.cMPSSUG),'ODL_MAST','OLTQTSAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_STATO),'ODL_MAST','OLTSTATO');
        +","+cp_NullLink(cp_ToStrODBC("F"),'ODL_MAST','OLTTICON');
        +","+cp_NullLink(cp_ToStrODBC("A"),'ODL_MAST','OLTTIPAT');
        +","+cp_NullLink(cp_ToStrODBC(this.cUNIMIS),'ODL_MAST','OLTUNMIS');
        +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ODL_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(0),'ODL_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ODL_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'ODL_MAST','UTDV');
        +","+cp_NullLink(cp_ToStrODBC(this.cCODDIS),'ODL_MAST','OLTDISBA');
        +","+cp_NullLink(cp_ToStrODBC(this.cCICLAV),'ODL_MAST','OLTCICLO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OLTSECIC),'ODL_MAST','OLTSECIC');
        +","+cp_NullLink(cp_ToStrODBC(this.cDatFinMPS),'ODL_MAST','OLTDTCON');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'OLTCONTR',this.w_OLTCONTR,'OLCODODL',this.w_OLSERIAL,'OLCRIFOR',this.w_CRIFOR,'OLDATODP',i_DATSYS,'OLOPEODP',i_CODUTE,'OLTCOART',this.cCODART,'OLTCOATT',this.cCODATT,'OLTCOCOS',this.cTIPCOS,'OLTCODIC',iif(empty(this.cCODICE),this.cCODART,this.cCODICE),'OLTCOFOR',this.w_CODCON,'OLTCOMAG',this.w_OLTCOMAG,'OLTCOMME',this.cCODCOM)
        insert into (i_cTable) (OLTCONTR,OLCODODL,OLCRIFOR,OLDATODP,OLOPEODP,OLTCOART,OLTCOATT,OLTCOCOS,OLTCODIC,OLTCOFOR,OLTCOMAG,OLTCOMME,OLTDINRIC,OLTDTLAN,OLTDTMPS,OLTDTRIC,OLTEMLAV,OLTFCOCO,OLTFLCON,OLTFLDAP,OLTFLEVA,OLTFLIMC,OLTFLIMP,OLTFLLAN,OLTFLORD,OLTFLPIA,OLTFLSUG,OLTFORCO,OLTIMCOM,OLTKEYSA,OLTLEMPS,OLTPERAS,OLTPROVE,OLTQTOD1,OLTQTODL,OLTQTOE1,OLTQTOEV,OLTQTSAL,OLTSTATO,OLTTICON,OLTTIPAT,OLTUNMIS,UTCC,UTCV,UTDC,UTDV,OLTDISBA,OLTCICLO,OLTSECIC,OLTDTCON &i_ccchkf. );
           values (;
             this.w_OLTCONTR;
             ,this.w_OLSERIAL;
             ,this.w_CRIFOR;
             ,i_DATSYS;
             ,i_CODUTE;
             ,this.cCODART;
             ,this.cCODATT;
             ,this.cTIPCOS;
             ,iif(empty(this.cCODICE),this.cCODART,this.cCODICE);
             ,this.w_CODCON;
             ,this.w_OLTCOMAG;
             ,this.cCODCOM;
             ,this.cDatIniPRO;
             ,this.w_DATLAN;
             ,this.cDatIniMPS;
             ,this.cDatFinMPS;
             ,this.cLEAPRO;
             ," ";
             ,this.w_FLCON;
             ,this.w_FLDAP;
             ,"N";
             ,this.w_OLTFLIMC;
             ," ";
             ," ";
             ,this.w_FLORD;
             ,this.w_FLPIA;
             ,this.w_FLSUG;
             ," ";
             ,this.w_IMPCOM;
             ,this.cKEYSAL;
             ,this.cLEAMPS;
             ,this.cPERASS;
             ,this.w_OLTPROVE;
             ,this.cMPSSUG;
             ,this.cMPSSUG;
             ,0;
             ,0;
             ,this.cMPSSUG;
             ,this.w_STATO;
             ,"F";
             ,"A";
             ,this.cUNIMIS;
             ,i_CODUTE;
             ,0;
             ,SetInfoDate( g_CALUTD );
             ,cp_CharToDate("  -  -    ");
             ,this.cCODDIS;
             ,this.cCICLAV;
             ,this.w_OLTSECIC;
             ,this.cDatFinMPS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if !EMPTY(this.w_FLORD)
        * --- Aggiorna Saldi
        * --- Try
        local bErr_04CAEF98
        bErr_04CAEF98=bTrsErr
        this.Try_04CAEF98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04CAEF98
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLORD,'SLQTOPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.cKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
                 )
        else
          update (i_cTable) set;
              SLQTOPER = &i_cOp1.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.cKEYSAL;
              and SLCODMAG = this.w_OLTCOMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Saldi commessa
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.cCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.cCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_SALCOM="S"
          if empty(nvl(this.cCODCOM,""))
            this.w_COMMAPPO = this.w_COMMDEFA
          else
            this.w_COMMAPPO = this.cCODCOM
          endif
          * --- Try
          local bErr_04CB1698
          bErr_04CB1698=bTrsErr
          this.Try_04CB1698()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04CB1698
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLORD,'SCQTOPER','this.cMPSSUG',this.cMPSSUG,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.cKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLTCOMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SCQTOPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.cKEYSAL;
                and SCCODMAG = this.w_OLTCOMAG;
                and SCCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
    enddo
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04CAEF98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+",SLCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDIART','SLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'SALDIART','SLCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.cKEYSAL,'SLCODMAG',this.w_OLTCOMAG,'SLCODART',this.cCODART)
      insert into (i_cTable) (SLCODICE,SLCODMAG,SLCODART &i_ccchkf. );
         values (;
           this.cKEYSAL;
           ,this.w_OLTCOMAG;
           ,this.cCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04CB1698()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.cKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMAPPO),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.cCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.cKEYSAL,'SCCODMAG',this.w_OLTCOMAG,'SCCODCAN',this.w_COMMAPPO,'SCCODART',this.cCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.cKEYSAL;
           ,this.w_OLTCOMAG;
           ,this.w_COMMAPPO;
           ,this.cCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola cMPSSUG e cLEAPRO
    if g_COLA = "S" and this.w_OLTPROVE="L"
      this.w_GIOAPP = 0
      this.w_QTAMIN = 0
      this.w_QTAMAX = 0
      this.w_QTALOT = 0
      this.w_OLTCONTR = space(5)
      if this.cFLCOMM<>"S"
        if empty(this.w_OLTCOFOR)
          this.w_GIOAPP = this.w_PRGIOAPP
          this.w_QTAMIN = this.w_PRQTAMIN
          this.w_QTAMAX = this.w_PRQTAMAX
          this.w_QTALOT = this.w_PRLOTRIO
        else
          * --- La quantit� massima non � presente sul contratto
          this.w_QTAMAX = this.w_PRQTAMAX
          * --- Se non c'�, crea il cursore dei contratti
          if not used("_Contratti_")
            this.w_DATELA = i_DATSYS
            vq_exec("..\COLA\exe\query\GSMREBGP", this, "_Contratti_")
          endif
          * --- Imposta le politiche di lottizzazione e il LT dal contratto
          Select _Contratti_
          if nvl(this.cPREZUM,"N")<>"S"
            * --- Ogni contratto valido va bene
            if not empty(this.cCODVAR)
              * --- Cerca il contratto per articolo-variante
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and cocodvar=this.cCODVAR
            endif
            if not Found() or empty(this.cCODVAR)
              * --- Cerca il contratto per articolo
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART
            endif
          else
            * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. in uso, perch�
            *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
            if not empty(this.cCODVAR)
              * --- Cerca il contratto per articolo-variante
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and cocodvar=this.cCODVAR and counimis=this.cUNIMIS
            endif
            if not Found() or empty(this.cCODVAR)
              * --- Cerca il contratto per articolo
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and counimis=this.cUNIMIS
            endif
          endif
          if Found()
            if counimis=this.cUNIMIS
              this.w_QTAMIN = nvl( _Contratti_.COQTAMIN , 0)
              this.w_QTALOT = nvl( _Contratti_.COLOTMUL , 0)
            else
              * --- Effettuo le conversioni (ho usato la Seconda Unit� di misura)
              this.w_QTAMIN = CALQTA(nvl( _Contratti_.COQTAMIN , 0), counimis, this.cUNMIS2, this.cOPERAT, this.cMOLTIP, "", "N", "",, "", "", 0)
              this.w_QTALOT = CALQTA(nvl( _Contratti_.COLOTMUL , 0), counimis, this.cUNMIS2, this.cOPERAT, this.cMOLTIP, "", "N", "",, "", "", 0)
            endif
            this.w_GIOAPP = nvl( _Contratti_.COGIOAPP , 0)
            this.w_OLTCONTR = _Contratti_.CONUMERO
          else
            this.w_GIOAPP = this.w_PRGIOAPP
            this.w_QTAMIN = this.w_PRQTAMIN
            this.w_QTALOT = this.w_PRLOTRIO
            this.w_OLTCONTR = space(5)
          endif
        endif
        * --- Adatta cMPSSUG
        if this.w_QTAMIN+this.w_QTALOT<>0 and this.cMPSSUG>0
          if this.cMPSSUG < this.w_QTAMIN and this.w_QTAMIN>0
            this.cMPSSUG = this.w_QTAMIN
          endif
          if this.w_QTALOT>0
            this.cMPSSUG = iif( Mod(this.cMPSSUG,this.w_QTALOT)=0, this.cMPSSUG, (Int(this.cMPSSUG/this.w_QTALOT)+1)*this.w_QTALOT)
          endif
        endif
      else
        if empty(this.w_OLTCOFOR)
          this.w_GIOAPP = this.w_PRGIOAPP
        else
          * --- Se non c'�, crea il cursore dei contratti
          if not used("_Contratti_")
            this.w_DATELA = i_DATSYS
            vq_exec("..\COLA\exe\query\GSMREBGP", this, "_Contratti_")
          endif
          * --- Imposta le politiche di lottizzazione e il LT dal contratto
          Select _Contratti_
          if nvl(this.cPREZUM,"N")<>"S"
            * --- Ogni contratto valido va bene
            if not empty(this.cCODVAR)
              * --- Cerca il contratto per articolo-variante
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and cocodvar=this.cCODVAR
            endif
            if not Found() or empty(this.cCODVAR)
              * --- Cerca il contratto per articolo
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART
            endif
          else
            * --- Considero solo i contratti con Unit� di Misura Contratto=U.M. in uso articolo, perch�
            *     in anagrafica articoli ho scelto di NON effettuare conversioni sull'U.M..
            if not empty(this.cCODVAR)
              * --- Cerca il contratto per articolo-variante
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and cocodvar=this.cCODVAR and counimis=this.cUNIMIS
            endif
            if not Found() or empty(this.cCODVAR)
              * --- Cerca il contratto per articolo
              Locate for cocodclf=this.w_OLTCOFOR and cocodart=this.cCODART and counimis=this.cUNIMIS
            endif
          endif
          if Found()
            this.w_GIOAPP = nvl( _Contratti_.COGIOAPP , 0)
            this.w_OLTCONTR = _Contratti_.CONUMERO
          else
            this.w_GIOAPP = this.w_PRGIOAPP
            this.w_OLTCONTR = space(5)
          endif
        endif
      endif
      * --- Calcola LT
      this.cLEAPRO = this.w_GIOAPP
    else
      * --- Adatta cMPSSUG
      if this.cLOTECO+this.cLOTPRO<>0 and this.cMPSSUG>0
        if this.cMPSSUG < this.cLOTECO and this.cLOTECO>0
          this.cMPSSUG = this.cLOTECO
        endif
        if this.cLOTPRO>0
          this.cMPSSUG = iif( Mod(this.cMPSSUG,this.cLOTPRO)=0, this.cMPSSUG, (Int(this.cMPSSUG/this.cLOTPRO)+1)*this.cLOTPRO)
        endif
      endif
      * --- Calcola LT
      this.cLEAPRO = this.cLEAFIS + iif(this.cLOTMED=0, 0, this.cLEAFIS*this.cCOEFLT*(this.cMPSSUG/this.cLOTMED-1))
      this.w_OLTCONTR = space(5)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili locali
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='MPS_TPER'
    this.cWorkTables[4]='ODL_MAST'
    this.cWorkTables[5]='PAR_PROD'
    this.cWorkTables[6]='PAR_RIOR'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='SALDIART'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='SALDICOM'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    if used('_Curs_MPS_TPER')
      use in _Curs_MPS_TPER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
