* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_bmg                                                        *
*              Gestione ODL/OCL                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1032]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-27                                                      *
* Last revis.: 2016-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Operazione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsco_bmg",oParentObject,m.Operazione)
return(i_retval)

define class tgsco_bmg as StdBatch
  * --- Local variables
  Operazione = space(15)
  PUNPAD = .NULL.
  w_PROG = .NULL.
  w_TIPGES = space(1)
  NC = space(10)
  w_OK = .f.
  w_APPO = space(10)
  w_MESS = space(10)
  w_PPCAUORD = space(5)
  w_PPCAUIMP = space(5)
  w_SERODL = space(15)
  w_SERDOC = space(10)
  w_DOCFIL = space(10)
  w_CODODL = space(15)
  w_ROWODL = 0
  w_QTAPRE = 0
  w_QTAPR1 = 0
  w_QTAUM1 = 0
  w_QTAEVA = 0
  w_QTAEV1 = 0
  w_DASTOR = 0
  w_QTASAL = 0
  w_DISMAG = space(1)
  w_FLPREV = space(1)
  w_ROWDOC = 0
  w_CODMAG = space(5)
  w_CODMAT = space(5)
  w_KEYSAL = space(20)
  w_MVSERIAL = space(10)
  w_MVTCAMAG = space(5)
  w_MVCAUCOL = space(5)
  w_MFLCASC = space(1)
  w_MFLORDI = space(1)
  w_MFLIMPE = space(1)
  w_MFLRISE = space(1)
  w_MF2CASC = space(1)
  w_MF2ORDI = space(1)
  w_MF2IMPE = space(1)
  w_MF2RISE = space(1)
  w_SERFIL = space(10)
  w_TIPGEN = space(2)
  w_ODLFIL = space(15)
  w_TOTALE = .f.
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_OREC = 0
  w_nRecSel = 0
  w_nRecEla = 0
  w_MVCODLOT = space(20)
  w_CPROWNUM = 0
  w_STORNAMAT = .f.
  w_MTCODMAT = space(40)
  w_MSGMATRI = space(10)
  w_MT__FLAG = space(1)
  w_AM_PRENO = 0
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_CODCOM = space(15)
  w_CODART = space(20)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_SALCOM = space(1)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_TIPGEN = space(2)
  w_PROVE = space(1)
  w_TIPOIN = space(5)
  w_FLEVAS = space(1)
  w_SPUNTA = space(1)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  w_CODESE = space(4)
  w_CLIFOR = space(15)
  w_DATAIN = ctod("  /  /  ")
  w_DATA1 = ctod("  /  /  ")
  w_DATAFI = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_NUMINI = 0
  w_NUMFIN = 0
  w_serie1 = space(2)
  w_serie2 = space(2)
  w_STAMPA = 0
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_TIPCON = space(1)
  w_REPSEC = space(1)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CATEGO = space(2)
  w_CODVET = space(5)
  w_CODZON = space(3)
  w_CODDES = space(3)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_CATCOM = space(3)
  w_DELRIFDT = space(1)
  w_FLVEAC = space(1)
  w_ANNDOC = space(4)
  w_PRD = space(2)
  w_NUMDOC = 0
  w_ALFDOC = space(2)
  w_PRP = space(2)
  w_NUMEST = 0
  w_ALFEST = space(2)
  w_ANNPRO = space(4)
  w_CLADOC = space(2)
  w_NUMRIF = 0
  w_NUMRIF = 0
  w_OLTSTATO = space(1)
  w_OCLDAAGG = .f.
  w_nReco = 0
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_TMPQTAOR1 = 0
  w_TMPQTAEV1 = 0
  w_TMPQTASAL = 0
  w_TMPFLEVAS = space(1)
  w_TMPQSALOR = 0
  w_TMPQPREV = 0
  w_OLCODMAG = space(5)
  w_OLMAGORI = space(5)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_NETMAG = space(1)
  w_MAGA1 = space(5)
  w_MAGA2 = space(5)
  w_QTAGSAL = 0
  w_KEYAGSA = space(40)
  w_OLCODODL = space(15)
  w_OLCODART = space(20)
  w_OLKEYSAL = space(40)
  w_OLMAGPRE = space(5)
  w_OLMAGWIP = space(5)
  w_NETMGORI = space(1)
  w_NETMGWIP = space(1)
  w_FLIMPE = space(1)
  w_FLIMP2 = space(1)
  w_FLORD = space(1)
  w_FLORD2 = space(1)
  w_FLRISE = space(1)
  w_FLRISE2 = space(1)
  w_OLTIPSCL = space(1)
  w_OLTCOMME = space(15)
  w_FLCOM = space(1)
  w_OLQTASAL = 0
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ODL_DETT_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  RIFMGODL_idx=0
  RIF_GODL_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  MAGAZZIN_idx=0
  TMPMODL_idx=0
  MOVIMATR_idx=0
  MAT_PROD_idx=0
  PEG_SELI_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione Piani di Generazione (Ordini/DDT Trasf./Buoni di Prelievo) (da GSCO_AMG)
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.Operazione = alltrim(this.Operazione)
    * --- Puntatore a padre
    this.PUNPAD = this.oParentObject
    this.w_TIPGES = this.oParentObject.pTIPGES
    this.w_TIPGEN = IIF(this.oParentObject.pTIPGEN="OR", "O", IIF(this.oParentObject.pTIPGEN="DT", "D", "B"))
    * --- Nome cursore collegato allo zoom
    this.NC = this.PUNPAD.w_ZoomDett.cCursor
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    do case
      case this.Operazione="TITOLO"
        * --- Assegna titolo a gestione
        do case
          case this.w_TIPGEN="O"
            do case
              case this.w_TIPGES="E"
                this.PunPAD.cComment = ah_Msgformat("Manutenzione ordini a fornitore da ODA")
              case this.w_TIPGES="L"
                this.PunPAD.cComment = ah_Msgformat("Manutenzione ordini a fornitore da OCL")
              otherwise
                this.PunPAD.cComment = ah_Msgformat("Manutenzione ordini a fornitore")
            endcase
          case this.w_TIPGEN="D"
            this.PunPAD.cComment = ah_Msgformat("Manutenzione DDT di trasferimento")
          case this.w_TIPGEN = "B"
            this.PunPAD.cComment = ah_Msgformat("Manutenzioni buoni di prelievo")
        endcase
        this.PunPAD.cAutozoom = "GSCO"+this.w_TIPGEN+this.w_TIPGES+ "AMG"
        this.PunPAD.SetCaption()     
      case this.Operazione="SELEZI"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET XCHK=0
          endif
        endif
      case this.Operazione="CARICA"
        * --- Visualizza Zoom Elenco Dettagli
        this.PUNPAD.NotifyEvent("Interroga")     
        this.oParentObject.w_SELEZI = "D"
        this.oParentObject.cFunction="Edit"
      case this.Operazione="CONFERMA"
        * --- Conferma Documento Provvisori
        * --- Write into RIFMGODL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RIFMGODL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIFMGODL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIFMGODL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'RIFMGODL','PDFLPROV');
              +i_ccchkf ;
          +" where ";
              +"PDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PDSERIAL);
                 )
        else
          update (i_cTable) set;
              PDFLPROV = "N";
              &i_ccchkf. ;
           where;
              PDSERIAL = this.oParentObject.w_PDSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_nRecSel = 0
        this.w_nRecEla = 0
        if used(this.NC)
          SELECT (this.NC)
          this.w_OREC = RECNO()
          GO TOP
          SCAN FOR NOT EMPTY(NVL(PDSERDOC,"")) 
          this.w_MVSERIAL = PDSERDOC
          this.w_MVTCAMAG = SPACE(5)
          this.w_MVCAUCOL = SPACE(5)
          this.w_APPO = " "
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLPROV,MVTCAMAG"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLPROV,MVTCAMAG;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.MVFLPROV),cp_NullValue(_read_.MVFLPROV))
            this.w_MVTCAMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_APPO="S" AND NOT EMPTY(this.w_MVTCAMAG)
            this.w_nRecSel = this.w_nRecSel + 1
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          SELECT (this.NC)
          ENDSCAN
          SELECT (this.NC)
          if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
            GOTO this.w_OREC
          else
            GO TOP
          endif
        endif
        ah_ErrorMsg("Elaborazione terminata%0N.%1 documenti confermati%0Su %2 da confermare",,"",alltrim(str(this.w_nRecEla,5,0)), alltrim(str(this.w_nRecSel,5,0)) )
      case this.Operazione="STAMPA"
        * --- Stampa Documenti Associati al Piano
        * --- Variabili da passare alle Stampe
        this.w_REPSEC = This.oParentObject.w_REPSEC
        this.w_FLEVAS = " "
        this.w_SPUNTA = "S"
        this.w_TIPOIN = this.oParentObject.w_PDTIPDOC
        this.w_CODESE = CALCESER(this.oParentObject.w_PDDATGEN, g_CODESE)
        this.w_CLIFOR = SPACE(15)
        this.w_DATAIN = i_INIDAT
        this.w_DATA1 = i_INIDAT
        this.w_DATAFI = i_FINDAT
        this.w_DATA2 = i_FINDAT
        this.w_NUMINI = 1
        this.w_NUMFIN = 999999
        this.w_serie1 = ""
        this.w_serie2 = ""
        this.w_STAMPA = 0
        this.w_MVRIFFAD = SPACE(10)
        this.w_MVRIFODL = this.oParentObject.w_PDSERIAL
        if EMPTY(this.w_TIPOIN)
          ah_ErrorMsg("Causale documento inesistente",,"")
        else
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLVEAC,TDCATDOC,TDPRGSTA"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPOIN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLVEAC,TDCATDOC,TDPRGSTA;
              from (i_cTable) where;
                  TDTIPDOC = this.w_TIPOIN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            this.w_STAMPA = NVL(cp_ToDate(_read_.TDPRGSTA),cp_NullValue(_read_.TDPRGSTA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if USED(this.NC)
            SELECT PDSERDOC FROM (this.NC) WHERE NOT EMPTY(NVL(PDSERDOC,"")) AND XCHK=1 INTO CURSOR SPUNTA
          endif
          SELECT SPUNTA
          if RECCOUNT()>0
            GSVE_BRD(this,"F")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            ah_ErrorMsg("Selezionare almeno un documento",,"")
          endif
          if USED("SPUNTA")
            * --- Elimina Cursore di Appoggio se esiste ancora
            use in SPUNTA
          endif
          if USED("__TMP__")
            * --- Elimina Cursore di Appoggio se esiste ancora
            use in __TMP__
          endif
        endif
      case this.Operazione="DETTAGLI" AND NOT EMPTY(this.oParentObject.w_SERIALE)
        * --- Visualizza Documento Collegato
        gsar_bzm(this,this.oParentObject.w_SERIALE,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Operazione $ "ELIMINATUTTO-ELIMINARIGHE"
        if NOT EMPTY(this.oParentObject.w_PDSERIAL)
          this.w_OK = .T.
          ah_Msg("Elimina riferiferimenti documenti e dettagli...",.T.)
          * --- Seleziona tutto il Dettaglio
          this.w_SERFIL = this.oParentObject.w_PDSERIAL
          this.w_TIPGEN = this.oParentObject.w_PDTIPGEN
          vq_exec("..\COLA\EXE\QUERY\GSCODBMG.VQR",this,"LISTAODL")
          * --- Metto seriale della generazione confiferimento all'ocl nella tabella temporanea
          *     in modo da poter riaprire le righe degli OCL evasi senza movimenti a causa dei controlli
          *     sulla disponibilit�
          * --- Create temporary table TMPMODL
          i_nIdx=cp_AddTableDef('TMPMODL') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\COLA\EXE\QUERY\GSCO1BMG',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPMODL_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          if USED("LISTAODL")
            if this.Operazione = "ELIMINARIGHE"
              * --- Seleziona le sole righe associate ai documenti da eliminare
              UPDATE LISTAODL SET SELEZI=" " WHERE 1=1
              SELECT LISTAODL
              GO TOP
              this.w_OK = .F.
              this.w_MESS = ah_Msgformat("Selezionare almeno un documento")
              if used(this.NC)
                SELECT (this.NC)
                GO TOP
                SCAN FOR NOT EMPTY(NVL(PDSERDOC,"")) AND XCHK=1
                this.w_APPO = PDSERDOC
                this.w_OK = .T.
                this.w_MESS = " "
                UPDATE LISTAODL SET SELEZI="S" WHERE NVL(PDSERDOC, SPACE(10))=this.w_APPO AND NOT EMPTY(NVL(PDSERDOC,""))
                SELECT LISTAODL
                GO TOP
                SELECT (this.NC)
                ENDSCAN
              endif
            endif
            * --- Elenco Documenti contenuti
            if this.w_OK=.T.
              SELECT PDSERDOC FROM LISTAODL WHERE SELEZI="S" GROUP BY PDSERDOC order by PDSERDOC DESCENDING INTO CURSOR LISTADOC 
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if USED("LISTAODL")
              * --- Elimina Cursore di Appoggio se esiste ancora
              use in LISTAODL
            endif
            if USED("LISTADOC")
              * --- Elimina Cursore di Appoggio se esiste ancora
              use in LISTADOC
            endif
          endif
          WAIT CLEAR
        else
          this.w_OK = .F.
          this.w_MESS = ah_Msgformat("Nessun piano selezionato")
        endif
        if this.w_OK=.F.
          if this.Operazione="ELIMINATUTTO"
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          else
            ah_ErrorMsg("%1",,"", this.w_MESS)
          endif
        endif
        * --- Visualizza Zoom Elenco Dettagli
        this.PUNPAD.NotifyEvent("Interroga")     
        this.oParentObject.w_SELEZI = "D"
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina il Piano (sotto transazione)
    this.w_PPCAUORD = " "
    this.w_PPCAUIMP = " "
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCAUORD,PPCAUIMP"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCAUORD,PPCAUIMP;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCAUORD = NVL(cp_ToDate(_read_.PPCAUORD),cp_NullValue(_read_.PPCAUORD))
      this.w_PPCAUIMP = NVL(cp_ToDate(_read_.PPCAUIMP),cp_NullValue(_read_.PPCAUIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_OK = .T.
    this.w_MESS = " "
    if this.oParentObject.w_PDTIPGEN="OR"
      this.w_oMess.AddMsgPartNL("ATTENZIONE:%0La cancellazione degli ordini a terzista comporter� il ritorno dell'OCL allo stato pianificato")     
      this.w_oMess.AddMsgPartNL("I piani relativi agli eventuali DDT di trasferimento verranno eliminati%0tuttavia gli stessi DDT rimarranno memorizzati%0Confermi eliminazione?")     
      this.w_OK = this.w_oMess.ah_YesNo()
      this.w_MESS = ah_Msgformat("Eliminazione abbandonata")
      if this.w_OK=.T.
        SELECT LISTAODL
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0 AND SELEZI="S"
        this.w_ODLFIL = PDCODODL
        this.w_DELRIFDT = "N"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_DELRIFDT<>"N"
          SELECT LISTAODL
          REPLACE DELRIFDT With this.w_DELRIFDT
        endif
        SELECT LISTAODL
        ENDSCAN 
      endif
    else
      if this.Operazione="ELIMINATUTTO"
        if this.oParentObject.w_PDTIPGEN="BP"
          this.w_oMess.AddMsgPartNL("ATTENZIONE:%0La cancellazione del piano buoni di prelievo comporter� l'eliminazione di tutti i documenti da esso generati")     
          this.w_oMess.AddMsgPartNL("I materiali ODL prelevati verranno riportati allo stato precedente la generazione del piano")     
        else
          this.w_oMess.AddMsgPartNL("ATTENZIONE:%0La cancellazione del piano DDT trasferimento comporter� l'eliminazione di tutti i documenti da esso generati")     
          this.w_oMess.AddMsgPartNL("I materiali OCL prelevati verranno riportati allo stato precedente la generazione del piano")     
        endif
        this.w_oMess.AddMsgPartNL("Confermi l'eliminazione?")     
      else
        this.w_oMess.AddMsgPartNL("Confermi l'eliminazione dei documenti selezionati?")     
        if this.oParentObject.w_PDTIPGEN="BP"
          this.w_oMess.AddMsgPartNL("(I materiali ODL associati verranno riportati allo stato precedente la loro generazione)")     
        else
          this.w_oMess.AddMsgPartNL("(I materiali OCL associati verranno riportati allo stato precedente la loro generazione)")     
        endif
      endif
      this.w_OK = this.w_oMess.ah_YesNo()
      this.w_MESS = ah_Msgformat("Eliminazione abbandonata")
    endif
    if this.w_OK=.T.
      * --- Legge i Documenti associati
      if USED("LISTADOC")
        SELECT LISTADOC
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDSERDOC,""))
        if this.w_OK=.T.
          this.w_SERDOC = PDSERDOC
          * --- Storna Saldi e verifica se non Evaso
          * --- Select from DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
                +" where MVSERIAL="+cp_ToStrODBC(this.w_SERDOC)+"";
                 ,"_Curs_DOC_DETT")
          else
            select * from (i_cTable);
             where MVSERIAL=this.w_SERDOC;
              into cursor _Curs_DOC_DETT
          endif
          if used('_Curs_DOC_DETT')
            select _Curs_DOC_DETT
            locate for 1=1
            do while not(eof())
            * --- Aggiorna Saldi
            if this.w_OK=.T.
              if NVL(_Curs_DOC_DETT.MVQTAEVA,0)<>0 OR NOT EMPTY(NVL(_Curs_DOC_DETT.MVFLEVAS," ")) OR NOT EMPTY(CP_TODATE(_Curs_DOC_DETT.MVDATGEN))
                this.w_OK = .F.
                this.w_NUMDOC = 0
                this.w_ALFDOC = Space(10)
                this.w_DATDOC = cp_CharToDate("  -  -  ")
                * --- Read from DOC_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVNUMDOC,MVALFDOC,MVDATDOC"+;
                    " from "+i_cTable+" DOC_MAST where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVNUMDOC,MVALFDOC,MVDATDOC;
                    from (i_cTable) where;
                        MVSERIAL = this.w_SERDOC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
                  this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
                  this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_MESS = ah_Msgformat("Documento: %1 - del %2%0Gi� evaso da altri documenti; impossibile eliminare", ALLTR(STR(this.w_NUMDOC,15))+ IIF(Not Empty(this.w_ALFDOC)," "+Alltrim(this.w_ALFDOC),"") , DTOC(this.w_DATDOC) )
              else
                * --- Storna Matricole: CONTROLLA CHE SIA L'ULTIMO MOVIMENTO
                this.w_STORNAMAT = False
                this.w_MTCODMAT = ""
                this.w_MSGMATRI = ""
                this.w_MVSERIAL = this.w_SERDOC
                this.w_CPROWNUM = _Curs_DOC_DETT.CPROWNUM
                this.w_NUMRIF = -20
                * --- Select from MOVIMATR
                i_nConn=i_TableProp[this.MOVIMATR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MTKEYSAL,MTCODMAT,MT_SALDO  from "+i_cTable+" MOVIMATR ";
                      +" where MTSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+" AND MTROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+" AND MTNUMRIF="+cp_ToStrODBC(this.w_NUMRIF)+"";
                       ,"_Curs_MOVIMATR")
                else
                  select MTKEYSAL,MTCODMAT,MT_SALDO from (i_cTable);
                   where MTSERIAL=this.w_MVSERIAL AND MTROWNUM=this.w_CPROWNUM AND MTNUMRIF=this.w_NUMRIF;
                    into cursor _Curs_MOVIMATR
                endif
                if used('_Curs_MOVIMATR')
                  select _Curs_MOVIMATR
                  locate for 1=1
                  do while not(eof())
                  this.w_STORNAMAT = True
                  this.w_MTCODMAT = iif(_Curs_MOVIMATR.MT_SALDO>0, _Curs_MOVIMATR.MTKEYSAL+_Curs_MOVIMATR.MTCODMAT, this.w_MTCODMAT)
                    select _Curs_MOVIMATR
                    continue
                  enddo
                  use
                endif
                if not empty(this.w_MTCODMAT)
                  * --- Verifica se qualche matricola � stata usata
                  this.w_MSGMATRI = ah_Msgformat("Impossibile stornare documento di scarico collegato,%0Movimentato articolo: %1%0Matricola: %2",left(this.w_MTCODMAT,40), alltrim(right(this.w_MTCODMAT,40)) )
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=this.w_MSGMATRI
                  i_retcode = 'stop'
                  return
                endif
                if this.w_STORNAMAT
                  * --- Storna i saldi su MOVIMATR
                  * --- Verifica se la matricola � stata nuovamente dichiarata in produzione
                  this.w_MTCODMAT = ""
                  this.w_MT__FLAG = " "
                  this.w_AM_PRENO = 1
                  * --- Select from GSCO_BAD
                  do vq_exec with 'GSCO_BAD',this,'_Curs_GSCO_BAD','',.f.,.t.
                  if used('_Curs_GSCO_BAD')
                    select _Curs_GSCO_BAD
                    locate for 1=1
                    do while not(eof())
                    this.w_MTCODMAT = _Curs_GSCO_BAD.AMKEYSAL+_Curs_GSCO_BAD.AMCODICE
                      select _Curs_GSCO_BAD
                      continue
                    enddo
                    use
                  endif
                  if not empty(this.w_MTCODMAT)
                    * --- Impossibile cancellare Doc. di Scarico
                    this.w_MSGMATRI = ah_Msgformat("Impossibile stornare documento collegato,%0Dichiarato in produzione articolo: %1%0Matricola: %2", left(this.w_MTCODMAT,40), alltrim(right(this.w_MTCODMAT,40)) )
                    * --- transaction error
                    bTrsErr=.t.
                    i_TrsMsg=this.w_MSGMATRI
                    i_retcode = 'stop'
                    return
                  endif
                  * --- Storna i saldi su MOVIMATR del movimento collegato
                  this.w_AM_PRENO = 0
                  this.w_MT__FLAG = "+"
                  * --- Write into MOVIMATR
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MOVIMATR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
                  if i_nConn<>0
                    local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                    declare i_aIndex[1]
                    i_cQueryTable=cp_getTempTableName(i_nConn)
                    i_aIndex(1)="MTSERIAL,MTROWNUM,MTNUMRIF,MTKEYSAL,MTCODMAT"
                    do vq_exec with 'GSCO_BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
                    i_cDB=cp_GetDatabaseType(i_nConn)
                    do case
                    case i_cDB="SQLServer"
                      i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                            +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                            +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                            +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                            +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                        +i_ccchkf;
                        +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
                    case i_cDB="MySQL"
                      i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                            +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                            +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                            +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                            +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
                    +"MOVIMATR.MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                        +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                        +" where "+i_cWhere)
                    case i_cDB="Oracle"
                      i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                            +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                            +" and "+"MOVIMATR.MTNUMRIF = t2.MTNUMRIF";
                            +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                            +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
                      
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
                        +"MT_SALDO";
                        +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                        +"MT_SALDO-"+cp_ToStrODBC(1)+"";
                        +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                        +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                    case i_cDB="PostgreSQL"
                      i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                            +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                            +" and "+"MOVIMATR.MTNUMRIF = _t2.MTNUMRIF";
                            +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                            +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
                    +"MT_SALDO =MOVIMATR.MT_SALDO- "+cp_ToStrODBC(1);
                        +i_ccchkf;
                        +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                    otherwise
                      i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                            +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                            +" and "+i_cTable+".MTNUMRIF = "+i_cQueryTable+".MTNUMRIF";
                            +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                            +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MT_SALDO =MT_SALDO- "+cp_ToStrODBC(1);
                        +i_ccchkf;
                        +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                    endcase
                    cp_DropTempTable(i_nConn,i_cQueryTable)
                  else
                    error "not yet implemented!"
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Impossibile aggiornare dettaglio matricole'
                    return
                  endif
                endif
                this.w_QTAPR1 = NVL(_Curs_DOC_DETT.MVQTASAL, 0)
                this.w_KEYSAL = NVL(_Curs_DOC_DETT.MVKEYSAL, " ")
                this.w_CODMAG = NVL(_Curs_DOC_DETT.MVCODMAG, " ")
                this.w_CODMAT = NVL(_Curs_DOC_DETT.MVCODMAT, " ")
                this.w_CODCOM = NVL(_Curs_DOC_DETT.MVCODCOM, " ")
                this.w_CODART = NVL(_Curs_DOC_DETT.MVCODART, " ")
                this.w_MFLCASC = NVL(_Curs_DOC_DETT.MVFLCASC, " ")
                this.w_MFLORDI = NVL(_Curs_DOC_DETT.MVFLORDI, " ")
                this.w_MFLIMPE = NVL(_Curs_DOC_DETT.MVFLIMPE, " ")
                this.w_MFLRISE = NVL(_Curs_DOC_DETT.MVFLRISE, " ")
                this.w_MF2CASC = NVL(_Curs_DOC_DETT.MVF2CASC, " ")
                this.w_MF2ORDI = NVL(_Curs_DOC_DETT.MVF2ORDI, " ")
                this.w_MF2IMPE = NVL(_Curs_DOC_DETT.MVF2IMPE, " ")
                this.w_MF2RISE = NVL(_Curs_DOC_DETT.MVF2RISE, " ")
                this.w_MVCODLOT = NVL(_Curs_DOC_DETT.MVCODLOT,"")
                this.w_MVCODUBI = NVL(_Curs_DOC_DETT.MVCODUBI,"")
                this.w_MVCODUB2 = NVL(_Curs_DOC_DETT.MVCODUB2,"")
                if this.w_QTAPR1<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
                  this.w_MFLCASC = IIF(this.w_MFLCASC="+", "-", IIF(this.w_MFLCASC="-", "+", " "))
                  this.w_MFLORDI = IIF(this.w_MFLORDI="+", "-", IIF(this.w_MFLORDI="-", "+", " "))
                  this.w_MFLIMPE = IIF(this.w_MFLIMPE="+", "-", IIF(this.w_MFLIMPE="-", "+", " "))
                  this.w_MFLRISE = IIF(this.w_MFLRISE="+", "-", IIF(this.w_MFLRISE="-", "+", " "))
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_CODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
                    * --- Write into SALDIART
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                          +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                             )
                    else
                      update (i_cTable) set;
                          SLQTAPER = &i_cOp1.;
                          ,SLQTRPER = &i_cOp2.;
                          ,SLQTOPER = &i_cOp3.;
                          ,SLQTIPER = &i_cOp4.;
                          &i_ccchkf. ;
                       where;
                          SLCODICE = this.w_KEYSAL;
                          and SLCODMAG = this.w_CODMAG;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura SALDIART (1)'
                      return
                    endif
                    if this.w_SALCOM="S"
                      if empty(nvl(this.w_CODCOM,""))
                        this.w_COMMAPPO = this.w_COMMDEFA
                      else
                        this.w_COMMAPPO = this.w_CODCOM
                      endif
                      * --- Write into SALDICOM
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDICOM_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                        +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                        +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                        +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                            +i_ccchkf ;
                        +" where ";
                            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                               )
                      else
                        update (i_cTable) set;
                            SCQTAPER = &i_cOp1.;
                            ,SCQTRPER = &i_cOp2.;
                            ,SCQTOPER = &i_cOp3.;
                            ,SCQTIPER = &i_cOp4.;
                            &i_ccchkf. ;
                         where;
                            SCCODICE = this.w_KEYSAL;
                            and SCCODMAG = this.w_CODMAG;
                            and SCCODCAN = this.w_COMMAPPO;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore Aggiornamento Saldi Commessa (1)'
                        return
                      endif
                    endif
                  endif
                  * --- Causale Collegata (DDT di Trasferimento)
                  if NOT EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) AND NOT EMPTY(this.w_CODMAT)
                    this.w_MF2CASC = IIF(this.w_MF2CASC="+", "-", IIF(this.w_MF2CASC="-", "+", " "))
                    this.w_MF2ORDI = IIF(this.w_MF2ORDI="+", "-", IIF(this.w_MF2ORDI="-", "+", " "))
                    this.w_MF2IMPE = IIF(this.w_MF2IMPE="+", "-", IIF(this.w_MF2IMPE="-", "+", " "))
                    this.w_MF2RISE = IIF(this.w_MF2RISE="+", "-", IIF(this.w_MF2RISE="-", "+", " "))
                    * --- Write into SALDIART
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
                      +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
                      +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                      +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                          +i_ccchkf ;
                      +" where ";
                          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                          +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                             )
                    else
                      update (i_cTable) set;
                          SLQTAPER = &i_cOp1.;
                          ,SLQTRPER = &i_cOp2.;
                          ,SLQTOPER = &i_cOp3.;
                          ,SLQTIPER = &i_cOp4.;
                          &i_ccchkf. ;
                       where;
                          SLCODICE = this.w_KEYSAL;
                          and SLCODMAG = this.w_CODMAT;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura SALDIART (2)'
                      return
                    endif
                    if this.w_SALCOM="S"
                      if empty(nvl(this.w_CODCOM,""))
                        this.w_COMMAPPO = this.w_COMMDEFA
                      else
                        this.w_COMMAPPO = this.w_CODCOM
                      endif
                      * --- Write into SALDICOM
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDICOM_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                        i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
                        +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
                        +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                        +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                            +i_ccchkf ;
                        +" where ";
                            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                               )
                      else
                        update (i_cTable) set;
                            SCQTAPER = &i_cOp1.;
                            ,SCQTRPER = &i_cOp2.;
                            ,SCQTOPER = &i_cOp3.;
                            ,SCQTIPER = &i_cOp4.;
                            &i_ccchkf. ;
                         where;
                            SCCODICE = this.w_KEYSAL;
                            and SCCODMAG = this.w_CODMAT;
                            and SCCODCAN = this.w_COMMAPPO;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore Aggiornamento Saldi Commessa (1)'
                        return
                      endif
                    endif
                  endif
                endif
                this.w_CPROWNUM = _Curs_DOC_DETT.CPROWNUM
                if g_PERUBI="S" OR g_PERLOT="S" and !empty(this.w_MVCODLOT)
                  GSMD_BRL (this, this.w_SERDOC , "V" , "-" , this.w_CPROWNUM ,.T. )
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
              select _Curs_DOC_DETT
              continue
            enddo
            use
          endif
          if this.w_OK
            * --- Select from gsco_bmg
            do vq_exec with 'gsco_bmg',this,'_Curs_gsco_bmg','',.f.,.t.
            if used('_Curs_gsco_bmg')
              select _Curs_gsco_bmg
              locate for 1=1
              do while not(eof())
              this.w_FLVEAC = NVL(_Curs_GSCO_BMG.MVFLVEAC,"")
              this.w_ANNDOC = NVL(_Curs_GSCO_BMG.MVANNDOC,"")
              this.w_PRD = NVL(_Curs_GSCO_BMG.MVPRD,"")
              this.w_NUMDOC = NVL(_Curs_GSCO_BMG.MVNUMDOC,0)
              this.w_ALFDOC = NVL(_Curs_GSCO_BMG.MVALFDOC,"")
              this.w_PRP = NVL(_Curs_GSCO_BMG.MVPRP,"")
              this.w_NUMEST = NVL(_Curs_GSCO_BMG.MVNUMEST,0)
              this.w_ALFEST = NVL(_Curs_GSCO_BMG.MVALFEST,"")
              this.w_ANNPRO = NVL(_Curs_GSCO_BMG.MVANNPRO,"")
              this.w_CLADOC = NVL(_Curs_GSCO_BMG.MVCLADOC,"")
                select _Curs_gsco_bmg
                continue
              enddo
              use
            endif
            GSAR_BRP(this,this.w_FLVEAC, this.w_ANNDOC, this.w_PRD, this.w_NUMDOC, this.w_ALFDOC, this.w_PRP, this.w_NUMEST, this.w_ALFEST, this.w_ANNPRO, this.w_CLADOC)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_NUMRIF = -20
            * --- Elimina Documento in Cascata
            GSAR_BED(this,this.w_SERDOC, this.w_NUMRIF)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_NUMRIF = -20
            * --- Elimina Documento in Cascata
            GSAR_BED(this,this.w_SERDOC, this.w_NUMRIF)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        SELECT LISTADOC
        ENDSCAN
      endif
      if this.w_OK=.T.
        SELECT LISTAODL
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0 AND SELEZI="S"
        this.w_CODODL = PDCODODL
        this.w_ROWODL = PDROWODL
        this.w_ROWDOC = PDROWDOC
        this.w_QTAPRE = NVL(PDQTAPRE, 0)
        this.w_QTAPR1 = NVL(PDQTAPR1, 0)
        this.w_FLPREV = NVL(PDFLPREV, " ")
        do case
          case this.oParentObject.w_PDTIPGEN="OR"
            * --- Riattivo Ordine su ODL_MAST
            this.w_CODMAG = " "
            this.w_KEYSAL = " "
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTCOMAG,OLTKEYSA,OLTCOART,OLTCOMME,OLTIPSCL"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTCOMAG,OLTKEYSA,OLTCOART,OLTCOMME,OLTIPSCL;
                from (i_cTable) where;
                    OLCODODL = this.w_CODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODMAG = NVL(cp_ToDate(_read_.OLTCOMAG),cp_NullValue(_read_.OLTCOMAG))
              this.w_KEYSAL = NVL(cp_ToDate(_read_.OLTKEYSA),cp_NullValue(_read_.OLTKEYSA))
              this.w_CODART = NVL(cp_ToDate(_read_.OLTCOART),cp_NullValue(_read_.OLTCOART))
              this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
              this.w_OLTIPSCL = NVL(cp_ToDate(_read_.OLTIPSCL),cp_NullValue(_read_.OLTIPSCL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = 'Errore in Lettura ODL_MAST'
              return
            endif
            select (i_nOldArea)
            * --- Write into ODL_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"OLTSTATO ="+cp_NullLink(cp_ToStrODBC("P"),'ODL_MAST','OLTSTATO');
              +",OLTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCAUORD),'ODL_MAST','OLTCAMAG');
              +",OLTFLORD ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_MAST','OLTFLORD');
              +",OLTFLIMP ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLIMP');
              +",OLTDTLAN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ODL_MAST','OLTDTLAN');
                  +i_ccchkf ;
              +" where ";
                  +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                     )
            else
              update (i_cTable) set;
                  OLTSTATO = "P";
                  ,OLTCAMAG = this.w_PPCAUORD;
                  ,OLTFLORD = "+";
                  ,OLTFLIMP = " ";
                  ,OLTDTLAN = cp_CharToDate("  -  -  ");
                  &i_ccchkf. ;
               where;
                  OLCODODL = this.w_CODODL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura ODL_MAST'
              return
            endif
            if this.w_QTAPR1<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
              * --- Write into SALDIART
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDIART_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER =SLQTOPER+ "+cp_ToStrODBC(this.w_QTAPR1);
                    +i_ccchkf ;
                +" where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                       )
              else
                update (i_cTable) set;
                    SLQTOPER = SLQTOPER + this.w_QTAPR1;
                    &i_ccchkf. ;
                 where;
                    SLCODICE = this.w_KEYSAL;
                    and SLCODMAG = this.w_CODMAG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore in scrittura SALDIART'
                return
              endif
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARSALCOM"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARSALCOM;
                  from (i_cTable) where;
                      ARCODART = this.w_CODART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_SALCOM="S"
                if empty(nvl(this.w_CODCOM,""))
                  this.w_COMMAPPO = this.w_COMMDEFA
                else
                  this.w_COMMAPPO = this.w_CODCOM
                endif
                * --- Write into SALDICOM
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICOM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER =SCQTOPER+ "+cp_ToStrODBC(this.w_QTAPR1);
                      +i_ccchkf ;
                  +" where ";
                      +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                      +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                      +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                         )
                else
                  update (i_cTable) set;
                      SCQTOPER = SCQTOPER + this.w_QTAPR1;
                      &i_ccchkf. ;
                   where;
                      SCCODICE = this.w_KEYSAL;
                      and SCCODMAG = this.w_CODMAG;
                      and SCCODCAN = this.w_COMMAPPO;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore Aggiornamento Saldi Commessa (1)'
                  return
                endif
              endif
            endif
            if this.w_TIPGES="L" and this.w_OLTIPSCL="O" and this.w_DELRIFDT<>"S"
              this.w_OLCODODL = this.w_CODODL
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.oParentObject.w_PDTIPGEN $ "DT-BP"
            * --- Generazione DDT di Trasferimento o Buono di Prelievo
            this.w_TOTALE = .F.
            this.w_KEYSAL = SPACE(20)
            this.w_CODMAG = SPACE(5)
            this.w_CODMAT = SPACE(5)
            this.w_QTAUM1 = 0
            this.w_DISMAG = " "
            this.w_FLEVAS = Space(1)
            * --- Lettura dell' ODL_DETT con i riferimenti ai piani in cui esso e' inserito
            vq_exec("..\COLA\EXE\QUERY\GSCOIBMG.VQR",this,"APPO")
            if USED("APPO")
              SELECT APPO
              GO TOP
              LOCATE FOR NVL(PDSERIAL, SPACE(10))<>this.oParentObject.w_PDSERIAL AND NOT EMPTY(NVL(PDSERIAL,""))
              if NOT FOUND()
                * --- Non esistono altri piani associati all' ODL_DETT
                * --- Ripristina l'ODL alla situazione Originaria
                this.w_TOTALE = .T.
                SELECT APPO
                GO TOP
                LOCATE FOR NVL(PDSERIAL, SPACE(10))=this.oParentObject.w_PDSERIAL
                * --- Legge i dati per il Ripristino della situazione Originaria
                this.w_KEYSAL = NVL(OLKEYSAL, SPACE(20))
                this.w_CODMAG = NVL(OLMAGPRE, SPACE(5))
                this.w_CODMAT = NVL(OLMAGWIP, SPACE(5))
                this.w_CODCOM = NVL(OLTCOMME, SPACE(15))
                this.w_CODART = NVL(OLCODART, SPACE(20))
                this.w_QTAUM1 = NVL(OLQTAUM1, 0)
                this.w_QTASAL = NVL(OLQTASAL, 0)
                this.w_QTAEV1 = NVL(OLQTAEV1, 0)
                this.w_DISMAG = NVL(MGDISMAG, " ")
                this.w_FLEVAS = NVL(OLFLEVAS, " ")
              else
                SELECT APPO
                GO TOP
                LOCATE FOR NVL(PDSERIAL, SPACE(10))=this.oParentObject.w_PDSERIAL
                if FOUND()
                  * --- Legge i dati per il Ripristino della situazione Originaria
                  this.w_KEYSAL = NVL(OLKEYSAL, SPACE(20))
                  this.w_CODMAG = NVL(OLMAGPRE, SPACE(5))
                  this.w_CODMAT = NVL(OLMAGWIP, SPACE(5))
                  this.w_CODCOM = NVL(OLTCOMME, SPACE(15))
                  this.w_CODART = NVL(OLCODART, SPACE(20))
                  this.w_QTAUM1 = NVL(OLQTAUM1, 0)
                  this.w_QTASAL = NVL(OLQTASAL, 0)
                  this.w_QTAEV1 = NVL(OLQTAEV1, 0)
                  this.w_DISMAG = NVL(MGDISMAG, " ")
                  this.w_FLEVAS = NVL(OLFLEVAS, " ")
                endif
              endif
              USE
            endif
            * --- Leggo lo stato dell'ODL
            * --- Read from ODL_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ODL_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "OLTSTATO,OLTIPSCL"+;
                " from "+i_cTable+" ODL_MAST where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                OLTSTATO,OLTIPSCL;
                from (i_cTable) where;
                    OLCODODL = this.w_CODODL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_OLTSTATO = NVL(cp_ToDate(_read_.OLTSTATO),cp_NullValue(_read_.OLTSTATO))
              this.w_OLTIPSCL = NVL(cp_ToDate(_read_.OLTIPSCL),cp_NullValue(_read_.OLTIPSCL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Nel caso sia Evaso al primo trasferimento storno il flag
            if this.w_FLEVAS="S"
              this.w_FLEVAS = IIF(this.w_OLTSTATO = "F" , this.w_FLEVAS, Space(1))
            endif
            if this.w_OLTSTATO="F"
              this.w_DASTOR = 0
              this.w_QTAUM1 = 0
            else
              * --- Devo verificare le quantit� trasferite da stornare
              this.w_DASTOR = MAX((this.w_QTAUM1-this.w_QTAEV1) + this.w_QTAPR1, 0)
              * --- Devo prendere la min tra la qta da stornare e la qta prelevata altrimenti viene errato il saldo
              this.w_DASTOR = MIN(this.w_DASTOR, this.w_QTAPR1)
              if this.w_TOTALE=.T.
                * --- Se ho trasferito meno all'ultimo trasferimento ripristino tutto l'impegnato
                if (this.w_DASTOR+this.w_QTASAL)<this.w_QTAUM1
                  this.w_DASTOR = this.w_QTAUM1-this.w_QTASAL
                endif
              endif
            endif
            * --- Nek caso w_TOTALE � True significa che non ci sono altri piani di generazione
            *     per la nostra riga dell'ordine, quindi posso ripristinare l'impegno
            if this.w_TOTALE=.T. and this.w_OLTSTATO<>"F"
              if this.oParentObject.w_PDTIPGEN="BP"
                this.w_OCLDAAGG = .T.
              else
                this.w_OCLDAAGG = this.w_TIPGES="L" and Not Inlist(this.w_OLTIPSCL , "O" , "X") and this.w_CODMAG<>this.w_CODMAT
              endif
              if this.w_OCLDAAGG
                * --- Ripristina l'ODL alla situazione Originaria
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
                  +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
                  +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
                  +",OLQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAEVA');
                  +",OLQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAEV1');
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                  +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_QTAUM1),'ODL_DETT','OLQTASAL');
                  +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLEVAS');
                  +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC("          "),'ODL_DETT','OLDTEVCL');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLCODMAG = this.w_CODMAG;
                      ,OLQTAPRE = 0;
                      ,OLQTAPR1 = 0;
                      ,OLQTAEVA = 0;
                      ,OLQTAEV1 = 0;
                      ,OLFLPREV = " ";
                      ,OLQTASAL = this.w_QTAUM1;
                      ,OLFLEVAS = " ";
                      ,OLDTEVCL = "          ";
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (1)'
                  return
                endif
                if this.w_DISMAG="S"
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_CODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_CODMAG<>this.w_CODMAT
                    * --- Lo storno dell' Impegno avviene una volta sola
                    if this.w_QTAUM1<>0 AND NOT EMPTY(this.w_CODMAG)
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_QTAUM1);
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                               )
                      else
                        update (i_cTable) set;
                            SLQTIPER = SLQTIPER + this.w_QTAUM1;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_KEYSAL;
                            and SLCODMAG = this.w_CODMAG;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura SALDIART (4)'
                        return
                      endif
                      if this.w_SALCOM="S"
                        if empty(nvl(this.w_CODCOM,""))
                          this.w_COMMAPPO = this.w_COMMDEFA
                        else
                          this.w_COMMAPPO = this.w_CODCOM
                        endif
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_QTAUM1);
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTIPER = SCQTIPER + this.w_QTAUM1;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_KEYSAL;
                              and SCCODMAG = this.w_CODMAG;
                              and SCCODCAN = this.w_COMMAPPO;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore Aggiornamento Saldi Commessa (1)'
                          return
                        endif
                      endif
                    endif
                    if this.w_DISMAG="S" AND this.w_QTAUM1<>0 AND NOT EMPTY(this.w_CODMAT)
                      * --- Il trasferimento dell'Impegno avviene una volta sola e solo se il Magazzino e' Nettificabile
                      * --- Write into SALDIART
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDIART_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SLQTIPER =SLQTIPER- "+cp_ToStrODBC(this.w_QTAUM1);
                            +i_ccchkf ;
                        +" where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                               )
                      else
                        update (i_cTable) set;
                            SLQTIPER = SLQTIPER - this.w_QTAUM1;
                            &i_ccchkf. ;
                         where;
                            SLCODICE = this.w_KEYSAL;
                            and SLCODMAG = this.w_CODMAT;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore in scrittura SALDIART (5)'
                        return
                      endif
                      if this.w_SALCOM="S"
                        if empty(nvl(this.w_CODCOM,""))
                          this.w_COMMAPPO = this.w_COMMDEFA
                        else
                          this.w_COMMAPPO = this.w_CODCOM
                        endif
                        * --- Write into SALDICOM
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_nConn=i_TableProp[this.SALDICOM_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                        i_ccchkf=''
                        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                          +"SCQTIPER =SCQTIPER- "+cp_ToStrODBC(this.w_QTAUM1);
                              +i_ccchkf ;
                          +" where ";
                              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                              +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                                 )
                        else
                          update (i_cTable) set;
                              SCQTIPER = SCQTIPER - this.w_QTAUM1;
                              &i_ccchkf. ;
                           where;
                              SCCODICE = this.w_KEYSAL;
                              and SCCODMAG = this.w_CODMAT;
                              and SCCODCAN = this.w_COMMAPPO;

                          i_Rows = _tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          i_Error='Errore Aggiornamento Saldi Commessa (1)'
                          return
                        endif
                      endif
                    endif
                  endif
                else
                  if this.w_DASTOR<>0 AND NOT EMPTY(this.w_CODMAG)
                    * --- Write into SALDIART
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_DASTOR);
                          +i_ccchkf ;
                      +" where ";
                          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                          +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                             )
                    else
                      update (i_cTable) set;
                          SLQTIPER = SLQTIPER + this.w_DASTOR;
                          &i_ccchkf. ;
                       where;
                          SLCODICE = this.w_KEYSAL;
                          and SLCODMAG = this.w_CODMAG;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura SALDIART (4)'
                      return
                    endif
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARSALCOM"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARSALCOM;
                        from (i_cTable) where;
                            ARCODART = this.w_CODART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if this.w_SALCOM="S"
                      if empty(nvl(this.w_CODCOM,""))
                        this.w_COMMAPPO = this.w_COMMDEFA
                      else
                        this.w_COMMAPPO = this.w_CODCOM
                      endif
                      * --- Write into SALDICOM
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.SALDICOM_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_DASTOR);
                            +i_ccchkf ;
                        +" where ";
                            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                            +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                               )
                      else
                        update (i_cTable) set;
                            SCQTIPER = SCQTIPER + this.w_DASTOR;
                            &i_ccchkf. ;
                         where;
                            SCCODICE = this.w_KEYSAL;
                            and SCCODMAG = this.w_CODMAG;
                            and SCCODCAN = this.w_COMMAPPO;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error='Errore Aggiornamento Saldi Commessa (1)'
                        return
                      endif
                    endif
                  endif
                endif
              else
                * --- Ripristina l'ODL alla situazione Originaria
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
                  +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                  +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC("          "),'ODL_DETT','OLDTEVCL');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLQTAPRE = 0;
                      ,OLQTAPR1 = 0;
                      ,OLFLPREV = " ";
                      ,OLDTEVCL = "          ";
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (1)'
                  return
                endif
              endif
              this.w_OLTIPSCL = "X"
              * --- Scrivo sulla testata dell'OCL che il trasferimento � stato genberato dall'ordine a fornitore
              * --- Write into ODL_MAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.ODL_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"OLTIPSCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPSCL),'ODL_MAST','OLTIPSCL');
                    +i_ccchkf ;
                +" where ";
                    +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                       )
              else
                update (i_cTable) set;
                    OLTIPSCL = this.w_OLTIPSCL;
                    &i_ccchkf. ;
                 where;
                    OLCODODL = this.w_CODODL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- ODL_DETT presente in altri piani, storna solo il prelievo
              if this.w_DISMAG="S"
                * --- Mag. Nettificabile
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAT),'ODL_DETT','OLCODMAG');
                  +",OLQTAPRE =OLQTAPRE- "+cp_ToStrODBC(this.w_QTAPRE);
                  +",OLQTAPR1 =OLQTAPR1- "+cp_ToStrODBC(this.w_QTAPR1);
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                  +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'ODL_DETT','OLFLEVAS');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLCODMAG = this.w_CODMAT;
                      ,OLQTAPRE = OLQTAPRE - this.w_QTAPRE;
                      ,OLQTAPR1 = OLQTAPR1 - this.w_QTAPR1;
                      ,OLFLPREV = " ";
                      ,OLFLEVAS = this.w_FLEVAS;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (1)'
                  return
                endif
              else
                this.w_QTAEVA = IIF(this.w_OLTSTATO="F", 0, this.w_QTAPRE)
                this.w_QTAEV1 = IIF(this.w_OLTSTATO="F", 0, this.w_QTAPR1)
                * --- Write into ODL_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
                  +",OLQTAPRE =OLQTAPRE- "+cp_ToStrODBC(this.w_QTAPRE);
                  +",OLQTAPR1 =OLQTAPR1- "+cp_ToStrODBC(this.w_QTAPR1);
                  +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                  +",OLQTAEVA =OLQTAEVA- "+cp_ToStrODBC(this.w_QTAEVA);
                  +",OLQTAEV1 =OLQTAEV1- "+cp_ToStrODBC(this.w_QTAEV1);
                  +",OLQTASAL =OLQTASAL+ "+cp_ToStrODBC(this.w_DASTOR);
                  +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'ODL_DETT','OLFLEVAS');
                      +i_ccchkf ;
                  +" where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                         )
                else
                  update (i_cTable) set;
                      OLCODMAG = this.w_CODMAG;
                      ,OLQTAPRE = OLQTAPRE - this.w_QTAPRE;
                      ,OLQTAPR1 = OLQTAPR1 - this.w_QTAPR1;
                      ,OLFLPREV = " ";
                      ,OLQTAEVA = OLQTAEVA - this.w_QTAEVA;
                      ,OLQTAEV1 = OLQTAEV1 - this.w_QTAEV1;
                      ,OLQTASAL = OLQTASAL + this.w_DASTOR;
                      ,OLFLEVAS = this.w_FLEVAS;
                      &i_ccchkf. ;
                   where;
                      OLCODODL = this.w_CODODL;
                      and CPROWNUM = this.w_ROWODL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='Errore in scrittura ODL_DETT (2)'
                  return
                endif
                * --- Se il Magazzino non e' nettificabile azzera la Causale
                if .f.
                  if this.w_QTAPR1 > this.w_QTAUM1
                  else
                    * --- Write into ODL_DETT
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
                      +",OLQTAPRE =OLQTAPRE- "+cp_ToStrODBC(this.w_QTAPRE);
                      +",OLQTAPR1 =OLQTAPR1- "+cp_ToStrODBC(this.w_QTAPR1);
                      +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
                      +",OLQTAEVA =OLQTAEVA- "+cp_ToStrODBC(this.w_QTAPRE);
                      +",OLQTAEV1 =OLQTAEV1- "+cp_ToStrODBC(this.w_QTAPR1);
                      +",OLQTASAL =OLQTASAL+ "+cp_ToStrODBC(this.w_QTAPR1);
                      +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_FLEVAS),'ODL_DETT','OLFLEVAS');
                          +i_ccchkf ;
                      +" where ";
                          +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                             )
                    else
                      update (i_cTable) set;
                          OLCODMAG = this.w_CODMAG;
                          ,OLQTAPRE = OLQTAPRE - this.w_QTAPRE;
                          ,OLQTAPR1 = OLQTAPR1 - this.w_QTAPR1;
                          ,OLFLPREV = " ";
                          ,OLQTAEVA = OLQTAEVA - this.w_QTAPRE;
                          ,OLQTAEV1 = OLQTAEV1 - this.w_QTAPR1;
                          ,OLQTASAL = OLQTASAL + this.w_QTAPR1;
                          ,OLFLEVAS = this.w_FLEVAS;
                          &i_ccchkf. ;
                       where;
                          OLCODODL = this.w_CODODL;
                          and CPROWNUM = this.w_ROWODL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore in scrittura ODL_DETT (2)'
                      return
                    endif
                  endif
                endif
              endif
              if this.w_DISMAG<>"S"
                if this.w_DASTOR<>0 AND NOT EMPTY(this.w_CODMAG)
                  * --- Write into SALDIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_DASTOR);
                        +i_ccchkf ;
                    +" where ";
                        +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                        +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                           )
                  else
                    update (i_cTable) set;
                        SLQTIPER = SLQTIPER + this.w_DASTOR;
                        &i_ccchkf. ;
                     where;
                        SLCODICE = this.w_KEYSAL;
                        and SLCODMAG = this.w_CODMAG;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore in scrittura SALDIART (4)'
                    return
                  endif
                  * --- Read from ART_ICOL
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ARSALCOM"+;
                      " from "+i_cTable+" ART_ICOL where ";
                          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ARSALCOM;
                      from (i_cTable) where;
                          ARCODART = this.w_CODART;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_SALCOM="S"
                    if empty(nvl(this.w_CODCOM,""))
                      this.w_COMMAPPO = this.w_COMMDEFA
                    else
                      this.w_COMMAPPO = this.w_CODCOM
                    endif
                    * --- Write into SALDICOM
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.SALDICOM_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_DASTOR);
                          +i_ccchkf ;
                      +" where ";
                          +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                          +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                          +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                             )
                    else
                      update (i_cTable) set;
                          SCQTIPER = SCQTIPER + this.w_DASTOR;
                          &i_ccchkf. ;
                       where;
                          SCCODICE = this.w_KEYSAL;
                          and SCCODMAG = this.w_CODMAG;
                          and SCCODCAN = this.w_COMMAPPO;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error='Errore Aggiornamento Saldi Commessa (1)'
                      return
                    endif
                  endif
                endif
              endif
            endif
        endcase
        if this.Operazione="ELIMINARIGHE"
          * --- Delete from RIF_GODL
          i_nConn=i_TableProp[this.RIF_GODL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PDSERIAL);
                  +" and PDTIPGEN = "+cp_ToStrODBC(this.oParentObject.w_PDTIPGEN);
                  +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
                  +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
                  +" and PDROWDOC = "+cp_ToStrODBC(this.w_ROWDOC);
                   )
          else
            delete from (i_cTable) where;
                  PDSERIAL = this.oParentObject.w_PDSERIAL;
                  and PDTIPGEN = this.oParentObject.w_PDTIPGEN;
                  and PDCODODL = this.w_CODODL;
                  and PDROWODL = this.w_ROWODL;
                  and PDROWDOC = this.w_ROWDOC;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Errore in cancellazione dettaglio riferimenti'
            return
          endif
        endif
        SELECT LISTAODL
        ENDSCAN
        if this.Operazione $ "ELIMINATUTTO-ELIMINARIGHE"
          * --- ripristino dati ODL_DETT nel caso di prelievo con quantit� Zero
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Drop temporary table TMPMODL
          i_nIdx=cp_GetTableDefIdx('TMPMODL')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPMODL')
          endif
        endif
        if this.Operazione="ELIMINATUTTO"
          * --- Elimina l'Intero Piano
          * --- Delete from RIF_GODL
          i_nConn=i_TableProp[this.RIF_GODL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PDSERIAL);
                  +" and PDTIPGEN = "+cp_ToStrODBC(this.oParentObject.w_PDTIPGEN);
                   )
          else
            delete from (i_cTable) where;
                  PDSERIAL = this.oParentObject.w_PDSERIAL;
                  and PDTIPGEN = this.oParentObject.w_PDTIPGEN;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error='Errore in cancellazione dettaglio riferimenti'
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Setta Il Flag Provvisorio a Definitivo
    * --- Try
    local bErr_04258488
    bErr_04258488=bTrsErr
    this.Try_04258488()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04258488
    * --- End
  endproc
  proc Try_04258488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'DOC_MAST','MVFLPROV');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVFLPROV = "N";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in aggiornamento DOC_MAST'
      return
    endif
    this.w_MFLCASC = " "
    this.w_MFLORDI = " "
    this.w_MFLIMPE = " "
    this.w_MFLRISE = " "
    this.w_MF2CASC = " "
    this.w_MF2ORDI = " "
    this.w_MF2IMPE = " "
    this.w_MF2RISE = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_MVTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMCAUCOL;
        from (i_cTable) where;
            CMCODICE = this.w_MVTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.w_MVCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_MVCAUCOL)
      * --- Caso DDT di Trasferimento
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.w_MVCAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
          from (i_cTable) where;
              CMCODICE = this.w_MVCAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MF2CASC),'DOC_DETT','MVF2CASC');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MF2ORDI),'DOC_DETT','MVF2ORDI');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2IMPE),'DOC_DETT','MVF2IMPE');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2RISE),'DOC_DETT','MVF2RISE');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM <> "+cp_ToStrODBC(0);
             )
    else
      update (i_cTable) set;
          MVFLCASC = this.w_MFLCASC;
          ,MVFLORDI = this.w_MFLORDI;
          ,MVFLIMPE = this.w_MFLIMPE;
          ,MVFLRISE = this.w_MFLRISE;
          ,MVF2CASC = this.w_MF2CASC;
          ,MVF2ORDI = this.w_MF2ORDI;
          ,MVF2IMPE = this.w_MF2IMPE;
          ,MVF2RISE = this.w_MF2RISE;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM <> 0;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+"";
           ,"_Curs_DOC_DETT")
    else
      select * from (i_cTable);
       where MVSERIAL=this.w_MVSERIAL;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      * --- Aggiorna Saldi
      this.w_QTAPR1 = NVL(MVQTASAL, 0)
      this.w_KEYSAL = NVL(MVKEYSAL, " ")
      this.w_CODMAG = NVL(MVCODMAG, " ")
      this.w_CODMAT = NVL(MVCODMAT, " ")
      this.w_CODCOM = NVL(MVCODCOM, " ")
      this.w_CODART = NVL(MVCODART, " ")
      this.w_CODODL = NVL(MVCODODL, " ")
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_QTAPR1<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
        if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (1)'
            return
          endif
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_CODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (1)'
              return
            endif
          endif
        endif
        * --- Causale Collegata (DDT di Trasferimento)
        if NOT EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) AND NOT EMPTY(this.w_CODMAT)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SLQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SLQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SLQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SLQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (2)'
            return
          endif
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_CODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SCQTAPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SCQTRPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SCQTOPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SCQTIPER','this.w_QTAPR1',this.w_QTAPR1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAT;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (1)'
              return
            endif
          endif
        endif
      endif
      * --- Leggo il flag dell'ordinato sull'ODL (per sicurezza, anche se sar� sempre +)
      * --- Read from ODL_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "OLTFLORD"+;
          " from "+i_cTable+" ODL_MAST where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          OLTFLORD;
          from (i_cTable) where;
              OLCODODL = this.w_CODODL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLORDI = NVL(cp_ToDate(_read_.OLTFLORD),cp_NullValue(_read_.OLTFLORD))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if nvl(this.w_FLORDI," ")="+"
        * --- Azzera il flag su ODL_MAST
        * --- Write into ODL_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLTFLORD ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_MAST','OLTFLORD');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
                 )
        else
          update (i_cTable) set;
              OLTFLORD = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_QTAPR1<>0 AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG)
        if NOT EMPTY(this.w_FLORDI)
          * --- Aggiorna i saldi
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTOPER =SLQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                   )
          else
            update (i_cTable) set;
                SLQTOPER = SLQTOPER - this.w_QTAPR1;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_CODMAG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore in scrittura SALDIART (1)'
            return
          endif
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_CODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTOPER =SCQTOPER- "+cp_ToStrODBC(this.w_QTAPR1);
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTOPER = SCQTOPER - this.w_QTAPR1;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYSAL;
                  and SCCODMAG = this.w_CODMAG;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa (1)'
              return
            endif
          endif
        endif
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    this.w_nRecEla = this.w_nRecEla + 1
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina tutti i Riferimenti ai DDT di Trasferimento
    * --- Lettura dell' ODL_DETT con i riferimenti ai piani in cui esso e' inserito
    vq_exec("..\COLA\EXE\QUERY\GSCOOBMG.VQR",this,"APPO")
    this.w_TOTALE = .F.
    if USED("APPO")
      SELECT APPO
      GO TOP
      SCAN FOR NOT EMPTY(NVL(PDSERIAL,"")) AND NOT EMPTY(NVL(PDCODODL,"")) AND NVL(PDROWODL,0)<>0
      this.w_DELRIFDT = "S"
      this.w_nReco = RECNO()
      * --- Legge i dati per il Ripristino della situazione Originaria
      this.w_SERODL = PDSERIAL
      SELECT APPO
      GO TOP
      LOCATE FOR NVL(PDSERIAL, SPACE(10))<>this.w_SERODL AND NOT EMPTY(NVL(PDSERIAL,"")) AND PROCESS<>"S"
      if NOT FOUND()
        this.w_TOTALE = .T.
      endif
      SELECT APPO
      GO this.w_nReco
      this.w_CODODL = PDCODODL
      this.w_ROWODL = PDROWODL
      this.w_KEYSAL = NVL(OLKEYSAL, SPACE(20))
      this.w_CODMAG = NVL(OLMAGPRE, SPACE(5))
      this.w_CODMAT = NVL(OLMAGWIP, SPACE(5))
      this.w_CODCOM = NVL(OLTCOMME, SPACE(15))
      this.w_CODART = NVL(OLCODART, SPACE(20))
      this.w_QTAUM1 = NVL(OLQTASAL, 0)
      this.w_DISMAG = NVL(MGDISMAG, " ")
      * --- Ripristina l'ODL_DETT alla situazione Pianificata
      if this.w_DISMAG="S"
        * --- Mag. Nettificabile
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
          +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
          +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
          +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_CODMAG;
              ,OLQTAPRE = 0;
              ,OLQTAPR1 = 0;
              ,OLFLPREV = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;
              and CPROWNUM = this.w_ROWODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura ODL_DETT (1)'
          return
        endif
      else
        * --- Se il Magazzino non e' nettificabile azzera la Causale
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'ODL_DETT','OLCODMAG');
          +",OLCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_PPCAUIMP),'ODL_DETT','OLCAUMAG');
          +",OLFLORDI ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLORDI');
          +",OLFLIMPE ="+cp_NullLink(cp_ToStrODBC("+"),'ODL_DETT','OLFLIMPE');
          +",OLFLRISE ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLRISE');
          +",OLQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPRE');
          +",OLQTAPR1 ="+cp_NullLink(cp_ToStrODBC(0),'ODL_DETT','OLQTAPR1');
          +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWODL);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_CODMAG;
              ,OLCAUMAG = this.w_PPCAUIMP;
              ,OLFLORDI = " ";
              ,OLFLIMPE = "+";
              ,OLFLRISE = " ";
              ,OLQTAPRE = 0;
              ,OLQTAPR1 = 0;
              ,OLFLPREV = " ";
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_CODODL;
              and CPROWNUM = this.w_ROWODL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore in scrittura ODL_DETT (2)'
          return
        endif
      endif
      if this.w_TOTALE
        if this.w_CODMAG<>this.w_CODMAT
          * --- Lo storno dell' Impegno avviene una volta sola
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_QTAUM1<>0 AND NOT EMPTY(this.w_CODMAG)
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER =SLQTIPER+ "+cp_ToStrODBC(this.w_QTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = SLQTIPER + this.w_QTAUM1;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_KEYSAL;
                  and SLCODMAG = this.w_CODMAG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (4)'
              return
            endif
            if this.w_SALCOM="S"
              if empty(nvl(this.w_CODCOM,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_CODCOM
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER =SCQTIPER+ "+cp_ToStrODBC(this.w_QTAUM1);
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = SCQTIPER + this.w_QTAUM1;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_KEYSAL;
                    and SCCODMAG = this.w_CODMAG;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa (1)'
                return
              endif
            endif
          endif
          if this.w_DISMAG="S" AND this.w_QTAUM1<>0 AND NOT EMPTY(this.w_CODMAT)
            * --- Il trasferimento dell'Impegno avviene una volta sola e solo se il Magazzino e' Nettificabile
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER =SLQTIPER- "+cp_ToStrODBC(this.w_QTAUM1);
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = SLQTIPER - this.w_QTAUM1;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_KEYSAL;
                  and SLCODMAG = this.w_CODMAT;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore in scrittura SALDIART (5)'
              return
            endif
            if this.w_SALCOM="S"
              if empty(nvl(this.w_CODCOM,""))
                this.w_COMMAPPO = this.w_COMMDEFA
              else
                this.w_COMMAPPO = this.w_CODCOM
              endif
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER =SCQTIPER- "+cp_ToStrODBC(this.w_QTAUM1);
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAT);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = SCQTIPER - this.w_QTAUM1;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_KEYSAL;
                    and SCCODMAG = this.w_CODMAT;
                    and SCCODCAN = this.w_COMMAPPO;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error='Errore Aggiornamento Saldi Commessa (1)'
                return
              endif
            endif
          endif
        endif
      endif
      * --- Elimina Riferimenti alle generazioni dei DDT di Trasferimento (i DDT gia' eseguiti non vengono modificati)
      * --- Delete from RIF_GODL
      i_nConn=i_TableProp[this.RIF_GODL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIF_GODL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PDSERIAL = "+cp_ToStrODBC(this.w_SERODL);
              +" and PDTIPGEN = "+cp_ToStrODBC("DT");
              +" and PDCODODL = "+cp_ToStrODBC(this.w_CODODL);
              +" and PDROWODL = "+cp_ToStrODBC(this.w_ROWODL);
               )
      else
        delete from (i_cTable) where;
              PDSERIAL = this.w_SERODL;
              and PDTIPGEN = "DT";
              and PDCODODL = this.w_CODODL;
              and PDROWODL = this.w_ROWODL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in cancellazione dettaglio riferimenti'
        return
      endif
      SELECT APPO
      REPLACE PROCESS WITH "S"
      ENDSCAN
      USE
      this.w_OLTIPSCL = "X"
      * --- Scrivo sulla testata dell'OCL che il trasferimento � stato genberato dall'ordine a fornitore
      * --- Write into ODL_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLTIPSCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPSCL),'ODL_MAST','OLTIPSCL');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(this.w_CODODL);
               )
      else
        update (i_cTable) set;
            OLTIPSCL = this.w_OLTIPSCL;
            &i_ccchkf. ;
         where;
            OLCODODL = this.w_CODODL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Righe evase a quantit� zero (senza invio dei materiali) - da GSCO_AMG
    vq_exec("..\COLA\EXE\QUERY\GSCO2BMG.VQR",this,"_Curs_ODL_DETT")
    SELECT "_Curs_ODL_DETT"
    GO TOP
    SCAN
    this.w_FLORDI = _Curs_ODL_DETT.OLFLORDI
    this.w_FLIMPE = _Curs_ODL_DETT.OLFLIMPE
    this.w_FLRISE = _Curs_ODL_DETT.OLFLRISE
    this.w_TMPQTAOR1 = _Curs_ODL_DETT.OLQTAUM1
    this.w_TMPQTAEV1 = _Curs_ODL_DETT.OLQTAEV1
    this.w_TMPQTASAL = _Curs_ODL_DETT.OLQTASAL
    this.w_TMPQPREV = _Curs_ODL_DETT.OLQTAPRE
    this.w_KEYAGSA = _Curs_ODL_DETT.OLKEYSAL
    this.w_CODART = _Curs_ODL_DETT.OLCODART
    this.w_OLCODMAG = _Curs_ODL_DETT.OLCODMAG
    this.w_OLMAGORI = _Curs_ODL_DETT.OLMAGPRE
    this.w_MAGA1 = this.w_OLMAGORI
    this.w_MAGA2 = this.w_OLCODMAG
    * --- Read from ODL_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2],.t.,this.ODL_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OLTCOMME"+;
        " from "+i_cTable+" ODL_MAST where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OLTCOMME;
        from (i_cTable) where;
            OLCODODL = _Curs_ODL_DETT.OLCODODL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODCOM = NVL(cp_ToDate(_read_.OLTCOMME),cp_NullValue(_read_.OLTCOMME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Verifica se magazzino di destinazione � nettificabile
    this.w_NETMAG = " "
    * --- Read from MAGAZZIN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MGDISMAG"+;
        " from "+i_cTable+" MAGAZZIN where ";
            +"MGCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MGDISMAG;
        from (i_cTable) where;
            MGCODMAG = this.w_OLCODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NETMAG = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_NETMAG="S"
      * --- Il magazzino era nettificabile
      if this.w_TMPQPREV>0
        * --- La quantit� realmente spedita � maggiore di zero (da altro DDT)
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
          +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC("          "),'ODL_DETT','OLDTEVCL');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              OLFLPREV = " ";
              ,OLDTEVCL = "          ";
              &i_ccchkf. ;
           where;
              OLCODODL = _Curs_ODL_DETT.OLCODODL;
              and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_OLCODMAG = this.w_OLMAGORI
        if this.w_TIPGEN="OR"
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGORI),'ODL_DETT','OLCODMAG');
            +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
            +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC("          "),'ODL_DETT','OLDTEVCL');
            +",OLFLEVAS ="+cp_NullLink(cp_ToStrODBC("N"),'ODL_DETT','OLFLEVAS');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                OLCODMAG = this.w_OLMAGORI;
                ,OLFLPREV = " ";
                ,OLDTEVCL = "          ";
                ,OLFLEVAS = "N";
                &i_ccchkf. ;
             where;
                OLCODODL = _Curs_ODL_DETT.OLCODODL;
                and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into ODL_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ODL_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGORI),'ODL_DETT','OLCODMAG');
            +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
            +",OLDTEVCL ="+cp_NullLink(cp_ToStrODBC("          "),'ODL_DETT','OLDTEVCL');
                +i_ccchkf ;
            +" where ";
                +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
                +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                OLCODMAG = this.w_OLMAGORI;
                ,OLFLPREV = " ";
                ,OLDTEVCL = "          ";
                &i_ccchkf. ;
             where;
                OLCODODL = _Curs_ODL_DETT.OLCODODL;
                and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if this.w_MAGA1<>this.w_MAGA2
          * --- Ripristina gli impegni solo se viene stornata tutta la quantit� richiesta
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGA1);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYAGSA;
                and SLCODMAG = this.w_MAGA1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_CODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGA1);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYAGSA;
                  and SCCODMAG = this.w_MAGA1;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
          this.w_FLIMPE = IIF(this.w_FLIMPE="+", "-", IIF(this.w_FLIMPE="-", "+", " "))
          this.w_FLRISE = IIF(this.w_FLRISE="+", "-", IIF(this.w_FLRISE="-", "+", " "))
          this.w_FLORDI = IIF(this.w_FLORDI="+", "-", IIF(this.w_FLORDI="-", "+", " "))
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGA2);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYAGSA;
                and SLCODMAG = this.w_MAGA2;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if this.w_SALCOM="S"
            if empty(nvl(this.w_CODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_CODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_TMPQTAOR1',this.w_TMPQTAOR1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_MAGA2);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_KEYAGSA;
                  and SCCODMAG = this.w_MAGA2;
                  and SCCODCAN = this.w_COMMAPPO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
        endif
      endif
    else
      * --- Ricalcola QTA a saldo
      this.w_TMPQTASAL = this.w_TMPQTAOR1 - this.w_TMPQTAEV1
      this.w_TMPFLEVAS = "N"
      if this.w_TMPQTASAL < 0
        this.w_TMPQTASAL = 0
      endif
      * --- Aggiorna dettaglio ODL
      * --- Write into ODL_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"OLFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_TMPFLEVAS),'ODL_DETT','OLFLEVAS');
        +",OLQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_TMPQTASAL),'ODL_DETT','OLQTASAL');
        +",OLFLPREV ="+cp_NullLink(cp_ToStrODBC(" "),'ODL_DETT','OLFLPREV');
            +i_ccchkf ;
        +" where ";
            +"OLCODODL = "+cp_ToStrODBC(_Curs_ODL_DETT.OLCODODL);
            +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ODL_DETT.CPROWNUM);
               )
      else
        update (i_cTable) set;
            OLFLEVAS = this.w_TMPFLEVAS;
            ,OLQTASAL = this.w_TMPQTASAL;
            ,OLFLPREV = " ";
            &i_ccchkf. ;
         where;
            OLCODODL = _Curs_ODL_DETT.OLCODODL;
            and CPROWNUM = _Curs_ODL_DETT.CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna saldi
      this.w_TMPQTASAL = this.w_TMPQTASAL - this.w_TMPQSALOR
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTIPER = &i_cOp1.;
            ,SLQTRPER = &i_cOp2.;
            ,SLQTOPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_KEYAGSA;
            and SLCODMAG = this.w_OLCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_SALCOM="S"
        if empty(nvl(this.w_CODCOM,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_CODCOM
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_TMPQTASAL',this.w_TMPQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYAGSA);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTIPER = &i_cOp1.;
              ,SCQTRPER = &i_cOp2.;
              ,SCQTOPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_KEYAGSA;
              and SCCODMAG = this.w_OLCODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa (1)'
          return
        endif
      endif
    endif
    SELECT "_Curs_ODL_DETT"
    ENDSCAN
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso in cui voglio spostare l'impegno all'ordine devo verificare se il magazzino del terzista � nettificabile
    *     altrimenti non faccio niente
    * --- Viene verificato il magazzino del terzista se nettificabile
    this.w_NETMGORI = " "
    this.w_NETMGWIP = " "
    this.w_FLIMPE = " "
    this.w_FLIMP2 = " "
    this.w_FLORD = " "
    this.w_FLORD2 = " "
    this.w_FLRISE = " "
    this.w_FLRISE2 = " "
    * --- Select from GSCO3BGD
    do vq_exec with 'GSCO3BGD',this,'_Curs_GSCO3BGD','',.f.,.t.
    if used('_Curs_GSCO3BGD')
      select _Curs_GSCO3BGD
      locate for 1=1
      do while not(eof())
      * --- Leggo i valori dei flag impegnato/ordinato/riservato
      this.w_FLIMPE = NVL(_Curs_GSCO3BGD.OLFLIMPE , SPACE(1))
      this.w_FLORD = NVL(_Curs_GSCO3BGD.OLFLORDI , SPACE(1))
      this.w_FLRISE = NVL(_Curs_GSCO3BGD.OLFLRISE , SPACE(1))
      this.w_OLQTASAL = NVL(_Curs_GSCO3BGD.OLQTASAL, 0)
      this.w_OLCODART = NVL(_Curs_GSCO3BGD.OLCODART, SPACE(20) )
      this.w_OLKEYSAL = NVL(_Curs_GSCO3BGD.OLKEYSAL, SPACE(40) )
      this.w_OLMAGPRE = NVL(_Curs_GSCO3BGD.OLMAGORI, SPACE(5) )
      this.w_OLTCOMME = NVL(_Curs_GSCO3BGD.OLTCOMME, SPACE(15) )
      this.w_FLCOM = NVL(_Curs_GSCO3BGD.ARSALCOM, "N" )
      this.w_OLMAGWIP = NVL(_Curs_GSCO3BGD.OLCODMAG, SPACE(5) )
      this.w_NETMGWIP = NVL(_Curs_GSCO3BGD.NETMGWIP, "N")
      if this.w_NETMGWIP="S" AND this.w_OLQTASAL<>0 AND NOT EMPTY(this.w_OLKEYSAL) AND NOT EMPTY(this.w_OLMAGPRE) AND NOT EMPTY(this.w_OLMAGWIP)
        this.w_NETMGORI = " "
        this.w_NETMGWIP = " "
        * --- Magazzino principale
        if NOT EMPTY(this.w_FLIMPE+this.w_FLORD+this.w_FLRISE)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGPRE);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLMAGPRE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- Try
            local bErr_044EF780
            bErr_044EF780=bTrsErr
            this.Try_044EF780()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_044EF780
            * --- End
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGPRE);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OLKEYSAL;
                  and SLCODMAG = this.w_OLMAGPRE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if NVL(this.w_FLCOM,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGPRE);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLMAGPRE;
                  and SCCODCAN = this.w_OLTCOMME;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Aggiorna i saldi commessa
              * --- Try
              local bErr_044ED320
              bErr_044ED320=bTrsErr
              this.Try_044ED320()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_044ED320
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGPRE);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OLKEYSAL;
                    and SCCODMAG = this.w_OLMAGPRE;
                    and SCCODCAN = this.w_OLTCOMME;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        * --- Imposto i flag per il magazzino del terzista
        this.w_FLIMP2 = IIF(this.w_FLIMPE="+", "-", IIF(this.w_FLIMPE="-", "+", " "))
        this.w_FLORD2 = IIF(this.w_FLORD="+", "-", IIF(this.w_FLORD="-", "+", " "))
        this.w_FLRISE2 = IIF(this.w_FLRISE="+", "-", IIF(this.w_FLRISE="-", "+", " "))
        * --- Magazzino terzista
        if NOT EMPTY(this.w_FLIMP2+this.w_FLORD2+this.w_FLRISE2)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                   )
          else
            update (i_cTable) set;
                SLQTIPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_OLKEYSAL;
                and SLCODMAG = this.w_OLMAGWIP;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- Try
            local bErr_044F7A30
            bErr_044F7A30=bTrsErr
            this.Try_044F7A30()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_044F7A30
            * --- End
            * --- Write into SALDIART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SLQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SLQTIPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SLCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SLCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                     )
            else
              update (i_cTable) set;
                  SLQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SLCODICE = this.w_OLKEYSAL;
                  and SLCODMAG = this.w_OLMAGWIP;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if NVL(this.w_FLCOM,"N")="S" and ! empty(nvl(this.w_OLTCOMME," "))
            * --- Aggiorna i saldi commessa
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                     )
            else
              update (i_cTable) set;
                  SCQTIPER = &i_cOp1.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_OLKEYSAL;
                  and SCCODMAG = this.w_OLMAGWIP;
                  and SCCODCAN = this.w_OLTCOMME;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if i_Rows=0
              * --- Try
              local bErr_044F5570
              bErr_044F5570=bTrsErr
              this.Try_044F5570()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_044F5570
              * --- End
              * --- Write into SALDICOM
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.SALDICOM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_FLIMP2,'SCQTIPER','this.w_OLQTASAL',this.w_OLQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SCQTIPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTIPER');
                    +i_ccchkf ;
                +" where ";
                    +"SCCODICE = "+cp_ToStrODBC(this.w_OLKEYSAL);
                    +" and SCCODMAG = "+cp_ToStrODBC(this.w_OLMAGWIP);
                    +" and SCCODCAN = "+cp_ToStrODBC(this.w_OLTCOMME);
                       )
              else
                update (i_cTable) set;
                    SCQTIPER = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    SCCODICE = this.w_OLKEYSAL;
                    and SCCODMAG = this.w_OLMAGWIP;
                    and SCCODCAN = this.w_OLTCOMME;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        * --- Write into ODL_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ODL_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"OLCODMAG ="+cp_NullLink(cp_ToStrODBC(this.w_OLMAGPRE),'ODL_DETT','OLCODMAG');
              +i_ccchkf ;
          +" where ";
              +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
              +" and CPROWNUM = "+cp_ToStrODBC(_Curs_GSCO3BGD.CPROWNUM);
                 )
        else
          update (i_cTable) set;
              OLCODMAG = this.w_OLMAGPRE;
              &i_ccchkf. ;
           where;
              OLCODODL = this.w_OLCODODL;
              and CPROWNUM = _Curs_GSCO3BGD.CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_GSCO3BGD
        continue
      enddo
      use
    endif
    this.w_OLTIPSCL = "X"
    * --- Scrivo sulla testata dell'OCL che il trasferimento � stato genberato dall'ordine a fornitore
    * --- Write into ODL_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ODL_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ODL_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ODL_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OLTIPSCL ="+cp_NullLink(cp_ToStrODBC(this.w_OLTIPSCL),'ODL_MAST','OLTIPSCL');
          +i_ccchkf ;
      +" where ";
          +"OLCODODL = "+cp_ToStrODBC(this.w_OLCODODL);
             )
    else
      update (i_cTable) set;
          OLTIPSCL = this.w_OLTIPSCL;
          &i_ccchkf. ;
       where;
          OLCODODL = this.w_OLCODODL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc
  proc Try_044EF780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGPRE),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLKEYSAL,'SLCODMAG',this.w_OLMAGPRE)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044ED320()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGPRE),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLKEYSAL,'SCCODMAG',this.w_OLMAGPRE,'SCCODCAN',this.w_OLTCOMME)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGPRE;
           ,this.w_OLTCOMME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044F7A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_OLKEYSAL,'SLCODMAG',this.w_OLMAGWIP)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGWIP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044F5570()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OLKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLMAGWIP),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OLTCOMME),'SALDICOM','SCCODCAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_OLKEYSAL,'SCCODMAG',this.w_OLMAGWIP,'SCCODCAN',this.w_OLTCOMME)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN &i_ccchkf. );
         values (;
           this.w_OLKEYSAL;
           ,this.w_OLMAGWIP;
           ,this.w_OLTCOMME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Operazione)
    this.Operazione=Operazione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,18)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='DOC_RATE'
    this.cWorkTables[5]='ODL_DETT'
    this.cWorkTables[6]='ODL_MAST'
    this.cWorkTables[7]='PAR_PROD'
    this.cWorkTables[8]='RIFMGODL'
    this.cWorkTables[9]='RIF_GODL'
    this.cWorkTables[10]='SALDIART'
    this.cWorkTables[11]='TIP_DOCU'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='*TMPMODL'
    this.cWorkTables[14]='MOVIMATR'
    this.cWorkTables[15]='MAT_PROD'
    this.cWorkTables[16]='PEG_SELI'
    this.cWorkTables[17]='ART_ICOL'
    this.cWorkTables[18]='SALDICOM'
    return(this.OpenAllTables(18))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MOVIMATR')
      use in _Curs_MOVIMATR
    endif
    if used('_Curs_GSCO_BAD')
      use in _Curs_GSCO_BAD
    endif
    if used('_Curs_gsco_bmg')
      use in _Curs_gsco_bmg
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_GSCO3BGD')
      use in _Curs_GSCO3BGD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Operazione"
endproc
