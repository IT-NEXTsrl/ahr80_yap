* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_kap                                                        *
*              Available-to-promise (ATP)                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-19                                                      *
* Last revis.: 2017-01-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdb_kap",oParentObject))

* --- Class definition
define class tgsdb_kap as StdForm
  Top    = 12
  Left   = 0

  * --- Standard Properties
  Width  = 813
  Height = 353
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-01-16"
  HelpContextID=135629719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  KEY_ARTI_IDX = 0
  MPS_TPER_IDX = 0
  ART_ICOL_IDX = 0
  PAR_PROD_IDX = 0
  PAR_RIOR_IDX = 0
  cPrg = "gsdb_kap"
  cComment = "Available-to-promise (ATP)"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PPCODICE = space(2)
  o_PPCODICE = space(2)
  w_NUMPER = 0
  w_DimFont = 0
  w_LargCol = 0
  w_CALSTA = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_CODICE = space(20)
  w_DESCRI = space(40)
  w_CODART = space(20)
  w_DESART = space(40)
  w_UNIMIS = space(3)
  w_CODVAR = space(20)
  w_CODSAL = space(20)
  w_CODART1 = space(20)
  w_LEAMPS = 0
  w_ONHAND = 0
  w_SUGGER = space(1)
  w_COL = 0
  w_PERIOD = space(3)
  w_PERREL = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_PERDTF = space(3)
  w_FLCOMM = space(1)
  w_MPS = space(1)
  w_CODDIS = space(20)
  w_PPTIPDTF = space(1)
  w_PP___DTF = 0
  w_PRTIPDTF = space(1)
  w_PRGIODTF = 0
  w_ZoomMPS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdb_kapPag1","gsdb_kap",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMPS = this.oPgFrm.Pages(1).oPag.ZoomMPS
    DoDefault()
    proc Destroy()
      this.w_ZoomMPS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='MPS_TPER'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='PAR_PROD'
    this.cWorkTables[5]='PAR_RIOR'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PPCODICE=space(2)
      .w_NUMPER=0
      .w_DimFont=0
      .w_LargCol=0
      .w_CALSTA=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICE=space(20)
      .w_DESCRI=space(40)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_UNIMIS=space(3)
      .w_CODVAR=space(20)
      .w_CODSAL=space(20)
      .w_CODART1=space(20)
      .w_LEAMPS=0
      .w_ONHAND=0
      .w_SUGGER=space(1)
      .w_COL=0
      .w_PERIOD=space(3)
      .w_PERREL=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PERDTF=space(3)
      .w_FLCOMM=space(1)
      .w_MPS=space(1)
      .w_CODDIS=space(20)
      .w_PPTIPDTF=space(1)
      .w_PP___DTF=0
      .w_PRTIPDTF=space(1)
      .w_PRGIODTF=0
        .w_PPCODICE = "PP"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PPCODICE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,5,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODICE))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CODART))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,12,.f.)
        .w_CODSAL = .w_CODART
        .w_CODART1 = .w_CODART
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODART1))
          .link_1_14('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomMPS.Calculate()
          .DoRTCalc(15,16,.f.)
        .w_SUGGER = '-'
        .w_COL = .w_ZOOMMPS.GRD.ACTIVECOLUMN
        .w_PERIOD = IIF(.w_COL>1,right("000"+alltrim(str(.w_COL-2,3,0)),3),"")
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_PERIOD))
          .link_1_26('Full')
        endif
          .DoRTCalc(20,22,.f.)
        .w_PERDTF = "000"
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
          .DoRTCalc(24,26,.f.)
        .w_PPTIPDTF = 'F'
    endwith
    this.DoRTCalc(28,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_PPCODICE<>.w_PPCODICE
          .link_1_1('Full')
        endif
        .DoRTCalc(2,8,.t.)
          .link_1_9('Full')
        .DoRTCalc(10,12,.t.)
            .w_CODSAL = .w_CODART
            .w_CODART1 = .w_CODART
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.ZoomMPS.Calculate()
        .DoRTCalc(15,17,.t.)
            .w_COL = .w_ZOOMMPS.GRD.ACTIVECOLUMN
            .w_PERIOD = IIF(.w_COL>1,right("000"+alltrim(str(.w_COL-2,3,0)),3),"")
          .link_1_26('Full')
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMPS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMPS.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PROD_IDX,3]
    i_lTable = "PAR_PROD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2], .t., this.PAR_PROD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPNUMPER,PP___DTF,PPFONMPS,PPCOLMPS,PPCALSTA";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_PPCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_PPCODICE)
            select PPCODICE,PPNUMPER,PP___DTF,PPFONMPS,PPCOLMPS,PPCALSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODICE = NVL(_Link_.PPCODICE,space(2))
      this.w_NUMPER = NVL(_Link_.PPNUMPER,0)
      this.w_PP___DTF = NVL(_Link_.PP___DTF,0)
      this.w_DimFont = NVL(_Link_.PPFONMPS,0)
      this.w_LargCol = NVL(_Link_.PPCOLMPS,0)
      this.w_CALSTA = NVL(_Link_.PPCALSTA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODICE = space(2)
      endif
      this.w_NUMPER = 0
      this.w_PP___DTF = 0
      this.w_DimFont = 0
      this.w_LargCol = 0
      this.w_CALSTA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PROD_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.PAR_PROD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODICE))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODICE_1_7'),i_cWhere,'',"Codice di ricerca",'GSDBZMPS.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODICE)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESCRI = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(20)
      endif
      this.w_DESCRI = space(40)
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARDESART,ARFLCOMM,ARCODDIS";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARUNMIS1,ARDESART,ARFLCOMM,ARCODDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_FLCOMM = NVL(_Link_.ARFLCOMM,space(1))
      this.w_CODDIS = NVL(_Link_.ARCODDIS,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_UNIMIS = space(3)
      this.w_DESART = space(40)
      this.w_FLCOMM = space(1)
      this.w_CODDIS = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART1
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_RIOR_IDX,3]
    i_lTable = "PAR_RIOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2], .t., this.PAR_RIOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODART,PRLEAMPS,PROGGMPS,PRTIPDTF,PRGIODTF";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODART="+cp_ToStrODBC(this.w_CODART1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODART',this.w_CODART1)
            select PRCODART,PRLEAMPS,PROGGMPS,PRTIPDTF,PRGIODTF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART1 = NVL(_Link_.PRCODART,space(20))
      this.w_LEAMPS = NVL(_Link_.PRLEAMPS,0)
      this.w_MPS = NVL(_Link_.PROGGMPS,space(1))
      this.w_PRTIPDTF = NVL(_Link_.PRTIPDTF,space(1))
      this.w_PRGIODTF = NVL(_Link_.PRGIODTF,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODART1 = space(20)
      endif
      this.w_LEAMPS = 0
      this.w_MPS = space(1)
      this.w_PRTIPDTF = space(1)
      this.w_PRGIODTF = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_RIOR_IDX,2])+'\'+cp_ToStr(_Link_.PRCODART,1)
      cp_ShowWarn(i_cKey,this.PAR_RIOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERIOD
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MPS_TPER_IDX,3]
    i_lTable = "MPS_TPER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MPS_TPER_IDX,2], .t., this.MPS_TPER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MPS_TPER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPPERASS,TPPERREL,TPDATINI,TPDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where TPPERASS="+cp_ToStrODBC(this.w_PERIOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPPERASS',this.w_PERIOD)
            select TPPERASS,TPPERREL,TPDATINI,TPDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIOD = NVL(_Link_.TPPERASS,space(3))
      this.w_PERREL = NVL(_Link_.TPPERREL,space(4))
      this.w_DATINI = NVL(cp_ToDate(_Link_.TPDATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.TPDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PERIOD = space(3)
      endif
      this.w_PERREL = space(4)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MPS_TPER_IDX,2])+'\'+cp_ToStr(_Link_.TPPERASS,1)
      cp_ShowWarn(i_cKey,this.MPS_TPER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_7.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_7.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_9.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_9.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_10.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_10.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_11.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_11.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oONHAND_1_17.value==this.w_ONHAND)
      this.oPgFrm.Page1.oPag.oONHAND_1_17.value=this.w_ONHAND
    endif
    if not(this.oPgFrm.Page1.oPag.oSUGGER_1_22.RadioValue()==this.w_SUGGER)
      this.oPgFrm.Page1.oPag.oSUGGER_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERREL_1_27.value==this.w_PERREL)
      this.oPgFrm.Page1.oPag.oPERREL_1_27.value=this.w_PERREL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_28.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_28.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_29.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_29.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPPTIPDTF_1_41.RadioValue()==this.w_PPTIPDTF)
      this.oPgFrm.Page1.oPag.oPPTIPDTF_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPP___DTF_1_42.value==this.w_PP___DTF)
      this.oPgFrm.Page1.oPag.oPP___DTF_1_42.value=this.w_PP___DTF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PPCODICE = this.w_PPCODICE
    return

enddefine

* --- Define pages as container
define class tgsdb_kapPag1 as StdContainer
  Width  = 809
  height = 353
  stdWidth  = 809
  stdheight = 353
  resizeXpos=726
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_7 as StdField with uid="RZVEYHZYWS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice di ricerca dell'articolo",;
    HelpContextID = 242016474,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=58, Top=4, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODICE"

  func oCODICE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODICE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice di ricerca",'GSDBZMPS.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oDESCRI_1_8 as StdField with uid="DYCUWXBENR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 159513290,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=299, Top=4, InputMask=replicate('X',40)

  add object oCODART_1_9 as StdField with uid="CWJCMOTSUY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice articolo",;
    HelpContextID = 243589338,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=58, Top=27, InputMask=replicate('X',20), cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESART_1_10 as StdField with uid="YBCVYSSJHB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243530442,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=299, Top=26, InputMask=replicate('X',40)

  add object oUNIMIS_1_11 as StdField with uid="QXEHTSDJNS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "UnitÓ di misura principale",;
    HelpContextID = 561338,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=58, Top=50, InputMask=replicate('X',3)


  add object ZoomMPS as cp_zoombox with uid="KXNCCWCFVF",left=-1, top=70, width=801,height=230,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSDB_KAP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,bQueryOnLoad=.t.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",;
    nPag=1;
    , HelpContextID = 223243546

  add object oONHAND_1_17 as StdField with uid="GRNXLIDWBX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ONHAND", cQueryName = "ONHAND",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "DisponibilitÓ iniziale (on hand)",;
    HelpContextID = 247767322,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=94, Top=313, cSayPict="v_pq(12)", cGetPict="v_pq(12)"


  add object oBtn_1_21 as StdButton with uid="HJDMCCFFZA",left=755, top=4, width=48,height=45,;
    CpPicture="BMP\REQUERY.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i dati";
    , HelpContextID = 41859818;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSDB_BAP(this.Parent.oContained,"G", .w_CODSAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_CODICE))
      endwith
    endif
  endfunc

  add object oSUGGER_1_22 as StdCheck with uid="OOIUBWOQSR",rtseq=17,rtrep=.f.,left=622, top=50, caption="ODP suggeriti",;
    ToolTipText = "Considera anche gli ODP suggeriti",;
    HelpContextID = 21932506,;
    cFormVar="w_SUGGER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSUGGER_1_22.RadioValue()
    return(iif(this.value =1,'+',;
    '-'))
  endfunc
  func oSUGGER_1_22.GetRadio()
    this.Parent.oContained.w_SUGGER = this.RadioValue()
    return .t.
  endfunc

  func oSUGGER_1_22.SetRadio()
    this.Parent.oContained.w_SUGGER=trim(this.Parent.oContained.w_SUGGER)
    this.value = ;
      iif(this.Parent.oContained.w_SUGGER=='+',1,;
      0)
  endfunc


  add object oBtn_1_23 as StdButton with uid="GYVFLVABWN",left=753, top=304, width=48,height=45,;
    CpPicture="BMP\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41859818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPERREL_1_27 as StdField with uid="DMAXNYEAZJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PERREL", cQueryName = "PERREL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 121833994,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=388, Top=313, InputMask=replicate('X',4)

  add object oDATINI_1_28 as StdField with uid="DLUYRASXEW",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale del periodo",;
    HelpContextID = 163311306,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=492, Top=313

  add object oDATFIN_1_29 as StdField with uid="TIMCEZUGYW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale del periodo",;
    HelpContextID = 84864714,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=607, Top=313


  add object oObj_1_36 as cp_runprogram with uid="VBUPPXVEUM",left=391, top=376, width=72,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSDB_BAP("Intestazione")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 223243546


  add object oObj_1_37 as cp_runprogram with uid="PTSBKEIYZE",left=467, top=376, width=72,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSDB_BAP("Azzera")',;
    cEvent = "w_CODICE Changed",;
    nPag=1;
    , HelpContextID = 223243546


  add object oPPTIPDTF_1_41 as StdCombo with uid="CAWZZMJXMN",rtseq=27,rtrep=.f.,left=299,top=50,width=153,height=22;
    , ToolTipText = "Indica la modalitÓ di gestione del DTF (demand time fence) dando la possibilitÓ di gestire il parametro per articolo oppure generico per elaborazione";
    , HelpContextID = 245096132;
    , cFormVar="w_PPTIPDTF",RowSource=""+"Da anagrafica articolo,"+"Forza valore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPTIPDTF_1_41.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPPTIPDTF_1_41.GetRadio()
    this.Parent.oContained.w_PPTIPDTF = this.RadioValue()
    return .t.
  endfunc

  func oPPTIPDTF_1_41.SetRadio()
    this.Parent.oContained.w_PPTIPDTF=trim(this.Parent.oContained.w_PPTIPDTF)
    this.value = ;
      iif(this.Parent.oContained.w_PPTIPDTF=='A',1,;
      iif(this.Parent.oContained.w_PPTIPDTF=='F',2,;
      0))
  endfunc

  add object oPP___DTF_1_42 as StdField with uid="UMQULEECOL",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PP___DTF", cQueryName = "PP___DTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Demand time fence (non considera le previsioni nei prossimi N giorni)",;
    HelpContextID = 227880644,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=554, Top=50

  add object oStr_1_18 as StdString with uid="SCTHOULFNM",Visible=.t., Left=11, Top=5,;
    Alignment=1, Width=45, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GQTQOGJXKI",Visible=.t., Left=215, Top=5,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YKXXWBOEIZ",Visible=.t., Left=5, Top=315,;
    Alignment=1, Width=85, Height=15,;
    Caption="DisponibilitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QOJYVYJRQJ",Visible=.t., Left=23, Top=51,;
    Alignment=1, Width=32, Height=15,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="HDWOFSYGWU",Visible=.t., Left=12, Top=27,;
    Alignment=1, Width=43, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="IASATHDMCP",Visible=.t., Left=254, Top=315,;
    Alignment=1, Width=132, Height=15,;
    Caption="Periodo selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="UXSOEPMCQW",Visible=.t., Left=446, Top=315,;
    Alignment=1, Width=43, Height=15,;
    Caption="inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ECVVAQWXKQ",Visible=.t., Left=570, Top=315,;
    Alignment=1, Width=34, Height=15,;
    Caption="fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="DAXTNAMCNQ",Visible=.t., Left=215, Top=27,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="LSCCHYBDBU",Visible=.t., Left=154, Top=51,;
    Alignment=1, Width=143, Height=18,;
    Caption="Tipo di gestione DTF:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="VXOPVPCUQD",Visible=.t., Left=483, Top=51,;
    Alignment=1, Width=68, Height=18,;
    Caption="Giorni DTF:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdb_kap','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
