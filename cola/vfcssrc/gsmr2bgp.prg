* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr2bgp                                                        *
*              Gestione archivio generazione MRP                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-10                                                      *
* Last revis.: 2018-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr2bgp",oParentObject,m.pParam)
return(i_retval)

define class tgsmr2bgp as StdBatch
  * --- Local variables
  w_obj = .NULL.
  pParam = space(1)
  w_TRTIPDOC = space(0)
  w_TRLISMAG = space(0)
  w_PROG = .NULL.
  w_TIPDOC = space(200)
  w_OCCCHR = 0
  w_CONTA = 0
  Padre = .NULL.
  NC = space(10)
  NM = space(10)
  w_LISMAG = space(0)
  w_CLASSE = .f.
  w_TIPDOCM = space(0)
  w_GRUPPO = 0
  w_UTENTE = 0
  w_GRUKEY = 0
  w_DATAPPO = ctod("  /  /  ")
  w_TROVATO = space(1)
  w_PARAM = space(1)
  w_EDITBTN = space(1)
  w_UseProgBar = .f.
  w_bTITLE = space(40)
  w_PEGGING = .f.
  w_MESSRIP = .f.
  w_KEYRIF = space(10)
  w_OBJPARENT = .NULL.
  * --- WorkFile variables
  PAR__MRP_idx=0
  cpusrgrp_idx=0
  ART_TEMP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Archivio
    *     Da GSMR_AGM, GSMR_APU, GSMR_KPD e GSMR_KGP
    this.Padre = this.oParentobject
    do case
      case this.pParam="V"
        * --- Apertura Archivio GSMR_AGM da GSMR_KGP - si apre il record con GMFLULEL ad 'S', cio� l'ultimo record elaborato
        this.w_PROG = GSMR_AGM()
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.EcpFilter()     
        this.w_PROG.w_GMFLULEL = "S"
        this.w_PROG.EcpSave()     
      case this.pParam="O"
        * --- Apertura Archivio GSMR_AGO da GSMR_KPD - si apre il record con GMFLULEL ad 'S', cio� l'ultimo record elaborato
        this.w_PROG = GSCO_AGO()
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.EcpFilter()     
        this.w_PROG.w_GMFLULEL = "S"
        this.w_PROG.w_GMGENODA = "S"
        this.w_PROG.EcpSave()     
      case this.pParam="Z"
        * --- Gestione zoom GSMR_AGM/GSMR_APU
        if UPPER(ALLTRIM(this.Padre.class)) = "TGSMR_AGM"
          this.NC = this.Padre.w_SZOOM1.cCursor
        else
          this.NC = this.Padre.w_SZOOM.cCursor
        endif
        this.w_CLASSE = Alltrim(Upper(this.Padre.Class))="TGSMR_APU"
        if this.w_CLASSE
          this.w_TRTIPDOC = this.oParentObject.w_PUTIPDOC
        else
          this.w_TRTIPDOC = this.oParentObject.w_GMTIPDOC
        endif
        Select(this.NC) 
 zap
        this.w_CONTA = 0
        this.w_OCCCHR = occurs(chr(13)+chr(10),this.w_TRTIPDOC)
        for this.w_CONTA=1 to this.w_OCCCHR
        this.w_TIPDOC = substr(this.w_TRTIPDOC,(52*(this.w_CONTA-1)+1),50)
        Select(this.NC) 
 m.tdtipdoc=substr(this.w_TIPDOC,1,5) 
 m.tddesdoc=substr(this.w_TIPDOC,6,35) 
 m.aggsaldi=substr(this.w_TIPDOC,41,9) 
 m.xchk=val(substr(this.w_TIPDOC,50,1)) 
 append blank 
 gather memvar
        * --- Aggiorno il cursore dello zoom
        endfor
        if this.w_CLASSE
          * --- Lista di tutte le causali
          vq_exec ("..\COLA\exe\query\GSCO_KGP", this, "_CauAgg_")
          Select _CauAgg_ 
 go top 
 scan
          Select (this.NC) 
 go top
          locate for tdtipdoc=_CauAgg_.tdtipdoc
          if ! found()
            * --- Se sono state aggiunte delle causali le aggiungo alla visualizzazione (di default deselezionate)
            Select _CauAgg_ 
 scatter memvar 
 m.xchk=0
            Select (this.NC) 
 append blank 
 gather memvar
          endif
          endscan
        endif
        if this.w_CLASSE
          this.oParentObject.w_STIPART = iif(((empty(this.oParentObject.w_PUPROFIN)or this.oParentObject.w_PUPROFIN="PF") and (empty(this.oParentObject.w_PUSEMLAV)or this.oParentObject.w_PUSEMLAV="SE") and (empty(this.oParentObject.w_PUMATPRI)or this.oParentObject.w_PUMATPRI="MP")),"S","N")
          Select (this.NC) 
 count for (xchk=0) to this.oParentObject.w_CAUSALI
        endif
        Select(this.NC) 
 go top
        if used("_CauAgg_")
          Use in _CauAgg_
        endif
      case this.pParam="S"
        this.Padre = this.oParentobject
        this.Padre.LockScreen = True
        this.NC = this.Padre.w_SZOOM.cCursor
        Select (this.NC)
        riga=recno()
        Select (this.NC)
        GO TOP
        riga2=reccount(this.NC)
        if riga>riga2
          riga=riga2
        endif
        count for (xchk=0) to this.oParentObject.w_CAUSALI
        go riga
        this.oParentObject.w_TIPDOCU = ""
        if this.oParentObject.w_CAUSALI <> 0
          Select (this.NC)
          Go top
          Scan for xchk=1
          this.oParentObject.w_TIPDOCU = this.oParentObject.w_TIPDOCU + "'" + TDTIPDOC + "', "
          Select (this.NC)
          EndScan
          this.oParentObject.w_TIPDOCU = LEFT(this.oParentObject.w_TIPDOCU, LEN(this.oParentObject.w_TIPDOCU)-2)
        endif
        if Empty(this.oParentObject.w_TIPDOCU)
          this.oParentObject.w_TIPDOCU = " ' ' "
        endif
        Select (this.NC)
        go riga
        this.NM = this.Padre.w_ZOOMMAGA.cCursor
        Select (this.NM)
        Riga=Recno()
        Select (this.NM)
        GO TOP
        riga2=reccount(this.NM)
        if riga>riga2
          riga=riga2
        endif
        count for (xchk=0) to this.oParentObject.w_MAGAZZINI
        this.oParentObject.w_LISTMAGA = ""
        if this.oParentObject.w_MAGAZZINI <> 0
          Select (this.NM)
          Go top
          Scan for xchk=1
          this.oParentObject.w_LISTMAGA = this.oParentObject.w_LISTMAGA + ALLTRIM(MGCODMAG)+CHR(255)
          Select (this.NM)
          EndScan
          this.oParentObject.w_LISTMAGA = LEFT(this.oParentObject.w_LISTMAGA, LEN(this.oParentObject.w_LISTMAGA)-1)
        endif
        if Reccount(this.NM)>0
          Select (this.NM)
          Go Riga
        endif
        if Empty(this.oParentObject.w_LISTMAGA)
          this.oParentObject.w_LISTMAGA = ""
        endif
        this.Padre.LockScreen = False
      case this.pParam="R"
        * --- Salvataggio zoom GSMR_APU sulla tabella PAR__MRP
        this.Padre = this.oParentobject
        this.NC = this.Padre.w_SZOOM.cCursor
        * --- Inserisco le scelte effettuate per le causali di documento in un campo MEMO
        Select(this.NC) 
 scan 
 scatter memvar
        this.w_TIPDOCM = this.w_TIPDOCM+left(m.tdtipdoc+space(5),5)+left(m.tddesdoc+space(35),35)+left(m.aggsaldi+space(9),9)+alltrim(str(m.xchk))+chr(13)+chr(10)
        endscan
        this.NM = this.Padre.w_ZOOMMAGA.cCursor
        * --- Inserisco le scelte effettuate per i filtri magazzino in un campo MEMO
        if this.oParentObject.w_PUCRIELA<>"A"
          Select(this.NM) 
 scan 
 scatter memvar
          if m.xchk=1
            this.w_LISMAG = this.w_LISMAG+alltrim(m.mgcodmag)+chr(255)
          endif
          endscan
          this.w_LISMAG = LEFT(this.w_LISMAG, LEN(this.w_LISMAG)-1)
        else
          * --- Nel caso di elaborazione aggregata si considerano tutti i magazzini
          this.w_LISMAG = ""
        endif
        * --- Write into PAR__MRP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR__MRP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PUTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_TIPDOCM),'PAR__MRP','PUTIPDOC');
          +",PULISMAG ="+cp_NullLink(cp_ToStrODBC(this.w_LISMAG),'PAR__MRP','PULISMAG');
              +i_ccchkf ;
          +" where ";
              +"PUNUMKEY = "+cp_ToStrODBC(this.oParentObject.w_PUNUMKEY);
                 )
        else
          update (i_cTable) set;
              PUTIPDOC = this.w_TIPDOCM;
              ,PULISMAG = this.w_LISMAG;
              &i_ccchkf. ;
           where;
              PUNUMKEY = this.oParentObject.w_PUNUMKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.pParam="I"
        * --- Cerco un record per l'inizializzazione dei parametri utente/gruppo
        * --- Cerco un record relativo all'utente
        this.w_UTENTE = i_codute
        this.w_TROVATO = "N"
        * --- Select from PAR__MRP
        i_nConn=i_TableProp[this.PAR__MRP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2],.t.,this.PAR__MRP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR__MRP ";
              +" where PUNUMKEY="+cp_ToStrODBC(this.w_utente)+" and PUCODUTE="+cp_ToStrODBC(this.w_utente)+"";
               ,"_Curs_PAR__MRP")
        else
          select * from (i_cTable);
           where PUNUMKEY=this.w_utente and PUCODUTE=this.w_utente;
            into cursor _Curs_PAR__MRP
        endif
        if used('_Curs_PAR__MRP')
          select _Curs_PAR__MRP
          locate for 1=1
          do while not(eof())
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_PAR__MRP
            continue
          enddo
          use
        endif
        if this.w_TROVATO<>"S"
          * --- Se non c'� nulla associato all'utente cerco se c'� un record associato al gruppo
          * --- Potrei trovare nessun record oppure pi� di uno, nel secondo caso prendo il primo gruppo che trovo
          * --- Select from cpusrgrp
          i_nConn=i_TableProp[this.cpusrgrp_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.cpusrgrp_idx,2],.t.,this.cpusrgrp_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select groupcode  from "+i_cTable+" cpusrgrp ";
                +" where usercode="+cp_ToStrODBC(this.w_UTENTE)+"";
                 ,"_Curs_cpusrgrp")
          else
            select groupcode from (i_cTable);
             where usercode=this.w_UTENTE;
              into cursor _Curs_cpusrgrp
          endif
          if used('_Curs_cpusrgrp')
            select _Curs_cpusrgrp
            locate for 1=1
            do while not(eof())
            if this.w_TROVATO<>"S"
              this.w_GRUPPO = groupcode
              this.w_GRUKEY = this.w_GRUPPO*10000
              * --- Select from PAR__MRP
              i_nConn=i_TableProp[this.PAR__MRP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR__MRP_idx,2],.t.,this.PAR__MRP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR__MRP ";
                    +" where PUNUMKEY="+cp_ToStrODBC(this.w_GRUKEY)+" and PUCODGRP="+cp_ToStrODBC(this.w_GRUPPO)+"";
                     ,"_Curs_PAR__MRP")
              else
                select * from (i_cTable);
                 where PUNUMKEY=this.w_GRUKEY and PUCODGRP=this.w_GRUPPO;
                  into cursor _Curs_PAR__MRP
              endif
              if used('_Curs_PAR__MRP')
                select _Curs_PAR__MRP
                locate for 1=1
                do while not(eof())
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                  select _Curs_PAR__MRP
                  continue
                enddo
                use
              endif
            endif
              select _Curs_cpusrgrp
              continue
            enddo
            use
          endif
          if this.w_TROVATO<>"S"
            * --- Nessun record associato, nessuna inizializzazione della maschera richiesta
          endif
        endif
        if TYPE("This.Padre.w_TROVATO")="C"
          this.Padre.w_TROVATO = this.w_TROVATO
        endif
        if used("_CauAgg_")
          Use in _CauAgg_
        endif
      case this.pParam="D"
        this.oParentObject.w_PROFIN = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","PF",IIF(this.oParentObject.w_PROFIN="XX","PF","XX")))
        this.oParentObject.w_SEMLAV = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","SE",IIF(this.oParentObject.w_SEMLAV="XX","SE","XX")))
        this.oParentObject.w_MATPRI = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","MP",IIF(this.oParentObject.w_MATPRI="XX","MP","XX")))
        this.oParentObject.w_STIPART = iif(this.oParentObject.w_PROFIN="PF" and this.oParentObject.w_SEMLAV="SE" and this.oParentObject.w_MATPRI="MP","S","N")
      case this.pParam="K"
        this.oParentObject.w_PUPROFIN = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","PF",IIF(this.oParentObject.w_PUPROFIN="XX","PF","XX")))
        this.oParentObject.w_PUSEMLAV = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","SE",IIF(this.oParentObject.w_PUSEMLAV="XX","SE","XX")))
        this.oParentObject.w_PUMATPRI = IIF(this.oParentObject.w_FLSELDES="D","XX",IIF(this.oParentObject.w_FLSELDES="S","MP",IIF(this.oParentObject.w_PUMATPRI="XX","MP","XX")))
      case this.pParam="GSMR_BGP"
        * --- Lancia la pianificazione MRP
        this.Padre = this.oParentobject
        do case
          case type("this.Padre")="O"
            this.w_PARAM = alltrim(upper(this.Padre.Class))
          otherwise
            i_retcode = 'stop'
            return
        endcase
        GSMR_BPP(this.Padre, "CHECKINIT")
        do case
          case this.oParentObject.w_CHECKINIT = -1
            * --- Con l'MRP si aspetta il padre di livello ...
            ah_ErrorMsg("Attenzione:%0Per un corretto funzionamento dell'MRP � necessaria l'impostazione <padre di livello>%0per la gestione delle distinte basi varianti (vedere parametri distinte)%0%0Elaborazione sospesa","stop","")
          case this.oParentObject.w_CHECKINIT = 2
            ah_errormsg("Campo obbligatorio!",48)
          case this.oParentObject.w_CHECKINIT = 3
            ah_errormsg("Attenzione!%0Non � possibile eseguire l'elaborazione MRP con un criterio diverso da Aggregata nella modalit� di pianificazione (Pegging di secondo livello):%0elaborazione sospesa!",16)
          case this.oParentObject.w_CHECKINIT = 1
            ah_errormsg("Flag di blocco MRP attivato: elaborazione abortita!",16)
        endcase
        if this.oParentObject.w_CHECKINIT = 0
          this.w_UseProgBar = g_UseProgBar and vartype(this.Padre.w_PROGBAR)="O"
          if this.w_UseProgBar
            this.w_MESSRIP = this.oParentObject.w_MESSRIPIA="S"
            this.w_PEGGING = this.oParentObject.w_PEGGING2="S"
            this.w_bTITLE = ah_msgformat("Avanzamento parziale")
            LOCAL L_SETCURSOR
            L_SETCURSOR = SET("CURSOR")
            SET CURSOR OFF
            * --- Assegno alla variabile pubblica l'oggetto Padre chiamante
            i_PROGBAR = this.Padre
            * --- Imposta La Progress Bar
            do case
              case this.oParentObject.w_INTERN
                if this.oParentObject.w_MODELA="N"
                  GesProgBar("InitParam", this, 0, 100, this.w_bTITLE, 3 + IIF(this.oParentObject.w_GENPDA="S",2,0) + IIF(this.oParentObject.w_CHECKDATI="S",1,0))
                else
                  GesProgBar("InitParam", this, 0, 100, this.w_bTITLE, 5 + IIF(this.oParentObject.w_GENPDA="S",2,0) + IIF(this.oParentObject.w_CHECKDATI="S",1,0))
                endif
              case this.w_PEGGING
                GesProgBar("InitParam", this, 0, 0, this.w_bTITLE, 3)
              case this.w_MESSRIP
                GesProgBar("InitParam", this, 0, 0, this.w_bTITLE, 2)
            endcase
          endif
          GSMR_BGP(this.Padre)
          if TYPE("This.Padre.w_KEYRIF")="C"
            this.w_KEYRIF = this.Padre.w_KEYRIF
            * --- Alla fine svuoto sempre ART_TEMP
            * --- Delete from ART_TEMP
            i_nConn=i_TableProp[this.ART_TEMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_TEMP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"CAKEYRIF = "+cp_ToStrODBC(this.w_KEYRIF);
                     )
            else
              delete from (i_cTable) where;
                    CAKEYRIF = this.w_KEYRIF;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        endif
        if this.oParentObject.w_CHECKINIT <> 1
          * --- Sblocca l'elaborazione solo nel caso non abbia trovato l'elaborazione bloccata
          GSMR_BPP(this,"UNLOCKGEN")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Resetto la variabile di check
        this.oParentObject.w_CHECKINIT = 0
        if vartype(this.Padre.w_EDITBTN)="C"
          * --- Variabile che serve per abilitare/disabilitare il bottone interrompi
          this.Padre.w_EDITBTN = "S"
          this.Padre.mEnableControls()     
          wait wind "" timeout 0.001
        endif
        if this.w_UseProgBar
          this.Padre.w_PROGBAR.ResetPBValue()     
          L_SETCURSOR = "SET CURSOR " + L_SETCURSOR 
 &L_SETCURSOR
          GesProgBar("Q", this)
        endif
    endcase
    if this.pParam="BLANK"
      if Upper(Alltrim(This.oParentObject.Class))="TGSCO_KGP"
        vq_exec("QUERY\GSVEFKGF", this, this.oParentObject.w_CursMaga)
        select 1 as xchk, * From (this.oParentObject.w_CursMaga) into cursor (this.oParentObject.w_CursMaga)
        vq_exec("QUERY\GSVEGKGF", this, this.oParentObject.w_CursGrpMaga)
        select 1 as xchk, * From (this.oParentObject.w_CursGrpMaga) into cursor (this.oParentObject.w_CursGrpMaga)
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJPARENT = this.oparentobject
    this.w_obj = this.w_OBJPARENT.GetCtrl( Alltrim(appo1) )
    if VarType( this.w_OBJ ) = "O"
      * --- Se controllo con un link invoco il link Full altrimenti eseguo la Valid
      *     (se eseguissi la valid per controlli con link eseguirebbe il Link con parametro Part
      *     e quindi eseguendo una Like sul database)
      if Not Empty( this.w_OBJ.clinkFile )
        * --- Se vi � un oggetto lancio la link ad esso legata con parametro Full.
        *     Per farlo utilizzo una Macro costruendo il nome della procedura ricercando 
        *     il secondo "_" a partire dalla fine (Es. _2_154 per MVCODMAG_2_154).
        *     Quindi invoco Link_2_154 dell'oggetto maschera/anagrafica..
        *     DI seguito invoco la changed Es w_MVQTAMOV Changed..
         
 L_Macro="This.w_OBJPARENT.link"+Right( this.w_OBJ.Name , LEN( this.w_OBJ.Name ) - RAT("_", this.w_OBJ.Name ,2)+1)+"('Full')" 
 &L_Macro 
 L_Macro="This.w_OBJPARENT.NotifyEvent('"+Alltrim( appo )+" Changed')" 
 &L_Macro
        * --- Aggiorno i calcoli derivati dalla modifica della variabile
        this.w_OBJPARENT.mCalc(.T.)     
        * --- Aggiorno le o_xxx
        this.w_OBJPARENT.SaveDependsOn()     
      else
        * --- Non � un campo con un link, valorizzo il controllo con il valore ed eseguo
        *     la valid.
        *     SetControlsValue valorizza tutti i controlli, occorre utilizzarlo per gestire
        *     correttamente anche i controlli di tipo Combo/Radio Group
        this.w_obj.bUpd = .t.
        this.w_OBJPARENT.SetControlsValue()     
        this.w_obj.Valid()     
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trovo almeno un record
    this.w_TROVATO = "S"
    this.oParentObject.w_CRIELA = NVL(_Curs_PAR__MRP.PUCRIELA," ")
    this.Padre.SetControlsValue()     
    this.Padre.SaveDependsOn()     
    if TYPE("This.Padre.w_TROVATO")="C"
      this.Padre.w_TROVATO = this.w_TROVATO
    endif
    this.Padre.NotifyEvent("AGZMAG")     
    this.oParentObject.w_CODINI = NVL(_Curs_PAR__MRP.PUCODINI,space(41))
    if ! empty(this.oParentObject.w_CODINI)
      appo=this.oParentObject.w_CODINI 
 appo1="w_CODINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_CODFIN = NVL(_Curs_PAR__MRP.PUCODFIN,space(41))
    if ! empty(this.oParentObject.w_CODFIN)
      appo=this.oParentObject.w_CODFIN 
 appo1="w_CODFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESFIN = space(40)
    endif
    this.oParentObject.w_FAMAINI = NVL(_Curs_PAR__MRP.PUFAMAIN,space(5))
    if ! empty(this.oParentObject.w_FAMAINI)
      appo=this.oParentObject.w_FAMAINI 
 appo1="w_FAMAINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_FAMAFIN = NVL(_Curs_PAR__MRP.PUFAMAFI,space(5))
    if ! empty(this.oParentObject.w_FAMAFIN)
      appo=this.oParentObject.w_FAMAFIN 
 appo1="w_FAMAFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESFAMAF = space(35)
    endif
    this.oParentObject.w_GRUINI = NVL(_Curs_PAR__MRP.PUGRUINI,space(5))
    if ! empty(this.oParentObject.w_GRUINI)
      appo=this.oParentObject.w_GRUINI 
 appo1="w_GRUINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_GRUFIN = NVL(_Curs_PAR__MRP.PUGRUFIN,space(5))
    if ! empty(this.oParentObject.w_GRUFIN)
      appo=this.oParentObject.w_GRUFIN 
 appo1="w_GRUFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESGRUF = space(35)
    endif
    this.oParentObject.w_CATINI = NVL(_Curs_PAR__MRP.PUCATINI,space(5))
    if ! empty(this.oParentObject.w_CATINI)
      appo=this.oParentObject.w_CATINI 
 appo1="w_CATINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_CATFIN = NVL(_Curs_PAR__MRP.PUCATFIN,space(5))
    if ! empty(this.oParentObject.w_CATFIN)
      appo=this.oParentObject.w_CATFIN 
 appo1="w_CATFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESCATF = space(35)
    endif
    this.oParentObject.w_MAGINI = NVL(_Curs_PAR__MRP.PUMAGINI,space(5))
    if ! empty(this.oParentObject.w_MAGINI)
      appo=this.oParentObject.w_MAGINI 
 appo1="w_MAGINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_MAGFIN = NVL(_Curs_PAR__MRP.PUMAGFIN,space(5))
    if ! empty(this.oParentObject.w_MAGFIN)
      appo=this.oParentObject.w_MAGFIN 
 appo1="w_MAGFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMAGF = space(30)
    endif
    this.oParentObject.w_MARINI = NVL(_Curs_PAR__MRP.PUMARINI,space(5))
    if ! empty(this.oParentObject.w_MARINI)
      appo=this.oParentObject.w_MARINI 
 appo1="w_MARINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_MARFIN = NVL(_Curs_PAR__MRP.PUMARFIN,space(5))
    if ! empty(this.oParentObject.w_MARFIN)
      appo=this.oParentObject.w_MARFIN 
 appo1="w_MARFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMARF = space(35)
    endif
    this.oParentObject.w_INICLI = NVL(_Curs_PAR__MRP.PUINICLI,space(15))
    if ! empty(this.oParentObject.w_INICLI)
      appo=this.oParentObject.w_INICLI 
 appo1="w_INICLI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_FINCLI = NVL(_Curs_PAR__MRP.PUFINCLI,space(15))
    if ! empty(this.oParentObject.w_FINCLI)
      appo=this.oParentObject.w_FINCLI 
 appo1="w_FINCLI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESCLIF = space(40)
    endif
    this.oParentObject.w_COMINI = NVL(_Curs_PAR__MRP.PUCOMINI,space(15))
    if ! empty(this.oParentObject.w_COMINI)
      appo=this.oParentObject.w_COMINI 
 appo1="w_COMINI"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.oParentObject.w_COMFIN = NVL(_Curs_PAR__MRP.PUCOMFIN,space(15))
    if ! empty(this.oParentObject.w_COMFIN)
      appo=this.oParentObject.w_COMFIN 
 appo1="w_COMFIN"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESCOMF = space(30)
    endif
    this.oParentObject.w_MAGFOR = NVL(_Curs_PAR__MRP.PUMAGFOR,space(5))
    if ! empty(this.oParentObject.w_MAGFOR)
      appo=this.oParentObject.w_MAGFOR 
 appo1="w_MAGFOR"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMFR = space(30)
    endif
    this.oParentObject.w_MAGFOC = NVL(_Curs_PAR__MRP.PUMAGFOC,space(5))
    if ! empty(this.oParentObject.w_MAGFOC)
      appo=this.oParentObject.w_MAGFOC 
 appo1="w_MAGFOC"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMOC = space(30)
    endif
    this.oParentObject.w_MAGFOL = NVL(_Curs_PAR__MRP.PUMAGFOL,space(5))
    if ! empty(this.oParentObject.w_MAGFOL)
      appo=this.oParentObject.w_MAGFOL 
 appo1="w_MAGFOL"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMOL = space(30)
    endif
    this.oParentObject.w_MAGFOA = NVL(_Curs_PAR__MRP.PUMAGFOA,space(5))
    if ! empty(this.oParentObject.w_MAGFOA)
      appo=this.oParentObject.w_MAGFOA 
 appo1="w_MAGFOA"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Azzero la descrizione valorizzata dalla calculate sul codice precedente
      this.oParentObject.w_DESMOA = space(30)
    endif
    this.oParentObject.w_COMODL = NVL(_Curs_PAR__MRP.PUCOMODL,"T")
    this.oParentObject.w_LLCINI = _Curs_PAR__MRP.PULLCINI
    this.oParentObject.w_LLCFIN = _Curs_PAR__MRP.PULLCFIN
    this.oParentObject.w_ELACAT = NVL(_Curs_PAR__MRP.PUELACAT," ")
    this.oParentObject.w_ELABID = NVL(_Curs_PAR__MRP.PUELABID," ")
    this.oParentObject.w_NOTMRP = NVL(_Curs_PAR__MRP.PUNOTEEL," ")
    this.oParentObject.w_DISMAG = NVL(_Curs_PAR__MRP.PUDISMAG," ")
    this.oParentObject.w_GIANEG = NVL(_Curs_PAR__MRP.PUGIANEG," ")
    this.oParentObject.w_ORDIPROD = NVL(_Curs_PAR__MRP.PUORDPRD," ")
    this.oParentObject.w_IMPEPROD = NVL(_Curs_PAR__MRP.PUIMPPRD," ")
    this.oParentObject.w_PERPIA = NVL(_Curs_PAR__MRP.PUPERPIA," ")
    this.oParentObject.w_PIAPUN = NVL(_Curs_PAR__MRP.PUPIAPUN," ")
    this.oParentObject.w_SELEIMPE = NVL(_Curs_PAR__MRP.PUSELIMP," ")
    this.oParentObject.w_ORDMPS = NVL(_Curs_PAR__MRP.PUORDMPS," ")
    this.oParentObject.w_STAORD = NVL(_Curs_PAR__MRP.PUSTAORD," ")
    if type("this.padre.w_DELORD")="C"
      this.oParentObject.w_DELORD = NVL(_Curs_PAR__MRP.PUDELORD," ")
    endif
    this.w_DATAPPO = _Curs_PAR__MRP.PUINIELA
    this.oParentObject.w_INIELA = iif(type("this.oParentObject.w_INIELA")<>type("this.w_DATAPPO") and type("this.oParentObject.w_INIELA")="D"and type("this.w_DATAPPO")="T",ttod(this.w_DATAPPO),this.w_DATAPPO)
    this.w_DATAPPO = _Curs_PAR__MRP.PUFINELA
    this.oParentObject.w_FINELA = iif(type("this.oParentObject.w_FINELA")<>type("this.w_DATAPPO") and type("this.oParentObject.w_FINELA")="D"and type("this.w_DATAPPO")="T",ttod(this.w_DATAPPO),this.w_DATAPPO)
    this.oParentObject.w_ORIODL = NVL(_Curs_PAR__MRP.PUORIODL,"T")
    this.oParentObject.w_PROFIN = NVL(_Curs_PAR__MRP.PUPROFIN,"PF")
    this.oParentObject.w_SEMLAV = NVL(_Curs_PAR__MRP.PUSEMLAV,"SE")
    this.oParentObject.w_MATPRI = NVL(_Curs_PAR__MRP.PUMATPRI,"MP")
    this.oParentObject.w_STIPART = iif(((empty(this.oParentObject.w_PROFIN)or this.oParentObject.w_PROFIN="PF") and (empty(this.oParentObject.w_SEMLAV)or this.oParentObject.w_SEMLAV="SE") and (empty(this.oParentObject.w_MATPRI)or this.oParentObject.w_MATPRI="MP")),"S","N")
    this.oParentObject.w_NUMINI = NVL(_Curs_PAR__MRP.PUNUMINI,1)
    this.oParentObject.w_NUMFIN = NVL(_Curs_PAR__MRP.PUNUMFIN,999999999999999)
    this.oParentObject.w_SERIE1 = NVL(_Curs_PAR__MRP.PUSERIE1,"  ")
    this.oParentObject.w_SERIE2 = NVL(_Curs_PAR__MRP.PUSERIE2,"  ")
    this.w_DATAPPO = _Curs_PAR__MRP.PUDOCINI
    this.oParentObject.w_DOCINI = iif(type("this.oParentObject.w_DOCINI")<>type("this.w_DATAPPO") and type("this.oParentObject.w_DOCINI")="D"and type("this.w_DATAPPO")="T",ttod(this.w_DATAPPO),NVL(this.w_DATAPPO,ctod("")))
    this.w_DATAPPO = _Curs_PAR__MRP.PUDOCFIN
    this.oParentObject.w_DOCFIN = iif(type("this.oParentObject.w_DOCFIN")<>type("this.w_DATAPPO") and type("this.oParentObject.w_DOCFIN")="D"and type("this.w_DATAPPO")="T",ttod(this.w_DATAPPO),NVL(this.w_DATAPPO,ctod("")))
    this.oParentObject.w_MRPLOG = NVL(_Curs_PAR__MRP.PUMRPLOG,"N")
    this.oParentObject.w_MODELA = NVL(_Curs_PAR__MRP.PUMODELA," ")
    this.oParentObject.w_CHECKDATI = NVL(_Curs_PAR__MRP.PUCHKDAT," ")
    this.oParentObject.w_MESSRIPIA = NVL(_Curs_PAR__MRP.PUMSGRIP," ")
    this.oParentObject.w_GENPDA = NVL(_Curs_PAR__MRP.PUGENPDA," ")
    if type("this.padre.w_ELAPDF")="C"
      this.oParentObject.w_ELAPDF = NVL(_Curs_PAR__MRP.PUELPDAF,"N")
    endif
    if type("this.padre.w_ELAPDS")="C"
      this.oParentObject.w_ELAPDS = NVL(_Curs_PAR__MRP.PUELPDAS,"N")
    endif
    if type("this.padre.w_PPFLAROB")="C"
      this.oParentObject.w_PPFLAROB = NVL(_Curs_PAR__MRP.PUFLAROB,"N")
    endif
    this.oParentObject.w_CRITFORN = NVL(_Curs_PAR__MRP.PUCRIFOR," ")
    this.oParentObject.w_GENODA = NVL(_Curs_PAR__MRP.PUGENODA," ")
    this.oParentObject.w_TIPPAR = NVL(_Curs_PAR__MRP.PUTIPPAR," ")
    this.oParentObject.w_MICOM = NVL(_Curs_PAR__MRP.PUMICOMP,"M")
    this.oParentObject.w_MIODL = NVL(_Curs_PAR__MRP.PUMRFODL,"M")
    this.oParentObject.w_MIOCL = NVL(_Curs_PAR__MRP.PUMRFOCL,"M")
    this.oParentObject.w_MIODA = NVL(_Curs_PAR__MRP.PUMRFODA,"M")
    this.w_TRTIPDOC = _Curs_PAR__MRP.PUTIPDOC
    this.NC = this.Padre.w_SZOOM.cCursor
    if ! empty(NVL(this.w_TRTIPDOC,""))
      Select(this.NC) 
 zap
      this.w_CONTA = 0
      this.w_OCCCHR = occurs(chr(13)+chr(10),this.w_TRTIPDOC)
      for this.w_CONTA=1 to this.w_OCCCHR
      this.w_TIPDOC = substr(this.w_TRTIPDOC,(52*(this.w_CONTA-1)+1),50)
      Select(this.NC) 
 m.tdtipdoc=substr(this.w_TIPDOC,1,5) 
 m.tddesdoc=substr(this.w_TIPDOC,6,35) 
 m.aggsaldi=substr(this.w_TIPDOC,41,9) 
 m.xchk=val(substr(this.w_TIPDOC,50,1)) 
 append blank 
 gather memvar
      * --- Aggiorno il cursore dello zoom
      endfor
      * --- Lista di tutte le causali
      vq_exec ("..\COLA\EXE\QUERY\GSCO_KGP", this, "_CauAgg_")
      Select _CauAgg_ 
 go top 
 scan
      Select (this.NC) 
 go top
      locate for tdtipdoc=_CauAgg_.tdtipdoc
      if ! found()
        * --- Se sono state aggiunte delle causali le aggiungo alla visualizzazione (di default deselezionate)
        Select _CauAgg_ 
 scatter memvar 
 m.xchk=0
        Select (this.NC) 
 append blank 
 gather memvar
      endif
      endscan
      Select (this.NC) 
 count for (xchk=0) to this.oParentObject.w_CAUSALI
      Select(this.NC) 
 go top
      this.NC = this.Padre.w_SZOOM.refresh()
    endif
    this.w_TRLISMAG = NVL(_Curs_PAR__MRP.PULISMAG, "")
    if ! Empty(this.w_TRLISMAG)
      if TYPE("This.Padre.w_LISTMAGA")="C"
        this.Padre.w_LISTMAGA = this.w_TRLISMAG
      endif
    endif
    this.Padre.SetControlsValue()     
    this.Padre.mEnableControls()     
    this.bupdateparentobject=.f.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR__MRP'
    this.cWorkTables[2]='cpusrgrp'
    this.cWorkTables[3]='ART_TEMP'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PAR__MRP')
      use in _Curs_PAR__MRP
    endif
    if used('_Curs_cpusrgrp')
      use in _Curs_cpusrgrp
    endif
    if used('_Curs_PAR__MRP')
      use in _Curs_PAR__MRP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
