* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_kcs                                                        *
*              Conferma ODP/ODR/ODF suggeriti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_152]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-15                                                      *
* Last revis.: 2015-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsdb_kcs",oParentObject))

* --- Class definition
define class tgsdb_kcs as StdForm
  Top    = 10
  Left   = 11

  * --- Standard Properties
  Width  = 758
  Height = 421+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-06-12"
  HelpContextID=99251305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  ODL_MAST_IDX = 0
  DISMBASE_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsdb_kcs"
  cComment = "Conferma ODP/ODR/ODF suggeriti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPGST = space(1)
  w_ODLINI = space(15)
  w_ODLFIN = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_SELEZI = space(1)
  w_TIPATT = space(1)
  w_TIPCON = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATFINE = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_SELODP = space(1)
  w_DISINI = space(20)
  w_DESDISI = space(40)
  w_DISFIN = space(20)
  w_DESDISF = space(40)
  w_MAGINI = space(5)
  w_DESMAGI = space(30)
  w_MAGFIN = space(5)
  w_DESMAGF = space(30)
  w_CODCOM = space(15)
  w_DESCOM = space(30)
  w_CODATT = space(15)
  w_DESATT = space(30)
  w_CODCON = space(15)
  w_DESFOR = space(40)
  w_MAGTER = space(5)
  w_DISMAG1 = space(1)
  w_FLWIP1 = space(1)
  w_DISMAG2 = space(1)
  w_FLWIP2 = space(1)
  w_TIPGES = space(1)
  w_SELODP = space(1)
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsdb_kcsPag1","gsdb_kcs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsdb_kcsPag2","gsdb_kcs",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco ordini")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oODLINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(2).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='DISMBASE'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='KEY_ARTI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSDB_BCS(this,"AG")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPGST=space(1)
      .w_ODLINI=space(15)
      .w_ODLFIN=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_TIPATT=space(1)
      .w_TIPCON=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATFINE=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_SELODP=space(1)
      .w_DISINI=space(20)
      .w_DESDISI=space(40)
      .w_DISFIN=space(20)
      .w_DESDISF=space(40)
      .w_MAGINI=space(5)
      .w_DESMAGI=space(30)
      .w_MAGFIN=space(5)
      .w_DESMAGF=space(30)
      .w_CODCOM=space(15)
      .w_DESCOM=space(30)
      .w_CODATT=space(15)
      .w_DESATT=space(30)
      .w_CODCON=space(15)
      .w_DESFOR=space(40)
      .w_MAGTER=space(5)
      .w_DISMAG1=space(1)
      .w_FLWIP1=space(1)
      .w_DISMAG2=space(1)
      .w_FLWIP2=space(1)
      .w_TIPGES=space(1)
      .w_SELODP=space(1)
        .w_TIPGST = this.oParentObject
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ODLINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ODLFIN))
          .link_1_3('Full')
        endif
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .w_SELEZI = "D"
      .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .w_TIPATT = 'A'
        .w_TIPCON = 'F'
          .DoRTCalc(8,12,.f.)
        .w_SELODP = "T"
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_DISINI))
          .link_1_19('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_DISFIN))
          .link_1_22('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_MAGINI))
          .link_1_28('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_MAGFIN))
          .link_1_31('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CODCOM))
          .link_1_34('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_CODATT))
          .link_1_37('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_CODCON))
          .link_1_39('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
          .DoRTCalc(27,32,.f.)
        .w_TIPGES = iif(this.oParentObject $ 'SCT', 'P', iif(this.oParentObject $ 'FRG', 'F','T'))
        .w_SELODP = "T"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsdb_kcs
    do case
      case this.w_TIPGST='S'
        this.cComment = ah_msgformat("Conferma ODP suggeriti")
      case this.w_TIPGST='F'
        this.cComment = ah_msgformat("Conferma ODR suggeriti")
      case this.w_TIPGST='H'
        this.cComment = ah_msgformat("Conferma ODF suggeriti")
      case this.w_TIPGST='C'
        this.cComment = ah_msgformat("Pianificazione ODP confermati")
      case this.w_TIPGST='R'
        this.cComment = ah_msgformat("Pianificazione ODR confermati")
      case this.w_TIPGST='I'
        this.cComment = ah_msgformat("Pianificazione ODF confermati")
      case this.w_TIPGST='T'
        this.cComment = ah_msgformat("Eliminazione ODP")
      case this.w_TIPGST='G'
        this.cComment = ah_msgformat("Eliminazione ODR")
      case this.w_TIPGST='N'
        this.cComment = ah_msgformat("Eliminazione ODF")
    endcase
    
    this.Caption = this.cComment
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomSel.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_3.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_34.enabled = this.oPgFrm.Page1.oPag.oCODCOM_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCODATT_1_37.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_39.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_39.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSELODP_1_18.visible=!this.oPgFrm.Page1.oPag.oSELODP_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oSELODP_1_55.visible=!this.oPgFrm.Page1.oPag.oSELODP_1_55.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_3.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ODLINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLINI))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLINI)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLINI) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLINI_1_2'),i_cWhere,'',"Elenco ODL/OCL",'GSDB_KCS.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLINI)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLINI = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLINI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLINI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ODLFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ODL_MAST_IDX,3]
    i_lTable = "ODL_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2], .t., this.ODL_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ODLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ODL_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OLCODODL like "+cp_ToStrODBC(trim(this.w_ODLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OLCODODL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OLCODODL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OLCODODL',trim(this.w_ODLFIN))
          select OLCODODL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OLCODODL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ODLFIN)==trim(_Link_.OLCODODL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ODLFIN) and !this.bDontReportError
            deferred_cp_zoom('ODL_MAST','*','OLCODODL',cp_AbsName(oSource.parent,'oODLFIN_1_3'),i_cWhere,'',"Elenco ODL/OCL",'GSDB_KCS.ODL_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                     +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',oSource.xKey(1))
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ODLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OLCODODL";
                   +" from "+i_cTable+" "+i_lTable+" where OLCODODL="+cp_ToStrODBC(this.w_ODLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OLCODODL',this.w_ODLFIN)
            select OLCODODL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ODLFIN = NVL(_Link_.OLCODODL,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ODLFIN = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ODLINI<=.w_ODLFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ODLFIN = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ODL_MAST_IDX,2])+'\'+cp_ToStr(_Link_.OLCODODL,1)
      cp_ShowWarn(i_cKey,this.ODL_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ODLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISINI
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISINI_1_19'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISINI = space(20)
      endif
      this.w_DESDISI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_DISINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_DISINI = space(20)
        this.w_DESDISI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DISFIN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DISFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DISFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DISFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DISFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DISFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDISFIN_1_22'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DISFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DISFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DISFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DISFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESDISF = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DISFIN = space(20)
      endif
      this.w_DESDISF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=COCHKAR(.w_DISFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
        endif
        this.w_DISFIN = space(20)
        this.w_DESDISF = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DISFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGINI
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGINI))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGINI_1_28'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGINI)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG1 = NVL(_Link_.MGDISMAG,space(1))
      this.w_FLWIP1 = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
      this.w_DISMAG1 = space(1)
      this.w_FLWIP1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MAGINI) OR (.w_DISMAG1='S' AND .w_FLWIP1<>'W')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.")
        endif
        this.w_MAGINI = space(5)
        this.w_DESMAGI = space(30)
        this.w_DISMAG1 = space(1)
        this.w_FLWIP1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAGFIN
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MAGFIN))
          select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMAGFIN_1_31'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MAGFIN)
            select MGCODMAG,MGDESMAG,MGDISMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG2 = NVL(_Link_.MGDISMAG,space(1))
      this.w_FLWIP2 = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
      this.w_DISMAG2 = space(1)
      this.w_FLWIP2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_MAGFIN) OR (.w_DISMAG2='S' AND .w_FLWIP2<>'W' AND .w_MAGINI <= .w_MAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.")
        endif
        this.w_MAGFIN = space(5)
        this.w_DESMAGF = space(30)
        this.w_DISMAG2 = space(1)
        this.w_FLWIP2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_34'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_37'),i_cWhere,'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_39'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'GSCO1KCS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANMAGTER,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_MAGTER = NVL(_Link_.ANMAGTER,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_MAGTER = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_MAGTER = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oODLINI_1_2.value==this.w_ODLINI)
      this.oPgFrm.Page1.oPag.oODLINI_1_2.value=this.w_ODLINI
    endif
    if not(this.oPgFrm.Page1.oPag.oODLFIN_1_3.value==this.w_ODLFIN)
      this.oPgFrm.Page1.oPag.oODLFIN_1_3.value=this.w_ODLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_2.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINE_1_12.value==this.w_DATFINE)
      this.oPgFrm.Page1.oPag.oDATFINE_1_12.value=this.w_DATFINE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_13.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_13.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oSELODP_1_18.RadioValue()==this.w_SELODP)
      this.oPgFrm.Page1.oPag.oSELODP_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDISINI_1_19.value==this.w_DISINI)
      this.oPgFrm.Page1.oPag.oDISINI_1_19.value=this.w_DISINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISI_1_21.value==this.w_DESDISI)
      this.oPgFrm.Page1.oPag.oDESDISI_1_21.value=this.w_DESDISI
    endif
    if not(this.oPgFrm.Page1.oPag.oDISFIN_1_22.value==this.w_DISFIN)
      this.oPgFrm.Page1.oPag.oDISFIN_1_22.value=this.w_DISFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDISF_1_24.value==this.w_DESDISF)
      this.oPgFrm.Page1.oPag.oDESDISF_1_24.value=this.w_DESDISF
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGINI_1_28.value==this.w_MAGINI)
      this.oPgFrm.Page1.oPag.oMAGINI_1_28.value=this.w_MAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_29.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_29.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGFIN_1_31.value==this.w_MAGFIN)
      this.oPgFrm.Page1.oPag.oMAGFIN_1_31.value=this.w_MAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_32.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_32.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_34.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_34.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_35.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_35.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_37.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_37.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_38.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_38.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_39.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_39.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_41.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_41.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oSELODP_1_55.RadioValue()==this.w_SELODP)
      this.oPgFrm.Page1.oPag.oSELODP_1_55.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ODLINI<=.w_ODLFIN or empty(.w_ODLFIN))  and not(empty(.w_ODLINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ODLINI<=.w_ODLFIN)  and not(empty(.w_ODLFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oODLFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(COCHKAR(.w_DISINI))  and not(empty(.w_DISINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISINI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(COCHKAR(.w_DISFIN))  and not(empty(.w_DISFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISFIN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice finale non valido, di provenienza esterna o non associato ad una distinta base")
          case   not(EMPTY(.w_MAGINI) OR (.w_DISMAG1='S' AND .w_FLWIP1<>'W'))  and not(empty(.w_MAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGINI_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.")
          case   not(EMPTY(.w_MAGFIN) OR (.w_DISMAG2='S' AND .w_FLWIP2<>'W' AND .w_MAGINI <= .w_MAGFIN))  and not(empty(.w_MAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMAGFIN_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.w_TIPGES<>'I')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsdb_kcsPag1 as StdContainer
  Width  = 754
  height = 421
  stdWidth  = 754
  stdheight = 421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oODLINI_1_2 as StdField with uid="TMVOMSBJOT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ODLINI", cQueryName = "ODLINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 129788698,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=164, Top=48, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLINI"

  func oODLINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ODL/OCL",'GSDB_KCS.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oODLFIN_1_3 as StdField with uid="KGBHZSHPWV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ODLFIN", cQueryName = "ODLFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice di fine selezione (vuoto=no selezione)",;
    HelpContextID = 51342106,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=164, Top=74, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ODL_MAST", oKey_1_1="OLCODODL", oKey_1_2="this.w_ODLFIN"

  proc oODLFIN_1_3.mDefault
    with this.Parent.oContained
      if empty(.w_ODLFIN)
        .w_ODLFIN = .w_ODLINI
      endif
    endwith
  endproc

  func oODLFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oODLFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oODLFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ODL_MAST','*','OLCODODL',cp_AbsName(this.parent,'oODLFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ODL/OCL",'GSDB_KCS.ODL_MAST_VZM',this.parent.oContained
  endproc

  add object oDATINI_1_10 as StdField with uid="BZZJIUDNZK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 129756874,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=411, Top=48

  add object oDATFIN_1_11 as StdField with uid="CRHOXZPXDK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 51310282,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=411, Top=74

  add object oDATFINE_1_12 as StdField with uid="FAXQJUJEXU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFINE", cQueryName = "DATFINE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione (vuota=nessuna selezione)",;
    HelpContextID = 217125174,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=615, Top=48

  add object oDATFIN1_1_13 as StdField with uid="QWARCSPHHS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione (vuota=nessuna selezione)",;
    HelpContextID = 51310282,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=615, Top=74


  add object oSELODP_1_18 as StdCombo with uid="GUYWJXKZKF",rtseq=13,rtrep=.f.,left=164,top=100,width=117,height=21;
    , ToolTipText = "Selezione Stato ordine";
    , HelpContextID = 22440410;
    , cFormVar="w_SELODP",RowSource=""+"Suggeriti,"+"Confermati,"+"Da pianificare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELODP_1_18.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"C",;
    iif(this.value =3,"D",;
    iif(this.value =4,"T",;
    space(1))))))
  endfunc
  func oSELODP_1_18.GetRadio()
    this.Parent.oContained.w_SELODP = this.RadioValue()
    return .t.
  endfunc

  func oSELODP_1_18.SetRadio()
    this.Parent.oContained.w_SELODP=trim(this.Parent.oContained.w_SELODP)
    this.value = ;
      iif(this.Parent.oContained.w_SELODP=="S",1,;
      iif(this.Parent.oContained.w_SELODP=="C",2,;
      iif(this.Parent.oContained.w_SELODP=="D",3,;
      iif(this.Parent.oContained.w_SELODP=="T",4,;
      0))))
  endfunc

  func oSELODP_1_18.mHide()
    with this.Parent.oContained
      return (!.w_TIPGST$"G-T")
    endwith
  endfunc

  add object oDISINI_1_19 as StdField with uid="COEWNKTBFQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DISINI", cQueryName = "DISINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di inizio selezione (vuota=no selezione)",;
    HelpContextID = 129758922,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=164, Top=172, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DISINI"

  func oDISINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISINI_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISINI_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISINI_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDISINI_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DISINI
     i_obj.ecpSave()
  endproc

  add object oDESDISI_1_21 as StdField with uid="QSEMNVVIFP",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESDISI", cQueryName = "DESDISI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235993802,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=172, InputMask=replicate('X',40)

  add object oDISFIN_1_22 as StdField with uid="ZYGJAJAVLU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DISFIN", cQueryName = "DISFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice finale non valido, di provenienza esterna o non associato ad una distinta base",;
    ToolTipText = "Codice articolo di fine selezione (vuota=no selezione)",;
    HelpContextID = 51312330,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=164, Top=198, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DISFIN"

  proc oDISFIN_1_22.mDefault
    with this.Parent.oContained
      if empty(.w_DISFIN)
        .w_DISFIN = .w_DISINI
      endif
    endwith
  endproc

  func oDISFIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oDISFIN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDISFIN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDISFIN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSCO_AOP.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDISFIN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DISFIN
     i_obj.ecpSave()
  endproc

  add object oDESDISF_1_24 as StdField with uid="BLVZKOKGVG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESDISF", cQueryName = "DESDISF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 32441654,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=198, InputMask=replicate('X',40)

  add object oMAGINI_1_28 as StdField with uid="WWAXPPIMPZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MAGINI", cQueryName = "MAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o non nettificabile o di tipo WIP o maggiore del magazzino finale.",;
    ToolTipText = "Codice magazzino di inizio selezione (vuoto=no selezione)",;
    HelpContextID = 129809978,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGINI"

  func oMAGINI_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGINI_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGINI_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGINI_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMAGINI_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGINI
     i_obj.ecpSave()
  endproc

  add object oDESMAGI_1_29 as StdField with uid="QONUNYBHAK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 91751734,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=259, InputMask=replicate('X',30)

  add object oMAGFIN_1_31 as StdField with uid="IPBLQAUROZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MAGFIN", cQueryName = "MAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice magazzino inesistente o non nettificabile o di tipo WIP o minore del magazzino iniziale.",;
    ToolTipText = "Codice magazzino di fine selezione (vuoto=no selezione)",;
    HelpContextID = 51363386,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=285, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MAGFIN"

  proc oMAGFIN_1_31.mDefault
    with this.Parent.oContained
      if empty(.w_MAGFIN)
        .w_MAGFIN = .w_MAGINI
      endif
    endwith
  endproc

  func oMAGFIN_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAGFIN_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAGFIN_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMAGFIN_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'GSCOPMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMAGFIN_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MAGFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAGF_1_32 as StdField with uid="EBXVFVWORM",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 91751734,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=285, InputMask=replicate('X',30)

  add object oCODCOM_1_34 as StdField with uid="GGQGKFAWGY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di selezione (vuota =no selezione)",;
    HelpContextID = 62054618,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=164, Top=311, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
   endif
  endfunc

  func oCODCOM_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_37('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oCODCOM_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_1_35 as StdField with uid="BCJHQPAVGJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 61995722,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=311, InputMask=replicate('X',30)

  add object oCODATT_1_37 as StdField with uid="BJCGFCEHYO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di selezione (vuota =no selezione)",;
    HelpContextID = 207937754,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=164, Top=337, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCOM) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oCODATT_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc

  add object oDESATT_1_38 as StdField with uid="TTNMSCEJIW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 207878858,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=337, InputMask=replicate('X',30)

  add object oCODCON_1_39 as StdField with uid="GRXGWWUYUW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
    ToolTipText = "Codice fornitore di selezione",;
    HelpContextID = 45277402,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=164, Top=363, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES<>'I')
    endwith
   endif
  endfunc

  func oCODCON_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'GSCO1KCS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_41 as StdField with uid="RJLIHNJXRO",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246348490,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=320, Top=363, InputMask=replicate('X',40)


  add object oBtn_1_42 as StdButton with uid="PPJFABIUST",left=652, top=375, width=48,height=45,;
    CpPicture="bmp\verifica.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza lista ordini che soddisfano le selezioni impostate";
    , HelpContextID = 60966141;
    , tabstop=.f., Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSDB_BCS(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="MJNXVZGNOX",left=703, top=375, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91933882;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSELODP_1_55 as StdCombo with uid="VDZFZIIYEC",rtseq=34,rtrep=.f.,left=164,top=100,width=117,height=21;
    , ToolTipText = "Selezione Stato ordine";
    , HelpContextID = 22440410;
    , cFormVar="w_SELODP",RowSource=""+"Suggeriti,"+"Confermati,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELODP_1_55.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"C",;
    iif(this.value =3,"T",;
    space(1)))))
  endfunc
  func oSELODP_1_55.GetRadio()
    this.Parent.oContained.w_SELODP = this.RadioValue()
    return .t.
  endfunc

  func oSELODP_1_55.SetRadio()
    this.Parent.oContained.w_SELODP=trim(this.Parent.oContained.w_SELODP)
    this.value = ;
      iif(this.Parent.oContained.w_SELODP=="S",1,;
      iif(this.Parent.oContained.w_SELODP=="C",2,;
      iif(this.Parent.oContained.w_SELODP=="T",3,;
      0)))
  endfunc

  func oSELODP_1_55.mHide()
    with this.Parent.oContained
      return (.w_TIPGST<>"N")
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="WVAVYFBMFI",Visible=.t., Left=57, Top=48,;
    Alignment=1, Width=105, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="QMCNFQOCKF",Visible=.t., Left=57, Top=74,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BISAHFOFSR",Visible=.t., Left=294, Top=48,;
    Alignment=1, Width=113, Height=15,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="MEPBCWUJDL",Visible=.t., Left=294, Top=74,;
    Alignment=1, Width=113, Height=15,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="EVNJJSWPZN",Visible=.t., Left=8, Top=17,;
    Alignment=0, Width=236, Height=18,;
    Caption="Selezioni codice e data inizio ordini"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=56, Top=172,;
    Alignment=1, Width=106, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=57, Top=198,;
    Alignment=1, Width=105, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WHGWHEWFXM",Visible=.t., Left=8, Top=141,;
    Alignment=0, Width=109, Height=18,;
    Caption="Selezione articoli"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=34, Top=259,;
    Alignment=1, Width=128, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=34, Top=285,;
    Alignment=1, Width=128, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=34, Top=313,;
    Alignment=1, Width=128, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="MSVOMVFWHL",Visible=.t., Left=34, Top=340,;
    Alignment=1, Width=128, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=34, Top=366,;
    Alignment=1, Width=128, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KMGQZVPEPN",Visible=.t., Left=8, Top=228,;
    Alignment=0, Width=126, Height=18,;
    Caption="Ulteriori selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DTJYHAGEOT",Visible=.t., Left=496, Top=48,;
    Alignment=1, Width=116, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="LKUZJNOGIO",Visible=.t., Left=496, Top=74,;
    Alignment=1, Width=116, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="OKKRCHZNDZ",Visible=.t., Left=78, Top=102,;
    Alignment=1, Width=84, Height=18,;
    Caption="Stato ordine:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (!.w_TIPGST$"N-G-T")
    endwith
  endfunc

  add object oBox_1_17 as StdBox with uid="FCEXGVXXNY",left=7, top=37, width=730,height=2

  add object oBox_1_26 as StdBox with uid="AHKBTGXZEK",left=7, top=160, width=730,height=2

  add object oBox_1_50 as StdBox with uid="SQPLBEFLBX",left=7, top=247, width=730,height=2
enddefine
define class tgsdb_kcsPag2 as StdContainer
  Width  = 754
  height = 421
  stdWidth  = 754
  stdheight = 421
  resizeXpos=504
  resizeYpos=190
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSel as cp_szoombox with uid="AEPLHZWOQV",left=0, top=3, width=754,height=354,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ODL_MAST",cZoomFile="GSDB_ZCS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "Interroga",;
    nPag=2;
    , HelpContextID = 78746342

  add object oSELEZI_2_2 as StdRadio with uid="KCMBJLDROQ",rtseq=5,rtrep=.f.,left=6, top=363, width=136,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 117467610
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 117467610
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_2.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_2_2.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_2.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oObj_2_3 as cp_runprogram with uid="BHANWDYKJR",left=4, top=432, width=219,height=22,;
    caption='GSCO_BCS',;
   bGlobalFont=.t.,;
    prg="GSDB_BCS('SS')",;
    cEvent = "w_SELEZI Changed",;
    nPag=2;
    , HelpContextID = 39392185


  add object oObj_2_4 as cp_runprogram with uid="EBVGCFPGTX",left=230, top=432, width=203,height=22,;
    caption='GSDB_BCS',;
   bGlobalFont=.t.,;
    prg="GSDB_BCS('INTERROGA')",;
    cEvent = "ActivatePage 2",;
    nPag=2;
    , HelpContextID = 38544313


  add object oBtn_2_5 as StdButton with uid="WQVAJMKWHR",left=653, top=360, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire gli aggiornamenti richiesti";
    , HelpContextID = 99222554;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        GSDB_BCS(this.Parent.oContained,"AG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_6 as StdButton with uid="IKSWHZXFAE",left=703, top=360, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91933882;
    , tabstop=.f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsdb_kcs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
