* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsmr_bpe                                                        *
*              Pegging 2 livello                                               *
*                                                                              *
*      Author: ZUCCHETTI TAM SPA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-09                                                      *
* Last revis.: 2018-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsmr_bpe",oParentObject,m.w_PARAM)
return(i_retval)

define class tgsmr_bpe as StdBatch
  * --- Local variables
  w_PARAM = space(10)
  w_PUNPAD = .NULL.
  w_CNT = 0
  DATIODL = .f.
  DATIORD = .f.
  DATIPPLI = .f.
  w_LVLKEY = space(240)
  w_nNODO = 0
  w_CI = 0
  w_CJ = 0
  w_ODLORD = space(15)
  w_LIVELLO = 0
  w_NODITV = .NULL.
  w_SUGG = space(150)
  w_PIAN = space(150)
  w_LANC = space(150)
  w_ORD = space(150)
  w_MAG = space(150)
  w_SCD = space(150)
  Albero = .NULL.
  w_cCursor = space(20)
  w_PROG = .NULL.
  w_PAR = space(1)
  w_MPS = space(1)
  w_NODITV = .NULL.
  w_LDettTec = space(1)
  w_PPCENCOS = space(15)
  w_PARAM = space(1)
  w_PARAM1 = space(1)
  w_PARAM2 = space(1)
  w_SELEZI = space(1)
  ChiaveTV = space(150)
  w_nNODO = 0
  w_NODITV = .NULL.
  ODLCOD = space(15)
  RIGODL = 0
  ODLKART = space(41)
  ODLART = space(20)
  ODLARTD = space(40)
  ODLUNMIS = space(3)
  QTAABB = 0
  TmpC = space(100)
  w_EXPANDED = space(1)
  w_STAMPFAS = space(1)
  w_STAMPODL = space(1)
  w_RIFORD = space(10)
  w_RIFORI = 0
  w_Cursor = space(10)
  w_ODLORI = space(15)
  w_RIGAORD = 0
  * --- WorkFile variables
  ART_PROD_idx=0
  ODL_MAST_idx=0
  PAR_PROD_idx=0
  ODL_DETT_idx=0
  ART_ICOL_idx=0
  DOC_DETT_idx=0
  TMPPEG_SELI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pegging di secondo livello (da GSMR_BGP, GSMR_KPE)
    * --- Parametro di elaborazione
    * --- Caller
    * --- Locali
    * --- Assegnamenti
    *     -----------------------
    * --- Puntatore alla Maschera
    this.w_PUNPAD = this.oParentObject
    * --- Tree-view
    this.Albero = this.oParentObject.w_TREEV
    * --- Cursore Tree-view
    this.w_cCursor = this.Albero.cCursor
    * --- Nodi Tree-view
    this.w_NODITV = this.Albero.oTree
    this.DATIODL = .T.
    this.DATIORD = .T.
    this.DATIPPLI = .T.
    * --- Gestione apertura multipla maschera gsmr_kpe (cambio il nome del cursore tutte le volte che apro la tree-view)
    if this.w_PARAM="Tree-FigPadre" or this.w_PARAM="Tree-PadreFig" or this.w_PARAM="Page2"
      if used(this.w_cCursor)
        use in (this.w_cCursor)
      endif
      this.w_cCursor = sys(2015)
      this.Albero.cCursor = this.w_cCursor
    endif
    private vnumdoc 
 vnumdoc="999999999999999"
    * --- Bitmap per tree-view
    this.w_SUGG = padr(".\bmp\odll.bmp",150)
    this.w_PIAN = padr(".\bmp\odll.bmp",150)
    this.w_LANC = padr(".\bmp\odll.bmp",150)
    this.w_ORD = padr(".\bmp\documenti.bmp",150)
    this.w_MAG = padr(".\bmp\magazzino.bmp",150)
    this.w_SCD = padr(".\bmp\odll.bmp",150)
    * --- Variabili utilizzate da GSDB_BCS
    this.w_STAMPFAS = "N"
    this.w_STAMPODL = "N"
    * --- -----------------------
    do case
      case this.w_PARAM="Tree-FigPadre"
        * --- Create temporary table TMPPEG_SELI
        i_nIdx=cp_AddTableDef('TMPPEG_SELI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_KJWTYEVQBN[1]
        indexes_KJWTYEVQBN[1]='PGSERODL,PGNUMRIGA'
        vq_exec('gsmrtbpe',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_KJWTYEVQBN,.f.)
        this.TMPPEG_SELI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Drop temporary table TMPPEG_SELI
        i_nIdx=cp_GetTableDefIdx('TMPPEG_SELI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPPEG_SELI')
        endif
      case this.w_PARAM="Tree-PadreFig"
        if not empty(nvl(this.oParentObject.w_TUTTI," "))or not empty(nvl(this.oParentObject.w_FATTIBILI," "))or not empty(nvl(this.oParentObject.w_NFATTIBILI," "))or not empty(nvl(this.oParentObject.w_PFATTIBILI," "))
          * --- Create temporary table TMPPEG_SELI
          i_nIdx=cp_AddTableDef('TMPPEG_SELI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_JXJIFFPMRG[1]
          indexes_JXJIFFPMRG[1]='PGSERODL,PGNUMRIGA'
          vq_exec('gsmrtbpe',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JXJIFFPMRG,.f.)
          this.TMPPEG_SELI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Drop temporary table TMPPEG_SELI
          i_nIdx=cp_GetTableDefIdx('TMPPEG_SELI')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPEG_SELI')
          endif
        else
          * --- Almeno uno dei flag deve essere abilitato
          ah_ErrorMsg("Non ci sono dati da visualizzare",48)
          this.w_NODITV.Nodes.Clear
        endif
      case this.w_PARAM="Done" and used(this.w_cCursor)
        use in (this.w_cCursor)
        * --- Rilascia anche popmenu
        DEACTIVATE POPUP MenuTV
        RELEASE POPUPS MenuTV EXTENDED
      case this.w_PARAM="Page2"
        if this.oParentObject.w_PEGG="FP"
          * --- Figlio-padre
          * --- Create temporary table TMPPEG_SELI
          i_nIdx=cp_AddTableDef('TMPPEG_SELI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_OYHWMSWWWL[1]
          indexes_OYHWMSWWWL[1]='PGSERODL,PGNUMRIGA'
          vq_exec('gsmrtbpe',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_OYHWMSWWWL,.f.)
          this.TMPPEG_SELI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Drop temporary table TMPPEG_SELI
          i_nIdx=cp_GetTableDefIdx('TMPPEG_SELI')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPEG_SELI')
          endif
        else
          * --- Padre-figlio
          * --- Create temporary table TMPPEG_SELI
          i_nIdx=cp_AddTableDef('TMPPEG_SELI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_FHUGABEYWD[1]
          indexes_FHUGABEYWD[1]='PGSERODL,PGNUMRIGA'
          vq_exec('gsmrtbpe',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FHUGABEYWD,.f.)
          this.TMPPEG_SELI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Drop temporary table TMPPEG_SELI
          i_nIdx=cp_GetTableDefIdx('TMPPEG_SELI')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPEG_SELI')
          endif
        endif
      case this.w_PARAM="PopUp" or this.w_PARAM="PopRid"
        Private Azione 
 store space(10) to Azione
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case Azione="Gestione" or Azione="Ordine"
            if this.oParentObject.w_ST="R" or (this.oParentObject.w_ST="L" and this.oParentObject.w_PROVE="L" and Azione="Ordine")
              * --- Dettaglio Documenti
              if this.oParentObject.w_ST="R"
                this.w_PROG = gsar_bzm(this,this.oParentObject.w_CODODL,-20)
              else
                * --- Read from DOC_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVSERIAL,CPROWNUM"+;
                    " from "+i_cTable+" DOC_DETT where ";
                        +"MVCODODL = "+cp_ToStrODBC(this.oParentObject.w_CODODL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVSERIAL,CPROWNUM;
                    from (i_cTable) where;
                        MVCODODL = this.oParentObject.w_CODODL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_RIFORD = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                  this.w_RIFORI = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_PROG = gsar_bzm(this,this.w_RIFORD,-20)
              endif
              * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
              if type("this.w_PROG")<>"O" or Isnull(this.w_PROG)
                i_retcode = 'stop'
                return
              endif
              if this.w_PROG.w_MVCLADOC="OR"
                nf = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3
                sf = this.w_PROG.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3.GotFocus()
              else
                nf = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
                sf = this.w_PROG.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.GotFocus()
              endif
              * --- Mi posiziono sulla riga
              Local L_cTrsName 
 L_cTrsName=this.w_PROG.cTrsname 
 Select (L_cTrsName) 
 Go Top
              if this.oParentObject.w_ST="R"
                Locate For CPROWNUM= this.oParentObject.w_ROWNUM
              else
                Locate For CPROWNUM= this.w_RIFORI
              endif
              if Found()
                this.w_PROG.oPgFrm.Page1.oPag.oBody.Refresh()     
                this.w_PROG.WorkFromTrs()     
                this.w_PROG.SaveDependsOn()     
                this.w_PROG.SetControlsValue()     
                this.w_PROG.mHideControls()     
                this.w_PROG.ChildrenChangeRow()     
                l_prova=this.w_prog.cfunction
                this.w_PROG.cFunction = "Edit"
                if this.w_PROG.w_MVCLADOC="OR"
                  this.w_PROG.oNewFocus = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3
                  this.w_PROG.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_3.GotFocus()     
                else
                  this.w_PROG.oNewFocus = this.w_prog.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
                  this.w_PROG.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.GotFocus()     
                endif
                this.w_prog.cfunction=l_prova
              endif
            else
              if this.oParentObject.w_ST<>"G"
                GSCO_BOR(this,this.oParentObject.w_cododl)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Giacenza di magazzino - Non devo aprire nessuna gestione.
              endif
            endif
          case Azione="Esplosione"
            this.Albero.ExpandAll(.T.)
            update (this.w_cCursor) set collapsed="+" where collapsed="-"
          case Azione="Implosione"
            this.Albero.ExpandAll(.F.)
            update (this.w_cCursor) set collapsed="-" where collapsed="+"
          case Azione="Lancia"
            this.TmpC = "Si � scelto di lanciare l'ODL n. %1%0Si � certi di voler proseguire l'operazione?%0"
            if ah_YesNo(this.TmpC,"",this.oParentObject.w_CODODL)
              this.w_SELEZI = "Z"
              vq_exec("..\COLA\EXE\QUERY\gsmr_kpe", this, "GSCOBGI")
              gsmr_blo(this,"AG")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if used("GSCOBGI")
              use in GSCOBGI
            endif
          case Azione="Chiudi"
            * --- Read from PAR_PROD
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_PROD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PPLOGTEC,PPCENCOS"+;
                " from "+i_cTable+" PAR_PROD where ";
                    +"PPCODICE = "+cp_ToStrODBC("PP");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PPLOGTEC,PPCENCOS;
                from (i_cTable) where;
                    PPCODICE = "PP";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LDettTec = NVL(cp_ToDate(_read_.PPLOGTEC),cp_NullValue(_read_.PPLOGTEC))
              this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.oParentObject.w_PROVE="I"
              * --- ODL
              this.w_PARAM = "K"
              this.TmpC = "Si � scelto di chiudere l'ODL n. %1%0Si � certi di voler proseguire l'operazione?%0"
            else
              if this.oParentObject.w_PROVE="E"
                * --- ODA
                this.w_PARAM = "A"
                this.TmpC = "Si � scelto di chiudere l'ODA n. %1%0Si � certi di voler proseguire l'operazione?%0"
              else
                * --- OCL
                this.w_PARAM = "U"
                this.TmpC = "Si � scelto di chiudere l'OCL n. %1%0Si � certi di voler proseguire l'operazione?%0"
              endif
            endif
            this.w_PARAM1 = " "
            this.w_PARAM2 = " "
            this.w_SELEZI = "Z"
            if ah_YesNo(this.TmpC,"",this.oParentObject.w_CODODL)
              this.w_Cursor = "GSCOSBCL"
              vq_exec("..\COLA\EXE\QUERY\gsmr_kpe", this, this.w_Cursor)
              if this.oParentObject.w_PROVE="I"
                gsco_bcl(this,"AG", "I")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                if this.oParentObject.w_PROVE="L"
                  gsco_bcl(this,"AG", "L")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  gsco_bcl(this,"AG", "E")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
            endif
            USE IN SELECT("GSCOSBCL")
          case Azione="DrillDown"
            * --- Memorizzo il nodo selezionato
            select (this.w_cCursor)
            this.w_nNODO = this.w_NODITV.selecteditem.key
            this.ChiaveTV = lvlkey
            this.w_RIGAORD = PERIGORD
            riga=recno() 
 update (this.w_cCursor) set collapsed="+" where recno()=riga 
 go top 
 scan
            if like(alltrim(this.ChiaveTV)+".*",lvlkey) 
              esci=.t. 
 exit
            else
               esci=.f.
            endif
            endscan 
 go riga
            if this.oParentObject.w_PEGG="PF"
              if ! esci
                * --- Tree-View Padre-Figlio
                vq_exec("..\COLA\EXE\QUERY\gsmr_bdd", this, "tmp")
                if reccount("tmp")>0
                  select tmp 
 go top 
 conta=0 
 scan 
 riga=recno() 
 conta=conta+1
                  if peserodl="Magazzino" or peserodl=" Magazz.:"
                    this.QTAABB = tmp.peqtaabb
                    this.ODLCOD = tmp.perifodl
                    this.RIGODL = tmp.perigord
                    if len(alltrim(this.ODLCOD))=15
                      * --- ODL - Seriale di 15 caratteri
                      * --- Read from ODL_DETT
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "OLCODICE,OLCODART"+;
                          " from "+i_cTable+" ODL_DETT where ";
                              +"OLCODODL = "+cp_ToStrODBC(this.ODLCOD);
                              +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          OLCODICE,OLCODART;
                          from (i_cTable) where;
                              OLCODODL = this.ODLCOD;
                              and CPROWNUM = this.RIGODL;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.ODLKART = NVL(cp_ToDate(_read_.OLCODICE),cp_NullValue(_read_.OLCODICE))
                        this.ODLART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      * --- Read from ART_ICOL
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "ARDESART,ARUNMIS1"+;
                          " from "+i_cTable+" ART_ICOL where ";
                              +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          ARDESART,ARUNMIS1;
                          from (i_cTable) where;
                              ARCODART = this.ODLART;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                        this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                    else
                      * --- Documento - Seriale di 10 caratteri
                      * --- Read from DOC_DETT
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "MVCODICE,MVCODART"+;
                          " from "+i_cTable+" DOC_DETT where ";
                              +"MVSERIAL = "+cp_ToStrODBC(this.ODLCOD);
                              +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          MVCODICE,MVCODART;
                          from (i_cTable) where;
                              MVSERIAL = this.ODLCOD;
                              and CPROWNUM = this.RIGODL;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.ODLKART = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
                        this.ODLART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      * --- Read from ART_ICOL
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "ARDESART,ARUNMIS1"+;
                          " from "+i_cTable+" ART_ICOL where ";
                              +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          ARDESART,ARUNMIS1;
                          from (i_cTable) where;
                              ARCODART = this.ODLART;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                        this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                    endif
                    go riga
                    replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
                  else
                    if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
                      replace peserodl with left(peserodl,10),oltstato1 with "R"
                    else
                      if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                        * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                        *     associata nella tabella del pegging ma non � valida, quindi la elimino
                        delete
                      endif
                    endif
                  endif
                  replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7))
                  endscan
                  update tmp set Descri=alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PESERODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PESERODL="Magazzino" or PESERODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1 $ "L-F",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
                  select tmp 
 go top 
 scan 
 scatter memvar 
 m.collapsed="-" 
 insert into (this.w_cCursor) from memvar 
 endscan
                  * --- Refresh della Tree-View (per aggiungere i nuovi nodi)
                  oldlocks=this.w_PUNPAD.lockscreen
                  this.w_PUNPAD.lockscreen = .t.
                  this.w_PUNPAD.NotifyEvent("updtreev")     
                  this.w_PUNPAD.lockscreen = oldlocks
                else
                  ah_msg("Nessun sottolivello da esplodere")
                endif
              else
                ah_msg("Sottolivello gi� esploso")
              endif
            else
              if ! esci
                * --- Tree-View Figlio-Padre
                vq_exec("..\COLA\EXE\QUERY\gsmr1bdd", this, "tmp")
                if reccount("tmp")>0
                  select tmp 
 go top 
 conta=0 
 scan 
 conta=conta+1
                  if perifodl="Magazzino" or perifodl=" Magazz.:"
                    this.QTAABB = tmp.peqtaabb
                    this.ODLKART = this.oparentobject.w_codric
                    this.ODLARTD = this.oparentobject.w_cadesar
                    this.ODLUNMIS = this.oparentobject.w_unimis
                    replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
                  else
                    if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
                      replace oltstato1 with "R"
                    else
                      if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                        * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                        *     ancora associata nella tabella del pegging ma non � + valida, quindi la elimino
                        delete
                      endif
                    endif
                  endif
                  replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7))
                  endscan
                  update tmp set Descri=alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PERIFODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PERIFODL="Magazzino" or PERIFODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1 $ "L-F",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
                  select tmp 
 go top 
 scan 
 scatter memvar 
 insert into (this.w_cCursor) from memvar 
 endscan
                  * --- Refresh della Tree-View (per aggiungere i nuovi nodi)
                  oldlocks=this.w_PUNPAD.lockscreen
                  this.w_PUNPAD.lockscreen = .t.
                  this.w_PUNPAD.NotifyEvent("updtreev")     
                  this.w_PUNPAD.lockscreen = oldlocks
                else
                  ah_msg("Nessun sottolivello da esplodere")
                endif
              else
                ah_msg("Sottolivello gi� esploso")
              endif
            endif
            * --- Espando il codice selezionato e mi ci posiziono.
            this.w_NODITV.Nodes(this.w_nNodo).Expanded = .t.
            this.w_NODITV.Nodes(this.w_nNODO).selected = .t.
            this.w_NODITV.parent.SetFocus()     
            if used("tmp")
              use in tmp
            endif
            * --- Espando tutte le righe aperte in precedenza
            select (this.w_cCursor)
            scan for collapsed="+"
            this.w_nNODO = "k"+alltrim(lvlkey)
            this.w_NODITV.Nodes(this.w_nNODO).Expanded = .t.
            endscan
          case Azione="DrillUp"
            * --- Memorizzo il nodo selezionato
            select (this.w_cCursor)
            this.w_nNODO = recno()
            this.ChiaveTV = lvlkey
            if this.oParentObject.w_PEGG="PF"
              * --- Tree-View Padre-Figlio
              select petiprif,peqtaabb,perifodl as peserodl,perigord,cprownum,oltstato,oltcodic,ardesart,peserodl as perifodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,lvlkey,tippro,; 
 cpbmpname,descri,tipdoc,"+" as collapsed from (this.w_cCursor) where this.w_nNODO=recno() into cursor tmpx
              * --- descri=descri- tutto quello che c'� prima di '=>'
              cur=wrcursor("tmpx") 
 update tmpx set Descri=right(descri,(len(descri)-(at("=>",descri)+2)))
              vq_exec("..\COLA\EXE\QUERY\gsmr1bdd", this, "tmp")
              if reccount("tmp")>0
                select tmp 
 go top 
 conta=0 
 scan 
 conta=conta+1
                if perifodl="Magazzino" or perifodl=" Magazz.:"
                  this.QTAABB = tmp.peqtaabb
                  this.ODLKART = this.oparentobject.w_codric
                  this.ODLARTD = this.oparentobject.w_cadesar
                  this.ODLUNMIS = this.oparentobject.w_unimis
                  replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
                else
                  if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
                    replace oltstato1 with "R"
                  else
                    if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                      * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                      *     associata nella tabella del pegging ma non � valida, quindi la elimino
                      delete
                    endif
                  endif
                endif
                replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7)),Descri with alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PERIFODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PESERODL="Magazzino" or PESERODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName with iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1 $ "L-F",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
                endscan
                if used(this.w_cCursor)
                  use in (this.w_cCursor)
                endif
                this.w_cCursor = sys(2015)
                select petiprif,peqtaabb,peserodl,perigord,cprownum,oltstato,oltcodic,ardesart,perifodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,left(alltrim(lvlkey)+space(240),240) as lvlkey1,; 
 tippro,cpbmpname,descri,tipdoc,"-" as collapsed from tmp union select * from tmpx into cursor (this.w_cCursor) order by 20 
 create cursor CHIAVE (lvlkey C(240)) 
 select left(alltrim(lvlkey)+space(240),240) as lvlkey from tmp union select lvlkey from tmpx into cursor CHIAVE1 
 select CHIAVE1 
 scan 
 scatter memvar 
 insert into CHIAVE from memvar 
 endscan 
 select petiprif,peqtaabb,peserodl,perigord,cprownum,oltstato,oltcodic,ardesart,perifodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,lvlkey,; 
 tippro,cpbmpname,descri,tipdoc,collapsed from (this.w_cCursor) inner join CHIAVE on lvlkey=lvlkey1 into cursor (this.w_cCursor) 
 index on lvlkey tag lvlkey collate "MACHINE"
                cur=wrcursor(this.w_cCursor)
                this.Albero.cCursor = this.w_cCursor
                this.oParentObject.w_PEGG = "FP"
                this.w_PUNPAD.opgfrm.page2.Caption = Ah_msgformat("Tree-View Figlio-Padre")
                this.w_PUNPAD.opgfrm.page1.opag.obox_1_93.visible = .f.
                this.w_PUNPAD.w_trflght1.visible = .f.
                this.w_PUNPAD.w_trflght2.visible = .f.
                this.w_PUNPAD.w_trflght3.visible = .f.
                this.oParentObject.w_TUTTI = "T"
                this.oParentObject.w_FATTIBILI = "F"
                this.oParentObject.w_NFATTIBILI = "N"
                this.oParentObject.w_PFATTIBILI = "P"
                if used("tmpx")
                  use in tmpx
                  if used("CHIAVE")
                    use in CHIAVE
                  endif
                  if used("CHIAVE1")
                    use in CHIAVE1
                  endif
                endif
                * --- Refresh della Tree-View (per aggiungere i nuovi nodi)
                oldlocks=this.w_PUNPAD.lockscreen
                this.w_PUNPAD.lockscreen = .t.
                this.w_PUNPAD.NotifyEvent("updtreev")     
                this.w_PUNPAD.lockscreen = oldlocks
                * --- Espando il codice selezionato e mi ci posiziono.
                this.w_nNODO = 1
                this.w_NODITV.Nodes(this.w_nNodo).Expanded = .t.
                this.w_NODITV.Nodes(this.w_nNODO).selected = .t.
                this.w_NODITV.parent.SetFocus()     
              else
                ah_msg("Nessun padre trovato")
                if used("tmpx")
                  use in tmpx
                endif
              endif
            else
              * --- Tree-View Figlio-Padre
              select petiprif,peqtaabb,peserodl as perifodl,perigord,cprownum,oltstato,oltcodic,ardesart,perifodl as peserodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,lvlkey,tippro,; 
 cpbmpname,descri,tipdoc,"+" as collapsed from (this.w_cCursor) where this.w_nNODO=recno() into cursor tmpx
              cur=wrcursor("tmpx") 
 update tmpx set Descri=right(descri,(len(descri)-(at("=>",descri)+2)))
              vq_exec("..\COLA\EXE\QUERY\gsmr_bdd", this, "tmp")
              if reccount("tmp")>0
                select tmp 
 go top 
 conta=0 
 scan 
 riga=recno() 
 conta=conta+1
                if peserodl="Magazzino" or peserodl=" Magazz.:"
                  this.QTAABB = tmp.peqtaabb
                  this.ODLCOD = tmp.perifodl
                  this.RIGODL = tmp.perigord
                  if len(alltrim(this.ODLCOD))=15
                    * --- ODL - Seriale di 15 caratteri
                    * --- Read from ODL_DETT
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ODL_DETT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "OLCODICE,OLCODART"+;
                        " from "+i_cTable+" ODL_DETT where ";
                            +"OLCODODL = "+cp_ToStrODBC(this.ODLCOD);
                            +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        OLCODICE,OLCODART;
                        from (i_cTable) where;
                            OLCODODL = this.ODLCOD;
                            and CPROWNUM = this.RIGODL;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.ODLKART = NVL(cp_ToDate(_read_.OLCODICE),cp_NullValue(_read_.OLCODICE))
                      this.ODLART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARDESART,ARUNMIS1"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARDESART,ARUNMIS1;
                        from (i_cTable) where;
                            ARCODART = this.ODLART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                      this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  else
                    * --- Documento - Seriale di 10 caratteri
                    * --- Read from DOC_DETT
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "MVCODICE,MVCODART"+;
                        " from "+i_cTable+" DOC_DETT where ";
                            +"MVSERIAL = "+cp_ToStrODBC(this.ODLCOD);
                            +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        MVCODICE,MVCODART;
                        from (i_cTable) where;
                            MVSERIAL = this.ODLCOD;
                            and CPROWNUM = this.RIGODL;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.ODLKART = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
                      this.ODLART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Read from ART_ICOL
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "ARDESART,ARUNMIS1"+;
                        " from "+i_cTable+" ART_ICOL where ";
                            +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        ARDESART,ARUNMIS1;
                        from (i_cTable) where;
                            ARCODART = this.ODLART;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                      this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  go riga
                  replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
                else
                  if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
                    replace peserodl with left(peserodl,10),oltstato1 with "R"
                  else
                    if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                      * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                      *     associata nella tabella del pegging ma non � valida, quindi la elimino
                      delete
                    endif
                  endif
                endif
                replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7)),Descri with alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PESERODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PERIFODL="Magazzino" or PERIFODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName with iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1 $ "L-F",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
                endscan
                if used(this.w_cCursor)
                  use in (this.w_cCursor)
                endif
                this.w_cCursor = sys(2015)
                select petiprif,peqtaabb,perifodl,perigord,cprownum,oltstato,oltcodic,ardesart,peserodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,left(alltrim(lvlkey)+space(240),240) as lvlkey1,; 
 tippro,cpbmpname,descri,tipdoc,"-" as collapsed from tmp union select * from tmpx into cursor (this.w_cCursor) order by 20 
 create cursor CHIAVE (lvlkey C(240)) 
 select left(alltrim(lvlkey)+space(240),240) as lvlkey from tmp union select lvlkey from tmpx into cursor CHIAVE1 
 select CHIAVE1 
 scan 
 scatter memvar 
 insert into CHIAVE from memvar 
 endscan 
 select petiprif,peqtaabb,perifodl,perigord,cprownum,oltstato,oltcodic,ardesart,peserodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,lvlkey,; 
 tippro,cpbmpname,descri,tipdoc,collapsed from (this.w_cCursor) inner join CHIAVE on lvlkey=lvlkey1 into cursor (this.w_cCursor) 
 index on lvlkey tag lvlkey collate "MACHINE"
                cur=wrcursor(this.w_cCursor)
                this.Albero.cCursor = this.w_cCursor
                if used("tmpx")
                  use in tmpx
                endif
                if used("CHIAVE")
                  use in CHIAVE
                endif
                if used("CHIAVE1")
                  use in CHIAVE1
                endif
                this.oParentObject.w_PEGG = "PF"
                this.w_PUNPAD.opgfrm.page2.Caption = Ah_msgformat("Tree-View Padre-Figlio")
                this.w_PUNPAD.opgfrm.page1.opag.obox_1_93.visible = .t.
                this.w_PUNPAD.w_trflght1.visible = .t.
                this.w_PUNPAD.w_trflght2.visible = .t.
                this.w_PUNPAD.w_trflght3.visible = .t.
                * --- Refresh della Tree-View (per aggiungere i nuovi nodi)
                oldlocks=this.w_PUNPAD.lockscreen
                this.w_PUNPAD.lockscreen = .t.
                this.w_PUNPAD.NotifyEvent("updtreev")     
                this.w_PUNPAD.lockscreen = oldlocks
                * --- Espando il codice selezionato e mi ci posiziono.
                this.w_nNODO = 1
                this.w_NODITV.Nodes(this.w_nNodo).Expanded = .t.
                this.w_NODITV.Nodes(this.w_nNODO).selected = .t.
                this.w_NODITV.parent.SetFocus()     
              else
                ah_msg("Nessun padre trovato")
                if used("tmpx")
                  use in tmpx
                endif
              endif
            endif
            if used("tmp")
              use in tmp
            endif
          case Azione="Esplodit"
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case Azione="Pianifica"
            this.TmpC = "Si � scelto di pianificare l'ODL n. %1%0Si � certi di voler proseguire l'operazione?%0"
            if ah_YesNo(this.TmpC,"",this.oParentObject.w_CODODL)
              this.w_SELEZI = "Z"
              vq_exec("..\COLA\EXE\QUERY\gsmr1kpe", this, "GSCOBCS")
              gsmr_blo(this,"PI")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if used("GSCOBCS")
              use in GSCOBCS
            endif
        endcase
      case this.w_PARAM="NOME"
        if this.oParentObject.w_PEGG="PF"
          this.w_PUNPAD.opgfrm.page2.Caption = Ah_msgformat("Tree-View Padre-Figlio")
          this.w_PUNPAD.opgfrm.page1.opag.obox_1_93.visible = .t.
          this.w_PUNPAD.w_trflght1.visible = .t.
          this.w_PUNPAD.w_trflght2.visible = .t.
          this.w_PUNPAD.w_trflght3.visible = .t.
        else
          this.w_PUNPAD.opgfrm.page2.Caption = Ah_msgformat("Tree-View Figlio-Padre")
          this.w_PUNPAD.opgfrm.page1.opag.obox_1_93.visible = .f.
          this.w_PUNPAD.w_trflght1.visible = .f.
          this.w_PUNPAD.w_trflght2.visible = .f.
          this.w_PUNPAD.w_trflght3.visible = .f.
          this.oParentObject.w_TUTTI = "T"
          this.oParentObject.w_FATTIBILI = "F"
          this.oParentObject.w_NFATTIBILI = "N"
          this.oParentObject.w_PFATTIBILI = "P"
        endif
      case this.w_PARAM="COLLNODE"
        * --- Implode il nodo
        select (this.w_cCursor)
        if not empty(nvl(this.Albero.nKeyCollapsed," "))
          if seek(substr(this.Albero.nKeyCollapsed,2))
            this.w_EXPANDED = collapsed
            if this.w_EXPANDED<>"-"
              select (this.w_cCursor) 
 replace collapsed with "-"
            endif
          endif
        endif
      case this.w_PARAM="EXPNODE"
        * --- Espande il nodo
        select (this.w_cCursor)
        if not empty(nvl(this.Albero.nKeyExpanded," "))
          if seek(substr(this.Albero.nKeyExpanded,2))
            this.w_EXPANDED = collapsed
            if this.w_EXPANDED<>"+"
              select (this.w_cCursor) 
 replace collapsed with "+"
            endif
          endif
        endif
      case this.w_PARAM="STAMPA"
        select * from (this.w_cCursor) order by 25 into cursor __tmp__
        l_pegg=this.oParentObject.w_PEGG
        CP_CHPRN("..\COLA\EXE\QUERY\GSMR_SPE","",this.oParentObject)
    endcase
    this.Albero = .NULL.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tree-Pad-Fig
    * --- Elabora l'albero per la visualizzazione padre figlio
    if this.oParentObject.w_PEGPLI="S"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.DATIPPLI = .F.
    endif
    if this.oParentObject.w_FLSELEZ $ "T-O"
      vq_exec("..\COLA\EXE\QUERY\gsmr1bpe", this, "ODL")
    endif
    if Used("ODL") and RecCount("ODL")>0
      select *, space(240) as lvlkey, " " as tippro, space(150) as CPBmpName, space(250) as Descri from ODL into cursor peggin 
 =wrcursor("peggin") 
 SCAN
      if livello=1
        this.w_CI = this.w_CI + 1
        this.w_CJ = 1
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)
      else
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)+"."+right("000000"+alltrim(str(this.w_CJ)),7)
        this.w_CJ = this.w_CJ + 1
      endif
      ENDSCAN
      this.w_LIVELLO = 2
      use in ODL 
 update peggin set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ PESERODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PESERODL="Magazzino" or PESERODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
    else
      * --- Cursore Vuoto
      this.DATIODL = .F.
      if used("ODL")
        use in ODL
      endif
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if ! this.DATIODL and ! this.DATIORD and ! this.DATIPPLI
      this.w_PUNPAD.oPgFrm.ActivePage = 1
      ah_ErrorMsg("Non ci sono dati da visualizzare",48)
      this.w_NODITV.Nodes.Clear
    else
      * --- Si deve usare un nuovo cursore per avere la corrispondenza tra il numero nodo della TV e il numero record del cursore
      if this.DATIODL
        select PETIPRIF,PEQTAABB,PERIFODL,PERIGORD, CPROWNUM, OLTSTATO,OLTCODIC,ARDESART,left(alltrim(PESERODL)+space(15),15); 
 as PESERODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,OLTQTOE1,nvl(ARUNMIS1,"") as ARUNMIS1,; 
 nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,CLADOC,LVLKEY,TIPPRO,CPBMPNAME,DESCRI,TIPDOC ; 
 from peggin order by lvlkey into cursor pegging1
      endif
      if this.DATIPPLI
        select PETIPRIF,PEQTAABB,left(alltrim(PERIFODL)+space(15),15) as PERIFODL,PERIGORD, CPROWNUM, OLTSTATO,OLTCODIC,ARDESART,; 
 left(alltrim(PESERODL)+space(15),15) as PESERODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,OLTQTOE1,; 
 nvl(ARUNMIS1,"") as ARUNMIS1,nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,CLADOC,LVLKEY,TIPPRO,; 
 CPBMPNAME,DESCRI,TIPDOC from peggin1 order by lvlkey into cursor pegging2
      endif
      if this.DATIORD
        select PETIPRIF,PEQTAABB,left(alltrim(PERIFODL)+space(15),15) as PERIFODL,PERIGORD, CPROWNUM, OLTSTATO,OLTCODIC,ARDESART,; 
 left(alltrim(PESERODL)+space(15),15) as PESERODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,OLTQTOE1,; 
 nvl(ARUNMIS1,"") as ARUNMIS1,nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,CLADOC,LVLKEY,TIPPRO,; 
 CPBMPNAME,DESCRI,TIPDOC from peggin2 order by lvlkey into cursor pegging3
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if used(this.w_cCursor)
        select (this.w_cCursor) 
 =wrcursor(this.w_cCursor)
        * --- Valuto Filtro Fattibili-Non Fattibili-Parzialmente Fattibili
        if nvl(this.oParentObject.w_TUTTI," ")="T" or (nvl(this.oParentObject.w_FATTIBILI," ")="F" and nvl(this.oParentObject.w_NFATTIBILI," ")="N" and nvl(this.oParentObject.w_PFATTIBILI," ")="P")
          * --- Voglio visualizzare tutto, non eseguo alcun ulteriore filtro e proseguo l'elaborazione
        else
          if nvl(this.oParentObject.w_FATTIBILI," ")="F" or nvl(this.oParentObject.w_PFATTIBILI," ")="P"
            select (this.w_cCursor) 
 go top
            scan for livello=1 
 odlcur=perifodl 
 test=.t.
            riga=recno() 
 scan for odlcur=perifodl and livello<>1
            if oltstato1<>"G"
              * --- C'� almeno un particolare non a magazzino, l'ordine non � fattibile
              test=.f.
            endif
            endscan
            if test
              select (this.w_cCursor) 
 scan for perifodl=odlcur
              replace FATTIBILE with "FFF" 
 endscan
            endif
            go riga
            endscan
          endif
          if nvl(this.oParentObject.w_NFATTIBILI," ")="N"
            select (this.w_cCursor) 
 go top
            scan for livello=1 and FATTIBILE<>"FFF" 
 odlcur=perifodl 
 test=.t.
            riga=recno() 
 scan for odlcur=perifodl and livello<>1
            if oltstato1="G"
              * --- C'� almeno un particolare a magazzino, l'ordine � almeno parzialmente fattibile
              test=.f.
            endif
            endscan
            if test
              select (this.w_cCursor) 
 scan for perifodl=odlcur
              replace FATTIBILE with "NNN" 
 endscan
            endif
            go riga
            endscan
          endif
          if nvl(this.oParentObject.w_PFATTIBILI," ")="P"
            select (this.w_cCursor) 
 go top
            scan for livello=1 and FATTIBILE="TTT" 
 odlcur=perifodl 
 test=.t.
            riga=recno() 
 scan for odlcur=perifodl and livello<>1
            if oltstato1="G"
              * --- C'� almeno un particolare a magazzino, l'ordine � parzialmente fattibile
              test=.f.
            endif
            endscan
            if ! test
              select (this.w_cCursor) 
 scan for perifodl=odlcur
              replace FATTIBILE with "PPP" 
 endscan
            endif
            go riga
            endscan
          endif
          select (this.w_cCursor)
          do case
            case nvl(this.oParentObject.w_FATTIBILI," ")="F" and nvl(this.oParentObject.w_NFATTIBILI," ")="N"
              delete for fattibile="TTT"
            case nvl(this.oParentObject.w_FATTIBILI," ")="F" and nvl(this.oParentObject.w_PFATTIBILI," ")="P"
              delete for fattibile $ "TTT-NNN"
            case nvl(this.oParentObject.w_NFATTIBILI," ")="N" and nvl(this.oParentObject.w_PFATTIBILI," ")="P"
              delete for fattibile $ "TTT-FFF"
            case nvl(this.oParentObject.w_NFATTIBILI," ")="N" and nvl(this.oParentObject.w_PFATTIBILI," ")<>"P" and nvl(this.oParentObject.w_FATTIBILI," ")<>"F"
              delete for fattibile<>"NNN"
            case nvl(this.oParentObject.w_NFATTIBILI," ")<>"N" and nvl(this.oParentObject.w_PFATTIBILI," ")="P" and nvl(this.oParentObject.w_FATTIBILI," ")<>"F"
              delete for fattibile<>"PPP"
            case nvl(this.oParentObject.w_NFATTIBILI," ")<>"N" and nvl(this.oParentObject.w_PFATTIBILI," ")<>"P" and nvl(this.oParentObject.w_FATTIBILI," ")="F"
              delete for fattibile<>"FFF"
          endcase
          select (this.w_cCursor) 
 pack
        endif
        * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
        select (this.w_cCursor) 
 index on lvlkey tag lvlkey collate "MACHINE"
      else
        this.w_NODITV.Nodes.Clear
      endif
      this.w_PUNPAD.NotifyEvent("updtreev")     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tree-Fig-Pad
    * --- Elabora l'albero per la visualizzazione Figlio padre
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_FLSELEZ $ "T-O"
      vq_exec("..\COLA\EXE\QUERY\gsmr_bpe", this, "ODL")
    endif
    if Used("ODL") and RecCount("ODL")>0
      select *, space(240) as lvlkey, " " as tippro, space(150) as CPBmpName, space(250) as Descri from ODL into cursor peggin 
 =wrcursor("peggin") 
 SCAN
      if livello=1
        this.w_CI = this.w_CI + 1
        this.w_CJ = 1
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)
      else
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)+"."+right("000000"+alltrim(str(this.w_CJ)),7)
        this.w_CJ = this.w_CJ + 1
      endif
      ENDSCAN
      this.w_LIVELLO = 2
      use in ODL 
 update peggin set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ PERIFODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PERIFODL="Magazzino" or PERIFODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
    else
      * --- Cursore Vuoto
      this.DATIODL = .F.
      if used("ODL")
        use in ODL
      endif
    endif
    if this.oParentObject.w_PEGPLI="S"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.DATIPPLI = .F.
    endif
    if ! this.DATIODL and ! this.DATIORD and ! this.DATIPPLI
      this.w_PUNPAD.oPgFrm.ActivePage = 1
      ah_ErrorMsg("Non ci sono dati da visualizzare",48)
      this.w_NODITV.Nodes.Clear
    else
      * --- Si deve usare un nuovo cursore per avere la corrispondenza tra il numero nodo della TV e il numero record del cursore
      if this.DATIODL
        select PETIPRIF,PEQTAABB,left(alltrim(PESERODL)+space(15),15) as PESERODL,PERIGORD, CPROWNUM, OLTSTATO,OLTCODIC,; 
 ARDESART,left(alltrim(PERIFODL)+space(15),15) as PERIFODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,; 
 OLTQTOE1,nvl(ARUNMIS1,"") as ARUNMIS1,nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,; 
 CLADOC,LVLKEY,TIPPRO,CPBMPNAME,DESCRI,TIPDOC from peggin order by lvlkey into cursor pegging1
      endif
      if this.DATIPPLI
        select PETIPRIF,PEQTAABB,left(alltrim(PESERODL)+space(15),15) as PESERODL,PERIGORD, CPROWNUM, OLTSTATO,OLTCODIC,; 
 ARDESART,left(alltrim(PERIFODL)+space(15),15) as PERIFODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,OLTQTOE1,; 
 nvl(ARUNMIS1,"") as ARUNMIS1,nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,CLADOC,LVLKEY,; 
 TIPPRO,CPBMPNAME,DESCRI,TIPDOC from peggin1 order by lvlkey into cursor pegging2
      endif
      if this.DATIORD
        select PETIPRIF,PEQTAABB,left(alltrim(PESERODL)+space(15),15) as PESERODL,val(PERIGORD) as PERIGORD, CPROWNUM, OLTSTATO,; 
 OLTCODIC,ARDESART,left(alltrim(PERIFODL)+space(15),15) as PERIFODL,OLTSTATO1,OLTDTRIC1,OLTCODIC1,ARDESART1,OLTQTOD1,; 
 OLTQTOE1,nvl(ARUNMIS1,"") as ARUNMIS1,nvl(OLTCOMAG,"") as OLTCOMAG,LIVELLO,NUMDOC,ALFDOC,PROVE,FLVEAC,CLADOC,; 
 LVLKEY,TIPPRO,CPBMPNAME,DESCRI,TIPDOC from peggin2 order by lvlkey into cursor pegging3
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Indicizzo il cursore e faccio il refresh della tree-view sulla maschera
      if used(this.w_cCursor)
        =wrcursor(this.w_cCursor) 
 index on lvlkey tag lvlkey collate "MACHINE"
      else
        this.w_NODITV.Nodes.Clear
      endif
      this.w_PUNPAD.NotifyEvent("updtreev")     
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora l'albero per la visualizzazione degli ordini provenienti dal pegging di 1� livello 
    if this.w_PARAM="Tree-FigPadre" or this.w_PARAM="Page2" and this.oParentObject.w_PEGG="FP"
      vq_exec("..\COLA\EXE\QUERY\gsmr_bpe4", this, "PEGPRILI")
    else
      vq_exec("..\COLA\EXE\QUERY\gsmr5bpe", this, "PEGPRILI")
    endif
    if Used("PEGPRILI") and RecCount("PEGPRILI")>0
      select *, space(240) as lvlkey, " " as tippro, space(150) as CPBmpName, space(250) as Descri from PEGPRILI into cursor peggin1 
 =wrcursor("peggin1") 
 SCAN
      if livello=1
        this.w_CI = this.w_CI + 1
        this.w_CJ = 1
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)
      else
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)+"."+right("000000"+alltrim(str(this.w_CJ)),7)
        this.w_CJ = this.w_CJ + 1
      endif
      ENDSCAN 
 use in PEGPRILI
      if this.w_PARAM="Tree-FigPadre" or this.w_PARAM="Page2" and this.oParentObject.w_PEGG="FP"
        update peggin1 set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ iif(livello=1,PESERODL,PERIFODL); 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PERIFODL="Magazzino" or PERIFODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC)+" ("+allt(ARDESART)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
      else
        update peggin1 set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ iif(livello=1,PERIFODL,PESERODL); 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 iif(PESERODL="Magazzino" or PESERODL=" Magazz.:","Art.:"," ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC)+" ("+allt(ARDESART)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
      endif
    else
      * --- Cursore Vuoto
      this.DATIPPLI = .F.
      if used("PEGPRILI")
        use in PEGPRILI
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora l'albero per la visualizzazione degli ordini provenienti dal pegging di 2� livello 
    if this.oParentObject.w_FLSELEZ $ "T-D"
      if this.w_PARAM="Tree-FigPadre" or this.w_PARAM="Page2" and this.oParentObject.w_PEGG="FP"
        vq_exec("..\COLA\EXE\QUERY\gsmr_bpe6", this, "ORD")
      else
        vq_exec("..\COLA\EXE\QUERY\gsmr8bpe", this, "ORD")
      endif
    endif
    if Used("ORD") and RecCount("ORD")>0
      select *, space(240) as lvlkey, " " as tippro, space(150) as CPBmpName, space(250) as Descri from ORD into cursor peggin2 
 =wrcursor("peggin2") 
 SCAN
      if livello=1
        this.w_CI = this.w_CI + 1
        this.w_CJ = 1
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)
      else
        replace lvlkey with right("000000"+alltrim(str(this.w_CI)),7)+"."+right("000000"+alltrim(str(this.w_CJ)),7)
        this.w_CJ = this.w_CJ + 1
      endif
      ENDSCAN 
 use in ORD
      if this.w_PARAM="Tree-FigPadre" or this.w_PARAM="Page2" and this.oParentObject.w_PEGG="FP"
        update peggin2 set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ iif(livello=1,PESERODL,nvl(PERIFODL,"")); 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 +iif(PERIFODL="Magazzino" or PERIFODL=" Magazz.:","Art:","("+allt(trans(OLTQTOD1,v_pq(12)))+; 
 " "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC)+" ("+allt(ARDESART)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
      else
        update peggin2 set Descri=iif(livello=1, "", alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => ")+ iif(livello=1,PERIFODL,nvl(PESERODL,"")); 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 +iif(PESERODL="Magazzino" or PESERODL=" Magazz.:","Art:","("+allt(trans(OLTQTOD1,v_pq(12)))+; 
 " "+nvl(ARUNMIS1,"")+") Art:")+allt(OLTCODIC)+" ("+allt(ARDESART)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
      endif
    else
      * --- Cursore vuoto
      this.DATIORD = .F.
      if used("ORD")
        use in ORD
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione cursore per tree-view
    * --- Se ho almeno due cursori devo ricalcolare il lvlkey se no la chiave � doppia
    activecursor=this.w_cCursor
    if this.DATIODL and this.DATIPPLI and this.DATIORD
      * --- lvlkey = 20esima colonna
      Select *,"-" as collapsed,"TTT" as fattibile from pegging1 union select *,"-" as collapsed,"TTT" as fattibile from pegging2 ; 
 union select *,"-" as collapsed,"TTT" as fattibile from pegging3 order by 20 into cursor &activecursor
    else
      if this.DATIODL and this.DATIPPLI
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging1 union select *,"-" as collapsed,"TTT" as fattibile from pegging2 order by 20 into cursor &activecursor
      endif
      if this.DATIODL and this.DATIORD
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging1 union select *,"-" as collapsed,"TTT" as fattibile from pegging3 order by 20 into cursor &activecursor
      endif
      if this.DATIPPLI and this.DATIORD
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging2 union select *,"-" as collapsed,"TTT" as fattibile from pegging3 order by 20 into cursor &activecursor
      endif
      if this.DATIODL and ! this.DATIPPLI and ! this.DATIORD
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging1 order by 20 into cursor &activecursor
      endif
      if ! this.DATIODL and this.DATIPPLI and ! this.DATIORD
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging2 order by 20 into cursor &activecursor
      endif
      if ! this.DATIODL and ! this.DATIPPLI and this.DATIORD
        * --- lvlkey = 20esima colonna
        Select *,"-" as collapsed,"TTT" as fattibile from pegging3 order by 20 into cursor &activecursor
      endif
    endif
    if used("pegging3")
      use in pegging3
    endif
    if used("pegging2")
      use in pegging2
    endif
    if used("pegging1")
      use in pegging1
    endif
    if used("peggin")
      use in peggin
    endif
    if used("peggin1")
      use in peggin1
    endif
    if used("peggin2")
      use in peggin2
    endif
    this.w_PunPad.oPgFrm.ActivePage = 2
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce SHORTCUT
    if !used(this.w_cCursor)
      i_retcode = 'stop'
      return
    endif
    DEFINE POPUP MenuTV from MRow()+1,MCol()+1 shortcut margin
    if this.w_PARAM="PopUp"
      if this.oParentObject.w_ST<>"G"
        DEFINE BAR 1 OF MenuTV prompt "Apertura Gestione"
      else
        * --- Per il Magazzino non c'� alcuna gestione da aprire...
        DEFINE BAR 1 OF MenuTV prompt "Apertura Gestione" skip
      endif
      DEFINE BAR 2 OF MenuTV prompt "Esplosione Albero"
      DEFINE BAR 3 OF MenuTV prompt "Implosione Albero"
      DEFINE BAR 4 OF MenuTV prompt "\-"
      if this.oParentObject.w_ST<>"G"
        DEFINE BAR 5 OF MenuTV prompt "Drill Down"
      else
        * --- Per il Magazzino non � possibile identificare alcun nodo figlio...
        DEFINE BAR 5 OF MenuTV prompt "Drill Down" skip
      endif
      if this.oParentObject.w_ST<>"G"
        DEFINE BAR 6 OF MenuTV prompt "Drill Up"
      else
        * --- Per il Magazzino non � possibile identificare alcun nodo padre...
        DEFINE BAR 6 OF MenuTV prompt "Drill Up" skip
      endif
      if this.oParentObject.w_PROVE $ "L-E-I" and this.oParentObject.w_ST="M"
        * --- La pianificazione pu� essere effettuata solo per ODL Suggeriti
        DEFINE BAR 7 OF MenuTV prompt "Pianifica ODL/OCL/ODA" 
      else
        DEFINE BAR 7 OF MenuTV prompt "Pianifica ODL/OCL/ODA" skip
      endif
      if this.oParentObject.w_PROVE="I" and this.oParentObject.w_ST="P"
        * --- Il lancio pu� essere effettuato solo per ODL Pianificati
        DEFINE BAR 8 OF MenuTV prompt "Lancia ODL" 
      else
        DEFINE BAR 8 OF MenuTV prompt "Lancia ODL" skip
      endif
      if (this.oParentObject.w_ST $ "L-P" and this.oParentObject.w_PROVE="I") or (this.oParentObject.w_PROVE $ "L-E" and this.oParentObject.w_ST="L")
        * --- La chiusura pu� essere effettuata solo su ODL Lanciati/Pianificati e OCL/ODA Lanciati
        DEFINE BAR 9 OF MenuTV prompt "Chiudi ODL/OCL/ODA"
      else
        DEFINE BAR 9 OF MenuTV prompt "Chiudi ODL/OCL/ODA" skip
      endif
      if this.oParentObject.w_ST<>"G"
        DEFINE BAR 10 OF MenuTV prompt "Esplodi tutto"
      else
        * --- Per il Magazzino non � possibile identificare alcun nodo figlio...
        DEFINE BAR 10 OF MenuTV prompt "Esplodi tutto" skip
      endif
      if this.oParentObject.w_ST="L" and this.oParentObject.w_PROVE="L"
        DEFINE BAR 11 OF MenuTV prompt "Apertura ordine a fornitore"
      else
        * --- Solo per gli ordini a fornitore legati a un OCL ordinato ha senso l'apertura del documento collegato
        DEFINE BAR 11 OF MenuTV prompt "Apertura ordine a fornitore" skip
      endif
    else
      * --- Pop-up Ridotto
      DEFINE BAR 1 OF MenuTV prompt "Esplosione Albero"
      DEFINE BAR 2 OF MenuTV prompt "Implosione Albero"
    endif
    if this.w_PARAM="PopUp"
      ON SELE BAR 1 OF MenuTV Azione="Gestione"
      ON SELE BAR 2 OF MenuTV Azione="Esplosione"
      ON SELE BAR 3 OF MenuTV Azione="Implosione"
      ON SELE BAR 5 OF MenuTV Azione="DrillDown"
      ON SELE BAR 6 OF MenuTV Azione="DrillUp"
      ON SELE BAR 7 OF MenuTV Azione="Pianifica"
      ON SELE BAR 8 OF MenuTV Azione="Lancia"
      ON SELE BAR 9 OF MenuTV Azione="Chiudi"
      ON SELE BAR 10 OF MenuTV Azione="Esplodit"
      ON SELE BAR 11 OF MenuTV Azione="Ordine"
    else
      * --- Pop-up Ridotto
      ON SELE BAR 1 OF MenuTV Azione="Esplosione"
      ON SELE BAR 2 OF MenuTV Azione="Implosione"
    endif
    ACTI POPUP MenuTV
    DEACTIVATE POPUP MenuTV
    RELEASE POPUPS MenuTV EXTENDED
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Memorizzo il nodo selezionato
    this.w_ODLORI = this.oParentObject.w_CODODL
    select (this.w_cCursor)
    this.w_nNODO = recno()
    this.w_LIVELLO = 1
    if this.oParentObject.w_PEGG="PF"
      * --- Tree-View Padre-Figlio
      select petiprif,peqtaabb,peserodl,perigord,cprownum,oltstato,oltcodic,ardesart,perifodl,oltstato1,oltdtric1,oltcodic1,ardesart1,; 
 oltqtod1,oltqtoe1,arunmis1,oltcomag,this.w_livello as livello,numdoc,alfdoc,prove,flveac,cladoc,left(alltrim("0000001")+space(240),240) as lvlkey,; 
 tippro,cpbmpname,descri,tipdoc,"+" as collapsed, "S" as PadreSup from (this.w_cCursor) where this.w_nNODO=recno() into cursor tmpx
      * --- descri=descri- tutto quello che c'� prima di '=>'
      cur=wrcursor("tmpx") 
 update tmpx set Descri=right(descri,(len(descri)-(at("=>",descri)+2)))
      select tmpx 
 go top 
 scan for PadreSup="S" 
 numriga=recno()
      * --- Faccio una scan sul cursore - Solo i Padri che sto analizzando (PadreSup='S')
      this.oParentObject.w_CODODL = iif(tmpx.OLTSTATO1="R" and tmpx.LIVELLO=1,tmpx.PERIFODL,tmpx.PESERODL)
      this.w_RIGAORD = tmpx.PERIGORD
      this.ChiaveTV = lvlkey
      * --- Esplosione ricorsiva
      if this.oParentObject.w_CODODL<>"Magazzino" and this.oParentObject.w_CODODL <> " Magazz.:"
        vq_exec("..\COLA\EXE\QUERY\gsmr_bdd", this, "tmp")
        if reccount("tmp")>0
          select tmp 
 go top 
 conta=0 
 scan 
 riga=recno() 
 conta=conta+1
          if peserodl="Magazzino" or peserodl=" Magazz.:"
            this.QTAABB = tmp.peqtaabb
            this.ODLCOD = tmp.perifodl
            this.RIGODL = tmp.perigord
            if len(alltrim(this.ODLCOD))=15
              * --- ODL - Seriale di 15 caratteri
              * --- Read from ODL_DETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ODL_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ODL_DETT_idx,2],.t.,this.ODL_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "OLCODICE,OLCODART"+;
                  " from "+i_cTable+" ODL_DETT where ";
                      +"OLCODODL = "+cp_ToStrODBC(this.ODLCOD);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  OLCODICE,OLCODART;
                  from (i_cTable) where;
                      OLCODODL = this.ODLCOD;
                      and CPROWNUM = this.RIGODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.ODLKART = NVL(cp_ToDate(_read_.OLCODICE),cp_NullValue(_read_.OLCODICE))
                this.ODLART = NVL(cp_ToDate(_read_.OLCODART),cp_NullValue(_read_.OLCODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARDESART,ARUNMIS1"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARDESART,ARUNMIS1;
                  from (i_cTable) where;
                      ARCODART = this.ODLART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Documento - Seriale di 10 caratteri
              * --- Read from DOC_DETT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVCODICE,MVCODART"+;
                  " from "+i_cTable+" DOC_DETT where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.ODLCOD);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.RIGODL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVCODICE,MVCODART;
                  from (i_cTable) where;
                      MVSERIAL = this.ODLCOD;
                      and CPROWNUM = this.RIGODL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.ODLKART = NVL(cp_ToDate(_read_.MVCODICE),cp_NullValue(_read_.MVCODICE))
                this.ODLART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from ART_ICOL
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ART_ICOL_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ARDESART,ARUNMIS1"+;
                  " from "+i_cTable+" ART_ICOL where ";
                      +"ARCODART = "+cp_ToStrODBC(this.ODLART);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ARDESART,ARUNMIS1;
                  from (i_cTable) where;
                      ARCODART = this.ODLART;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.ODLARTD = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
                this.ODLUNMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            go riga
            replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
          else
            if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
              replace peserodl with left(peserodl,10),oltstato1 with "R"
            else
              if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                *     associata nella tabella del pegging ma non � valida, quindi la elimino
                delete
              endif
            endif
          endif
          replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7))
          endscan
          update tmp set Descri=alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PESERODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 " ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:"+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
          select tmp 
 go top 
 scan 
 scatter memvar 
 m.collapsed="-" 
 m.PadreSup="S" 
 insert into tmpx from memvar 
 endscan
        endif
      endif
      select tmpx 
 go numriga 
 replace PadreSup with "N", Collapsed with "+" 
 endscan
    else
      * --- Tree-View Figlio-Padre
      select petiprif,peqtaabb,perifodl,perigord,cprownum,oltstato,oltcodic,ardesart,peserodl,oltstato1,oltdtric1,oltcodic1,ardesart1,; 
 oltqtod1,oltqtoe1,arunmis1,oltcomag,this.w_livello as livello,numdoc,alfdoc,prove,flveac,cladoc,left(alltrim("0000001")+space(240),240) as lvlkey,; 
 tippro,cpbmpname,descri,tipdoc,"+" as collapsed, "S" as PadreSup from (this.w_cCursor) where this.w_nNODO=recno() into cursor tmpx
      cur=wrcursor("tmpx") 
 update tmpx set Descri=right(descri,(len(descri)-(at("=>",descri)+2)))
      select tmpx 
 go top 
 scan for PadreSup="S" 
 numriga=recno()
      * --- Faccio una scan sul cursore - Solo i Padri che sto analizzando (PadreSup='S')
      this.oParentObject.w_CODODL = tmpx.PERIFODL
      this.w_RIGAORD = tmpx.PERIGORD
      this.ChiaveTV = lvlkey
      * --- Esplosione ricorsiva
      if this.oParentObject.w_CODODL<>"Magazzino" and this.oParentObject.w_CODODL <> " Magazz.:"
        * --- Tree-View Figlio-Padre
        vq_exec("..\COLA\EXE\QUERY\gsmr1bdd", this, "tmp")
        if reccount("tmp")>0
          select tmp 
 go top 
 conta=0 
 scan 
 conta=conta+1
          if perifodl="Magazzino" or perifodl=" Magazz.:"
            this.QTAABB = tmp.peqtaabb
            this.ODLKART = this.oparentobject.w_codric
            this.ODLARTD = this.oparentobject.w_cadesar
            this.ODLUNMIS = this.oparentobject.w_unimis
            replace oltstato1 with "G",oltdtric1 with cp_CharToDate(""),oltcodic1 with this.odlkart,ardesart1 with this.odlartd,oltqtod1 with this.qtaabb,oltqtoe1 with 0,; 
 arunmis1 with this.odlunmis, oltcomag with space(5)
          else
            if not empty(nvl(cladoc," ")) and not empty(nvl(flveac," ")) 
              replace oltstato1 with "R"
            else
              if empty(nvl(oltcodic1," ")) and empty(nvl(oltstato1,""))
                * --- Dopo aver fatto girare MRP/Pegging 2� Livello si � eliminato il documento/ordine di lavorazione, la riga risulta 
                *     ancora associata nella tabella del pegging ma non � + valida, quindi la elimino
                delete
              endif
            endif
          endif
          replace LvlKey with (alltrim(this.ChiaveTV)+"."+right("000000"+alltrim(str(conta)),7))
          endscan
          update tmp set Descri=alltrim(trans(PEQTAABB,v_pq(12)))+" "+nvl(ARUNMIS1,"")+" => "+ PERIFODL; 
 +iif(oltstato1="R"," Tipo Doc.:"+TIPDOC+" Doc.N.:"+allt(trans(NUMDOC, "@Z "+vnumdoc))+iif(empty(ALFDOC),"","/")+ALFDOC+" Causale Mag.:"+CAUMAG,"")+; 
 " ("+allt(trans(OLTQTOD1,v_pq(12)))+" "+nvl(ARUNMIS1,"")+") Art:"+allt(OLTCODIC1)+" ("+allt(ARDESART1)+")", ; 
 CPBmpName = iif(oltstato1="M",this.w_SUGG, iif(oltstato1="P",this.w_PIAN, iif(oltstato1="L",this.w_LANC,; 
 iif(oltstato1="R",this.w_ORD,iif(oltstato1="G",this.w_MAG,iif(oltstato $ "S-C-D",this.w_SCD,space(150)))))))
          select tmp 
 go top 
 scan 
 scatter memvar 
 m.PadreSup="S" 
 insert into tmpx from memvar 
 endscan
        endif
      endif
      select tmpx 
 go numriga 
 replace PadreSup with "N", Collapsed with "+" 
 endscan
    endif
    * --- Cancello il cursore che stavo usando in precedenza e lo sostituisco con quello appena creato
    if used(this.w_cCursor)
      use in (this.w_cCursor)
    endif
    this.w_cCursor = sys(2015)
    select petiprif,peqtaabb,peserodl,perigord,cprownum,oltstato,oltcodic,ardesart,perifodl,oltstato1,oltdtric1,oltcodic1,; 
 ardesart1,oltqtod1,oltqtoe1,arunmis1,oltcomag,livello,numdoc,alfdoc,prove,flveac,cladoc,left(alltrim(lvlkey)+space(240),240) as lvlkey,; 
 tippro,cpbmpname,descri,tipdoc,"-" as collapsed from tmpx into cursor (this.w_cCursor) order by 20 
 Select (this.w_cCursor) 
 index on lvlkey tag lvlkey collate "MACHINE"
    cur=wrcursor(this.w_cCursor)
    this.Albero.cCursor = this.w_cCursor
    if used("tmpx")
      use in tmpx
    endif
    if used("tmp")
      use in tmp
    endif
    this.oParentObject.w_CODODL = this.w_ODLORI
    * --- Refresh della Tree-View (per aggiungere i nuovi nodi)
    oldlocks=this.w_PUNPAD.lockscreen
    this.w_PUNPAD.lockscreen = .t.
    this.w_PUNPAD.NotifyEvent("updtreev")     
    this.Albero.ExpandAll(.T.)
    this.w_PUNPAD.lockscreen = oldlocks
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_PARAM)
    this.w_PARAM=w_PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ART_PROD'
    this.cWorkTables[2]='ODL_MAST'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='ODL_DETT'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='*TMPPEG_SELI'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PARAM"
endproc
