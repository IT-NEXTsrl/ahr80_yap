* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_amg                                                        *
*              Piani di generazione ODL/OCL                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_705]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-11                                                      *
* Last revis.: 2010-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsco_amg
PARAMETERS pTipGes, pTipGen
* --- Fine Area Manuale
return(createobject("tgsco_amg"))

* --- Class definition
define class tgsco_amg as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 735
  Height = 384+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-30"
  HelpContextID=210347369
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  RIFMGODL_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "RIFMGODL"
  cKeySelect = "PDSERIAL"
  cKeyWhere  = "PDSERIAL=this.w_PDSERIAL"
  cKeyWhereODBC = '"PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';

  cKeyWhereODBCqualified = '"RIFMGODL.PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';

  cPrg = "gsco_amg"
  cComment = "Piani di generazione ODL/OCL"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPGES = space(1)
  w_TIPGEN = space(2)
  w_PDPROVEN = space(1)
  w_PDDATGEN = ctod('  /  /  ')
  w_PDFLPROV = space(1)
  w_PDCODUTE = 0
  w_PDSERIAL = space(10)
  w_PDTIPGEN = space(2)
  w_PDTIPDOC = space(5)
  w_DESDOC = space(35)
  w_SELEZI = space(1)
  w_SERIALE = space(10)
  w_CICLO = space(1)
  w_CLADOC = space(2)
  w_REPSEC = space(1)
  w_FC = space(1)
  o_FC = space(1)
  w_TIPCN = space(1)
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_amg
  pTIPGES=' '
  pTIPGEN='  '
  * ---- Disattiva i metodi Load e Edit (posso solo Leggere e Eliminare)
  proc ecpLoad()
        * ----
  endproc
  proc ecpEdit()
      * ----
  endproc
  proc ecpDelete()
      * ----
      this.cFunction='Query'
      DoDefault()
  endproc
  * --- Esce Subito
  proc ecpQuit()
      * ----
      this.bUpdated=.f.
      DoDefault()
      Keyboard "{ESC}"
  endproc
  * --- Disabilita la Variazione e il Caricamento sulla Toolbar
  proc SetCPToolBar()
       doDefault()
       oCpToolBar.b2.enabled=.f.
       oCpToolBar.b3.enabled=.f.
       oCpToolBar.b4.enabled=.f.
  endproc
  
    cKeyWhere  = "PDSERIAL=this.w_PDSERIAL and PDTIPGEN=this.pTIPGEN"
    cKeyWhereODBC = '"PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';
        +'+" and PDTIPGEN="+cp_ToStrODBC(this.pTIPGEN)'
    cKeyWhereODBCqualified = '"RIFMGODL.PDSERIAL="+cp_ToStrODBC(this.w_PDSERIAL)';
        +'+" and RIFMGODL.PDTIPGEN="+cp_ToStrODBC(this.pTIPGEN)'
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIFMGODL','gsco_amg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_amgPag1","gsco_amg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano di generazione")
      .Pages(1).HelpContextID = 200546199
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDSERIAL_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsco_amg
    this.parent.pTIPGES =pTipGes
    this.parent.pTIPGEN =pTipGen
    DO CASE
       CASE this.parent.pTIPGEN = 'BP'
         this.parent.cComment = Ah_Msgformat('Manutenzione buoni di prelievo')
       CASE this.parent.pTIPGEN = 'OR'
         this.parent.cComment = Ah_Msgformat('Manutenzione ordini a fornitori')
       CASE this.parent.pTIPGEN = 'DT'
         this.parent.cComment = Ah_Msgformat('Manutenzione DDT di trasferimento')
    ENDCASE  
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
      DoDefault()
    proc Destroy()
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='RIFMGODL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIFMGODL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIFMGODL_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PDSERIAL = NVL(PDSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RIFMGODL where PDSERIAL=KeySet.PDSERIAL
    *
    i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIFMGODL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIFMGODL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIFMGODL '
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPGES = this.pTIPGES
        .w_TIPGEN = this.pTIPGEN
        .w_DESDOC = space(35)
        .w_SELEZI = 'D'
        .w_REPSEC = 'S'
        .w_FC = space(1)
        .w_PDPROVEN = NVL(PDPROVEN,space(1))
        .w_PDDATGEN = NVL(cp_ToDate(PDDATGEN),ctod("  /  /  "))
        .w_PDFLPROV = NVL(PDFLPROV,space(1))
        .w_PDCODUTE = NVL(PDCODUTE,0)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_PDSERIAL = NVL(PDSERIAL,space(10))
        .w_PDTIPGEN = NVL(PDTIPGEN,space(2))
        .w_PDTIPDOC = NVL(PDTIPDOC,space(5))
          if link_1_14_joined
            this.w_PDTIPDOC = NVL(TDTIPDOC114,NVL(this.w_PDTIPDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC114,space(35))
            this.w_FC = NVL(TDFLINTE114,space(1))
          else
          .link_1_14('Load')
          endif
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_PDSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_SERIALE = NVL(.w_ZoomDett.getVar('PDSERDOC'), SPACE(10))
        .w_CICLO = NVL(.w_ZoomDett.getVar('MVFLVEAC'),' ')
        .w_CLADOC = NVL(.w_ZoomDett.getVar('MVCLADOC'), '  ')
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .w_TIPCN = IIF(.w_FC='C' OR .w_FC='F',.w_FC,'')
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        cp_LoadRecExtFlds(this,'RIFMGODL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsco_amg
    if this.w_PDTIPGEN <> this.pTIPGEN
      this.BlankRec()
    endif
    
    if this.w_PDPROVEN <> this.pTIPGES
      this.BlankRec()
    endif
    
    l_ctrl = .NULL.
    l_ctrl = This.GetCtrl('w_SELEZI')
    l_ctrl.Enabled_(.t.)
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPGES = space(1)
      .w_TIPGEN = space(2)
      .w_PDPROVEN = space(1)
      .w_PDDATGEN = ctod("  /  /  ")
      .w_PDFLPROV = space(1)
      .w_PDCODUTE = 0
      .w_PDSERIAL = space(10)
      .w_PDTIPGEN = space(2)
      .w_PDTIPDOC = space(5)
      .w_DESDOC = space(35)
      .w_SELEZI = space(1)
      .w_SERIALE = space(10)
      .w_CICLO = space(1)
      .w_CLADOC = space(2)
      .w_REPSEC = space(1)
      .w_FC = space(1)
      .w_TIPCN = space(1)
      if .cFunction<>"Filter"
        .w_TIPGES = this.pTIPGES
        .w_TIPGEN = this.pTIPGEN
        .w_PDPROVEN = IIF(.w_TIPGES='E', 'E', 'L')
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(4,9,.f.)
          if not(empty(.w_PDTIPDOC))
          .link_1_14('Full')
          endif
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_PDSERIAL)
          .DoRTCalc(10,10,.f.)
        .w_SELEZI = 'D'
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_SERIALE = NVL(.w_ZoomDett.getVar('PDSERDOC'), SPACE(10))
        .w_CICLO = NVL(.w_ZoomDett.getVar('MVFLVEAC'),' ')
        .w_CLADOC = NVL(.w_ZoomDett.getVar('MVCLADOC'), '  ')
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .w_REPSEC = 'S'
          .DoRTCalc(16,16,.f.)
        .w_TIPCN = IIF(.w_FC='C' OR .w_FC='F',.w_FC,'')
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIFMGODL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPDSERIAL_1_11.enabled = i_bVal
      .Page1.oPag.oSELEZI_1_19.enabled_(i_bVal)
      .Page1.oPag.oREPSEC_1_29.enabled = i_bVal
      .Page1.oPag.oBtn_1_15.enabled = .Page1.oPag.oBtn_1_15.mCond()
      .Page1.oPag.oBtn_1_21.enabled = .Page1.oPag.oBtn_1_21.mCond()
      .Page1.oPag.oBtn_1_22.enabled = .Page1.oPag.oBtn_1_22.mCond()
      .Page1.oPag.oBtn_1_24.enabled = .Page1.oPag.oBtn_1_24.mCond()
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
      .Page1.oPag.oObj_1_10.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_28.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPDSERIAL_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPDSERIAL_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIFMGODL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDPROVEN,"PDPROVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDDATGEN,"PDDATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDFLPROV,"PDFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDCODUTE,"PDCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDSERIAL,"PDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDTIPGEN,"PDTIPGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PDTIPDOC,"PDTIPDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsco_amg
    i_cKey = this.cKeySelect + ",PDTIPGEN"
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
    i_lTable = "RIFMGODL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIFMGODL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RIFMGODL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RIFMGODL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIFMGODL')
        i_extval=cp_InsertValODBCExtFlds(this,'RIFMGODL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PDPROVEN,PDDATGEN,PDFLPROV,PDCODUTE,PDSERIAL"+;
                  ",PDTIPGEN,PDTIPDOC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PDPROVEN)+;
                  ","+cp_ToStrODBC(this.w_PDDATGEN)+;
                  ","+cp_ToStrODBC(this.w_PDFLPROV)+;
                  ","+cp_ToStrODBC(this.w_PDCODUTE)+;
                  ","+cp_ToStrODBC(this.w_PDSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PDTIPGEN)+;
                  ","+cp_ToStrODBCNull(this.w_PDTIPDOC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIFMGODL')
        i_extval=cp_InsertValVFPExtFlds(this,'RIFMGODL')
        cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_PDSERIAL)
        INSERT INTO (i_cTable);
              (PDPROVEN,PDDATGEN,PDFLPROV,PDCODUTE,PDSERIAL,PDTIPGEN,PDTIPDOC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PDPROVEN;
                  ,this.w_PDDATGEN;
                  ,this.w_PDFLPROV;
                  ,this.w_PDCODUTE;
                  ,this.w_PDSERIAL;
                  ,this.w_PDTIPGEN;
                  ,this.w_PDTIPDOC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RIFMGODL_IDX,i_nConn)
      *
      * update RIFMGODL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RIFMGODL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PDPROVEN="+cp_ToStrODBC(this.w_PDPROVEN)+;
             ",PDDATGEN="+cp_ToStrODBC(this.w_PDDATGEN)+;
             ",PDFLPROV="+cp_ToStrODBC(this.w_PDFLPROV)+;
             ",PDCODUTE="+cp_ToStrODBC(this.w_PDCODUTE)+;
             ",PDTIPGEN="+cp_ToStrODBC(this.w_PDTIPGEN)+;
             ",PDTIPDOC="+cp_ToStrODBCNull(this.w_PDTIPDOC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RIFMGODL')
        i_cWhere = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  )
        UPDATE (i_cTable) SET;
              PDPROVEN=this.w_PDPROVEN;
             ,PDDATGEN=this.w_PDDATGEN;
             ,PDFLPROV=this.w_PDFLPROV;
             ,PDCODUTE=this.w_PDCODUTE;
             ,PDTIPGEN=this.w_PDTIPGEN;
             ,PDTIPDOC=this.w_PDTIPDOC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RIFMGODL_IDX,i_nConn)
      *
      * delete RIFMGODL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PDSERIAL',this.w_PDSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIFMGODL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIFMGODL_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(1,8,.t.)
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_PDSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(10,11,.t.)
            .w_SERIALE = NVL(.w_ZoomDett.getVar('PDSERDOC'), SPACE(10))
            .w_CICLO = NVL(.w_ZoomDett.getVar('MVFLVEAC'),' ')
            .w_CLADOC = NVL(.w_ZoomDett.getVar('MVCLADOC'), '  ')
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .DoRTCalc(15,16,.t.)
        if .o_FC<>.w_FC
            .w_TIPCN = IIF(.w_FC='C' OR .w_FC='F',.w_FC,'')
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_PDSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return

  proc Calculate_UVNURBXGYC()
    with this
          * --- Abilito repsec
          Abilita_REPSEC(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSELEZI_1_19.enabled_(this.oPgFrm.Page1.oPag.oSELEZI_1_19.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_UVNURBXGYC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PDTIPDOC
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PDTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PDTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_FC = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PDTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_FC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.TDTIPDOC as TDTIPDOC114"+ ",link_1_14.TDDESDOC as TDDESDOC114"+ ",link_1_14.TDFLINTE as TDFLINTE114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on RIFMGODL.PDTIPDOC=link_1_14.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and RIFMGODL.PDTIPDOC=link_1_14.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPDDATGEN_1_5.value==this.w_PDDATGEN)
      this.oPgFrm.Page1.oPag.oPDDATGEN_1_5.value=this.w_PDDATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFLPROV_1_7.RadioValue()==this.w_PDFLPROV)
      this.oPgFrm.Page1.oPag.oPDFLPROV_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODUTE_1_8.value==this.w_PDCODUTE)
      this.oPgFrm.Page1.oPag.oPDCODUTE_1_8.value=this.w_PDCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPDSERIAL_1_11.value==this.w_PDSERIAL)
      this.oPgFrm.Page1.oPag.oPDSERIAL_1_11.value=this.w_PDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPDTIPDOC_1_14.value==this.w_PDTIPDOC)
      this.oPgFrm.Page1.oPag.oPDTIPDOC_1_14.value=this.w_PDTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_17.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_17.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_19.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREPSEC_1_29.RadioValue()==this.w_REPSEC)
      this.oPgFrm.Page1.oPag.oREPSEC_1_29.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'RIFMGODL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FC = this.w_FC
    return

enddefine

* --- Define pages as container
define class tgsco_amgPag1 as StdContainer
  Width  = 731
  height = 384
  stdWidth  = 731
  stdheight = 384
  resizeXpos=388
  resizeYpos=240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPDDATGEN_1_5 as StdField with uid="CVPWDIYIVS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PDDATGEN", cQueryName = "PDDATGEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di generazione",;
    HelpContextID = 268166212,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=238, Top=13


  add object oPDFLPROV_1_7 as StdCombo with uid="WBGAHSFZTV",rtseq=5,rtrep=.f.,left=616,top=13,width=110,height=21, enabled=.f.;
    , DisabledBackColor=rgb(255,255,255);
    , ToolTipText = "Stato dei documento generati";
    , HelpContextID = 180814924;
    , cFormVar="w_PDFLPROV",RowSource=""+"Provvisori,"+"Confermati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPDFLPROV_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPDFLPROV_1_7.GetRadio()
    this.Parent.oContained.w_PDFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oPDFLPROV_1_7.SetRadio()
    this.Parent.oContained.w_PDFLPROV=trim(this.Parent.oContained.w_PDFLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_PDFLPROV=='S',1,;
      iif(this.Parent.oContained.w_PDFLPROV=='N',2,;
      0))
  endfunc

  add object oPDCODUTE_1_8 as StdField with uid="CVGCKSWRLV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PDCODUTE", cQueryName = "PDCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operatore che ha effettuato la generazione",;
    HelpContextID = 218747963,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=522, Top=13, cSayPict='"9999"', cGetPict='"9999"'


  add object oObj_1_10 as cp_runprogram with uid="XIMJWGGOWY",left=1, top=401, width=177,height=17,;
    caption='GSCO_BMG',;
   bGlobalFont=.t.,;
    prg="GSCO_BMG('TITOLO')",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riassegna titolo e zoom a gestione";
    , HelpContextID = 71703891

  add object oPDSERIAL_1_11 as StdField with uid="HUCWOXUTLG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDSERIAL", cQueryName = "PDSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31511618,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=116, Top=13, InputMask=replicate('X',10)

  proc oPDSERIAL_1_11.mAfter
    with this.Parent.oContained
      .cFunction='Query'
    endwith
  endproc

  add object oPDTIPDOC_1_14 as StdField with uid="CJUUWNDKFC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PDTIPDOC", cQueryName = "PDTIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 214230073,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=238, Top=41, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PDTIPDOC"

  func oPDTIPDOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_15 as StdButton with uid="SPLVTUESRR",left=678, top=37, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i documenti associati al piano";
    , HelpContextID = 204713607;
    , Caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSCO_BMG(this.Parent.oContained,"CONFERMA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERIAL) AND .w_PDFLPROV='S')
      endwith
    endif
  endfunc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PDFLPROV<>'S')
     endwith
    endif
  endfunc

  add object oDESDOC_1_17 as StdField with uid="GALREXVGFM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196072502,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=304, Top=41, InputMask=replicate('X',35)


  add object ZoomDett as cp_szoombox with uid="XXSGZLMPZJ",left=-2, top=84, width=735,height=249,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="RIF_GODL",cZoomFile="GSCO_ZMG",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSZM_BCC",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 236085734

  add object oSELEZI_1_19 as StdRadio with uid="QLKQNQKFQQ",rtseq=11,rtrep=.f.,left=4, top=339, width=141,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_19.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 39871782
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 39871782
      this.Buttons(2).Top=15
      this.SetAll("Width",139)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_19.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_19.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.t.)
    endwith
   endif
  endfunc


  add object oObj_1_20 as cp_runprogram with uid="HDPQPFNITD",left=188, top=401, width=207,height=19,;
    caption='GSCO_BMG',;
   bGlobalFont=.t.,;
    prg="GSCO_BMG('SELEZI')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 71703891


  add object oBtn_1_21 as StdButton with uid="PNDIUHMGEL",left=519, top=338, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i documenti selezionati sul piano di generazione";
    , HelpContextID = 142236486;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSCO_BMG(this.Parent.oContained,"ELIMINARIGHE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="EERZOILSBK",left=678, top=338, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 203029946;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_23 as cp_runprogram with uid="TBQHIMKVVP",left=408, top=403, width=177,height=17,;
    caption='GSCO_BMG',;
   bGlobalFont=.t.,;
    prg="GSCO_BMG('CARICA')",;
    cEvent = "Load",;
    nPag=1;
    , ToolTipText = "Riassegna titolo e zoom a gestione";
    , HelpContextID = 71703891


  add object oBtn_1_24 as StdButton with uid="EWXXYWJXZJ",left=471, top=338, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento selezionato";
    , HelpContextID = 16909471;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSCO_BMG(this.Parent.oContained,"DETTAGLI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERIAL) AND NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oObj_1_28 as cp_runprogram with uid="PXTMGCWWPR",left=596, top=404, width=177,height=17,;
    caption='GSCO_BMG',;
   bGlobalFont=.t.,;
    prg="GSCO_BMG('ELIMINATUTTO')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 71703891


  add object oREPSEC_1_29 as StdCombo with uid="ILFBHAZQOE",rtseq=15,rtrep=.f.,left=247,top=346,width=170,height=21;
    , ToolTipText = "Stampa report secondari";
    , HelpContextID = 186557718;
    , cFormVar="w_REPSEC",RowSource=""+"Tutti i report secondari,"+"Nessun report secondario,"+"Escludi report opzionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREPSEC_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oREPSEC_1_29.GetRadio()
    this.Parent.oContained.w_REPSEC = this.RadioValue()
    return .t.
  endfunc

  func oREPSEC_1_29.SetRadio()
    this.Parent.oContained.w_REPSEC=trim(this.Parent.oContained.w_REPSEC)
    this.value = ;
      iif(this.Parent.oContained.w_REPSEC=='S',1,;
      iif(this.Parent.oContained.w_REPSEC=='N',2,;
      iif(this.Parent.oContained.w_REPSEC=='C',3,;
      0)))
  endfunc


  add object oBtn_1_30 as StdButton with uid="HVRBFDASDR",left=422, top=338, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i documenti selezionati";
    , HelpContextID = 199877670;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSCO_BMG(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERIAL))
      endwith
    endif
  endfunc


  add object oObj_1_33 as cp_runprogram with uid="YQTEZRQDLA",left=1, top=421, width=261,height=19,;
    caption='GSCO_BMG(DETTAGLI)',;
   bGlobalFont=.t.,;
    prg="GSCO_BMG('DETTAGLI')",;
    cEvent = "w_zoomdett selected",;
    nPag=1;
    , HelpContextID = 229464311

  add object oStr_1_4 as StdString with uid="PSUJNLMIEH",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=107, Height=18,;
    Caption="Rif.generazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="ZRZPGARMOT",Visible=.t., Left=570, Top=13,;
    Alignment=1, Width=43, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="SWHTUPFCVN",Visible=.t., Left=437, Top=13,;
    Alignment=1, Width=82, Height=15,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZDKKXLVDBE",Visible=.t., Left=204, Top=13,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="JUAIMSIJKZ",Visible=.t., Left=101, Top=41,;
    Alignment=1, Width=134, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YQPOXLZVJK",Visible=.t., Left=153, Top=346,;
    Alignment=1, Width=93, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_amg','RIFMGODL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PDSERIAL=RIFMGODL.PDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsco_amg
Proc Abilita_REPSEC(pParent)
   local obj
   obj = pParent.getCtrl("w_REPSEC")
   obj.enabled=.t.
   obj=.null.
EndProc
* --- Fine Area Manuale
