* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsco_mcm                                                        *
*              Matricole dei componenti                                        *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_362]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-03                                                      *
* Last revis.: 2015-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsco_mcm")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsco_mcm")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsco_mcm")
  return

* --- Class definition
define class tgsco_mcm as StdPCForm
  Width  = 499
  Height = 288
  Top    = 10
  Left   = 10
  cComment = "Matricole dei componenti"
  cPrg = "gsco_mcm"
  HelpContextID=171334295
  add object cnt as tcgsco_mcm
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsco_mcm as PCContext
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  w_MTCODMAT = space(40)
  w_FLLOTT = space(1)
  w_MTCODLOT = space(20)
  w_MTCODUBI = space(20)
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  w_QTAUM1 = 0
  w_MGUBIC = space(1)
  w_RIGA = 0
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_VARLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = space(1)
  w_MAGUBI = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = space(1)
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_CODLOTTES = space(20)
  w_ARTLOTTES = space(20)
  w_CODUBITES = space(20)
  w_FLPRG = space(1)
  w_TOTALE = 0
  w_MTMAGCAR = space(5)
  w_MTROWODL = 0
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_FLPRD = space(1)
  w_NOMSG = space(1)
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_MTNUMRIF = 0
  w_MTCODCOM = space(15)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  proc Save(i_oFrom)
    this.w_MTSERIAL = i_oFrom.w_MTSERIAL
    this.w_MTROWNUM = i_oFrom.w_MTROWNUM
    this.w_MTKEYSAL = i_oFrom.w_MTKEYSAL
    this.w_MTCODMAT = i_oFrom.w_MTCODMAT
    this.w_MTCODMAT = i_oFrom.w_MTCODMAT
    this.w_FLLOTT = i_oFrom.w_FLLOTT
    this.w_MTCODLOT = i_oFrom.w_MTCODLOT
    this.w_MTCODUBI = i_oFrom.w_MTCODUBI
    this.w_MTSERRIF = i_oFrom.w_MTSERRIF
    this.w_MTROWRIF = i_oFrom.w_MTROWRIF
    this.w_CODART = i_oFrom.w_CODART
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_MGUBIC = i_oFrom.w_MGUBIC
    this.w_RIGA = i_oFrom.w_RIGA
    this.w_MTFLSCAR = i_oFrom.w_MTFLSCAR
    this.w_MTMAGSCA = i_oFrom.w_MTMAGSCA
    this.w_MTFLCARI = i_oFrom.w_MTFLCARI
    this.w_CODMAG = i_oFrom.w_CODMAG
    this.w_ESIRIS = i_oFrom.w_ESIRIS
    this.w_KEYSAL = i_oFrom.w_KEYSAL
    this.w_MTRIFNUM = i_oFrom.w_MTRIFNUM
    this.w_MT__FLAG = i_oFrom.w_MT__FLAG
    this.w_MT_SALDO = i_oFrom.w_MT_SALDO
    this.w_CODFOR = i_oFrom.w_CODFOR
    this.w_ARTLOT = i_oFrom.w_ARTLOT
    this.w_VARLOT = i_oFrom.w_VARLOT
    this.w_STALOT = i_oFrom.w_STALOT
    this.w_LOTZOOM = i_oFrom.w_LOTZOOM
    this.w_MAGUBI = i_oFrom.w_MAGUBI
    this.w_SERRIF = i_oFrom.w_SERRIF
    this.w_ROWRIF = i_oFrom.w_ROWRIF
    this.w_FLRISE = i_oFrom.w_FLRISE
    this.w_NUMRIF = i_oFrom.w_NUMRIF
    this.w_MOVMAT = i_oFrom.w_MOVMAT
    this.w_DATREG = i_oFrom.w_DATREG
    this.w_ULTPROG = i_oFrom.w_ULTPROG
    this.w_SECODUBI = i_oFrom.w_SECODUBI
    this.w_SECODLOT = i_oFrom.w_SECODLOT
    this.w_CODLOTTES = i_oFrom.w_CODLOTTES
    this.w_ARTLOTTES = i_oFrom.w_ARTLOTTES
    this.w_CODUBITES = i_oFrom.w_CODUBITES
    this.w_FLPRG = i_oFrom.w_FLPRG
    this.w_TOTALE = i_oFrom.w_TOTALE
    this.w_MTMAGCAR = i_oFrom.w_MTMAGCAR
    this.w_MTROWODL = i_oFrom.w_MTROWODL
    this.w_CODICE = i_oFrom.w_CODICE
    this.w_DESART = i_oFrom.w_DESART
    this.w_UNIMIS1 = i_oFrom.w_UNIMIS1
    this.w_FLPRD = i_oFrom.w_FLPRD
    this.w_NOMSG = i_oFrom.w_NOMSG
    this.w_CODUBI = i_oFrom.w_CODUBI
    this.w_MTRIFSTO = i_oFrom.w_MTRIFSTO
    this.w_MTNUMRIF = i_oFrom.w_MTNUMRIF
    this.w_MTCODCOM = i_oFrom.w_MTCODCOM
    this.w_COMCAR = i_oFrom.w_COMCAR
    this.w_COMSCA = i_oFrom.w_COMSCA
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MTSERIAL = this.w_MTSERIAL
    i_oTo.w_MTROWNUM = this.w_MTROWNUM
    i_oTo.w_MTKEYSAL = this.w_MTKEYSAL
    i_oTo.w_MTCODMAT = this.w_MTCODMAT
    i_oTo.w_MTCODMAT = this.w_MTCODMAT
    i_oTo.w_FLLOTT = this.w_FLLOTT
    i_oTo.w_MTCODLOT = this.w_MTCODLOT
    i_oTo.w_MTCODUBI = this.w_MTCODUBI
    i_oTo.w_MTSERRIF = this.w_MTSERRIF
    i_oTo.w_MTROWRIF = this.w_MTROWRIF
    i_oTo.w_CODART = this.w_CODART
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_MGUBIC = this.w_MGUBIC
    i_oTo.w_RIGA = this.w_RIGA
    i_oTo.w_MTFLSCAR = this.w_MTFLSCAR
    i_oTo.w_MTMAGSCA = this.w_MTMAGSCA
    i_oTo.w_MTFLCARI = this.w_MTFLCARI
    i_oTo.w_CODMAG = this.w_CODMAG
    i_oTo.w_ESIRIS = this.w_ESIRIS
    i_oTo.w_KEYSAL = this.w_KEYSAL
    i_oTo.w_MTRIFNUM = this.w_MTRIFNUM
    i_oTo.w_MT__FLAG = this.w_MT__FLAG
    i_oTo.w_MT_SALDO = this.w_MT_SALDO
    i_oTo.w_CODFOR = this.w_CODFOR
    i_oTo.w_ARTLOT = this.w_ARTLOT
    i_oTo.w_VARLOT = this.w_VARLOT
    i_oTo.w_STALOT = this.w_STALOT
    i_oTo.w_LOTZOOM = this.w_LOTZOOM
    i_oTo.w_MAGUBI = this.w_MAGUBI
    i_oTo.w_SERRIF = this.w_SERRIF
    i_oTo.w_ROWRIF = this.w_ROWRIF
    i_oTo.w_FLRISE = this.w_FLRISE
    i_oTo.w_NUMRIF = this.w_NUMRIF
    i_oTo.w_MOVMAT = this.w_MOVMAT
    i_oTo.w_DATREG = this.w_DATREG
    i_oTo.w_ULTPROG = this.w_ULTPROG
    i_oTo.w_SECODUBI = this.w_SECODUBI
    i_oTo.w_SECODLOT = this.w_SECODLOT
    i_oTo.w_CODLOTTES = this.w_CODLOTTES
    i_oTo.w_ARTLOTTES = this.w_ARTLOTTES
    i_oTo.w_CODUBITES = this.w_CODUBITES
    i_oTo.w_FLPRG = this.w_FLPRG
    i_oTo.w_TOTALE = this.w_TOTALE
    i_oTo.w_MTMAGCAR = this.w_MTMAGCAR
    i_oTo.w_MTROWODL = this.w_MTROWODL
    i_oTo.w_CODICE = this.w_CODICE
    i_oTo.w_DESART = this.w_DESART
    i_oTo.w_UNIMIS1 = this.w_UNIMIS1
    i_oTo.w_FLPRD = this.w_FLPRD
    i_oTo.w_NOMSG = this.w_NOMSG
    i_oTo.w_CODUBI = this.w_CODUBI
    i_oTo.w_MTRIFSTO = this.w_MTRIFSTO
    i_oTo.w_MTNUMRIF = this.w_MTNUMRIF
    i_oTo.w_MTCODCOM = this.w_MTCODCOM
    i_oTo.w_COMCAR = this.w_COMCAR
    i_oTo.w_COMSCA = this.w_COMSCA
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsco_mcm as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 499
  Height = 288
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-29"
  HelpContextID=171334295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=56

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIC_MCOM_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  MATRICOL_IDX = 0
  MOVIMATR_IDX = 0
  cFile = "DIC_MCOM"
  cKeySelect = "MTSERIAL,MTROWNUM,MTROWODL"
  cKeyWhere  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTROWODL=this.w_MTROWODL"
  cKeyDetail  = "MTSERIAL=this.w_MTSERIAL and MTROWNUM=this.w_MTROWNUM and MTKEYSAL=this.w_MTKEYSAL and MTCODMAT=this.w_MTCODMAT and MTCODMAT=this.w_MTCODMAT and MTROWODL=this.w_MTROWODL"
  cKeyWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTROWODL="+cp_ToStrODBC(this.w_MTROWODL)';

  cKeyDetailWhereODBC = '"MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)';
      +'+" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)';
      +'+" and MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)';
      +'+" and MTROWODL="+cp_ToStrODBC(this.w_MTROWODL)';

  cKeyWhereODBCqualified = '"DIC_MCOM.MTSERIAL="+cp_ToStrODBC(this.w_MTSERIAL)';
      +'+" and DIC_MCOM.MTROWNUM="+cp_ToStrODBC(this.w_MTROWNUM)';
      +'+" and DIC_MCOM.MTROWODL="+cp_ToStrODBC(this.w_MTROWODL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DIC_MCOM.MTCODLOT,DIC_MCOM.MTCODUBI,DIC_MCOM.MTCODMAT'
  cPrg = "gsco_mcm"
  cComment = "Matricole dei componenti"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MTSERIAL = space(15)
  w_MTROWNUM = 0
  w_MTKEYSAL = space(20)
  w_MTCODMAT = space(40)
  o_MTCODMAT = space(40)
  w_MTCODMAT = space(40)
  w_FLLOTT = space(1)
  w_MTCODLOT = space(20)
  o_MTCODLOT = space(20)
  w_MTCODUBI = space(20)
  w_MTSERRIF = space(10)
  w_MTROWRIF = 0
  w_CODART = space(20)
  o_CODART = space(20)
  w_QTAUM1 = 0
  w_MGUBIC = space(1)
  w_RIGA = 0
  w_MTFLSCAR = space(1)
  w_MTMAGSCA = space(5)
  w_MTFLCARI = space(1)
  w_CODMAG = space(5)
  w_ESIRIS = space(1)
  w_KEYSAL = space(20)
  w_MTRIFNUM = 0
  w_MT__FLAG = space(1)
  w_MT_SALDO = 0
  w_CODFOR = space(15)
  w_ARTLOT = space(20)
  w_VARLOT = space(20)
  w_STALOT = space(1)
  w_LOTZOOM = .F.
  w_MAGUBI = space(5)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_FLRISE = space(1)
  w_NUMRIF = 0
  w_MOVMAT = .F.
  w_DATREG = space(5)
  w_ULTPROG = 0
  w_SECODUBI = space(20)
  w_SECODLOT = space(20)
  w_CODLOTTES = space(20)
  w_ARTLOTTES = space(20)
  w_CODUBITES = space(20)
  w_FLPRG = space(1)
  w_TOTALE = 0
  w_MTMAGCAR = space(5)
  w_MTROWODL = 0
  w_CODICE = space(41)
  w_DESART = space(40)
  w_UNIMIS1 = space(3)
  w_FLPRD = space(1)
  w_NOMSG = .F.
  w_CODUBI = space(20)
  w_MTRIFSTO = 0
  w_MTNUMRIF = 0
  w_MTCODCOM = space(15)
  w_COMCAR = space(15)
  w_COMSCA = space(15)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsco_mcm
  Procedure InitSon
  * Valorizzo le variabili che arrivano dal padre
  	With This
      .cFunction=.oParentObject.cFunction
  		.w_MTMAGCAR=Space(5)
  		.w_MTMAGSCA=Space(5)
  		.w_MTFLSCAR=' '
  		.w_MTFLCARI=' '
  		* Scarico da evasione
  	  .w_SERRIF=Space(10)
      .w_ROWRIF=0
      .w_FLRISE=' '
      .w_NUMRIF=0
   		.w_CODICE=.oParentObject.w_MTCODRIC
  		.w_DESART=.oParentObject.w_DESART
  		.w_CODART=.oParentObject.w_CODART
      .w_FLLOTT=.oParentObject.w_FLLOTT
  		.w_QTAUM1=.oParentObject.w_MTQTAMOV
  		.w_UNIMIS1=.oParentObject.w_MTUNIMIS
      .w_MTKEYSAL=.oParentObject.w_MTKEYSAL
  		.w_KEYSAL=.w_MTKEYSAL
      .w_DATREG=.oParentObject.w_DATREG
      .w_COMCAR=.oParentObject.w_MTCODCOM
      .w_COMSCA=.oParentObject.w_MTCODCOM
      * Determino Magazzino di Scarico
  		.w_MTMAGSCA=.oParentObject.w_MTMAGSCA
  		.w_MTFLSCAR='E'
  			
      * tipo di operazione (Esistenza/Riservato)
  		.w_ESIRIS = IIF( Not Empty( .w_MTFLSCAR) , .w_MTFLSCAR , .w_MTFLCARI )
  		
  		* Magazzino di Riferimento (se presente magazzino di scarico)
  		.w_CODMAG = IIF( Not Empty( .w_MTMAGSCA) , .w_MTMAGSCA , .w_MTMAGCAR )
  		
  		.SetControlsValue()
  		
  		* Mostro / Nascondo i Controls
  		.mHideControls()
  	EndWith
  EndProc
  
  PROC F6()
  * --- disabilita F6 per righe non cancellabili
  * --- Es. matricola movimentata in seguito
  	if inlist(this.cFunction,'Edit','Load')
  		If this.w_MT_SALDO<>0
  			Ah_errormsg("Impossibile cancellare una riga succesivamente movimentata",'!',"")
  		else
  			dodefault()
  		Endif
  	Endif
  EndProc
  
  PROC ChildrenChangeRow()
  * --- al cambio riga aggiorna le variabili del detail
   this.initson()
   dodefault()
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsco_mcmPag1","gsco_mcm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='LOTTIART'
    this.cWorkTables[2]='UBICAZIO'
    this.cWorkTables[3]='MATRICOL'
    this.cWorkTables[4]='MOVIMATR'
    this.cWorkTables[5]='DIC_MCOM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_MCOM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_MCOM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsco_mcm'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsco_mcm
    This.InitSon()
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIC_MCOM where MTSERIAL=KeySet.MTSERIAL
    *                            and MTROWNUM=KeySet.MTROWNUM
    *                            and MTKEYSAL=KeySet.MTKEYSAL
    *                            and MTCODMAT=KeySet.MTCODMAT
    *                            and MTCODMAT=KeySet.MTCODMAT
    *                            and MTROWODL=KeySet.MTROWODL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2],this.bLoadRecFilter,this.DIC_MCOM_IDX,"gsco_mcm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_MCOM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_MCOM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_MCOM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MTSERIAL',this.w_MTSERIAL  ,'MTROWNUM',this.w_MTROWNUM  ,'MTROWODL',this.w_MTROWODL  )
      select * from (i_cTable) DIC_MCOM where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MTFLSCAR = "E"
        .w_MTMAGSCA = .oParentObject.w_MTMAGSCA
        .w_MTFLCARI = space(1)
        .w_MOVMAT = .t.
        .w_ULTPROG = 0
        .w_SECODUBI = space(20)
        .w_SECODLOT = space(20)
        .w_CODLOTTES = space(20)
        .w_ARTLOTTES = space(20)
        .w_CODUBITES = space(20)
        .w_TOTALE = 0
        .w_MTMAGCAR = space(5)
        .w_CODICE = space(41)
        .w_FLPRD = '+'
        .w_NOMSG = .f.
        .w_CODUBI = space(20)
        .w_MTSERIAL = NVL(MTSERIAL,space(15))
        .w_MTROWNUM = NVL(MTROWNUM,0)
        .w_CODART = .oParentObject.w_CODART
        .w_QTAUM1 = .oParentObject.w_MTQTAMOV
        .w_MGUBIC = .w_MGUBIC
        .w_CODMAG = .w_MTMAGSCA
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .oParentObject.w_MTKEYSAL
        .w_CODFOR = .w_CODFOR
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_DATREG = .w_DATREG
        .w_FLPRG = .w_FLPRG
        .w_MTROWODL = NVL(MTROWODL,0)
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_DESART = .oParentObject.w_DESART
        .w_UNIMIS1 = .oParentObject.w_MTUNIMIS
        .w_MTNUMRIF = NVL(MTNUMRIF,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
        cp_LoadRecExtFlds(this,'DIC_MCOM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_ARTLOT = space(20)
          .w_VARLOT = space(20)
          .w_STALOT = space(1)
          .w_MTKEYSAL = NVL(MTKEYSAL,space(20))
          .w_MTCODMAT = NVL(MTCODMAT,space(40))
          .w_MTCODMAT = NVL(MTCODMAT,space(40))
          * evitabile
          *.link_2_3('Load')
        .w_FLLOTT = .oParentObject.w_FLLOTT
          .w_MTCODLOT = NVL(MTCODLOT,space(20))
          .link_2_5('Load')
          .w_MTCODUBI = NVL(MTCODUBI,space(20))
          .w_MTSERRIF = NVL(MTSERRIF,space(10))
          .w_MTROWRIF = NVL(MTROWRIF,0)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_MTRIFNUM = NVL(MTRIFNUM,0)
          .w_MT__FLAG = NVL(MT__FLAG,space(1))
          .w_MT_SALDO = NVL(MT_SALDO,0)
        .w_LOTZOOM = .F.
          .w_MTRIFSTO = NVL(MTRIFSTO,0)
          .w_MTCODCOM = NVL(MTCODCOM,space(15))
          select (this.cTrsName)
          append blank
          replace MTKEYSAL with .w_MTKEYSAL
          replace MTCODMAT with .w_MTCODMAT
          replace MTCODMAT with .w_MTCODMAT
          replace MT__FLAG with .w_MT__FLAG
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_RIGA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CODART = .oParentObject.w_CODART
        .w_QTAUM1 = .oParentObject.w_MTQTAMOV
        .w_MGUBIC = .w_MGUBIC
        .w_CODMAG = .w_MTMAGSCA
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .oParentObject.w_MTKEYSAL
        .w_CODFOR = .w_CODFOR
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_DATREG = .w_DATREG
        .w_FLPRG = .w_FLPRG
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_DESART = .oParentObject.w_DESART
        .w_UNIMIS1 = .oParentObject.w_MTUNIMIS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MTSERIAL=space(15)
      .w_MTROWNUM=0
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTCODMAT=space(40)
      .w_FLLOTT=space(1)
      .w_MTCODLOT=space(20)
      .w_MTCODUBI=space(20)
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_CODART=space(20)
      .w_QTAUM1=0
      .w_MGUBIC=space(1)
      .w_RIGA=0
      .w_MTFLSCAR=space(1)
      .w_MTMAGSCA=space(5)
      .w_MTFLCARI=space(1)
      .w_CODMAG=space(5)
      .w_ESIRIS=space(1)
      .w_KEYSAL=space(20)
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_CODFOR=space(15)
      .w_ARTLOT=space(20)
      .w_VARLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MAGUBI=space(5)
      .w_SERRIF=space(10)
      .w_ROWRIF=0
      .w_FLRISE=space(1)
      .w_NUMRIF=0
      .w_MOVMAT=.f.
      .w_DATREG=space(5)
      .w_ULTPROG=0
      .w_SECODUBI=space(20)
      .w_SECODLOT=space(20)
      .w_CODLOTTES=space(20)
      .w_ARTLOTTES=space(20)
      .w_CODUBITES=space(20)
      .w_FLPRG=space(1)
      .w_TOTALE=0
      .w_MTMAGCAR=space(5)
      .w_MTROWODL=0
      .w_CODICE=space(41)
      .w_DESART=space(40)
      .w_UNIMIS1=space(3)
      .w_FLPRD=space(1)
      .w_NOMSG=.f.
      .w_CODUBI=space(20)
      .w_MTRIFSTO=0
      .w_MTNUMRIF=0
      .w_MTCODCOM=space(15)
      .w_COMCAR=space(15)
      .w_COMSCA=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_MTKEYSAL = .oParentObject.w_MTKEYSAL
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_MTCODMAT))
         .link_2_3('Full')
        endif
        .w_FLLOTT = .oParentObject.w_FLLOTT
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MTCODLOT))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_MTROWRIF = 0
        .w_CODART = .oParentObject.w_CODART
        .w_QTAUM1 = .oParentObject.w_MTQTAMOV
        .w_MGUBIC = .w_MGUBIC
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
        .w_MTFLSCAR = "E"
        .w_MTMAGSCA = .oParentObject.w_MTMAGSCA
        .DoRTCalc(17,17,.f.)
        .w_CODMAG = .w_MTMAGSCA
        .w_ESIRIS = .w_ESIRIS
        .w_KEYSAL = .oParentObject.w_MTKEYSAL
        .DoRTCalc(21,21,.f.)
        .w_MT__FLAG = ' '
        .DoRTCalc(23,23,.f.)
        .w_CODFOR = .w_CODFOR
        .DoRTCalc(25,27,.f.)
        .w_LOTZOOM = .F.
        .w_MAGUBI = .w_MAGUBI
        .w_SERRIF = .w_SERRIF
        .w_ROWRIF = .w_ROWRIF
        .w_FLRISE = .w_FLRISE
        .w_NUMRIF = .w_NUMRIF
        .w_MOVMAT = .t.
        .w_DATREG = .w_DATREG
        .DoRTCalc(36,41,.f.)
        .w_FLPRG = .w_FLPRG
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(43,46,.f.)
        .w_DESART = .oParentObject.w_DESART
        .w_UNIMIS1 = .oParentObject.w_MTUNIMIS
        .w_FLPRD = '+'
        .w_NOMSG = .f.
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(51,54,.f.)
        .w_COMCAR = .w_COMCAR
        .w_COMSCA = .w_COMSCA
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_MCOM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsco_mcm
    * Dopo l'inserzione della riga rileggo tutto
    This.InitSon()
    * per aggiornare w_MTKEYSAL (sul transitorio)
    This.TrsFromWork()
    
    * verifico se lanciare il caricamento
    If Not Empty(This.w_MTMAGCAR) or Not Empty(This.w_MTMAGSCA)
      * sono in un caricamento verifico se riga aggiunta
      Local L_area,L_Srv
      L_Area=Select()
      Select(This.oParentObject.cTrsName)
      L_Srv=I_Srv
      Select (L_Area)
      * se riga in append e attivo flag caricamento rapido
      * sulla causale
      If L_Srv='A' And This.oParentObject.w_MTCARI='S'
        This.NotifyEvent('CarRapido')
      Endif
    Endif
    
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gsco_mcm
    this.InitSon()
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_29.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DIC_MCOM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTSERIAL,"MTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWNUM,"MTROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTROWODL,"MTROWODL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MTNUMRIF,"MTNUMRIF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MTCODMAT C(40);
      ,t_MTCODLOT C(20);
      ,MTKEYSAL C(20);
      ,MTCODMAT C(40);
      ,MT__FLAG C(1);
      ,t_MTKEYSAL C(20);
      ,t_FLLOTT C(1);
      ,t_MTCODUBI C(20);
      ,t_MTSERRIF C(10);
      ,t_MTROWRIF N(4);
      ,t_RIGA N(10);
      ,t_MTRIFNUM N(4);
      ,t_MT__FLAG C(1);
      ,t_MT_SALDO N(1);
      ,t_ARTLOT C(20);
      ,t_VARLOT C(20);
      ,t_STALOT C(1);
      ,t_LOTZOOM L(1);
      ,t_MTRIFSTO N(4);
      ,t_MTCODCOM C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsco_mcmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.controlsource=this.cTrsName+'.t_MTCODMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_5.controlsource=this.cTrsName+'.t_MTCODLOT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(310)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2])
      *
      * insert into DIC_MCOM
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_MCOM')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_MCOM')
        i_cFldBody=" "+;
                  "(MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT,MTCODLOT"+;
                  ",MTCODUBI,MTSERRIF,MTROWRIF,MTRIFNUM,MT__FLAG"+;
                  ",MT_SALDO,MTROWODL,MTRIFSTO,MTNUMRIF,MTCODCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MTSERIAL)+","+cp_ToStrODBC(this.w_MTROWNUM)+","+cp_ToStrODBC(this.w_MTKEYSAL)+","+cp_ToStrODBC(this.w_MTCODMAT)+","+cp_ToStrODBCNull(this.w_MTCODLOT)+;
             ","+cp_ToStrODBC(this.w_MTCODUBI)+","+cp_ToStrODBC(this.w_MTSERRIF)+","+cp_ToStrODBC(this.w_MTROWRIF)+","+cp_ToStrODBC(this.w_MTRIFNUM)+","+cp_ToStrODBC(this.w_MT__FLAG)+;
             ","+cp_ToStrODBC(this.w_MT_SALDO)+","+cp_ToStrODBC(this.w_MTROWODL)+","+cp_ToStrODBC(this.w_MTRIFSTO)+","+cp_ToStrODBC(this.w_MTNUMRIF)+","+cp_ToStrODBC(this.w_MTCODCOM)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_MCOM')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_MCOM')
        cp_CheckDeletedKey(i_cTable,0,'MTSERIAL',this.w_MTSERIAL,'MTROWNUM',this.w_MTROWNUM,'MTKEYSAL',this.w_MTKEYSAL,'MTCODMAT',this.w_MTCODMAT,'MTCODMAT',this.w_MTCODMAT,'MTROWODL',this.w_MTROWODL)
        INSERT INTO (i_cTable) (;
                   MTSERIAL;
                  ,MTROWNUM;
                  ,MTKEYSAL;
                  ,MTCODMAT;
                  ,MTCODLOT;
                  ,MTCODUBI;
                  ,MTSERRIF;
                  ,MTROWRIF;
                  ,MTRIFNUM;
                  ,MT__FLAG;
                  ,MT_SALDO;
                  ,MTROWODL;
                  ,MTRIFSTO;
                  ,MTNUMRIF;
                  ,MTCODCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MTSERIAL;
                  ,this.w_MTROWNUM;
                  ,this.w_MTKEYSAL;
                  ,this.w_MTCODMAT;
                  ,this.w_MTCODLOT;
                  ,this.w_MTCODUBI;
                  ,this.w_MTSERRIF;
                  ,this.w_MTROWRIF;
                  ,this.w_MTRIFNUM;
                  ,this.w_MT__FLAG;
                  ,this.w_MT_SALDO;
                  ,this.w_MTROWODL;
                  ,this.w_MTRIFSTO;
                  ,this.w_MTNUMRIF;
                  ,this.w_MTCODCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MTCODMAT)) and not empty(t_MTKEYSAL)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_MCOM')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                 " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                 " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_MCOM')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MTNUMRIF=this.w_MTNUMRIF;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                      and MTCODMAT=&i_TN.->MTCODMAT;
                      and MTCODMAT=&i_TN.->MTCODMAT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MTCODMAT)) and not empty(t_MTKEYSAL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MTKEYSAL=&i_TN.->MTKEYSAL;
                            and MTCODMAT=&i_TN.->MTCODMAT;
                            and MTCODMAT=&i_TN.->MTCODMAT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MTKEYSAL with this.w_MTKEYSAL
              replace MTCODMAT with this.w_MTCODMAT
              replace MTCODMAT with this.w_MTCODMAT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIC_MCOM
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_MCOM')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MTCODLOT="+cp_ToStrODBCNull(this.w_MTCODLOT)+;
                     ",MTCODUBI="+cp_ToStrODBC(this.w_MTCODUBI)+;
                     ",MTSERRIF="+cp_ToStrODBC(this.w_MTSERRIF)+;
                     ",MTROWRIF="+cp_ToStrODBC(this.w_MTROWRIF)+;
                     ",MTRIFNUM="+cp_ToStrODBC(this.w_MTRIFNUM)+;
                     ",MT__FLAG="+cp_ToStrODBC(this.w_MT__FLAG)+;
                     ",MT_SALDO="+cp_ToStrODBC(this.w_MT_SALDO)+;
                     ",MTRIFSTO="+cp_ToStrODBC(this.w_MTRIFSTO)+;
                     ",MTNUMRIF="+cp_ToStrODBC(this.w_MTNUMRIF)+;
                     ",MTCODCOM="+cp_ToStrODBC(this.w_MTCODCOM)+;
                     ",MTKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL)+;
                     ",MTCODMAT="+cp_ToStrODBC(this.w_MTCODMAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MTKEYSAL="+cp_ToStrODBC(MTKEYSAL)+;
                             " and MTCODMAT="+cp_ToStrODBC(MTCODMAT)+;
                             " and MTCODMAT="+cp_ToStrODBC(MTCODMAT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_MCOM')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MTCODLOT=this.w_MTCODLOT;
                     ,MTCODUBI=this.w_MTCODUBI;
                     ,MTSERRIF=this.w_MTSERRIF;
                     ,MTROWRIF=this.w_MTROWRIF;
                     ,MTRIFNUM=this.w_MTRIFNUM;
                     ,MT__FLAG=this.w_MT__FLAG;
                     ,MT_SALDO=this.w_MT_SALDO;
                     ,MTRIFSTO=this.w_MTRIFSTO;
                     ,MTNUMRIF=this.w_MTNUMRIF;
                     ,MTCODCOM=this.w_MTCODCOM;
                     ,MTKEYSAL=this.w_MTKEYSAL;
                     ,MTCODMAT=this.w_MTCODMAT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MTKEYSAL=&i_TN.->MTKEYSAL;
                                      and MTCODMAT=&i_TN.->MTCODMAT;
                                      and MTCODMAT=&i_TN.->MTCODMAT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MT__FLAG,space(1)),'AM_PRENO','', 1,'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MTCODMAT,space(40))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" AM_PRENO="+i_cOp1+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE AMCODICE="+cp_ToStrODBC(NVL(&i_cF..MTCODMAT,space(40)));
             +" AND AMKEYSAL="+cp_ToStrODBC(NVL(&i_cF..MTKEYSAL,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MT__FLAG,'AM_PRENO',' 1', 1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'AMKEYSAL',&i_cF..MTKEYSAL;
                 ,'AMCODICE',&i_cF..MTCODMAT)
      UPDATE (i_cTable) SET ;
           AM_PRENO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MT__FLAG,space(1))==this.w_MT__FLAG;
              and NVL(&i_cF..MTCODMAT,space(40))==this.w_MTCODMAT;
              and NVL(&i_cF..MTKEYSAL,space(20))==this.w_MTKEYSAL;

      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'AM_PRENO',' 1', 1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MTCODMAT)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" AM_PRENO="+i_cOp1  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE AMCODICE="+cp_ToStrODBC(this.w_MTCODMAT);
           +" AND AMKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
           )
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MT__FLAG,'AM_PRENO',' 1', 1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'AMKEYSAL',this.w_MTKEYSAL;
                 ,'AMCODICE',this.w_MTCODMAT)
      UPDATE (i_cTable) SET;
           AM_PRENO=&i_cOp1.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MTCODMAT)) and not empty(t_MTKEYSAL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIC_MCOM
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MTKEYSAL="+cp_ToStrODBC(&i_TN.->MTKEYSAL)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and MTCODMAT="+cp_ToStrODBC(&i_TN.->MTCODMAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MTKEYSAL=&i_TN.->MTKEYSAL;
                              and MTCODMAT=&i_TN.->MTCODMAT;
                              and MTCODMAT=&i_TN.->MTCODMAT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MTCODMAT)) and not empty(t_MTKEYSAL)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_MCOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_MCOM_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_MTKEYSAL = .oParentObject.w_MTKEYSAL
        .DoRTCalc(4,4,.t.)
          .link_2_3('Full')
          .w_FLLOTT = .oParentObject.w_FLLOTT
        .DoRTCalc(7,10,.t.)
          .w_CODART = .oParentObject.w_CODART
          .w_QTAUM1 = .oParentObject.w_MTQTAMOV
          .w_MGUBIC = .w_MGUBIC
        if .o_MTCODMAT<>.w_MTCODMAT
          .w_TOTALE = .w_TOTALE-.w_riga
          .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
          .w_TOTALE = .w_TOTALE+.w_riga
        endif
        .DoRTCalc(15,17,.t.)
          .w_CODMAG = .w_MTMAGSCA
          .w_ESIRIS = .w_ESIRIS
          .w_KEYSAL = .oParentObject.w_MTKEYSAL
        .DoRTCalc(21,23,.t.)
          .w_CODFOR = .w_CODFOR
        .DoRTCalc(25,27,.t.)
        if .o_MTCODLOT<>.w_MTCODLOT
          .w_LOTZOOM = .F.
        endif
          .w_MAGUBI = .w_MAGUBI
          .w_SERRIF = .w_SERRIF
          .w_ROWRIF = .w_ROWRIF
          .w_FLRISE = .w_FLRISE
          .w_NUMRIF = .w_NUMRIF
        .DoRTCalc(34,34,.t.)
          .w_DATREG = .w_DATREG
        .DoRTCalc(36,41,.t.)
          .w_FLPRG = .w_FLPRG
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(43,46,.t.)
          .w_DESART = .oParentObject.w_DESART
          .w_UNIMIS1 = .oParentObject.w_MTUNIMIS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(49,54,.t.)
          .w_COMCAR = .w_COMCAR
          .w_COMSCA = .w_COMSCA
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MTKEYSAL with this.w_MTKEYSAL
      replace t_MTCODMAT with this.w_MTCODMAT
      replace t_FLLOTT with this.w_FLLOTT
      replace t_MTCODUBI with this.w_MTCODUBI
      replace t_MTSERRIF with this.w_MTSERRIF
      replace t_MTROWRIF with this.w_MTROWRIF
      replace t_RIGA with this.w_RIGA
      replace t_MTRIFNUM with this.w_MTRIFNUM
      replace t_MT__FLAG with this.w_MT__FLAG
      replace t_MT_SALDO with this.w_MT_SALDO
      replace t_ARTLOT with this.w_ARTLOT
      replace t_VARLOT with this.w_VARLOT
      replace t_STALOT with this.w_STALOT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_MTRIFSTO with this.w_MTRIFSTO
      replace t_MTCODCOM with this.w_MTCODCOM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODLOT_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMTCODLOT_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MTCODMAT
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_MTCODMAT);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_MTKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_MTKEYSAL;
                       ,'AMCODICE',this.w_MTCODMAT)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODMAT = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODMAT = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MTCODLOT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MTCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_MTCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_CODART;
                     ,'LOCODICE',trim(this.w_MTCODLOT))
          select LOCODART,LOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MTCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MTCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oMTCODLOT_2_5'),i_cWhere,'GSAR_ALO',"Elenco lotti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MTCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MTCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_CODART;
                       ,'LOCODICE',this.w_MTCODLOT)
            select LOCODART,LOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MTCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_ARTLOT = NVL(_Link_.LOCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MTCODLOT = space(20)
      endif
      this.w_ARTLOT = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARTLOT=.w_CODART
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Lotto inesistente o di un altro articolo")
        endif
        this.w_MTCODLOT = space(20)
        this.w_ARTLOT = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MTCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_36.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_36.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value==this.w_MTCODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value=this.w_MTCODMAT
      replace t_MTCODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODMAT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_5.value==this.w_MTCODLOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_5.value=this.w_MTCODLOT
      replace t_MTCODLOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'DIC_MCOM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_QTAUM1=.w_TOTALE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Il numero di matricole movimentate deve coincidere con la quantit� di riga."))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_MTCODLOT) or not(.w_ARTLOT=.w_CODART)) and (g_PERLOT='S' AND .w_FLLOTT='S' And .w_CODMAG=.w_MTMAGCAR And .w_ESIRIS='E' And .w_MT_SALDO=0) and (not(Empty(.w_MTCODMAT)) and not empty(.w_MTKEYSAL))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMTCODLOT_2_5
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Lotto inesistente o di un altro articolo")
      endcase
      if not(Empty(.w_MTCODMAT)) and not empty(.w_MTKEYSAL)
        * --- Area Manuale = Check Row
        * --- gsco_mcm
        * Controllo che la matricola inserita non sia gia presente
        * nel dettaglio
        Local L_Posizione,L_Area,L_CodMat,L_Trovato
        
        L_Area = Select()
        
        Select (this.cTrsName)
        L_Posizione=Recno(this.cTrsName)
        L_CodMat= t_MTCODMAT
        
        if L_Posizione<>0 And Not Empty(L_CodMat)
        
        Go Top
        
        LOCATE FOR t_MTCODMAT = L_CodMat And Not Deleted()
        
        L_Trovato=Found()
        
        if L_Trovato And Recno()<>L_Posizione
                  i_bRes = .f.
                  i_bnoChk = .f.
                  i_cErrorMsg = ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
        ELse
                  if L_Trovato
                   Continue
                   if Found() And Recno()<>L_Posizione
                    i_bRes = .f.
                    i_bnoChk = .f.
                    i_cErrorMsg = ah_MsgFormat("Matricola gi� utilizzata nel dettaglio")
                   endif
                  Endif
        Endif
        
        * mi riposiziono nella riga di partenza
        Select (this.cTrsName)
        Go L_Posizione
        endif
        
        * mi rimetto nella vecchia area
        Select (L_Area)
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MTCODMAT = this.w_MTCODMAT
    this.o_MTCODLOT = this.w_MTCODLOT
    this.o_CODART = this.w_CODART
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MTCODMAT)) and not empty(t_MTKEYSAL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MTKEYSAL=space(20)
      .w_MTCODMAT=space(40)
      .w_MTCODMAT=space(40)
      .w_FLLOTT=space(1)
      .w_MTCODLOT=space(20)
      .w_MTCODUBI=space(20)
      .w_MTSERRIF=space(10)
      .w_MTROWRIF=0
      .w_RIGA=0
      .w_MTRIFNUM=0
      .w_MT__FLAG=space(1)
      .w_MT_SALDO=0
      .w_ARTLOT=space(20)
      .w_VARLOT=space(20)
      .w_STALOT=space(1)
      .w_LOTZOOM=.f.
      .w_MTRIFSTO=0
      .w_MTCODCOM=space(15)
      .DoRTCalc(1,2,.f.)
        .w_MTKEYSAL = .oParentObject.w_MTKEYSAL
      .DoRTCalc(4,5,.f.)
      if not(empty(.w_MTCODMAT))
        .link_2_3('Full')
      endif
        .w_FLLOTT = .oParentObject.w_FLLOTT
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_MTCODLOT))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,9,.f.)
        .w_MTROWRIF = 0
      .DoRTCalc(11,13,.f.)
        .w_RIGA = IIF( Empty( .w_MTCODMAT ) , 0 ,1 )
      .DoRTCalc(15,21,.f.)
        .w_MT__FLAG = ' '
      .DoRTCalc(23,27,.f.)
        .w_LOTZOOM = .F.
    endwith
    this.DoRTCalc(29,56,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MTKEYSAL = t_MTKEYSAL
    this.w_MTCODMAT = t_MTCODMAT
    this.w_MTCODMAT = t_MTCODMAT
    this.w_FLLOTT = t_FLLOTT
    this.w_MTCODLOT = t_MTCODLOT
    this.w_MTCODUBI = t_MTCODUBI
    this.w_MTSERRIF = t_MTSERRIF
    this.w_MTROWRIF = t_MTROWRIF
    this.w_RIGA = t_RIGA
    this.w_MTRIFNUM = t_MTRIFNUM
    this.w_MT__FLAG = t_MT__FLAG
    this.w_MT_SALDO = t_MT_SALDO
    this.w_ARTLOT = t_ARTLOT
    this.w_VARLOT = t_VARLOT
    this.w_STALOT = t_STALOT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_MTRIFSTO = t_MTRIFSTO
    this.w_MTCODCOM = t_MTCODCOM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MTKEYSAL with this.w_MTKEYSAL
    replace t_MTCODMAT with this.w_MTCODMAT
    replace t_MTCODMAT with this.w_MTCODMAT
    replace t_FLLOTT with this.w_FLLOTT
    replace t_MTCODLOT with this.w_MTCODLOT
    replace t_MTCODUBI with this.w_MTCODUBI
    replace t_MTSERRIF with this.w_MTSERRIF
    replace t_MTROWRIF with this.w_MTROWRIF
    replace t_RIGA with this.w_RIGA
    replace t_MTRIFNUM with this.w_MTRIFNUM
    replace t_MT__FLAG with this.w_MT__FLAG
    replace t_MT_SALDO with this.w_MT_SALDO
    replace t_ARTLOT with this.w_ARTLOT
    replace t_VARLOT with this.w_VARLOT
    replace t_STALOT with this.w_STALOT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_MTRIFSTO with this.w_MTRIFSTO
    replace t_MTCODCOM with this.w_MTCODCOM
    if i_srv='A'
      replace MTKEYSAL with this.w_MTKEYSAL
      replace MTCODMAT with this.w_MTCODMAT
      replace MTCODMAT with this.w_MTCODMAT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_riga
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsco_mcmPag1 as StdContainer
  Width  = 495
  height = 288
  stdWidth  = 495
  stdheight = 288
  resizeXpos=176
  resizeYpos=134
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_29 as cp_runprogram with uid="ZRZPFWLUDD",left=2, top=302, width=183,height=19,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CHANGED')",;
    cEvent = "w_MTCODMAT Changed",;
    nPag=1;
    , ToolTipText = "Al cambio della matricola effettua tutti i controlli pi� eventuali letture";
    , HelpContextID = 227573069


  add object oObj_1_30 as cp_runprogram with uid="ACNKDSSMKQ",left=2, top=320, width=183,height=19,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('DELETEROW')",;
    cEvent = "Delete row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 226893133


  add object oObj_1_31 as cp_runprogram with uid="AWQPAKNIED",left=2, top=338, width=183,height=19,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('UPDATEROW')",;
    cEvent = "Update row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la modifica della matricola";
    , HelpContextID = 226893133


  add object oObj_1_32 as cp_runprogram with uid="VECODULZZQ",left=190, top=301, width=183,height=19,;
    caption='GSCO_BCM',;
   bGlobalFont=.t.,;
    prg="GSCO_BCM('INSERTROW')",;
    cEvent = "Insert row start",;
    nPag=1;
    , ToolTipText = "Se il movimento non � l'ultimo impedisce la cancellazione della matricola";
    , HelpContextID = 226893133


  add object oObj_1_33 as cp_runprogram with uid="RIFVIWSAID",left=190, top=319, width=183,height=19,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CARRAPIDO')",;
    cEvent = "CarRapido",;
    nPag=1;
    , ToolTipText = "lancia il caricamento rapido";
    , HelpContextID = 227573069


  add object oObj_1_34 as cp_runprogram with uid="DHTIGXOYWX",left=190, top=337, width=183,height=19,;
    caption='GSMD_BGM',;
   bGlobalFont=.t.,;
    prg="GSMD_BGM('CARMAT')",;
    cEvent = "CarMat",;
    nPag=1;
    , ToolTipText = "lancia il caricamento dettaglio";
    , HelpContextID = 227573069

  add object oDESART_1_36 as StdField with uid="DZOJCGBEDM",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207825866,;
   bGlobalFont=.t.,;
    Height=19, Width=274, Left=61, Top=3, InputMask=replicate('X',40)


  add object oBtn_1_40 as StdButton with uid="BRHJBWXMYQ",left=8, top=239, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per attivare inserimento rapido";
    , HelpContextID = 237576154;
    , TabStop=.f.,Caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        .NotifyEvent("CarRapido")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTALE>=.w_QTAUM1)
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=27, width=478,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=2,Field1="MTCODMAT",Label1="Matricola",Field2="MTCODLOT",Label2="Lotto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49445766

  add object oStr_1_12 as StdString with uid="AHQVXNXIMF",Visible=.t., Left=2, Top=363,;
    Alignment=0, Width=427, Height=18,;
    Caption="Le variabili provenienti dal padre sono inizializzate con la funzione initson"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="QDOLLFGSCR",Visible=.t., Left=2, Top=384,;
    Alignment=0, Width=414, Height=18,;
    Caption="nell'area manuale <declare>. Le variabili cos� costruite vanno calcolate"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="PPFHKRJTGP",Visible=.t., Left=2, Top=401,;
    Alignment=0, Width=204, Height=18,;
    Caption="su se stesse senza dipendenze"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="DHQCBSBXJQ",Visible=.t., Left=17, Top=5,;
    Alignment=1, Width=43, Height=16,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=46,;
    width=474+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=47,width=473+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='LOTTIART|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='LOTTIART'
        oDropInto=this.oBodyCol.oRow.oMTCODLOT_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="HCNNONBZZQ",rtseq=43,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Numero totale di matricole movimentate",;
    HelpContextID = 197333194,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=385, Top=244

  add object oStr_3_2 as StdString with uid="PTKMNIEUIA",Visible=.t., Left=204, Top=245,;
    Alignment=1, Width=177, Height=18,;
    Caption="Totale matricole movimentate:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsco_mcmBodyRow as CPBodyRowCnt
  Width=464
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMTCODMAT_2_2 as StdTrsField with uid="SPOHKTCFXW",rtseq=4,rtrep=.t.,;
    cFormVar="w_MTCODMAT",value=space(40),isprimarykey=.t.,;
    HelpContextID = 70654950,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=298, Left=-2, Top=0, InputMask=replicate('X',40), bHasZoom = .t. 

  proc oMTCODMAT_2_2.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"ZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMTCODLOT_2_5 as StdTrsField with uid="YIIZNTYANT",rtseq=7,rtrep=.t.,;
    cFormVar="w_MTCODLOT",value=space(20),;
    HelpContextID = 181003290,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Lotto inesistente o di un altro articolo",;
   bGlobalFont=.t.,;
    Height=17, Width=159, Left=300, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , TabStop=.f., cLinkFile="LOTTIART", cZoomOnZoom="GSAR_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_CODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_MTCODLOT"

  func oMTCODLOT_2_5.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND .w_FLLOTT='S' And .w_CODMAG=.w_MTMAGCAR And .w_ESIRIS='E' And .w_MT_SALDO=0)
    endwith
  endfunc

  func oMTCODLOT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMTCODLOT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMTCODLOT_2_5.mZoom
      with this.Parent.oContained
        GSMD_BGM(this.Parent.oContained,"LOTTIZOOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMTCODLOT_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_CODART
     i_obj.w_LOCODICE=this.parent.oContained.w_MTCODLOT
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover
  * ---
  func oMTCODMAT_2_2.When()
    return(.t.)
  proc oMTCODMAT_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMTCODMAT_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsco_mcm','DIC_MCOM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MTSERIAL=DIC_MCOM.MTSERIAL";
  +" and "+i_cAliasName2+".MTROWNUM=DIC_MCOM.MTROWNUM";
  +" and "+i_cAliasName2+".MTROWODL=DIC_MCOM.MTROWODL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
