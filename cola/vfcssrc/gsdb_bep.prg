* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsdb_bep                                                        *
*              Eliminazione previsioni di vendita                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-20                                                      *
* Last revis.: 2012-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsdb_bep",oParentObject,m.pParam)
return(i_retval)

define class tgsdb_bep as StdBatch
  * --- Local variables
  pParam = space(1)
  PADRE = .NULL.
  w_TIPREC = space(1)
  w_CODDIS = space(20)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_QUANTI = 0
  w_RECEL = 0
  w_FDATA = ctod("  /  /  ")
  w_FRIFPAD = space(20)
  w_FCODART = space(20)
  w_FCODVAR = space(20)
  w_FQTAORIG = 0
  w_FQTARIF = 0
  w_RECDETT = 0
  * --- WorkFile variables
  VEN_PREV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione eventi da Eliminazione previsioni di vendita (GSDB_KEP)
    * --- pParam: 'I'  Interrogazione, 'E' Eliminazione previsioni
    this.PADRE = this.oParentObject
    NC = this.PADRE.w_ZOOM.cCursor
    do case
      case this.pParam="S"
        * --- Seleziona/deseleziona tutto
        if used(NC)
          UPDATE (NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
        endif
      case this.pParam="E"
        * --- Eliminazione previsioni di vendita
        if used(NC)
          Select * from (NC) where xChk=1 into cursor _elab_
          if reccount("_elab_") = 0
            ah_ErrorMsg("Selezionare almeno un record!",64)
          else
            Select _elab_ 
 scan
            this.w_RECEL = recno()
            this.w_CODART = left(NVL(PVCODART,""),20)
            * --- Delete from VEN_PREV
            i_nConn=i_TableProp[this.VEN_PREV_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VEN_PREV_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PVCODART = "+cp_ToStrODBC(this.w_CODART);
                    +" and PV__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                     )
            else
              delete from (i_cTable) where;
                    PVCODART = this.w_CODART;
                    and PV__ANNO = this.oParentObject.w_ANNO;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            Select _elab_ 
 GOTO this.w_RECEL
            endscan
            AH_ErrorMsg("Eliminazione eseguita con successo")
            this.PADRE.NotifyEvent("Interroga")     
          endif
          USE IN SELECT("_tmpexp_")
          USE IN SELECT("_elab_")
          USE IN SELECT("_elabdett_")
        else
          ah_ErrorMsg("Selezionare almeno un record!",64)
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VEN_PREV'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
